package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm4 extends jn4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ char[] f3972a;
    @DexIgnore
    public static /* final */ char[] b; // = {'T', 'N', '*', 'E'};
    @DexIgnore
    public static /* final */ char[] c; // = {'/', ':', '+', '.'};
    @DexIgnore
    public static /* final */ char d;

    /*
    static {
        char[] cArr = {'A', 'B', 'C', 'D'};
        f3972a = cArr;
        d = (char) cArr[0];
    }
    */

    @DexIgnore
    @Override // com.fossil.jn4
    public boolean[] c(String str) {
        int i;
        if (str.length() < 2) {
            str = d + str + d;
        } else {
            char upperCase = Character.toUpperCase(str.charAt(0));
            char upperCase2 = Character.toUpperCase(str.charAt(str.length() - 1));
            boolean a2 = vm4.a(f3972a, upperCase);
            boolean a3 = vm4.a(f3972a, upperCase2);
            boolean a4 = vm4.a(b, upperCase);
            boolean a5 = vm4.a(b, upperCase2);
            if (a2) {
                if (!a3) {
                    throw new IllegalArgumentException("Invalid start/end guards: " + str);
                }
            } else if (a4) {
                if (!a5) {
                    throw new IllegalArgumentException("Invalid start/end guards: " + str);
                }
            } else if (a3 || a5) {
                throw new IllegalArgumentException("Invalid start/end guards: " + str);
            } else {
                str = d + str + d;
            }
        }
        int i2 = 20;
        for (int i3 = 1; i3 < str.length() - 1; i3++) {
            if (Character.isDigit(str.charAt(i3)) || str.charAt(i3) == '-' || str.charAt(i3) == '$') {
                i2 += 9;
            } else if (vm4.a(c, str.charAt(i3))) {
                i2 += 10;
            } else {
                throw new IllegalArgumentException("Cannot encode : '" + str.charAt(i3) + '\'');
            }
        }
        boolean[] zArr = new boolean[((str.length() - 1) + i2)];
        int i4 = 0;
        for (int i5 = 0; i5 < str.length(); i5++) {
            char upperCase3 = Character.toUpperCase(str.charAt(i5));
            if (i5 == 0 || i5 == str.length() - 1) {
                if (upperCase3 == '*') {
                    upperCase3 = 'C';
                } else if (upperCase3 == 'E') {
                    upperCase3 = 'D';
                } else if (upperCase3 == 'N') {
                    upperCase3 = 'B';
                } else if (upperCase3 == 'T') {
                    upperCase3 = 'A';
                }
            }
            int i6 = 0;
            while (true) {
                char[] cArr = vm4.f3783a;
                if (i6 >= cArr.length) {
                    i = 0;
                    break;
                } else if (upperCase3 == cArr[i6]) {
                    i = vm4.b[i6];
                    break;
                } else {
                    i6++;
                }
            }
            int i7 = 0;
            int i8 = i4;
            boolean z = true;
            while (true) {
                int i9 = 0;
                while (true) {
                    i4 = i8;
                    if (i7 >= 7) {
                        break;
                    }
                    zArr[i4] = z;
                    i8 = i4 + 1;
                    if (((i >> (6 - i7)) & 1) == 0 || i9 == 1) {
                        z = !z;
                        i7++;
                    } else {
                        i9++;
                    }
                }
                z = !z;
                i7++;
            }
            if (i5 < str.length() - 1) {
                zArr[i4] = false;
                i4++;
            }
        }
        return zArr;
    }
}
