package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia8 {
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        com.fossil.so7.a(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0052, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File a(android.content.Context r8, java.lang.String r9, java.lang.String r10, int r11, boolean r12) {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r0 = "context"
            com.fossil.pq7.c(r8, r0)
            java.lang.String r0 = "assetId"
            com.fossil.pq7.c(r9, r0)
            java.lang.String r0 = "extName"
            com.fossil.pq7.c(r10, r0)
            java.io.File r1 = r7.b(r8, r9, r10, r12)
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x001c
            r0 = r1
        L_0x001b:
            return r0
        L_0x001c:
            android.content.ContentResolver r2 = r8.getContentResolver()
            r0 = 1
            if (r11 != r0) goto L_0x0045
            android.net.Uri r0 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r9)
        L_0x0029:
            if (r12 == 0) goto L_0x002f
            android.net.Uri r0 = android.provider.MediaStore.setRequireOriginal(r0)
        L_0x002f:
            java.io.InputStream r0 = r2.openInputStream(r0)
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r1)
            if (r0 == 0) goto L_0x0040
            r3 = 0
            r4 = 2
            r5 = 0
            com.fossil.ro7.b(r0, r2, r3, r4, r5)     // Catch:{ all -> 0x004c }
        L_0x0040:
            com.fossil.so7.a(r2, r6)
            r0 = r1
            goto L_0x001b
        L_0x0045:
            android.net.Uri r0 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r9)
            goto L_0x0029
        L_0x004c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r1 = move-exception
            com.fossil.so7.a(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ia8.a(android.content.Context, java.lang.String, java.lang.String, int, boolean):java.io.File");
    }

    @DexIgnore
    public final File b(Context context, String str, String str2, boolean z) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        pq7.c(str2, "displayName");
        String str3 = z ? "_origin" : "";
        return new File(context.getCacheDir(), str + str3 + '_' + str2);
    }

    @DexIgnore
    public final void c(Context context, ka8 ka8, byte[] bArr, boolean z) {
        pq7.c(context, "context");
        pq7.c(ka8, "asset");
        pq7.c(bArr, "byteArray");
        File b = b(context, ka8.e(), ka8.b(), z);
        if (b.exists()) {
            xa8.b(ka8.e() + " , isOrigin: " + z + ", cache file exists, ignore save");
            return;
        }
        File parentFile = b.getParentFile();
        if (parentFile == null || !parentFile.exists()) {
            b.mkdirs();
        }
        ap7.b(b, bArr);
        xa8.b(ka8.e() + " , isOrigin: " + z + ", cached");
    }
}
