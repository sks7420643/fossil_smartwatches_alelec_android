package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fossil.yh1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai1 implements yh1 {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ yh1.a c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ BroadcastReceiver f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ai1 ai1 = ai1.this;
            boolean z = ai1.d;
            ai1.d = ai1.c(context);
            if (z != ai1.this.d) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + ai1.this.d);
                }
                ai1 ai12 = ai1.this;
                ai12.c.a(ai12.d);
            }
        }
    }

    @DexIgnore
    public ai1(Context context, yh1.a aVar) {
        this.b = context.getApplicationContext();
        this.c = aVar;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public boolean c(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        ik1.d(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (RuntimeException e2) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e2);
            }
            return true;
        }
    }

    @DexIgnore
    public final void e() {
        if (!this.e) {
            this.d = c(this.b);
            try {
                this.b.registerReceiver(this.f, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.e = true;
            } catch (SecurityException e2) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e2);
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        if (this.e) {
            this.b.unregisterReceiver(this.f);
            this.e = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStart() {
        e();
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStop() {
        g();
    }
}
