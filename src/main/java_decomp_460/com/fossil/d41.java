package com.fossil;

import android.content.Context;
import android.os.PowerManager;
import java.util.HashMap;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f734a; // = x01.f("WakeLocks");
    @DexIgnore
    public static /* final */ WeakHashMap<PowerManager.WakeLock, String> b; // = new WeakHashMap<>();

    @DexIgnore
    public static void a() {
        HashMap hashMap = new HashMap();
        synchronized (b) {
            hashMap.putAll(b);
        }
        for (PowerManager.WakeLock wakeLock : hashMap.keySet()) {
            if (wakeLock != null && wakeLock.isHeld()) {
                x01.c().h(f734a, String.format("WakeLock held for %s", hashMap.get(wakeLock)), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public static PowerManager.WakeLock b(Context context, String str) {
        String str2 = "WorkManager: " + str;
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getApplicationContext().getSystemService("power")).newWakeLock(1, str2);
        synchronized (b) {
            b.put(newWakeLock, str2);
        }
        return newWakeLock;
    }
}
