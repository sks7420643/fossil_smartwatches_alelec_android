package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eg2 extends ke2 {
    @DexIgnore
    public int b;

    @DexIgnore
    public eg2(byte[] bArr) {
        rc2.a(bArr.length == 25);
        this.b = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] n(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof le2)) {
            try {
                le2 le2 = (le2) obj;
                if (le2.zzc() != hashCode()) {
                    return false;
                }
                rg2 zzb = le2.zzb();
                if (zzb == null) {
                    return false;
                }
                return Arrays.equals(i(), (byte[]) tg2.i(zzb));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    public abstract byte[] i();

    @DexIgnore
    @Override // com.fossil.le2
    public final rg2 zzb() {
        return tg2.n(i());
    }

    @DexIgnore
    @Override // com.fossil.le2
    public final int zzc() {
        return hashCode();
    }
}
