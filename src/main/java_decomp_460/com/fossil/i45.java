package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.buddy_challenge.customview.CircleProgressView;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i45 extends h45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131363010, 1);
        E.put(2131362527, 2);
        E.put(2131362425, 3);
        E.put(2131362029, 4);
        E.put(2131362392, 5);
        E.put(2131362382, 6);
        E.put(2131362389, 7);
        E.put(2131362504, 8);
        E.put(2131362508, 9);
        E.put(2131362381, 10);
        E.put(2131362428, 11);
    }
    */

    @DexIgnore
    public i45(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public i45(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (CircleProgressView) objArr[4], (TimerTextView) objArr[10], (FlexibleTextView) objArr[6], (TimerTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[2], (ConstraintLayout) objArr[1], (InterceptSwipe) objArr[0]);
        this.C = -1;
        this.B.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
