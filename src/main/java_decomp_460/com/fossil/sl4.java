package com.fossil;

import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl4 implements ql4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Charset f3277a; // = Charset.forName("ISO-8859-1");

    @DexIgnore
    public static bm4 b(String str, kl4 kl4, int i, int i2, Charset charset, int i3, int i4) {
        if (kl4 == kl4.AZTEC) {
            return c(vl4.d(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got " + kl4);
    }

    @DexIgnore
    public static bm4 c(tl4 tl4, int i, int i2) {
        bm4 a2 = tl4.a();
        if (a2 != null) {
            int l = a2.l();
            int j = a2.j();
            int max = Math.max(i, l);
            int max2 = Math.max(i2, j);
            int min = Math.min(max / l, max2 / j);
            int i3 = (max - (l * min)) / 2;
            bm4 bm4 = new bm4(max, max2);
            int i4 = (max2 - (j * min)) / 2;
            for (int i5 = 0; i5 < j; i5++) {
                int i6 = 0;
                int i7 = i3;
                while (i6 < l) {
                    if (a2.i(i6, i5)) {
                        bm4.o(i7, i4, min, min);
                    }
                    i6++;
                    i7 += min;
                }
                i4 += min;
            }
            return bm4;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    @Override // com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) {
        int i3;
        int i4;
        Charset charset;
        int i5 = 33;
        Charset charset2 = f3277a;
        if (map != null) {
            if (map.containsKey(ml4.CHARACTER_SET)) {
                charset2 = Charset.forName(map.get(ml4.CHARACTER_SET).toString());
            }
            if (map.containsKey(ml4.ERROR_CORRECTION)) {
                i5 = Integer.parseInt(map.get(ml4.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(ml4.AZTEC_LAYERS)) {
                i3 = Integer.parseInt(map.get(ml4.AZTEC_LAYERS).toString());
                i4 = i5;
                charset = charset2;
                return b(str, kl4, i, i2, charset, i4, i3);
            }
        }
        i3 = 0;
        i4 = i5;
        charset = charset2;
        return b(str, kl4, i, i2, charset, i4, i3);
    }
}
