package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga4 extends ta4.d.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1281a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.c.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f1282a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c a() {
            String str = "";
            if (this.f1282a == null) {
                str = " arch";
            }
            if (this.b == null) {
                str = str + " model";
            }
            if (this.c == null) {
                str = str + " cores";
            }
            if (this.d == null) {
                str = str + " ram";
            }
            if (this.e == null) {
                str = str + " diskSpace";
            }
            if (this.f == null) {
                str = str + " simulator";
            }
            if (this.g == null) {
                str = str + " state";
            }
            if (this.h == null) {
                str = str + " manufacturer";
            }
            if (this.i == null) {
                str = str + " modelClass";
            }
            if (str.isEmpty()) {
                return new ga4(this.f1282a.intValue(), this.b, this.c.intValue(), this.d.longValue(), this.e.longValue(), this.f.booleanValue(), this.g.intValue(), this.h, this.i);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a b(int i2) {
            this.f1282a = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a c(int i2) {
            this.c = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a d(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a e(String str) {
            if (str != null) {
                this.h = str;
                return this;
            }
            throw new NullPointerException("Null manufacturer");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null model");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a g(String str) {
            if (str != null) {
                this.i = str;
                return this;
            }
            throw new NullPointerException("Null modelClass");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a h(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a i(boolean z) {
            this.f = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.c.a
        public ta4.d.c.a j(int i2) {
            this.g = Integer.valueOf(i2);
            return this;
        }
    }

    @DexIgnore
    public ga4(int i2, String str, int i3, long j, long j2, boolean z, int i4, String str2, String str3) {
        this.f1281a = i2;
        this.b = str;
        this.c = i3;
        this.d = j;
        this.e = j2;
        this.f = z;
        this.g = i4;
        this.h = str2;
        this.i = str3;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public int b() {
        return this.f1281a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public int c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public long d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public String e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.c)) {
            return false;
        }
        ta4.d.c cVar = (ta4.d.c) obj;
        return this.f1281a == cVar.b() && this.b.equals(cVar.f()) && this.c == cVar.c() && this.d == cVar.h() && this.e == cVar.d() && this.f == cVar.j() && this.g == cVar.i() && this.h.equals(cVar.e()) && this.i.equals(cVar.g());
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public String f() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public String g() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public long h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = this.f1281a;
        int hashCode = this.b.hashCode();
        int i3 = this.c;
        long j = this.d;
        int i4 = (int) (j ^ (j >>> 32));
        long j2 = this.e;
        return (((((((this.f ? 1231 : 1237) ^ ((((((((((i2 ^ 1000003) * 1000003) ^ hashCode) * 1000003) ^ i3) * 1000003) ^ i4) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003)) * 1000003) ^ this.g) * 1000003) ^ this.h.hashCode()) * 1000003) ^ this.i.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public int i() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.c
    public boolean j() {
        return this.f;
    }

    @DexIgnore
    public String toString() {
        return "Device{arch=" + this.f1281a + ", model=" + this.b + ", cores=" + this.c + ", ram=" + this.d + ", diskSpace=" + this.e + ", simulator=" + this.f + ", state=" + this.g + ", manufacturer=" + this.h + ", modelClass=" + this.i + "}";
    }
}
