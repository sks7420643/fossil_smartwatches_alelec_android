package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.ta4;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sb4 {
    @DexIgnore
    public static /* final */ Charset g; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ int h; // = 15;
    @DexIgnore
    public static /* final */ cb4 i; // = new cb4();
    @DexIgnore
    public static /* final */ Comparator<? super File> j; // = qb4.a();
    @DexIgnore
    public static /* final */ FilenameFilter k; // = rb4.a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicInteger f3231a; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ rc4 f;

    @DexIgnore
    public sb4(File file, rc4 rc4) {
        File file2 = new File(file, "report-persistence");
        this.b = new File(file2, Constants.SESSIONS);
        this.c = new File(file2, "priority-reports");
        this.d = new File(file2, "reports");
        this.e = new File(file2, "native-reports");
        this.f = rc4;
    }

    @DexIgnore
    public static File D(File file) throws IOException {
        if (y(file)) {
            return file;
        }
        throw new IOException("Could not create directory " + file);
    }

    @DexIgnore
    public static String E(File file) throws IOException {
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        FileInputStream fileInputStream = new FileInputStream(file);
        while (true) {
            try {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    String str = new String(byteArrayOutputStream.toByteArray(), g);
                    fileInputStream.close();
                    return str;
                }
            } catch (Throwable th) {
            }
        }
        throw th;
    }

    @DexIgnore
    public static void F(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                for (File file2 : file.listFiles()) {
                    F(file2);
                }
            }
            file.delete();
        }
    }

    @DexIgnore
    public static List<File> G(List<File>... listArr) {
        for (List<File> list : listArr) {
            Collections.sort(list, j);
        }
        return f(listArr);
    }

    @DexIgnore
    public static void H(File file, File file2, ta4.c cVar, String str) {
        try {
            ta4 n = i.D(E(file)).n(cVar);
            D(file2);
            L(new File(file2, str), i.E(n));
        } catch (IOException e2) {
            x74 f2 = x74.f();
            f2.c("Could not synthesize final native report file for " + file, e2);
        }
    }

    @DexIgnore
    public static void J(File file, File file2, List<ta4.d.AbstractC0224d> list, long j2, boolean z, String str) {
        try {
            ta4 m = i.D(E(file)).o(j2, z, str).m(ua4.a(list));
            ta4.d j3 = m.j();
            if (j3 != null) {
                D(file2);
                L(new File(file2, j3.h()), i.E(m));
            }
        } catch (IOException e2) {
            x74 f2 = x74.f();
            f2.c("Could not synthesize final report file for " + file, e2);
        }
    }

    @DexIgnore
    public static int K(File file, int i2) {
        List<File> p = p(file, ob4.a());
        Collections.sort(p, pb4.a());
        return d(p, i2);
    }

    @DexIgnore
    public static void L(File file, String str) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), g);
        try {
            outputStreamWriter.write(str);
            outputStreamWriter.close();
            return;
        } catch (Throwable th) {
        }
        throw th;
    }

    @DexIgnore
    public static int d(List<File> list, int i2) {
        int size = list.size();
        for (File file : list) {
            if (size <= i2) {
                break;
            }
            F(file);
            size--;
        }
        return size;
    }

    @DexIgnore
    public static List<File> f(List<File>... listArr) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        for (List<File> list : listArr) {
            i2 += list.size();
        }
        arrayList.ensureCapacity(i2);
        for (List<File> list2 : listArr) {
            arrayList.addAll(list2);
        }
        return arrayList;
    }

    @DexIgnore
    public static String k(int i2, boolean z) {
        String format = String.format(Locale.US, "%010d", Integer.valueOf(i2));
        String str = z ? LocaleConverter.LOCALE_DELIMITER : "";
        return Constants.EVENT + format + str;
    }

    @DexIgnore
    public static List<File> l(File file) {
        return o(file, null);
    }

    @DexIgnore
    public static String n(String str) {
        return str.substring(0, h);
    }

    @DexIgnore
    public static List<File> o(File file, FileFilter fileFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = fileFilter == null ? file.listFiles() : file.listFiles(fileFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    @DexIgnore
    public static List<File> p(File file, FilenameFilter filenameFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = filenameFilter == null ? file.listFiles() : file.listFiles(filenameFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    @DexIgnore
    public static boolean r(String str) {
        return str.startsWith(Constants.EVENT) && str.endsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public static boolean s(File file, String str) {
        return str.startsWith(Constants.EVENT) && !str.endsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public static /* synthetic */ boolean t(String str, File file) {
        return file.isDirectory() && !file.getName().equals(str);
    }

    @DexIgnore
    public static boolean y(File file) {
        return file.exists() || file.mkdirs();
    }

    @DexIgnore
    public static int z(File file, File file2) {
        return n(file.getName()).compareTo(n(file2.getName()));
    }

    @DexIgnore
    public void A(ta4.d.AbstractC0224d dVar, String str, boolean z) {
        int i2 = this.f.b().b().f4303a;
        File q = q(str);
        try {
            L(new File(q, k(this.f3231a.getAndIncrement(), z)), i.i(dVar));
        } catch (IOException e2) {
            x74 f2 = x74.f();
            f2.c("Could not persist event for session " + str, e2);
        }
        K(q, i2);
    }

    @DexIgnore
    public void B(ta4 ta4) {
        ta4.d j2 = ta4.j();
        if (j2 == null) {
            x74.f().b("Could not get session for report");
            return;
        }
        String h2 = j2.h();
        try {
            File q = q(h2);
            D(q);
            L(new File(q, "report"), i.E(ta4));
        } catch (IOException e2) {
            x74 f2 = x74.f();
            f2.c("Could not persist report for session " + h2, e2);
        }
    }

    @DexIgnore
    public void C(String str, String str2) {
        try {
            L(new File(q(str2), "user"), str);
        } catch (IOException e2) {
            x74 f2 = x74.f();
            f2.c("Could not persist user ID for session " + str2, e2);
        }
    }

    @DexIgnore
    public final void I(File file, long j2) {
        boolean z;
        List<File> p = p(file, k);
        if (!p.isEmpty()) {
            Collections.sort(p);
            ArrayList arrayList = new ArrayList();
            Iterator<File> it = p.iterator();
            loop0:
            while (true) {
                z = false;
                while (true) {
                    if (!it.hasNext()) {
                        break loop0;
                    }
                    File next = it.next();
                    try {
                        arrayList.add(i.h(E(next)));
                        if (z || r(next.getName())) {
                            z = true;
                        }
                    } catch (IOException e2) {
                        x74 f2 = x74.f();
                        f2.c("Could not add event to report for " + next, e2);
                    }
                }
            }
            String str = null;
            File file2 = new File(file, "user");
            if (file2.isFile()) {
                try {
                    str = E(file2);
                } catch (IOException e3) {
                    x74 f3 = x74.f();
                    f3.c("Could not read user ID file in " + file.getName(), e3);
                }
            }
            J(new File(file, "report"), z ? this.c : this.d, arrayList, j2, z, str);
        }
    }

    @DexIgnore
    public final List<File> c(String str) {
        List<File> o = o(this.b, nb4.a(str));
        Collections.sort(o, j);
        if (o.size() <= 8) {
            return o;
        }
        for (File file : o.subList(8, o.size())) {
            F(file);
        }
        return o.subList(0, 8);
    }

    @DexIgnore
    public final void e() {
        int i2 = this.f.b().b().b;
        List<File> m = m();
        int size = m.size();
        if (size > i2) {
            for (File file : m.subList(i2, size)) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public void g() {
        for (File file : m()) {
            file.delete();
        }
    }

    @DexIgnore
    public void h(String str) {
        FilenameFilter a2 = mb4.a(str);
        for (File file : f(p(this.c, a2), p(this.e, a2), p(this.d, a2))) {
            file.delete();
        }
    }

    @DexIgnore
    public void i(String str, long j2) {
        for (File file : c(str)) {
            I(file, j2);
            F(file);
        }
        e();
    }

    @DexIgnore
    public void j(String str, ta4.c cVar) {
        H(new File(q(str), "report"), this.e, cVar, str);
    }

    @DexIgnore
    public final List<File> m() {
        return G(f(l(this.c), l(this.e)), l(this.d));
    }

    @DexIgnore
    public final File q(String str) {
        return new File(this.b, str);
    }

    @DexIgnore
    public List<z84> x() {
        List<File> m = m();
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(m.size());
        for (File file : m()) {
            try {
                arrayList.add(z84.a(i.D(E(file)), file.getName()));
            } catch (IOException e2) {
                x74 f2 = x74.f();
                f2.c("Could not load report file " + file + "; deleting", e2);
                file.delete();
            }
        }
        return arrayList;
    }
}
