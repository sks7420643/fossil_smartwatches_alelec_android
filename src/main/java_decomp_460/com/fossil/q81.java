package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q81 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static boolean f2937a; // = false;
    @DexIgnore
    public static int b; // = 3;
    @DexIgnore
    public static /* final */ q81 c; // = new q81();

    @DexIgnore
    public final boolean a() {
        return f2937a;
    }

    @DexIgnore
    public final int b() {
        return b;
    }
}
