package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class w90 extends Enum<w90> {
    @DexIgnore
    public static /* final */ w90 c;
    @DexIgnore
    public static /* final */ w90 d;
    @DexIgnore
    public static /* final */ /* synthetic */ w90[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        w90 w90 = new w90("CLOCK_WISE", 0, (byte) 0);
        c = w90;
        w90 w902 = new w90("COUNTER_CLOCK_WISE", 1, (byte) 1);
        w90 w903 = new w90("SHORTEST_PATH", 2, (byte) 2);
        d = w903;
        e = new w90[]{w90, w902, w903};
    }
    */

    @DexIgnore
    public w90(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static w90 valueOf(String str) {
        return (w90) Enum.valueOf(w90.class, str);
    }

    @DexIgnore
    public static w90[] values() {
        return (w90[]) e.clone();
    }
}
