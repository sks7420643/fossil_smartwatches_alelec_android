package com.fossil;

import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xz3 implements yz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ yz3 f4217a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public xz3(float f, yz3 yz3) {
        yz3 yz32 = yz3;
        while (yz32 instanceof xz3) {
            yz3 yz33 = ((xz3) yz32).f4217a;
            f += ((xz3) yz33).b;
            yz32 = yz33;
        }
        this.f4217a = yz32;
        this.b = f;
    }

    @DexIgnore
    @Override // com.fossil.yz3
    public float a(RectF rectF) {
        return Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f4217a.a(rectF) + this.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof xz3)) {
            return false;
        }
        xz3 xz3 = (xz3) obj;
        return this.f4217a.equals(xz3.f4217a) && this.b == xz3.b;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f4217a, Float.valueOf(this.b)});
    }
}
