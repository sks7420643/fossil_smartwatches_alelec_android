package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ kp1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bu1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bu1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(uq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Wo\u2026class.java.classLoader)!!");
                return new bu1((uq1) readParcelable, (nt1) parcel.readParcelable(nt1.class.getClassLoader()), (kp1) parcel.readParcelable(kp1.class.getClassLoader()));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bu1[] newArray(int i) {
            return new bu1[i];
        }
    }

    @DexIgnore
    public bu1(uq1 uq1, kp1 kp1) {
        super(uq1, null);
        this.d = kp1;
    }

    @DexIgnore
    public bu1(uq1 uq1, nt1 nt1) {
        super(uq1, nt1);
        this.d = null;
    }

    @DexIgnore
    public bu1(uq1 uq1, nt1 nt1, kp1 kp1) {
        super(uq1, nt1);
        this.d = kp1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        JSONObject jSONObject;
        JSONObject a2 = super.a();
        kp1 kp1 = this.d;
        if (kp1 == null || (jSONObject = kp1.toJSONObject()) == null) {
            jSONObject = new JSONObject();
        }
        return gy1.c(a2, jSONObject);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            kp1 kp1 = this.d;
            if (kp1 != null) {
                JSONObject k = g80.k(new JSONObject(), jd0.H, kp1.getFileName());
                JSONObject jSONObject2 = new JSONObject();
                jd0 jd0 = jd0.H5;
                jq1 deviceRequest2 = getDeviceRequest();
                if (deviceRequest2 != null) {
                    jSONObject.put("workoutApp._.config.images", g80.k(g80.k(g80.k(jSONObject2, jd0, Long.valueOf(((uq1) deviceRequest2).getSessionId())), jd0.K5, k), jd0.O5, kp1.e().b));
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
                }
            }
            if (getDeviceMessage() != null) {
                jSONObject.put("workoutApp._.config.response", getDeviceMessage().toJSONObject());
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        pq7.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final kp1 b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(bu1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((bu1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WorkoutRouteImageData");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        kp1 kp1 = this.d;
        return (kp1 != null ? kp1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
