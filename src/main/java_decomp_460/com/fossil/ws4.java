package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws4 {
    @rj4("notificationId")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3991a;
    @DexIgnore
    @rj4("title-loc-key")
    public String b;
    @DexIgnore
    @rj4("createdAt")
    public Date c;
    @DexIgnore
    @rj4("loc-key")
    public String d;
    @DexIgnore
    @rj4("challenge")
    public ss4 e;
    @DexIgnore
    @rj4("profile")
    public lt4 f;
    @DexIgnore
    @rj4("confirm")
    public Boolean g;
    @DexIgnore
    @rj4("rank")
    public Integer h;

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final ss4 b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.f3991a;
    }

    @DexIgnore
    public final lt4 d() {
        return this.f;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ws4) {
                ws4 ws4 = (ws4) obj;
                if (!pq7.a(this.f3991a, ws4.f3991a) || !pq7.a(this.b, ws4.b) || !pq7.a(this.c, ws4.c) || !pq7.a(this.d, ws4.d) || !pq7.a(this.e, ws4.e) || !pq7.a(this.f, ws4.f) || !pq7.a(this.g, ws4.g) || !pq7.a(this.h, ws4.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f3991a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        Date date = this.c;
        int hashCode3 = date != null ? date.hashCode() : 0;
        String str3 = this.d;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        ss4 ss4 = this.e;
        int hashCode5 = ss4 != null ? ss4.hashCode() : 0;
        lt4 lt4 = this.f;
        int hashCode6 = lt4 != null ? lt4.hashCode() : 0;
        Boolean bool = this.g;
        int hashCode7 = bool != null ? bool.hashCode() : 0;
        Integer num = this.h;
        if (num != null) {
            i = num.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "FCMNotificationData(id=" + this.f3991a + ", titleLoc=" + this.b + ", createdAt=" + this.c + ", bodyLoc=" + this.d + ", challenge=" + this.e + ", profile=" + this.f + ", confirm=" + this.g + ", rank=" + this.h + ")";
    }
}
