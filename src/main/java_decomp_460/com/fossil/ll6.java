package com.fossil;

import com.fossil.jl6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ll6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2216a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[mi5.values().length];
        f2216a = iArr;
        iArr[mi5.RUNNING.ordinal()] = 1;
        f2216a[mi5.HIKING.ordinal()] = 2;
        f2216a[mi5.WALKING.ordinal()] = 3;
        f2216a[mi5.CYCLING.ordinal()] = 4;
        int[] iArr2 = new int[jl6.c.values().length];
        b = iArr2;
        iArr2[jl6.c.ACTIVE_TIME.ordinal()] = 1;
        b[jl6.c.STEPS.ordinal()] = 2;
        b[jl6.c.CALORIES.ordinal()] = 3;
        b[jl6.c.HEART_RATE.ordinal()] = 4;
    }
    */
}
