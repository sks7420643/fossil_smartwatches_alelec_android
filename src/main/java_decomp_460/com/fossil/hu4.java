package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1532a;
    @DexIgnore
    public /* final */ fu4 b;
    @DexIgnore
    public /* final */ gu4 c;
    @DexIgnore
    public /* final */ on5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {49}, m = "changeSocialId")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ hu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hu4 hu4, qn7 qn7) {
            super(qn7);
            this.this$0 = hu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {22}, m = "fetchSocialProfile")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ hu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hu4 hu4, qn7 qn7) {
            super(qn7);
            this.this$0 = hu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore
    public hu4(fu4 fu4, gu4 gu4, on5 on5) {
        pq7.c(fu4, "local");
        pq7.c(gu4, "remote");
        pq7.c(on5, "mSharedPreferencesManager");
        this.b = fu4;
        this.c = gu4;
        this.d = on5;
        String name = hu4.class.getName();
        pq7.b(name, "ProfileRepository::class.java.name");
        this.f1532a = name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r7, com.fossil.qn7<? super com.fossil.kz4<com.fossil.it4>> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r8 instanceof com.fossil.hu4.a
            if (r0 == 0) goto L_0x0068
            r0 = r8
            com.fossil.hu4$a r0 = (com.fossil.hu4.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0068
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0077
            if (r3 != r4) goto L_0x006f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.hu4 r0 = (com.fossil.hu4) r0
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002e:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x009f
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.it4 r0 = (com.fossil.it4) r0
            if (r0 != 0) goto L_0x0089
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.f1532a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "generateSocialProfile - Success - "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            r1.<init>(r2, r5)
            r0.<init>(r1)
        L_0x0067:
            return r0
        L_0x0068:
            com.fossil.hu4$a r0 = new com.fossil.hu4$a
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x006f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0077:
            com.fossil.el7.b(r2)
            com.fossil.gu4 r2 = r6.c
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r2.b(r7, r1)
            if (r1 != r0) goto L_0x002e
            goto L_0x0067
        L_0x0089:
            com.fossil.on5 r1 = r6.d
            java.lang.String r2 = r0.c()
            r1.G0(r2)
            com.fossil.fu4 r1 = r6.b
            r1.b(r0)
            com.fossil.kz4 r1 = new com.fossil.kz4
            r2 = 2
            r1.<init>(r0, r5, r2, r5)
            r0 = r1
            goto L_0x0067
        L_0x009f:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00d5
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.f1532a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "changeSocialId - Failure - "
            r3.append(r4)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r4 = r0.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            com.fossil.kz4 r1 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            int r0 = r0.a()
            r2.<init>(r0, r5)
            r1.<init>(r2)
            r0 = r1
            goto L_0x0067
        L_0x00d5:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hu4.a(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void b() {
        this.b.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.qn7<? super com.fossil.kz4<com.fossil.it4>> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r7 instanceof com.fossil.hu4.b
            if (r0 == 0) goto L_0x0046
            r0 = r7
            com.fossil.hu4$b r0 = (com.fossil.hu4.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0054
            if (r3 != r4) goto L_0x004c
            java.lang.Object r0 = r0.L$0
            com.fossil.hu4 r0 = (com.fossil.hu4) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x007b
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.it4 r0 = (com.fossil.it4) r0
            if (r0 != 0) goto L_0x0065
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            r1.<init>(r2, r5)
            r0.<init>(r1)
        L_0x0045:
            return r0
        L_0x0046:
            com.fossil.hu4$b r0 = new com.fossil.hu4$b
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.el7.b(r1)
            com.fossil.gu4 r1 = r6.c
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.c(r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0045
        L_0x0065:
            com.fossil.on5 r1 = r6.d
            java.lang.String r2 = r0.c()
            r1.G0(r2)
            com.fossil.fu4 r1 = r6.b
            r1.b(r0)
            com.fossil.kz4 r1 = new com.fossil.kz4
            r2 = 2
            r1.<init>(r0, r5, r2, r5)
            r0 = r1
            goto L_0x0045
        L_0x007b:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00b1
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.f1532a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "downloadProfile - Failure - "
            r3.append(r4)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r4 = r0.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            com.fossil.kz4 r1 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            int r0 = r0.a()
            r2.<init>(r0, r5)
            r1.<init>(r2)
            r0 = r1
            goto L_0x0045
        L_0x00b1:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hu4.c(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final it4 d() {
        return this.b.c();
    }

    @DexIgnore
    public final LiveData<it4> e() {
        return this.b.d();
    }
}
