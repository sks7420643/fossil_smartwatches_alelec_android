package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final i18 f1572a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements i18 {
        @DexIgnore
        @Override // com.fossil.i18
        public void a(q18 q18, List<h18> list) {
        }

        @DexIgnore
        @Override // com.fossil.i18
        public List<h18> b(q18 q18) {
            return Collections.emptyList();
        }
    }

    @DexIgnore
    void a(q18 q18, List<h18> list);

    @DexIgnore
    List<h18> b(q18 q18);
}
