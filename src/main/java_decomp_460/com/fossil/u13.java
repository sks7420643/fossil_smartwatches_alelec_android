package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u13 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public volatile m23 f3503a;
    @DexIgnore
    public volatile xz2 b;

    /*
    static {
        q03.a();
    }
    */

    @DexIgnore
    public final m23 a(m23 m23) {
        m23 m232 = this.f3503a;
        this.b = null;
        this.f3503a = m23;
        return m232;
    }

    @DexIgnore
    public final int b() {
        if (this.b != null) {
            return this.b.zza();
        }
        if (this.f3503a != null) {
            return this.f3503a.l();
        }
        return 0;
    }

    @DexIgnore
    public final m23 c(m23 m23) {
        if (this.f3503a == null) {
            synchronized (this) {
                if (this.f3503a == null) {
                    try {
                        this.f3503a = m23;
                        this.b = xz2.zza;
                    } catch (l13 e) {
                        this.f3503a = m23;
                        this.b = xz2.zza;
                    }
                }
            }
        }
        return this.f3503a;
    }

    @DexIgnore
    public final xz2 d() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            if (this.f3503a == null) {
                this.b = xz2.zza;
            } else {
                this.b = this.f3503a.g();
            }
            return this.b;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u13)) {
            return false;
        }
        u13 u13 = (u13) obj;
        m23 m23 = this.f3503a;
        m23 m232 = u13.f3503a;
        return (m23 == null && m232 == null) ? d().equals(u13.d()) : (m23 == null || m232 == null) ? m23 != null ? m23.equals(u13.c(m23.d())) : c(m232.d()).equals(m232) : m23.equals(m232);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }
}
