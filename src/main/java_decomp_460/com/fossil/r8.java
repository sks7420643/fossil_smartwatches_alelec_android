package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum r8 {
    FIRE_TIME((byte) 0),
    TITLE((byte) 1),
    MESSAGE((byte) 2);
    
    @DexIgnore
    public static /* final */ q8 g; // = new q8(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public r8(byte b2) {
        this.b = (byte) b2;
    }
}
