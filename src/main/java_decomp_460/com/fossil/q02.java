package com.fossil;

import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q02 extends v02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ v02.a f2903a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public q02(v02.a aVar, long j) {
        if (aVar != null) {
            this.f2903a = aVar;
            this.b = j;
            return;
        }
        throw new NullPointerException("Null status");
    }

    @DexIgnore
    @Override // com.fossil.v02
    public long b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v02
    public v02.a c() {
        return this.f2903a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v02)) {
            return false;
        }
        v02 v02 = (v02) obj;
        return this.f2903a.equals(v02.c()) && this.b == v02.b();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f2903a.hashCode();
        long j = this.b;
        return ((hashCode ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "BackendResponse{status=" + this.f2903a + ", nextRequestWaitMillis=" + this.b + "}";
    }
}
