package com.fossil;

import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl4 {
    @DexIgnore
    public static /* final */ yl4 e; // = new yl4(zl4.b, 0, 0, 0);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f4333a;
    @DexIgnore
    public /* final */ zl4 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public yl4(zl4 zl4, int i, int i2, int i3) {
        this.b = zl4;
        this.f4333a = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public yl4 a(int i) {
        zl4 zl4 = this.b;
        int i2 = this.f4333a;
        int i3 = this.d;
        if (i2 == 4 || i2 == 2) {
            int i4 = wl4.c[i2][0];
            int i5 = i4 >> 16;
            zl4 = zl4.a(i4 & 65535, i5);
            i3 += i5;
            i2 = 0;
        }
        int i6 = this.c;
        yl4 yl4 = new yl4(zl4, i2, this.c + 1, i3 + ((i6 == 0 || i6 == 31) ? 18 : i6 == 62 ? 9 : 8));
        return yl4.c == 2078 ? yl4.b(i + 1) : yl4;
    }

    @DexIgnore
    public yl4 b(int i) {
        int i2 = this.c;
        return i2 == 0 ? this : new yl4(this.b.b(i - i2, i2), this.f4333a, 0, this.d);
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.f4333a;
    }

    @DexIgnore
    public boolean f(yl4 yl4) {
        int i;
        int i2 = this.d + (wl4.c[this.f4333a][yl4.f4333a] >> 16);
        int i3 = yl4.c;
        if (i3 > 0 && ((i = this.c) == 0 || i > i3)) {
            i2 += 10;
        }
        return i2 <= yl4.d;
    }

    @DexIgnore
    public yl4 g(int i, int i2) {
        zl4 zl4;
        int i3 = this.d;
        zl4 zl42 = this.b;
        int i4 = this.f4333a;
        if (i != i4) {
            int i5 = wl4.c[i4][i];
            int i6 = i5 >> 16;
            i3 += i6;
            zl4 = zl42.a(i5 & 65535, i6);
        } else {
            zl4 = zl42;
        }
        int i7 = i == 2 ? 4 : 5;
        return new yl4(zl4.a(i2, i7), i, 0, i7 + i3);
    }

    @DexIgnore
    public yl4 h(int i, int i2) {
        zl4 zl4 = this.b;
        int i3 = this.f4333a == 2 ? 4 : 5;
        return new yl4(zl4.a(wl4.e[this.f4333a][i], i3).a(i2, 5), this.f4333a, 0, i3 + this.d + 5);
    }

    @DexIgnore
    public am4 i(byte[] bArr) {
        LinkedList<zl4> linkedList = new LinkedList();
        for (zl4 zl4 = b(bArr.length).b; zl4 != null; zl4 = zl4.d()) {
            linkedList.addFirst(zl4);
        }
        am4 am4 = new am4();
        for (zl4 zl42 : linkedList) {
            zl42.c(am4, bArr);
        }
        return am4;
    }

    @DexIgnore
    public String toString() {
        return String.format("%s bits=%d bytes=%d", wl4.b[this.f4333a], Integer.valueOf(this.d), Integer.valueOf(this.c));
    }
}
