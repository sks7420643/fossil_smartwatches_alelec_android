package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class di4 implements Parcelable.Creator<ci4> {
    @DexIgnore
    public static void c(ci4 ci4, Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.e(parcel, 2, ci4.b, false);
        bd2.b(parcel, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public ci4 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 2) {
                ad2.B(parcel, t);
            } else {
                bundle = ad2.a(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ci4(bundle);
    }

    @DexIgnore
    /* renamed from: b */
    public ci4[] newArray(int i) {
        return new ci4[i];
    }
}
