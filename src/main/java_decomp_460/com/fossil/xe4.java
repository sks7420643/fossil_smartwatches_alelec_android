package com.fossil;

import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xe4 implements ht3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CountDownLatch f4113a;

    @DexIgnore
    public xe4(CountDownLatch countDownLatch) {
        this.f4113a = countDownLatch;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3 nt3) {
        this.f4113a.countDown();
    }
}
