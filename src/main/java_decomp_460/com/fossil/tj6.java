package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.bn6;
import com.fossil.n04;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj6 extends pv5 implements sj6 {
    @DexIgnore
    public g37<jb5> g;
    @DexIgnore
    public rj6 h;
    @DexIgnore
    public bn6 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ String k; // = qn5.l.a().d("nonBrandSurface");
    @DexIgnore
    public /* final */ String l; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String m; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tj6 f3425a;
        @DexIgnore
        public /* final */ /* synthetic */ jb5 b;

        @DexIgnore
        public c(tj6 tj6, jb5 jb5) {
            this.f3425a = tj6;
            this.b = jb5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayFragment", "onPageScrolled " + i);
            if (!TextUtils.isEmpty(this.f3425a.m)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepOverviewDayFragment", "set icon color " + this.f3425a.m);
                int parseColor = Color.parseColor(this.f3425a.m);
                TabLayout.g v = this.b.r.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.f3425a.l) && this.f3425a.j != i) {
                int parseColor2 = Color.parseColor(this.f3425a.l);
                TabLayout.g v2 = this.b.r.v(this.f3425a.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.f3425a.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tj6 f3426a;

        @DexIgnore
        public d(tj6 tj6) {
            this.f3426a = tj6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f3426a.l) && !TextUtils.isEmpty(this.f3426a.m)) {
                int parseColor = Color.parseColor(this.f3426a.l);
                int parseColor2 = Color.parseColor(this.f3426a.m);
                gVar.o(2131230966);
                if (i == this.f3426a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void O6(jb5 jb5) {
        jb5.s.setOnClickListener(a.b);
        String str = this.k;
        if (str != null) {
            jb5.v.setBackgroundColor(Color.parseColor(str));
            jb5.q.setBackgroundColor(Color.parseColor(str));
        }
        jb5.t.setOnClickListener(b.b);
        this.i = new bn6(new ArrayList());
        ViewPager2 viewPager2 = jb5.u;
        pq7.b(viewPager2, "binding.rvSleeps");
        viewPager2.setAdapter(this.i);
        jb5.u.g(new c(this, jb5));
        new n04(jb5.r, jb5.u, new d(this)).a();
        ViewPager2 viewPager22 = jb5.u;
        pq7.b(viewPager22, "binding.rvSleeps");
        viewPager22.setCurrentItem(this.j);
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(rj6 rj6) {
        pq7.c(rj6, "presenter");
        this.h = rj6;
    }

    @DexIgnore
    @Override // com.fossil.sj6
    public void S4(List<bn6.b> list) {
        jb5 a2;
        TabLayout tabLayout;
        jb5 a3;
        TabLayout tabLayout2;
        jb5 a4;
        FlexibleTextView flexibleTextView;
        jb5 a5;
        FlexibleTextView flexibleTextView2;
        pq7.c(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            g37<jb5> g37 = this.g;
            if (g37 != null && (a5 = g37.a()) != null && (flexibleTextView2 = a5.v) != null) {
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        g37<jb5> g372 = this.g;
        if (!(g372 == null || (a4 = g372.a()) == null || (flexibleTextView = a4.v) == null)) {
            flexibleTextView.setVisibility(8);
        }
        if (list.size() > 1) {
            g37<jb5> g373 = this.g;
            if (!(g373 == null || (a3 = g373.a()) == null || (tabLayout2 = a3.r) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            g37<jb5> g374 = this.g;
            if (!(g374 == null || (a2 = g374.a()) == null || (tabLayout = a2.r) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        bn6 bn6 = this.i;
        if (bn6 != null) {
            bn6.i(list);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        jb5 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        jb5 jb5 = (jb5) aq0.f(layoutInflater, 2131558625, viewGroup, false, A6());
        pq7.b(jb5, "binding");
        O6(jb5);
        g37<jb5> g37 = new g37<>(this, jb5);
        this.g = g37;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        rj6 rj6 = this.h;
        if (rj6 != null) {
            rj6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        rj6 rj6 = this.h;
        if (rj6 != null) {
            rj6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
