package com.fossil;

import com.fossil.dl7;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ vz7 f4003a; // = new vz7("UNDEFINED");
    @DexIgnore
    public static /* final */ vz7 b; // = new vz7("REUSABLE_CLAIMED");

    @DexIgnore
    public static final <T> void b(qn7<? super T> qn7, Object obj) {
        boolean z;
        if (qn7 instanceof vv7) {
            vv7 vv7 = (vv7) qn7;
            Object b2 = wu7.b(obj);
            if (vv7.h.Q(vv7.getContext())) {
                vv7.e = b2;
                vv7.d = 1;
                vv7.h.M(vv7.getContext(), vv7);
                return;
            }
            hw7 b3 = wx7.b.b();
            if (b3.o0()) {
                vv7.e = b2;
                vv7.d = 1;
                b3.X(vv7);
                return;
            }
            b3.g0(true);
            try {
                xw7 xw7 = (xw7) vv7.getContext().get(xw7.r);
                if (xw7 == null || xw7.isActive()) {
                    z = false;
                } else {
                    CancellationException k = xw7.k();
                    dl7.a aVar = dl7.Companion;
                    vv7.resumeWith(dl7.m1constructorimpl(el7.a(k)));
                    z = true;
                }
                if (!z) {
                    tn7 context = vv7.getContext();
                    Object c = zz7.c(context, vv7.g);
                    try {
                        vv7.i.resumeWith(obj);
                        tl7 tl7 = tl7.f3441a;
                    } finally {
                        zz7.a(context, c);
                    }
                }
                do {
                } while (b3.r0());
            } catch (Throwable th) {
                b3.S(true);
                throw th;
            }
            b3.S(true);
            return;
        }
        qn7.resumeWith(obj);
    }

    @DexIgnore
    public static final boolean c(vv7<? super tl7> vv7) {
        tl7 tl7 = tl7.f3441a;
        hw7 b2 = wx7.b.b();
        if (b2.p0()) {
            return false;
        }
        if (b2.o0()) {
            vv7.e = tl7;
            vv7.d = 1;
            b2.X(vv7);
            return true;
        }
        b2.g0(true);
        try {
            vv7.run();
            do {
            } while (b2.r0());
        } catch (Throwable th) {
            b2.S(true);
            throw th;
        }
        b2.S(true);
        return false;
    }
}
