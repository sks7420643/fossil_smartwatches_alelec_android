package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class me3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<me3> CREATOR; // = new ef3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Float c;

    @DexIgnore
    public me3(int i, Float f) {
        boolean z = true;
        if (i != 1 && (f == null || f.floatValue() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            z = false;
        }
        String valueOf = String.valueOf(f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 45);
        sb.append("Invalid PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        rc2.b(z, sb.toString());
        this.b = i;
        this.c = f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof me3)) {
            return false;
        }
        me3 me3 = (me3) obj;
        return this.b == me3.b && pc2.a(this.c, me3.c);
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(Integer.valueOf(this.b), this.c);
    }

    @DexIgnore
    public String toString() {
        int i = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
        sb.append("[PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 2, this.b);
        bd2.l(parcel, 3, this.c, false);
        bd2.b(parcel, a2);
    }
}
