package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int[] f2213a;
        @DexIgnore
        public /* final */ float[] b;

        @DexIgnore
        public a(int i, int i2) {
            this.f2213a = new int[]{i, i2};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f};
        }

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.f2213a = new int[]{i, i2, i3};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
        }

        @DexIgnore
        public a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.f2213a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; i++) {
                this.f2213a[i] = list.get(i).intValue();
                this.b[i] = list2.get(i).floatValue();
            }
        }
    }

    @DexIgnore
    public static a a(a aVar, int i, int i2, boolean z, int i3) {
        return aVar != null ? aVar : z ? new a(i, i3, i2) : new a(i, i2);
    }

    @DexIgnore
    public static Shader b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            TypedArray k = ol0.k(resources, theme, attributeSet, qk0.GradientColor);
            float f = ol0.f(k, xmlPullParser, "startX", qk0.GradientColor_android_startX, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = ol0.f(k, xmlPullParser, "startY", qk0.GradientColor_android_startY, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f3 = ol0.f(k, xmlPullParser, "endX", qk0.GradientColor_android_endX, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f4 = ol0.f(k, xmlPullParser, "endY", qk0.GradientColor_android_endY, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f5 = ol0.f(k, xmlPullParser, "centerX", qk0.GradientColor_android_centerX, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f6 = ol0.f(k, xmlPullParser, "centerY", qk0.GradientColor_android_centerY, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            int g = ol0.g(k, xmlPullParser, "type", qk0.GradientColor_android_type, 0);
            int b = ol0.b(k, xmlPullParser, "startColor", qk0.GradientColor_android_startColor, 0);
            boolean j = ol0.j(xmlPullParser, "centerColor");
            int b2 = ol0.b(k, xmlPullParser, "centerColor", qk0.GradientColor_android_centerColor, 0);
            int b3 = ol0.b(k, xmlPullParser, "endColor", qk0.GradientColor_android_endColor, 0);
            int g2 = ol0.g(k, xmlPullParser, "tileMode", qk0.GradientColor_android_tileMode, 0);
            float f7 = ol0.f(k, xmlPullParser, "gradientRadius", qk0.GradientColor_android_gradientRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            k.recycle();
            a a2 = a(c(resources, xmlPullParser, attributeSet, theme), b, b3, j, b2);
            if (g != 1) {
                return g != 2 ? new LinearGradient(f, f2, f3, f4, a2.f2213a, a2.b, d(g2)) : new SweepGradient(f5, f6, a2.f2213a, a2.b);
            }
            if (f7 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return new RadialGradient(f5, f6, f7, a2.f2213a, a2.b, d(g2));
            }
            throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0083, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r8.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' attribute!");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.ll0.a c(android.content.res.Resources r7, org.xmlpull.v1.XmlPullParser r8, android.util.AttributeSet r9, android.content.res.Resources.Theme r10) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r3 = 20
            int r0 = r8.getDepth()
            int r0 = r0 + 1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r3)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r3)
        L_0x0012:
            int r3 = r8.next()
            r4 = 1
            if (r3 == r4) goto L_0x0084
            int r4 = r8.getDepth()
            if (r4 >= r0) goto L_0x0022
            r5 = 3
            if (r3 == r5) goto L_0x0084
        L_0x0022:
            r5 = 2
            if (r3 != r5) goto L_0x0012
            if (r4 > r0) goto L_0x0012
            java.lang.String r3 = r8.getName()
            java.lang.String r4 = "item"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0012
            int[] r3 = com.fossil.qk0.GradientColorItem
            android.content.res.TypedArray r3 = com.fossil.ol0.k(r7, r10, r9, r3)
            int r4 = com.fossil.qk0.GradientColorItem_android_color
            boolean r4 = r3.hasValue(r4)
            int r5 = com.fossil.qk0.GradientColorItem_android_offset
            boolean r5 = r3.hasValue(r5)
            if (r4 == 0) goto L_0x0069
            if (r5 == 0) goto L_0x0069
            int r4 = com.fossil.qk0.GradientColorItem_android_color
            r5 = 0
            int r4 = r3.getColor(r4, r5)
            int r5 = com.fossil.qk0.GradientColorItem_android_offset
            r6 = 0
            float r5 = r3.getFloat(r5, r6)
            r3.recycle()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r2.add(r3)
            java.lang.Float r3 = java.lang.Float.valueOf(r5)
            r1.add(r3)
            goto L_0x0012
        L_0x0069:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r8.getPositionDescription()
            r0.append(r1)
            java.lang.String r1 = ": <item> tag requires a 'color' attribute and a 'offset' attribute!"
            r0.append(r1)
            org.xmlpull.v1.XmlPullParserException r1 = new org.xmlpull.v1.XmlPullParserException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0084:
            int r0 = r2.size()
            if (r0 <= 0) goto L_0x0090
            com.fossil.ll0$a r0 = new com.fossil.ll0$a
            r0.<init>(r2, r1)
        L_0x008f:
            return r0
        L_0x0090:
            r0 = 0
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll0.c(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):com.fossil.ll0$a");
    }

    @DexIgnore
    public static Shader.TileMode d(int i) {
        return i != 1 ? i != 2 ? Shader.TileMode.CLAMP : Shader.TileMode.MIRROR : Shader.TileMode.REPEAT;
    }
}
