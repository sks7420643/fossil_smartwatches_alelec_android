package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.gw5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e96 extends pv5 implements d96 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<v35> g;
    @DexIgnore
    public gw5 h;
    @DexIgnore
    public c96 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return e96.k;
        }

        @DexIgnore
        public final e96 b() {
            return new e96();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e96 b;

        @DexIgnore
        public b(e96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public final void onClick(View view) {
            v35 a2 = this.b.M6().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ e96 b;

        @DexIgnore
        public c(e96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            FlexibleTextView flexibleTextView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                v35 a2 = this.b.M6().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.b.z("");
                this.b.N6().n();
                return;
            }
            v35 a3 = this.b.M6().a();
            if (!(a3 == null || (flexibleTextView = a3.w) == null)) {
                flexibleTextView.setVisibility(8);
            }
            v35 a4 = this.b.M6().a();
            if (!(a4 == null || (imageView = a4.r) == null)) {
                imageView.setVisibility(0);
            }
            this.b.z(String.valueOf(charSequence));
            this.b.N6().p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e96 b;

        @DexIgnore
        public d(e96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements gw5.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ e96 f896a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(e96 e96) {
            this.f896a = e96;
        }

        @DexIgnore
        @Override // com.fossil.gw5.c
        public void a(String str) {
            pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            v35 a2 = this.f896a.M6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "it.tvNotFound");
                hr7 hr7 = hr7.f1520a;
                String c = um5.c(PortfolioApp.h0.c(), 2131886813);
                pq7.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements gw5.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ e96 f897a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(e96 e96) {
            this.f897a = e96;
        }

        @DexIgnore
        @Override // com.fossil.gw5.d
        public void a(Complication complication) {
            pq7.c(complication, "item");
            this.f897a.N6().o(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ v35 f898a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(v35 v35, long j) {
            this.f898a = v35;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleButton flexibleButton = this.f898a.q;
            pq7.b(flexibleButton, "it");
            if (flexibleButton.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                flexibleButton.animate().setDuration(this.b).alpha(1.0f);
            } else {
                flexibleButton.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = e96.class.getSimpleName();
        pq7.b(simpleName, "ComplicationSearchFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.d96
    public void B(List<cl7<Complication, String>> list) {
        pq7.c(list, "results");
        gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.m(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d96
    public void D() {
        gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.m(null);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.d96
    public void K(List<cl7<Complication, String>> list) {
        pq7.c(list, "recentSearchResult");
        gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.l(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void K6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            xk5 xk5 = xk5.f4136a;
            g37<v35> g37 = this.g;
            if (g37 != null) {
                v35 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final g37<v35> M6() {
        g37<v35> g37 = this.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final c96 N6() {
        c96 c96 = this.i;
        if (c96 != null) {
            return c96;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = is5.f1661a.a(j2);
        Window window = fragmentActivity.getWindow();
        pq7.b(window, "context.window");
        window.setEnterTransition(a2);
        g37<v35> g37 = this.g;
        if (g37 != null) {
            v35 a3 = g37.a();
            if (a3 != null) {
                pq7.b(a3, "binding");
                P6(a2, j2, a3);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet P6(TransitionSet transitionSet, long j2, v35 v35) {
        FlexibleButton flexibleButton = v35.q;
        pq7.b(flexibleButton, "binding.btnCancel");
        flexibleButton.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new g(v35, j2));
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(c96 c96) {
        pq7.c(c96, "presenter");
        this.i = c96;
    }

    @DexIgnore
    @Override // com.fossil.d96
    public void b3(Complication complication) {
        pq7.c(complication, "selectedComplication");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, new Intent().putExtra("SEARCH_COMPLICATION_RESULT_ID", complication.getComplicationId()));
            xk5 xk5 = xk5.f4136a;
            g37<v35> g37 = this.g;
            if (g37 != null) {
                v35 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<v35> g37 = new g37<>(this, (v35) aq0.f(layoutInflater, 2131558521, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            v35 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        c96 c96 = this.i;
        if (c96 != null) {
            c96.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        c96 c96 = this.i;
        if (c96 != null) {
            c96.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, "it");
            O6(activity, 550);
        }
        this.h = new gw5();
        g37<v35> g37 = this.g;
        if (g37 != null) {
            v35 a2 = g37.a();
            if (a2 != null) {
                v35 v35 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = v35.v;
                pq7.b(recyclerViewEmptySupport, "this.rvResults");
                gw5 gw5 = this.h;
                if (gw5 != null) {
                    recyclerViewEmptySupport.setAdapter(gw5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = v35.v;
                    pq7.b(recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = v35.v;
                    FlexibleTextView flexibleTextView = v35.w;
                    pq7.b(flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = v35.r;
                    pq7.b(imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    v35.r.setOnClickListener(new b(this));
                    v35.t.addTextChangedListener(new c(this));
                    v35.q.setOnClickListener(new d(this));
                    gw5 gw52 = this.h;
                    if (gw52 != null) {
                        gw52.o(new e(this));
                        gw5 gw53 = this.h;
                        if (gw53 != null) {
                            gw53.p(new f(this));
                        } else {
                            pq7.n("mAdapter");
                            throw null;
                        }
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.d96
    public void z(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.n(str);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }
}
