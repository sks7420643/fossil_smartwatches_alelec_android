package com.fossil;

import com.fossil.dl7;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<P, E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku7 f1790a;
        @DexIgnore
        public /* final */ /* synthetic */ iq4 b;

        @DexIgnore
        public a(ku7 ku7, iq4 iq4, iq4.b bVar) {
            this.f1790a = ku7;
            this.b = iq4;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(E e) {
            pq7.c(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String h = this.b.h();
            local.d(h, "fail continuation " + this.f1790a.hashCode());
            if (this.f1790a.isActive()) {
                ku7 ku7 = this.f1790a;
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(e));
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(P p) {
            pq7.c(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String h = this.b.h();
            local.d(h, "success continuation " + this.f1790a.hashCode());
            if (this.f1790a.isActive()) {
                ku7 ku7 = this.f1790a;
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(p));
            }
        }
    }

    @DexIgnore
    public static final <R extends iq4.b, P extends iq4.d, E extends iq4.a> Object a(iq4<? super R, P, E> iq4, R r, qn7<? super iq4.c> qn7) {
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        iq4.e(r, new a(lu7, iq4, r));
        Object t = lu7.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }
}
