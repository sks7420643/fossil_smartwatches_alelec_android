package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.ad2;
import com.fossil.av2;
import com.fossil.ev2;
import com.fossil.wu2;
import com.fossil.yu2;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr3 extends zq3 {
    @DexIgnore
    public gr3(yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    public static String D(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    @DexIgnore
    public static List<Long> E(BitSet bitSet) {
        int i;
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        int i2 = 0;
        while (i2 < length) {
            long j = 0;
            int i3 = 0;
            while (i3 < 64 && (i = (i2 << 6) + i3) < bitSet.length()) {
                if (bitSet.get(i)) {
                    j |= 1 << i3;
                }
                i3++;
            }
            arrayList.add(Long.valueOf(j));
            i2++;
        }
        return arrayList;
    }

    @DexIgnore
    public static List<yu2> G(Bundle[] bundleArr) {
        ArrayList arrayList = new ArrayList();
        for (Bundle bundle : bundleArr) {
            if (bundle != null) {
                yu2.a f0 = yu2.f0();
                for (String str : bundle.keySet()) {
                    yu2.a f02 = yu2.f0();
                    f02.E(str);
                    Object obj = bundle.get(str);
                    if (obj instanceof Long) {
                        f02.z(((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        f02.H((String) obj);
                    } else if (obj instanceof Double) {
                        f02.y(((Double) obj).doubleValue());
                    }
                    f0.B(f02);
                }
                if (f0.J() > 0) {
                    arrayList.add((yu2) ((e13) f0.h()));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static void K(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    @DexIgnore
    public static void N(StringBuilder sb, int i, String str, cv2 cv2, String str2) {
        if (cv2 != null) {
            K(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (cv2.S() != 0) {
                K(sb, 4);
                sb.append("results: ");
                int i2 = 0;
                for (Long l : cv2.P()) {
                    if (i2 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l);
                    i2++;
                }
                sb.append('\n');
            }
            if (cv2.J() != 0) {
                K(sb, 4);
                sb.append("status: ");
                int i3 = 0;
                for (Long l2 : cv2.D()) {
                    if (i3 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l2);
                    i3++;
                }
                sb.append('\n');
            }
            if (cv2.Y() != 0) {
                K(sb, 4);
                sb.append("dynamic_filter_timestamps: {");
                int i4 = 0;
                for (vu2 vu2 : cv2.W()) {
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(vu2.H() ? Integer.valueOf(vu2.I()) : null);
                    sb.append(":");
                    sb.append(vu2.J() ? Long.valueOf(vu2.K()) : null);
                    i4++;
                }
                sb.append("}\n");
            }
            if (cv2.a0() != 0) {
                K(sb, 4);
                sb.append("sequence_filter_timestamps: {");
                int i5 = 0;
                for (dv2 dv2 : cv2.Z()) {
                    if (i5 != 0) {
                        sb.append(", ");
                    }
                    sb.append(dv2.H() ? Integer.valueOf(dv2.I()) : null);
                    sb.append(": [");
                    int i6 = 0;
                    for (Long l3 : dv2.K()) {
                        long longValue = l3.longValue();
                        if (i6 != 0) {
                            sb.append(", ");
                        }
                        sb.append(longValue);
                        i6++;
                    }
                    sb.append("]");
                    i5++;
                }
                sb.append("}\n");
            }
            K(sb, 3);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public static void O(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            K(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append('\n');
        }
    }

    @DexIgnore
    public static boolean R(vg3 vg3, or3 or3) {
        rc2.k(vg3);
        rc2.k(or3);
        return !TextUtils.isEmpty(or3.c) || !TextUtils.isEmpty(or3.x);
    }

    @DexIgnore
    public static boolean S(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    @DexIgnore
    public static boolean T(List<Long> list, int i) {
        if (i < (list.size() << 6)) {
            if ((list.get(i / 64).longValue() & (1 << (i % 64))) != 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static int u(av2.a aVar, String str) {
        if (aVar == null) {
            return -1;
        }
        for (int i = 0; i < aVar.b0(); i++) {
            if (str.equals(aVar.Z(i).R())) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static yu2 y(wu2 wu2, String str) {
        for (yu2 yu2 : wu2.D()) {
            if (yu2.O().equals(str)) {
                return yu2;
            }
        }
        return null;
    }

    @DexIgnore
    public static <Builder extends p23> Builder z(Builder builder, byte[] bArr) throws l13 {
        q03 c = q03.c();
        if (c != null) {
            builder.k(bArr, c);
        } else {
            builder.A(bArr);
        }
        return builder;
    }

    @DexIgnore
    public final String A(hu2 hu2) {
        if (hu2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        if (hu2.I()) {
            O(sb, 0, "filter_id", Integer.valueOf(hu2.J()));
        }
        O(sb, 0, BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, j().v(hu2.K()));
        String D = D(hu2.P(), hu2.Q(), hu2.S());
        if (!D.isEmpty()) {
            O(sb, 0, "filter_type", D);
        }
        if (hu2.N()) {
            M(sb, 1, "event_count_filter", hu2.O());
        }
        if (hu2.M() > 0) {
            sb.append("  filters {\n");
            for (iu2 iu2 : hu2.L()) {
                L(sb, 2, iu2);
            }
        }
        K(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String B(ku2 ku2) {
        if (ku2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        if (ku2.E()) {
            O(sb, 0, "filter_id", Integer.valueOf(ku2.G()));
        }
        O(sb, 0, "property_name", j().z(ku2.H()));
        String D = D(ku2.J(), ku2.K(), ku2.M());
        if (!D.isEmpty()) {
            O(sb, 0, "filter_type", D);
        }
        L(sb, 1, ku2.I());
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String C(zu2 zu2) {
        if (zu2 == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        for (av2 av2 : zu2.D()) {
            if (av2 != null) {
                K(sb, 1);
                sb.append("bundle {\n");
                if (av2.W()) {
                    O(sb, 1, "protocol_version", Integer.valueOf(av2.x0()));
                }
                O(sb, 1, "platform", av2.A2());
                if (av2.J2()) {
                    O(sb, 1, "gmp_version", Long.valueOf(av2.X()));
                }
                if (av2.Y()) {
                    O(sb, 1, "uploading_gmp_version", Long.valueOf(av2.Z()));
                }
                if (av2.J0()) {
                    O(sb, 1, "dynamite_version", Long.valueOf(av2.K0()));
                }
                if (av2.r0()) {
                    O(sb, 1, "config_version", Long.valueOf(av2.s0()));
                }
                O(sb, 1, "gmp_app_id", av2.j0());
                O(sb, 1, "admob_app_id", av2.I0());
                O(sb, 1, "app_id", av2.H2());
                O(sb, 1, "app_version", av2.I2());
                if (av2.o0()) {
                    O(sb, 1, "app_version_major", Integer.valueOf(av2.p0()));
                }
                O(sb, 1, "firebase_instance_id", av2.n0());
                if (av2.e0()) {
                    O(sb, 1, "dev_cert_hash", Long.valueOf(av2.f0()));
                }
                O(sb, 1, "app_store", av2.G2());
                if (av2.L1()) {
                    O(sb, 1, "upload_timestamp_millis", Long.valueOf(av2.M1()));
                }
                if (av2.X1()) {
                    O(sb, 1, "start_timestamp_millis", Long.valueOf(av2.Y1()));
                }
                if (av2.i2()) {
                    O(sb, 1, "end_timestamp_millis", Long.valueOf(av2.j2()));
                }
                if (av2.q2()) {
                    O(sb, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(av2.r2()));
                }
                if (av2.w2()) {
                    O(sb, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(av2.x2()));
                }
                O(sb, 1, "app_instance_id", av2.d0());
                O(sb, 1, "resettable_device_id", av2.a0());
                O(sb, 1, "device_id", av2.q0());
                O(sb, 1, "ds_id", av2.v0());
                if (av2.b0()) {
                    O(sb, 1, "limited_ad_tracking", Boolean.valueOf(av2.c0()));
                }
                O(sb, 1, Constants.OS_VERSION, av2.B2());
                O(sb, 1, "device_model", av2.C2());
                O(sb, 1, "user_default_language", av2.D2());
                if (av2.E2()) {
                    O(sb, 1, "time_zone_offset_minutes", Integer.valueOf(av2.F2()));
                }
                if (av2.g0()) {
                    O(sb, 1, "bundle_sequential_index", Integer.valueOf(av2.h0()));
                }
                if (av2.k0()) {
                    O(sb, 1, "service_upload", Boolean.valueOf(av2.l0()));
                }
                O(sb, 1, "health_monitor", av2.i0());
                if (!m().s(xg3.M0) && av2.t0() && av2.u0() != 0) {
                    O(sb, 1, "android_id", Long.valueOf(av2.u0()));
                }
                if (av2.w0()) {
                    O(sb, 1, "retry_counter", Integer.valueOf(av2.H0()));
                }
                List<ev2> p1 = av2.p1();
                if (p1 != null) {
                    for (ev2 ev2 : p1) {
                        if (ev2 != null) {
                            K(sb, 2);
                            sb.append("user_property {\n");
                            O(sb, 2, "set_timestamp_millis", ev2.K() ? Long.valueOf(ev2.L()) : null);
                            O(sb, 2, "name", j().z(ev2.R()));
                            O(sb, 2, "string_value", ev2.U());
                            O(sb, 2, "int_value", ev2.V() ? Long.valueOf(ev2.W()) : null);
                            O(sb, 2, "double_value", ev2.X() ? Double.valueOf(ev2.Y()) : null);
                            K(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<uu2> m0 = av2.m0();
                String H2 = av2.H2();
                if (m0 != null) {
                    for (uu2 uu2 : m0) {
                        if (uu2 != null) {
                            K(sb, 2);
                            sb.append("audience_membership {\n");
                            if (uu2.J()) {
                                O(sb, 2, "audience_id", Integer.valueOf(uu2.K()));
                            }
                            if (uu2.Q()) {
                                O(sb, 2, "new_audience", Boolean.valueOf(uu2.R()));
                            }
                            N(sb, 2, "current_data", uu2.N(), H2);
                            if (uu2.O()) {
                                N(sb, 2, "previous_data", uu2.P(), H2);
                            }
                            K(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<wu2> W0 = av2.W0();
                if (W0 != null) {
                    for (wu2 wu2 : W0) {
                        if (wu2 != null) {
                            K(sb, 2);
                            sb.append("event {\n");
                            O(sb, 2, "name", j().v(wu2.V()));
                            if (wu2.W()) {
                                O(sb, 2, "timestamp_millis", Long.valueOf(wu2.X()));
                            }
                            if (wu2.Y()) {
                                O(sb, 2, "previous_timestamp_millis", Long.valueOf(wu2.Z()));
                            }
                            if (wu2.a0()) {
                                O(sb, 2, "count", Integer.valueOf(wu2.b0()));
                            }
                            if (wu2.R() != 0) {
                                P(sb, 2, wu2.D());
                            }
                            K(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                K(sb, 1);
                sb.append("}\n");
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final List<Long> F(List<Long> list, List<Integer> list2) {
        ArrayList arrayList = new ArrayList(list);
        for (Integer num : list2) {
            if (num.intValue() < 0) {
                d().I().b("Ignoring negative bit index to be cleared", num);
            } else {
                int intValue = num.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    d().I().c("Ignoring bit index greater than bitSet size", num, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf((1 << (num.intValue() % 64)) & ((Long) arrayList.get(intValue)).longValue()));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        int i = size;
        while (size2 >= 0 && ((Long) arrayList.get(size2)).longValue() == 0) {
            i = size2;
            size2--;
        }
        return arrayList.subList(0, i);
    }

    @DexIgnore
    public final void H(wu2.a aVar, String str, Object obj) {
        int i;
        List<yu2> I = aVar.I();
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= I.size()) {
                i = -1;
                break;
            } else if (str.equals(I.get(i).O())) {
                break;
            } else {
                i2 = i + 1;
            }
        }
        yu2.a f0 = yu2.f0();
        f0.E(str);
        if (obj instanceof Long) {
            f0.z(((Long) obj).longValue());
        } else if (obj instanceof String) {
            f0.H((String) obj);
        } else if (obj instanceof Double) {
            f0.y(((Double) obj).doubleValue());
        } else if (s53.a() && m().s(xg3.G0) && (obj instanceof Bundle[])) {
            f0.C(G((Bundle[]) obj));
        }
        if (i >= 0) {
            aVar.x(i, f0);
        } else {
            aVar.B(f0);
        }
    }

    @DexIgnore
    public final void I(yu2.a aVar, Object obj) {
        rc2.k(obj);
        aVar.x();
        aVar.G();
        aVar.I();
        aVar.K();
        if (obj instanceof String) {
            aVar.H((String) obj);
        } else if (obj instanceof Long) {
            aVar.z(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.y(((Double) obj).doubleValue());
        } else if (!s53.a() || !m().s(xg3.G0) || !(obj instanceof Bundle[])) {
            d().F().b("Ignoring invalid (type) event param value", obj);
        } else {
            aVar.C(G((Bundle[]) obj));
        }
    }

    @DexIgnore
    public final void J(ev2.a aVar, Object obj) {
        rc2.k(obj);
        aVar.x();
        aVar.C();
        aVar.H();
        if (obj instanceof String) {
            aVar.G((String) obj);
        } else if (obj instanceof Long) {
            aVar.E(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.y(((Double) obj).doubleValue());
        } else {
            d().F().b("Ignoring invalid (type) user attribute value", obj);
        }
    }

    @DexIgnore
    public final void L(StringBuilder sb, int i, iu2 iu2) {
        if (iu2 != null) {
            K(sb, i);
            sb.append("filter {\n");
            if (iu2.J()) {
                O(sb, i, "complement", Boolean.valueOf(iu2.K()));
            }
            if (iu2.L()) {
                O(sb, i, "param_name", j().y(iu2.M()));
            }
            if (iu2.E()) {
                int i2 = i + 1;
                lu2 G = iu2.G();
                if (G != null) {
                    K(sb, i2);
                    sb.append("string_filter");
                    sb.append(" {\n");
                    if (G.C()) {
                        O(sb, i2, "match_type", G.D().name());
                    }
                    if (G.E()) {
                        O(sb, i2, "expression", G.G());
                    }
                    if (G.H()) {
                        O(sb, i2, "case_sensitive", Boolean.valueOf(G.I()));
                    }
                    if (G.K() > 0) {
                        K(sb, i2 + 1);
                        sb.append("expression_list {\n");
                        for (String str : G.J()) {
                            K(sb, i2 + 2);
                            sb.append(str);
                            sb.append("\n");
                        }
                        sb.append("}\n");
                    }
                    K(sb, i2);
                    sb.append("}\n");
                }
            }
            if (iu2.H()) {
                M(sb, i + 1, "number_filter", iu2.I());
            }
            K(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void M(StringBuilder sb, int i, String str, ju2 ju2) {
        if (ju2 != null) {
            K(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (ju2.C()) {
                O(sb, i, "comparison_type", ju2.D().name());
            }
            if (ju2.E()) {
                O(sb, i, "match_as_float", Boolean.valueOf(ju2.G()));
            }
            if (ju2.H()) {
                O(sb, i, "comparison_value", ju2.I());
            }
            if (ju2.J()) {
                O(sb, i, "min_comparison_value", ju2.K());
            }
            if (ju2.L()) {
                O(sb, i, "max_comparison_value", ju2.M());
            }
            K(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void P(StringBuilder sb, int i, List<yu2> list) {
        if (list != null) {
            int i2 = i + 1;
            for (yu2 yu2 : list) {
                if (yu2 != null) {
                    K(sb, i2);
                    sb.append("param {\n");
                    if (!s53.a() || !m().s(xg3.E0)) {
                        O(sb, i2, "name", j().y(yu2.O()));
                        O(sb, i2, "string_value", yu2.U());
                        O(sb, i2, "int_value", yu2.X() ? Long.valueOf(yu2.Y()) : null);
                        O(sb, i2, "double_value", yu2.b0() ? Double.valueOf(yu2.c0()) : null);
                    } else {
                        O(sb, i2, "name", yu2.N() ? j().y(yu2.O()) : null);
                        O(sb, i2, "string_value", yu2.T() ? yu2.U() : null);
                        O(sb, i2, "int_value", yu2.X() ? Long.valueOf(yu2.Y()) : null);
                        O(sb, i2, "double_value", yu2.b0() ? Double.valueOf(yu2.c0()) : null);
                        if (yu2.e0() > 0) {
                            P(sb, i2, yu2.d0());
                        }
                    }
                    K(sb, i2);
                    sb.append("}\n");
                }
            }
        }
    }

    @DexIgnore
    public final boolean Q(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(zzm().b() - j) > j2;
    }

    @DexIgnore
    public final Object U(wu2 wu2, String str) {
        yu2 y = y(wu2, str);
        if (y != null) {
            if (y.T()) {
                return y.U();
            }
            if (y.X()) {
                return Long.valueOf(y.Y());
            }
            if (y.b0()) {
                return Double.valueOf(y.c0());
            }
            if (s53.a() && m().s(xg3.G0) && y.e0() > 0) {
                List<yu2> d0 = y.d0();
                ArrayList arrayList = new ArrayList();
                for (yu2 yu2 : d0) {
                    if (yu2 != null) {
                        Bundle bundle = new Bundle();
                        for (yu2 yu22 : yu2.d0()) {
                            if (yu22.T()) {
                                bundle.putString(yu22.O(), yu22.U());
                            } else if (yu22.X()) {
                                bundle.putLong(yu22.O(), yu22.Y());
                            } else if (yu22.b0()) {
                                bundle.putDouble(yu22.O(), yu22.c0());
                            }
                        }
                        if (!bundle.isEmpty()) {
                            arrayList.add(bundle);
                        }
                    }
                }
                return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
            }
        }
        return null;
    }

    @DexIgnore
    public final byte[] V(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            d().F().b("Failed to ungzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final byte[] W(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            d().F().b("Failed to gzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final List<Integer> X() {
        Map<String, String> c = xg3.c(this.b.e());
        if (c == null || c.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = xg3.P.a(null).intValue();
        Iterator<Map.Entry<String, String>> it = c.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<String, String> next = it.next();
            if (next.getKey().startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt(next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            d().I().b("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    d().I().b("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final long v(byte[] bArr) {
        rc2.k(bArr);
        k().h();
        MessageDigest I0 = kr3.I0();
        if (I0 != null) {
            return kr3.y(I0.digest(bArr));
        }
        d().F().a("Failed to get MD5");
        return 0;
    }

    @DexIgnore
    public final <T extends Parcelable> T w(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            return creator.createFromParcel(obtain);
        } catch (ad2.a e) {
            d().F().a("Failed to load parcelable from buffer");
            return null;
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public final wu2 x(sg3 sg3) {
        wu2.a c0 = wu2.c0();
        c0.M(sg3.e);
        Iterator<String> it = sg3.f.iterator();
        while (it.hasNext()) {
            String next = it.next();
            yu2.a f0 = yu2.f0();
            f0.E(next);
            I(f0, sg3.f.h(next));
            c0.B(f0);
        }
        return (wu2) ((e13) c0.h());
    }
}
