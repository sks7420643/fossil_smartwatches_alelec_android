package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe2 extends rl2 implements ne2 {
    @DexIgnore
    public oe2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    @Override // com.fossil.ne2
    public final boolean y2(kg2 kg2, rg2 rg2) throws RemoteException {
        Parcel d = d();
        sl2.d(d, kg2);
        sl2.c(d, rg2);
        Parcel e = e(5, d);
        boolean e2 = sl2.e(e);
        e.recycle();
        return e2;
    }
}
