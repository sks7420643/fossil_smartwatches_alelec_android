package com.fossil;

import com.fossil.tj0;
import com.fossil.uj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xj0 extends uj0 {
    @DexIgnore
    public float k0; // = -1.0f;
    @DexIgnore
    public int l0; // = -1;
    @DexIgnore
    public int m0; // = -1;
    @DexIgnore
    public tj0 n0; // = this.t;
    @DexIgnore
    public int o0; // = 0;
    @DexIgnore
    public boolean p0; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f4132a;

        /*
        static {
            int[] iArr = new int[tj0.d.values().length];
            f4132a = iArr;
            try {
                iArr[tj0.d.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f4132a[tj0.d.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f4132a[tj0.d.TOP.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f4132a[tj0.d.BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f4132a[tj0.d.BASELINE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f4132a[tj0.d.CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f4132a[tj0.d.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f4132a[tj0.d.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f4132a[tj0.d.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
        */
    }

    @DexIgnore
    public xj0() {
        this.B.clear();
        this.B.add(this.n0);
        int length = this.A.length;
        for (int i = 0; i < length; i++) {
            this.A[i] = this.n0;
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void G0(kj0 kj0) {
        if (u() != null) {
            int y = kj0.y(this.n0);
            if (this.o0 == 1) {
                C0(y);
                D0(0);
                b0(u().r());
                y0(0);
                return;
            }
            C0(0);
            D0(y);
            y0(u().D());
            b0(0);
        }
    }

    @DexIgnore
    public int I0() {
        return this.o0;
    }

    @DexIgnore
    public void J0(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void K0(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    @DexIgnore
    public void L0(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void M0(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            this.B.clear();
            if (this.o0 == 1) {
                this.n0 = this.s;
            } else {
                this.n0 = this.t;
            }
            this.B.add(this.n0);
            int length = this.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.A[i2] = this.n0;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void b(kj0 kj0) {
        tj0 tj0;
        boolean z = true;
        vj0 vj0 = (vj0) u();
        if (vj0 != null) {
            tj0 h = vj0.h(tj0.d.LEFT);
            tj0 h2 = vj0.h(tj0.d.RIGHT);
            uj0 uj0 = this.D;
            boolean z2 = uj0 != null && uj0.C[0] == uj0.b.WRAP_CONTENT;
            if (this.o0 == 0) {
                tj0 h3 = vj0.h(tj0.d.TOP);
                tj0 = vj0.h(tj0.d.BOTTOM);
                uj0 uj02 = this.D;
                if (uj02 == null || uj02.C[1] != uj0.b.WRAP_CONTENT) {
                    z = false;
                    h = h3;
                } else {
                    h = h3;
                }
            } else {
                z = z2;
                tj0 = h2;
            }
            if (this.l0 != -1) {
                oj0 r = kj0.r(this.n0);
                kj0.e(r, kj0.r(h), this.l0, 6);
                if (z) {
                    kj0.i(kj0.r(tj0), r, 0, 5);
                }
            } else if (this.m0 != -1) {
                oj0 r2 = kj0.r(this.n0);
                oj0 r3 = kj0.r(tj0);
                kj0.e(r2, r3, -this.m0, 6);
                if (z) {
                    kj0.i(r2, kj0.r(h), 0, 5);
                    kj0.i(r3, r2, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                kj0.d(kj0.t(kj0, kj0.r(this.n0), kj0.r(h), kj0.r(tj0), this.k0, this.p0));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void d(int i) {
        uj0 u = u();
        if (u != null) {
            if (I0() == 1) {
                this.t.f().h(1, u.t.f(), 0);
                this.v.f().h(1, u.t.f(), 0);
                if (this.l0 != -1) {
                    this.s.f().h(1, u.s.f(), this.l0);
                    this.u.f().h(1, u.s.f(), this.l0);
                } else if (this.m0 != -1) {
                    this.s.f().h(1, u.u.f(), -this.m0);
                    this.u.f().h(1, u.u.f(), -this.m0);
                } else if (this.k0 != -1.0f && u.s() == uj0.b.FIXED) {
                    int i2 = (int) (((float) u.E) * this.k0);
                    this.s.f().h(1, u.s.f(), i2);
                    this.u.f().h(1, u.s.f(), i2);
                }
            } else {
                this.s.f().h(1, u.s.f(), 0);
                this.u.f().h(1, u.s.f(), 0);
                if (this.l0 != -1) {
                    this.t.f().h(1, u.t.f(), this.l0);
                    this.v.f().h(1, u.t.f(), this.l0);
                } else if (this.m0 != -1) {
                    this.t.f().h(1, u.v.f(), -this.m0);
                    this.v.f().h(1, u.v.f(), -this.m0);
                } else if (this.k0 != -1.0f && u.B() == uj0.b.FIXED) {
                    int i3 = (int) (((float) u.F) * this.k0);
                    this.t.f().h(1, u.t.f(), i3);
                    this.v.f().h(1, u.t.f(), i3);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public tj0 h(tj0.d dVar) {
        switch (a.f4132a[dVar.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(dVar.name());
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public ArrayList<tj0> i() {
        return this.B;
    }
}
