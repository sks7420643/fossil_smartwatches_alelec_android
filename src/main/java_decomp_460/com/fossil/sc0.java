package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc0 implements Parcelable.Creator<tc0> {
    @DexIgnore
    public /* synthetic */ sc0(kq7 kq7) {
    }

    @DexIgnore
    public tc0 a(Parcel parcel) {
        return a(parcel);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public tc0 createFromParcel(Parcel parcel) {
        return a(parcel);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public tc0[] newArray(int i) {
        return new tc0[i];
    }
}
