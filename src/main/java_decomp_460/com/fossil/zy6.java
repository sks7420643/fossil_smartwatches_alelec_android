package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy6 implements Factory<yy6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<HybridPresetRepository> f4557a;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> f;

    @DexIgnore
    public zy6(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        this.f4557a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static zy6 a(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        return new zy6(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static yy6 c(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        return new yy6(hybridPresetRepository, microAppRepository, deviceRepository, notificationsRepository, categoryRepository, alarmsRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public yy6 get() {
        return c(this.f4557a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }
}
