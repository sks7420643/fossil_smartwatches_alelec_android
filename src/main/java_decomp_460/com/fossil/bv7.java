package com.fossil;

import com.fossil.bv7;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bv7<T extends Throwable & bv7<T>> {
    @DexIgnore
    T createCopy();
}
