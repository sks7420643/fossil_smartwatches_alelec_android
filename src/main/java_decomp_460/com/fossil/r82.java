package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r82 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ h82 b;

    @DexIgnore
    public r82(h82 h82) {
        this.b = h82;
    }

    @DexIgnore
    public /* synthetic */ r82(h82 h82, k82 k82) {
        this(h82);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.b.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.b.b.unlock();
            }
        } catch (RuntimeException e) {
            this.b.f1448a.q(e);
        } finally {
            this.b.b.unlock();
        }
    }
}
