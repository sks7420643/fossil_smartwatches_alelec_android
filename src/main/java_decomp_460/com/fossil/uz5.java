package com.fossil;

import dagger.internal.Factory;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz5 implements Factory<WeakReference<ao6>> {
    @DexIgnore
    public static WeakReference<ao6> a(oz5 oz5) {
        WeakReference<ao6> f = oz5.f();
        lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
