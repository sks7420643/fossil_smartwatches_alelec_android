package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so2 extends gn2 {
    @DexIgnore
    public /* final */ /* synthetic */ DataSet s;
    @DexIgnore
    public /* final */ /* synthetic */ boolean t; // = false;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public so2(to2 to2, r62 r62, DataSet dataSet, boolean z) {
        super(r62);
        this.s = dataSet;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.m62$b] */
    @Override // com.fossil.i72
    public final /* synthetic */ void u(zm2 zm2) throws RemoteException {
        ((fo2) zm2.I()).E(new cj2(this.s, (mo2) new wo2(this), this.t));
    }
}
