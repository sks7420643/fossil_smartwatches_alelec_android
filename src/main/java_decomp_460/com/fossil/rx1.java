package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rx1<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ry1 f3179a;

    @DexIgnore
    public rx1(ry1 ry1) {
        pq7.c(ry1, "baseVersion");
        this.f3179a = ry1;
    }

    @DexIgnore
    public final ry1 a() {
        return this.f3179a;
    }

    @DexIgnore
    public abstract T b(byte[] bArr) throws sx1;
}
