package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic7 extends i11 {
    @DexIgnore
    public /* final */ Map<Class<? extends ListenableWorker>, Provider<jc7<? extends ListenableWorker>>> b;

    @DexIgnore
    public ic7(Map<Class<? extends ListenableWorker>, Provider<jc7<? extends ListenableWorker>>> map) {
        pq7.c(map, "workerFactoryMap");
        this.b = map;
    }

    @DexIgnore
    @Override // com.fossil.i11
    public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
        T t;
        Provider provider;
        pq7.c(context, "appContext");
        pq7.c(str, "workerClassName");
        pq7.c(workerParameters, "workerParameters");
        Iterator<T> it = this.b.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Class.forName(str).isAssignableFrom((Class) next.getKey())) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null && (provider = (Provider) t2.getValue()) != null) {
            return ((jc7) provider.get()).a(context, workerParameters);
        }
        throw new IllegalArgumentException("could not find worker: " + str);
    }
}
