package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ch7 {
    @DexIgnore
    public static volatile long f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ng7 f616a;
    @DexIgnore
    public gg7 b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public ch7(ng7 ng7) {
        this.f616a = ng7;
        this.b = fg7.I();
        this.c = ng7.g();
        this.d = ng7.f();
    }

    @DexIgnore
    public void b() {
        if (!h()) {
            if (fg7.J > 0 && this.e >= f) {
                ig7.u(this.d);
                f = this.e + fg7.K;
                if (fg7.K()) {
                    th7 th7 = ig7.m;
                    th7.h("nextFlushTime=" + f);
                }
            }
            if (tg7.a(this.d).j()) {
                if (fg7.K()) {
                    th7 th72 = ig7.m;
                    th72.h("sendFailedCount=" + ig7.p);
                }
                if (!ig7.g()) {
                    e();
                    return;
                }
                gh7.b(this.d).g(this.f616a, null, this.c, false);
                if (this.e - ig7.q > 1800000) {
                    ig7.q(this.d);
                    return;
                }
                return;
            }
            gh7.b(this.d).g(this.f616a, null, this.c, false);
        }
    }

    @DexIgnore
    public final void c(pi7 pi7) {
        qi7.f(ig7.r).c(this.f616a, pi7);
    }

    @DexIgnore
    public final void e() {
        if (this.f616a.e() != null && this.f616a.e().e()) {
            this.b = gg7.INSTANT;
        }
        if (fg7.z && tg7.a(ig7.r).i()) {
            this.b = gg7.INSTANT;
        }
        if (fg7.K()) {
            th7 th7 = ig7.m;
            th7.h("strategy=" + this.b.name());
        }
        switch (wg7.f3935a[this.b.ordinal()]) {
            case 1:
                f();
                return;
            case 2:
                gh7.b(this.d).g(this.f616a, null, this.c, false);
                if (fg7.K()) {
                    th7 th72 = ig7.m;
                    th72.h("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + ig7.s + ",difftime=" + (ig7.s - this.e));
                }
                if (ig7.s == 0) {
                    ig7.s = ii7.b(this.d, "last_period_ts", 0);
                    if (this.e > ig7.s) {
                        ig7.s(this.d);
                    }
                    long F = this.e + ((long) (fg7.F() * 60 * 1000));
                    if (ig7.s > F) {
                        ig7.s = F;
                    }
                    li7.b(this.d).c();
                }
                if (fg7.K()) {
                    th7 th73 = ig7.m;
                    th73.h("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + ig7.s + ",difftime=" + (ig7.s - this.e));
                }
                if (this.e > ig7.s) {
                    ig7.s(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                gh7.b(this.d).g(this.f616a, null, this.c, false);
                return;
            case 5:
                gh7.b(this.d).g(this.f616a, new dh7(this), this.c, true);
                return;
            case 6:
                if (tg7.a(ig7.r).g() == 1) {
                    f();
                    return;
                } else {
                    gh7.b(this.d).g(this.f616a, null, this.c, false);
                    return;
                }
            case 7:
                if (ei7.x(this.d)) {
                    c(new eh7(this));
                    return;
                }
                return;
            default:
                th7 th74 = ig7.m;
                th74.f("Invalid stat strategy:" + fg7.I());
                return;
        }
    }

    @DexIgnore
    public final void f() {
        if (gh7.u().f <= 0 || !fg7.I) {
            c(new fh7(this));
            return;
        }
        gh7.u().g(this.f616a, null, this.c, true);
        gh7.u().d(-1);
    }

    @DexIgnore
    public final boolean h() {
        if (fg7.w > 0) {
            if (this.e > ig7.d) {
                ig7.c.clear();
                long unused = ig7.d = this.e + fg7.x;
                if (fg7.K()) {
                    th7 th7 = ig7.m;
                    th7.h("clear methodsCalledLimitMap, nextLimitCallClearTime=" + ig7.d);
                }
            }
            Integer valueOf = Integer.valueOf(this.f616a.a().a());
            Integer num = (Integer) ig7.c.get(valueOf);
            if (num != null) {
                ig7.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
                if (num.intValue() > fg7.w) {
                    if (fg7.K()) {
                        th7 th72 = ig7.m;
                        th72.d("event " + this.f616a.h() + " was discard, cause of called limit, current:" + num + ", limit:" + fg7.w + ", period:" + fg7.x + " ms");
                    }
                    return true;
                }
            } else {
                ig7.c.put(valueOf, 1);
            }
        }
        return false;
    }
}
