package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class w12 implements Runnable {
    @DexIgnore
    public /* final */ b22 b;
    @DexIgnore
    public /* final */ h02 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Runnable e;

    @DexIgnore
    public w12(b22 b22, h02 h02, int i, Runnable runnable) {
        this.b = b22;
        this.c = h02;
        this.d = i;
        this.e = runnable;
    }

    @DexIgnore
    public static Runnable a(b22 b22, h02 h02, int i, Runnable runnable) {
        return new w12(b22, h02, i, runnable);
    }

    @DexIgnore
    public void run() {
        b22.e(this.b, this.c, this.d, this.e);
    }
}
