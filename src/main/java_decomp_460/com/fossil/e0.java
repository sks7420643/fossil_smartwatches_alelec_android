package com.fossil;

import android.database.Cursor;
import com.portfolio.platform.data.model.Firmware;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e0 implements Callable<k0> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ tw0 f864a;
    @DexIgnore
    public /* final */ /* synthetic */ g0 b;

    @DexIgnore
    public e0(g0 g0Var, tw0 tw0) {
        this.b = g0Var;
        this.f864a = tw0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public k0 call() throws Exception {
        k0 k0Var = null;
        Cursor b2 = ex0.b(this.b.f1243a, this.f864a, false, null);
        try {
            int c = dx0.c(b2, "id");
            int c2 = dx0.c(b2, "classifier");
            int c3 = dx0.c(b2, "packageOSVersion");
            int c4 = dx0.c(b2, "checksum");
            int c5 = dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c6 = dx0.c(b2, "updatedAt");
            int c7 = dx0.c(b2, "createdAt");
            int c8 = dx0.c(b2, "data");
            if (b2.moveToFirst()) {
                k0Var = new k0(b2.getString(c), this.b.c.b(b2.getInt(c2)), this.b.d.a(b2.getString(c3)), b2.getString(c4), b2.getString(c5), this.b.e.b(b2.getLong(c6)), this.b.e.b(b2.getLong(c7)), b2.getBlob(c8));
            }
            return k0Var;
        } finally {
            b2.close();
            this.f864a.m();
        }
    }
}
