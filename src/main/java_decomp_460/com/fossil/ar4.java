package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.AnimationImageView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar4 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String c; // = "ExploreWatchAdapter";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<Explore> f308a; // = new ArrayList<>();
    @DexIgnore
    public a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f309a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ AnimationImageView c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            pq7.c(view, "itemView");
            View findViewById = view.findViewById(2131362546);
            pq7.b(findViewById, "itemView.findViewById(R.id.ftv_title)");
            this.f309a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362405);
            pq7.b(findViewById2, "itemView.findViewById(R.id.ftv_desc)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362691);
            pq7.b(findViewById3, "itemView.findViewById(R.id.iv_device)");
            this.c = (AnimationImageView) findViewById3;
        }

        @DexIgnore
        public final AnimationImageView a() {
            return this.c;
        }

        @DexIgnore
        public final void b(Explore explore) {
            if (explore != null) {
                this.f309a.setText(explore.getTitle());
                this.b.setText(explore.getDescription());
                Explore.ExploreType exploreType = explore.getExploreType();
                if (exploreType != null) {
                    int i = zq4.f4515a[exploreType.ordinal()];
                    if (i == 1) {
                        AnimationImageView animationImageView = this.c;
                        String string = PortfolioApp.h0.c().getString(2131887271);
                        pq7.b(string, "PortfolioApp.instance.ge\u2026ng.animation_wrist_flick)");
                        animationImageView.q(string, 1, 80, 30, MFNetworkReturnCode.BAD_REQUEST, 2000);
                    } else if (i == 2) {
                        AnimationImageView animationImageView2 = this.c;
                        String string2 = PortfolioApp.h0.c().getString(2131887269);
                        pq7.b(string2, "PortfolioApp.instance.ge\u2026ing.animation_double_tap)");
                        animationImageView2.q(string2, 1, 50, 30, 600, 2400);
                    } else if (i == 3) {
                        AnimationImageView animationImageView3 = this.c;
                        String string3 = PortfolioApp.h0.c().getString(2131887270);
                        pq7.b(string3, "PortfolioApp.instance.ge\u2026animation_press_and_hold)");
                        animationImageView3.q(string3, 1, 50, 30, 1000, 3000);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "welcomeViewHolder");
        aVar.b(this.f308a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f308a.size();
    }

    @DexIgnore
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558726, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(inflate);
    }

    @DexIgnore
    public final void i() {
        AnimationImageView a2;
        FLogger.INSTANCE.getLocal().d(c, "onStopAnimation");
        a aVar = this.b;
        if (aVar != null && (a2 = aVar.a()) != null) {
            a2.r();
        }
    }

    @DexIgnore
    /* renamed from: j */
    public void onViewAttachedToWindow(a aVar) {
        pq7.c(aVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewAttachedToWindow");
        this.b = aVar;
        aVar.a().n();
        super.onViewAttachedToWindow(aVar);
    }

    @DexIgnore
    /* renamed from: k */
    public void onViewDetachedFromWindow(a aVar) {
        pq7.c(aVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewDetachedFromWindow");
        aVar.a().r();
        super.onViewDetachedFromWindow(aVar);
    }

    @DexIgnore
    public final void l(List<? extends Explore> list) {
        pq7.c(list, "data");
        this.f308a.clear();
        this.f308a.addAll(list);
        notifyDataSetChanged();
    }
}
