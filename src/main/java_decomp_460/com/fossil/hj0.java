package com.fossil;

import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.kj0;
import com.fossil.oj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hj0 implements kj0.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public oj0 f1485a; // = null;
    @DexIgnore
    public float b; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ gj0 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public hj0(ij0 ij0) {
        this.d = new gj0(this, ij0);
    }

    @DexIgnore
    @Override // com.fossil.kj0.a
    public void a(oj0 oj0) {
        int i = oj0.d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.d.l(oj0, f);
    }

    @DexIgnore
    @Override // com.fossil.kj0.a
    public oj0 b(kj0 kj0, boolean[] zArr) {
        return this.d.g(zArr, null);
    }

    @DexIgnore
    @Override // com.fossil.kj0.a
    public void c(kj0.a aVar) {
        if (aVar instanceof hj0) {
            hj0 hj0 = (hj0) aVar;
            this.f1485a = null;
            this.d.c();
            int i = 0;
            while (true) {
                gj0 gj0 = hj0.d;
                if (i < gj0.f1313a) {
                    this.d.a(gj0.h(i), hj0.d.i(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.kj0.a
    public void clear() {
        this.d.c();
        this.f1485a = null;
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public hj0 d(kj0 kj0, int i) {
        this.d.l(kj0.p(i, "ep"), 1.0f);
        this.d.l(kj0.p(i, UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    public hj0 e(oj0 oj0, int i) {
        this.d.l(oj0, (float) i);
        return this;
    }

    @DexIgnore
    public boolean f(kj0 kj0) {
        boolean z;
        oj0 b2 = this.d.b(kj0);
        if (b2 == null) {
            z = true;
        } else {
            v(b2);
            z = false;
        }
        if (this.d.f1313a == 0) {
            this.e = true;
        }
        return z;
    }

    @DexIgnore
    public hj0 g(oj0 oj0, oj0 oj02, int i, float f, oj0 oj03, oj0 oj04, int i2) {
        if (oj02 == oj03) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj04, 1.0f);
            this.d.l(oj02, -2.0f);
        } else if (f == 0.5f) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, -1.0f);
            this.d.l(oj04, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (float) ((-i) + i2);
            }
        } else if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.b = (float) i;
        } else if (f >= 1.0f) {
            this.d.l(oj03, -1.0f);
            this.d.l(oj04, 1.0f);
            this.b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.d.l(oj0, f2 * 1.0f);
            this.d.l(oj02, f2 * -1.0f);
            this.d.l(oj03, -1.0f * f);
            this.d.l(oj04, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.kj0.a
    public oj0 getKey() {
        return this.f1485a;
    }

    @DexIgnore
    public hj0 h(oj0 oj0, int i) {
        this.f1485a = oj0;
        float f = (float) i;
        oj0.e = f;
        this.b = f;
        this.e = true;
        return this;
    }

    @DexIgnore
    public hj0 i(oj0 oj0, oj0 oj02, oj0 oj03, float f) {
        this.d.l(oj0, -1.0f);
        this.d.l(oj02, 1.0f - f);
        this.d.l(oj03, f);
        return this;
    }

    @DexIgnore
    public hj0 j(oj0 oj0, oj0 oj02, oj0 oj03, oj0 oj04, float f) {
        this.d.l(oj0, -1.0f);
        this.d.l(oj02, 1.0f);
        this.d.l(oj03, f);
        this.d.l(oj04, -f);
        return this;
    }

    @DexIgnore
    public hj0 k(float f, float f2, float f3, oj0 oj0, oj0 oj02, oj0 oj03, oj0 oj04) {
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj04, 1.0f);
            this.d.l(oj03, -1.0f);
        } else if (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
        } else if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj03, 1.0f);
            this.d.l(oj04, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj04, f4);
            this.d.l(oj03, -f4);
        }
        return this;
    }

    @DexIgnore
    public hj0 l(oj0 oj0, int i) {
        if (i < 0) {
            this.b = (float) (i * -1);
            this.d.l(oj0, 1.0f);
        } else {
            this.b = (float) i;
            this.d.l(oj0, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public hj0 m(oj0 oj0, oj0 oj02, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public hj0 n(oj0 oj0, oj0 oj02, oj0 oj03, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.d.l(oj03, 1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public hj0 o(oj0 oj0, oj0 oj02, oj0 oj03, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.d.l(oj03, -1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, 1.0f);
        }
        return this;
    }

    @DexIgnore
    public hj0 p(oj0 oj0, oj0 oj02, oj0 oj03, oj0 oj04, float f) {
        this.d.l(oj03, 0.5f);
        this.d.l(oj04, 0.5f);
        this.d.l(oj0, -0.5f);
        this.d.l(oj02, -0.5f);
        this.b = -f;
        return this;
    }

    @DexIgnore
    public void q() {
        float f = this.b;
        if (f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b = f * -1.0f;
            this.d.j();
        }
    }

    @DexIgnore
    public boolean r() {
        oj0 oj0 = this.f1485a;
        return oj0 != null && (oj0.g == oj0.a.UNRESTRICTED || this.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public boolean s(oj0 oj0) {
        return this.d.d(oj0);
    }

    @DexIgnore
    public boolean t() {
        return this.f1485a == null && this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.d.f1313a == 0;
    }

    @DexIgnore
    public String toString() {
        return x();
    }

    @DexIgnore
    public oj0 u(oj0 oj0) {
        return this.d.g(null, oj0);
    }

    @DexIgnore
    public void v(oj0 oj0) {
        oj0 oj02 = this.f1485a;
        if (oj02 != null) {
            this.d.l(oj02, -1.0f);
            this.f1485a = null;
        }
        float m = this.d.m(oj0, true) * -1.0f;
        this.f1485a = oj0;
        if (m != 1.0f) {
            this.b /= m;
            this.d.e(m);
        }
    }

    @DexIgnore
    public void w() {
        this.f1485a = null;
        this.d.c();
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String x() {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hj0.x():java.lang.String");
    }
}
