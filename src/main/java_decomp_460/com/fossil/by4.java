package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.fossil.gz4;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class by4 extends pv5 implements oy5, gz4.a, t47.g {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<h35> h;
    @DexIgnore
    public ey4 i;
    @DexIgnore
    public ay4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return by4.l;
        }

        @DexIgnore
        public final by4 b() {
            return new by4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f525a;

        @DexIgnore
        public b(by4 by4) {
            this.f525a = by4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            h35 h35 = (h35) by4.K6(this.f525a).a();
            if (!(h35 == null || (flexibleTextView = h35.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            by4.N6(this.f525a).w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends gz4 {
        @DexIgnore
        public /* final */ /* synthetic */ h35 p;
        @DexIgnore
        public /* final */ /* synthetic */ by4 q;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(h35 h35, RecyclerView recyclerView, by4 by4) {
            super(recyclerView);
            this.p = h35;
            this.q = by4;
        }

        @DexIgnore
        @Override // com.fossil.gz4
        public void N(RecyclerView.ViewHolder viewHolder, List<gz4.c> list) {
            Integer num = null;
            pq7.c(viewHolder, "viewHolder");
            pq7.c(list, "revealedButtons");
            int adapterPosition = viewHolder.getAdapterPosition();
            if (adapterPosition != -1) {
                Object h = by4.L6(this.q).h(adapterPosition);
                if (!(h instanceof xs4)) {
                    h = null;
                }
                xs4 xs4 = (xs4) h;
                FLogger.INSTANCE.getLocal().e(by4.m.a(), "friend: " + xs4);
                if (xs4 != null) {
                    num = Integer.valueOf(xs4.c());
                }
                if (num != null && num.intValue() == 2) {
                    RecyclerView recyclerView = this.p.t;
                    pq7.b(recyclerView, "rvFriends");
                    list.add(new gz4.c("", 2131231018, gl0.d(recyclerView.getContext(), 2131099706), gz4.b.BLOCK, this.q));
                } else if (num != null && num.intValue() == 0) {
                    if (xs4.f() == 0) {
                        RecyclerView recyclerView2 = this.p.t;
                        pq7.b(recyclerView2, "rvFriends");
                        list.add(new gz4.c("", 2131231042, gl0.d(recyclerView2.getContext(), 2131100357), gz4.b.UNFRIEND, this.q));
                        RecyclerView recyclerView3 = this.p.t;
                        pq7.b(recyclerView3, "rvFriends");
                        list.add(new gz4.c("", 2131231018, gl0.d(recyclerView3.getContext(), 2131099706), gz4.b.BLOCK, this.q));
                    }
                } else if (num != null && num.intValue() == -1) {
                    RecyclerView recyclerView4 = this.p.t;
                    pq7.b(recyclerView4, "rvFriends");
                    list.add(new gz4.c("", 2131231041, gl0.d(recyclerView4.getContext(), 2131099706), gz4.b.UNBLOCK, this.q));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<cl7<? extends Boolean, ? extends Boolean>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f526a;

        @DexIgnore
        public d(by4 by4) {
            this.f526a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, Boolean> cl7) {
            h35 h35;
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = cl7.getFirst();
            Boolean second = cl7.getSecond();
            if (!(first == null || (h35 = (h35) by4.K6(this.f526a).a()) == null || (swipeRefreshLayout = h35.u) == null)) {
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.f526a.b();
            } else {
                this.f526a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<List<? extends Object>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f527a;

        @DexIgnore
        public e(by4 by4) {
            this.f527a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            h35 h35;
            FlexibleTextView flexibleTextView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = by4.m.a();
            local.e(a2, "allFriends: " + list);
            pq7.b(list, "it");
            if (!(!(!list.isEmpty()) || (h35 = (h35) by4.K6(this.f527a).a()) == null || (flexibleTextView = h35.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            by4.L6(this.f527a).i(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f528a;

        @DexIgnore
        public f(by4 by4) {
            this.f528a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            h35 h35 = (h35) by4.K6(this.f528a).a();
            if (h35 != null) {
                pq7.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = h35.q;
                    pq7.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = h35.t;
                    pq7.b(recyclerView, "rvFriends");
                    recyclerView.setVisibility(8);
                    by4.L6(this.f528a).g();
                    return;
                }
                FlexibleTextView flexibleTextView2 = h35.q;
                pq7.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = h35.t;
                pq7.b(recyclerView2, "rvFriends");
                recyclerView2.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f529a;

        @DexIgnore
        public g(by4 by4) {
            this.f529a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            h35 h35 = (h35) by4.K6(this.f529a).a();
            if (h35 != null) {
                pq7.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = h35.r;
                    pq7.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = h35.r;
                pq7.b(flexibleTextView2, "ftvError");
                flexibleTextView2.setVisibility(8);
                if (this.f529a.isResumed()) {
                    FlexibleTextView flexibleTextView3 = h35.r;
                    pq7.b(flexibleTextView3, "ftvError");
                    String c = um5.c(flexibleTextView3.getContext(), 2131886231);
                    FragmentActivity activity = this.f529a.getActivity();
                    if (activity != null) {
                        Toast.makeText(activity, c, 0).show();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ h f530a; // = new h();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            cl5.c.d(PortfolioApp.h0.c(), str.hashCode());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f531a;

        @DexIgnore
        public i(by4 by4) {
            this.f531a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886795);
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886794);
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f531a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            pq7.b(c, "title");
            pq7.b(c2, "des");
            s37.E(childFragmentManager, c, c2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<cl7<? extends String, ? extends String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ j f532a; // = new j();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<String, String> cl7) {
            xr4.f4164a.b(cl7.getFirst(), PortfolioApp.h0.c().l0(), cl7.getSecond(), PortfolioApp.h0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<ServerError> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ by4 f533a;

        @DexIgnore
        public k(by4 by4) {
            this.f533a = by4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ServerError serverError) {
            jl5 jl5 = jl5.b;
            pq7.b(serverError, "it");
            Integer code = serverError.getCode();
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            String e = jl5.e(code, message);
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f533a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.A0(childFragmentManager, e);
        }
    }

    /*
    static {
        String simpleName = by4.class.getSimpleName();
        pq7.b(simpleName, "BCFriendTabFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(by4 by4) {
        g37<h35> g37 = by4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ay4 L6(by4 by4) {
        ay4 ay4 = by4.j;
        if (ay4 != null) {
            return ay4;
        }
        pq7.n("friendAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ey4 N6(by4 by4) {
        ey4 ey4 = by4.i;
        if (ey4 != null) {
            return ey4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        ay4 ay4 = new ay4(this);
        this.j = ay4;
        if (ay4 != null) {
            ay4.setHasStableIds(true);
            g37<h35> g37 = this.h;
            if (g37 != null) {
                h35 a2 = g37.a();
                if (a2 != null) {
                    a2.u.setOnRefreshListener(new b(this));
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    ay4 ay42 = this.j;
                    if (ay42 != null) {
                        recyclerView.setAdapter(ay42);
                        RecyclerView recyclerView2 = a2.t;
                        pq7.b(recyclerView2, "rvFriends");
                        new c(a2, recyclerView2, this);
                        return;
                    }
                    pq7.n("friendAdapter");
                    throw null;
                }
                return;
            }
            pq7.n("binding");
            throw null;
        }
        pq7.n("friendAdapter");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        ey4 ey4 = this.i;
        if (ey4 != null) {
            ey4.q().h(getViewLifecycleOwner(), new d(this));
            ey4 ey42 = this.i;
            if (ey42 != null) {
                ey42.m().h(getViewLifecycleOwner(), new e(this));
                ey4 ey43 = this.i;
                if (ey43 != null) {
                    ey43.o().h(getViewLifecycleOwner(), new f(this));
                    ey4 ey44 = this.i;
                    if (ey44 != null) {
                        ey44.p().h(getViewLifecycleOwner(), new g(this));
                        ey4 ey45 = this.i;
                        if (ey45 != null) {
                            ey45.n().h(getViewLifecycleOwner(), h.f530a);
                            ey4 ey46 = this.i;
                            if (ey46 != null) {
                                ey46.r().h(getViewLifecycleOwner(), new i(this));
                                ey4 ey47 = this.i;
                                if (ey47 != null) {
                                    ey47.s().h(getViewLifecycleOwner(), j.f532a);
                                    ey4 ey48 = this.i;
                                    if (ey48 != null) {
                                        ey48.t().h(getViewLifecycleOwner(), new k(this));
                                    } else {
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oy5
    public void R3(xs4 xs4) {
        pq7.c(xs4, "friend");
        ey4 ey4 = this.i;
        if (ey4 != null) {
            ey4.x(xs4, false);
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (i2 == 2131363373) {
            int hashCode = str.hashCode();
            if (hashCode != -1742668797) {
                if (hashCode != -322581840) {
                    if (hashCode == -19037436 && str.equals("UN_FRIEND")) {
                        xs4 xs4 = intent != null ? (xs4) intent.getParcelableExtra("friend_extra") : null;
                        if (xs4 != null) {
                            ey4 ey4 = this.i;
                            if (ey4 != null) {
                                ey4.z(xs4);
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        }
                    }
                } else if (str.equals("BLOCK_FRIEND")) {
                    xs4 xs42 = intent != null ? (xs4) intent.getParcelableExtra("friend_extra") : null;
                    if (xs42 != null) {
                        ey4 ey42 = this.i;
                        if (ey42 != null) {
                            ey42.j(xs42);
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    }
                }
            } else if (str.equals("CANCEL_FRIEND")) {
                xs4 xs43 = intent != null ? (xs4) intent.getParcelableExtra("friend_extra") : null;
                if (xs43 != null) {
                    ey4 ey43 = this.i;
                    if (ey43 != null) {
                        ey43.k(xs43);
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gz4.a
    public void T3(int i2, gz4.b bVar) {
        pq7.c(bVar, "type");
        ay4 ay4 = this.j;
        if (ay4 != null) {
            Object h2 = ay4.h(i2);
            if (!(h2 instanceof xs4)) {
                h2 = null;
            }
            xs4 xs4 = (xs4) h2;
            FLogger.INSTANCE.getLocal().e(l, "position: " + i2 + " - type: " + bVar + " - friend: " + xs4);
            if (xs4 != null) {
                int i3 = cy4.f693a[bVar.ordinal()];
                if (i3 == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("friend_extra", xs4);
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886301);
                    pq7.b(c2, "LanguageHelper.getString\u2026friendThisUserTheyWillBe)");
                    s37.d(childFragmentManager, bundle, "UN_FRIEND", c2);
                } else if (i3 == 2) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putParcelable("friend_extra", xs4);
                    s37 s372 = s37.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    pq7.b(childFragmentManager2, "childFragmentManager");
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886291);
                    pq7.b(c3, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    s372.d(childFragmentManager2, bundle2, "BLOCK_FRIEND", c3);
                } else if (i3 == 3) {
                    ey4 ey4 = this.i;
                    if (ey4 != null) {
                        ey4.y(xs4);
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else if (i3 == 4) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putParcelable("friend_extra", xs4);
                    s37 s373 = s37.c;
                    FragmentManager childFragmentManager3 = getChildFragmentManager();
                    pq7.b(childFragmentManager3, "childFragmentManager");
                    String c4 = um5.c(PortfolioApp.h0.c(), 2131886303);
                    pq7.b(c4, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    s373.d(childFragmentManager3, bundle3, "CANCEL_FRIEND", c4);
                }
            }
        } else {
            pq7.n("friendAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oy5
    public void e4(xs4 xs4) {
        pq7.c(xs4, "friend");
        ey4 ey4 = this.i;
        if (ey4 != null) {
            ey4.x(xs4, true);
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().Q1().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ey4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.i = (ey4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        h35 h35 = (h35) aq0.f(layoutInflater, 2131558513, viewGroup, false, A6());
        this.h = new g37<>(this, h35);
        pq7.b(h35, "binding");
        return h35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        O6();
        P6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
