package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int[] f3780a; // = {4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

    @DexIgnore
    public static int[] a(am4 am4, int i, int i2) {
        int[] iArr = new int[i2];
        int n = am4.n() / i;
        for (int i3 = 0; i3 < n; i3++) {
            int i4 = 0;
            int i5 = 0;
            while (i5 < i) {
                int i6 = am4.l((i3 * i) + i5) ? 1 << ((i - i5) - 1) : 0;
                i5++;
                i4 = i6 | i4;
            }
            iArr[i3] = i4;
        }
        return iArr;
    }

    @DexIgnore
    public static void b(bm4 bm4, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3 += 2) {
            int i4 = i - i3;
            int i5 = i4;
            while (true) {
                int i6 = i + i3;
                if (i5 > i6) {
                    break;
                }
                bm4.n(i5, i4);
                bm4.n(i5, i6);
                bm4.n(i4, i5);
                bm4.n(i6, i5);
                i5++;
            }
        }
        int i7 = i - i2;
        bm4.n(i7, i7);
        int i8 = i7 + 1;
        bm4.n(i8, i7);
        bm4.n(i7, i8);
        int i9 = i + i2;
        bm4.n(i9, i7);
        bm4.n(i9, i8);
        bm4.n(i9, i9 - 1);
    }

    @DexIgnore
    public static void c(bm4 bm4, boolean z, int i, am4 am4) {
        int i2 = 0;
        int i3 = i / 2;
        if (z) {
            while (i2 < 7) {
                int i4 = (i3 - 3) + i2;
                if (am4.l(i2)) {
                    bm4.n(i4, i3 - 5);
                }
                if (am4.l(i2 + 7)) {
                    bm4.n(i3 + 5, i4);
                }
                if (am4.l(20 - i2)) {
                    bm4.n(i4, i3 + 5);
                }
                if (am4.l(27 - i2)) {
                    bm4.n(i3 - 5, i4);
                }
                i2++;
            }
            return;
        }
        while (i2 < 10) {
            int i5 = (i3 - 5) + i2 + (i2 / 5);
            if (am4.l(i2)) {
                bm4.n(i5, i3 - 7);
            }
            if (am4.l(i2 + 10)) {
                bm4.n(i3 + 7, i5);
            }
            if (am4.l(29 - i2)) {
                bm4.n(i5, i3 + 7);
            }
            if (am4.l(39 - i2)) {
                bm4.n(i3 - 7, i5);
            }
            i2++;
        }
    }

    @DexIgnore
    public static tl4 d(byte[] bArr, int i, int i2) {
        am4 am4;
        int i3;
        boolean z;
        int i4;
        int i5;
        int i6;
        am4 a2 = new wl4(bArr).a();
        int n = ((a2.n() * i) / 100) + 11;
        int n2 = a2.n();
        int i7 = 32;
        if (i2 != 0) {
            boolean z2 = i2 < 0;
            i4 = Math.abs(i2);
            if (z2) {
                i7 = 4;
            }
            if (i4 <= i7) {
                i5 = i(i4, z2);
                i3 = f3780a[i4];
                am4 = h(a2, i3);
                if (am4.n() + n > i5 - (i5 % i3)) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                } else if (!z2) {
                    z = z2;
                } else if (am4.n() <= (i3 << 6)) {
                    z = z2;
                } else {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                }
            } else {
                throw new IllegalArgumentException(String.format("Illegal value %s for layers", Integer.valueOf(i2)));
            }
        } else {
            am4 = null;
            int i8 = 0;
            i3 = 0;
            while (i8 <= 32) {
                z = i8 <= 3;
                i4 = z ? i8 + 1 : i8;
                int i9 = i(i4, z);
                if (n2 + n <= i9) {
                    int[] iArr = f3780a;
                    if (i3 != iArr[i4]) {
                        i3 = iArr[i4];
                        am4 = h(a2, i3);
                    }
                    if ((!z || am4.n() <= (i3 << 6)) && am4.n() + n <= i9 - (i9 % i3)) {
                        i5 = i9;
                    }
                }
                i8++;
            }
            throw new IllegalArgumentException("Data too large for an Aztec code");
        }
        am4 e = e(am4, i5, i3);
        int n3 = am4.n() / i3;
        am4 f = f(z, i4, n3);
        int i10 = (i4 << 2) + (z ? 11 : 14);
        int[] iArr2 = new int[i10];
        if (z) {
            for (int i11 = 0; i11 < i10; i11++) {
                iArr2[i11] = i11;
            }
            i6 = i10;
        } else {
            int i12 = i10 / 2;
            i6 = i10 + 1 + (((i12 - 1) / 15) * 2);
            int i13 = i6 / 2;
            for (int i14 = 0; i14 < i12; i14++) {
                int i15 = (i14 / 15) + i14;
                iArr2[(i12 - i14) - 1] = (i13 - i15) - 1;
                iArr2[i12 + i14] = i15 + i13 + 1;
            }
        }
        bm4 bm4 = new bm4(i6);
        int i16 = 0;
        for (int i17 = 0; i17 < i4; i17++) {
            int i18 = ((i4 - i17) << 2) + (z ? 9 : 12);
            int i19 = 0;
            while (true) {
                if (i19 >= i18) {
                    break;
                }
                int i20 = i19 << 1;
                for (int i21 = 0; i21 < 2; i21++) {
                    if (e.l(i16 + i20 + i21)) {
                        int i22 = i17 << 1;
                        bm4.n(iArr2[i22 + i21], iArr2[i22 + i19]);
                    }
                    if (e.l((i18 << 1) + i16 + i20 + i21)) {
                        int i23 = i17 << 1;
                        bm4.n(iArr2[i23 + i19], iArr2[((i10 - 1) - i23) - i21]);
                    }
                    if (e.l((i18 << 2) + i16 + i20 + i21)) {
                        int i24 = (i10 - 1) - (i17 << 1);
                        bm4.n(iArr2[i24 - i21], iArr2[i24 - i19]);
                    }
                    if (e.l((i18 * 6) + i16 + i20 + i21)) {
                        int i25 = i17 << 1;
                        bm4.n(iArr2[((i10 - 1) - i25) - i19], iArr2[i25 + i21]);
                    }
                }
                i19++;
            }
            i16 = (i18 << 3) + i16;
        }
        c(bm4, z, i6, f);
        if (z) {
            b(bm4, i6 / 2, 5);
        } else {
            int i26 = i6 / 2;
            b(bm4, i26, 7);
            int i27 = 0;
            int i28 = 0;
            while (i28 < (i10 / 2) - 1) {
                for (int i29 = i26 & 1; i29 < i6; i29 += 2) {
                    int i30 = i26 - i27;
                    bm4.n(i30, i29);
                    int i31 = i26 + i27;
                    bm4.n(i31, i29);
                    bm4.n(i29, i30);
                    bm4.n(i29, i31);
                }
                i28 += 15;
                i27 += 16;
            }
        }
        tl4 tl4 = new tl4();
        tl4.c(z);
        tl4.f(i6);
        tl4.d(i4);
        tl4.b(n3);
        tl4.e(bm4);
        return tl4;
    }

    @DexIgnore
    public static am4 e(am4 am4, int i, int i2) {
        fm4 fm4 = new fm4(g(i2));
        int i3 = i / i2;
        int[] a2 = a(am4, i2, i3);
        fm4.b(a2, i3 - (am4.n() / i2));
        am4 am42 = new am4();
        am42.g(0, i % i2);
        for (int i4 : a2) {
            am42.g(i4, i2);
        }
        return am42;
    }

    @DexIgnore
    public static am4 f(boolean z, int i, int i2) {
        am4 am4 = new am4();
        if (z) {
            am4.g(i - 1, 2);
            am4.g(i2 - 1, 6);
            return e(am4, 28, 4);
        }
        am4.g(i - 1, 5);
        am4.g(i2 - 1, 11);
        return e(am4, 40, 4);
    }

    @DexIgnore
    public static dm4 g(int i) {
        if (i == 4) {
            return dm4.j;
        }
        if (i == 6) {
            return dm4.i;
        }
        if (i == 8) {
            return dm4.l;
        }
        if (i == 10) {
            return dm4.h;
        }
        if (i == 12) {
            return dm4.g;
        }
        throw new IllegalArgumentException("Unsupported word size " + i);
    }

    @DexIgnore
    public static am4 h(am4 am4, int i) {
        int i2;
        am4 am42 = new am4();
        int n = am4.n();
        int i3 = (1 << i) - 2;
        int i4 = 0;
        while (i4 < n) {
            int i5 = 0;
            for (int i6 = 0; i6 < i; i6++) {
                int i7 = i4 + i6;
                if (i7 >= n || am4.l(i7)) {
                    i5 |= 1 << ((i - 1) - i6);
                }
            }
            int i8 = i5 & i3;
            if (i8 == i3) {
                am42.g(i8, i);
            } else if (i8 == 0) {
                am42.g(i5 | 1, i);
            } else {
                am42.g(i5, i);
                i2 = i4;
                i4 = i2 + i;
            }
            i2 = i4 - 1;
            i4 = i2 + i;
        }
        return am42;
    }

    @DexIgnore
    public static int i(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }
}
