package com.fossil;

import com.fossil.tn7;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un7 implements tn7, Serializable {
    @DexIgnore
    public static /* final */ un7 INSTANCE; // = new un7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public <R> R fold(R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
        pq7.c(vp7, "operation");
        return r;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public <E extends tn7.b> E get(tn7.c<E> cVar) {
        pq7.c(cVar, "key");
        return null;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 minusKey(tn7.c<?> cVar) {
        pq7.c(cVar, "key");
        return this;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 plus(tn7 tn7) {
        pq7.c(tn7, "context");
        return tn7;
    }

    @DexIgnore
    public String toString() {
        return "EmptyCoroutineContext";
    }
}
