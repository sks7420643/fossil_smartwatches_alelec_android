package com.fossil;

import com.fossil.fitness.WorkoutMode;
import com.fossil.fitness.WorkoutType;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionItem;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r47 {
    @DexIgnore
    public static final WorkoutDetectionSetting a(List<WorkoutSetting> list) {
        pq7.c(list, "$this$toWorkoutDetectionSetting");
        ArrayList arrayList = new ArrayList();
        for (WorkoutSetting workoutSetting : list) {
            WorkoutType c = mi5.Companion.c(workoutSetting.getType());
            WorkoutMode b = gi5.Companion.b(workoutSetting.getMode());
            WorkoutDetectionItem workoutDetectionItem = new WorkoutDetectionItem();
            workoutDetectionItem.setType(c);
            workoutDetectionItem.setMode(b);
            workoutDetectionItem.setEnable(workoutSetting.getEnable());
            workoutDetectionItem.setUserConfirmation(workoutSetting.getAskMeFirst());
            workoutDetectionItem.setStartLatency(workoutSetting.getStartLatency());
            workoutDetectionItem.setPauseLatency(workoutSetting.getPauseLatency());
            workoutDetectionItem.setResumeLatency(workoutSetting.getResumeLatency());
            workoutDetectionItem.setStopLatency(workoutSetting.getStopLatency());
            arrayList.add(workoutDetectionItem);
        }
        return new WorkoutDetectionSetting(arrayList);
    }
}
