package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv extends tv {
    @DexIgnore
    public long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wv(short s, k5 k5Var, int i, int i2) {
        super(sv.LEGACY_GET_SIZE_WRITTEN, s, hs.N, k5Var, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.r3, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            long o = hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            this.L = o;
            g80.k(jSONObject, jd0.r3, Long.valueOf(o));
        }
        return jSONObject;
    }
}
