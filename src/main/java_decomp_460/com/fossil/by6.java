package com.fossil;

import com.fossil.jn5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class by6 extends xx6 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ yx6 f;

    @DexIgnore
    public by6(yx6 yx6) {
        pq7.c(yx6, "mPairingInstructionsView");
        this.f = yx6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.f.f();
        this.f.y0(!this.e);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.xx6
    public void n() {
        jn5 jn5 = jn5.b;
        yx6 yx6 = this.f;
        if (yx6 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (jn5.c(jn5, ((sx6) yx6).getContext(), jn5.a.PAIR_DEVICE, false, false, false, null, 60, null)) {
            this.f.l1();
        }
    }

    @DexIgnore
    @Override // com.fossil.xx6
    public boolean o() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.xx6
    public void p(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void q() {
        this.f.M5(this);
    }
}
