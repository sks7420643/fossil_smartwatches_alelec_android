package com.fossil;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cc2 implements DialogInterface.OnClickListener {
    @DexIgnore
    public static cc2 a(Activity activity, Intent intent, int i) {
        return new rd2(intent, activity, i);
    }

    @DexIgnore
    public static cc2 b(o72 o72, Intent intent, int i) {
        return new sd2(intent, o72, i);
    }

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            c();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }
}
