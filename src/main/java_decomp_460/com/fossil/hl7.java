package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl7 {
    @DexIgnore
    public static final <A, B> cl7<A, B> a(A a2, B b) {
        return new cl7<>(a2, b);
    }
}
