package com.fossil;

import java.util.AbstractCollection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px2 extends AbstractCollection<V> {
    @DexIgnore
    public /* final */ /* synthetic */ hx2 b;

    @DexIgnore
    public px2(hx2 hx2) {
        this.b = hx2;
    }

    @DexIgnore
    public final void clear() {
        this.b.clear();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public final Iterator<V> iterator() {
        return this.b.zzg();
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }
}
