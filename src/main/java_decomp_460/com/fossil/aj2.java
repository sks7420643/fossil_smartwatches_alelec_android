package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj2 implements Parcelable.Creator<wi2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wi2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        IBinder iBinder = null;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        zh2 zh2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                zh2 = (zh2) ad2.e(parcel, t, zh2.CREATOR);
            } else if (l == 2) {
                arrayList2 = ad2.j(parcel, t, DataSet.CREATOR);
            } else if (l == 3) {
                arrayList = ad2.j(parcel, t, DataPoint.CREATOR);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                iBinder = ad2.u(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new wi2(zh2, arrayList2, arrayList, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wi2[] newArray(int i) {
        return new wi2[i];
    }
}
