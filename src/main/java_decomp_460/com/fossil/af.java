package com.fossil;

import com.fossil.ix1;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class af extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.FILE_CONFIG, ow.TRANSFER_DATA));
    @DexIgnore
    public /* final */ long D; // = ((long) this.N.length);
    @DexIgnore
    public long E;
    @DexIgnore
    public float F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public /* final */ ve M; // = hd0.y.m();
    @DexIgnore
    public /* final */ byte[] N;
    @DexIgnore
    public /* final */ boolean O;
    @DexIgnore
    public /* final */ short P;
    @DexIgnore
    public /* final */ float Q;

    @DexIgnore
    public af(k5 k5Var, i60 i60, byte[] bArr, boolean z, short s, float f, String str) {
        super(k5Var, i60, yp.l0, str, false, 16);
        this.N = bArr;
        this.O = z;
        this.P = (short) s;
        this.Q = f;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.h(this, new tl(this.w, this.x, this.M, this.z), new jn(this), new vn(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(g80.k(g80.k(super.C(), jd0.J, Long.valueOf(ix1.f1688a.b(this.N, ix1.a.CRC32))), jd0.x0, Boolean.valueOf(this.O)), jd0.A0, hy1.l(this.P, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void D() {
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
    }

    @DexIgnore
    public final void T() {
        long j = this.J + this.I;
        lp.i(this, new aw(j, Math.min(6144L, this.D - j), this.D, this.P, this.w, 0, 32), new mm(this), new xm(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void U() {
        lp.i(this, new ew(this.P, this.w, 0, 4), new up(this), new iq(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void V() {
        long j = this.H;
        lp.i(this, new fw(0, j, this.D, this.P, this.w, 0, 32), new wq(this, ix1.f1688a.a(this.N, (int) 0, (int) j, ix1.a.CRC32)), new kr(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
