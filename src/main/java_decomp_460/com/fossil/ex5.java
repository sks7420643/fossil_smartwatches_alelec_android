package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex5 extends RecyclerView.g<c> implements Filterable {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public List<i06> b; // = new ArrayList();
    @DexIgnore
    public b c;
    @DexIgnore
    public List<i06> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ex5.e;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(i06 i06, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ nd5 f1001a;
        @DexIgnore
        public /* final */ /* synthetic */ ex5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c b;

            @DexIgnore
            public a(c cVar) {
                this.b = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.d;
                    if (list == null) {
                        pq7.i();
                        throw null;
                    } else if (((i06) list.get(adapterPosition)).getUri() != null) {
                        List list2 = this.b.b.d;
                        if (list2 != null) {
                            InstalledApp installedApp = ((i06) list2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = ex5.f.a();
                                local.d(a2, "isSelected = " + booleanValue);
                                b bVar = this.b.b.c;
                                if (bVar != null) {
                                    List list3 = this.b.b.d;
                                    if (list3 != null) {
                                        bVar.a((i06) list3.get(adapterPosition), !booleanValue);
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ex5 ex5, nd5 nd5) {
            super(nd5.n());
            pq7.c(nd5, "binding");
            this.b = ex5;
            this.f1001a = nd5;
            String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (d != null) {
                this.f1001a.q.setBackgroundColor(Color.parseColor(d));
            }
            if (d2 != null) {
                this.f1001a.t.setBackgroundColor(Color.parseColor(d2));
            }
            this.f1001a.u.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(i06 i06) {
            pq7.c(i06, "appWrapper");
            ImageView imageView = this.f1001a.s;
            pq7.b(imageView, "binding.ivAppIcon");
            wj5 a2 = tj5.a(imageView.getContext());
            InstalledApp installedApp = i06.getInstalledApp();
            if (installedApp != null) {
                a2.I(new qj5(installedApp)).c0(i06.getIconResourceId()).n().F0(this.f1001a.s);
                FlexibleTextView flexibleTextView = this.f1001a.r;
                pq7.b(flexibleTextView, "binding.ftvAppName");
                InstalledApp installedApp2 = i06.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (i06.getUri() != null) {
                    FlexibleTextView flexibleTextView2 = this.f1001a.r;
                    pq7.b(flexibleTextView2, "binding.ftvAppName");
                    flexibleTextView2.setTextColor(flexibleTextView2.getTextColors().withAlpha(255));
                    FlexibleSwitchCompat flexibleSwitchCompat = this.f1001a.u;
                    pq7.b(flexibleSwitchCompat, "binding.swEnabled");
                    flexibleSwitchCompat.setEnabled(true);
                    FlexibleSwitchCompat flexibleSwitchCompat2 = this.f1001a.u;
                    pq7.b(flexibleSwitchCompat2, "binding.swEnabled");
                    Drawable background = flexibleSwitchCompat2.getBackground();
                    pq7.b(background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    FlexibleTextView flexibleTextView3 = this.f1001a.r;
                    pq7.b(flexibleTextView3, "binding.ftvAppName");
                    flexibleTextView3.setTextColor(flexibleTextView3.getTextColors().withAlpha(100));
                    FlexibleSwitchCompat flexibleSwitchCompat3 = this.f1001a.u;
                    pq7.b(flexibleSwitchCompat3, "binding.swEnabled");
                    flexibleSwitchCompat3.setEnabled(false);
                    FlexibleSwitchCompat flexibleSwitchCompat4 = this.f1001a.u;
                    pq7.b(flexibleSwitchCompat4, "binding.swEnabled");
                    Drawable background2 = flexibleSwitchCompat4.getBackground();
                    pq7.b(background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                FlexibleSwitchCompat flexibleSwitchCompat5 = this.f1001a.u;
                pq7.b(flexibleSwitchCompat5, "binding.swEnabled");
                InstalledApp installedApp3 = i06.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    flexibleSwitchCompat5.setChecked(isSelected.booleanValue());
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Filter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ex5 f1002a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(ex5 ex5) {
            this.f1002a = ex5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            pq7.c(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.f1002a.b;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (i06 i06 : this.f1002a.b) {
                        InstalledApp installedApp = i06.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            pq7.b(str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && wt7.v(str, lowerCase, false, 2, null)) {
                            arrayList.add(i06);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            pq7.c(charSequence, "charSequence");
            pq7.c(filterResults, "results");
            this.f1002a.d = (List) filterResults.values;
            this.f1002a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = ex5.class.getSimpleName();
        pq7.b(simpleName, "NotificationAppsAdapter::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ex5() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new d(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<i06> list = this.d;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(c cVar, int i) {
        pq7.c(cVar, "holder");
        List<i06> list = this.d;
        if (list != null) {
            cVar.a(list.get(i));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: m */
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        nd5 z = nd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new c(this, z);
    }

    @DexIgnore
    public final void n(List<i06> list) {
        pq7.c(list, "listAppWrapper");
        this.b.clear();
        this.b.addAll(list);
        this.d = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(b bVar) {
        pq7.c(bVar, "listener");
        this.c = bVar;
    }
}
