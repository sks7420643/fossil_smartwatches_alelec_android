package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gb0 implements Parcelable.Creator<ga0> {
    @DexIgnore
    public /* synthetic */ gb0(kq7 kq7) {
    }

    @DexIgnore
    public ga0 a(Parcel parcel) {
        return new ga0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ga0 createFromParcel(Parcel parcel) {
        return new ga0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ga0[] newArray(int i) {
        return new ga0[i];
    }
}
