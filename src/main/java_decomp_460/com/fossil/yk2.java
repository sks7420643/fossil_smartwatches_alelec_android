package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.d42;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk2 extends ec2<zk2> {
    @DexIgnore
    public /* final */ d42.a E;

    @DexIgnore
    public yk2(Context context, Looper looper, ac2 ac2, d42.a aVar, r62.b bVar, r62.c cVar) {
        super(context, looper, 68, ac2, bVar, cVar);
        this.E = aVar;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final Bundle F() {
        d42.a aVar = this.E;
        return aVar == null ? new Bundle() : aVar.a();
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        return queryLocalInterface instanceof zk2 ? (zk2) queryLocalInterface : new al2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public final int s() {
        return 12800000;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
