package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.ix1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fs {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1183a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public long e;
    @DexIgnore
    public a90 f;
    @DexIgnore
    public /* final */ ArrayList<hw> g;
    @DexIgnore
    public long h;
    @DexIgnore
    public gw i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public u5 k;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<fs, tl7>> l;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<fs, tl7>> m;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<fs, tl7>> n;
    @DexIgnore
    public CopyOnWriteArrayList<vp7<fs, Float, tl7>> o;
    @DexIgnore
    public /* final */ gp7<tl7> p;
    @DexIgnore
    public /* final */ bs q;
    @DexIgnore
    public /* final */ ds r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public mw v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ hs x;
    @DexIgnore
    public /* final */ k5 y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public fs(hs hsVar, k5 k5Var, int i2) {
        this.x = hsVar;
        this.y = k5Var;
        this.z = i2;
        this.f1183a = ey1.a(hsVar);
        this.b = e.a("UUID.randomUUID().toString()");
        this.c = "";
        this.d = "";
        this.e = System.currentTimeMillis();
        System.currentTimeMillis();
        this.f = new a90(ey1.a(this.x), v80.e, this.y.x, this.c, this.d, false, null, null, null, g80.k(new JSONObject(), jd0.g2, this.b), 448);
        this.g = new ArrayList<>();
        this.h = 6500;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.j = new Handler(myLooper);
            this.l = new CopyOnWriteArrayList<>();
            this.m = new CopyOnWriteArrayList<>();
            this.n = new CopyOnWriteArrayList<>();
            this.o = new CopyOnWriteArrayList<>();
            this.p = new zr(this);
            this.q = new bs(this);
            this.r = new ds(this);
            this.v = new mw(this.x, this.b, lw.c, null, null, 24);
            this.w = true;
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fs(hs hsVar, k5 k5Var, int i2, int i3) {
        this(hsVar, k5Var, (i3 & 4) != 0 ? 3 : i2);
    }

    @DexIgnore
    public JSONObject A() {
        return new JSONObject();
    }

    @DexIgnore
    public final void B() {
        gw gwVar = this.i;
        if (gwVar != null) {
            this.j.removeCallbacks(gwVar);
        }
        gw gwVar2 = this.i;
        if (gwVar2 != null) {
            gwVar2.b = true;
        }
        this.i = null;
    }

    @DexIgnore
    public final void C() {
        a90 a90 = this.f;
        if (a90 != null) {
            g80.k(a90.n, jd0.u1, Double.valueOf(hy1.f(System.currentTimeMillis())));
            if (this.w) {
                d90.i.d(a90);
            }
            this.f = null;
        }
    }

    @DexIgnore
    public long a(o7 o7Var) {
        return 0;
    }

    @DexIgnore
    public final fs c(rp7<? super fs, tl7> rp7) {
        if (!this.t) {
            this.m.add(rp7);
        } else if (this.v.d != lw.b) {
            rp7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final void d() {
        boolean z2 = true;
        if (!this.t) {
            this.t = true;
            this.u = false;
            B();
            this.y.i.remove(this.q);
            this.y.j.remove(this.r);
            C();
            if (this.w) {
                d90 d90 = d90.i;
                Object[] array = this.g.toArray(new hw[0]);
                if (array != null) {
                    hw[] hwVarArr = (hw[]) array;
                    JSONObject k2 = g80.k(g80.k(new JSONObject(), jd0.k, u(this.v)), jd0.g2, this.b);
                    mw mwVar = this.v;
                    if (mwVar.d == lw.b) {
                        gy1.b(k2, A(), false, 2, null);
                    } else {
                        g80.k(k2, jd0.P0, mwVar.toJSONObject());
                    }
                    if (!(hwVarArr.length == 0)) {
                        JSONArray jSONArray = new JSONArray();
                        for (hw hwVar : hwVarArr) {
                            jSONArray.put(hwVar.toJSONObject());
                        }
                        g80.k(k2, jd0.E0, jSONArray);
                    }
                    String a2 = ey1.a(this.x);
                    v80 v80 = v80.f;
                    String str = this.y.x;
                    String str2 = this.c;
                    String str3 = this.d;
                    if (lw.b != this.v.d) {
                        z2 = false;
                    }
                    d90.d(new a90(a2, v80, str, str2, str3, z2, null, null, null, k2, 448));
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            lw lwVar = lw.b;
            mw mwVar2 = this.v;
            if (lwVar == mwVar2.d) {
                ky1 ky1 = ky1.DEBUG;
                gy1.c(mwVar2.toJSONObject(), A()).toString(2);
                Iterator<T> it = this.l.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                ky1 ky12 = ky1.ERROR;
                gy1.c(mwVar2.toJSONObject(), A()).toString(2);
                Iterator<T> it2 = this.m.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.n.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public final void e(float f2) {
        Iterator<T> it = this.o.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void f(long j2) {
        this.h = j2;
    }

    @DexIgnore
    public void g(u5 u5Var) {
    }

    @DexIgnore
    public final void h(n7 n7Var) {
        if (n7Var instanceof p7) {
            i((p7) n7Var);
        } else if (n7Var instanceof o7) {
            s((o7) n7Var);
        }
    }

    @DexIgnore
    public void i(p7 p7Var) {
        if (p7Var.f2790a == f5.CONNECTED) {
            return;
        }
        if (p7Var.b == x6.k.b) {
            l(lw.o);
        } else {
            l(lw.g);
        }
    }

    @DexIgnore
    public final void k(hw hwVar) {
        this.g.add(hwVar);
    }

    @DexIgnore
    public final void l(lw lwVar) {
        if (!this.t && !this.u) {
            ky1 ky1 = ky1.DEBUG;
            this.u = true;
            this.v = mw.a(this.v, null, null, lwVar, null, null, 27);
            u5 u5Var = this.k;
            if (u5Var == null || u5Var.d) {
                d();
                return;
            }
            int i2 = xr.d[lwVar.ordinal()];
            this.y.n(this.k, i2 != 1 ? i2 != 2 ? i2 != 3 ? r5.m : r5.g : r5.n : r5.f);
        }
    }

    @DexIgnore
    public final void m(mw mwVar) {
        this.v = mwVar;
        d();
    }

    @DexIgnore
    public final void n(gp7<tl7> gp7) {
        B();
        if (x() > 0) {
            this.i = new gw(this, gp7);
            ky1 ky1 = ky1.DEBUG;
            x();
            gw gwVar = this.i;
            if (gwVar != null) {
                this.j.postDelayed(gwVar, x());
            }
        }
    }

    @DexIgnore
    public final fs o(rp7<? super fs, tl7> rp7) {
        if (!this.t) {
            this.l.add(rp7);
        } else if (this.v.d == lw.b) {
            rp7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public String p(mw mwVar) {
        lw lwVar;
        if (xr.b[mwVar.e.c.ordinal()] == 1) {
            return ey1.a(x6.n.a(mwVar.e.d.c));
        }
        switch (xr.f4162a[mwVar.d.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                lwVar = mwVar.d;
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                lwVar = lw.e;
                break;
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                lwVar = lw.u;
                break;
            default:
                lwVar = lw.u;
                break;
        }
        return ey1.a(lwVar);
    }

    @DexIgnore
    public final void q() {
        JSONObject jSONObject;
        if (this.t || this.u) {
            d();
            return;
        }
        u5 w2 = w();
        this.k = w2;
        if (w2 != null) {
            a90 a90 = this.f;
            if (a90 == null || (jSONObject = a90.n) == null) {
                jSONObject = new JSONObject();
            }
            JSONObject c2 = gy1.c(jSONObject, w2.b(false));
            JSONObject jSONObject2 = new JSONObject();
            boolean z2 = this instanceof zs;
            byte[] bArr = z2 ? ((zs) this).G.d : w2 instanceof j6 ? ((j6) w2).m : new byte[0];
            if (!(bArr.length == 0)) {
                int length = bArr.length;
                g80.k(g80.k(g80.k(jSONObject2, jd0.S0, (length > 500 || (z2 && pq7.a(this.c, ey1.a(yp.z)))) ? "" : dy1.e(bArr, null, 1, null)), jd0.T0, Integer.valueOf(length)), jd0.U0, Long.valueOf(ix1.f1688a.b(bArr, ix1.a.CRC32)));
            }
            JSONObject c3 = gy1.c(c2, jSONObject2);
            a90 a902 = this.f;
            if (a902 != null) {
                a902.n = c3;
            }
            k5 k5Var = this.y;
            u5 u5Var = this.k;
            if (u5Var != null) {
                k5Var.m(u5Var);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            y();
        }
    }

    @DexIgnore
    public void r(u5 u5Var) {
        JSONObject jSONObject;
        JSONObject k2;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = false;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null || (k2 = g80.k(jSONObject, jd0.k, p(this.v))) == null)) {
            g80.k(k2, jd0.P0, u5Var.e.toJSONObject());
        }
        C();
        d();
    }

    @DexIgnore
    public void s(o7 o7Var) {
    }

    @DexIgnore
    public final Handler t() {
        return this.j;
    }

    @DexIgnore
    public String u(mw mwVar) {
        String logName;
        switch (xr.c[mwVar.d.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                mt mtVar = mwVar.f;
                return (mtVar == null || (logName = mtVar.getLogName()) == null) ? ey1.a(lw.u) : logName;
            case 8:
            case 9:
                return ey1.a(lw.r);
            default:
                return p(mwVar);
        }
    }

    @DexIgnore
    public void v(u5 u5Var) {
        JSONObject jSONObject;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null)) {
            g80.k(jSONObject, jd0.k, ey1.a(lw.b));
        }
        if (this.v.d == lw.b) {
            g(u5Var);
        }
    }

    @DexIgnore
    public abstract u5 w();

    @DexIgnore
    public long x() {
        return this.h;
    }

    @DexIgnore
    public abstract void y();

    @DexIgnore
    public JSONObject z() {
        return new JSONObject();
    }
}
