package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u7 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ k5 f3520a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public u7(k5 k5Var) {
        this.f3520a = k5Var;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == -1099894406 && action.equals("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED")) {
            b5 a2 = b5.g.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", 0));
            b5 a3 = b5.g.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", 0));
            if (pq7.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE"), this.f3520a.A)) {
                d90.i.d(new a90("hid_connection_state_changed", v80.h, this.f3520a.x, "", "", true, null, null, null, g80.k(g80.k(new JSONObject(), jd0.C0, ey1.a(a2)), jd0.B0, ey1.a(a3)), 448));
                this.f3520a.f(a2, a3);
            }
        }
    }
}
