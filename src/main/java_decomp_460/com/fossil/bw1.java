package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw1 extends ox1 implements Parcelable, Serializable, nx1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bw1 createFromParcel(Parcel parcel) {
            return new bw1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bw1[] newArray(int i) {
            return new bw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ bw1(Parcel parcel, kq7 kq7) {
        String readString = parcel.readString();
        if (readString != null) {
            this.b = readString;
            this.c = parcel.readInt();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public bw1(String str, int i) {
        this.b = str;
        this.c = i;
    }

    @DexIgnore
    public bw1(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("loc");
            pq7.b(string, "jsonObject.getString(UIScriptConstant.LOC)");
            this.b = string;
            this.c = jSONObject.getInt("utc");
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("loc", this.b).put("utc", this.c);
        pq7.b(put, "JSONObject()\n           \u2026ant.UTC, offsetInMinutes)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object
    public bw1 clone() {
        return new bw1(this.b, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getLocation() {
        return this.b;
    }

    @DexIgnore
    public final int getOffsetInMinutes() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
    }
}
