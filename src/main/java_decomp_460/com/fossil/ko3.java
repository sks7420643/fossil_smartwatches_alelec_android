package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ un3 c;

    @DexIgnore
    public ko3(un3 un3, boolean z) {
        this.c = un3;
        this.b = z;
    }

    @DexIgnore
    public final void run() {
        boolean o = this.c.f1780a.o();
        boolean n = this.c.f1780a.n();
        this.c.f1780a.m(this.b);
        if (n == this.b) {
            this.c.f1780a.d().N().b("Default data collection state already set to", Boolean.valueOf(this.b));
        }
        if (this.c.f1780a.o() == o || this.c.f1780a.o() != this.c.f1780a.n()) {
            this.c.f1780a.d().K().c("Default data collection is different than actual status", Boolean.valueOf(this.b), Boolean.valueOf(o));
        }
        this.c.i0();
    }
}
