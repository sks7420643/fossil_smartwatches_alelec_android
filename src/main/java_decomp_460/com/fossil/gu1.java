package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gu1 {
    BUDDY_CHALLENGE((byte) 0);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final gu1 a(byte b) {
            gu1[] values = gu1.values();
            for (gu1 gu1 : values) {
                if (gu1.a() == b) {
                    return gu1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public gu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
