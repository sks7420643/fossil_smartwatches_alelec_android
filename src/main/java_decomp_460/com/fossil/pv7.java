package com.fossil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pv7 extends iw7 implements Runnable {
    @DexIgnore
    public static volatile Thread _thread;
    @DexIgnore
    public static volatile int debugStatus;
    @DexIgnore
    public static /* final */ long h;
    @DexIgnore
    public static /* final */ pv7 i;

    /*
    static {
        Long l;
        pv7 pv7 = new pv7();
        i = pv7;
        hw7.i0(pv7, false, 1, null);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000);
        } catch (SecurityException e) {
            l = 1000L;
        }
        h = timeUnit.toNanos(l.longValue());
    }
    */

    @DexIgnore
    @Override // com.fossil.tv7, com.fossil.iw7
    public dw7 G(long j, Runnable runnable) {
        return H0(j, runnable);
    }

    @DexIgnore
    public final void K0() {
        synchronized (this) {
            if (M0()) {
                debugStatus = 3;
                E0();
                notifyAll();
            }
        }
    }

    @DexIgnore
    public final Thread L0() {
        Thread thread;
        synchronized (this) {
            thread = _thread;
            if (thread == null) {
                thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
                _thread = thread;
                thread.setDaemon(true);
                thread.start();
            }
        }
        return thread;
    }

    @DexIgnore
    public final boolean M0() {
        int i2 = debugStatus;
        return i2 == 2 || i2 == 3;
    }

    @DexIgnore
    public final boolean N0() {
        synchronized (this) {
            if (M0()) {
                return false;
            }
            debugStatus = 1;
            notifyAll();
            return true;
        }
    }

    @DexIgnore
    public void run() {
        wx7.b.d(this);
        xx7 a2 = yx7.a();
        if (a2 != null) {
            a2.c();
        }
        try {
            if (N0()) {
                long j = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long q0 = q0();
                    if (q0 == Long.MAX_VALUE) {
                        int i2 = (j > Long.MAX_VALUE ? 1 : (j == Long.MAX_VALUE ? 0 : -1));
                        if (i2 == 0) {
                            xx7 a3 = yx7.a();
                            long a4 = a3 != null ? a3.a() : System.nanoTime();
                            if (i2 == 0) {
                                j = h + a4;
                            }
                            long j2 = j - a4;
                            if (j2 <= 0) {
                                _thread = null;
                                K0();
                                xx7 a5 = yx7.a();
                                if (a5 != null) {
                                    a5.g();
                                }
                                if (!C0()) {
                                    t0();
                                    return;
                                }
                                return;
                            }
                            q0 = bs7.h(q0, j2);
                        } else {
                            q0 = bs7.h(q0, h);
                        }
                    }
                    if (q0 > 0) {
                        if (M0()) {
                            _thread = null;
                            K0();
                            xx7 a6 = yx7.a();
                            if (a6 != null) {
                                a6.g();
                            }
                            if (!C0()) {
                                t0();
                                return;
                            }
                            return;
                        }
                        xx7 a7 = yx7.a();
                        if (a7 != null) {
                            a7.f(this, q0);
                        } else {
                            LockSupport.parkNanos(this, q0);
                        }
                    }
                }
            }
        } finally {
            _thread = null;
            K0();
            xx7 a8 = yx7.a();
            if (a8 != null) {
                a8.g();
            }
            if (!C0()) {
                t0();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jw7
    public Thread t0() {
        Thread thread = _thread;
        return thread != null ? thread : L0();
    }
}
