package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl7 implements Comparable<jl7> {
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public /* synthetic */ jl7(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static final /* synthetic */ jl7 a(byte b2) {
        return new jl7(b2);
    }

    @DexIgnore
    public static int c(byte b2, byte b3) {
        return pq7.d(b2 & 255, b3 & 255);
    }

    @DexIgnore
    public static byte e(byte b2) {
        return b2;
    }

    @DexIgnore
    public static boolean f(byte b2, Object obj) {
        return (obj instanceof jl7) && b2 == ((jl7) obj).j();
    }

    @DexIgnore
    public static int h(byte b2) {
        return b2;
    }

    @DexIgnore
    public static String i(byte b2) {
        return String.valueOf(b2 & 255);
    }

    @DexIgnore
    public final int b(byte b2) {
        return c(this.b, b2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(jl7 jl7) {
        return b(jl7.j());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return f(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        byte b2 = this.b;
        h(b2);
        return b2;
    }

    @DexIgnore
    public final /* synthetic */ byte j() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return i(this.b);
    }
}
