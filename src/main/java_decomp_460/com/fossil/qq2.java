package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq2 implements Parcelable.Creator<pq2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pq2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Status status = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 1) {
                ad2.B(parcel, t);
            } else {
                status = (Status) ad2.e(parcel, t, Status.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new pq2(status);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pq2[] newArray(int i) {
        return new pq2[i];
    }
}
