package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.d71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e71 extends i71 implements d71 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i81<?> f889a;
    @DexIgnore
    public /* final */ s61 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.memory.PoolableTargetDelegate", f = "TargetDelegate.kt", l = {234}, m = "error")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ e71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(e71 e71, qn7 qn7) {
            super(qn7);
            this.this$0 = e71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.memory.PoolableTargetDelegate", f = "TargetDelegate.kt", l = {213}, m = "success")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ e71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(e71 e71, qn7 qn7) {
            super(qn7);
            this.this$0 = e71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, false, null, this);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e71(i81<?> i81, s61 s61) {
        super(null);
        pq7.c(i81, "target");
        pq7.c(s61, "referenceCounter");
        this.f889a = i81;
        this.b = s61;
    }

    @DexIgnore
    @Override // com.fossil.d71
    public s61 a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.d71
    public i81<?> b() {
        return this.f889a;
    }

    @DexIgnore
    @Override // com.fossil.d71
    public void c(Bitmap bitmap) {
        d71.a.a(this, bitmap);
    }

    @DexIgnore
    @Override // com.fossil.d71
    public void d(Bitmap bitmap) {
        d71.a.b(this, bitmap);
    }

    @DexIgnore
    @Override // com.fossil.i71
    public void e() {
        d(null);
        b().e();
        c(null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    @Override // com.fossil.i71
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object f(android.graphics.drawable.Drawable r9, com.fossil.n81 r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
            r8 = this;
            r7 = 0
            r6 = 5
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.fossil.e71.a
            if (r0 == 0) goto L_0x004b
            r0 = r11
            com.fossil.e71$a r0 = (com.fossil.e71.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004b
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0016:
            java.lang.Object r4 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x005a
            if (r0 != r5) goto L_0x0052
            java.lang.Object r0 = r2.L$7
            com.fossil.n81 r0 = (com.fossil.n81) r0
            java.lang.Object r0 = r2.L$6
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            java.lang.Object r0 = r2.L$5
            com.fossil.j81 r0 = (com.fossil.j81) r0
            java.lang.Object r0 = r2.L$4
            com.fossil.i81 r0 = (com.fossil.i81) r0
            java.lang.Object r0 = r2.L$3
            com.fossil.e71 r0 = (com.fossil.e71) r0
            java.lang.Object r1 = r2.L$2
            com.fossil.n81 r1 = (com.fossil.n81) r1
            java.lang.Object r1 = r2.L$1
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            java.lang.Object r1 = r2.L$0
            com.fossil.e71 r1 = (com.fossil.e71) r1
            com.fossil.el7.b(r4)
        L_0x0045:
            r0.c(r7)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x004a:
            return r0
        L_0x004b:
            com.fossil.e71$a r0 = new com.fossil.e71$a
            r0.<init>(r8, r11)
            r2 = r0
            goto L_0x0016
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005a:
            com.fossil.el7.b(r4)
            r8.d(r7)
            com.fossil.i81 r1 = r8.b()
            if (r10 != 0) goto L_0x006b
            r1.c(r9)
        L_0x0069:
            r0 = r8
            goto L_0x0045
        L_0x006b:
            boolean r0 = r1 instanceof com.fossil.p81
            if (r0 != 0) goto L_0x00a6
            com.fossil.q81 r0 = com.fossil.q81.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00a2
            com.fossil.q81 r0 = com.fossil.q81.c
            int r0 = r0.b()
            if (r0 > r6) goto L_0x00a2
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Ignoring '"
            r0.append(r2)
            r0.append(r10)
            java.lang.String r2 = "' as '"
            r0.append(r2)
            r0.append(r1)
            java.lang.String r2 = "' does not implement coil.transition.TransitionTarget."
            r0.append(r2)
            java.lang.String r2 = "TargetDelegate"
            java.lang.String r0 = r0.toString()
            android.util.Log.println(r6, r2, r0)
        L_0x00a2:
            r1.c(r9)
            goto L_0x0069
        L_0x00a6:
            r0 = r1
            com.fossil.p81 r0 = (com.fossil.p81) r0
            com.fossil.o81$a r4 = new com.fossil.o81$a
            r4.<init>(r9)
            r2.L$0 = r8
            r2.L$1 = r9
            r2.L$2 = r10
            r2.L$3 = r8
            r2.L$4 = r1
            r2.L$5 = r1
            r2.L$6 = r9
            r2.L$7 = r10
            r2.label = r5
            java.lang.Object r0 = r10.a(r0, r4, r2)
            if (r0 != r3) goto L_0x0069
            r0 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e71.f(android.graphics.drawable.Drawable, com.fossil.n81, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.i71
    public void h(BitmapDrawable bitmapDrawable, Drawable drawable) {
        Bitmap bitmap = bitmapDrawable != null ? bitmapDrawable.getBitmap() : null;
        d(bitmap);
        b().d(drawable);
        c(bitmap);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    @Override // com.fossil.i71
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object i(android.graphics.drawable.Drawable r9, boolean r10, com.fossil.n81 r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
        // Method dump skipped, instructions count: 223
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e71.i(android.graphics.drawable.Drawable, boolean, com.fossil.n81, com.fossil.qn7):java.lang.Object");
    }
}
