package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am7<T> implements Collection<T>, jr7 {
    @DexIgnore
    public /* final */ T[] b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public am7(T[] tArr, boolean z) {
        pq7.c(tArr, "values");
        this.b = tArr;
        this.c = z;
    }

    @DexIgnore
    public int a() {
        return this.b.length;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return em7.B(this.b, obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        pq7.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.b.length == 0;
    }

    @DexIgnore
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<T> iterator() {
        return eq7.a(this.b);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public final Object[] toArray() {
        return gm7.a(this.b, this.c);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) jq7.b(this, tArr);
    }
}
