package com.fossil;

import android.content.Context;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kg7 extends ng7 {
    @DexIgnore
    public lg7 m;
    @DexIgnore
    public long n; // = -1;

    @DexIgnore
    public kg7(Context context, int i, String str, jg7 jg7) {
        super(context, i, jg7);
        lg7 lg7 = new lg7();
        this.m = lg7;
        lg7.f2191a = str;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public og7 a() {
        return og7.d;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public boolean b(JSONObject jSONObject) {
        String str;
        jSONObject.put("ei", this.m.f2191a);
        long j = this.n;
        if (j > 0) {
            jSONObject.put("du", j);
        }
        Object obj = this.m.b;
        if (obj == null) {
            j();
            obj = this.m.c;
            str = "kv";
        } else {
            str = "ar";
        }
        jSONObject.put(str, obj);
        return true;
    }

    @DexIgnore
    public lg7 i() {
        return this.m;
    }

    @DexIgnore
    public final void j() {
        Properties w;
        String str = this.m.f2191a;
        if (!(str == null || (w = ig7.w(str)) == null || w.size() <= 0)) {
            JSONObject jSONObject = this.m.c;
            if (jSONObject == null || jSONObject.length() == 0) {
                this.m.c = new JSONObject(w);
                return;
            }
            for (Map.Entry entry : w.entrySet()) {
                try {
                    this.m.c.put(entry.getKey().toString(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
