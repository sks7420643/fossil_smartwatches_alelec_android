package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.migration.MigrationActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c27 extends pv5 implements j07 {
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public i07 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final c27 a() {
            return new c27();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: K6 */
    public void M5(i07 i07) {
        pq7.c(i07, "presenter");
        this.g = i07;
    }

    @DexIgnore
    @Override // com.fossil.j07
    public void a3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.j07
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            pq7.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.j07
    public void o3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            MigrationActivity.a aVar = MigrationActivity.A;
            pq7.b(activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558628, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(2131362731);
        ImageView imageView2 = (ImageView) inflate.findViewById(2131362672);
        if (wr4.f3989a.a().l()) {
            pq7.b(imageView, "ivLogo");
            imageView.setVisibility(0);
            pq7.b(imageView2, "ivBackground");
            imageView2.setVisibility(4);
        }
        return inflate;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        i07 i07 = this.g;
        if (i07 != null) {
            i07.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        i07 i07 = this.g;
        if (i07 != null) {
            i07.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
