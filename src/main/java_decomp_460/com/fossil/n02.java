package com.fossil;

import android.content.Context;
import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n02 implements Closeable {

    @DexIgnore
    public interface a {
        @DexIgnore
        a a(Context context);

        @DexIgnore
        n02 build();
    }

    @DexIgnore
    public abstract k22 a();

    @DexIgnore
    public abstract m02 b();

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        a().close();
    }
}
