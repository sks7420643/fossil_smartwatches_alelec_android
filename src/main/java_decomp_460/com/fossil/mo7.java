package com.fossil;

import com.facebook.internal.NativeProtocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lo7 f2407a;

    /*
    static {
        lo7 lo7;
        Object newInstance;
        Object newInstance2;
        int a2 = a();
        if (a2 >= 65544) {
            try {
                Object newInstance3 = Class.forName("com.fossil.po7").newInstance();
                pq7.b(newInstance3, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance3 != null) {
                    try {
                        lo7 = (lo7) newInstance3;
                        f2407a = lo7;
                    } catch (ClassCastException e) {
                        ClassLoader classLoader = newInstance3.getClass().getClassLoader();
                        ClassLoader classLoader2 = lo7.class.getClassLoader();
                        Throwable initCause = new ClassCastException("Instance classloader: " + classLoader + ", base type classloader: " + classLoader2).initCause(e);
                        pq7.b(initCause, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                        throw initCause;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
            } catch (ClassNotFoundException e2) {
                try {
                    newInstance2 = Class.forName("kotlin.internal.JRE8PlatformImplementations").newInstance();
                    pq7.b(newInstance2, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance2 != null) {
                        lo7 = (lo7) newInstance2;
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException e3) {
                }
            } catch (ClassCastException e4) {
                ClassLoader classLoader3 = newInstance2.getClass().getClassLoader();
                ClassLoader classLoader4 = lo7.class.getClassLoader();
                Throwable initCause2 = new ClassCastException("Instance classloader: " + classLoader3 + ", base type classloader: " + classLoader4).initCause(e4);
                pq7.b(initCause2, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause2;
            }
        }
        if (a2 >= 65543) {
            try {
                Object newInstance4 = Class.forName("com.fossil.oo7").newInstance();
                pq7.b(newInstance4, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance4 != null) {
                    try {
                        lo7 = (lo7) newInstance4;
                        f2407a = lo7;
                    } catch (ClassCastException e5) {
                        ClassLoader classLoader5 = newInstance4.getClass().getClassLoader();
                        ClassLoader classLoader6 = lo7.class.getClassLoader();
                        Throwable initCause3 = new ClassCastException("Instance classloader: " + classLoader5 + ", base type classloader: " + classLoader6).initCause(e5);
                        pq7.b(initCause3, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                        throw initCause3;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
            } catch (ClassNotFoundException e6) {
                try {
                    newInstance = Class.forName("kotlin.internal.JRE7PlatformImplementations").newInstance();
                    pq7.b(newInstance, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance != null) {
                        lo7 = (lo7) newInstance;
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException e7) {
                }
            } catch (ClassCastException e8) {
                ClassLoader classLoader7 = newInstance.getClass().getClassLoader();
                ClassLoader classLoader8 = lo7.class.getClassLoader();
                Throwable initCause4 = new ClassCastException("Instance classloader: " + classLoader7 + ", base type classloader: " + classLoader8).initCause(e8);
                pq7.b(initCause4, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause4;
            }
        }
        lo7 = new lo7();
        f2407a = lo7;
    }
    */

    @DexIgnore
    public static final int a() {
        int i;
        String property = System.getProperty("java.specification.version");
        if (property == null) {
            return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
        }
        int F = wt7.F(property, '.', 0, false, 6, null);
        if (F < 0) {
            try {
                i = Integer.parseInt(property) * 65536;
            } catch (NumberFormatException e) {
                i = 65542;
            }
            return i;
        }
        int i2 = F + 1;
        int F2 = wt7.F(property, '.', i2, false, 4, null);
        if (F2 < 0) {
            F2 = property.length();
        }
        if (property != null) {
            String substring = property.substring(0, F);
            pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            if (property != null) {
                String substring2 = property.substring(i2, F2);
                pq7.b(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                try {
                    return (Integer.parseInt(substring) * 65536) + Integer.parseInt(substring2);
                } catch (NumberFormatException e2) {
                    return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
                }
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        } else {
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
    }
}
