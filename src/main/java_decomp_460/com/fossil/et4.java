package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ss4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<lt4> {
    }

    @DexIgnore
    public final String a(ss4 ss4) {
        try {
            return new Gson().t(ss4);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String b(Date date) {
        String str;
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.n.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                return str;
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            str = null;
        }
    }

    @DexIgnore
    public final String c(lt4 lt4) {
        try {
            return new Gson().t(lt4);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final ss4 d(String str) {
        try {
            return (ss4) new Gson().l(str, new a().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final lt4 e(String str) {
        try {
            return (lt4) new Gson().l(str, new b().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final Date f(String str) {
        Date date;
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.n.get();
            if (simpleDateFormat != null) {
                date = simpleDateFormat.parse(str);
                return date;
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            date = null;
        }
    }
}
