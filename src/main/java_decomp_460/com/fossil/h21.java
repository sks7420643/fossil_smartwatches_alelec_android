package com.fossil;

import android.content.Context;
import com.fossil.k21;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h21 implements k21.a {
    @DexIgnore
    public static /* final */ String d; // = x01.f("WorkConstraintsTracker");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ g21 f1413a;
    @DexIgnore
    public /* final */ k21<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public h21(Context context, k41 k41, g21 g21) {
        Context applicationContext = context.getApplicationContext();
        this.f1413a = g21;
        this.b = new k21[]{new i21(applicationContext, k41), new j21(applicationContext, k41), new p21(applicationContext, k41), new l21(applicationContext, k41), new o21(applicationContext, k41), new n21(applicationContext, k41), new m21(applicationContext, k41)};
    }

    @DexIgnore
    @Override // com.fossil.k21.a
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String str : list) {
                if (c(str)) {
                    x01.c().a(d, String.format("Constraints met for %s", str), new Throwable[0]);
                    arrayList.add(str);
                }
            }
            if (this.f1413a != null) {
                this.f1413a.f(arrayList);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k21.a
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.f1413a != null) {
                this.f1413a.b(list);
            }
        }
    }

    @DexIgnore
    public boolean c(String str) {
        synchronized (this.c) {
            k21<?>[] k21Arr = this.b;
            for (k21<?> k21 : k21Arr) {
                if (k21.d(str)) {
                    x01.c().a(d, String.format("Work %s constrained by %s", str, k21.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void d(Iterable<o31> iterable) {
        synchronized (this.c) {
            for (k21<?> k21 : this.b) {
                k21.g(null);
            }
            for (k21<?> k212 : this.b) {
                k212.e(iterable);
            }
            for (k21<?> k213 : this.b) {
                k213.g(this);
            }
        }
    }

    @DexIgnore
    public void e() {
        synchronized (this.c) {
            for (k21<?> k21 : this.b) {
                k21.f();
            }
        }
    }
}
