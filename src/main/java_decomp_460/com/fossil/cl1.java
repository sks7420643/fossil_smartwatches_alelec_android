package com.fossil;

import com.fossil.bl1;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum cl1 implements zx1 {
    BLUETOOTH_OFF(0),
    INVALID_PARAMETERS(1),
    DEVICE_BUSY(2),
    EXECUTION_TIMEOUT(3),
    REQUEST_FAILED(4),
    REQUEST_UNSUPPORTED(5),
    RESPONSE_TIMEOUT(6),
    RESPONSE_FAILED(7),
    CONNECTION_DROPPED(8),
    INTERRUPTED(9),
    SERVICE_CHANGED(10),
    AUTHENTICATION_FAILED(11),
    SECRET_KEY_IS_REQUIRED(12),
    DATA_SIZE_OVER_LIMIT(13),
    UNSUPPORTED_FORMAT(14),
    TARGET_FIRMWARE_NOT_MATCHED(15),
    REQUEST_TIMEOUT(16),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final cl1 a(nr nrVar, HashMap<bl1.b, Object> hashMap) {
            Boolean bool = (Boolean) hashMap.get(bl1.b.HAS_SERVICE_CHANGED);
            boolean booleanValue = bool != null ? bool.booleanValue() : false;
            switch (c1.f542a[nrVar.c.ordinal()]) {
                case 1:
                    return cl1.INTERRUPTED;
                case 2:
                    return booleanValue ? cl1.SERVICE_CHANGED : cl1.CONNECTION_DROPPED;
                case 3:
                    return cl1.REQUEST_UNSUPPORTED;
                case 4:
                    mw mwVar = nrVar.d;
                    lw lwVar = mwVar.d;
                    return lwVar == lw.q ? cl1.REQUEST_TIMEOUT : lwVar == lw.r ? cl1.RESPONSE_TIMEOUT : (lwVar == lw.e || lwVar == lw.s) ? cl1.RESPONSE_FAILED : mwVar.f == ku.e ? cl1.DATA_SIZE_OVER_LIMIT : cl1.REQUEST_FAILED;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    return cl1.RESPONSE_FAILED;
                case 16:
                    return cl1.EXECUTION_TIMEOUT;
                case 17:
                case 18:
                    return cl1.REQUEST_UNSUPPORTED;
                case 19:
                    return cl1.BLUETOOTH_OFF;
                case 20:
                case 21:
                    return cl1.AUTHENTICATION_FAILED;
                case 22:
                    return cl1.SECRET_KEY_IS_REQUIRED;
                case 23:
                    return cl1.INVALID_PARAMETERS;
                case 24:
                    return cl1.REQUEST_FAILED;
                case 25:
                    return cl1.DEVICE_BUSY;
                case 26:
                    return cl1.UNSUPPORTED_FORMAT;
                case 27:
                    return cl1.TARGET_FIRMWARE_NOT_MATCHED;
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                    return cl1.UNKNOWN_ERROR;
                default:
                    throw new al7();
            }
        }
    }

    @DexIgnore
    public cl1(int i) {
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public int getCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public String getLogName() {
        return this.b;
    }
}
