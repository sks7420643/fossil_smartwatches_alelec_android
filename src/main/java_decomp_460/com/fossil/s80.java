package com.fossil;

import java.io.File;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s80 implements c88<String> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ u80 f3216a;
    @DexIgnore
    public /* final */ /* synthetic */ File b;

    @DexIgnore
    public s80(u80 u80, File file) {
        this.f3216a = u80;
        this.b = file;
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onFailure(Call<String> call, Throwable th) {
        m80.c.a("LogUploader", "Upload Log Fail: throw=%s.", th.getMessage());
        this.f3216a.d();
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onResponse(Call<String> call, q88<String> q88) {
        m80 m80 = m80.c;
        String f = q88.f();
        int b2 = q88.b();
        String a2 = q88.a();
        w18 d = q88.d();
        m80.a("LogUploader", "Upload Log Response: message=%s, code=%s, body=%s, error body=%s.", f, Integer.valueOf(b2), a2, d != null ? d.string() : null);
        int b3 = q88.b();
        if ((200 <= b3 && 299 >= b3) || b3 == 400 || b3 == 413) {
            this.f3216a.c.remove(this.b);
            this.b.delete();
            this.f3216a.e();
            return;
        }
        this.f3216a.d();
    }
}
