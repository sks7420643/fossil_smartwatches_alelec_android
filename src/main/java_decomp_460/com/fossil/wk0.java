package com.fossil;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wk0 extends Service {
    @DexIgnore
    public static /* final */ HashMap<ComponentName, h> h; // = new HashMap<>();
    @DexIgnore
    public b b;
    @DexIgnore
    public h c;
    @DexIgnore
    public a d;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f; // = false;
    @DexIgnore
    public /* final */ ArrayList<d> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            while (true) {
                e a2 = wk0.this.a();
                if (a2 == null) {
                    return null;
                }
                wk0.this.e(a2.getIntent());
                a2.a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void onCancelled(Void r2) {
            wk0.this.g();
        }

        @DexIgnore
        /* renamed from: c */
        public void onPostExecute(Void r2) {
            wk0.this.g();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        IBinder a();

        @DexIgnore
        e b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends h {
        @DexIgnore
        public /* final */ PowerManager.WakeLock d;
        @DexIgnore
        public /* final */ PowerManager.WakeLock e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public c(Context context, ComponentName componentName) {
            super(componentName);
            context.getApplicationContext();
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            PowerManager.WakeLock newWakeLock = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.d = newWakeLock;
            newWakeLock.setReferenceCounted(false);
            PowerManager.WakeLock newWakeLock2 = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.e = newWakeLock2;
            newWakeLock2.setReferenceCounted(false);
        }

        @DexIgnore
        @Override // com.fossil.wk0.h
        public void b() {
            synchronized (this) {
                if (this.g) {
                    if (this.f) {
                        this.d.acquire(60000);
                    }
                    this.g = false;
                    this.e.release();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.wk0.h
        public void c() {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    this.e.acquire(600000);
                    this.d.release();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.wk0.h
        public void d() {
            synchronized (this) {
                this.f = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Intent f3957a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public d(Intent intent, int i) {
            this.f3957a = intent;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.wk0.e
        public void a() {
            wk0.this.stopSelf(this.b);
        }

        @DexIgnore
        @Override // com.fossil.wk0.e
        public Intent getIntent() {
            return this.f3957a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Intent getIntent();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends JobServiceEngine implements b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ wk0 f3958a;
        @DexIgnore
        public /* final */ Object b; // = new Object();
        @DexIgnore
        public JobParameters c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ JobWorkItem f3959a;

            @DexIgnore
            public a(JobWorkItem jobWorkItem) {
                this.f3959a = jobWorkItem;
            }

            @DexIgnore
            @Override // com.fossil.wk0.e
            public void a() {
                synchronized (f.this.b) {
                    if (f.this.c != null) {
                        f.this.c.completeWork(this.f3959a);
                    }
                }
            }

            @DexIgnore
            @Override // com.fossil.wk0.e
            public Intent getIntent() {
                return this.f3959a.getIntent();
            }
        }

        @DexIgnore
        public f(wk0 wk0) {
            super(wk0);
            this.f3958a = wk0;
        }

        @DexIgnore
        @Override // com.fossil.wk0.b
        public IBinder a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            return new com.fossil.wk0.f.a(r3, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0011, code lost:
            if (r2 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
            r2.getIntent().setExtrasClassLoader(r3.f3958a.getClassLoader());
         */
        @DexIgnore
        @Override // com.fossil.wk0.b
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.fossil.wk0.e b() {
            /*
                r3 = this;
                r0 = 0
                java.lang.Object r1 = r3.b
                monitor-enter(r1)
                android.app.job.JobParameters r2 = r3.c     // Catch:{ all -> 0x0026 }
                if (r2 != 0) goto L_0x000a
                monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            L_0x0009:
                return r0
            L_0x000a:
                android.app.job.JobParameters r2 = r3.c     // Catch:{ all -> 0x0026 }
                android.app.job.JobWorkItem r2 = r2.dequeueWork()     // Catch:{ all -> 0x0026 }
                monitor-exit(r1)     // Catch:{ all -> 0x0026 }
                if (r2 == 0) goto L_0x0009
                android.content.Intent r0 = r2.getIntent()
                com.fossil.wk0 r1 = r3.f3958a
                java.lang.ClassLoader r1 = r1.getClassLoader()
                r0.setExtrasClassLoader(r1)
                com.fossil.wk0$f$a r0 = new com.fossil.wk0$f$a
                r0.<init>(r2)
                goto L_0x0009
            L_0x0026:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wk0.f.b():com.fossil.wk0$e");
        }

        @DexIgnore
        public boolean onStartJob(JobParameters jobParameters) {
            this.c = jobParameters;
            this.f3958a.c(false);
            return true;
        }

        @DexIgnore
        public boolean onStopJob(JobParameters jobParameters) {
            boolean b2 = this.f3958a.b();
            synchronized (this.b) {
                this.c = null;
            }
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends h {
        @DexIgnore
        public /* final */ JobInfo d;

        @DexIgnore
        public g(Context context, ComponentName componentName, int i) {
            super(componentName);
            a(i);
            this.d = new JobInfo.Builder(i, this.f3960a).setOverrideDeadline(0).build();
            JobScheduler jobScheduler = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ComponentName f3960a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c;

        @DexIgnore
        public h(ComponentName componentName) {
            this.f3960a = componentName;
        }

        @DexIgnore
        public void a(int i) {
            if (!this.b) {
                this.b = true;
                this.c = i;
            } else if (this.c != i) {
                throw new IllegalArgumentException("Given job ID " + i + " is different than previous " + this.c);
            }
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void c() {
        }

        @DexIgnore
        public void d() {
        }
    }

    @DexIgnore
    public wk0() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.g = null;
        } else {
            this.g = new ArrayList<>();
        }
    }

    @DexIgnore
    public static h d(Context context, ComponentName componentName, boolean z, int i) {
        h hVar = h.get(componentName);
        if (hVar == null) {
            if (Build.VERSION.SDK_INT < 26) {
                hVar = new c(context, componentName);
            } else if (z) {
                hVar = new g(context, componentName, i);
            } else {
                throw new IllegalArgumentException("Can't be here without a job id");
            }
            h.put(componentName, hVar);
        }
        return hVar;
    }

    @DexIgnore
    public e a() {
        b bVar = this.b;
        if (bVar != null) {
            return bVar.b();
        }
        synchronized (this.g) {
            if (this.g.size() <= 0) {
                return null;
            }
            return this.g.remove(0);
        }
    }

    @DexIgnore
    public boolean b() {
        a aVar = this.d;
        if (aVar != null) {
            aVar.cancel(this.e);
        }
        return f();
    }

    @DexIgnore
    public void c(boolean z) {
        if (this.d == null) {
            this.d = new a();
            h hVar = this.c;
            if (hVar != null && z) {
                hVar.c();
            }
            this.d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @DexIgnore
    public abstract void e(Intent intent);

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public void g() {
        ArrayList<d> arrayList = this.g;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.d = null;
                if (this.g != null && this.g.size() > 0) {
                    c(false);
                } else if (!this.f) {
                    this.c.b();
                }
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        b bVar = this.b;
        if (bVar != null) {
            return bVar.a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.b = new f(this);
            this.c = null;
            return;
        }
        this.b = null;
        this.c = d(this, new ComponentName(this, wk0.class), false, 0);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        ArrayList<d> arrayList = this.g;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.f = true;
                this.c.b();
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.g == null) {
            return 2;
        }
        this.c.d();
        synchronized (this.g) {
            ArrayList<d> arrayList = this.g;
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new d(intent, i2));
            c(true);
        }
        return 3;
    }
}
