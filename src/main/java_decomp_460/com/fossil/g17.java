package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class g17 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1246a;

    /*
    static {
        int[] iArr = new int[zh5.values().length];
        f1246a = iArr;
        iArr[zh5.SYNC_IS_IN_PROGRESS.ordinal()] = 1;
        f1246a[zh5.FAIL_DUE_TO_LACK_PERMISSION.ordinal()] = 2;
        f1246a[zh5.FAIL_DUE_TO_PENDING_WORKOUT.ordinal()] = 3;
        f1246a[zh5.FAIL_DUE_TO_SYNC_FAIL.ordinal()] = 4;
        f1246a[zh5.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT.ordinal()] = 5;
    }
    */
}
