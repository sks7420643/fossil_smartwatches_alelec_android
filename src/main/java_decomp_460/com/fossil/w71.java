package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w71 implements Iterable<cl7<? extends String, ? extends c>>, jr7 {
    @DexIgnore
    public static /* final */ w71 c; // = new a().a();
    @DexIgnore
    public static /* final */ b d; // = new b(null);
    @DexIgnore
    public /* final */ SortedMap<String, c> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SortedMap<String, c> f3890a; // = ym7.d(new cl7[0]);

        @DexIgnore
        public final w71 a() {
            return new w71(ym7.f(this.f3890a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f3891a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (!pq7.a(this.f3891a, cVar.f3891a) || !pq7.a(this.b, cVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Object obj = this.f3891a;
            int hashCode = obj != null ? obj.hashCode() : 0;
            String str = this.b;
            if (str != null) {
                i = str.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Entry(value=" + this.f3891a + ", cacheKey=" + this.b + ")";
        }
    }

    @DexIgnore
    public w71(SortedMap<String, c> sortedMap) {
        this.b = sortedMap;
    }

    @DexIgnore
    public /* synthetic */ w71(SortedMap sortedMap, kq7 kq7) {
        this(sortedMap);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return pq7.a(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final boolean isEmpty() {
        return this.b.isEmpty();
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator<com.fossil.cl7<java.lang.String, com.fossil.w71$c>>' to match base method */
    @Override // java.lang.Iterable
    public Iterator<cl7<? extends String, ? extends c>> iterator() {
        SortedMap<String, c> sortedMap = this.b;
        ArrayList arrayList = new ArrayList(sortedMap.size());
        for (Map.Entry<String, c> entry : sortedMap.entrySet()) {
            arrayList.add(hl7.a(entry.getKey(), entry.getValue()));
        }
        return arrayList.iterator();
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
