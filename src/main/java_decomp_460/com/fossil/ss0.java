package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ss0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ls0<X> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ js0 f3291a;
        @DexIgnore
        public /* final */ /* synthetic */ gi0 b;

        @DexIgnore
        public a(js0 js0, gi0 gi0) {
            this.f3291a = js0;
            this.b = gi0;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(X x) {
            this.f3291a.o(this.b.apply(x));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ls0<X> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public LiveData<Y> f3292a;
        @DexIgnore
        public /* final */ /* synthetic */ gi0 b;
        @DexIgnore
        public /* final */ /* synthetic */ js0 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ls0<Y> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.ls0
            public void onChanged(Y y) {
                b.this.c.o(y);
            }
        }

        @DexIgnore
        public b(gi0 gi0, js0 js0) {
            this.b = gi0;
            this.c = js0;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.f3292a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.q(liveData2);
                }
                this.f3292a = liveData;
                if (liveData != null) {
                    this.c.p(liveData, new a());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ls0<X> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3294a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ js0 b;

        @DexIgnore
        public c(js0 js0) {
            this.b = js0;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(X x) {
            Object e = this.b.e();
            if (this.f3294a || ((e == null && x != null) || (e != null && !e.equals(x)))) {
                this.f3294a = false;
                this.b.o(x);
            }
        }
    }

    @DexIgnore
    public static <X> LiveData<X> a(LiveData<X> liveData) {
        js0 js0 = new js0();
        js0.p(liveData, new c(js0));
        return js0;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, gi0<X, Y> gi0) {
        js0 js0 = new js0();
        js0.p(liveData, new a(js0, gi0));
        return js0;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> c(LiveData<X> liveData, gi0<X, LiveData<Y>> gi0) {
        js0 js0 = new js0();
        js0.p(liveData, new b(gi0, js0));
        return js0;
    }
}
