package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import com.fossil.kl0;
import com.fossil.nl0;
import com.fossil.zm0;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"NewApi"})
public class sl0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ yl0 f3276a;
    @DexIgnore
    public static /* final */ ej0<String, Typeface> b; // = new ej0<>(16);

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            f3276a = new xl0();
        } else if (i >= 28) {
            f3276a = new wl0();
        } else if (i >= 26) {
            f3276a = new vl0();
        } else if (i >= 24 && ul0.m()) {
            f3276a = new ul0();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f3276a = new tl0();
        } else {
            f3276a = new yl0();
        }
    }
    */

    @DexIgnore
    public static Typeface a(Context context, Typeface typeface, int i) {
        Typeface g;
        if (context != null) {
            return (Build.VERSION.SDK_INT >= 21 || (g = g(context, typeface, i)) == null) ? Typeface.create(typeface, i) : g;
        }
        throw new IllegalArgumentException("Context cannot be null");
    }

    @DexIgnore
    public static Typeface b(Context context, CancellationSignal cancellationSignal, zm0.f[] fVarArr, int i) {
        return f3276a.c(context, cancellationSignal, fVarArr, i);
    }

    @DexIgnore
    public static Typeface c(Context context, kl0.a aVar, Resources resources, int i, int i2, nl0.a aVar2, Handler handler, boolean z) {
        Typeface b2;
        if (aVar instanceof kl0.d) {
            kl0.d dVar = (kl0.d) aVar;
            boolean z2 = false;
            if (!z ? aVar2 == null : dVar.a() == 0) {
                z2 = true;
            }
            b2 = zm0.g(context, dVar.b(), aVar2, handler, z2, z ? dVar.c() : -1, i2);
        } else {
            b2 = f3276a.b(context, (kl0.b) aVar, resources, i2);
            if (aVar2 != null) {
                if (b2 != null) {
                    aVar2.b(b2, handler);
                } else {
                    aVar2.a(-3, handler);
                }
            }
        }
        if (b2 != null) {
            b.f(e(resources, i, i2), b2);
        }
        return b2;
    }

    @DexIgnore
    public static Typeface d(Context context, Resources resources, int i, String str, int i2) {
        Typeface e = f3276a.e(context, resources, i, str, i2);
        if (e != null) {
            b.f(e(resources, i, i2), e);
        }
        return e;
    }

    @DexIgnore
    public static String e(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
    }

    @DexIgnore
    public static Typeface f(Resources resources, int i, int i2) {
        return b.d(e(resources, i, i2));
    }

    @DexIgnore
    public static Typeface g(Context context, Typeface typeface, int i) {
        kl0.b i2 = f3276a.i(typeface);
        if (i2 == null) {
            return null;
        }
        return f3276a.b(context, i2, context.getResources(), i);
    }
}
