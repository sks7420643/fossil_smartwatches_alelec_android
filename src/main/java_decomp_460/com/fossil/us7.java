package com.fossil;

import com.fossil.dl7;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us7<T> extends vs7<T> implements Iterator<T>, qn7<tl7>, jr7 {
    @DexIgnore
    public int b;
    @DexIgnore
    public T c;
    @DexIgnore
    public Iterator<? extends T> d;
    @DexIgnore
    public qn7<? super tl7> e;

    @DexIgnore
    @Override // com.fossil.vs7
    public Object a(T t, qn7<? super tl7> qn7) {
        this.c = t;
        this.b = 3;
        this.e = qn7;
        Object d2 = yn7.d();
        if (d2 == yn7.d()) {
            go7.c(qn7);
        }
        return d2 == yn7.d() ? d2 : tl7.f3441a;
    }

    @DexIgnore
    public final Throwable c() {
        int i = this.b;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        return new IllegalStateException("Unexpected state of the iterator: " + this.b);
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public tn7 getContext() {
        return un7.INSTANCE;
    }

    @DexIgnore
    public final T h() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public boolean hasNext() {
        while (true) {
            int i = this.b;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.d;
                    if (it == null) {
                        pq7.i();
                        throw null;
                    } else if (it.hasNext()) {
                        this.b = 2;
                        return true;
                    } else {
                        this.d = null;
                    }
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw c();
                }
            }
            this.b = 5;
            qn7<? super tl7> qn7 = this.e;
            if (qn7 != null) {
                this.e = null;
                tl7 tl7 = tl7.f3441a;
                dl7.a aVar = dl7.Companion;
                qn7.resumeWith(dl7.m1constructorimpl(tl7));
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void i(qn7<? super tl7> qn7) {
        this.e = qn7;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        int i = this.b;
        if (i == 0 || i == 1) {
            return h();
        }
        if (i == 2) {
            this.b = 1;
            Iterator<? extends T> it = this.d;
            if (it != null) {
                return (T) it.next();
            }
            pq7.i();
            throw null;
        } else if (i == 3) {
            this.b = 0;
            T t = this.c;
            this.c = null;
            return t;
        } else {
            throw c();
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void resumeWith(Object obj) {
        el7.b(obj);
        this.b = 4;
    }
}
