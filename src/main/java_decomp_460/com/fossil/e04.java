package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e04 implements yz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f867a;

    @DexIgnore
    public e04(float f) {
        this.f867a = f;
    }

    @DexIgnore
    @Override // com.fossil.yz3
    public float a(RectF rectF) {
        return this.f867a * rectF.height();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e04)) {
            return false;
        }
        return this.f867a == ((e04) obj).f867a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.f867a)});
    }
}
