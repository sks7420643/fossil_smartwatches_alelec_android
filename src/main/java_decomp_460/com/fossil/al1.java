package com.fossil;

import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum al1 {
    UNKNOWN(false, q3.f.m()),
    DIANA(false, q3.f.g()),
    SE1(true, q3.f.k()),
    SLIM(true, q3.f.l()),
    MINI(true, q3.f.j()),
    HELLAS(false, q3.f.h()),
    SE0(true, q3.f.k()),
    WEAR_OS(false, new Type[0]),
    IVY(false, q3.f.g());
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ Type[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final al1 a(String str, String str2) {
            if (vt7.s(str, "DN", false, 2, null)) {
                return al1.DIANA;
            }
            if (!vt7.s(str, "HW", false, 2, null)) {
                return vt7.s(str, "HL", false, 2, null) ? al1.SLIM : vt7.s(str, "HM", false, 2, null) ? al1.MINI : vt7.s(str, "AW", false, 2, null) ? al1.HELLAS : vt7.s(str, "IV", false, 2, null) ? al1.IVY : b(str2);
            }
            al1 b = b(str2);
            return b == al1.UNKNOWN ? al1.SE1 : b;
        }

        @DexIgnore
        public final al1 b(String str) {
            return !zk1.u.d(str) ? al1.UNKNOWN : vt7.s(str, "D", false, 2, null) ? al1.DIANA : vt7.s(str, "W", false, 2, null) ? al1.SE1 : vt7.s(str, "L", false, 2, null) ? al1.SLIM : vt7.s(str, "M", false, 2, null) ? al1.MINI : vt7.s(str, "Y", false, 2, null) ? al1.HELLAS : vt7.s(str, "Z", false, 2, null) ? al1.SE0 : vt7.s(str, "V", false, 2, null) ? al1.IVY : al1.UNKNOWN;
        }
    }

    @DexIgnore
    public al1(boolean z, Type[] typeArr) {
        this.b = z;
        this.c = typeArr;
    }

    @DexIgnore
    public final Type[] a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }
}
