package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.d46;
import com.fossil.kw5;
import com.fossil.t47;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i46 extends qv5 implements h46, t47.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g46 h;
    @DexIgnore
    public g37<z85> i;
    @DexIgnore
    public kw5 j;
    @DexIgnore
    public d46 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return i46.m;
        }

        @DexIgnore
        public final i46 b() {
            return new i46();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i46 f1581a;

        @DexIgnore
        public b(i46 i46) {
            this.f1581a = i46;
        }

        @DexIgnore
        @Override // com.fossil.kw5.b
        public void a(j06 j06) {
            pq7.c(j06, "contactWrapper");
            d46 d46 = this.f1581a.k;
            if (d46 != null) {
                Contact contact = j06.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = j06.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = j06.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            d46.E6(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            d46 d462 = this.f1581a.k;
            if (d462 != null) {
                FragmentManager childFragmentManager = this.f1581a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                d462.show(childFragmentManager, d46.z.a());
            }
        }

        @DexIgnore
        @Override // com.fossil.kw5.b
        public void b() {
            i46.L6(this.f1581a).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements d46.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i46 f1582a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(i46 i46) {
            this.f1582a = i46;
        }

        @DexIgnore
        @Override // com.fossil.d46.b
        public void a(int i, boolean z, boolean z2) {
            i46.L6(this.f1582a).x(i, z, z2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i46 b;

        @DexIgnore
        public d(i46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i46 b;

        @DexIgnore
        public e(i46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i46 b;

        @DexIgnore
        public f(i46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            i46.L6(this.b).s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i46 b;

        @DexIgnore
        public g(i46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            i46.L6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i46 b;

        @DexIgnore
        public h(i46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            i46.L6(this.b).t();
        }
    }

    /*
    static {
        String simpleName = i46.class.getSimpleName();
        pq7.b(simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g46 L6(i46 i46) {
        g46 g46 = i46.h;
        if (g46 != null) {
            return g46;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void B3(boolean z) {
        ImageView imageView;
        if (isActive()) {
            g37<z85> g37 = this.i;
            if (g37 != null) {
                z85 a2 = g37.a();
                if (a2 != null && (imageView = a2.y) != null) {
                    pq7.b(imageView, "doneButton");
                    imageView.setEnabled(z);
                    imageView.setClickable(z);
                    if (z) {
                        imageView.setBackground(gl0.f(requireContext(), 2131099967));
                    } else {
                        imageView.setBackground(gl0.f(requireContext(), 2131099820));
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void F0(List<Object> list) {
        pq7.c(list, "contactAndAppData");
        kw5 kw5 = this.j;
        if (kw5 != null) {
            kw5.p(list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                g37<z85> g37 = this.i;
                if (g37 != null) {
                    z85 a2 = g37.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.r;
                        pq7.b(flexibleTextView, "it.ftvAssignSection");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.D;
                        pq7.b(recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            g37<z85> g372 = this.i;
            if (g372 != null) {
                z85 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.r;
                    pq7.b(flexibleTextView2, "it.ftvAssignSection");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a3.D;
                    pq7.b(recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        N6();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void N4(int i2, ArrayList<String> arrayList) {
        pq7.c(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    public void N6() {
        g37<z85> g37 = this.i;
        if (g37 == null) {
            pq7.n("mBinding");
            throw null;
        } else if (g37.a() != null) {
            g46 g46 = this.h;
            if (g46 == null) {
                pq7.n("mPresenter");
                throw null;
            } else if (!g46.q()) {
                close();
            } else {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    public final void O6() {
        kw5 kw5 = this.j;
        if (kw5 == null) {
            pq7.n("mAdapter");
            throw null;
        } else if (kw5.m()) {
            g46 g46 = this.h;
            if (g46 != null) {
                g46.v();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.a(childFragmentManager);
        }
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(g46 g46) {
        pq7.c(g46, "presenter");
        this.h = g46;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i2 == 2131363291) {
            close();
        } else if (i2 == 2131363373) {
            O6();
        }
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void X1(int i2, ArrayList<j06> arrayList) {
        pq7.c(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void m6(int i2, ArrayList<j06> arrayList) {
        pq7.c(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 4567) {
            if (i2 != 5678) {
                if (i2 == 6789 && i3 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList<j06> arrayList = (ArrayList) serializableExtra;
                        g46 g46 = this.h;
                        if (g46 != null) {
                            g46.o(arrayList);
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i3 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList<j06> arrayList2 = (ArrayList) serializableExtra2;
                    g46 g462 = this.h;
                    if (g462 != null) {
                        g462.n(arrayList2);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i3 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            g46 g463 = this.h;
            if (g463 != null) {
                pq7.b(stringArrayListExtra, "stringAppsSelected");
                g463.u(stringArrayListExtra);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        z85 z85 = (z85) aq0.f(layoutInflater, 2131558591, viewGroup, false, A6());
        d46 d46 = (d46) getChildFragmentManager().Z(d46.z.a());
        this.k = d46;
        if (d46 == null) {
            this.k = d46.z.b();
        }
        d46 d462 = this.k;
        if (d462 != null) {
            d462.D6(new c(this));
        }
        FlexibleTextView flexibleTextView = z85.r;
        pq7.b(flexibleTextView, "binding.ftvAssignSection");
        flexibleTextView.setVisibility(8);
        RecyclerView recyclerView = z85.D;
        pq7.b(recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = z85.y;
        pq7.b(imageView, "binding.ivDone");
        imageView.setBackground(gl0.f(requireContext(), 2131230888));
        z85.x.setOnClickListener(new d(this));
        z85.y.setOnClickListener(new e(this));
        z85.B.setOnClickListener(new f(this));
        z85.A.setOnClickListener(new g(this));
        z85.z.setOnClickListener(new h(this));
        kw5 kw5 = new kw5();
        kw5.q(new b(this));
        this.j = kw5;
        RecyclerView recyclerView2 = z85.D;
        recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
        recyclerView2.setHasFixedSize(true);
        kw5 kw52 = this.j;
        if (kw52 != null) {
            recyclerView2.setAdapter(kw52);
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                int parseColor = Color.parseColor(d2);
                z85.E.setBackgroundColor(parseColor);
                z85.F.setBackgroundColor(parseColor);
                z85.G.setBackgroundColor(parseColor);
            }
            this.i = new g37<>(this, z85);
            pq7.b(z85, "binding");
            return z85.n();
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        g46 g46 = this.h;
        if (g46 != null) {
            g46.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        g46 g46 = this.h;
        if (g46 != null) {
            g46.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void u() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.h46
    public void w5(int i2) {
        FlexibleTextView flexibleTextView;
        g37<z85> g37 = this.i;
        if (g37 != null) {
            z85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.w) != null) {
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(getContext(), 2131886171);
                pq7.b(c2, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }
}
