package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.m62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SparseIntArray f1608a; // = new SparseIntArray();
    @DexIgnore
    public d62 b;

    @DexIgnore
    public ic2(d62 d62) {
        rc2.k(d62);
        this.b = d62;
    }

    @DexIgnore
    public void a() {
        this.f1608a.clear();
    }

    @DexIgnore
    public int b(Context context, m62.f fVar) {
        int i;
        rc2.k(context);
        rc2.k(fVar);
        if (!fVar.r()) {
            return 0;
        }
        int s = fVar.s();
        int i2 = this.f1608a.get(s, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.f1608a.size()) {
                i = i2;
                break;
            }
            int keyAt = this.f1608a.keyAt(i3);
            if (keyAt > s && this.f1608a.get(keyAt) == 0) {
                i = 0;
                break;
            }
            i3++;
        }
        if (i == -1) {
            i = this.b.j(context, s);
        }
        this.f1608a.put(s, i);
        return i;
    }
}
