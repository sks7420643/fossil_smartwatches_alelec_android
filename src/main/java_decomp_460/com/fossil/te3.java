package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te3 extends ce3 {
    @DexIgnore
    public te3() {
        super(1);
    }

    @DexIgnore
    @Override // com.fossil.ce3
    public final String toString() {
        return "[SquareCap]";
    }
}
