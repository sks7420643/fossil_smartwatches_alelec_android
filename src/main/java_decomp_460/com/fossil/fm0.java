package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm0 extends Drawable.ConstantState {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1154a;
    @DexIgnore
    public Drawable.ConstantState b;
    @DexIgnore
    public ColorStateList c; // = null;
    @DexIgnore
    public PorterDuff.Mode d; // = dm0.h;

    @DexIgnore
    public fm0(fm0 fm0) {
        if (fm0 != null) {
            this.f1154a = fm0.f1154a;
            this.b = fm0.b;
            this.c = fm0.c;
            this.d = fm0.d;
        }
    }

    @DexIgnore
    public boolean a() {
        return this.b != null;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int i = this.f1154a;
        Drawable.ConstantState constantState = this.b;
        return (constantState != null ? constantState.getChangingConfigurations() : 0) | i;
    }

    @DexIgnore
    public Drawable newDrawable() {
        return newDrawable(null);
    }

    @DexIgnore
    public Drawable newDrawable(Resources resources) {
        return Build.VERSION.SDK_INT >= 21 ? new em0(this, resources) : new dm0(this, resources);
    }
}
