package com.fossil;

import com.fossil.tq4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w27 extends tq4<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return w27.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3870a;

        @DexIgnore
        public b(String str) {
            pq7.c(str, "errorMessage");
            this.f3870a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f3870a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeatherSettings.TEMP_UNIT f3871a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public c(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            pq7.c(latLng, "latLng");
            pq7.c(temp_unit, "tempUnit");
            uj4.b(temp_unit);
            pq7.b(temp_unit, "checkNotNull(tempUnit)");
            this.f3871a = temp_unit;
            uj4.b(latLng);
            pq7.b(latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.f3871a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements tq4.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Weather f3872a;

        @DexIgnore
        public d(Weather weather) {
            pq7.c(weather, "weather");
            this.f3872a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.f3872a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ c $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ w27 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
        public static final class a extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(1, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(qn7<?> qn7) {
                pq7.c(qn7, "completion");
                return new a(this.this$0, qn7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public final Object invoke(qn7<? super q88<gj4>> qn7) {
                return ((a) create(qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    ApiServiceV2 apiServiceV2 = this.this$0.this$0.d;
                    double d2 = this.this$0.$requestValues.a().b;
                    double d3 = this.this$0.$requestValues.a().c;
                    String value = this.this$0.$requestValues.b().getValue();
                    this.label = 1;
                    Object weather = apiServiceV2.getWeather(String.valueOf(d2), String.valueOf(d3), value, this);
                    return weather == d ? d : weather;
                } else if (i == 1) {
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(w27 w27, c cVar, qn7 qn7) {
            super(2, qn7);
            this.this$0 = w27;
            this.$requestValues = cVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$requestValues, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d;
            Object d2 = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                d = jq5.d(aVar, this);
                if (d == d2) {
                    return d2;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                d = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            iq5 iq5 = (iq5) d;
            if (iq5 instanceof kq5) {
                oq5 oq5 = new oq5();
                oq5.b((gj4) ((kq5) iq5).a());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = w27.f.a();
                local.d(a2, "onSuccess" + oq5.a());
                tq4.d b = this.this$0.b();
                Weather a3 = oq5.a();
                pq7.b(a3, "mfWeatherResponse.weather");
                b.onSuccess(new d(a3));
            } else if (iq5 instanceof hq5) {
                this.this$0.b().a(new b(yh5.NETWORK_ERROR.name()));
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = w27.class.getSimpleName();
        pq7.b(simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public w27(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    /* renamed from: i */
    public void a(c cVar) {
        pq7.c(cVar, "requestValues");
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new e(this, cVar, null), 3, null);
    }
}
