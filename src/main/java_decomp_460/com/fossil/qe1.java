package com.fossil;

import android.util.Log;
import com.fossil.af1;
import com.fossil.wb1;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qe1 implements af1<File, ByteBuffer> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements wb1<ByteBuffer> {
        @DexIgnore
        public /* final */ File b;

        @DexIgnore
        public a(File file) {
            this.b = file;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super ByteBuffer> aVar) {
            try {
                aVar.e(zj1.a(this.b));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements bf1<File, ByteBuffer> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<File, ByteBuffer> b(ef1 ef1) {
            return new qe1();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<ByteBuffer> b(File file, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(file), new a(file));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(File file) {
        return true;
    }
}
