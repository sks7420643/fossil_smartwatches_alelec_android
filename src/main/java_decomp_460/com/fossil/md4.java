package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class md4 implements nd4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2359a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public String c;

    @DexIgnore
    public md4(Context context) {
        this.f2359a = context;
    }

    @DexIgnore
    @Override // com.fossil.nd4
    public String a() {
        if (!this.b) {
            this.c = r84.F(this.f2359a);
            this.b = true;
        }
        String str = this.c;
        if (str != null) {
            return str;
        }
        return null;
    }
}
