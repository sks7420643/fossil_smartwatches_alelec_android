package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb0 implements Parcelable.Creator<yb0> {
    @DexIgnore
    public /* synthetic */ xb0(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public yb0 createFromParcel(Parcel parcel) {
        jw1 jw1 = jw1.values()[parcel.readInt()];
        Parcelable readParcelable = parcel.readParcelable(ry1.class.getClassLoader());
        if (readParcelable != null) {
            ry1 ry1 = (ry1) readParcelable;
            boolean z = parcel.readByte() != ((byte) 0);
            byte[] createByteArray = parcel.createByteArray();
            return new yb0(jw1, ry1, z, createByteArray != null ? createByteArray : new byte[0]);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public yb0[] newArray(int i) {
        return new yb0[i];
    }
}
