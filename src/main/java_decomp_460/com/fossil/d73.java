package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d73 implements xw2<g73> {
    @DexIgnore
    public static d73 c; // = new d73();
    @DexIgnore
    public /* final */ xw2<g73> b;

    @DexIgnore
    public d73() {
        this(ww2.b(new f73()));
    }

    @DexIgnore
    public d73(xw2<g73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((g73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ g73 zza() {
        return this.b.zza();
    }
}
