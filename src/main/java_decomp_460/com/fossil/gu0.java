package com.fossil;

import com.fossil.bu0;
import com.fossil.xt0;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gu0<T> extends xt0<Integer, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Value> extends vt0<Integer, Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ gu0<Value> f1360a;

        @DexIgnore
        public a(gu0<Value> gu0) {
            this.f1360a = gu0;
        }

        @DexIgnore
        /* renamed from: a */
        public void dispatchLoadInitial(Integer num, int i, int i2, boolean z, Executor executor, bu0.a<Value> aVar) {
            int max;
            Integer valueOf;
            if (num == null) {
                max = i;
                valueOf = 0;
            } else {
                max = Math.max(i / i2, 2) * i2;
                valueOf = Integer.valueOf(Math.max(0, ((num.intValue() - (max / 2)) / i2) * i2));
            }
            this.f1360a.dispatchLoadInitial(false, valueOf.intValue(), max, i2, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public void addInvalidatedCallback(xt0.c cVar) {
            this.f1360a.addInvalidatedCallback(cVar);
        }

        @DexIgnore
        /* renamed from: b */
        public Integer getKey(int i, Value value) {
            return Integer.valueOf(i);
        }

        @DexIgnore
        @Override // com.fossil.vt0
        public void dispatchLoadAfter(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
            this.f1360a.dispatchLoadRange(1, i + 1, i2, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.vt0
        public void dispatchLoadBefore(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
            int i3 = i - 1;
            if (i3 < 0) {
                this.f1360a.dispatchLoadRange(2, i3, 0, executor, aVar);
                return;
            }
            int min = Math.min(i2, i3 + 1);
            this.f1360a.dispatchLoadRange(2, (i3 - min) + 1, min, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public void invalidate() {
            this.f1360a.invalidate();
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public boolean isInvalid() {
            return this.f1360a.isInvalid();
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public <ToValue> xt0<Integer, ToValue> map(gi0<Value, ToValue> gi0) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public <ToValue> xt0<Integer, ToValue> mapByPage(gi0<List<Value>, List<ToValue>> gi0) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.fossil.xt0
        public void removeInvalidatedCallback(xt0.c cVar) {
            this.f1360a.removeInvalidatedCallback(cVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<T> {
        @DexIgnore
        public abstract void a(List<T> list, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> extends b<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0.d<T> f1361a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public c(gu0 gu0, boolean z, int i, bu0.a<T> aVar) {
            this.f1361a = new xt0.d<>(gu0, 0, null, aVar);
            this.b = z;
            this.c = i;
            if (i < 1) {
                throw new IllegalArgumentException("Page size must be non-negative");
            }
        }

        @DexIgnore
        @Override // com.fossil.gu0.b
        public void a(List<T> list, int i, int i2) {
            if (!this.f1361a.a()) {
                xt0.d.d(list, i, i2);
                if (list.size() + i != i2 && list.size() % this.c != 0) {
                    throw new IllegalArgumentException("PositionalDataSource requires initial load size to be a multiple of page size to support internal tiling. loadSize " + list.size() + ", position " + i + ", totalCount " + i2 + ", pageSize " + this.c);
                } else if (this.b) {
                    this.f1361a.b(new bu0<>(list, i, (i2 - i) - list.size(), 0));
                } else {
                    this.f1361a.b(new bu0<>(list, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1362a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(int i, int i2, int i3, boolean z) {
            this.f1362a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {
        @DexIgnore
        public abstract void a(List<T> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<T> extends e<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public xt0.d<T> f1363a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(gu0 gu0, int i, int i2, Executor executor, bu0.a<T> aVar) {
            this.f1363a = new xt0.d<>(gu0, i, executor, aVar);
            this.b = i2;
        }

        @DexIgnore
        @Override // com.fossil.gu0.e
        public void a(List<T> list) {
            if (!this.f1363a.a()) {
                this.f1363a.b(new bu0<>(list, 0, 0, this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1364a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public g(int i, int i2) {
            this.f1364a = i;
            this.b = i2;
        }
    }

    @DexIgnore
    public static int computeInitialLoadPosition(d dVar, int i) {
        int i2 = dVar.f1362a;
        int i3 = dVar.b;
        int i4 = dVar.c;
        return Math.max(0, Math.min(((((i - i3) + i4) - 1) / i4) * i4, (i2 / i4) * i4));
    }

    @DexIgnore
    public static int computeInitialLoadSize(d dVar, int i, int i2) {
        return Math.min(i2 - i, dVar.b);
    }

    @DexIgnore
    public final void dispatchLoadInitial(boolean z, int i, int i2, int i3, Executor executor, bu0.a<T> aVar) {
        c cVar = new c(this, z, i3, aVar);
        loadInitial(new d(i, i2, i3, z), cVar);
        cVar.f1361a.c(executor);
    }

    @DexIgnore
    public final void dispatchLoadRange(int i, int i2, int i3, Executor executor, bu0.a<T> aVar) {
        f fVar = new f(this, i, i2, executor, aVar);
        if (i3 == 0) {
            fVar.a(Collections.emptyList());
        } else {
            loadRange(new g(i2, i3), fVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isContiguous() {
        return false;
    }

    @DexIgnore
    public abstract void loadInitial(d dVar, b<T> bVar);

    @DexIgnore
    public abstract void loadRange(g gVar, e<T> eVar);

    @DexIgnore
    @Override // com.fossil.xt0
    public final <V> gu0<V> map(gi0<T, V> gi0) {
        return mapByPage((gi0) xt0.createListFunction(gi0));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public final <V> gu0<V> mapByPage(gi0<List<T>, List<V>> gi0) {
        return new lu0(this, gi0);
    }

    @DexIgnore
    public vt0<Integer, T> wrapAsContiguousWithoutPlaceholders() {
        return new a(this);
    }
}
