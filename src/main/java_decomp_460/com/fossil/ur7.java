package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ur7 implements Iterable<Integer>, jr7 {
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ur7 a(int i, int i2, int i3) {
            return new ur7(i, i2, i3);
        }
    }

    @DexIgnore
    public ur7(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.b = i;
            this.c = no7.c(i, i2, i3);
            this.d = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: e */
    public um7 iterator() {
        return new vr7(this.b, this.c, this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ur7) {
            if (!isEmpty() || !((ur7) obj).isEmpty()) {
                ur7 ur7 = (ur7) obj;
                if (!(this.b == ur7.b && this.c == ur7.c && this.d == ur7.d)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public boolean isEmpty() {
        if (this.d <= 0) {
            return this.b < this.c;
        }
        if (this.b > this.c) {
            return true;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb;
        int i;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i = -this.d;
        }
        sb.append(i);
        return sb.toString();
    }
}
