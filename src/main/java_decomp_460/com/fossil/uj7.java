package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj7 {
    @DexIgnore
    public static void a(Activity activity) {
        lk7.c(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof dk7) {
            e(activity, (dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void b(Service service) {
        lk7.c(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof dk7) {
            e(service, (dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void c(BroadcastReceiver broadcastReceiver, Context context) {
        lk7.c(broadcastReceiver, "broadcastReceiver");
        lk7.c(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof dk7) {
            e(broadcastReceiver, (dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void d(ContentProvider contentProvider) {
        lk7.c(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof dk7) {
            e(contentProvider, (dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void e(Object obj, dk7 dk7) {
        vj7<Object> c = dk7.c();
        lk7.d(c, "%s.androidInjector() returned null", dk7.getClass());
        c.a(obj);
    }
}
