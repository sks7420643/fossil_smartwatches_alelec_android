package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ky2 extends cz2<T> {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public ky2(Object obj) {
        this.c = obj;
    }

    @DexIgnore
    public final boolean hasNext() {
        return !this.b;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final T next() {
        if (!this.b) {
            this.b = true;
            return (T) this.c;
        }
        throw new NoSuchElementException();
    }
}
