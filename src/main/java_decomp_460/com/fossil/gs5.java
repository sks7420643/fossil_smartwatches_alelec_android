package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs5 implements Factory<fs5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f1352a;
    @DexIgnore
    public /* final */ Provider<LocationSource> b;

    @DexIgnore
    public gs5(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        this.f1352a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static gs5 a(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        return new gs5(provider, provider2);
    }

    @DexIgnore
    public static fs5 c(PortfolioApp portfolioApp, LocationSource locationSource) {
        return new fs5(portfolioApp, locationSource);
    }

    @DexIgnore
    /* renamed from: b */
    public fs5 get() {
        return c(this.f1352a.get(), this.b.get());
    }
}
