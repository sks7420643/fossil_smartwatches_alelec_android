package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l81 implements m81 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ PorterDuffXfermode f2157a; // = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);

    @DexIgnore
    @Override // com.fossil.m81
    public Object a(g51 g51, Bitmap bitmap, f81 f81, qn7<? super Bitmap> qn7) {
        Paint paint = new Paint(3);
        int min = Math.min(bitmap.getWidth(), bitmap.getHeight());
        float f = ((float) min) / 2.0f;
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "input.config");
        Bitmap c = g51.c(min, min, config);
        Canvas canvas = new Canvas(c);
        canvas.drawCircle(f, f, f, paint);
        paint.setXfermode(f2157a);
        canvas.drawBitmap(bitmap, f - (((float) bitmap.getWidth()) / 2.0f), f - (((float) bitmap.getHeight()) / 2.0f), paint);
        g51.b(bitmap);
        return c;
    }

    @DexIgnore
    @Override // com.fossil.m81
    public String key() {
        String name = l81.class.getName();
        pq7.b(name, "CircleCropTransformation::class.java.name");
        return name;
    }
}
