package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr7 extends ur7 {
    @DexIgnore
    public static /* final */ wr7 f; // = new wr7(1, 0);
    @DexIgnore
    public static /* final */ a g; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final wr7 a() {
            return wr7.f;
        }
    }

    @DexIgnore
    public wr7(int i, int i2) {
        super(i, i2, 1);
    }

    @DexIgnore
    @Override // com.fossil.ur7
    public boolean equals(Object obj) {
        if (obj instanceof wr7) {
            if (!isEmpty() || !((wr7) obj).isEmpty()) {
                wr7 wr7 = (wr7) obj;
                if (!(a() == wr7.a() && b() == wr7.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public Integer g() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public Integer h() {
        return Integer.valueOf(a());
    }

    @DexIgnore
    @Override // com.fossil.ur7
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @DexIgnore
    @Override // com.fossil.ur7
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    @Override // com.fossil.ur7
    public String toString() {
        return a() + ".." + b();
    }
}
