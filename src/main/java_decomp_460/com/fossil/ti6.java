package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ti6 implements MembersInjector<HeartRateOverviewFragment> {
    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, qi6 qi6) {
        heartRateOverviewFragment.h = qi6;
    }

    @DexIgnore
    public static void b(HeartRateOverviewFragment heartRateOverviewFragment, bj6 bj6) {
        heartRateOverviewFragment.j = bj6;
    }

    @DexIgnore
    public static void c(HeartRateOverviewFragment heartRateOverviewFragment, hj6 hj6) {
        heartRateOverviewFragment.i = hj6;
    }
}
