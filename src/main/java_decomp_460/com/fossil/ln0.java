package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ln0<F, S> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ F f2221a;
    @DexIgnore
    public /* final */ S b;

    @DexIgnore
    public ln0(F f, S s) {
        this.f2221a = f;
        this.b = s;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof ln0)) {
            return false;
        }
        ln0 ln0 = (ln0) obj;
        return kn0.a(ln0.f2221a, this.f2221a) && kn0.a(ln0.b, this.b);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        F f = this.f2221a;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Pair{" + String.valueOf(this.f2221a) + " " + String.valueOf(this.b) + "}";
    }
}
