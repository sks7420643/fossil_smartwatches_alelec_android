package com.fossil;

import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public byte[] f1550a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<n6, kx> d; // = new Hashtable<>();

    @DexIgnore
    public final kx a(n6 n6Var) {
        kx kxVar = this.d.get(n6Var);
        if (kxVar == null) {
            kxVar = new kx(n6Var, this.b, this.c, 0, 0);
        }
        this.d.put(n6Var, kxVar);
        return kxVar;
    }

    @DexIgnore
    public final void b(byte[] bArr, byte[] bArr2) {
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final byte[] c() {
        byte[] bArr = this.f1550a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final void d(n6 n6Var) {
        kx a2 = a(n6Var);
        byte[] bArr = this.b;
        byte[] bArr2 = this.c;
        if (!Arrays.equals(a2.b, bArr) || !Arrays.equals(a2.c, bArr2)) {
            a2.d = 0;
        }
        a2.b = bArr;
        a2.c = bArr2;
        a2.d++;
        a2.e = 0;
    }
}
