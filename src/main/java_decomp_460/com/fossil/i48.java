package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i48 implements k48, j48, Cloneable, ByteChannel {
    @DexIgnore
    public x48 b;
    @DexIgnore
    public long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ /* synthetic */ i48 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(i48 i48) {
            this.b = i48;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            return (int) Math.min(this.b.p0(), (long) Integer.MAX_VALUE);
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            if (this.b.p0() > 0) {
                return this.b.readByte() & 255;
            }
            return -1;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "sink");
            return this.b.read(bArr, i, i2);
        }

        @DexIgnore
        public String toString() {
            return this.b + ".inputStream()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends OutputStream {
        @DexIgnore
        public /* final */ /* synthetic */ i48 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(i48 i48) {
            this.b = i48;
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
        }

        @DexIgnore
        public String toString() {
            return this.b + ".outputStream()";
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(int i) {
            this.b.w0(i);
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "data");
            this.b.v0(bArr, i, i2);
        }
    }

    @DexIgnore
    public i48 A0(int i) {
        x48 s0 = s0(2);
        byte[] bArr = s0.f4040a;
        int i2 = s0.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((byte) ((i >>> 8) & 255));
        bArr[i3] = (byte) ((byte) (i & 255));
        s0.c = i3 + 1;
        o0(p0() + 2);
        return this;
    }

    @DexIgnore
    public final i48 B() {
        i48 i48 = new i48();
        if (p0() != 0) {
            x48 x48 = this.b;
            if (x48 != null) {
                x48 d = x48.d();
                i48.b = d;
                d.g = d;
                d.f = d;
                for (x48 x482 = x48.f; x482 != x48; x482 = x482.f) {
                    x48 x483 = d.g;
                    if (x483 == null) {
                        pq7.i();
                        throw null;
                    } else if (x482 != null) {
                        x483.c(x482.d());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                i48.o0(p0());
            } else {
                pq7.i();
                throw null;
            }
        }
        return i48;
    }

    @DexIgnore
    public i48 B0(String str, int i, int i2, Charset charset) {
        boolean z = true;
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        pq7.c(charset, "charset");
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 > str.length()) {
                    z = false;
                }
                if (z) {
                    if (pq7.a(charset, et7.f986a)) {
                        E0(str, i, i2);
                    } else {
                        String substring = str.substring(i, i2);
                        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        if (substring != null) {
                            byte[] bytes = substring.getBytes(charset);
                            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                            v0(bytes, 0, bytes.length);
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
    }

    @DexIgnore
    public final i48 C(i48 i48, long j, long j2) {
        pq7.c(i48, "out");
        f48.b(p0(), j, j2);
        if (j2 != 0) {
            i48.o0(i48.p0() + j2);
            x48 x48 = this.b;
            long j3 = j;
            while (x48 != null) {
                int i = x48.c;
                int i2 = x48.b;
                if (j3 >= ((long) (i - i2))) {
                    j3 -= (long) (i - i2);
                    x48 = x48.f;
                } else {
                    while (j2 > 0) {
                        if (x48 != null) {
                            x48 d = x48.d();
                            int i3 = ((int) j3) + d.b;
                            d.b = i3;
                            d.c = Math.min(i3 + ((int) j2), d.c);
                            x48 x482 = i48.b;
                            if (x482 == null) {
                                d.g = d;
                                d.f = d;
                                i48.b = d;
                            } else if (x482 != null) {
                                x48 x483 = x482.g;
                                if (x483 != null) {
                                    x483.c(d);
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                            j2 -= (long) (d.c - d.b);
                            x48 = x48.f;
                            j3 = 0;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
            pq7.i();
            throw null;
        }
        return this;
    }

    @DexIgnore
    public i48 C0(String str, Charset charset) {
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        pq7.c(charset, "charset");
        B0(str, 0, str.length(), charset);
        return this;
    }

    @DexIgnore
    public i48 D0(String str) {
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        E0(str, 0, str.length());
        return this;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 E(String str) {
        D0(str);
        return this;
    }

    @DexIgnore
    public i48 E0(String str, int i, int i2) {
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 <= str.length()) {
                    while (i < i2) {
                        char charAt = str.charAt(i);
                        if (charAt < '\u0080') {
                            x48 s0 = s0(1);
                            byte[] bArr = s0.f4040a;
                            int i3 = s0.c - i;
                            int min = Math.min(i2, 8192 - i3);
                            int i4 = i + 1;
                            bArr[i + i3] = (byte) ((byte) charAt);
                            while (i4 < min) {
                                char charAt2 = str.charAt(i4);
                                if (charAt2 >= '\u0080') {
                                    break;
                                }
                                bArr[i4 + i3] = (byte) ((byte) charAt2);
                                i4++;
                            }
                            int i5 = s0.c;
                            int i6 = (i3 + i4) - i5;
                            s0.c = i5 + i6;
                            o0(((long) i6) + p0());
                            i = i4;
                        } else {
                            if (charAt < '\u0800') {
                                x48 s02 = s0(2);
                                byte[] bArr2 = s02.f4040a;
                                int i7 = s02.c;
                                bArr2[i7] = (byte) ((byte) ((charAt >> 6) | 192));
                                bArr2[i7 + 1] = (byte) ((byte) ((charAt & '?') | 128));
                                s02.c = i7 + 2;
                                o0(p0() + 2);
                            } else if (charAt < '\ud800' || charAt > '\udfff') {
                                x48 s03 = s0(3);
                                byte[] bArr3 = s03.f4040a;
                                int i8 = s03.c;
                                bArr3[i8] = (byte) ((byte) ((charAt >> '\f') | 224));
                                bArr3[i8 + 1] = (byte) ((byte) (((charAt >> 6) & 63) | 128));
                                bArr3[i8 + 2] = (byte) ((byte) ((charAt & '?') | 128));
                                s03.c = i8 + 3;
                                o0(p0() + 3);
                            } else {
                                int i9 = i + 1;
                                char charAt3 = i9 < i2 ? str.charAt(i9) : 0;
                                if (charAt > '\udbff' || '\udc00' > charAt3 || '\udfff' < charAt3) {
                                    w0(63);
                                    i = i9;
                                } else {
                                    int i10 = ((charAt3 & '\u03ff') | ((charAt & '\u03ff') << 10)) + 65536;
                                    x48 s04 = s0(4);
                                    byte[] bArr4 = s04.f4040a;
                                    int i11 = s04.c;
                                    bArr4[i11] = (byte) ((byte) ((i10 >> 18) | 240));
                                    bArr4[i11 + 1] = (byte) ((byte) (((i10 >> 12) & 63) | 128));
                                    bArr4[i11 + 2] = (byte) ((byte) (((i10 >> 6) & 63) | 128));
                                    bArr4[i11 + 3] = (byte) ((byte) ((i10 & 63) | 128));
                                    s04.c = i11 + 4;
                                    o0(p0() + 4);
                                    i += 2;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
    }

    @DexIgnore
    public i48 F0(int i) {
        if (i < 128) {
            w0(i);
        } else if (i < 2048) {
            x48 s0 = s0(2);
            byte[] bArr = s0.f4040a;
            int i2 = s0.c;
            bArr[i2] = (byte) ((byte) ((i >> 6) | 192));
            bArr[i2 + 1] = (byte) ((byte) ((i & 63) | 128));
            s0.c = i2 + 2;
            o0(p0() + 2);
        } else if (55296 <= i && 57343 >= i) {
            w0(63);
        } else if (i < 65536) {
            x48 s02 = s0(3);
            byte[] bArr2 = s02.f4040a;
            int i3 = s02.c;
            bArr2[i3] = (byte) ((byte) ((i >> 12) | 224));
            bArr2[i3 + 1] = (byte) ((byte) (((i >> 6) & 63) | 128));
            bArr2[i3 + 2] = (byte) ((byte) ((i & 63) | 128));
            s02.c = i3 + 3;
            o0(p0() + 3);
        } else if (i <= 1114111) {
            x48 s03 = s0(4);
            byte[] bArr3 = s03.f4040a;
            int i4 = s03.c;
            bArr3[i4] = (byte) ((byte) ((i >> 18) | 240));
            bArr3[i4 + 1] = (byte) ((byte) (((i >> 12) & 63) | 128));
            bArr3[i4 + 2] = (byte) ((byte) (((i >> 6) & 63) | 128));
            bArr3[i4 + 3] = (byte) ((byte) ((i & 63) | 128));
            s03.c = i4 + 4;
            o0(p0() + 4);
        } else {
            throw new IllegalArgumentException("Unexpected code point: 0x" + f48.f(i));
        }
        return this;
    }

    @DexIgnore
    public i48 G() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean H(long j, l48 l48) {
        pq7.c(l48, "bytes");
        return Q(j, l48, 0, l48.size());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String I(Charset charset) {
        pq7.c(charset, "charset");
        return X(this.c, charset);
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 J(byte[] bArr, int i, int i2) {
        v0(bArr, i, i2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.a58
    public void K(i48 i48, long j) {
        x48 x48;
        pq7.c(i48, "source");
        if (i48 != this) {
            f48.b(i48.p0(), 0, j);
            while (j > 0) {
                x48 x482 = i48.b;
                if (x482 != null) {
                    int i = x482.c;
                    if (x482 != null) {
                        if (j < ((long) (i - x482.b))) {
                            x48 x483 = this.b;
                            if (x483 == null) {
                                x48 = null;
                            } else if (x483 != null) {
                                x48 = x483.g;
                            } else {
                                pq7.i();
                                throw null;
                            }
                            if (x48 != null && x48.e) {
                                if ((((long) x48.c) + j) - ((long) (x48.d ? 0 : x48.b)) <= ((long) 8192)) {
                                    x48 x484 = i48.b;
                                    if (x484 != null) {
                                        x484.f(x48, (int) j);
                                        i48.o0(i48.p0() - j);
                                        o0(p0() + j);
                                        return;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                            }
                            x48 x485 = i48.b;
                            if (x485 != null) {
                                i48.b = x485.e((int) j);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        x48 x486 = i48.b;
                        if (x486 != null) {
                            long j2 = (long) (x486.c - x486.b);
                            i48.b = x486.b();
                            x48 x487 = this.b;
                            if (x487 == null) {
                                this.b = x486;
                                x486.g = x486;
                                x486.f = x486;
                            } else if (x487 != null) {
                                x48 x488 = x487.g;
                                if (x488 != null) {
                                    x488.c(x486);
                                    x486.a();
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                            i48.o0(i48.p0() - j2);
                            o0(p0() + j2);
                            j -= j2;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    @DexIgnore
    public final byte M(long j) {
        f48.b(p0(), j, 1);
        x48 x48 = this.b;
        if (x48 == null) {
            pq7.i();
            throw null;
        } else if (p0() - j < j) {
            long p0 = p0();
            while (p0 > j) {
                x48 = x48.g;
                if (x48 != null) {
                    p0 -= (long) (x48.c - x48.b);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            if (x48 != null) {
                return x48.f4040a[(int) ((((long) x48.b) + j) - p0)];
            }
            pq7.i();
            throw null;
        } else {
            long j2 = 0;
            x48 x482 = x48;
            while (true) {
                int i = x482.c;
                int i2 = x482.b;
                long j3 = ((long) (i - i2)) + j2;
                if (j3 <= j) {
                    x482 = x482.f;
                    if (x482 != null) {
                        j2 = j3;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (x482 != null) {
                    return x482.f4040a[(int) ((((long) i2) + j) - j2)];
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public long N(c58 c58) throws IOException {
        pq7.c(c58, "source");
        long j = 0;
        while (true) {
            long d0 = c58.d0(this, (long) 8192);
            if (d0 == -1) {
                return j;
            }
            j += d0;
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 O(long j) {
        y0(j);
        return this;
    }

    @DexIgnore
    public long P(byte b2, long j, long j2) {
        x48 x48;
        int i;
        long j3 = 0;
        if (0 <= j && j2 >= j) {
            if (j2 > p0()) {
                j2 = p0();
            }
            if (!(j == j2 || (x48 = this.b) == null)) {
                if (p0() - j < j) {
                    j3 = p0();
                    while (j3 > j) {
                        x48 = x48.g;
                        if (x48 != null) {
                            j3 -= (long) (x48.c - x48.b);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    if (x48 != null) {
                        while (j3 < j2) {
                            byte[] bArr = x48.f4040a;
                            int min = (int) Math.min((long) x48.c, (((long) x48.b) + j2) - j3);
                            i = (int) ((((long) x48.b) + j) - j3);
                            while (i < min) {
                                if (bArr[i] != b2) {
                                    i++;
                                }
                            }
                            long j4 = ((long) (x48.c - x48.b)) + j3;
                            x48 = x48.f;
                            if (x48 != null) {
                                j3 = j4;
                                j = j4;
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    }
                } else {
                    while (true) {
                        long j5 = ((long) (x48.c - x48.b)) + j3;
                        if (j5 <= j) {
                            x48 = x48.f;
                            if (x48 != null) {
                                j3 = j5;
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else if (x48 != null) {
                            while (j3 < j2) {
                                byte[] bArr2 = x48.f4040a;
                                int min2 = (int) Math.min((long) x48.c, (((long) x48.b) + j2) - j3);
                                i = (int) ((((long) x48.b) + j) - j3);
                                while (i < min2) {
                                    if (bArr2[i] != b2) {
                                        i++;
                                    }
                                }
                                long j6 = ((long) (x48.c - x48.b)) + j3;
                                x48 = x48.f;
                                if (x48 != null) {
                                    j3 = j6;
                                    j = j6;
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                return ((long) (i - x48.b)) + j3;
            }
            return -1;
        }
        throw new IllegalArgumentException(("size=" + p0() + " fromIndex=" + j + " toIndex=" + j2).toString());
    }

    @DexIgnore
    public boolean Q(long j, l48 l48, int i, int i2) {
        pq7.c(l48, "bytes");
        if (j < 0 || i < 0 || i2 < 0 || p0() - j < ((long) i2) || l48.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (M(((long) i3) + j) != l48.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean R(long j) {
        return this.c >= j;
    }

    @DexIgnore
    public l48 S() {
        return i(p0());
    }

    @DexIgnore
    public int T() throws EOFException {
        return f48.c(readInt());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String U() throws EOFException {
        return z(Long.MAX_VALUE);
    }

    @DexIgnore
    public short V() throws EOFException {
        return f48.d(readShort());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte[] W(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (p0() >= j) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    @DexIgnore
    public String X(long j, Charset charset) throws EOFException {
        pq7.c(charset, "charset");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (this.c < j) {
            throw new EOFException();
        } else if (i == 0) {
            return "";
        } else {
            x48 x48 = this.b;
            if (x48 != null) {
                int i2 = x48.b;
                if (((long) i2) + j > ((long) x48.c)) {
                    return new String(W(j), charset);
                }
                int i3 = (int) j;
                String str = new String(x48.f4040a, i2, i3, charset);
                int i4 = x48.b + i3;
                x48.b = i4;
                this.c -= j;
                if (i4 != x48.c) {
                    return str;
                }
                this.b = x48.b();
                y48.c.a(x48);
                return str;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 Z(byte[] bArr) {
        u0(bArr);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 a0(l48 l48) {
        t0(l48);
        return this;
    }

    @DexIgnore
    public String b0() {
        return X(this.c, et7.f986a);
    }

    @DexIgnore
    @Override // com.fossil.a58, java.lang.AutoCloseable, java.io.Closeable, com.fossil.c58, java.nio.channels.Channel
    public void close() {
    }

    @DexIgnore
    @Override // com.fossil.j48, com.fossil.k48
    public i48 d() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.c58
    public long d0(i48 i48, long j) {
        pq7.c(i48, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (p0() == 0) {
            return -1;
        } else {
            if (j > p0()) {
                j = p0();
            }
            i48.K(this, j);
            return j;
        }
    }

    @DexIgnore
    @Override // com.fossil.a58, com.fossil.c58
    public d58 e() {
        return d58.d;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public long e0(a58 a58) throws IOException {
        pq7.c(a58, "sink");
        long p0 = p0();
        if (p0 > 0) {
            a58.K(this, p0);
        }
        return p0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof i48) {
                i48 i48 = (i48) obj;
                if (p0() == i48.p0()) {
                    if (p0() != 0) {
                        x48 x48 = this.b;
                        if (x48 != null) {
                            x48 x482 = i48.b;
                            if (x482 != null) {
                                int i = x48.b;
                                int i2 = x482.b;
                                long j = 0;
                                while (j < p0()) {
                                    long min = (long) Math.min(x48.c - i, x482.c - i2);
                                    long j2 = 0;
                                    while (j2 < min) {
                                        if (x48.f4040a[i] == x482.f4040a[i2]) {
                                            j2++;
                                            i++;
                                            i2++;
                                        }
                                    }
                                    if (i == x48.c) {
                                        x48 = x48.f;
                                        if (x48 != null) {
                                            i = x48.b;
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    }
                                    if (i2 == x482.c) {
                                        x482 = x482.f;
                                        if (x482 != null) {
                                            i2 = x482.b;
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    }
                                    j += min;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.a58, com.fossil.j48, java.io.Flushable
    public void flush() {
    }

    @DexIgnore
    public String g0(long j) throws EOFException {
        return X(j, et7.f986a);
    }

    @DexIgnore
    public int hashCode() {
        x48 x48 = this.b;
        if (x48 == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = x48.c;
            for (int i3 = x48.b; i3 < i2; i3++) {
                i = (i * 31) + x48.f4040a[i3];
            }
            x48 = x48.f;
            if (x48 == null) {
                pq7.i();
                throw null;
            }
        } while (x48 != this.b);
        return i;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public l48 i(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (p0() < j) {
            throw new EOFException();
        } else if (j < ((long) 4096)) {
            return new l48(W(j));
        } else {
            l48 r0 = r0((int) j);
            skip(j);
            return r0;
        }
    }

    @DexIgnore
    public int i0() throws EOFException {
        int i;
        int i2;
        int i3;
        if (p0() != 0) {
            byte M = M(0);
            if ((M & 128) == 0) {
                i = M & Byte.MAX_VALUE;
                i3 = 0;
                i2 = 1;
            } else if ((M & 224) == 192) {
                i = M & 31;
                i2 = 2;
                i3 = 128;
            } else if ((M & 240) == 224) {
                i = M & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY;
                i2 = 3;
                i3 = 2048;
            } else if ((M & 248) == 240) {
                i = M & 7;
                i2 = 4;
                i3 = 65536;
            } else {
                skip(1);
                return 65533;
            }
            long j = (long) i2;
            if (p0() >= j) {
                for (int i4 = 1; i4 < i2; i4++) {
                    long j2 = (long) i4;
                    byte M2 = M(j2);
                    if ((M2 & 192) == 128) {
                        i = (i << 6) | (M2 & 63);
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (i > 1114111) {
                    return 65533;
                }
                if (55296 <= i && 57343 >= i) {
                    return 65533;
                }
                if (i < i3) {
                    return 65533;
                }
                return i;
            }
            throw new EOFException("size < " + i2 + ": " + p0() + " (to read code point prefixed 0x" + f48.e(M) + ')');
        }
        throw new EOFException();
    }

    @DexIgnore
    public boolean isOpen() {
        return true;
    }

    @DexIgnore
    public final void j() {
        skip(p0());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void j0(long j) throws EOFException {
        if (this.c < j) {
            throw new EOFException();
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 k0(long j) {
        x0(j);
        return this;
    }

    @DexIgnore
    /* renamed from: l */
    public i48 clone() {
        return B();
    }

    @DexIgnore
    @Override // com.fossil.j48
    public OutputStream l0() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.k48
    public long m0() throws EOFException {
        int i;
        int i2 = 0;
        if (p0() != 0) {
            boolean z = false;
            long j = 0;
            while (true) {
                x48 x48 = this.b;
                if (x48 != null) {
                    byte[] bArr = x48.f4040a;
                    int i3 = x48.b;
                    int i4 = x48.c;
                    i2 = i2;
                    while (true) {
                        if (i3 >= i4) {
                            break;
                        }
                        byte b2 = bArr[i3];
                        byte b3 = (byte) 48;
                        if (b2 < b3 || b2 > ((byte) 57)) {
                            byte b4 = (byte) 97;
                            if ((b2 >= b4 && b2 <= ((byte) 102)) || (b2 >= (b4 = (byte) 65) && b2 <= ((byte) 70))) {
                                i = (b2 - b4) + 10;
                            } else if (i2 != 0) {
                                z = true;
                            } else {
                                throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + f48.e(b2));
                            }
                        } else {
                            i = b2 - b3;
                        }
                        if ((-1152921504606846976L & j) == 0) {
                            j = (j << 4) | ((long) i);
                            i2++;
                            i3++;
                        } else {
                            i48 i48 = new i48();
                            i48.y0(j);
                            i48.w0(b2);
                            throw new NumberFormatException("Number too large: " + i48.b0());
                        }
                    }
                    if (i3 == i4) {
                        this.b = x48.b();
                        y48.c.a(x48);
                    } else {
                        x48.b = i3;
                    }
                    if (z || this.b == null) {
                        o0(p0() - ((long) i2));
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            o0(p0() - ((long) i2));
            return j;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 n(int i) {
        A0(i);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public InputStream n0() {
        return new a(this);
    }

    @DexIgnore
    public final long o() {
        long p0 = p0();
        if (p0 == 0) {
            return 0;
        }
        x48 x48 = this.b;
        if (x48 != null) {
            x48 x482 = x48.g;
            if (x482 != null) {
                int i = x482.c;
                return (i >= 8192 || !x482.e) ? p0 : p0 - ((long) (i - x482.b));
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void o0(long j) {
        this.c = j;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 p(int i) {
        z0(i);
        return this;
    }

    @DexIgnore
    public final long p0() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public k48 peek() {
        return s48.d(new u48(this));
    }

    @DexIgnore
    public final l48 q0() {
        if (p0() <= ((long) Integer.MAX_VALUE)) {
            return r0((int) p0());
        }
        throw new IllegalStateException(("size > Int.MAX_VALUE: " + p0()).toString());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte[] r() {
        return W(p0());
    }

    @DexIgnore
    public final l48 r0(int i) {
        if (i == 0) {
            return l48.EMPTY;
        }
        f48.b(p0(), 0, (long) i);
        int i2 = 0;
        int i3 = 0;
        x48 x48 = this.b;
        while (i3 < i) {
            if (x48 != null) {
                int i4 = x48.c;
                int i5 = x48.b;
                if (i4 != i5) {
                    i3 += i4 - i5;
                    x48 = x48.f;
                    i2++;
                } else {
                    throw new AssertionError("s.limit == s.pos");
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        byte[][] bArr = new byte[i2][];
        int[] iArr = new int[(i2 * 2)];
        x48 x482 = this.b;
        int i6 = 0;
        int i7 = 0;
        while (i7 < i) {
            if (x482 != null) {
                bArr[i6] = x482.f4040a;
                int i8 = (x482.c - x482.b) + i7;
                iArr[i6] = Math.min(i8, i);
                iArr[i6 + i2] = x482.b;
                x482.d = true;
                i6++;
                x482 = x482.f;
                i7 = i8;
            } else {
                pq7.i();
                throw null;
            }
        }
        return new z48(bArr, iArr);
    }

    @DexIgnore
    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) throws IOException {
        pq7.c(byteBuffer, "sink");
        x48 x48 = this.b;
        if (x48 == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), x48.c - x48.b);
        byteBuffer.put(x48.f4040a, x48.b, min);
        int i = x48.b + min;
        x48.b = i;
        this.c -= (long) min;
        if (i != x48.c) {
            return min;
        }
        this.b = x48.b();
        y48.c.a(x48);
        return min;
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) {
        pq7.c(bArr, "sink");
        f48.b((long) bArr.length, (long) i, (long) i2);
        x48 x48 = this.b;
        if (x48 == null) {
            return -1;
        }
        int min = Math.min(i2, x48.c - x48.b);
        byte[] bArr2 = x48.f4040a;
        int i3 = x48.b;
        dm7.g(bArr2, bArr, i, i3, i3 + min);
        x48.b += min;
        o0(p0() - ((long) min));
        if (x48.b != x48.c) {
            return min;
        }
        this.b = x48.b();
        y48.c.a(x48);
        return min;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte readByte() throws EOFException {
        if (p0() != 0) {
            x48 x48 = this.b;
            if (x48 != null) {
                int i = x48.b;
                int i2 = x48.c;
                int i3 = i + 1;
                byte b2 = x48.f4040a[i];
                o0(p0() - 1);
                if (i3 == i2) {
                    this.b = x48.b();
                    y48.c.a(x48);
                } else {
                    x48.b = i3;
                }
                return b2;
            }
            pq7.i();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void readFully(byte[] bArr) throws EOFException {
        pq7.c(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int read = read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public int readInt() throws EOFException {
        if (p0() >= 4) {
            x48 x48 = this.b;
            if (x48 != null) {
                int i = x48.b;
                int i2 = x48.c;
                if (((long) (i2 - i)) < 4) {
                    return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
                }
                byte[] bArr = x48.f4040a;
                int i3 = i + 1;
                byte b2 = bArr[i];
                int i4 = i3 + 1;
                byte b3 = bArr[i3];
                int i5 = i4 + 1;
                byte b4 = bArr[i4];
                int i6 = i5 + 1;
                byte b5 = bArr[i5];
                o0(p0() - 4);
                if (i6 == i2) {
                    this.b = x48.b();
                    y48.c.a(x48);
                } else {
                    x48.b = i6;
                }
                return ((b2 & 255) << 24) | ((b3 & 255) << 16) | ((b4 & 255) << 8) | (b5 & 255);
            }
            pq7.i();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public short readShort() throws EOFException {
        if (p0() >= 2) {
            x48 x48 = this.b;
            if (x48 != null) {
                int i = x48.b;
                int i2 = x48.c;
                if (i2 - i < 2) {
                    return (short) (((readByte() & 255) << 8) | (readByte() & 255));
                }
                byte[] bArr = x48.f4040a;
                int i3 = i + 1;
                byte b2 = bArr[i];
                int i4 = i3 + 1;
                byte b3 = bArr[i3];
                o0(p0() - 2);
                if (i4 == i2) {
                    this.b = x48.b();
                    y48.c.a(x48);
                } else {
                    x48.b = i4;
                }
                return (short) (((b2 & 255) << 8) | (b3 & 255));
            }
            pq7.i();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    public final x48 s0(int i) {
        boolean z = true;
        if (i < 1 || i > 8192) {
            z = false;
        }
        if (z) {
            x48 x48 = this.b;
            if (x48 == null) {
                x48 b2 = y48.c.b();
                this.b = b2;
                b2.g = b2;
                b2.f = b2;
                return b2;
            } else if (x48 != null) {
                x48 x482 = x48.g;
                if (x482 == null) {
                    pq7.i();
                    throw null;
                } else if (x482.c + i <= 8192 && x482.e) {
                    return x482;
                } else {
                    x48 b3 = y48.c.b();
                    x482.c(b3);
                    return b3;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            throw new IllegalArgumentException("unexpected capacity".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void skip(long j) throws EOFException {
        while (j > 0) {
            x48 x48 = this.b;
            if (x48 != null) {
                int min = (int) Math.min(j, (long) (x48.c - x48.b));
                long j2 = (long) min;
                o0(p0() - j2);
                j -= j2;
                int i = min + x48.b;
                x48.b = i;
                if (i == x48.c) {
                    this.b = x48.b();
                    y48.c.a(x48);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public i48 t() {
        return this;
    }

    @DexIgnore
    public i48 t0(l48 l48) {
        pq7.c(l48, "byteString");
        l48.write$okio(this, 0, l48.size());
        return this;
    }

    @DexIgnore
    public String toString() {
        return q0().toString();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean u() {
        return this.c == 0;
    }

    @DexIgnore
    public i48 u0(byte[] bArr) {
        pq7.c(bArr, "source");
        v0(bArr, 0, bArr.length);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 v(int i) {
        w0(i);
        return this;
    }

    @DexIgnore
    public i48 v0(byte[] bArr, int i, int i2) {
        pq7.c(bArr, "source");
        long j = (long) i2;
        f48.b((long) bArr.length, (long) i, j);
        int i3 = i2 + i;
        while (i < i3) {
            x48 s0 = s0(1);
            int min = Math.min(i3 - i, 8192 - s0.c);
            int i4 = i + min;
            dm7.g(bArr, s0.f4040a, s0.c, i, i4);
            s0.c = min + s0.c;
            i = i4;
        }
        o0(p0() + j);
        return this;
    }

    @DexIgnore
    public i48 w0(int i) {
        x48 s0 = s0(1);
        byte[] bArr = s0.f4040a;
        int i2 = s0.c;
        s0.c = i2 + 1;
        bArr[i2] = (byte) ((byte) i);
        o0(p0() + 1);
        return this;
    }

    @DexIgnore
    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        pq7.c(byteBuffer, "source");
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            x48 s0 = s0(1);
            int min = Math.min(i, 8192 - s0.c);
            byteBuffer.get(s0.f4040a, s0.c, min);
            i -= min;
            s0.c = min + s0.c;
        }
        this.c += (long) remaining;
        return remaining;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public /* bridge */ /* synthetic */ j48 x() {
        G();
        return this;
    }

    @DexIgnore
    public i48 x0(long j) {
        long j2;
        boolean z;
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            w0(48);
        } else {
            int i2 = 1;
            if (i < 0) {
                j2 = -j;
                if (j2 < 0) {
                    D0("-9223372036854775808");
                } else {
                    z = true;
                }
            } else {
                j2 = j;
                z = false;
            }
            if (j2 >= 100000000) {
                i2 = j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
            } else if (j2 >= ButtonService.CONNECT_TIMEOUT) {
                i2 = j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8;
            } else if (j2 >= 100) {
                i2 = j2 < 1000 ? 3 : 4;
            } else if (j2 >= 10) {
                i2 = 2;
            }
            if (z) {
                i2++;
            }
            x48 s0 = s0(i2);
            byte[] bArr = s0.f4040a;
            int i3 = s0.c + i2;
            while (j2 != 0) {
                long j3 = (long) 10;
                i3--;
                bArr[i3] = (byte) e58.a()[(int) (j2 % j3)];
                j2 /= j3;
            }
            if (z) {
                bArr[i3 - 1] = (byte) ((byte) 45);
            }
            s0.c += i2;
            o0(((long) i2) + p0());
        }
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        if (r3 != false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004b, code lost:
        r2.readByte();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0068, code lost:
        throw new java.lang.NumberFormatException("Number too large: " + r2.b0());
     */
    @DexIgnore
    @Override // com.fossil.k48
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long y() throws java.io.EOFException {
        /*
        // Method dump skipped, instructions count: 212
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i48.y():long");
    }

    @DexIgnore
    public i48 y0(long j) {
        if (j == 0) {
            w0(48);
        } else {
            long j2 = (j >>> 1) | j;
            long j3 = j2 | (j2 >>> 2);
            long j4 = j3 | (j3 >>> 4);
            long j5 = j4 | (j4 >>> 8);
            long j6 = j5 | (j5 >>> 16);
            long j7 = j6 | (j6 >>> 32);
            long j8 = j7 - ((j7 >>> 1) & 6148914691236517205L);
            long j9 = (j8 & 3689348814741910323L) + ((j8 >>> 2) & 3689348814741910323L);
            long j10 = (j9 + (j9 >>> 4)) & 1085102592571150095L;
            long j11 = j10 + (j10 >>> 8);
            long j12 = j11 + (j11 >>> 16);
            int i = (int) (((((j12 >>> 32) & 63) + (63 & j12)) + ((long) 3)) / ((long) 4));
            x48 s0 = s0(i);
            byte[] bArr = s0.f4040a;
            int i2 = s0.c;
            for (int i3 = (i2 + i) - 1; i3 >= i2; i3--) {
                bArr[i3] = (byte) e58.a()[(int) (15 & j)];
                j >>>= 4;
            }
            s0.c += i;
            o0(((long) i) + p0());
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String z(long j) throws EOFException {
        long j2 = Long.MAX_VALUE;
        if (j >= 0) {
            if (j != Long.MAX_VALUE) {
                j2 = j + 1;
            }
            byte b2 = (byte) 10;
            long P = P(b2, 0, j2);
            if (P != -1) {
                return e58.b(this, P);
            }
            if (j2 < p0() && M(j2 - 1) == ((byte) 13) && M(j2) == b2) {
                return e58.b(this, j2);
            }
            i48 i48 = new i48();
            C(i48, 0, Math.min((long) 32, p0()));
            throw new EOFException("\\n not found: limit=" + Math.min(p0(), j) + " content=" + i48.S().hex() + '\u2026');
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    @DexIgnore
    public i48 z0(int i) {
        x48 s0 = s0(4);
        byte[] bArr = s0.f4040a;
        int i2 = s0.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((byte) ((i >>> 24) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((byte) ((i >>> 16) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((byte) ((i >>> 8) & 255));
        bArr[i5] = (byte) ((byte) (i & 255));
        s0.c = i5 + 1;
        o0(p0() + 4);
        return this;
    }
}
