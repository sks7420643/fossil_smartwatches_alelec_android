package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum DEFAULT can be incorrect */
/* JADX WARN: Init of enum BLACK can be incorrect */
public enum cw1 {
    DEFAULT(r0),
    WHITE((int) 4294967295L),
    LIGHT_GRAY((int) 4290295992L),
    DARK_GRAY((int) 4285032552L),
    BLACK(r0);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final cw1 a(String str) {
            cw1[] values = cw1.values();
            for (cw1 cw1 : values) {
                if (vt7.j(cw1.name(), str, true)) {
                    return cw1;
                }
            }
            return null;
        }
    }

    /*
    static {
        int i = (int) 4278190080L;
    }
    */

    @DexIgnore
    public cw1(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int getColorCode() {
        return this.b;
    }
}
