package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class u87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3550a;

    /*
    static {
        int[] iArr = new int[lb7.values().length];
        f3550a = iArr;
        iArr[lb7.WEATHER.ordinal()] = 1;
        f3550a[lb7.SECOND_TIME.ordinal()] = 2;
        f3550a[lb7.ACTIVE.ordinal()] = 3;
        f3550a[lb7.BATTERY.ordinal()] = 4;
        f3550a[lb7.CALORIES.ordinal()] = 5;
        f3550a[lb7.RAIN.ordinal()] = 6;
        f3550a[lb7.DATE.ordinal()] = 7;
        f3550a[lb7.HEART.ordinal()] = 8;
        f3550a[lb7.STEP.ordinal()] = 9;
    }
    */
}
