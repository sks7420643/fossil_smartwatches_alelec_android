package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uh7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3592a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = "0";
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public long g; // = 0;

    @DexIgnore
    public uh7(String str, String str2, int i) {
        this.f3592a = str;
        this.b = str2;
        this.e = i;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            ji7.d(jSONObject, "ui", this.f3592a);
            ji7.d(jSONObject, "mc", this.b);
            ji7.d(jSONObject, "mid", this.d);
            ji7.d(jSONObject, "aid", this.c);
            jSONObject.put("ts", this.g);
            jSONObject.put("ver", this.f);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    @DexIgnore
    public void b(int i) {
        this.e = i;
    }

    @DexIgnore
    public String c() {
        return this.f3592a;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        return a().toString();
    }
}
