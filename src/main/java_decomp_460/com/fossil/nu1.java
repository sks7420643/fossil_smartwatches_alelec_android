package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nu1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public nu1(String str, byte[] bArr) {
        this.c = bArr;
        this.d = ix1.f1688a.b(bArr, ix1.a.CRC32);
        this.b = str.length() == 0 ? hy1.k((int) this.d, null, 1, null) : str;
    }

    @DexIgnore
    public final byte[] a() {
        if (!(!(this.c.length == 0))) {
            return new byte[0];
        }
        String str = this.b;
        Charset defaultCharset = Charset.defaultCharset();
        pq7.b(defaultCharset, "Charset.defaultCharset()");
        if (str != null) {
            byte[] bytes = str.getBytes(defaultCharset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] p = dm7.p(bytes, (byte) 0);
            int length = this.c.length;
            ByteBuffer allocate = ByteBuffer.allocate(p.length + 2 + length);
            pq7.b(allocate, "ByteBuffer.allocate(totalLen)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putShort((short) (length + p.length));
            allocate.put(p);
            allocate.put(this.c);
            byte[] array = allocate.array();
            pq7.b(array, "result.array()");
            return array;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final long b() {
        return this.d;
    }

    @DexIgnore
    public final Object c() {
        Object obj = d() ? JSONObject.NULL : this.b;
        pq7.b(obj, "if (isEmptyFile()) {\n   \u2026   fileName\n            }");
        return obj;
    }

    @DexIgnore
    public final boolean d() {
        return this.c.length == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            rl1 rl1 = (rl1) obj;
            return pq7.a(this.b, rl1.getFileName()) && Arrays.equals(this.c, rl1.getFileData());
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImage");
    }

    @DexIgnore
    public final byte[] getFileData() {
        return this.c;
    }

    @DexIgnore
    public final String getFileName() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + Arrays.hashCode(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.H, this.b);
            g80.k(jSONObject, jd0.I, Integer.valueOf(this.c.length));
            g80.k(jSONObject, jd0.J, Long.valueOf(this.d));
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.c);
        }
    }
}
