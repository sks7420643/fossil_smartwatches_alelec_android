package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc0 implements Parcelable.Creator<cc0> {
    @DexIgnore
    public /* synthetic */ bc0(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public cc0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray == null) {
                createByteArray = new byte[0];
            }
            return new cc0(readString, createByteArray);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public cc0[] newArray(int i) {
        return new cc0[i];
    }
}
