package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.SecureApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to5 implements Factory<so5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ApiServiceV2> f3448a;
    @DexIgnore
    public /* final */ Provider<SecureApiService> b;

    @DexIgnore
    public to5(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        this.f3448a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static to5 a(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        return new to5(provider, provider2);
    }

    @DexIgnore
    public static so5 c(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        return new so5(apiServiceV2, secureApiService);
    }

    @DexIgnore
    /* renamed from: b */
    public so5 get() {
        return c(this.f3448a.get(), this.b.get());
    }
}
