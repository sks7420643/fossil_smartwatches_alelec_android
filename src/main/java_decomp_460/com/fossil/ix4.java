package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix4 extends pv5 implements t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<f45> g;
    @DexIgnore
    public lx4 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ix4.k;
        }

        @DexIgnore
        public final ix4 b() {
            return new ix4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ix4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ix4 ix4) {
            super(1);
            this.this$0 = ix4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            ix4.L6(this.this$0).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<iz4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ix4 f1690a;

        @DexIgnore
        public c(ix4 ix4) {
            this.f1690a = ix4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(iz4 iz4) {
            boolean z = true;
            if (iz4 != null) {
                int i = jx4.f1828a[iz4.ordinal()];
                if (i == 1) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = this.f1690a.getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    String c = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886233);
                    pq7.b(c2, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    s37.E(childFragmentManager, c, c2);
                } else if (i == 2) {
                    if (PortfolioApp.h0.c().J().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        s37 s372 = s37.c;
                        FragmentManager childFragmentManager2 = this.f1690a.getChildFragmentManager();
                        pq7.b(childFragmentManager2, "childFragmentManager");
                        String c3 = um5.c(PortfolioApp.h0.c(), 2131886235);
                        pq7.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String c4 = um5.c(PortfolioApp.h0.c(), 2131886234);
                        pq7.b(c4, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        s372.E(childFragmentManager2, c3, c4);
                        return;
                    }
                    s37 s373 = s37.c;
                    FragmentManager childFragmentManager3 = this.f1690a.getChildFragmentManager();
                    pq7.b(childFragmentManager3, "childFragmentManager");
                    String c5 = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c5, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c6 = um5.c(PortfolioApp.h0.c(), 2131886226);
                    pq7.b(c6, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    s373.E(childFragmentManager3, c5, c6);
                } else if (i == 3) {
                    s37 s374 = s37.c;
                    FragmentManager childFragmentManager4 = this.f1690a.getChildFragmentManager();
                    pq7.b(childFragmentManager4, "childFragmentManager");
                    s374.C0(childFragmentManager4);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ix4 f1691a;

        @DexIgnore
        public d(ix4 ix4) {
            this.f1691a = ix4;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            this.f1691a.P6();
        }
    }

    /*
    static {
        String name = ix4.class.getName();
        pq7.b(name, "BCCreateSubTabFragment::class.java.name");
        k = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ lx4 L6(ix4 ix4) {
        lx4 lx4 = ix4.h;
        if (lx4 != null) {
            return lx4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        FlexibleButton flexibleButton;
        g37<f45> g37 = this.g;
        if (g37 != null) {
            f45 a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.q) != null) {
                fz4.a(flexibleButton, new b(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        lx4 lx4 = this.h;
        if (lx4 != null) {
            lx4.g().h(getViewLifecycleOwner(), new c(this));
            lx4 lx42 = this.h;
            if (lx42 != null) {
                lx42.f().h(getViewLifecycleOwner(), new d(this));
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        BCCreateChallengeIntroActivity.a aVar = BCCreateChallengeIntroActivity.A;
        Fragment requireParentFragment = requireParentFragment();
        pq7.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment);
    }

    @DexIgnore
    public final void Q6() {
        BCFindFriendsActivity.A.a(this, null);
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() != 1839815732 || !str.equals("FINDING_FRIEND")) {
            return;
        }
        if (i2 == 2131363291) {
            Q6();
        } else if (i2 == 2131363373) {
            P6();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        N6();
        O6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        f45 f45 = (f45) aq0.f(layoutInflater, 2131558527, viewGroup, false, A6());
        this.g = new g37<>(this, f45);
        PortfolioApp.h0.c().M().B1().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(lx4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.h = (lx4) a2;
            pq7.b(f45, "binding");
            return f45.n();
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
