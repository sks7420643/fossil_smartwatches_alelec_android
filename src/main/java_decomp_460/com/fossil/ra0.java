package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ra0 implements Parcelable.Creator<sa0> {
    @DexIgnore
    public /* synthetic */ ra0(kq7 kq7) {
    }

    @DexIgnore
    public sa0 a(Parcel parcel) {
        return new sa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public sa0 createFromParcel(Parcel parcel) {
        return new sa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public sa0[] newArray(int i) {
        return new sa0[i];
    }
}
