package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c53 implements z43 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f563a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.sampling.calculate_bundle_timestamp_before_sampling", true);

    @DexIgnore
    @Override // com.fossil.z43
    public final boolean zza() {
        return f563a.o().booleanValue();
    }
}
