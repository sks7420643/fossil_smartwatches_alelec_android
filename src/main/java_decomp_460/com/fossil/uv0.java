package com.fossil;

import android.renderscript.BaseObj;
import androidx.renderscript.RenderScript;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f3648a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public RenderScript c;

    @DexIgnore
    public uv0(long j, RenderScript renderScript) {
        renderScript.I();
        this.c = renderScript;
        this.f3648a = j;
    }

    @DexIgnore
    public void a() {
        if (this.f3648a == 0 && d() == null) {
            throw new yv0("Invalid object.");
        }
    }

    @DexIgnore
    public void b() {
        if (!this.b) {
            e();
            return;
        }
        throw new zv0("Object already destroyed.");
    }

    @DexIgnore
    public long c(RenderScript renderScript) {
        this.c.I();
        if (this.b) {
            throw new zv0("using a destroyed object.");
        } else if (this.f3648a == 0) {
            throw new aw0("Internal error: Object id 0.");
        } else if (renderScript == null || renderScript == this.c) {
            return this.f3648a;
        } else {
            throw new zv0("using object with mismatched context.");
        }
    }

    @DexIgnore
    public BaseObj d() {
        return null;
    }

    @DexIgnore
    public final void e() {
        boolean z = true;
        synchronized (this) {
            if (!this.b) {
                this.b = true;
            } else {
                z = false;
            }
        }
        if (z) {
            ReentrantReadWriteLock.ReadLock readLock = this.c.k.readLock();
            readLock.lock();
            if (this.c.h()) {
                this.c.A(this.f3648a);
            }
            readLock.unlock();
            this.c = null;
            this.f3648a = 0;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.f3648a == ((uv0) obj).f3648a;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        e();
        super.finalize();
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f3648a;
        return (int) ((j & 268435455) ^ (j >> 32));
    }
}
