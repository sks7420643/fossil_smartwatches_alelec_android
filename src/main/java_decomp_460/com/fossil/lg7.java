package com.fossil;

import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lg7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2191a;
    @DexIgnore
    public JSONArray b;
    @DexIgnore
    public JSONObject c; // = null;

    @DexIgnore
    public lg7() {
    }

    @DexIgnore
    public lg7(String str, String[] strArr, Properties properties) {
        JSONObject jSONObject;
        this.f2191a = str;
        if (properties != null) {
            jSONObject = new JSONObject(properties);
        } else if (strArr != null) {
            this.b = new JSONArray();
            for (String str2 : strArr) {
                this.b.put(str2);
            }
            return;
        } else {
            jSONObject = new JSONObject();
        }
        this.c = jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof lg7) {
            return toString().equals(((lg7) obj).toString());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return toString().hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.f2191a);
        sb.append(",");
        JSONArray jSONArray = this.b;
        if (jSONArray != null) {
            sb.append(jSONArray.toString());
        }
        JSONObject jSONObject = this.c;
        if (jSONObject != null) {
            sb.append(jSONObject.toString());
        }
        return sb.toString();
    }
}
