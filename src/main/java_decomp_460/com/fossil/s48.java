package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s48 {
    @DexIgnore
    public static final a58 a(File file) throws FileNotFoundException {
        pq7.c(file, "$this$appendingSink");
        return h(new FileOutputStream(file, true));
    }

    @DexIgnore
    public static final a58 b() {
        return new h48();
    }

    @DexIgnore
    public static final j48 c(a58 a58) {
        pq7.c(a58, "$this$buffer");
        return new v48(a58);
    }

    @DexIgnore
    public static final k48 d(c58 c58) {
        pq7.c(c58, "$this$buffer");
        return new w48(c58);
    }

    @DexIgnore
    public static final boolean e(AssertionError assertionError) {
        pq7.c(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? wt7.v(message, "getsockname failed", false, 2, null) : false;
    }

    @DexIgnore
    public static final a58 f(File file) throws FileNotFoundException {
        return j(file, false, 1, null);
    }

    @DexIgnore
    public static final a58 g(File file, boolean z) throws FileNotFoundException {
        pq7.c(file, "$this$sink");
        return h(new FileOutputStream(file, z));
    }

    @DexIgnore
    public static final a58 h(OutputStream outputStream) {
        pq7.c(outputStream, "$this$sink");
        return new t48(outputStream, new d58());
    }

    @DexIgnore
    public static final a58 i(Socket socket) throws IOException {
        pq7.c(socket, "$this$sink");
        b58 b58 = new b58(socket);
        OutputStream outputStream = socket.getOutputStream();
        pq7.b(outputStream, "getOutputStream()");
        return b58.v(new t48(outputStream, b58));
    }

    @DexIgnore
    public static /* synthetic */ a58 j(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return g(file, z);
    }

    @DexIgnore
    public static final c58 k(File file) throws FileNotFoundException {
        pq7.c(file, "$this$source");
        return l(new FileInputStream(file));
    }

    @DexIgnore
    public static final c58 l(InputStream inputStream) {
        pq7.c(inputStream, "$this$source");
        return new r48(inputStream, new d58());
    }

    @DexIgnore
    public static final c58 m(Socket socket) throws IOException {
        pq7.c(socket, "$this$source");
        b58 b58 = new b58(socket);
        InputStream inputStream = socket.getInputStream();
        pq7.b(inputStream, "getInputStream()");
        return b58.w(new r48(inputStream, b58));
    }
}
