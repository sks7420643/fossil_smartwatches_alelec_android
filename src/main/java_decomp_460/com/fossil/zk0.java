package com.fossil;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zk0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Bundle f4483a;
        @DexIgnore
        public IconCompat b;
        @DexIgnore
        public /* final */ dl0[] c;
        @DexIgnore
        public /* final */ dl0[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ boolean h;
        @DexIgnore
        @Deprecated
        public int i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public PendingIntent k;

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2 != 0 ? IconCompat.b(null, "", i2) : null, charSequence, pendingIntent);
        }

        @DexIgnore
        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false);
        }

        @DexIgnore
        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, dl0[] dl0Arr, dl0[] dl0Arr2, boolean z, int i2, boolean z2, boolean z3) {
            this.f = true;
            this.b = iconCompat;
            if (iconCompat != null && iconCompat.g() == 2) {
                this.i = iconCompat.c();
            }
            this.j = e.e(charSequence);
            this.k = pendingIntent;
            this.f4483a = bundle == null ? new Bundle() : bundle;
            this.c = dl0Arr;
            this.d = dl0Arr2;
            this.e = z;
            this.g = i2;
            this.f = z2;
            this.h = z3;
        }

        @DexIgnore
        public PendingIntent a() {
            return this.k;
        }

        @DexIgnore
        public boolean b() {
            return this.e;
        }

        @DexIgnore
        public dl0[] c() {
            return this.d;
        }

        @DexIgnore
        public Bundle d() {
            return this.f4483a;
        }

        @DexIgnore
        public IconCompat e() {
            int i2;
            if (this.b == null && (i2 = this.i) != 0) {
                this.b = IconCompat.b(null, "", i2);
            }
            return this.b;
        }

        @DexIgnore
        public dl0[] f() {
            return this.c;
        }

        @DexIgnore
        public int g() {
            return this.g;
        }

        @DexIgnore
        public boolean h() {
            return this.f;
        }

        @DexIgnore
        public CharSequence i() {
            return this.j;
        }

        @DexIgnore
        public boolean j() {
            return this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f {
        @DexIgnore
        public Bitmap e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        @Override // com.fossil.zk0.f
        public void b(yk0 yk0) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(yk0.a()).setBigContentTitle(this.b).bigPicture(this.e);
                if (this.g) {
                    bigPicture.bigLargeIcon(this.f);
                }
                if (this.d) {
                    bigPicture.setSummaryText(this.c);
                }
            }
        }

        @DexIgnore
        public b g(Bitmap bitmap) {
            this.f = bitmap;
            this.g = true;
            return this;
        }

        @DexIgnore
        public b h(Bitmap bitmap) {
            this.e = bitmap;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f {
        @DexIgnore
        public CharSequence e;

        @DexIgnore
        @Override // com.fossil.zk0.f
        public void b(yk0 yk0) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(yk0.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }

        @DexIgnore
        public c g(CharSequence charSequence) {
            this.e = e.e(charSequence);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public static Notification.BubbleMetadata h(d dVar) {
            if (dVar == null) {
                return null;
            }
            Notification.BubbleMetadata.Builder suppressNotification = new Notification.BubbleMetadata.Builder().setAutoExpandBubble(dVar.a()).setDeleteIntent(dVar.b()).setIcon(dVar.e().n()).setIntent(dVar.f()).setSuppressNotification(dVar.g());
            if (dVar.c() != 0) {
                suppressNotification.setDesiredHeight(dVar.c());
            }
            if (dVar.d() != 0) {
                suppressNotification.setDesiredHeightResId(dVar.d());
            }
            return suppressNotification.build();
        }

        @DexIgnore
        public abstract boolean a();

        @DexIgnore
        public abstract PendingIntent b();

        @DexIgnore
        public abstract int c();

        @DexIgnore
        public abstract int d();

        @DexIgnore
        public abstract IconCompat e();

        @DexIgnore
        public abstract PendingIntent f();

        @DexIgnore
        public abstract boolean g();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public String A;
        @DexIgnore
        public Bundle B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public Notification E;
        @DexIgnore
        public RemoteViews F;
        @DexIgnore
        public RemoteViews G;
        @DexIgnore
        public RemoteViews H;
        @DexIgnore
        public String I;
        @DexIgnore
        public int J;
        @DexIgnore
        public String K;
        @DexIgnore
        public long L;
        @DexIgnore
        public int M;
        @DexIgnore
        public boolean N;
        @DexIgnore
        public d O;
        @DexIgnore
        public Notification P;
        @DexIgnore
        public boolean Q;
        @DexIgnore
        @Deprecated
        public ArrayList<String> R;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f4484a;
        @DexIgnore
        public ArrayList<a> b;
        @DexIgnore
        public ArrayList<a> c;
        @DexIgnore
        public CharSequence d;
        @DexIgnore
        public CharSequence e;
        @DexIgnore
        public PendingIntent f;
        @DexIgnore
        public PendingIntent g;
        @DexIgnore
        public RemoteViews h;
        @DexIgnore
        public Bitmap i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public int k;
        @DexIgnore
        public int l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public f o;
        @DexIgnore
        public CharSequence p;
        @DexIgnore
        public CharSequence[] q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public String u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public String w;
        @DexIgnore
        public boolean x;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public boolean z;

        @DexIgnore
        @Deprecated
        public e(Context context) {
            this(context, null);
        }

        @DexIgnore
        public e(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            Notification notification = new Notification();
            this.P = notification;
            this.f4484a = context;
            this.I = str;
            notification.when = System.currentTimeMillis();
            this.P.audioStreamType = -1;
            this.l = 0;
            this.R = new ArrayList<>();
            this.N = true;
        }

        @DexIgnore
        public static CharSequence e(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        @DexIgnore
        public e A(f fVar) {
            if (this.o != fVar) {
                this.o = fVar;
                if (fVar != null) {
                    fVar.f(this);
                }
            }
            return this;
        }

        @DexIgnore
        public e B(CharSequence charSequence) {
            this.P.tickerText = e(charSequence);
            return this;
        }

        @DexIgnore
        public e C(long[] jArr) {
            this.P.vibrate = jArr;
            return this;
        }

        @DexIgnore
        public e D(int i2) {
            this.D = i2;
            return this;
        }

        @DexIgnore
        public e E(long j2) {
            this.P.when = j2;
            return this;
        }

        @DexIgnore
        public e a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new a(i2, charSequence, pendingIntent));
            return this;
        }

        @DexIgnore
        public e b(a aVar) {
            this.b.add(aVar);
            return this;
        }

        @DexIgnore
        public Notification c() {
            return new al0(this).c();
        }

        @DexIgnore
        public Bundle d() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        @DexIgnore
        public final Bitmap f(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.f4484a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(ok0.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(ok0.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(min * ((double) bitmap.getHeight())), true);
        }

        @DexIgnore
        public e g(boolean z2) {
            q(16, z2);
            return this;
        }

        @DexIgnore
        public e h(int i2) {
            this.J = i2;
            return this;
        }

        @DexIgnore
        public e i(String str) {
            this.A = str;
            return this;
        }

        @DexIgnore
        public e j(String str) {
            this.I = str;
            return this;
        }

        @DexIgnore
        public e k(int i2) {
            this.C = i2;
            return this;
        }

        @DexIgnore
        public e l(PendingIntent pendingIntent) {
            this.f = pendingIntent;
            return this;
        }

        @DexIgnore
        public e m(CharSequence charSequence) {
            this.e = e(charSequence);
            return this;
        }

        @DexIgnore
        public e n(CharSequence charSequence) {
            this.d = e(charSequence);
            return this;
        }

        @DexIgnore
        public e o(int i2) {
            Notification notification = this.P;
            notification.defaults = i2;
            if ((i2 & 4) != 0) {
                notification.flags |= 1;
            }
            return this;
        }

        @DexIgnore
        public e p(PendingIntent pendingIntent) {
            this.P.deleteIntent = pendingIntent;
            return this;
        }

        @DexIgnore
        public final void q(int i2, boolean z2) {
            if (z2) {
                this.P.flags |= i2;
                return;
            }
            this.P.flags &= i2;
        }

        @DexIgnore
        public e r(Bitmap bitmap) {
            this.i = f(bitmap);
            return this;
        }

        @DexIgnore
        public e s(int i2, int i3, int i4) {
            Notification notification = this.P;
            notification.ledARGB = i2;
            notification.ledOnMS = i3;
            notification.ledOffMS = i4;
            int i5 = (i3 == 0 || i4 == 0) ? 0 : 1;
            Notification notification2 = this.P;
            notification2.flags = i5 | (notification2.flags & -2);
            return this;
        }

        @DexIgnore
        public e t(boolean z2) {
            this.x = z2;
            return this;
        }

        @DexIgnore
        public e u(int i2) {
            this.k = i2;
            return this;
        }

        @DexIgnore
        public e v(boolean z2) {
            q(2, z2);
            return this;
        }

        @DexIgnore
        public e w(int i2) {
            this.l = i2;
            return this;
        }

        @DexIgnore
        public e x(boolean z2) {
            this.m = z2;
            return this;
        }

        @DexIgnore
        public e y(int i2) {
            this.P.icon = i2;
            return this;
        }

        @DexIgnore
        public e z(Uri uri) {
            Notification notification = this.P;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public e f4485a;
        @DexIgnore
        public CharSequence b;
        @DexIgnore
        public CharSequence c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public void a(Bundle bundle) {
        }

        @DexIgnore
        public abstract void b(yk0 yk0);

        @DexIgnore
        public RemoteViews c(yk0 yk0) {
            return null;
        }

        @DexIgnore
        public RemoteViews d(yk0 yk0) {
            return null;
        }

        @DexIgnore
        public RemoteViews e(yk0 yk0) {
            return null;
        }

        @DexIgnore
        public void f(e eVar) {
            if (this.f4485a != eVar) {
                this.f4485a = eVar;
                if (eVar != null) {
                    eVar.A(this);
                }
            }
        }
    }

    @DexIgnore
    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return bl0.c(notification);
        }
        return null;
    }
}
