package com.fossil;

import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ku extends Enum<ku> implements mt {
    @DexIgnore
    public static /* final */ ku d;
    @DexIgnore
    public static /* final */ ku e;
    @DexIgnore
    public static /* final */ ku f;
    @DexIgnore
    public static /* final */ ku g;
    @DexIgnore
    public static /* final */ /* synthetic */ ku[] h;
    @DexIgnore
    public static /* final */ ju i; // = new ju(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        ku kuVar = new ku("SUCCESS", 0, (byte) 0);
        d = kuVar;
        ku kuVar2 = new ku("INVALID_OPERATION_DATA", 1, (byte) 1);
        ku kuVar3 = new ku("OPERATION_IN_PROGRESS", 2, (byte) 2);
        ku kuVar4 = new ku("MISS_PACKET", 3, (byte) 3);
        ku kuVar5 = new ku("SOCKET_BUSY", 4, (byte) 4);
        ku kuVar6 = new ku("VERIFICATION_FAIL", 5, (byte) 5);
        ku kuVar7 = new ku("OVERFLOW", 6, (byte) 6);
        ku kuVar8 = new ku("SIZE_OVER_LIMIT", 7, (byte) 7);
        e = kuVar8;
        ku kuVar9 = new ku("FIRMWARE_INTERNAL_ERROR", 8, (byte) 128);
        f = kuVar9;
        ku kuVar10 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_OPEN", 9, (byte) 129);
        ku kuVar11 = new ku("FIRMWARE_INTERNAL_ERROR_ACCESS_ERROR", 10, (byte) 130);
        ku kuVar12 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_FOUND", 11, (byte) 131);
        ku kuVar13 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_VALID", 12, (byte) 132);
        ku kuVar14 = new ku("FIRMWARE_INTERNAL_ERROR_ALREADY_CREATE", 13, (byte) 133);
        ku kuVar15 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_ENOUGH_MEMORY", 14, (byte) 134);
        ku kuVar16 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_IMPLEMENTED", 15, (byte) 135);
        ku kuVar17 = new ku("FIRMWARE_INTERNAL_ERROR_NOT_SUPPORT", 16, (byte) 136);
        g = kuVar17;
        h = new ku[]{kuVar, kuVar2, kuVar3, kuVar4, kuVar5, kuVar6, kuVar7, kuVar8, kuVar9, kuVar10, kuVar11, kuVar12, kuVar13, kuVar14, kuVar15, kuVar16, kuVar17, new ku("FIRMWARE_INTERNAL_ERROR_SOCKET_BUSY", 17, (byte) 137), new ku("FIRMWARE_INTERNAL_ERROR_SOCKET_ALREADY_OPEN", 18, (byte) 138), new ku("FIRMWARE_INTERNAL_ERROR_INPUT_DATA_INVALID", 19, (byte) 139), new ku("FIRMWARE_INTERNAL_NOT_AUTHENTICATE", 20, (byte) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL), new ku("FIRMWARE_INTERNAL_SIZE_OVER_LIMIT", 21, (byte) 141)};
    }
    */

    @DexIgnore
    public ku(String str, int i2, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static ku valueOf(String str) {
        return (ku) Enum.valueOf(ku.class, str);
    }

    @DexIgnore
    public static ku[] values() {
        return (ku[]) h.clone();
    }

    @DexIgnore
    @Override // com.fossil.mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.mt
    public String getLogName() {
        return this.b;
    }
}
