package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.fossil.ry5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy5 extends AsyncTask<Void, Void, a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ WeakReference<CropImageView> f3047a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public FilterType f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Uri f3048a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Exception e;
        @DexIgnore
        public /* final */ Bitmap f;

        @DexIgnore
        public a(Uri uri, Bitmap bitmap, int i, int i2, Bitmap bitmap2) {
            this.f3048a = uri;
            this.b = bitmap;
            this.c = i;
            this.d = i2;
            this.e = null;
            this.f = bitmap2;
        }

        @DexIgnore
        public a(Uri uri, Exception exc) {
            this.f3048a = uri;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = exc;
            this.f = null;
        }
    }

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public qy5(CropImageView cropImageView, Uri uri, FilterType filterType) {
        this.b = uri;
        this.f = filterType;
        this.f3047a = new WeakReference<>(cropImageView);
        this.c = cropImageView.getContext();
        DisplayMetrics displayMetrics = cropImageView.getResources().getDisplayMetrics();
        float f2 = displayMetrics.density;
        double d2 = f2 > 1.0f ? (double) (1.0f / f2) : 1.0d;
        this.d = (int) (((double) displayMetrics.widthPixels) * d2);
        this.e = (int) (d2 * ((double) displayMetrics.heightPixels));
    }

    @DexIgnore
    /* renamed from: a */
    public a doInBackground(Void... voidArr) {
        ry5.b G;
        try {
            if (!isCancelled()) {
                ry5.a o = ry5.o(this.c, this.b, this.d, this.e);
                if (!isCancelled()) {
                    if (!TextUtils.equals(this.b.getLastPathSegment(), "pickerImage.jpg")) {
                        G = ry5.F(o.f3184a, this.c, this.b);
                    } else {
                        G = ry5.G(o.f3184a, new eq0(this.b.getPath()));
                    }
                    return new a(this.b, G.f3185a, o.b, G.b, EInkImageFilter.create().apply(G.f3185a, this.f, false, false, new OutputSettings(G.f3185a.getWidth(), G.f3185a.getHeight(), Format.RAW, false, false)).getPreview());
                }
            }
            return null;
        } catch (Exception e2) {
            return new a(this.b, e2);
        }
    }

    @DexIgnore
    public Uri b() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: c */
    public void onPostExecute(a aVar) {
        Bitmap bitmap;
        CropImageView cropImageView;
        if (aVar != null) {
            boolean z = false;
            if (!isCancelled() && (cropImageView = this.f3047a.get()) != null) {
                z = true;
                cropImageView.p(aVar);
            }
            if (!z && (bitmap = aVar.b) != null) {
                bitmap.recycle();
            }
        }
    }
}
