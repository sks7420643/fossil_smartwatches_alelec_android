package com.fossil;

import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp4 implements Factory<ShortcutApiService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3803a;
    @DexIgnore
    public /* final */ Provider<qq5> b;
    @DexIgnore
    public /* final */ Provider<uq5> c;

    @DexIgnore
    public vp4(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        this.f3803a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static vp4 a(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        return new vp4(uo4, provider, provider2);
    }

    @DexIgnore
    public static ShortcutApiService c(uo4 uo4, qq5 qq5, uq5 uq5) {
        ShortcutApiService C = uo4.C(qq5, uq5);
        lk7.c(C, "Cannot return null from a non-@Nullable @Provides method");
        return C;
    }

    @DexIgnore
    /* renamed from: b */
    public ShortcutApiService get() {
        return c(this.f3803a, this.b.get(), this.c.get());
    }
}
