package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m81 {
    @DexIgnore
    Object a(g51 g51, Bitmap bitmap, f81 f81, qn7<? super Bitmap> qn7);

    @DexIgnore
    String key();
}
