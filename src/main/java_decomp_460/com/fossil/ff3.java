package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ff3 implements Parcelable.Creator<oe3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ oe3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        int i2 = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i3 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    arrayList2 = ad2.j(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    ad2.x(parcel, t, arrayList, ff3.class.getClassLoader());
                    break;
                case 4:
                    f = ad2.r(parcel, t);
                    break;
                case 5:
                    i = ad2.v(parcel, t);
                    break;
                case 6:
                    i2 = ad2.v(parcel, t);
                    break;
                case 7:
                    f2 = ad2.r(parcel, t);
                    break;
                case 8:
                    z = ad2.m(parcel, t);
                    break;
                case 9:
                    z2 = ad2.m(parcel, t);
                    break;
                case 10:
                    z3 = ad2.m(parcel, t);
                    break;
                case 11:
                    i3 = ad2.v(parcel, t);
                    break;
                case 12:
                    arrayList3 = ad2.j(parcel, t, me3.CREATOR);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new oe3(arrayList2, arrayList, f, i, i2, f2, z, z2, z3, i3, arrayList3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ oe3[] newArray(int i) {
        return new oe3[i];
    }
}
