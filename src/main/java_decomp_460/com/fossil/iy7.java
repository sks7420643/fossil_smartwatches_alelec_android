package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy7 {
    /*
    static {
        Object r0;
        try {
            dl7.a aVar = dl7.Companion;
            r0 = dl7.m1constructorimpl(new gy7(a(Looper.getMainLooper(), true), "Main"));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            r0 = dl7.m1constructorimpl(el7.a(th));
        }
        if (dl7.m6isFailureimpl(r0)) {
            r0 = null;
        }
        hy7 hy7 = (hy7) r0;
    }
    */

    @DexIgnore
    public static final Handler a(Looper looper, boolean z) {
        int i;
        if (!z || (i = Build.VERSION.SDK_INT) < 16) {
            return new Handler(looper);
        }
        if (i >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            if (invoke != null) {
                return (Handler) invoke;
            }
            throw new il7("null cannot be cast to non-null type android.os.Handler");
        }
        try {
            return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        } catch (NoSuchMethodException e) {
            return new Handler(looper);
        }
    }
}
