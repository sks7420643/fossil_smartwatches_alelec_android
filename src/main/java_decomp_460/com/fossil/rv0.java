package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f3170a;
    @DexIgnore
    public a b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3171a; // = 0;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a(int i) {
            this.f3171a |= i;
        }

        @DexIgnore
        public boolean b() {
            int i = this.f3171a;
            if ((i & 7) != 0 && (i & (c(this.d, this.b) << 0)) == 0) {
                return false;
            }
            int i2 = this.f3171a;
            if ((i2 & 112) != 0 && (i2 & (c(this.d, this.c) << 4)) == 0) {
                return false;
            }
            int i3 = this.f3171a;
            if ((i3 & 1792) != 0 && (i3 & (c(this.e, this.b) << 8)) == 0) {
                return false;
            }
            int i4 = this.f3171a;
            return (i4 & 28672) == 0 || (i4 & (c(this.e, this.c) << 12)) != 0;
        }

        @DexIgnore
        public int c(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        @DexIgnore
        public void d() {
            this.f3171a = 0;
        }

        @DexIgnore
        public void e(int i, int i2, int i3, int i4) {
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        View a(int i);

        @DexIgnore
        int b(View view);

        @DexIgnore
        int c();

        @DexIgnore
        int d();

        @DexIgnore
        int e(View view);
    }

    @DexIgnore
    public rv0(b bVar) {
        this.f3170a = bVar;
    }

    @DexIgnore
    public View a(int i, int i2, int i3, int i4) {
        int c = this.f3170a.c();
        int d = this.f3170a.d();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View a2 = this.f3170a.a(i);
            this.b.e(c, d, this.f3170a.b(a2), this.f3170a.e(a2));
            if (i3 != 0) {
                this.b.d();
                this.b.a(i3);
                if (this.b.b()) {
                    return a2;
                }
            }
            if (i4 != 0) {
                this.b.d();
                this.b.a(i4);
                if (this.b.b()) {
                    i += i5;
                    view = a2;
                }
            }
            a2 = view;
            i += i5;
            view = a2;
        }
        return view;
    }

    @DexIgnore
    public boolean b(View view, int i) {
        this.b.e(this.f3170a.c(), this.f3170a.d(), this.f3170a.b(view), this.f3170a.e(view));
        if (i == 0) {
            return false;
        }
        this.b.d();
        this.b.a(i);
        return this.b.b();
    }
}
