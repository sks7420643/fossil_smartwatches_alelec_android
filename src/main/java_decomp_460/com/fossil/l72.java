package com.fossil;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.m62;
import com.fossil.p72;
import com.fossil.pc2;
import com.fossil.r62;
import com.fossil.yb2;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l72 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static l72 q;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f2151a; // = 5000;
    @DexIgnore
    public long b; // = 120000;
    @DexIgnore
    public long c; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ c62 e;
    @DexIgnore
    public /* final */ ic2 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<g72<?>, a<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public b82 j; // = null;
    @DexIgnore
    public /* final */ Set<g72<?>> k; // = new aj0();
    @DexIgnore
    public /* final */ Set<g72<?>> l; // = new aj0();
    @DexIgnore
    public /* final */ Handler m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a<O extends m62.d> implements r62.b, r62.c, va2 {
        @DexIgnore
        public /* final */ Queue<z82> b; // = new LinkedList();
        @DexIgnore
        public /* final */ m62.f c;
        @DexIgnore
        public /* final */ m62.b d;
        @DexIgnore
        public /* final */ g72<O> e;
        @DexIgnore
        public /* final */ a82 f;
        @DexIgnore
        public /* final */ Set<pa2> g; // = new HashSet();
        @DexIgnore
        public /* final */ Map<p72.a<?>, u92> h; // = new HashMap();
        @DexIgnore
        public /* final */ int i;
        @DexIgnore
        public /* final */ x92 j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public /* final */ List<b> l; // = new ArrayList();
        @DexIgnore
        public z52 m; // = null;

        @DexIgnore
        public a(q62<O> q62) {
            m62.f n = q62.n(l72.this.m.getLooper(), this);
            this.c = n;
            if (n instanceof wc2) {
                this.d = ((wc2) n).t0();
            } else {
                this.d = n;
            }
            this.e = q62.a();
            this.f = new a82();
            this.i = q62.l();
            if (this.c.v()) {
                this.j = q62.p(l72.this.d, l72.this.m);
            } else {
                this.j = null;
            }
        }

        @DexIgnore
        public final Map<p72.a<?>, u92> A() {
            return this.h;
        }

        @DexIgnore
        public final void B() {
            rc2.d(l72.this.m);
            this.m = null;
        }

        @DexIgnore
        public final z52 C() {
            rc2.d(l72.this.m);
            return this.m;
        }

        @DexIgnore
        public final void D() {
            if (this.k) {
                l72.this.m.removeMessages(11, this.e);
                l72.this.m.removeMessages(9, this.e);
                this.k = false;
            }
        }

        @DexIgnore
        public final void E() {
            l72.this.m.removeMessages(12, this.e);
            l72.this.m.sendMessageDelayed(l72.this.m.obtainMessage(12, this.e), l72.this.c);
        }

        @DexIgnore
        public final boolean F() {
            return J(true);
        }

        @DexIgnore
        public final ys3 G() {
            x92 x92 = this.j;
            if (x92 == null) {
                return null;
            }
            return x92.b3();
        }

        @DexIgnore
        public final void H(Status status) {
            rc2.d(l72.this.m);
            k(status, null, false);
        }

        @DexIgnore
        public final void I(z82 z82) {
            z82.d(this.f, f());
            try {
                z82.c(this);
            } catch (DeadObjectException e2) {
                d(1);
                this.c.a();
            } catch (Throwable th) {
                throw new IllegalStateException(String.format("Error in GoogleApi implementation for client %s.", this.d.getClass().getName()), th);
            }
        }

        @DexIgnore
        public final boolean J(boolean z) {
            rc2.d(l72.this.m);
            if (!this.c.c() || this.h.size() != 0) {
                return false;
            }
            if (!this.f.e()) {
                this.c.a();
                return true;
            } else if (!z) {
                return false;
            } else {
                E();
                return false;
            }
        }

        @DexIgnore
        public final void N(z52 z52) {
            rc2.d(l72.this.m);
            this.c.a();
            n(z52);
        }

        @DexIgnore
        public final boolean O(z52 z52) {
            synchronized (l72.p) {
                if (l72.this.j == null || !l72.this.k.contains(this.e)) {
                    return false;
                }
                l72.this.j.n(z52, this.i);
                return true;
            }
        }

        @DexIgnore
        public final void P(z52 z52) {
            for (pa2 pa2 : this.g) {
                String str = null;
                if (pc2.a(z52, z52.f)) {
                    str = this.c.k();
                }
                pa2.b(this.e, z52, str);
            }
            this.g.clear();
        }

        @DexIgnore
        public final Status Q(z52 z52) {
            String a2 = this.e.a();
            String valueOf = String.valueOf(z52);
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 63 + String.valueOf(valueOf).length());
            sb.append("API: ");
            sb.append(a2);
            sb.append(" is not available on this device. Connection failed with: ");
            sb.append(valueOf);
            return new Status(17, sb.toString());
        }

        @DexIgnore
        public final m62.f R() {
            return this.c;
        }

        @DexIgnore
        public final void a() {
            rc2.d(l72.this.m);
            if (!this.c.c() && !this.c.j()) {
                try {
                    int b2 = l72.this.f.b(l72.this.d, this.c);
                    if (b2 != 0) {
                        z52 z52 = new z52(b2, null);
                        String name = this.d.getClass().getName();
                        String valueOf = String.valueOf(z52);
                        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 35 + String.valueOf(valueOf).length());
                        sb.append("The service for ");
                        sb.append(name);
                        sb.append(" is not available: ");
                        sb.append(valueOf);
                        Log.w("GoogleApiManager", sb.toString());
                        n(z52);
                        return;
                    }
                    c cVar = new c(this.c, this.e);
                    if (this.c.v()) {
                        this.j.a3(cVar);
                    }
                    try {
                        this.c.l(cVar);
                    } catch (SecurityException e2) {
                        j(new z52(10), e2);
                    }
                } catch (IllegalStateException e3) {
                    j(new z52(10), e3);
                }
            }
        }

        @DexIgnore
        public final int b() {
            return this.i;
        }

        @DexIgnore
        public final boolean c() {
            return this.c.c();
        }

        @DexIgnore
        @Override // com.fossil.k72
        public final void d(int i2) {
            if (Looper.myLooper() == l72.this.m.getLooper()) {
                x();
            } else {
                l72.this.m.post(new i92(this));
            }
        }

        @DexIgnore
        @Override // com.fossil.k72
        public final void e(Bundle bundle) {
            if (Looper.myLooper() == l72.this.m.getLooper()) {
                w();
            } else {
                l72.this.m.post(new j92(this));
            }
        }

        @DexIgnore
        public final boolean f() {
            return this.c.v();
        }

        @DexIgnore
        public final void g() {
            rc2.d(l72.this.m);
            if (this.k) {
                a();
            }
        }

        @DexIgnore
        public final b62 h(b62[] b62Arr) {
            if (!(b62Arr == null || b62Arr.length == 0)) {
                b62[] t = this.c.t();
                if (t == null) {
                    t = new b62[0];
                }
                zi0 zi0 = new zi0(t.length);
                for (b62 b62 : t) {
                    zi0.put(b62.c(), Long.valueOf(b62.f()));
                }
                for (b62 b622 : b62Arr) {
                    if (!zi0.containsKey(b622.c()) || ((Long) zi0.get(b622.c())).longValue() < b622.f()) {
                        return b622;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.va2
        public final void i(z52 z52, m62<?> m62, boolean z) {
            if (Looper.myLooper() == l72.this.m.getLooper()) {
                n(z52);
            } else {
                l72.this.m.post(new l92(this, z52));
            }
        }

        @DexIgnore
        public final void j(z52 z52, Exception exc) {
            rc2.d(l72.this.m);
            x92 x92 = this.j;
            if (x92 != null) {
                x92.c3();
            }
            B();
            l72.this.f.a();
            P(z52);
            if (z52.c() == 4) {
                H(l72.o);
            } else if (this.b.isEmpty()) {
                this.m = z52;
            } else if (exc != null) {
                rc2.d(l72.this.m);
                k(null, exc, false);
            } else {
                k(Q(z52), null, true);
                if (!this.b.isEmpty() && !O(z52) && !l72.this.v(z52, this.i)) {
                    if (z52.c() == 18) {
                        this.k = true;
                    }
                    if (this.k) {
                        l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 9, this.e), l72.this.f2151a);
                    } else {
                        H(Q(z52));
                    }
                }
            }
        }

        @DexIgnore
        public final void k(Status status, Exception exc, boolean z) {
            boolean z2 = true;
            rc2.d(l72.this.m);
            boolean z3 = status == null;
            if (exc != null) {
                z2 = false;
            }
            if (z3 != z2) {
                Iterator<z82> it = this.b.iterator();
                while (it.hasNext()) {
                    z82 next = it.next();
                    if (!z || next.f4431a == 2) {
                        if (status != null) {
                            next.b(status);
                        } else {
                            next.e(exc);
                        }
                        it.remove();
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Status XOR exception should be null");
        }

        @DexIgnore
        @Override // com.fossil.r72
        public final void n(z52 z52) {
            j(z52, null);
        }

        @DexIgnore
        public final void o(b bVar) {
            if (!this.l.contains(bVar) || this.k) {
                return;
            }
            if (!this.c.c()) {
                a();
            } else {
                y();
            }
        }

        @DexIgnore
        public final void p(z82 z82) {
            rc2.d(l72.this.m);
            if (!this.c.c()) {
                this.b.add(z82);
                z52 z52 = this.m;
                if (z52 == null || !z52.k()) {
                    a();
                } else {
                    n(this.m);
                }
            } else if (v(z82)) {
                E();
            } else {
                this.b.add(z82);
            }
        }

        @DexIgnore
        public final void q(pa2 pa2) {
            rc2.d(l72.this.m);
            this.g.add(pa2);
        }

        @DexIgnore
        public final void s() {
            rc2.d(l72.this.m);
            if (this.k) {
                D();
                H(l72.this.e.i(l72.this.d) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
                this.c.a();
            }
        }

        @DexIgnore
        public final void u(b bVar) {
            b62[] g2;
            if (this.l.remove(bVar)) {
                l72.this.m.removeMessages(15, bVar);
                l72.this.m.removeMessages(16, bVar);
                b62 b62 = bVar.b;
                ArrayList arrayList = new ArrayList(this.b.size());
                for (z82 z82 : this.b) {
                    if ((z82 instanceof ja2) && (g2 = ((ja2) z82).g(this)) != null && bf2.b(g2, b62)) {
                        arrayList.add(z82);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    z82 z822 = (z82) obj;
                    this.b.remove(z822);
                    z822.e(new e72(b62));
                }
            }
        }

        @DexIgnore
        public final boolean v(z82 z82) {
            if (!(z82 instanceof ja2)) {
                I(z82);
                return true;
            }
            ja2 ja2 = (ja2) z82;
            b62 h2 = h(ja2.g(this));
            if (h2 == null) {
                I(z82);
                return true;
            }
            String name = this.d.getClass().getName();
            String c2 = h2.c();
            long f2 = h2.f();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 77 + String.valueOf(c2).length());
            sb.append(name);
            sb.append(" could not execute call because it requires feature (");
            sb.append(c2);
            sb.append(", ");
            sb.append(f2);
            sb.append(").");
            Log.w("GoogleApiManager", sb.toString());
            if (ja2.h(this)) {
                b bVar = new b(this.e, h2, null);
                int indexOf = this.l.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.l.get(indexOf);
                    l72.this.m.removeMessages(15, bVar2);
                    l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 15, bVar2), l72.this.f2151a);
                } else {
                    this.l.add(bVar);
                    l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 15, bVar), l72.this.f2151a);
                    l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 16, bVar), l72.this.b);
                    z52 z52 = new z52(2, null);
                    if (!O(z52)) {
                        l72.this.v(z52, this.i);
                    }
                }
                return false;
            }
            ja2.e(new e72(h2));
            return true;
        }

        @DexIgnore
        public final void w() {
            B();
            P(z52.f);
            D();
            Iterator<u92> it = this.h.values().iterator();
            while (it.hasNext()) {
                u92 next = it.next();
                if (h(next.f3556a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.f3556a.d(this.d, new ot3<>());
                    } catch (DeadObjectException e2) {
                        d(1);
                        this.c.a();
                    } catch (RemoteException e3) {
                        it.remove();
                    }
                }
            }
            y();
            E();
        }

        @DexIgnore
        public final void x() {
            B();
            this.k = true;
            this.f.g();
            l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 9, this.e), l72.this.f2151a);
            l72.this.m.sendMessageDelayed(Message.obtain(l72.this.m, 11, this.e), l72.this.b);
            l72.this.f.a();
            for (u92 u92 : this.h.values()) {
                u92.c.run();
            }
        }

        @DexIgnore
        public final void y() {
            ArrayList arrayList = new ArrayList(this.b);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                z82 z82 = (z82) obj;
                if (!this.c.c()) {
                    return;
                }
                if (v(z82)) {
                    this.b.remove(z82);
                }
            }
        }

        @DexIgnore
        public final void z() {
            rc2.d(l72.this.m);
            H(l72.n);
            this.f.f();
            for (p72.a aVar : (p72.a[]) this.h.keySet().toArray(new p72.a[this.h.size()])) {
                p(new ma2(aVar, new ot3()));
            }
            P(new z52(4));
            if (this.c.c()) {
                this.c.n(new k92(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ g72<?> f2152a;
        @DexIgnore
        public /* final */ b62 b;

        @DexIgnore
        public b(g72<?> g72, b62 b62) {
            this.f2152a = g72;
            this.b = b62;
        }

        @DexIgnore
        public /* synthetic */ b(g72 g72, b62 b62, h92 h92) {
            this(g72, b62);
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                return pc2.a(this.f2152a, bVar.f2152a) && pc2.a(this.b, bVar.b);
            }
        }

        @DexIgnore
        public final int hashCode() {
            return pc2.b(this.f2152a, this.b);
        }

        @DexIgnore
        public final String toString() {
            pc2.a c = pc2.c(this);
            c.a("key", this.f2152a);
            c.a("feature", this.b);
            return c.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements aa2, yb2.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ m62.f f2153a;
        @DexIgnore
        public /* final */ g72<?> b;
        @DexIgnore
        public jc2 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public c(m62.f fVar, g72<?> g72) {
            this.f2153a = fVar;
            this.b = g72;
        }

        @DexIgnore
        @Override // com.fossil.yb2.c
        public final void a(z52 z52) {
            l72.this.m.post(new n92(this, z52));
        }

        @DexIgnore
        @Override // com.fossil.aa2
        public final void b(jc2 jc2, Set<Scope> set) {
            if (jc2 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                c(new z52(4));
                return;
            }
            this.c = jc2;
            this.d = set;
            g();
        }

        @DexIgnore
        @Override // com.fossil.aa2
        public final void c(z52 z52) {
            ((a) l72.this.i.get(this.b)).N(z52);
        }

        @DexIgnore
        public final void g() {
            jc2 jc2;
            if (this.e && (jc2 = this.c) != null) {
                this.f2153a.i(jc2, this.d);
            }
        }
    }

    @DexIgnore
    public l72(Context context, Looper looper, c62 c62) {
        this.d = context;
        this.m = new ol2(looper, this);
        this.e = c62;
        this.f = new ic2(c62);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static void b() {
        synchronized (p) {
            if (q != null) {
                l72 l72 = q;
                l72.h.incrementAndGet();
                l72.m.sendMessageAtFrontOfQueue(l72.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static l72 m() {
        l72 l72;
        synchronized (p) {
            rc2.l(q, "Must guarantee manager is non-null before using getInstance");
            l72 = q;
        }
        return l72;
    }

    @DexIgnore
    public static l72 o(Context context) {
        l72 l72;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new l72(context.getApplicationContext(), handlerThread.getLooper(), c62.q());
            }
            l72 = q;
        }
        return l72;
    }

    @DexIgnore
    public final void D() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final PendingIntent c(g72<?> g72, int i2) {
        a<?> aVar = this.i.get(g72);
        if (aVar == null) {
            return null;
        }
        ys3 G = aVar.G();
        if (G == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, G.u(), 134217728);
    }

    @DexIgnore
    public final <O extends m62.d> nt3<Boolean> e(q62<O> q62, p72.a<?> aVar) {
        ot3 ot3 = new ot3();
        ma2 ma2 = new ma2(aVar, ot3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new t92(ma2, this.h.get(), q62)));
        return ot3.a();
    }

    @DexIgnore
    public final <O extends m62.d> nt3<Void> f(q62<O> q62, s72<m62.b, ?> s72, y72<m62.b, ?> y72, Runnable runnable) {
        ot3 ot3 = new ot3();
        ka2 ka2 = new ka2(new u92(s72, y72, runnable), ot3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new t92(ka2, this.h.get(), q62)));
        return ot3.a();
    }

    @DexIgnore
    public final nt3<Map<g72<?>, String>> g(Iterable<? extends s62<?>> iterable) {
        pa2 pa2 = new pa2(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, pa2));
        return pa2.a();
    }

    @DexIgnore
    public final void h(z52 z52, int i2) {
        if (!v(z52, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, z52));
        }
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        a<?> aVar;
        int i2 = message.what;
        switch (i2) {
            case 1:
                this.c = ((Boolean) message.obj).booleanValue() ? ButtonService.CONNECT_TIMEOUT : 300000;
                this.m.removeMessages(12);
                for (g72<?> g72 : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, g72), this.c);
                }
                break;
            case 2:
                pa2 pa2 = (pa2) message.obj;
                Iterator<g72<?>> it = pa2.c().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        g72<?> next = it.next();
                        a<?> aVar2 = this.i.get(next);
                        if (aVar2 == null) {
                            pa2.b(next, new z52(13), null);
                            break;
                        } else if (aVar2.c()) {
                            pa2.b(next, z52.f, aVar2.R().k());
                        } else if (aVar2.C() != null) {
                            pa2.b(next, aVar2.C(), null);
                        } else {
                            aVar2.q(pa2);
                            aVar2.a();
                        }
                    }
                }
            case 3:
                for (a<?> aVar3 : this.i.values()) {
                    aVar3.B();
                    aVar3.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                t92 t92 = (t92) message.obj;
                a<?> aVar4 = this.i.get(t92.c.a());
                if (aVar4 == null) {
                    p(t92.c);
                    aVar4 = this.i.get(t92.c.a());
                }
                if (!aVar4.f() || this.h.get() == t92.b) {
                    aVar4.p(t92.f3379a);
                    break;
                } else {
                    t92.f3379a.b(n);
                    aVar4.z();
                    break;
                }
            case 5:
                int i3 = message.arg1;
                z52 z52 = (z52) message.obj;
                Iterator<a<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = it2.next();
                        if (aVar.b() == i3) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar != null) {
                    String g2 = this.e.g(z52.c());
                    String f2 = z52.f();
                    StringBuilder sb = new StringBuilder(String.valueOf(g2).length() + 69 + String.valueOf(f2).length());
                    sb.append("Error resolution was canceled by the user, original error message: ");
                    sb.append(g2);
                    sb.append(": ");
                    sb.append(f2);
                    aVar.H(new Status(17, sb.toString()));
                    break;
                } else {
                    StringBuilder sb2 = new StringBuilder(76);
                    sb2.append("Could not find API instance ");
                    sb2.append(i3);
                    sb2.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb2.toString(), new Exception());
                    break;
                }
            case 6:
                if (this.d.getApplicationContext() instanceof Application) {
                    h72.c((Application) this.d.getApplicationContext());
                    h72.b().a(new h92(this));
                    if (!h72.b().f(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                p((q62) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 10:
                for (g72<?> g722 : this.l) {
                    this.i.remove(g722).z();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).s();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).F();
                    break;
                }
                break;
            case 14:
                e82 e82 = (e82) message.obj;
                g72<?> a2 = e82.a();
                if (!this.i.containsKey(a2)) {
                    e82.b().c(Boolean.FALSE);
                    break;
                } else {
                    e82.b().c(Boolean.valueOf(this.i.get(a2).J(false)));
                    break;
                }
            case 15:
                b bVar = (b) message.obj;
                if (this.i.containsKey(bVar.f2152a)) {
                    this.i.get(bVar.f2152a).o(bVar);
                    break;
                }
                break;
            case 16:
                b bVar2 = (b) message.obj;
                if (this.i.containsKey(bVar2.f2152a)) {
                    this.i.get(bVar2.f2152a).u(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void i(q62<?> q62) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, q62));
    }

    @DexIgnore
    public final <O extends m62.d> void j(q62<O> q62, int i2, i72<? extends z62, m62.b> i72) {
        la2 la2 = new la2(i2, i72);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new t92(la2, this.h.get(), q62)));
    }

    @DexIgnore
    public final <O extends m62.d, ResultT> void k(q62<O> q62, int i2, w72<m62.b, ResultT> w72, ot3<ResultT> ot3, u72 u72) {
        na2 na2 = new na2(i2, w72, ot3, u72);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new t92(na2, this.h.get(), q62)));
    }

    @DexIgnore
    public final void l(b82 b82) {
        synchronized (p) {
            if (this.j != b82) {
                this.j = b82;
                this.k.clear();
            }
            this.k.addAll(b82.r());
        }
    }

    @DexIgnore
    public final void p(q62<?> q62) {
        g72<?> a2 = q62.a();
        a<?> aVar = this.i.get(a2);
        if (aVar == null) {
            aVar = new a<>(q62);
            this.i.put(a2, aVar);
        }
        if (aVar.f()) {
            this.l.add(a2);
        }
        aVar.a();
    }

    @DexIgnore
    public final void q(b82 b82) {
        synchronized (p) {
            if (this.j == b82) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final int r() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final boolean v(z52 z52, int i2) {
        return this.e.B(this.d, z52, i2);
    }
}
