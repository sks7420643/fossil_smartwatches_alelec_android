package com.fossil;

import java.lang.reflect.AccessibleObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lk4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lk4 f2209a; // = (xj4.c() < 9 ? new kk4() : new mk4());

    @DexIgnore
    public static lk4 a() {
        return f2209a;
    }

    @DexIgnore
    public abstract void b(AccessibleObject accessibleObject);
}
