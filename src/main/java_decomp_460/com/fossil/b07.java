package com.fossil;

import com.fossil.c37;
import com.fossil.iq4;
import com.fossil.z27;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b07 extends yz6 {
    @DexIgnore
    public z27 e;
    @DexIgnore
    public c37 f;
    @DexIgnore
    public SignUpEmailAuth g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ zz6 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<z27.c, z27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ b07 f377a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(b07 b07) {
            this.f377a = b07;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(z27.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f377a.i.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "resendOtpCode errorCode=" + bVar.a() + " message=" + bVar.b());
            this.f377a.i.l5(bVar.a(), bVar.b());
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(z27.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f377a.i.h();
            this.f377a.i.D5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<c37.c, c37.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ b07 f378a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(b07 b07) {
            this.f378a = b07;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(c37.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f378a.i.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "verifyOtpCode errorCode=" + bVar.a() + " message=" + bVar.b());
            this.f378a.i.c5(bVar.a() == 401);
            this.f378a.i.l5(bVar.a(), bVar.b());
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(c37.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f378a.i.h();
            zz6 zz6 = this.f378a.i;
            SignUpEmailAuth t = this.f378a.t();
            if (t != null) {
                zz6.z1(t.getEmail(), 10, 20);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public b07(zz6 zz6) {
        pq7.c(zz6, "mView");
        this.i = zz6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            this.i.T0(signUpEmailAuth.getEmail());
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.yz6
    public void n(String[] strArr) {
        String str;
        boolean z = true;
        pq7.c(strArr, "codes");
        v(strArr);
        String str2 = this.h;
        if ((str2 == null || vt7.l(str2)) || (str = this.h) == null || str.length() != 4) {
            z = false;
        }
        this.i.M1(z);
    }

    @DexIgnore
    @Override // com.fossil.yz6
    public void o() {
        this.i.U1();
    }

    @DexIgnore
    @Override // com.fossil.yz6
    public void p() {
        this.i.c5(false);
        this.i.i();
        z27 z27 = this.e;
        if (z27 != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            String email = signUpEmailAuth != null ? signUpEmailAuth.getEmail() : null;
            if (email != null) {
                z27.e(new z27.a(email), new a(this));
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yz6
    public void q() {
        zz6 zz6 = this.i;
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            zz6.B1(signUpEmailAuth);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yz6
    public void r() {
        this.i.c5(false);
        this.i.i();
        c37 c37 = this.f;
        if (c37 != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            if (signUpEmailAuth != null) {
                String email = signUpEmailAuth.getEmail();
                String str = this.h;
                if (str != null) {
                    c37.e(new c37.a(email, str), new b(this));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mVerifyEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public final SignUpEmailAuth t() {
        return this.g;
    }

    @DexIgnore
    public final void u(SignUpEmailAuth signUpEmailAuth) {
        pq7.c(signUpEmailAuth, "emailAuth");
        this.g = signUpEmailAuth;
    }

    @DexIgnore
    public final void v(String[] strArr) {
        this.h = "";
        for (String str : strArr) {
            String str2 = this.h;
            if (str != null) {
                this.h = pq7.h(str2, wt7.u0(str).toString());
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }

    @DexIgnore
    public void w() {
        this.i.M5(this);
    }
}
