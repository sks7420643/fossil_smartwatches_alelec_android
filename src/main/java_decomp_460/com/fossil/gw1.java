package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.Format;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gw1 extends fw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gw1 createFromParcel(Parcel parcel) {
            return new gw1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gw1[] newArray(int i) {
            return new gw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ gw1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public gw1(String str, byte[] bArr) {
        super(str, bArr, Format.RLE);
    }

    @DexIgnore
    @Override // com.fossil.fw1, com.fossil.fw1, java.lang.Object
    public gw1 clone() {
        String name = getName();
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new gw1(name, copyOf);
    }
}
