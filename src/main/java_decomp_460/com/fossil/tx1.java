package com.fossil;

import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tx1<T> extends qx1<T> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tx1(ry1 ry1) {
        super(ry1);
        pq7.c(ry1, "version");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ tx1(ry1 ry1, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ry1(1, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.qx1
    public byte[] a(short s, T t) {
        pq7.c(t, "entries");
        byte[] b = b(t);
        byte[] array = ByteBuffer.allocate(b.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put((byte) c().getMajor()).put((byte) c().getMinor()).putInt(0).putInt(b.length).put(b).putInt((int) ix1.f1688a.b(b, ix1.a.CRC32)).array();
        pq7.b(array, "result.array()");
        return array;
    }
}
