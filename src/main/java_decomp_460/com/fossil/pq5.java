package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.DeclarationFile;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2859a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public List<DeclarationFile> d;
    @DexIgnore
    public long e;
    @DexIgnore
    public long f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;

    @DexIgnore
    public MicroAppVariant a() {
        MicroAppVariant microAppVariant = new MicroAppVariant();
        microAppVariant.setAppId(this.f2859a);
        microAppVariant.setName(this.b);
        microAppVariant.setDescription(this.c);
        microAppVariant.setCreateAt(this.e);
        microAppVariant.setUpdateAt(this.f);
        microAppVariant.setMajorNumber(this.g);
        microAppVariant.setMinorNumber(this.h);
        microAppVariant.setSerialNumbers(this.i);
        return microAppVariant;
    }

    @DexIgnore
    public String b() {
        return this.f2859a;
    }

    @DexIgnore
    public List<DeclarationFile> c() {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.h;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public void f(String str) {
        this.f2859a = str;
    }

    @DexIgnore
    public void g(long j) {
        this.e = j;
    }

    @DexIgnore
    public void h(List<DeclarationFile> list) {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        this.d.addAll(list);
    }

    @DexIgnore
    public void i(String str) {
        this.c = str;
    }

    @DexIgnore
    public void j(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void k(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void l(String str) {
        this.b = str;
    }

    @DexIgnore
    public void m(String str) {
        this.i = str;
    }

    @DexIgnore
    public void n(long j) {
        this.f = j;
    }
}
