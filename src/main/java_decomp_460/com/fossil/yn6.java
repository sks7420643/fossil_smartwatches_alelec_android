package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Date f4337a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public yn6(Date date, long j) {
        pq7.c(date, "date");
        this.f4337a = date;
        this.b = j;
    }

    @DexIgnore
    public final Date a() {
        return this.f4337a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof yn6) {
                yn6 yn6 = (yn6) obj;
                if (!pq7.a(this.f4337a, yn6.f4337a) || this.b != yn6.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.f4337a;
        return ((date != null ? date.hashCode() : 0) * 31) + c.a(this.b);
    }

    @DexIgnore
    public String toString() {
        return "ActivityDailyBest(date=" + this.f4337a + ", value=" + this.b + ")";
    }
}
