package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xr3 b;
    @DexIgnore
    public /* final */ /* synthetic */ or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 d;

    @DexIgnore
    public gn3(qm3 qm3, xr3 xr3, or3 or3) {
        this.d = qm3;
        this.b = xr3;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        this.d.b.d0();
        if (this.b.d.c() == null) {
            this.d.b.O(this.b, this.c);
        } else {
            this.d.b.x(this.b, this.c);
        }
    }
}
