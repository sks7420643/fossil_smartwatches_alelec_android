package com.fossil;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ja7;
import com.fossil.s87;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea7 extends pv5 {
    @DexIgnore
    public po4 g;
    @DexIgnore
    public dh5 h;
    @DexIgnore
    public ia7 i;
    @DexIgnore
    public /* final */ Map<o87, Integer> j; // = zm7.j(hl7.a(o87.BLACK, 2131362976), hl7.a(o87.WHITE, 2131362979), hl7.a(o87.LIGHT_GRAY, 2131362978), hl7.a(o87.DARK_GRAY, 2131362977));
    @DexIgnore
    public Typeface k; // = z87.d.d();
    @DexIgnore
    public o87 l; // = o87.Companion.a();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<Typeface, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ea7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ea7 ea7) {
            super(1);
            this.this$0 = ea7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Typeface typeface) {
            invoke(typeface);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(Typeface typeface) {
            pq7.c(typeface, "it");
            this.this$0.k = typeface;
            gc7 e = hc7.c.e(this.this$0);
            if (e != null) {
                e.D(typeface);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ea7 f905a;

        @DexIgnore
        public b(ea7 ea7) {
            this.f905a = ea7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            T t3 = !(t2 instanceof s87.c) ? null : t2;
            if (t3 != null) {
                this.f905a.k = t3.j();
                this.f905a.l = t3.g();
                ea7 ea7 = this.f905a;
                ea7.S6(ea7.k, this.f905a.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RadioGroup.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ea7 f906a;

        @DexIgnore
        public c(ea7 ea7) {
            this.f906a = ea7;
        }

        @DexIgnore
        public final void onCheckedChanged(RadioGroup radioGroup, int i) {
            o87 o87;
            ea7 ea7 = this.f906a;
            switch (i) {
                case 2131362976:
                    o87 = o87.BLACK;
                    break;
                case 2131362977:
                    o87 = o87.DARK_GRAY;
                    break;
                case 2131362978:
                    o87 = o87.LIGHT_GRAY;
                    break;
                case 2131362979:
                    o87 = o87.WHITE;
                    break;
                default:
                    return;
            }
            ea7.l = o87;
            gc7 e = hc7.c.e(this.f906a);
            if (e != null) {
                e.C(this.f906a.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ea7 b;

        @DexIgnore
        public d(ea7 ea7) {
            this.b = ea7;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.U6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ja7.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ea7 f907a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(ea7 ea7) {
            this.f907a = ea7;
        }

        @DexIgnore
        @Override // com.fossil.ja7.b
        public void a(String str) {
            pq7.c(str, "text");
            s87.c cVar = new s87.c(str, this.f907a.k, 60.0f, this.f907a.l, null, 0, 0, 112, null);
            gc7 e = hc7.c.e(this.f907a);
            if (e != null) {
                e.b(cVar);
            }
        }

        @DexIgnore
        @Override // com.fossil.ja7.b
        public void onCancel() {
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceTextFragment";
    }

    @DexIgnore
    public final void Q6() {
        ia7 ia7 = new ia7(new a(this));
        this.i = ia7;
        if (ia7 != null) {
            ia7.m(z87.d.f());
            dh5 dh5 = this.h;
            if (dh5 != null) {
                RecyclerView recyclerView = dh5.x;
                pq7.b(recyclerView, "binding.rvTypefacePreview");
                ia7 ia72 = this.i;
                if (ia72 != null) {
                    recyclerView.setAdapter(ia72);
                } else {
                    pq7.n("adapter");
                    throw null;
                }
            } else {
                pq7.n("binding");
                throw null;
            }
        } else {
            pq7.n("adapter");
            throw null;
        }
    }

    @DexIgnore
    public final void R6() {
        LiveData<s87> i2;
        gc7 e2 = hc7.c.e(this);
        if (e2 != null && (i2 = e2.i()) != null) {
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
            i2.h(viewLifecycleOwner, new b(this));
        }
    }

    @DexIgnore
    public final void S6(Typeface typeface, o87 o87) {
        Integer num = this.j.get(o87);
        if (num != null) {
            int intValue = num.intValue();
            dh5 dh5 = this.h;
            if (dh5 != null) {
                dh5.v.check(intValue);
            } else {
                pq7.n("binding");
                throw null;
            }
        }
        if (typeface != null) {
            ia7 ia7 = this.i;
            if (ia7 != null) {
                int k2 = ia7.k(typeface);
                if (k2 != -1) {
                    dh5 dh52 = this.h;
                    if (dh52 != null) {
                        dh52.x.smoothScrollToPosition(k2);
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
            } else {
                pq7.n("adapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void T6() {
        dh5 dh5 = this.h;
        if (dh5 != null) {
            dh5.v.setOnCheckedChangeListener(new c(this));
            dh5 dh52 = this.h;
            if (dh52 != null) {
                dh52.q.setOnClickListener(new d(this));
            } else {
                pq7.n("binding");
                throw null;
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void U6() {
        if (getChildFragmentManager().Z(ja7.h.a()) == null) {
            ja7 b2 = ja7.h.b(null, new e(this));
            if (isActive()) {
                b2.show(getChildFragmentManager(), ja7.h.a());
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().a1().a(this);
        ts0 a2 = vs0.c(this).a(ga7.class);
        pq7.b(a2, "ViewModelProviders.of(th\u2026extViewModel::class.java)");
        ga7 ga7 = (ga7) a2;
        String d2 = qn5.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d2)) {
            dh5 dh5 = this.h;
            if (dh5 != null) {
                dh5.w.setBackgroundColor(Color.parseColor(d2));
            } else {
                pq7.n("binding");
                throw null;
            }
        }
        Q6();
        R6();
        T6();
        S6(this.k, this.l);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        dh5 z = dh5.z(layoutInflater, viewGroup, false);
        pq7.b(z, "WatchFaceTextFragmentBin\u2026flater, container, false)");
        this.h = z;
        if (z != null) {
            View n = z.n();
            pq7.b(n, "binding.root");
            return n;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
