package com.fossil;

import com.fossil.xx0;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j47 implements xx0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicInteger f1710a; // = new AtomicInteger(0);
    @DexIgnore
    public volatile xx0.a b;

    @DexIgnore
    public j47(String str) {
    }

    @DexIgnore
    @Override // com.fossil.xx0
    public boolean a() {
        return this.f1710a.get() == 0;
    }

    @DexIgnore
    public void b() {
        int decrementAndGet = this.f1710a.decrementAndGet();
        if (decrementAndGet == 0 && this.b != null) {
            this.b.a();
        }
        if (decrementAndGet < 0) {
            throw new IllegalArgumentException("Counter has been corrupted!");
        }
    }

    @DexIgnore
    public void c() {
        this.f1710a.getAndIncrement();
    }
}
