package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma4 extends ta4.d.AbstractC0224d.a.b.AbstractC0230d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2334a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2335a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a
        public ta4.d.AbstractC0224d.a.b.AbstractC0230d a() {
            String str = "";
            if (this.f2335a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " code";
            }
            if (this.c == null) {
                str = str + " address";
            }
            if (str.isEmpty()) {
                return new ma4(this.f2335a, this.b, this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a
        public ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a
        public ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a c(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null code");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a
        public ta4.d.AbstractC0224d.a.b.AbstractC0230d.AbstractC0231a d(String str) {
            if (str != null) {
                this.f2335a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }
    }

    @DexIgnore
    public ma4(String str, String str2, long j) {
        this.f2334a = str;
        this.b = str2;
        this.c = j;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d
    public long b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d
    public String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0230d
    public String d() {
        return this.f2334a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a.b.AbstractC0230d)) {
            return false;
        }
        ta4.d.AbstractC0224d.a.b.AbstractC0230d dVar = (ta4.d.AbstractC0224d.a.b.AbstractC0230d) obj;
        return this.f2334a.equals(dVar.d()) && this.b.equals(dVar.c()) && this.c == dVar.b();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f2334a.hashCode();
        int hashCode2 = this.b.hashCode();
        long j = this.c;
        return ((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Signal{name=" + this.f2334a + ", code=" + this.b + ", address=" + this.c + "}";
    }
}
