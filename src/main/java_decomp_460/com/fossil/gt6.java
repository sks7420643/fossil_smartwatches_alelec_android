package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt6 implements Factory<ft6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f1359a;

    @DexIgnore
    public gt6(Provider<ThemeRepository> provider) {
        this.f1359a = provider;
    }

    @DexIgnore
    public static gt6 a(Provider<ThemeRepository> provider) {
        return new gt6(provider);
    }

    @DexIgnore
    public static ft6 c(ThemeRepository themeRepository) {
        return new ft6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ft6 get() {
        return c(this.f1359a.get());
    }
}
