package com.fossil;

import com.fossil.gh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract jh4 a();

        @DexIgnore
        public abstract a b(kh4 kh4);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(String str);

        @DexIgnore
        public abstract a e(b bVar);

        @DexIgnore
        public abstract a f(String str);
    }

    @DexIgnore
    public enum b {
        OK,
        BAD_CONFIG
    }

    @DexIgnore
    public static a a() {
        return new gh4.b();
    }

    @DexIgnore
    public abstract kh4 b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract b e();

    @DexIgnore
    public abstract String f();
}
