package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.nk5;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je6 extends pv5 implements ie6 {
    @DexIgnore
    public g37<w15> g;
    @DexIgnore
    public he6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements RecyclerViewCalendar.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ je6 f1748a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(je6 je6) {
            this.f1748a = je6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            pq7.c(calendar, "calendar");
            he6 he6 = this.f1748a.h;
            if (he6 != null) {
                Date time = calendar.getTime();
                pq7.b(time, "calendar.time");
                he6.o(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.a {
        @DexIgnore
        public /* final */ /* synthetic */ je6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(je6 je6) {
            this.b = je6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
        public void k0(int i, Calendar calendar) {
            pq7.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.C;
                Date time = calendar.getTime();
                pq7.b(time, "it.time");
                pq7.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActiveTimeOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        w15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        g37<w15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            nk5.a aVar = nk5.o;
            he6 he6 = this.h;
            if (aVar.w(he6 != null ? he6.n() : null)) {
                recyclerViewCalendar.J("dianaActiveMinutesTab");
            } else {
                recyclerViewCalendar.J("hybridActiveMinutesTab");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(he6 he6) {
        pq7.c(he6, "presenter");
        this.h = he6;
    }

    @DexIgnore
    @Override // com.fossil.ie6
    public void e(TreeMap<Long, Float> treeMap) {
        w15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        pq7.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        g37<w15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.ie6
    public void g(Date date, Date date2) {
        w15 a2;
        pq7.c(date, "selectDate");
        pq7.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        g37<w15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            pq7.b(instance, "selectCalendar");
            instance.setTime(date);
            pq7.b(instance2, "startCalendar");
            instance2.setTime(lk5.V(date2));
            pq7.b(instance3, "endCalendar");
            instance3.setTime(lk5.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        w15 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onCreateView");
        w15 w15 = (w15) aq0.f(layoutInflater, 2131558494, viewGroup, false, A6());
        RecyclerViewCalendar recyclerViewCalendar = w15.q;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        w15.q.setOnCalendarMonthChanged(new a(this));
        w15.q.setOnCalendarItemClickListener(new b(this));
        this.g = new g37<>(this, w15);
        L6();
        g37<w15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onResume");
        L6();
        he6 he6 = this.h;
        if (he6 != null) {
            he6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onStop");
        he6 he6 = this.h;
        if (he6 != null) {
            he6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
