package com.fossil;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int[][] f1161a; // = {new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] b; // = {new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, 102, -1, -1}, new int[]{6, 28, 54, 80, 106, -1, -1}, new int[]{6, 32, 58, 84, 110, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, 102, 126, -1}, new int[]{6, 26, 52, 78, 104, 130, -1}, new int[]{6, 30, 56, 82, 108, 134, -1}, new int[]{6, 34, 60, 86, 112, 138, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, 102, 126, 150}, new int[]{6, 24, 50, 76, 102, 128, 154}, new int[]{6, 28, 54, 80, 106, 132, 158}, new int[]{6, 32, 58, 84, 110, 136, 162}, new int[]{6, 26, 54, 82, 110, 138, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};
    @DexIgnore
    public static /* final */ int[][] d; // = {new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};

    @DexIgnore
    public static void a(am4 am4, yn4 yn4, ao4 ao4, int i, co4 co4) throws rl4 {
        c(co4);
        d(ao4, co4);
        l(yn4, i, co4);
        s(ao4, co4);
        f(am4, i, co4);
    }

    @DexIgnore
    public static int b(int i, int i2) {
        if (i2 != 0) {
            int n = n(i2);
            int i3 = i << (n - 1);
            while (n(i3) >= n) {
                i3 ^= i2 << (n(i3) - n);
            }
            return i3;
        }
        throw new IllegalArgumentException("0 polynomial");
    }

    @DexIgnore
    public static void c(co4 co4) {
        co4.a((byte) -1);
    }

    @DexIgnore
    public static void d(ao4 ao4, co4 co4) throws rl4 {
        j(co4);
        e(co4);
        r(ao4, co4);
        k(co4);
    }

    @DexIgnore
    public static void e(co4 co4) throws rl4 {
        if (co4.b(8, co4.d() - 8) != 0) {
            co4.f(8, co4.d() - 8, 1);
            return;
        }
        throw new rl4();
    }

    @DexIgnore
    public static void f(am4 am4, int i, co4 co4) throws rl4 {
        int i2;
        int i3;
        boolean z;
        int d2 = co4.d() - 1;
        int i4 = -1;
        int i5 = 0;
        for (int e = co4.e() - 1; e > 0; e = i2 - 2) {
            if (e == 6) {
                i3 = d2;
                i2 = e - 1;
            } else {
                i3 = d2;
                i2 = e;
            }
            while (i3 >= 0 && i3 < co4.d()) {
                int i6 = 0;
                while (i6 < 2) {
                    int i7 = i2 - i6;
                    if (o(co4.b(i7, i3))) {
                        if (i5 < am4.n()) {
                            z = am4.l(i5);
                            i5++;
                        } else {
                            z = false;
                        }
                        if (i != -1 && eo4.f(i, i7, i3)) {
                            z = !z;
                        }
                        co4.g(i7, i3, z);
                    }
                    i6++;
                    i5 = i5;
                }
                i3 += i4;
            }
            i4 = -i4;
            d2 = i3 + i4;
        }
        if (i5 != am4.n()) {
            throw new rl4("Not all bits consumed: " + i5 + '/' + am4.n());
        }
    }

    @DexIgnore
    public static void g(int i, int i2, co4 co4) throws rl4 {
        for (int i3 = 0; i3 < 8; i3++) {
            int i4 = i + i3;
            if (o(co4.b(i4, i2))) {
                co4.f(i4, i2, 0);
            } else {
                throw new rl4();
            }
        }
    }

    @DexIgnore
    public static void h(int i, int i2, co4 co4) {
        for (int i3 = 0; i3 < 5; i3++) {
            for (int i4 = 0; i4 < 5; i4++) {
                co4.f(i + i4, i2 + i3, b[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void i(int i, int i2, co4 co4) {
        for (int i3 = 0; i3 < 7; i3++) {
            for (int i4 = 0; i4 < 7; i4++) {
                co4.f(i + i4, i2 + i3, f1161a[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void j(co4 co4) throws rl4 {
        int length = f1161a[0].length;
        i(0, 0, co4);
        i(co4.e() - length, 0, co4);
        i(0, co4.e() - length, co4);
        g(0, 7, co4);
        g(co4.e() - 8, 7, co4);
        g(0, co4.e() - 8, co4);
        m(7, 0, co4);
        m((co4.d() - 7) - 1, 0, co4);
        m(7, co4.d() - 7, co4);
    }

    @DexIgnore
    public static void k(co4 co4) {
        int i = 8;
        while (i < co4.e() - 8) {
            int i2 = i + 1;
            int i3 = i2 % 2;
            if (o(co4.b(i, 6))) {
                co4.f(i, 6, i3);
            }
            if (o(co4.b(6, i))) {
                co4.f(6, i, i3);
            }
            i = i2;
        }
    }

    @DexIgnore
    public static void l(yn4 yn4, int i, co4 co4) throws rl4 {
        am4 am4 = new am4();
        p(yn4, i, am4);
        for (int i2 = 0; i2 < am4.n(); i2++) {
            boolean l = am4.l((am4.n() - 1) - i2);
            int[][] iArr = d;
            co4.g(iArr[i2][0], iArr[i2][1], l);
            if (i2 < 8) {
                co4.g((co4.e() - i2) - 1, 8, l);
            } else {
                co4.g(8, (co4.d() - 7) + (i2 - 8), l);
            }
        }
    }

    @DexIgnore
    public static void m(int i, int i2, co4 co4) throws rl4 {
        for (int i3 = 0; i3 < 7; i3++) {
            int i4 = i2 + i3;
            if (o(co4.b(i, i4))) {
                co4.f(i, i4, 0);
            } else {
                throw new rl4();
            }
        }
    }

    @DexIgnore
    public static int n(int i) {
        return 32 - Integer.numberOfLeadingZeros(i);
    }

    @DexIgnore
    public static boolean o(int i) {
        return i == -1;
    }

    @DexIgnore
    public static void p(yn4 yn4, int i, am4 am4) throws rl4 {
        if (go4.b(i)) {
            int bits = (yn4.getBits() << 3) | i;
            am4.g(bits, 5);
            am4.g(b(bits, FailureCode.FAILED_TO_START_STREAMING), 10);
            am4 am42 = new am4();
            am42.g(21522, 15);
            am4.r(am42);
            if (am4.n() != 15) {
                throw new rl4("should not happen but we got: " + am4.n());
            }
            return;
        }
        throw new rl4("Invalid mask pattern");
    }

    @DexIgnore
    public static void q(ao4 ao4, am4 am4) throws rl4 {
        am4.g(ao4.f(), 6);
        am4.g(b(ao4.f(), 7973), 12);
        if (am4.n() != 18) {
            throw new rl4("should not happen but we got: " + am4.n());
        }
    }

    @DexIgnore
    public static void r(ao4 ao4, co4 co4) {
        if (ao4.f() >= 2) {
            int f = ao4.f() - 1;
            int[][] iArr = c;
            int[] iArr2 = iArr[f];
            int length = iArr[f].length;
            for (int i = 0; i < length; i++) {
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = iArr2[i];
                    int i4 = iArr2[i2];
                    if (!(i4 == -1 || i3 == -1 || !o(co4.b(i4, i3)))) {
                        h(i4 - 2, i3 - 2, co4);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void s(ao4 ao4, co4 co4) throws rl4 {
        if (ao4.f() >= 7) {
            am4 am4 = new am4();
            q(ao4, am4);
            int i = 17;
            for (int i2 = 0; i2 < 6; i2++) {
                for (int i3 = 0; i3 < 3; i3++) {
                    boolean l = am4.l(i);
                    i--;
                    co4.g(i2, (co4.d() - 11) + i3, l);
                    co4.g((co4.d() - 11) + i3, i2, l);
                }
            }
        }
    }
}
