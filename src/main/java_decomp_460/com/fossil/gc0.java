package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ gc0 f1287a; // = new gc0();

    @DexIgnore
    public final hc0 a(byte[] bArr) {
        try {
            iw1 iw1 = (iw1) ga.d.f(bArr);
            if (iw1 instanceof ow1) {
                return new pw1(iw1.h(), iw1.g(), iw1.f(), iw1.b(), iw1.d(), iw1.e(), iw1.c(), iw1.a(), iw1.i());
            }
            if (iw1 instanceof tw1) {
                return new uw1(iw1.h(), iw1.g(), iw1.f(), iw1.b(), iw1.d(), iw1.e(), iw1.c(), iw1.a(), iw1.i());
            }
            return null;
        } catch (IllegalArgumentException e) {
        }
    }
}
