package com.fossil;

import android.annotation.TargetApi;
import android.media.AudioAttributes;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@TargetApi(21)
public class et0 implements dt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public AudioAttributes f983a;
    @DexIgnore
    public int b; // = -1;

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof et0)) {
            return false;
        }
        return this.f983a.equals(((et0) obj).f983a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f983a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "AudioAttributesCompat: audioattributes=" + this.f983a;
    }
}
