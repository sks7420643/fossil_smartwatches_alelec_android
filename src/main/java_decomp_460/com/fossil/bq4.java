package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq4 implements Factory<xn5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f483a;

    @DexIgnore
    public bq4(uo4 uo4) {
        this.f483a = uo4;
    }

    @DexIgnore
    public static bq4 a(uo4 uo4) {
        return new bq4(uo4);
    }

    @DexIgnore
    public static xn5 c(uo4 uo4) {
        xn5 I = uo4.I();
        lk7.c(I, "Cannot return null from a non-@Nullable @Provides method");
        return I;
    }

    @DexIgnore
    /* renamed from: b */
    public xn5 get() {
        return c(this.f483a);
    }
}
