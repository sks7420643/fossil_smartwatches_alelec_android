package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Excluder f4476a; // = Excluder.h;
    @DexIgnore
    public nj4 b; // = nj4.DEFAULT;
    @DexIgnore
    public yi4 c; // = xi4.IDENTITY;
    @DexIgnore
    public /* final */ Map<Type, aj4<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ List<oj4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<oj4> f; // = new ArrayList();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i; // = 2;
    @DexIgnore
    public int j; // = 2;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public zi4 a(vi4 vi4) {
        this.f4476a = this.f4476a.t(vi4, false, true);
        return this;
    }

    @DexIgnore
    public zi4 b(vi4 vi4) {
        this.f4476a = this.f4476a.t(vi4, true, false);
        return this;
    }

    @DexIgnore
    public final void c(String str, int i2, int i3, List<oj4> list) {
        ui4 ui4;
        ui4 ui42;
        ui4 ui43;
        if (str != null && !"".equals(str.trim())) {
            ui4 = new ui4(Date.class, str);
            ui42 = new ui4(Timestamp.class, str);
            ui43 = new ui4(java.sql.Date.class, str);
        } else if (i2 != 2 && i3 != 2) {
            ui4 = new ui4(Date.class, i2, i3);
            ui42 = new ui4(Timestamp.class, i2, i3);
            ui43 = new ui4(java.sql.Date.class, i2, i3);
        } else {
            return;
        }
        list.add(TypeAdapters.b(Date.class, ui4));
        list.add(TypeAdapters.b(Timestamp.class, ui42));
        list.add(TypeAdapters.b(java.sql.Date.class, ui43));
    }

    @DexIgnore
    public Gson d() {
        List<oj4> arrayList = new ArrayList<>(this.e.size() + this.f.size() + 3);
        arrayList.addAll(this.e);
        Collections.reverse(arrayList);
        ArrayList arrayList2 = new ArrayList(this.f);
        Collections.reverse(arrayList2);
        arrayList.addAll(arrayList2);
        c(this.h, this.i, this.j, arrayList);
        return new Gson(this.f4476a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.p, this.l, this.b, this.h, this.i, this.j, this.e, this.f, arrayList);
    }

    @DexIgnore
    public zi4 e(int... iArr) {
        this.f4476a = this.f4476a.u(iArr);
        return this;
    }

    @DexIgnore
    public zi4 f(Type type, Object obj) {
        boolean z = obj instanceof lj4;
        uj4.a(z || (obj instanceof dj4) || (obj instanceof aj4) || (obj instanceof TypeAdapter));
        if (obj instanceof aj4) {
            this.d.put(type, (aj4) obj);
        }
        if (z || (obj instanceof dj4)) {
            this.e.add(TreeTypeAdapter.b(TypeToken.get(type), obj));
        }
        if (obj instanceof TypeAdapter) {
            this.e.add(TypeAdapters.a(TypeToken.get(type), (TypeAdapter) obj));
        }
        return this;
    }

    @DexIgnore
    public zi4 g(String str) {
        this.h = str;
        return this;
    }

    @DexIgnore
    public zi4 h(xi4 xi4) {
        this.c = xi4;
        return this;
    }
}
