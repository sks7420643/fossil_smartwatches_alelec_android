package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Object d;
    @DexIgnore
    public /* final */ /* synthetic */ long e;
    @DexIgnore
    public /* final */ /* synthetic */ un3 f;

    @DexIgnore
    public zn3(un3 un3, String str, String str2, Object obj, long j) {
        this.f = un3;
        this.b = str;
        this.c = str2;
        this.d = obj;
        this.e = j;
    }

    @DexIgnore
    public final void run() {
        this.f.S(this.b, this.c, this.d, this.e);
    }
}
