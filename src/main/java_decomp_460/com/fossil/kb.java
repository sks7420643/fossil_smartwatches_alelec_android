package com.fossil;

import java.nio.ByteBuffer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kb extends ox1 {
    @DexIgnore
    public static /* final */ ib e; // = new ib(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public kb(byte b2, byte b3) {
        this(ByteBuffer.allocate(2).put(b2).put(b3).getShort(0));
    }

    @DexIgnore
    public kb(short s) {
        this.d = (short) s;
        ByteBuffer putShort = ByteBuffer.allocate(2).putShort(this.d);
        pq7.b(putShort, "ByteBuffer.allocate(2).putShort(fileHandle)");
        this.b = putShort.get(0);
        this.c = putShort.get(1);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.J2, hy1.j(this.b, null, 1, null)), jd0.t4, hy1.j(this.c, null, 1, null)), jd0.A0, hy1.l(this.d, null, 1, null)), jd0.g4, e.a(this.d));
    }
}
