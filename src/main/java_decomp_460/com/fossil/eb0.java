package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb0 implements Parcelable.Creator<fb0> {
    @DexIgnore
    public /* synthetic */ eb0(kq7 kq7) {
    }

    @DexIgnore
    public fb0 a(Parcel parcel) {
        return new fb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public fb0 createFromParcel(Parcel parcel) {
        return new fb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public fb0[] newArray(int i) {
        return new fb0[i];
    }
}
