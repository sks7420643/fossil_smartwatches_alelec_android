package com.fossil;

import android.os.RemoteException;
import com.fossil.l72;
import com.fossil.p72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma2 extends v92<Boolean> {
    @DexIgnore
    public /* final */ p72.a<?> c;

    @DexIgnore
    public ma2(p72.a<?> aVar, ot3<Boolean> ot3) {
        super(4, ot3);
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final /* bridge */ /* synthetic */ void d(a82 a82, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final b62[] g(l72.a<?> aVar) {
        u92 u92 = aVar.A().get(this.c);
        if (u92 == null) {
            return null;
        }
        return u92.f3556a.c();
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final boolean h(l72.a<?> aVar) {
        u92 u92 = aVar.A().get(this.c);
        return u92 != null && u92.f3556a.e();
    }

    @DexIgnore
    @Override // com.fossil.v92
    public final void i(l72.a<?> aVar) throws RemoteException {
        u92 remove = aVar.A().remove(this.c);
        if (remove != null) {
            remove.b.b(aVar.R(), this.b);
            remove.f3556a.a();
            return;
        }
        this.b.e((T) Boolean.FALSE);
    }
}
