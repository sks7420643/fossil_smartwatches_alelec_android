package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mb {
    @DexIgnore
    public /* synthetic */ mb(kq7 kq7) {
    }

    @DexIgnore
    public final ob a(byte b) {
        ob[] values = ob.values();
        for (ob obVar : values) {
            if (obVar.c == b) {
                return obVar;
            }
        }
        return null;
    }

    @DexIgnore
    public final ob b(short s) {
        ob[] values = ob.values();
        for (ob obVar : values) {
            short s2 = obVar.b;
            if (((short) (s | s2)) == s2) {
                return obVar;
            }
        }
        return null;
    }
}
