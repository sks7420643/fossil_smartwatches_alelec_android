package com.fossil;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.xw4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw4 extends pv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public g37<f35> g;
    @DexIgnore
    public vw4 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public xw4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return sw4.s;
        }

        @DexIgnore
        public final sw4 b(String str) {
            sw4 sw4 = new sw4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_history_id_extra", str);
            sw4.setArguments(bundle);
            return sw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sw4 b;

        @DexIgnore
        public b(sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            sw4.Q6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ sw4 b;

        @DexIgnore
        public c(sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            sw4.Q6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextView.OnEditorActionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f35 f3321a;
        @DexIgnore
        public /* final */ /* synthetic */ sw4 b;

        @DexIgnore
        public d(f35 f35, sw4 sw4) {
            this.f3321a = f35;
            this.b = sw4;
        }

        @DexIgnore
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            pq7.c(textView, "v");
            if (i != 3) {
                return false;
            }
            sw4 sw4 = this.b;
            FlexibleEditText flexibleEditText = this.f3321a.s;
            pq7.b(flexibleEditText, "etSearch");
            sw4.U6(flexibleEditText);
            sw4.Q6(this.b).s();
            sw4.Q6(this.b).u(textView.getText().toString());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sw4 b;

        @DexIgnore
        public e(sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity;
            if (!(this.b.k == null || !this.b.l || (activity = this.b.getActivity()) == null)) {
                activity.setResult(-1);
            }
            FragmentActivity activity2 = this.b.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements xw4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sw4 f3322a;

        @DexIgnore
        public f(sw4 sw4) {
            this.f3322a = sw4;
        }

        @DexIgnore
        @Override // com.fossil.xw4.b
        public void a(xs4 xs4, int i) {
            pq7.c(xs4, "friend");
            sw4.Q6(this.f3322a).v(xs4, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ g b; // = new g();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                xr4.f4164a.f(PortfolioApp.h0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sw4 f3323a;

        @DexIgnore
        public h(sw4 sw4) {
            this.f3323a = sw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "it");
            if (bool.booleanValue()) {
                this.f3323a.b();
            } else {
                this.f3323a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<kz4<List<xs4>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sw4 f3324a;

        @DexIgnore
        public i(sw4 sw4) {
            this.f3324a = sw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(kz4<List<xs4>> kz4) {
            FlexibleTextView flexibleTextView;
            ProgressBar progressBar;
            f35 f35 = (f35) sw4.N6(this.f3324a).a();
            if (!(f35 == null || (progressBar = f35.x) == null)) {
                progressBar.setVisibility(8);
            }
            f35 f352 = (f35) sw4.N6(this.f3324a).a();
            if (!(f352 == null || (flexibleTextView = f352.u) == null)) {
                flexibleTextView.setVisibility(8);
            }
            List<xs4> c = kz4.c();
            if (c == null) {
                ServerError a2 = kz4.a();
                f35 f353 = (f35) sw4.N6(this.f3324a).a();
                if (f353 != null) {
                    FlexibleTextView flexibleTextView2 = f353.t;
                    pq7.b(flexibleTextView2, "ftvNotFound");
                    flexibleTextView2.setText(String.valueOf(a2));
                }
            } else if (c.isEmpty()) {
                f35 f354 = (f35) sw4.N6(this.f3324a).a();
                if (f354 != null) {
                    if (this.f3324a.k == null) {
                        FlexibleEditText flexibleEditText = f354.s;
                        pq7.b(flexibleEditText, "etSearch");
                        String valueOf = String.valueOf(flexibleEditText.getText());
                        hr7 hr7 = hr7.f1520a;
                        String c2 = um5.c(PortfolioApp.h0.c(), 2131886286);
                        pq7.b(c2, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                        String format = String.format(c2, Arrays.copyOf(new Object[]{valueOf}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        FlexibleTextView flexibleTextView3 = f354.t;
                        pq7.b(flexibleTextView3, "ftvNotFound");
                        flexibleTextView3.setText(format);
                    } else {
                        FlexibleTextView flexibleTextView4 = f354.t;
                        pq7.b(flexibleTextView4, "ftvNotFound");
                        flexibleTextView4.setText("");
                    }
                    FlexibleTextView flexibleTextView5 = f354.t;
                    pq7.b(flexibleTextView5, "ftvNotFound");
                    flexibleTextView5.setVisibility(0);
                }
            } else {
                xw4 xw4 = this.f3324a.j;
                if (xw4 != null) {
                    xw4.l(c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<cl7<? extends kz4<Boolean>, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sw4 f3325a;

        @DexIgnore
        public j(sw4 sw4) {
            this.f3325a = sw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<kz4<Boolean>, Integer> cl7) {
            String str;
            Boolean c = cl7.getFirst().c();
            int intValue = cl7.getSecond().intValue();
            if (c != null) {
                sw4 sw4 = this.f3325a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886332);
                pq7.b(c2, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                sw4.T6(c2);
                xw4 xw4 = this.f3325a.j;
                if (xw4 != null) {
                    xw4.m(intValue);
                    return;
                }
                return;
            }
            ServerError a2 = cl7.getFirst().a();
            Integer code = a2 != null ? a2.getCode() : null;
            if (code != null && code.intValue() == 404001) {
                this.f3325a.l = true;
                xw4 xw42 = this.f3325a.j;
                if (xw42 != null) {
                    xw42.m(intValue);
                }
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f3325a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886235);
                pq7.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886299);
                pq7.b(c4, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
                s37.E(childFragmentManager, c3, c4);
            } else if (code != null && code.intValue() == 409001) {
                sw4 sw42 = this.f3325a;
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886332);
                pq7.b(c5, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                sw42.T6(c5);
                xw4 xw43 = this.f3325a.j;
                if (xw43 != null) {
                    xw43.m(intValue);
                }
            } else {
                jl5 jl5 = jl5.b;
                ServerError a3 = cl7.getFirst().a();
                Integer code2 = a3 != null ? a3.getCode() : null;
                ServerError a4 = cl7.getFirst().a();
                if (a4 == null || (str = a4.getMessage()) == null) {
                    str = "";
                }
                String e = jl5.e(code2, str);
                s37 s372 = s37.c;
                FragmentManager childFragmentManager2 = this.f3325a.getChildFragmentManager();
                pq7.b(childFragmentManager2, "childFragmentManager");
                s372.g(code, e, childFragmentManager2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<yy4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sw4 f3326a;

        @DexIgnore
        public k(sw4 sw4) {
            this.f3326a = sw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yy4 yy4) {
            f35 f35;
            if (yy4 != null) {
                int i = tw4.f3478a[yy4.ordinal()];
                if (i == 1) {
                    f35 f352 = (f35) sw4.N6(this.f3326a).a();
                    if (f352 != null) {
                        f352.s.setText("");
                        ProgressBar progressBar = f352.x;
                        pq7.b(progressBar, "prg");
                        progressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView = f352.u;
                        pq7.b(flexibleTextView, "ftvSearch");
                        flexibleTextView.setVisibility(8);
                    }
                    xw4 xw4 = this.f3326a.j;
                    if (xw4 != null) {
                        xw4.h();
                    }
                } else if (i == 2) {
                    f35 f353 = (f35) sw4.N6(this.f3326a).a();
                    if (f353 != null) {
                        FlexibleTextView flexibleTextView2 = f353.t;
                        pq7.b(flexibleTextView2, "ftvNotFound");
                        flexibleTextView2.setVisibility(8);
                        FlexibleEditText flexibleEditText = f353.s;
                        pq7.b(flexibleEditText, "etSearch");
                        if (TextUtils.isEmpty(String.valueOf(flexibleEditText.getText()))) {
                            RTLImageView rTLImageView = f353.q;
                            pq7.b(rTLImageView, "btnSearchClear");
                            rTLImageView.setVisibility(8);
                            return;
                        }
                        RTLImageView rTLImageView2 = f353.q;
                        pq7.b(rTLImageView2, "btnSearchClear");
                        rTLImageView2.setVisibility(0);
                    }
                } else if (i == 3 && (f35 = (f35) sw4.N6(this.f3326a).a()) != null) {
                    FlexibleTextView flexibleTextView3 = f35.t;
                    pq7.b(flexibleTextView3, "ftvNotFound");
                    flexibleTextView3.setVisibility(8);
                    ProgressBar progressBar2 = f35.x;
                    pq7.b(progressBar2, "prg");
                    progressBar2.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = f35.u;
                    pq7.b(flexibleTextView4, "ftvSearch");
                    flexibleTextView4.setVisibility(0);
                    xw4 xw42 = this.f3326a.j;
                    if (xw42 != null) {
                        xw42.h();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<cl7<? extends String, ? extends String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ l f3327a; // = new l();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<String, String> cl7) {
            xr4.f4164a.i(cl7.getFirst(), cl7.getSecond(), PortfolioApp.h0.c());
        }
    }

    /*
    static {
        String simpleName = sw4.class.getSimpleName();
        pq7.b(simpleName, "BCFindFriendsFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 N6(sw4 sw4) {
        g37<f35> g37 = sw4.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ vw4 Q6(sw4 sw4) {
        vw4 vw4 = sw4.h;
        if (vw4 != null) {
            return vw4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T6(String str) {
        LayoutInflater layoutInflater = getLayoutInflater();
        pq7.b(layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558835, activity != null ? (ViewGroup) activity.findViewById(2131362178) : null);
        View findViewById = inflate.findViewById(2131362386);
        pq7.b(findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void U6(View view) {
        Object systemService = view.getContext().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).hideSoftInputFromWindow(view.getWindowToken(), 0);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public final void V6() {
        g37<f35> g37 = this.g;
        if (g37 != null) {
            f35 a2 = g37.a();
            if (a2 != null) {
                if (this.k != null) {
                    ConstraintLayout constraintLayout = a2.r;
                    pq7.b(constraintLayout, "cSearch");
                    constraintLayout.setVisibility(8);
                    View view = a2.w;
                    pq7.b(view, "line");
                    view.setVisibility(0);
                }
                RTLImageView rTLImageView = a2.q;
                pq7.b(rTLImageView, "btnSearchClear");
                rTLImageView.setVisibility(8);
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnFocusChangeListener(g.b);
                a2.s.addTextChangedListener(new c(this));
                a2.s.setOnEditorActionListener(new d(a2, this));
                a2.v.setOnClickListener(new e(this));
                xw4 xw4 = new xw4();
                this.j = xw4;
                xw4.k(new f(this));
                RecyclerView recyclerView = a2.z;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.setAdapter(this.j);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        vw4 vw4 = this.h;
        if (vw4 != null) {
            vw4.m().h(getViewLifecycleOwner(), new h(this));
            vw4 vw42 = this.h;
            if (vw42 != null) {
                vw42.n().h(getViewLifecycleOwner(), new i(this));
                vw4 vw43 = this.h;
                if (vw43 != null) {
                    vw43.p().h(getViewLifecycleOwner(), new j(this));
                    vw4 vw44 = this.h;
                    if (vw44 != null) {
                        vw44.o().h(getViewLifecycleOwner(), new k(this));
                        vw4 vw45 = this.h;
                        if (vw45 != null) {
                            vw45.l().h(getViewLifecycleOwner(), l.f3327a);
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.k = arguments != null ? arguments.getString("challenge_history_id_extra") : null;
        PortfolioApp.h0.c().M().T().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(vw4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ndsViewModel::class.java)");
            this.h = (vw4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        f35 f35 = (f35) aq0.f(layoutInflater, 2131558512, viewGroup, false, A6());
        this.g = new g37<>(this, f35);
        pq7.b(f35, "binding");
        return f35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ck5 g2 = ck5.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_friend_add", activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        String str = this.k;
        if (str != null) {
            vw4 vw4 = this.h;
            if (vw4 == null) {
                pq7.n("viewModel");
                throw null;
            } else if (str != null) {
                vw4.q(str);
            } else {
                pq7.i();
                throw null;
            }
        }
        V6();
        W6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
