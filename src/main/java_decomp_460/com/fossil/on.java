package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ix1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on extends zj {
    @DexIgnore
    public /* final */ byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ on(k5 k5Var, i60 i60, byte[] bArr, String str, int i) {
        super(k5Var, i60, yp.C0, true, ke.b.b(k5Var.x, ob.UI_ENCRYPTED_FILE), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, 64);
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(g80.k(super.C(), jd0.F0, Integer.valueOf(this.T.length)), jd0.U0, Long.valueOf(ix1.f1688a.b(this.T, ix1.a.CRC32)));
    }
}
