package com.fossil;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw5 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<cl7<Complication, String>> f1384a;
    @DexIgnore
    public List<cl7<Complication, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public c e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gw5 gw5, View view) {
            super(view);
            pq7.c(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f1385a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gw5 gw5, View view) {
            super(gw5, view);
            pq7.c(view, "itemView");
            View findViewById = view.findViewById(2131363383);
            if (findViewById != null) {
                this.f1385a = (FlexibleTextView) findViewById;
                String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(d)) {
                    this.f1385a.setBackgroundColor(Color.parseColor(d));
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.f1385a;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(Complication complication);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ CustomizeWidget f1386a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ gw5 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e b;

            @DexIgnore
            public a(e eVar) {
                this.b = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Complication a2 = this.b.a();
                if (a2 != null) {
                    d dVar = this.b.f.d;
                    if (dVar != null) {
                        dVar.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(gw5.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(gw5.f, "itemClick(), complication tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gw5 gw5, View view) {
            super(gw5, view);
            pq7.c(view, "itemView");
            this.f = gw5;
            View findViewById = view.findViewById(2131363547);
            pq7.b(findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.f1386a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363370);
            pq7.b(findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131363263);
            if (findViewById3 != null) {
                this.c = (FlexibleTextView) findViewById3;
                View findViewById4 = view.findViewById(2131363010);
                if (findViewById4 != null) {
                    this.d = findViewById4;
                    String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                    if (!TextUtils.isEmpty(d2)) {
                        this.d.setBackgroundColor(Color.parseColor(d2));
                    }
                    view.setOnClickListener(new a(this));
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final Complication a() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.f1386a;
        }

        @DexIgnore
        public final void e(Complication complication) {
            this.e = complication;
        }
    }

    /*
    static {
        String name = gw5.class.getName();
        pq7.b(name, "ComplicationSearchAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<cl7<Complication, String>> list = this.f1384a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return (this.f1384a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final SpannableString i(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                jt7 jt7 = new jt7(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    ts7<ht7> findAll$default = jt7.findAll$default(jt7, lowerCase, 0, 2, null);
                    SpannableString spannableString = new SpannableString(str);
                    for (ht7 ht7 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), ht7.a().h().intValue(), ht7.a().h().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    /* renamed from: j */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (!(aVar instanceof b)) {
            e eVar = (e) aVar;
            List<cl7<Complication, String>> list = this.f1384a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty(this.b.get(i2).getSecond())) {
                        eVar.b().setVisibility(0);
                        eVar.b().setText(um5.c(PortfolioApp.h0.c(), 2131886516));
                    } else {
                        eVar.b().setVisibility(8);
                    }
                    eVar.d().O(this.b.get(i2).getFirst().getComplicationId());
                    eVar.c().setText(um5.d(PortfolioApp.h0.c(), this.b.get(i2).getFirst().getNameKey(), this.b.get(i2).getFirst().getName()));
                    eVar.e(this.b.get(i2).getFirst());
                    return;
                }
                eVar.e(null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty(list.get(i).getSecond())) {
                    eVar.b().setVisibility(0);
                    eVar.b().setText(um5.c(PortfolioApp.h0.c(), 2131886516));
                } else {
                    eVar.b().setVisibility(8);
                }
                eVar.d().O(list.get(i).getFirst().getComplicationId());
                String d2 = um5.d(PortfolioApp.h0.c(), list.get(i).getFirst().getNameKey(), list.get(i).getFirst().getName());
                FlexibleTextView c2 = eVar.c();
                pq7.b(d2, "name");
                c2.setText(i(d2, this.c));
                eVar.e(list.get(i).getFirst());
            } else {
                eVar.e(null);
            }
        } else if (this.b.isEmpty()) {
            ((b) aVar).a().setVisibility(4);
        } else {
            ((b) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    /* renamed from: k */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558671, viewGroup, false);
            pq7.b(inflate, "view");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558670, viewGroup, false);
        pq7.b(inflate2, "view");
        return new e(this, inflate2);
    }

    @DexIgnore
    public final void l(List<cl7<Complication, String>> list) {
        pq7.c(list, "value");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(List<cl7<Complication, String>> list) {
        c cVar;
        this.f1384a = list;
        if (!(list == null || !list.isEmpty() || (cVar = this.e) == null)) {
            cVar.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(String str) {
        pq7.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final void o(c cVar) {
        pq7.c(cVar, "listener");
        this.e = cVar;
    }

    @DexIgnore
    public final void p(d dVar) {
        pq7.c(dVar, "listener");
        this.d = dVar;
    }
}
