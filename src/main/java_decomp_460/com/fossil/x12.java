package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class x12 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b22 f4030a;
    @DexIgnore
    public /* final */ h02 b;

    @DexIgnore
    public x12(b22 b22, h02 h02) {
        this.f4030a = b22;
        this.b = h02;
    }

    @DexIgnore
    public static s32.a b(b22 b22, h02 h02) {
        return new x12(b22, h02);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return this.f4030a.c.q(this.b);
    }
}
