package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zs2 f1125a;

    @DexIgnore
    public interface a extends sn3 {
    }

    @DexIgnore
    public fg3(zs2 zs2) {
        this.f1125a = zs2;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        this.f1125a.s(str, str2, bundle);
    }

    @DexIgnore
    public void b(a aVar) {
        this.f1125a.n(aVar);
    }

    @DexIgnore
    public void c(String str, String str2, Object obj) {
        this.f1125a.u(str, str2, obj);
    }

    @DexIgnore
    public final void d(boolean z) {
        this.f1125a.F(z);
    }
}
