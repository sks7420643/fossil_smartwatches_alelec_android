package com.fossil;

import android.os.Bundle;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il3 extends in3 {
    @DexIgnore
    public static /* final */ AtomicReference<String[]> c; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> d; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> e; // = new AtomicReference<>();

    @DexIgnore
    public il3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public static String w(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        int i = 0;
        rc2.k(strArr);
        rc2.k(strArr2);
        rc2.k(atomicReference);
        rc2.a(strArr.length == strArr2.length);
        while (true) {
            if (i >= strArr.length) {
                break;
            } else if (kr3.z0(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str = strArr3[i];
                }
            } else {
                i++;
            }
        }
        return str;
    }

    @DexIgnore
    public final boolean A() {
        b();
        return this.f1780a.I() && this.f1780a.d().B(3);
    }

    @DexIgnore
    @Override // com.fossil.in3
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final String t(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!A()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Bundle[{");
        for (String str : bundle.keySet()) {
            if (sb.length() != 8) {
                sb.append(", ");
            }
            sb.append(y(str));
            sb.append(SimpleComparison.EQUAL_TO_OPERATION);
            if (!s53.a() || !m().s(xg3.E0)) {
                sb.append(bundle.get(str));
            } else {
                Object obj = bundle.get(str);
                sb.append(obj instanceof Bundle ? x(new Object[]{obj}) : obj instanceof Object[] ? x((Object[]) obj) : obj instanceof ArrayList ? x(((ArrayList) obj).toArray()) : String.valueOf(obj));
            }
        }
        sb.append("}]");
        return sb.toString();
    }

    @DexIgnore
    public final String u(vg3 vg3) {
        String str = null;
        if (vg3 == null) {
            return null;
        }
        if (!A()) {
            return vg3.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("origin=");
        sb.append(vg3.d);
        sb.append(",name=");
        sb.append(v(vg3.b));
        sb.append(",params=");
        ug3 ug3 = vg3.c;
        if (ug3 != null) {
            str = !A() ? ug3.toString() : t(ug3.k());
        }
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public final String v(String str) {
        if (str == null) {
            return null;
        }
        return A() ? w(str, on3.b, on3.f2699a, c) : str;
    }

    @DexIgnore
    public final String x(Object[] objArr) {
        if (objArr == null) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object obj : objArr) {
            String t = obj instanceof Bundle ? t((Bundle) obj) : String.valueOf(obj);
            if (t != null) {
                if (sb.length() != 1) {
                    sb.append(", ");
                }
                sb.append(t);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final String y(String str) {
        if (str == null) {
            return null;
        }
        return A() ? w(str, nn3.b, nn3.f2549a, d) : str;
    }

    @DexIgnore
    public final String z(String str) {
        if (str == null) {
            return null;
        }
        if (!A()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return w(str, qn3.b, qn3.f2999a, e);
        }
        return "experiment_id(" + str + ")";
    }
}
