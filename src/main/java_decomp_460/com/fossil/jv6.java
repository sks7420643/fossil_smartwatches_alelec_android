package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.hq4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv6 extends pv5 {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public jg5 g;
    @DexIgnore
    public lv6 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final jv6 a() {
            return new jv6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jv6 b;

        @DexIgnore
        public b(jv6 jv6) {
            this.b = jv6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jv6.K6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jv6 f1817a;

        @DexIgnore
        public c(jv6 jv6) {
            this.f1817a = jv6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar.a()) {
                this.f1817a.H6("");
            } else {
                this.f1817a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<hq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jv6 f1818a;

        @DexIgnore
        public d(jv6 jv6) {
            this.f1818a = jv6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.a aVar) {
            int a2 = aVar.a();
            if (a2 == 0) {
                FragmentActivity activity = this.f1818a.getActivity();
                if (activity != null) {
                    HomeActivity.a aVar2 = HomeActivity.B;
                    pq7.b(activity, "it");
                    HomeActivity.a.b(aVar2, activity, null, 2, null);
                    activity.finish();
                }
            } else if (a2 == 1) {
                String c = um5.c(PortfolioApp.h0.c(), 2131886795);
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886794);
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f1818a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                pq7.b(c, "title");
                pq7.b(c2, "des");
                s37.E(childFragmentManager, c, c2);
            } else if (a2 == 2) {
                s37 s372 = s37.c;
                FragmentManager childFragmentManager2 = this.f1818a.getChildFragmentManager();
                pq7.b(childFragmentManager2, "childFragmentManager");
                s372.C(childFragmentManager2);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ lv6 K6(jv6 jv6) {
        lv6 lv6 = jv6.h;
        if (lv6 != null) {
            return lv6;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void L6() {
        jg5 jg5 = this.g;
        if (jg5 != null) {
            jg5.q.setOnClickListener(new b(this));
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void M6() {
        lv6 lv6 = this.h;
        if (lv6 != null) {
            lv6.j().h(getViewLifecycleOwner(), new c(this));
            lv6 lv62 = this.h;
            if (lv62 != null) {
                lv62.h().h(getViewLifecycleOwner(), new d(this));
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        jg5 z = jg5.z(layoutInflater);
        pq7.b(z, "MigrationFragmentBinding.inflate(inflater)");
        this.g = z;
        if (z != null) {
            return z.n();
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.h0.c().M().v1().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(lv6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.h = (lv6) a2;
            L6();
            M6();
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
