package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1420a;
    @DexIgnore
    public static /* final */ h37 b; // = new h37();

    /*
    static {
        String simpleName = h37.class.getSimpleName();
        pq7.b(simpleName, "BackendURLUtils::class.java.simpleName");
        f1420a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return b(PortfolioApp.h0.c(), i);
    }

    @DexIgnore
    public final String b(PortfolioApp portfolioApp, int i) {
        pq7.c(portfolioApp, "app");
        int u = portfolioApp.k0().u();
        boolean z = false;
        if (u == 0 ? !vt7.j("release", "release", true) : u == 1) {
            z = true;
        }
        FLogger.INSTANCE.getLocal().d(f1420a, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        switch (i) {
            case 0:
                return z ? rq4.G.l() : rq4.G.i();
            case 1:
                return z ? rq4.G.p() : rq4.G.o();
            case 2:
                return z ? rq4.G.u() : rq4.G.t();
            case 3:
            default:
                return "";
            case 4:
                return z ? rq4.G.g() : rq4.G.f();
            case 5:
                return z ? rq4.G.c() : rq4.G.b();
            case 6:
                return z ? e(portfolioApp, rq4.G.E()) : e(portfolioApp, rq4.G.D());
            case 7:
                return z ? rq4.G.y() : rq4.G.x();
            case 8:
                return z ? rq4.G.A() : rq4.G.z();
        }
    }

    @DexIgnore
    public final String c(PortfolioApp portfolioApp, int i, String str) {
        pq7.c(portfolioApp, "app");
        pq7.c(str, "version");
        int u = portfolioApp.k0().u();
        boolean z = false;
        if (u == 0 ? !vt7.j("release", "release", true) : u == 1) {
            z = true;
        }
        if (i != 0) {
            return b(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return rq4.G.m();
                }
            } else if (str.equals("1")) {
                return rq4.G.l();
            }
            return rq4.G.n();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return rq4.G.j();
            }
        } else if (str.equals("1")) {
            return rq4.G.i();
        }
        return rq4.G.k();
    }

    @DexIgnore
    public final String d() {
        return PortfolioApp.h0.c().z0() ? rq4.G.r() : rq4.G.s();
    }

    @DexIgnore
    public final String e(PortfolioApp portfolioApp, String str) {
        String P = portfolioApp.P();
        Locale locale = Locale.getDefault();
        pq7.b(locale, "Locale.getDefault()");
        String language = locale.getLanguage();
        if (pq7.a(language, "zh")) {
            hr7 hr7 = hr7.f1520a;
            Locale locale2 = Locale.getDefault();
            pq7.b(locale2, "Locale.getDefault()");
            language = String.format("%s_%s", Arrays.copyOf(new Object[]{language, locale2.getCountry()}, 2));
            pq7.b(language, "java.lang.String.format(format, *args)");
        }
        hr7 hr72 = hr7.f1520a;
        String format = String.format(str + "/%s?locale=%s", Arrays.copyOf(new Object[]{P, language}, 2));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }
}
