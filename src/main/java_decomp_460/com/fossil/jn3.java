package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jn3 implements ln3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ pm3 f1780a;

    @DexIgnore
    public jn3(pm3 pm3) {
        rc2.k(pm3);
        this.f1780a = pm3;
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public yr3 b() {
        return this.f1780a.b();
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public im3 c() {
        return this.f1780a.c();
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public kl3 d() {
        return this.f1780a.d();
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public Context e() {
        return this.f1780a.e();
    }

    @DexIgnore
    public void f() {
        this.f1780a.q();
    }

    @DexIgnore
    public void g() {
        this.f1780a.c().g();
    }

    @DexIgnore
    public void h() {
        this.f1780a.c().h();
    }

    @DexIgnore
    public pg3 i() {
        return this.f1780a.P();
    }

    @DexIgnore
    public il3 j() {
        return this.f1780a.G();
    }

    @DexIgnore
    public kr3 k() {
        return this.f1780a.F();
    }

    @DexIgnore
    public xl3 l() {
        return this.f1780a.z();
    }

    @DexIgnore
    public zr3 m() {
        return this.f1780a.w();
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public ef2 zzm() {
        return this.f1780a.zzm();
    }
}
