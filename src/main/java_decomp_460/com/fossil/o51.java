package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int[] f2631a;
    @DexIgnore
    public int b;

    @DexIgnore
    public o51(int i) {
        this.f2631a = new int[i];
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o51(int i, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? 10 : i);
    }

    @DexIgnore
    public final boolean a(int i) {
        boolean z = false;
        int f = dm7.f(this.f2631a, i, 0, this.b, 2, null);
        if (f < 0) {
            z = true;
        }
        if (z) {
            this.f2631a = m51.f2303a.b(this.f2631a, this.b, f, i);
            this.b++;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(int i) {
        boolean z = false;
        int f = dm7.f(this.f2631a, i, 0, this.b, 2, null);
        if (f >= 0) {
            z = true;
        }
        if (z) {
            c(f);
        }
        return z;
    }

    @DexIgnore
    public final void c(int i) {
        int[] iArr = this.f2631a;
        int i2 = i + 1;
        System.arraycopy(iArr, i2, iArr, i, this.b - i2);
        this.b--;
    }
}
