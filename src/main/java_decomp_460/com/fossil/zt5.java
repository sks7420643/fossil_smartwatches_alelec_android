package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zt5 implements Factory<yt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DeviceRepository> f4529a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public zt5(Provider<DeviceRepository> provider, Provider<on5> provider2) {
        this.f4529a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static zt5 a(Provider<DeviceRepository> provider, Provider<on5> provider2) {
        return new zt5(provider, provider2);
    }

    @DexIgnore
    public static yt5 c(DeviceRepository deviceRepository, on5 on5) {
        return new yt5(deviceRepository, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public yt5 get() {
        return c(this.f4529a.get(), this.b.get());
    }
}
