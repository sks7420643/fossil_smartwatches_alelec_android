package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fp7 extends Error {
    @DexIgnore
    public fp7() {
        super("Kotlin reflection implementation is not found at runtime. Make sure you have kotlin-reflect.jar in the classpath");
    }

    @DexIgnore
    public fp7(String str) {
        super(str);
    }

    @DexIgnore
    public fp7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public fp7(Throwable th) {
        super(th);
    }
}
