package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n12 {
    @DexIgnore
    public static h22 a(Context context, k22 k22, v12 v12, t32 t32) {
        return Build.VERSION.SDK_INT >= 21 ? new t12(context, k22, v12) : new p12(context, k22, t32, v12);
    }
}
