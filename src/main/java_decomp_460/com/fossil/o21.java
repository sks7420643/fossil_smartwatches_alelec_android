package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o21 extends k21<f21> {
    @DexIgnore
    public o21(Context context, k41 k41) {
        super(w21.c(context, k41).d());
    }

    @DexIgnore
    @Override // com.fossil.k21
    public boolean b(o31 o31) {
        return o31.j.b() == y01.UNMETERED;
    }

    @DexIgnore
    /* renamed from: i */
    public boolean c(f21 f21) {
        return !f21.a() || f21.b();
    }
}
