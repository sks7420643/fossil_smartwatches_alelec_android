package com.fossil;

import android.text.TextUtils;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.gson.AlarmDeserializer;
import com.portfolio.platform.gson.DianaAppSettingDeserializer;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.gson.DianaRecommendPresetDeserializer;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.gson.HybridRecommendPresetDeserializer;
import com.portfolio.platform.gson.NotificationDeserializer;
import com.portfolio.platform.gson.ThemeDeserializer;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Retrofit.b f3452a;
    @DexIgnore
    public static Retrofit b;
    @DexIgnore
    public static Cache c;
    @DexIgnore
    public static /* final */ c57 d;
    @DexIgnore
    public static /* final */ OkHttpClient.b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public static /* final */ tq5 g; // = new tq5();

    /*
    static {
        OkHttpClient.b bVar;
        Retrofit.b bVar2 = new Retrofit.b();
        zi4 zi4 = new zi4();
        zi4.b(new ak5());
        zi4.a(new ak5());
        zi4.f(Date.class, new GsonConvertDate());
        zi4.f(DianaPreset.class, new DianaPresetDeserializer());
        zi4.f(DianaRecommendPreset.class, new DianaRecommendPresetDeserializer());
        zi4.f(HybridPreset.class, new HybridPresetDeserializer());
        zi4.f(HybridRecommendPreset.class, new HybridRecommendPresetDeserializer());
        zi4.f(DateTime.class, new GsonConvertDateTime());
        zi4.f(Alarm.class, new AlarmDeserializer());
        zi4.f(Theme.class, new ThemeDeserializer());
        zi4.f(dt4.class, new NotificationDeserializer());
        zi4.f(DianaAppSetting.class, new DianaAppSettingDeserializer());
        bVar2.a(GsonConverterFactory.g(zi4.d()));
        f3452a = bVar2;
        c57 c57 = new c57();
        c57.c(pq7.a("release", "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
        d = c57;
        if (pq7.a("release", "release")) {
            bVar = new OkHttpClient.b();
            bVar.a(d);
        } else {
            bVar = new OkHttpClient.b();
            bVar.a(d);
            bVar.b(new StethoInterceptor());
        }
        e = bVar;
    }
    */

    @DexIgnore
    public final void a() {
        Cache cache = c;
        if (cache != null) {
            cache.b();
        }
    }

    @DexIgnore
    public final <S> S b(Class<S> cls) {
        pq7.c(cls, "serviceClass");
        Retrofit retrofit3 = b;
        if (retrofit3 != null) {
            return (S) retrofit3.b(cls);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void c(y08 y08) {
        pq7.c(y08, "authenticator");
        e.c(y08);
        f3452a.f(e.d());
        b = f3452a.d();
    }

    @DexIgnore
    public final void d(String str) {
        pq7.c(str, "baseUrl");
        if (!TextUtils.equals(f, str)) {
            f = str;
            Retrofit.b bVar = f3452a;
            if (str != null) {
                bVar.b(str);
                b = f3452a.d();
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void e(File file) {
        pq7.c(file, "cacheDir");
        if (c == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            c = new Cache(file2, 10485760);
        }
        e.e(c);
    }

    @DexIgnore
    public final void f(Interceptor interceptor) {
        e.m(10, TimeUnit.SECONDS);
        e.g(10, TimeUnit.SECONDS);
        e.n(10, TimeUnit.SECONDS);
        e.j().clear();
        e.a(d);
        if (interceptor != null && !e.j().contains(interceptor)) {
            e.a(interceptor);
        }
        f3452a.f(e.d());
        b = f3452a.d();
    }

    @DexIgnore
    public final void g(long j) {
        e.m(j, TimeUnit.SECONDS);
        e.g(j, TimeUnit.SECONDS);
        b = f3452a.d();
    }
}
