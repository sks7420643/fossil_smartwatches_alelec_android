package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi6 implements Factory<zi6> {
    @DexIgnore
    public static zi6 a(ui6 ui6) {
        zi6 b = ui6.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
