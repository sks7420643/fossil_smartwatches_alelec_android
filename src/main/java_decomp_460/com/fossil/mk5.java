package com.fossil;

import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class mk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2397a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[nk5.b.values().length];
        f2397a = iArr;
        iArr[nk5.b.SMALL.ordinal()] = 1;
        f2397a[nk5.b.NORMAL.ordinal()] = 2;
        f2397a[nk5.b.LARGE.ordinal()] = 3;
        f2397a[nk5.b.WATCH_COMPLETED.ordinal()] = 4;
        f2397a[nk5.b.HYBRID_WATCH_HOUR.ordinal()] = 5;
        f2397a[nk5.b.HYBRID_WATCH_MINUTE.ordinal()] = 6;
        f2397a[nk5.b.HYBRID_WATCH_SUBEYE.ordinal()] = 7;
        f2397a[nk5.b.DIANA_WATCH_HOUR.ordinal()] = 8;
        f2397a[nk5.b.DIANA_WATCH_MINUTE.ordinal()] = 9;
        int[] iArr2 = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        b = iArr2;
        iArr2[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        b[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
    }
    */
}
