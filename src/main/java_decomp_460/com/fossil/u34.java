package com.fossil;

import com.fossil.e14;
import com.fossil.v34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u34 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f3510a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public v34.p d;
    @DexIgnore
    public v34.p e;
    @DexIgnore
    public z04<Object> f;

    @DexIgnore
    @CanIgnoreReturnValue
    public u34 a(int i) {
        boolean z = true;
        i14.u(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        i14.d(z);
        this.c = i;
        return this;
    }

    @DexIgnore
    public int b() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    @DexIgnore
    public int c() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    @DexIgnore
    public z04<Object> d() {
        return (z04) e14.a(this.f, e().defaultEquivalence());
    }

    @DexIgnore
    public v34.p e() {
        return (v34.p) e14.a(this.d, v34.p.STRONG);
    }

    @DexIgnore
    public v34.p f() {
        return (v34.p) e14.a(this.e, v34.p.STRONG);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public u34 g(int i) {
        boolean z = true;
        i14.u(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        i14.d(z);
        this.b = i;
        return this;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public u34 h(z04<Object> z04) {
        i14.v(this.f == null, "key equivalence was already set to %s", this.f);
        i14.l(z04);
        this.f = z04;
        this.f3510a = true;
        return this;
    }

    @DexIgnore
    public <K, V> ConcurrentMap<K, V> i() {
        return !this.f3510a ? new ConcurrentHashMap(c(), 0.75f, b()) : v34.create(this);
    }

    @DexIgnore
    public u34 j(v34.p pVar) {
        i14.v(this.d == null, "Key strength was already set to %s", this.d);
        i14.l(pVar);
        this.d = pVar;
        if (pVar != v34.p.STRONG) {
            this.f3510a = true;
        }
        return this;
    }

    @DexIgnore
    public u34 k(v34.p pVar) {
        i14.v(this.e == null, "Value strength was already set to %s", this.e);
        i14.l(pVar);
        this.e = pVar;
        if (pVar != v34.p.STRONG) {
            this.f3510a = true;
        }
        return this;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public u34 l() {
        j(v34.p.WEAK);
        return this;
    }

    @DexIgnore
    public String toString() {
        e14.b b2 = e14.b(this);
        int i = this.b;
        if (i != -1) {
            b2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            b2.a("concurrencyLevel", i2);
        }
        v34.p pVar = this.d;
        if (pVar != null) {
            b2.b("keyStrength", y04.b(pVar.toString()));
        }
        v34.p pVar2 = this.e;
        if (pVar2 != null) {
            b2.b("valueStrength", y04.b(pVar2.toString()));
        }
        if (this.f != null) {
            b2.f("keyEquivalence");
        }
        return b2.toString();
    }
}
