package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q47 implements tf7 {
    @DexIgnore
    public static volatile q47 e;
    @DexIgnore
    public a b;
    @DexIgnore
    public sf7 c;
    @DexIgnore
    public String d;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        Object c();  // void declaration

        @DexIgnore
        void d(String str);
    }

    @DexIgnore
    public static q47 i() {
        q47 q47;
        synchronized (q47.class) {
            try {
                if (e == null) {
                    synchronized (q47.class) {
                        try {
                            if (e == null) {
                                e = new q47();
                            }
                        } catch (Throwable th) {
                            throw th;
                        }
                    }
                }
                q47 = e;
            } finally {
            }
        }
        return q47;
    }

    @DexIgnore
    @Override // com.fossil.tf7
    public void a(df7 df7) {
    }

    @DexIgnore
    @Override // com.fossil.tf7
    public void b(ef7 ef7) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, onResp...");
        int i = ef7.f933a;
        if (i == -4) {
            f(ef7);
        } else if (i == -2) {
            e(ef7);
        } else if (i != 0) {
            a aVar = this.b;
            if (aVar != null) {
                aVar.a();
            }
        } else {
            g(ef7);
        }
    }

    @DexIgnore
    public void c(Intent intent, tf7 tf7) {
        sf7 sf7 = this.c;
        if (sf7 != null) {
            sf7.a(intent, tf7);
        }
    }

    @DexIgnore
    public void d(Context context) {
        sf7 a2 = vf7.a(context, this.d, false);
        this.c = a2;
        a2.c(this.d);
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, initialize");
    }

    @DexIgnore
    public final void e(ef7 ef7) {
        if (ef7.b() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize canceled!");
            a aVar = this.b;
            if (aVar != null) {
                aVar.c();
            }
        }
    }

    @DexIgnore
    public final void f(ef7 ef7) {
        if (ef7.b() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize denied!");
            a aVar = this.b;
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    @DexIgnore
    public final void g(ef7 ef7) {
        if (ef7.b() == 1) {
            nf7 nf7 = (nf7) ef7;
            if (nf7.d.equals("com.fossil.wearables.fossil.tag_wechat_login")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GraphRequest.TAG;
                local.d(str, "Wechat authorize succeed, data = " + nf7.c + ", openid: " + nf7.b);
                a aVar = this.b;
                if (aVar != null) {
                    aVar.d(nf7.c);
                }
            }
        }
    }

    @DexIgnore
    public void h(String str) {
        this.d = str;
    }

    @DexIgnore
    public void j(a aVar) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, login...");
        this.b = aVar;
        if (!this.c.b()) {
            this.b.b();
            return;
        }
        mf7 mf7 = new mf7();
        mf7.c = "snsapi_userinfo";
        mf7.d = "com.fossil.wearables.fossil.tag_wechat_login";
        if (!this.c.d(mf7)) {
            this.b.c();
        }
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, start authorize...");
    }
}
