package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.gx5;
import com.fossil.t47;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e56 extends pv5 implements d56, t47.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public c56 g;
    @DexIgnore
    public gx5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return e56.j;
        }

        @DexIgnore
        public final e56 b() {
            return new e56();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gx5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ e56 f881a;

        @DexIgnore
        public b(e56 e56) {
            this.f881a = e56;
        }

        @DexIgnore
        @Override // com.fossil.gx5.a
        public void a(i06 i06, boolean z) {
            pq7.c(i06, "appWrapper");
            if (i06.getCurrentHandGroup() == 0 || i06.getCurrentHandGroup() == e56.L6(this.f881a).n()) {
                e56.L6(this.f881a).p(i06, z);
                return;
            }
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f881a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = i06.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                s37.a0(childFragmentManager, title, i06.getCurrentHandGroup(), e56.L6(this.f881a).n(), i06);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e56 b;

        @DexIgnore
        public c(e56 e56) {
            this.b = e56;
        }

        @DexIgnore
        public final void onClick(View view) {
            e56.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ e56 b;
        @DexIgnore
        public /* final */ /* synthetic */ f95 c;

        @DexIgnore
        public d(e56 e56, f95 f95) {
            this.b = e56;
            this.c = f95;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.c.t;
            pq7.b(imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            e56.K6(this.b).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ f95 b;

        @DexIgnore
        public e(f95 f95) {
            this.b = f95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                f95 f95 = this.b;
                pq7.b(f95, "binding");
                f95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ f95 b;

        @DexIgnore
        public f(f95 f95) {
            this.b = f95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = e56.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gx5 K6(e56 e56) {
        gx5 gx5 = e56.h;
        if (gx5 != null) {
            return gx5;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ c56 L6(e56 e56) {
        c56 c56 = e56.g;
        if (c56 != null) {
            return c56;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        c56 c56 = this.g;
        if (c56 != null) {
            c56.o();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d56
    public void I(ArrayList<String> arrayList) {
        pq7.c(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(c56 c56) {
        pq7.c(c56, "presenter");
        this.g = c56;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        i06 i06;
        i06 i062;
        pq7.c(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i2 != 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (i062 = (i06) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                c56 c56 = this.g;
                if (c56 != null) {
                    c56.p(i062, false);
                    gx5 gx5 = this.h;
                    if (gx5 != null) {
                        gx5.notifyDataSetChanged();
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null && (i06 = (i06) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                c56 c562 = this.g;
                if (c562 != null) {
                    c562.p(i06, true);
                    gx5 gx52 = this.h;
                    if (gx52 != null) {
                        gx52.notifyDataSetChanged();
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.d56
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.d56
    public void m() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        f95 f95 = (f95) aq0.f(layoutInflater, 2131558593, viewGroup, false, A6());
        f95.s.setOnClickListener(new c(this));
        f95.q.addTextChangedListener(new d(this, f95));
        f95.q.setOnFocusChangeListener(new e(f95));
        f95.t.setOnClickListener(new f(f95));
        gx5 gx5 = new gx5();
        gx5.o(new b(this));
        this.h = gx5;
        RecyclerView recyclerView = f95.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        gx5 gx52 = this.h;
        if (gx52 != null) {
            recyclerView.setAdapter(gx52);
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                f95.u.setBackgroundColor(Color.parseColor(d2));
            }
            new g37(this, f95);
            pq7.b(f95, "binding");
            return f95.n();
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        c56 c56 = this.g;
        if (c56 != null) {
            c56.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        c56 c56 = this.g;
        if (c56 != null) {
            c56.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.d56
    public void y2(List<i06> list, int i2) {
        pq7.c(list, "listAppWrapper");
        gx5 gx5 = this.h;
        if (gx5 != null) {
            gx5.n(list, i2);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }
}
