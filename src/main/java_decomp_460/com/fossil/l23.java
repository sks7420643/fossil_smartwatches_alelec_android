package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ j23 f2136a; // = c();
    @DexIgnore
    public static /* final */ j23 b; // = new i23();

    @DexIgnore
    public static j23 a() {
        return f2136a;
    }

    @DexIgnore
    public static j23 b() {
        return b;
    }

    @DexIgnore
    public static j23 c() {
        try {
            return (j23) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
