package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu6 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<WorkoutSetting> f1534a; // = new ArrayList<>();
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void M3(WorkoutSetting workoutSetting, boolean z);

        @DexIgnore
        void m5(WorkoutSetting workoutSetting, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ gg5 f1535a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hu6 hu6, gg5 gg5) {
            super(gg5.n());
            pq7.c(gg5, "binding");
            this.f1535a = gg5;
        }

        @DexIgnore
        public final gg5 a() {
            return this.f1535a;
        }

        @DexIgnore
        public final void b(WorkoutSetting workoutSetting) {
            pq7.c(workoutSetting, "workout");
            View n = this.f1535a.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            oi5.a aVar = oi5.Companion;
            cl7<Integer, Integer> a2 = aVar.a(aVar.d(workoutSetting.getType(), workoutSetting.getMode()));
            String c = um5.c(context, a2.getSecond().intValue());
            String c2 = um5.c(context, 2131886635);
            gg5 gg5 = this.f1535a;
            gg5.x.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = gg5.s;
            pq7.b(flexibleTextView, "it.ftvName");
            flexibleTextView.setText(c);
            FlexibleSwitchCompat flexibleSwitchCompat = gg5.y;
            pq7.b(flexibleSwitchCompat, "it.swEnabled");
            flexibleSwitchCompat.setChecked(workoutSetting.getEnable());
            FlexibleCheckBox flexibleCheckBox = gg5.q;
            pq7.b(flexibleCheckBox, "it.cbConfirmation");
            flexibleCheckBox.setChecked(workoutSetting.getAskMeFirst());
            hr7 hr7 = hr7.f1520a;
            String format = String.format("%d " + c2, Arrays.copyOf(new Object[]{Integer.valueOf(workoutSetting.getStartLatency())}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = gg5.r;
            pq7.b(flexibleTextView2, "it.ftvLatency");
            flexibleTextView2.setText(format);
        }

        @DexIgnore
        public final void c(boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.f1535a.t;
                pq7.b(flexibleTextView, "binding.ftvUserConfirmation");
                flexibleTextView.setVisibility(0);
                FlexibleCheckBox flexibleCheckBox = this.f1535a.q;
                pq7.b(flexibleCheckBox, "binding.cbConfirmation");
                flexibleCheckBox.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = this.f1535a.t;
            pq7.b(flexibleTextView2, "binding.ftvUserConfirmation");
            flexibleTextView2.setVisibility(8);
            FlexibleCheckBox flexibleCheckBox2 = this.f1535a.q;
            pq7.b(flexibleCheckBox2, "binding.cbConfirmation");
            flexibleCheckBox2.setVisibility(8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hu6 f1536a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ b c;

        @DexIgnore
        public c(hu6 hu6, WorkoutSetting workoutSetting, b bVar) {
            this.f1536a = hu6;
            this.b = workoutSetting;
            this.c = bVar;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setEnable(z);
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            pq7.b(flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(z);
            this.c.c(this.b.getEnable());
            a aVar = this.f1536a.b;
            if (aVar != null) {
                aVar.m5(this.b, z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hu6 f1537a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ b c;

        @DexIgnore
        public d(hu6 hu6, WorkoutSetting workoutSetting, b bVar) {
            this.f1537a = hu6;
            this.b = workoutSetting;
            this.c = bVar;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            pq7.b(flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(this.b.getAskMeFirst());
            a aVar = this.f1537a.b;
            if (aVar != null) {
                aVar.M3(this.b, z);
            }
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1534a.size();
    }

    @DexIgnore
    public final List<WorkoutSetting> h() {
        return this.f1534a;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        WorkoutSetting workoutSetting = this.f1534a.get(i);
        pq7.b(workoutSetting, "mData[position]");
        WorkoutSetting workoutSetting2 = workoutSetting;
        bVar.a().y.setOnCheckedChangeListener(null);
        bVar.a().q.setOnCheckedChangeListener(null);
        bVar.b(workoutSetting2);
        bVar.c(workoutSetting2.getEnable());
        bVar.a().y.setOnCheckedChangeListener(new c(this, workoutSetting2, bVar));
        bVar.a().q.setOnCheckedChangeListener(new d(this, workoutSetting2, bVar));
    }

    @DexIgnore
    /* renamed from: j */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        gg5 z = gg5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemWorkoutSettingBindin\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final void k(List<WorkoutSetting> list) {
        pq7.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingAdapter", "setData, " + list.size());
        this.f1534a.clear();
        this.f1534a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }
}
