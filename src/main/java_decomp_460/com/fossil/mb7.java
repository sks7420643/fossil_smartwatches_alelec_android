package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mb7 implements gb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2355a;
    @DexIgnore
    public String b;
    @DexIgnore
    public float c;
    @DexIgnore
    public nb7 d;
    @DexIgnore
    public jb7 e;
    @DexIgnore
    public lb7 f;
    @DexIgnore
    public int g;

    @DexIgnore
    public mb7(String str, String str2, float f2, nb7 nb7, jb7 jb7, lb7 lb7, int i) {
        pq7.c(str, "text");
        pq7.c(str2, "fontType");
        pq7.c(nb7, "colour");
        pq7.c(jb7, "metric");
        pq7.c(lb7, "type");
        this.f2355a = str;
        this.b = str2;
        this.c = f2;
        this.d = nb7;
        this.e = jb7;
        this.f = lb7;
        this.g = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ mb7(String str, String str2, float f2, nb7 nb7, jb7 jb7, lb7 lb7, int i, int i2, kq7 kq7) {
        this(str, str2, f2, (i2 & 8) != 0 ? nb7.DEFAULT : nb7, jb7, (i2 & 32) != 0 ? lb7.TEXT : lb7, i);
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public int a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public jb7 b() {
        return this.e;
    }

    @DexIgnore
    public final nb7 c() {
        return this.d;
    }

    @DexIgnore
    public final float d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof mb7) {
                mb7 mb7 = (mb7) obj;
                if (!pq7.a(this.f2355a, mb7.f2355a) || !pq7.a(this.b, mb7.b) || Float.compare(this.c, mb7.c) != 0 || !pq7.a(this.d, mb7.d) || !pq7.a(b(), mb7.b()) || !pq7.a(getType(), mb7.getType()) || a() != mb7.a()) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.f2355a;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public lb7 getType() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2355a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        int floatToIntBits = Float.floatToIntBits(this.c);
        nb7 nb7 = this.d;
        int hashCode3 = nb7 != null ? nb7.hashCode() : 0;
        jb7 b2 = b();
        int hashCode4 = b2 != null ? b2.hashCode() : 0;
        lb7 type = getType();
        if (type != null) {
            i = type.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + floatToIntBits) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + a();
    }

    @DexIgnore
    public String toString() {
        return "WFTextData(text=" + this.f2355a + ", fontType=" + this.b + ", fontScaled=" + this.c + ", colour=" + this.d + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
