package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vp7<P1, P2, R> extends uk7<R> {
    @DexIgnore
    R invoke(P1 p1, P2 p2);
}
