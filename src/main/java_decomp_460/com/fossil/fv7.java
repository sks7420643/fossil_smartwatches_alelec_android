package com.fossil;

import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv7 {
    @DexIgnore
    public static final void a(tn7 tn7, Throwable th) {
        try {
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) tn7.get(CoroutineExceptionHandler.q);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(tn7, th);
            } else {
                ev7.a(tn7, th);
            }
        } catch (Throwable th2) {
            ev7.a(tn7, b(th, th2));
        }
    }

    @DexIgnore
    public static final Throwable b(Throwable th, Throwable th2) {
        if (th == th2) {
            return th;
        }
        RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
        tk7.a(runtimeException, th);
        return runtimeException;
    }
}
