package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw2<T> implements xw2<T>, Serializable {
    @DexIgnore
    public volatile transient boolean b;
    @DexIgnore
    @NullableDecl
    public transient T c;
    @DexIgnore
    public /* final */ xw2<T> zza;

    @DexIgnore
    public zw2(xw2<T> xw2) {
        sw2.b(xw2);
        this.zza = xw2;
    }

    @DexIgnore
    public final String toString() {
        Object obj;
        if (this.b) {
            String valueOf = String.valueOf(this.c);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            obj = sb.toString();
        } else {
            obj = this.zza;
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    @Override // com.fossil.xw2
    public final T zza() {
        if (!this.b) {
            synchronized (this) {
                if (!this.b) {
                    T zza2 = this.zza.zza();
                    this.c = zza2;
                    this.b = true;
                    return zza2;
                }
            }
        }
        return this.c;
    }
}
