package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yi2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<yi2> CREATOR; // = new xi2();
    @DexIgnore
    public /* final */ mo2 b;

    @DexIgnore
    public yi2(IBinder iBinder) {
        this.b = oo2.e(iBinder);
    }

    @DexIgnore
    public yi2(mo2 mo2) {
        this.b = mo2;
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.m(parcel, 1, this.b.asBinder(), false);
        bd2.b(parcel, a2);
    }
}
