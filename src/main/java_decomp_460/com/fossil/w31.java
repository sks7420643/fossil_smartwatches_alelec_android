package com.fossil;

import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import com.fossil.a11;
import com.fossil.r01;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w31 implements Runnable {
    @DexIgnore
    public static /* final */ String d; // = x01.f("EnqueueRunnable");
    @DexIgnore
    public /* final */ p11 b;
    @DexIgnore
    public /* final */ l11 c; // = new l11();

    @DexIgnore
    public w31(p11 p11) {
        this.b = p11;
    }

    @DexIgnore
    public static boolean b(p11 p11) {
        boolean c2 = c(p11.g(), p11.f(), (String[]) p11.l(p11).toArray(new String[0]), p11.d(), p11.b());
        p11.k();
        return c2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean c(com.fossil.s11 r18, java.util.List<? extends com.fossil.h11> r19, java.lang.String[] r20, java.lang.String r21, com.fossil.s01 r22) {
        /*
        // Method dump skipped, instructions count: 564
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w31.c(com.fossil.s11, java.util.List, java.lang.String[], java.lang.String, com.fossil.s01):boolean");
    }

    @DexIgnore
    public static boolean e(p11 p11) {
        boolean z;
        List<p11> e = p11.e();
        if (e != null) {
            z = false;
            for (p11 p112 : e) {
                if (!p112.j()) {
                    z = e(p112) | z;
                } else {
                    x01.c().h(d, String.format("Already enqueued work ids (%s).", TextUtils.join(", ", p112.c())), new Throwable[0]);
                }
            }
        } else {
            z = false;
        }
        return b(p11) | z;
    }

    @DexIgnore
    public static void g(o31 o31) {
        p01 p01 = o31.j;
        if (p01.f() || p01.i()) {
            String str = o31.c;
            r01.a aVar = new r01.a();
            aVar.c(o31.e);
            aVar.e("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            o31.c = ConstraintTrackingWorker.class.getName();
            o31.e = aVar.a();
        }
    }

    @DexIgnore
    public static boolean h(s11 s11, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (n11 n11 : s11.o()) {
                if (cls.isAssignableFrom(n11.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException e) {
        }
        return false;
    }

    @DexIgnore
    public boolean a() {
        WorkDatabase p = this.b.g().p();
        p.beginTransaction();
        try {
            boolean e = e(this.b);
            p.setTransactionSuccessful();
            return e;
        } finally {
            p.endTransaction();
        }
    }

    @DexIgnore
    public a11 d() {
        return this.c;
    }

    @DexIgnore
    public void f() {
        s11 g = this.b.g();
        o11.b(g.j(), g.p(), g.o());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.b.h()) {
                if (a()) {
                    y31.a(this.b.g().i(), RescheduleReceiver.class, true);
                    f();
                }
                this.c.a(a11.f178a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", this.b));
        } catch (Throwable th) {
            this.c.a(new a11.b.a(th));
        }
    }
}
