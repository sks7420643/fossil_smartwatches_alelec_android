package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a18 extends Cloneable {

    @DexIgnore
    public interface a {
        @DexIgnore
        a18 d(v18 v18);
    }

    @DexIgnore
    Response a() throws IOException;

    @DexIgnore
    v18 c();

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    boolean f();

    @DexIgnore
    void m(b18 b18);
}
