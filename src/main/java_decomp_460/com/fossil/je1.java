package com.fossil;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1744a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ int i; // = (Build.VERSION.SDK_INT < 26 ? 4 : 1);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f1745a;
        @DexIgnore
        public ActivityManager b;
        @DexIgnore
        public c c;
        @DexIgnore
        public float d; // = 2.0f;
        @DexIgnore
        public float e; // = ((float) i);
        @DexIgnore
        public float f; // = 0.4f;
        @DexIgnore
        public float g; // = 0.33f;
        @DexIgnore
        public int h; // = 4194304;

        @DexIgnore
        public a(Context context) {
            this.f1745a = context;
            this.b = (ActivityManager) context.getSystemService(Constants.ACTIVITY);
            this.c = new b(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && je1.e(this.b)) {
                this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
        }

        @DexIgnore
        public je1 a() {
            return new je1(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ DisplayMetrics f1746a;

        @DexIgnore
        public b(DisplayMetrics displayMetrics) {
            this.f1746a = displayMetrics;
        }

        @DexIgnore
        @Override // com.fossil.je1.c
        public int a() {
            return this.f1746a.heightPixels;
        }

        @DexIgnore
        @Override // com.fossil.je1.c
        public int b() {
            return this.f1746a.widthPixels;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        int a();

        @DexIgnore
        int b();
    }

    @DexIgnore
    public je1(a aVar) {
        this.c = aVar.f1745a;
        this.d = e(aVar.b) ? aVar.h / 2 : aVar.h;
        int c2 = c(aVar.b, aVar.f, aVar.g);
        float b2 = (float) (aVar.c.b() * aVar.c.a() * 4);
        int round = Math.round(aVar.e * b2);
        int round2 = Math.round(b2 * aVar.d);
        int i = c2 - this.d;
        int i2 = round2 + round;
        if (i2 <= i) {
            this.b = round2;
            this.f1744a = round;
        } else {
            float f = (float) i;
            float f2 = aVar.e;
            float f3 = aVar.d;
            float f4 = f / (f2 + f3);
            this.b = Math.round(f3 * f4);
            this.f1744a = Math.round(f4 * aVar.e);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Calculation complete, Calculated memory cache size: ");
            sb.append(f(this.b));
            sb.append(", pool size: ");
            sb.append(f(this.f1744a));
            sb.append(", byte array size: ");
            sb.append(f(this.d));
            sb.append(", memory class limited? ");
            sb.append(i2 > c2);
            sb.append(", max size: ");
            sb.append(f(c2));
            sb.append(", memoryClass: ");
            sb.append(aVar.b.getMemoryClass());
            sb.append(", isLowMemoryDevice: ");
            sb.append(e(aVar.b));
            Log.d("MemorySizeCalculator", sb.toString());
        }
    }

    @DexIgnore
    public static int c(ActivityManager activityManager, float f, float f2) {
        float memoryClass = (float) (activityManager.getMemoryClass() * 1024 * 1024);
        if (!e(activityManager)) {
            f2 = f;
        }
        return Math.round(memoryClass * f2);
    }

    @DexIgnore
    @TargetApi(19)
    public static boolean e(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return true;
    }

    @DexIgnore
    public int a() {
        return this.d;
    }

    @DexIgnore
    public int b() {
        return this.f1744a;
    }

    @DexIgnore
    public int d() {
        return this.b;
    }

    @DexIgnore
    public final String f(int i) {
        return Formatter.formatFileSize(this.c, (long) i);
    }
}
