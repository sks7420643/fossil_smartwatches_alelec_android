package com.fossil;

import com.fossil.nm1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class b9 extends nq7 implements rp7<byte[], nm1> {
    @DexIgnore
    public b9(nm1.c cVar) {
        super(1, cVar);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final fs7 getOwner() {
        return er7.b(nm1.c.class);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/BiometricProfile;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public nm1 invoke(byte[] bArr) {
        return ((nm1.c) this.receiver).a(bArr);
    }
}
