package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<Style> f1180a; // = new ArrayList<>();
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public /* final */ ArrayList<Theme> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void D2(String str);

        @DexIgnore
        void L4(String str);

        @DexIgnore
        void g1(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AppCompatCheckBox f1181a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ fr4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar = this.b.d.d;
                if (aVar != null) {
                    aVar.D2(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
                fr4 fr4 = this.b.d;
                fr4.b = ((Theme) fr4.c.get(this.b.getAdapterPosition())).getId();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fr4$b$b")
        /* renamed from: com.fossil.fr4$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0089b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0089b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar = this.b.d.d;
                if (aVar != null) {
                    aVar.g1(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public c(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar = this.b.d.d;
                if (aVar != null) {
                    aVar.L4(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fr4 fr4, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = fr4;
            this.f1181a = (AppCompatCheckBox) view.findViewById(2131361998);
            this.b = (ImageView) view.findViewById(2131362695);
            this.c = (ImageView) view.findViewById(2131362690);
            this.f1181a.setOnClickListener(new a(this));
            this.b.setOnClickListener(new View$OnClickListenerC0089b(this));
            this.c.setOnClickListener(new c(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Theme theme) {
            pq7.c(theme, "theme");
            if (pq7.a(theme.getType(), "fixed")) {
                ImageView imageView = this.b;
                pq7.b(imageView, "ivEdit");
                imageView.setVisibility(4);
                ImageView imageView2 = this.c;
                pq7.b(imageView2, "ivDelete");
                imageView2.setVisibility(4);
            }
            AppCompatCheckBox appCompatCheckBox = this.f1181a;
            pq7.b(appCompatCheckBox, "cbCurrentTheme");
            appCompatCheckBox.setChecked(pq7.a(this.d.b, theme.getId()));
            AppCompatCheckBox appCompatCheckBox2 = this.f1181a;
            pq7.b(appCompatCheckBox2, "cbCurrentTheme");
            appCompatCheckBox2.setText(theme.getName());
            String e = qn5.l.a().e("primaryText", this.d.f1180a);
            String e2 = qn5.l.a().e("nonBrandSurface", this.d.f1180a);
            Typeface g = qn5.l.a().g("headline2", this.d.f1180a);
            if (e != null) {
                this.f1181a.setTextColor(Color.parseColor(e));
                ep0.c(this.f1181a, new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(e), Color.parseColor(e2), Color.parseColor(e2)}));
                this.c.setColorFilter(Color.parseColor(e));
                this.b.setColorFilter(Color.parseColor(e));
            }
            if (g != null) {
                AppCompatCheckBox appCompatCheckBox3 = this.f1181a;
                pq7.b(appCompatCheckBox3, "cbCurrentTheme");
                appCompatCheckBox3.setTypeface(g);
            }
        }
    }

    @DexIgnore
    public fr4(ArrayList<Theme> arrayList, a aVar) {
        pq7.c(arrayList, "mData");
        this.c = arrayList;
        this.d = aVar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void l(List<Style> list, String str) {
        pq7.c(list, "allStyles");
        pq7.c(str, "selectedThemeId");
        this.f1180a.clear();
        this.f1180a.addAll(list);
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: m */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Theme theme = this.c.get(i);
            pq7.b(theme, "mData[position]");
            bVar.a(theme);
        }
    }

    @DexIgnore
    /* renamed from: n */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558721, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026tem_theme, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void o(ArrayList<Theme> arrayList) {
        pq7.c(arrayList, "ids");
        this.c.clear();
        this.c.addAll(arrayList);
        notifyDataSetChanged();
    }
}
