package com.fossil;

import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i6 extends m5 {
    @DexIgnore
    public byte[] m; // = new byte[0];
    @DexIgnore
    public /* final */ boolean n;

    @DexIgnore
    public i6(n6 n6Var, boolean z, n4 n4Var) {
        super(v5.h, n6Var, n4Var);
        this.n = z;
    }

    @DexIgnore
    @Override // com.fossil.m5, com.fossil.u5
    public JSONObject b(boolean z) {
        return g80.k(super.b(z), jd0.F, Boolean.valueOf(this.n));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d9  */
    @Override // com.fossil.u5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(com.fossil.k5 r11) {
        /*
        // Method dump skipped, instructions count: 364
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i6.d(com.fossil.k5):void");
    }

    @DexIgnore
    @Override // com.fossil.m5, com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        this.k = false;
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = s5.a(this.e, null, Arrays.equals(this.m, ((b7) h7Var).d) ? r5.b : r5.e, h7Var.f1435a, 1);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        if (h7Var instanceof b7) {
            b7 b7Var = (b7) h7Var;
            return b7Var.b == this.l && b7Var.c == u6.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.b;
    }
}
