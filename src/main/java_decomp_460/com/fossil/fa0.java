package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class fa0 extends Enum<fa0> {
    @DexIgnore
    public static /* final */ fa0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ fa0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        fa0 fa0 = new fa0("NONE", 0, (byte) 0);
        fa0 fa02 = new fa0("OPEN", 1, (byte) 1);
        fa0 fa03 = new fa0("ERROR", 2, (byte) 2);
        c = fa03;
        d = new fa0[]{fa0, fa02, fa03, new fa0("READY", 3, (byte) 3), new fa0("START", 4, (byte) 4), new fa0("STOP", 5, (byte) 5), new fa0("RESET", 6, (byte) 6)};
    }
    */

    @DexIgnore
    public fa0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static fa0 valueOf(String str) {
        return (fa0) Enum.valueOf(fa0.class, str);
    }

    @DexIgnore
    public static fa0[] values() {
        return (fa0[]) d.clone();
    }
}
