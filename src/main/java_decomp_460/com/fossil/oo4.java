package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo4 implements Factory<no4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ oo4 f2702a; // = new oo4();
    }

    @DexIgnore
    public static oo4 a() {
        return a.f2702a;
    }

    @DexIgnore
    public static no4 c() {
        return new no4();
    }

    @DexIgnore
    /* renamed from: b */
    public no4 get() {
        return c();
    }
}
