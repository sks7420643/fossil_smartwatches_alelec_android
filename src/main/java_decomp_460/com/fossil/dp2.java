package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dp2 extends xm2 implements bp2 {
    @DexIgnore
    public dp2() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    @Override // com.fossil.xm2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            j0((yo2) qo2.a(parcel, yo2.CREATOR), xn2.e(parcel.readStrongBinder()));
        } else if (i == 2) {
            h1((gj2) qo2.a(parcel, gj2.CREATOR), oo2.e(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            R1((zo2) qo2.a(parcel, zo2.CREATOR), oo2.e(parcel.readStrongBinder()));
        }
        return true;
    }
}
