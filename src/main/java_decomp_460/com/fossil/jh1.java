package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jh1 extends zg1<hh1> implements ed1 {
    @DexIgnore
    public jh1(hh1 hh1) {
        super(hh1);
    }

    @DexIgnore
    @Override // com.fossil.ed1, com.fossil.zg1
    public void a() {
        ((hh1) this.b).e().prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
        ((hh1) this.b).stop();
        ((hh1) this.b).k();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return ((hh1) this.b).i();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<hh1> d() {
        return hh1.class;
    }
}
