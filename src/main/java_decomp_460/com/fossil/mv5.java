package com.fossil;

import android.content.Context;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mv5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f2426a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ArrayList<String> a(Context context, BarChart.c cVar) {
            pq7.c(context, "context");
            pq7.c(cVar, "chartModel");
            ArrayList<String> arrayList = new ArrayList<>();
            Calendar instance = Calendar.getInstance();
            for (T t : cVar.b()) {
                if (t.c() > 0) {
                    pq7.b(instance, "calendar");
                    instance.setTimeInMillis(t.c());
                    Boolean p0 = lk5.p0(instance.getTime());
                    pq7.b(p0, "DateHelper.isToday(calendar.time)");
                    if (p0.booleanValue()) {
                        arrayList.add(um5.c(context, 2131886648));
                    } else {
                        switch (instance.get(7)) {
                            case 1:
                                arrayList.add(um5.c(context, 2131886770));
                                continue;
                            case 2:
                                arrayList.add(um5.c(context, 2131886769));
                                continue;
                            case 3:
                                arrayList.add(um5.c(context, 2131886772));
                                continue;
                            case 4:
                                arrayList.add(um5.c(context, 2131886774));
                                continue;
                            case 5:
                                arrayList.add(um5.c(context, 2131886773));
                                continue;
                            case 6:
                                arrayList.add(um5.c(context, 2131886768));
                                continue;
                            case 7:
                                arrayList.add(um5.c(context, 2131886771));
                                continue;
                        }
                    }
                } else {
                    arrayList.add("");
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = (int) (((double) i) * 1.15d);
            return (i2 >= 0 && 9 >= i2) ? i2 % 2 != 0 ? ((i2 / 2) * 2) + 2 : i2 : (10 <= i2 && 99 >= i2) ? i2 % 10 != 0 ? ((i2 / 10) * 10) + 10 : i2 : (100 <= i2 && 999 >= i2) ? i2 % 20 != 0 ? ((i2 / 20) * 20) + 20 : i2 : i2 % 200 != 0 ? ((i2 / 200) * 200) + 200 : i2;
        }
    }
}
