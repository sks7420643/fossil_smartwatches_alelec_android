package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu4 implements Factory<du4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<bu4> f988a;
    @DexIgnore
    public /* final */ Provider<cu4> b;

    @DexIgnore
    public eu4(Provider<bu4> provider, Provider<cu4> provider2) {
        this.f988a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static eu4 a(Provider<bu4> provider, Provider<cu4> provider2) {
        return new eu4(provider, provider2);
    }

    @DexIgnore
    public static du4 c(bu4 bu4, cu4 cu4) {
        return new du4(bu4, cu4);
    }

    @DexIgnore
    /* renamed from: b */
    public du4 get() {
        return c(this.f988a.get(), this.b.get());
    }
}
