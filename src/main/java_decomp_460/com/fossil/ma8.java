package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2348a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public ma8(String str, String str2, int i, int i2, boolean z) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        this.f2348a = str;
        this.b = str2;
        this.c = i;
        this.d = i2;
        this.e = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ma8(String str, String str2, int i, int i2, boolean z, int i3, kq7 kq7) {
        this(str, str2, i, i2, (i3 & 16) != 0 ? false : z);
    }

    @DexIgnore
    public final String a() {
        return this.f2348a;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof ma8)) {
                return false;
            }
            ma8 ma8 = (ma8) obj;
            if (!pq7.a(this.f2348a, ma8.f2348a) || !pq7.a(this.b, ma8.b)) {
                return false;
            }
            if (!(this.c == ma8.c)) {
                return false;
            }
            if (!(this.d == ma8.d)) {
                return false;
            }
            if (!(this.e == ma8.e)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2348a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i2 = this.c;
        int i3 = this.d;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((hashCode * 31) + i) * 31) + i2) * 31) + i3) * 31) + i4;
    }

    @DexIgnore
    public String toString() {
        return "GalleryEntity(id=" + this.f2348a + ", name=" + this.b + ", length=" + this.c + ", typeInt=" + this.d + ", isAll=" + this.e + ")";
    }
}
