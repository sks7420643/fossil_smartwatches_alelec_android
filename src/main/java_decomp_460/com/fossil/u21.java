package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u21 extends t21<f21> {
    @DexIgnore
    public static /* final */ String j; // = x01.f("NetworkStateTracker");
    @DexIgnore
    public /* final */ ConnectivityManager g; // = ((ConnectivityManager) this.b.getSystemService("connectivity"));
    @DexIgnore
    public b h;
    @DexIgnore
    public a i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                x01.c().a(u21.j, "Network broadcast received", new Throwable[0]);
                u21 u21 = u21.this;
                u21.d(u21.g());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            x01.c().a(u21.j, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            u21 u21 = u21.this;
            u21.d(u21.g());
        }

        @DexIgnore
        public void onLost(Network network) {
            x01.c().a(u21.j, "Network connection lost", new Throwable[0]);
            u21 u21 = u21.this;
            u21.d(u21.g());
        }
    }

    @DexIgnore
    public u21(Context context, k41 k41) {
        super(context, k41);
        if (j()) {
            this.h = new b();
        } else {
            this.i = new a();
        }
    }

    @DexIgnore
    public static boolean j() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    @Override // com.fossil.t21
    public void e() {
        if (j()) {
            try {
                x01.c().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                x01.c().b(j, "Received exception while registering network callback", e);
            }
        } else {
            x01.c().a(j, "Registering broadcast receiver", new Throwable[0]);
            this.b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    @Override // com.fossil.t21
    public void f() {
        if (j()) {
            try {
                x01.c().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                x01.c().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            x01.c().a(j, "Unregistering broadcast receiver", new Throwable[0]);
            this.b.unregisterReceiver(this.i);
        }
    }

    @DexIgnore
    public f21 g() {
        boolean z = true;
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean i2 = i();
        boolean a2 = km0.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new f21(z2, i2, a2, z);
    }

    @DexIgnore
    /* renamed from: h */
    public f21 b() {
        return g();
    }

    @DexIgnore
    public final boolean i() {
        NetworkCapabilities networkCapabilities;
        return Build.VERSION.SDK_INT >= 23 && (networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork())) != null && networkCapabilities.hasCapability(16);
    }
}
