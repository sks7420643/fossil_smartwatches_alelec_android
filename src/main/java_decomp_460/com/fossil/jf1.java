package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.af1;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jf1<Data> implements af1<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c<Data> f1749a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements bf1<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContentResolver f1750a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.f1750a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.jf1.c
        public wb1<AssetFileDescriptor> a(Uri uri) {
            return new tb1(this.f1750a, uri);
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, AssetFileDescriptor> b(ef1 ef1) {
            return new jf1(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements bf1<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContentResolver f1751a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.f1751a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.jf1.c
        public wb1<ParcelFileDescriptor> a(Uri uri) {
            return new bc1(this.f1751a, uri);
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, ParcelFileDescriptor> b(ef1 ef1) {
            return new jf1(this);
        }
    }

    @DexIgnore
    public interface c<Data> {
        @DexIgnore
        wb1<Data> a(Uri uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements bf1<Uri, InputStream>, c<InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContentResolver f1752a;

        @DexIgnore
        public d(ContentResolver contentResolver) {
            this.f1752a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.jf1.c
        public wb1<InputStream> a(Uri uri) {
            return new hc1(this.f1752a, uri);
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, InputStream> b(ef1 ef1) {
            return new jf1(this);
        }
    }

    @DexIgnore
    public jf1(c<Data> cVar) {
        this.f1749a = cVar;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(Uri uri, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(uri), this.f1749a.a(uri));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
