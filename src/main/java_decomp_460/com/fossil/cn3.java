package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ fr3 b;
    @DexIgnore
    public /* final */ /* synthetic */ or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 d;

    @DexIgnore
    public cn3(qm3 qm3, fr3 fr3, or3 or3) {
        this.d = qm3;
        this.b = fr3;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        this.d.b.d0();
        if (this.b.c() == null) {
            this.d.b.L(this.b, this.c);
        } else {
            this.d.b.u(this.b, this.c);
        }
    }
}
