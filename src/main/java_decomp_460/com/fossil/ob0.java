package com.fossil;

import android.os.Parcel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ob0 extends mb0 {
    @DexIgnore
    public static /* final */ nb0 CREATOR; // = new nb0(null);

    @DexIgnore
    public /* synthetic */ ob0(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public ob0(bv1 bv1, ry1 ry1) {
        super(bv1, ry1);
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public List<va0> b() {
        return gm7.b(new ka0());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }
}
