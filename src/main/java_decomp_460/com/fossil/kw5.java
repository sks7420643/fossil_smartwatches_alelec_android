package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw5 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public b f2105a;
    @DexIgnore
    public List<? extends Object> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ fj1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ff5 f2106a;
        @DexIgnore
        public /* final */ /* synthetic */ kw5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kw5$a$a")
        /* renamed from: com.fossil.kw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0138a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0138a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.b;
                    if (list != null) {
                        Object obj = list.get(adapterPosition);
                        if (obj instanceof j06) {
                            j06 j06 = (j06) obj;
                            Contact contact = j06.getContact();
                            if (contact == null || contact.getContactId() != -100) {
                                Contact contact2 = j06.getContact();
                                if ((contact2 == null || contact2.getContactId() != -200) && (bVar = this.b.b.f2105a) != null) {
                                    bVar.a(j06);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                FLogger.INSTANCE.getLocal().d(kw5.e, "ivRemove.setOnClickListener");
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.b;
                    if (list != null) {
                        Object obj = list.get(adapterPosition);
                        if (obj instanceof j06) {
                            j06 j06 = (j06) obj;
                            j06.setAdded(!j06.isAdded());
                            FLogger.INSTANCE.getLocal().d(kw5.e, "isAdded=" + j06.isAdded());
                            if (j06.isAdded()) {
                                this.b.b.c++;
                            } else {
                                kw5 kw5 = this.b.b;
                                kw5.c--;
                            }
                        } else if (obj != null) {
                            i06 i06 = (i06) obj;
                            InstalledApp installedApp = i06.getInstalledApp();
                            if (installedApp != null) {
                                InstalledApp installedApp2 = i06.getInstalledApp();
                                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                                if (isSelected != null) {
                                    installedApp.setSelected(!isSelected.booleanValue());
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str = kw5.e;
                            StringBuilder sb = new StringBuilder();
                            sb.append("isSelected=");
                            InstalledApp installedApp3 = i06.getInstalledApp();
                            Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                            if (isSelected2 != null) {
                                sb.append(isSelected2.booleanValue());
                                local.d(str, sb.toString());
                                InstalledApp installedApp4 = i06.getInstalledApp();
                                Boolean isSelected3 = installedApp4 != null ? installedApp4.isSelected() : null;
                                if (isSelected3 == null) {
                                    pq7.i();
                                    throw null;
                                } else if (isSelected3.booleanValue()) {
                                    this.b.b.c++;
                                } else {
                                    kw5 kw52 = this.b.b;
                                    kw52.c--;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                        }
                        b bVar = this.b.b.f2105a;
                        if (bVar != null) {
                            bVar.b();
                        }
                        this.b.b.notifyItemChanged(adapterPosition);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements ej1<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ j06 c;

            @DexIgnore
            public c(a aVar, j06 j06) {
                this.b = aVar;
                this.c = j06;
            }

            @DexIgnore
            /* renamed from: a */
            public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
                FLogger.INSTANCE.getLocal().d(kw5.e, "renderContactData onResourceReady");
                this.b.f2106a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            @Override // com.fossil.ej1
            public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
                FLogger.INSTANCE.getLocal().d(kw5.e, "renderContactData onLoadFailed");
                Contact contact = this.c.getContact();
                if (contact == null) {
                    return false;
                }
                contact.setPhotoThumbUri(null);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(kw5 kw5, ff5 ff5) {
            super(ff5.n());
            pq7.c(ff5, "binding");
            this.b = kw5;
            this.f2106a = ff5;
            String d = qn5.l.a().d("nonBrandSeparatorLine");
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d)) {
                this.f2106a.v.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                this.f2106a.q.setBackgroundColor(Color.parseColor(d2));
            }
            this.f2106a.q.setOnClickListener(new View$OnClickListenerC0138a(this));
            this.f2106a.u.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void b(i06 i06) {
            pq7.c(i06, "appWrapper");
            wj5 b2 = tj5.b(this.f2106a.t);
            InstalledApp installedApp = i06.getInstalledApp();
            if (installedApp != null) {
                b2.I(new qj5(installedApp)).u0(((fj1) new fj1().o0(new hk5())).n()).F0(this.f2106a.t);
                FlexibleTextView flexibleTextView = this.f2106a.s;
                pq7.b(flexibleTextView, "binding.ftvHybridName");
                InstalledApp installedApp2 = i06.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                FlexibleTextView flexibleTextView2 = this.f2106a.r;
                pq7.b(flexibleTextView2, "binding.ftvHybridFeature");
                flexibleTextView2.setVisibility(8);
                InstalledApp installedApp3 = i06.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected == null) {
                    pq7.i();
                    throw null;
                } else if (isSelected.booleanValue()) {
                    this.f2106a.u.setImageResource(2131231155);
                } else {
                    this.f2106a.u.setImageResource(2131231123);
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final void c(j06 j06) {
            Uri uri;
            String str;
            String str2;
            String str3;
            Contact contact;
            String str4;
            pq7.c(j06, "contactWrapper");
            Contact contact2 = j06.getContact();
            if (!TextUtils.isEmpty(contact2 != null ? contact2.getPhotoThumbUri() : null)) {
                Contact contact3 = j06.getContact();
                uri = Uri.parse(contact3 != null ? contact3.getPhotoThumbUri() : null);
            } else {
                uri = null;
            }
            Contact contact4 = j06.getContact();
            if (contact4 == null || (str = contact4.getFirstName()) == null) {
                str = "";
            }
            Contact contact5 = j06.getContact();
            if (contact5 == null || (str2 = contact5.getLastName()) == null) {
                str2 = "";
            }
            Contact contact6 = j06.getContact();
            if (contact6 == null || contact6.getContactId() != -100) {
                Contact contact7 = j06.getContact();
                if (contact7 == null || contact7.getContactId() != -200) {
                    String str5 = str + " " + str2;
                    wj5 b2 = tj5.b(this.f2106a.t);
                    Contact contact8 = j06.getContact();
                    vj5<Drawable> T0 = b2.I(new sj5(uri, contact8 != null ? contact8.getDisplayName() : null)).u0(this.b.d);
                    wj5 b3 = tj5.b(this.f2106a.t);
                    Contact contact9 = j06.getContact();
                    pq7.b(T0.a1(b3.I(new sj5((Uri) null, contact9 != null ? contact9.getDisplayName() : null)).u0(this.b.d)).b1(new c(this, j06)).F0(this.f2106a.t), "GlideApp.with(binding.iv\u2026nto(binding.ivHybridIcon)");
                    str3 = str5;
                } else {
                    this.f2106a.t.setImageDrawable(gl0.f(PortfolioApp.h0.c(), 2131231113));
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886155);
                    pq7.b(str3, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
                }
            } else {
                this.f2106a.t.setImageDrawable(gl0.f(PortfolioApp.h0.c(), 2131231112));
                str3 = um5.c(PortfolioApp.h0.c(), 2131886154);
                pq7.b(str3, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
            }
            FlexibleTextView flexibleTextView = this.f2106a.s;
            pq7.b(flexibleTextView, "binding.ftvHybridName");
            flexibleTextView.setText(str3);
            Contact contact10 = j06.getContact();
            if ((contact10 == null || contact10.getContactId() != -100) && ((contact = j06.getContact()) == null || contact.getContactId() != -200)) {
                Contact contact11 = j06.getContact();
                Boolean valueOf = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                if (valueOf != null) {
                    if (valueOf.booleanValue()) {
                        Contact contact12 = j06.getContact();
                        Boolean valueOf2 = contact12 != null ? Boolean.valueOf(contact12.isUseCall()) : null;
                        if (valueOf2 == null) {
                            pq7.i();
                            throw null;
                        } else if (valueOf2.booleanValue()) {
                            str4 = um5.c(PortfolioApp.h0.c(), 2131886147);
                            pq7.b(str4, "LanguageHelper.getString\u2026pped_Text__CallsMessages)");
                            FlexibleTextView flexibleTextView2 = this.f2106a.r;
                            pq7.b(flexibleTextView2, "binding.ftvHybridFeature");
                            flexibleTextView2.setText(str4);
                            FlexibleTextView flexibleTextView3 = this.f2106a.r;
                            pq7.b(flexibleTextView3, "binding.ftvHybridFeature");
                            flexibleTextView3.setVisibility(0);
                        }
                    }
                    Contact contact13 = j06.getContact();
                    Boolean valueOf3 = contact13 != null ? Boolean.valueOf(contact13.isUseSms()) : null;
                    if (valueOf3 != null) {
                        if (valueOf3.booleanValue()) {
                            str4 = um5.c(PortfolioApp.h0.c(), 2131886148);
                            pq7.b(str4, "LanguageHelper.getString\u2026nedTapped_Text__Messages)");
                        } else {
                            str4 = "";
                        }
                        Contact contact14 = j06.getContact();
                        Boolean valueOf4 = contact14 != null ? Boolean.valueOf(contact14.isUseCall()) : null;
                        if (valueOf4 != null) {
                            if (valueOf4.booleanValue()) {
                                str4 = um5.c(PortfolioApp.h0.c(), 2131886146);
                                pq7.b(str4, "LanguageHelper.getString\u2026signedTapped_Text__Calls)");
                            }
                            FlexibleTextView flexibleTextView22 = this.f2106a.r;
                            pq7.b(flexibleTextView22, "binding.ftvHybridFeature");
                            flexibleTextView22.setText(str4);
                            FlexibleTextView flexibleTextView32 = this.f2106a.r;
                            pq7.b(flexibleTextView32, "binding.ftvHybridFeature");
                            flexibleTextView32.setVisibility(0);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                FlexibleTextView flexibleTextView4 = this.f2106a.r;
                pq7.b(flexibleTextView4, "binding.ftvHybridFeature");
                flexibleTextView4.setText("");
                FlexibleTextView flexibleTextView5 = this.f2106a.r;
                pq7.b(flexibleTextView5, "binding.ftvHybridFeature");
                flexibleTextView5.setVisibility(8);
            }
            if (j06.isAdded()) {
                this.f2106a.u.setImageResource(2131231155);
            } else {
                this.f2106a.u.setImageResource(2131231123);
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(j06 j06);

        @DexIgnore
        Object b();  // void declaration
    }

    /*
    static {
        String name = kw5.class.getName();
        pq7.b(name, "NotificationContactsAndA\u2026dAdapter::class.java.name");
        e = name;
    }
    */

    @DexIgnore
    public kw5() {
        yi1 m0 = ((fj1) ((fj1) ((fj1) new fj1().o0(new hk5())).n()).l(wc1.f3916a)).m0(true);
        pq7.b(m0, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        this.d = (fj1) m0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends Object> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final boolean m() {
        return getItemCount() + this.c <= 12;
    }

    @DexIgnore
    /* renamed from: n */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        List<? extends Object> list = this.b;
        if (list != null) {
            Object obj = list.get(i);
            if (obj instanceof j06) {
                aVar.c((j06) obj);
            } else if (obj != null) {
                aVar.b((i06) obj);
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: o */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        ff5 z = ff5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemNotificationHybridBi\u2026.context), parent, false)");
        return new a(this, z);
    }

    @DexIgnore
    public final void p(List<? extends Object> list) {
        pq7.c(list, "data");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void q(b bVar) {
        pq7.c(bVar, "listener");
        this.f2105a = bVar;
    }
}
