package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class os0 extends Fragment {
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object onResume();  // void declaration

        @DexIgnore
        Object onStart();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
        }

        @DexIgnore
        public void onActivityPostCreated(Activity activity, Bundle bundle) {
            os0.a(activity, Lifecycle.a.ON_CREATE);
        }

        @DexIgnore
        public void onActivityPostResumed(Activity activity) {
            os0.a(activity, Lifecycle.a.ON_RESUME);
        }

        @DexIgnore
        public void onActivityPostStarted(Activity activity) {
            os0.a(activity, Lifecycle.a.ON_START);
        }

        @DexIgnore
        public void onActivityPreDestroyed(Activity activity) {
            os0.a(activity, Lifecycle.a.ON_DESTROY);
        }

        @DexIgnore
        public void onActivityPrePaused(Activity activity) {
            os0.a(activity, Lifecycle.a.ON_PAUSE);
        }

        @DexIgnore
        public void onActivityPreStopped(Activity activity) {
            os0.a(activity, Lifecycle.a.ON_STOP);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static void a(Activity activity, Lifecycle.a aVar) {
        if (activity instanceof es0) {
            ((es0) activity).getLifecycle().i(aVar);
        } else if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).i(aVar);
            }
        }
    }

    @DexIgnore
    public static os0 f(Activity activity) {
        return (os0) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }

    @DexIgnore
    public static void g(Activity activity) {
        if (Build.VERSION.SDK_INT >= 29) {
            activity.registerActivityLifecycleCallbacks(new b());
        }
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new os0(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    @DexIgnore
    public final void b(Lifecycle.a aVar) {
        if (Build.VERSION.SDK_INT < 29) {
            a(getActivity(), aVar);
        }
    }

    @DexIgnore
    public final void c(a aVar) {
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public final void d(a aVar) {
        if (aVar != null) {
            aVar.onResume();
        }
    }

    @DexIgnore
    public final void e(a aVar) {
        if (aVar != null) {
            aVar.onStart();
        }
    }

    @DexIgnore
    public void h(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        c(this.b);
        b(Lifecycle.a.ON_CREATE);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        b(Lifecycle.a.ON_DESTROY);
        this.b = null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        b(Lifecycle.a.ON_PAUSE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        d(this.b);
        b(Lifecycle.a.ON_RESUME);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        e(this.b);
        b(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        b(Lifecycle.a.ON_STOP);
    }
}
