package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cu0<T> extends AbstractList<T> {
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ Executor c;
    @DexIgnore
    public /* final */ c<T> d;
    @DexIgnore
    public /* final */ f e;
    @DexIgnore
    public /* final */ eu0<T> f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public T h; // = null;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public int l; // = Integer.MAX_VALUE;
    @DexIgnore
    public int m; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public /* final */ AtomicBoolean s; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ArrayList<WeakReference<e>> t; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public a(boolean z, boolean z2, boolean z3) {
            this.b = z;
            this.c = z2;
            this.d = z3;
        }

        @DexIgnore
        public void run() {
            if (this.b) {
                cu0.this.d.c();
            }
            if (this.c) {
                cu0.this.j = true;
            }
            if (this.d) {
                cu0.this.k = true;
            }
            cu0.this.E(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(boolean z, boolean z2) {
            this.b = z;
            this.c = z2;
        }

        @DexIgnore
        public void run() {
            cu0.this.n(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<T> {
        @DexIgnore
        public abstract void a(T t);

        @DexIgnore
        public abstract void b(T t);

        @DexIgnore
        public abstract void c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<Key, Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0<Key, Value> f655a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public Executor c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public c e;
        @DexIgnore
        public Key f;

        @DexIgnore
        public d(xt0<Key, Value> xt0, f fVar) {
            if (xt0 == null) {
                throw new IllegalArgumentException("DataSource may not be null");
            } else if (fVar != null) {
                this.f655a = xt0;
                this.b = fVar;
            } else {
                throw new IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        public cu0<Value> a() {
            Executor executor = this.c;
            if (executor != null) {
                Executor executor2 = this.d;
                if (executor2 != null) {
                    return cu0.k(this.f655a, executor, executor2, this.e, this.b, this.f);
                }
                throw new IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new IllegalArgumentException("MainThreadExecutor required");
        }

        @DexIgnore
        public d<Key, Value> b(c cVar) {
            this.e = cVar;
            return this;
        }

        @DexIgnore
        public d<Key, Value> c(Executor executor) {
            this.d = executor;
            return this;
        }

        @DexIgnore
        public d<Key, Value> d(Key key) {
            this.f = key;
            return this;
        }

        @DexIgnore
        public d<Key, Value> e(Executor executor) {
            this.c = executor;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public abstract void a(int i, int i2);

        @DexIgnore
        public abstract void b(int i, int i2);

        @DexIgnore
        public abstract void c(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f656a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public int f657a; // = -1;
            @DexIgnore
            public int b; // = -1;
            @DexIgnore
            public int c; // = -1;
            @DexIgnore
            public boolean d; // = true;
            @DexIgnore
            public int e; // = Integer.MAX_VALUE;

            @DexIgnore
            public f a() {
                if (this.b < 0) {
                    this.b = this.f657a;
                }
                if (this.c < 0) {
                    this.c = this.f657a * 3;
                }
                if (this.d || this.b != 0) {
                    int i = this.e;
                    if (i == Integer.MAX_VALUE || i >= this.f657a + (this.b * 2)) {
                        return new f(this.f657a, this.b, this.d, this.c, this.e);
                    }
                    throw new IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.f657a + ", prefetchDist=" + this.b + ", maxSize=" + this.e);
                }
                throw new IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }

            @DexIgnore
            public a b(boolean z) {
                this.d = z;
                return this;
            }

            @DexIgnore
            public a c(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public a d(int i) {
                if (i >= 1) {
                    this.f657a = i;
                    return this;
                }
                throw new IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            public a e(int i) {
                this.b = i;
                return this;
            }
        }

        @DexIgnore
        public f(int i, int i2, boolean z, int i3, int i4) {
            this.f656a = i;
            this.b = i2;
            this.c = z;
            this.e = i3;
            this.d = i4;
        }
    }

    @DexIgnore
    public cu0(eu0<T> eu0, Executor executor, Executor executor2, c<T> cVar, f fVar) {
        this.f = eu0;
        this.b = executor;
        this.c = executor2;
        this.d = cVar;
        this.e = fVar;
        this.i = (fVar.b * 2) + fVar.f656a;
    }

    @DexIgnore
    public static <K, T> cu0<T> k(xt0<K, T> xt0, Executor executor, Executor executor2, c<T> cVar, f fVar, K k2) {
        int i2;
        xt0<K, T> xt02;
        if (xt0.isContiguous() || !fVar.c) {
            if (!xt0.isContiguous()) {
                xt0 = ((gu0) xt0).wrapAsContiguousWithoutPlaceholders();
                if (k2 != null) {
                    i2 = k2.intValue();
                    xt02 = xt0;
                    return new wt0((vt0) xt02, executor, executor2, cVar, fVar, k2, i2);
                }
            }
            i2 = -1;
            xt02 = xt0;
            return new wt0((vt0) xt02, executor, executor2, cVar, fVar, k2, i2);
        }
        return new iu0((gu0) xt0, executor, executor2, cVar, fVar, k2 != null ? k2.intValue() : 0);
    }

    @DexIgnore
    public void A(int i2) {
        this.g += i2;
        this.l += i2;
        this.m += i2;
    }

    @DexIgnore
    public void B(e eVar) {
        for (int size = this.t.size() - 1; size >= 0; size--) {
            e eVar2 = this.t.get(size).get();
            if (eVar2 == null || eVar2 == eVar) {
                this.t.remove(size);
            }
        }
    }

    @DexIgnore
    public List<T> D() {
        return u() ? this : new hu0(this);
    }

    @DexIgnore
    public void E(boolean z) {
        boolean z2 = true;
        boolean z3 = this.j && this.l <= this.e.b;
        if (!this.k || this.m < (size() - 1) - this.e.b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.j = false;
            }
            if (z2) {
                this.k = false;
            }
            if (z) {
                this.b.execute(new b(z3, z2));
            } else {
                n(z3, z2);
            }
        }
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public T get(int i2) {
        T t2 = this.f.get(i2);
        if (t2 != null) {
            this.h = t2;
        }
        return t2;
    }

    @DexIgnore
    public void j(List<T> list, e eVar) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                o((cu0) list, eVar);
            } else if (!this.f.isEmpty()) {
                eVar.b(0, this.f.size());
            }
        }
        for (int size = this.t.size() - 1; size >= 0; size--) {
            if (this.t.get(size).get() == null) {
                this.t.remove(size);
            }
        }
        this.t.add(new WeakReference<>(eVar));
    }

    @DexIgnore
    public void l(boolean z, boolean z2, boolean z3) {
        if (this.d != null) {
            if (this.l == Integer.MAX_VALUE) {
                this.l = this.f.size();
            }
            if (this.m == Integer.MIN_VALUE) {
                this.m = 0;
            }
            if (z || z2 || z3) {
                this.b.execute(new a(z, z2, z3));
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    public void m() {
        this.s.set(true);
    }

    @DexIgnore
    public void n(boolean z, boolean z2) {
        if (z) {
            this.d.b(this.f.f());
        }
        if (z2) {
            this.d.a(this.f.g());
        }
    }

    @DexIgnore
    public abstract void o(cu0<T> cu0, e eVar);

    @DexIgnore
    public abstract xt0<?, T> p();

    @DexIgnore
    public abstract Object q();

    @DexIgnore
    public int r() {
        return this.f.m();
    }

    @DexIgnore
    public abstract boolean s();

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    public boolean t() {
        return this.s.get();
    }

    @DexIgnore
    public boolean u() {
        return t();
    }

    @DexIgnore
    public void v(int i2) {
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        this.g = r() + i2;
        w(i2);
        this.l = Math.min(this.l, i2);
        this.m = Math.max(this.m, i2);
        E(true);
    }

    @DexIgnore
    public abstract void w(int i2);

    @DexIgnore
    public void x(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                e eVar = this.t.get(size).get();
                if (eVar != null) {
                    eVar.a(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void y(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                e eVar = this.t.get(size).get();
                if (eVar != null) {
                    eVar.b(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void z(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                e eVar = this.t.get(size).get();
                if (eVar != null) {
                    eVar.c(i2, i3);
                }
            }
        }
    }
}
