package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class er4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ArrayList<Ringtone> f979a; // = new ArrayList<>();
    @DexIgnore
    public Ringtone b;
    @DexIgnore
    public /* final */ a c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void p2(Ringtone ringtone);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ImageView f980a;
        @DexIgnore
        public View b;
        @DexIgnore
        public /* final */ TextView c;
        @DexIgnore
        public /* final */ ConstraintLayout d;
        @DexIgnore
        public /* final */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ er4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                er4 er4 = this.b.f;
                Object obj = er4.f979a.get(this.b.getAdapterPosition());
                pq7.b(obj, "mRingPhones[adapterPosition]");
                er4.b = (Ringtone) obj;
                this.b.f.c.p2(er4.i(this.b.f));
                this.b.f.notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(er4 er4, View view) {
            super(view);
            pq7.c(view, "view");
            this.f = er4;
            View findViewById = view.findViewById(2131362747);
            pq7.b(findViewById, "view.findViewById(R.id.iv_ringphone)");
            this.f980a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131362748);
            pq7.b(findViewById2, "view.findViewById(R.id.iv_ringphone_selected)");
            this.b = findViewById2;
            View findViewById3 = view.findViewById(2131363386);
            pq7.b(findViewById3, "view.findViewById(R.id.tv_ring_phone)");
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(2131363010);
            pq7.b(findViewById4, "view.findViewById(R.id.root_background)");
            this.d = (ConstraintLayout) findViewById4;
            View findViewById5 = view.findViewById(2131362632);
            pq7.b(findViewById5, "view.findViewById(R.id.indicator)");
            this.e = findViewById5;
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                this.d.setBackgroundColor(Color.parseColor(d2));
            }
            String d3 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d3)) {
                this.e.setBackgroundColor(Color.parseColor(d3));
            }
            this.f980a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(Ringtone ringtone) {
            pq7.c(ringtone, "ringTone");
            this.c.setText(ringtone.getRingtoneName());
            this.b.setVisibility(0);
            if (pq7.a(ringtone.getRingtoneName(), er4.i(this.f).getRingtoneName())) {
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public er4(a aVar) {
        pq7.c(aVar, "mListener");
        this.c = aVar;
    }

    @DexIgnore
    public static final /* synthetic */ Ringtone i(er4 er4) {
        Ringtone ringtone = er4.b;
        if (ringtone != null) {
            return ringtone;
        }
        pq7.n("mSelectedRingPhone");
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f979a.size();
    }

    @DexIgnore
    public final void k(List<Ringtone> list, Ringtone ringtone) {
        pq7.c(list, "data");
        pq7.c(ringtone, "selectedRingtone");
        this.f979a.clear();
        this.f979a.addAll(list);
        this.b = ringtone;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        int adapterPosition = bVar.getAdapterPosition();
        if (getItemCount() > adapterPosition && adapterPosition != -1) {
            Ringtone ringtone = this.f979a.get(adapterPosition);
            pq7.b(ringtone, "mRingPhones[adapterPos]");
            bVar.a(ringtone);
        }
    }

    @DexIgnore
    /* renamed from: m */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558712, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026ing_phone, parent, false)");
        return new b(this, inflate);
    }
}
