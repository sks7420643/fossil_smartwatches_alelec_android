package com.fossil;

import android.graphics.Bitmap;
import com.fossil.s87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b97 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ConcurrentLinkedQueue<s87.b> f406a; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public static /* final */ Map<String, Bitmap> b; // = new LinkedHashMap();
    @DexIgnore
    public static /* final */ b97 c; // = new b97();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.f(), t2.f());
        }
    }

    @DexIgnore
    public final void a(String str, Bitmap bitmap) {
        pq7.c(str, "stickerName");
        pq7.c(bitmap, "bitmap");
        b.put(str, bitmap);
    }

    @DexIgnore
    public final void b() {
        b.clear();
        f406a.clear();
    }

    @DexIgnore
    public final Bitmap c(String str) {
        return b.get(str);
    }

    @DexIgnore
    public final boolean d() {
        return !f406a.isEmpty();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0055, code lost:
        if (r1 != null) goto L_0x0057;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.s87.b e(java.lang.String r10) {
        /*
            r9 = this;
            r5 = 0
            r2 = 0
            java.lang.String r0 = "name"
            com.fossil.pq7.c(r10, r0)
            java.util.concurrent.ConcurrentLinkedQueue<com.fossil.s87$b> r0 = com.fossil.b97.f406a
            java.util.Iterator r3 = r0.iterator()
        L_0x000d:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0080
            java.lang.Object r1 = r3.next()
            r0 = r1
            com.fossil.s87$b r0 = (com.fossil.s87.b) r0
            java.util.List r0 = r0.m()
            boolean r4 = r0 instanceof java.util.Collection
            if (r4 == 0) goto L_0x0064
            boolean r4 = r0.isEmpty()
            if (r4 == 0) goto L_0x0064
        L_0x0028:
            r0 = r5
        L_0x0029:
            if (r0 == 0) goto L_0x000d
            r0 = r1
        L_0x002c:
            com.fossil.s87$b r0 = (com.fossil.s87.b) r0
            if (r0 == 0) goto L_0x0088
            java.util.List r1 = r0.m()
            if (r1 == 0) goto L_0x0088
            java.util.Iterator r4 = r1.iterator()
            r3 = r5
        L_0x003b:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x0086
            java.lang.Object r1 = r4.next()
            com.fossil.x87 r1 = (com.fossil.x87) r1
            java.lang.String r1 = r1.b()
            boolean r1 = com.fossil.pq7.a(r1, r10)
            if (r1 == 0) goto L_0x0082
        L_0x0051:
            com.fossil.o87 r1 = com.fossil.r87.a(r3)
            if (r1 == 0) goto L_0x0088
        L_0x0057:
            if (r0 == 0) goto L_0x0063
            r7 = 62
            r3 = r2
            r4 = r2
            r6 = r5
            r8 = r2
            com.fossil.s87$b r2 = com.fossil.s87.b.g(r0, r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0063:
            return r2
        L_0x0064:
            java.util.Iterator r4 = r0.iterator()
        L_0x0068:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r4.next()
            com.fossil.x87 r0 = (com.fossil.x87) r0
            java.lang.String r0 = r0.b()
            boolean r0 = com.fossil.pq7.a(r0, r10)
            if (r0 == 0) goto L_0x0068
            r0 = 1
            goto L_0x0029
        L_0x0080:
            r0 = r2
            goto L_0x002c
        L_0x0082:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x003b
        L_0x0086:
            r3 = -1
            goto L_0x0051
        L_0x0088:
            com.fossil.o87$a r1 = com.fossil.o87.Companion
            com.fossil.o87 r1 = r1.a()
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.b97.e(java.lang.String):com.fossil.s87$b");
    }

    @DexIgnore
    public final ConcurrentLinkedQueue<s87.b> f() {
        return f406a;
    }

    @DexIgnore
    public final void g(List<p77> list) {
        pq7.c(list, "stickerAssets");
        List b0 = pm7.b0(list, new a());
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : b0) {
            String p0 = wt7.p0(((p77) obj).f(), ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, null, 2, null);
            Object obj2 = linkedHashMap.get(p0);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(p0, obj2);
            }
            ((List) obj2).add(obj);
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            List list2 = (List) entry.getValue();
            ArrayList arrayList = new ArrayList();
            int size = list2.size();
            for (int i = 0; i < size; i++) {
                p77 p77 = (p77) list2.get(i);
                arrayList.add(new x87(p77.f(), p77.c().a()));
            }
            f406a.add(new s87.b(o87.Companion.a(), arrayList, null, null, 0, 0, 60, null));
        }
        FLogger.INSTANCE.getLocal().d("WatchFaceStickerStorage", "init " + f406a);
    }
}
