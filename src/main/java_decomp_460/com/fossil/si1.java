package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class si1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<a<?>> f3264a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<T> f3265a;
        @DexIgnore
        public /* final */ jb1<T> b;

        @DexIgnore
        public a(Class<T> cls, jb1<T> jb1) {
            this.f3265a = cls;
            this.b = jb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.f3265a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public <T> void a(Class<T> cls, jb1<T> jb1) {
        synchronized (this) {
            this.f3264a.add(new a<>(cls, jb1));
        }
    }

    @DexIgnore
    public <T> jb1<T> b(Class<T> cls) {
        synchronized (this) {
            for (a<?> aVar : this.f3264a) {
                if (aVar.a(cls)) {
                    return aVar.b;
                }
            }
            return null;
        }
    }
}
