package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f2258a;

    @DexIgnore
    public lw2(kw2 kw2) {
        sw2.c(kw2, "BuildInfo must be non-null");
        this.f2258a = !kw2.zza();
    }

    @DexIgnore
    public final boolean a(String str) {
        sw2.c(str, "flagName must not be null");
        if (!this.f2258a) {
            return true;
        }
        return nw2.f2588a.zza().zza(str);
    }
}
