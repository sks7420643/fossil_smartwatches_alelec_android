package com.fossil;

import com.fossil.fi0;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ei0<K, V> extends fi0<K, V> {
    @DexIgnore
    public HashMap<K, fi0.c<K, V>> f; // = new HashMap<>();

    @DexIgnore
    @Override // com.fossil.fi0
    public fi0.c<K, V> b(K k) {
        return this.f.get(k);
    }

    @DexIgnore
    public boolean contains(K k) {
        return this.f.containsKey(k);
    }

    @DexIgnore
    @Override // com.fossil.fi0
    public V f(K k, V v) {
        fi0.c<K, V> b = b(k);
        if (b != null) {
            return b.c;
        }
        this.f.put(k, e(k, v));
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fi0
    public V g(K k) {
        V v = (V) super.g(k);
        this.f.remove(k);
        return v;
    }

    @DexIgnore
    public Map.Entry<K, V> h(K k) {
        if (contains(k)) {
            return this.f.get(k).e;
        }
        return null;
    }
}
