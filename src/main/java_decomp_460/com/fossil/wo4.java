package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo4 implements Factory<ck5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3977a;

    @DexIgnore
    public wo4(uo4 uo4) {
        this.f3977a = uo4;
    }

    @DexIgnore
    public static wo4 a(uo4 uo4) {
        return new wo4(uo4);
    }

    @DexIgnore
    public static ck5 c(uo4 uo4) {
        ck5 c = uo4.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    /* renamed from: b */
    public ck5 get() {
        return c(this.f3977a);
    }
}
