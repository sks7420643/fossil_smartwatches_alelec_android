package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c81 extends f81 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f579a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public c81(int i, int i2) {
        super(null);
        this.f579a = i;
        this.b = i2;
        if (!(i > 0 && i2 > 0)) {
            throw new IllegalArgumentException("Width and height must be > 0.".toString());
        }
    }

    @DexIgnore
    public final int a() {
        return this.f579a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.f579a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof c81) {
                c81 c81 = (c81) obj;
                if (!(this.f579a == c81.f579a && this.b == c81.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f579a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "PixelSize(width=" + this.f579a + ", height=" + this.b + ")";
    }
}
