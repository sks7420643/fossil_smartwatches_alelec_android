package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx5 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<ContactGroup> f1235a; // = new ArrayList();
    @DexIgnore
    public a b;
    @DexIgnore
    public /* final */ fj1 c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(ContactGroup contactGroup);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ le5 f1236a;
        @DexIgnore
        public /* final */ /* synthetic */ fx5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar = this.b.b.b;
                if (aVar != null) {
                    aVar.b();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fx5$b$b")
        /* renamed from: com.fossil.fx5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0093b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0093b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (aVar = this.b.b.b) != null) {
                    aVar.a((ContactGroup) this.b.b.f1235a.get(adapterPosition));
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements ej1<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ b b;
            @DexIgnore
            public /* final */ /* synthetic */ ContactGroup c;

            @DexIgnore
            public c(b bVar, ContactGroup contactGroup) {
                this.b = bVar;
                this.c = contactGroup;
            }

            @DexIgnore
            /* renamed from: a */
            public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onResourceReady");
                this.b.f1236a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            @Override // com.fossil.ej1
            public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onLoadFailed");
                Contact contact = this.c.getContacts().get(0);
                pq7.b(contact, "contactGroup.contacts[0]");
                contact.setPhotoThumbUri(null);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fx5 fx5, le5 le5) {
            super(le5.n());
            pq7.c(le5, "binding");
            this.b = fx5;
            this.f1236a = le5;
            String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                this.f1236a.q.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                this.f1236a.u.setBackgroundColor(Color.parseColor(d2));
            }
            this.f1236a.q.setOnClickListener(new a(this));
            this.f1236a.s.setOnClickListener(new View$OnClickListenerC0093b(this));
        }

        @DexIgnore
        public final void b(ContactGroup contactGroup) {
            Uri uri;
            pq7.c(contactGroup, "contactGroup");
            FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "set favorite contact data");
            if (contactGroup.getContacts().size() > 0) {
                Contact contact = contactGroup.getContacts().get(0);
                pq7.b(contact, "contactGroup.contacts[0]");
                if (!TextUtils.isEmpty(contact.getPhotoThumbUri())) {
                    Contact contact2 = contactGroup.getContacts().get(0);
                    pq7.b(contact2, "contactGroup.contacts[0]");
                    uri = Uri.parse(contact2.getPhotoThumbUri());
                } else {
                    uri = null;
                }
                FossilNotificationImageView fossilNotificationImageView = this.f1236a.t;
                pq7.b(fossilNotificationImageView, "binding.ivFavoriteContactIcon");
                wj5 a2 = tj5.a(fossilNotificationImageView.getContext());
                Contact contact3 = contactGroup.getContacts().get(0);
                pq7.b(contact3, "contactGroup.contacts[0]");
                vj5<Drawable> T0 = a2.I(new sj5(uri, contact3.getDisplayName())).m0(true).l(wc1.f3916a).u0(this.b.c);
                FossilNotificationImageView fossilNotificationImageView2 = this.f1236a.t;
                pq7.b(fossilNotificationImageView2, "binding.ivFavoriteContactIcon");
                wj5 a3 = tj5.a(fossilNotificationImageView2.getContext());
                Contact contact4 = contactGroup.getContacts().get(0);
                pq7.b(contact4, "contactGroup.contacts[0]");
                vj5<Drawable> b1 = T0.a1(a3.I(new sj5((Uri) null, contact4.getDisplayName())).u0(this.b.c)).b1(new c(this, contactGroup));
                FossilNotificationImageView fossilNotificationImageView3 = this.f1236a.t;
                pq7.b(fossilNotificationImageView3, "binding.ivFavoriteContactIcon");
                b1.F0(fossilNotificationImageView3.getFossilCircleImageView());
                Contact contact5 = contactGroup.getContacts().get(0);
                pq7.b(contact5, "contactGroup.contacts[0]");
                if (TextUtils.isEmpty(contact5.getPhotoThumbUri())) {
                    this.f1236a.t.b();
                    FossilNotificationImageView fossilNotificationImageView4 = this.f1236a.t;
                    pq7.b(fossilNotificationImageView4, "binding.ivFavoriteContactIcon");
                    fossilNotificationImageView4.setBackground(gl0.f(PortfolioApp.h0.c(), 2131231287));
                } else {
                    this.f1236a.t.h();
                    this.f1236a.t.setBackgroundResource(R.color.transparent);
                }
                FlexibleTextView flexibleTextView = this.f1236a.r;
                pq7.b(flexibleTextView, "binding.ftvFavoriteContactName");
                Contact contact6 = contactGroup.getContacts().get(0);
                pq7.b(contact6, "contactGroup.contacts[0]");
                flexibleTextView.setText(contact6.getDisplayName());
            }
        }
    }

    @DexIgnore
    public fx5() {
        yi1 o0 = new fj1().o0(new hk5());
        pq7.b(o0, "RequestOptions().transform(CircleTransform())");
        this.c = (fj1) o0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1235a.size();
    }

    @DexIgnore
    public final List<ContactGroup> j() {
        return this.f1235a;
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        bVar.b(this.f1235a.get(i));
    }

    @DexIgnore
    /* renamed from: l */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        le5 z = le5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemFavoriteContactNotif\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0075 A[LOOP:0: B:1:0x000e->B:14:0x0075, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0068 A[EDGE_INSN: B:17:0x0068->B:11:0x0068 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m(com.fossil.wearables.fsl.contact.ContactGroup r9) {
        /*
            r8 = this;
            r3 = -1
            r2 = 0
            java.lang.String r0 = "contactGroup"
            com.fossil.pq7.c(r9, r0)
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r0 = r8.f1235a
            java.util.Iterator r5 = r0.iterator()
            r1 = r2
        L_0x000e:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r0 = r5.next()
            com.fossil.wearables.fsl.contact.ContactGroup r0 = (com.fossil.wearables.fsl.contact.ContactGroup) r0
            java.util.List r4 = r0.getContacts()
            java.lang.String r6 = "it.contacts"
            com.fossil.pq7.b(r4, r6)
            boolean r6 = r4.isEmpty()
            r4 = 1
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x0073
            java.util.List r6 = r9.getContacts()
            java.lang.String r7 = "contactGroup.contacts"
            com.fossil.pq7.b(r6, r7)
            boolean r6 = r6.isEmpty()
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x0073
            java.util.List r0 = r0.getContacts()
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r6 = "it.contacts[0]"
            com.fossil.pq7.b(r0, r6)
            com.fossil.wearables.fsl.contact.Contact r0 = (com.fossil.wearables.fsl.contact.Contact) r0
            int r6 = r0.getContactId()
            java.util.List r0 = r9.getContacts()
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r7 = "contactGroup.contacts[0]"
            com.fossil.pq7.b(r0, r7)
            com.fossil.wearables.fsl.contact.Contact r0 = (com.fossil.wearables.fsl.contact.Contact) r0
            int r0 = r0.getContactId()
            if (r6 != r0) goto L_0x0073
            r0 = r4
        L_0x0066:
            if (r0 == 0) goto L_0x0075
        L_0x0068:
            if (r1 == r3) goto L_0x0072
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r0 = r8.f1235a
            r0.remove(r1)
            r8.notifyItemRemoved(r1)
        L_0x0072:
            return
        L_0x0073:
            r0 = r2
            goto L_0x0066
        L_0x0075:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000e
        L_0x0079:
            r1 = r3
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fx5.m(com.fossil.wearables.fsl.contact.ContactGroup):void");
    }

    @DexIgnore
    public final void n(List<ContactGroup> list) {
        pq7.c(list, "listContactGroup");
        this.f1235a.clear();
        this.f1235a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }
}
