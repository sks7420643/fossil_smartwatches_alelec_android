package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.zendesk.sdk.support.help.HelpRecyclerViewAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r21 extends s21<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = x01.f("BatteryNotLowTracker");

    @DexIgnore
    public r21(Context context, k41 k41) {
        super(context, k41);
    }

    @DexIgnore
    @Override // com.fossil.s21
    public IntentFilter g() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004f, code lost:
        if (r2.equals("android.intent.action.BATTERY_OKAY") != false) goto L_0x0035;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0037  */
    @Override // com.fossil.s21
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h(android.content.Context r8, android.content.Intent r9) {
        /*
            r7 = this;
            r1 = 1
            r0 = 0
            java.lang.String r2 = r9.getAction()
            if (r2 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            com.fossil.x01 r2 = com.fossil.x01.c()
            java.lang.String r3 = com.fossil.r21.i
            java.lang.String r4 = r9.getAction()
            java.lang.String r5 = "Received %s"
            java.lang.Object[] r6 = new java.lang.Object[r1]
            r6[r0] = r4
            java.lang.String r4 = java.lang.String.format(r5, r6)
            java.lang.Throwable[] r5 = new java.lang.Throwable[r0]
            r2.a(r3, r4, r5)
            java.lang.String r2 = r9.getAction()
            int r3 = r2.hashCode()
            r4 = -1980154005(0xffffffff89f93f6b, float:-6.0004207E-33)
            if (r3 == r4) goto L_0x0049
            r0 = 490310653(0x1d398bfd, float:2.4556918E-21)
            if (r3 == r0) goto L_0x003f
        L_0x0034:
            r0 = -1
        L_0x0035:
            if (r0 == 0) goto L_0x0052
            if (r0 != r1) goto L_0x0008
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r7.d(r0)
            goto L_0x0008
        L_0x003f:
            java.lang.String r0 = "android.intent.action.BATTERY_LOW"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = r1
            goto L_0x0035
        L_0x0049:
            java.lang.String r3 = "android.intent.action.BATTERY_OKAY"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0034
            goto L_0x0035
        L_0x0052:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r7.d(r0)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r21.h(android.content.Context, android.content.Intent):void");
    }

    @DexIgnore
    /* renamed from: i */
    public Boolean b() {
        boolean z = false;
        Intent registerReceiver = this.b.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            x01.c().b(i, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        float intExtra2 = ((float) registerReceiver.getIntExtra(HelpRecyclerViewAdapter.CategoryViewHolder.ROTATION_PROPERTY_NAME, -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
        if (intExtra == 1 || intExtra2 > 0.15f) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
