package com.fossil;

import com.fossil.e13;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r03 extends s03<e13.e> {
    @DexIgnore
    @Override // com.fossil.s03
    public final int a(Map.Entry<?, ?> entry) {
        e13.e eVar = (e13.e) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final t03<e13.e> b(Object obj) {
        return ((e13.b) obj).zzc;
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final Object c(q03 q03, m23 m23, int i) {
        return q03.b(m23, i);
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final void d(r43 r43, Map.Entry<?, ?> entry) throws IOException {
        e13.e eVar = (e13.e) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final boolean e(m23 m23) {
        return m23 instanceof e13.b;
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final t03<e13.e> f(Object obj) {
        return ((e13.b) obj).C();
    }

    @DexIgnore
    @Override // com.fossil.s03
    public final void g(Object obj) {
        b(obj).k();
    }
}
