package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.util.Arrays;
import java.util.StringTokenizer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CallbackManager f3619a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return un5.b;
        }

        @DexIgnore
        public final String[] b(String str) {
            pq7.c(str, "fullName");
            String[] strArr = {"", ""};
            StringTokenizer stringTokenizer = new StringTokenizer(str);
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                pq7.b(nextToken, "tokenizer.nextToken()");
                strArr[0] = nextToken;
            }
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken2 = stringTokenizer.nextToken();
                pq7.b(nextToken2, "tokenizer.nextToken()");
                strArr[1] = nextToken2;
            }
            return strArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements GraphRequest.GraphJSONObjectCallback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yn5 f3620a;
        @DexIgnore
        public /* final */ /* synthetic */ AccessToken b;

        @DexIgnore
        public b(yn5 yn5, AccessToken accessToken) {
            this.f3620a = yn5;
            this.b = accessToken;
        }

        @DexIgnore
        @Override // com.facebook.GraphRequest.GraphJSONObjectCallback
        public final void onCompleted(JSONObject jSONObject, GraphResponse graphResponse) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = un5.c.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .fetchGraphDataFacebook response=");
            sb.append(graphResponse);
            sb.append(", error=");
            pq7.b(graphResponse, "response");
            sb.append(graphResponse.getError());
            local.d(a2, sb.toString());
            if (graphResponse.getError() != null) {
                this.f3620a.b(600, null, "");
            } else if (jSONObject != null) {
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String optString = jSONObject.optString(Constants.EMAIL);
                pq7.b(optString, "me.optString(\"email\")");
                signUpSocialAuth.setEmail(optString);
                String optString2 = jSONObject.optString("name");
                String optString3 = jSONObject.optString("first_name");
                String optString4 = jSONObject.optString("last_name");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = un5.c.a();
                local2.d(a3, "Facebook email is " + jSONObject.optString(Constants.EMAIL) + " facebook name " + jSONObject.optString("name"));
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = un5.c.a();
                local3.d(a4, "Facebook first name = " + optString3 + " last name = " + optString4);
                if (!TextUtils.isEmpty(optString3) && !TextUtils.isEmpty(optString4)) {
                    pq7.b(optString3, Constants.PROFILE_KEY_FIRST_NAME);
                    signUpSocialAuth.setFirstName(optString3);
                    pq7.b(optString4, Constants.PROFILE_KEY_LAST_NAME);
                    signUpSocialAuth.setLastName(optString4);
                } else if (!TextUtils.isEmpty(optString2)) {
                    a aVar = un5.c;
                    pq7.b(optString2, "name");
                    String[] b2 = aVar.b(optString2);
                    signUpSocialAuth.setFirstName(b2[0]);
                    signUpSocialAuth.setLastName(b2[1]);
                }
                String token = this.b.getToken();
                pq7.b(token, "token.token");
                signUpSocialAuth.setToken(token);
                signUpSocialAuth.setService(Constants.FACEBOOK);
                this.f3620a.a(signUpSocialAuth);
            } else {
                this.f3620a.b(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements FacebookCallback<LoginResult> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ un5 f3621a;
        @DexIgnore
        public /* final */ /* synthetic */ yn5 b;

        @DexIgnore
        public c(un5 un5, yn5 yn5) {
            this.f3621a = un5;
            this.b = yn5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginResult loginResult) {
            pq7.c(loginResult, "loginResult");
            AccessToken accessToken = loginResult.getAccessToken();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = un5.c.a();
            local.d(a2, "Step 1: Login using facebook success token=" + accessToken);
            un5 un5 = this.f3621a;
            pq7.b(accessToken, Constants.PROFILE_KEY_ACCESS_TOKEN);
            un5.b(accessToken, this.b);
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onCancel() {
            FLogger.INSTANCE.getLocal().e(un5.c.a(), "loginWithEmail facebook is cancel");
            this.b.b(2, null, "");
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onError(FacebookException facebookException) {
            pq7.c(facebookException, "e");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = un5.c.a();
            local.e(a2, "loginWithEmail facebook fail " + facebookException.getMessage());
            this.b.b(600, null, "");
        }
    }

    /*
    static {
        String canonicalName = un5.class.getCanonicalName();
        if (canonicalName != null) {
            pq7.b(canonicalName, "MFLoginFacebookManager::class.java.canonicalName!!");
            b = canonicalName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public final void b(AccessToken accessToken, yn5 yn5) {
        pq7.c(accessToken, "token");
        pq7.c(yn5, Constants.CALLBACK);
        GraphRequest newMeRequest = GraphRequest.newMeRequest(accessToken, new b(yn5, accessToken));
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id, name, email, first_name, last_name");
        pq7.b(newMeRequest, "request");
        newMeRequest.setParameters(bundle);
        newMeRequest.executeAsync();
    }

    @DexIgnore
    public final void c(Activity activity, yn5 yn5) {
        pq7.c(activity, "activityContext");
        pq7.c(yn5, Constants.CALLBACK);
        this.f3619a = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList(Constants.EMAIL, "user_photos", "public_profile"));
        LoginManager instance = LoginManager.getInstance();
        CallbackManager callbackManager = this.f3619a;
        if (callbackManager != null) {
            instance.registerCallback(callbackManager, new c(this, yn5));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final boolean d(int i, int i2, Intent intent) {
        pq7.c(intent, "data");
        CallbackManager callbackManager = this.f3619a;
        if (callbackManager == null) {
            return false;
        }
        if (callbackManager != null) {
            boolean onActivityResult = callbackManager.onActivityResult(i, i2, intent);
            this.f3619a = null;
            return onActivityResult;
        }
        pq7.i();
        throw null;
    }
}
