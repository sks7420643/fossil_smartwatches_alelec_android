package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class r5 extends Enum<r5> {
    @DexIgnore
    public static /* final */ r5 b;
    @DexIgnore
    public static /* final */ r5 c;
    @DexIgnore
    public static /* final */ r5 d;
    @DexIgnore
    public static /* final */ r5 e;
    @DexIgnore
    public static /* final */ r5 f;
    @DexIgnore
    public static /* final */ r5 g;
    @DexIgnore
    public static /* final */ r5 h;
    @DexIgnore
    public static /* final */ r5 i;
    @DexIgnore
    public static /* final */ r5 j;
    @DexIgnore
    public static /* final */ r5 k;
    @DexIgnore
    public static /* final */ r5 l;
    @DexIgnore
    public static /* final */ r5 m;
    @DexIgnore
    public static /* final */ r5 n;
    @DexIgnore
    public static /* final */ /* synthetic */ r5[] o;
    @DexIgnore
    public static /* final */ q5 p; // = new q5(null);

    /*
    static {
        r5 r5Var = new r5("SUCCESS", 0, 0);
        b = r5Var;
        r5 r5Var2 = new r5("NOT_START", 1, 1);
        c = r5Var2;
        r5 r5Var3 = new r5("WRONG_STATE", 2, 6);
        r5 r5Var4 = new r5("GATT_ERROR", 3, 7);
        d = r5Var4;
        r5 r5Var5 = new r5("UNEXPECTED_RESULT", 4, 8);
        e = r5Var5;
        r5 r5Var6 = new r5("INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT", 5, 9);
        f = r5Var6;
        r5 r5Var7 = new r5("BLUETOOTH_OFF", 6, 10);
        g = r5Var7;
        r5 r5Var8 = new r5("UNSUPPORTED", 7, 11);
        h = r5Var8;
        r5 r5Var9 = new r5("HID_PROXY_NOT_CONNECTED", 8, 256);
        i = r5Var9;
        r5 r5Var10 = new r5("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 9, 257);
        j = r5Var10;
        r5 r5Var11 = new r5("HID_INPUT_DEVICE_DISABLED", 10, 258);
        k = r5Var11;
        r5 r5Var12 = new r5("HID_UNKNOWN_ERROR", 11, 511);
        l = r5Var12;
        r5 r5Var13 = new r5("INTERRUPTED", 12, 254);
        m = r5Var13;
        r5 r5Var14 = new r5("CONNECTION_DROPPED", 13, 255);
        n = r5Var14;
        o = new r5[]{r5Var, r5Var2, r5Var3, r5Var4, r5Var5, r5Var6, r5Var7, r5Var8, r5Var9, r5Var10, r5Var11, r5Var12, r5Var13, r5Var14};
    }
    */

    @DexIgnore
    public r5(String str, int i2, int i3) {
    }

    @DexIgnore
    public static r5 valueOf(String str) {
        return (r5) Enum.valueOf(r5.class, str);
    }

    @DexIgnore
    public static r5[] values() {
        return (r5[]) o.clone();
    }
}
