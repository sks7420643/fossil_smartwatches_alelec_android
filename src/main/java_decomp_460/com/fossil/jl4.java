package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String[] f1775a; // = new String[0];

    @DexIgnore
    public static final int a(el4 el4, int i) throws IOException {
        int b = el4.b();
        el4.t(i);
        int i2 = 1;
        while (el4.q() == i) {
            el4.t(i);
            i2++;
        }
        el4.s(b);
        return i2;
    }

    @DexIgnore
    public static int b(int i) {
        return i >>> 3;
    }

    @DexIgnore
    public static int c(int i) {
        return i & 7;
    }

    @DexIgnore
    public static int d(int i, int i2) {
        return (i << 3) | i2;
    }

    @DexIgnore
    public static boolean e(el4 el4, int i) throws IOException {
        return el4.t(i);
    }
}
