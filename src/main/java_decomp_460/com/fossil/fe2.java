package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fe2 implements Parcelable.Creator<dc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dc2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        b62[] b62Arr = null;
        b62[] b62Arr2 = null;
        Account account = null;
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    i2 = ad2.v(parcel, t);
                    break;
                case 3:
                    i3 = ad2.v(parcel, t);
                    break;
                case 4:
                    str = ad2.f(parcel, t);
                    break;
                case 5:
                    iBinder = ad2.u(parcel, t);
                    break;
                case 6:
                    scopeArr = (Scope[]) ad2.i(parcel, t, Scope.CREATOR);
                    break;
                case 7:
                    bundle = ad2.a(parcel, t);
                    break;
                case 8:
                    account = (Account) ad2.e(parcel, t, Account.CREATOR);
                    break;
                case 9:
                default:
                    ad2.B(parcel, t);
                    break;
                case 10:
                    b62Arr2 = (b62[]) ad2.i(parcel, t, b62.CREATOR);
                    break;
                case 11:
                    b62Arr = (b62[]) ad2.i(parcel, t, b62.CREATOR);
                    break;
                case 12:
                    z = ad2.m(parcel, t);
                    break;
                case 13:
                    i4 = ad2.v(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new dc2(i, i2, i3, str, iBinder, scopeArr, bundle, account, b62Arr2, b62Arr, z, i4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dc2[] newArray(int i) {
        return new dc2[i];
    }
}
