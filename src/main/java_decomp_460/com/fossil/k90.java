package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k90 {
    @DexIgnore
    public /* synthetic */ k90(kq7 kq7) {
    }

    @DexIgnore
    public final l90 a(String str) {
        l90[] values = l90.values();
        for (l90 l90 : values) {
            if (pq7.a(l90.b, str)) {
                return l90;
            }
        }
        return null;
    }
}
