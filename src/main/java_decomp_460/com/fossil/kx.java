package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ n6 f2107a;
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public kx(n6 n6Var, byte[] bArr, byte[] bArr2, int i, int i2) {
        this.f2107a = n6Var;
        this.b = bArr;
        this.c = bArr2;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = new byte[8];
        byte[] bArr2 = new byte[8];
        bArr[0] = (byte) this.f2107a.c;
        System.arraycopy(this.b, 0, bArr, 2, 6);
        System.arraycopy(this.c, 0, bArr2, 1, 7);
        dy1.c(bArr, this.d);
        dy1.c(bArr2, this.e);
        return dm7.q(bArr, bArr2);
    }
}
