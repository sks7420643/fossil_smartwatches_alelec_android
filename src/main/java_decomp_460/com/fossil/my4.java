package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2437a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public Range c;
    @DexIgnore
    public /* final */ LiveData<List<qt4>> d;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f;
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> g;
    @DexIgnore
    public /* final */ tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengeFromServer$2", f = "BCHistoryViewModel.kt", l = {124}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends bt4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ int $offset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ my4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(my4 my4, int i, int i2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = my4;
            this.$limit = i;
            this.$offset = i2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$limit, this.$offset, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super List<? extends bt4>> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x006b, code lost:
            if (r0 == r1) goto L_0x004b;
         */
        @DexIgnore
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 278
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.my4.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ my4 f2438a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1", f = "BCHistoryViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $histories;
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $result;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.my4$b$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1$uiHistories$1", f = "BCHistoryViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.my4$b$a$a  reason: collision with other inner class name */
            public static final class C0161a extends ko7 implements vp7<iv7, qn7<? super List<? extends qt4>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0161a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0161a aVar = new C0161a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends qt4>> qn7) {
                    return ((C0161a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        List list = this.this$0.$histories;
                        pq7.b(list, "histories");
                        return py4.l(list);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(List list, MutableLiveData mutableLiveData, qn7 qn7) {
                super(2, qn7);
                this.$histories = list;
                this.$result = mutableLiveData;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$histories, this.$result, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 a2 = bw7.a();
                    C0161a aVar = new C0161a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(a2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.$result.l((List) g);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public b(my4 my4) {
            this.f2438a = my4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<qt4>> apply(List<bt4> list) {
            MutableLiveData<List<qt4>> mutableLiveData = new MutableLiveData<>();
            if (!list.isEmpty()) {
                this.f2438a.f.l(Boolean.FALSE);
                this.f2438a.e.l(Boolean.FALSE);
            } else if (!this.f2438a.b) {
                this.f2438a.f.l(Boolean.TRUE);
            }
            if (this.f2438a.b) {
                this.f2438a.b = false;
                this.f2438a.p();
            }
            xw7 unused = gu7.d(us0.a(this.f2438a), null, null, new a(list, mutableLiveData, null), 3, null);
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadHistoryChallenge$1", f = "BCHistoryViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ my4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(my4 my4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = my4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                my4 my4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (my4.o(5, 0, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadMore$1", f = "BCHistoryViewModel.kt", l = {112, 115}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public /* final */ /* synthetic */ int $total;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ my4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(my4 my4, int i, int i2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = my4;
            this.$position = i;
            this.$total = i2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$position, this.$total, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Integer e;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.this$0.f2437a;
                StringBuilder sb = new StringBuilder();
                sb.append("loadMore: hasNext: ");
                Range range = this.this$0.c;
                sb.append(range != null ? ao7.a(range.isHasNext()) : null);
                sb.append(" - offset: ");
                Range range2 = this.this$0.c;
                sb.append(range2 != null ? ao7.e(range2.getOffset()) : null);
                sb.append(" - position: ");
                sb.append(this.$position);
                sb.append(" - total: ");
                sb.append(this.$total);
                local.e(str, sb.toString());
                int i2 = this.$position + 1;
                if (i2 % 5 == 0 && i2 == this.$total) {
                    List<qt4> e2 = this.this$0.m().e();
                    if (!(e2 == null || e2.isEmpty()) && e2.size() < 15) {
                        if (this.this$0.c == null) {
                            my4 my4 = this.this$0;
                            this.L$0 = iv7;
                            this.I$0 = i2;
                            this.L$1 = e2;
                            this.label = 1;
                            if (my4.o(5, i2, this) == d) {
                                return d;
                            }
                        } else if (this.this$0.c != null) {
                            Range range3 = this.this$0.c;
                            if (range3 == null) {
                                pq7.i();
                                throw null;
                            } else if (range3.isHasNext()) {
                                Range range4 = this.this$0.c;
                                if (i2 > ((range4 == null || (e = ao7.e(range4.getOffset())) == null) ? 0 : e.intValue())) {
                                    my4 my42 = this.this$0;
                                    this.L$0 = iv7;
                                    this.I$0 = i2;
                                    this.L$1 = e2;
                                    this.label = 2;
                                    if (my42.o(5, i2, this) == d) {
                                        return d;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2) {
                List list = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$refresh$1", f = "BCHistoryViewModel.kt", l = {75, 79, 82, 84, 89}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ my4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(my4 my4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = my4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                List<qt4> e = this.this$0.m().e();
                if (e == null || e.isEmpty()) {
                    my4 my4 = this.this$0;
                    this.L$0 = iv7;
                    this.L$1 = e;
                    this.label = 5;
                    if (my4.o(5, 0, this) == d) {
                        return d;
                    }
                } else if (e.size() == 15) {
                    my4 my42 = this.this$0;
                    this.L$0 = iv7;
                    this.L$1 = e;
                    this.label = 1;
                    if (my42.o(5, 0, this) == d) {
                        return d;
                    }
                } else {
                    int size = (e.size() / 5) * 5;
                    if (this.this$0.c == null) {
                        my4 my43 = this.this$0;
                        this.L$0 = iv7;
                        this.L$1 = e;
                        this.I$0 = size;
                        this.label = 2;
                        if (my43.o(5, size, this) == d) {
                            return d;
                        }
                    } else {
                        Range range = this.this$0.c;
                        if (range == null) {
                            pq7.i();
                            throw null;
                        } else if (range.isHasNext()) {
                            my4 my44 = this.this$0;
                            this.L$0 = iv7;
                            this.L$1 = e;
                            this.I$0 = size;
                            this.label = 3;
                            if (my44.o(5, size, this) == d) {
                                return d;
                            }
                        } else {
                            my4 my45 = this.this$0;
                            this.L$0 = iv7;
                            this.L$1 = e;
                            this.I$0 = size;
                            this.label = 4;
                            if (my45.o(5, 0, this) == d) {
                                return d;
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
                List list = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public my4(tt4 tt4) {
        pq7.c(tt4, "challengeRepository");
        this.h = tt4;
        String simpleName = my4.class.getSimpleName();
        pq7.b(simpleName, "BCHistoryViewModel::class.java.simpleName");
        this.f2437a = simpleName;
        LiveData<List<qt4>> c2 = ss0.c(cz4.a(this.h.A()), new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026  }\n\n        result\n    }");
        this.d = c2;
        this.e = new MutableLiveData<>();
        this.f = new MutableLiveData<>();
        this.g = new MutableLiveData<>();
    }

    @DexIgnore
    public final LiveData<Boolean> k() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> l() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<List<qt4>> m() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> n() {
        return this.e;
    }

    @DexIgnore
    public final /* synthetic */ Object o(int i, int i2, qn7<? super List<bt4>> qn7) {
        return eu7.g(bw7.b(), new a(this, i, i2, null), qn7);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().e(this.f2437a, "loadHistoryChallenge");
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void q(int i, int i2) {
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, i, i2, null), 3, null);
    }

    @DexIgnore
    public final void r(int i, int i2) {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }
}
