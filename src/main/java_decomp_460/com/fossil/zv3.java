package com.fossil;

import com.fossil.zu3;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DataHolder b;
    @DexIgnore
    public /* final */ /* synthetic */ zu3.d c;

    @DexIgnore
    public zv3(zu3.d dVar, DataHolder dataHolder) {
        this.c = dVar;
        this.b = dataHolder;
    }

    @DexIgnore
    public final void run() {
        uu3 uu3 = new uu3(this.b);
        try {
            zu3.this.j(uu3);
        } finally {
            uu3.release();
        }
    }
}
