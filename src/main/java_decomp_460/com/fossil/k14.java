package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k14 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> implements j14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Collection<?> target;

        @DexIgnore
        public b(Collection<?> collection) {
            i14.l(collection);
            this.target = collection;
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean apply(T t) {
            try {
                return this.target.contains(t);
            } catch (ClassCastException | NullPointerException e) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.target.equals(((b) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.in(" + this.target + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> implements j14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public c(T t) {
            this.target = t;
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean apply(T t) {
            return this.target.equals(t);
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean equals(Object obj) {
            if (obj instanceof c) {
                return this.target.equals(((c) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.equalTo(" + ((Object) this.target) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> implements j14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ j14<T> predicate;

        @DexIgnore
        public d(j14<T> j14) {
            i14.l(j14);
            this.predicate = j14;
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean apply(T t) {
            return !this.predicate.apply(t);
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.predicate.equals(((d) obj).predicate);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.predicate.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.not(" + this.predicate + ")";
        }
    }

    @DexIgnore
    public enum e implements j14<Object> {
        ALWAYS_TRUE {
            @DexIgnore
            @Override // com.fossil.j14, com.fossil.k14.e
            public boolean apply(Object obj) {
                return true;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        },
        ALWAYS_FALSE {
            @DexIgnore
            @Override // com.fossil.j14, com.fossil.k14.e
            public boolean apply(Object obj) {
                return false;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        },
        IS_NULL {
            @DexIgnore
            @Override // com.fossil.j14, com.fossil.k14.e
            public boolean apply(Object obj) {
                return obj == null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.isNull()";
            }
        },
        NOT_NULL {
            @DexIgnore
            @Override // com.fossil.j14, com.fossil.k14.e
            public boolean apply(Object obj) {
                return obj != null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.notNull()";
            }
        };

        @DexIgnore
        @Override // com.fossil.j14
        @CanIgnoreReturnValue
        public abstract /* synthetic */ boolean apply(T t);

        @DexIgnore
        public <T> j14<T> withNarrowedType() {
            return this;
        }
    }

    /*
    static {
        d14.h(',');
    }
    */

    @DexIgnore
    public static <T> j14<T> a(T t) {
        return t == null ? c() : new c(t);
    }

    @DexIgnore
    public static <T> j14<T> b(Collection<? extends T> collection) {
        return new b(collection);
    }

    @DexIgnore
    public static <T> j14<T> c() {
        return e.IS_NULL.withNarrowedType();
    }

    @DexIgnore
    public static <T> j14<T> d(j14<T> j14) {
        return new d(j14);
    }
}
