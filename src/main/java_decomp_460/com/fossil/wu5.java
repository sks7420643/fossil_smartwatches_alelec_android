package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu5 implements Factory<vu5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f3998a;

    @DexIgnore
    public wu5(Provider<UserRepository> provider) {
        this.f3998a = provider;
    }

    @DexIgnore
    public static wu5 a(Provider<UserRepository> provider) {
        return new wu5(provider);
    }

    @DexIgnore
    public static vu5 c(UserRepository userRepository) {
        return new vu5(userRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public vu5 get() {
        return c(this.f3998a.get());
    }
}
