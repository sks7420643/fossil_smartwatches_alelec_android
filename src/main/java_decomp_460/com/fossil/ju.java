package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju {
    @DexIgnore
    public /* synthetic */ ju(kq7 kq7) {
    }

    @DexIgnore
    public final ku a(byte b) {
        ku kuVar;
        ku[] values = ku.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                kuVar = null;
                break;
            }
            kuVar = values[i];
            if (kuVar.c == b) {
                break;
            }
            i++;
        }
        return kuVar != null ? kuVar : ku.f;
    }
}
