package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.e13;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q23<T> implements f33<T> {
    @DexIgnore
    public static /* final */ int[] q; // = new int[0];
    @DexIgnore
    public static /* final */ Unsafe r; // = e43.t();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int[] f2913a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ m23 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int[] i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ u23 l;
    @DexIgnore
    public /* final */ v13 m;
    @DexIgnore
    public /* final */ x33<?, ?> n;
    @DexIgnore
    public /* final */ s03<?> o;
    @DexIgnore
    public /* final */ j23 p;

    @DexIgnore
    public q23(int[] iArr, Object[] objArr, int i2, int i3, m23 m23, boolean z, boolean z2, int[] iArr2, int i4, int i5, u23 u23, v13 v13, x33<?, ?> x33, s03<?> s03, j23 j23) {
        this.f2913a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        boolean z3 = m23 instanceof e13;
        this.g = z;
        this.f = s03 != null && s03.e(m23);
        this.h = false;
        this.i = iArr2;
        this.j = i4;
        this.k = i5;
        this.l = u23;
        this.m = v13;
        this.n = x33;
        this.o = s03;
        this.e = m23;
        this.p = j23;
    }

    @DexIgnore
    public static <T> float E(T t, long j2) {
        return ((Float) e43.F(t, j2)).floatValue();
    }

    @DexIgnore
    public static <T> int I(T t, long j2) {
        return ((Integer) e43.F(t, j2)).intValue();
    }

    @DexIgnore
    public static <T> long K(T t, long j2) {
        return ((Long) e43.F(t, j2)).longValue();
    }

    @DexIgnore
    public static w33 L(Object obj) {
        e13 e13 = (e13) obj;
        w33 w33 = e13.zzb;
        if (w33 != w33.a()) {
            return w33;
        }
        w33 g2 = w33.g();
        e13.zzb = g2;
        return g2;
    }

    @DexIgnore
    public static <T> boolean M(T t, long j2) {
        return ((Boolean) e43.F(t, j2)).booleanValue();
    }

    @DexIgnore
    public static <UT, UB> int d(x33<UT, UB> x33, T t) {
        return x33.l(x33.f(t));
    }

    @DexIgnore
    public static int i(byte[] bArr, int i2, int i3, l43 l43, Class<?> cls, sz2 sz2) throws IOException {
        switch (t23.f3352a[l43.ordinal()]) {
            case 1:
                int k2 = tz2.k(bArr, i2, sz2);
                sz2.c = Boolean.valueOf(sz2.b != 0);
                return k2;
            case 2:
                return tz2.q(bArr, i2, sz2);
            case 3:
                sz2.c = Double.valueOf(tz2.m(bArr, i2));
                return i2 + 8;
            case 4:
            case 5:
                sz2.c = Integer.valueOf(tz2.h(bArr, i2));
                return i2 + 4;
            case 6:
            case 7:
                sz2.c = Long.valueOf(tz2.l(bArr, i2));
                return i2 + 8;
            case 8:
                sz2.c = Float.valueOf(tz2.o(bArr, i2));
                return i2 + 4;
            case 9:
            case 10:
            case 11:
                int i4 = tz2.i(bArr, i2, sz2);
                sz2.c = Integer.valueOf(sz2.f3339a);
                return i4;
            case 12:
            case 13:
                int k3 = tz2.k(bArr, i2, sz2);
                sz2.c = Long.valueOf(sz2.b);
                return k3;
            case 14:
                return tz2.g(b33.a().b(cls), bArr, i2, i3, sz2);
            case 15:
                int i5 = tz2.i(bArr, i2, sz2);
                sz2.c = Integer.valueOf(j03.c(sz2.f3339a));
                return i5;
            case 16:
                int k4 = tz2.k(bArr, i2, sz2);
                sz2.c = Long.valueOf(j03.a(sz2.b));
                return k4;
            case 17:
                return tz2.p(bArr, i2, sz2);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    @DexIgnore
    public static <T> q23<T> j(Class<T> cls, k23 k23, u23 u23, v13 v13, x33<?, ?> x33, s03<?> s03, j23 j23) {
        int i2;
        char c2;
        int i3;
        int i4;
        int i5;
        int i6;
        char c3;
        char c4;
        int i7;
        char c5;
        int i8;
        char c6;
        int[] iArr;
        int i9;
        char c7;
        char c8;
        int i10;
        int i11;
        char charAt;
        int i12;
        char charAt2;
        char charAt3;
        char charAt4;
        char charAt5;
        char charAt6;
        char charAt7;
        char charAt8;
        int i13;
        int i14;
        char c9;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        Field n2;
        char charAt9;
        int i25;
        char c10;
        int i26;
        Field n3;
        Field n4;
        int i27;
        int i28;
        char charAt10;
        int i29;
        char charAt11;
        char charAt12;
        int i30;
        char charAt13;
        if (k23 instanceof d33) {
            d33 d33 = (d33) k23;
            boolean z = d33.zza() == e13.f.j;
            String a2 = d33.a();
            int length = a2.length();
            if (a2.charAt(0) >= '\ud800') {
                int i31 = 1;
                while (true) {
                    i2 = i31 + 1;
                    if (a2.charAt(i31) < '\ud800') {
                        break;
                    }
                    i31 = i2;
                }
            } else {
                i2 = 1;
            }
            int i32 = i2 + 1;
            char charAt14 = a2.charAt(i2);
            if (charAt14 >= '\ud800') {
                int i33 = 13;
                int i34 = charAt14 & '\u1fff';
                while (true) {
                    i30 = i32 + 1;
                    charAt13 = a2.charAt(i32);
                    if (charAt13 < '\ud800') {
                        break;
                    }
                    i34 |= (charAt13 & '\u1fff') << i33;
                    i33 += 13;
                    i32 = i30;
                }
                c2 = (charAt13 << i33) | i34;
                i3 = i30;
            } else {
                c2 = charAt14;
                i3 = i32;
            }
            if (c2 == 0) {
                iArr = q;
                i9 = 0;
                i10 = 0;
                c7 = 0;
                c3 = 0;
                c4 = 0;
                c6 = 0;
                c8 = 0;
            } else {
                int i35 = i3 + 1;
                char charAt15 = a2.charAt(i3);
                if (charAt15 >= '\ud800') {
                    int i36 = charAt15 & '\u1fff';
                    int i37 = 13;
                    while (true) {
                        i4 = i35 + 1;
                        charAt8 = a2.charAt(i35);
                        if (charAt8 < '\ud800') {
                            break;
                        }
                        i36 |= (charAt8 & '\u1fff') << i37;
                        i37 += 13;
                        i35 = i4;
                    }
                    charAt15 = (charAt8 << i37) | i36;
                } else {
                    i4 = i35;
                }
                int i38 = i4 + 1;
                int charAt16 = a2.charAt(i4);
                if (charAt16 >= 55296) {
                    int i39 = charAt16 & 8191;
                    int i40 = 13;
                    while (true) {
                        i38++;
                        charAt7 = a2.charAt(i38);
                        if (charAt7 < '\ud800') {
                            break;
                        }
                        i39 |= (charAt7 & '\u1fff') << i40;
                        i40 += 13;
                    }
                    charAt16 = i39 | (charAt7 << i40);
                }
                int i41 = i38 + 1;
                int charAt17 = a2.charAt(i38);
                if (charAt17 >= 55296) {
                    int i42 = charAt17 & 8191;
                    int i43 = 13;
                    while (true) {
                        i5 = i41 + 1;
                        charAt6 = a2.charAt(i41);
                        if (charAt6 < '\ud800') {
                            break;
                        }
                        i42 |= (charAt6 & '\u1fff') << i43;
                        i43 += 13;
                        i41 = i5;
                    }
                    charAt17 = i42 | (charAt6 << i43);
                } else {
                    i5 = i41;
                }
                int i44 = i5 + 1;
                char charAt18 = a2.charAt(i5);
                if (charAt18 >= '\ud800') {
                    int i45 = 13;
                    int i46 = charAt18 & '\u1fff';
                    while (true) {
                        i6 = i44 + 1;
                        charAt5 = a2.charAt(i44);
                        if (charAt5 < '\ud800') {
                            break;
                        }
                        i46 |= (charAt5 & '\u1fff') << i45;
                        i45 += 13;
                        i44 = i6;
                    }
                    charAt18 = (charAt5 << i45) | i46;
                } else {
                    i6 = i44;
                }
                int i47 = i6 + 1;
                char charAt19 = a2.charAt(i6);
                if (charAt19 >= '\ud800') {
                    int i48 = charAt19 & '\u1fff';
                    int i49 = 13;
                    while (true) {
                        i47++;
                        charAt4 = a2.charAt(i47);
                        if (charAt4 < '\ud800') {
                            break;
                        }
                        i48 |= (charAt4 & '\u1fff') << i49;
                        i49 += 13;
                    }
                    c3 = i48 | (charAt4 << i49);
                } else {
                    c3 = charAt19;
                }
                int i50 = i47 + 1;
                char charAt20 = a2.charAt(i47);
                if (charAt20 >= '\ud800') {
                    int i51 = charAt20 & '\u1fff';
                    int i52 = 13;
                    int i53 = i51;
                    while (true) {
                        i7 = i50 + 1;
                        charAt3 = a2.charAt(i50);
                        if (charAt3 < '\ud800') {
                            break;
                        }
                        i53 |= (charAt3 & '\u1fff') << i52;
                        i52 += 13;
                        i50 = i7;
                    }
                    c4 = (charAt3 << i52) | i53;
                } else {
                    c4 = charAt20;
                    i7 = i50;
                }
                int i54 = i7 + 1;
                char charAt21 = a2.charAt(i7);
                if (charAt21 >= '\ud800') {
                    int i55 = 13;
                    int i56 = charAt21 & '\u1fff';
                    while (true) {
                        i12 = i54 + 1;
                        charAt2 = a2.charAt(i54);
                        if (charAt2 < '\ud800') {
                            break;
                        }
                        i56 |= (charAt2 & '\u1fff') << i55;
                        i55 += 13;
                        i54 = i12;
                    }
                    c5 = (charAt2 << i55) | i56;
                    i8 = i12;
                } else {
                    c5 = charAt21;
                    i8 = i54;
                }
                int i57 = i8 + 1;
                char charAt22 = a2.charAt(i8);
                if (charAt22 >= '\ud800') {
                    int i58 = 13;
                    int i59 = charAt22 & '\u1fff';
                    while (true) {
                        i11 = i57 + 1;
                        charAt = a2.charAt(i57);
                        if (charAt < '\ud800') {
                            break;
                        }
                        i59 |= (charAt & '\u1fff') << i58;
                        i58 += 13;
                        i57 = i11;
                    }
                    i3 = i11;
                    c6 = (charAt << i58) | i59;
                } else {
                    i3 = i57;
                    c6 = charAt22;
                }
                iArr = new int[(c6 + c4 + c5)];
                i9 = charAt16 + (charAt15 << 1);
                c7 = charAt18;
                c8 = charAt15;
                i10 = charAt17;
            }
            Unsafe unsafe = r;
            Object[] b2 = d33.b();
            Class<?> cls2 = d33.zzc().getClass();
            int[] iArr2 = new int[(c3 * 3)];
            Object[] objArr = new Object[(c3 << 1)];
            int i60 = c4 + c6;
            int i61 = 0;
            int i62 = 0;
            int i63 = i9;
            int i64 = i60;
            int i65 = c6;
            int i66 = c6;
            while (i3 < length) {
                int i67 = i3 + 1;
                char charAt23 = a2.charAt(i3);
                if (charAt23 >= '\ud800') {
                    int i68 = charAt23 & '\u1fff';
                    int i69 = 13;
                    while (true) {
                        i13 = i67 + 1;
                        charAt12 = a2.charAt(i67);
                        if (charAt12 < '\ud800') {
                            break;
                        }
                        i68 |= (charAt12 & '\u1fff') << i69;
                        i69 += 13;
                        i67 = i13;
                    }
                    i14 = i68 | (charAt12 << i69);
                } else {
                    i13 = i67;
                    i14 = charAt23;
                }
                int i70 = i13 + 1;
                char charAt24 = a2.charAt(i13);
                if (charAt24 >= '\ud800') {
                    int i71 = charAt24 & '\u1fff';
                    int i72 = 13;
                    int i73 = i70;
                    while (true) {
                        i29 = i73 + 1;
                        charAt11 = a2.charAt(i73);
                        if (charAt11 < '\ud800') {
                            break;
                        }
                        i71 |= (charAt11 & '\u1fff') << i72;
                        i72 += 13;
                        i73 = i29;
                    }
                    c9 = i71 | (charAt11 << i72);
                    i15 = i29;
                    i16 = i66;
                } else {
                    c9 = charAt24;
                    i15 = i70;
                    i16 = i66;
                }
                int i74 = c9 & '\u00ff';
                if ((c9 & '\u0400') != 0) {
                    iArr[i61] = i62;
                    i61++;
                }
                if (i74 >= 51) {
                    int i75 = i15 + 1;
                    char charAt25 = a2.charAt(i15);
                    if (charAt25 >= '\ud800') {
                        int i76 = charAt25 & '\u1fff';
                        int i77 = 13;
                        while (true) {
                            i28 = i75 + 1;
                            charAt10 = a2.charAt(i75);
                            if (charAt10 < '\ud800') {
                                break;
                            }
                            i76 |= (charAt10 & '\u1fff') << i77;
                            i77 += 13;
                            i75 = i28;
                        }
                        c10 = (charAt10 << i77) | i76;
                        i26 = i28;
                    } else {
                        c10 = charAt25;
                        i26 = i75;
                    }
                    int i78 = i74 - 51;
                    if (i78 == 9 || i78 == 17) {
                        objArr[((i62 / 3) << 1) + 1] = b2[i63];
                        i17 = i63 + 1;
                    } else {
                        if (i78 != 12 || z) {
                            i27 = i63;
                        } else {
                            objArr[((i62 / 3) << 1) + 1] = b2[i63];
                            i27 = i63 + 1;
                        }
                        i17 = i27;
                    }
                    int i79 = c10 << 1;
                    Object obj = b2[i79];
                    if (obj instanceof Field) {
                        n3 = (Field) obj;
                    } else {
                        n3 = n(cls2, (String) obj);
                        b2[i79] = n3;
                    }
                    i22 = (int) unsafe.objectFieldOffset(n3);
                    int i80 = i79 + 1;
                    Object obj2 = b2[i80];
                    if (obj2 instanceof Field) {
                        n4 = (Field) obj2;
                    } else {
                        n4 = n(cls2, (String) obj2);
                        b2[i80] = n4;
                    }
                    i18 = (int) unsafe.objectFieldOffset(n4);
                    i19 = 0;
                    i23 = i26;
                } else {
                    int i81 = i63 + 1;
                    Field n5 = n(cls2, (String) b2[i63]);
                    if (i74 == 9 || i74 == 17) {
                        objArr[((i62 / 3) << 1) + 1] = n5.getType();
                        i17 = i81;
                    } else {
                        if (i74 == 27 || i74 == 49) {
                            i17 = i81 + 1;
                            objArr[((i62 / 3) << 1) + 1] = b2[i81];
                            i25 = i65;
                        } else if (i74 == 12 || i74 == 30 || i74 == 44) {
                            if (!z) {
                                i17 = i81 + 1;
                                objArr[((i62 / 3) << 1) + 1] = b2[i81];
                                i25 = i65;
                            } else {
                                i17 = i81;
                            }
                        } else if (i74 == 50) {
                            i25 = i65 + 1;
                            iArr[i65] = i62;
                            int i82 = (i62 / 3) << 1;
                            i17 = i81 + 1;
                            objArr[i82] = b2[i81];
                            if ((c9 & '\u0800') != 0) {
                                objArr[i82 + 1] = b2[i17];
                                i17++;
                                i65 = i25;
                            }
                        } else {
                            i17 = i81;
                        }
                        i65 = i25;
                    }
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(n5);
                    if ((c9 & '\u1000') != 4096 || i74 > 17) {
                        i18 = 1048575;
                        i19 = 0;
                        i20 = i15;
                    } else {
                        int i83 = i15 + 1;
                        char charAt26 = a2.charAt(i15);
                        if (charAt26 >= '\ud800') {
                            int i84 = charAt26 & '\u1fff';
                            int i85 = 13;
                            while (true) {
                                i24 = i83 + 1;
                                charAt9 = a2.charAt(i83);
                                if (charAt9 < '\ud800') {
                                    break;
                                }
                                i84 |= (charAt9 & '\u1fff') << i85;
                                i85 += 13;
                                i83 = i24;
                            }
                            charAt26 = (charAt9 << i85) | i84;
                        } else {
                            i24 = i83;
                        }
                        int i86 = (charAt26 / ' ') + (c8 << 1);
                        Object obj3 = b2[i86];
                        if (obj3 instanceof Field) {
                            n2 = (Field) obj3;
                        } else {
                            n2 = n(cls2, (String) obj3);
                            b2[i86] = n2;
                        }
                        i18 = (int) unsafe.objectFieldOffset(n2);
                        i19 = charAt26 % ' ';
                        i20 = i24;
                    }
                    if (i74 < 18 || i74 > 49) {
                        i21 = i64;
                    } else {
                        iArr[i64] = objectFieldOffset;
                        i21 = i64 + 1;
                    }
                    i22 = objectFieldOffset;
                    i64 = i21;
                    i23 = i20;
                }
                int i87 = i62 + 1;
                iArr2[i62] = i14;
                int i88 = i87 + 1;
                iArr2[i87] = ((c9 & '\u0100') != 0 ? SQLiteDatabase.CREATE_IF_NECESSARY : 0) | ((c9 & '\u0200') != 0 ? 536870912 : 0) | (i74 << 20) | i22;
                iArr2[i88] = (i19 << 20) | i18;
                i62 = i88 + 1;
                i63 = i17;
                i3 = i23;
                i66 = i16;
            }
            return new q23<>(iArr2, objArr, i10, c7, d33.zzc(), z, false, iArr, i66, i60, u23, v13, x33, s03, j23);
        }
        ((q33) k23).zza();
        throw null;
    }

    @DexIgnore
    public static Field n(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException e2) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @DexIgnore
    public static List<?> o(Object obj, long j2) {
        return (List) e43.F(obj, j2);
    }

    @DexIgnore
    public static void p(int i2, Object obj, r43 r43) throws IOException {
        if (obj instanceof String) {
            r43.zza(i2, (String) obj);
        } else {
            r43.f(i2, (xz2) obj);
        }
    }

    @DexIgnore
    public static <UT, UB> void q(x33<UT, UB> x33, T t, r43 r43) throws IOException {
        x33.d(x33.f(t), r43);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.f33 */
    /* JADX WARN: Multi-variable type inference failed */
    public static boolean w(Object obj, int i2, f33 f33) {
        return f33.zzd(e43.F(obj, (long) (1048575 & i2)));
    }

    @DexIgnore
    public static <T> double x(T t, long j2) {
        return ((Double) e43.F(t, j2)).doubleValue();
    }

    @DexIgnore
    public final void A(T t, int i2) {
        int J = J(i2);
        long j2 = (long) (1048575 & J);
        if (j2 != 1048575) {
            e43.h(t, j2, (1 << (J >>> 20)) | e43.b(t, j2));
        }
    }

    @DexIgnore
    public final void B(T t, int i2, int i3) {
        e43.h(t, (long) (J(i3) & 1048575), i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0644  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0650  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void C(T r17, com.fossil.r43 r18) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1772
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.C(java.lang.Object, com.fossil.r43):void");
    }

    @DexIgnore
    public final void D(T t, T t2, int i2) {
        int H = H(i2);
        int i3 = this.f2913a[i2];
        long j2 = (long) (H & 1048575);
        if (u(t2, i3, i2)) {
            Object F = e43.F(t, j2);
            Object F2 = e43.F(t2, j2);
            if (F != null && F2 != null) {
                e43.j(t, j2, h13.e(F, F2));
                B(t, i3, i2);
            } else if (F2 != null) {
                e43.j(t, j2, F2);
                B(t, i3, i2);
            }
        }
    }

    @DexIgnore
    public final i13 F(int i2) {
        return (i13) this.b[((i2 / 3) << 1) + 1];
    }

    @DexIgnore
    public final boolean G(T t, T t2, int i2) {
        return t(t, i2) == t(t2, i2);
    }

    @DexIgnore
    public final int H(int i2) {
        return this.f2913a[i2 + 1];
    }

    @DexIgnore
    public final int J(int i2) {
        return this.f2913a[i2 + 2];
    }

    @DexIgnore
    public final int N(int i2) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return y(i2, 0);
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.fossil.f33
    public final void a(T t, byte[] bArr, int i2, int i3, sz2 sz2) throws IOException {
        int i4;
        int e2;
        m13 m13;
        int i5;
        sz2 sz22;
        int i6;
        byte[] bArr2;
        T t2;
        q23<T> q23;
        int i7;
        int i8;
        int i9;
        if (this.g) {
            Unsafe unsafe = r;
            int i10 = 0;
            int i11 = 0;
            int i12 = 1048575;
            int i13 = -1;
            sz2 sz23 = sz2;
            int i14 = i3;
            byte[] bArr3 = bArr;
            T t3 = t;
            q23<T> q232 = this;
            while (i2 < i14) {
                int i15 = i2 + 1;
                byte b2 = bArr3[i2];
                int i16 = b2;
                if (b2 < 0) {
                    i15 = tz2.d(b2, bArr3, i15, sz23);
                    i16 = sz23.f3339a;
                }
                int i17 = (i16 == 1 ? 1 : 0) >>> 3;
                int i18 = (i16 == 1 ? 1 : 0) & 7;
                i10 = i17 > i13 ? q232.c(i17, i10 / 3) : q232.N(i17);
                if (i10 == -1) {
                    i10 = 0;
                    i8 = i12;
                    i9 = i11;
                } else {
                    int[] iArr = q232.f2913a;
                    int i19 = iArr[i10 + 1];
                    int i20 = (267386880 & i19) >>> 20;
                    long j2 = (long) (1048575 & i19);
                    if (i20 <= 17) {
                        int i21 = iArr[i10 + 2];
                        int i22 = 1 << (i21 >>> 20);
                        i5 = i21 & 1048575;
                        if (i5 != i12) {
                            if (i12 != 1048575) {
                                unsafe.putInt(t3, (long) i12, i11);
                            }
                            if (i5 != 1048575) {
                                i11 = unsafe.getInt(t3, (long) i5);
                            }
                            i7 = i11;
                        } else {
                            i5 = i12;
                            i7 = i11;
                        }
                        switch (i20) {
                            case 0:
                                if (i18 == 1) {
                                    e43.f(t3, j2, tz2.m(bArr3, i15));
                                    e2 = i15 + 8;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 1:
                                if (i18 == 5) {
                                    e43.g(t3, j2, tz2.o(bArr3, i15));
                                    e2 = i15 + 4;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 2:
                            case 3:
                                if (i18 == 0) {
                                    int k2 = tz2.k(bArr3, i15, sz23);
                                    unsafe.putLong(t, j2, sz23.b);
                                    i11 = i7 | i22;
                                    e2 = k2;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 4:
                            case 11:
                                if (i18 == 0) {
                                    e2 = tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, sz23.f3339a);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 5:
                            case 14:
                                if (i18 == 1) {
                                    unsafe.putLong(t, j2, tz2.l(bArr3, i15));
                                    e2 = i15 + 8;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 6:
                            case 13:
                                if (i18 == 5) {
                                    unsafe.putInt(t3, j2, tz2.h(bArr3, i15));
                                    e2 = i15 + 4;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 7:
                                if (i18 == 0) {
                                    int k3 = tz2.k(bArr3, i15, sz23);
                                    e43.k(t3, j2, sz23.b != 0);
                                    e2 = k3;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 8:
                                if (i18 == 2) {
                                    e2 = (536870912 & i19) == 0 ? tz2.n(bArr3, i15, sz23) : tz2.p(bArr3, i15, sz23);
                                    unsafe.putObject(t3, j2, sz23.c);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 9:
                                if (i18 == 2) {
                                    e2 = tz2.g(q232.k(i10), bArr3, i15, i14, sz23);
                                    Object object = unsafe.getObject(t3, j2);
                                    if (object == null) {
                                        unsafe.putObject(t3, j2, sz23.c);
                                    } else {
                                        unsafe.putObject(t3, j2, h13.e(object, sz23.c));
                                    }
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 10:
                                if (i18 == 2) {
                                    e2 = tz2.q(bArr3, i15, sz23);
                                    unsafe.putObject(t3, j2, sz23.c);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 12:
                                if (i18 == 0) {
                                    e2 = tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, sz23.f3339a);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 15:
                                if (i18 == 0) {
                                    e2 = tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, j03.c(sz23.f3339a));
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 16:
                                if (i18 == 0) {
                                    int k4 = tz2.k(bArr3, i15, sz23);
                                    unsafe.putLong(t, j2, j03.a(sz23.b));
                                    i11 = i7 | i22;
                                    e2 = k4;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            default:
                                i8 = i5;
                                i9 = i7;
                                break;
                        }
                    } else {
                        if (i20 == 27) {
                            if (i18 == 2) {
                                m13 m132 = (m13) unsafe.getObject(t3, j2);
                                if (!m132.zza()) {
                                    int size = m132.size();
                                    m13 = m132.zza(size == 0 ? 10 : size << 1);
                                    unsafe.putObject(t3, j2, m13);
                                } else {
                                    m13 = m132;
                                }
                                e2 = tz2.e(q232.k(i10), i16, bArr, i15, i3, m13, sz2);
                                i5 = i12;
                            }
                        } else if (i20 <= 49) {
                            e2 = f(t, bArr, i15, i3, i16, i17, i18, i10, (long) i19, i20, j2, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        } else if (i20 != 50) {
                            e2 = e(t, bArr, i15, i3, i16, i17, i18, i19, i20, j2, i10, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        } else if (i18 == 2) {
                            e2 = g(t, bArr, i15, i3, i10, j2, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        }
                        i4 = i15;
                        e2 = tz2.c(i16, bArr, i4, i3, L(t), sz2);
                        i5 = i12;
                        sz22 = sz2;
                        i6 = i3;
                        bArr2 = bArr;
                        t2 = t;
                        q23 = this;
                        i12 = i5;
                        i13 = i17;
                        i2 = e2;
                        sz23 = sz22;
                        i14 = i6;
                        bArr3 = bArr2;
                        t3 = t2;
                        q232 = q23;
                    }
                    sz22 = sz23;
                    i6 = i14;
                    bArr2 = bArr3;
                    t2 = t3;
                    q23 = q232;
                    i12 = i5;
                    i13 = i17;
                    i2 = e2;
                    sz23 = sz22;
                    i14 = i6;
                    bArr3 = bArr2;
                    t3 = t2;
                    q232 = q23;
                }
                i12 = i8;
                i4 = i15;
                i11 = i9;
                e2 = tz2.c(i16, bArr, i4, i3, L(t), sz2);
                i5 = i12;
                sz22 = sz2;
                i6 = i3;
                bArr2 = bArr;
                t2 = t;
                q23 = this;
                i12 = i5;
                i13 = i17;
                i2 = e2;
                sz23 = sz22;
                i14 = i6;
                bArr3 = bArr2;
                t3 = t2;
                q232 = q23;
            }
            if (i12 != 1048575) {
                unsafe.putInt(t, (long) i12, i11);
            }
            if (i2 != i3) {
                throw l13.zzg();
            }
            return;
        }
        h(t, bArr, i2, i3, 0, sz2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x096e  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x0975  */
    /* JADX WARNING: Removed duplicated region for block: B:540:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003a  */
    @Override // com.fossil.f33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(T r14, com.fossil.r43 r15) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 2716
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.b(java.lang.Object, com.fossil.r43):void");
    }

    @DexIgnore
    public final int c(int i2, int i3) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return y(i2, i3);
    }

    @DexIgnore
    public final int e(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, sz2 sz2) throws IOException {
        int k2;
        Unsafe unsafe = r;
        long j3 = (long) (this.f2913a[i9 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Double.valueOf(tz2.m(bArr, i2)));
                    k2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 52:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Float.valueOf(tz2.o(bArr, i2)));
                    k2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 53:
            case 54:
                if (i6 == 0) {
                    k2 = tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Long.valueOf(sz2.b));
                    break;
                } else {
                    return i2;
                }
            case 55:
            case 62:
                if (i6 == 0) {
                    k2 = tz2.i(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Integer.valueOf(sz2.f3339a));
                    break;
                } else {
                    return i2;
                }
            case 56:
            case 65:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Long.valueOf(tz2.l(bArr, i2)));
                    k2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 57:
            case 64:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Integer.valueOf(tz2.h(bArr, i2)));
                    k2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 58:
                if (i6 == 0) {
                    int k3 = tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Boolean.valueOf(sz2.b != 0));
                    k2 = k3;
                    break;
                } else {
                    return i2;
                }
            case 59:
                if (i6 != 2) {
                    return i2;
                }
                int i10 = tz2.i(bArr, i2, sz2);
                int i11 = sz2.f3339a;
                if (i11 == 0) {
                    unsafe.putObject(t, j2, "");
                } else if ((536870912 & i7) == 0 || g43.g(bArr, i10, i10 + i11)) {
                    unsafe.putObject(t, j2, new String(bArr, i10, i11, h13.f1410a));
                    i10 += i11;
                } else {
                    throw l13.zzh();
                }
                unsafe.putInt(t, j3, i5);
                return i10;
            case 60:
                if (i6 != 2) {
                    return i2;
                }
                int g2 = tz2.g(k(i9), bArr, i2, i3, sz2);
                Object object = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                if (object == null) {
                    unsafe.putObject(t, j2, sz2.c);
                } else {
                    unsafe.putObject(t, j2, h13.e(object, sz2.c));
                }
                unsafe.putInt(t, j3, i5);
                return g2;
            case 61:
                if (i6 == 2) {
                    k2 = tz2.q(bArr, i2, sz2);
                    unsafe.putObject(t, j2, sz2.c);
                    break;
                } else {
                    return i2;
                }
            case 63:
                if (i6 != 0) {
                    return i2;
                }
                k2 = tz2.i(bArr, i2, sz2);
                int i12 = sz2.f3339a;
                i13 F = F(i9);
                if (F == null || F.zza(i12)) {
                    unsafe.putObject(t, j2, Integer.valueOf(i12));
                    break;
                } else {
                    L(t).c(i4, Long.valueOf((long) i12));
                    return k2;
                }
                break;
            case 66:
                if (i6 == 0) {
                    k2 = tz2.i(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Integer.valueOf(j03.c(sz2.f3339a)));
                    break;
                } else {
                    return i2;
                }
            case 67:
                if (i6 == 0) {
                    k2 = tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Long.valueOf(j03.a(sz2.b)));
                    break;
                } else {
                    return i2;
                }
            case 68:
                if (i6 == 3) {
                    k2 = tz2.f(k(i9), bArr, i2, i3, (i4 & -8) | 4, sz2);
                    Object object2 = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                    if (object2 != null) {
                        unsafe.putObject(t, j2, h13.e(object2, sz2.c));
                        break;
                    } else {
                        unsafe.putObject(t, j2, sz2.c);
                        break;
                    }
                } else {
                    return i2;
                }
            default:
                return i2;
        }
        unsafe.putInt(t, j3, i5);
        return k2;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0027 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x0026 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x019a  */
    public final int f(T r13, byte[] r14, int r15, int r16, int r17, int r18, int r19, int r20, long r21, int r23, long r24, com.fossil.sz2 r26) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1364
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.f(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.fossil.sz2):int");
    }

    @DexIgnore
    public final <K, V> int g(T t, byte[] bArr, int i2, int i3, int i4, long j2, sz2 sz2) throws IOException {
        Object obj;
        Unsafe unsafe = r;
        Object z = z(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.p.zzd(object)) {
            obj = this.p.a(z);
            this.p.zza(obj, object);
            unsafe.putObject(t, j2, obj);
        } else {
            obj = object;
        }
        h23<?, ?> zzb = this.p.zzb(z);
        Map<?, ?> zza = this.p.zza(obj);
        int i5 = tz2.i(bArr, i2, sz2);
        int i6 = sz2.f3339a;
        if (i6 < 0 || i6 > i3 - i5) {
            throw l13.zza();
        }
        int i7 = i6 + i5;
        K k2 = zzb.b;
        Object obj2 = zzb.d;
        int i8 = i5;
        Object obj3 = k2;
        while (i8 < i7) {
            int i9 = i8 + 1;
            byte b2 = bArr[i8];
            int i10 = b2;
            if (b2 < 0) {
                i9 = tz2.d(b2, bArr, i9, sz2);
                i10 = sz2.f3339a;
            }
            int i11 = (i10 == 1 ? 1 : 0) >>> 3;
            int i12 = (i10 == 1 ? 1 : 0) & 7;
            if (i11 != 1) {
                if (i11 == 2 && i12 == zzb.c.zzb()) {
                    int i13 = i(bArr, i9, i3, zzb.c, zzb.d.getClass(), sz2);
                    obj2 = sz2.c;
                    i8 = i13;
                }
            } else if (i12 == zzb.f1414a.zzb()) {
                i8 = i(bArr, i9, i3, zzb.f1414a, null, sz2);
                obj3 = sz2.c;
            }
            i8 = tz2.a(i10, bArr, i9, i3, sz2);
        }
        if (i8 == i7) {
            zza.put(obj3, obj2);
            return i7;
        }
        throw l13.zzg();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v94, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int h(T r42, byte[] r43, int r44, int r45, int r46, com.fossil.sz2 r47) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.h(java.lang.Object, byte[], int, int, int, com.fossil.sz2):int");
    }

    @DexIgnore
    public final f33 k(int i2) {
        int i3 = (i2 / 3) << 1;
        f33 f33 = (f33) this.b[i3];
        if (f33 != null) {
            return f33;
        }
        f33<T> b2 = b33.a().b((Class) this.b[i3 + 1]);
        this.b[i3] = b2;
        return b2;
    }

    @DexIgnore
    public final <K, V, UT, UB> UB l(int i2, int i3, Map<K, V> map, i13 i13, UB ub, x33<UT, UB> x33) {
        h23<?, ?> zzb = this.p.zzb(z(i2));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        UB ub2 = ub;
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!i13.zza(next.getValue().intValue())) {
                UB a2 = ub2 == null ? x33.a() : ub2;
                f03 zzc = xz2.zzc(e23.a(zzb, next.getKey(), next.getValue()));
                try {
                    e23.b(zzc.b(), zzb, next.getKey(), next.getValue());
                    x33.c(a2, i3, zzc.a());
                    it.remove();
                    ub2 = a2;
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
        return ub2;
    }

    @DexIgnore
    public final <UT, UB> UB m(Object obj, int i2, UB ub, x33<UT, UB> x33) {
        i13 F;
        int i3 = this.f2913a[i2];
        Object F2 = e43.F(obj, (long) (H(i2) & 1048575));
        return (F2 == null || (F = F(i2)) == null) ? ub : (UB) l(i2, i3, (Map<K, V>) this.p.zza(F2), F, ub, x33);
    }

    @DexIgnore
    public final <K, V> void r(r43 r43, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            r43.c(i2, this.p.zzb(z(i3)), this.p.zzc(obj));
        }
    }

    @DexIgnore
    public final void s(T t, T t2, int i2) {
        long H = (long) (H(i2) & 1048575);
        if (t(t2, i2)) {
            Object F = e43.F(t, H);
            Object F2 = e43.F(t2, H);
            if (F != null && F2 != null) {
                e43.j(t, H, h13.e(F, F2));
                A(t, i2);
            } else if (F2 != null) {
                e43.j(t, H, F2);
                A(t, i2);
            }
        }
    }

    @DexIgnore
    public final boolean t(T t, int i2) {
        int J = J(i2);
        long j2 = (long) (J & 1048575);
        if (j2 == 1048575) {
            int H = H(i2);
            long j3 = (long) (H & 1048575);
            switch ((H & 267386880) >>> 20) {
                case 0:
                    return e43.C(t, j3) != 0.0d;
                case 1:
                    return e43.x(t, j3) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return e43.o(t, j3) != 0;
                case 3:
                    return e43.o(t, j3) != 0;
                case 4:
                    return e43.b(t, j3) != 0;
                case 5:
                    return e43.o(t, j3) != 0;
                case 6:
                    return e43.b(t, j3) != 0;
                case 7:
                    return e43.w(t, j3);
                case 8:
                    Object F = e43.F(t, j3);
                    if (F instanceof String) {
                        return !((String) F).isEmpty();
                    }
                    if (F instanceof xz2) {
                        return !xz2.zza.equals(F);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return e43.F(t, j3) != null;
                case 10:
                    return !xz2.zza.equals(e43.F(t, j3));
                case 11:
                    return e43.b(t, j3) != 0;
                case 12:
                    return e43.b(t, j3) != 0;
                case 13:
                    return e43.b(t, j3) != 0;
                case 14:
                    return e43.o(t, j3) != 0;
                case 15:
                    return e43.b(t, j3) != 0;
                case 16:
                    return e43.o(t, j3) != 0;
                case 17:
                    return e43.F(t, j3) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return ((1 << (J >>> 20)) & e43.b(t, j2)) != 0;
        }
    }

    @DexIgnore
    public final boolean u(T t, int i2, int i3) {
        return e43.b(t, (long) (J(i3) & 1048575)) == i2;
    }

    @DexIgnore
    public final boolean v(T t, int i2, int i3, int i4, int i5) {
        return i3 == 1048575 ? t(t, i2) : (i4 & i5) != 0;
    }

    @DexIgnore
    public final int y(int i2, int i3) {
        int length = (this.f2913a.length / 3) - 1;
        while (i3 <= length) {
            int i4 = (length + i3) >>> 1;
            int i5 = i4 * 3;
            int i6 = this.f2913a[i5];
            if (i2 == i6) {
                return i5;
            }
            if (i2 < i6) {
                length = i4 - 1;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }

    @DexIgnore
    public final Object z(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final int zza(T t) {
        int i2;
        int i3;
        int b2;
        int length = this.f2913a.length;
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5 += 3) {
            int H = H(i5);
            int i6 = this.f2913a[i5];
            long j2 = (long) (1048575 & H);
            int i7 = 37;
            switch ((H & 267386880) >>> 20) {
                case 0:
                    i3 = i4 * 53;
                    b2 = h13.b(Double.doubleToLongBits(e43.C(t, j2)));
                    i2 = i3 + b2;
                    break;
                case 1:
                    i3 = i4 * 53;
                    b2 = Float.floatToIntBits(e43.x(t, j2));
                    i2 = i3 + b2;
                    break;
                case 2:
                    i3 = i4 * 53;
                    b2 = h13.b(e43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 3:
                    i3 = i4 * 53;
                    b2 = h13.b(e43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 4:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 5:
                    i3 = i4 * 53;
                    b2 = h13.b(e43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 6:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 7:
                    i3 = i4 * 53;
                    b2 = h13.c(e43.w(t, j2));
                    i2 = i3 + b2;
                    break;
                case 8:
                    b2 = ((String) e43.F(t, j2)).hashCode();
                    i3 = i4 * 53;
                    i2 = i3 + b2;
                    break;
                case 9:
                    Object F = e43.F(t, j2);
                    if (F != null) {
                        i7 = F.hashCode();
                    }
                    i2 = i7 + (i4 * 53);
                    break;
                case 10:
                    i3 = i4 * 53;
                    b2 = e43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 11:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 12:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 13:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 14:
                    i3 = i4 * 53;
                    b2 = h13.b(e43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 15:
                    i3 = i4 * 53;
                    b2 = e43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 16:
                    i3 = i4 * 53;
                    b2 = h13.b(e43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 17:
                    Object F2 = e43.F(t, j2);
                    if (F2 != null) {
                        i7 = F2.hashCode();
                    }
                    i2 = i7 + (i4 * 53);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i4 * 53;
                    b2 = e43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 50:
                    i3 = i4 * 53;
                    b2 = e43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 51:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(Double.doubleToLongBits(x(t, j2)));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 52:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = Float.floatToIntBits(E(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 53:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 54:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 55:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 56:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 57:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 58:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.c(M(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 59:
                    if (u(t, i6, i5)) {
                        b2 = ((String) e43.F(t, j2)).hashCode();
                        i3 = i4 * 53;
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 60:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = e43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 61:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = e43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 62:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 63:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 64:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 65:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 66:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 67:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = h13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 68:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = e43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                default:
                    i2 = i4;
                    break;
            }
            i4 = i2;
        }
        int hashCode = (i4 * 53) + this.n.f(t).hashCode();
        return this.f ? (hashCode * 53) + this.o.b(t).hashCode() : hashCode;
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final T zza() {
        return (T) this.l.zza(this.e);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (com.fossil.h33.q(com.fossil.e43.F(r12, r6), com.fossil.e43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006c, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007e, code lost:
        if (com.fossil.e43.o(r12, r6) == com.fossil.e43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008f, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a2, code lost:
        if (com.fossil.e43.o(r12, r6) == com.fossil.e43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b3, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c4, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d6, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ec, code lost:
        if (com.fossil.h33.q(com.fossil.e43.F(r12, r6), com.fossil.e43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0102, code lost:
        if (com.fossil.h33.q(com.fossil.e43.F(r12, r6), com.fossil.e43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0118, code lost:
        if (com.fossil.h33.q(com.fossil.e43.F(r12, r6), com.fossil.e43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x012a, code lost:
        if (com.fossil.e43.w(r12, r6) == com.fossil.e43.w(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x013c, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0150, code lost:
        if (com.fossil.e43.o(r12, r6) == com.fossil.e43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0162, code lost:
        if (com.fossil.e43.b(r12, r6) == com.fossil.e43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0176, code lost:
        if (com.fossil.e43.o(r12, r6) == com.fossil.e43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018a, code lost:
        if (com.fossil.e43.o(r12, r6) == com.fossil.e43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.fossil.e43.x(r12, r6)) == java.lang.Float.floatToIntBits(com.fossil.e43.x(r13, r6))) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01c0, code lost:
        if (java.lang.Double.doubleToLongBits(com.fossil.e43.C(r12, r6)) == java.lang.Double.doubleToLongBits(com.fossil.e43.C(r13, r6))) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (com.fossil.h33.q(com.fossil.e43.F(r12, r6), com.fossil.e43.F(r13, r6)) != false) goto L_0x006c;
     */
    @DexIgnore
    @Override // com.fossil.f33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r12, T r13) {
        /*
        // Method dump skipped, instructions count: 642
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.zza(java.lang.Object, java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final int zzb(T t) {
        int i2;
        int i3;
        int i4;
        int c0;
        int H;
        int B0;
        int V;
        int h0;
        int p0;
        int B;
        int V2;
        int h02;
        int p02;
        if (this.g) {
            Unsafe unsafe = r;
            int i5 = 0;
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i5 >= this.f2913a.length) {
                    return d(this.n, t) + i7;
                }
                int H2 = H(i5);
                int i8 = (267386880 & H2) >>> 20;
                int i9 = this.f2913a[i5];
                long j2 = (long) (H2 & 1048575);
                int i10 = (i8 < y03.DOUBLE_LIST_PACKED.zza() || i8 > y03.SINT64_LIST_PACKED.zza()) ? 0 : this.f2913a[i5 + 2] & 1048575;
                switch (i8) {
                    case 0:
                        if (t(t, i5)) {
                            B = l03.B(i9, 0.0d);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 1:
                        if (t(t, i5)) {
                            B = l03.C(i9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 2:
                        if (t(t, i5)) {
                            B = l03.c0(i9, e43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 3:
                        if (t(t, i5)) {
                            B = l03.i0(i9, e43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 4:
                        if (t(t, i5)) {
                            B = l03.m0(i9, e43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 5:
                        if (t(t, i5)) {
                            B = l03.r0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 6:
                        if (t(t, i5)) {
                            B = l03.y0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 7:
                        if (t(t, i5)) {
                            B = l03.H(i9, true);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 8:
                        if (t(t, i5)) {
                            Object F = e43.F(t, j2);
                            if (F instanceof xz2) {
                                B = l03.U(i9, (xz2) F);
                                break;
                            } else {
                                B = l03.G(i9, (String) F);
                                break;
                            }
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 9:
                        if (t(t, i5)) {
                            B = h33.a(i9, e43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 10:
                        if (t(t, i5)) {
                            B = l03.U(i9, (xz2) e43.F(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 11:
                        if (t(t, i5)) {
                            B = l03.q0(i9, e43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 12:
                        if (t(t, i5)) {
                            B = l03.D0(i9, e43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 13:
                        if (t(t, i5)) {
                            B = l03.B0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 14:
                        if (t(t, i5)) {
                            B = l03.v0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 15:
                        if (t(t, i5)) {
                            B = l03.u0(i9, e43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 16:
                        if (t(t, i5)) {
                            B = l03.n0(i9, e43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 17:
                        if (t(t, i5)) {
                            B = l03.V(i9, (m23) e43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 18:
                        B = h33.U(i9, o(t, j2), false);
                        break;
                    case 19:
                        B = h33.R(i9, o(t, j2), false);
                        break;
                    case 20:
                        B = h33.d(i9, o(t, j2), false);
                        break;
                    case 21:
                        B = h33.t(i9, o(t, j2), false);
                        break;
                    case 22:
                        B = h33.H(i9, o(t, j2), false);
                        break;
                    case 23:
                        B = h33.U(i9, o(t, j2), false);
                        break;
                    case 24:
                        B = h33.R(i9, o(t, j2), false);
                        break;
                    case 25:
                        B = h33.X(i9, o(t, j2), false);
                        break;
                    case 26:
                        B = h33.b(i9, o(t, j2));
                        break;
                    case 27:
                        B = h33.c(i9, o(t, j2), k(i5));
                        break;
                    case 28:
                        B = h33.r(i9, o(t, j2));
                        break;
                    case 29:
                        B = h33.L(i9, o(t, j2), false);
                        break;
                    case 30:
                        B = h33.D(i9, o(t, j2), false);
                        break;
                    case 31:
                        B = h33.R(i9, o(t, j2), false);
                        break;
                    case 32:
                        B = h33.U(i9, o(t, j2), false);
                        break;
                    case 33:
                        B = h33.O(i9, o(t, j2), false);
                        break;
                    case 34:
                        B = h33.z(i9, o(t, j2), false);
                        break;
                    case 35:
                        V2 = h33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 36:
                        V2 = h33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 37:
                        V2 = h33.e((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 38:
                        V2 = h33.u((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 39:
                        V2 = h33.I((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 40:
                        V2 = h33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 41:
                        V2 = h33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 42:
                        V2 = h33.Y((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 43:
                        V2 = h33.M((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 44:
                        V2 = h33.E((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 45:
                        V2 = h33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 46:
                        V2 = h33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 47:
                        V2 = h33.P((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 48:
                        V2 = h33.A((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = l03.h0(i9);
                            p02 = l03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 49:
                        B = h33.s(i9, o(t, j2), k(i5));
                        break;
                    case 50:
                        B = this.p.zza(i9, e43.F(t, j2), z(i5));
                        break;
                    case 51:
                        if (u(t, i9, i5)) {
                            B = l03.B(i9, 0.0d);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 52:
                        if (u(t, i9, i5)) {
                            B = l03.C(i9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 53:
                        if (u(t, i9, i5)) {
                            B = l03.c0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 54:
                        if (u(t, i9, i5)) {
                            B = l03.i0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 55:
                        if (u(t, i9, i5)) {
                            B = l03.m0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 56:
                        if (u(t, i9, i5)) {
                            B = l03.r0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 57:
                        if (u(t, i9, i5)) {
                            B = l03.y0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 58:
                        if (u(t, i9, i5)) {
                            B = l03.H(i9, true);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 59:
                        if (u(t, i9, i5)) {
                            Object F2 = e43.F(t, j2);
                            if (F2 instanceof xz2) {
                                B = l03.U(i9, (xz2) F2);
                                break;
                            } else {
                                B = l03.G(i9, (String) F2);
                                break;
                            }
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 60:
                        if (u(t, i9, i5)) {
                            B = h33.a(i9, e43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 61:
                        if (u(t, i9, i5)) {
                            B = l03.U(i9, (xz2) e43.F(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 62:
                        if (u(t, i9, i5)) {
                            B = l03.q0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 63:
                        if (u(t, i9, i5)) {
                            B = l03.D0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 64:
                        if (u(t, i9, i5)) {
                            B = l03.B0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 65:
                        if (u(t, i9, i5)) {
                            B = l03.v0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 66:
                        if (u(t, i9, i5)) {
                            B = l03.u0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 67:
                        if (u(t, i9, i5)) {
                            B = l03.n0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 68:
                        if (u(t, i9, i5)) {
                            B = l03.V(i9, (m23) e43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    default:
                        i6 = i7;
                        i5 += 3;
                }
                i7 += B;
                i6 = i7;
                i5 += 3;
            }
        } else {
            Unsafe unsafe2 = r;
            int i11 = 0;
            int i12 = 0;
            int i13 = 1048575;
            int i14 = 0;
            while (i11 < this.f2913a.length) {
                int H3 = H(i11);
                int[] iArr = this.f2913a;
                int i15 = iArr[i11];
                int i16 = (267386880 & H3) >>> 20;
                if (i16 <= 17) {
                    i4 = iArr[i11 + 2];
                    int i17 = 1048575 & i4;
                    int i18 = 1 << (i4 >>> 20);
                    if (i17 != i13) {
                        i14 = unsafe2.getInt(t, (long) i17);
                        i13 = i17;
                    }
                    i2 = i18;
                    i3 = i13;
                } else {
                    i2 = 0;
                    i3 = i13;
                    i4 = (!this.h || i16 < y03.DOUBLE_LIST_PACKED.zza() || i16 > y03.SINT64_LIST_PACKED.zza()) ? 0 : this.f2913a[i11 + 2] & 1048575;
                }
                long j3 = (long) (1048575 & H3);
                switch (i16) {
                    case 0:
                        if ((i14 & i2) != 0) {
                            i12 += l03.B(i15, 0.0d);
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if ((i14 & i2) != 0) {
                            i12 += l03.C(i15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if ((i14 & i2) != 0) {
                            c0 = l03.c0(i15, unsafe2.getLong(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if ((i14 & i2) != 0) {
                            c0 = l03.i0(i15, unsafe2.getLong(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if ((i14 & i2) != 0) {
                            c0 = l03.m0(i15, unsafe2.getInt(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if ((i14 & i2) != 0) {
                            c0 = l03.r0(i15, 0);
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if ((i14 & i2) != 0) {
                            i12 += l03.y0(i15, 0);
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if ((i14 & i2) != 0) {
                            H = l03.H(i15, true);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if ((i14 & i2) != 0) {
                            Object object = unsafe2.getObject(t, j3);
                            H = object instanceof xz2 ? l03.U(i15, (xz2) object) : l03.G(i15, (String) object);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        if ((i14 & i2) != 0) {
                            H = h33.a(i15, unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 10:
                        if ((i14 & i2) != 0) {
                            H = l03.U(i15, (xz2) unsafe2.getObject(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if ((i14 & i2) != 0) {
                            H = l03.q0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if ((i14 & i2) != 0) {
                            H = l03.D0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if ((i14 & i2) != 0) {
                            B0 = l03.B0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if ((i14 & i2) != 0) {
                            H = l03.v0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if ((i14 & i2) != 0) {
                            H = l03.u0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if ((i14 & i2) != 0) {
                            H = l03.n0(i15, unsafe2.getLong(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        if ((i14 & i2) != 0) {
                            H = l03.V(i15, (m23) unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        H = h33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 19:
                        H = h33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 20:
                        H = h33.d(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 21:
                        H = h33.t(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 22:
                        H = h33.H(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 23:
                        H = h33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 24:
                        H = h33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 25:
                        H = h33.X(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 26:
                        H = h33.b(i15, (List) unsafe2.getObject(t, j3));
                        i12 += H;
                        break;
                    case 27:
                        H = h33.c(i15, (List) unsafe2.getObject(t, j3), k(i11));
                        i12 += H;
                        break;
                    case 28:
                        H = h33.r(i15, (List) unsafe2.getObject(t, j3));
                        i12 += H;
                        break;
                    case 29:
                        H = h33.L(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 30:
                        H = h33.D(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 31:
                        H = h33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 32:
                        H = h33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 33:
                        H = h33.O(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 34:
                        H = h33.z(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 35:
                        V = h33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 36:
                        V = h33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 37:
                        V = h33.e((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 38:
                        V = h33.u((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 39:
                        V = h33.I((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 40:
                        V = h33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 41:
                        V = h33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 42:
                        V = h33.Y((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 43:
                        V = h33.M((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 44:
                        V = h33.E((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 45:
                        V = h33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 46:
                        V = h33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 47:
                        V = h33.P((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 48:
                        V = h33.A((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = l03.h0(i15);
                            p0 = l03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 49:
                        H = h33.s(i15, (List) unsafe2.getObject(t, j3), k(i11));
                        i12 += H;
                        break;
                    case 50:
                        H = this.p.zza(i15, unsafe2.getObject(t, j3), z(i11));
                        i12 += H;
                        break;
                    case 51:
                        if (u(t, i15, i11)) {
                            H = l03.B(i15, 0.0d);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 52:
                        if (u(t, i15, i11)) {
                            B0 = l03.C(i15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 53:
                        if (u(t, i15, i11)) {
                            H = l03.c0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 54:
                        if (u(t, i15, i11)) {
                            H = l03.i0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 55:
                        if (u(t, i15, i11)) {
                            H = l03.m0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 56:
                        if (u(t, i15, i11)) {
                            H = l03.r0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 57:
                        if (u(t, i15, i11)) {
                            B0 = l03.y0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 58:
                        if (u(t, i15, i11)) {
                            H = l03.H(i15, true);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 59:
                        if (u(t, i15, i11)) {
                            Object object2 = unsafe2.getObject(t, j3);
                            H = object2 instanceof xz2 ? l03.U(i15, (xz2) object2) : l03.G(i15, (String) object2);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        if (u(t, i15, i11)) {
                            H = h33.a(i15, unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 61:
                        if (u(t, i15, i11)) {
                            H = l03.U(i15, (xz2) unsafe2.getObject(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 62:
                        if (u(t, i15, i11)) {
                            H = l03.q0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 63:
                        if (u(t, i15, i11)) {
                            H = l03.D0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 64:
                        if (u(t, i15, i11)) {
                            B0 = l03.B0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 65:
                        if (u(t, i15, i11)) {
                            H = l03.v0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 66:
                        if (u(t, i15, i11)) {
                            H = l03.u0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 67:
                        if (u(t, i15, i11)) {
                            H = l03.n0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        if (u(t, i15, i11)) {
                            H = l03.V(i15, (m23) unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                }
                i11 += 3;
                i13 = i3;
            }
            int i19 = 0;
            int d2 = i12 + d(this.n, t);
            if (!this.f) {
                return d2;
            }
            t03<?> b2 = this.o.b(t);
            int i20 = 0;
            while (true) {
                int i21 = i19;
                if (i20 < b2.f3344a.k()) {
                    Map.Entry<T, Object> i22 = b2.f3344a.i(i20);
                    i19 = t03.a(i22.getKey(), i22.getValue()) + i21;
                    i20++;
                } else {
                    for (Map.Entry<T, Object> entry : b2.f3344a.n()) {
                        i21 += t03.a(entry.getKey(), entry.getValue());
                    }
                    return d2 + i21;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.f2913a.length; i2 += 3) {
                int H = H(i2);
                long j2 = (long) (1048575 & H);
                int i3 = this.f2913a[i2];
                switch ((H & 267386880) >>> 20) {
                    case 0:
                        if (t(t2, i2)) {
                            e43.f(t, j2, e43.C(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if (t(t2, i2)) {
                            e43.g(t, j2, e43.x(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if (t(t2, i2)) {
                            e43.i(t, j2, e43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if (t(t2, i2)) {
                            e43.i(t, j2, e43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if (t(t2, i2)) {
                            e43.i(t, j2, e43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if (t(t2, i2)) {
                            e43.k(t, j2, e43.w(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if (t(t2, i2)) {
                            e43.j(t, j2, e43.F(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        s(t, t2, i2);
                        break;
                    case 10:
                        if (t(t2, i2)) {
                            e43.j(t, j2, e43.F(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if (t(t2, i2)) {
                            e43.i(t, j2, e43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if (t(t2, i2)) {
                            e43.h(t, j2, e43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if (t(t2, i2)) {
                            e43.i(t, j2, e43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        s(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.m.b(t, t2, j2);
                        break;
                    case 50:
                        h33.n(this.p, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (u(t2, i3, i2)) {
                            e43.j(t, j2, e43.F(t2, j2));
                            B(t, i3, i2);
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        D(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (u(t2, i3, i2)) {
                            e43.j(t, j2, e43.F(t2, j2));
                            B(t, i3, i2);
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        D(t, t2, i2);
                        break;
                }
            }
            h33.o(this.n, t, t2);
            if (this.f) {
                h33.m(this.o, t, t2);
                return;
            }
            return;
        }
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void zzc(T t) {
        int i2;
        int i3 = this.j;
        while (true) {
            i2 = this.k;
            if (i3 >= i2) {
                break;
            }
            long H = (long) (H(this.i[i3]) & 1048575);
            Object F = e43.F(t, H);
            if (F != null) {
                e43.j(t, H, this.p.b(F));
            }
            i3++;
        }
        int length = this.i.length;
        for (int i4 = i2; i4 < length; i4++) {
            this.m.d(t, (long) this.i[i4]);
        }
        this.n.j(t);
        if (this.f) {
            this.o.g(t);
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.f33] */
    /* JADX WARN: Type inference failed for: r0v39 */
    /* JADX WARN: Type inference failed for: r0v41, types: [com.fossil.f33] */
    /* JADX WARN: Type inference failed for: r0v48 */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0064 A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.f33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzd(T r15) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q23.zzd(java.lang.Object):boolean");
    }
}
