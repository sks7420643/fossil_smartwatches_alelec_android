package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.ac2;
import com.fossil.m62;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db2 implements s92 {
    @DexIgnore
    public /* final */ Map<m62.c<?>, bb2<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<m62.c<?>, bb2<?>> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<m62<?>, Boolean> d;
    @DexIgnore
    public /* final */ l72 e;
    @DexIgnore
    public /* final */ t82 f;
    @DexIgnore
    public /* final */ Lock g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ d62 i;
    @DexIgnore
    public /* final */ Condition j;
    @DexIgnore
    public /* final */ ac2 k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public /* final */ Queue<i72<?, ?>> s; // = new LinkedList();
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Map<g72<?>, z52> u;
    @DexIgnore
    public Map<g72<?>, z52> v;
    @DexIgnore
    public eb2 w;
    @DexIgnore
    public z52 x;

    @DexIgnore
    public db2(Context context, Lock lock, Looper looper, d62 d62, Map<m62.c<?>, m62.f> map, ac2 ac2, Map<m62<?>, Boolean> map2, m62.a<? extends ys3, gs3> aVar, ArrayList<wa2> arrayList, t82 t82, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.g = lock;
        this.h = looper;
        this.j = lock.newCondition();
        this.i = d62;
        this.f = t82;
        this.d = map2;
        this.k = ac2;
        this.l = z;
        HashMap hashMap = new HashMap();
        for (m62<?> m62 : map2.keySet()) {
            hashMap.put(m62.a(), m62);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            wa2 wa2 = arrayList.get(i2);
            i2++;
            wa2 wa22 = wa2;
            hashMap2.put(wa22.b, wa22);
        }
        boolean z5 = false;
        boolean z6 = true;
        boolean z7 = false;
        for (Map.Entry<m62.c<?>, m62.f> entry : map.entrySet()) {
            m62 m622 = (m62) hashMap.get(entry.getKey());
            m62.f value = entry.getValue();
            if (!value.r()) {
                z2 = false;
                z3 = z7;
                z4 = z5;
            } else if (!this.d.get(m622).booleanValue()) {
                z2 = z6;
                z3 = true;
                z4 = true;
            } else {
                z2 = z6;
                z3 = z7;
                z4 = true;
            }
            bb2<?> bb2 = new bb2<>(context, m622, looper, value, (wa2) hashMap2.get(m622), ac2, aVar);
            this.b.put(entry.getKey(), bb2);
            if (value.v()) {
                this.c.put(entry.getKey(), bb2);
            }
            z6 = z2;
            z7 = z3;
            z5 = z4;
        }
        this.m = z5 && !z6 && !z7;
        this.e = l72.m();
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void a() {
        this.g.lock();
        try {
            this.t = false;
            this.u = null;
            this.v = null;
            if (this.w != null) {
                this.w.a();
                this.w = null;
            }
            this.x = null;
            while (!this.s.isEmpty()) {
                i72<?, ?> remove = this.s.remove();
                remove.n(null);
                remove.e();
            }
            this.j.signalAll();
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void b() {
        this.g.lock();
        try {
            if (!this.t) {
                this.t = true;
                this.u = null;
                this.v = null;
                this.w = null;
                this.x = null;
                this.e.D();
                this.e.g(this.b.values()).c(new rf2(this.h), new fb2(this));
                this.g.unlock();
            }
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final boolean c() {
        this.g.lock();
        try {
            return this.u != null && this.x == null;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    public final z52 d(m62<?> m62) {
        return i(m62.a());
    }

    @DexIgnore
    public final boolean e() {
        this.g.lock();
        try {
            return this.u == null && this.t;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.s92
    public final boolean g(t72 t72) {
        this.g.lock();
        try {
            if (!this.t || t()) {
                this.g.unlock();
                return false;
            }
            this.e.D();
            this.w = new eb2(this, t72);
            this.e.g(this.c.values()).c(new rf2(this.h), this.w);
            this.g.unlock();
            return true;
        } catch (Throwable th) {
            this.g.unlock();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void h() {
        this.g.lock();
        try {
            this.e.a();
            if (this.w != null) {
                this.w.a();
                this.w = null;
            }
            if (this.v == null) {
                this.v = new zi0(this.c.size());
            }
            z52 z52 = new z52(4);
            for (bb2<?> bb2 : this.c.values()) {
                this.v.put(bb2.a(), z52);
            }
            if (this.u != null) {
                this.u.putAll(this.v);
            }
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    public final z52 i(m62.c<?> cVar) {
        this.g.lock();
        try {
            bb2<?> bb2 = this.b.get(cVar);
            if (this.u != null && bb2 != null) {
                return this.u.get(bb2.a());
            }
            this.g.unlock();
            return null;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final <A extends m62.b, T extends i72<? extends z62, A>> T j(T t2) {
        m62.c<A> w2 = t2.w();
        if (!this.l || !y(t2)) {
            this.f.y.b(t2);
            this.b.get(w2).h(t2);
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t2) {
        if (!this.l || !y(t2)) {
            if (!c()) {
                this.s.add(t2);
            } else {
                this.f.y.b(t2);
                this.b.get(t2.w()).d(t2);
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void l() {
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final z52 m() {
        b();
        while (e()) {
            try {
                this.j.await();
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                return new z52(15, null);
            }
        }
        if (c()) {
            return z52.f;
        }
        z52 z52 = this.x;
        return z52 == null ? new z52(13, null) : z52;
    }

    @DexIgnore
    public final boolean q(bb2<?> bb2, z52 z52) {
        return !z52.A() && !z52.k() && this.d.get(bb2.i()).booleanValue() && bb2.s().r() && this.i.m(z52.c());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean t() {
        /*
            r3 = this;
            r1 = 0
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.lock()
            boolean r0 = r3.t     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x000e
            boolean r0 = r3.l     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0015
        L_0x000e:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.util.Map<com.fossil.m62$c<?>, com.fossil.bb2<?>> r0 = r3.c
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x001f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r2.next()
            com.fossil.m62$c r0 = (com.fossil.m62.c) r0
            com.fossil.z52 r0 = r3.i(r0)
            if (r0 == 0) goto L_0x0037
            boolean r0 = r0.A()
            if (r0 != 0) goto L_0x001f
        L_0x0037:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = r1
            goto L_0x0014
        L_0x003e:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = 1
            goto L_0x0014
        L_0x0045:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.g
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.db2.t():boolean");
    }

    @DexIgnore
    public final void u() {
        if (this.k == null) {
            this.f.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(this.k.j());
        Map<m62<?>, ac2.b> g2 = this.k.g();
        for (m62<?> m62 : g2.keySet()) {
            z52 d2 = d(m62);
            if (d2 != null && d2.A()) {
                hashSet.addAll(g2.get(m62).f245a);
            }
        }
        this.f.q = hashSet;
    }

    @DexIgnore
    public final void v() {
        while (!this.s.isEmpty()) {
            j(this.s.remove());
        }
        this.f.b(null);
    }

    @DexIgnore
    public final z52 w() {
        int i2 = 0;
        z52 z52 = null;
        z52 z522 = null;
        int i3 = 0;
        for (bb2<?> bb2 : this.b.values()) {
            m62<?> i4 = bb2.i();
            z52 z523 = this.u.get(bb2.a());
            if (!z523.A() && (!this.d.get(i4).booleanValue() || z523.k() || this.i.m(z523.c()))) {
                if (z523.c() != 4 || !this.l) {
                    int b2 = i4.c().b();
                    if (z522 == null || i3 > b2) {
                        z522 = z523;
                        i3 = b2;
                    }
                } else {
                    int b3 = i4.c().b();
                    if (z52 == null || i2 > b3) {
                        i2 = b3;
                        z52 = z523;
                    }
                }
            }
        }
        return (z522 == null || z52 == null || i3 <= i2) ? z522 : z52;
    }

    @DexIgnore
    public final <T extends i72<? extends z62, ? extends m62.b>> boolean y(T t2) {
        m62.c<?> w2 = t2.w();
        z52 i2 = i(w2);
        if (i2 == null || i2.c() != 4) {
            return false;
        }
        t2.A(new Status(4, null, this.e.c(this.b.get(w2).a(), System.identityHashCode(this.f))));
        return true;
    }
}
