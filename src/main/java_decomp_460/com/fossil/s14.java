package com.fossil;

import com.fossil.c44;
import com.fossil.d44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s14<E> extends v14<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -2250766705698539974L;
    @DexIgnore
    public transient Map<E, e24> d;
    @DexIgnore
    public transient long e; // = ((long) super.size());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Iterator<c44.a<E>> {
        @DexIgnore
        public Map.Entry<E, e24> b;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s14$a$a")
        /* renamed from: com.fossil.s14$a$a  reason: collision with other inner class name */
        public class C0212a extends d44.b<E> {
            @DexIgnore
            public /* final */ /* synthetic */ Map.Entry b;

            @DexIgnore
            public C0212a(Map.Entry entry) {
                this.b = entry;
            }

            @DexIgnore
            @Override // com.fossil.c44.a
            public int getCount() {
                e24 e24;
                e24 e242 = (e24) this.b.getValue();
                if ((e242 == null || e242.get() == 0) && (e24 = (e24) s14.this.d.get(getElement())) != null) {
                    return e24.get();
                }
                if (e242 == null) {
                    return 0;
                }
                return e242.get();
            }

            @DexIgnore
            @Override // com.fossil.c44.a
            public E getElement() {
                return (E) this.b.getKey();
            }
        }

        @DexIgnore
        public a(Iterator it) {
            this.c = it;
        }

        @DexIgnore
        /* renamed from: a */
        public c44.a<E> next() {
            Map.Entry<E, e24> entry = (Map.Entry) this.c.next();
            this.b = entry;
            return new C0212a(entry);
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c.hasNext();
        }

        @DexIgnore
        public void remove() {
            a24.c(this.b != null);
            s14.access$122(s14.this, (long) this.b.getValue().getAndSet(0));
            this.c.remove();
            this.b = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<E, e24>> b;
        @DexIgnore
        public Map.Entry<E, e24> c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b() {
            this.b = s14.this.d.entrySet().iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.d > 0 || this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (this.d == 0) {
                Map.Entry<E, e24> next = this.b.next();
                this.c = next;
                this.d = next.getValue().get();
            }
            this.d--;
            this.e = true;
            return this.c.getKey();
        }

        @DexIgnore
        public void remove() {
            a24.c(this.e);
            if (this.c.getValue().get() > 0) {
                if (this.c.getValue().addAndGet(-1) == 0) {
                    this.b.remove();
                }
                s14.access$110(s14.this);
                this.e = false;
                return;
            }
            throw new ConcurrentModificationException();
        }
    }

    @DexIgnore
    public s14(Map<E, e24> map) {
        i14.l(map);
        this.d = map;
    }

    @DexIgnore
    public static int a(e24 e24, int i) {
        if (e24 == null) {
            return 0;
        }
        return e24.getAndSet(i);
    }

    @DexIgnore
    public static /* synthetic */ long access$110(s14 s14) {
        long j = s14.e;
        s14.e = j - 1;
        return j;
    }

    @DexIgnore
    public static /* synthetic */ long access$122(s14 s14, long j) {
        long j2 = s14.e - j;
        s14.e = j2;
        return j2;
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.v14
    @CanIgnoreReturnValue
    public int add(E e2, int i) {
        boolean z = true;
        int i2 = 0;
        if (i == 0) {
            return count(e2);
        }
        i14.f(i > 0, "occurrences cannot be negative: %s", i);
        e24 e24 = this.d.get(e2);
        if (e24 == null) {
            this.d.put(e2, new e24(i));
        } else {
            int i3 = e24.get();
            long j = ((long) i3) + ((long) i);
            if (j > 2147483647L) {
                z = false;
            }
            i14.g(z, "too many occurrences: %s", j);
            e24.add(i);
            i2 = i3;
        }
        this.e += (long) i;
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.v14
    public void clear() {
        for (e24 e24 : this.d.values()) {
            e24.set(0);
        }
        this.d.clear();
        this.e = 0;
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.v14
    public int count(Object obj) {
        e24 e24 = (e24) x34.n(this.d, obj);
        if (e24 == null) {
            return 0;
        }
        return e24.get();
    }

    @DexIgnore
    @Override // com.fossil.v14
    public int distinctElements() {
        return this.d.size();
    }

    @DexIgnore
    @Override // com.fossil.v14
    public Iterator<c44.a<E>> entryIterator() {
        return new a(this.d.entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.v14
    public Set<c44.a<E>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.v14, java.lang.Iterable
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.v14
    @CanIgnoreReturnValue
    public int remove(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        i14.f(i > 0, "occurrences cannot be negative: %s", i);
        e24 e24 = this.d.get(obj);
        if (e24 == null) {
            return 0;
        }
        int i2 = e24.get();
        if (i2 <= i) {
            this.d.remove(obj);
            i = i2;
        }
        e24.add(-i);
        this.e -= (long) i;
        return i2;
    }

    @DexIgnore
    public void setBackingMap(Map<E, e24> map) {
        this.d = map;
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.v14
    @CanIgnoreReturnValue
    public int setCount(E e2, int i) {
        int i2;
        a24.b(i, "count");
        if (i == 0) {
            i2 = a(this.d.remove(e2), i);
        } else {
            e24 e24 = this.d.get(e2);
            int a2 = a(e24, i);
            if (e24 == null) {
                this.d.put(e2, new e24(i));
            }
            i2 = a2;
        }
        this.e += (long) (i - i2);
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.v14
    public int size() {
        return v54.b(this.e);
    }
}
