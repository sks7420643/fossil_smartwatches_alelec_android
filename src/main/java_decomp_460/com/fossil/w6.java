package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w6 {
    @DexIgnore
    public /* synthetic */ w6(kq7 kq7) {
    }

    @DexIgnore
    public final x6 a(int i) {
        x6 x6Var;
        x6[] values = x6.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                x6Var = null;
                break;
            }
            x6Var = values[i2];
            if (x6Var.b == i) {
                break;
            }
            i2++;
        }
        return x6Var != null ? x6Var : x6.l;
    }
}
