package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb7 implements gb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public byte[] f2812a;
    @DexIgnore
    public String b;
    @DexIgnore
    public nb7 c;
    @DexIgnore
    public jb7 d;
    @DexIgnore
    public lb7 e;
    @DexIgnore
    public int f;

    @DexIgnore
    public pb7(byte[] bArr, String str, nb7 nb7, jb7 jb7, lb7 lb7, int i) {
        pq7.c(bArr, "tickerData");
        pq7.c(str, "nameOfTicker");
        pq7.c(nb7, "colour");
        pq7.c(jb7, "metric");
        pq7.c(lb7, "type");
        this.f2812a = bArr;
        this.b = str;
        this.c = nb7;
        this.d = jb7;
        this.e = lb7;
        this.f = i;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public int a() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public jb7 b() {
        return this.d;
    }

    @DexIgnore
    public final nb7 c() {
        return this.c;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final byte[] e() {
        return this.f2812a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(pb7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            pb7 pb7 = (pb7) obj;
            if (!pq7.a(this.b, pb7.b)) {
                return false;
            }
            if (this.c != pb7.c) {
                return false;
            }
            if (!pq7.a(b(), pb7.b())) {
                return false;
            }
            if (getType() != pb7.getType()) {
                return false;
            }
            return a() == pb7.a();
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFTickerData");
    }

    @DexIgnore
    public final void f(byte[] bArr) {
        pq7.c(bArr, "<set-?>");
        this.f2812a = bArr;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public lb7 getType() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.b.hashCode() * 31) + this.c.hashCode()) * 31) + b().hashCode()) * 31) + getType().hashCode()) * 31) + a();
    }

    @DexIgnore
    public String toString() {
        return "WFTickerData(tickerData=" + Arrays.toString(this.f2812a) + ", nameOfTicker=" + this.b + ", colour=" + this.c + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
