package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it {
    @DexIgnore
    public /* synthetic */ it(kq7 kq7) {
    }

    @DexIgnore
    public final kt a(byte b) {
        kt ktVar;
        kt[] values = kt.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                ktVar = null;
                break;
            }
            ktVar = values[i];
            if (ktVar.c == b) {
                break;
            }
            i++;
        }
        return ktVar != null ? ktVar : kt.i;
    }
}
