package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b23 implements n23 {
    @DexIgnore
    @Override // com.fossil.n23
    public final boolean zza(Class<?> cls) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.n23
    public final k23 zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
