package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj4 extends JsonElement {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fj4 f1138a; // = new fj4();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof fj4);
    }

    @DexIgnore
    public int hashCode() {
        return fj4.class.hashCode();
    }
}
