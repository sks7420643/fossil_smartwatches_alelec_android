package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.zk0;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c62 extends d62 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static /* final */ c62 e; // = new c62();
    @DexIgnore
    public String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"HandlerLeak"})
    public final class a extends ol2 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f566a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f566a = context.getApplicationContext();
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int i2 = c62.this.i(this.f566a);
            if (c62.this.m(i2)) {
                c62.this.t(this.f566a, i2);
            }
        }
    }

    @DexIgnore
    public static c62 q() {
        return e;
    }

    @DexIgnore
    public static Dialog u(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(bc2.d(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        x(activity, create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    @DexIgnore
    public static Dialog v(Context context, int i, cc2 cc2, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(bc2.d(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String c2 = bc2.c(context, i);
        if (c2 != null) {
            builder.setPositiveButton(c2, cc2);
        }
        String g = bc2.g(context, i);
        if (g != null) {
            builder.setTitle(g);
        }
        return builder.create();
    }

    @DexIgnore
    public static void x(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            k62.v6(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        a62.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @DexIgnore
    public final boolean A(Activity activity, o72 o72, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog v = v(activity, i, cc2.b(o72, d(activity, i, "d"), 2), onCancelListener);
        if (v == null) {
            return false;
        }
        x(activity, v, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @DexIgnore
    public final boolean B(Context context, z52 z52, int i) {
        PendingIntent p = p(context, z52);
        if (p == null) {
            return false;
        }
        z(context, z52.c(), null, GoogleApiActivity.a(context, p, i));
        return true;
    }

    @DexIgnore
    public final String C() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    @DexIgnore
    @Override // com.fossil.d62
    public Intent d(Context context, int i, String str) {
        return super.d(context, i, str);
    }

    @DexIgnore
    @Override // com.fossil.d62
    public PendingIntent e(Context context, int i, int i2) {
        return super.e(context, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.d62
    public final String g(int i) {
        return super.g(i);
    }

    @DexIgnore
    @Override // com.fossil.d62
    public int i(Context context) {
        return super.i(context);
    }

    @DexIgnore
    @Override // com.fossil.d62
    public int j(Context context, int i) {
        return super.j(context, i);
    }

    @DexIgnore
    @Override // com.fossil.d62
    public final boolean m(int i) {
        return super.m(i);
    }

    @DexIgnore
    public Dialog o(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return v(activity, i, cc2.a(activity, d(activity, i, "d"), i2), onCancelListener);
    }

    @DexIgnore
    public PendingIntent p(Context context, z52 z52) {
        return z52.k() ? z52.h() : e(context, z52.c(), 0);
    }

    @DexIgnore
    public boolean r(Activity activity, int i, int i2) {
        return s(activity, i, i2, null);
    }

    @DexIgnore
    public boolean s(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog o = o(activity, i, i2, onCancelListener);
        if (o == null) {
            return false;
        }
        x(activity, o, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @DexIgnore
    public void t(Context context, int i) {
        z(context, i, null, f(context, i, 0, "n"));
    }

    @DexIgnore
    public final o92 w(Context context, q92 q92) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        o92 o92 = new o92(q92);
        context.registerReceiver(o92, intentFilter);
        o92.b(context);
        if (l(context, "com.google.android.gms")) {
            return o92;
        }
        q92.a();
        o92.a();
        return null;
    }

    @DexIgnore
    public final void y(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000);
    }

    @DexIgnore
    @TargetApi(20)
    public final void z(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        Log.w("GoogleApiAvailability", String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", Integer.valueOf(i), null), new IllegalArgumentException());
        if (i == 18) {
            y(context);
        } else if (pendingIntent != null) {
            String f = bc2.f(context, i);
            String e2 = bc2.e(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            zk0.e eVar = new zk0.e(context);
            eVar.t(true);
            eVar.g(true);
            eVar.n(f);
            zk0.c cVar = new zk0.c();
            cVar.g(e2);
            eVar.A(cVar);
            if (if2.b(context)) {
                rc2.n(mf2.g());
                eVar.y(context.getApplicationInfo().icon);
                eVar.w(2);
                if (if2.d(context)) {
                    eVar.a(v52.common_full_open_on_phone, resources.getString(w52.common_open_on_phone), pendingIntent);
                } else {
                    eVar.l(pendingIntent);
                }
            } else {
                eVar.y(17301642);
                eVar.B(resources.getString(w52.common_google_play_services_notification_ticker));
                eVar.E(System.currentTimeMillis());
                eVar.l(pendingIntent);
                eVar.m(e2);
            }
            if (mf2.j()) {
                rc2.n(mf2.j());
                String C = C();
                if (C == null) {
                    C = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
                    String b = bc2.b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel("com.google.android.gms.availability", b, 4));
                    } else if (!b.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(b);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                eVar.j(C);
            }
            Notification c2 = eVar.c();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                h62.d.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, c2);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
