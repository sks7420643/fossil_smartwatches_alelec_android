package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l93 implements xw2<k93> {
    @DexIgnore
    public static l93 c; // = new l93();
    @DexIgnore
    public /* final */ xw2<k93> b;

    @DexIgnore
    public l93() {
        this(ww2.b(new n93()));
    }

    @DexIgnore
    public l93(xw2<k93> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((k93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ k93 zza() {
        return this.b.zza();
    }
}
