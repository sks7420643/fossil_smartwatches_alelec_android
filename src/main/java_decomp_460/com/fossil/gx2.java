package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx2 extends kx2<K> {
    @DexIgnore
    public /* final */ /* synthetic */ hx2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gx2(hx2 hx2) {
        super(hx2, null);
        this.f = hx2;
    }

    @DexIgnore
    @Override // com.fossil.kx2
    public final K a(int i) {
        return (K) this.f.zzb[i];
    }
}
