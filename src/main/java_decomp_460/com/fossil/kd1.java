package com.fossil;

import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd1 implements mb1 {
    @DexIgnore
    public static /* final */ fk1<Class<?>, byte[]> j; // = new fk1<>(50);
    @DexIgnore
    public /* final */ od1 b;
    @DexIgnore
    public /* final */ mb1 c;
    @DexIgnore
    public /* final */ mb1 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ ob1 h;
    @DexIgnore
    public /* final */ sb1<?> i;

    @DexIgnore
    public kd1(od1 od1, mb1 mb1, mb1 mb12, int i2, int i3, sb1<?> sb1, Class<?> cls, ob1 ob1) {
        this.b = od1;
        this.c = mb1;
        this.d = mb12;
        this.e = i2;
        this.f = i3;
        this.i = sb1;
        this.g = cls;
        this.h = ob1;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.e(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        sb1<?> sb1 = this.i;
        if (sb1 != null) {
            sb1.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(c());
        this.b.f(bArr);
    }

    @DexIgnore
    public final byte[] c() {
        byte[] g2 = j.g(this.g);
        if (g2 != null) {
            return g2;
        }
        byte[] bytes = this.g.getName().getBytes(mb1.f2349a);
        j.k(this.g, bytes);
        return bytes;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof kd1)) {
            return false;
        }
        kd1 kd1 = (kd1) obj;
        return this.f == kd1.f && this.e == kd1.e && jk1.d(this.i, kd1.i) && this.g.equals(kd1.g) && this.c.equals(kd1.c) && this.d.equals(kd1.d) && this.h.equals(kd1.h);
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        sb1<?> sb1 = this.i;
        if (sb1 != null) {
            hashCode = (hashCode * 31) + sb1.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + "', options=" + this.h + '}';
    }
}
