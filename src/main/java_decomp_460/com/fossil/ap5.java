package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap5 implements Factory<zo5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<yo5> f302a;

    @DexIgnore
    public ap5(Provider<yo5> provider) {
        this.f302a = provider;
    }

    @DexIgnore
    public static ap5 a(Provider<yo5> provider) {
        return new ap5(provider);
    }

    @DexIgnore
    public static zo5 c(yo5 yo5) {
        return new zo5(yo5);
    }

    @DexIgnore
    /* renamed from: b */
    public zo5 get() {
        return c(this.f302a.get());
    }
}
