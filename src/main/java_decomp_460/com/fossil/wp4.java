package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp4 implements Factory<uq4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3980a;

    @DexIgnore
    public wp4(uo4 uo4) {
        this.f3980a = uo4;
    }

    @DexIgnore
    public static wp4 a(uo4 uo4) {
        return new wp4(uo4);
    }

    @DexIgnore
    public static uq4 c(uo4 uo4) {
        uq4 D = uo4.D();
        lk7.c(D, "Cannot return null from a non-@Nullable @Provides method");
        return D;
    }

    @DexIgnore
    /* renamed from: b */
    public uq4 get() {
        return c(this.f3980a);
    }
}
