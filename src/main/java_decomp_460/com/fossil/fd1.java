package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fd1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<mb1, yc1<?>> f1107a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<mb1, yc1<?>> b; // = new HashMap();

    @DexIgnore
    public yc1<?> a(mb1 mb1, boolean z) {
        return b(z).get(mb1);
    }

    @DexIgnore
    public final Map<mb1, yc1<?>> b(boolean z) {
        return z ? this.b : this.f1107a;
    }

    @DexIgnore
    public void c(mb1 mb1, yc1<?> yc1) {
        b(yc1.p()).put(mb1, yc1);
    }

    @DexIgnore
    public void d(mb1 mb1, yc1<?> yc1) {
        Map<mb1, yc1<?>> b2 = b(yc1.p());
        if (yc1.equals(b2.get(mb1))) {
            b2.remove(mb1);
        }
    }
}
