package com.fossil;

import com.fossil.w94;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x94 implements u94 {
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f4078a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public w94 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements w94.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ byte[] f4079a;
        @DexIgnore
        public /* final */ /* synthetic */ int[] b;

        @DexIgnore
        public a(x94 x94, byte[] bArr, int[] iArr) {
            this.f4079a = bArr;
            this.b = iArr;
        }

        @DexIgnore
        @Override // com.fossil.w94.d
        public void a(InputStream inputStream, int i) throws IOException {
            try {
                inputStream.read(this.f4079a, this.b[0], i);
                int[] iArr = this.b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ byte[] f4080a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(x94 x94, byte[] bArr, int i) {
            this.f4080a = bArr;
            this.b = i;
        }
    }

    @DexIgnore
    public x94(File file, int i) {
        this.f4078a = file;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.u94
    public void a() {
        r84.e(this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    @DexIgnore
    @Override // com.fossil.u94
    public String b() {
        byte[] c2 = c();
        if (c2 != null) {
            return new String(c2, d);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.u94
    public byte[] c() {
        b g = g();
        if (g == null) {
            return null;
        }
        int i = g.b;
        byte[] bArr = new byte[i];
        System.arraycopy(g.f4080a, 0, bArr, 0, i);
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.u94
    public void d() {
        a();
        this.f4078a.delete();
    }

    @DexIgnore
    @Override // com.fossil.u94
    public void e(long j, String str) {
        h();
        f(j, str);
    }

    @DexIgnore
    public final void f(long j, String str) {
        if (this.c != null) {
            String str2 = str == null ? "null" : str;
            try {
                int i = this.b / 4;
                if (str2.length() > i) {
                    str2 = "..." + str2.substring(str2.length() - i);
                }
                this.c.h(String.format(Locale.US, "%d %s%n", Long.valueOf(j), str2.replaceAll("\r", " ").replaceAll("\n", " ")).getBytes(d));
                while (!this.c.A() && this.c.T() > this.b) {
                    this.c.M();
                }
            } catch (IOException e) {
                x74.f().e("There was a problem writing to the Crashlytics log.", e);
            }
        }
    }

    @DexIgnore
    public final b g() {
        if (!this.f4078a.exists()) {
            return null;
        }
        h();
        w94 w94 = this.c;
        if (w94 == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[w94.T()];
        try {
            this.c.m(new a(this, bArr, iArr));
        } catch (IOException e) {
            x74.f().e("A problem occurred while reading the Crashlytics log file.", e);
        }
        return new b(this, bArr, iArr[0]);
    }

    @DexIgnore
    public final void h() {
        if (this.c == null) {
            try {
                this.c = new w94(this.f4078a);
            } catch (IOException e) {
                x74 f = x74.f();
                f.e("Could not open log file: " + this.f4078a, e);
            }
        }
    }
}
