package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p83 implements q83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2799a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.experiment.enable_experiment_reporting", true);

    @DexIgnore
    @Override // com.fossil.q83
    public final boolean zza() {
        return f2799a.o().booleanValue();
    }
}
