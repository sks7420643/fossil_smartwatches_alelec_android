package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug1 implements qb1<Bitmap, Bitmap> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements id1<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap b;

        @DexIgnore
        public a(Bitmap bitmap) {
            this.b = bitmap;
        }

        @DexIgnore
        /* renamed from: a */
        public Bitmap get() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.id1
        public void b() {
        }

        @DexIgnore
        @Override // com.fossil.id1
        public int c() {
            return jk1.h(this.b);
        }

        @DexIgnore
        @Override // com.fossil.id1
        public Class<Bitmap> d() {
            return Bitmap.class;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(Bitmap bitmap, int i, int i2, ob1 ob1) {
        return new a(bitmap);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Bitmap bitmap, ob1 ob1) {
        return true;
    }
}
