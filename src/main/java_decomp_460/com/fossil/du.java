package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum du {
    HEARTBEAT_INTERVAL(new byte[]{(byte) 240});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public du(byte[] bArr) {
        this.b = bArr;
    }
}
