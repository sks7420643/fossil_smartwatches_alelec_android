package com.fossil;

import android.graphics.Point;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2784a; // = 0;
    @DexIgnore
    public Point b;
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public void a() {
        this.f2784a = 0;
        this.c = 0;
        this.d = false;
        this.e = false;
    }
}
