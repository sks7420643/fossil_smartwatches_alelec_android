package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rb extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ qb CREATOR; // = new qb(null);
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public rb(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("is_front_light_enabled", this.b);
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(rb.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((rb) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.frontlight.FrontLightConfig");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.b).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
        }
    }
}
