package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez7 implements iv7 {
    @DexIgnore
    public /* final */ tn7 b;

    @DexIgnore
    public ez7(tn7 tn7) {
        this.b = tn7;
    }

    @DexIgnore
    @Override // com.fossil.iv7
    public tn7 h() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineScope(coroutineContext=" + h() + ')';
    }
}
