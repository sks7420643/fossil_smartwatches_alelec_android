package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu4 implements Factory<xu4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<zt4> f4378a;
    @DexIgnore
    public /* final */ Provider<tt4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public yu4(Provider<zt4> provider, Provider<tt4> provider2, Provider<on5> provider3) {
        this.f4378a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static yu4 a(Provider<zt4> provider, Provider<tt4> provider2, Provider<on5> provider3) {
        return new yu4(provider, provider2, provider3);
    }

    @DexIgnore
    public static xu4 c(zt4 zt4, tt4 tt4, on5 on5) {
        return new xu4(zt4, tt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public xu4 get() {
        return c(this.f4378a.get(), this.b.get(), this.c.get());
    }
}
