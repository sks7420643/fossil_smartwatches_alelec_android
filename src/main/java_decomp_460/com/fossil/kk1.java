package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ g<Object> f1923a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g<Object> {
        @DexIgnore
        @Override // com.fossil.kk1.g
        public void a(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements d<List<T>> {
        @DexIgnore
        /* renamed from: a */
        public List<T> create() {
            return new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements g<List<T>> {
        @DexIgnore
        /* renamed from: b */
        public void a(List<T> list) {
            list.clear();
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T create();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements mn0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d<T> f1924a;
        @DexIgnore
        public /* final */ g<T> b;
        @DexIgnore
        public /* final */ mn0<T> c;

        @DexIgnore
        public e(mn0<T> mn0, d<T> dVar, g<T> gVar) {
            this.c = mn0;
            this.f1924a = dVar;
            this.b = gVar;
        }

        @DexIgnore
        @Override // com.fossil.mn0
        public boolean a(T t) {
            if (t instanceof f) {
                t.f().b(true);
            }
            this.b.a(t);
            return this.c.a(t);
        }

        @DexIgnore
        @Override // com.fossil.mn0
        public T b() {
            T b2 = this.c.b();
            if (b2 == null) {
                b2 = this.f1924a.create();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + b2.getClass());
                }
            }
            if (b2 instanceof f) {
                b2.f().b(false);
            }
            return b2;
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        mk1 f();
    }

    @DexIgnore
    public interface g<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public static <T extends f> mn0<T> a(mn0<T> mn0, d<T> dVar) {
        return b(mn0, dVar, c());
    }

    @DexIgnore
    public static <T> mn0<T> b(mn0<T> mn0, d<T> dVar, g<T> gVar) {
        return new e(mn0, dVar, gVar);
    }

    @DexIgnore
    public static <T> g<T> c() {
        return (g<T>) f1923a;
    }

    @DexIgnore
    public static <T extends f> mn0<T> d(int i, d<T> dVar) {
        return a(new on0(i), dVar);
    }

    @DexIgnore
    public static <T> mn0<List<T>> e() {
        return f(20);
    }

    @DexIgnore
    public static <T> mn0<List<T>> f(int i) {
        return b(new on0(i), new b(), new c());
    }
}
