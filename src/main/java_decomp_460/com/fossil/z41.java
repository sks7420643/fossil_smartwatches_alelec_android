package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ dv7 f4417a;
    @DexIgnore
    public /* final */ n81 b;
    @DexIgnore
    public /* final */ d81 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Drawable f;
    @DexIgnore
    public /* final */ Drawable g;
    @DexIgnore
    public /* final */ Drawable h;

    @DexIgnore
    public z41() {
        this(null, null, null, false, false, null, null, null, 255, null);
    }

    @DexIgnore
    public z41(dv7 dv7, n81 n81, d81 d81, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        pq7.c(dv7, "dispatcher");
        pq7.c(d81, "precision");
        this.f4417a = dv7;
        this.b = n81;
        this.c = d81;
        this.d = z;
        this.e = z2;
        this.f = drawable;
        this.g = drawable2;
        this.h = drawable3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ z41(dv7 dv7, n81 n81, d81 d81, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3, int i, kq7 kq7) {
        this((i & 1) != 0 ? bw7.b() : dv7, (i & 2) != 0 ? null : n81, (i & 4) != 0 ? d81.AUTOMATIC : d81, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? null : drawable, (i & 64) != 0 ? null : drawable2, (i & 128) == 0 ? drawable3 : null);
    }

    @DexIgnore
    public final boolean a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final dv7 c() {
        return this.f4417a;
    }

    @DexIgnore
    public final Drawable d() {
        return this.g;
    }

    @DexIgnore
    public final Drawable e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof z41) {
                z41 z41 = (z41) obj;
                if (!pq7.a(this.f4417a, z41.f4417a) || !pq7.a(this.b, z41.b) || !pq7.a(this.c, z41.c) || this.d != z41.d || this.e != z41.e || !pq7.a(this.f, z41.f) || !pq7.a(this.g, z41.g) || !pq7.a(this.h, z41.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Drawable f() {
        return this.f;
    }

    @DexIgnore
    public final d81 g() {
        return this.c;
    }

    @DexIgnore
    public final n81 h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        dv7 dv7 = this.f4417a;
        int hashCode = dv7 != null ? dv7.hashCode() : 0;
        n81 n81 = this.b;
        int hashCode2 = n81 != null ? n81.hashCode() : 0;
        d81 d81 = this.c;
        int hashCode3 = d81 != null ? d81.hashCode() : 0;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        boolean z2 = this.e;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        Drawable drawable = this.f;
        int hashCode4 = drawable != null ? drawable.hashCode() : 0;
        Drawable drawable2 = this.g;
        int hashCode5 = drawable2 != null ? drawable2.hashCode() : 0;
        Drawable drawable3 = this.h;
        if (drawable3 != null) {
            i2 = drawable3.hashCode();
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + i) * 31) + hashCode4) * 31) + hashCode5) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "DefaultRequestOptions(dispatcher=" + this.f4417a + ", transition=" + this.b + ", precision=" + this.c + ", allowHardware=" + this.d + ", allowRgb565=" + this.e + ", placeholder=" + this.f + ", error=" + this.g + ", fallback=" + this.h + ")";
    }
}
