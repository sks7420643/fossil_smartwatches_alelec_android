package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.z61;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a71 extends i71 implements z61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j81 f206a;
    @DexIgnore
    public /* final */ s61 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.memory.InvalidatableTargetDelegate", f = "TargetDelegate.kt", l = {220}, m = "error")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(a71 a71, qn7 qn7) {
            super(qn7);
            this.this$0 = a71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.memory.InvalidatableTargetDelegate", f = "TargetDelegate.kt", l = {203}, m = "success")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(a71 a71, qn7 qn7) {
            super(qn7);
            this.this$0 = a71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, false, null, this);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a71(j81 j81, s61 s61) {
        super(null);
        pq7.c(j81, "target");
        pq7.c(s61, "referenceCounter");
        this.f206a = j81;
        this.b = s61;
    }

    @DexIgnore
    @Override // com.fossil.z61
    public s61 a() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    @Override // com.fossil.i71
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object f(android.graphics.drawable.Drawable r7, com.fossil.n81 r8, com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r6 = this;
            r4 = 5
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.a71.a
            if (r0 == 0) goto L_0x0037
            r0 = r9
            com.fossil.a71$a r0 = (com.fossil.a71.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0037
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0046
            if (r0 != r5) goto L_0x003e
            java.lang.Object r0 = r2.L$3
            com.fossil.j81 r0 = (com.fossil.j81) r0
            java.lang.Object r0 = r2.L$2
            com.fossil.n81 r0 = (com.fossil.n81) r0
            java.lang.Object r0 = r2.L$1
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            java.lang.Object r0 = r2.L$0
            com.fossil.a71 r0 = (com.fossil.a71) r0
            com.fossil.el7.b(r1)
        L_0x0034:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0036:
            return r0
        L_0x0037:
            com.fossil.a71$a r0 = new com.fossil.a71$a
            r0.<init>(r6, r9)
            r2 = r0
            goto L_0x0015
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            com.fossil.el7.b(r1)
            com.fossil.j81 r1 = r6.f206a
            if (r8 != 0) goto L_0x0051
            r1.c(r7)
            goto L_0x0034
        L_0x0051:
            boolean r0 = r1 instanceof com.fossil.p81
            if (r0 != 0) goto L_0x008c
            com.fossil.q81 r0 = com.fossil.q81.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0088
            com.fossil.q81 r0 = com.fossil.q81.c
            int r0 = r0.b()
            if (r0 > r4) goto L_0x0088
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Ignoring '"
            r0.append(r2)
            r0.append(r8)
            java.lang.String r2 = "' as '"
            r0.append(r2)
            r0.append(r1)
            java.lang.String r2 = "' does not implement coil.transition.TransitionTarget."
            r0.append(r2)
            java.lang.String r2 = "TargetDelegate"
            java.lang.String r0 = r0.toString()
            android.util.Log.println(r4, r2, r0)
        L_0x0088:
            r1.c(r7)
            goto L_0x0034
        L_0x008c:
            r0 = r1
            com.fossil.p81 r0 = (com.fossil.p81) r0
            com.fossil.o81$a r4 = new com.fossil.o81$a
            r4.<init>(r7)
            r2.L$0 = r6
            r2.L$1 = r7
            r2.L$2 = r8
            r2.L$3 = r1
            r2.label = r5
            java.lang.Object r0 = r8.a(r0, r4, r2)
            if (r0 != r3) goto L_0x0034
            r0 = r3
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a71.f(android.graphics.drawable.Drawable, com.fossil.n81, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.i71
    public void h(BitmapDrawable bitmapDrawable, Drawable drawable) {
        k(bitmapDrawable != null ? bitmapDrawable.getBitmap() : null);
        this.f206a.d(drawable);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    @Override // com.fossil.i71
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object i(android.graphics.drawable.Drawable r7, boolean r8, com.fossil.n81 r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r6 = this;
            r4 = 5
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.fossil.a71.b
            if (r0 == 0) goto L_0x0039
            r0 = r10
            com.fossil.a71$b r0 = (com.fossil.a71.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0048
            if (r0 != r5) goto L_0x0040
            java.lang.Object r0 = r2.L$3
            com.fossil.j81 r0 = (com.fossil.j81) r0
            java.lang.Object r0 = r2.L$2
            com.fossil.n81 r0 = (com.fossil.n81) r0
            boolean r0 = r2.Z$0
            java.lang.Object r0 = r2.L$1
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            java.lang.Object r0 = r2.L$0
            com.fossil.a71 r0 = (com.fossil.a71) r0
            com.fossil.el7.b(r1)
        L_0x0036:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.a71$b r0 = new com.fossil.a71$b
            r0.<init>(r6, r10)
            r2 = r0
            goto L_0x0015
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r1)
            android.graphics.Bitmap r0 = com.fossil.j71.a(r7)
            r6.k(r0)
            com.fossil.j81 r1 = r6.f206a
            if (r9 != 0) goto L_0x005a
            r1.a(r7)
            goto L_0x0036
        L_0x005a:
            boolean r0 = r1 instanceof com.fossil.p81
            if (r0 != 0) goto L_0x0095
            com.fossil.q81 r0 = com.fossil.q81.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0091
            com.fossil.q81 r0 = com.fossil.q81.c
            int r0 = r0.b()
            if (r0 > r4) goto L_0x0091
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Ignoring '"
            r0.append(r2)
            r0.append(r9)
            java.lang.String r2 = "' as '"
            r0.append(r2)
            r0.append(r1)
            java.lang.String r2 = "' does not implement coil.transition.TransitionTarget."
            r0.append(r2)
            java.lang.String r2 = "TargetDelegate"
            java.lang.String r0 = r0.toString()
            android.util.Log.println(r4, r2, r0)
        L_0x0091:
            r1.a(r7)
            goto L_0x0036
        L_0x0095:
            r0 = r1
            com.fossil.p81 r0 = (com.fossil.p81) r0
            com.fossil.o81$b r4 = new com.fossil.o81$b
            r4.<init>(r7, r8)
            r2.L$0 = r6
            r2.L$1 = r7
            r2.Z$0 = r8
            r2.L$2 = r9
            r2.L$3 = r1
            r2.label = r5
            java.lang.Object r0 = r9.a(r0, r4, r2)
            if (r0 != r3) goto L_0x0036
            r0 = r3
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a71.i(android.graphics.drawable.Drawable, boolean, com.fossil.n81, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public void k(Bitmap bitmap) {
        z61.a.a(this, bitmap);
    }
}
