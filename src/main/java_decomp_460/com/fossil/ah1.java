package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah1 extends zg1<Drawable> {
    @DexIgnore
    public ah1(Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    public static id1<Drawable> f(Drawable drawable) {
        if (drawable != null) {
            return new ah1(drawable);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return Math.max(1, this.b.getIntrinsicWidth() * this.b.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<android.graphics.drawable.Drawable> */
    @Override // com.fossil.id1
    public Class<Drawable> d() {
        return this.b.getClass();
    }
}
