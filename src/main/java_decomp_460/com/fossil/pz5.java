package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pz5 implements Factory<y36> {
    @DexIgnore
    public static y36 a(oz5 oz5) {
        y36 a2 = oz5.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
