package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ long f; // = hy1.b(oq7.f2710a);
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ short e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gn1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final gn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new gn1(hy1.o(order.getInt(0)), order.getShort(4), order.getShort(6));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public gn1 b(Parcel parcel) {
            return new gn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gn1 createFromParcel(Parcel parcel) {
            return new gn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gn1[] newArray(int i) {
            return new gn1[i];
        }
    }

    @DexIgnore
    public gn1(long j, short s, short s2) throws IllegalArgumentException {
        super(zm1.TIME);
        this.c = j;
        this.d = (short) s;
        this.e = (short) s2;
        d();
    }

    @DexIgnore
    public /* synthetic */ gn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readLong();
        this.d = (short) ((short) parcel.readInt());
        this.e = (short) ((short) parcel.readInt());
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.c).putShort(this.d).putShort(this.e).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("second", this.c);
            jSONObject.put("millisecond", Short.valueOf(this.d));
            jSONObject.put("timezone_offset_in_minute", Short.valueOf(this.e));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        long j = f;
        long j2 = this.c;
        if (0 <= j2 && j >= j2) {
            short s = this.d;
            if (s >= 0 && 1000 >= s) {
                short s2 = this.e;
                if (Short.MIN_VALUE > s2 || Short.MAX_VALUE < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(e.c(e.e("timezoneOffsetInMinute("), this.e, ") is out of range ", "[-32768, 32767]."));
                }
                return;
            }
            throw new IllegalArgumentException(e.c(e.e("millisecond("), this.d, ") is out of range ", "[0, 1000]."));
        }
        StringBuilder e2 = e.e("second(");
        e2.append(this.c);
        e2.append(") is out of range ");
        e2.append("[0, ");
        e2.append(f);
        e2.append("].");
        throw new IllegalArgumentException(e2.toString());
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(gn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            gn1 gn1 = (gn1) obj;
            if (this.c != gn1.c) {
                return false;
            }
            if (this.d != gn1.d) {
                return false;
            }
            return this.e == gn1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.TimeConfig");
    }

    @DexIgnore
    public final short getMillisecond() {
        return this.d;
    }

    @DexIgnore
    public final long getSecond() {
        return this.c;
    }

    @DexIgnore
    public final short getTimezoneOffsetInMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (((((super.hashCode() * 31) + Long.valueOf(this.c).hashCode()) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.d));
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.e));
        }
    }
}
