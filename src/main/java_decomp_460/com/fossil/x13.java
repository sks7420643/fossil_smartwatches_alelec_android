package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x13 extends v13 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public x13() {
        super();
    }

    @DexIgnore
    public static <L> List<L> e(Object obj, long j, int i) {
        List<L> f = f(obj, j);
        if (f.isEmpty()) {
            List<L> t13 = f instanceof w13 ? new t13(i) : (!(f instanceof y23) || !(f instanceof m13)) ? new ArrayList<>(i) : ((m13) f).zza(i);
            e43.j(obj, j, t13);
            return t13;
        } else if (c.isAssignableFrom(f.getClass())) {
            ArrayList arrayList = new ArrayList(f.size() + i);
            arrayList.addAll(f);
            e43.j(obj, j, arrayList);
            return arrayList;
        } else if (f instanceof y33) {
            t13 t132 = new t13(f.size() + i);
            t132.addAll((y33) f);
            e43.j(obj, j, t132);
            return t132;
        } else if (!(f instanceof y23) || !(f instanceof m13)) {
            return f;
        } else {
            m13 m13 = (m13) f;
            if (m13.zza()) {
                return f;
            }
            m13 zza = m13.zza(f.size() + i);
            e43.j(obj, j, zza);
            return zza;
        }
    }

    @DexIgnore
    public static <E> List<E> f(Object obj, long j) {
        return (List) e43.F(obj, j);
    }

    @DexIgnore
    @Override // com.fossil.v13
    public final <E> void b(Object obj, Object obj2, long j) {
        List f = f(obj2, j);
        List e = e(obj, j, f.size());
        int size = e.size();
        int size2 = f.size();
        if (size > 0 && size2 > 0) {
            e.addAll(f);
        }
        if (size <= 0) {
            e = f;
        }
        e43.j(obj, j, e);
    }

    @DexIgnore
    @Override // com.fossil.v13
    public final void d(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) e43.F(obj, j);
        if (list instanceof w13) {
            unmodifiableList = ((w13) list).zze();
        } else if (c.isAssignableFrom(list.getClass())) {
            return;
        } else {
            if (!(list instanceof y23) || !(list instanceof m13)) {
                unmodifiableList = Collections.unmodifiableList(list);
            } else {
                m13 m13 = (m13) list;
                if (m13.zza()) {
                    m13.zzb();
                    return;
                }
                return;
            }
        }
        e43.j(obj, j, unmodifiableList);
    }
}
