package com.fossil;

import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.i38;
import com.fossil.v18;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l28 extends i38.j implements e18 {
    @DexIgnore
    public /* final */ f18 b;
    @DexIgnore
    public /* final */ x18 c;
    @DexIgnore
    public Socket d;
    @DexIgnore
    public Socket e;
    @DexIgnore
    public o18 f;
    @DexIgnore
    public t18 g;
    @DexIgnore
    public i38 h;
    @DexIgnore
    public k48 i;
    @DexIgnore
    public j48 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 1;
    @DexIgnore
    public /* final */ List<Reference<p28>> n; // = new ArrayList();
    @DexIgnore
    public long o; // = Long.MAX_VALUE;

    @DexIgnore
    public l28(f18 f18, x18 x18) {
        this.b = f18;
        this.c = x18;
    }

    @DexIgnore
    @Override // com.fossil.e18
    public t18 a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.i38.j
    public void b(i38 i38) {
        synchronized (this.b) {
            this.m = i38.G();
        }
    }

    @DexIgnore
    @Override // com.fossil.i38.j
    public void c(k38 k38) throws IOException {
        k38.f(d38.REFUSED_STREAM);
    }

    @DexIgnore
    public void d() {
        b28.h(this.d);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005c, code lost:
        if (r8.c.c() == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
        if (r8.d == null) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0064, code lost:
        if (r8.h == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0066, code lost:
        r1 = r8.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0068, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r8.m = r8.h.G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0071, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0129, code lost:
        throw new com.fossil.n28(new java.net.ProtocolException("Too many tunnel connections attempted: 21"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:40:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x012d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e(int r9, int r10, int r11, int r12, boolean r13, com.fossil.a18 r14, com.fossil.m18 r15) {
        /*
        // Method dump skipped, instructions count: 319
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l28.e(int, int, int, int, boolean, com.fossil.a18, com.fossil.m18):void");
    }

    @DexIgnore
    public final void f(int i2, int i3, a18 a18, m18 m18) throws IOException {
        Proxy b2 = this.c.b();
        this.d = (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) ? this.c.a().j().createSocket() : new Socket(b2);
        m18.f(a18, this.c.d(), b2);
        this.d.setSoTimeout(i3);
        try {
            w38.j().h(this.d, this.c.d(), i2);
            try {
                this.i = s48.d(s48.m(this.d));
                this.j = s48.c(s48.i(this.d));
            } catch (NullPointerException e2) {
                if ("throw with null exception".equals(e2.getMessage())) {
                    throw new IOException(e2);
                }
            }
        } catch (ConnectException e3) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.c.d());
            connectException.initCause(e3);
            throw connectException;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00be A[Catch:{ all -> 0x00c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x014e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g(com.fossil.k28 r9) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l28.g(com.fossil.k28):void");
    }

    @DexIgnore
    public final void h(int i2, int i3, int i4, a18 a18, m18 m18) throws IOException {
        v18 j2 = j();
        q18 j3 = j2.j();
        for (int i5 = 0; i5 < 21; i5++) {
            f(i2, i3, a18, m18);
            j2 = i(i3, i4, j2, j3);
            if (j2 != null) {
                b28.h(this.d);
                this.d = null;
                this.j = null;
                this.i = null;
                m18.d(a18, this.c.d(), this.c.b(), null);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final v18 i(int i2, int i3, v18 v18, q18 q18) throws IOException {
        Response c2;
        String str = "CONNECT " + b28.s(q18, true) + " HTTP/1.1";
        do {
            b38 b38 = new b38(null, null, this.i, this.j);
            this.i.e().g((long) i2, TimeUnit.MILLISECONDS);
            this.j.e().g((long) i3, TimeUnit.MILLISECONDS);
            b38.o(v18.e(), str);
            b38.a();
            Response.a d2 = b38.d(false);
            d2.p(v18);
            c2 = d2.c();
            long b2 = u28.b(c2);
            if (b2 == -1) {
                b2 = 0;
            }
            c58 k2 = b38.k(b2);
            b28.D(k2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            k2.close();
            int f2 = c2.f();
            if (f2 != 200) {
                if (f2 == 407) {
                    v18 = this.c.a().h().authenticate(this.c, c2);
                    if (v18 == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + c2.f());
                }
            } else if (this.i.d().u() && this.j.d().u()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        } while (!"close".equalsIgnoreCase(c2.j(WebSocketHandler.HEADER_CONNECTION)));
        return v18;
    }

    @DexIgnore
    public final v18 j() throws IOException {
        v18.a aVar = new v18.a();
        aVar.l(this.c.a().l());
        aVar.g("CONNECT", null);
        aVar.e("Host", b28.s(this.c.a().l(), true));
        aVar.e("Proxy-Connection", "Keep-Alive");
        aVar.e("User-Agent", c28.a());
        v18 b2 = aVar.b();
        Response.a aVar2 = new Response.a();
        aVar2.p(b2);
        aVar2.n(t18.HTTP_1_1);
        aVar2.g(407);
        aVar2.k("Preemptive Authenticate");
        aVar2.b(b28.c);
        aVar2.q(-1);
        aVar2.o(-1);
        aVar2.i("Proxy-Authenticate", "OkHttp-Preemptive");
        v18 authenticate = this.c.a().h().authenticate(this.c, aVar2.c());
        return authenticate != null ? authenticate : b2;
    }

    @DexIgnore
    public final void k(k28 k28, int i2, a18 a18, m18 m18) throws IOException {
        if (this.c.a().k() != null) {
            m18.u(a18);
            g(k28);
            m18.t(a18, this.f);
            if (this.g == t18.HTTP_2) {
                s(i2);
            }
        } else if (this.c.a().f().contains(t18.H2_PRIOR_KNOWLEDGE)) {
            this.e = this.d;
            this.g = t18.H2_PRIOR_KNOWLEDGE;
            s(i2);
        } else {
            this.e = this.d;
            this.g = t18.HTTP_1_1;
        }
    }

    @DexIgnore
    public o18 l() {
        return this.f;
    }

    @DexIgnore
    public boolean m(x08 x08, x18 x18) {
        if (this.n.size() >= this.m || this.k || !z18.f4406a.g(this.c.a(), x08)) {
            return false;
        }
        if (x08.l().m().equals(q().a().l().m())) {
            return true;
        }
        if (this.h == null || x18 == null || x18.b().type() != Proxy.Type.DIRECT || this.c.b().type() != Proxy.Type.DIRECT || !this.c.d().equals(x18.d()) || x18.a().e() != b48.f394a || !t(x08.l())) {
            return false;
        }
        try {
            x08.a().a(x08.l().m(), l().e());
            return true;
        } catch (SSLPeerUnverifiedException e2) {
            return false;
        }
    }

    @DexIgnore
    public boolean n(boolean z) {
        if (this.e.isClosed() || this.e.isInputShutdown() || this.e.isOutputShutdown()) {
            return false;
        }
        i38 i38 = this.h;
        if (i38 != null) {
            return i38.F(System.nanoTime());
        }
        if (z) {
            try {
                int soTimeout = this.e.getSoTimeout();
                try {
                    this.e.setSoTimeout(1);
                    if (this.i.u()) {
                        return false;
                    }
                    this.e.setSoTimeout(soTimeout);
                    return true;
                } finally {
                    this.e.setSoTimeout(soTimeout);
                }
            } catch (SocketTimeoutException e2) {
            } catch (IOException e3) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean o() {
        return this.h != null;
    }

    @DexIgnore
    public s28 p(OkHttpClient okHttpClient, Interceptor.Chain chain, p28 p28) throws SocketException {
        if (this.h != null) {
            return new h38(okHttpClient, chain, p28, this.h);
        }
        this.e.setSoTimeout(chain.a());
        this.i.e().g((long) chain.a(), TimeUnit.MILLISECONDS);
        this.j.e().g((long) chain.b(), TimeUnit.MILLISECONDS);
        return new b38(okHttpClient, p28, this.i, this.j);
    }

    @DexIgnore
    public x18 q() {
        return this.c;
    }

    @DexIgnore
    public Socket r() {
        return this.e;
    }

    @DexIgnore
    public final void s(int i2) throws IOException {
        this.e.setSoTimeout(0);
        i38.h hVar = new i38.h(true);
        hVar.d(this.e, this.c.a().l().m(), this.i, this.j);
        hVar.b(this);
        hVar.c(i2);
        i38 a2 = hVar.a();
        this.h = a2;
        a2.o0();
    }

    @DexIgnore
    public boolean t(q18 q18) {
        if (q18.z() != this.c.a().l().z()) {
            return false;
        }
        if (q18.m().equals(this.c.a().l().m())) {
            return true;
        }
        return this.f != null && b48.f394a.c(q18.m(), (X509Certificate) this.f.e().get(0));
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.c.a().l().m());
        sb.append(":");
        sb.append(this.c.a().l().z());
        sb.append(", proxy=");
        sb.append(this.c.b());
        sb.append(" hostAddress=");
        sb.append(this.c.d());
        sb.append(" cipherSuite=");
        o18 o18 = this.f;
        sb.append(o18 != null ? o18.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
