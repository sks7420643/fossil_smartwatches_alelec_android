package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ab7 f911a;
    @DexIgnore
    public /* final */ db7 b;

    @DexIgnore
    public eb7(ab7 ab7, db7 db7) {
        pq7.c(ab7, Constants.EVENT);
        pq7.c(db7, "selectedComplications");
        this.f911a = ab7;
        this.b = db7;
    }

    @DexIgnore
    public final ab7 a() {
        return this.f911a;
    }

    @DexIgnore
    public final db7 b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof eb7) {
                eb7 eb7 = (eb7) obj;
                if (!pq7.a(this.f911a, eb7.f911a) || !pq7.a(this.b, eb7.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        ab7 ab7 = this.f911a;
        int hashCode = ab7 != null ? ab7.hashCode() : 0;
        db7 db7 = this.b;
        if (db7 != null) {
            i = db7.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "SelectedComplicationsEvent(event=" + this.f911a + ", selectedComplications=" + this.b + ")";
    }
}
