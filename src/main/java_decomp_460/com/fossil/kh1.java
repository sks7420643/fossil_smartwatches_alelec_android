package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kh1 implements sb1<hh1> {
    @DexIgnore
    public /* final */ sb1<Bitmap> b;

    @DexIgnore
    public kh1(sb1<Bitmap> sb1) {
        ik1.d(sb1);
        this.b = sb1;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.sb1
    public id1<hh1> b(Context context, id1<hh1> id1, int i, int i2) {
        hh1 hh1 = id1.get();
        id1<Bitmap> yf1 = new yf1(hh1.e(), oa1.c(context).f());
        id1<Bitmap> b2 = this.b.b(context, yf1, i, i2);
        if (!yf1.equals(b2)) {
            yf1.b();
        }
        hh1.m(this.b, b2.get());
        return id1;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (obj instanceof kh1) {
            return this.b.equals(((kh1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return this.b.hashCode();
    }
}
