package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public byte e;
    @DexIgnore
    public byte f;
    @DexIgnore
    public byte g;
    @DexIgnore
    public byte h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public byte j;
    @DexIgnore
    public /* final */ zu1 k; // = p90.b.a(this.d);
    @DexIgnore
    public /* final */ av1 l; // = p90.b.b(this.d);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bv1 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                pq7.b(createByteArray, "parcel.createByteArray()!!");
                return new bv1(createByteArray);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bv1[] newArray(int i) {
            return new bv1[i];
        }
    }

    @DexIgnore
    public bv1(byte[] bArr) {
        this.b = bArr;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.c = order.get(0);
        this.d = order.getShort(1);
        this.e = order.get(3);
        this.f = order.get(4);
        this.g = order.get(5);
        this.h = order.get(6);
        this.i = order.get(7);
        this.j = order.get(8);
    }

    @DexIgnore
    public final byte[] a() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(bv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((bv1) obj).b);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppEvent");
    }

    @DexIgnore
    public final zu1 getMicroAppId() {
        return this.k;
    }

    @DexIgnore
    public final byte getRequestId() {
        return this.i;
    }

    @DexIgnore
    public final av1 getVariant() {
        return this.l;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.b);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.F3, this.k.name()), jd0.J3, this.l), jd0.t3, Byte.valueOf(this.c)), jd0.I3, Byte.valueOf(this.e)), jd0.O3, Byte.valueOf(this.f)), jd0.P3, Byte.valueOf(this.g)), jd0.O2, Byte.valueOf(this.h)), jd0.q, Byte.valueOf(this.i)), jd0.v3, Byte.valueOf(this.j));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
