package com.fossil;

import android.text.SpannableString;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d36 extends x26 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> m;
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> n; // = this.q.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ y26 o;
    @DexIgnore
    public /* final */ on5 p;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1", f = "NotificationWatchRemindersPresenter.kt", l = {151, 160}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d36 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d36$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.d36$a$a  reason: collision with other inner class name */
        public static final class C0042a extends ko7 implements vp7<iv7, qn7<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0042a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0042a aVar = new C0042a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends InactivityNudgeTimeModel>> qn7) {
                return ((C0042a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.q.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$remindMinutes$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super RemindTimeModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super RemindTimeModel> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.q.getRemindTimeDao().getRemindTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(d36 d36, qn7 qn7) {
            super(2, qn7);
            this.this$0 = d36;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00de  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0151  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0154  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 352
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d36.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<List<? extends InactivityNudgeTimeModel>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d36 f727a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {60}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $lInActivityNudgeTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d36$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.d36$b$a$a  reason: collision with other inner class name */
            public static final class C0043a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0043a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0043a aVar = new C0043a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0043a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.f727a.q.getInactivityNudgeTimeDao().upsertListInactivityNudgeTime(this.this$0.$lInActivityNudgeTimeModel);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$lInActivityNudgeTimeModel = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$lInActivityNudgeTimeModel, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 i2 = this.this$0.f727a.i();
                    C0043a aVar = new C0043a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public b(d36 d36) {
            this.f727a = d36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            if (list == null || list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
                arrayList.add(inactivityNudgeTimeModel);
                arrayList.add(inactivityNudgeTimeModel2);
                xw7 unused = gu7.d(this.f727a.k(), null, null, new a(this, arrayList, null), 3, null);
                return;
            }
            for (T t : list) {
                if (t.getNudgeTimeType() == 0) {
                    y26 y26 = this.f727a.o;
                    SpannableString h = ll5.h(t.getMinutes());
                    pq7.b(h, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    y26.t5(h);
                } else {
                    y26 y262 = this.f727a.o;
                    SpannableString h2 = ll5.h(t.getMinutes());
                    pq7.b(h2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    y262.Y5(h2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<RemindTimeModel> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d36 f728a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1", f = "NotificationWatchRemindersPresenter.kt", l = {79}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ RemindTimeModel $tempRemindTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d36$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.d36$c$a$a  reason: collision with other inner class name */
            public static final class C0044a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0044a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0044a aVar = new C0044a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0044a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.f728a.q.getRemindTimeDao().upsertRemindTimeModel(this.this$0.$tempRemindTimeModel);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, RemindTimeModel remindTimeModel, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$tempRemindTimeModel = remindTimeModel;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$tempRemindTimeModel, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 i2 = this.this$0.f728a.i();
                    C0044a aVar = new C0044a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(d36 d36) {
            this.f728a = d36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            if (remindTimeModel == null) {
                xw7 unused = gu7.d(this.f728a.k(), null, null, new a(this, new RemindTimeModel("RemindTime", 20), null), 3, null);
                return;
            }
            y26 y26 = this.f728a.o;
            String g = ll5.g(remindTimeModel.getMinutes());
            pq7.b(g, "TimeUtils.getRemindTimeS\u2026(remindTimeModel.minutes)");
            y26.b6(g);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<? extends InactivityNudgeTimeModel>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d36 f729a;

        @DexIgnore
        public d(d36 d36) {
            this.f729a = d36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            y26 unused = this.f729a.o;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<RemindTimeModel> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d36 f730a;

        @DexIgnore
        public e(d36 d36) {
            this.f730a = d36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            y26 unused = this.f730a.o;
        }
    }

    /*
    static {
        String simpleName = d36.class.getSimpleName();
        pq7.b(simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public d36(y26 y26, on5 on5, RemindersSettingsDatabase remindersSettingsDatabase) {
        pq7.c(y26, "mView");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.o = y26;
        this.p = on5;
        this.q = remindersSettingsDatabase;
        this.m = remindersSettingsDatabase.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
        boolean u0 = this.p.u0();
        this.e = u0;
        this.i = u0;
    }

    @DexIgnore
    public final boolean B() {
        return (this.i == this.e && this.j == this.f && this.k == this.g && this.l == this.h) ? false : true;
    }

    @DexIgnore
    public void C() {
        this.o.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.m;
        y26 y26 = this.o;
        if (y26 != null) {
            liveData.h((z26) y26, new b(this));
            this.n.h((LifecycleOwner) this.o, new c(this));
            boolean v0 = this.p.v0();
            this.f = v0;
            this.j = v0;
            boolean w0 = this.p.w0();
            this.g = w0;
            this.k = w0;
            boolean t0 = this.p.t0();
            this.h = t0;
            this.l = t0;
            this.o.w4(this.i);
            this.o.S1(this.j);
            this.o.n4(this.k);
            this.o.s1(this.l);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.m.m(new d(this));
        this.n.m(new e(this));
    }

    @DexIgnore
    @Override // com.fossil.x26
    public void n() {
        this.l = !this.l;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.x26
    public void o() {
        this.i = !this.i;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
        this.o.w4(this.i);
    }

    @DexIgnore
    @Override // com.fossil.x26
    public void p() {
        this.j = !this.j;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.x26
    public void q() {
        this.k = !this.k;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.x26
    public void r() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }
}
