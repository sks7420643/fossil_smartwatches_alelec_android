package com.fossil;

import java.util.Collection;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk1 extends wk1 {
    @DexIgnore
    public /* final */ HashMap<String, wk1> b; // = new HashMap<>();

    @DexIgnore
    @Override // com.fossil.wk1
    public boolean a(e60 e60) {
        for (wk1 wk1 : this.b.values()) {
            if (!wk1.a(e60)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final xk1 setDeviceTypes(al1[] al1Arr) {
        HashMap<String, wk1> hashMap = this.b;
        String simpleName = l.class.getSimpleName();
        pq7.b(simpleName, "DeviceTypesScanFilter::class.java.simpleName");
        hashMap.put(simpleName, new l(al1Arr));
        return this;
    }

    @DexIgnore
    public final xk1 setSerialNumberPattern(String str) {
        HashMap<String, wk1> hashMap = this.b;
        String simpleName = m.class.getSimpleName();
        pq7.b(simpleName, "SerialNumberPatternScanF\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new m(str));
        return this;
    }

    @DexIgnore
    public final xk1 setSerialNumberPrefixes(String[] strArr) {
        HashMap<String, wk1> hashMap = this.b;
        String simpleName = n.class.getSimpleName();
        pq7.b(simpleName, "SerialNumberPrefixesScan\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new n(strArr));
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        jd0 jd0 = jd0.M1;
        Collection<wk1> values = this.b.values();
        pq7.b(values, "deviceFilters.values");
        Object[] array = values.toArray(new wk1[0]);
        if (array != null) {
            return g80.k(jSONObject, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
