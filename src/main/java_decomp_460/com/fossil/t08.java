package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t08 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f3347a;

    @DexIgnore
    public t08(Object obj) {
        this.f3347a = obj;
    }

    @DexIgnore
    public String toString() {
        return "Empty[" + this.f3347a + ']';
    }
}
