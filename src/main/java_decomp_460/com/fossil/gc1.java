package com.fossil;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gc1 extends ub1<InputStream> {
    @DexIgnore
    public gc1(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    /* renamed from: f */
    public void b(InputStream inputStream) throws IOException {
        inputStream.close();
    }

    @DexIgnore
    /* renamed from: g */
    public InputStream e(AssetManager assetManager, String str) throws IOException {
        return assetManager.open(str);
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
