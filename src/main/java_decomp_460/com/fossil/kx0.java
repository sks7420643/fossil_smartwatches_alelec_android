package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx0 implements ox0 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;

    @DexIgnore
    public kx0(String str) {
        this(str, null);
    }

    @DexIgnore
    public kx0(String str, Object[] objArr) {
        this.b = str;
        this.c = objArr;
    }

    @DexIgnore
    public static void d(nx0 nx0, int i, Object obj) {
        if (obj == null) {
            nx0.bindNull(i);
        } else if (obj instanceof byte[]) {
            nx0.bindBlob(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            nx0.bindDouble(i, (double) ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            nx0.bindDouble(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            nx0.bindLong(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            nx0.bindLong(i, (long) ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            nx0.bindLong(i, (long) ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            nx0.bindLong(i, (long) ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            nx0.bindString(i, (String) obj);
        } else if (obj instanceof Boolean) {
            nx0.bindLong(i, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte, string");
        }
    }

    @DexIgnore
    public static void e(nx0 nx0, Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                Object obj = objArr[i];
                i++;
                d(nx0, i, obj);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ox0
    public int a() {
        Object[] objArr = this.c;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    @DexIgnore
    @Override // com.fossil.ox0
    public String b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ox0
    public void c(nx0 nx0) {
        e(nx0, this.c);
    }
}
