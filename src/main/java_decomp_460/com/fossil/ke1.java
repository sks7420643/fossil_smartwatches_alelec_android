package com.fossil;

import com.fossil.kk1;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ke1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ fk1<mb1, String> f1903a; // = new fk1<>(1000);
    @DexIgnore
    public /* final */ mn0<b> b; // = kk1.d(10, new a(this));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements kk1.d<b> {
        @DexIgnore
        public a(ke1 ke1) {
        }

        @DexIgnore
        /* renamed from: a */
        public b create() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kk1.f {
        @DexIgnore
        public /* final */ MessageDigest b;
        @DexIgnore
        public /* final */ mk1 c; // = mk1.a();

        @DexIgnore
        public b(MessageDigest messageDigest) {
            this.b = messageDigest;
        }

        @DexIgnore
        @Override // com.fossil.kk1.f
        public mk1 f() {
            return this.c;
        }
    }

    @DexIgnore
    public final String a(mb1 mb1) {
        b b2 = this.b.b();
        ik1.d(b2);
        b bVar = b2;
        try {
            mb1.a(bVar.b);
            return jk1.t(bVar.b.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    @DexIgnore
    public String b(mb1 mb1) {
        String g;
        synchronized (this.f1903a) {
            g = this.f1903a.g(mb1);
        }
        if (g == null) {
            g = a(mb1);
        }
        synchronized (this.f1903a) {
            this.f1903a.k(mb1, g);
        }
        return g;
    }
}
