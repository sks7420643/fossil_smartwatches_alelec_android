package com.fossil;

import com.fossil.f38;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j38 implements Closeable {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(g38.class.getName());
    @DexIgnore
    public /* final */ k48 b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ f38.a e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c58 {
        @DexIgnore
        public /* final */ k48 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public byte d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public short g;

        @DexIgnore
        public a(k48 k48) {
            this.b = k48;
        }

        @DexIgnore
        public final void a() throws IOException {
            int i = this.e;
            int l = j38.l(this.b);
            this.f = l;
            this.c = l;
            byte readByte = (byte) (this.b.readByte() & 255);
            this.d = (byte) ((byte) (this.b.readByte() & 255));
            if (j38.f.isLoggable(Level.FINE)) {
                j38.f.fine(g38.b(true, this.e, this.c, readByte, this.d));
            }
            int readInt = this.b.readInt() & Integer.MAX_VALUE;
            this.e = readInt;
            if (readByte != 9) {
                g38.d("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (readInt != i) {
                g38.d("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @DexIgnore
        @Override // com.fossil.c58
        public long d0(i48 i48, long j) throws IOException {
            while (true) {
                int i = this.f;
                if (i == 0) {
                    this.b.skip((long) this.g);
                    this.g = (short) 0;
                    if ((this.d & 4) != 0) {
                        return -1;
                    }
                    a();
                } else {
                    long d0 = this.b.d0(i48, Math.min(j, (long) i));
                    if (d0 == -1) {
                        return -1;
                    }
                    this.f = (int) (((long) this.f) - d0);
                    return d0;
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public d58 e() {
            return this.b.e();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(boolean z, o38 o38);

        @DexIgnore
        void c(boolean z, int i, int i2, List<e38> list);

        @DexIgnore
        void d(int i, long j);

        @DexIgnore
        void e(boolean z, int i, k48 k48, int i2) throws IOException;

        @DexIgnore
        void f(boolean z, int i, int i2);

        @DexIgnore
        void g(int i, int i2, int i3, boolean z);

        @DexIgnore
        void h(int i, d38 d38);

        @DexIgnore
        void i(int i, int i2, List<e38> list) throws IOException;

        @DexIgnore
        void j(int i, d38 d38, l48 l48);
    }

    @DexIgnore
    public j38(k48 k48, boolean z) {
        this.b = k48;
        this.d = z;
        a aVar = new a(k48);
        this.c = aVar;
        this.e = new f38.a(4096, aVar);
    }

    @DexIgnore
    public static int a(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        g38.d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public static int l(k48 k48) throws IOException {
        return ((k48.readByte() & 255) << 16) | ((k48.readByte() & 255) << 8) | (k48.readByte() & 255);
    }

    @DexIgnore
    public final void A(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 5) {
            g38.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            o(bVar, i2);
        } else {
            g38.d("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void B(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            if ((b2 & 8) != 0) {
                s = (short) (this.b.readByte() & 255);
            }
            bVar.i(i2, this.b.readInt() & Integer.MAX_VALUE, j(a(i - 4, b2, s), s, b2, i2));
            return;
        }
        g38.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void C(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 4) {
            g38.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            int readInt = this.b.readInt();
            d38 fromHttp2 = d38.fromHttp2(readInt);
            if (fromHttp2 != null) {
                bVar.h(i2, fromHttp2);
                return;
            }
            g38.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            g38.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void D(b bVar, int i, byte b2, int i2) throws IOException {
        if (i2 != 0) {
            g38.d("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b2 & 1) != 0) {
            if (i == 0) {
                bVar.a();
            } else {
                g38.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i % 6 == 0) {
            o38 o38 = new o38();
            for (int i3 = 0; i3 < i; i3 += 6) {
                int readShort = this.b.readShort() & 65535;
                int readInt = this.b.readInt();
                if (readShort != 2) {
                    if (readShort == 3) {
                        readShort = 4;
                    } else if (readShort == 4) {
                        readShort = 7;
                        if (readInt < 0) {
                            g38.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    } else if (readShort == 5 && (readInt < 16384 || readInt > 16777215)) {
                        g38.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                        throw null;
                    }
                } else if (!(readInt == 0 || readInt == 1)) {
                    g38.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                    throw null;
                }
                o38.i(readShort, readInt);
            }
            bVar.b(false, o38);
        } else {
            g38.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public final void F(b bVar, int i, byte b2, int i2) throws IOException {
        if (i == 4) {
            long readInt = ((long) this.b.readInt()) & 2147483647L;
            if (readInt != 0) {
                bVar.d(i2, readInt);
                return;
            }
            g38.d("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        g38.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public boolean b(boolean z, b bVar) throws IOException {
        try {
            this.b.j0(9);
            int l = l(this.b);
            if (l < 0 || l > 16384) {
                g38.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(l));
                throw null;
            }
            byte readByte = (byte) (this.b.readByte() & 255);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.b.readByte() & 255);
                int readInt = this.b.readInt() & Integer.MAX_VALUE;
                if (f.isLoggable(Level.FINE)) {
                    f.fine(g38.b(true, readInt, l, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        f(bVar, l, readByte2, readInt);
                        return true;
                    case 1:
                        k(bVar, l, readByte2, readInt);
                        return true;
                    case 2:
                        A(bVar, l, readByte2, readInt);
                        return true;
                    case 3:
                        C(bVar, l, readByte2, readInt);
                        return true;
                    case 4:
                        D(bVar, l, readByte2, readInt);
                        return true;
                    case 5:
                        B(bVar, l, readByte2, readInt);
                        return true;
                    case 6:
                        m(bVar, l, readByte2, readInt);
                        return true;
                    case 7:
                        h(bVar, l, readByte2, readInt);
                        return true;
                    case 8:
                        F(bVar, l, readByte2, readInt);
                        return true;
                    default:
                        this.b.skip((long) l);
                        return true;
                }
            } else {
                g38.d("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
                throw null;
            }
        } catch (IOException e2) {
            return false;
        }
    }

    @DexIgnore
    public void c(b bVar) throws IOException {
        if (!this.d) {
            l48 i = this.b.i((long) g38.f1257a.size());
            if (f.isLoggable(Level.FINE)) {
                f.fine(b28.r("<< CONNECTION %s", i.hex()));
            }
            if (!g38.f1257a.equals(i)) {
                g38.d("Expected a connection header but was %s", i.utf8());
                throw null;
            }
        } else if (!b(true, bVar)) {
            g38.d("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    public final void f(b bVar, int i, byte b2, int i2) throws IOException {
        boolean z = true;
        short s = 0;
        if (i2 != 0) {
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b2 & 8) != 0) {
                    s = (short) (this.b.readByte() & 255);
                }
                bVar.e(z2, i2, this.b, a(i, b2, s));
                this.b.skip((long) s);
                return;
            }
            g38.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        g38.d("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void h(b bVar, int i, byte b2, int i2) throws IOException {
        if (i < 8) {
            g38.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.b.readInt();
            int readInt2 = this.b.readInt();
            int i3 = i - 8;
            d38 fromHttp2 = d38.fromHttp2(readInt2);
            if (fromHttp2 != null) {
                l48 l48 = l48.EMPTY;
                if (i3 > 0) {
                    l48 = this.b.i((long) i3);
                }
                bVar.j(readInt, fromHttp2, l48);
                return;
            }
            g38.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            g38.d("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final List<e38> j(int i, short s, byte b2, int i2) throws IOException {
        a aVar = this.c;
        aVar.f = i;
        aVar.c = i;
        aVar.g = (short) s;
        aVar.d = (byte) b2;
        aVar.e = i2;
        this.e.k();
        return this.e.e();
    }

    @DexIgnore
    public final void k(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.b.readByte() & 255);
            }
            if ((b2 & 32) != 0) {
                o(bVar, i2);
                i -= 5;
            }
            bVar.c(z, i2, -1, j(a(i, b2, s), s, b2, i2));
            return;
        }
        g38.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void m(b bVar, int i, byte b2, int i2) throws IOException {
        boolean z = true;
        if (i != 8) {
            g38.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.b.readInt();
            int readInt2 = this.b.readInt();
            if ((b2 & 1) == 0) {
                z = false;
            }
            bVar.f(z, readInt, readInt2);
        } else {
            g38.d("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void o(b bVar, int i) throws IOException {
        int readInt = this.b.readInt();
        bVar.g(i, readInt & Integer.MAX_VALUE, (this.b.readByte() & 255) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }
}
