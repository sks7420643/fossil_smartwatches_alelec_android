package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz5 implements MembersInjector<az5> {
    @DexIgnore
    public static void a(az5 az5, sd6 sd6) {
        az5.H = sd6;
    }

    @DexIgnore
    public static void b(az5 az5, we6 we6) {
        az5.G = we6;
    }

    @DexIgnore
    public static void c(az5 az5, bg6 bg6) {
        az5.I = bg6;
    }

    @DexIgnore
    public static void d(az5 az5, fh6 fh6) {
        az5.L = fh6;
    }

    @DexIgnore
    public static void e(az5 az5, ji6 ji6) {
        az5.J = ji6;
    }

    @DexIgnore
    public static void f(az5 az5, nj6 nj6) {
        az5.K = nj6;
    }
}
