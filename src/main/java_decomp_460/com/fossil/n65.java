package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ DashBar v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public n65(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, RTLImageView rTLImageView, DashBar dashBar, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = rTLImageView;
        this.v = dashBar;
        this.w = constraintLayout;
        this.x = flexibleTextView4;
    }
}
