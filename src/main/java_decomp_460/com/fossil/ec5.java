package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f912a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ Group c;
    @DexIgnore
    public /* final */ TextView d;
    @DexIgnore
    public /* final */ WebView e;

    @DexIgnore
    public ec5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, Group group, TextView textView, WebView webView) {
        this.f912a = constraintLayout;
        this.b = flexibleButton;
        this.c = group;
        this.d = textView;
        this.e = webView;
    }

    @DexIgnore
    public static ec5 a(View view) {
        int i = 2131363553;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131361957);
        if (flexibleButton != null) {
            Group group = (Group) view.findViewById(2131362571);
            if (group != null) {
                TextView textView = (TextView) view.findViewById(2131363330);
                if (textView != null) {
                    WebView webView = (WebView) view.findViewById(2131363553);
                    if (webView != null) {
                        return new ec5((ConstraintLayout) view, flexibleButton, group, textView, webView);
                    }
                } else {
                    i = 2131363330;
                }
            } else {
                i = 2131362571;
            }
        } else {
            i = 2131361957;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static ec5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static ec5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558638, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f912a;
    }
}
