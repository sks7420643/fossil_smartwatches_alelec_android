package com.fossil;

import android.content.Context;
import android.os.Binder;
import com.fossil.r62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i52 extends d52 {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public i52(Context context) {
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.c52
    public final void a() {
        e();
        o42 b2 = o42.b(this.b);
        GoogleSignInAccount c = b2.c();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.v;
        if (c != null) {
            googleSignInOptions = b2.d();
        }
        r62.a aVar = new r62.a(this.b);
        aVar.b(d42.e, googleSignInOptions);
        r62 g = aVar.g();
        try {
            if (g.d().A()) {
                if (c != null) {
                    d42.f.b(g);
                } else {
                    g.e();
                }
            }
        } finally {
            g.g();
        }
    }

    @DexIgnore
    public final void e() {
        if (!h62.i(this.b, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.c52
    public final void p() {
        e();
        b52.c(this.b).a();
    }
}
