package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw1 extends tw1 implements hc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<uw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public uw1 createFromParcel(Parcel parcel) {
            return new uw1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public uw1[] newArray(int i) {
            return new uw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ uw1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public uw1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
    }
}
