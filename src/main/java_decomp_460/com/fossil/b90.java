package com.fossil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b90 {
    @DexIgnore
    public final JSONArray a(File file) {
        JSONArray jSONArray = new JSONArray();
        String c = jy1.f1832a.c(file);
        if (c != null) {
            List Y = wt7.Y(c, new String[]{hd0.y.w()}, false, 0, 6, null);
            ArrayList<String> arrayList = new ArrayList();
            for (Object obj : Y) {
                if (!vt7.l((String) obj)) {
                    arrayList.add(obj);
                }
            }
            for (String str : arrayList) {
                String n = mx1.j.n(str);
                if (n != null) {
                    try {
                        jSONArray.put(new JSONObject(n));
                    } catch (Exception e) {
                        tl7 tl7 = tl7.f3441a;
                    }
                }
            }
        }
        return jSONArray;
    }

    @DexIgnore
    public JSONObject b(File file) {
        JSONObject jSONObject = new JSONObject();
        JSONArray a2 = a(file);
        if (a2.length() > 0) {
            jSONObject.put("data", a2);
        }
        return jSONObject;
    }
}
