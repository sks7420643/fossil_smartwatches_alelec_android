package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vc1<DataType, ResourceType, Transcode> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Class<DataType> f3747a;
    @DexIgnore
    public /* final */ List<? extends qb1<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ th1<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ mn0<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface a<ResourceType> {
        @DexIgnore
        id1<ResourceType> a(id1<ResourceType> id1);
    }

    @DexIgnore
    public vc1(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends qb1<DataType, ResourceType>> list, th1<ResourceType, Transcode> th1, mn0<List<Throwable>> mn0) {
        this.f3747a = cls;
        this.b = list;
        this.c = th1;
        this.d = mn0;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public id1<Transcode> a(xb1<DataType> xb1, int i, int i2, ob1 ob1, a<ResourceType> aVar) throws dd1 {
        return this.c.a(aVar.a(b(xb1, i, i2, ob1)), ob1);
    }

    @DexIgnore
    public final id1<ResourceType> b(xb1<DataType> xb1, int i, int i2, ob1 ob1) throws dd1 {
        List<Throwable> b2 = this.d.b();
        ik1.d(b2);
        List<Throwable> list = b2;
        try {
            return c(xb1, i, i2, ob1, list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final id1<ResourceType> c(xb1<DataType> xb1, int i, int i2, ob1 ob1, List<Throwable> list) throws dd1 {
        id1<ResourceType> id1;
        int size = this.b.size();
        id1<ResourceType> id12 = null;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                id1 = id12;
                break;
            }
            qb1 qb1 = (qb1) this.b.get(i3);
            try {
                id1 = qb1.a(xb1.b(), ob1) ? qb1.b(xb1.b(), i, i2, ob1) : id12;
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + qb1, e2);
                }
                list.add(e2);
                id1 = id12;
            }
            if (id1 != null) {
                break;
            }
            i3++;
            id12 = id1;
        }
        if (id1 != null) {
            return id1;
        }
        throw new dd1(this.e, new ArrayList(list));
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.f3747a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }
}
