package com.fossil;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj2 implements ThreadFactory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicInteger f3423a; // = new AtomicInteger(1);

    @DexIgnore
    public tj2(pj2 pj2) {
    }

    @DexIgnore
    public final Thread newThread(Runnable runnable) {
        int andIncrement = this.f3423a.getAndIncrement();
        StringBuilder sb = new StringBuilder(20);
        sb.append("gcm-task#");
        sb.append(andIncrement);
        Thread thread = new Thread(runnable, sb.toString());
        thread.setPriority(4);
        return thread;
    }
}
