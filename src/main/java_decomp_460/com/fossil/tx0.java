package com.fossil;

import android.database.sqlite.SQLiteProgram;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tx0 implements nx0 {
    @DexIgnore
    public /* final */ SQLiteProgram b;

    @DexIgnore
    public tx0(SQLiteProgram sQLiteProgram) {
        this.b = sQLiteProgram;
    }

    @DexIgnore
    @Override // com.fossil.nx0
    public void bindBlob(int i, byte[] bArr) {
        this.b.bindBlob(i, bArr);
    }

    @DexIgnore
    @Override // com.fossil.nx0
    public void bindDouble(int i, double d) {
        this.b.bindDouble(i, d);
    }

    @DexIgnore
    @Override // com.fossil.nx0
    public void bindLong(int i, long j) {
        this.b.bindLong(i, j);
    }

    @DexIgnore
    @Override // com.fossil.nx0
    public void bindNull(int i) {
        this.b.bindNull(i);
    }

    @DexIgnore
    @Override // com.fossil.nx0
    public void bindString(int i, String str) {
        this.b.bindString(i, str);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.b.close();
    }
}
