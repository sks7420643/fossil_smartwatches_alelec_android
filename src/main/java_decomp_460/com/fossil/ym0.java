package com.fossil;

import android.util.Base64;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4334a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ List<List<byte[]>> d;
    @DexIgnore
    public /* final */ int e; // = 0;
    @DexIgnore
    public /* final */ String f; // = (this.f4334a + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + this.b + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + this.c);

    @DexIgnore
    public ym0(String str, String str2, String str3, List<List<byte[]>> list) {
        pn0.d(str);
        this.f4334a = str;
        pn0.d(str2);
        this.b = str2;
        pn0.d(str3);
        this.c = str3;
        pn0.d(list);
        this.d = list;
    }

    @DexIgnore
    public List<List<byte[]>> a() {
        return this.d;
    }

    @DexIgnore
    public int b() {
        return this.e;
    }

    @DexIgnore
    public String c() {
        return this.f;
    }

    @DexIgnore
    public String d() {
        return this.f4334a;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public String f() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.f4334a + ", mProviderPackage: " + this.b + ", mQuery: " + this.c + ", mCertificates:");
        for (int i = 0; i < this.d.size(); i++) {
            sb.append(" [");
            List<byte[]> list = this.d.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString(list.get(i2), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.e);
        return sb.toString();
    }
}
