package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lr4 f2557a;

    @DexIgnore
    public nr4(lr4 lr4) {
        pq7.c(lr4, "dao");
        this.f2557a = lr4;
    }

    @DexIgnore
    public final rr4 a(String str) {
        pq7.c(str, "id");
        return this.f2557a.a(str);
    }

    @DexIgnore
    public final Long[] b(List<rr4> list) {
        pq7.c(list, "flag");
        return this.f2557a.insert(list);
    }
}
