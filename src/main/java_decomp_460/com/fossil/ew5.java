package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew5 extends hv0 {
    @DexIgnore
    public RecyclerView f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ a i;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a4(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ew5 f999a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ew5 ew5) {
            this.f999a = ew5;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            pq7.c(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0) {
                try {
                    View h = this.f999a.h(recyclerView.getLayoutManager());
                    ew5 ew5 = this.f999a;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager == null) {
                        pq7.i();
                        throw null;
                    } else if (h != null) {
                        ew5.x(layoutManager.i0(h));
                        if (this.f999a.h != this.f999a.w()) {
                            this.f999a.h = this.f999a.w();
                            a aVar = this.f999a.i;
                            if (aVar != null) {
                                aVar.a4(this.f999a.w());
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("BlurLinearSnapHelper", "attachToRecyclerView - e=" + e);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            pq7.c(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            this.f999a.h(recyclerView.getLayoutManager());
        }
    }

    @DexIgnore
    public ew5(a aVar) {
        this.i = aVar;
        this.h = -1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ew5(a aVar, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? null : aVar);
    }

    @DexIgnore
    @Override // com.fossil.qv0
    public void b(RecyclerView recyclerView) {
        super.b(recyclerView);
        this.f = recyclerView;
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new b(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.hv0, com.fossil.qv0
    public View h(RecyclerView.m mVar) {
        View h2 = super.h(mVar);
        v(mVar, h2);
        return h2;
    }

    @DexIgnore
    public final void v(RecyclerView.m mVar, View view) {
        View view2;
        View view3;
        if (view != null && mVar != null) {
            view.setAlpha(1.0f);
            int i2 = -1;
            int i3 = 0;
            int K = mVar.K() - 1;
            if (K >= 0) {
                while (true) {
                    if (!pq7.a(mVar.J(i3), view)) {
                        if (i3 == K) {
                            break;
                        }
                        i3++;
                    } else {
                        i2 = i3;
                        break;
                    }
                }
            }
            if (i2 >= 0) {
                if (i2 > 0 && i2 < mVar.K() - 1) {
                    view2 = mVar.J(i2 - 1);
                    view3 = mVar.J(i2 + 1);
                } else if (i2 == 0) {
                    view3 = mVar.J(i2 + 1);
                    view2 = null;
                } else if (i2 == mVar.K() - 1) {
                    view2 = mVar.J(i2 - 1);
                    view3 = null;
                } else {
                    view2 = null;
                    view3 = null;
                }
                if (view2 != null) {
                    view2.setAlpha(0.8f);
                }
                if (view3 != null) {
                    view3.setAlpha(0.8f);
                }
            }
        }
    }

    @DexIgnore
    public final int w() {
        return this.g;
    }

    @DexIgnore
    public final void x(int i2) {
        this.g = i2;
    }
}
