package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ vw1 e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public kq1 createFromParcel(Parcel parcel) {
            return new kq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public kq1[] newArray(int i) {
            return new kq1[i];
        }
    }

    @DexIgnore
    public kq1(byte b, int i, vw1 vw1, String str) {
        super(np1.IFTTT_APP, b, i);
        this.e = vw1;
        this.f = str;
    }

    @DexIgnore
    public /* synthetic */ kq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = vw1.values()[parcel.readInt()];
        String readString = parcel.readString();
        if (readString != null) {
            this.f = readString;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(kq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            kq1 kq1 = (kq1) obj;
            if (this.e != kq1.e) {
                return false;
            }
            String str = this.f;
            String str2 = kq1.f;
            if (str != null) {
                return !str.contentEquals(str2);
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest");
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.e.hashCode()) * 31) + this.f.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
    }
}
