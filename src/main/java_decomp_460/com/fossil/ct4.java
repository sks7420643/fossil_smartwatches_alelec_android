package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f651a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<? extends ms4>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends ms4>> {
    }

    @DexIgnore
    public ct4() {
        Gson d = new zi4().d();
        pq7.b(d, "GsonBuilder().create()");
        this.f651a = d;
    }

    @DexIgnore
    public final String a(ht4 ht4) {
        pq7.c(ht4, "owner");
        try {
            return this.f651a.u(ht4, ht4.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String b(ms4 ms4) {
        try {
            String u = this.f651a.u(ms4, ms4.class);
            pq7.b(u, "gson.toJson(player, BCPlayer::class.java)");
            return u;
        } catch (Exception e) {
            return "";
        }
    }

    @DexIgnore
    public final String c(List<ms4> list) {
        pq7.c(list, "players");
        String t = new Gson().t(list);
        pq7.b(t, "Gson().toJson(players)");
        return t;
    }

    @DexIgnore
    public final ht4 d(String str) {
        pq7.c(str, "value");
        try {
            return (ht4) this.f651a.k(str, ht4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final ms4 e(String str) {
        pq7.c(str, "value");
        try {
            return (ms4) this.f651a.k(str, ms4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final List<ms4> f(String str) {
        List<ms4> e;
        try {
            Object l = this.f651a.l(str, new a().getType());
            pq7.b(l, "gson.fromJson(value, obj\u2026ist<BCPlayer>>() {}.type)");
            return (List) l;
        } catch (Exception e2) {
            if (!(e2 instanceof mj4)) {
                return hm7.e();
            }
            try {
                FLogger.INSTANCE.getLocal().e("HistoryChallengeConverter", "stringToPlayers - apply date format for special case");
                zi4 zi4 = new zi4();
                zi4.g("MM dd, yyyy HH:mm:ss");
                e = (List) zi4.d().l(str, new b().getType());
            } catch (Exception e3) {
                e3.printStackTrace();
                e = hm7.e();
            }
            pq7.b(e, "try {\n                  \u2026ayer>()\n                }");
            return e;
        }
    }
}
