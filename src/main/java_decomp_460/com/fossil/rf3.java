package com.fossil;

import com.fossil.qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rf3 extends uc3 {
    @DexIgnore
    public /* final */ /* synthetic */ qb3.k b;

    @DexIgnore
    public rf3(qb3 qb3, qb3.k kVar) {
        this.b = kVar;
    }

    @DexIgnore
    @Override // com.fossil.tc3
    public final void U1(ls2 ls2) {
        this.b.onMarkerDragStart(new ke3(ls2));
    }

    @DexIgnore
    @Override // com.fossil.tc3
    public final void q2(ls2 ls2) {
        this.b.onMarkerDragEnd(new ke3(ls2));
    }

    @DexIgnore
    @Override // com.fossil.tc3
    public final void t2(ls2 ls2) {
        this.b.onMarkerDrag(new ke3(ls2));
    }
}
