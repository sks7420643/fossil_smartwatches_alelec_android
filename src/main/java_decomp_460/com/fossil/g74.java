package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class g74 implements mg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set f1271a;

    @DexIgnore
    public g74(Set set) {
        this.f1271a = set;
    }

    @DexIgnore
    public static mg4 a(Set set) {
        return new g74(set);
    }

    @DexIgnore
    @Override // com.fossil.mg4
    public Object get() {
        return i74.f(this.f1271a);
    }
}
