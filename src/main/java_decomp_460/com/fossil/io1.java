package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ oo1 d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<io1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public io1 createFromParcel(Parcel parcel) {
            byte readByte = parcel.readByte();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                Parcelable readParcelable = parcel.readParcelable(oo1.class.getClassLoader());
                if (readParcelable != null) {
                    return new io1(readByte, readString, (oo1) readParcelable);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public io1[] newArray(int i) {
            return new io1[i];
        }
    }

    @DexIgnore
    public io1(byte b2, String str, oo1 oo1) {
        this.b = (byte) b2;
        this.c = str;
        this.d = oo1;
        this.e = a();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ io1(byte b2, String str, oo1 oo1, int i, kq7 kq7) {
        this(b2, str, (i & 4) != 0 ? new oo1("", new byte[0]) : oo1);
    }

    @DexIgnore
    public final byte[] a() {
        byte[] p;
        String a2 = iy1.a(this.c);
        Charset c2 = hd0.y.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            if (this.d.d()) {
                p = new byte[0];
            } else {
                String fileName = this.d.getFileName();
                Charset defaultCharset = Charset.defaultCharset();
                pq7.b(defaultCharset, "Charset.defaultCharset()");
                if (fileName != null) {
                    byte[] bytes2 = fileName.getBytes(defaultCharset);
                    pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                    p = dm7.p(bytes2, (byte) 0);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            int length = bytes.length;
            int length2 = p.length;
            int i = length + 8 + length2;
            byte[] array = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN).putShort((short) i).put((byte) 8).put(this.b).putShort((short) length).putShort((short) length2).put(bytes).put(p).array();
            pq7.b(array, "ByteBuffer.allocate(entr\u2026\n                .array()");
            return array;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] b() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(io1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            io1 io1 = (io1) obj;
            if (this.b != io1.b) {
                return false;
            }
            if (!pq7.a(this.c, io1.c)) {
                return false;
            }
            return !(pq7.a(this.d, io1.d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationReplyMessage");
    }

    @DexIgnore
    public final String getMessageContent() {
        return this.c;
    }

    @DexIgnore
    public final oo1 getMessageIcon() {
        return this.d;
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        short p = hy1.p(this.b);
        return (((p * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.I4, Byte.valueOf(this.b)), jd0.k, this.c), jd0.g5, this.d.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.b);
        parcel.writeString(this.c);
        parcel.writeParcelable(this.d, i);
    }
}
