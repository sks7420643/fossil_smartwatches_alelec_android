package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir5 implements MembersInjector<ComplicationWeatherService> {
    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, CustomizeRealDataRepository customizeRealDataRepository) {
        complicationWeatherService.m = customizeRealDataRepository;
    }

    @DexIgnore
    public static void b(ComplicationWeatherService complicationWeatherService, w27 w27) {
        complicationWeatherService.i = w27;
    }

    @DexIgnore
    public static void c(ComplicationWeatherService complicationWeatherService, PortfolioApp portfolioApp) {
        complicationWeatherService.l = portfolioApp;
    }

    @DexIgnore
    public static void d(ComplicationWeatherService complicationWeatherService, on5 on5) {
        complicationWeatherService.k = on5;
    }

    @DexIgnore
    public static void e(ComplicationWeatherService complicationWeatherService, uq4 uq4) {
        complicationWeatherService.j = uq4;
    }

    @DexIgnore
    public static void f(ComplicationWeatherService complicationWeatherService, UserRepository userRepository) {
        complicationWeatherService.s = userRepository;
    }
}
