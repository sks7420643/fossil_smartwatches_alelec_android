package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yu1 {
    ETA(true),
    TRAVEL_TIME(false),
    DEFAULT(false);
    
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public yu1(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final boolean a() {
        return this.b;
    }
}
