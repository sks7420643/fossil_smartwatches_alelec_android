package com.fossil;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k08 extends mw7 implements o08, Executor {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater g; // = AtomicIntegerFieldUpdater.newUpdater(k08.class, "inFlightTasks");
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<Runnable> c; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ i08 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public volatile int inFlightTasks; // = 0;

    @DexIgnore
    public k08(i08 i08, int i, int i2) {
        this.d = i08;
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void M(tn7 tn7, Runnable runnable) {
        T(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void P(tn7 tn7, Runnable runnable) {
        T(runnable, true);
    }

    @DexIgnore
    @Override // com.fossil.mw7
    public Executor S() {
        return this;
    }

    @DexIgnore
    public final void T(Runnable runnable, boolean z) {
        Runnable poll;
        while (g.incrementAndGet(this) > this.e) {
            this.c.add(runnable);
            if (g.decrementAndGet(this) < this.e && (poll = this.c.poll()) != null) {
                runnable = poll;
            } else {
                return;
            }
        }
        this.d.X(runnable, this, z);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        T(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.o08
    public void h() {
        Runnable poll = this.c.poll();
        if (poll != null) {
            this.d.X(poll, this, true);
            return;
        }
        g.decrementAndGet(this);
        Runnable poll2 = this.c.poll();
        if (poll2 != null) {
            T(poll2, true);
        }
    }

    @DexIgnore
    @Override // com.fossil.o08
    public int o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public String toString() {
        return super.toString() + "[dispatcher = " + this.d + ']';
    }
}
