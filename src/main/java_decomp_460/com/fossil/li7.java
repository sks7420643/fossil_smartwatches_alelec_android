package com.fossil;

import android.content.Context;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class li7 {
    @DexIgnore
    public static volatile li7 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Timer f2202a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public li7(Context context) {
        this.b = context.getApplicationContext();
        this.f2202a = new Timer(false);
    }

    @DexIgnore
    public static li7 b(Context context) {
        if (c == null) {
            synchronized (li7.class) {
                try {
                    if (c == null) {
                        c = new li7(context);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void c() {
        if (fg7.I() == gg7.PERIOD) {
            long F = (long) (fg7.F() * 60 * 1000);
            if (fg7.K()) {
                th7 p = ei7.p();
                p.h("setupPeriodTimer delay:" + F);
            }
            d(new mi7(this), F);
        }
    }

    @DexIgnore
    public void d(TimerTask timerTask, long j) {
        if (this.f2202a != null) {
            if (fg7.K()) {
                th7 p = ei7.p();
                p.h("setupPeriodTimer schedule delay:" + j);
            }
            this.f2202a.schedule(timerTask, j);
        } else if (fg7.K()) {
            ei7.p().l("setupPeriodTimer schedule timer == null");
        }
    }
}
