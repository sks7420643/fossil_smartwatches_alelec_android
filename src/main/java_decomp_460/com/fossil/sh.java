package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sh extends cq {
    @DexIgnore
    public /* final */ ArrayList<ow> E; // = by1.a(this.C, hm7.c(ow.ASYNC));
    @DexIgnore
    public /* final */ ho1 F;

    @DexIgnore
    public sh(k5 k5Var, i60 i60, ho1 ho1) {
        super(k5Var, i60, yp.p0, new us(ho1, k5Var));
        this.F = ho1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.cq
    public JSONObject C() {
        return g80.k(super.C(), jd0.e4, this.F.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.cq
    public ArrayList<ow> z() {
        return this.E;
    }
}
