package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fu1 {
    NOT_ENCRYPTED((byte) 0),
    AES_CTR((byte) 1),
    AES_CBC_PKCS5((byte) 2),
    XOR((byte) 3);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final fu1 a(byte b) {
            fu1[] values = fu1.values();
            for (fu1 fu1 : values) {
                if (fu1.a() == b) {
                    return fu1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public fu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
