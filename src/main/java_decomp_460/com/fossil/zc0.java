package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.v18;
import com.zendesk.sdk.network.Constants;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc0 implements Interceptor {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4446a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public zc0(String str, String str2) {
        this.f4446a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        String str;
        String m = hy1.m(System.currentTimeMillis(), false);
        v18.a h = chain.c().h();
        h.e("Content-Type", Constants.APPLICATION_JSON);
        h.e("X-Cyc-Auth-Method", "signature");
        h.e("X-Cyc-Access-Key-Id", this.f4446a);
        h.e("X-Cyc-Timestamp", m);
        StringBuilder e = e.e("Signature=");
        String str2 = this.b;
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            String str3 = m + str2;
            Charset c = hd0.y.c();
            if (str3 != null) {
                byte[] bytes = str3.getBytes(c);
                pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                instance.update(bytes);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b2 : digest) {
                    hr7 hr7 = hr7.f1520a;
                    String format = String.format("%02X", Arrays.copyOf(new Object[]{Byte.valueOf(b2)}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                }
                str = sb.toString();
                pq7.b(str, "stringBuilder.toString()");
                e.append(str);
                h.e("Authorization", e.toString());
                Response d = chain.d(h.b());
                pq7.b(d, "chain.proceed(request)");
                return d;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        } catch (Exception e2) {
            d90.i.i(e2);
            str = "";
        }
    }
}
