package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et5 implements Factory<dt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<MicroAppRepository> f985a;
    @DexIgnore
    public /* final */ Provider<on5> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> f;
    @DexIgnore
    public /* final */ Provider<ck5> g;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> h;

    @DexIgnore
    public et5(Provider<MicroAppRepository> provider, Provider<on5> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<ck5> provider7, Provider<AlarmsRepository> provider8) {
        this.f985a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static et5 a(Provider<MicroAppRepository> provider, Provider<on5> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<ck5> provider7, Provider<AlarmsRepository> provider8) {
        return new et5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static dt5 c(MicroAppRepository microAppRepository, on5 on5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, ck5 ck5, AlarmsRepository alarmsRepository) {
        return new dt5(microAppRepository, on5, deviceRepository, portfolioApp, hybridPresetRepository, notificationsRepository, ck5, alarmsRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public dt5 get() {
        return c(this.f985a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }
}
