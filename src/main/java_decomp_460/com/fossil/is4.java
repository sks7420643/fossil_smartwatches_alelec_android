package com.fossil;

import android.os.CountDownTimer;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface is4 extends bz4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.is4$a$a")
        /* renamed from: com.fossil.is4$a$a  reason: collision with other inner class name */
        public static final class CountDownTimerC0123a extends CountDownTimer {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ is4 f1660a;
            @DexIgnore
            public /* final */ /* synthetic */ long b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public CountDownTimerC0123a(is4 is4, long j, long j2, long j3, long j4) {
                super(j3, j4);
                this.f1660a = is4;
                this.b = j;
            }

            @DexIgnore
            public void onFinish() {
                this.f1660a.onDone();
            }

            @DexIgnore
            public void onTick(long j) {
                this.f1660a.b(j, this.b);
            }
        }

        @DexIgnore
        public static void a(is4 is4) {
            FLogger.INSTANCE.getLocal().e(is4.getTag(), "onPause");
            CountDownTimer timer = is4.getTimer();
            if (timer != null) {
                timer.cancel();
            }
        }

        @DexIgnore
        public static void b(is4 is4) {
            FLogger.INSTANCE.getLocal().e(is4.getTag(), "onResume");
            is4.d(is4.getTotalMillisecond(), is4.getEndTime() - xy4.f4212a.b());
        }

        @DexIgnore
        public static void c(is4 is4, TimerViewObserver timerViewObserver, int i) {
            if (is4.getTimerViewObserver() == null) {
                is4.setTimerViewObserver(timerViewObserver);
                TimerViewObserver timerViewObserver2 = is4.getTimerViewObserver();
                if (timerViewObserver2 != null) {
                    timerViewObserver2.c(is4, i);
                }
            }
        }

        @DexIgnore
        public static void d(is4 is4, long j, long j2) {
            CountDownTimer timer = is4.getTimer();
            if (timer != null) {
                timer.cancel();
            }
            is4.setTimer(new CountDownTimerC0123a(is4, j, j2, j2, is4.getInterval()));
            CountDownTimer timer2 = is4.getTimer();
            if (timer2 != null) {
                timer2.start();
            }
        }
    }

    @DexIgnore
    void b(long j, long j2);

    @DexIgnore
    void d(long j, long j2);

    @DexIgnore
    long getEndTime();

    @DexIgnore
    long getInterval();

    @DexIgnore
    String getTag();

    @DexIgnore
    CountDownTimer getTimer();

    @DexIgnore
    TimerViewObserver getTimerViewObserver();

    @DexIgnore
    long getTotalMillisecond();

    @DexIgnore
    Object onDone();  // void declaration

    @DexIgnore
    void setTimer(CountDownTimer countDownTimer);

    @DexIgnore
    void setTimerViewObserver(TimerViewObserver timerViewObserver);
}
