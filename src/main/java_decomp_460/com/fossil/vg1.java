package com.fossil;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.nb1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vg1<T> implements qb1<T, Bitmap> {
    @DexIgnore
    public static /* final */ nb1<Long> d; // = nb1.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new a());
    @DexIgnore
    public static /* final */ nb1<Integer> e; // = nb1.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new b());
    @DexIgnore
    public static /* final */ e f; // = new e();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f<T> f3762a;
    @DexIgnore
    public /* final */ rd1 b;
    @DexIgnore
    public /* final */ e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements nb1.b<Long> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ByteBuffer f3763a; // = ByteBuffer.allocate(8);

        @DexIgnore
        /* renamed from: b */
        public void a(byte[] bArr, Long l, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.f3763a) {
                this.f3763a.position(0);
                messageDigest.update(this.f3763a.putLong(l.longValue()).array());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements nb1.b<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ByteBuffer f3764a; // = ByteBuffer.allocate(4);

        @DexIgnore
        /* renamed from: b */
        public void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.f3764a) {
                    this.f3764a.position(0);
                    messageDigest.update(this.f3764a.putInt(num.intValue()).array());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements f<AssetFileDescriptor> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(a aVar) {
            this();
        }

        @DexIgnore
        /* renamed from: b */
        public void a(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements f<ByteBuffer> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends MediaDataSource {
            @DexIgnore
            public /* final */ /* synthetic */ ByteBuffer b;

            @DexIgnore
            public a(d dVar, ByteBuffer byteBuffer) {
                this.b = byteBuffer;
            }

            @DexIgnore
            @Override // java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public long getSize() {
                return (long) this.b.limit();
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public int readAt(long j, byte[] bArr, int i, int i2) {
                if (j >= ((long) this.b.limit())) {
                    return -1;
                }
                this.b.position((int) j);
                int min = Math.min(i2, this.b.remaining());
                this.b.get(bArr, i, min);
                return min;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void a(MediaMetadataRetriever mediaMetadataRetriever, ByteBuffer byteBuffer) {
            mediaMetadataRetriever.setDataSource(new a(this, byteBuffer));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }

    @DexIgnore
    public interface f<T> {
        @DexIgnore
        void a(MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements f<ParcelFileDescriptor> {
        @DexIgnore
        /* renamed from: b */
        public void a(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    @DexIgnore
    public vg1(rd1 rd1, f<T> fVar) {
        this(rd1, fVar, f);
    }

    @DexIgnore
    public vg1(rd1 rd1, f<T> fVar, e eVar) {
        this.b = rd1;
        this.f3762a = fVar;
        this.c = eVar;
    }

    @DexIgnore
    public static qb1<AssetFileDescriptor, Bitmap> c(rd1 rd1) {
        return new vg1(rd1, new c(null));
    }

    @DexIgnore
    public static qb1<ByteBuffer, Bitmap> d(rd1 rd1) {
        return new vg1(rd1, new d());
    }

    @DexIgnore
    public static Bitmap e(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, fg1 fg1) {
        Bitmap g2 = (Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || fg1 == fg1.d) ? null : g(mediaMetadataRetriever, j, i, i2, i3, fg1);
        return g2 == null ? f(mediaMetadataRetriever, j, i) : g2;
    }

    @DexIgnore
    public static Bitmap f(MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }

    @DexIgnore
    @TargetApi(27)
    public static Bitmap g(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, fg1 fg1) {
        int i4;
        int i5;
        try {
            int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                i5 = parseInt;
                i4 = parseInt2;
            } else {
                i5 = parseInt2;
                i4 = parseInt;
            }
            float b2 = fg1.b(i4, i5, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, Math.round(((float) i4) * b2), Math.round(((float) i5) * b2));
        } catch (Throwable th) {
            if (Log.isLoggable("VideoDecoder", 3)) {
                Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            }
            return null;
        }
    }

    @DexIgnore
    public static qb1<ParcelFileDescriptor, Bitmap> h(rd1 rd1) {
        return new vg1(rd1, new g());
    }

    @DexIgnore
    @Override // com.fossil.qb1
    public boolean a(T t, ob1 ob1) {
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: com.fossil.ob1 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.qb1
    public id1<Bitmap> b(T t, int i, int i2, ob1 ob1) throws IOException {
        long longValue = ((Long) ob1.c(d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) ob1.c(e);
            Integer num2 = num == null ? 2 : num;
            fg1 fg1 = (fg1) ob1.c(fg1.f);
            fg1 fg12 = fg1 == null ? fg1.e : fg1;
            MediaMetadataRetriever a2 = this.c.a();
            try {
                this.f3762a.a(a2, t);
                Bitmap e2 = e(a2, longValue, num2.intValue(), i, i2, fg12);
                a2.release();
                return yf1.f(e2, this.b);
            } catch (RuntimeException e3) {
                throw new IOException(e3);
            } catch (Throwable th) {
                a2.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }
}
