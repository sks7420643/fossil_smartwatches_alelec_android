package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d91 implements q91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f753a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public d91() {
        this(2500, 1, 1.0f);
    }

    @DexIgnore
    public d91(int i, int i2, float f) {
        this.f753a = i;
        this.c = i2;
        this.d = f;
    }

    @DexIgnore
    @Override // com.fossil.q91
    public void a(t91 t91) throws t91 {
        this.b++;
        int i = this.f753a;
        this.f753a = i + ((int) (((float) i) * this.d));
        if (!d()) {
            throw t91;
        }
    }

    @DexIgnore
    @Override // com.fossil.q91
    public int b() {
        return this.f753a;
    }

    @DexIgnore
    @Override // com.fossil.q91
    public int c() {
        return this.b;
    }

    @DexIgnore
    public boolean d() {
        return this.b <= this.c;
    }
}
