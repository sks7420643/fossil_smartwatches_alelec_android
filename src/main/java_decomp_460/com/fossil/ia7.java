package com.fossil;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia7 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f1601a; // = true;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public List<a> c; // = hm7.e();
    @DexIgnore
    public /* final */ rp7<Typeface, tl7> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Typeface f1602a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(Typeface typeface, boolean z) {
            pq7.c(typeface, "typeface");
            this.f1602a = typeface;
            this.b = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Typeface typeface, boolean z, int i, kq7 kq7) {
            this(typeface, (i & 2) != 0 ? false : z);
        }

        @DexIgnore
        public final Typeface a() {
            return this.f1602a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1602a, aVar.f1602a) || this.b != aVar.b) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Typeface typeface = this.f1602a;
            int hashCode = typeface != null ? typeface.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "TypeFacePreviewModel(typeface=" + this.f1602a + ", isSelected=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dg5 f1603a;
        @DexIgnore
        public /* final */ /* synthetic */ ia7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.h()) {
                    b bVar = this.b;
                    bVar.b.l(bVar.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ia7 ia7, dg5 dg5) {
            super(dg5.b());
            pq7.c(dg5, "binding");
            this.b = ia7;
            this.f1603a = dg5;
        }

        @DexIgnore
        public final void a(a aVar) {
            pq7.c(aVar, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            TextView textView = this.f1603a.b;
            pq7.b(textView, "binding.previewText");
            textView.setActivated(aVar.b());
            TextView textView2 = this.f1603a.b;
            pq7.b(textView2, "binding.previewText");
            textView2.setTypeface(aVar.a());
            this.f1603a.b().setOnClickListener(new a(this));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rp7<? super android.graphics.Typeface, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public ia7(rp7<? super Typeface, tl7> rp7) {
        this.d = rp7;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final boolean h() {
        return this.f1601a;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        bVar.a(this.c.get(i));
    }

    @DexIgnore
    /* renamed from: j */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        dg5 c2 = dg5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(c2, "ItemTypefacePreviewBindi\u2026tInflater, parent, false)");
        return new b(this, c2);
    }

    @DexIgnore
    public final int k(Typeface typeface) {
        int i;
        pq7.c(typeface, "typeface");
        Iterator<a> it = this.c.iterator();
        int i2 = 0;
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (pq7.a(it.next().a(), typeface)) {
                break;
            } else {
                i2 = i + 1;
            }
        }
        if (i != -1) {
            l(i);
        }
        return i;
    }

    @DexIgnore
    public final void l(int i) {
        int i2 = this.b;
        if (i2 != -1) {
            this.c.get(i2).c(false);
            notifyItemChanged(this.b);
        }
        this.b = i;
        this.c.get(i).c(true);
        notifyItemChanged(i);
        rp7<Typeface, tl7> rp7 = this.d;
        if (rp7 != null) {
            rp7.invoke(this.c.get(i).a());
        }
    }

    @DexIgnore
    public final void m(List<? extends Typeface> list) {
        pq7.c(list, "typefaceList");
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new a(it.next(), false, 2, null));
        }
        this.c = arrayList;
        notifyDataSetChanged();
    }
}
