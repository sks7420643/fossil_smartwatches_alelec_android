package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nx3 extends ze0 {
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends BottomSheetBehavior.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, float f) {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void b(View view, int i) {
            if (i == 5) {
                nx3.this.w6();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void dismiss() {
        if (!y6(false)) {
            super.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void dismissAllowingStateLoss() {
        if (!y6(true)) {
            super.dismissAllowingStateLoss();
        }
    }

    @DexIgnore
    @Override // com.fossil.ze0, com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        return new mx3(getContext(), getTheme());
    }

    @DexIgnore
    public final void w6() {
        if (this.b) {
            super.dismissAllowingStateLoss();
        } else {
            super.dismiss();
        }
    }

    @DexIgnore
    public final void x6(BottomSheetBehavior<?> bottomSheetBehavior, boolean z) {
        this.b = z;
        if (bottomSheetBehavior.q() == 5) {
            w6();
            return;
        }
        if (getDialog() instanceof mx3) {
            ((mx3) getDialog()).h();
        }
        bottomSheetBehavior.g(new b());
        bottomSheetBehavior.E(5);
    }

    @DexIgnore
    public final boolean y6(boolean z) {
        Dialog dialog = getDialog();
        if (dialog instanceof mx3) {
            mx3 mx3 = (mx3) dialog;
            BottomSheetBehavior<FrameLayout> f = mx3.f();
            if (f.s() && mx3.g()) {
                x6(f, z);
                return true;
            }
        }
        return false;
    }
}
