package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final a f2953a = a.f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ boolean f2954a; // = (Build.VERSION.SDK_INT >= 29);
        @DexIgnore
        public static /* final */ String[] b; // = {"_display_name", "_data", FieldType.FOREIGN_ID_FIELD_SUFFIX, "title", "bucket_id", "bucket_display_name", "width", "height", "date_modified", "mime_type", "datetaken"};
        @DexIgnore
        public static /* final */ String[] c; // = {"_display_name", "_data", FieldType.FOREIGN_ID_FIELD_SUFFIX, "title", "bucket_id", "bucket_display_name", "datetaken", "width", "height", "date_modified", "mime_type", "duration"};
        @DexIgnore
        public static /* final */ String[] d; // = {MessengerShareContentUtility.MEDIA_TYPE, "_display_name"};
        @DexIgnore
        public static /* final */ String[] e; // = {"bucket_id", "bucket_display_name"};
        @DexIgnore
        public static /* final */ /* synthetic */ a f; // = new a();

        @DexIgnore
        public final String[] a() {
            return e;
        }

        @DexIgnore
        public final String[] b() {
            return b;
        }

        @DexIgnore
        public final String[] c() {
            return c;
        }

        @DexIgnore
        public final String[] d() {
            return d;
        }

        @DexIgnore
        public final boolean e() {
            return f2954a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static List<String> a(qa8 qa8, Context context, List<String> list) {
            pq7.c(context, "context");
            pq7.c(list, "ids");
            try {
                return context.getContentResolver().delete(qa8.n(), "_id in (?)", new String[]{pm7.N(list, null, null, null, 0, null, null, 63, null)}) > 0 ? list : hm7.e();
            } catch (Exception e) {
                return hm7.e();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
            com.fossil.so7.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0041, code lost:
            throw r2;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean b(com.fossil.qa8 r8, android.content.Context r9, java.lang.String r10) {
            /*
                r5 = 0
                r6 = 1
                r7 = 0
                java.lang.String r0 = "context"
                com.fossil.pq7.c(r9, r0)
                java.lang.String r0 = "id"
                com.fossil.pq7.c(r10, r0)
                android.content.ContentResolver r0 = r9.getContentResolver()
                com.fossil.pa8 r1 = com.fossil.pa8.f
                android.net.Uri r1 = r1.n()
                java.lang.String[] r2 = new java.lang.String[r6]
                java.lang.String r3 = "_id"
                r2[r7] = r3
                java.lang.String r3 = "MediaStore.Files.FileColumns._ID = ?"
                java.lang.String[] r4 = new java.lang.String[r6]
                r4[r7] = r10
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)
                if (r1 != 0) goto L_0x002d
                com.fossil.so7.a(r1, r5)
            L_0x002c:
                return r7
            L_0x002d:
                int r0 = r1.getCount()     // Catch:{ all -> 0x003b }
                if (r0 < r6) goto L_0x0039
                r0 = r6
            L_0x0034:
                com.fossil.so7.a(r1, r5)
                r7 = r0
                goto L_0x002c
            L_0x0039:
                r0 = r7
                goto L_0x0034
            L_0x003b:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x003d }
            L_0x003d:
                r2 = move-exception
                com.fossil.so7.a(r1, r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qa8.b.b(com.fossil.qa8, android.content.Context, java.lang.String):boolean");
        }

        @DexIgnore
        public static Uri c(qa8 qa8) {
            Uri contentUri = MediaStore.Files.getContentUri("external");
            pq7.b(contentUri, "MediaStore.Files.getContentUri(VOLUME_EXTERNAL)");
            return contentUri;
        }

        @DexIgnore
        public static /* synthetic */ List d(qa8 qa8, Context context, String str, int i, int i2, int i3, long j, la8 la8, ja8 ja8, int i4, Object obj) {
            if (obj == null) {
                return qa8.p(context, str, i, i2, (i4 & 16) != 0 ? 0 : i3, j, la8, (i4 & 128) != 0 ? null : ja8);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getAssetFromGalleryId");
        }

        @DexIgnore
        public static String e(qa8 qa8, int i, la8 la8, ArrayList<String> arrayList) {
            pq7.c(la8, "filterOptions");
            pq7.c(arrayList, "args");
            String g = la8.g();
            String[] f = la8.f();
            String b = la8.b();
            String[] a2 = la8.a();
            if (i == 1) {
                arrayList.add(String.valueOf(1));
                String str = "AND media_type = ?AND " + g;
                mm7.t(arrayList, f);
                return str;
            } else if (i != 2) {
                String str2 = "AND (" + MessengerShareContentUtility.MEDIA_TYPE + " = ? OR (" + MessengerShareContentUtility.MEDIA_TYPE + " = ? AND " + b + ")) AND " + g;
                arrayList.add(String.valueOf(1));
                arrayList.add(String.valueOf(3));
                mm7.t(arrayList, a2);
                mm7.t(arrayList, f);
                return str2;
            } else {
                arrayList.add(String.valueOf(3));
                mm7.t(arrayList, f);
                String str3 = ("AND media_type = ?AND " + g) + "AND " + b;
                mm7.t(arrayList, a2);
                return str3;
            }
        }

        @DexIgnore
        public static double f(qa8 qa8, Cursor cursor, String str) {
            pq7.c(cursor, "$this$getDouble");
            pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getDouble(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static int g(qa8 qa8, Cursor cursor, String str) {
            pq7.c(cursor, "$this$getInt");
            pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getInt(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static long h(qa8 qa8, Cursor cursor, String str) {
            pq7.c(cursor, "$this$getLong");
            pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getLong(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static int i(qa8 qa8, int i) {
            if (i != 1) {
                return i != 3 ? 0 : 2;
            }
            return 1;
        }

        @DexIgnore
        public static String j(qa8 qa8, Cursor cursor, String str) {
            pq7.c(cursor, "$this$getString");
            pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            String string = cursor.getString(cursor.getColumnIndex(str));
            pq7.b(string, "getString(getColumnIndex(columnName))");
            return string;
        }

        @DexIgnore
        public static String k(qa8 qa8, Integer num) {
            if (num == null || num.intValue() == 2) {
                return "";
            }
            if (num.intValue() == 1) {
                return "AND width > 0 AND height > 0";
            }
            return "AND (media_type = 3 or (media_type = 1 AND width > 0 AND height > 0))";
        }
    }

    @DexIgnore
    void a(Context context, ka8 ka8, byte[] bArr);

    @DexIgnore
    boolean b(Context context, String str);

    @DexIgnore
    List<ma8> c(Context context, int i, long j, la8 la8);

    @DexIgnore
    List<String> d(Context context, List<String> list);

    @DexIgnore
    List<ka8> e(Context context, String str, int i, int i2, int i3, long j, la8 la8);

    @DexIgnore
    byte[] f(Context context, ka8 ka8, boolean z);

    @DexIgnore
    Bitmap g(Context context, String str, int i, int i2, Integer num);

    @DexIgnore
    String h(Context context, String str, boolean z);

    @DexIgnore
    ka8 i(Context context, byte[] bArr, String str, String str2);

    @DexIgnore
    ka8 j(Context context, String str);

    @DexIgnore
    ka8 k(Context context, InputStream inputStream, String str, String str2);

    @DexIgnore
    Object l();  // void declaration

    @DexIgnore
    eq0 m(Context context, String str);

    @DexIgnore
    Uri n();

    @DexIgnore
    ma8 o(Context context, String str, int i, long j, la8 la8);

    @DexIgnore
    List<ka8> p(Context context, String str, int i, int i2, int i3, long j, la8 la8, ja8 ja8);
}
