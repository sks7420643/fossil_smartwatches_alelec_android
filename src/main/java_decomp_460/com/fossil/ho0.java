package com.fossil;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ho0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1500a;
    @DexIgnore
    public int b;

    @DexIgnore
    public ho0(ViewGroup viewGroup) {
    }

    @DexIgnore
    public int a() {
        return this.f1500a | this.b;
    }

    @DexIgnore
    public void b(View view, View view2, int i) {
        c(view, view2, i, 0);
    }

    @DexIgnore
    public void c(View view, View view2, int i, int i2) {
        if (i2 == 1) {
            this.b = i;
        } else {
            this.f1500a = i;
        }
    }

    @DexIgnore
    public void d(View view) {
        e(view, 0);
    }

    @DexIgnore
    public void e(View view, int i) {
        if (i == 1) {
            this.b = 0;
        } else {
            this.f1500a = 0;
        }
    }
}
