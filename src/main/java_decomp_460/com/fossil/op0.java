package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.fossil.pp0;
import com.j256.ormlite.field.FieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class op0 extends BaseAdapter implements Filterable, pp0.a {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public Cursor d;
    @DexIgnore
    public Context e;
    @DexIgnore
    public int f;
    @DexIgnore
    public a g;
    @DexIgnore
    public DataSetObserver h;
    @DexIgnore
    public pp0 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            op0.this.i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends DataSetObserver {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onChanged() {
            op0 op0 = op0.this;
            op0.b = true;
            op0.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            op0 op0 = op0.this;
            op0.b = false;
            op0.notifyDataSetInvalidated();
        }
    }

    @DexIgnore
    public op0(Context context, Cursor cursor, boolean z) {
        f(context, cursor, z ? 1 : 2);
    }

    @DexIgnore
    @Override // com.fossil.pp0.a
    public void a(Cursor cursor) {
        Cursor j = j(cursor);
        if (j != null) {
            j.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.pp0.a
    public Cursor b() {
        return this.d;
    }

    @DexIgnore
    public abstract void c(View view, Context context, Cursor cursor);

    @DexIgnore
    @Override // com.fossil.pp0.a
    public abstract CharSequence d(Cursor cursor);

    @DexIgnore
    public void f(Context context, Cursor cursor, int i2) {
        boolean z = true;
        if ((i2 & 1) == 1) {
            i2 |= 2;
            this.c = true;
        } else {
            this.c = false;
        }
        if (cursor == null) {
            z = false;
        }
        this.d = cursor;
        this.b = z;
        this.e = context;
        this.f = z ? cursor.getColumnIndexOrThrow(FieldType.FOREIGN_ID_FIELD_SUFFIX) : -1;
        if ((i2 & 2) == 2) {
            this.g = new a();
            this.h = new b();
        } else {
            this.g = null;
            this.h = null;
        }
        if (z) {
            a aVar = this.g;
            if (aVar != null) {
                cursor.registerContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    @DexIgnore
    public abstract View g(Context context, Cursor cursor, ViewGroup viewGroup);

    @DexIgnore
    public int getCount() {
        Cursor cursor;
        if (!this.b || (cursor = this.d) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    @DexIgnore
    public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
        if (!this.b) {
            return null;
        }
        this.d.moveToPosition(i2);
        if (view == null) {
            view = g(this.e, this.d, viewGroup);
        }
        c(view, this.e, this.d);
        return view;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.i == null) {
            this.i = new pp0(this);
        }
        return this.i;
    }

    @DexIgnore
    public Object getItem(int i2) {
        Cursor cursor;
        if (!this.b || (cursor = this.d) == null) {
            return null;
        }
        cursor.moveToPosition(i2);
        return this.d;
    }

    @DexIgnore
    public long getItemId(int i2) {
        Cursor cursor;
        if (!this.b || (cursor = this.d) == null || !cursor.moveToPosition(i2)) {
            return 0;
        }
        return this.d.getLong(this.f);
    }

    @DexIgnore
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (!this.b) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.d.moveToPosition(i2)) {
            if (view == null) {
                view = h(this.e, this.d, viewGroup);
            }
            c(view, this.e, this.d);
            return view;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i2);
        }
    }

    @DexIgnore
    public abstract View h(Context context, Cursor cursor, ViewGroup viewGroup);

    @DexIgnore
    public void i() {
        Cursor cursor;
        if (this.c && (cursor = this.d) != null && !cursor.isClosed()) {
            this.b = this.d.requery();
        }
    }

    @DexIgnore
    public Cursor j(Cursor cursor) {
        Cursor cursor2 = this.d;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            a aVar = this.g;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.d = cursor;
        if (cursor != null) {
            a aVar2 = this.g;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.h;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.f = cursor.getColumnIndexOrThrow(FieldType.FOREIGN_ID_FIELD_SUFFIX);
            this.b = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.f = -1;
        this.b = false;
        notifyDataSetInvalidated();
        return cursor2;
    }
}
