package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n18 extends RequestBody {
    @DexIgnore
    public static /* final */ r18 c; // = r18.c("application/x-www-form-urlencoded");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<String> f2450a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<String> f2451a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public a() {
            this(null);
        }

        @DexIgnore
        public a(Charset charset) {
            this.f2451a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.f2451a.add(q18.c(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(q18.c(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.f2451a.add(q18.c(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(q18.c(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public n18 c() {
            return new n18(this.f2451a, this.b);
        }
    }

    @DexIgnore
    public n18(List<String> list, List<String> list2) {
        this.f2450a = b28.t(list);
        this.b = b28.t(list2);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() {
        return i(null, true);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public r18 b() {
        return c;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void h(j48 j48) throws IOException {
        i(j48, false);
    }

    @DexIgnore
    public final long i(j48 j48, boolean z) {
        i48 i48 = z ? new i48() : j48.d();
        int size = this.f2450a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                i48.w0(38);
            }
            i48.D0(this.f2450a.get(i));
            i48.w0(61);
            i48.D0(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long p0 = i48.p0();
        i48.j();
        return p0;
    }
}
