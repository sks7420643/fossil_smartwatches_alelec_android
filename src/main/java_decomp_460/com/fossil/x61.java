package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f4043a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final x61 a() {
            if (Build.VERSION.SDK_INT < 26 || w61.b.a()) {
                return new y61(false);
            }
            int i = Build.VERSION.SDK_INT;
            return (i == 26 || i == 27) ? b71.e : new y61(true);
        }
    }

    @DexIgnore
    public x61() {
    }

    @DexIgnore
    public /* synthetic */ x61(kq7 kq7) {
        this();
    }

    @DexIgnore
    public abstract boolean a(f81 f81);
}
