package com.fossil;

import java.util.List;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uy4 {
    @DexIgnore
    public static /* final */ uy4 c; // = d.a(hm7.h(-1739917, -1023342, -4560696, -6982195, -8812853, -10177034, -11549705, -11677471, -11684180, -8271996, -5319295, -30107, -2825897, -10929, -18611, -6190977, -7297874));
    @DexIgnore
    public static /* final */ a d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Random f3673a;
    @DexIgnore
    public /* final */ List<Integer> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final uy4 a(List<Integer> list) {
            pq7.c(list, "colorList");
            return new uy4(list, null);
        }

        @DexIgnore
        public final uy4 b() {
            return uy4.c;
        }
    }

    /*
    static {
        a aVar = new a(null);
        d = aVar;
        aVar.a(hm7.h(-957596, -686759, -416706, -1784274, -9977996, -10902850, -14642227, -5414233, -8366207));
    }
    */

    @DexIgnore
    public uy4(List<Integer> list) {
        this.b = list;
        this.f3673a = new Random(System.currentTimeMillis());
    }

    @DexIgnore
    public /* synthetic */ uy4(List list, kq7 kq7) {
        this(list);
    }

    @DexIgnore
    public final int b(Object obj) {
        pq7.c(obj, "key");
        return this.b.get(Math.abs(obj.hashCode()) % this.b.size()).intValue();
    }

    @DexIgnore
    public final int c() {
        List<Integer> list = this.b;
        return list.get(this.f3673a.nextInt(list.size())).intValue();
    }
}
