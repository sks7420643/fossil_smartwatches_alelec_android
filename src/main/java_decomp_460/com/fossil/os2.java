package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface os2 extends IInterface {
    @DexIgnore
    boolean L2(os2 os2) throws RemoteException;

    @DexIgnore
    int a() throws RemoteException;

    @DexIgnore
    void b(boolean z) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    void remove() throws RemoteException;

    @DexIgnore
    void setFillColor(int i) throws RemoteException;

    @DexIgnore
    void setGeodesic(boolean z) throws RemoteException;

    @DexIgnore
    void setPoints(List<LatLng> list) throws RemoteException;

    @DexIgnore
    void setStrokeColor(int i) throws RemoteException;

    @DexIgnore
    void setStrokeWidth(float f) throws RemoteException;

    @DexIgnore
    void setVisible(boolean z) throws RemoteException;

    @DexIgnore
    void setZIndex(float f) throws RemoteException;
}
