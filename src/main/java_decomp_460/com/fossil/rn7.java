package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rn7 extends tn7.b {
    @DexIgnore
    public static final b p = b.f3134a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends tn7.b> E a(rn7 rn7, tn7.c<E> cVar) {
            pq7.c(cVar, "key");
            if (cVar instanceof on7) {
                on7 on7 = (on7) cVar;
                if (!on7.a(rn7.getKey())) {
                    return null;
                }
                E e = (E) on7.b(rn7);
                if (!(e instanceof tn7.b)) {
                    return null;
                }
                return e;
            }
            if (rn7.p != cVar) {
                rn7 = null;
            } else if (rn7 == null) {
                throw new il7("null cannot be cast to non-null type E");
            }
            return rn7;
        }

        @DexIgnore
        public static tn7 b(rn7 rn7, tn7.c<?> cVar) {
            pq7.c(cVar, "key");
            if (!(cVar instanceof on7)) {
                return rn7.p == cVar ? un7.INSTANCE : rn7;
            }
            on7 on7 = (on7) cVar;
            return (!on7.a(rn7.getKey()) || on7.b(rn7) == null) ? rn7 : un7.INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tn7.c<rn7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ b f3134a; // = new b();
    }

    @DexIgnore
    void a(qn7<?> qn7);

    @DexIgnore
    <T> qn7<T> c(qn7<? super T> qn7);
}
