package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uz0 extends zz0 implements Animatable {
    @DexIgnore
    public b c;
    @DexIgnore
    public Context d;
    @DexIgnore
    public ArgbEvaluator e;
    @DexIgnore
    public /* final */ Drawable.Callback f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Drawable.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            uz0.this.invalidateSelf();
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            uz0.this.scheduleSelf(runnable, j);
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            uz0.this.unscheduleSelf(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3675a;
        @DexIgnore
        public a01 b;
        @DexIgnore
        public AnimatorSet c;
        @DexIgnore
        public ArrayList<Animator> d;
        @DexIgnore
        public zi0<Animator, String> e;

        @DexIgnore
        public b(Context context, b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.f3675a = bVar.f3675a;
                a01 a01 = bVar.b;
                if (a01 != null) {
                    Drawable.ConstantState constantState = a01.getConstantState();
                    if (resources != null) {
                        this.b = (a01) constantState.newDrawable(resources);
                    } else {
                        this.b = (a01) constantState.newDrawable();
                    }
                    a01 a012 = this.b;
                    a012.mutate();
                    a01 a013 = a012;
                    this.b = a013;
                    a013.setCallback(callback);
                    this.b.setBounds(bVar.b.getBounds());
                    this.b.h(false);
                }
                ArrayList<Animator> arrayList = bVar.d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.d = new ArrayList<>(size);
                    this.e = new zi0<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = bVar.d.get(i);
                        Animator clone = animator.clone();
                        String str = bVar.e.get(animator);
                        clone.setTarget(this.b.d(str));
                        this.d.add(clone);
                        this.e.put(clone, str);
                    }
                    a();
                }
            }
        }

        @DexIgnore
        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether(this.d);
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f3675a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Drawable.ConstantState f3676a;

        @DexIgnore
        public c(Drawable.ConstantState constantState) {
            this.f3676a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.f3676a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f3676a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            uz0 uz0 = new uz0();
            Drawable newDrawable = this.f3676a.newDrawable();
            uz0.b = newDrawable;
            newDrawable.setCallback(uz0.f);
            return uz0;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            uz0 uz0 = new uz0();
            Drawable newDrawable = this.f3676a.newDrawable(resources);
            uz0.b = newDrawable;
            newDrawable.setCallback(uz0.f);
            return uz0;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            uz0 uz0 = new uz0();
            Drawable newDrawable = this.f3676a.newDrawable(resources, theme);
            uz0.b = newDrawable;
            newDrawable.setCallback(uz0.f);
            return uz0;
        }
    }

    @DexIgnore
    public uz0() {
        this(null, null, null);
    }

    @DexIgnore
    public uz0(Context context) {
        this(context, null, null);
    }

    @DexIgnore
    public uz0(Context context, b bVar, Resources resources) {
        this.e = null;
        this.f = new a();
        this.d = context;
        if (bVar != null) {
            this.c = bVar;
        } else {
            this.c = new b(context, bVar, this.f, resources);
        }
    }

    @DexIgnore
    public static uz0 a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        uz0 uz0 = new uz0(context);
        uz0.inflate(resources, xmlPullParser, attributeSet, theme);
        return uz0;
    }

    @DexIgnore
    @Override // com.fossil.zz0
    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.a(drawable, theme);
        }
    }

    @DexIgnore
    public final void b(String str, Animator animator) {
        animator.setTarget(this.c.b.d(str));
        if (Build.VERSION.SDK_INT < 21) {
            c(animator);
        }
        b bVar = this.c;
        if (bVar.d == null) {
            bVar.d = new ArrayList<>();
            this.c.e = new zi0<>();
        }
        this.c.d.add(animator);
        this.c.e.put(animator, str);
    }

    @DexIgnore
    public final void c(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                c(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.e == null) {
                    this.e = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.e);
            }
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.b;
        if (drawable != null) {
            return am0.b(drawable);
        }
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.c.b.draw(canvas);
        if (this.c.c.isStarted()) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.b;
        return drawable != null ? am0.d(drawable) : this.c.b.getAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getChangingConfigurations() : super.getChangingConfigurations() | this.c.f3675a;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        Drawable drawable = this.b;
        return drawable != null ? am0.e(drawable) : this.c.b.getColorFilter();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        if (this.b == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new c(this.b.getConstantState());
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getIntrinsicHeight() : this.c.b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getIntrinsicWidth() : this.c.b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getOpacity() : this.c.b.getOpacity();
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.g(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth();
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth + 1 || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray k = ol0.k(resources, theme, attributeSet, sz0.e);
                    int resourceId = k.getResourceId(0, 0);
                    if (resourceId != 0) {
                        a01 b2 = a01.b(resources, resourceId, theme);
                        b2.h(false);
                        b2.setCallback(this.f);
                        a01 a01 = this.c.b;
                        if (a01 != null) {
                            a01.setCallback(null);
                        }
                        this.c.b = b2;
                    }
                    k.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, sz0.f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.d;
                        if (context != null) {
                            b(string, wz0.i(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.c.a();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.b;
        return drawable != null ? am0.h(drawable) : this.c.b.isAutoMirrored();
    }

    @DexIgnore
    public boolean isRunning() {
        Drawable drawable = this.b;
        return drawable != null ? ((AnimatedVectorDrawable) drawable).isRunning() : this.c.c.isRunning();
    }

    @DexIgnore
    public boolean isStateful() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.isStateful() : this.c.b.isStateful();
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.c.b.setBounds(rect);
        }
    }

    @DexIgnore
    @Override // com.fossil.zz0
    public boolean onLevelChange(int i) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.setLevel(i) : this.c.b.setLevel(i);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.setState(iArr) : this.c.b.setState(iArr);
    }

    @DexIgnore
    public void setAlpha(int i) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else {
            this.c.b.setAlpha(i);
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.j(drawable, z);
        } else {
            this.c.b.setAutoMirrored(z);
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.c.b.setColorFilter(colorFilter);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTint(int i) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.n(drawable, i);
        } else {
            this.c.b.setTint(i);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.o(drawable, colorStateList);
        } else {
            this.c.b.setTintList(colorStateList);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.p(drawable, mode);
        } else {
            this.c.b.setTintMode(mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.b;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.c.b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        Drawable drawable = this.b;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.c.c.isStarted()) {
            this.c.c.start();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void stop() {
        Drawable drawable = this.b;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.c.c.end();
        }
    }
}
