package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp5 implements MembersInjector<up5> {
    @DexIgnore
    public static void a(up5 up5, mj5 mj5) {
        up5.c = mj5;
    }

    @DexIgnore
    public static void b(up5 up5, d26 d26) {
        up5.e = d26;
    }

    @DexIgnore
    public static void c(up5 up5, v36 v36) {
        up5.d = v36;
    }

    @DexIgnore
    public static void d(up5 up5, NotificationSettingsDatabase notificationSettingsDatabase) {
        up5.f = notificationSettingsDatabase;
    }

    @DexIgnore
    public static void e(up5 up5, NotificationsRepository notificationsRepository) {
        up5.f3632a = notificationsRepository;
    }

    @DexIgnore
    public static void f(up5 up5, on5 on5) {
        up5.b = on5;
    }
}
