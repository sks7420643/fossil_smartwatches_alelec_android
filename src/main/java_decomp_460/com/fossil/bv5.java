package com.fossil;

import com.fossil.tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bv5 extends tq4<a.b, a.c, a.C0023a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bv5$a$a")
        /* renamed from: com.fossil.bv5$a$a  reason: collision with other inner class name */
        public static final class C0023a implements tq4.a {
            @DexIgnore
            public C0023a(int i) {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements tq4.b {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements tq4.c {
            @DexIgnore
            public c(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return bv5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServerSettingDataSource.OnGetServerSettingList {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bv5 f514a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(bv5 bv5) {
            this.f514a = bv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.f514a.b().a(new a.C0023a(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(bv5.f.a(), "executeUseCase - onSuccess");
            this.f514a.b().onSuccess(new a.c(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = bv5.class.getSimpleName();
        pq7.b(simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public bv5(ServerSettingRepository serverSettingRepository) {
        pq7.c(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    /* renamed from: h */
    public void a(a.b bVar) {
        this.d.getServerSettingList(new b(this));
    }
}
