package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w04<T> extends g14<T> {
    @DexIgnore
    public static /* final */ w04<Object> INSTANCE; // = new w04<>();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public static <T> g14<T> withType() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public Set<T> asSet() {
        return Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.g14
    public boolean equals(Object obj) {
        return obj == this;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    @DexIgnore
    @Override // com.fossil.g14
    public int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public boolean isPresent() {
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.g14<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.g14
    public g14<T> or(g14<? extends T> g14) {
        i14.l(g14);
        return g14;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T or(m14<? extends T> m14) {
        T t = (T) m14.get();
        i14.m(t, "use Optional.orNull() instead of a Supplier that returns null");
        return t;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T or(T t) {
        i14.m(t, "use Optional.orNull() instead of Optional.or(null)");
        return t;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T orNull() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    @Override // com.fossil.g14
    public <V> g14<V> transform(b14<? super T, V> b14) {
        i14.l(b14);
        return g14.absent();
    }
}
