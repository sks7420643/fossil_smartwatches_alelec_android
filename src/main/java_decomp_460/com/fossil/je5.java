package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.watchface.WatchFaceCoverView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class je5 extends ie5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d N; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public long M;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131363348, 1);
        O.put(2131361980, 2);
        O.put(2131363467, 3);
        O.put(2131363376, 4);
        O.put(2131362767, 5);
        O.put(2131363471, 6);
        O.put(2131362054, 7);
        O.put(2131362770, 8);
        O.put(2131363538, 9);
        O.put(2131362769, 10);
        O.put(2131362028, 11);
        O.put(2131362254, 12);
        O.put(2131362790, 13);
        O.put(2131362791, 14);
        O.put(2131362789, 15);
        O.put(2131362432, 16);
        O.put(2131362283, 17);
        O.put(2131362094, 18);
        O.put(2131363531, 19);
        O.put(2131363530, 20);
        O.put(2131363529, 21);
    }
    */

    @DexIgnore
    public je5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 22, N, O));
    }

    @DexIgnore
    public je5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[2], (View) objArr[11], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[18], (FloatingActionButton) objArr[12], (FlexibleButton) objArr[17], (FlexibleTextView) objArr[16], (RTLImageView) objArr[5], (WatchFaceCoverView) objArr[10], (ImageView) objArr[8], (View) objArr[15], (View) objArr[13], (View) objArr[14], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[4], (View) objArr[3], (View) objArr[6], (CustomizeWidget) objArr[21], (CustomizeWidget) objArr[20], (CustomizeWidget) objArr[19], (WatchFacePreviewView) objArr[9]);
        this.M = -1;
        this.D.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.M = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.M != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.M = 1;
        }
        w();
    }
}
