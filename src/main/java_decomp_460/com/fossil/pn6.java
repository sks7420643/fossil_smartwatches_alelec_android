package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.WorkoutSessionRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn6 extends hq4 {
    @DexIgnore
    public String h;
    @DexIgnore
    public MutableLiveData<a> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutSessionRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2850a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public a() {
            this(0, null, false, 7, null);
        }

        @DexIgnore
        public a(int i, String str, boolean z) {
            pq7.c(str, "networkErrorMessage");
            this.f2850a = i;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(int i, String str, boolean z, int i2, kq7 kq7) {
            this((i2 & 1) != 0 ? -1 : i, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? false : z);
        }

        @DexIgnore
        public final boolean a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.f2850a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pn6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<Object>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<Object>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WorkoutSessionRepository workoutSessionRepository = this.this$0.this$0.j;
                    String str = this.this$0.this$0.h;
                    if (str != null) {
                        this.L$0 = iv7;
                        this.label = 1;
                        Object deleteWorkoutSession = workoutSessionRepository.deleteWorkoutSession(str, this);
                        return deleteWorkoutSession == d ? d : deleteWorkoutSession;
                    }
                    pq7.i();
                    throw null;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qn7 qn7, pn6 pn6) {
            super(2, qn7);
            this.this$0 = pn6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(qn7, this.this$0);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hq4.d(this.this$0, true, false, null, 6, null);
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            iq5 iq5 = (iq5) g;
            if (iq5 instanceof kq5) {
                hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.i.l(new a(0, null, true, 3, null));
            } else if (iq5 instanceof hq5) {
                hq4.d(this.this$0, false, true, null, 5, null);
                hq5 hq5 = (hq5) iq5;
                hq5.c();
                MutableLiveData mutableLiveData = this.this$0.i;
                ServerError c = hq5.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                mutableLiveData.l(new a(hq5.a(), str, false));
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public pn6(WorkoutSessionRepository workoutSessionRepository) {
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.j = workoutSessionRepository;
    }

    @DexIgnore
    public final void q() {
        if (this.h != null) {
            xw7 unused = gu7.d(us0.a(this), null, null, new b(null, this), 3, null);
        }
    }

    @DexIgnore
    public final MutableLiveData<a> r() {
        return this.i;
    }

    @DexIgnore
    public final void s(String str) {
        pq7.c(str, "id");
        this.h = str;
    }
}
