package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.MapView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mf3 extends qc3 {
    @DexIgnore
    public /* final */ /* synthetic */ sb3 b;

    @DexIgnore
    public mf3(MapView.a aVar, sb3 sb3) {
        this.b = sb3;
    }

    @DexIgnore
    @Override // com.fossil.pc3
    public final void l2(zb3 zb3) throws RemoteException {
        this.b.onMapReady(new qb3(zb3));
    }
}
