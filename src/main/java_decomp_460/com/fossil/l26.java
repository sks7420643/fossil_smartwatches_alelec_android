package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.h26;
import com.fossil.hq4;
import com.fossil.jn5;
import com.fossil.n26;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l26 extends qv5 implements h26.a {
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public g37<ra5> i;
    @DexIgnore
    public n26 j;
    @DexIgnore
    public h26 k;
    @DexIgnore
    public ArrayList<QuickResponseMessage> l; // = new ArrayList<>();
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final l26 a() {
            return new l26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<n26.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l26 f2137a;

        @DexIgnore
        public b(l26 l26) {
            this.f2137a = l26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(n26.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("QuickResponseFragment", "data changed " + aVar);
                this.f2137a.N6(aVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l26 f2138a;

        @DexIgnore
        public c(l26 l26) {
            this.f2138a = l26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar != null) {
                if (bVar.a()) {
                    this.f2138a.b();
                }
                if (bVar.b()) {
                    this.f2138a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<hq4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l26 f2139a;

        @DexIgnore
        public d(l26 l26) {
            this.f2139a = l26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.c cVar) {
            if (cVar != null && (!cVar.a().isEmpty())) {
                jn5.c(jn5.b, this.f2139a.getContext(), jn5.a.SET_BLE_COMMAND, false, false, true, 113, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ra5 b;
        @DexIgnore
        public /* final */ /* synthetic */ l26 c;

        @DexIgnore
        public e(ra5 ra5, l26 l26) {
            this.b = ra5;
            this.c = l26;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleEditText flexibleEditText = this.b.s;
            pq7.b(flexibleEditText, "etMessage");
            this.c.P6().x(String.valueOf(flexibleEditText.getText()), 50);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l26 b;

        @DexIgnore
        public f(l26 l26) {
            this.b = l26;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().s(this.b.l);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ra5 b;
        @DexIgnore
        public /* final */ /* synthetic */ l26 c;

        @DexIgnore
        public g(ra5 ra5, l26 l26) {
            this.b = ra5;
            this.c = l26;
        }

        @DexIgnore
        public final void onClick(View view) {
            n26 P6 = this.c.P6();
            FlexibleEditText flexibleEditText = this.b.s;
            pq7.b(flexibleEditText, "etMessage");
            P6.r(String.valueOf(flexibleEditText.getText()), this.c.l.size(), 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<List<? extends QuickResponseMessage>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l26 f2140a;

        @DexIgnore
        public h(l26 l26) {
            this.f2140a = l26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<QuickResponseMessage> list) {
            ra5 a2;
            FlexibleEditText flexibleEditText;
            if (list.isEmpty()) {
                this.f2140a.P6().z();
                return;
            }
            if (!(list.size() <= this.f2140a.l.size() || (a2 = this.f2140a.O6().a()) == null || (flexibleEditText = a2.s) == null)) {
                flexibleEditText.setText("");
            }
            this.f2140a.l.clear();
            this.f2140a.l.addAll(list);
            h26 L6 = l26.L6(this.f2140a);
            pq7.b(list, "qrs");
            L6.k(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ h26 L6(l26 l26) {
        h26 h26 = l26.k;
        if (h26 != null) {
            return h26;
        }
        pq7.n("mResponseAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        n26 n26 = this.j;
        if (n26 != null) {
            n26.s(this.l);
            return false;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void N6(n26.a aVar) {
        FragmentActivity activity;
        Boolean a2 = aVar.a();
        if (a2 != null && a2.booleanValue()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
        }
        Boolean f2 = aVar.f();
        if (f2 != null && f2.booleanValue()) {
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.t0(childFragmentManager2);
        }
        Boolean c2 = aVar.c();
        if (!(c2 == null || !c2.booleanValue() || (activity = getActivity()) == null)) {
            activity.finish();
        }
        Boolean b2 = aVar.b();
        if (b2 != null && b2.booleanValue()) {
            s37 s373 = s37.c;
            FragmentManager childFragmentManager3 = getChildFragmentManager();
            pq7.b(childFragmentManager3, "childFragmentManager");
            s373.Y(childFragmentManager3);
        }
        Boolean d2 = aVar.d();
        if (d2 != null) {
            if (d2.booleanValue()) {
                g37<ra5> g37 = this.i;
                if (g37 != null) {
                    ra5 a3 = g37.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.m)) {
                            FlexibleEditText flexibleEditText = a3.s;
                            pq7.b(flexibleEditText, "etMessage");
                            flexibleEditText.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.m)));
                        } else {
                            FlexibleEditText flexibleEditText2 = a3.s;
                            pq7.b(flexibleEditText2, "etMessage");
                            flexibleEditText2.setBackgroundTintList(ColorStateList.valueOf(-65536));
                        }
                        FlexibleTextView flexibleTextView = a3.v;
                        pq7.b(flexibleTextView, "ftvLimitedWarning");
                        flexibleTextView.setVisibility(0);
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                g37<ra5> g372 = this.i;
                if (g372 != null) {
                    ra5 a4 = g372.a();
                    if (a4 != null) {
                        FlexibleTextView flexibleTextView2 = a4.v;
                        pq7.b(flexibleTextView2, "ftvLimitedWarning");
                        if (flexibleTextView2.getVisibility() == 0) {
                            FlexibleTextView flexibleTextView3 = a4.v;
                            pq7.b(flexibleTextView3, "ftvLimitedWarning");
                            flexibleTextView3.setVisibility(8);
                            if (!TextUtils.isEmpty(this.s)) {
                                FlexibleEditText flexibleEditText3 = a4.s;
                                pq7.b(flexibleEditText3, "etMessage");
                                flexibleEditText3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                            } else {
                                FlexibleEditText flexibleEditText4 = a4.s;
                                pq7.b(flexibleEditText4, "etMessage");
                                flexibleEditText4.setBackgroundTintList(ColorStateList.valueOf(-1));
                            }
                        }
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            }
        }
        Boolean e2 = aVar.e();
        if (e2 != null && e2.booleanValue()) {
            hr7 hr7 = hr7.f1520a;
            String c3 = um5.c(getContext(), 2131887551);
            pq7.b(c3, "LanguageHelper.getString\u2026response_limited_warning)");
            String format = String.format(c3, Arrays.copyOf(new Object[]{String.valueOf(10)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            Toast.makeText(getContext(), format, 1).show();
        }
        Integer g2 = aVar.g();
        if (g2 != null) {
            int intValue = g2.intValue();
            g37<ra5> g373 = this.i;
            if (g373 != null) {
                ra5 a5 = g373.a();
                if (a5 != null) {
                    FlexibleTextView flexibleTextView4 = a5.u;
                    pq7.b(flexibleTextView4, "ftvLimitValue");
                    flexibleTextView4.setText(String.valueOf(intValue));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final g37<ra5> O6() {
        g37<ra5> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final n26 P6() {
        n26 n26 = this.j;
        if (n26 != null) {
            return n26;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != -4398250 || !str.equals("SET TO WATCH FAIL")) {
                return;
            }
            if (i2 == 2131363291) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363373) {
                n26 n26 = this.j;
                if (n26 != null) {
                    Context requireContext = requireContext();
                    pq7.b(requireContext, "requireContext()");
                    n26.A(requireContext, this.l, true);
                    return;
                }
                pq7.n("mViewModel");
                throw null;
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363373) {
                n26 n262 = this.j;
                if (n262 != null) {
                    Context requireContext2 = requireContext();
                    pq7.b(requireContext2, "requireContext()");
                    n262.A(requireContext2, this.l, true);
                    return;
                }
                pq7.n("mViewModel");
                throw null;
            } else if (i2 == 2131363291 && (activity = getActivity()) != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.h26.a
    public void T2() {
        View n;
        g37<ra5> g37 = this.i;
        if (g37 != null) {
            ra5 a2 = g37.a();
            if (a2 != null && (n = a2.n()) != null) {
                n.requestFocus();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.h26.a
    public void a1(QuickResponseMessage quickResponseMessage) {
        pq7.c(quickResponseMessage, "qr");
        n26 n26 = this.j;
        if (n26 != null) {
            n26.y(quickResponseMessage);
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "onActivityResult result code is " + i3);
        if (i3 != 1) {
            return;
        }
        if (i2 == 112) {
            n26 n26 = this.j;
            if (n26 != null) {
                Context requireContext = requireContext();
                pq7.b(requireContext, "requireContext()");
                n26.A(requireContext, this.l, false);
                return;
            }
            pq7.n("mViewModel");
            throw null;
        } else if (i2 == 113 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ra5 ra5 = (ra5) aq0.f(LayoutInflater.from(getContext()), 2131558613, null, false, A6());
        PortfolioApp.h0.c().M().y1().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(n26.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026nseViewModel::class.java)");
            n26 n26 = (n26) a2;
            this.j = n26;
            if (n26 != null) {
                n26.v().h(getViewLifecycleOwner(), new b(this));
                n26 n262 = this.j;
                if (n262 != null) {
                    n262.j().h(getViewLifecycleOwner(), new c(this));
                    n26 n263 = this.j;
                    if (n263 != null) {
                        n263.l().h(getViewLifecycleOwner(), new d(this));
                        this.m = qn5.l.a().d("error");
                        this.s = qn5.l.a().d("nonBrandSurface");
                        this.i = new g37<>(this, ra5);
                        pq7.b(ra5, "binding");
                        return ra5.n();
                    }
                    pq7.n("mViewModel");
                    throw null;
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<ra5> g37 = this.i;
        if (g37 != null) {
            ra5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "ftvLimitValue");
                flexibleTextView.setText("0");
                FlexibleEditText flexibleEditText = a2.s;
                pq7.b(flexibleEditText, "etMessage");
                flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(50)});
                FlexibleEditText flexibleEditText2 = a2.s;
                pq7.b(flexibleEditText2, "etMessage");
                flexibleEditText2.addTextChangedListener(new e(a2, this));
                this.k = new h26(new ArrayList(), this, 50);
                RecyclerView recyclerView = a2.x;
                pq7.b(recyclerView, "rvMessage");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.x;
                pq7.b(recyclerView2, "rvMessage");
                h26 h26 = this.k;
                if (h26 != null) {
                    recyclerView2.setAdapter(h26);
                    a2.r.setOnClickListener(new f(this));
                    a2.q.setOnClickListener(new g(a2, this));
                } else {
                    pq7.n("mResponseAdapter");
                    throw null;
                }
            }
            n26 n26 = this.j;
            if (n26 != null) {
                LiveData<List<QuickResponseMessage>> u2 = n26.u();
                if (u2 != null) {
                    u2.h(getViewLifecycleOwner(), new h(this));
                    return;
                }
                return;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.h26.a
    public void x5(int i2, String str) {
        T t2;
        boolean z;
        pq7.c(str, "message");
        Iterator<T> it = this.l.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (next.getId() == i2) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "changed message " + ((Object) t3) + ", new message " + str);
        if (t3 != null) {
            t3.setResponse(str);
        }
    }
}
