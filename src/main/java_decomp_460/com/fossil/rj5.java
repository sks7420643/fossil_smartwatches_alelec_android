package com.fossil;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.fossil.af1;
import com.fossil.wb1;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rj5 implements af1<sj5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements wb1<InputStream> {
        @DexIgnore
        public wb1<InputStream> b;
        @DexIgnore
        public volatile boolean c;
        @DexIgnore
        public /* final */ sj5 d;

        @DexIgnore
        public a(rj5 rj5, sj5 sj5) {
            pq7.c(sj5, "mAvatarModel");
            this.d = sj5;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
            wb1<InputStream> wb1 = this.b;
            if (wb1 != null) {
                wb1.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
            this.c = true;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super InputStream> aVar) {
            pq7.c(sa1, "priority");
            pq7.c(aVar, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.d.e())) {
                    this.b = new cc1(new te1(this.d.e()), 100);
                } else if (this.d.d() != null) {
                    this.b = new hc1(PortfolioApp.h0.c().getContentResolver(), this.d.d());
                }
                wb1<InputStream> wb1 = this.b;
                if (wb1 != null) {
                    wb1.d(sa1, aVar);
                }
            } catch (Exception e) {
                wb1<InputStream> wb12 = this.b;
                if (wb12 != null) {
                    wb12.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String c2 = this.d.c();
            if (c2 == null) {
                c2 = "";
            }
            Bitmap j = i37.j(c2);
            if (j != null) {
                j.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            aVar.e(this.c ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bf1<sj5, InputStream> {
        @DexIgnore
        /* renamed from: a */
        public rj5 b(ef1 ef1) {
            pq7.c(ef1, "multiFactory");
            return new rj5();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(sj5 sj5, int i, int i2, ob1 ob1) {
        pq7.c(sj5, "avatarModel");
        pq7.c(ob1, "options");
        return new af1.a<>(sj5, new a(this, sj5));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(sj5 sj5) {
        pq7.c(sj5, "avatarModel");
        return true;
    }
}
