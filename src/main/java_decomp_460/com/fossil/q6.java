package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q6 {
    @DexIgnore
    public /* synthetic */ q6(kq7 kq7) {
    }

    @DexIgnore
    public final r6[] a(int i) {
        r6[] values = r6.values();
        ArrayList arrayList = new ArrayList();
        for (r6 r6Var : values) {
            if ((r6Var.b & i) != 0) {
                arrayList.add(r6Var);
            }
        }
        Object[] array = arrayList.toArray(new r6[0]);
        if (array != null) {
            return (r6[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
