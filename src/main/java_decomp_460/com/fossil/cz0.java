package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static boolean f694a; // = true;
    @DexIgnore
    public static Method b;
    @DexIgnore
    public static boolean c;

    @DexIgnore
    public static int a(ViewGroup viewGroup, int i) {
        if (Build.VERSION.SDK_INT >= 29) {
            return viewGroup.getChildDrawingOrder(i);
        }
        if (!c) {
            try {
                Method declaredMethod = ViewGroup.class.getDeclaredMethod("getChildDrawingOrder", Integer.TYPE, Integer.TYPE);
                b = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e) {
            }
            c = true;
        }
        Method method = b;
        if (method == null) {
            return i;
        }
        try {
            return ((Integer) method.invoke(viewGroup, Integer.valueOf(viewGroup.getChildCount()), Integer.valueOf(i))).intValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            return i;
        }
    }

    @DexIgnore
    public static bz0 b(ViewGroup viewGroup) {
        return Build.VERSION.SDK_INT >= 18 ? new az0(viewGroup) : zy0.g(viewGroup);
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void c(ViewGroup viewGroup, boolean z) {
        if (f694a) {
            try {
                viewGroup.suppressLayout(z);
            } catch (NoSuchMethodError e) {
                f694a = false;
            }
        }
    }

    @DexIgnore
    public static void d(ViewGroup viewGroup, boolean z) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            viewGroup.suppressLayout(z);
        } else if (i >= 18) {
            c(viewGroup, z);
        } else {
            dz0.b(viewGroup, z);
        }
    }
}
