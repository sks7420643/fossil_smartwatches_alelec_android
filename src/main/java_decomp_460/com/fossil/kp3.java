package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ or3 b;
    @DexIgnore
    public /* final */ /* synthetic */ u93 c;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 d;

    @DexIgnore
    public kp3(fp3 fp3, or3 or3, u93 u93) {
        this.d = fp3;
        this.b = or3;
        this.c = u93;
    }

    @DexIgnore
    public final void run() {
        try {
            cl3 cl3 = this.d.d;
            if (cl3 == null) {
                this.d.d().F().a("Failed to get app instance id");
                return;
            }
            String n0 = cl3.n0(this.b);
            if (n0 != null) {
                this.d.p().M(n0);
                this.d.l().l.b(n0);
            }
            this.d.e0();
            this.d.k().Q(this.c, n0);
        } catch (RemoteException e) {
            this.d.d().F().b("Failed to get app instance id", e);
        } finally {
            this.d.k().Q(this.c, null);
        }
    }
}
