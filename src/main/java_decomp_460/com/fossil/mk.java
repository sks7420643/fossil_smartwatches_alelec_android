package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk extends lp {
    @DexIgnore
    public r4 C;

    @DexIgnore
    public mk(k5 k5Var, i60 i60, String str) {
        super(k5Var, i60, yp.g0, str, false, 16);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.i(this, new cs(this.w), new bj(this), nj.b, null, new ak(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.o1, ey1.a(this.w.H()));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E = super.E();
        jd0 jd0 = jd0.D0;
        r4 r4Var = this.C;
        return g80.k(E, jd0, r4Var != null ? ey1.a(r4Var) : null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        r4 r4Var = this.C;
        return r4Var != null ? r4Var : r4.BOND_NONE;
    }
}
