package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo6 extends qo6 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ ro6 e;

    /*
    static {
        String simpleName = vo6.class.getSimpleName();
        pq7.b(simpleName, "ReplaceBatteryPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public vo6(ro6 ro6) {
        pq7.c(ro6, "mView");
        this.e = ro6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void n() {
        this.e.M5(this);
    }
}
