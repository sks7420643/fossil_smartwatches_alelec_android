package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ byte[] f462a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public bo4(byte[] bArr, byte[] bArr2) {
        this.f462a = bArr;
        this.b = bArr2;
    }

    @DexIgnore
    public byte[] a() {
        return this.f462a;
    }

    @DexIgnore
    public byte[] b() {
        return this.b;
    }
}
