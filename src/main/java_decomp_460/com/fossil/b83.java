package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b83 implements xw2<e83> {
    @DexIgnore
    public static b83 c; // = new b83();
    @DexIgnore
    public /* final */ xw2<e83> b;

    @DexIgnore
    public b83() {
        this(ww2.b(new d83()));
    }

    @DexIgnore
    public b83(xw2<e83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((e83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ e83 zza() {
        return this.b.zza();
    }
}
