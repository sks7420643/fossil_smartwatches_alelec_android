package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wh6 extends pv5 implements vh6, RecyclerViewCalendar.a {
    @DexIgnore
    public y67 g;
    @DexIgnore
    public g37<v65> h;
    @DexIgnore
    public uh6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh6 b;

        @DexIgnore
        public a(wh6 wh6, v65 v65) {
            this.b = wh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wh6.K6(this.b).a().l(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ wh6 f3940a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(wh6 wh6) {
            this.f3940a = wh6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            pq7.c(calendar, "calendar");
            uh6 uh6 = this.f3940a.i;
            if (uh6 != null) {
                Date time = calendar.getTime();
                pq7.b(time, "calendar.time");
                uh6.n(time);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ y67 K6(wh6 wh6) {
        y67 y67 = wh6.g;
        if (y67 != null) {
            return y67;
        }
        pq7.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "GoalTrackingOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6() {
        v65 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        g37<v65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.J("hybridGoalTrackingTab");
        }
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(uh6 uh6) {
        pq7.c(uh6, "presenter");
        this.i = uh6;
    }

    @DexIgnore
    @Override // com.fossil.vh6
    public void e(TreeMap<Long, Float> treeMap) {
        v65 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        pq7.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        g37<v65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.vh6
    public void g(Date date, Date date2) {
        v65 a2;
        pq7.c(date, "selectDate");
        pq7.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        g37<v65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            pq7.b(instance, "selectCalendar");
            instance.setTime(date);
            pq7.b(instance2, "startCalendar");
            instance2.setTime(lk5.V(date2));
            pq7.b(instance3, "endCalendar");
            instance3.setTime(lk5.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
    public void k0(int i2, Calendar calendar) {
        pq7.c(calendar, "calendar");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i2 + ", calendar=" + calendar);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.C;
            Date time = calendar.getTime();
            pq7.b(time, "it.time");
            pq7.b(activity, Constants.ACTIVITY);
            aVar.a(time, activity);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        v65 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onCreateView");
        v65 v65 = (v65) aq0.f(layoutInflater, 2131558561, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ts0 a3 = vs0.e(activity).a(y67.class);
            pq7.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (y67) a3;
            v65.s.setOnClickListener(new a(this, v65));
        }
        RecyclerViewCalendar recyclerViewCalendar = v65.q;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        v65.q.setOnCalendarMonthChanged(new b(this));
        v65.q.setOnCalendarItemClickListener(this);
        this.h = new g37<>(this, v65);
        M6();
        g37<v65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onResume");
        M6();
        uh6 uh6 = this.i;
        if (uh6 != null) {
            uh6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onStop");
        uh6 uh6 = this.i;
        if (uh6 != null) {
            uh6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.vh6
    public void x(boolean z) {
        v65 a2;
        g37<v65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                pq7.b(recyclerViewCalendar, "binding.calendarMonth");
                recyclerViewCalendar.setVisibility(4);
                ConstraintLayout constraintLayout = a2.r;
                pq7.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            RecyclerViewCalendar recyclerViewCalendar2 = a2.q;
            pq7.b(recyclerViewCalendar2, "binding.calendarMonth");
            recyclerViewCalendar2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.r;
            pq7.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
