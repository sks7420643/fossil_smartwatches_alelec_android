package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p21 extends k21<Boolean> {
    @DexIgnore
    public p21(Context context, k41 k41) {
        super(w21.c(context, k41).e());
    }

    @DexIgnore
    @Override // com.fossil.k21
    public boolean b(o31 o31) {
        return o31.j.i();
    }

    @DexIgnore
    /* renamed from: i */
    public boolean c(Boolean bool) {
        return !bool.booleanValue();
    }
}
