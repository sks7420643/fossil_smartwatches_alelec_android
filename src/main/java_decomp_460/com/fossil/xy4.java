package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xy4 f4212a; // = new xy4();

    @DexIgnore
    public final Date a() {
        return new Date();
    }

    @DexIgnore
    public final long b() {
        return new Date().getTime();
    }
}
