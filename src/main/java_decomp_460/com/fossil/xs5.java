package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs5 extends us5 {
    @DexIgnore
    public String d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xs5(String str, String str2, String str3) {
        super(str, str2);
        pq7.c(str, "tagName");
        pq7.c(str2, "title");
        pq7.c(str3, "text");
        this.d = str3;
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final void g(String str) {
        pq7.c(str, "<set-?>");
        this.d = str;
    }
}
