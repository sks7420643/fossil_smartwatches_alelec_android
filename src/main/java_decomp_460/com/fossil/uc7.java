package com.fossil;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface uc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final uc7 f3568a = new a();
    @DexIgnore
    public static final uc7 b = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements uc7 {
        @DexIgnore
        @Override // com.fossil.uc7
        public void a(nc7 nc7) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements uc7 {
        @DexIgnore
        @Override // com.fossil.uc7
        public void a(nc7 nc7) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + nc7 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(nc7 nc7);
}
