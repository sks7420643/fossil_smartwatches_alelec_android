package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.os.Build;
import com.fossil.p18;
import com.fossil.w71;
import com.fossil.x71;
import com.fossil.y71;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y71<T extends y71<T>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object f4251a;
    @DexIgnore
    public String b;
    @DexIgnore
    public List<String> c;
    @DexIgnore
    public x71.a d;
    @DexIgnore
    public g81 e;
    @DexIgnore
    public e81 f;
    @DexIgnore
    public d81 g;
    @DexIgnore
    public u51 h;
    @DexIgnore
    public dv7 i;
    @DexIgnore
    public List<? extends m81> j;
    @DexIgnore
    public Bitmap.Config k;
    @DexIgnore
    public ColorSpace l;
    @DexIgnore
    public p18.a m;
    @DexIgnore
    public w71.a n;
    @DexIgnore
    public s71 o;
    @DexIgnore
    public s71 p;
    @DexIgnore
    public s71 q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;

    @DexIgnore
    public y71(z41 z41) {
        this.f4251a = null;
        this.b = null;
        this.c = hm7.e();
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = z41.g();
        this.h = null;
        this.i = z41.c();
        this.j = hm7.e();
        this.k = y81.f4257a.e();
        if (Build.VERSION.SDK_INT >= 26) {
            this.l = null;
        }
        this.m = null;
        this.n = null;
        s71 s71 = s71.ENABLED;
        this.o = s71;
        this.p = s71;
        this.q = s71;
        this.r = z41.a();
        this.s = z41.b();
    }

    @DexIgnore
    public /* synthetic */ y71(z41 z41, kq7 kq7) {
        this(z41);
    }

    @DexIgnore
    public final List<String> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.r;
    }

    @DexIgnore
    public final boolean c() {
        return this.s;
    }

    @DexIgnore
    public final Bitmap.Config d() {
        return this.k;
    }

    @DexIgnore
    public final ColorSpace e() {
        return this.l;
    }

    @DexIgnore
    public final Object f() {
        return this.f4251a;
    }

    @DexIgnore
    public final u51 g() {
        return this.h;
    }

    @DexIgnore
    public final s71 h() {
        return this.p;
    }

    @DexIgnore
    public final dv7 i() {
        return this.i;
    }

    @DexIgnore
    public final p18.a j() {
        return this.m;
    }

    @DexIgnore
    public final String k() {
        return this.b;
    }

    @DexIgnore
    public final x71.a l() {
        return this.d;
    }

    @DexIgnore
    public final s71 m() {
        return this.q;
    }

    @DexIgnore
    public final s71 n() {
        return this.o;
    }

    @DexIgnore
    public final w71.a o() {
        return this.n;
    }

    @DexIgnore
    public final d81 p() {
        return this.g;
    }

    @DexIgnore
    public final e81 q() {
        return this.f;
    }

    @DexIgnore
    public final g81 r() {
        return this.e;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.m81>, java.util.List<com.fossil.m81> */
    public final List<m81> s() {
        return this.j;
    }

    @DexIgnore
    public final T t(x71.a aVar) {
        this.d = aVar;
        return this;
    }

    @DexIgnore
    public final void u(Object obj) {
        this.f4251a = obj;
    }

    @DexIgnore
    public final T v(m81... m81Arr) {
        pq7.c(m81Arr, "transformations");
        this.j = em7.d0(m81Arr);
        return this;
    }
}
