package com.fossil;

import com.fossil.r62;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d82 extends r62 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public d82(String str) {
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.r62
    public z52 d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public t62<Status> e() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public void f() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public void g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public void h(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public boolean n() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public void q(r62.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.r62
    public void r(r62.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }
}
