package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class mb4 implements FilenameFilter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2350a;

    @DexIgnore
    public mb4(String str) {
        this.f2350a = str;
    }

    @DexIgnore
    public static FilenameFilter a(String str) {
        return new mb4(str);
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(this.f2350a);
    }
}
