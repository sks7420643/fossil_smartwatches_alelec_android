package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class of1 implements af1<Uri, InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2674a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<Uri, InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f2675a;

        @DexIgnore
        public a(Context context) {
            this.f2675a = context;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, InputStream> b(ef1 ef1) {
            return new of1(this.f2675a);
        }
    }

    @DexIgnore
    public of1(Context context) {
        this.f2674a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(Uri uri, int i, int i2, ob1 ob1) {
        if (!jc1.d(i, i2) || !e(ob1)) {
            return null;
        }
        return new af1.a<>(new yj1(uri), kc1.f(this.f2674a, uri));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return jc1.c(uri);
    }

    @DexIgnore
    public final boolean e(ob1 ob1) {
        Long l = (Long) ob1.c(vg1.d);
        return l != null && l.longValue() == -1;
    }
}
