package com.fossil;

import com.fossil.p72;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar2 implements p72.b<fa3> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ LocationResult f306a;

    @DexIgnore
    public ar2(zq2 zq2, LocationResult locationResult) {
        this.f306a = locationResult;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.p72.b
    public final /* synthetic */ void a(fa3 fa3) {
        fa3.onLocationResult(this.f306a);
    }

    @DexIgnore
    @Override // com.fossil.p72.b
    public final void b() {
    }
}
