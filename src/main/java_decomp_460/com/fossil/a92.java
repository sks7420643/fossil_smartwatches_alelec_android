package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a92 extends q92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public WeakReference<t82> f226a;

    @DexIgnore
    public a92(t82 t82) {
        this.f226a = new WeakReference<>(t82);
    }

    @DexIgnore
    @Override // com.fossil.q92
    public final void a() {
        t82 t82 = this.f226a.get();
        if (t82 != null) {
            t82.w();
        }
    }
}
