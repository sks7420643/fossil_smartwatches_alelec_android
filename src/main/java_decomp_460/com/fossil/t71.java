package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import com.fossil.x71;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t71 extends x71 {
    @DexIgnore
    public /* final */ Drawable A;
    @DexIgnore
    public /* final */ Drawable B;
    @DexIgnore
    public /* final */ Drawable C;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3373a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ j81 c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ n81 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ List<String> g;
    @DexIgnore
    public /* final */ x71.a h;
    @DexIgnore
    public /* final */ g81 i;
    @DexIgnore
    public /* final */ e81 j;
    @DexIgnore
    public /* final */ d81 k;
    @DexIgnore
    public /* final */ u51 l;
    @DexIgnore
    public /* final */ dv7 m;
    @DexIgnore
    public /* final */ List<m81> n;
    @DexIgnore
    public /* final */ Bitmap.Config o;
    @DexIgnore
    public /* final */ ColorSpace p;
    @DexIgnore
    public /* final */ p18 q;
    @DexIgnore
    public /* final */ w71 r;
    @DexIgnore
    public /* final */ s71 s;
    @DexIgnore
    public /* final */ s71 t;
    @DexIgnore
    public /* final */ s71 u;
    @DexIgnore
    public /* final */ boolean v;
    @DexIgnore
    public /* final */ boolean w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r16v0, resolved type: java.util.List<? extends com.fossil.m81> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t71(Context context, Object obj, j81 j81, Lifecycle lifecycle, n81 n81, String str, List<String> list, x71.a aVar, g81 g81, e81 e81, d81 d81, u51 u51, dv7 dv7, List<? extends m81> list2, Bitmap.Config config, ColorSpace colorSpace, p18 p18, w71 w71, s71 s71, s71 s712, s71 s713, boolean z2, boolean z3, int i2, int i3, int i4, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        super(null);
        pq7.c(context, "context");
        pq7.c(list, "aliasKeys");
        pq7.c(d81, "precision");
        pq7.c(dv7, "dispatcher");
        pq7.c(list2, "transformations");
        pq7.c(config, "bitmapConfig");
        pq7.c(p18, "headers");
        pq7.c(w71, "parameters");
        pq7.c(s71, "networkCachePolicy");
        pq7.c(s712, "diskCachePolicy");
        pq7.c(s713, "memoryCachePolicy");
        this.f3373a = context;
        this.b = obj;
        this.c = j81;
        this.d = lifecycle;
        this.e = n81;
        this.f = str;
        this.g = list;
        this.h = aVar;
        this.i = g81;
        this.j = e81;
        this.k = d81;
        this.l = u51;
        this.m = dv7;
        this.n = list2;
        this.o = config;
        this.p = colorSpace;
        this.q = p18;
        this.r = w71;
        this.s = s71;
        this.t = s712;
        this.u = s713;
        this.v = z2;
        this.w = z3;
        this.x = i2;
        this.y = i3;
        this.z = i4;
        this.A = drawable;
        this.B = drawable2;
        this.C = drawable3;
    }

    @DexIgnore
    public Lifecycle A() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public List<String> a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public boolean b() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public boolean c() {
        return this.w;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public Bitmap.Config d() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public ColorSpace e() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public u51 f() {
        return this.l;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public s71 g() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public dv7 h() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public Drawable i() {
        return z(this.f3373a, this.B, this.y);
    }

    @DexIgnore
    @Override // com.fossil.x71
    public Drawable j() {
        return z(this.f3373a, this.C, this.z);
    }

    @DexIgnore
    @Override // com.fossil.x71
    public p18 k() {
        return this.q;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public String l() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public x71.a m() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public s71 n() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public s71 o() {
        return this.s;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public w71 p() {
        return this.r;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public Drawable q() {
        return z(this.f3373a, this.A, this.x);
    }

    @DexIgnore
    @Override // com.fossil.x71
    public d81 r() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public e81 s() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public g81 t() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public j81 u() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public List<m81> v() {
        return this.n;
    }

    @DexIgnore
    @Override // com.fossil.x71
    public n81 w() {
        return this.e;
    }

    @DexIgnore
    public final Context x() {
        return this.f3373a;
    }

    @DexIgnore
    public Object y() {
        return this.b;
    }

    @DexIgnore
    public final Drawable z(Context context, Drawable drawable, int i2) {
        if (drawable != null) {
            return drawable;
        }
        if (i2 != 0) {
            return t81.a(context, i2);
        }
        return null;
    }
}
