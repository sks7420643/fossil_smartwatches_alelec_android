package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rg1 implements qb1<Uri, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bh1 f3109a;
    @DexIgnore
    public /* final */ rd1 b;

    @DexIgnore
    public rg1(bh1 bh1, rd1 rd1) {
        this.f3109a = bh1;
        this.b = rd1;
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(Uri uri, int i, int i2, ob1 ob1) {
        id1<Drawable> c = this.f3109a.b(uri, i, i2, ob1);
        if (c == null) {
            return null;
        }
        return hg1.a(this.b, c.get(), i, i2);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri, ob1 ob1) {
        return "android.resource".equals(uri.getScheme());
    }
}
