package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq4 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<String> f4354a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f4355a;
        @DexIgnore
        public /* final */ ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            pq7.c(view, "itemView");
            View findViewById = view.findViewById(2131362406);
            pq7.b(findViewById, "itemView.findViewById(R.id.ftv_description)");
            this.f4355a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362691);
            pq7.b(findViewById2, "itemView.findViewById(R.id.iv_device)");
            this.b = (ImageView) findViewById2;
        }

        @DexIgnore
        public final void a(String str) {
            if (str != null) {
                this.f4355a.setText(str);
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "viewHolder");
        aVar.a(this.f4354a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4354a.size();
    }

    @DexIgnore
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558705, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(inflate);
    }

    @DexIgnore
    public final void i(List<String> list) {
        pq7.c(list, "data");
        this.f4354a.clear();
        this.f4354a.addAll(list);
        notifyDataSetChanged();
    }
}
