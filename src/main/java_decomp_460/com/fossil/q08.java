package com.fossil;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q08 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ long f2907a; // = yz7.e("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, null);
    @DexIgnore
    public static /* final */ int b; // = yz7.d("kotlinx.coroutines.scheduler.core.pool.size", bs7.d(wz7.a(), 2), 1, 0, 8, null);
    @DexIgnore
    public static /* final */ int c; // = yz7.d("kotlinx.coroutines.scheduler.max.pool.size", bs7.i(wz7.a() * 128, b, 2097150), 0, 2097150, 4, null);
    @DexIgnore
    public static /* final */ long d; // = TimeUnit.SECONDS.toNanos(yz7.e("kotlinx.coroutines.scheduler.keep.alive.sec", 60, 0, 0, 12, null));
    @DexIgnore
    public static r08 e; // = l08.f2131a;

    /*
    static {
        int unused = yz7.d("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, null);
    }
    */
}
