package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f2815a;
    @DexIgnore
    public /* final */ Method b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public boolean d; // = true;

    @DexIgnore
    public pc7(Object obj, Method method) {
        if (obj == null) {
            throw new NullPointerException("EventHandler target cannot be null.");
        } else if (method != null) {
            this.f2815a = obj;
            this.b = method;
            method.setAccessible(true);
            this.c = ((method.hashCode() + 31) * 31) + obj.hashCode();
        } else {
            throw new NullPointerException("EventHandler method cannot be null.");
        }
    }

    @DexIgnore
    public void a(Object obj) throws InvocationTargetException {
        if (this.d) {
            try {
                this.b.invoke(this.f2815a, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (InvocationTargetException e2) {
                if (e2.getCause() instanceof Error) {
                    throw ((Error) e2.getCause());
                }
                throw e2;
            }
        } else {
            throw new IllegalStateException(toString() + " has been invalidated and can no longer handle events.");
        }
    }

    @DexIgnore
    public void b() {
        this.d = false;
    }

    @DexIgnore
    public boolean c() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (pc7.class != obj.getClass()) {
            return false;
        }
        pc7 pc7 = (pc7) obj;
        return this.b.equals(pc7.b) && this.f2815a == pc7.f2815a;
    }

    @DexIgnore
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "[EventHandler " + this.b + "]";
    }
}
