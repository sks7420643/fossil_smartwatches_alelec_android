package com.fossil;

import com.fossil.eh4;
import com.fossil.fh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch4 extends fh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f614a;
    @DexIgnore
    public /* final */ eh4.a b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fh4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f615a;
        @DexIgnore
        public eh4.a b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;
        @DexIgnore
        public String g;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(fh4 fh4) {
            this.f615a = fh4.d();
            this.b = fh4.g();
            this.c = fh4.b();
            this.d = fh4.f();
            this.e = Long.valueOf(fh4.c());
            this.f = Long.valueOf(fh4.h());
            this.g = fh4.e();
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4 a() {
            String str = "";
            if (this.b == null) {
                str = " registrationStatus";
            }
            if (this.e == null) {
                str = str + " expiresInSecs";
            }
            if (this.f == null) {
                str = str + " tokenCreationEpochInSecs";
            }
            if (str.isEmpty()) {
                return new ch4(this.f615a, this.b, this.c, this.d, this.e.longValue(), this.f.longValue(), this.g);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a c(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a d(String str) {
            this.f615a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a e(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a f(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a g(eh4.a aVar) {
            if (aVar != null) {
                this.b = aVar;
                return this;
            }
            throw new NullPointerException("Null registrationStatus");
        }

        @DexIgnore
        @Override // com.fossil.fh4.a
        public fh4.a h(long j) {
            this.f = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public ch4(String str, eh4.a aVar, String str2, String str3, long j, long j2, String str4) {
        this.f614a = str;
        this.b = aVar;
        this.c = str2;
        this.d = str3;
        this.e = j;
        this.f = j2;
        this.g = str4;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public long c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public String d() {
        return this.f614a;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fh4)) {
            return false;
        }
        fh4 fh4 = (fh4) obj;
        String str3 = this.f614a;
        if (str3 != null ? str3.equals(fh4.d()) : fh4.d() == null) {
            if (this.b.equals(fh4.g()) && ((str = this.c) != null ? str.equals(fh4.b()) : fh4.b() == null) && ((str2 = this.d) != null ? str2.equals(fh4.f()) : fh4.f() == null) && this.e == fh4.c() && this.f == fh4.h()) {
                String str4 = this.g;
                if (str4 == null) {
                    if (fh4.e() == null) {
                        return true;
                    }
                } else if (str4.equals(fh4.e())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public String f() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public eh4.a g() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public long h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f614a;
        int hashCode = str == null ? 0 : str.hashCode();
        int hashCode2 = this.b.hashCode();
        String str2 = this.c;
        int hashCode3 = str2 == null ? 0 : str2.hashCode();
        String str3 = this.d;
        int hashCode4 = str3 == null ? 0 : str3.hashCode();
        long j = this.e;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.f;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        String str4 = this.g;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return ((((((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i2) * 1000003) ^ i3) * 1000003) ^ i;
    }

    @DexIgnore
    @Override // com.fossil.fh4
    public fh4.a n() {
        return new b(this);
    }

    @DexIgnore
    public String toString() {
        return "PersistedInstallationEntry{firebaseInstallationId=" + this.f614a + ", registrationStatus=" + this.b + ", authToken=" + this.c + ", refreshToken=" + this.d + ", expiresInSecs=" + this.e + ", tokenCreationEpochInSecs=" + this.f + ", fisError=" + this.g + "}";
    }
}
