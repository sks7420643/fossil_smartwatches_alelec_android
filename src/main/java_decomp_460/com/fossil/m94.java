package com.fossil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m94 {
    @DexIgnore
    public static void a(InputStream inputStream, File file) throws IOException {
        GZIPOutputStream gZIPOutputStream;
        if (inputStream != null) {
            byte[] bArr = new byte[8192];
            try {
                gZIPOutputStream = new GZIPOutputStream(new FileOutputStream(file));
                while (true) {
                    try {
                        int read = inputStream.read(bArr);
                        if (read > 0) {
                            gZIPOutputStream.write(bArr, 0, read);
                        } else {
                            gZIPOutputStream.finish();
                            r84.f(gZIPOutputStream);
                            return;
                        }
                    } catch (Throwable th) {
                        th = th;
                        r84.f(gZIPOutputStream);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream = null;
                r84.f(gZIPOutputStream);
                throw th;
            }
        }
    }

    @DexIgnore
    public static void b(File file, List<l94> list) {
        InputStream inputStream;
        InputStream inputStream2;
        Throwable th;
        for (l94 l94 : list) {
            try {
                inputStream2 = l94.b();
                if (inputStream2 == null) {
                    inputStream = inputStream2;
                } else {
                    try {
                        a(inputStream2, new File(file, l94.a()));
                        inputStream = inputStream2;
                    } catch (IOException e) {
                        inputStream = inputStream2;
                    } catch (Throwable th2) {
                        th = th2;
                        r84.f(inputStream2);
                        throw th;
                    }
                }
            } catch (IOException e2) {
                inputStream = null;
            } catch (Throwable th3) {
                th = th3;
                inputStream2 = null;
                r84.f(inputStream2);
                throw th;
            }
            r84.f(inputStream);
        }
    }
}
