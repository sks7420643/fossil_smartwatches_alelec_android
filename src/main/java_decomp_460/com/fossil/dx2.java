package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dx2<K, V> implements Map.Entry<K, V> {
    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            return qw2.a(getKey(), entry.getKey()) && qw2.a(getValue(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract K getKey();

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract V getValue();

    @DexIgnore
    public int hashCode() {
        int i = 0;
        K key = getKey();
        V value = getValue();
        int hashCode = key == null ? 0 : key.hashCode();
        if (value != null) {
            i = value.hashCode();
        }
        return i ^ hashCode;
    }

    @DexIgnore
    public String toString() {
        String valueOf = String.valueOf(getKey());
        String valueOf2 = String.valueOf(getValue());
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        return sb.toString();
    }
}
