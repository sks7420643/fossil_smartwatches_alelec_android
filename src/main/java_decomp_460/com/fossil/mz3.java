package com.fossil;

import android.graphics.Typeface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz3 extends rz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Typeface f2441a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Typeface typeface);
    }

    @DexIgnore
    public mz3(a aVar, Typeface typeface) {
        this.f2441a = typeface;
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.rz3
    public void a(int i) {
        d(this.f2441a);
    }

    @DexIgnore
    @Override // com.fossil.rz3
    public void b(Typeface typeface, boolean z) {
        d(typeface);
    }

    @DexIgnore
    public void c() {
        this.c = true;
    }

    @DexIgnore
    public final void d(Typeface typeface) {
        if (!this.c) {
            this.b.a(typeface);
        }
    }
}
