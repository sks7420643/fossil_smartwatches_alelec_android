package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu6 implements Factory<vu6> {
    @DexIgnore
    public static vu6 a(xu6 xu6) {
        vu6 b = xu6.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
