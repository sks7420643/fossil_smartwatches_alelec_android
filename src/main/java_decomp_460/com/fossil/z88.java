package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z88 implements e88<w18, Byte> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ z88 f4433a; // = new z88();

    @DexIgnore
    /* renamed from: b */
    public Byte a(w18 w18) throws IOException {
        return Byte.valueOf(w18.string());
    }
}
