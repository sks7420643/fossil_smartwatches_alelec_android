package com.fossil;

import com.fossil.e13;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w33 {
    @DexIgnore
    public static /* final */ w33 f; // = new w33(0, new int[0], new Object[0], false);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3875a;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public w33() {
        this(0, new int[8], new Object[8], true);
    }

    @DexIgnore
    public w33(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.f3875a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    @DexIgnore
    public static w33 a() {
        return f;
    }

    @DexIgnore
    public static w33 b(w33 w33, w33 w332) {
        int i = w33.f3875a + w332.f3875a;
        int[] copyOf = Arrays.copyOf(w33.b, i);
        System.arraycopy(w332.b, 0, copyOf, w33.f3875a, w332.f3875a);
        Object[] copyOf2 = Arrays.copyOf(w33.c, i);
        System.arraycopy(w332.c, 0, copyOf2, w33.f3875a, w332.f3875a);
        return new w33(i, copyOf, copyOf2, true);
    }

    @DexIgnore
    public static void d(int i, Object obj, r43 r43) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            r43.zza(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            r43.zzd(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            r43.f(i2, (xz2) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                r43.zzd(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(l13.zzf());
        } else if (r43.zza() == e13.f.k) {
            r43.zza(i2);
            ((w33) obj).h(r43);
            r43.zzb(i2);
        } else {
            r43.zzb(i2);
            ((w33) obj).h(r43);
            r43.zza(i2);
        }
    }

    @DexIgnore
    public static w33 g() {
        return new w33();
    }

    @DexIgnore
    public final void c(int i, Object obj) {
        if (this.e) {
            int i2 = this.f3875a;
            if (i2 == this.b.length) {
                int i3 = (i2 < 4 ? 8 : i2 >> 1) + this.f3875a;
                this.b = Arrays.copyOf(this.b, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr = this.b;
            int i4 = this.f3875a;
            iArr[i4] = i;
            this.c[i4] = obj;
            this.f3875a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void e(r43 r43) throws IOException {
        if (r43.zza() == e13.f.l) {
            for (int i = this.f3875a - 1; i >= 0; i--) {
                r43.zza(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.f3875a; i2++) {
            r43.zza(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof w33)) {
            return false;
        }
        w33 w33 = (w33) obj;
        int i = this.f3875a;
        if (i == w33.f3875a) {
            int[] iArr = this.b;
            int[] iArr2 = w33.b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = w33.c;
                int i3 = this.f3875a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void f(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.f3875a; i2++) {
            r23.d(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    @DexIgnore
    public final void h(r43 r43) throws IOException {
        if (this.f3875a != 0) {
            if (r43.zza() == e13.f.k) {
                for (int i = 0; i < this.f3875a; i++) {
                    d(this.b[i], this.c[i], r43);
                }
                return;
            }
            for (int i2 = this.f3875a - 1; i2 >= 0; i2--) {
                d(this.b[i2], this.c[i2], r43);
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int i = 17;
        int i2 = this.f3875a;
        int[] iArr = this.b;
        int i3 = 17;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 = (i3 * 31) + iArr[i4];
        }
        Object[] objArr = this.c;
        int i5 = this.f3875a;
        for (int i6 = 0; i6 < i5; i6++) {
            i = (i * 31) + objArr[i6].hashCode();
        }
        return i + ((((i2 + 527) * 31) + i3) * 31);
    }

    @DexIgnore
    public final void i() {
        this.e = false;
    }

    @DexIgnore
    public final int j() {
        int i = this.d;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f3875a; i2++) {
                i += l03.d0(this.b[i2] >>> 3, (xz2) this.c[i2]);
            }
            this.d = i;
        }
        return i;
    }

    @DexIgnore
    public final int k() {
        int i0;
        int i = this.d;
        if (i == -1) {
            i = 0;
            int i2 = 0;
            while (i2 < this.f3875a) {
                int i3 = this.b[i2];
                int i4 = i3 >>> 3;
                int i5 = i3 & 7;
                if (i5 == 0) {
                    i0 = l03.i0(i4, ((Long) this.c[i2]).longValue());
                } else if (i5 == 1) {
                    i0 = l03.r0(i4, ((Long) this.c[i2]).longValue());
                } else if (i5 == 2) {
                    i0 = l03.U(i4, (xz2) this.c[i2]);
                } else if (i5 == 3) {
                    i0 = ((w33) this.c[i2]).k() + (l03.h0(i4) << 1);
                } else if (i5 == 5) {
                    i0 = l03.y0(i4, ((Integer) this.c[i2]).intValue());
                } else {
                    throw new IllegalStateException(l13.zzf());
                }
                i2++;
                i = i0 + i;
            }
            this.d = i;
        }
        return i;
    }
}
