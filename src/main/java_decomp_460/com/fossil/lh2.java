package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh2 extends rl2 implements mh2 {
    @DexIgnore
    public lh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    @Override // com.fossil.mh2
    public final rg2 G(rg2 rg2, String str, int i, rg2 rg22) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        sl2.c(d, rg22);
        Parcel e = e(2, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.mh2
    public final rg2 L(rg2 rg2, String str, int i, rg2 rg22) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        sl2.c(d, rg22);
        Parcel e = e(3, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
