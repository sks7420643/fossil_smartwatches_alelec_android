package com.fossil;

import com.fossil.ta4;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p94 implements Comparator {
    @DexIgnore
    public static /* final */ p94 b; // = new p94();

    @DexIgnore
    public static Comparator a() {
        return b;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((ta4.b) obj).b().compareTo(((ta4.b) obj2).b());
    }
}
