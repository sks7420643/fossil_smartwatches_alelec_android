package com.fossil;

import android.util.SparseIntArray;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ox5 extends rv5<nx5> {
    @DexIgnore
    Object B0();  // void declaration

    @DexIgnore
    void E(int i);

    @DexIgnore
    Object W2();  // void declaration

    @DexIgnore
    void Y(boolean z);

    @DexIgnore
    void Y1(Alarm alarm, boolean z);

    @DexIgnore
    void Z4(boolean z);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void b5(SparseIntArray sparseIntArray);

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    void l2(String str);

    @DexIgnore
    void m2(boolean z);

    @DexIgnore
    Object q4();  // void declaration

    @DexIgnore
    Object s5();  // void declaration

    @DexIgnore
    void t(String str);
}
