package com.fossil;

import com.fossil.ks7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ls7<R> extends ks7<R>, gp7<R> {

    @DexIgnore
    public interface a<R> extends ks7.a<R>, gp7<R> {
    }

    @DexIgnore
    Object getDelegate();

    @DexIgnore
    a<R> getGetter();
}
