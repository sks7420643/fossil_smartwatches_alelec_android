package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.NoSuchElementException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class el1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ o8 b;
    @DexIgnore
    public /* final */ gl1[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<el1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:62:0x004c */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:64:0x005a */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:65:0x0066 */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r6v8, types: [com.fossil.jl1] */
        /* JADX WARN: Type inference failed for: r6v11, types: [com.fossil.pl1] */
        /* JADX WARN: Type inference failed for: r6v12, types: [com.fossil.ll1] */
        /* JADX WARNING: Unknown variable types count: 3 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.el1[] a(byte[] r14) throws java.lang.IllegalArgumentException {
            /*
            // Method dump skipped, instructions count: 236
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.el1.a.a(byte[]):com.fossil.el1[]");
        }

        @DexIgnore
        /* renamed from: b */
        public el1 createFromParcel(Parcel parcel) {
            ql1 ql1;
            il1 il1;
            o8 o8Var = o8.values()[parcel.readInt()];
            Object[] createTypedArray = parcel.createTypedArray(gl1.CREATOR);
            if (createTypedArray != null) {
                pq7.b(createTypedArray, "parcel.createTypedArray(AlarmSubEntry.CREATOR)!!");
                gl1[] gl1Arr = (gl1[]) createTypedArray;
                for (gl1 gl1 : gl1Arr) {
                    if (gl1 instanceof hl1) {
                        if (gl1 != null) {
                            hl1 hl1 = (hl1) gl1;
                            int length = gl1Arr.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    ql1 = null;
                                    break;
                                }
                                ql1 = gl1Arr[i];
                                if (ql1 instanceof ql1) {
                                    break;
                                }
                                i++;
                            }
                            ql1 ql12 = ql1 != null ? ql1 : null;
                            int length2 = gl1Arr.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length2) {
                                    il1 = null;
                                    break;
                                }
                                gl1 gl12 = gl1Arr[i2];
                                if (gl12 instanceof il1) {
                                    il1 = gl12;
                                    break;
                                }
                                i2++;
                            }
                            il1 il12 = il1 != null ? il1 : null;
                            int i3 = m8.b[o8Var.ordinal()];
                            if (i3 == 1) {
                                return hl1 instanceof kl1 ? new jl1((kl1) hl1, ql12, il12) : new nl1((ol1) hl1, ql12, il12);
                            }
                            if (i3 == 2) {
                                return hl1 instanceof kl1 ? new ll1((kl1) hl1, ql12, il12) : new pl1((ol1) hl1, ql12, il12);
                            }
                            throw new al7();
                        } else {
                            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
                        }
                    }
                }
                throw new NoSuchElementException("Array contains no element matching the predicate.");
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public el1[] newArray(int i) {
            return new el1[i];
        }
    }

    @DexIgnore
    public el1(o8 o8Var, gl1[] gl1Arr) {
        this.b = o8Var;
        this.c = gl1Arr;
    }

    @DexIgnore
    public final byte[] a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (gl1 gl1 : this.c) {
            byteArrayOutputStream.write(gl1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "byteArrayOutputStreamWriter.toByteArray()");
        byte[] array = ByteBuffer.allocate(byteArray.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.b.b).putShort((short) byteArray.length).put(byteArray).array();
        pq7.b(array, "ByteBuffer.allocate(ENTR\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final gl1[] b() {
        return this.c;
    }

    @DexIgnore
    public final o8 c() {
        return this.b;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            fl1 fl1 = (fl1) obj;
            return this.b == fl1.c() && cm7.c(this.c, fl1.b());
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.Alarm");
    }

    @DexIgnore
    public hl1 getFireTime() {
        gl1[] gl1Arr = this.c;
        for (gl1 gl1 : gl1Arr) {
            if (gl1 instanceof ql1) {
                if (gl1 != null) {
                    return (hl1) gl1;
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final il1 getMessage() {
        il1 il1;
        gl1[] gl1Arr = this.c;
        int length = gl1Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                il1 = null;
                break;
            }
            il1 = gl1Arr[i];
            if (il1 instanceof il1) {
                break;
            }
            i++;
        }
        return il1;
    }

    @DexIgnore
    public final ql1 getTitle() {
        ql1 ql1;
        gl1[] gl1Arr = this.c;
        int length = gl1Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                ql1 = null;
                break;
            }
            ql1 = gl1Arr[i];
            if (ql1 instanceof ql1) {
                break;
            }
            i++;
        }
        return ql1;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + bm7.a(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(new JSONObject(), jd0.e, ey1.a(this.b));
        for (gl1 gl1 : this.c) {
            gy1.b(k, gl1.toJSONObject(), false, 2, null);
        }
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
