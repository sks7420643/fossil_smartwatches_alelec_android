package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bf6 f1617a;
    @DexIgnore
    public /* final */ tf6 b;
    @DexIgnore
    public /* final */ nf6 c;

    @DexIgnore
    public if6(bf6 bf6, tf6 tf6, nf6 nf6) {
        pq7.c(bf6, "mActivityOverviewDayView");
        pq7.c(tf6, "mActivityOverviewWeekView");
        pq7.c(nf6, "mActivityOverviewMonthView");
        this.f1617a = bf6;
        this.b = tf6;
        this.c = nf6;
    }

    @DexIgnore
    public final bf6 a() {
        return this.f1617a;
    }

    @DexIgnore
    public final nf6 b() {
        return this.c;
    }

    @DexIgnore
    public final tf6 c() {
        return this.b;
    }
}
