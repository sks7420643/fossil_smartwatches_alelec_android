package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.facebook.stetho.server.http.HttpHeaders;
import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.p18;
import com.fossil.v18;
import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q28 implements Interceptor {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i18 f2917a;

    @DexIgnore
    public q28(i18 i18) {
        this.f2917a = i18;
    }

    @DexIgnore
    public final String a(List<h18> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            h18 h18 = list.get(i);
            sb.append(h18.c());
            sb.append('=');
            sb.append(h18.k());
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        boolean z = false;
        v18 c = chain.c();
        v18.a h = c.h();
        RequestBody a2 = c.a();
        if (a2 != null) {
            r18 b = a2.b();
            if (b != null) {
                h.e("Content-Type", b.toString());
            }
            long a3 = a2.a();
            if (a3 != -1) {
                h.e(HttpHeaders.CONTENT_LENGTH, Long.toString(a3));
                h.i("Transfer-Encoding");
            } else {
                h.e("Transfer-Encoding", "chunked");
                h.i(HttpHeaders.CONTENT_LENGTH);
            }
        }
        if (c.c("Host") == null) {
            h.e("Host", b28.s(c.j(), false));
        }
        if (c.c(WebSocketHandler.HEADER_CONNECTION) == null) {
            h.e(WebSocketHandler.HEADER_CONNECTION, "Keep-Alive");
        }
        if (c.c("Accept-Encoding") == null && c.c("Range") == null) {
            z = true;
            h.e("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        }
        List<h18> b2 = this.f2917a.b(c.j());
        if (!b2.isEmpty()) {
            h.e("Cookie", a(b2));
        }
        if (c.c("User-Agent") == null) {
            h.e("User-Agent", c28.a());
        }
        Response d = chain.d(h.b());
        u28.g(this.f2917a, c.j(), d.l());
        Response.a B = d.B();
        B.p(c);
        if (z && DecompressionHelper.GZIP_ENCODING.equalsIgnoreCase(d.j(GraphRequest.CONTENT_ENCODING_HEADER)) && u28.c(d)) {
            p48 p48 = new p48(d.a().source());
            p18.a f = d.l().f();
            f.g(GraphRequest.CONTENT_ENCODING_HEADER);
            f.g(HttpHeaders.CONTENT_LENGTH);
            B.j(f.e());
            B.b(new x28(d.j("Content-Type"), -1, s48.d(p48)));
        }
        return B.c();
    }
}
