package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.in6;
import com.fossil.jr4;
import com.fossil.kn6;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn6 extends pv5 implements t47.g, in6.b {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public kn6 g;
    @DexIgnore
    public g37<lc5> h;
    @DexIgnore
    public jr4 i;
    @DexIgnore
    public in6 j;
    @DexIgnore
    public po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final fn6 a(String str) {
            pq7.c(str, "workoutSessionId");
            fn6 fn6 = new fn6();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WORKOUT_SESSION_ID", str);
            fn6.setArguments(bundle);
            return fn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements jr4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fn6 f1159a;

        @DexIgnore
        public b(fn6 fn6) {
            this.f1159a = fn6;
        }

        @DexIgnore
        @Override // com.fossil.jr4.a
        public void a(oi5 oi5) {
            pq7.c(oi5, "workoutWrapperType");
            fn6.L6(this.f1159a).s(oi5);
            fn6.N6(this.f1159a).o(oi5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fn6 b;

        @DexIgnore
        public c(fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            in6 in6 = this.b.j;
            if (in6 != null) {
                String c = um5.c(PortfolioApp.h0.c(), 2131886729);
                pq7.b(c, "LanguageHelper.getString\u2026ctivity_Title__StartTime)");
                in6.setTitle(c);
            }
            in6 in62 = this.b.j;
            if (in62 != null) {
                in62.M6(hi5.START_TIME);
            }
            in6 in63 = this.b.j;
            if (in63 != null) {
                cl7<ii5, ii5> i = fn6.L6(this.b).i();
                in63.N6(i != null ? i.getFirst() : null);
            }
            in6 in64 = this.b.j;
            if (in64 != null) {
                cl7<ii5, ii5> i2 = fn6.L6(this.b).i();
                in64.P6(i2 != null ? i2.getSecond() : null);
            }
            in6 in65 = this.b.j;
            if (in65 != null) {
                in65.Q6(fn6.L6(this.b).k());
            }
            in6 in66 = this.b.j;
            if (in66 != null) {
                in66.R6(fn6.L6(this.b).l());
            }
            in6 in67 = this.b.j;
            if (in67 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                in67.show(childFragmentManager, nn6.v.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fn6 b;

        @DexIgnore
        public d(fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            in6 in6 = this.b.j;
            if (in6 != null) {
                String c = um5.c(PortfolioApp.h0.c(), 2131886727);
                pq7.b(c, "LanguageHelper.getString\u2026Activity_Label__Duration)");
                in6.setTitle(c);
            }
            in6 in62 = this.b.j;
            if (in62 != null) {
                in62.M6(hi5.DURATION);
            }
            in6 in63 = this.b.j;
            if (in63 != null) {
                cl7<ii5, ii5> h = fn6.L6(this.b).h();
                in63.N6(h != null ? h.getFirst() : null);
            }
            in6 in64 = this.b.j;
            if (in64 != null) {
                cl7<ii5, ii5> h2 = fn6.L6(this.b).h();
                in64.P6(h2 != null ? h2.getSecond() : null);
            }
            in6 in65 = this.b.j;
            if (in65 != null) {
                in65.S6(fn6.L6(this.b).m());
            }
            in6 in66 = this.b.j;
            if (in66 != null) {
                in66.Q6(fn6.L6(this.b).k());
            }
            in6 in67 = this.b.j;
            if (in67 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                in67.show(childFragmentManager, nn6.v.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fn6 b;

        @DexIgnore
        public e(fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.V6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fn6 b;

        @DexIgnore
        public f(fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<kn6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fn6 f1160a;

        @DexIgnore
        public g(fn6 fn6) {
            this.f1160a = fn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(kn6.a aVar) {
            WorkoutSession d = aVar.d();
            if (d != null) {
                this.f1160a.U6(d);
            }
            Boolean f = aVar.f();
            if (f != null) {
                this.f1160a.W6(f.booleanValue());
            }
            if (aVar.a()) {
                this.f1160a.m();
            } else {
                this.f1160a.k();
            }
            Boolean c = aVar.c();
            if (c != null && c.booleanValue()) {
                this.f1160a.e0();
            }
            cl7<Integer, String> b = aVar.b();
            if (b != null) {
                this.f1160a.o(b.getFirst().intValue(), b.getSecond());
            }
            List<oi5> e = aVar.e();
            if (e != null) {
                fn6.N6(this.f1160a).m(e);
            }
        }
    }

    /*
    static {
        String simpleName = fn6.class.getSimpleName();
        pq7.b(simpleName, "WorkoutEditFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ kn6 L6(fn6 fn6) {
        kn6 kn6 = fn6.g;
        if (kn6 != null) {
            return kn6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ jr4 N6(fn6 fn6) {
        jr4 jr4 = fn6.i;
        if (jr4 != null) {
            return jr4;
        }
        pq7.n("mWorkoutTypeAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        kn6 kn6 = this.g;
        if (kn6 == null) {
            pq7.n("mViewModel");
            throw null;
        } else if (kn6.n()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
            return true;
        } else {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return true;
            }
            activity.finish();
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        pq7.c(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).R5(str, i2, intent);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363291) {
            e0();
        } else if (i2 == 2131363373) {
            V6();
        }
    }

    @DexIgnore
    public final void U6(WorkoutSession workoutSession) {
        Integer second;
        Integer second2;
        g37<lc5> g37 = this.h;
        if (g37 != null) {
            lc5 a2 = g37.a();
            if (a2 != null) {
                DateTime editedEndTime = workoutSession.getEditedEndTime();
                if (editedEndTime != null) {
                    long millis = editedEndTime.getMillis();
                    DateTime editedStartTime = workoutSession.getEditedStartTime();
                    if (editedStartTime != null) {
                        gl7<Integer, Integer, Integer> e0 = lk5.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                        pq7.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                        oi5 d2 = oi5.Companion.d(workoutSession.getEditedType(), workoutSession.getEditedMode());
                        jr4 jr4 = this.i;
                        if (jr4 != null) {
                            jr4.o(d2);
                            FlexibleTextView flexibleTextView = a2.A;
                            pq7.b(flexibleTextView, "binding.tvStartTimeValue");
                            DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                            flexibleTextView.setText(lk5.i0(editedStartTime2 != null ? editedStartTime2.toDate() : null));
                            Integer first = e0.getFirst();
                            if (first != null && first.intValue() == 0 && ((second = e0.getSecond()) == null || second.intValue() != 0)) {
                                FlexibleTextView flexibleTextView2 = a2.y;
                                pq7.b(flexibleTextView2, "binding.tvDurationValue");
                                hr7 hr7 = hr7.f1520a;
                                String string = getResources().getString(2131886697);
                                pq7.b(string, "resources.getString(R.st\u2026ilPage_Label__NumberMins)");
                                String format = String.format(string, Arrays.copyOf(new Object[]{e0.getSecond()}, 1));
                                pq7.b(format, "java.lang.String.format(format, *args)");
                                flexibleTextView2.setText(format);
                            } else {
                                Integer first2 = e0.getFirst();
                                if ((first2 != null && first2.intValue() == 0) || (second2 = e0.getSecond()) == null || second2.intValue() != 0) {
                                    FlexibleTextView flexibleTextView3 = a2.y;
                                    pq7.b(flexibleTextView3, "binding.tvDurationValue");
                                    hr7 hr72 = hr7.f1520a;
                                    String string2 = getResources().getString(2131886655);
                                    pq7.b(string2, "resources.getString(R.st\u2026bel__NumberHrsNumberMins)");
                                    String format2 = String.format(string2, Arrays.copyOf(new Object[]{e0.getFirst(), e0.getSecond()}, 2));
                                    pq7.b(format2, "java.lang.String.format(format, *args)");
                                    flexibleTextView3.setText(format2);
                                } else {
                                    FlexibleTextView flexibleTextView4 = a2.y;
                                    pq7.b(flexibleTextView4, "binding.tvDurationValue");
                                    hr7 hr73 = hr7.f1520a;
                                    String string3 = getResources().getString(2131886203);
                                    pq7.b(string3, "resources.getString(R.st\u2026enge_Label__StartInHours)");
                                    String format3 = String.format(string3, Arrays.copyOf(new Object[]{e0.getFirst()}, 1));
                                    pq7.b(format3, "java.lang.String.format(format, *args)");
                                    flexibleTextView4.setText(format3);
                                }
                            }
                            jr4 jr42 = this.i;
                            if (jr42 != null) {
                                int i2 = jr42.i(d2);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str = m;
                                local.d(str, "receiveWorkoutSession scrollTo " + i2);
                                if (i2 >= 0) {
                                    a2.v.scrollToPosition(i2);
                                    return;
                                }
                                return;
                            }
                            pq7.n("mWorkoutTypeAdapter");
                            throw null;
                        }
                        pq7.n("mWorkoutTypeAdapter");
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6() {
        kn6 kn6 = this.g;
        if (kn6 != null) {
            kn6.u();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void W6(boolean z) {
        g37<lc5> g37 = this.h;
        if (g37 != null) {
            lc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                pq7.b(flexibleButton, "it.fbSave");
                flexibleButton.setEnabled(z);
                if (z) {
                    a2.s.d("flexible_button_primary");
                } else {
                    a2.s.d("flexible_button_disabled");
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void k() {
        a();
    }

    @DexIgnore
    public final void m() {
        b();
    }

    @DexIgnore
    public final void o(int i2, String str) {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        lc5 lc5 = (lc5) aq0.f(LayoutInflater.from(getContext()), 2131558642, null, false, A6());
        PortfolioApp.h0.c().M().W().a(this);
        po4 po4 = this.k;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(kn6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
            this.g = (kn6) a2;
            in6 in6 = (in6) getChildFragmentManager().Z(in6.G.a());
            this.j = in6;
            if (in6 == null) {
                this.j = in6.G.b();
            }
            in6 in62 = this.j;
            if (in62 != null) {
                in62.O6(this);
                lc5.A.setOnClickListener(new c(this));
                lc5.y.setOnClickListener(new d(this));
                lc5.s.setOnClickListener(new e(this));
                lc5.t.setOnClickListener(new f(this));
                jr4 jr4 = new jr4(null, null, 3, null);
                jr4.n(new b(this));
                this.i = jr4;
                RecyclerView recyclerView = lc5.v;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                jr4 jr42 = this.i;
                if (jr42 != null) {
                    recyclerView.setAdapter(jr42);
                    kn6 kn6 = this.g;
                    if (kn6 != null) {
                        kn6.j().h(getViewLifecycleOwner(), new g(this));
                        this.h = new g37<>(this, lc5);
                        pq7.b(lc5, "binding");
                        return lc5.n();
                    }
                    pq7.n("mViewModel");
                    throw null;
                }
                pq7.n("mWorkoutTypeAdapter");
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        kn6 kn6 = this.g;
        if (kn6 != null) {
            Bundle arguments = getArguments();
            String string = arguments != null ? arguments.getString("EXTRA_WORKOUT_SESSION_ID") : null;
            if (string != null) {
                pq7.b(string, "arguments?.getString(EXTRA_WORKOUT_SESSION_ID)!!");
                kn6.t(string);
                vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.in6.b
    public void z4(hi5 hi5, int i2, Integer num, float f2, Float f3) {
        int i3 = 0;
        pq7.c(hi5, "editMode");
        int i4 = gn6.f1337a[hi5.ordinal()];
        if (i4 == 1) {
            kn6 kn6 = this.g;
            if (kn6 != null) {
                if (num != null) {
                    i3 = num.intValue();
                }
                kn6.r(i2, i3);
                return;
            }
            pq7.n("mViewModel");
            throw null;
        } else if (i4 == 2) {
            kn6 kn62 = this.g;
            if (kn62 != null) {
                if (num != null) {
                    i3 = num.intValue();
                }
                kn62.q(i2, i3);
                return;
            }
            pq7.n("mViewModel");
            throw null;
        } else if (i4 == 3) {
            kn6 kn63 = this.g;
            if (kn63 != null) {
                float f4 = (float) i2;
                if (num != null) {
                    i3 = num.intValue();
                }
                float f5 = (float) i3;
                if (f3 != null) {
                    kn63.p((double) dl5.h((f5 * f3.floatValue()) + f4, 1));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else if (i4 == 4) {
            kn6 kn64 = this.g;
            if (kn64 != null) {
                kn64.o((int) (((float) i2) * f2));
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }
}
