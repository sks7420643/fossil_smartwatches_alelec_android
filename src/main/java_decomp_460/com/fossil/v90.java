package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum v90 {
    SIMPLE_MOVEMENT((byte) 1);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public v90(byte b2) {
        this.b = (byte) b2;
    }
}
