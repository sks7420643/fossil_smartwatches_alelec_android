package com.fossil;

import com.fossil.a34;
import com.fossil.f64;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c f567a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e64 {
        @DexIgnore
        public /* final */ /* synthetic */ Map b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public a(Map map, Type type) {
            this.b = map;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void b(Class<?> cls) {
            if (!(this.c instanceof WildcardType)) {
                throw new IllegalArgumentException("No type mapping from " + cls + " to " + this.c);
            }
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void c(GenericArrayType genericArrayType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                Type j = f64.j(type);
                i14.h(j != null, "%s is not an array type.", this.c);
                c64.f(this.b, genericArrayType.getGenericComponentType(), j);
            }
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void d(ParameterizedType parameterizedType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                ParameterizedType parameterizedType2 = (ParameterizedType) c64.e(ParameterizedType.class, type);
                if (!(parameterizedType.getOwnerType() == null || parameterizedType2.getOwnerType() == null)) {
                    c64.f(this.b, parameterizedType.getOwnerType(), parameterizedType2.getOwnerType());
                }
                i14.i(parameterizedType.getRawType().equals(parameterizedType2.getRawType()), "Inconsistent raw type: %s vs. %s", parameterizedType, this.c);
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                i14.i(actualTypeArguments.length == actualTypeArguments2.length, "%s not compatible with %s", parameterizedType, parameterizedType2);
                for (int i = 0; i < actualTypeArguments.length; i++) {
                    c64.f(this.b, actualTypeArguments[i], actualTypeArguments2[i]);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void e(TypeVariable<?> typeVariable) {
            this.b.put(new d(typeVariable), this.c);
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void f(WildcardType wildcardType) {
            Type type = this.c;
            if (type instanceof WildcardType) {
                WildcardType wildcardType2 = (WildcardType) type;
                Type[] upperBounds = wildcardType.getUpperBounds();
                Type[] upperBounds2 = wildcardType2.getUpperBounds();
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] lowerBounds2 = wildcardType2.getLowerBounds();
                i14.i(upperBounds.length == upperBounds2.length && lowerBounds.length == lowerBounds2.length, "Incompatible type: %s vs. %s", wildcardType, this.c);
                for (int i = 0; i < upperBounds.length; i++) {
                    c64.f(this.b, upperBounds[i], upperBounds2[i]);
                }
                for (int i2 = 0; i2 < lowerBounds.length; i2++) {
                    c64.f(this.b, lowerBounds[i2], lowerBounds2[i2]);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends e64 {
        @DexIgnore
        public static /* final */ e c; // = new e(null);
        @DexIgnore
        public /* final */ Map<d, Type> b; // = x34.j();

        @DexIgnore
        public static a34<d, Type> g(Type type) {
            b bVar = new b();
            bVar.a(c.a(type));
            return a34.copyOf(bVar.b);
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void b(Class<?> cls) {
            a(cls.getGenericSuperclass());
            a(cls.getGenericInterfaces());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void d(ParameterizedType parameterizedType) {
            Class cls = (Class) parameterizedType.getRawType();
            TypeVariable[] typeParameters = cls.getTypeParameters();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            i14.s(typeParameters.length == actualTypeArguments.length);
            for (int i = 0; i < typeParameters.length; i++) {
                h(new d(typeParameters[i]), actualTypeArguments[i]);
            }
            a(cls);
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void e(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public final void h(d dVar, Type type) {
            if (!this.b.containsKey(dVar)) {
                Type type2 = type;
                while (type2 != null) {
                    if (dVar.a(type2)) {
                        while (type != null) {
                            type = this.b.remove(d.c(type));
                        }
                        return;
                    }
                    type2 = this.b.get(d.c(type2));
                }
                this.b.put(dVar, type);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ a34<d, Type> f568a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends c {
            @DexIgnore
            public /* final */ /* synthetic */ TypeVariable b;
            @DexIgnore
            public /* final */ /* synthetic */ c c;

            @DexIgnore
            public a(c cVar, TypeVariable typeVariable, c cVar2) {
                this.b = typeVariable;
                this.c = cVar2;
            }

            @DexIgnore
            @Override // com.fossil.c64.c
            public Type b(TypeVariable<?> typeVariable, c cVar) {
                return typeVariable.getGenericDeclaration().equals(this.b.getGenericDeclaration()) ? typeVariable : this.c.b(typeVariable, cVar);
            }
        }

        @DexIgnore
        public c() {
            this.f568a = a34.of();
        }

        @DexIgnore
        public c(a34<d, Type> a34) {
            this.f568a = a34;
        }

        @DexIgnore
        public final Type a(TypeVariable<?> typeVariable) {
            return b(typeVariable, new a(this, typeVariable, this));
        }

        @DexIgnore
        public Type b(TypeVariable<?> typeVariable, c cVar) {
            Type type = this.f568a.get(new d(typeVariable));
            if (type != null) {
                return new c64(cVar, null).i(type);
            }
            Type[] bounds = typeVariable.getBounds();
            if (bounds.length == 0) {
                return typeVariable;
            }
            Type[] j = new c64(cVar, null).j(bounds);
            return (!f64.f.f1058a || !Arrays.equals(bounds, j)) ? f64.l(typeVariable.getGenericDeclaration(), typeVariable.getName(), j) : typeVariable;
        }

        @DexIgnore
        public final c c(Map<d, ? extends Type> map) {
            a34.b builder = a34.builder();
            builder.f(this.f568a);
            for (Map.Entry<d, ? extends Type> entry : map.entrySet()) {
                d key = entry.getKey();
                Type type = (Type) entry.getValue();
                i14.h(!key.a(type), "Type variable %s bound to itself", key);
                builder.c(key, type);
            }
            return new c(builder.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TypeVariable<?> f569a;

        @DexIgnore
        public d(TypeVariable<?> typeVariable) {
            i14.l(typeVariable);
            this.f569a = typeVariable;
        }

        @DexIgnore
        public static d c(Type type) {
            if (type instanceof TypeVariable) {
                return new d((TypeVariable) type);
            }
            return null;
        }

        @DexIgnore
        public boolean a(Type type) {
            if (type instanceof TypeVariable) {
                return b((TypeVariable) type);
            }
            return false;
        }

        @DexIgnore
        public final boolean b(TypeVariable<?> typeVariable) {
            return this.f569a.getGenericDeclaration().equals(typeVariable.getGenericDeclaration()) && this.f569a.getName().equals(typeVariable.getName());
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return b(((d) obj).f569a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return f14.b(this.f569a.getGenericDeclaration(), this.f569a.getName());
        }

        @DexIgnore
        public String toString() {
            return this.f569a.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicInteger f570a;

        @DexIgnore
        public e() {
            this.f570a = new AtomicInteger();
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }

        @DexIgnore
        public Type a(Type type) {
            i14.l(type);
            if ((type instanceof Class) || (type instanceof TypeVariable)) {
                return type;
            }
            if (type instanceof GenericArrayType) {
                return f64.k(a(((GenericArrayType) type).getGenericComponentType()));
            }
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                return f64.n(c(parameterizedType.getOwnerType()), (Class) parameterizedType.getRawType(), b(parameterizedType.getActualTypeArguments()));
            } else if (type instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) type;
                if (wildcardType.getLowerBounds().length != 0) {
                    return type;
                }
                Type[] upperBounds = wildcardType.getUpperBounds();
                return f64.l(e.class, "capture#" + this.f570a.incrementAndGet() + "-of ? extends " + d14.h('&').g(upperBounds), wildcardType.getUpperBounds());
            } else {
                throw new AssertionError("must have been one of the known types");
            }
        }

        @DexIgnore
        public final Type[] b(Type[] typeArr) {
            Type[] typeArr2 = new Type[typeArr.length];
            for (int i = 0; i < typeArr.length; i++) {
                typeArr2[i] = a(typeArr[i]);
            }
            return typeArr2;
        }

        @DexIgnore
        public final Type c(Type type) {
            if (type == null) {
                return null;
            }
            return a(type);
        }
    }

    @DexIgnore
    public c64() {
        this.f567a = new c();
    }

    @DexIgnore
    public c64(c cVar) {
        this.f567a = cVar;
    }

    @DexIgnore
    public /* synthetic */ c64(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public static c64 d(Type type) {
        return new c64().m(b.g(type));
    }

    @DexIgnore
    public static <T> T e(Class<T> cls, Object obj) {
        try {
            return cls.cast(obj);
        } catch (ClassCastException e2) {
            throw new IllegalArgumentException(obj + " is not a " + cls.getSimpleName());
        }
    }

    @DexIgnore
    public static void f(Map<d, Type> map, Type type, Type type2) {
        if (!type.equals(type2)) {
            new a(map, type2).a(type);
        }
    }

    @DexIgnore
    public final Type g(GenericArrayType genericArrayType) {
        return f64.k(i(genericArrayType.getGenericComponentType()));
    }

    @DexIgnore
    public final ParameterizedType h(ParameterizedType parameterizedType) {
        Type ownerType = parameterizedType.getOwnerType();
        return f64.n(ownerType == null ? null : i(ownerType), (Class) i(parameterizedType.getRawType()), j(parameterizedType.getActualTypeArguments()));
    }

    @DexIgnore
    public Type i(Type type) {
        i14.l(type);
        return type instanceof TypeVariable ? this.f567a.a((TypeVariable) type) : type instanceof ParameterizedType ? h((ParameterizedType) type) : type instanceof GenericArrayType ? g((GenericArrayType) type) : type instanceof WildcardType ? k((WildcardType) type) : type;
    }

    @DexIgnore
    public final Type[] j(Type[] typeArr) {
        Type[] typeArr2 = new Type[typeArr.length];
        for (int i = 0; i < typeArr.length; i++) {
            typeArr2[i] = i(typeArr[i]);
        }
        return typeArr2;
    }

    @DexIgnore
    public final WildcardType k(WildcardType wildcardType) {
        return new f64.j(j(wildcardType.getLowerBounds()), j(wildcardType.getUpperBounds()));
    }

    @DexIgnore
    public c64 l(Type type, Type type2) {
        HashMap j = x34.j();
        i14.l(type);
        i14.l(type2);
        f(j, type, type2);
        return m(j);
    }

    @DexIgnore
    public c64 m(Map<d, ? extends Type> map) {
        return new c64(this.f567a.c(map));
    }
}
