package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i74 extends x64 {
    @DexIgnore
    public static /* final */ mg4<Set<Object>> e; // = h74.a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<a74<?>, p74<?>> f1590a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, p74<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, p74<Set<?>>> c; // = new HashMap();
    @DexIgnore
    public /* final */ o74 d;

    @DexIgnore
    public i74(Executor executor, Iterable<e74> iterable, a74<?>... a74Arr) {
        this.d = new o74(executor);
        ArrayList<a74<?>> arrayList = new ArrayList();
        arrayList.add(a74.n(this.d, o74.class, ge4.class, fe4.class));
        for (e74 e74 : iterable) {
            arrayList.addAll(e74.getComponents());
        }
        for (a74<?> a74 : a74Arr) {
            if (a74 != null) {
                arrayList.add(a74);
            }
        }
        j74.a(arrayList);
        for (a74<?> a742 : arrayList) {
            this.f1590a.put(a742, new p74<>(f74.a(this, a742)));
        }
        g();
        h();
    }

    @DexIgnore
    public static /* synthetic */ Set f(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((p74) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    @Override // com.fossil.b74
    public <T> mg4<T> a(Class<T> cls) {
        r74.c(cls, "Null interface requested.");
        return this.b.get(cls);
    }

    @DexIgnore
    @Override // com.fossil.b74
    public <T> mg4<Set<T>> b(Class<T> cls) {
        p74<Set<?>> p74 = this.c.get(cls);
        return p74 != null ? p74 : (mg4<Set<T>>) e;
    }

    @DexIgnore
    public void d(boolean z) {
        for (Map.Entry<a74<?>, p74<?>> entry : this.f1590a.entrySet()) {
            a74<?> key = entry.getKey();
            p74<?> value = entry.getValue();
            if (key.i() || (key.j() && z)) {
                value.get();
            }
        }
        this.d.c();
    }

    @DexIgnore
    public final void g() {
        for (Map.Entry<a74<?>, p74<?>> entry : this.f1590a.entrySet()) {
            a74<?> key = entry.getKey();
            if (key.k()) {
                p74<?> value = entry.getValue();
                for (Class<? super Object> cls : key.e()) {
                    this.b.put(cls, value);
                }
            }
        }
        i();
    }

    @DexIgnore
    public final void h() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<a74<?>, p74<?>> entry : this.f1590a.entrySet()) {
            a74<?> key = entry.getKey();
            if (!key.k()) {
                p74<?> value = entry.getValue();
                for (Class<? super Object> cls : key.e()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(value);
                }
            }
        }
        for (Map.Entry entry2 : hashMap.entrySet()) {
            this.c.put((Class) entry2.getKey(), new p74<>(g74.a((Set) entry2.getValue())));
        }
    }

    @DexIgnore
    public final void i() {
        for (a74<?> a74 : this.f1590a.keySet()) {
            Iterator<k74> it = a74.c().iterator();
            while (true) {
                if (it.hasNext()) {
                    k74 next = it.next();
                    if (next.c() && !this.b.containsKey(next.a())) {
                        throw new q74(String.format("Unsatisfied dependency for component %s: %s", a74, next.a()));
                    }
                }
            }
        }
    }
}
