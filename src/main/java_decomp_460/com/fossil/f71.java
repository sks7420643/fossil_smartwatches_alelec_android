package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.c71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f71 implements c71 {
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ s61 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ej0<String, c71.b> {
        @DexIgnore
        public /* final */ /* synthetic */ f71 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(f71 f71, int i2, int i3) {
            super(i3);
            this.i = f71;
        }

        @DexIgnore
        /* renamed from: l */
        public void b(boolean z, String str, c71.b bVar, c71.b bVar2) {
            pq7.c(str, "key");
            pq7.c(bVar, "oldValue");
            this.i.c.a(bVar.a());
        }

        @DexIgnore
        /* renamed from: m */
        public int j(String str, c71.b bVar) {
            pq7.c(str, "key");
            pq7.c(bVar, "value");
            return bVar.b();
        }
    }

    @DexIgnore
    public f71(s61 s61, int i) {
        pq7.c(s61, "referenceCounter");
        this.c = s61;
        this.b = new a(this, i, i);
    }

    @DexIgnore
    @Override // com.fossil.c71
    public void a(int i) {
        if (q81.c.a() && q81.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "trimMemory, level=" + i);
        }
        if (i >= 40) {
            e();
        } else if (10 <= i && 20 > i) {
            this.b.k(g() / 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.c71
    public c71.b b(String str) {
        pq7.c(str, "key");
        return (c71.b) this.b.d(str);
    }

    @DexIgnore
    @Override // com.fossil.c71
    public void c(String str, Bitmap bitmap, boolean z) {
        pq7.c(str, "key");
        pq7.c(bitmap, "value");
        int b2 = w81.b(bitmap);
        if (b2 > f()) {
            this.b.g(str);
            return;
        }
        this.c.b(bitmap);
        this.b.f(str, new c71.b(bitmap, z, b2));
    }

    @DexIgnore
    public void e() {
        if (q81.c.a() && q81.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "clearMemory");
        }
        this.b.k(-1);
    }

    @DexIgnore
    public int f() {
        return this.b.e();
    }

    @DexIgnore
    public int g() {
        return this.b.i();
    }
}
