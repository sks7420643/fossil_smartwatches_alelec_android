package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rf7 extends ef7 {
    @DexIgnore
    public rf7(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.ef7
    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.getString("_wxapi_payresp_prepayid");
        bundle.getString("_wxapi_payresp_returnkey");
        bundle.getString("_wxapi_payresp_extdata");
    }

    @DexIgnore
    @Override // com.fossil.ef7
    public int b() {
        return 5;
    }
}
