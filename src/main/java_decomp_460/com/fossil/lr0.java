package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr0 implements Closeable, iv7 {
    @DexIgnore
    public /* final */ tn7 b;

    @DexIgnore
    public lr0(tn7 tn7) {
        pq7.c(tn7, "context");
        this.b = tn7;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        dx7.d(h(), null, 1, null);
    }

    @DexIgnore
    @Override // com.fossil.iv7
    public tn7 h() {
        return this.b;
    }
}
