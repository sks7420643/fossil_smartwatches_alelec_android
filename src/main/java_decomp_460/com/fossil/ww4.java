package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww4 implements Factory<vw4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<zt4> f4006a;
    @DexIgnore
    public /* final */ Provider<tt4> b;

    @DexIgnore
    public ww4(Provider<zt4> provider, Provider<tt4> provider2) {
        this.f4006a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ww4 a(Provider<zt4> provider, Provider<tt4> provider2) {
        return new ww4(provider, provider2);
    }

    @DexIgnore
    public static vw4 c(zt4 zt4, tt4 tt4) {
        return new vw4(zt4, tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public vw4 get() {
        return c(this.f4006a.get(), this.b.get());
    }
}
