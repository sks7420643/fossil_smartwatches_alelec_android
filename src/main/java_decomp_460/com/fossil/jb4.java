package com.fossil;

import com.fossil.q18;
import com.fossil.s18;
import com.fossil.v18;
import com.fossil.z08;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jb4 {
    @DexIgnore
    public static /* final */ OkHttpClient f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ib4 f1736a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ Map<String, String> d;
    @DexIgnore
    public s18.a e; // = null;

    /*
    static {
        OkHttpClient.b y = new OkHttpClient().y();
        y.f(ButtonService.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        f = y.d();
    }
    */

    @DexIgnore
    public jb4(ib4 ib4, String str, Map<String, String> map) {
        this.f1736a = ib4;
        this.b = str;
        this.c = map;
        this.d = new HashMap();
    }

    @DexIgnore
    public final v18 a() {
        v18.a aVar = new v18.a();
        z08.a aVar2 = new z08.a();
        aVar2.c();
        aVar.c(aVar2.a());
        q18.a p = q18.r(this.b).p();
        for (Map.Entry<String, String> entry : this.c.entrySet()) {
            p.a(entry.getKey(), entry.getValue());
        }
        aVar.l(p.c());
        for (Map.Entry<String, String> entry2 : this.d.entrySet()) {
            aVar.e(entry2.getKey(), entry2.getValue());
        }
        s18.a aVar3 = this.e;
        aVar.g(this.f1736a.name(), aVar3 == null ? null : aVar3.e());
        return aVar.b();
    }

    @DexIgnore
    public lb4 b() throws IOException {
        return lb4.c(f.d(a()).a());
    }

    @DexIgnore
    public final s18.a c() {
        if (this.e == null) {
            s18.a aVar = new s18.a();
            aVar.f(s18.f);
            this.e = aVar;
        }
        return this.e;
    }

    @DexIgnore
    public jb4 d(String str, String str2) {
        this.d.put(str, str2);
        return this;
    }

    @DexIgnore
    public jb4 e(Map.Entry<String, String> entry) {
        d(entry.getKey(), entry.getValue());
        return this;
    }

    @DexIgnore
    public String f() {
        return this.f1736a.name();
    }

    @DexIgnore
    public jb4 g(String str, String str2) {
        s18.a c2 = c();
        c2.a(str, str2);
        this.e = c2;
        return this;
    }

    @DexIgnore
    public jb4 h(String str, String str2, String str3, File file) {
        RequestBody c2 = RequestBody.c(r18.d(str3), file);
        s18.a c3 = c();
        c3.b(str, str2, c2);
        this.e = c3;
        return this;
    }
}
