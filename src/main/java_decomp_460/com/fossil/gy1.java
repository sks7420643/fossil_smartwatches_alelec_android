package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gy1 {
    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONObject jSONObject2, boolean z) {
        pq7.c(jSONObject, "$this$add");
        pq7.c(jSONObject2, FacebookRequestErrorClassification.KEY_OTHER);
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (z || !jSONObject.has(next)) {
                try {
                    jSONObject.putOpt(next, jSONObject2.get(next));
                } catch (JSONException e) {
                }
            }
        }
        return jSONObject;
    }

    @DexIgnore
    public static /* synthetic */ JSONObject b(JSONObject jSONObject, JSONObject jSONObject2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = true;
        }
        a(jSONObject, jSONObject2, z);
        return jSONObject;
    }

    @DexIgnore
    public static final JSONObject c(JSONObject jSONObject, JSONObject jSONObject2) {
        pq7.c(jSONObject, "$this$combine");
        pq7.c(jSONObject2, FacebookRequestErrorClassification.KEY_OTHER);
        JSONObject jSONObject3 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                jSONObject3.putOpt(next, jSONObject.get(next));
            } catch (JSONException e) {
            }
        }
        Iterator<String> keys2 = jSONObject2.keys();
        while (keys2.hasNext()) {
            String next2 = keys2.next();
            try {
                jSONObject3.putOpt(next2, jSONObject2.get(next2));
            } catch (JSONException e2) {
            }
        }
        return jSONObject3;
    }

    @DexIgnore
    public static final JSONObject d(JSONObject jSONObject, Enum<?> r3, Object obj) {
        pq7.c(jSONObject, "$this$put");
        pq7.c(r3, "key");
        JSONObject put = jSONObject.put(ey1.a(r3), obj);
        pq7.b(put, "this.put(key.lowerCaseName, value)");
        return put;
    }
}
