package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicReference<byte[]> f4480a; // = new AtomicReference<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends InputStream {
        @DexIgnore
        public /* final */ ByteBuffer b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public a(ByteBuffer byteBuffer) {
            this.b = byteBuffer;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            return this.b.remaining();
        }

        @DexIgnore
        public void mark(int i) {
            synchronized (this) {
                this.c = this.b.position();
            }
        }

        @DexIgnore
        public boolean markSupported() {
            return true;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            if (!this.b.hasRemaining()) {
                return -1;
            }
            return this.b.get() & 255;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            if (!this.b.hasRemaining()) {
                return -1;
            }
            int min = Math.min(i2, available());
            this.b.get(bArr, i, min);
            return min;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public void reset() throws IOException {
            synchronized (this) {
                if (this.c != -1) {
                    this.b.position(this.c);
                } else {
                    throw new IOException("Cannot reset to unset mark position");
                }
            }
        }

        @DexIgnore
        @Override // java.io.InputStream
        public long skip(long j) throws IOException {
            if (!this.b.hasRemaining()) {
                return -1;
            }
            long min = Math.min(j, (long) available());
            ByteBuffer byteBuffer = this.b;
            byteBuffer.position((int) (((long) byteBuffer.position()) + min));
            return min;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4481a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ byte[] c;

        @DexIgnore
        public b(byte[] bArr, int i, int i2) {
            this.c = bArr;
            this.f4481a = i;
            this.b = i2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[SYNTHETIC, Splitter:B:21:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003d A[SYNTHETIC, Splitter:B:24:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.nio.ByteBuffer a(java.io.File r8) throws java.io.IOException {
        /*
            r6 = 0
            r2 = 0
            long r4 = r8.length()     // Catch:{ all -> 0x0049 }
            r0 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x004e
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x0041
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0049 }
            java.lang.String r0 = "r"
            r6.<init>(r8, r0)     // Catch:{ all -> 0x0049 }
            java.nio.channels.FileChannel r0 = r6.getChannel()     // Catch:{ all -> 0x0032 }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x005e }
            r2 = 0
            java.nio.MappedByteBuffer r1 = r0.map(r1, r2, r4)     // Catch:{ all -> 0x005e }
            java.nio.MappedByteBuffer r1 = r1.load()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x002e
            r0.close()     // Catch:{ IOException -> 0x0056 }
        L_0x002e:
            r6.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0031:
            return r1
        L_0x0032:
            r0 = move-exception
            r1 = r0
        L_0x0034:
            r3 = r6
            r4 = r2
        L_0x0036:
            if (r4 == 0) goto L_0x003b
            r4.close()     // Catch:{ IOException -> 0x005a }
        L_0x003b:
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ IOException -> 0x005c }
        L_0x0040:
            throw r1
        L_0x0041:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "File unsuitable for memory mapping"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            r0 = move-exception
            r3 = r2
            r4 = r2
            r1 = r0
            goto L_0x0036
        L_0x004e:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "File too large to map into memory"
            r0.<init>(r1)
            throw r0
        L_0x0056:
            r0 = move-exception
            goto L_0x002e
        L_0x0058:
            r0 = move-exception
            goto L_0x0031
        L_0x005a:
            r0 = move-exception
            goto L_0x003b
        L_0x005c:
            r0 = move-exception
            goto L_0x0040
        L_0x005e:
            r1 = move-exception
            r2 = r0
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zj1.a(java.io.File):java.nio.ByteBuffer");
    }

    @DexIgnore
    public static ByteBuffer b(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        byte[] andSet = f4480a.getAndSet(null);
        if (andSet == null) {
            andSet = new byte[16384];
        }
        while (true) {
            int read = inputStream.read(andSet);
            if (read >= 0) {
                byteArrayOutputStream.write(andSet, 0, read);
            } else {
                f4480a.set(andSet);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return (ByteBuffer) ByteBuffer.allocateDirect(byteArray.length).put(byteArray).position(0);
            }
        }
    }

    @DexIgnore
    public static b c(ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly() || !byteBuffer.hasArray()) {
            return null;
        }
        return new b(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.limit());
    }

    @DexIgnore
    public static byte[] d(ByteBuffer byteBuffer) {
        b c = c(byteBuffer);
        if (c != null && c.f4481a == 0 && c.b == c.c.length) {
            return byteBuffer.array();
        }
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        byte[] bArr = new byte[asReadOnlyBuffer.limit()];
        asReadOnlyBuffer.position(0);
        asReadOnlyBuffer.get(bArr);
        return bArr;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a A[SYNTHETIC, Splitter:B:13:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f A[SYNTHETIC, Splitter:B:16:0x002f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void e(java.nio.ByteBuffer r4, java.io.File r5) throws java.io.IOException {
        /*
            r0 = 0
            r1 = 0
            r4.position(r0)
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = "rw"
            r3.<init>(r5, r0)     // Catch:{ all -> 0x0033 }
            java.nio.channels.FileChannel r1 = r3.getChannel()     // Catch:{ all -> 0x0026 }
            r1.write(r4)     // Catch:{ all -> 0x0026 }
            r0 = 0
            r1.force(r0)     // Catch:{ all -> 0x0026 }
            r1.close()     // Catch:{ all -> 0x0026 }
            r3.close()     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0022:
            r3.close()     // Catch:{ IOException -> 0x0039 }
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
            r2 = r1
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x003d }
        L_0x0032:
            throw r0
        L_0x0033:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0028
        L_0x0037:
            r0 = move-exception
            goto L_0x0022
        L_0x0039:
            r0 = move-exception
            goto L_0x0025
        L_0x003b:
            r1 = move-exception
            goto L_0x002d
        L_0x003d:
            r1 = move-exception
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zj1.e(java.nio.ByteBuffer, java.io.File):void");
    }

    @DexIgnore
    public static InputStream f(ByteBuffer byteBuffer) {
        return new a(byteBuffer);
    }
}
