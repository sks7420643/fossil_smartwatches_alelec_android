package com.fossil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ExecutorService f3903a; // = new ThreadPoolExecutor(0, 4, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new tf2("GAC_Transform"));

    @DexIgnore
    public static ExecutorService a() {
        return f3903a;
    }
}
