package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba2 implements ft3<Boolean, Void> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.nt3] */
    @Override // com.fossil.ft3
    public final /* synthetic */ Void then(nt3<Boolean> nt3) throws Exception {
        if (nt3.m().booleanValue()) {
            return null;
        }
        throw new n62(new Status(13, "listener already unregistered"));
    }
}
