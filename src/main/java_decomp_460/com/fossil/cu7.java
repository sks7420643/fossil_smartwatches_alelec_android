package com.fossil;

import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu7<T> extends au7<T> {
    @DexIgnore
    public /* final */ Thread e;
    @DexIgnore
    public /* final */ hw7 f;

    @DexIgnore
    public cu7(tn7 tn7, Thread thread, hw7 hw7) {
        super(tn7, true);
        this.e = thread;
        this.f = hw7;
    }

    @DexIgnore
    public final T A0() {
        vu7 vu7 = null;
        xx7 a2 = yx7.a();
        if (a2 != null) {
            a2.c();
        }
        try {
            hw7 hw7 = this.f;
            if (hw7 != null) {
                hw7.i0(hw7, false, 1, null);
            }
            while (!Thread.interrupted()) {
                try {
                    hw7 hw72 = this.f;
                    long q0 = hw72 != null ? hw72.q0() : Long.MAX_VALUE;
                    if (U()) {
                        T t = (T) gx7.h(Q());
                        if (t instanceof vu7) {
                            vu7 = t;
                        }
                        vu7 vu72 = vu7;
                        if (vu72 == null) {
                            return t;
                        }
                        throw vu72.f3837a;
                    }
                    xx7 a3 = yx7.a();
                    if (a3 != null) {
                        a3.f(this, q0);
                    } else {
                        LockSupport.parkNanos(this, q0);
                    }
                } finally {
                    hw7 hw73 = this.f;
                    if (hw73 != null) {
                        hw7.T(hw73, false, 1, null);
                    }
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            s(interruptedException);
            throw interruptedException;
        } finally {
            xx7 a4 = yx7.a();
            if (a4 != null) {
                a4.g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public boolean V() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void p(Object obj) {
        if (!pq7.a(Thread.currentThread(), this.e)) {
            LockSupport.unpark(this.e);
        }
    }
}
