package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mc4 {
    @DexIgnore
    public static /* final */ cb4 b; // = new cb4();
    @DexIgnore
    public static /* final */ String c; // = d("hts/cahyiseot-agolai.o/1frlglgc/aclg", "tp:/rsltcrprsp.ogepscmv/ieo/eaybtho");
    @DexIgnore
    public static /* final */ String d; // = d("AzSBpY4F0rHiHFdinTvM", "IayrSTFL9eJ69YeSUO2");
    @DexIgnore
    public static /* final */ wy1<ta4, byte[]> e; // = lc4.a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xy1<ta4> f2357a;

    @DexIgnore
    public mc4(xy1<ta4> xy1, wy1<ta4, byte[]> wy1) {
        this.f2357a = xy1;
    }

    @DexIgnore
    public static mc4 a(Context context) {
        m02.f(context);
        return new mc4(m02.c().g(new az1(c, d)).b("FIREBASE_CRASHLYTICS_REPORT", ta4.class, ty1.b("json"), e), e);
    }

    @DexIgnore
    public static /* synthetic */ void b(ot3 ot3, z84 z84, Exception exc) {
        if (exc != null) {
            ot3.d(exc);
        } else {
            ot3.e(z84);
        }
    }

    @DexIgnore
    public static String d(String str, String str2) {
        int length = str.length() - str2.length();
        if (length < 0 || length > 1) {
            throw new IllegalArgumentException("Invalid input received");
        }
        StringBuilder sb = new StringBuilder(str.length() + str2.length());
        for (int i = 0; i < str.length(); i++) {
            sb.append(str.charAt(i));
            if (str2.length() > i) {
                sb.append(str2.charAt(i));
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public nt3<z84> e(z84 z84) {
        ta4 b2 = z84.b();
        ot3 ot3 = new ot3();
        this.f2357a.b(uy1.f(b2), kc4.b(ot3, z84));
        return ot3.a();
    }
}
