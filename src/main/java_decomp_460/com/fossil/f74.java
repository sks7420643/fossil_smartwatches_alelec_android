package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class f74 implements mg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i74 f1064a;
    @DexIgnore
    public /* final */ a74 b;

    @DexIgnore
    public f74(i74 i74, a74 a74) {
        this.f1064a = i74;
        this.b = a74;
    }

    @DexIgnore
    public static mg4 a(i74 i74, a74 a74) {
        return new f74(i74, a74);
    }

    @DexIgnore
    @Override // com.fossil.mg4
    public Object get() {
        a74 a74;
        return a74.d().a(new s74(this.b, this.f1064a));
    }
}
