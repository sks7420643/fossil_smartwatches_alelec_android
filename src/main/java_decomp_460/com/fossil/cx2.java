package com.fossil;

import java.util.Collection;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cx2<K, V> implements jy2<K, V> {
    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof jy2) {
            return zza().equals(((jy2) obj).zza());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return zza().hashCode();
    }

    @DexIgnore
    public String toString() {
        return zza().toString();
    }

    @DexIgnore
    @Override // com.fossil.jy2
    public abstract Map<K, Collection<V>> zza();

    @DexIgnore
    public boolean zza(@NullableDecl Object obj) {
        for (Collection<V> collection : zza().values()) {
            if (collection.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> zzb();
}
