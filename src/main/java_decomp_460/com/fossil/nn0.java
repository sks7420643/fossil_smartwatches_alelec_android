package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nn0<T> implements mn0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object[] f2548a;
    @DexIgnore
    public int b;

    @DexIgnore
    public nn0(int i) {
        if (i > 0) {
            this.f2548a = new Object[i];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }

    @DexIgnore
    @Override // com.fossil.mn0
    public boolean a(T t) {
        if (!c(t)) {
            int i = this.b;
            Object[] objArr = this.f2548a;
            if (i >= objArr.length) {
                return false;
            }
            objArr[i] = t;
            this.b = i + 1;
            return true;
        }
        throw new IllegalStateException("Already in the pool!");
    }

    @DexIgnore
    @Override // com.fossil.mn0
    public T b() {
        int i = this.b;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        Object[] objArr = this.f2548a;
        T t = (T) objArr[i2];
        objArr[i2] = null;
        this.b = i - 1;
        return t;
    }

    @DexIgnore
    public final boolean c(T t) {
        for (int i = 0; i < this.b; i++) {
            if (this.f2548a[i] == t) {
                return true;
            }
        }
        return false;
    }
}
