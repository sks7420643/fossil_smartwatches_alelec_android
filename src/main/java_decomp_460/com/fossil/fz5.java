package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz5 implements MembersInjector<ez5> {
    @DexIgnore
    public static void a(ez5 ez5, a46 a46) {
        ez5.m = a46;
    }

    @DexIgnore
    public static void b(ez5 ez5, f06 f06) {
        ez5.l = f06;
    }

    @DexIgnore
    public static void c(ez5 ez5, md6 md6) {
        ez5.h = md6;
    }

    @DexIgnore
    public static void d(ez5 ez5, z66 z66) {
        ez5.i = z66;
    }

    @DexIgnore
    public static void e(ez5 ez5, mb6 mb6) {
        ez5.j = mb6;
    }

    @DexIgnore
    public static void f(ez5 ez5, bo6 bo6) {
        ez5.k = bo6;
    }

    @DexIgnore
    public static void g(ez5 ez5, un6 un6) {
        ez5.s = un6;
    }
}
