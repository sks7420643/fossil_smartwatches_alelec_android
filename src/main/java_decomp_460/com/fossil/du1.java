package com.fossil;

import com.facebook.share.internal.VideoUploader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum du1 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final du1 a(String str) {
            du1[] values = du1.values();
            for (du1 du1 : values) {
                if (pq7.a(du1.a(), str)) {
                    return du1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public du1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
