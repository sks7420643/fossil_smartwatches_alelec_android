package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t51 {
    /*
    static {
        l48.Companion.d("GIF");
        l48.Companion.d("RIFF");
        l48.Companion.d("WEBP");
        l48.Companion.d("VP8X");
    }
    */

    @DexIgnore
    public static final int a(int i, int i2, int i3, int i4, e81 e81) {
        pq7.c(e81, "scale");
        int d = bs7.d(Integer.highestOneBit(i / i3), 1);
        int d2 = bs7.d(Integer.highestOneBit(i2 / i4), 1);
        int i5 = s51.f3205a[e81.ordinal()];
        if (i5 == 1) {
            return Math.min(d, d2);
        }
        if (i5 == 2) {
            return Math.max(d, d2);
        }
        throw new al7();
    }

    @DexIgnore
    public static final double b(double d, double d2, double d3, double d4, e81 e81) {
        pq7.c(e81, "scale");
        double d5 = d3 / d;
        double d6 = d4 / d2;
        int i = s51.d[e81.ordinal()];
        if (i == 1) {
            return Math.max(d5, d6);
        }
        if (i == 2) {
            return Math.min(d5, d6);
        }
        throw new al7();
    }

    @DexIgnore
    public static final double c(int i, int i2, int i3, int i4, e81 e81) {
        pq7.c(e81, "scale");
        double d = ((double) i3) / ((double) i);
        double d2 = ((double) i4) / ((double) i2);
        int i5 = s51.b[e81.ordinal()];
        if (i5 == 1) {
            return Math.max(d, d2);
        }
        if (i5 == 2) {
            return Math.min(d, d2);
        }
        throw new al7();
    }
}
