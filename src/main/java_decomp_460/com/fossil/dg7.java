package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dg7 extends Exception {
    @DexIgnore
    public dg7() {
    }

    @DexIgnore
    public dg7(String str) {
        super(str);
    }

    @DexIgnore
    public dg7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public dg7(Throwable th) {
        super(th);
    }
}
