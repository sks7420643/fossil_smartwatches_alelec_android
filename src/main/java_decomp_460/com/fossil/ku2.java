package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku2 extends e13<ku2, a> implements o23 {
    @DexIgnore
    public static /* final */ ku2 zzj;
    @DexIgnore
    public static volatile z23<ku2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public iu2 zzf;
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;
    @DexIgnore
    public boolean zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<ku2, a> implements o23 {
        @DexIgnore
        public a() {
            super(ku2.zzj);
        }

        @DexIgnore
        public /* synthetic */ a(fu2 fu2) {
            this();
        }

        @DexIgnore
        public final a x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ku2) this.c).D(str);
            return this;
        }
    }

    /*
    static {
        ku2 ku2 = new ku2();
        zzj = ku2;
        e13.u(ku2.class, ku2);
    }
    */

    @DexIgnore
    public static a N() {
        return (a) zzj.w();
    }

    @DexIgnore
    public final void D(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int G() {
        return this.zzd;
    }

    @DexIgnore
    public final String H() {
        return this.zze;
    }

    @DexIgnore
    public final iu2 I() {
        iu2 iu2 = this.zzf;
        return iu2 == null ? iu2.N() : iu2;
    }

    @DexIgnore
    public final boolean J() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean K() {
        return this.zzh;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean M() {
        return this.zzi;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (fu2.f1206a[i - 1]) {
            case 1:
                return new ku2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1008\u0001\u0003\u1009\u0002\u0004\u1007\u0003\u0005\u1007\u0004\u0006\u1007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                z23<ku2> z232 = zzk;
                if (z232 != null) {
                    return z232;
                }
                synchronized (ku2.class) {
                    try {
                        z23 = zzk;
                        if (z23 == null) {
                            z23 = new e13.c(zzj);
                            zzk = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
