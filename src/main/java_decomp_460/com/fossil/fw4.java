package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw4 extends pv5 implements t47.g {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public iw4 h;
    @DexIgnore
    public g37<t95> i;
    @DexIgnore
    public ps4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public kv4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return fw4.s;
        }

        @DexIgnore
        public final fw4 b(ps4 ps4, String str) {
            fw4 fw4 = new fw4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", ps4);
            bundle.putString("challenge_history_id_extra", str);
            fw4.setArguments(bundle);
            return fw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fw4 b;

        @DexIgnore
        public b(fw4 fw4) {
            this.b = fw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ fw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fw4 fw4) {
            super(1);
            this.this$0 = fw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.T6().E();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1219a;

        @DexIgnore
        public d(fw4 fw4) {
            this.f1219a = fw4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            t95 a2 = this.f1219a.S6().a();
            if (!(a2 == null || (flexibleTextView = a2.t) == null)) {
                flexibleTextView.setVisibility(8);
            }
            this.f1219a.T6().G();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e f1220a; // = new e();

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<cl7<? extends Boolean, ? extends Boolean>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1221a;

        @DexIgnore
        public f(fw4 fw4) {
            this.f1221a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, Boolean> cl7) {
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = cl7.getFirst();
            if (first != null) {
                boolean booleanValue = first.booleanValue();
                t95 a2 = this.f1221a.S6().a();
                if (!(a2 == null || (swipeRefreshLayout = a2.z) == null)) {
                    swipeRefreshLayout.setRefreshing(booleanValue);
                }
            }
            Boolean second = cl7.getSecond();
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.f1221a.b();
            } else {
                this.f1221a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1222a;

        @DexIgnore
        public g(fw4 fw4) {
            this.f1222a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fw4.t.a();
            local.e(a2, "challengeLive - oldChallenge: " + this.f1222a.j + " - new : " + ps4);
            this.f1222a.j = ps4;
            fw4 fw4 = this.f1222a;
            pq7.b(ps4, "it");
            fw4.Z6(ps4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<List<? extends Object>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1223a;

        @DexIgnore
        public h(fw4 fw4) {
            this.f1223a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fw4.t.a();
            local.e(a2, "highlightPlayersLive - " + list);
            kv4 L6 = fw4.L6(this.f1223a);
            pq7.b(list, "it");
            L6.g(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<cl7<? extends Integer, ? extends ms4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1224a;

        @DexIgnore
        public i(fw4 fw4) {
            this.f1224a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Integer, ms4> cl7) {
            Integer q;
            String str = null;
            int intValue = cl7.getFirst().intValue();
            Integer h = cl7.getSecond().h();
            int intValue2 = h != null ? h.intValue() : 0;
            hr7 hr7 = hr7.f1520a;
            String c = um5.c(PortfolioApp.h0.c(), 2131886316);
            pq7.b(c, "LanguageHelper.getString\u2026urrentRankNumberInNumber)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2), Integer.valueOf(intValue)}, 2));
            pq7.b(format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fw4.t.a();
            local.e(a2, "current - rank: " + intValue2 + " - numberOfPlayers: " + intValue);
            t95 a3 = this.f1224a.S6().a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.v;
                pq7.b(flexibleTextView, "ftvRank");
                flexibleTextView.setText(jl5.b(jl5.b, String.valueOf(intValue2), format, 0, 4, null));
                ps4 ps4 = this.f1224a.j;
                if (ps4 != null) {
                    str = ps4.r();
                }
                if (pq7.a("activity_reach_goal", str)) {
                    ps4 ps42 = this.f1224a.j;
                    int intValue3 = (ps42 == null || (q = ps42.q()) == null) ? 0 : q.intValue();
                    Integer n = cl7.getSecond().n();
                    int intValue4 = n != null ? n.intValue() : 0;
                    hr7 hr72 = hr7.f1520a;
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131887313);
                    pq7.b(c2, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                    String format2 = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue3 - intValue4), Integer.valueOf(intValue3)}, 2));
                    pq7.b(format2, "java.lang.String.format(format, *args)");
                    TimerTextView timerTextView = a3.r;
                    pq7.b(timerTextView, "ftvCountDown");
                    timerTextView.setTag(Integer.valueOf(intValue3));
                    TimerTextView timerTextView2 = a3.r;
                    pq7.b(timerTextView2, "ftvCountDown");
                    timerTextView2.setText(format2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1225a;

        @DexIgnore
        public j(fw4 fw4) {
            this.f1225a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            String str = null;
            if (cl7.getFirst().booleanValue()) {
                cl5.c.e();
                FragmentActivity activity = this.f1225a.getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            s37 s37 = s37.c;
            ServerError serverError = (ServerError) cl7.getSecond();
            Integer code = serverError != null ? serverError.getCode() : null;
            ServerError serverError2 = (ServerError) cl7.getSecond();
            if (serverError2 != null) {
                str = serverError2.getMessage();
            }
            FragmentManager childFragmentManager = this.f1225a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.g(code, str, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<gl7<? extends Boolean, ? extends Boolean, ? extends Boolean>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1226a;

        @DexIgnore
        public k(fw4 fw4) {
            this.f1226a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<Boolean, Boolean, Boolean> gl7) {
            boolean booleanValue = gl7.getFirst().booleanValue();
            boolean booleanValue2 = gl7.getSecond().booleanValue();
            boolean booleanValue3 = gl7.getThird().booleanValue();
            if (booleanValue) {
                this.f1226a.X6();
            } else if (booleanValue2) {
                fw4 fw4 = this.f1226a;
                ps4 ps4 = fw4.j;
                String f = ps4 != null ? ps4.f() : null;
                if (f != null) {
                    fw4.Y6(f);
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (booleanValue3) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f1226a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.h(childFragmentManager);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<List<? extends gs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1227a;

        @DexIgnore
        public l(fw4 fw4) {
            this.f1227a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<gs4> list) {
            fw4 fw4 = this.f1227a;
            pq7.b(list, "it");
            fw4.W6(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1228a;

        @DexIgnore
        public m(fw4 fw4) {
            this.f1228a = fw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            t95 a2 = this.f1228a.S6().a();
            if (a2 == null) {
                return;
            }
            if (cl7.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = a2.t;
                pq7.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.t;
            pq7.b(flexibleTextView2, "ftvError");
            String c = um5.c(flexibleTextView2.getContext(), 2131886231);
            FragmentActivity activity = this.f1228a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, c, 0).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<gl7<? extends String, ? extends String, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ n f1229a; // = new n();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<String, String, Integer> gl7) {
            xr4.f4164a.h("bc_left_challenge_after_start", gl7.getFirst(), gl7.getSecond(), gl7.getThird().intValue(), PortfolioApp.h0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements my5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fw4 f1230a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public o(fw4 fw4) {
            this.f1230a = fw4;
        }

        @DexIgnore
        @Override // com.fossil.my5
        public void a(gs4 gs4) {
            pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            Object a2 = gs4.a();
            if (!(a2 instanceof vy4)) {
                a2 = null;
            }
            vy4 vy4 = (vy4) a2;
            if (vy4 != null) {
                this.f1230a.T6().F(vy4);
            }
        }
    }

    /*
    static {
        String simpleName = fw4.class.getSimpleName();
        pq7.b(simpleName, "BCOverviewLeaderBoardFra\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ kv4 L6(fw4 fw4) {
        kv4 kv4 = fw4.l;
        if (kv4 != null) {
            return kv4;
        }
        pq7.n("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363373) {
            iw4 iw4 = this.h;
            if (iw4 != null) {
                iw4.D();
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final g37<t95> S6() {
        g37<t95> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final iw4 T6() {
        iw4 iw4 = this.h;
        if (iw4 != null) {
            return iw4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void U6() {
        Date date;
        int i2;
        Integer q;
        g37<t95> g37 = this.i;
        if (g37 != null) {
            t95 a2 = g37.a();
            if (a2 != null) {
                a2.x.setOnClickListener(new b(this));
                ImageView imageView = a2.w;
                pq7.b(imageView, "imgOption");
                fz4.a(imageView, new c(this));
                a2.z.setOnRefreshListener(new d(this));
                ps4 ps4 = this.j;
                if (ps4 == null || (date = ps4.m()) == null) {
                    date = new Date();
                }
                ps4 ps42 = this.j;
                if (pq7.a("activity_reach_goal", ps42 != null ? ps42.r() : null)) {
                    ps4 ps43 = this.j;
                    i2 = (ps43 == null || (q = ps43.q()) == null) ? 15000 : q.intValue();
                } else {
                    i2 = -1;
                }
                this.l = new kv4(date.getTime(), i2);
                RecyclerView recyclerView = a2.y;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                kv4 kv4 = this.l;
                if (kv4 != null) {
                    recyclerView.setAdapter(kv4);
                } else {
                    pq7.n("leaderBoardAdapter");
                    throw null;
                }
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6() {
        iw4 iw4 = this.h;
        if (iw4 != null) {
            iw4.y().h(getViewLifecycleOwner(), new f(this));
            iw4 iw42 = this.h;
            if (iw42 != null) {
                iw42.r().h(getViewLifecycleOwner(), new g(this));
                iw4 iw43 = this.h;
                if (iw43 != null) {
                    iw43.u().h(getViewLifecycleOwner(), new h(this));
                    iw4 iw44 = this.h;
                    if (iw44 != null) {
                        iw44.s().h(getViewLifecycleOwner(), new i(this));
                        iw4 iw45 = this.h;
                        if (iw45 != null) {
                            iw45.x().h(getViewLifecycleOwner(), new j(this));
                            iw4 iw46 = this.h;
                            if (iw46 != null) {
                                iw46.A().h(getViewLifecycleOwner(), new k(this));
                                iw4 iw47 = this.h;
                                if (iw47 != null) {
                                    iw47.z().h(getViewLifecycleOwner(), new l(this));
                                    iw4 iw48 = this.h;
                                    if (iw48 != null) {
                                        iw48.t().h(getViewLifecycleOwner(), new m(this));
                                        iw4 iw49 = this.h;
                                        if (iw49 != null) {
                                            iw49.w().h(getViewLifecycleOwner(), n.f1229a);
                                            iw4 iw410 = this.h;
                                            if (iw410 != null) {
                                                LiveData<Object> v = iw410.v();
                                                if (v != null) {
                                                    v.h(getViewLifecycleOwner(), e.f1220a);
                                                    return;
                                                }
                                                return;
                                            }
                                            pq7.n("viewModel");
                                            throw null;
                                        }
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                    pq7.n("viewModel");
                                    throw null;
                                }
                                pq7.n("viewModel");
                                throw null;
                            }
                            pq7.n("viewModel");
                            throw null;
                        }
                        pq7.n("viewModel");
                        throw null;
                    }
                    pq7.n("viewModel");
                    throw null;
                }
                pq7.n("viewModel");
                throw null;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void W6(List<gs4> list) {
        es4 b2 = es4.A.b();
        String c2 = um5.c(requireContext(), 2131886322);
        pq7.b(c2, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b2.setTitle(c2);
        b2.E6(list);
        b2.G6(new o(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, es4.A.a());
    }

    @DexIgnore
    public final void X6() {
        BCWaitingChallengeDetailActivity.B.a(this, this.j, "joined_challenge", -1, true);
    }

    @DexIgnore
    public final void Y6(String str) {
        Date date;
        int i2;
        Integer q;
        ps4 ps4 = this.j;
        if (ps4 == null || (date = ps4.m()) == null) {
            date = new Date();
        }
        ps4 ps42 = this.j;
        if (pq7.a("activity_reach_goal", ps42 != null ? ps42.r() : null)) {
            ps4 ps43 = this.j;
            i2 = (ps43 == null || (q = ps43.q()) == null) ? 15000 : q.intValue();
        } else {
            i2 = -1;
        }
        ps4 ps44 = this.j;
        BCLeaderBoardActivity.A.a(this, str, date.getTime(), i2, ps44 != null ? ps44.n() : null);
    }

    @DexIgnore
    public final void Z6(ps4 ps4) {
        String str;
        String a2;
        FLogger.INSTANCE.getLocal().e(s, "updateView - challenge: " + ps4);
        g37<t95> g37 = this.i;
        if (g37 != null) {
            t95 a3 = g37.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.A;
                pq7.b(flexibleTextView, "tvTitle");
                flexibleTextView.setText(ps4.g());
                String r = ps4.r();
                if (r != null) {
                    int hashCode = r.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && r.equals("activity_reach_goal")) {
                            a3.r.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099689));
                            Integer q = ps4.q();
                            int intValue = q != null ? q.intValue() : 0;
                            TimerTextView timerTextView = a3.r;
                            pq7.b(timerTextView, "ftvCountDown");
                            Object tag = timerTextView.getTag();
                            if (!(tag instanceof Integer)) {
                                tag = null;
                            }
                            if (((Integer) tag) == null) {
                                TimerTextView timerTextView2 = a3.r;
                                pq7.b(timerTextView2, "ftvCountDown");
                                timerTextView2.setTag(ps4.q());
                                hr7 hr7 = hr7.f1520a;
                                String c2 = um5.c(PortfolioApp.h0.c(), 2131887313);
                                pq7.b(c2, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue), Integer.valueOf(intValue)}, 2));
                                pq7.b(format, "java.lang.String.format(format, *args)");
                                TimerTextView timerTextView3 = a3.r;
                                pq7.b(timerTextView3, "ftvCountDown");
                                timerTextView3.setText(format);
                            }
                            TimerTextView timerTextView4 = a3.u;
                            pq7.b(timerTextView4, "ftvLeft");
                            timerTextView4.setVisibility(0);
                        }
                    } else if (r.equals("activity_best_result")) {
                        a3.r.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099677));
                        if (pq7.a("completed", ps4.n())) {
                            TimerTextView timerTextView5 = a3.r;
                            pq7.b(timerTextView5, "ftvCountDown");
                            timerTextView5.setText(um5.c(PortfolioApp.h0.c(), 2131887316));
                        } else {
                            TimerTextView timerTextView6 = a3.r;
                            Date e2 = ps4.e();
                            timerTextView6.setTime(e2 != null ? e2.getTime() : 0);
                            a3.r.setDisplayType(wy4.HOUR_MIN_SEC);
                        }
                    }
                }
                String l0 = PortfolioApp.h0.c().l0();
                ht4 i2 = ps4.i();
                if (pq7.a(l0, i2 != null ? i2.b() : null)) {
                    a2 = um5.c(PortfolioApp.h0.c(), 2131886250);
                } else {
                    hz4 hz4 = hz4.f1561a;
                    ht4 i3 = ps4.i();
                    String a4 = i3 != null ? i3.a() : null;
                    ht4 i4 = ps4.i();
                    String c3 = i4 != null ? i4.c() : null;
                    ht4 i5 = ps4.i();
                    if (i5 == null || (str = i5.d()) == null) {
                        str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                    }
                    a2 = hz4.a(a4, c3, str);
                }
                Date b2 = ps4.b();
                if (b2 != null) {
                    String c4 = lk5.c(b2);
                    hr7 hr72 = hr7.f1520a;
                    String c5 = um5.c(PortfolioApp.h0.c(), 2131886312);
                    pq7.b(c5, "LanguageHelper.getString\u2026bel__CreatedByNameOnDate)");
                    String format2 = String.format(c5, Arrays.copyOf(new Object[]{a2, c4}, 2));
                    pq7.b(format2, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView2 = a3.s;
                    pq7.b(flexibleTextView2, "ftvCreated");
                    jl5 jl5 = jl5.b;
                    pq7.b(a2, Constants.PROFILE_KEY_LAST_NAME);
                    flexibleTextView2.setText(jl5.b(jl5, a2, format2, 0, 4, null));
                    return;
                }
                pq7.i();
                throw null;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 15 && i3 == -1 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.j = arguments != null ? (ps4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.k = arguments2 != null ? arguments2.getString("challenge_history_id_extra") : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.e(str, "onCreate - challenge: " + this.j + " - historyId: " + this.k);
        PortfolioApp.h0.c().M().E0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(iw4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.h = (iw4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        t95 t95 = (t95) aq0.f(layoutInflater, 2131558600, viewGroup, false, A6());
        this.i = new g37<>(this, t95);
        pq7.b(t95, "binding");
        return t95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.k == null) {
            iw4 iw4 = this.h;
            if (iw4 != null) {
                iw4.G();
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
        ck5 g2 = ck5.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_leader_board", activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        iw4 iw4 = this.h;
        if (iw4 != null) {
            iw4.B(this.j, this.k);
            U6();
            V6();
            ps4 ps4 = this.j;
            if (ps4 != null) {
                Z6(ps4);
                return;
            }
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
