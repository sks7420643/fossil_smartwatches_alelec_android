package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ek2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static ek2 f951a;

    @DexIgnore
    public static ek2 b() {
        ek2 ek2;
        synchronized (ek2.class) {
            try {
                if (f951a == null) {
                    f951a = new ak2();
                }
                ek2 = f951a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ek2;
    }

    @DexIgnore
    public abstract fk2<Boolean> a(String str, boolean z);
}
