package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f320a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public at4(String str, String str2, String str3, String str4, String str5) {
        pq7.c(str, "id");
        pq7.c(str2, Constants.PROFILE_KEY_FIRST_NAME);
        pq7.c(str3, Constants.PROFILE_KEY_LAST_NAME);
        pq7.c(str4, "socialId");
        pq7.c(str5, "avatar");
        this.f320a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.f320a;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof at4) {
                at4 at4 = (at4) obj;
                if (!pq7.a(this.f320a, at4.f320a) || !pq7.a(this.b, at4.b) || !pq7.a(this.c, at4.c) || !pq7.a(this.d, at4.d) || !pq7.a(this.e, at4.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f320a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "FriendIn(id=" + this.f320a + ", firstName=" + this.b + ", lastName=" + this.c + ", socialId=" + this.d + ", avatar=" + this.e + ")";
    }
}
