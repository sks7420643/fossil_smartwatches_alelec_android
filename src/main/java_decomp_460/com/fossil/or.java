package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or extends zj {
    @DexIgnore
    public /* final */ pu1 T;

    @DexIgnore
    public or(k5 k5Var, i60 i60, pu1 pu1) {
        super(k5Var, i60, yp.f0, true, pu1.a(), pu1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = pu1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.n3, this.T.toJSONObject());
    }

    @DexIgnore
    public final pu1 R() {
        return this.T;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.T.getLocaleString();
    }
}
