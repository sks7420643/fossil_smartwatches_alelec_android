package com.fossil;

import com.misfit.frameworks.buttonservice.communite.CommunicateMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p17 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2766a;

    /*
    static {
        int[] iArr = new int[CommunicateMode.values().length];
        f2766a = iArr;
        iArr[CommunicateMode.RESET_HAND.ordinal()] = 1;
        f2766a[CommunicateMode.ENTER_CALIBRATION.ordinal()] = 2;
        f2766a[CommunicateMode.APPLY_HAND_POSITION.ordinal()] = 3;
        f2766a[CommunicateMode.MOVE_HAND.ordinal()] = 4;
        f2766a[CommunicateMode.EXIT_CALIBRATION.ordinal()] = 5;
    }
    */
}
