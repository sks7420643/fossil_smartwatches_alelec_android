package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zv6 extends gq4<yv6> {
    @DexIgnore
    Object H2();  // void declaration

    @DexIgnore
    void P4(boolean z);

    @DexIgnore
    Object S0();  // void declaration

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    Object i();  // void declaration

    @DexIgnore
    Object m3();  // void declaration

    @DexIgnore
    void n6(String str);

    @DexIgnore
    void o(int i, String str);

    @DexIgnore
    void r5(String str);
}
