package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.watchface.Data;
import com.portfolio.platform.data.model.watchface.MetaData;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f2645a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Data> {
    }

    @DexIgnore
    public o77() {
        Gson d = new zi4().d();
        pq7.b(d, "GsonBuilder().create()");
        this.f2645a = d;
    }

    @DexIgnore
    public final int a(l77 l77) {
        pq7.c(l77, "type");
        return l77.getValue();
    }

    @DexIgnore
    public final String b(q77 q77) {
        pq7.c(q77, "owner");
        try {
            return this.f2645a.u(q77, q77.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String c(Data data) {
        pq7.c(data, "data");
        String u = new Gson().u(data, new a().getType());
        pq7.b(u, "Gson().toJson(data, type)");
        return u;
    }

    @DexIgnore
    public final Data d(String str) {
        pq7.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Data) new Gson().l(str, new b().getType());
    }

    @DexIgnore
    public final MetaData e(String str) {
        try {
            return (MetaData) this.f2645a.k(str, MetaData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final String f(MetaData metaData) {
        try {
            return this.f2645a.u(metaData, MetaData.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final l77 g(int i) {
        l77[] values = l77.values();
        for (l77 l77 : values) {
            if (l77.getValue() == i) {
                return l77;
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final q77 h(String str) {
        pq7.c(str, "value");
        try {
            return (q77) this.f2645a.k(str, q77.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
