package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.fossil.kp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ od0 f2669a; // = new od0();

    @DexIgnore
    public final kp1 a(Location[] locationArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, float f) {
        Location location;
        Location location2;
        Location location3;
        Location[] locationArr2;
        Location location4;
        double d;
        double d2;
        double d3;
        double d4;
        if (locationArr.length <= 1) {
            return null;
        }
        Location[] locationArr3 = (Location[]) locationArr.clone();
        if (locationArr.length == 0) {
            location = null;
        } else {
            location = locationArr[0];
            int K = em7.K(locationArr);
            if (K != 0) {
                double longitude = location.getLongitude();
                if (1 <= K) {
                    int i9 = 1;
                    while (true) {
                        Location location5 = locationArr[i9];
                        double longitude2 = location5.getLongitude();
                        if (Double.compare(longitude, longitude2) > 0) {
                            longitude = longitude2;
                            location = location5;
                        }
                        if (i9 == K) {
                            break;
                        }
                        i9++;
                    }
                }
            }
        }
        if (location != null) {
            double longitude3 = location.getLongitude();
            if (locationArr.length == 0) {
                location2 = null;
            } else {
                location2 = locationArr[0];
                int K2 = em7.K(locationArr);
                if (K2 != 0) {
                    double latitude = location2.getLatitude();
                    if (1 <= K2) {
                        int i10 = 1;
                        while (true) {
                            Location location6 = locationArr[i10];
                            double latitude2 = location6.getLatitude();
                            if (Double.compare(latitude, latitude2) > 0) {
                                latitude = latitude2;
                                location2 = location6;
                            }
                            if (i10 == K2) {
                                break;
                            }
                            i10++;
                        }
                    }
                }
            }
            if (location2 != null) {
                double latitude3 = location2.getLatitude();
                if (locationArr.length == 0) {
                    location3 = null;
                    locationArr2 = locationArr3;
                } else {
                    Location location7 = locationArr[0];
                    int K3 = em7.K(locationArr);
                    if (K3 != 0) {
                        double longitude4 = location7.getLongitude();
                        if (1 <= K3) {
                            int i11 = 1;
                            location3 = location7;
                            while (true) {
                                Location location8 = locationArr[i11];
                                double longitude5 = location8.getLongitude();
                                if (Double.compare(longitude4, longitude5) < 0) {
                                    location3 = location8;
                                } else {
                                    longitude5 = longitude4;
                                }
                                if (i11 == K3) {
                                    break;
                                }
                                i11++;
                                longitude4 = longitude5;
                            }
                            locationArr2 = locationArr3;
                        }
                    }
                    location3 = location7;
                    locationArr2 = locationArr3;
                }
                if (location3 != null) {
                    double longitude6 = location3.getLongitude();
                    if (locationArr.length == 0) {
                        location4 = null;
                    } else {
                        location4 = locationArr[0];
                        int K4 = em7.K(locationArr);
                        if (K4 != 0) {
                            double latitude4 = location4.getLatitude();
                            if (1 <= K4) {
                                int i12 = 1;
                                while (true) {
                                    Location location9 = locationArr[i12];
                                    double latitude5 = location9.getLatitude();
                                    if (Double.compare(latitude4, latitude5) < 0) {
                                        latitude4 = latitude5;
                                        location4 = location9;
                                    }
                                    if (i12 == K4) {
                                        break;
                                    }
                                    i12++;
                                }
                            }
                        }
                    }
                    if (location4 != null) {
                        double d5 = longitude6 - longitude3;
                        double latitude6 = location4.getLatitude() - latitude3;
                        int i13 = (d5 > latitude6 ? 1 : (d5 == latitude6 ? 0 : -1));
                        if (i13 > 0) {
                            d = (double) i5;
                            d2 = (double) i3;
                            d4 = (double) ((i - i3) - i4);
                            i2 /= 2;
                            d3 = ((((double) i2) / 2.0d) - d) - ((double) i6);
                        } else {
                            d = (double) i3;
                            d2 = (double) i5;
                            double d6 = ((double) i) / 2.0d;
                            d3 = (double) ((i2 - i3) - i4);
                            i /= 2;
                            d4 = (d6 - d2) - ((double) i6);
                        }
                        double d7 = d5 / d4;
                        double d8 = latitude6 / d3;
                        double max = Math.max(d7, d8);
                        for (Location location10 : locationArr2) {
                            location10.setLongitude(location10.getLongitude() - longitude3);
                            location10.setLatitude(location10.getLatitude() - latitude3);
                            location10.setLongitude(location10.getLongitude() / max);
                            location10.setLatitude(location10.getLatitude() / max);
                            if (d7 > d8) {
                                location10.setLatitude(location10.getLatitude() + ((d3 - (latitude6 / max)) / ((double) 2)));
                            } else {
                                location10.setLongitude(location10.getLongitude() + ((d4 - (d5 / max)) / ((double) 2)));
                            }
                            location10.setLatitude(d3 - location10.getLatitude());
                            location10.setLongitude(location10.getLongitude() + d2);
                            location10.setLatitude(location10.getLatitude() + d);
                        }
                        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
                        pq7.b(createBitmap, "Bitmap\n                .\u2026 Bitmap.Config.ARGB_8888)");
                        Paint paint = new Paint();
                        paint.setColor(i8);
                        paint.setStrokeWidth(f);
                        Canvas canvas = new Canvas(createBitmap);
                        canvas.drawColor(i7);
                        int length = locationArr2.length;
                        int i14 = 0;
                        while (i14 < length - 1) {
                            int i15 = i14 + 1;
                            canvas.drawLine((float) locationArr2[i14].getLongitude(), (float) locationArr2[i14].getLatitude(), (float) locationArr2[i15].getLongitude(), (float) locationArr2[i15].getLatitude(), paint);
                            i14 = i15;
                        }
                        OutputSettings outputSettings = new OutputSettings(i, i2, Format.RLE, false, false);
                        int hashCode = createBitmap.hashCode();
                        FilterResult apply = EInkImageFilter.create().apply(createBitmap, FilterType.DIRECT_MAPPING, false, false, outputSettings);
                        pq7.b(apply, "EInkImageFilter.create()\u2026se, false, routeSettings)");
                        byte[] data = apply.getData();
                        pq7.b(data, "EInkImageFilter.create()\u2026alse, routeSettings).data");
                        return new kp1(String.valueOf(hashCode), data, i13 > 0 ? kp1.b.HORIZONTAL : kp1.b.VERTICAL);
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }
}
