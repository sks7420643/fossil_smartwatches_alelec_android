package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ mo1 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ ko1[] j;
    @DexIgnore
    public /* final */ long k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zn1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zn1 createFromParcel(Parcel parcel) {
            return new zn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zn1[] newArray(int i) {
            return new zn1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ zn1(android.os.Parcel r13, com.fossil.kq7 r14) {
        /*
            r12 = this;
            r9 = 0
            java.io.Serializable r2 = r13.readSerializable()
            if (r2 == 0) goto L_0x0071
            com.fossil.mo1 r2 = (com.fossil.mo1) r2
            int r3 = r13.readInt()
            java.lang.String r4 = r13.readString()
            if (r4 == 0) goto L_0x006d
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r4, r0)
            java.lang.String r5 = r13.readString()
            if (r5 == 0) goto L_0x0069
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r5, r0)
            java.lang.String r6 = r13.readString()
            if (r6 == 0) goto L_0x0065
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r6, r0)
            int r7 = r13.readInt()
            java.lang.String r8 = r13.readString()
            if (r8 == 0) goto L_0x0061
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r8, r0)
            com.fossil.ko1$a r0 = com.fossil.ko1.d
            java.lang.String[] r1 = r13.createStringArray()
            if (r1 == 0) goto L_0x005d
            java.lang.String r9 = "parcel.createStringArray()!!"
            com.fossil.pq7.b(r1, r9)
            com.fossil.ko1[] r9 = r0.a(r1)
            long r10 = r13.readLong()
            r1 = r12
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            long r0 = r13.readLong()
            r12.d = r0
            return
        L_0x005d:
            com.fossil.pq7.i()
            throw r9
        L_0x0061:
            com.fossil.pq7.i()
            throw r9
        L_0x0065:
            com.fossil.pq7.i()
            throw r9
        L_0x0069:
            com.fossil.pq7.i()
            throw r9
        L_0x006d:
            com.fossil.pq7.i()
            throw r9
        L_0x0071:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationType"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zn1.<init>(android.os.Parcel, com.fossil.kq7):void");
    }

    @DexIgnore
    public zn1(mo1 mo1, int i2, String str, String str2, String str3, int i3, String str4, ko1[] ko1Arr, long j2) {
        this.b = mo1;
        this.c = i2;
        this.e = str;
        ix1 ix1 = ix1.f1688a;
        byte[] bytes = str.getBytes(hd0.y.c());
        pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
        this.d = ix1.b(dm7.p(bytes, (byte) 0), ix1.a.CRC32);
        this.f = iy1.e(str2, 99, null, null, 6, null);
        this.g = iy1.e(str3, 97, null, null, 6, null);
        this.h = i3;
        this.i = iy1.e(str4, 249, null, null, 6, null);
        this.j = ko1Arr;
        this.k = j2;
    }

    @DexIgnore
    public final byte[] a() {
        byte b2 = (byte) 12;
        byte b3 = this.b.b();
        byte b4 = 0;
        for (ko1 ko1 : this.j) {
            b4 = (byte) (b4 | ko1.a());
        }
        byte b5 = (byte) 4;
        String a2 = iy1.a(this.f);
        Charset c2 = hd0.y.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            String a3 = iy1.a(this.g);
            Charset c3 = hd0.y.c();
            if (a3 != null) {
                byte[] bytes2 = a3.getBytes(c3);
                pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                String a4 = iy1.a(this.i);
                Charset c4 = hd0.y.c();
                if (a4 != null) {
                    byte[] bytes3 = a4.getBytes(c4);
                    pq7.b(bytes3, "(this as java.lang.String).getBytes(charset)");
                    short length = (short) (b2 + b5 + b5 + bytes.length + bytes2.length + b5 + bytes3.length + b5);
                    ByteBuffer allocate = ByteBuffer.allocate(length);
                    pq7.b(allocate, "ByteBuffer.allocate(totalLen.toInt())");
                    allocate.order(ByteOrder.LITTLE_ENDIAN);
                    allocate.putShort(length);
                    allocate.put(b2);
                    allocate.put(b3);
                    allocate.put(b4);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.put((byte) bytes.length);
                    allocate.put((byte) bytes2.length);
                    allocate.put((byte) bytes3.length);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.putInt(this.c);
                    allocate.putInt((int) this.d);
                    allocate.put(bytes);
                    allocate.put(bytes2);
                    allocate.put(bytes3);
                    allocate.putInt(this.h);
                    allocate.putInt((int) (this.k / ((long) 1000)));
                    byte[] array = allocate.array();
                    pq7.b(array, "notificationData.array()");
                    return array;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(zn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            zn1 zn1 = (zn1) obj;
            if (this.b != zn1.b) {
                return false;
            }
            if (this.c != zn1.c) {
                return false;
            }
            if (this.d != zn1.d) {
                return false;
            }
            if (!pq7.a(this.f, zn1.f)) {
                return false;
            }
            if (!pq7.a(this.g, zn1.g)) {
                return false;
            }
            if (this.h != zn1.h) {
                return false;
            }
            if (!pq7.a(this.i, zn1.i)) {
                return false;
            }
            if (!Arrays.equals(this.j, zn1.j)) {
                return false;
            }
            return this.k == zn1.k;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotification");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.d;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.e;
    }

    @DexIgnore
    public final ko1[] getFlags() {
        return this.j;
    }

    @DexIgnore
    public final String getMessage() {
        return this.i;
    }

    @DexIgnore
    public final long getReceivedTimestampInMilliSecond() {
        return this.k;
    }

    @DexIgnore
    public final String getSender() {
        return this.g;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.h;
    }

    @DexIgnore
    public final String getTitle() {
        return this.f;
    }

    @DexIgnore
    public final mo1 getType() {
        return this.b;
    }

    @DexIgnore
    public final int getUid() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int i2 = this.c;
        int hashCode2 = Long.valueOf(this.d).hashCode();
        int hashCode3 = this.f.hashCode();
        int hashCode4 = this.g.hashCode();
        int hashCode5 = Integer.valueOf(this.h).hashCode();
        int hashCode6 = this.i.hashCode();
        return (((((((((((((((hashCode * 31) + i2) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + Arrays.hashCode(this.j)) * 31) + Long.valueOf(this.k).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        String str = this.i;
        Charset c2 = hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            JSONObject k2 = g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.e, ey1.a(this.b)), jd0.f, Integer.valueOf(this.c)), jd0.X2, this.e), jd0.W2, hy1.k((int) this.d, null, 1, null)), jd0.h, this.f), jd0.i, this.g), jd0.j, Integer.valueOf(this.h)), jd0.x1, Integer.valueOf(this.i.length())), jd0.y1, hy1.k((int) ix1.f1688a.b(bytes, ix1.a.CRC32), null, 1, null));
            jd0 jd0 = jd0.l;
            ko1[] ko1Arr = this.j;
            JSONArray jSONArray = new JSONArray();
            for (ko1 ko1 : ko1Arr) {
                jSONArray.put(ey1.a(ko1));
            }
            return g80.k(g80.k(k2, jd0, jSONArray), jd0.m, Float.valueOf(((float) this.k) / 1000.0f));
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeSerializable(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h);
        parcel.writeString(this.i);
        ko1[] ko1Arr = this.j;
        ArrayList arrayList = new ArrayList(ko1Arr.length);
        for (ko1 ko1 : ko1Arr) {
            arrayList.add(ko1.name());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            parcel.writeStringArray((String[]) array);
            parcel.writeLong(this.k);
            parcel.writeLong(this.d);
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
