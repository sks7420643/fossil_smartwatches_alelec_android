package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n68 {
    @DexIgnore
    public static /* final */ ByteArrayBuffer e; // = c(p68.f2789a, ": ");
    @DexIgnore
    public static /* final */ ByteArrayBuffer f; // = c(p68.f2789a, "\r\n");
    @DexIgnore
    public static /* final */ ByteArrayBuffer g; // = c(p68.f2789a, "--");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Charset f2475a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ List<l68> c;
    @DexIgnore
    public /* final */ o68 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f2476a;

        /*
        static {
            int[] iArr = new int[o68.values().length];
            f2476a = iArr;
            try {
                iArr[o68.STRICT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2476a[o68.BROWSER_COMPATIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public n68(String str, Charset charset, String str2, o68 o68) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 != null) {
            this.f2475a = charset == null ? p68.f2789a : charset;
            this.b = str2;
            this.c = new ArrayList();
            this.d = o68;
        } else {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        }
    }

    @DexIgnore
    public static ByteArrayBuffer c(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    @DexIgnore
    public static void g(String str, OutputStream outputStream) throws IOException {
        i(c(p68.f2789a, str), outputStream);
    }

    @DexIgnore
    public static void h(String str, Charset charset, OutputStream outputStream) throws IOException {
        i(c(charset, str), outputStream);
    }

    @DexIgnore
    public static void i(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    @DexIgnore
    public static void j(q68 q68, OutputStream outputStream) throws IOException {
        g(q68.b(), outputStream);
        i(e, outputStream);
        g(q68.a(), outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public static void k(q68 q68, Charset charset, OutputStream outputStream) throws IOException {
        h(q68.b(), charset, outputStream);
        i(e, outputStream);
        h(q68.a(), charset, outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public void a(l68 l68) {
        if (l68 != null) {
            this.c.add(l68);
        }
    }

    @DexIgnore
    public final void b(o68 o68, OutputStream outputStream, boolean z) throws IOException {
        ByteArrayBuffer c2 = c(this.f2475a, e());
        for (l68 l68 : this.c) {
            i(g, outputStream);
            i(c2, outputStream);
            i(f, outputStream);
            m68 f2 = l68.f();
            int i = a.f2476a[o68.ordinal()];
            if (i == 1) {
                Iterator<q68> it = f2.iterator();
                while (it.hasNext()) {
                    j(it.next(), outputStream);
                }
            } else if (i == 2) {
                k(l68.f().b("Content-Disposition"), this.f2475a, outputStream);
                if (l68.e().d() != null) {
                    k(l68.f().b("Content-Type"), this.f2475a, outputStream);
                }
            }
            i(f, outputStream);
            if (z) {
                l68.e().writeTo(outputStream);
            }
            i(f, outputStream);
        }
        i(g, outputStream);
        i(c2, outputStream);
        i(g, outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public List<l68> d() {
        return this.c;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public long f() {
        long j = 0;
        for (l68 l68 : this.c) {
            long contentLength = l68.e().getContentLength();
            if (contentLength < 0) {
                return -1;
            }
            j = contentLength + j;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            b(this.d, byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e2) {
            return -1;
        }
    }

    @DexIgnore
    public void l(OutputStream outputStream) throws IOException {
        b(this.d, outputStream, true);
    }
}
