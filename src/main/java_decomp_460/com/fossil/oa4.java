package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa4 extends ta4.d.AbstractC0224d.a.b.e.AbstractC0233b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f2660a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Long f2661a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b a() {
            String str = "";
            if (this.f2661a == null) {
                str = " pc";
            }
            if (this.b == null) {
                str = str + " symbol";
            }
            if (this.d == null) {
                str = str + " offset";
            }
            if (this.e == null) {
                str = str + " importance";
            }
            if (str.isEmpty()) {
                return new oa4(this.f2661a.longValue(), this.b, this.c, this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a c(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a d(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a e(long j) {
            this.f2661a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.AbstractC0234a f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null symbol");
        }
    }

    @DexIgnore
    public oa4(long j, String str, String str2, long j2, int i) {
        this.f2660a = j;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b
    public int c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b
    public long d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b
    public long e() {
        return this.f2660a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a.b.e.AbstractC0233b)) {
            return false;
        }
        ta4.d.AbstractC0224d.a.b.e.AbstractC0233b bVar = (ta4.d.AbstractC0224d.a.b.e.AbstractC0233b) obj;
        return this.f2660a == bVar.e() && this.b.equals(bVar.f()) && ((str = this.c) != null ? str.equals(bVar.b()) : bVar.b() == null) && this.d == bVar.d() && this.e == bVar.c();
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0233b
    public String f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f2660a;
        int i = (int) (j ^ (j >>> 32));
        int hashCode = this.b.hashCode();
        String str = this.c;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j2 = this.d;
        return ((((hashCode2 ^ ((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003)) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.e;
    }

    @DexIgnore
    public String toString() {
        return "Frame{pc=" + this.f2660a + ", symbol=" + this.b + ", file=" + this.c + ", offset=" + this.d + ", importance=" + this.e + "}";
    }
}
