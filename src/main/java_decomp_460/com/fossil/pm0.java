package com.fossil;

import android.content.res.Configuration;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm0 {
    @DexIgnore
    public static rm0 a(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 24) {
            return rm0.d(configuration.getLocales());
        }
        return rm0.a(configuration.locale);
    }
}
