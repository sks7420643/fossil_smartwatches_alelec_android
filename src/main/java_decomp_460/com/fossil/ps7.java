package com.fossil;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps7<T> implements ts7<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicReference<ts7<T>> f2865a;

    @DexIgnore
    public ps7(ts7<? extends T> ts7) {
        pq7.c(ts7, "sequence");
        this.f2865a = new AtomicReference<>(ts7);
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<T> iterator() {
        ts7<T> andSet = this.f2865a.getAndSet(null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
