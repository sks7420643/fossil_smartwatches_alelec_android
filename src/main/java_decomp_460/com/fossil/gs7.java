package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gs7<R> extends ds7<R>, uk7<R> {
    @DexIgnore
    boolean isExternal();

    @DexIgnore
    boolean isInfix();

    @DexIgnore
    boolean isInline();

    @DexIgnore
    boolean isOperator();

    @DexIgnore
    @Override // com.fossil.ds7
    boolean isSuspend();
}
