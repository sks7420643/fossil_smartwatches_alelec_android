package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v2 implements Parcelable.Creator<w2> {
    @DexIgnore
    public /* synthetic */ v2(kq7 kq7) {
    }

    @DexIgnore
    public w2 a(Parcel parcel) {
        return new w2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public w2 createFromParcel(Parcel parcel) {
        return new w2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public w2[] newArray(int i) {
        return new w2[i];
    }
}
