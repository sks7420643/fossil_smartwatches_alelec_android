package com.fossil;

import android.bluetooth.BluetoothProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k80 implements BluetoothProfile.ServiceListener {
    @DexIgnore
    public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
        m80.c.a("HIDProfile", "onServiceConnected: profile=%s, proxy=%s.", Integer.valueOf(i), bluetoothProfile);
        if (i == 4) {
            l80 l80 = l80.d;
            l80.b = bluetoothProfile;
        }
    }

    @DexIgnore
    public void onServiceDisconnected(int i) {
        m80.c.a("HIDProfile", "onServiceDisconnected: profile=%s.", Integer.valueOf(i));
        if (i == 4) {
            l80 l80 = l80.d;
            l80.b = null;
        }
    }
}
