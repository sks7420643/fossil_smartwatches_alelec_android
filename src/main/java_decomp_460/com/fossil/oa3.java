package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa3 implements Parcelable.Creator<LocationAvailability> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 0;
        xa3[] xa3Arr = null;
        int i = 1000;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i3 = ad2.v(parcel, t);
            } else if (l == 2) {
                i2 = ad2.v(parcel, t);
            } else if (l == 3) {
                j = ad2.y(parcel, t);
            } else if (l == 4) {
                i = ad2.v(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                xa3Arr = (xa3[]) ad2.i(parcel, t, xa3.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new LocationAvailability(i, i3, i2, j, xa3Arr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
