package com.fossil;

import android.app.Notification;
import android.app.RemoteInput;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import com.fossil.zk0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class al0 implements yk0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Notification.Builder f284a;
    @DexIgnore
    public /* final */ zk0.e b;
    @DexIgnore
    public RemoteViews c;
    @DexIgnore
    public RemoteViews d;
    @DexIgnore
    public /* final */ List<Bundle> e; // = new ArrayList();
    @DexIgnore
    public /* final */ Bundle f; // = new Bundle();
    @DexIgnore
    public int g;
    @DexIgnore
    public RemoteViews h;

    @DexIgnore
    public al0(zk0.e eVar) {
        ArrayList<String> arrayList;
        this.b = eVar;
        if (Build.VERSION.SDK_INT >= 26) {
            this.f284a = new Notification.Builder(eVar.f4484a, eVar.I);
        } else {
            this.f284a = new Notification.Builder(eVar.f4484a);
        }
        Notification notification = eVar.P;
        this.f284a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, eVar.h).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(eVar.d).setContentText(eVar.e).setContentInfo(eVar.j).setContentIntent(eVar.f).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(eVar.g, (notification.flags & 128) != 0).setLargeIcon(eVar.i).setNumber(eVar.k).setProgress(eVar.r, eVar.s, eVar.t);
        if (Build.VERSION.SDK_INT < 21) {
            this.f284a.setSound(notification.sound, notification.audioStreamType);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            this.f284a.setSubText(eVar.p).setUsesChronometer(eVar.n).setPriority(eVar.l);
            Iterator<zk0.a> it = eVar.b.iterator();
            while (it.hasNext()) {
                b(it.next());
            }
            Bundle bundle = eVar.B;
            if (bundle != null) {
                this.f.putAll(bundle);
            }
            if (Build.VERSION.SDK_INT < 20) {
                if (eVar.x) {
                    this.f.putBoolean("android.support.localOnly", true);
                }
                String str = eVar.u;
                if (str != null) {
                    this.f.putString("android.support.groupKey", str);
                    if (eVar.v) {
                        this.f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                String str2 = eVar.w;
                if (str2 != null) {
                    this.f.putString("android.support.sortKey", str2);
                }
            }
            this.c = eVar.F;
            this.d = eVar.G;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            this.f284a.setShowWhen(eVar.m);
            if (Build.VERSION.SDK_INT < 21 && (arrayList = eVar.R) != null && !arrayList.isEmpty()) {
                Bundle bundle2 = this.f;
                ArrayList<String> arrayList2 = eVar.R;
                bundle2.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
            }
        }
        if (Build.VERSION.SDK_INT >= 20) {
            this.f284a.setLocalOnly(eVar.x).setGroup(eVar.u).setGroupSummary(eVar.v).setSortKey(eVar.w);
            this.g = eVar.M;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.f284a.setCategory(eVar.A).setColor(eVar.C).setVisibility(eVar.D).setPublicVersion(eVar.E).setSound(notification.sound, notification.audioAttributes);
            Iterator<String> it2 = eVar.R.iterator();
            while (it2.hasNext()) {
                this.f284a.addPerson(it2.next());
            }
            this.h = eVar.H;
            if (eVar.c.size() > 0) {
                Bundle bundle3 = eVar.d().getBundle("android.car.EXTENSIONS");
                Bundle bundle4 = bundle3 == null ? new Bundle() : bundle3;
                Bundle bundle5 = new Bundle();
                for (int i = 0; i < eVar.c.size(); i++) {
                    bundle5.putBundle(Integer.toString(i), bl0.b(eVar.c.get(i)));
                }
                bundle4.putBundle("invisible_actions", bundle5);
                eVar.d().putBundle("android.car.EXTENSIONS", bundle4);
                this.f.putBundle("android.car.EXTENSIONS", bundle4);
            }
        }
        if (Build.VERSION.SDK_INT >= 24) {
            this.f284a.setExtras(eVar.B).setRemoteInputHistory(eVar.q);
            RemoteViews remoteViews = eVar.F;
            if (remoteViews != null) {
                this.f284a.setCustomContentView(remoteViews);
            }
            RemoteViews remoteViews2 = eVar.G;
            if (remoteViews2 != null) {
                this.f284a.setCustomBigContentView(remoteViews2);
            }
            RemoteViews remoteViews3 = eVar.H;
            if (remoteViews3 != null) {
                this.f284a.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (Build.VERSION.SDK_INT >= 26) {
            this.f284a.setBadgeIconType(eVar.J).setShortcutId(eVar.K).setTimeoutAfter(eVar.L).setGroupAlertBehavior(eVar.M);
            if (eVar.z) {
                this.f284a.setColorized(eVar.y);
            }
            if (!TextUtils.isEmpty(eVar.I)) {
                this.f284a.setSound(null).setDefaults(0).setLights(0, 0, 0).setVibrate(null);
            }
        }
        if (Build.VERSION.SDK_INT >= 29) {
            this.f284a.setAllowSystemGeneratedContextualActions(eVar.N);
            this.f284a.setBubbleMetadata(zk0.d.h(eVar.O));
        }
        if (eVar.Q) {
            if (this.b.v) {
                this.g = 2;
            } else {
                this.g = 1;
            }
            this.f284a.setVibrate(null);
            this.f284a.setSound(null);
            int i2 = notification.defaults & -2;
            notification.defaults = i2;
            int i3 = i2 & -3;
            notification.defaults = i3;
            this.f284a.setDefaults(i3);
            if (Build.VERSION.SDK_INT >= 26) {
                if (TextUtils.isEmpty(this.b.u)) {
                    this.f284a.setGroup("silent");
                }
                this.f284a.setGroupAlertBehavior(this.g);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.yk0
    public Notification.Builder a() {
        return this.f284a;
    }

    @DexIgnore
    public final void b(zk0.a aVar) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 20) {
            IconCompat e2 = aVar.e();
            Notification.Action.Builder builder = Build.VERSION.SDK_INT >= 23 ? new Notification.Action.Builder(e2 != null ? e2.n() : null, aVar.i(), aVar.a()) : new Notification.Action.Builder(e2 != null ? e2.c() : 0, aVar.i(), aVar.a());
            if (aVar.f() != null) {
                for (RemoteInput remoteInput : dl0.b(aVar.f())) {
                    builder.addRemoteInput(remoteInput);
                }
            }
            Bundle bundle = aVar.d() != null ? new Bundle(aVar.d()) : new Bundle();
            bundle.putBoolean("android.support.allowGeneratedReplies", aVar.b());
            if (Build.VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(aVar.b());
            }
            bundle.putInt("android.support.action.semanticAction", aVar.g());
            if (Build.VERSION.SDK_INT >= 28) {
                builder.setSemanticAction(aVar.g());
            }
            if (Build.VERSION.SDK_INT >= 29) {
                builder.setContextual(aVar.j());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", aVar.h());
            builder.addExtras(bundle);
            this.f284a.addAction(builder.build());
        } else if (i >= 16) {
            this.e.add(bl0.f(this.f284a, aVar));
        }
    }

    @DexIgnore
    public Notification c() {
        Bundle a2;
        RemoteViews e2;
        RemoteViews c2;
        zk0.f fVar = this.b.o;
        if (fVar != null) {
            fVar.b(this);
        }
        RemoteViews d2 = fVar != null ? fVar.d(this) : null;
        Notification d3 = d();
        if (d2 != null) {
            d3.contentView = d2;
        } else {
            RemoteViews remoteViews = this.b.F;
            if (remoteViews != null) {
                d3.contentView = remoteViews;
            }
        }
        if (!(Build.VERSION.SDK_INT < 16 || fVar == null || (c2 = fVar.c(this)) == null)) {
            d3.bigContentView = c2;
        }
        if (!(Build.VERSION.SDK_INT < 21 || fVar == null || (e2 = this.b.o.e(this)) == null)) {
            d3.headsUpContentView = e2;
        }
        if (!(Build.VERSION.SDK_INT < 16 || fVar == null || (a2 = zk0.a(d3)) == null)) {
            fVar.a(a2);
        }
        return d3;
    }

    @DexIgnore
    public Notification d() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            return this.f284a.build();
        }
        if (i >= 24) {
            Notification build = this.f284a.build();
            if (this.g == 0) {
                return build;
            }
            if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.g != 2)) {
                e(build);
            }
            if (!(build.getGroup() != null && (build.flags & 512) == 0 && this.g == 1)) {
                return build;
            }
            e(build);
            return build;
        } else if (i >= 21) {
            this.f284a.setExtras(this.f);
            Notification build2 = this.f284a.build();
            RemoteViews remoteViews = this.c;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.d;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.h;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.g == 0) {
                return build2;
            }
            if (!(build2.getGroup() == null || (build2.flags & 512) == 0 || this.g != 2)) {
                e(build2);
            }
            if (!(build2.getGroup() != null && (build2.flags & 512) == 0 && this.g == 1)) {
                return build2;
            }
            e(build2);
            return build2;
        } else if (i >= 20) {
            this.f284a.setExtras(this.f);
            Notification build3 = this.f284a.build();
            RemoteViews remoteViews4 = this.c;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            RemoteViews remoteViews5 = this.d;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.g == 0) {
                return build3;
            }
            if (!(build3.getGroup() == null || (build3.flags & 512) == 0 || this.g != 2)) {
                e(build3);
            }
            if (!(build3.getGroup() != null && (build3.flags & 512) == 0 && this.g == 1)) {
                return build3;
            }
            e(build3);
            return build3;
        } else if (i >= 19) {
            SparseArray<Bundle> a2 = bl0.a(this.e);
            if (a2 != null) {
                this.f.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.f284a.setExtras(this.f);
            Notification build4 = this.f284a.build();
            RemoteViews remoteViews6 = this.c;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            RemoteViews remoteViews7 = this.d;
            if (remoteViews7 == null) {
                return build4;
            }
            build4.bigContentView = remoteViews7;
            return build4;
        } else if (i < 16) {
            return this.f284a.getNotification();
        } else {
            Notification build5 = this.f284a.build();
            Bundle a3 = zk0.a(build5);
            Bundle bundle = new Bundle(this.f);
            for (String str : this.f.keySet()) {
                if (a3.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a3.putAll(bundle);
            SparseArray<Bundle> a4 = bl0.a(this.e);
            if (a4 != null) {
                zk0.a(build5).putSparseParcelableArray("android.support.actionExtras", a4);
            }
            RemoteViews remoteViews8 = this.c;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            RemoteViews remoteViews9 = this.d;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        }
    }

    @DexIgnore
    public final void e(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        int i = notification.defaults & -2;
        notification.defaults = i;
        notification.defaults = i & -3;
    }
}
