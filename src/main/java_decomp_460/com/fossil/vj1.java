package com.fossil;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj1 implements mb1 {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ mb1 c;

    @DexIgnore
    public vj1(int i, mb1 mb1) {
        this.b = i;
        this.c = mb1;
    }

    @DexIgnore
    public static mb1 c(Context context) {
        return new vj1(context.getResources().getConfiguration().uiMode & 48, wj1.c(context));
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof vj1)) {
            return false;
        }
        vj1 vj1 = (vj1) obj;
        return this.b == vj1.b && this.c.equals(vj1.c);
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return jk1.n(this.c, this.b);
    }
}
