package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py4 {
    @DexIgnore
    public static final ps4 a(ps4 ps4) {
        pq7.c(ps4, "$this$clone");
        return new ps4(ps4.f(), ps4.r(), ps4.g(), ps4.c(), ps4.i(), ps4.h(), ps4.m(), ps4.e(), ps4.q(), ps4.d(), ps4.k(), ps4.t(), ps4.n(), ps4.p(), ps4.b(), ps4.s(), ps4.a());
    }

    @DexIgnore
    public static final bt4 b(bt4 bt4, List<ms4> list) {
        pq7.c(bt4, "$this$clone");
        pq7.c(list, "list");
        return new bt4(bt4.g(), bt4.q(), bt4.h(), bt4.d(), bt4.j(), bt4.i(), bt4.m(), bt4.f(), bt4.o(), bt4.e(), bt4.l(), bt4.s(), bt4.n(), list, bt4.p(), bt4.c(), bt4.b(), bt4.r(), bt4.a(), false, 524288, null);
    }

    @DexIgnore
    public static final boolean c(ts4 ts4, ts4 ts42) {
        pq7.c(ts4, "$this$isEditableFieldTheSame");
        pq7.c(ts42, "draft");
        if (!(!pq7.a(ts4.e(), ts42.e()))) {
            String a2 = ts4.a();
            if (a2 == null) {
                a2 = "";
            }
            String a3 = ts42.a();
            if (a3 == null) {
                a3 = "";
            }
            return !(pq7.a(a2, a3) ^ true) && ts4.i() == ts42.i() && ts4.b() == ts42.b();
        }
    }

    @DexIgnore
    public static final List<Object> d(List<ms4> list, List<ms4> list2) {
        pq7.c(list, "$this$toAllPlayerWithHeader");
        ArrayList arrayList = new ArrayList();
        if (list2 == null || list2.isEmpty()) {
            arrayList.addAll(list);
        } else {
            String c = um5.c(PortfolioApp.h0.c(), 2131886318);
            pq7.b(c, "LanguageHelper.getString\u2026rd_Subtitle__Top3Players)");
            arrayList.add(c);
            arrayList.addAll(list);
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886317);
            pq7.b(c2, "LanguageHelper.getString\u2026btitle__CloseCompetitors)");
            arrayList.add(c2);
            arrayList.addAll(list2);
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<gs4> e(List<? extends Date> list, Date date) {
        String format;
        pq7.c(list, "$this$toBottomDialogModels");
        pq7.c(date, "currentDate");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            boolean m0 = lk5.m0(t, date);
            String i0 = lk5.i0(t);
            long time = (t.getTime() - date.getTime()) / ((long) 60000);
            if (m0) {
                hr7 hr7 = hr7.f1520a;
                String string = PortfolioApp.h0.c().getResources().getString(2131886345);
                pq7.b(string, "PortfolioApp.instance.re\u2026l__AtTimeInNumberMinutes)");
                format = String.format(string, Arrays.copyOf(new Object[]{i0, Long.valueOf(time)}, 2));
                pq7.b(format, "java.lang.String.format(format, *args)");
            } else {
                hr7 hr72 = hr7.f1520a;
                String string2 = PortfolioApp.h0.c().getResources().getString(2131886346);
                pq7.b(string2, "PortfolioApp.instance.re\u2026orrowTimeInNumberMinutes)");
                format = String.format(string2, Arrays.copyOf(new Object[]{i0, Long.valueOf(time)}, 2));
                pq7.b(format, "java.lang.String.format(format, *args)");
            }
            arrayList.add(new gs4(format, t, false));
        }
        if (!arrayList.isEmpty()) {
            ((gs4) arrayList.get(0)).d(true);
        }
        return arrayList;
    }

    @DexIgnore
    public static final cl7<String[], Long[]> f(List<? extends Date> list) {
        pq7.c(list, "$this$toDateStringArray");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (T t : list) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(t);
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            Date time = instance.getTime();
            pq7.b(time, "calendar.time");
            long time2 = time.getTime();
            String c = lk5.m0(t, new Date()) ? um5.c(PortfolioApp.h0.c(), 2131886665) : lk5.d(t);
            pq7.b(c, LegacyTokenHelper.TYPE_STRING);
            arrayList.add(c);
            arrayList2.add(Long.valueOf(time2));
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Object[] array2 = arrayList2.toArray(new Long[0]);
            if (array2 != null) {
                return new cl7<>(array, array2);
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public static final ts4 g(ps4 ps4) {
        pq7.c(ps4, "$this$toDraft");
        String f = ps4.f();
        String g = ps4.g();
        if (g == null) {
            g = "";
        }
        String c = ps4.c();
        String r = ps4.r();
        if (r == null) {
            r = "activity_reach_goal";
        }
        String k = ps4.k();
        if (k == null) {
            k = "public_with_friend";
        }
        Integer q = ps4.q();
        int intValue = q != null ? q.intValue() : 15000;
        Integer d = ps4.d();
        return new ts4(f, g, c, r, k, intValue, d != null ? d.intValue() : 259200, null, null, false, null, 1920, null);
    }

    @DexIgnore
    public static final List<at4> h(List<ms4> list) {
        pq7.c(list, "$this$toFriendUI");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            String d = t.d();
            String c = t.c();
            if (c == null) {
                c = "";
            }
            String e = t.e();
            if (e == null) {
                e = "";
            }
            String i = t.i();
            String g = t.g();
            if (g == null) {
                g = "";
            }
            arrayList.add(new at4(d, c, e, i, g));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<at4> i(List<xs4> list, List<ms4> list2) {
        T t;
        pq7.c(list, "$this$toFriendsIn");
        pq7.c(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        if (list.size() == list2.size() - 1) {
            for (T t2 : list) {
                String d = t2.d();
                String b = t2.b();
                if (b == null) {
                    b = "";
                }
                String e = t2.e();
                if (e == null) {
                    e = "";
                }
                String i = t2.i();
                String h = t2.h();
                if (h == null) {
                    h = "";
                }
                arrayList.add(new at4(d, b, e, i, h));
            }
        } else {
            for (T t3 : list) {
                Iterator<T> it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.d(), t3.d())) {
                        t = next;
                        break;
                    }
                }
                if (t != null) {
                    String d2 = t3.d();
                    String b2 = t3.b();
                    if (b2 == null) {
                        b2 = "";
                    }
                    String e2 = t3.e();
                    if (e2 == null) {
                        e2 = "";
                    }
                    String i2 = t3.i();
                    String h2 = t3.h();
                    if (h2 == null) {
                        h2 = "";
                    }
                    arrayList.add(new at4(d2, b2, e2, i2, h2));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final cl7<List<at4>, List<at4>> j(List<xs4> list, List<ms4> list2) {
        T t;
        pq7.c(list, "$this$toPairFriendInAndMember");
        pq7.c(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : list2) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.d(), t2.d())) {
                    t = next;
                    break;
                }
            }
            if (t == null) {
                String d = t2.d();
                String c = t2.c();
                if (c == null) {
                    c = "";
                }
                String e = t2.e();
                if (e == null) {
                    e = "";
                }
                String i = t2.i();
                String g = t2.g();
                if (g == null) {
                    g = "";
                }
                arrayList2.add(new at4(d, c, e, i, g));
            } else {
                String d2 = t2.d();
                String c2 = t2.c();
                if (c2 == null) {
                    c2 = "";
                }
                String e2 = t2.e();
                if (e2 == null) {
                    e2 = "";
                }
                String i2 = t2.i();
                String g2 = t2.g();
                if (g2 == null) {
                    g2 = "";
                }
                arrayList.add(new at4(d2, c2, e2, i2, g2));
            }
        }
        return hl7.a(arrayList, arrayList2);
    }

    @DexIgnore
    public static final List<Object> k(List<mt4> list) {
        int i = 0;
        pq7.c(list, "$this$toRecommendedChallenges");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        boolean z = false;
        for (T t : list) {
            if (i >= 0) {
                T t2 = t;
                if (i == 0) {
                    if (pq7.a(t2.a(), "pending-invitation")) {
                        String c = um5.c(PortfolioApp.h0.c(), 2131886245);
                        pq7.b(c, "LanguageHelper.getString\u2026Title__PendingInvitation)");
                        arrayList.add(c);
                    } else {
                        String c2 = um5.c(PortfolioApp.h0.c(), 2131886244);
                        pq7.b(c2, "LanguageHelper.getString\u2026tle__AvailableChallenges)");
                        arrayList.add(c2);
                        z = true;
                    }
                } else if (!z && pq7.a(t2.a(), "available-challenge")) {
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886244);
                    pq7.b(c3, "LanguageHelper.getString\u2026tle__AvailableChallenges)");
                    arrayList.add(c3);
                    z = true;
                }
                arrayList.add(t2);
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<qt4> l(List<bt4> list) {
        T t;
        T t2;
        pq7.c(list, "$this$toUIHistories");
        ArrayList arrayList = new ArrayList();
        for (T t3 : list) {
            Iterator<T> it = t3.k().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                Integer h = next.h();
                boolean z = true;
                if (h == null || 1 != h.intValue()) {
                    z = false;
                    continue;
                }
                if (z) {
                    t = next;
                    break;
                }
            }
            T t4 = t;
            Iterator<T> it2 = t3.k().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t2 = null;
                    break;
                }
                T next2 = it2.next();
                if (pq7.a(PortfolioApp.h0.c().l0(), next2.d())) {
                    t2 = next2;
                    break;
                }
            }
            arrayList.add(new qt4(new ps4(t3.g(), t3.q(), t3.h(), t3.d(), t3.j(), t3.i(), t3.m(), t3.f(), t3.o(), t3.e(), t3.l(), t3.s(), t3.n(), null, t3.b(), t3.r(), null, 65536, null), t4, t2, t3.t()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<ot4> m(List<xs4> list, List<ms4> list2) {
        T t;
        pq7.c(list, "$this$toUnInvitedFriends");
        pq7.c(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.d(), t2.d())) {
                    t = next;
                    break;
                }
            }
            if (t == null) {
                arrayList.add(new ot4(t2.d(), t2.i(), t2.b(), t2.e(), t2.h(), false, 32, null));
            }
        }
        return arrayList;
    }
}
