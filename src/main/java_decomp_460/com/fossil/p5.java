package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class p5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2777a;

    /*
    static {
        int[] iArr = new int[f7.values().length];
        f2777a = iArr;
        iArr[f7.SUCCESS.ordinal()] = 1;
        f2777a[f7.HID_PROXY_NOT_CONNECTED.ordinal()] = 2;
        f2777a[f7.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.ordinal()] = 3;
        f2777a[f7.HID_INPUT_DEVICE_DISABLED.ordinal()] = 4;
        f2777a[f7.HID_UNKNOWN_ERROR.ordinal()] = 5;
        f2777a[f7.BLUETOOTH_OFF.ordinal()] = 6;
    }
    */
}
