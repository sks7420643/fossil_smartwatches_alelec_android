package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum tn1 {
    CLEAR_DAY,
    CLEAR_NIGHT,
    CLOUDY,
    PARTLY_CLOUDY_DAY,
    PARTLY_CLOUDY_NIGHT,
    RAIN,
    SNOW,
    SLEET,
    STORMY,
    FOG,
    WIND;
    
    @DexIgnore
    public /* final */ int b; // = ordinal();

    @DexIgnore
    public tn1() {
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
