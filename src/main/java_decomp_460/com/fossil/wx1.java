package com.fossil;

import com.fossil.ix1;
import com.fossil.sx1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wx1<T> extends rx1<T> {
    @DexIgnore
    public /* final */ ix1.a b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wx1(byte b2, byte b3, ry1 ry1) {
        super(ry1);
        pq7.c(ry1, "baseVersion");
        this.c = (byte) b2;
        this.d = (byte) b3;
        this.b = ix1.a.CRC32;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wx1(byte b2, byte b3, ry1 ry1, int i, kq7 kq7) {
        this(b2, (i & 2) != 0 ? (byte) 255 : b3, (i & 4) != 0 ? new ry1(1, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.rx1
    public T b(byte[] bArr) throws sx1 {
        pq7.c(bArr, "data");
        if (e(bArr)) {
            return d(bArr);
        }
        throw new sx1(sx1.a.INVALID_FILE_DATA, "Invalid file.", null, 4, null);
    }

    @DexIgnore
    public ix1.a c() {
        return this.b;
    }

    @DexIgnore
    public abstract T d(byte[] bArr) throws sx1;

    @DexIgnore
    public boolean e(byte[] bArr) {
        pq7.c(bArr, "fileData");
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        if (this.c != order.get(1)) {
            return false;
        }
        byte b2 = order.get(0);
        byte b3 = this.d;
        if ((b3 != ((byte) 255) && b3 != b2) || new ry1(bArr[2], bArr[3]).getMajor() != a().getMajor() || hy1.o(order.getInt(8)) != hy1.o((bArr.length - 12) - 4)) {
            return false;
        }
        return hy1.o(order.getInt(bArr.length + -4)) == ix1.f1688a.b(dm7.k(bArr, 12, bArr.length + -4), c());
    }
}
