package com.fossil;

import android.os.Bundle;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sm6 extends fq4 {
    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE n();

    @DexIgnore
    public abstract void o(Bundle bundle);

    @DexIgnore
    public abstract void p(Date date);

    @DexIgnore
    public abstract void q();

    @DexIgnore
    public abstract void r();
}
