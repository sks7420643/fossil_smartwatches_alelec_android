package com.fossil;

import android.graphics.Bitmap;
import com.fossil.gg1;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg1 implements qb1<InputStream, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gg1 f3257a;
    @DexIgnore
    public /* final */ od1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements gg1.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ qg1 f3258a;
        @DexIgnore
        public /* final */ ck1 b;

        @DexIgnore
        public a(qg1 qg1, ck1 ck1) {
            this.f3258a = qg1;
            this.b = ck1;
        }

        @DexIgnore
        @Override // com.fossil.gg1.b
        public void a(rd1 rd1, Bitmap bitmap) throws IOException {
            IOException a2 = this.b.a();
            if (a2 != null) {
                if (bitmap != null) {
                    rd1.b(bitmap);
                }
                throw a2;
            }
        }

        @DexIgnore
        @Override // com.fossil.gg1.b
        public void b() {
            this.f3258a.b();
        }
    }

    @DexIgnore
    public sg1(gg1 gg1, od1 od1) {
        this.f3257a = gg1;
        this.b = od1;
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(InputStream inputStream, int i, int i2, ob1 ob1) throws IOException {
        boolean z;
        qg1 qg1;
        if (inputStream instanceof qg1) {
            qg1 = (qg1) inputStream;
            z = false;
        } else {
            z = true;
            qg1 = new qg1(inputStream, this.b);
        }
        ck1 b2 = ck1.b(qg1);
        try {
            return this.f3257a.g(new gk1(b2), i, i2, ob1, new a(qg1, b2));
        } finally {
            b2.c();
            if (z) {
                qg1.c();
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(InputStream inputStream, ob1 ob1) {
        return this.f3257a.p(inputStream);
    }
}
