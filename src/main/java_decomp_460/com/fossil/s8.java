package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s8 {
    @DexIgnore
    public /* synthetic */ s8(kq7 kq7) {
    }

    @DexIgnore
    public final t8 a(byte b) {
        t8[] values = t8.values();
        for (t8 t8Var : values) {
            if (t8Var.b == b) {
                return t8Var;
            }
        }
        return null;
    }
}
