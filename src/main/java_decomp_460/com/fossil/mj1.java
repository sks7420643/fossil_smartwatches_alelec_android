package com.fossil;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mj1 extends nj1<Drawable> {
    @DexIgnore
    public mj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: q */
    public void o(Drawable drawable) {
        ((ImageView) this.b).setImageDrawable(drawable);
    }
}
