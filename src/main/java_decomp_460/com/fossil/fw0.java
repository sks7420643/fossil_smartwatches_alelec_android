package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f1218a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fw0$a$a")
        @eo7(c = "androidx.room.CoroutinesRoom$Companion$execute$2", f = "CoroutinesRoom.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.fw0$a$a  reason: collision with other inner class name */
        public static final class C0091a extends ko7 implements vp7<iv7, qn7<? super R>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Callable $callable;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0091a(Callable callable, qn7 qn7) {
                super(2, qn7);
                this.$callable = callable;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0091a aVar = new C0091a(this.$callable, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, Object obj) {
                return ((C0091a) create(iv7, (qn7) obj)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.$callable.call();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final <R> Object a(qw0 qw0, boolean z, Callable<R> callable, qn7<? super R> qn7) {
            rn7 b;
            if (qw0.isOpen() && qw0.inTransaction()) {
                return callable.call();
            }
            yw0 yw0 = (yw0) qn7.getContext().get(yw0.c);
            if (yw0 == null || (b = yw0.b()) == null) {
                b = z ? gw0.b(qw0) : gw0.a(qw0);
            }
            return eu7.g(b, new C0091a(callable, null), qn7);
        }
    }

    @DexIgnore
    public static final <R> Object a(qw0 qw0, boolean z, Callable<R> callable, qn7<? super R> qn7) {
        return f1218a.a(qw0, z, callable, qn7);
    }
}
