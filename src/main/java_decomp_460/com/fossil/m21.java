package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m21 extends k21<f21> {
    @DexIgnore
    public static /* final */ String e; // = x01.f("NetworkMeteredCtrlr");

    @DexIgnore
    public m21(Context context, k41 k41) {
        super(w21.c(context, k41).d());
    }

    @DexIgnore
    @Override // com.fossil.k21
    public boolean b(o31 o31) {
        return o31.j.b() == y01.METERED;
    }

    @DexIgnore
    /* renamed from: i */
    public boolean c(f21 f21) {
        if (Build.VERSION.SDK_INT >= 26) {
            return !f21.a() || !f21.b();
        }
        x01.c().a(e, "Metered network constraint is not supported before API 26, only checking for connected state.", new Throwable[0]);
        return !f21.a();
    }
}
