package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a65 extends z55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131362546, 1);
        z.put(2131362832, 2);
        z.put(2131362901, 3);
        z.put(2131362903, 4);
        z.put(2131362902, 5);
        z.put(2131362285, 6);
    }
    */

    @DexIgnore
    public a65(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 7, y, z));
    }

    @DexIgnore
    public a65(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[6], (FlexibleTextView) objArr[1], (LinearLayout) objArr[2], (NumberPicker) objArr[3], (NumberPicker) objArr[5], (NumberPicker) objArr[4], (ConstraintLayout) objArr[0]);
        this.x = -1;
        this.w.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.x != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.x = 1;
        }
        w();
    }
}
