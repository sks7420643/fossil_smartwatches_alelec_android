package com.fossil;

import androidx.lifecycle.CompositeGeneratedAdaptersObserver;
import androidx.lifecycle.FullLifecycleObserverAdapter;
import androidx.lifecycle.ReflectiveGenericLifecycleObserver;
import androidx.lifecycle.SingleGeneratedAdapterObserver;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gs0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static Map<Class<?>, Integer> f1350a; // = new HashMap();
    @DexIgnore
    public static Map<Class<?>, List<Constructor<? extends vr0>>> b; // = new HashMap();

    @DexIgnore
    public static vr0 a(Constructor<? extends vr0> constructor, Object obj) {
        try {
            return (vr0) constructor.newInstance(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }

    @DexIgnore
    public static Constructor<? extends vr0> b(Class<?> cls) {
        try {
            Package r1 = cls.getPackage();
            String canonicalName = cls.getCanonicalName();
            String name = r1 != null ? r1.getName() : "";
            if (!name.isEmpty()) {
                canonicalName = canonicalName.substring(name.length() + 1);
            }
            String c = c(canonicalName);
            if (!name.isEmpty()) {
                c = name + CodelessMatcher.CURRENT_CLASS_NAME + c;
            }
            Constructor declaredConstructor = Class.forName(c).getDeclaredConstructor(cls);
            if (declaredConstructor.isAccessible()) {
                return declaredConstructor;
            }
            declaredConstructor.setAccessible(true);
            return declaredConstructor;
        } catch (ClassNotFoundException e) {
            return null;
        } catch (NoSuchMethodException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public static String c(String str) {
        return str.replace(CodelessMatcher.CURRENT_CLASS_NAME, LocaleConverter.LOCALE_DELIMITER) + "_LifecycleAdapter";
    }

    @DexIgnore
    public static int d(Class<?> cls) {
        Integer num = f1350a.get(cls);
        if (num != null) {
            return num.intValue();
        }
        int g = g(cls);
        f1350a.put(cls, Integer.valueOf(g));
        return g;
    }

    @DexIgnore
    public static boolean e(Class<?> cls) {
        return cls != null && cs0.class.isAssignableFrom(cls);
    }

    @DexIgnore
    public static as0 f(Object obj) {
        boolean z = obj instanceof as0;
        boolean z2 = obj instanceof ur0;
        if (z && z2) {
            return new FullLifecycleObserverAdapter((ur0) obj, (as0) obj);
        }
        if (z2) {
            return new FullLifecycleObserverAdapter((ur0) obj, null);
        }
        if (z) {
            return (as0) obj;
        }
        Class<?> cls = obj.getClass();
        if (d(cls) != 2) {
            return new ReflectiveGenericLifecycleObserver(obj);
        }
        List<Constructor<? extends vr0>> list = b.get(cls);
        if (list.size() == 1) {
            return new SingleGeneratedAdapterObserver(a(list.get(0), obj));
        }
        vr0[] vr0Arr = new vr0[list.size()];
        for (int i = 0; i < list.size(); i++) {
            vr0Arr[i] = a(list.get(i), obj);
        }
        return new CompositeGeneratedAdaptersObserver(vr0Arr);
    }

    @DexIgnore
    public static int g(Class<?> cls) {
        if (cls.getCanonicalName() == null) {
            return 1;
        }
        Constructor<? extends vr0> b2 = b(cls);
        if (b2 != null) {
            b.put(cls, Collections.singletonList(b2));
            return 2;
        } else if (kr0.c.d(cls)) {
            return 1;
        } else {
            Class<? super Object> superclass = cls.getSuperclass();
            ArrayList arrayList = null;
            if (e(superclass)) {
                if (d(superclass) == 1) {
                    return 1;
                }
                arrayList = new ArrayList(b.get(superclass));
            }
            Class<?>[] interfaces = cls.getInterfaces();
            for (Class<?> cls2 : interfaces) {
                if (e(cls2)) {
                    if (d(cls2) == 1) {
                        return 1;
                    }
                    ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                    arrayList2.addAll(b.get(cls2));
                    arrayList = arrayList2;
                }
            }
            if (arrayList == null) {
                return 1;
            }
            b.put(cls, arrayList);
            return 2;
        }
    }
}
