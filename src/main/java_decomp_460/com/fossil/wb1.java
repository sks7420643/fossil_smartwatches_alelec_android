package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wb1<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void b(Exception exc);

        @DexIgnore
        void e(T t);
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    gb1 c();

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    void d(sa1 sa1, a<? super T> aVar);

    @DexIgnore
    Class<T> getDataClass();
}
