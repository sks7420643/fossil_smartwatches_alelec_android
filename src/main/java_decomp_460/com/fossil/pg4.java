package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class pg4 implements Runnable {
    @DexIgnore
    public /* final */ tg4 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public pg4(tg4 tg4, boolean z) {
        this.b = tg4;
        this.c = z;
    }

    @DexIgnore
    public static Runnable a(tg4 tg4, boolean z) {
        return new pg4(tg4, z);
    }

    @DexIgnore
    public void run() {
        this.b.e(this.c);
    }
}
