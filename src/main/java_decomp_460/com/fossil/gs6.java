package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs6 extends ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f1353a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a(null, null, null, null, 15, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f1354a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        public a() {
            this(null, null, null, null, 15, null);
        }

        @DexIgnore
        public a(Integer num, Integer num2, Integer num3, Integer num4) {
            this.f1354a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Integer num, Integer num2, Integer num3, Integer num4, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4);
        }

        @DexIgnore
        public static /* synthetic */ void f(a aVar, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            aVar.e(num, num2, num3, num4);
        }

        @DexIgnore
        public final Integer a() {
            return this.f1354a;
        }

        @DexIgnore
        public final Integer b() {
            return this.c;
        }

        @DexIgnore
        public final Integer c() {
            return this.b;
        }

        @DexIgnore
        public final Integer d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Integer num, Integer num2, Integer num3, Integer num4) {
            this.f1354a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonViewModel$saveColor$1", f = "CustomizeButtonViewModel.kt", l = {56, 57, 79}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $primaryColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $secondaryColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gs6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gs6 gs6, String str, String str2, String str3, qn7 qn7) {
            super(2, qn7);
            this.this$0 = gs6;
            this.$id = str;
            this.$primaryColor = str2;
            this.$secondaryColor = str3;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, this.$primaryColor, this.$secondaryColor, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0048  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00ad  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d6  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00f1  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0158  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x015b  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 351
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gs6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = gs6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeButtonViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public gs6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.f1353a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<a> e() {
        return this.f1353a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = d;
        local.d(str4, "saveColor primaryColor=" + str2 + " secondaryColor=" + str3);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, str, str2, str3, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = ds6.s.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String b2 = ds6.s.b();
        int parseColor2 = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        this.b.e(Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor), Integer.valueOf(parseColor2));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        if (i == 201) {
            a.f(this.b, Integer.valueOf(i2), null, Integer.valueOf(i2), null, 10, null);
            d();
        } else if (i == 203) {
            a.f(this.b, null, Integer.valueOf(i2), null, Integer.valueOf(i2), 5, null);
            d();
        }
    }
}
