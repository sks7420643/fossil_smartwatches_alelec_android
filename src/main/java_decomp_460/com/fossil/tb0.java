package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb0 extends qq7 implements rp7<rl1, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tb0(ArrayList arrayList) {
        super(1);
        this.b = arrayList;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public Boolean invoke(rl1 rl1) {
        Object obj;
        at1 a2;
        boolean z;
        rl1 rl12 = rl1;
        Iterator it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            Object next = it.next();
            dm1 dm1 = (dm1) next;
            if (dm1.getPositionConfig().getDistanceFromCenter() == rl12.getPositionConfig().getDistanceFromCenter() && dm1.getPositionConfig().getAngle() == rl12.getPositionConfig().getAngle()) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                obj = next;
                break;
            }
        }
        dm1 dm12 = (dm1) obj;
        return Boolean.valueOf((dm12 == null || (a2 = dm12.a()) == null) ? false : a2.a());
    }
}
