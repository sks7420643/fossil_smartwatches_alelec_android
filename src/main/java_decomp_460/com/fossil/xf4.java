package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xf4 implements ft3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bg4 f4114a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public xf4(bg4 bg4, Bundle bundle) {
        this.f4114a = bg4;
        this.b = bundle;
    }

    @DexIgnore
    @Override // com.fossil.ft3
    public final Object then(nt3 nt3) {
        return this.f4114a.i(this.b, nt3);
    }
}
