package com.fossil;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y33 extends AbstractList<String> implements w13, RandomAccess {
    @DexIgnore
    public /* final */ w13 b;

    @DexIgnore
    public y33(w13 w13) {
        this.b = w13;
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final void V(xz2 xz2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ String get(int i) {
        return (String) this.b.get(i);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.lang.Iterable
    public final Iterator<String> iterator() {
        return new a43(this);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final ListIterator<String> listIterator(int i) {
        return new b43(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final Object zzb(int i) {
        return this.b.zzb(i);
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final List<?> zzd() {
        return this.b.zzd();
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final w13 zze() {
        return this;
    }
}
