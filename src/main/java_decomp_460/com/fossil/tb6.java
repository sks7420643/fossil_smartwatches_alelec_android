package com.fossil;

import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb6 implements Factory<sb6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<HybridPresetRepository> f3391a;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> c;

    @DexIgnore
    public tb6(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        this.f3391a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static tb6 a(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        return new tb6(provider, provider2, provider3);
    }

    @DexIgnore
    public static sb6 c(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        return new sb6(hybridPresetRepository, microAppLastSettingRepository, microAppRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public sb6 get() {
        return c(this.f3391a.get(), this.b.get(), this.c.get());
    }
}
