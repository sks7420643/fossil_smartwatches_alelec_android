package com.fossil;

import android.content.Context;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3897a;
    @DexIgnore
    public /* final */ j64 b;
    @DexIgnore
    public /* final */ c94 c;
    @DexIgnore
    public /* final */ long d; // = System.currentTimeMillis();
    @DexIgnore
    public x84 e;
    @DexIgnore
    public x84 f;
    @DexIgnore
    public u84 g;
    @DexIgnore
    public /* final */ h94 h;
    @DexIgnore
    public /* final */ i84 i;
    @DexIgnore
    public /* final */ b84 j;
    @DexIgnore
    public ExecutorService k;
    @DexIgnore
    public s84 l;
    @DexIgnore
    public w74 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<nt3<Void>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ rc4 f3898a;

        @DexIgnore
        public a(rc4 rc4) {
            this.f3898a = rc4;
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Void> call() throws Exception {
            return w84.this.f(this.f3898a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ rc4 b;

        @DexIgnore
        public b(rc4 rc4) {
            this.b = rc4;
        }

        @DexIgnore
        public void run() {
            w84.this.f(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Boolean> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() throws Exception {
            try {
                boolean d = w84.this.e.d();
                x74 f = x74.f();
                f.b("Initialization marker file removed: " + d);
                return Boolean.valueOf(d);
            } catch (Exception e) {
                x74.f().e("Problem encountered deleting Crashlytics initialization marker.", e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Boolean> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() throws Exception {
            return Boolean.valueOf(w84.this.g.H());
        }
    }

    @DexIgnore
    public w84(j64 j64, h94 h94, w74 w74, c94 c94, i84 i84, b84 b84, ExecutorService executorService) {
        this.b = j64;
        this.c = c94;
        this.f3897a = j64.g();
        this.h = h94;
        this.m = w74;
        this.i = i84;
        this.j = b84;
        this.k = executorService;
        this.l = new s84(executorService);
    }

    @DexIgnore
    public static String i() {
        return "17.1.1";
    }

    @DexIgnore
    public static boolean j(String str, boolean z) {
        if (!z) {
            x74.f().b("Configured not to require a build ID.");
            return true;
        } else if (!r84.D(str)) {
            return true;
        } else {
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".     |  | ");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".   \\ |  | /");
            Log.e("FirebaseCrashlytics", ".    \\    /");
            Log.e("FirebaseCrashlytics", ".     \\  /");
            Log.e("FirebaseCrashlytics", ".      \\/");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".      /\\");
            Log.e("FirebaseCrashlytics", ".     /  \\");
            Log.e("FirebaseCrashlytics", ".    /    \\");
            Log.e("FirebaseCrashlytics", ".   / |  | \\");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }

    @DexIgnore
    public final void d() {
        try {
            Boolean.TRUE.equals((Boolean) t94.a(this.l.h(new d())));
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public boolean e() {
        return this.e.c();
    }

    @DexIgnore
    public final nt3<Void> f(rc4 rc4) {
        m();
        this.g.B();
        try {
            this.i.a(v84.b(this));
            zc4 b2 = rc4.b();
            if (!b2.a().f4097a) {
                x74.f().b("Collection of crash reports disabled in Crashlytics settings.");
                return qt3.e(new RuntimeException("Collection of crash reports disabled in Crashlytics settings."));
            }
            if (!this.g.P(b2.b().f4303a)) {
                x74.f().b("Could not finalize previous sessions.");
            }
            nt3<Void> x0 = this.g.x0(1.0f, rc4.a());
            l();
            return x0;
        } catch (Exception e2) {
            x74.f().e("Crashlytics encountered a problem during asynchronous initialization.", e2);
            return qt3.e(e2);
        } finally {
            l();
        }
    }

    @DexIgnore
    public nt3<Void> g(rc4 rc4) {
        return t94.b(this.k, new a(rc4));
    }

    @DexIgnore
    public final void h(rc4 rc4) {
        Future<?> submit = this.k.submit(new b(rc4));
        x74.f().b("Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            x74.f().e("Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            x74.f().e("Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            x74.f().e("Crashlytics timed out during initialization.", e4);
        }
    }

    @DexIgnore
    public void k(String str) {
        this.g.Q0(System.currentTimeMillis() - this.d, str);
    }

    @DexIgnore
    public void l() {
        this.l.h(new c());
    }

    @DexIgnore
    public void m() {
        this.l.b();
        this.e.a();
        x74.f().b("Initialization marker file created.");
    }

    @DexIgnore
    public boolean n(rc4 rc4) {
        String p = r84.p(this.f3897a);
        x74 f2 = x74.f();
        f2.b("Mapping file ID is: " + p);
        if (j(p, r84.l(this.f3897a, "com.crashlytics.RequireBuildId", true))) {
            String c2 = this.b.j().c();
            try {
                x74 f3 = x74.f();
                f3.g("Initializing Crashlytics " + i());
                ub4 ub4 = new ub4(this.f3897a);
                this.f = new x84("crash_marker", ub4);
                this.e = new x84("initialization_marker", ub4);
                kb4 kb4 = new kb4();
                l84 a2 = l84.a(this.f3897a, this.h, c2, p);
                md4 md4 = new md4(this.f3897a);
                x74 f4 = x74.f();
                f4.b("Installer package name is: " + a2.c);
                this.g = new u84(this.f3897a, this.l, kb4, this.h, this.c, ub4, this.f, a2, null, null, this.m, md4, this.j, rc4);
                boolean e2 = e();
                d();
                this.g.M(Thread.getDefaultUncaughtExceptionHandler(), rc4);
                if (!e2 || !r84.c(this.f3897a)) {
                    x74.f().b("Exception handling initialization successful");
                    return true;
                }
                x74.f().b("Crashlytics did not finish previous background initialization. Initializing synchronously.");
                h(rc4);
                return false;
            } catch (Exception e3) {
                x74.f().e("Crashlytics was not started due to an exception during initialization", e3);
                this.g = null;
                return false;
            }
        } else {
            throw new IllegalStateException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    public void o(String str, String str2) {
        this.g.v0(str, str2);
    }

    @DexIgnore
    public void p(String str) {
        this.g.w0(str);
    }
}
