package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c08 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object[] f541a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ tn7 c;

    @DexIgnore
    public c08(tn7 tn7, int i) {
        this.c = tn7;
        this.f541a = new Object[i];
    }

    @DexIgnore
    public final void a(Object obj) {
        Object[] objArr = this.f541a;
        int i = this.b;
        this.b = i + 1;
        objArr[i] = obj;
    }

    @DexIgnore
    public final tn7 b() {
        return this.c;
    }

    @DexIgnore
    public final void c() {
        this.b = 0;
    }

    @DexIgnore
    public final Object d() {
        Object[] objArr = this.f541a;
        int i = this.b;
        this.b = i + 1;
        return objArr[i];
    }
}
