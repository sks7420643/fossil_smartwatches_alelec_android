package com.fossil;

import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ArrayList<String> f1640a; // = hm7.c("commute-time", "second-timezone");
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = hm7.c("commute-time", "weather", "chance-of-rain");
    @DexIgnore
    public static /* final */ ArrayList<String> c; // = hm7.c("steps", "calories", "active-minutes", Constants.BATTERY);
    @DexIgnore
    public static /* final */ ik5 d; // = new ik5();

    @DexIgnore
    public final String a(String str) {
        pq7.c(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 134170930 && str.equals("second-timezone")) {
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886533);
                pq7.b(c2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return c2;
            }
        } else if (str.equals("commute-time")) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886369);
            pq7.b(c3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return c3;
        }
        return "";
    }

    @DexIgnore
    public final List<String> b(String str) {
        pq7.c(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode == -829740640 ? !str.equals("commute-time") : hashCode == -48173007 ? !str.equals("chance-of-rain") : hashCode != 1223440372 || !str.equals("weather")) {
            return new ArrayList();
        }
        int i = Build.VERSION.SDK_INT;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "android.os.Build.VERSION.SDK_INT=" + i);
        if (i >= 29) {
            return hm7.c(InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION);
        }
        return hm7.c(InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE);
    }

    @DexIgnore
    public final boolean c(String str) {
        pq7.c(str, "complicationId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        pq7.c(str, "complicationId");
        return f1640a.contains(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        pq7.c(str, "complicationId");
        return c.contains(str);
    }

    @DexIgnore
    public final boolean f(String str) {
        pq7.c(str, "complicationId");
        List<String> b2 = b(str);
        String[] a2 = g47.f1261a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "isPermissionGrantedForComplication " + str + " granted=" + a2 + " required=" + b2);
        Iterator<T> it = b2.iterator();
        while (it.hasNext()) {
            if (!em7.B(a2, it.next())) {
                return false;
            }
        }
        return true;
    }
}
