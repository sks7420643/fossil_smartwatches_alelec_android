package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c90 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f584a; // = e.a("UUID.randomUUID().toString()");
    @DexIgnore
    public static /* final */ Hashtable<String, zk1> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
    @DexIgnore
    public static e90 d; // = new e90(id0.i.g(), id0.i.h(), id0.i.l(), id0.i.k(), "5.13.5-production-release", null, 32);
    @DexIgnore
    public static /* final */ c90 e; // = new c90();

    /*
    static {
        String string;
        SharedPreferences b2 = g80.b(ld0.c);
        if (b2 != null && (string = b2.getString("log.device.mapping", null)) != null) {
            pq7.b(string, "getSharedPreferences(Sha\u2026_MAP_KEY, null) ?: return");
            String n = mx1.j.n(string);
            if (n != null) {
                try {
                    JSONObject jSONObject = new JSONObject(n);
                    b.clear();
                    b.putAll(e.c(jSONObject));
                } catch (Exception e2) {
                    d90.i.i(e2);
                }
            }
        }
    }
    */

    @DexIgnore
    public final e90 a() {
        return d;
    }

    @DexIgnore
    public final zk1 b(String str) {
        return b.get(str);
    }

    @DexIgnore
    public final HashMap<String, zk1> c(JSONObject jSONObject) {
        JSONObject optJSONObject;
        zk1 b2;
        HashMap<String, zk1> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        pq7.b(keys, "jsonObject.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            if (!(!BluetoothAdapter.checkBluetoothAddress(next) || (optJSONObject = jSONObject.optJSONObject(next)) == null || (b2 = zk1.u.b(optJSONObject)) == null)) {
                pq7.b(next, "key");
                hashMap.put(next, b2);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void d(String str, zk1 zk1) {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putString;
        zk1 zk12 = b.get(str);
        Hashtable<String, zk1> hashtable = b;
        if (zk12 != null) {
            zk1 = zk1.u.a(zk12, zk1);
        }
        hashtable.put(str, zk1);
        Hashtable<String, zk1> hashtable2 = b;
        JSONObject jSONObject = new JSONObject();
        for (T t : hashtable2.keySet()) {
            zk1 zk13 = hashtable2.get(t);
            if (zk13 != null) {
                jSONObject.put(t, zk13.toJSONObject());
            }
        }
        String jSONObject2 = jSONObject.toString();
        pq7.b(jSONObject2, "convertDeviceInfoMapToJS\u2026formationMaps).toString()");
        String o = mx1.j.o(jSONObject2);
        SharedPreferences b2 = g80.b(ld0.c);
        if (!(b2 == null || (edit = b2.edit()) == null || (putString = edit.putString("log.device.mapping", o)) == null)) {
            putString.apply();
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        c.put(str, str2);
    }

    @DexIgnore
    public final String f(String str) {
        String str2 = c.get(str);
        return str2 != null ? str2 : f584a;
    }
}
