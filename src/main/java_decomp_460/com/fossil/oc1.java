package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc1 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7530898992688511851L;

    @DexIgnore
    public oc1(Throwable th) {
        super("Unexpected exception thrown by non-Glide code", th);
    }
}
