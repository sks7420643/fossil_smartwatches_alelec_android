package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.b88;
import com.fossil.e88;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n88 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ n88 f2482a; // = e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends n88 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n88$a$a")
        /* renamed from: com.fossil.n88$a$a  reason: collision with other inner class name */
        public static class ExecutorC0167a implements Executor {
            @DexIgnore
            public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

            @DexIgnore
            public void execute(Runnable runnable) {
                this.b.post(runnable);
            }
        }

        @DexIgnore
        @Override // com.fossil.n88
        public List<? extends b88.a> a(Executor executor) {
            if (executor != null) {
                f88 f88 = new f88(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(f88);
                }
                return Arrays.asList(d88.f748a, f88);
            }
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.n88
        public Executor b() {
            return new ExecutorC0167a();
        }

        @DexIgnore
        @Override // com.fossil.n88
        public List<? extends e88.a> c() {
            return Build.VERSION.SDK_INT >= 24 ? Collections.singletonList(l88.f2159a) : Collections.emptyList();
        }

        @DexIgnore
        @Override // com.fossil.n88
        public int d() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        @DexIgnore
        @Override // com.fossil.n88
        @IgnoreJRERequirement
        public boolean h(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static class b extends n88 {
        @DexIgnore
        @Override // com.fossil.n88
        public List<? extends b88.a> a(Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(d88.f748a);
            arrayList.add(new f88(executor));
            return Collections.unmodifiableList(arrayList);
        }

        @DexIgnore
        @Override // com.fossil.n88
        public List<? extends e88.a> c() {
            return Collections.singletonList(l88.f2159a);
        }

        @DexIgnore
        @Override // com.fossil.n88
        public int d() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.n88
        public Object g(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            Constructor declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
            declaredConstructor.setAccessible(true);
            return ((MethodHandles.Lookup) declaredConstructor.newInstance(cls, -1)).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        @DexIgnore
        @Override // com.fossil.n88
        public boolean h(Method method) {
            return method.isDefault();
        }
    }

    @DexIgnore
    public static n88 e() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                return new a();
            }
        } catch (ClassNotFoundException e) {
        }
        try {
            Class.forName("java.util.Optional");
            return new b();
        } catch (ClassNotFoundException e2) {
            return new n88();
        }
    }

    @DexIgnore
    public static n88 f() {
        return f2482a;
    }

    @DexIgnore
    public List<? extends b88.a> a(Executor executor) {
        return Collections.singletonList(new f88(executor));
    }

    @DexIgnore
    public Executor b() {
        return null;
    }

    @DexIgnore
    public List<? extends e88.a> c() {
        return Collections.emptyList();
    }

    @DexIgnore
    public int d() {
        return 0;
    }

    @DexIgnore
    public Object g(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean h(Method method) {
        return false;
    }
}
