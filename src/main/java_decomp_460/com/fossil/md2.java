package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md2 extends ec2<nd2> {
    @DexIgnore
    public md2(Context context, Looper looper, ac2 ac2, r62.b bVar, r62.c cVar) {
        super(context, looper, 39, ac2, bVar, cVar);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        return queryLocalInterface instanceof nd2 ? (nd2) queryLocalInterface : new pd2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.common.service.START";
    }
}
