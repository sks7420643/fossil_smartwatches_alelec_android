package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class g12 implements Runnable {
    @DexIgnore
    public /* final */ i12 b;
    @DexIgnore
    public /* final */ h02 c;
    @DexIgnore
    public /* final */ zy1 d;
    @DexIgnore
    public /* final */ c02 e;

    @DexIgnore
    public g12(i12 i12, h02 h02, zy1 zy1, c02 c02) {
        this.b = i12;
        this.c = h02;
        this.d = zy1;
        this.e = c02;
    }

    @DexIgnore
    public static Runnable a(i12 i12, h02 h02, zy1 zy1, c02 c02) {
        return new g12(i12, h02, zy1, c02);
    }

    @DexIgnore
    public void run() {
        i12.c(this.b, this.c, this.d, this.e);
    }
}
