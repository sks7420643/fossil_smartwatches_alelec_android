package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js extends ss {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    public js(int i, k5 k5Var) {
        super(hs.g, k5Var);
        this.B = i;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.k1, Integer.valueOf(this.A));
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new h6(this.B, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void g(u5 u5Var) {
        this.A = ((h6) u5Var).k;
        this.g.add(new hw(0, null, null, g80.k(new JSONObject(), jd0.k1, Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.j1, Integer.valueOf(this.B));
    }
}
