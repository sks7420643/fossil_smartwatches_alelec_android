package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij4 {
    @DexIgnore
    public JsonElement a(JsonReader jsonReader) throws ej4, mj4 {
        boolean o = jsonReader.o();
        jsonReader.o0(true);
        try {
            JsonElement a2 = ek4.a(jsonReader);
            jsonReader.o0(o);
            return a2;
        } catch (StackOverflowError e) {
            throw new hj4("Failed parsing JSON source: " + jsonReader + " to Json", e);
        } catch (OutOfMemoryError e2) {
            throw new hj4("Failed parsing JSON source: " + jsonReader + " to Json", e2);
        } catch (Throwable th) {
            jsonReader.o0(o);
            throw th;
        }
    }

    @DexIgnore
    public JsonElement b(Reader reader) throws ej4, mj4 {
        try {
            JsonReader jsonReader = new JsonReader(reader);
            JsonElement a2 = a(jsonReader);
            if (a2.h() || jsonReader.V() == nk4.END_DOCUMENT) {
                return a2;
            }
            throw new mj4("Did not consume the entire document.");
        } catch (ok4 e) {
            throw new mj4(e);
        } catch (IOException e2) {
            throw new ej4(e2);
        } catch (NumberFormatException e3) {
            throw new mj4(e3);
        }
    }

    @DexIgnore
    public JsonElement c(String str) throws mj4 {
        return b(new StringReader(str));
    }
}
