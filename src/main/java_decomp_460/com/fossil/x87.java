package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4075a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public x87(String str, String str2) {
        pq7.c(str, "name");
        pq7.c(str2, "url");
        this.f4075a = str;
        this.b = str2;
    }

    @DexIgnore
    public final Bitmap a() {
        return b97.c.c(this.f4075a);
    }

    @DexIgnore
    public final String b() {
        return this.f4075a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof x87) {
                x87 x87 = (x87) obj;
                if (!pq7.a(this.f4075a, x87.f4075a) || !pq7.a(this.b, x87.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f4075a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "StickerInfo(name=" + this.f4075a + ", url=" + this.b + ")";
    }
}
