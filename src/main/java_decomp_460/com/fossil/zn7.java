package com.fossil;

import com.fossil.dl7;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zn7 implements qn7<Object>, do7, Serializable {
    @DexIgnore
    public /* final */ qn7<Object> completion;

    @DexIgnore
    public zn7(qn7<Object> qn7) {
        this.completion = qn7;
    }

    @DexIgnore
    public qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    @DexIgnore
    public qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }

    @DexIgnore
    @Override // com.fossil.do7
    public do7 getCallerFrame() {
        qn7<Object> qn7 = this.completion;
        if (!(qn7 instanceof do7)) {
            qn7 = null;
        }
        return (do7) qn7;
    }

    @DexIgnore
    public final qn7<Object> getCompletion() {
        return this.completion;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public abstract /* synthetic */ tn7 getContext();

    @DexIgnore
    @Override // com.fossil.do7
    public StackTraceElement getStackTraceElement() {
        return fo7.d(this);
    }

    @DexIgnore
    public abstract Object invokeSuspend(Object obj);

    @DexIgnore
    public void releaseIntercepted() {
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public final void resumeWith(Object obj) {
        while (true) {
            go7.b(this);
            qn7<Object> qn7 = this.completion;
            if (qn7 != null) {
                try {
                    Object invokeSuspend = this.invokeSuspend(obj);
                    if (invokeSuspend != yn7.d()) {
                        dl7.a aVar = dl7.Companion;
                        obj = dl7.m1constructorimpl(invokeSuspend);
                        this.releaseIntercepted();
                        if (qn7 instanceof zn7) {
                            this = (zn7) qn7;
                        } else {
                            qn7.resumeWith(obj);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    dl7.a aVar2 = dl7.Companion;
                    obj = dl7.m1constructorimpl(el7.a(th));
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        sb.append(stackTraceElement);
        return sb.toString();
    }
}
