package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ts7<View> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ViewGroup f2851a;

        @DexIgnore
        public a(ViewGroup viewGroup) {
            this.f2851a = viewGroup;
        }

        @DexIgnore
        @Override // com.fossil.ts7
        public Iterator<View> iterator() {
            return po0.b(this.f2851a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Iterator<View>, Object {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup c;

        @DexIgnore
        public b(ViewGroup viewGroup) {
            this.c = viewGroup;
        }

        @DexIgnore
        /* renamed from: a */
        public View next() {
            ViewGroup viewGroup = this.c;
            int i = this.b;
            this.b = i + 1;
            View childAt = viewGroup.getChildAt(i);
            if (childAt != null) {
                return childAt;
            }
            throw new IndexOutOfBoundsException();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.getChildCount();
        }

        @DexIgnore
        public void remove() {
            ViewGroup viewGroup = this.c;
            int i = this.b - 1;
            this.b = i;
            viewGroup.removeViewAt(i);
        }
    }

    @DexIgnore
    public static final ts7<View> a(ViewGroup viewGroup) {
        pq7.c(viewGroup, "$this$children");
        return new a(viewGroup);
    }

    @DexIgnore
    public static final Iterator<View> b(ViewGroup viewGroup) {
        pq7.c(viewGroup, "$this$iterator");
        return new b(viewGroup);
    }
}
