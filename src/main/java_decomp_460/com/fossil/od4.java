package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class od4 implements d74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ od4 f2670a; // = new od4();

    @DexIgnore
    public static d74 b() {
        return f2670a;
    }

    @DexIgnore
    @Override // com.fossil.d74
    public Object a(b74 b74) {
        return m02.f((Context) b74.get(Context.class));
    }
}
