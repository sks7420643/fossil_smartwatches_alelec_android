package com.fossil;

import android.database.Cursor;
import com.fossil.o31;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q31 implements p31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f2919a;
    @DexIgnore
    public /* final */ jw0<o31> b;
    @DexIgnore
    public /* final */ xw0 c;
    @DexIgnore
    public /* final */ xw0 d;
    @DexIgnore
    public /* final */ xw0 e;
    @DexIgnore
    public /* final */ xw0 f;
    @DexIgnore
    public /* final */ xw0 g;
    @DexIgnore
    public /* final */ xw0 h;
    @DexIgnore
    public /* final */ xw0 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<o31> {
        @DexIgnore
        public a(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, o31 o31) {
            String str = o31.f2626a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            px0.bindLong(2, (long) u31.h(o31.b));
            String str2 = o31.c;
            if (str2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, str2);
            }
            String str3 = o31.d;
            if (str3 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, str3);
            }
            byte[] k = r01.k(o31.e);
            if (k == null) {
                px0.bindNull(5);
            } else {
                px0.bindBlob(5, k);
            }
            byte[] k2 = r01.k(o31.f);
            if (k2 == null) {
                px0.bindNull(6);
            } else {
                px0.bindBlob(6, k2);
            }
            px0.bindLong(7, o31.g);
            px0.bindLong(8, o31.h);
            px0.bindLong(9, o31.i);
            px0.bindLong(10, (long) o31.k);
            px0.bindLong(11, (long) u31.a(o31.l));
            px0.bindLong(12, o31.m);
            px0.bindLong(13, o31.n);
            px0.bindLong(14, o31.o);
            px0.bindLong(15, o31.p);
            px0.bindLong(16, o31.q ? 1 : 0);
            p01 p01 = o31.j;
            if (p01 != null) {
                px0.bindLong(17, (long) u31.g(p01.b()));
                px0.bindLong(18, p01.g() ? 1 : 0);
                px0.bindLong(19, p01.h() ? 1 : 0);
                px0.bindLong(20, p01.f() ? 1 : 0);
                px0.bindLong(21, p01.i() ? 1 : 0);
                px0.bindLong(22, p01.c());
                px0.bindLong(23, p01.d());
                byte[] c = u31.c(p01.a());
                if (c == null) {
                    px0.bindNull(24);
                } else {
                    px0.bindBlob(24, c);
                }
            } else {
                px0.bindNull(17);
                px0.bindNull(18);
                px0.bindNull(19);
                px0.bindNull(20);
                px0.bindNull(21);
                px0.bindNull(22);
                px0.bindNull(23);
                px0.bindNull(24);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xw0 {
        @DexIgnore
        public d(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends xw0 {
        @DexIgnore
        public e(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends xw0 {
        @DexIgnore
        public f(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends xw0 {
        @DexIgnore
        public g(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends xw0 {
        @DexIgnore
        public h(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends xw0 {
        @DexIgnore
        public i(q31 q31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public q31(qw0 qw0) {
        this.f2919a = qw0;
        this.b = new a(this, qw0);
        this.c = new b(this, qw0);
        this.d = new c(this, qw0);
        this.e = new d(this, qw0);
        this.f = new e(this, qw0);
        this.g = new f(this, qw0);
        this.h = new g(this, qw0);
        this.i = new h(this, qw0);
        new i(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.p31
    public int a(f11 f11, String... strArr) {
        this.f2919a.assertNotSuspendingTransaction();
        StringBuilder b2 = hx0.b();
        b2.append("UPDATE workspec SET state=");
        b2.append("?");
        b2.append(" WHERE id IN (");
        hx0.a(b2, strArr.length);
        b2.append(")");
        px0 compileStatement = this.f2919a.compileStatement(b2.toString());
        compileStatement.bindLong(1, (long) u31.h(f11));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.f2919a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f2919a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public void b(String str) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f2919a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
        } finally {
            this.f2919a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31> c() {
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 ORDER BY period_start_time", 0);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public int d(String str, long j) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.h.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.f2919a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f2919a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31.a> e(String str) {
        tw0 f2 = tw0.f("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "state");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                o31.a aVar = new o31.a();
                aVar.f2627a = b2.getString(c2);
                aVar.b = u31.f(b2.getInt(c3));
                arrayList.add(aVar);
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31> f(long j) {
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        f2.bindLong(1, j);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31> g(int i2) {
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        f2.bindLong(1, (long) i2);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public void h(o31 o31) {
        this.f2919a.assertNotSuspendingTransaction();
        this.f2919a.beginTransaction();
        try {
            this.b.insert((jw0<o31>) o31);
            this.f2919a.setTransactionSuccessful();
        } finally {
            this.f2919a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31> i() {
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public void j(String str, r01 r01) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.d.acquire();
        byte[] k = r01.k(r01);
        if (k == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindBlob(1, k);
        }
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.f2919a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
        } finally {
            this.f2919a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<o31> k() {
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=1", 0);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<String> l() {
        tw0 f2 = tw0.f("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<String> m(String str) {
        tw0 f2 = tw0.f("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public f11 n(String str) {
        f11 f11 = null;
        tw0 f2 = tw0.f("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                f11 = u31.f(b2.getInt(0));
            }
            return f11;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public o31 o(String str) {
        Throwable th;
        o31 o31;
        tw0 f2 = tw0.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "required_network_type");
            int c3 = dx0.c(b2, "requires_charging");
            int c4 = dx0.c(b2, "requires_device_idle");
            int c5 = dx0.c(b2, "requires_battery_not_low");
            int c6 = dx0.c(b2, "requires_storage_not_low");
            int c7 = dx0.c(b2, "trigger_content_update_delay");
            int c8 = dx0.c(b2, "trigger_max_content_delay");
            int c9 = dx0.c(b2, "content_uri_triggers");
            int c10 = dx0.c(b2, "id");
            int c11 = dx0.c(b2, "state");
            int c12 = dx0.c(b2, "worker_class_name");
            int c13 = dx0.c(b2, "input_merger_class_name");
            int c14 = dx0.c(b2, "input");
            int c15 = dx0.c(b2, "output");
            try {
                int c16 = dx0.c(b2, "initial_delay");
                int c17 = dx0.c(b2, "interval_duration");
                int c18 = dx0.c(b2, "flex_duration");
                int c19 = dx0.c(b2, "run_attempt_count");
                int c20 = dx0.c(b2, "backoff_policy");
                int c21 = dx0.c(b2, "backoff_delay_duration");
                int c22 = dx0.c(b2, "period_start_time");
                int c23 = dx0.c(b2, "minimum_retention_duration");
                int c24 = dx0.c(b2, "schedule_requested_at");
                int c25 = dx0.c(b2, "run_in_foreground");
                if (b2.moveToFirst()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    p01 p01 = new p01();
                    p01.k(u31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(u31.b(b2.getBlob(c9)));
                    o31 = new o31(string, string2);
                    o31.b = u31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = r01.g(b2.getBlob(c14));
                    o31.f = r01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = u31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                } else {
                    o31 = null;
                }
                b2.close();
                f2.m();
                return o31;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public int p(String str) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.g.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f2919a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f2919a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public List<r01> q(String str) {
        tw0 f2 = tw0.f("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f2919a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2919a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(r01.g(b2.getBlob(0)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public int r(String str) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.f.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f2919a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f2919a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public void s(String str, long j) {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.e.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.f2919a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
        } finally {
            this.f2919a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.p31
    public int t() {
        this.f2919a.assertNotSuspendingTransaction();
        px0 acquire = this.i.acquire();
        this.f2919a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f2919a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f2919a.endTransaction();
            this.i.release(acquire);
        }
    }
}
