package com.fossil;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj4 extends JsonElement implements Iterable<JsonElement> {
    @DexIgnore
    public /* final */ List<JsonElement> b; // = new ArrayList();

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public boolean a() {
        if (this.b.size() == 1) {
            return this.b.get(0).a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public int b() {
        if (this.b.size() == 1) {
            return this.b.get(0).b();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof bj4) && ((bj4) obj).b.equals(this.b));
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public String f() {
        if (this.b.size() == 1) {
            return this.b.get(0).f();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<JsonElement> iterator() {
        return this.b.iterator();
    }

    @DexIgnore
    public void k(JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = fj4.f1138a;
        }
        this.b.add(jsonElement);
    }

    @DexIgnore
    public void l(String str) {
        this.b.add(str == null ? fj4.f1138a : new jj4(str));
    }

    @DexIgnore
    public JsonElement m(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }
}
