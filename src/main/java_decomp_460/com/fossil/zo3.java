package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ xo3 c;
    @DexIgnore
    public /* final */ /* synthetic */ xo3 d;
    @DexIgnore
    public /* final */ /* synthetic */ long e;
    @DexIgnore
    public /* final */ /* synthetic */ ap3 f;

    @DexIgnore
    public zo3(ap3 ap3, Bundle bundle, xo3 xo3, xo3 xo32, long j) {
        this.f = ap3;
        this.b = bundle;
        this.c = xo3;
        this.d = xo32;
        this.e = j;
    }

    @DexIgnore
    public final void run() {
        ap3.O(this.f, this.b, this.c, this.d, this.e);
    }
}
