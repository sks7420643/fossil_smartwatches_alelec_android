package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd2 extends hl2 implements nd2 {
    @DexIgnore
    public pd2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    @Override // com.fossil.nd2
    public final void N(ld2 ld2) throws RemoteException {
        Parcel d = d();
        il2.c(d, ld2);
        n(1, d);
    }
}
