package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g7 extends ox1 {
    @DexIgnore
    public static /* final */ d7 d; // = new d7(null);
    @DexIgnore
    public /* final */ f7 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public g7(f7 f7Var, int i) {
        this.b = f7Var;
        this.c = i;
    }

    @DexIgnore
    public /* synthetic */ g7(f7 f7Var, int i, int i2) {
        i = (i2 & 2) != 0 ? 0 : i;
        this.b = f7Var;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.O0, ey1.a(this.b));
            if (this.c != 0) {
                g80.k(jSONObject, jd0.k4, Integer.valueOf(this.c));
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }
}
