package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qh6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ kh6 f2982a;
    @DexIgnore
    public /* final */ bi6 b;
    @DexIgnore
    public /* final */ vh6 c;

    @DexIgnore
    public qh6(kh6 kh6, bi6 bi6, vh6 vh6) {
        pq7.c(kh6, "mGoalTrackingOverviewDayView");
        pq7.c(bi6, "mGoalTrackingOverviewWeekView");
        pq7.c(vh6, "mGoalTrackingOverviewMonthView");
        this.f2982a = kh6;
        this.b = bi6;
        this.c = vh6;
    }

    @DexIgnore
    public final kh6 a() {
        return this.f2982a;
    }

    @DexIgnore
    public final vh6 b() {
        return this.c;
    }

    @DexIgnore
    public final bi6 c() {
        return this.b;
    }
}
