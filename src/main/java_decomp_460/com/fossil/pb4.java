package com.fossil;

import java.io.File;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class pb4 implements Comparator {
    @DexIgnore
    public static /* final */ pb4 b; // = new pb4();

    @DexIgnore
    public static Comparator a() {
        return b;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return sb4.z((File) obj, (File) obj2);
    }
}
