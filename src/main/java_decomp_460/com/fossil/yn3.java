package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ un3 c;

    @DexIgnore
    public yn3(un3 un3, long j) {
        this.c = un3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        this.c.l().q.b(this.b);
        this.c.d().M().b("Session timeout duration set", Long.valueOf(this.b));
    }
}
