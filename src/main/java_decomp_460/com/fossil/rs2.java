package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rs2 extends IInterface {
    @DexIgnore
    boolean B0(rs2 rs2) throws RemoteException;

    @DexIgnore
    int a() throws RemoteException;

    @DexIgnore
    void b(boolean z) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    void remove() throws RemoteException;

    @DexIgnore
    void setColor(int i) throws RemoteException;

    @DexIgnore
    void setEndCap(ce3 ce3) throws RemoteException;

    @DexIgnore
    void setGeodesic(boolean z) throws RemoteException;

    @DexIgnore
    void setJointType(int i) throws RemoteException;

    @DexIgnore
    void setPattern(List<me3> list) throws RemoteException;

    @DexIgnore
    void setPoints(List<LatLng> list) throws RemoteException;

    @DexIgnore
    void setStartCap(ce3 ce3) throws RemoteException;

    @DexIgnore
    void setVisible(boolean z) throws RemoteException;

    @DexIgnore
    void setWidth(float f) throws RemoteException;

    @DexIgnore
    void setZIndex(float f) throws RemoteException;
}
