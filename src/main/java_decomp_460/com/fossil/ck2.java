package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck2<T> implements fk2<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f624a;

    @DexIgnore
    public ck2(T t) {
        this.f624a = t;
    }

    @DexIgnore
    @Override // com.fossil.fk2
    public final T get() {
        return this.f624a;
    }
}
