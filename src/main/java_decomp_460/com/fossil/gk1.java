package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gk1 extends FilterInputStream {
    @DexIgnore
    public int b; // = RecyclerView.UNDEFINED_DURATION;

    @DexIgnore
    public gk1(InputStream inputStream) {
        super(inputStream);
    }

    @DexIgnore
    public final long a(long j) {
        int i = this.b;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() throws IOException {
        int i = this.b;
        return i == Integer.MIN_VALUE ? super.available() : Math.min(i, super.available());
    }

    @DexIgnore
    public final void b(long j) {
        int i = this.b;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.b = (int) (((long) i) - j);
        }
    }

    @DexIgnore
    public void mark(int i) {
        synchronized (this) {
            super.mark(i);
            this.b = i;
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        if (a(1) == -1) {
            return -1;
        }
        int read = super.read();
        b(1);
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int a2 = (int) a((long) i2);
        if (a2 == -1) {
            return -1;
        }
        int read = super.read(bArr, i, a2);
        b((long) read);
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public void reset() throws IOException {
        synchronized (this) {
            super.reset();
            this.b = RecyclerView.UNDEFINED_DURATION;
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        long a2 = a(j);
        if (a2 == -1) {
            return 0;
        }
        long skip = super.skip(a2);
        b(skip);
        return skip;
    }
}
