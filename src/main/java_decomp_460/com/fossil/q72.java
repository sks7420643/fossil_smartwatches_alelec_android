package com.fossil;

import android.os.Looper;
import com.fossil.p72;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q72 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<p72<?>> f2933a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public static <L> p72<L> a(L l, Looper looper, String str) {
        rc2.l(l, "Listener must not be null");
        rc2.l(looper, "Looper must not be null");
        rc2.l(str, "Listener type must not be null");
        return new p72<>(looper, l, str);
    }

    @DexIgnore
    public static <L> p72.a<L> b(L l, String str) {
        rc2.l(l, "Listener must not be null");
        rc2.l(str, "Listener type must not be null");
        rc2.h(str, "Listener type must not be empty");
        return new p72.a<>(l, str);
    }

    @DexIgnore
    public final void c() {
        for (p72<?> p72 : this.f2933a) {
            p72.a();
        }
        this.f2933a.clear();
    }
}
