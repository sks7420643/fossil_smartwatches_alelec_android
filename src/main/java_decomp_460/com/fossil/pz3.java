package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.nl0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f2896a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ ColorStateList h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ float j;
    @DexIgnore
    public /* final */ float k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public Typeface n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends nl0.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ rz3 f2897a;

        @DexIgnore
        public a(rz3 rz3) {
            this.f2897a = rz3;
        }

        @DexIgnore
        @Override // com.fossil.nl0.a
        public void c(int i) {
            pz3.this.m = true;
            this.f2897a.a(i);
        }

        @DexIgnore
        @Override // com.fossil.nl0.a
        public void d(Typeface typeface) {
            pz3 pz3 = pz3.this;
            pz3.n = Typeface.create(typeface, pz3.e);
            pz3.this.m = true;
            this.f2897a.b(pz3.this.n, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends rz3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ TextPaint f2898a;
        @DexIgnore
        public /* final */ /* synthetic */ rz3 b;

        @DexIgnore
        public b(TextPaint textPaint, rz3 rz3) {
            this.f2898a = textPaint;
            this.b = rz3;
        }

        @DexIgnore
        @Override // com.fossil.rz3
        public void a(int i) {
            this.b.a(i);
        }

        @DexIgnore
        @Override // com.fossil.rz3
        public void b(Typeface typeface, boolean z) {
            pz3.this.k(this.f2898a, typeface);
            this.b.b(typeface, z);
        }
    }

    @DexIgnore
    public pz3(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, tw3.TextAppearance);
        this.f2896a = obtainStyledAttributes.getDimension(tw3.TextAppearance_android_textSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b = oz3.a(context, obtainStyledAttributes, tw3.TextAppearance_android_textColor);
        this.c = oz3.a(context, obtainStyledAttributes, tw3.TextAppearance_android_textColorHint);
        this.d = oz3.a(context, obtainStyledAttributes, tw3.TextAppearance_android_textColorLink);
        this.e = obtainStyledAttributes.getInt(tw3.TextAppearance_android_textStyle, 0);
        this.f = obtainStyledAttributes.getInt(tw3.TextAppearance_android_typeface, 1);
        int e2 = oz3.e(obtainStyledAttributes, tw3.TextAppearance_fontFamily, tw3.TextAppearance_android_fontFamily);
        this.l = obtainStyledAttributes.getResourceId(e2, 0);
        this.g = obtainStyledAttributes.getString(e2);
        obtainStyledAttributes.getBoolean(tw3.TextAppearance_textAllCaps, false);
        this.h = oz3.a(context, obtainStyledAttributes, tw3.TextAppearance_android_shadowColor);
        this.i = obtainStyledAttributes.getFloat(tw3.TextAppearance_android_shadowDx, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.j = obtainStyledAttributes.getFloat(tw3.TextAppearance_android_shadowDy, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.k = obtainStyledAttributes.getFloat(tw3.TextAppearance_android_shadowRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final void d() {
        String str;
        if (this.n == null && (str = this.g) != null) {
            this.n = Typeface.create(str, this.e);
        }
        if (this.n == null) {
            int i2 = this.f;
            if (i2 == 1) {
                this.n = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.n = Typeface.SERIF;
            } else if (i2 != 3) {
                this.n = Typeface.DEFAULT;
            } else {
                this.n = Typeface.MONOSPACE;
            }
            this.n = Typeface.create(this.n, this.e);
        }
    }

    @DexIgnore
    public Typeface e() {
        d();
        return this.n;
    }

    @DexIgnore
    public Typeface f(Context context) {
        if (this.m) {
            return this.n;
        }
        if (!context.isRestricted()) {
            try {
                Typeface b2 = nl0.b(context, this.l);
                this.n = b2;
                if (b2 != null) {
                    this.n = Typeface.create(b2, this.e);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException e2) {
            } catch (Exception e3) {
                Log.d("TextAppearance", "Error loading font " + this.g, e3);
            }
        }
        d();
        this.m = true;
        return this.n;
    }

    @DexIgnore
    public void g(Context context, TextPaint textPaint, rz3 rz3) {
        k(textPaint, e());
        h(context, new b(textPaint, rz3));
    }

    @DexIgnore
    public void h(Context context, rz3 rz3) {
        if (qz3.a()) {
            f(context);
        } else {
            d();
        }
        if (this.l == 0) {
            this.m = true;
        }
        if (this.m) {
            rz3.b(this.n, true);
            return;
        }
        try {
            nl0.d(context, this.l, new a(rz3), null);
        } catch (Resources.NotFoundException e2) {
            this.m = true;
            rz3.a(1);
        } catch (Exception e3) {
            Log.d("TextAppearance", "Error loading font " + this.g, e3);
            this.m = true;
            rz3.a(-3);
        }
    }

    @DexIgnore
    public void i(Context context, TextPaint textPaint, rz3 rz3) {
        j(context, textPaint, rz3);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.k;
        float f3 = this.i;
        float f4 = this.j;
        ColorStateList colorStateList2 = this.h;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    @DexIgnore
    public void j(Context context, TextPaint textPaint, rz3 rz3) {
        if (qz3.a()) {
            k(textPaint, f(context));
        } else {
            g(context, textPaint, rz3);
        }
    }

    @DexIgnore
    public void k(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int style = typeface.getStyle() & this.e;
        textPaint.setFakeBoldText((style & 1) != 0);
        textPaint.setTextSkewX((style & 2) != 0 ? -0.25f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        textPaint.setTextSize(this.f2896a);
    }
}
