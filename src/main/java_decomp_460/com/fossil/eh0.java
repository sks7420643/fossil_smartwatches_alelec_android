package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eh0 extends ListView {
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int g;
    @DexIgnore
    public Field h;
    @DexIgnore
    public a i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public ro0 m;
    @DexIgnore
    public hp0 s;
    @DexIgnore
    public b t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jf0 {
        @DexIgnore
        public boolean c; // = true;

        @DexIgnore
        public a(Drawable drawable) {
            super(drawable);
        }

        @DexIgnore
        public void c(boolean z) {
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.jf0
        public void draw(Canvas canvas) {
            if (this.c) {
                super.draw(canvas);
            }
        }

        @DexIgnore
        @Override // com.fossil.jf0
        public void setHotspot(float f, float f2) {
            if (this.c) {
                super.setHotspot(f, f2);
            }
        }

        @DexIgnore
        @Override // com.fossil.jf0
        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.c) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        @DexIgnore
        @Override // com.fossil.jf0
        public boolean setState(int[] iArr) {
            if (this.c) {
                return super.setState(iArr);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.jf0
        public boolean setVisible(boolean z, boolean z2) {
            if (this.c) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a() {
            eh0 eh0 = eh0.this;
            eh0.t = null;
            eh0.removeCallbacks(this);
        }

        @DexIgnore
        public void b() {
            eh0.this.post(this);
        }

        @DexIgnore
        public void run() {
            eh0 eh0 = eh0.this;
            eh0.t = null;
            eh0.drawableStateChanged();
        }
    }

    @DexIgnore
    public eh0(Context context, boolean z) {
        super(context, null, le0.dropDownListViewStyle);
        this.k = z;
        setCacheColorHint(0);
        try {
            Field declaredField = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.h = declaredField;
            declaredField.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    private void setSelectorEnabled(boolean z) {
        a aVar = this.i;
        if (aVar != null) {
            aVar.c(z);
        }
    }

    @DexIgnore
    public final void a() {
        this.l = false;
        setPressed(false);
        drawableStateChanged();
        View childAt = getChildAt(this.g - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        ro0 ro0 = this.m;
        if (ro0 != null) {
            ro0.b();
            this.m = null;
        }
    }

    @DexIgnore
    public final void b(View view, int i2) {
        performItemClick(view, i2, getItemIdAtPosition(i2));
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        Drawable selector;
        if (!this.b.isEmpty() && (selector = getSelector()) != null) {
            selector.setBounds(this.b);
            selector.draw(canvas);
        }
    }

    @DexIgnore
    public int d(int i2, int i3, int i4, int i5, int i6) {
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i7 = listPaddingBottom + listPaddingTop;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int count = adapter.getCount();
        View view = null;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        while (i10 < count) {
            int itemViewType = adapter.getItemViewType(i10);
            if (itemViewType != i8) {
                view = null;
                i8 = itemViewType;
            }
            view = adapter.getView(i10, view, this);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            int i11 = layoutParams.height;
            view.measure(i2, i11 > 0 ? View.MeasureSpec.makeMeasureSpec(i11, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
            view.forceLayout();
            int measuredHeight = (i10 > 0 ? i7 + dividerHeight : i7) + view.getMeasuredHeight();
            if (measuredHeight >= i5) {
                return (i6 < 0 || i10 <= i6 || i9 <= 0 || measuredHeight == i5) ? i5 : i9;
            }
            if (i6 >= 0 && i10 >= i6) {
                i9 = measuredHeight;
            }
            i10++;
            i7 = measuredHeight;
        }
        return i7;
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        c(canvas);
        super.dispatchDraw(canvas);
    }

    @DexIgnore
    public void drawableStateChanged() {
        if (this.t == null) {
            super.drawableStateChanged();
            setSelectorEnabled(true);
            k();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r3 != 3) goto L_0x000e;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e(android.view.MotionEvent r9, int r10) {
        /*
            r8 = this;
            r2 = 0
            r1 = 1
            int r3 = r9.getActionMasked()
            if (r3 == r1) goto L_0x0039
            r0 = 2
            if (r3 == r0) goto L_0x002f
            r0 = 3
            if (r3 == r0) goto L_0x0036
        L_0x000e:
            r0 = r1
            r3 = r2
        L_0x0010:
            if (r0 == 0) goto L_0x0014
            if (r3 == 0) goto L_0x0017
        L_0x0014:
            r8.a()
        L_0x0017:
            if (r0 == 0) goto L_0x0063
            com.fossil.hp0 r2 = r8.s
            if (r2 != 0) goto L_0x0024
            com.fossil.hp0 r2 = new com.fossil.hp0
            r2.<init>(r8)
            r8.s = r2
        L_0x0024:
            com.fossil.hp0 r2 = r8.s
            r2.m(r1)
            com.fossil.hp0 r1 = r8.s
            r1.onTouch(r8, r9)
        L_0x002e:
            return r0
        L_0x002f:
            r0 = r1
        L_0x0030:
            int r4 = r9.findPointerIndex(r10)
            if (r4 >= 0) goto L_0x003b
        L_0x0036:
            r0 = r2
            r3 = r2
            goto L_0x0010
        L_0x0039:
            r0 = r2
            goto L_0x0030
        L_0x003b:
            float r5 = r9.getX(r4)
            int r5 = (int) r5
            float r4 = r9.getY(r4)
            int r4 = (int) r4
            int r6 = r8.pointToPosition(r5, r4)
            r7 = -1
            if (r6 != r7) goto L_0x004e
            r3 = r1
            goto L_0x0010
        L_0x004e:
            int r0 = r8.getFirstVisiblePosition()
            int r0 = r6 - r0
            android.view.View r0 = r8.getChildAt(r0)
            float r5 = (float) r5
            float r4 = (float) r4
            r8.i(r0, r6, r5, r4)
            if (r3 != r1) goto L_0x000e
            r8.b(r0, r6)
            goto L_0x000e
        L_0x0063:
            com.fossil.hp0 r1 = r8.s
            if (r1 == 0) goto L_0x002e
            r1.m(r2)
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eh0.e(android.view.MotionEvent, int):boolean");
    }

    @DexIgnore
    public final void f(int i2, View view) {
        Rect rect = this.b;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.c;
        rect.top -= this.d;
        rect.right += this.e;
        rect.bottom += this.f;
        try {
            boolean z = this.h.getBoolean(this);
            if (view.isEnabled() != z) {
                this.h.set(this, Boolean.valueOf(!z));
                if (i2 != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void g(int i2, View view) {
        boolean z = true;
        Drawable selector = getSelector();
        boolean z2 = (selector == null || i2 == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        f(i2, view);
        if (z2) {
            Rect rect = this.b;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            am0.k(selector, exactCenterX, exactCenterY);
        }
    }

    @DexIgnore
    public final void h(int i2, View view, float f2, float f3) {
        g(i2, view);
        Drawable selector = getSelector();
        if (selector != null && i2 != -1) {
            am0.k(selector, f2, f3);
        }
    }

    @DexIgnore
    public boolean hasFocus() {
        return this.k || super.hasFocus();
    }

    @DexIgnore
    public boolean hasWindowFocus() {
        return this.k || super.hasWindowFocus();
    }

    @DexIgnore
    public final void i(View view, int i2, float f2, float f3) {
        View childAt;
        this.l = true;
        if (Build.VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f2, f3);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        int i3 = this.g;
        if (!(i3 == -1 || (childAt = getChildAt(i3 - getFirstVisiblePosition())) == null || childAt == view || !childAt.isPressed())) {
            childAt.setPressed(false);
        }
        this.g = i2;
        float left = (float) view.getLeft();
        float top = (float) view.getTop();
        if (Build.VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(f2 - left, f3 - top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        h(i2, view, f2, f3);
        setSelectorEnabled(false);
        refreshDrawableState();
    }

    @DexIgnore
    public boolean isFocused() {
        return this.k || super.isFocused();
    }

    @DexIgnore
    public boolean isInTouchMode() {
        return (this.k && this.j) || super.isInTouchMode();
    }

    @DexIgnore
    public final boolean j() {
        return this.l;
    }

    @DexIgnore
    public final void k() {
        Drawable selector = getSelector();
        if (selector != null && j() && isPressed()) {
            selector.setState(getDrawableState());
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.t = null;
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        if (Build.VERSION.SDK_INT < 26) {
            return super.onHoverEvent(motionEvent);
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 10 && this.t == null) {
            b bVar = new b();
            this.t = bVar;
            bVar.b();
        }
        boolean onHoverEvent = super.onHoverEvent(motionEvent);
        if (actionMasked == 9 || actionMasked == 7) {
            int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
            if (pointToPosition == -1 || pointToPosition == getSelectedItemPosition()) {
                return onHoverEvent;
            }
            View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
            if (childAt.isEnabled()) {
                setSelectionFromTop(pointToPosition, childAt.getTop() - getTop());
            }
            k();
            return onHoverEvent;
        }
        setSelection(-1);
        return onHoverEvent;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.g = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        b bVar = this.t;
        if (bVar != null) {
            bVar.a();
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setListSelectionHidden(boolean z) {
        this.j = z;
    }

    @DexIgnore
    @Override // android.widget.AbsListView
    public void setSelector(Drawable drawable) {
        a aVar = drawable != null ? new a(drawable) : null;
        this.i = aVar;
        super.setSelector(aVar);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.c = rect.left;
        this.d = rect.top;
        this.e = rect.right;
        this.f = rect.bottom;
    }
}
