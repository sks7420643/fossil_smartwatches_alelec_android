package com.fossil;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x81 {
    @DexIgnore
    public static final void a(String str, Throwable th) {
        pq7.c(str, "tag");
        pq7.c(th, "throwable");
        if (q81.c.a() && q81.c.b() <= 6) {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            Log.println(6, str, stringWriter.toString());
        }
    }
}
