package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.fossil.lh1;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hh1 extends Drawable implements lh1.b, Animatable {
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Rect k;
    @DexIgnore
    public List<tz0> l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ lh1 f1477a;

        @DexIgnore
        public a(lh1 lh1) {
            this.f1477a = lh1;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new hh1(this);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }
    }

    @DexIgnore
    public hh1(Context context, bb1 bb1, sb1<Bitmap> sb1, int i2, int i3, Bitmap bitmap) {
        this(new a(new lh1(oa1.c(context), bb1, i2, i3, sb1, bitmap)));
    }

    @DexIgnore
    public hh1(a aVar) {
        this.f = true;
        this.h = -1;
        ik1.d(aVar);
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.lh1.b
    public void a() {
        if (b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (g() == f() - 1) {
            this.g++;
        }
        int i2 = this.h;
        if (i2 != -1 && this.g >= i2) {
            j();
            stop();
        }
    }

    @DexIgnore
    public final Drawable.Callback b() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    @DexIgnore
    public ByteBuffer c() {
        return this.b.f1477a.b();
    }

    @DexIgnore
    public final Rect d() {
        if (this.k == null) {
            this.k = new Rect();
        }
        return this.k;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!this.e) {
            if (this.i) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), d());
                this.i = false;
            }
            canvas.drawBitmap(this.b.f1477a.c(), (Rect) null, d(), h());
        }
    }

    @DexIgnore
    public Bitmap e() {
        return this.b.f1477a.e();
    }

    @DexIgnore
    public int f() {
        return this.b.f1477a.f();
    }

    @DexIgnore
    public int g() {
        return this.b.f1477a.d();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.b.f1477a.h();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.b.f1477a.k();
    }

    @DexIgnore
    public int getOpacity() {
        return -2;
    }

    @DexIgnore
    public final Paint h() {
        if (this.j == null) {
            this.j = new Paint(2);
        }
        return this.j;
    }

    @DexIgnore
    public int i() {
        return this.b.f1477a.j();
    }

    @DexIgnore
    public boolean isRunning() {
        return this.c;
    }

    @DexIgnore
    public final void j() {
        List<tz0> list = this.l;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.l.get(i2).a(this);
            }
        }
    }

    @DexIgnore
    public void k() {
        this.e = true;
        this.b.f1477a.a();
    }

    @DexIgnore
    public final void l() {
        this.g = 0;
    }

    @DexIgnore
    public void m(sb1<Bitmap> sb1, Bitmap bitmap) {
        this.b.f1477a.o(sb1, bitmap);
    }

    @DexIgnore
    public final void n() {
        ik1.a(!this.e, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.b.f1477a.f() == 1) {
            invalidateSelf();
        } else if (!this.c) {
            this.c = true;
            this.b.f1477a.r(this);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void o() {
        this.c = false;
        this.b.f1477a.s(this);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.i = true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        h().setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        h().setColorFilter(colorFilter);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        ik1.a(!this.e, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.f = z;
        if (!z) {
            o();
        } else if (this.d) {
            n();
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        this.d = true;
        l();
        if (this.f) {
            n();
        }
    }

    @DexIgnore
    public void stop() {
        this.d = false;
        o();
    }
}
