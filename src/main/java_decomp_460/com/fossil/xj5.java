package com.fossil;

import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj5 implements af1<yj5, InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f4135a;
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return xj5.f4135a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bf1<yj5, InputStream> {
        @DexIgnore
        /* renamed from: a */
        public xj5 b(ef1 ef1) {
            pq7.c(ef1, "multiFactory");
            return new xj5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements wb1<InputStream> {
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ yj5 c;

        @DexIgnore
        public c(xj5 xj5, yj5 yj5) {
            this.c = yj5;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (wrap: int : 0x018f: ARITH  (r1v21 int) = (wrap: int : ?: ARITH  null = (r2v1 int) - (4 int)) + (1 int))] */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x01e1  */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x01ec  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x026b  */
        @Override // com.fossil.wb1
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void d(com.fossil.sa1 r9, com.fossil.wb1.a<? super java.io.InputStream> r10) {
            /*
            // Method dump skipped, instructions count: 630
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xj5.c.d(com.fossil.sa1, com.fossil.wb1$a):void");
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    /*
    static {
        String simpleName = xj5.class.getSimpleName();
        pq7.b(simpleName, "NotificationLoader::class.java.simpleName");
        f4135a = simpleName;
    }
    */

    @DexIgnore
    /* renamed from: d */
    public af1.a<InputStream> b(yj5 yj5, int i, int i2, ob1 ob1) {
        pq7.c(yj5, "notificationModel");
        pq7.c(ob1, "options");
        return new af1.a<>(yj5, new c(this, yj5));
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(yj5 yj5) {
        pq7.c(yj5, "notificationModel");
        return true;
    }
}
