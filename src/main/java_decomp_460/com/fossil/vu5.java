package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu5 extends iq4<iq4.b, a, iq4.a> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ MFUser f3836a;

        @DexIgnore
        public a(MFUser mFUser) {
            this.f3836a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.f3836a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.information.domain.usecase.GetUser", f = "GetUser.kt", l = {13}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vu5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vu5 vu5, qn7 qn7) {
            super(qn7);
            this.this$0 = vu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public vu5(UserRepository userRepository) {
        pq7.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetUser";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    @Override // com.fossil.iq4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.iq4.b r6, com.fossil.qn7<java.lang.Object> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.vu5.b
            if (r0 == 0) goto L_0x0035
            r0 = r7
            com.fossil.vu5$b r0 = (com.fossil.vu5.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0035
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0044
            if (r0 != r4) goto L_0x003c
            java.lang.Object r0 = r1.L$1
            com.fossil.iq4$b r0 = (com.fossil.iq4.b) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.vu5 r0 = (com.fossil.vu5) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002c:
            com.fossil.vu5$a r1 = new com.fossil.vu5$a
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0034:
            return r0
        L_0x0035:
            com.fossil.vu5$b r0 = new com.fossil.vu5$b
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.UserRepository r0 = r5.d
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r0 = r0.getCurrentUser(r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vu5.k(com.fossil.iq4$b, com.fossil.qn7):java.lang.Object");
    }
}
