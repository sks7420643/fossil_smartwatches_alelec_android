package com.fossil;

import com.fossil.xw7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ex7<J extends xw7> extends zu7 implements dw7, sw7 {
    @DexIgnore
    public /* final */ J e;

    @DexIgnore
    public ex7(J j) {
        this.e = j;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public kx7 b() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.dw7
    public void dispose() {
        J j = this.e;
        if (j != null) {
            ((fx7) j).i0(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public boolean isActive() {
        return true;
    }
}
