package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class od2 extends gl2 implements ld2 {
    @DexIgnore
    public od2() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.gl2
    public boolean X2(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        u0(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
