package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy1<T> extends uy1<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Integer f3331a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ vy1 c;

    @DexIgnore
    public sy1(Integer num, T t, vy1 vy1) {
        this.f3331a = num;
        if (t != null) {
            this.b = t;
            if (vy1 != null) {
                this.c = vy1;
                return;
            }
            throw new NullPointerException("Null priority");
        }
        throw new NullPointerException("Null payload");
    }

    @DexIgnore
    @Override // com.fossil.uy1
    public Integer a() {
        return this.f3331a;
    }

    @DexIgnore
    @Override // com.fossil.uy1
    public T b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.uy1
    public vy1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof uy1)) {
            return false;
        }
        uy1 uy1 = (uy1) obj;
        Integer num = this.f3331a;
        if (num != null ? num.equals(uy1.a()) : uy1.a() == null) {
            if (this.b.equals(uy1.b()) && this.c.equals(uy1.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.f3331a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Event{code=" + this.f3331a + ", payload=" + ((Object) this.b) + ", priority=" + this.c + "}";
    }
}
