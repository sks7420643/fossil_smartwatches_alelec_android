package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu7 {
    @DexIgnore
    public static final <T> Object a(Object obj, qn7<? super T> qn7) {
        if (obj instanceof vu7) {
            dl7.a aVar = dl7.Companion;
            Throwable th = ((vu7) obj).f3837a;
            if (nv7.d() && (qn7 instanceof do7)) {
                th = uz7.a(th, (do7) qn7);
            }
            return dl7.m1constructorimpl(el7.a(th));
        }
        dl7.a aVar2 = dl7.Companion;
        return dl7.m1constructorimpl(obj);
    }

    @DexIgnore
    public static final <T> Object b(Object obj) {
        Throwable r0 = dl7.m4exceptionOrNullimpl(obj);
        return r0 == null ? obj : new vu7(r0, false, 2, null);
    }

    @DexIgnore
    public static final <T> Object c(Object obj, ku7<?> ku7) {
        Throwable r0 = dl7.m4exceptionOrNullimpl(obj);
        if (r0 == null) {
            return obj;
        }
        if (nv7.d() && (ku7 instanceof do7)) {
            r0 = uz7.a(r0, (do7) ku7);
        }
        return new vu7(r0, false, 2, null);
    }
}
