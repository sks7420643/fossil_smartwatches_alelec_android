package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nd6 implements Factory<md6> {
    @DexIgnore
    public static md6 a(ld6 ld6, PortfolioApp portfolioApp, DeviceRepository deviceRepository, mj5 mj5, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, on5 on5, HeartRateSampleRepository heartRateSampleRepository, tt4 tt4) {
        return new md6(ld6, portfolioApp, deviceRepository, mj5, summariesRepository, goalTrackingRepository, sleepSummariesRepository, on5, heartRateSampleRepository, tt4);
    }
}
