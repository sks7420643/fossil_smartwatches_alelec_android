package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq4 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<Alarm> f4160a; // = new ArrayList();
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Alarm alarm);

        @DexIgnore
        void b(Alarm alarm);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ jd5 f4161a;
        @DexIgnore
        public /* final */ /* synthetic */ xq4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (aVar = this.b.b.b) != null) {
                    aVar.b((Alarm) this.b.b.f4160a.get(adapterPosition));
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xq4$b$b")
        /* renamed from: com.fossil.xq4$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0287b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0287b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a aVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (aVar = this.b.b.b) != null) {
                    aVar.a((Alarm) this.b.b.f4160a.get(adapterPosition));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xq4 xq4, jd5 jd5) {
            super(jd5.n());
            pq7.c(jd5, "binding");
            this.b = xq4;
            this.f4161a = jd5;
            jd5.q.setOnClickListener(new a(this));
            this.f4161a.u.setOnClickListener(new View$OnClickListenerC0287b(this));
            String d = qn5.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d)) {
                this.f4161a.q.setBackgroundColor(Color.parseColor(d));
            }
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Alarm alarm, boolean z) {
            int i = 12;
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            FLogger.INSTANCE.getLocal().d(xq4.c, "Alarm: " + alarm + ", isSingleAlarm=" + z);
            int totalMinutes = alarm.getTotalMinutes();
            int hour = alarm.getHour();
            int minute = alarm.getMinute();
            View n = this.f4161a.n();
            pq7.b(n, "binding.root");
            if (DateFormat.is24HourFormat(n.getContext())) {
                FlexibleTextView flexibleTextView = this.f4161a.s;
                pq7.b(flexibleTextView, "binding.ftvTime");
                StringBuilder sb = new StringBuilder();
                hr7 hr7 = hr7.f1520a;
                Locale locale = Locale.US;
                pq7.b(locale, "Locale.US");
                String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(hour)}, 1));
                pq7.b(format, "java.lang.String.format(locale, format, *args)");
                sb.append(format);
                sb.append(':');
                hr7 hr72 = hr7.f1520a;
                Locale locale2 = Locale.US;
                pq7.b(locale2, "Locale.US");
                String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                pq7.b(format2, "java.lang.String.format(locale, format, *args)");
                sb.append(format2);
                flexibleTextView.setText(sb.toString());
            } else if (totalMinutes < 720) {
                if (hour != 0) {
                    i = hour;
                }
                FlexibleTextView flexibleTextView2 = this.f4161a.s;
                pq7.b(flexibleTextView2, "binding.ftvTime");
                jl5 jl5 = jl5.b;
                StringBuilder sb2 = new StringBuilder();
                hr7 hr73 = hr7.f1520a;
                Locale locale3 = Locale.US;
                pq7.b(locale3, "Locale.US");
                String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                pq7.b(format3, "java.lang.String.format(locale, format, *args)");
                sb2.append(format3);
                sb2.append(':');
                hr7 hr74 = hr7.f1520a;
                Locale locale4 = Locale.US;
                pq7.b(locale4, "Locale.US");
                String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                pq7.b(format4, "java.lang.String.format(locale, format, *args)");
                sb2.append(format4);
                sb2.append(' ');
                String sb3 = sb2.toString();
                View n2 = this.f4161a.n();
                pq7.b(n2, "binding.root");
                String c = um5.c(n2.getContext(), 2131886102);
                pq7.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                flexibleTextView2.setText(jl5.g(sb3, c, 1.0f));
            } else {
                if (hour > 12) {
                    i = hour - 12;
                }
                FlexibleTextView flexibleTextView3 = this.f4161a.s;
                pq7.b(flexibleTextView3, "binding.ftvTime");
                jl5 jl52 = jl5.b;
                StringBuilder sb4 = new StringBuilder();
                hr7 hr75 = hr7.f1520a;
                Locale locale5 = Locale.US;
                pq7.b(locale5, "Locale.US");
                String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                pq7.b(format5, "java.lang.String.format(locale, format, *args)");
                sb4.append(format5);
                sb4.append(':');
                hr7 hr76 = hr7.f1520a;
                Locale locale6 = Locale.US;
                pq7.b(locale6, "Locale.US");
                String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                pq7.b(format6, "java.lang.String.format(locale, format, *args)");
                sb4.append(format6);
                sb4.append(' ');
                String sb5 = sb4.toString();
                View n3 = this.f4161a.n();
                pq7.b(n3, "binding.root");
                String c2 = um5.c(n3.getContext(), 2131886104);
                pq7.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                flexibleTextView3.setText(jl52.g(sb5, c2, 1.0f));
            }
            int[] days = alarm.getDays();
            int length = days != null ? days.length : 0;
            StringBuilder sb6 = new StringBuilder("");
            if (length > 0 && alarm.isRepeated()) {
                if (length == 7) {
                    sb6.append(um5.c(PortfolioApp.h0.c(), 2131886108));
                    pq7.b(sb6, "strDays.append(LanguageH\u2026_Alerts_Label__EveryDay))");
                } else {
                    if (length == 2) {
                        xq4 xq4 = this.b;
                        if (days == null) {
                            pq7.i();
                            throw null;
                        } else if (xq4.k(days)) {
                            sb6.append(um5.c(PortfolioApp.h0.c(), 2131886109));
                            pq7.b(sb6, "strDays.append(LanguageH\u2026rts_Label__EveryWeekend))");
                        }
                    }
                    if (days != null) {
                        dm7.t(days);
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = 1;
                            while (true) {
                                if (i3 > 7) {
                                    break;
                                } else if (i3 == days[i2]) {
                                    sb6.append(jl5.b.h(i3));
                                    if (i2 < length - 1) {
                                        sb6.append(", ");
                                    }
                                } else {
                                    i3++;
                                }
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
            FlexibleTextView flexibleTextView4 = this.f4161a.t;
            pq7.b(flexibleTextView4, "binding.ftvTitle");
            flexibleTextView4.setText(alarm.getTitle());
            FlexibleTextView flexibleTextView5 = this.f4161a.r;
            pq7.b(flexibleTextView5, "binding.ftvRepeatedDays");
            flexibleTextView5.setText(sb6.toString());
            FlexibleSwitchCompat flexibleSwitchCompat = this.f4161a.u;
            pq7.b(flexibleSwitchCompat, "binding.swEnabled");
            flexibleSwitchCompat.setChecked(alarm.isActive());
            if (z) {
                CardView cardView = this.f4161a.q;
                pq7.b(cardView, "binding.cvRoot");
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                al5 a2 = al5.a();
                pq7.b(a2, "MeasureHelper.getInstance()");
                layoutParams.width = (int) (((float) a2.b()) - p47.b(32.0f));
                return;
            }
            CardView cardView2 = this.f4161a.q;
            pq7.b(cardView2, "binding.cvRoot");
            ViewGroup.LayoutParams layoutParams2 = cardView2.getLayoutParams();
            al5 a3 = al5.a();
            pq7.b(a3, "MeasureHelper.getInstance()");
            layoutParams2.width = (int) ((((float) a3.b()) - p47.b(32.0f)) / 2.2f);
        }
    }

    /*
    static {
        String simpleName = xq4.class.getSimpleName();
        pq7.b(simpleName, "AlarmsAdapter::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4160a.size();
    }

    @DexIgnore
    public final boolean k(int[] iArr) {
        if (iArr.length != 2) {
            return false;
        }
        return (iArr[0] == 1 && iArr[1] == 7) || (iArr[0] == 7 && iArr[1] == 1);
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(b bVar, int i) {
        boolean z = true;
        pq7.c(bVar, "holder");
        Alarm alarm = this.f4160a.get(i);
        if (this.f4160a.size() != 1) {
            z = false;
        }
        bVar.a(alarm, z);
    }

    @DexIgnore
    /* renamed from: m */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        jd5 z = jd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemAlarmBinding.inflate\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final void n(List<Alarm> list) {
        pq7.c(list, "alarms");
        this.f4160a.clear();
        this.f4160a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }
}
