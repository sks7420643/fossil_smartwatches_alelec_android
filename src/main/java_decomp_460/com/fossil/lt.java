package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum lt {
    JSON_FILE_EVENT((byte) 1),
    HEARTBEAT_EVENT((byte) 2),
    CONNECTION_PARAM_CHANGE_EVENT((byte) 3),
    APP_NOTIFICATION_EVENT((byte) 4),
    MUSIC_EVENT((byte) 5),
    BACKGROUND_SYNC_EVENT((byte) 6),
    SERVICE_CHANGE_EVENT((byte) 7),
    MICRO_APP_EVENT((byte) 8),
    AUTHENTICATION_REQUEST_EVENT((byte) 9),
    TIME_SYNC_EVENT((byte) 11),
    BATTERY_EVENT((byte) 12),
    ENCRYPTED_DATA((byte) 13);
    
    @DexIgnore
    public static /* final */ jt p; // = new jt(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public lt(byte b2) {
        this.b = (byte) b2;
    }
}
