package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fw5;
import com.fossil.ir4;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x96 extends pv5 implements w96, t47.g {
    @DexIgnore
    public g37<cc5> g;
    @DexIgnore
    public v96 h;
    @DexIgnore
    public fw5 i;
    @DexIgnore
    public ir4 j;
    @DexIgnore
    public po4 k;
    @DexIgnore
    public k76 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ir4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x96 f4081a;

        @DexIgnore
        public a(x96 x96) {
            this.f4081a = x96;
        }

        @DexIgnore
        @Override // com.fossil.ir4.a
        public void a(WatchApp watchApp) {
            pq7.c(watchApp, "watchApp");
            x96.K6(this.f4081a).r(watchApp.getWatchappId());
        }

        @DexIgnore
        @Override // com.fossil.ir4.a
        public void b(WatchApp watchApp) {
            pq7.c(watchApp, "watchApp");
            jn5.c(jn5.b, this.f4081a.getContext(), hl5.f1493a.e(watchApp.getWatchappId()), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x96 f4082a;

        @DexIgnore
        public b(x96 x96) {
            this.f4082a = x96;
        }

        @DexIgnore
        @Override // com.fossil.fw5.b
        public void a(Category category) {
            pq7.c(category, "category");
            x96.K6(this.f4082a).q(category);
        }

        @DexIgnore
        @Override // com.fossil.fw5.b
        public void b() {
            x96.K6(this.f4082a).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x96 b;

        @DexIgnore
        public c(x96 x96) {
            this.b = x96;
        }

        @DexIgnore
        public final void onClick(View view) {
            x96.K6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x96 b;

        @DexIgnore
        public d(x96 x96) {
            this.b = x96;
        }

        @DexIgnore
        public final void onClick(View view) {
            x96.K6(this.b).p();
        }
    }

    @DexIgnore
    public static final /* synthetic */ v96 K6(x96 x96) {
        v96 v96 = x96.h;
        if (v96 != null) {
            return v96;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final void L6() {
        if (isActive()) {
            ir4 ir4 = this.j;
            if (ir4 != null) {
                ir4.j();
            } else {
                pq7.n("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(v96 v96) {
        pq7.c(v96, "presenter");
        this.h = v96;
    }

    @DexIgnore
    public final void N6(WatchApp watchApp) {
        g37<cc5> g37 = this.g;
        if (g37 != null) {
            cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "binding.tvSelectedWatchapps");
                flexibleTextView.setText(um5.d(PortfolioApp.h0.c(), watchApp.getNameKey(), watchApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "binding.tvWatchappsDetail");
                flexibleTextView2.setText(um5.d(PortfolioApp.h0.c(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                ir4 ir4 = this.j;
                if (ir4 != null) {
                    int k2 = ir4.k(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + k2);
                    if (k2 >= 0) {
                        a2.t.scrollToPosition(k2);
                        return;
                    }
                    return;
                }
                pq7.n("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        pq7.c(str, "tag");
        if (pq7.a(str, "REQUEST_NOTIFICATION_ACCESS")) {
            if (i2 == 2131363291) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (pq7.a(str, s37.c.f())) {
            if (i2 == 2131363373 && (activity3 = getActivity()) != null) {
                if (!v78.d(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    g47.f1261a.t(this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity3 != null) {
                    ((ls5) activity3).x();
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (pq7.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363373 && (activity2 = getActivity()) != null) {
                if (activity2 != null) {
                    ((ls5) activity2).x();
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (pq7.a(str, "LOCATION_PERMISSION_TAG")) {
            if (i2 == 2131363373 && (activity = getActivity()) != null) {
                if (activity != null) {
                    ((ls5) activity).x();
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (pq7.a(str, InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363373) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void S(List<Category> list) {
        pq7.c(list, "categories");
        fw5 fw5 = this.i;
        if (fw5 != null) {
            fw5.n(list);
        } else {
            pq7.n("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void S5(List<WatchApp> list) {
        pq7.c(list, "watchApps");
        ir4 ir4 = this.j;
        if (ir4 != null) {
            ir4.o(list);
        } else {
            pq7.n("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void U4(String str) {
        pq7.c(str, "content");
        g37<cc5> g37 = this.g;
        if (g37 != null) {
            cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "it.tvWatchappsDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void a0(String str, String str2, String str3) {
        pq7.c(str, "topWatchApp");
        pq7.c(str2, "middleWatchApp");
        pq7.c(str3, "bottomWatchApp");
        WatchAppSearchActivity.B.a(this, str, str2, str3);
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void c1(boolean z) {
        if (isActive()) {
            g37<cc5> g37 = this.g;
            if (g37 != null) {
                cc5 a2 = g37.a();
                if (a2 != null) {
                    a2.v.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? 2131231087 : 0, 0);
                    Drawable[] compoundDrawables = a2.v.getCompoundDrawables();
                    pq7.b(compoundDrawables, "it.tvSelectedWatchapps.getCompoundDrawables()");
                    if (compoundDrawables[2] != null) {
                        Drawable drawable = compoundDrawables[2];
                        if (drawable != null) {
                            drawable.setColorFilter(getResources().getColor(2131099968), PorterDuff.Mode.MULTIPLY);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void d6(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void f0(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.A.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void g0(boolean z, String str, String str2, String str3) {
        pq7.c(str, "watchAppId");
        pq7.c(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        g37<cc5> g37 = this.g;
        if (g37 != null) {
            cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.x;
                pq7.b(flexibleTextView2, "it.tvWatchappsPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.y;
                    pq7.b(flexibleTextView3, "it.tvWatchappsSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.y;
                        pq7.b(flexibleTextView4, "it.tvWatchappsSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.y;
                    pq7.b(flexibleTextView5, "it.tvWatchappsSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.y;
                pq7.b(flexibleTextView6, "it.tvWatchappsSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void i0(String str) {
        pq7.c(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.A;
            pq7.b(activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void o5(WatchApp watchApp) {
        if (watchApp != null) {
            ir4 ir4 = this.j;
            if (ir4 != null) {
                ir4.q(watchApp.getWatchappId());
                N6(watchApp);
                return;
            }
            pq7.n("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchAppEditActivity watchAppEditActivity = (WatchAppEditActivity) activity;
            po4 po4 = this.k;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchAppEditActivity, po4).a(k76.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ditViewModel::class.java)");
                k76 k76 = (k76) a2;
                this.l = k76;
                v96 v96 = this.h;
                if (v96 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (k76 != null) {
                    v96.s(k76);
                    v96 v962 = this.h;
                    if (v962 != null) {
                        v962.l();
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    pq7.n("mShareViewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 101) {
            if (i2 != 105) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING")) != null) {
                    v96 v96 = this.h;
                    if (v96 != null) {
                        v96.t("commute-time", commuteTimeWatchAppSetting);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            } else if (i3 == -1 && intent != null && (weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING")) != null) {
                v96 v962 = this.h;
                if (v962 != null) {
                    v962.t("weather", weatherWatchAppSetting);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                v96 v963 = this.h;
                if (v963 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (stringExtra != null) {
                    v963.r(stringExtra);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        cc5 cc5 = (cc5) aq0.f(layoutInflater, 2131558637, viewGroup, false, A6());
        PortfolioApp.h0.c().M().T1(new z96(this)).a(this);
        this.g = new g37<>(this, cc5);
        pq7.b(cc5, "binding");
        return cc5.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        v96 v96 = this.h;
        if (v96 != null) {
            v96.m();
            super.onDestroy();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        ir4 ir4 = new ir4(null, null, 3, null);
        ir4.p(new a(this));
        this.j = ir4;
        fw5 fw5 = new fw5(null, null, 3, null);
        fw5.o(new b(this));
        this.i = fw5;
        g37<cc5> g37 = this.g;
        if (g37 != null) {
            cc5 a2 = g37.a();
            if (a2 != null) {
                String d2 = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d2)) {
                    a2.r.setBackgroundColor(Color.parseColor(d2));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                fw5 fw52 = this.i;
                if (fw52 != null) {
                    recyclerView.setAdapter(fw52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    ir4 ir42 = this.j;
                    if (ir42 != null) {
                        recyclerView2.setAdapter(ir42);
                        a2.y.setOnClickListener(new c(this));
                        a2.v.setOnClickListener(new d(this));
                        return;
                    }
                    pq7.n("mWatchAppAdapter");
                    throw null;
                }
                pq7.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w96
    public void u0(String str) {
        pq7.c(str, "category");
        g37<cc5> g37 = this.g;
        if (g37 != null) {
            cc5 a2 = g37.a();
            if (a2 != null) {
                fw5 fw5 = this.i;
                if (fw5 != null) {
                    int j2 = fw5.j(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + j2);
                    if (j2 >= 0) {
                        fw5 fw52 = this.i;
                        if (fw52 != null) {
                            fw52.p(j2);
                            a2.s.smoothScrollToPosition(j2);
                            return;
                        }
                        pq7.n("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                pq7.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
