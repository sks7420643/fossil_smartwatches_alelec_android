package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xz5 extends u47 implements r36 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<z55> l;
    @DexIgnore
    public q36 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return xz5.t;
        }

        @DexIgnore
        public final xz5 b() {
            return new xz5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xz5 f4219a;
        @DexIgnore
        public /* final */ /* synthetic */ z55 b;

        @DexIgnore
        public b(xz5 xz5, z55 z55) {
            this.f4219a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            q36 A6 = xz5.A6(this.f4219a);
            NumberPicker numberPicker2 = this.b.v;
            pq7.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.u;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xz5 f4220a;
        @DexIgnore
        public /* final */ /* synthetic */ z55 b;

        @DexIgnore
        public c(xz5 xz5, z55 z55) {
            this.f4220a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            q36 A6 = xz5.A6(this.f4220a);
            NumberPicker numberPicker2 = this.b.t;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.u;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xz5 f4221a;
        @DexIgnore
        public /* final */ /* synthetic */ z55 b;

        @DexIgnore
        public d(xz5 xz5, z55 z55) {
            this.f4221a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            q36 A6 = xz5.A6(this.f4221a);
            NumberPicker numberPicker2 = this.b.t;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.v;
            pq7.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xz5 b;

        @DexIgnore
        public e(xz5 xz5) {
            this.b = xz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            xz5.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ z55 b;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 c;

        @DexIgnore
        public f(z55 z55, dr7 dr7) {
            this.b = z55;
            this.c = dr7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.w;
            pq7.b(constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        z55 z55 = this.b;
                        pq7.b(z55, "it");
                        View n = z55.n();
                        pq7.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new il7("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = xz5.class.getSimpleName();
        pq7.b(simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ q36 A6(xz5 xz5) {
        q36 q36 = xz5.m;
        if (q36 != null) {
            return q36;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6(z55 z55) {
        String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d2)) {
            z55.w.setBackgroundColor(Color.parseColor(d2));
        }
        NumberPicker numberPicker = z55.t;
        pq7.b(numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = z55.t;
        pq7.b(numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        z55.t.setOnValueChangedListener(new b(this, z55));
        NumberPicker numberPicker3 = z55.v;
        pq7.b(numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = z55.v;
        pq7.b(numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        z55.v.setOnValueChangedListener(new c(this, z55));
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886102);
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886104);
        NumberPicker numberPicker5 = z55.u;
        pq7.b(numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = z55.u;
        pq7.b(numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        z55.u.setDisplayedValues(new String[]{c2, c3});
        z55.u.setOnValueChangedListener(new d(this, z55));
    }

    @DexIgnore
    public final void D6(int i) {
        q36 q36 = this.m;
        if (q36 != null) {
            q36.o(i);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.r36
    public void E(int i) {
        int i2;
        int i3 = i / 60;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 == 0) {
            i3 = 12;
        }
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                pq7.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                pq7.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i % 60);
                NumberPicker numberPicker3 = a2.u;
                pq7.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: E6 */
    public void M5(q36 q36) {
        pq7.c(q36, "presenter");
        this.m = q36;
    }

    @DexIgnore
    @Override // com.fossil.r36
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        z55 z55 = (z55) aq0.f(layoutInflater, 2131558550, viewGroup, false, this.k);
        z55.q.setOnClickListener(new e(this));
        pq7.b(z55, "binding");
        C6(z55);
        this.l = new g37<>(this, z55);
        return z55.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        q36 q36 = this.m;
        if (q36 != null) {
            q36.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        q36 q36 = this.m;
        if (q36 != null) {
            q36.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null) {
                dr7 dr7 = new dr7();
                dr7.element = null;
                dr7.element = (T) new f(a2, dr7);
                pq7.b(a2, "it");
                View n = a2.n();
                pq7.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(dr7.element);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.r36
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "title");
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
