package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum e94 {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    
    @DexIgnore
    public /* final */ int id;

    @DexIgnore
    public e94(int i) {
        this.id = i;
    }

    @DexIgnore
    public static e94 determineFrom(String str) {
        return str != null ? APP_STORE : DEVELOPER;
    }

    @DexIgnore
    public int getId() {
        return this.id;
    }

    @DexIgnore
    public String toString() {
        return Integer.toString(this.id);
    }
}
