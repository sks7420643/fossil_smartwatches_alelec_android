package com.fossil;

import com.fossil.cv2;
import com.fossil.dv2;
import com.fossil.uu2;
import com.fossil.vu2;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3145a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public cv2 c;
    @DexIgnore
    public BitSet d;
    @DexIgnore
    public BitSet e;
    @DexIgnore
    public Map<Integer, Long> f;
    @DexIgnore
    public Map<Integer, List<Long>> g;
    @DexIgnore
    public /* final */ /* synthetic */ pr3 h;

    @DexIgnore
    public rr3(pr3 pr3, String str) {
        this.h = pr3;
        this.f3145a = str;
        this.b = true;
        this.d = new BitSet();
        this.e = new BitSet();
        this.f = new zi0();
        this.g = new zi0();
    }

    @DexIgnore
    public rr3(pr3 pr3, String str, cv2 cv2, BitSet bitSet, BitSet bitSet2, Map<Integer, Long> map, Map<Integer, Long> map2) {
        this.h = pr3;
        this.f3145a = str;
        this.d = bitSet;
        this.e = bitSet2;
        this.f = map;
        this.g = new zi0();
        if (map2 != null) {
            for (Integer num : map2.keySet()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(map2.get(num));
                this.g.put(num, arrayList);
            }
        }
        this.b = false;
        this.c = cv2;
    }

    @DexIgnore
    public /* synthetic */ rr3(pr3 pr3, String str, cv2 cv2, BitSet bitSet, BitSet bitSet2, Map map, Map map2, sr3 sr3) {
        this(pr3, str, cv2, bitSet, bitSet2, map, map2);
    }

    @DexIgnore
    public /* synthetic */ rr3(pr3 pr3, String str, sr3 sr3) {
        this(pr3, str);
    }

    @DexIgnore
    public final uu2 a(int i) {
        ArrayList arrayList;
        List list;
        uu2.a S = uu2.S();
        S.x(i);
        S.B(this.b);
        cv2 cv2 = this.c;
        if (cv2 != null) {
            S.z(cv2);
        }
        cv2.a b0 = cv2.b0();
        b0.E(gr3.E(this.d));
        b0.z(gr3.E(this.e));
        if (this.f == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList(this.f.size());
            for (Integer num : this.f.keySet()) {
                int intValue = num.intValue();
                vu2.a L = vu2.L();
                L.x(intValue);
                L.y(this.f.get(Integer.valueOf(intValue)).longValue());
                arrayList2.add((vu2) ((e13) L.h()));
            }
            arrayList = arrayList2;
        }
        b0.G(arrayList);
        if (this.g == null) {
            list = Collections.emptyList();
        } else {
            ArrayList arrayList3 = new ArrayList(this.g.size());
            for (Integer num2 : this.g.keySet()) {
                dv2.a M = dv2.M();
                M.x(num2.intValue());
                List<Long> list2 = this.g.get(num2);
                if (list2 != null) {
                    Collections.sort(list2);
                    M.y(list2);
                }
                arrayList3.add((dv2) ((e13) M.h()));
            }
            list = arrayList3;
        }
        b0.H(list);
        S.y(b0);
        return (uu2) ((e13) S.h());
    }

    @DexIgnore
    public final void c(wr3 wr3) {
        int a2 = wr3.a();
        Boolean bool = wr3.c;
        if (bool != null) {
            this.e.set(a2, bool.booleanValue());
        }
        Boolean bool2 = wr3.d;
        if (bool2 != null) {
            this.d.set(a2, bool2.booleanValue());
        }
        if (wr3.e != null) {
            Long l = this.f.get(Integer.valueOf(a2));
            long longValue = wr3.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f.put(Integer.valueOf(a2), Long.valueOf(longValue));
            }
        }
        if (wr3.f != null) {
            List<Long> list = this.g.get(Integer.valueOf(a2));
            if (list == null) {
                list = new ArrayList<>();
                this.g.put(Integer.valueOf(a2), list);
            }
            if (wr3.i()) {
                list.clear();
            }
            if (e63.a() && this.h.m().y(this.f3145a, xg3.g0) && wr3.j()) {
                list.clear();
            }
            if (!e63.a() || !this.h.m().y(this.f3145a, xg3.g0)) {
                list.add(Long.valueOf(wr3.f.longValue() / 1000));
                return;
            }
            long longValue2 = wr3.f.longValue() / 1000;
            if (!list.contains(Long.valueOf(longValue2))) {
                list.add(Long.valueOf(longValue2));
            }
        }
    }
}
