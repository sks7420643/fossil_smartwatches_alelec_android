package com.fossil;

import android.content.Context;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Uri f1545a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public hw2(Uri uri) {
        this(null, uri, "", "", false, false, false, false, null);
    }

    @DexIgnore
    public hw2(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, rw2<Context, Boolean> rw2) {
        this.f1545a = uri;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final xv2<Double> a(String str, double d) {
        return xv2.a(this, str, -3.0d, true);
    }

    @DexIgnore
    public final xv2<Long> b(String str, long j) {
        return xv2.b(this, str, j, true);
    }

    @DexIgnore
    public final xv2<String> c(String str, String str2) {
        return xv2.c(this, str, str2, true);
    }

    @DexIgnore
    public final xv2<Boolean> d(String str, boolean z) {
        return xv2.d(this, str, z, true);
    }
}
