package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cy2 extends ss2 implements bw2 {
    @DexIgnore
    public cy2(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Bundle c(Bundle bundle) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        Parcel e = e(1, d);
        Bundle bundle2 = (Bundle) qt2.a(e, Bundle.CREATOR);
        e.recycle();
        return bundle2;
    }
}
