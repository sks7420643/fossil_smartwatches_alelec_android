package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.it0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jt0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends it0.b {
        @DexIgnore
        public a(Context context, b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((b) this.b).b(str, new it0.c<>(result));
        }
    }

    @DexIgnore
    public interface b extends it0.d {
        @DexIgnore
        void b(String str, it0.c<Parcel> cVar);
    }

    @DexIgnore
    public static Object a(Context context, b bVar) {
        return new a(context, bVar);
    }
}
