package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n77 implements Factory<m77> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ n77 f2480a; // = new n77();
    }

    @DexIgnore
    public static n77 a() {
        return a.f2480a;
    }

    @DexIgnore
    public static m77 c() {
        return new m77();
    }

    @DexIgnore
    /* renamed from: b */
    public m77 get() {
        return c();
    }
}
