package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t44 extends i44<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ t44 INSTANCE; // = new t44();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        i14.l(comparable);
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2) {
        return (E) ((Comparable) e44.INSTANCE.min(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) e44.INSTANCE.min(e, e2, e3, eArr));
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends Comparable> E max(Iterable<E> iterable) {
        return (E) ((Comparable) e44.INSTANCE.min(iterable));
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends Comparable> E max(Iterator<E> it) {
        return (E) ((Comparable) e44.INSTANCE.min(it));
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2) {
        return (E) ((Comparable) e44.INSTANCE.max(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) e44.INSTANCE.max(e, e2, e3, eArr));
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends Comparable> E min(Iterable<E> iterable) {
        return (E) ((Comparable) e44.INSTANCE.max(iterable));
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends Comparable> E min(Iterator<E> it) {
        return (E) ((Comparable) e44.INSTANCE.max(it));
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends Comparable> i44<S> reverse() {
        return i44.natural();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural().reverse()";
    }
}
