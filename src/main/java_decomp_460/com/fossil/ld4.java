package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ld4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2182a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ ld4 d;

    @DexIgnore
    public ld4(Throwable th, kd4 kd4) {
        this.f2182a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = kd4.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new ld4(cause, kd4) : null;
    }
}
