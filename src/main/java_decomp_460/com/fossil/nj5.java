package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj5 implements Factory<mj5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<wy6> f2533a;
    @DexIgnore
    public /* final */ Provider<yy6> b;
    @DexIgnore
    public /* final */ Provider<dt5> c;
    @DexIgnore
    public /* final */ Provider<at5> d;

    @DexIgnore
    public nj5(Provider<wy6> provider, Provider<yy6> provider2, Provider<dt5> provider3, Provider<at5> provider4) {
        this.f2533a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static nj5 a(Provider<wy6> provider, Provider<yy6> provider2, Provider<dt5> provider3, Provider<at5> provider4) {
        return new nj5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static mj5 c(wy6 wy6, yy6 yy6, dt5 dt5, at5 at5) {
        return new mj5(wy6, yy6, dt5, at5);
    }

    @DexIgnore
    /* renamed from: b */
    public mj5 get() {
        return c(this.f2533a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
