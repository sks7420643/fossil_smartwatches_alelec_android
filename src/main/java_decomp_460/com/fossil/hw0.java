package com.fossil;

import android.content.Context;
import com.fossil.mx0;
import com.fossil.qw0;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hw0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ mx0.c f1544a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ qw0.d d;
    @DexIgnore
    public /* final */ List<qw0.b> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ qw0.c g;
    @DexIgnore
    public /* final */ Executor h;
    @DexIgnore
    public /* final */ Executor i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Set<Integer> m;

    @DexIgnore
    public hw0(Context context, String str, mx0.c cVar, qw0.d dVar, List<qw0.b> list, boolean z, qw0.c cVar2, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.f1544a = cVar;
        this.b = context;
        this.c = str;
        this.d = dVar;
        this.e = list;
        this.f = z;
        this.g = cVar2;
        this.h = executor;
        this.i = executor2;
        this.j = z2;
        this.k = z3;
        this.l = z4;
        this.m = set;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        Set<Integer> set;
        boolean z = true;
        if ((i2 > i3) && this.l) {
            return false;
        }
        if (!this.k || ((set = this.m) != null && set.contains(Integer.valueOf(i2)))) {
            z = false;
        }
        return z;
    }
}
