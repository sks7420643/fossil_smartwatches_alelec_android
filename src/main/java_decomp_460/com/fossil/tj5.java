package com.fossil;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj5 {
    @DexIgnore
    public static wj5 a(Context context) {
        return (wj5) oa1.t(context);
    }

    @DexIgnore
    public static wj5 b(View view) {
        return (wj5) oa1.u(view);
    }

    @DexIgnore
    public static wj5 c(Fragment fragment) {
        return (wj5) oa1.v(fragment);
    }
}
