package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.fossil.wb1;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kc1 implements wb1<InputStream> {
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ mc1 c;
    @DexIgnore
    public InputStream d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements lc1 {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContentResolver f1897a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.f1897a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.lc1
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f1897a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements lc1 {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContentResolver f1898a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.f1898a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.lc1
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f1898a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore
    public kc1(Uri uri, mc1 mc1) {
        this.b = uri;
        this.c = mc1;
    }

    @DexIgnore
    public static kc1 b(Context context, Uri uri, lc1 lc1) {
        return new kc1(uri, new mc1(oa1.c(context).j().g(), lc1, oa1.c(context).e(), context.getContentResolver()));
    }

    @DexIgnore
    public static kc1 e(Context context, Uri uri) {
        return b(context, uri, new a(context.getContentResolver()));
    }

    @DexIgnore
    public static kc1 f(Context context, Uri uri) {
        return b(context, uri, new b(context.getContentResolver()));
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void a() {
        InputStream inputStream = this.d;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public gb1 c() {
        return gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void d(sa1 sa1, wb1.a<? super InputStream> aVar) {
        try {
            InputStream g = g();
            this.d = g;
            aVar.e(g);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.b(e);
        }
    }

    @DexIgnore
    public final InputStream g() throws FileNotFoundException {
        InputStream d2 = this.c.d(this.b);
        int a2 = d2 != null ? this.c.a(this.b) : -1;
        return a2 != -1 ? new zb1(d2, a2) : d2;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
