package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku5 extends iq4<a, iq4.d, iq4.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSampleRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Date f2097a;

        @DexIgnore
        public a(Date date) {
            pq7.c(date, "date");
            this.f2097a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.f2097a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples", f = "FetchHeartRateSamples.kt", l = {27, 45, 46}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ku5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ku5 ku5, qn7 qn7) {
            super(qn7);
            this.this$0 = ku5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = ku5.class.getSimpleName();
        pq7.b(simpleName, "FetchHeartRateSamples::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public ku5(HeartRateSampleRepository heartRateSampleRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository) {
        pq7.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        this.d = heartRateSampleRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ku5.a r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 432
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ku5.k(com.fossil.ku5$a, com.fossil.qn7):java.lang.Object");
    }
}
