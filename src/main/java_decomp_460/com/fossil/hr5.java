package com.fossil;

import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class hr5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1519a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[WeatherSettings.TEMP_UNIT.values().length];
        f1519a = iArr;
        iArr[WeatherSettings.TEMP_UNIT.CELSIUS.ordinal()] = 1;
        f1519a[WeatherSettings.TEMP_UNIT.FAHRENHEIT.ordinal()] = 2;
        int[] iArr2 = new int[LocationSource.ErrorState.values().length];
        b = iArr2;
        iArr2[LocationSource.ErrorState.LOCATION_PERMISSION_OFF.ordinal()] = 1;
        b[LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF.ordinal()] = 2;
        b[LocationSource.ErrorState.LOCATION_SERVICE_OFF.ordinal()] = 3;
    }
    */
}
