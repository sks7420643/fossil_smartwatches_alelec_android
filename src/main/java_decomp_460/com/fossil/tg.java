package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg extends qq7 implements rp7<mw, Boolean> {
    @DexIgnore
    public static /* final */ tg b; // = new tg();

    @DexIgnore
    public tg() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public Boolean invoke(mw mwVar) {
        mw mwVar2 = mwVar;
        lw lwVar = mwVar2.d;
        return Boolean.valueOf(lwVar == lw.t || lwVar == lw.g || lwVar == lw.o || mwVar2.e.d.b == f7.GATT_NULL);
    }
}
