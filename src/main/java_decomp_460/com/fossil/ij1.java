package com.fossil;

import com.fossil.cj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ij1 implements cj1, bj1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ cj1 f1636a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public volatile bj1 c;
    @DexIgnore
    public volatile bj1 d;
    @DexIgnore
    public cj1.a e;
    @DexIgnore
    public cj1.a f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public ij1(Object obj, cj1 cj1) {
        cj1.a aVar = cj1.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.b = obj;
        this.f1636a = cj1;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public void a(bj1 bj1) {
        synchronized (this.b) {
            if (!bj1.equals(this.c)) {
                this.f = cj1.a.FAILED;
                return;
            }
            this.e = cj1.a.FAILED;
            if (this.f1636a != null) {
                this.f1636a.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1, com.fossil.cj1
    public boolean b() {
        boolean z;
        synchronized (this.b) {
            z = this.d.b() || this.c.b();
        }
        return z;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.cj1] */
    /* JADX WARN: Type inference failed for: r2v3 */
    @Override // com.fossil.cj1
    public cj1 c() {
        Object r2;
        synchronized (this.b) {
            cj1 cj1 = this.f1636a;
            this = this;
            if (cj1 != null) {
                r2 = this.f1636a.c();
            }
        }
        return r2;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            this.e = cj1.a.CLEARED;
            this.f = cj1.a.CLEARED;
            this.d.clear();
            this.c.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean d(bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = m() && bj1.equals(this.c) && !b();
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean e(bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = n() && (bj1.equals(this.c) || this.e != cj1.a.SUCCESS);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void f() {
        synchronized (this.b) {
            this.g = true;
            try {
                if (!(this.e == cj1.a.SUCCESS || this.f == cj1.a.RUNNING)) {
                    this.f = cj1.a.RUNNING;
                    this.d.f();
                }
                if (this.g && this.e != cj1.a.RUNNING) {
                    this.e = cj1.a.RUNNING;
                    this.c.f();
                }
            } finally {
                this.g = false;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0017 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.fossil.bj1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.fossil.bj1 r4) {
        /*
            r3 = this;
            boolean r1 = r4 instanceof com.fossil.ij1
            r0 = 0
            if (r1 == 0) goto L_0x0018
            com.fossil.ij1 r4 = (com.fossil.ij1) r4
            com.fossil.bj1 r1 = r3.c
            if (r1 != 0) goto L_0x0019
            com.fossil.bj1 r1 = r4.c
            if (r1 != 0) goto L_0x0018
        L_0x000f:
            com.fossil.bj1 r1 = r3.d
            if (r1 != 0) goto L_0x0024
            com.fossil.bj1 r1 = r4.d
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            return r0
        L_0x0019:
            com.fossil.bj1 r1 = r3.c
            com.fossil.bj1 r2 = r4.c
            boolean r1 = r1.g(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x000f
        L_0x0024:
            com.fossil.bj1 r1 = r3.d
            com.fossil.bj1 r2 = r4.d
            boolean r1 = r1.g(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ij1.g(com.fossil.bj1):boolean");
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean h() {
        boolean z;
        synchronized (this.b) {
            z = this.e == cj1.a.CLEARED;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public void i(bj1 bj1) {
        synchronized (this.b) {
            if (bj1.equals(this.d)) {
                this.f = cj1.a.SUCCESS;
                return;
            }
            this.e = cj1.a.SUCCESS;
            if (this.f1636a != null) {
                this.f1636a.i(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == cj1.a.RUNNING;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean j() {
        boolean z;
        synchronized (this.b) {
            z = this.e == cj1.a.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean k(bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = l() && bj1.equals(this.c) && this.e != cj1.a.PAUSED;
        }
        return z;
    }

    @DexIgnore
    public final boolean l() {
        cj1 cj1 = this.f1636a;
        return cj1 == null || cj1.k(this);
    }

    @DexIgnore
    public final boolean m() {
        cj1 cj1 = this.f1636a;
        return cj1 == null || cj1.d(this);
    }

    @DexIgnore
    public final boolean n() {
        cj1 cj1 = this.f1636a;
        return cj1 == null || cj1.e(this);
    }

    @DexIgnore
    public void o(bj1 bj1, bj1 bj12) {
        this.c = bj1;
        this.d = bj12;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void pause() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = cj1.a.PAUSED;
                this.d.pause();
            }
            if (!this.e.isComplete()) {
                this.e = cj1.a.PAUSED;
                this.c.pause();
            }
        }
    }
}
