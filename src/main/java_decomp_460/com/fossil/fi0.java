package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fi0<K, V> implements Iterable<Map.Entry<K, V>> {
    @DexIgnore
    public c<K, V> b;
    @DexIgnore
    public c<K, V> c;
    @DexIgnore
    public WeakHashMap<f<K, V>, Boolean> d; // = new WeakHashMap<>();
    @DexIgnore
    public int e; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends e<K, V> {
        @DexIgnore
        public a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        @Override // com.fossil.fi0.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.e;
        }

        @DexIgnore
        @Override // com.fossil.fi0.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends e<K, V> {
        @DexIgnore
        public b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        @Override // com.fossil.fi0.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.d;
        }

        @DexIgnore
        @Override // com.fossil.fi0.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public /* final */ V c;
        @DexIgnore
        public c<K, V> d;
        @DexIgnore
        public c<K, V> e;

        @DexIgnore
        public c(K k, V v) {
            this.b = k;
            this.c = v;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.b.equals(cVar.b) && this.c.equals(cVar.c);
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            return this.b;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            return this.b.hashCode() ^ this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        @DexIgnore
        public String toString() {
            return ((Object) this.b) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> b;
        @DexIgnore
        public boolean c; // = true;

        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.fi0.f
        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.b;
            if (cVar == cVar2) {
                c<K, V> cVar3 = cVar2.e;
                this.b = cVar3;
                this.c = cVar3 == null;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public Map.Entry<K, V> next() {
            if (this.c) {
                this.c = false;
                this.b = fi0.this.b;
            } else {
                c<K, V> cVar = this.b;
                this.b = cVar != null ? cVar.d : null;
            }
            return this.b;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c) {
                return fi0.this.b != null;
            }
            c<K, V> cVar = this.b;
            return (cVar == null || cVar.d == null) ? false : true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> b;
        @DexIgnore
        public c<K, V> c;

        @DexIgnore
        public e(c<K, V> cVar, c<K, V> cVar2) {
            this.b = cVar2;
            this.c = cVar;
        }

        @DexIgnore
        @Override // com.fossil.fi0.f
        public void a(c<K, V> cVar) {
            if (this.b == cVar && cVar == this.c) {
                this.c = null;
                this.b = null;
            }
            c<K, V> cVar2 = this.b;
            if (cVar2 == cVar) {
                this.b = b(cVar2);
            }
            if (this.c == cVar) {
                this.c = e();
            }
        }

        @DexIgnore
        public abstract c<K, V> b(c<K, V> cVar);

        @DexIgnore
        public abstract c<K, V> c(c<K, V> cVar);

        @DexIgnore
        /* renamed from: d */
        public Map.Entry<K, V> next() {
            c<K, V> cVar = this.c;
            this.c = e();
            return cVar;
        }

        @DexIgnore
        public final c<K, V> e() {
            c<K, V> cVar = this.c;
            c<K, V> cVar2 = this.b;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return c(cVar);
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c != null;
        }
    }

    @DexIgnore
    public interface f<K, V> {
        @DexIgnore
        void a(c<K, V> cVar);
    }

    @DexIgnore
    public Map.Entry<K, V> a() {
        return this.b;
    }

    @DexIgnore
    public c<K, V> b(K k) {
        c<K, V> cVar = this.b;
        while (cVar != null && !cVar.b.equals(k)) {
            cVar = cVar.d;
        }
        return cVar;
    }

    @DexIgnore
    public fi0<K, V>.d c() {
        fi0<K, V>.d dVar = new d();
        this.d.put(dVar, Boolean.FALSE);
        return dVar;
    }

    @DexIgnore
    public Map.Entry<K, V> d() {
        return this.c;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> descendingIterator() {
        b bVar = new b(this.c, this.b);
        this.d.put(bVar, Boolean.FALSE);
        return bVar;
    }

    @DexIgnore
    public c<K, V> e(K k, V v) {
        c<K, V> cVar = new c<>(k, v);
        this.e++;
        c<K, V> cVar2 = this.c;
        if (cVar2 == null) {
            this.b = cVar;
            this.c = cVar;
        } else {
            cVar2.d = cVar;
            cVar.e = cVar2;
            this.c = cVar;
        }
        return cVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fi0)) {
            return false;
        }
        fi0 fi0 = (fi0) obj;
        if (size() != fi0.size()) {
            return false;
        }
        Iterator<Map.Entry<K, V>> it = iterator();
        Iterator<Map.Entry<K, V>> it2 = fi0.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry<K, V> next = it.next();
            Map.Entry<K, V> next2 = it2.next();
            if (next == null && next2 != null) {
                return false;
            }
            if (next != null && !next.equals(next2)) {
                return false;
            }
        }
        return !it.hasNext() && !it2.hasNext();
    }

    @DexIgnore
    public V f(K k, V v) {
        c<K, V> b2 = b(k);
        if (b2 != null) {
            return b2.c;
        }
        e(k, v);
        return null;
    }

    @DexIgnore
    public V g(K k) {
        c<K, V> b2 = b(k);
        if (b2 == null) {
            return null;
        }
        this.e--;
        if (!this.d.isEmpty()) {
            for (f<K, V> fVar : this.d.keySet()) {
                fVar.a(b2);
            }
        }
        c<K, V> cVar = b2.e;
        if (cVar != null) {
            cVar.d = b2.d;
        } else {
            this.b = b2.d;
        }
        c<K, V> cVar2 = b2.d;
        if (cVar2 != null) {
            cVar2.e = b2.e;
        } else {
            this.c = b2.e;
        }
        b2.d = null;
        b2.e = null;
        return b2.c;
    }

    @DexIgnore
    public int hashCode() {
        Iterator<Map.Entry<K, V>> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i = it.next().hashCode() + i;
        }
        return i;
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.b, this.c);
        this.d.put(aVar, Boolean.FALSE);
        return aVar;
    }

    @DexIgnore
    public int size() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Map.Entry<K, V>> it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
