package com.fossil;

import android.content.Intent;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ hz4 f1561a; // = new hz4();

    @DexIgnore
    public final String a(String str, String str2, String str3) {
        boolean z = false;
        pq7.c(str3, "socialId");
        if (!(str == null || str.length() == 0)) {
            return str;
        }
        if (str2 == null || str2.length() == 0) {
            z = true;
        }
        return !z ? str2 : str3;
    }

    @DexIgnore
    public final String b(String str, String str2, String str3) {
        pq7.c(str3, "socialId");
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(PortfolioApp.h0.c(), 2131887310);
        pq7.b(c, "LanguageHelper.getString\u2026allenge_format_full_name)");
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        String format = String.format(c, Arrays.copyOf(new Object[]{str, str2}, 2));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return pq7.a(format, " ") ? str3 : format;
    }

    @DexIgnore
    public final void c(boolean z) {
        Intent intent = new Intent("set_sync_data_event");
        intent.putExtra("set_sync_data_extra", z);
        ct0.b(PortfolioApp.h0.c()).d(intent);
    }
}
