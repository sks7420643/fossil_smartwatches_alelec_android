package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ui5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f3598a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ui5(String str, boolean z) {
        this.b = str;
        this.f3598a = z;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.f3598a;
    }
}
