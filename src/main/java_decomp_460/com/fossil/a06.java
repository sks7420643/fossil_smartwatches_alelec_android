package com.fossil;

import android.text.SpannableString;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a06 extends rv5<zz5> {
    @DexIgnore
    void F(boolean z);

    @DexIgnore
    void J0(SpannableString spannableString);

    @DexIgnore
    Object U();  // void declaration

    @DexIgnore
    Object W();  // void declaration

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm);

    @DexIgnore
    void n3(boolean z);

    @DexIgnore
    void o1(String str);

    @DexIgnore
    void p0(List<Alarm> list);

    @DexIgnore
    void q2(SpannableString spannableString);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    Object v0();  // void declaration
}
