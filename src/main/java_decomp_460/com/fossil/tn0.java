package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f3443a;

    @DexIgnore
    public tn0(Object obj) {
        this.f3443a = obj;
    }

    @DexIgnore
    public static tn0 a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new tn0(obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || tn0.class != obj.getClass()) {
            return false;
        }
        tn0 tn0 = (tn0) obj;
        Object obj2 = this.f3443a;
        return obj2 == null ? tn0.f3443a == null : obj2.equals(tn0.f3443a);
    }

    @DexIgnore
    public int hashCode() {
        Object obj = this.f3443a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DisplayCutoutCompat{" + this.f3443a + "}";
    }
}
