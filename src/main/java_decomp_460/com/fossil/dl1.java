package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ry1 b;
    @DexIgnore
    public /* final */ ry1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dl1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(ry1.class.getClassLoader());
            if (readParcelable != null) {
                ry1 ry1 = (ry1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new dl1(ry1, (ry1) readParcelable2);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dl1[] newArray(int i) {
            return new dl1[i];
        }
    }

    @DexIgnore
    public dl1(ry1 ry1, ry1 ry12) {
        this.b = ry1;
        this.c = ry12;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(dl1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            dl1 dl1 = (dl1) obj;
            if (!pq7.a(this.b, dl1.b)) {
                return false;
            }
            return !(pq7.a(this.c, dl1.c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.VersionInformation");
    }

    @DexIgnore
    public final ry1 getCurrentVersion() {
        return this.b;
    }

    @DexIgnore
    public final ry1 getSupportedVersion() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.B4, this.b), jd0.C4, this.c);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
