package com.fossil;

import com.fossil.zd1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ud1<K extends zd1, V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a<K, V> f3569a; // = new a<>();
    @DexIgnore
    public /* final */ Map<K, a<K, V>> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ K f3570a;
        @DexIgnore
        public List<V> b;
        @DexIgnore
        public a<K, V> c;
        @DexIgnore
        public a<K, V> d;

        @DexIgnore
        public a() {
            this(null);
        }

        @DexIgnore
        public a(K k) {
            this.d = this;
            this.c = this;
            this.f3570a = k;
        }

        @DexIgnore
        public void a(V v) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(v);
        }

        @DexIgnore
        public V b() {
            int c2 = c();
            if (c2 > 0) {
                return this.b.remove(c2 - 1);
            }
            return null;
        }

        @DexIgnore
        public int c() {
            List<V> list = this.b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }
    }

    @DexIgnore
    public static <K, V> void e(a<K, V> aVar) {
        a<K, V> aVar2 = aVar.d;
        aVar2.c = aVar.c;
        aVar.c.d = aVar2;
    }

    @DexIgnore
    public static <K, V> void g(a<K, V> aVar) {
        aVar.c.d = aVar;
        aVar.d.c = aVar;
    }

    @DexIgnore
    public V a(K k) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        b(aVar);
        return aVar.b();
    }

    @DexIgnore
    public final void b(a<K, V> aVar) {
        e(aVar);
        a<K, V> aVar2 = this.f3569a;
        aVar.d = aVar2;
        aVar.c = aVar2.c;
        g(aVar);
    }

    @DexIgnore
    public final void c(a<K, V> aVar) {
        e(aVar);
        a<K, V> aVar2 = this.f3569a;
        aVar.d = aVar2.d;
        aVar.c = aVar2;
        g(aVar);
    }

    @DexIgnore
    public void d(K k, V v) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            c(aVar);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        aVar.a(v);
    }

    @DexIgnore
    public V f() {
        for (a<K, V> aVar = this.f3569a.d; !aVar.equals(this.f3569a); aVar = aVar.d) {
            V b2 = aVar.b();
            if (b2 != null) {
                return b2;
            }
            e(aVar);
            this.b.remove(aVar.f3570a);
            aVar.f3570a.a();
        }
        return null;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (a<K, V> aVar = this.f3569a.c; !aVar.equals(this.f3569a); aVar = aVar.c) {
            z = true;
            sb.append('{');
            sb.append((Object) aVar.f3570a);
            sb.append(':');
            sb.append(aVar.c());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }
}
