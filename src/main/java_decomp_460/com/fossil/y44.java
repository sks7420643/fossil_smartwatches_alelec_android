package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y44<K, V> extends t24<K, V> {
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient t24<V, K> inverse;
    @DexIgnore
    public /* final */ transient K singleKey;
    @DexIgnore
    public /* final */ transient V singleValue;

    @DexIgnore
    public y44(K k, V v) {
        a24.a(k, v);
        this.singleKey = k;
        this.singleValue = v;
    }

    @DexIgnore
    public y44(K k, V v, t24<V, K> t24) {
        this.singleKey = k;
        this.singleValue = v;
        this.inverse = t24;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean containsKey(Object obj) {
        return this.singleKey.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean containsValue(Object obj) {
        return this.singleValue.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<Map.Entry<K, V>> createEntrySet() {
        return h34.of(x34.e(this.singleKey, this.singleValue));
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<K> createKeySet() {
        return h34.of(this.singleKey);
    }

    @DexIgnore
    @Override // com.fossil.a34, java.util.Map
    public V get(Object obj) {
        if (this.singleKey.equals(obj)) {
            return this.singleValue;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.t24, com.fossil.t24
    public t24<V, K> inverse() {
        t24<V, K> t24 = this.inverse;
        if (t24 != null) {
            return t24;
        }
        y44 y44 = new y44(this.singleValue, this.singleKey, this);
        this.inverse = y44;
        return y44;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }
}
