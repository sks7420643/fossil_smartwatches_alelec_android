package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.a37;
import com.fossil.ft5;
import com.fossil.iq4;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ly6 extends hy6 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ShineDevice e;
    @DexIgnore
    public /* final */ List<cl7<ShineDevice, String>> f; // = new ArrayList();
    @DexIgnore
    public /* final */ HashMap<String, List<Integer>> g; // = new HashMap<>();
    @DexIgnore
    public Handler h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ ArrayList<Device> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<SKUModel> l; // = new ArrayList<>();
    @DexIgnore
    public ul5 m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ d q; // = new d(this);
    @DexIgnore
    public /* final */ e r; // = new e(this);
    @DexIgnore
    public /* final */ iy6 s;
    @DexIgnore
    public /* final */ ft5 t;
    @DexIgnore
    public /* final */ DeviceRepository u;
    @DexIgnore
    public /* final */ mj5 v;
    @DexIgnore
    public /* final */ or5 w;
    @DexIgnore
    public /* final */ a37 x;
    @DexIgnore
    public /* final */ on5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            String simpleName = ly6.class.getSimpleName();
            pq7.b(simpleName, "PairingPresenter::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public void run() {
            ly6.this.l0(true);
            if (ly6.this.c0().isEmpty()) {
                ly6.this.s.x4();
                return;
            }
            iy6 iy6 = ly6.this.s;
            ly6 ly6 = ly6.this;
            iy6.o4(ly6.W(ly6.c0()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1", f = "PairingPresenter.kt", l = {425}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ShineDevice $shineDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ly6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$deviceName$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    DeviceRepository deviceRepository = this.this$0.this$0.u;
                    String serial = this.this$0.$shineDevice.getSerial();
                    pq7.b(serial, "shineDevice.serial");
                    return deviceRepository.getDeviceNameBySerial(serial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ly6 ly6, ShineDevice shineDevice, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ly6;
            this.$shineDevice = shineDevice;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$shineDevice, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            T t;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) g;
            Iterator<T> it = this.this$0.c0().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (ao7.a(pq7.a(((ShineDevice) next.getFirst()).getSerial(), this.$shineDevice.getSerial())).booleanValue()) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 == null) {
                this.this$0.c0().add(new cl7<>(this.$shineDevice, str));
            } else {
                ((ShineDevice) t2.getFirst()).updateRssi(this.$shineDevice.getRssi());
            }
            iy6 iy6 = this.this$0.s;
            ly6 ly6 = this.this$0;
            iy6.t2(ly6.W(ly6.c0()));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.e<ft5.j, ft5.i> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ly6 f2279a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1", f = "PairingPresenter.kt", l = {312}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ft5.i $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ly6$d$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ly6$d$a$a  reason: collision with other inner class name */
            public static final class C0148a extends ko7 implements vp7<iv7, qn7<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0148a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0148a aVar = new C0148a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super HashMap<String, String>> qn7) {
                    return ((C0148a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        a aVar = this.this$0;
                        return aVar.this$0.f2279a.f0(aVar.$errorValue.a());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, ft5.i iVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$errorValue = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$errorValue, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = ly6.z.a();
                    local.d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
                    dv7 h = this.this$0.f2279a.h();
                    C0148a aVar = new C0148a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                HashMap hashMap = (HashMap) g;
                if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
                    this.this$0.f2279a.U("pair_fail", hashMap);
                }
                this.this$0.f2279a.s.a();
                ft5.i iVar = this.$errorValue;
                if (iVar instanceof ft5.d) {
                    if (this.this$0.f2279a.p) {
                        iy6 iy6 = this.this$0.f2279a.s;
                        ShineDevice shineDevice = this.this$0.f2279a.e;
                        if (shineDevice != null) {
                            String serial = shineDevice.getSerial();
                            pq7.b(serial, "mPairingDevice!!.serial");
                            iy6.i2(serial);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        iy6 iy62 = this.this$0.f2279a.s;
                        ShineDevice shineDevice2 = this.this$0.f2279a.e;
                        if (shineDevice2 != null) {
                            String serial2 = shineDevice2.getSerial();
                            pq7.b(serial2, "mPairingDevice!!.serial");
                            iy62.l4(serial2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else if (iVar instanceof ft5.k) {
                    this.this$0.f2279a.s.h4(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
                } else if (iVar instanceof ft5.e) {
                    this.this$0.f2279a.s.D0();
                }
                String valueOf = String.valueOf(this.$errorValue.b());
                sl5 b = ck5.f.b("setup_device_session_optional_error");
                b.a("error_code", valueOf);
                ul5 Z = this.this$0.f2279a.Z();
                if (Z != null) {
                    Z.a(b);
                }
                ul5 Z2 = this.this$0.f2279a.Z();
                if (Z2 != null) {
                    Z2.c(valueOf);
                }
                ck5.f.k("setup_device_session");
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1", f = "PairingPresenter.kt", l = {283, 289, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $devicePairingSerial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    String str;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        String deviceNameBySerial = this.this$0.this$0.f2279a.u.getDeviceNameBySerial(this.this$0.$devicePairingSerial);
                        MisfitDeviceProfile m = nk5.o.j().m(this.this$0.$devicePairingSerial);
                        if (m == null || (str = m.getFirmwareVersion()) == null) {
                            str = "";
                        }
                        ck5.f.g().h(nk5.o.m(this.this$0.$devicePairingSerial), deviceNameBySerial, str);
                        return this.this$0.this$0.f2279a.x.e(new a37.a(this.this$0.$devicePairingSerial), null);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ly6$d$b$b")
            @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ly6$d$b$b  reason: collision with other inner class name */
            public static final class C0149b extends ko7 implements vp7<iv7, qn7<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0149b(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0149b bVar = new C0149b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super HashMap<String, String>> qn7) {
                    return ((C0149b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        b bVar = this.this$0;
                        return bVar.this$0.f2279a.f0(bVar.$devicePairingSerial);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$devicePairingSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$devicePairingSerial, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0064  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00ab  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 0
                    r6 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ae
                    if (r0 == r4) goto L_0x0066
                    if (r0 == r5) goto L_0x002f
                    if (r0 != r6) goto L_0x0027
                    java.lang.Object r0 = r8.L$1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                L_0x001d:
                    com.fossil.ly6$d r0 = r8.this$0
                    com.fossil.ly6 r0 = r0.f2279a
                    r0.j0()
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0026:
                    return r0
                L_0x0027:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x002f:
                    java.lang.Object r0 = r8.L$1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r9)
                L_0x003a:
                    com.fossil.ly6$d r2 = r8.this$0
                    com.fossil.ly6 r2 = r2.f2279a
                    com.fossil.ul5 r2 = r2.Z()
                    if (r2 == 0) goto L_0x0049
                    java.lang.String r4 = ""
                    r2.c(r4)
                L_0x0049:
                    com.fossil.ck5$a r2 = com.fossil.ck5.f
                    java.lang.String r4 = "setup_device_session"
                    r2.k(r4)
                    com.fossil.ly6$d r2 = r8.this$0
                    com.fossil.ly6 r2 = r2.f2279a
                    com.fossil.or5 r2 = com.fossil.ly6.H(r2)
                    r8.L$0 = r1
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r0 = r2.n(r8)
                    if (r0 != r3) goto L_0x001d
                    r0 = r3
                    goto L_0x0026
                L_0x0066:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                    r2 = r0
                    r1 = r9
                L_0x006f:
                    r0 = r1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.String r1 = "Style_Number"
                    boolean r1 = r0.containsKey(r1)
                    if (r1 == 0) goto L_0x0092
                    java.lang.String r1 = "Device_Name"
                    boolean r1 = r0.containsKey(r1)
                    if (r1 == 0) goto L_0x0092
                    com.fossil.ly6$d r1 = r8.this$0
                    com.fossil.ly6 r1 = r1.f2279a
                    java.lang.String r4 = "pair_success"
                    r1.U(r4, r0)
                    com.fossil.ly6$d r1 = r8.this$0
                    com.fossil.ly6 r1 = r1.f2279a
                    r1.V(r0)
                L_0x0092:
                    com.fossil.ly6$d r1 = r8.this$0
                    com.fossil.ly6 r1 = r1.f2279a
                    com.fossil.dv7 r1 = com.fossil.ly6.D(r1)
                    com.fossil.ly6$d$b$a r4 = new com.fossil.ly6$d$b$a
                    r4.<init>(r8, r7)
                    r8.L$0 = r2
                    r8.L$1 = r0
                    r8.label = r5
                    java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r8)
                    if (r1 != r3) goto L_0x00cd
                    r0 = r3
                    goto L_0x0026
                L_0x00ae:
                    com.fossil.el7.b(r9)
                    com.fossil.iv7 r0 = r8.p$
                    com.fossil.ly6$d r1 = r8.this$0
                    com.fossil.ly6 r1 = r1.f2279a
                    com.fossil.dv7 r1 = com.fossil.ly6.C(r1)
                    com.fossil.ly6$d$b$b r2 = new com.fossil.ly6$d$b$b
                    r2.<init>(r8, r7)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
                    if (r1 != r3) goto L_0x00d0
                    r0 = r3
                    goto L_0x0026
                L_0x00cd:
                    r1 = r2
                    goto L_0x003a
                L_0x00d0:
                    r2 = r0
                    goto L_0x006f
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ly6.d.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(ly6 ly6) {
            this.f2279a = ly6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ft5.i iVar) {
            pq7.c(iVar, "errorValue");
            xw7 unused = gu7.d(this.f2279a.k(), null, null, new a(this, iVar, null), 3, null);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ft5.j jVar) {
            pq7.c(jVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ly6.z.a();
            local.d(a2, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.f2279a.y.y0() + ", response=" + jVar.getClass().getSimpleName());
            if (jVar instanceof ft5.a) {
                this.f2279a.s.a();
                this.f2279a.s.v2(((ft5.a) jVar).a(), this.f2279a.a0());
            } else if (jVar instanceof ft5.f) {
                this.f2279a.s.a();
                this.f2279a.s.F2(((ft5.f) jVar).a(), this.f2279a.a0());
            } else if (jVar instanceof ft5.m) {
                ft5.m mVar = (ft5.m) jVar;
                this.f2279a.s.P3(mVar.a(), this.f2279a.a0(), mVar.b());
            } else if (jVar instanceof ft5.b) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = ly6.z.a();
                StringBuilder sb = new StringBuilder();
                sb.append("authorizeDeviceResponse, isStartTimer=");
                ft5.b bVar = (ft5.b) jVar;
                sb.append(bVar.a());
                local2.d(a3, sb.toString());
                if (bVar.a()) {
                    this.f2279a.s.b4(true);
                    return;
                }
                this.f2279a.p = false;
                this.f2279a.s.b();
                this.f2279a.s.b4(false);
            } else if (jVar instanceof ft5.l) {
                String deviceId = ((ft5.l) jVar).a().getDeviceId();
                PortfolioApp.h0.c().S1(this.f2279a.v, false, 13);
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, deviceId, ly6.z.a(), "Pair Success");
                xw7 unused = gu7.d(this.f2279a.k(), null, null, new b(this, deviceId, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ly6 f2280a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(ly6 ly6) {
            this.f2280a = ly6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            T t2;
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            ShineDevice shineDevice = (ShineDevice) intent.getParcelableExtra("device");
            if (shineDevice == null) {
                FLogger.INSTANCE.getLocal().d(ly6.z.a(), "scanReceiver - ShineDevice is NULL!!!");
                return;
            }
            String serial = shineDevice.getSerial();
            if (TextUtils.isEmpty(serial)) {
                FLogger.INSTANCE.getLocal().d(ly6.z.a(), "scanReceiver - ShineDeviceSerial is NULL => wearOS device=" + shineDevice);
                this.f2280a.e0(shineDevice);
                return;
            }
            FLogger.INSTANCE.getLocal().d(ly6.z.a(), "scanReceiver - receive device serial=" + serial + " allSkuModelSize " + this.f2280a.Y().size());
            nk5 j = nk5.o.j();
            if (serial == null) {
                pq7.i();
                throw null;
            } else if (j.n(serial, this.f2280a.Y())) {
                int rssi = shineDevice.getRssi();
                this.f2280a.k0(serial, rssi);
                Iterator<T> it = this.f2280a.c0().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(((ShineDevice) next.getFirst()).getSerial(), serial)) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                Iterator<T> it2 = this.f2280a.d0().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (pq7.a(next2.getDeviceId(), serial)) {
                        t2 = next2;
                        break;
                    }
                }
                T t4 = t2;
                if (t4 == null && t3 == null) {
                    FLogger.INSTANCE.getLocal().d(ly6.z.a(), "Add device " + serial + " to list");
                    this.f2280a.T(shineDevice);
                } else if (t4 != null || t3 == null || this.f2280a.b0()) {
                    FLogger.INSTANCE.getLocal().d(ly6.z.a(), "Device already in list, ignore it " + serial);
                } else {
                    FLogger.INSTANCE.getLocal().d(ly6.z.a(), "Pre-scan is not complete, update RSSI for scanned devices");
                    int X = this.f2280a.X(serial);
                    ((ShineDevice) t3.getFirst()).updateRssi(X == Integer.MIN_VALUE ? rssi : X);
                    iy6 iy6 = this.f2280a.s;
                    ly6 ly6 = this.f2280a;
                    iy6.t2(ly6.W(ly6.c0()));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1", f = "PairingPresenter.kt", l = {99, 101}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ly6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Device>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.u.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends SKUModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends SKUModel>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.u.getSupportedSku();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ly6 ly6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ly6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0113  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 292
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ly6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements iq4.e<ut5, st5> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ly6 f2281a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public g(ly6 ly6, String str) {
            this.f2281a = ly6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(st5 st5) {
            pq7.c(st5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ly6.z.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + st5.a());
            this.f2281a.s.a();
            int i = my6.f2439a[st5.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    this.f2281a.s.s6(this.b);
                } else if (i == 3) {
                    this.f2281a.s.v3(this.b);
                } else if (i != 4) {
                    this.f2281a.s.K2(st5.a().ordinal(), this.b);
                } else {
                    FLogger.INSTANCE.getLocal().d(ly6.z.a(), "User deny stopping workout");
                }
            } else if (st5.b() != null) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(new ArrayList(st5.b()));
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                iy6 iy6 = this.f2281a.s;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    iy6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ut5 ut5) {
            pq7.c(ut5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ly6.z.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            this.f2281a.x();
        }
    }

    @DexIgnore
    public ly6(iy6 iy6, ft5 ft5, DeviceRepository deviceRepository, mj5 mj5, or5 or5, a37 a37, on5 on5) {
        pq7.c(iy6, "mPairingView");
        pq7.c(ft5, "mLinkDeviceUseCase");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(or5, "mMusicControlComponent");
        pq7.c(a37, "mSetNotificationUseCase");
        pq7.c(on5, "mSharePrefs");
        this.s = iy6;
        this.t = ft5;
        this.u = deviceRepository;
        this.v = mj5;
        this.w = or5;
        this.x = a37;
        this.y = on5;
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void A(ShineDevice shineDevice) {
        pq7.c(shineDevice, "device");
        jn5 jn5 = jn5.b;
        iy6 iy6 = this.s;
        if (iy6 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
        } else if (jn5.c(jn5, ((rx6) iy6).getContext(), jn5.a.PAIR_DEVICE, false, false, false, null, 60, null)) {
            this.s.b();
            this.o = false;
            p0();
            this.e = shineDevice;
            String J = PortfolioApp.h0.c().J();
            if (!vt7.l(J)) {
                q0(J);
            } else {
                x();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void B() {
        this.t.H();
        iy6 iy6 = this.s;
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            pq7.b(serial, "mPairingDevice!!.serial");
            iy6.v2(serial, this.j);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void T(ShineDevice shineDevice) {
        pq7.c(shineDevice, "shineDevice");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "addDevice serial=" + shineDevice.getSerial());
        String serial = shineDevice.getSerial();
        pq7.b(serial, "shineDevice.serial");
        int X = X(serial);
        if (X == Integer.MIN_VALUE) {
            X = shineDevice.getRssi();
        }
        shineDevice.updateRssi(X);
        xw7 unused = gu7.d(k(), null, null, new c(this, shineDevice, null), 3, null);
    }

    @DexIgnore
    public final void U(String str, Map<String, String> map) {
        pq7.c(str, Constants.EVENT);
        pq7.c(map, "values");
        ck5.f.g().l(str, map);
    }

    @DexIgnore
    public final void V(Map<String, String> map) {
        pq7.c(map, "properties");
        ck5.f.g().r(map);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0010 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.cl7<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> W(java.util.List<com.fossil.cl7<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> r9) {
        /*
            r8 = this;
            r4 = 1
            r3 = 0
            java.lang.String r0 = "devices"
            com.fossil.pq7.c(r9, r0)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r6 = r9.iterator()
        L_0x0010:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r1 = r6.next()
            r0 = r1
            com.fossil.cl7 r0 = (com.fossil.cl7) r0
            java.lang.Object r2 = r0.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r2 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r2
            int r2 = r2.getRssi()
            r7 = -150(0xffffffffffffff6a, float:NaN)
            if (r2 > r7) goto L_0x0043
            java.lang.Object r0 = r0.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r0 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r0
            java.lang.String r0 = r0.getSerial()
            java.lang.String r2 = "it.first.serial"
            com.fossil.pq7.b(r0, r2)
            int r0 = r0.length()
            if (r0 != 0) goto L_0x004a
            r0 = r4
        L_0x0041:
            if (r0 == 0) goto L_0x0051
        L_0x0043:
            r0 = r4
        L_0x0044:
            if (r0 == 0) goto L_0x0010
            r5.add(r1)
            goto L_0x0010
        L_0x004a:
            r0 = r3
            goto L_0x0041
        L_0x004c:
            java.util.List r0 = com.fossil.pm7.j0(r5)
            return r0
        L_0x0051:
            r0 = r3
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ly6.W(java.util.List):java.util.List");
    }

    @DexIgnore
    public final int X(String str) {
        pq7.c(str, "serial");
        if (this.g.containsKey(str)) {
            double d2 = 0.0d;
            List<Integer> list = this.g.get(str);
            if (list != null) {
                for (Integer num : list) {
                    d2 += (double) num.intValue();
                }
                int size = list.size();
                if (size <= 0) {
                    size = 1;
                }
                return (int) (d2 / ((double) size));
            }
        }
        return RecyclerView.UNDEFINED_DURATION;
    }

    @DexIgnore
    public final ArrayList<SKUModel> Y() {
        return this.l;
    }

    @DexIgnore
    public final ul5 Z() {
        return this.m;
    }

    @DexIgnore
    public final boolean a0() {
        return this.j;
    }

    @DexIgnore
    public final boolean b0() {
        return this.n;
    }

    @DexIgnore
    public final List<cl7<ShineDevice, String>> c0() {
        return this.f;
    }

    @DexIgnore
    public final ArrayList<Device> d0() {
        return this.k;
    }

    @DexIgnore
    public final void e0(ShineDevice shineDevice) {
        T t2;
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (pq7.a(((ShineDevice) next.getFirst()).getMacAddress(), shineDevice.getMacAddress())) {
                t2 = next;
                break;
            }
        }
        if (t2 == null) {
            ArrayList<SKUModel> arrayList = this.l;
            ArrayList<SKUModel> arrayList2 = new ArrayList();
            for (T t3 : arrayList) {
                if (pq7.a(t3.getGroupName(), "WearOS")) {
                    arrayList2.add(t3);
                }
            }
            if (!arrayList2.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = z.a();
                local.d(a2, "wearOS devices in DB=" + arrayList2);
                for (SKUModel sKUModel : arrayList2) {
                    String name = shineDevice.getName();
                    pq7.b(name, "shineDevice.name");
                    if (wt7.u(name, String.valueOf(sKUModel.getDeviceName()), true)) {
                        FLogger.INSTANCE.getLocal().d(z.a(), "Detected wearOS device is acceptable");
                        this.f.add(new cl7<>(shineDevice, shineDevice.getName()));
                        this.s.t2(W(this.f));
                        return;
                    }
                }
            }
        }
    }

    @DexIgnore
    public final HashMap<String, String> f0(String str) {
        pq7.c(str, "serial");
        HashMap<String, String> hashMap = new HashMap<>();
        SKUModel skuModelBySerialPrefix = this.u.getSkuModelBySerialPrefix(nk5.o.m(str));
        if (skuModelBySerialPrefix != null) {
            String sku = skuModelBySerialPrefix.getSku();
            if (sku != null) {
                hashMap.put("Style_Number", sku);
                String deviceName = skuModelBySerialPrefix.getDeviceName();
                if (deviceName != null) {
                    hashMap.put("Device_Name", deviceName);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final boolean g0() {
        Locale locale = Locale.getDefault();
        pq7.b(locale, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale.getLanguage())) {
            return false;
        }
        Locale locale2 = Locale.getDefault();
        pq7.b(locale2, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale2.getCountry())) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        Locale locale3 = Locale.getDefault();
        pq7.b(locale3, "Locale.getDefault()");
        sb.append(locale3.getLanguage());
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        Locale locale4 = Locale.getDefault();
        pq7.b(locale4, "Locale.getDefault()");
        sb.append(locale4.getCountry());
        String sb2 = sb.toString();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "language: " + sb2);
        return vt7.j(sb2, "zh_CN", true) || vt7.j(sb2, "zh_SG", true);
    }

    @DexIgnore
    public boolean h0() {
        return this.i;
    }

    @DexIgnore
    public void i0() {
        if (!this.i && this.o) {
            if (this.f.isEmpty()) {
                this.s.Z0();
                this.n = false;
                Handler handler = this.h;
                if (handler != null) {
                    handler.postDelayed(new b(), (long) 15000);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                this.s.o4(W(this.f));
            }
            o0();
        }
    }

    @DexIgnore
    public final void j0() {
        FLogger.INSTANCE.getLocal().d(z.a(), "onUserContinueToNextStep");
        this.s.a();
        if (nk5.o.x(PortfolioApp.h0.c().J())) {
            this.s.d0();
        } else {
            this.s.l();
        }
    }

    @DexIgnore
    public final void k0(String str, int i2) {
        pq7.c(str, "serial");
        int i3 = (i2 == 0 || i2 == -999999) ? 0 : i2;
        if (this.g.containsKey(str)) {
            List<Integer> list = this.g.get(str);
            if (list != null && !list.contains(0)) {
                if (list.size() < 5) {
                    list.add(Integer.valueOf(i2));
                    return;
                }
                list.remove(0);
                list.add(Integer.valueOf(i2));
                return;
            }
            return;
        }
        this.g.put(str, hm7.i(Integer.valueOf(i3)));
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final void l0(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        Handler handler = this.h;
        if (handler != null) {
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            } else {
                pq7.i();
                throw null;
            }
        }
        p0();
        this.t.O();
        ct0.b(PortfolioApp.h0.c()).e(this.r);
    }

    @DexIgnore
    public void m0(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void n() {
        FLogger.INSTANCE.getLocal().d(z.a(), "cancelPairDevice()");
        PortfolioApp c2 = PortfolioApp.h0.c();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            pq7.b(serial, "mPairingDevice!!.serial");
            c2.s(serial);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void n0() {
        this.s.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void o(String str) {
        pq7.c(str, "serial");
        this.s.b();
        PortfolioApp.h0.c().x(str, true);
    }

    @DexIgnore
    public void o0() {
        try {
            IButtonConnectivity b2 = PortfolioApp.h0.b();
            if (b2 != null) {
                b2.deviceStartScan();
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void p(String str) {
        pq7.c(str, "serial");
        this.s.b();
        PortfolioApp.h0.c().x(str, false);
    }

    @DexIgnore
    public final void p0() {
        try {
            IButtonConnectivity b2 = PortfolioApp.h0.b();
            if (b2 != null) {
                b2.deviceStopScan();
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public ShineDevice q() {
        return this.e;
    }

    @DexIgnore
    public void q0(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "syncDevice - serial=" + str);
        this.v.b(str).e(new tt5(!nk5.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new g(this, str));
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void r(int i2) {
        this.s.e0();
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public boolean s() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void t() {
        this.s.x4();
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void u(ShineDevice shineDevice) {
        pq7.c(shineDevice, "device");
        this.s.y4(g0());
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void v() {
        List<String> i2 = hm7.i(um5.c(PortfolioApp.h0.c(), 2131886893), um5.c(PortfolioApp.h0.c(), 2131886894));
        this.p = true;
        this.s.G3(i2);
        this.t.E(30000);
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void w(ShineDevice shineDevice) {
        pq7.c(shineDevice, "pairingDevice");
        this.e = shineDevice;
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void x() {
        this.s.b();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = z.a();
            local.d(a2, "pairDevice - serial=" + shineDevice.getSerial());
            String serial = shineDevice.getSerial();
            pq7.b(serial, "it.serial");
            String macAddress = shineDevice.getMacAddress();
            pq7.b(macAddress, "it.macAddress");
            ft5.h hVar = new ft5.h(serial, macAddress);
            ul5 c2 = ck5.f.c("setup_device_session");
            this.m = c2;
            ck5.f.a("setup_device_session", c2);
            PortfolioApp c3 = PortfolioApp.h0.c();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            c3.t(communicateMode, "", communicateMode, hVar.a());
            this.t.e(hVar, this.q);
            ul5 ul5 = this.m;
            if (ul5 != null) {
                ul5.i();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void y(boolean z2) {
        this.o = z2;
    }

    @DexIgnore
    @Override // com.fossil.hy6
    public void z(boolean z2) {
        this.j = z2;
    }
}
