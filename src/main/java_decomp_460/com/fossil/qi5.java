package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qi5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f2986a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public qi5(boolean z, String str) {
        this.f2986a = z;
        this.b = str;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.f2986a;
    }
}
