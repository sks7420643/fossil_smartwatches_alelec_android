package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p48 implements c58 {
    @DexIgnore
    public byte b;
    @DexIgnore
    public /* final */ w48 c;
    @DexIgnore
    public /* final */ Inflater d;
    @DexIgnore
    public /* final */ q48 e;
    @DexIgnore
    public /* final */ CRC32 f; // = new CRC32();

    @DexIgnore
    public p48(c58 c58) {
        pq7.c(c58, "source");
        this.c = new w48(c58);
        Inflater inflater = new Inflater(true);
        this.d = inflater;
        this.e = new q48(this.c, inflater);
    }

    @DexIgnore
    public final void a(String str, int i, int i2) {
        if (i2 != i) {
            String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}, 3));
            pq7.b(format, "java.lang.String.format(this, *args)");
            throw new IOException(format);
        }
    }

    @DexIgnore
    public final void b() throws IOException {
        this.c.j0(10);
        byte M = this.c.b.M(3);
        boolean z = ((M >> 1) & 1) == 1;
        if (z) {
            f(this.c.b, 0, 10);
        }
        a("ID1ID2", 8075, this.c.readShort());
        this.c.skip(8);
        if (((M >> 2) & 1) == 1) {
            this.c.j0(2);
            if (z) {
                f(this.c.b, 0, 2);
            }
            long V = (long) this.c.b.V();
            this.c.j0(V);
            if (z) {
                f(this.c.b, 0, V);
            }
            this.c.skip(V);
        }
        if (((M >> 3) & 1) == 1) {
            long a2 = this.c.a((byte) 0);
            if (a2 != -1) {
                if (z) {
                    f(this.c.b, 0, 1 + a2);
                }
                this.c.skip(1 + a2);
            } else {
                throw new EOFException();
            }
        }
        if (((M >> 4) & 1) == 1) {
            long a3 = this.c.a((byte) 0);
            if (a3 != -1) {
                if (z) {
                    f(this.c.b, 0, 1 + a3);
                }
                this.c.skip(1 + a3);
            } else {
                throw new EOFException();
            }
        }
        if (z) {
            a("FHCRC", this.c.h(), (short) ((int) this.f.getValue()));
            this.f.reset();
        }
    }

    @DexIgnore
    public final void c() throws IOException {
        a("CRC", this.c.f(), (int) this.f.getValue());
        a("ISIZE", this.c.f(), (int) this.d.getBytesWritten());
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    @Override // com.fossil.c58
    public long d0(i48 i48, long j) throws IOException {
        pq7.c(i48, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (i == 0) {
            return 0;
        } else {
            if (this.b == 0) {
                b();
                this.b = (byte) 1;
            }
            if (this.b == 1) {
                long p0 = i48.p0();
                long d0 = this.e.d0(i48, j);
                if (d0 != -1) {
                    f(i48, p0, d0);
                    return d0;
                }
                this.b = (byte) 2;
            }
            if (this.b == 2) {
                c();
                this.b = (byte) 3;
                if (!this.c.u()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    @DexIgnore
    @Override // com.fossil.c58
    public d58 e() {
        return this.c.e();
    }

    @DexIgnore
    public final void f(i48 i48, long j, long j2) {
        x48 x48 = i48.b;
        if (x48 != null) {
            do {
                int i = x48.c;
                int i2 = x48.b;
                if (j >= ((long) (i - i2))) {
                    j -= (long) (i - i2);
                    x48 = x48.f;
                } else {
                    while (j2 > 0) {
                        int i3 = (int) (((long) x48.b) + j);
                        int min = (int) Math.min((long) (x48.c - i3), j2);
                        this.f.update(x48.f4040a, i3, min);
                        j2 -= (long) min;
                        x48 = x48.f;
                        if (x48 != null) {
                            j = 0;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    return;
                }
            } while (x48 != null);
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }
}
