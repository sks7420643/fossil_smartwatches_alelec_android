package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f4237a; // = x01.f("PackageManagerHelper");

    @DexIgnore
    public static void a(Context context, Class<?> cls, boolean z) {
        String str = "enabled";
        try {
            context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, cls.getName()), z ? 1 : 2, 1);
            x01.c().a(f4237a, String.format("%s %s", cls.getName(), z ? "enabled" : "disabled"), new Throwable[0]);
        } catch (Exception e) {
            x01 c = x01.c();
            String str2 = f4237a;
            String name = cls.getName();
            if (!z) {
                str = "disabled";
            }
            c.a(str2, String.format("%s could not be %s", name, str), e);
        }
    }
}
