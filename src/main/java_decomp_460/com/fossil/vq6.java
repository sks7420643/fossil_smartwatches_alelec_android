package com.fossil;

import com.fossil.nu5;
import com.fossil.tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq6 extends qq6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public /* final */ rq6 e;
    @DexIgnore
    public /* final */ nu5 f;
    @DexIgnore
    public /* final */ uq4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return vq6.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.d<nu5.d, nu5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vq6 f3808a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(vq6 vq6) {
            this.f3808a = vq6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(nu5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vq6.i.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + cVar);
            if (this.f3808a.e.isActive()) {
                this.f3808a.e.k();
                int a3 = cVar.a();
                if (a3 == 400004) {
                    this.f3808a.e.P2();
                } else if (a3 != 403005) {
                    this.f3808a.e.I0(cVar.a(), cVar.b());
                } else {
                    this.f3808a.e.f4();
                }
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(nu5.d dVar) {
            pq7.c(dVar, "successResponse");
            if (this.f3808a.e.isActive()) {
                this.f3808a.e.k();
                this.f3808a.e.l6();
            }
        }
    }

    /*
    static {
        String simpleName = vq6.class.getSimpleName();
        pq7.b(simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public vq6(rq6 rq6, nu5 nu5, uq4 uq4) {
        pq7.c(rq6, "mView");
        pq7.c(nu5, "mChangePasswordUseCase");
        pq7.c(uq4, "mUseCaseHandler");
        this.e = rq6;
        this.f = nu5;
        this.g = uq4;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(h, "presenter starts");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.qq6
    public void n(String str, String str2) {
        pq7.c(str, "oldPass");
        pq7.c(str2, "newPass");
        if (!o37.b(PortfolioApp.h0.c())) {
            this.e.I0(601, null);
            return;
        }
        this.e.m();
        this.g.a(this.f, new nu5.b(str, str2), new b(this));
    }

    @DexIgnore
    public void q() {
        this.e.M5(this);
    }
}
