package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ fc3 f4086a;

    @DexIgnore
    public xb3(fc3 fc3) {
        this.f4086a = fc3;
    }

    @DexIgnore
    public final boolean a() {
        try {
            return this.f4086a.A0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean b() {
        try {
            return this.f4086a.l1();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean c() {
        try {
            return this.f4086a.D0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.f4086a.U();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean e() {
        try {
            return this.f4086a.r2();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean f() {
        try {
            return this.f4086a.e0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean g() {
        try {
            return this.f4086a.J2();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean h() {
        try {
            return this.f4086a.f0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.f4086a.setCompassEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void j(boolean z) {
        try {
            this.f4086a.setMapToolbarEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void k(boolean z) {
        try {
            this.f4086a.setMyLocationButtonEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void l(boolean z) {
        try {
            this.f4086a.setRotateGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void m(boolean z) {
        try {
            this.f4086a.setScrollGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void n(boolean z) {
        try {
            this.f4086a.setTiltGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void o(boolean z) {
        try {
            this.f4086a.setZoomControlsEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void p(boolean z) {
        try {
            this.f4086a.setZoomGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
