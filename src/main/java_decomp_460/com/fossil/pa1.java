package com.fossil;

import android.content.Context;
import com.fossil.be1;
import com.fossil.hi1;
import com.fossil.je1;
import com.fossil.oa1;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Class<?>, xa1<?, ?>> f2806a; // = new zi0();
    @DexIgnore
    public xc1 b;
    @DexIgnore
    public rd1 c;
    @DexIgnore
    public od1 d;
    @DexIgnore
    public ie1 e;
    @DexIgnore
    public le1 f;
    @DexIgnore
    public le1 g;
    @DexIgnore
    public be1.a h;
    @DexIgnore
    public je1 i;
    @DexIgnore
    public zh1 j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public oa1.a l; // = new a(this);
    @DexIgnore
    public hi1.b m;
    @DexIgnore
    public le1 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<ej1<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements oa1.a {
        @DexIgnore
        public a(pa1 pa1) {
        }

        @DexIgnore
        @Override // com.fossil.oa1.a
        public fj1 build() {
            return new fj1();
        }
    }

    @DexIgnore
    public oa1 a(Context context) {
        if (this.f == null) {
            this.f = le1.g();
        }
        if (this.g == null) {
            this.g = le1.e();
        }
        if (this.n == null) {
            this.n = le1.c();
        }
        if (this.i == null) {
            this.i = new je1.a(context).a();
        }
        if (this.j == null) {
            this.j = new bi1();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new xd1((long) b2);
            } else {
                this.c = new sd1();
            }
        }
        if (this.d == null) {
            this.d = new wd1(this.i.a());
        }
        if (this.e == null) {
            this.e = new he1((long) this.i.d());
        }
        if (this.h == null) {
            this.h = new ge1(context);
        }
        if (this.b == null) {
            this.b = new xc1(this.e, this.h, this.g, this.f, le1.h(), this.n, this.o);
        }
        List<ej1<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new oa1(context, this.b, this.e, this.c, this.d, new hi1(this.m), this.j, this.k, this.l, this.f2806a, this.p, this.q, this.r);
    }

    @DexIgnore
    public void b(hi1.b bVar) {
        this.m = bVar;
    }
}
