package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oo2 extends xm2 implements mo2 {
    @DexIgnore
    public oo2() {
        super("com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public static mo2 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IStatusCallback");
        return queryLocalInterface instanceof mo2 ? (mo2) queryLocalInterface : new no2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.xm2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        m0((Status) qo2.a(parcel, Status.CREATOR));
        return true;
    }
}
