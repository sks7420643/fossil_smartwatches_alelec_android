package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s32 {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    <T> T a(a<T> aVar);
}
