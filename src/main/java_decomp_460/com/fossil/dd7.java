package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.fossil.ld7;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f769a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Downloader d;
    @DexIgnore
    public /* final */ Map<String, xc7> e; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ Map<Object, vc7> f; // = new WeakHashMap();
    @DexIgnore
    public /* final */ Map<Object, vc7> g; // = new WeakHashMap();
    @DexIgnore
    public /* final */ Set<Object> h; // = new HashSet();
    @DexIgnore
    public /* final */ Handler i; // = new a(this.f769a.getLooper(), this);
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ yc7 k;
    @DexIgnore
    public /* final */ td7 l;
    @DexIgnore
    public /* final */ List<xc7> m;
    @DexIgnore
    public /* final */ c n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dd7 f770a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dd7$a$a")
        /* renamed from: com.fossil.dd7$a$a  reason: collision with other inner class name */
        public class RunnableC0050a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message b;

            @DexIgnore
            public RunnableC0050a(a aVar, Message message) {
                this.b = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.b.what);
            }
        }

        @DexIgnore
        public a(Looper looper, dd7 dd7) {
            super(looper);
            this.f770a = dd7;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            boolean z = true;
            switch (message.what) {
                case 1:
                    this.f770a.v((vc7) message.obj);
                    return;
                case 2:
                    this.f770a.o((vc7) message.obj);
                    return;
                case 3:
                case 8:
                default:
                    Picasso.p.post(new RunnableC0050a(this, message));
                    return;
                case 4:
                    this.f770a.p((xc7) message.obj);
                    return;
                case 5:
                    this.f770a.u((xc7) message.obj);
                    return;
                case 6:
                    this.f770a.q((xc7) message.obj, false);
                    return;
                case 7:
                    this.f770a.n();
                    return;
                case 9:
                    this.f770a.r((NetworkInfo) message.obj);
                    return;
                case 10:
                    dd7 dd7 = this.f770a;
                    if (message.arg1 != 1) {
                        z = false;
                    }
                    dd7.m(z);
                    return;
                case 11:
                    this.f770a.s(message.obj);
                    return;
                case 12:
                    this.f770a.t(message.obj);
                    return;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends HandlerThread {
        @DexIgnore
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dd7 f771a;

        @DexIgnore
        public c(dd7 dd7) {
            this.f771a = dd7;
        }

        @DexIgnore
        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.f771a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.f771a.b.registerReceiver(this, intentFilter);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra("state")) {
                        this.f771a.b(intent.getBooleanExtra("state", false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.f771a.f(((ConnectivityManager) xd7.p(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    @DexIgnore
    public dd7(Context context, ExecutorService executorService, Handler handler, Downloader downloader, yc7 yc7, td7 td7) {
        b bVar = new b();
        this.f769a = bVar;
        bVar.start();
        xd7.j(this.f769a.getLooper());
        this.b = context;
        this.c = executorService;
        this.d = downloader;
        this.j = handler;
        this.k = yc7;
        this.l = td7;
        this.m = new ArrayList(4);
        this.p = xd7.r(this.b);
        this.o = xd7.q(context, "android.permission.ACCESS_NETWORK_STATE");
        c cVar = new c(this);
        this.n = cVar;
        cVar.a();
    }

    @DexIgnore
    public final void a(xc7 xc7) {
        if (!xc7.s()) {
            this.m.add(xc7);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    @DexIgnore
    public void b(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    @DexIgnore
    public void c(vc7 vc7) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, vc7));
    }

    @DexIgnore
    public void d(xc7 xc7) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, xc7));
    }

    @DexIgnore
    public void e(xc7 xc7) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, xc7));
    }

    @DexIgnore
    public void f(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    @DexIgnore
    public void g(xc7 xc7) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, xc7), 500);
    }

    @DexIgnore
    public void h(vc7 vc7) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, vc7));
    }

    @DexIgnore
    public final void i() {
        if (!this.f.isEmpty()) {
            Iterator<vc7> it = this.f.values().iterator();
            while (it.hasNext()) {
                vc7 next = it.next();
                it.remove();
                if (next.g().n) {
                    xd7.u("Dispatcher", "replaying", next.i().d());
                }
                w(next, false);
            }
        }
    }

    @DexIgnore
    public final void j(List<xc7> list) {
        if (!(list == null || list.isEmpty() || !list.get(0).o().n)) {
            StringBuilder sb = new StringBuilder();
            for (xc7 xc7 : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(xd7.l(xc7));
            }
            xd7.u("Dispatcher", "delivered", sb.toString());
        }
    }

    @DexIgnore
    public final void k(vc7 vc7) {
        Object k2 = vc7.k();
        if (k2 != null) {
            vc7.k = true;
            this.f.put(k2, vc7);
        }
    }

    @DexIgnore
    public final void l(xc7 xc7) {
        vc7 h2 = xc7.h();
        if (h2 != null) {
            k(h2);
        }
        List<vc7> i2 = xc7.i();
        if (i2 != null) {
            int size = i2.size();
            for (int i3 = 0; i3 < size; i3++) {
                k(i2.get(i3));
            }
        }
    }

    @DexIgnore
    public void m(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void n() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        j(arrayList);
    }

    @DexIgnore
    public void o(vc7 vc7) {
        String d2 = vc7.d();
        xc7 xc7 = this.e.get(d2);
        if (xc7 != null) {
            xc7.f(vc7);
            if (xc7.c()) {
                this.e.remove(d2);
                if (vc7.g().n) {
                    xd7.u("Dispatcher", "canceled", vc7.i().d());
                }
            }
        }
        if (this.h.contains(vc7.j())) {
            this.g.remove(vc7.k());
            if (vc7.g().n) {
                xd7.v("Dispatcher", "canceled", vc7.i().d(), "because paused request got canceled");
            }
        }
        vc7 remove = this.f.remove(vc7.k());
        if (remove != null && remove.g().n) {
            xd7.v("Dispatcher", "canceled", remove.i().d(), "from replaying");
        }
    }

    @DexIgnore
    public void p(xc7 xc7) {
        if (jd7.shouldWriteToMemoryCache(xc7.n())) {
            this.k.c(xc7.l(), xc7.q());
        }
        this.e.remove(xc7.l());
        a(xc7);
        if (xc7.o().n) {
            xd7.v("Dispatcher", "batched", xd7.l(xc7), "for completion");
        }
    }

    @DexIgnore
    public void q(xc7 xc7, boolean z) {
        if (xc7.o().n) {
            String l2 = xd7.l(xc7);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            xd7.v("Dispatcher", "batched", l2, sb.toString());
        }
        this.e.remove(xc7.l());
        a(xc7);
    }

    @DexIgnore
    public void r(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof od7) {
            ((od7) executorService).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            i();
        }
    }

    @DexIgnore
    public void s(Object obj) {
        if (this.h.add(obj)) {
            Iterator<xc7> it = this.e.values().iterator();
            while (it.hasNext()) {
                xc7 next = it.next();
                boolean z = next.o().n;
                vc7 h2 = next.h();
                List<vc7> i2 = next.i();
                boolean z2 = i2 != null && !i2.isEmpty();
                if (h2 != null || z2) {
                    if (h2 != null && h2.j().equals(obj)) {
                        next.f(h2);
                        this.g.put(h2.k(), h2);
                        if (z) {
                            xd7.v("Dispatcher", "paused", h2.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = i2.size() - 1; size >= 0; size--) {
                            vc7 vc7 = i2.get(size);
                            if (vc7.j().equals(obj)) {
                                next.f(vc7);
                                this.g.put(vc7.k(), vc7);
                                if (z) {
                                    xd7.v("Dispatcher", "paused", vc7.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.c()) {
                        it.remove();
                        if (z) {
                            xd7.v("Dispatcher", "canceled", xd7.l(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public void t(Object obj) {
        if (this.h.remove(obj)) {
            Iterator<vc7> it = this.g.values().iterator();
            ArrayList arrayList = null;
            while (it.hasNext()) {
                vc7 next = it.next();
                if (next.j().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @DexIgnore
    public void u(xc7 xc7) {
        boolean z = false;
        if (!xc7.s()) {
            if (this.c.isShutdown()) {
                q(xc7, false);
                return;
            }
            NetworkInfo activeNetworkInfo = this.o ? ((ConnectivityManager) xd7.p(this.b, "connectivity")).getActiveNetworkInfo() : null;
            boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            boolean u = xc7.u(this.p, activeNetworkInfo);
            boolean v = xc7.v();
            if (!u) {
                if (this.o && v) {
                    z = true;
                }
                q(xc7, z);
                if (z) {
                    l(xc7);
                }
            } else if (!this.o || z2) {
                if (xc7.o().n) {
                    xd7.u("Dispatcher", "retrying", xd7.l(xc7));
                }
                if (xc7.k() instanceof ld7.a) {
                    xc7.j |= kd7.NO_CACHE.index;
                }
                xc7.t = this.c.submit(xc7);
            } else {
                q(xc7, v);
                if (v) {
                    l(xc7);
                }
            }
        }
    }

    @DexIgnore
    public void v(vc7 vc7) {
        w(vc7, true);
    }

    @DexIgnore
    public void w(vc7 vc7, boolean z) {
        if (this.h.contains(vc7.j())) {
            this.g.put(vc7.k(), vc7);
            if (vc7.g().n) {
                String d2 = vc7.b.d();
                xd7.v("Dispatcher", "paused", d2, "because tag '" + vc7.j() + "' is paused");
                return;
            }
            return;
        }
        xc7 xc7 = this.e.get(vc7.d());
        if (xc7 != null) {
            xc7.b(vc7);
        } else if (!this.c.isShutdown()) {
            xc7 g2 = xc7.g(vc7.g(), this, this.k, this.l, vc7);
            g2.t = this.c.submit(g2);
            this.e.put(vc7.d(), g2);
            if (z) {
                this.f.remove(vc7.k());
            }
            if (vc7.g().n) {
                xd7.u("Dispatcher", "enqueued", vc7.b.d());
            }
        } else if (vc7.g().n) {
            xd7.v("Dispatcher", "ignored", vc7.b.d(), "because shut down");
        }
    }
}
