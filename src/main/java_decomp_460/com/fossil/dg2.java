package com.fossil;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dg2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static volatile ne2 f783a;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Context c;

    @DexIgnore
    public static mg2 a(String str, eg2 eg2, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return d(str, eg2, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public static final /* synthetic */ String b(boolean z, String str, eg2 eg2) throws Exception {
        boolean z2 = true;
        if (z || !d(str, eg2, true, false).f2377a) {
            z2 = false;
        }
        return mg2.e(str, eg2, z, z2);
    }

    @DexIgnore
    public static void c(Context context) {
        synchronized (dg2.class) {
            try {
                if (c != null) {
                    Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
                } else if (context != null) {
                    c = context.getApplicationContext();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static mg2 d(String str, eg2 eg2, boolean z, boolean z2) {
        try {
            if (f783a == null) {
                rc2.k(c);
                synchronized (b) {
                    if (f783a == null) {
                        f783a = pe2.e(DynamiteModule.e(c, DynamiteModule.k, "com.google.android.gms.googlecertificates").d("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            rc2.k(c);
            try {
                return f783a.y2(new kg2(str, eg2, z, z2), tg2.n(c.getPackageManager())) ? mg2.f() : mg2.c(new fg2(z, str, eg2));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return mg2.b("module call", e);
            }
        } catch (DynamiteModule.a e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return mg2.b(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }
}
