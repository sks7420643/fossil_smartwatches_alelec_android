package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class dx7 {
    @DexIgnore
    public static final uu7 a(xw7 xw7) {
        return new ax7(xw7);
    }

    @DexIgnore
    public static /* synthetic */ uu7 b(xw7 xw7, int i, Object obj) {
        if ((i & 1) != 0) {
            xw7 = null;
        }
        return bx7.a(xw7);
    }

    @DexIgnore
    public static final void c(tn7 tn7, CancellationException cancellationException) {
        xw7 xw7 = (xw7) tn7.get(xw7.r);
        if (xw7 != null) {
            xw7.D(cancellationException);
        }
    }

    @DexIgnore
    public static /* synthetic */ void d(tn7 tn7, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        bx7.c(tn7, cancellationException);
    }

    @DexIgnore
    public static final dw7 e(xw7 xw7, dw7 dw7) {
        return xw7.A(new fw7(xw7, dw7));
    }

    @DexIgnore
    public static final void f(tn7 tn7) {
        xw7 xw7 = (xw7) tn7.get(xw7.r);
        if (xw7 != null) {
            bx7.h(xw7);
            return;
        }
        throw new IllegalStateException(("Context cannot be checked for liveness because it does not have a job: " + tn7).toString());
    }

    @DexIgnore
    public static final void g(xw7 xw7) {
        if (!xw7.isActive()) {
            throw xw7.k();
        }
    }
}
