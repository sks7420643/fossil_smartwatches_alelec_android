package com.fossil;

import android.app.Activity;
import android.app.SharedElementCallback;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.View;
import androidx.core.app.SharedElementCallback;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rk0 extends gl0 {
    @DexIgnore
    public static c c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ String[] b;
        @DexIgnore
        public /* final */ /* synthetic */ Activity c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public a(String[] strArr, Activity activity, int i) {
            this.b = strArr;
            this.c = activity;
            this.d = i;
        }

        @DexIgnore
        public void run() {
            int[] iArr = new int[this.b.length];
            PackageManager packageManager = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            int length = this.b.length;
            for (int i = 0; i < length; i++) {
                iArr[i] = packageManager.checkPermission(this.b[i], packageName);
            }
            ((b) this.c).onRequestPermissionsResult(this.d, this.b, iArr);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        boolean a(Activity activity, int i, int i2, Intent intent);

        @DexIgnore
        boolean b(Activity activity, String[] strArr, int i);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void validateRequestPermissionsRequestCode(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends SharedElementCallback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ androidx.core.app.SharedElementCallback f3119a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements SharedElementCallback.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ SharedElementCallback.OnSharedElementsReadyListener f3120a;

            @DexIgnore
            public a(e eVar, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
                this.f3120a = onSharedElementsReadyListener;
            }

            @DexIgnore
            @Override // androidx.core.app.SharedElementCallback.a
            public void a() {
                this.f3120a.onSharedElementsReady();
            }
        }

        @DexIgnore
        public e(androidx.core.app.SharedElementCallback sharedElementCallback) {
            this.f3119a = sharedElementCallback;
        }

        @DexIgnore
        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.f3119a.b(view, matrix, rectF);
        }

        @DexIgnore
        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.f3119a.c(context, parcelable);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.f3119a.d(list, map);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onRejectSharedElements(List<View> list) {
            this.f3119a.e(list);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.f3119a.f(list, list2, list3);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.f3119a.g(list, list2, list3);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementsArrived(List<String> list, List<View> list2, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.f3119a.h(list, list2, new a(this, onSharedElementsReadyListener));
        }
    }

    @DexIgnore
    public static void A(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    @DexIgnore
    public static void p(Activity activity) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static void q(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static c r() {
        return c;
    }

    @DexIgnore
    public static void s(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    @DexIgnore
    public static void t(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
        } else if (!tk0.i(activity)) {
            activity.recreate();
        }
    }

    @DexIgnore
    public static void u(Activity activity, String[] strArr, int i) {
        c cVar = c;
        if (cVar != null && cVar.b(activity, strArr, i)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof d) {
                ((d) activity).validateRequestPermissionsRequestCode(i);
            }
            activity.requestPermissions(strArr, i);
        } else if (activity instanceof b) {
            new Handler(Looper.getMainLooper()).post(new a(strArr, activity, i));
        }
    }

    @DexIgnore
    public static void v(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setEnterSharedElementCallback(sharedElementCallback != null ? new e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static void w(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setExitSharedElementCallback(sharedElementCallback != null ? new e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static boolean x(Activity activity, String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return activity.shouldShowRequestPermissionRationale(str);
        }
        return false;
    }

    @DexIgnore
    public static void y(Activity activity, Intent intent, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    public static void z(Activity activity, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws IntentSender.SendIntentException {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }
}
