package com.fossil;

import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm2 extends OutputStream {
    @DexIgnore
    public final String toString() {
        return "ByteStreams.nullOutputStream()";
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public final void write(int i) {
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public final void write(byte[] bArr) {
        km2.a(bArr);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public final void write(byte[] bArr, int i, int i2) {
        km2.a(bArr);
    }
}
