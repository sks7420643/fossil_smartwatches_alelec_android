package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yn1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yn1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                byte readByte = parcel.readByte();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        pq7.b(readString3, "parcel.readString()!!");
                        String readString4 = parcel.readString();
                        if (readString4 != null) {
                            pq7.b(readString4, "parcel.readString()!!");
                            return new yn1(readString, readByte, readString2, readString3, readString4);
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yn1[] newArray(int i) {
            return new yn1[i];
        }
    }

    @DexIgnore
    public yn1(String str, byte b2, String str2, String str3, String str4) {
        this.b = str;
        this.c = (byte) b2;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    @DexIgnore
    public final byte[] a() {
        ByteBuffer allocate = ByteBuffer.allocate(6);
        pq7.b(allocate, "ByteBuffer.allocate(HEADER_LENGTH.toInt())");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        String str = this.b;
        Charset c2 = hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            byte min = (byte) Math.min(bytes.length + 1, 50);
            String str2 = this.d;
            Charset c3 = hd0.y.c();
            if (str2 != null) {
                byte[] bytes2 = str2.getBytes(c3);
                pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte min2 = (byte) Math.min(bytes2.length + 1, 50);
                String str3 = this.e;
                Charset c4 = hd0.y.c();
                if (str3 != null) {
                    byte[] bytes3 = str3.getBytes(c4);
                    pq7.b(bytes3, "(this as java.lang.String).getBytes(charset)");
                    byte min3 = (byte) Math.min(bytes3.length + 1, 50);
                    String str4 = this.f;
                    Charset c5 = hd0.y.c();
                    if (str4 != null) {
                        byte[] bytes4 = str4.getBytes(c5);
                        pq7.b(bytes4, "(this as java.lang.String).getBytes(charset)");
                        byte min4 = (byte) Math.min(bytes4.length + 1, 50);
                        short s = (short) (min + 7 + min2 + min3 + min4);
                        allocate.putShort(s);
                        allocate.put(min);
                        allocate.put(min2);
                        allocate.put(min3);
                        allocate.put(min4);
                        ByteBuffer allocate2 = ByteBuffer.allocate(s);
                        pq7.b(allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                        allocate2.order(ByteOrder.LITTLE_ENDIAN);
                        allocate2.put(allocate.array());
                        ByteBuffer allocate3 = ByteBuffer.allocate(s - 6);
                        pq7.b(allocate3, "ByteBuffer.allocate(totalLen - HEADER_LENGTH)");
                        allocate3.order(ByteOrder.LITTLE_ENDIAN);
                        allocate3.put(this.c);
                        String str5 = this.b;
                        Charset c6 = hd0.y.c();
                        if (str5 != null) {
                            byte[] bytes5 = str5.getBytes(c6);
                            pq7.b(bytes5, "(this as java.lang.String).getBytes(charset)");
                            allocate3.put(Arrays.copyOfRange(bytes5, 0, min - 1)).put((byte) 0);
                            String str6 = this.d;
                            Charset c7 = hd0.y.c();
                            if (str6 != null) {
                                byte[] bytes6 = str6.getBytes(c7);
                                pq7.b(bytes6, "(this as java.lang.String).getBytes(charset)");
                                allocate3.put(Arrays.copyOfRange(bytes6, 0, min2 - 1)).put((byte) 0);
                                String str7 = this.e;
                                Charset c8 = hd0.y.c();
                                if (str7 != null) {
                                    byte[] bytes7 = str7.getBytes(c8);
                                    pq7.b(bytes7, "(this as java.lang.String).getBytes(charset)");
                                    allocate3.put(Arrays.copyOfRange(bytes7, 0, min3 - 1)).put((byte) 0);
                                    String str8 = this.f;
                                    Charset c9 = hd0.y.c();
                                    if (str8 != null) {
                                        byte[] bytes8 = str8.getBytes(c9);
                                        pq7.b(bytes8, "(this as java.lang.String).getBytes(charset)");
                                        allocate3.put(Arrays.copyOfRange(bytes8, 0, min4 - 1)).put((byte) 0);
                                        allocate2.put(allocate3.array());
                                        byte[] array = allocate2.array();
                                        pq7.b(array, "trackInfoData.array()");
                                        return array;
                                    }
                                    throw new il7("null cannot be cast to non-null type java.lang.String");
                                }
                                throw new il7("null cannot be cast to non-null type java.lang.String");
                            }
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(yn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            yn1 yn1 = (yn1) obj;
            if (!pq7.a(this.b, yn1.b)) {
                return false;
            }
            if (this.c != yn1.c) {
                return false;
            }
            if (!pq7.a(this.d, yn1.d)) {
                return false;
            }
            if (!pq7.a(this.e, yn1.e)) {
                return false;
            }
            return !(pq7.a(this.f, yn1.f) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.TrackInfo");
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.f;
    }

    @DexIgnore
    public final String getAppName() {
        return this.b;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.e;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.d;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        byte b2 = this.c;
        int hashCode2 = this.d.hashCode();
        return (((((((hashCode * 31) + b2) * 31) + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.g, this.b), jd0.w, Byte.valueOf(this.c)), jd0.h, this.d), jd0.x, this.e), jd0.y, this.f);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
    }
}
