package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lb6 extends rv5<kb6> {
    @DexIgnore
    void C5(String str);

    @DexIgnore
    void J2(List<m66> list);

    @DexIgnore
    Object N3();  // void declaration

    @DexIgnore
    void O(int i);

    @DexIgnore
    void c0(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j2(boolean z);

    @DexIgnore
    void m0(int i);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    Object u();  // void declaration

    @DexIgnore
    Object w();  // void declaration

    @DexIgnore
    Object y();  // void declaration
}
