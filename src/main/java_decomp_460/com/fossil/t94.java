package com.fossil;

import android.os.Looper;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t94 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ FilenameFilter f3380a; // = new a();
    @DexIgnore
    public static /* final */ ExecutorService b; // = f94.c("awaitEvenIfOnMainThread task continuation executor");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ft3<T, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ot3 f3381a;

        @DexIgnore
        public b(ot3 ot3) {
            this.f3381a = ot3;
        }

        @DexIgnore
        /* renamed from: a */
        public Void then(nt3<T> nt3) throws Exception {
            if (nt3.q()) {
                this.f3381a.e(nt3.m());
                return null;
            }
            this.f3381a.d(nt3.l());
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Callable b;
        @DexIgnore
        public /* final */ /* synthetic */ ot3 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ft3<T, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            /* renamed from: a */
            public Void then(nt3<T> nt3) throws Exception {
                if (nt3.q()) {
                    c.this.c.c(nt3.m());
                    return null;
                }
                c.this.c.b(nt3.l());
                return null;
            }
        }

        @DexIgnore
        public c(Callable callable, ot3 ot3) {
            this.b = callable;
            this.c = ot3;
        }

        @DexIgnore
        public void run() {
            try {
                ((nt3) this.b.call()).h(new a());
            } catch (Exception e) {
                this.c.b(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ft3<T, Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CountDownLatch f3383a;

        @DexIgnore
        public d(CountDownLatch countDownLatch) {
            this.f3383a = countDownLatch;
        }

        @DexIgnore
        @Override // com.fossil.ft3
        public Object then(nt3<T> nt3) throws Exception {
            this.f3383a.countDown();
            return null;
        }
    }

    @DexIgnore
    public static <T> T a(nt3<T> nt3) throws InterruptedException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        nt3.i(b, new d(countDownLatch));
        if (Looper.getMainLooper() == Looper.myLooper()) {
            countDownLatch.await(4, TimeUnit.SECONDS);
        } else {
            countDownLatch.await();
        }
        if (nt3.p()) {
            return nt3.m();
        }
        throw new TimeoutException();
    }

    @DexIgnore
    public static <T> nt3<T> b(Executor executor, Callable<nt3<T>> callable) {
        ot3 ot3 = new ot3();
        executor.execute(new c(callable, ot3));
        return ot3.a();
    }

    @DexIgnore
    public static int c(File file, int i, Comparator<File> comparator) {
        return d(file, f3380a, i, comparator);
    }

    @DexIgnore
    public static int d(File file, FilenameFilter filenameFilter, int i, Comparator<File> comparator) {
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles == null) {
            return 0;
        }
        return e(Arrays.asList(listFiles), i, comparator);
    }

    @DexIgnore
    public static int e(List<File> list, int i, Comparator<File> comparator) {
        int size = list.size();
        Collections.sort(list, comparator);
        for (File file : list) {
            if (size <= i) {
                break;
            }
            h(file);
            size--;
        }
        return size;
    }

    @DexIgnore
    public static int f(File file, File file2, int i, Comparator<File> comparator) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        File[] listFiles2 = file2.listFiles(f3380a);
        if (listFiles == null) {
            listFiles = new File[0];
        }
        if (listFiles2 == null) {
            listFiles2 = new File[0];
        }
        arrayList.addAll(Arrays.asList(listFiles));
        arrayList.addAll(Arrays.asList(listFiles2));
        return e(arrayList, i, comparator);
    }

    @DexIgnore
    public static <T> nt3<T> g(nt3<T> nt3, nt3<T> nt32) {
        ot3 ot3 = new ot3();
        b bVar = new b(ot3);
        nt3.h(bVar);
        nt32.h(bVar);
        return ot3.a();
    }

    @DexIgnore
    public static void h(File file) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                h(file2);
            }
        }
        file.delete();
    }
}
