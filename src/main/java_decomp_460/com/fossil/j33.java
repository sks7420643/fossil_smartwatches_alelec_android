package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j33 extends g33<FieldDescriptorType, Object> {
    @DexIgnore
    public j33(int i) {
        super(i, null);
    }

    @DexIgnore
    @Override // com.fossil.g33
    public final void f() {
        if (!j()) {
            for (int i = 0; i < k(); i++) {
                Map.Entry i2 = i(i);
                if (((v03) i2.getKey()).zzd()) {
                    i2.setValue(Collections.unmodifiableList((List) i2.getValue()));
                }
            }
            for (Map.Entry entry : n()) {
                if (((v03) entry.getKey()).zzd()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.f();
    }
}
