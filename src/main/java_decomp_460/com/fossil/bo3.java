package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ un3 c;

    @DexIgnore
    public bo3(un3 un3, long j) {
        this.c = un3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        un3 un3 = this.c;
        long j = this.b;
        un3.h();
        un3.f();
        un3.x();
        un3.d().M().a("Resetting analytics data (FE)");
        jq3 u = un3.u();
        u.h();
        u.e.a();
        boolean o = un3.f1780a.o();
        xl3 l = un3.l();
        l.j.b(j);
        if (!TextUtils.isEmpty(l.l().z.a())) {
            l.z.b(null);
        }
        if (w63.a() && l.m().s(xg3.w0)) {
            l.u.b(0);
        }
        if (!l.m().G()) {
            l.z(!o);
        }
        l.A.b(null);
        l.B.b(0);
        l.C.b(null);
        un3.r().X();
        if (w63.a() && un3.m().s(xg3.w0)) {
            un3.u().d.a();
        }
        un3.i = !o;
        this.c.r().S(new AtomicReference<>());
    }
}
