package com.fossil;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a62 extends DialogFragment {
    @DexIgnore
    public Dialog b;
    @DexIgnore
    public DialogInterface.OnCancelListener c;

    @DexIgnore
    public static a62 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        a62 a62 = new a62();
        rc2.l(dialog, "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        a62.b = dialog2;
        if (onCancelListener != null) {
            a62.c = onCancelListener;
        }
        return a62;
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.c;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.b == null) {
            setShowsDialog(false);
        }
        return this.b;
    }

    @DexIgnore
    @Override // android.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
