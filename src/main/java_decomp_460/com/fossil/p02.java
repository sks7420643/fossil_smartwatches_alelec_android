package com.fossil;

import com.fossil.u02;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p02 extends u02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Iterable<c02> f2760a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends u02.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Iterable<c02> f2761a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.u02.a
        public u02 a() {
            String str = "";
            if (this.f2761a == null) {
                str = " events";
            }
            if (str.isEmpty()) {
                return new p02(this.f2761a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.u02.a
        public u02.a b(Iterable<c02> iterable) {
            if (iterable != null) {
                this.f2761a = iterable;
                return this;
            }
            throw new NullPointerException("Null events");
        }

        @DexIgnore
        @Override // com.fossil.u02.a
        public u02.a c(byte[] bArr) {
            this.b = bArr;
            return this;
        }
    }

    @DexIgnore
    public p02(Iterable<c02> iterable, byte[] bArr) {
        this.f2760a = iterable;
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.u02
    public Iterable<c02> b() {
        return this.f2760a;
    }

    @DexIgnore
    @Override // com.fossil.u02
    public byte[] c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof u02)) {
            return false;
        }
        u02 u02 = (u02) obj;
        if (this.f2760a.equals(u02.b())) {
            if (Arrays.equals(this.b, u02 instanceof p02 ? ((p02) u02).b : u02.c())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f2760a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "BackendRequest{events=" + this.f2760a + ", extras=" + Arrays.toString(this.b) + "}";
    }
}
