package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu4 implements Factory<hu4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<fu4> f1671a;
    @DexIgnore
    public /* final */ Provider<gu4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public iu4(Provider<fu4> provider, Provider<gu4> provider2, Provider<on5> provider3) {
        this.f1671a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static iu4 a(Provider<fu4> provider, Provider<gu4> provider2, Provider<on5> provider3) {
        return new iu4(provider, provider2, provider3);
    }

    @DexIgnore
    public static hu4 c(fu4 fu4, gu4 gu4, on5 on5) {
        return new hu4(fu4, gu4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public hu4 get() {
        return c(this.f1671a.get(), this.b.get(), this.c.get());
    }
}
