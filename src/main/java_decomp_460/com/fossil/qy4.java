package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ qy4 f3046a; // = new qy4();

    @DexIgnore
    public final List<vs4> a() {
        return hm7.i(new vs4(PortfolioApp.h0.c().getString(2131886272), PortfolioApp.h0.c().getString(2131886270), 2131231014, "activity_best_result", 0, 86400, "public_with_friend"), new vs4(PortfolioApp.h0.c().getString(2131886284), PortfolioApp.h0.c().getString(2131886283), 2131231017, "activity_reach_goal", 15000, 259200, "public_with_friend"));
    }

    @DexIgnore
    public final List<Date> b() {
        FLogger.INSTANCE.getLocal().e("BCGenerator", "laterTime - phoneDate: " + new Date() + " - exactDate: " + xy4.f4212a.a());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(xy4.f4212a.a());
        Date time = instance.getTime();
        pq7.b(time, "currentDate");
        arrayList.add(time);
        for (int i = 1; i <= 6; i++) {
            instance.add(6, 1);
            Date time2 = instance.getTime();
            pq7.b(time2, "calendar.time");
            arrayList.add(time2);
        }
        return arrayList;
    }

    @DexIgnore
    public final List<gs4> c() {
        String c = um5.c(PortfolioApp.h0.c(), 2131886319);
        pq7.b(c, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        gs4 gs4 = new gs4(c, vy4.ABOUT, false, 4, null);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886321);
        pq7.b(c2, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        return hm7.i(gs4, new gs4(c2, vy4.LEADER_BOARD, false, 4, null));
    }

    @DexIgnore
    public final List<gs4> d() {
        String c = um5.c(PortfolioApp.h0.c(), 2131886319);
        pq7.b(c, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        gs4 gs4 = new gs4(c, vy4.ABOUT, false, 4, null);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886321);
        pq7.b(c2, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        gs4 gs42 = new gs4(c2, vy4.LEADER_BOARD, false, 4, null);
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886320);
        pq7.b(c3, "LanguageHelper.getString\u2026enu_List__LeaveChallenge)");
        return hm7.i(gs4, gs42, new gs4(c3, vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<gs4> e() {
        String c = um5.c(PortfolioApp.h0.c(), 2131886200);
        pq7.b(c, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return hm7.i(new gs4(c, vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<gs4> f() {
        String c = um5.c(PortfolioApp.h0.c(), 2131886199);
        pq7.b(c, "LanguageHelper.getString\u2026ailable_Allow_List__Edit)");
        gs4 gs4 = new gs4(c, vy4.EDIT, false, 4, null);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886198);
        pq7.b(c2, "LanguageHelper.getString\u2026e_Allow_List__AddFriends)");
        gs4 gs42 = new gs4(c2, vy4.ADD_FRIENDS, false, 4, null);
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886200);
        pq7.b(c3, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return hm7.i(gs4, gs42, new gs4(c3, vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<gs4> g() {
        String c = um5.c(PortfolioApp.h0.c(), 2131886273);
        pq7.b(c, "LanguageHelper.getString\u2026cySettings_List__Friends)");
        gs4 gs4 = new gs4(c, "public_with_friend", true);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886274);
        pq7.b(c2, "LanguageHelper.getString\u2026cySettings_List__Private)");
        return hm7.i(gs4, new gs4(c2, "private", false));
    }

    @DexIgnore
    public final List<gs4> h() {
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("BCGenerator", "soonTime - phoneDate: " + new Date() + " - exactDate: " + xy4.f4212a.a());
        Calendar instance = Calendar.getInstance();
        Date a2 = xy4.f4212a.a();
        pq7.b(instance, "calendar");
        instance.setTime(a2);
        instance.set(12, (((instance.get(12) + 5) / 15) + 1) * 15);
        Date time = instance.getTime();
        pq7.b(time, "calendar.time");
        arrayList.add(time);
        instance.add(12, 15);
        Date time2 = instance.getTime();
        pq7.b(time2, "calendar.time");
        arrayList.add(time2);
        instance.add(12, 15);
        Date time3 = instance.getTime();
        pq7.b(time3, "calendar.time");
        arrayList.add(time3);
        return py4.e(arrayList, a2);
    }
}
