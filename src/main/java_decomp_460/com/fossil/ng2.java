package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ng2 implements Parcelable.Creator<kg2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ kg2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        IBinder iBinder = null;
        String str = null;
        boolean z2 = false;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str = ad2.f(parcel, t);
            } else if (l == 2) {
                iBinder = ad2.u(parcel, t);
            } else if (l == 3) {
                z2 = ad2.m(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                z = ad2.m(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new kg2(str, iBinder, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ kg2[] newArray(int i) {
        return new kg2[i];
    }
}
