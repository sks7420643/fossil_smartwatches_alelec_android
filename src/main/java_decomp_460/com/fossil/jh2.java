package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jh2 extends rl2 implements kh2 {
    @DexIgnore
    public jh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final int L1(rg2 rg2, String str, boolean z) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        sl2.a(d, z);
        Parcel e = e(3, d);
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final rg2 V1(rg2 rg2, String str, int i) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Parcel e = e(4, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final int f2() throws RemoteException {
        Parcel e = e(6, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final int m1(rg2 rg2, String str, boolean z) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        sl2.a(d, z);
        Parcel e = e(5, d);
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final rg2 w0(rg2 rg2, String str, int i) throws RemoteException {
        Parcel d = d();
        sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Parcel e = e(2, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
