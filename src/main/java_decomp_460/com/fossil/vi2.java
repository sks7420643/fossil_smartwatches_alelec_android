package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.RawDataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi2 implements Parcelable.Creator<RawDataPoint> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ RawDataPoint createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        ai2[] ai2Arr = null;
        int i = 0;
        int i2 = 0;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    j4 = ad2.y(parcel, t);
                    break;
                case 2:
                    j3 = ad2.y(parcel, t);
                    break;
                case 3:
                    ai2Arr = (ai2[]) ad2.i(parcel, t, ai2.CREATOR);
                    break;
                case 4:
                    i = ad2.v(parcel, t);
                    break;
                case 5:
                    i2 = ad2.v(parcel, t);
                    break;
                case 6:
                    j2 = ad2.y(parcel, t);
                    break;
                case 7:
                    j = ad2.y(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new RawDataPoint(j4, j3, ai2Arr, i, i2, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ RawDataPoint[] newArray(int i) {
        return new RawDataPoint[i];
    }
}
