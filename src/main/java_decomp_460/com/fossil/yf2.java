package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static Context f4310a;
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static boolean a(Context context) {
        boolean booleanValue;
        synchronized (yf2.class) {
            try {
                Context applicationContext = context.getApplicationContext();
                if (f4310a == null || b == null || f4310a != applicationContext) {
                    b = null;
                    if (mf2.j()) {
                        b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                    } else {
                        try {
                            context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                            b = Boolean.TRUE;
                        } catch (ClassNotFoundException e) {
                            b = Boolean.FALSE;
                        }
                    }
                    f4310a = applicationContext;
                    booleanValue = b.booleanValue();
                } else {
                    booleanValue = b.booleanValue();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return booleanValue;
    }
}
