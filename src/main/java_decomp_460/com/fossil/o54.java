package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o54 implements Closeable {
    @DexIgnore
    public static /* final */ c e; // = (b.c() ? b.f2634a : a.f2633a);
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Deque<Closeable> c; // = new ArrayDeque(4);
    @DexIgnore
    public Throwable d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f2633a; // = new a();

        @DexIgnore
        @Override // com.fossil.o54.c
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            Logger logger = n54.f2470a;
            Level level = Level.WARNING;
            logger.log(level, "Suppressing exception thrown when closing " + closeable, th2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f2634a; // = new b();
        @DexIgnore
        public static /* final */ Method b; // = b();

        @DexIgnore
        public static Method b() {
            try {
                return Throwable.class.getMethod("addSuppressed", Throwable.class);
            } catch (Throwable th) {
                return null;
            }
        }

        @DexIgnore
        public static boolean c() {
            return b != null;
        }

        @DexIgnore
        @Override // com.fossil.o54.c
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            if (th != th2) {
                try {
                    b.invoke(th, th2);
                } catch (Throwable th3) {
                    a.f2633a.a(closeable, th, th2);
                }
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Closeable closeable, Throwable th, Throwable th2);
    }

    @DexIgnore
    public o54(c cVar) {
        i14.l(cVar);
        this.b = cVar;
    }

    @DexIgnore
    public static o54 a() {
        return new o54(e);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <C extends Closeable> C b(C c2) {
        if (c2 != null) {
            this.c.addFirst(c2);
        }
        return c2;
    }

    @DexIgnore
    public RuntimeException c(Throwable th) throws IOException {
        i14.l(th);
        this.d = th;
        n14.g(th, IOException.class);
        throw new RuntimeException(th);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        Throwable th = this.d;
        while (!this.c.isEmpty()) {
            Closeable removeFirst = this.c.removeFirst();
            try {
                removeFirst.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.b.a(removeFirst, th, th2);
                }
            }
        }
        if (this.d == null && th != null) {
            n14.g(th, IOException.class);
            throw new AssertionError(th);
        }
    }
}
