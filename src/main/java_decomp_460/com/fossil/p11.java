package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p11 extends e11 {
    @DexIgnore
    public static /* final */ String j; // = x01.f("WorkContinuationImpl");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ s11 f2763a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ s01 c;
    @DexIgnore
    public /* final */ List<? extends h11> d;
    @DexIgnore
    public /* final */ List<String> e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<p11> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public a11 i;

    @DexIgnore
    public p11(s11 s11, String str, s01 s01, List<? extends h11> list) {
        this(s11, str, s01, list, null);
    }

    @DexIgnore
    public p11(s11 s11, String str, s01 s01, List<? extends h11> list, List<p11> list2) {
        this.f2763a = s11;
        this.b = str;
        this.c = s01;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(this.d.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (p11 p11 : list2) {
                this.f.addAll(p11.f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String b2 = ((h11) list.get(i2)).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }

    @DexIgnore
    public p11(s11 s11, List<? extends h11> list) {
        this(s11, null, s01.KEEP, list, null);
    }

    @DexIgnore
    public static boolean i(p11 p11, Set<String> set) {
        set.addAll(p11.c());
        Set<String> l = l(p11);
        for (String str : set) {
            if (l.contains(str)) {
                return true;
            }
        }
        List<p11> e2 = p11.e();
        if (e2 != null && !e2.isEmpty()) {
            for (p11 p112 : e2) {
                if (i(p112, set)) {
                    return true;
                }
            }
        }
        set.removeAll(p11.c());
        return false;
    }

    @DexIgnore
    public static Set<String> l(p11 p11) {
        HashSet hashSet = new HashSet();
        List<p11> e2 = p11.e();
        if (e2 != null && !e2.isEmpty()) {
            for (p11 p112 : e2) {
                hashSet.addAll(p112.c());
            }
        }
        return hashSet;
    }

    @DexIgnore
    public a11 a() {
        if (!this.h) {
            w31 w31 = new w31(this);
            this.f2763a.q().b(w31);
            this.i = w31.d();
        } else {
            x01.c().h(j, String.format("Already enqueued work ids (%s)", TextUtils.join(", ", this.e)), new Throwable[0]);
        }
        return this.i;
    }

    @DexIgnore
    public s01 b() {
        return this.c;
    }

    @DexIgnore
    public List<String> c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public List<p11> e() {
        return this.g;
    }

    @DexIgnore
    public List<? extends h11> f() {
        return this.d;
    }

    @DexIgnore
    public s11 g() {
        return this.f2763a;
    }

    @DexIgnore
    public boolean h() {
        return i(this, new HashSet());
    }

    @DexIgnore
    public boolean j() {
        return this.h;
    }

    @DexIgnore
    public void k() {
        this.h = true;
    }
}
