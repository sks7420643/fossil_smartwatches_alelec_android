package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q14<K, V> extends r14<K, V> implements s34<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 6588350623831699109L;

    @DexIgnore
    public q14(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    @Override // com.fossil.r14
    public abstract List<V> createCollection();

    @DexIgnore
    @Override // com.fossil.r14
    public List<V> createUnmodifiableEmptyCollection() {
        return y24.of();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.y34
    public List<V> get(K k) {
        return (List) super.get((q14<K, V>) k);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.u14, com.fossil.y34
    @CanIgnoreReturnValue
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    @Override // com.fossil.r14
    @CanIgnoreReturnValue
    public List<V> removeAll(Object obj) {
        return (List) super.removeAll(obj);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.u14
    @CanIgnoreReturnValue
    public List<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (List) super.replaceValues((q14<K, V>) k, (Iterable) iterable);
    }
}
