package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn5 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static mn5 o;
    @DexIgnore
    public static /* final */ a p; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2401a; // = "Anonymous";
    @DexIgnore
    public ContactProvider b;
    @DexIgnore
    public LocationProvider c;
    @DexIgnore
    public AppFilterProvider d;
    @DexIgnore
    public DeviceProvider e;
    @DexIgnore
    public lp5 f;
    @DexIgnore
    public MFSleepSessionProvider g;
    @DexIgnore
    public hp5 h;
    @DexIgnore
    public pp5 i;
    @DexIgnore
    public jp5 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public np5 l;
    @DexIgnore
    public rp5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final mn5 a() {
            if (mn5.o == null) {
                mn5.o = new mn5();
            }
            mn5 mn5 = mn5.o;
            if (mn5 != null) {
                return mn5;
            }
            pq7.i();
            throw null;
        }
    }

    /*
    static {
        String simpleName = mn5.class.getSimpleName();
        pq7.b(simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public mn5() {
        PortfolioApp.h0.c().M().x0(this);
    }

    @DexIgnore
    public final AppFilterProvider c() {
        AppFilterProvider appFilterProvider;
        synchronized (this) {
            String str = this.f2401a;
            if (this.d == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
                this.d = new ep5(PortfolioApp.h0.c().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider dbPath " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
                AppFilterProvider appFilterProvider2 = this.d;
                if (appFilterProvider2 != null) {
                    String dbPath = appFilterProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str3, dbPath))) {
                        this.d = new ep5(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            appFilterProvider = this.d;
            if (appFilterProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return appFilterProvider;
    }

    @DexIgnore
    public final ContactProvider d() {
        ContactProvider contactProvider;
        synchronized (this) {
            String str = this.f2401a;
            if (this.b == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider newPath=" + str2);
                this.b = new fp5(PortfolioApp.h0.c().getApplicationContext(), str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
                ContactProvider contactProvider2 = this.b;
                if (contactProvider2 != null) {
                    String dbPath = contactProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str3, dbPath))) {
                        this.b = new fp5(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            contactProvider = this.b;
            if (contactProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return contactProvider;
    }

    @DexIgnore
    public final DeviceProvider e() {
        DeviceProviderImp deviceProviderImp;
        DeviceProvider deviceProvider;
        synchronized (this) {
            String str = this.f2401a;
            FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + str);
            if (this.e == null) {
                this.e = new DeviceProviderImp(PortfolioApp.h0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME);
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
                try {
                    DeviceProvider deviceProvider2 = this.e;
                    if (deviceProvider2 != null) {
                        String dbPath = deviceProvider2.getDbPath();
                        if (TextUtils.isEmpty(dbPath) || (!pq7.a(str2, dbPath))) {
                            deviceProviderImp = new DeviceProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
                            this.e = deviceProviderImp;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                    if (TextUtils.isEmpty(null) || (!pq7.a(str2, null))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
                    }
                } catch (Throwable th) {
                    if (TextUtils.isEmpty(null) || (!pq7.a(str2, null))) {
                        this.e = new DeviceProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
                    }
                    throw th;
                }
            }
            deviceProvider = this.e;
            if (deviceProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return deviceProvider;
    }

    @DexIgnore
    public final DeviceProvider f(String str) {
        DeviceProvider deviceProvider;
        synchronized (this) {
            pq7.c(str, ButtonService.USER_ID);
            if (this.e == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
                FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
                this.e = new DeviceProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
            }
            deviceProvider = this.e;
            if (deviceProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return deviceProvider;
    }

    @DexIgnore
    public final jp5 g() {
        jp5 jp5;
        synchronized (this) {
            String str = this.f2401a;
            if (this.j == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
                this.j = new kp5(PortfolioApp.h0.c().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider dbPath=" + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
                jp5 jp52 = this.j;
                if (jp52 != null) {
                    String dbPath = jp52.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str3, dbPath))) {
                        this.j = new kp5(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            jp5 = this.j;
            if (jp5 == null) {
                pq7.i();
                throw null;
            }
        }
        return jp5;
    }

    @DexIgnore
    public final hp5 h() {
        hp5 hp5;
        synchronized (this) {
            if (this.h == null) {
                String str = this.f2401a;
                this.h = new ip5(PortfolioApp.h0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "hourNotification.db");
            }
            hp5 = this.h;
            if (hp5 == null) {
                pq7.i();
                throw null;
            }
        }
        return hp5;
    }

    @DexIgnore
    public final LocationProvider i() {
        LocationProvider locationProvider;
        synchronized (this) {
            String str = this.f2401a;
            if (this.c == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
                this.c = new LocationProviderImpl(PortfolioApp.h0.c().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider path " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
                LocationProvider locationProvider2 = this.c;
                if (locationProvider2 != null) {
                    String dbPath = locationProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str3, dbPath))) {
                        this.c = new LocationProviderImpl(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            locationProvider = this.c;
            if (locationProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return locationProvider;
    }

    @DexIgnore
    public final lp5 j() {
        lp5 lp5;
        synchronized (this) {
            String str = this.f2401a;
            if (this.f == null) {
                this.f = new mp5(PortfolioApp.h0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db");
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db";
                lp5 lp52 = this.f;
                if (lp52 != null) {
                    String dbPath = lp52.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str2, dbPath))) {
                        this.f = new mp5(PortfolioApp.h0.c().getApplicationContext(), str2);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            lp5 = this.f;
            if (lp5 == null) {
                pq7.i();
                throw null;
            }
        }
        return lp5;
    }

    @DexIgnore
    public final np5 k() {
        if (this.l == null) {
            this.l = new op5(PortfolioApp.h0.c().getApplicationContext(), "phone_favorites_contact.db");
        }
        np5 np5 = this.l;
        if (np5 != null) {
            return np5;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final pp5 l() {
        qp5 qp5;
        pp5 pp5;
        synchronized (this) {
            String str = this.f2401a;
            if (this.i == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
                this.i = new qp5(PortfolioApp.h0.c().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider dbPath=" + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
                try {
                    pp5 pp52 = this.i;
                    if (pp52 != null) {
                        String dbPath = pp52.getDbPath();
                        FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                        if (c78.a(dbPath) || (!pq7.a(str3, dbPath))) {
                            qp5 = new qp5(PortfolioApp.h0.c().getApplicationContext(), str3);
                            this.i = qp5;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                    if (c78.a(null) || (!pq7.a(str3, null))) {
                        qp5 = new qp5(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } catch (Throwable th) {
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                    if (c78.a(null) || (!pq7.a(str3, null))) {
                        this.i = new qp5(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                    throw th;
                }
            }
            pp5 = this.i;
            if (pp5 == null) {
                pq7.i();
                throw null;
            }
        }
        return pp5;
    }

    @DexIgnore
    public final SecondTimezoneProvider m() {
        SecondTimezoneProvider secondTimezoneProvider;
        synchronized (this) {
            String str = this.f2401a;
            if (this.k == null) {
                this.k = new SecondTimezoneProviderImp(PortfolioApp.h0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME);
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME;
                SecondTimezoneProvider secondTimezoneProvider2 = this.k;
                if (secondTimezoneProvider2 != null) {
                    String dbPath = secondTimezoneProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str2, dbPath))) {
                        this.k = new SecondTimezoneProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            secondTimezoneProvider = this.k;
            if (secondTimezoneProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    public final rp5 n() {
        if (this.m == null) {
            synchronized (mn5.class) {
                try {
                    if (this.m == null) {
                        String str = this.f2401a + LocaleConverter.LOCALE_DELIMITER + "serverSetting.db";
                        FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                        Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                        pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        this.m = new sp5(applicationContext, str);
                    }
                    tl7 tl7 = tl7.f3441a;
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        rp5 rp5 = this.m;
        if (rp5 != null) {
            return rp5;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final MFSleepSessionProvider o() {
        MFSleepSessionProvider mFSleepSessionProvider;
        synchronized (this) {
            String str = this.f2401a;
            if (this.g == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
                this.g = new MFSleepSessionProviderImp(PortfolioApp.h0.c().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider dbPath " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
                MFSleepSessionProvider mFSleepSessionProvider2 = this.g;
                if (mFSleepSessionProvider2 != null) {
                    String dbPath = mFSleepSessionProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!pq7.a(str3, dbPath))) {
                        this.g = new MFSleepSessionProviderImp(PortfolioApp.h0.c().getApplicationContext(), str3);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            mFSleepSessionProvider = this.g;
            if (mFSleepSessionProvider == null) {
                pq7.i();
                throw null;
            }
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(n, "reset database");
        this.b = null;
        this.c = null;
        this.d = null;
        this.g = null;
        this.i = null;
        this.e = null;
        this.h = null;
        this.f = null;
        this.m = null;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, ButtonService.USER_ID);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateUserId current " + str + " updated " + str);
        if (TextUtils.isEmpty(str)) {
            str = "Anonymous";
        }
        this.f2401a = str;
    }
}
