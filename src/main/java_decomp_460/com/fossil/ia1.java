package com.fossil;

import com.fossil.o91;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ia1<T> extends m91<T> {
    @DexIgnore
    public static /* final */ String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ String PROTOCOL_CONTENT_TYPE; // = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    @DexIgnore
    public o91.b<T> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ String mRequestBody;

    @DexIgnore
    public ia1(int i, String str, String str2, o91.b<T> bVar, o91.a aVar) {
        super(i, str, aVar);
        this.mLock = new Object();
        this.mListener = bVar;
        this.mRequestBody = str2;
    }

    @DexIgnore
    @Deprecated
    public ia1(String str, String str2, o91.b<T> bVar, o91.a aVar) {
        this(-1, str, str2, bVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.m91
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.m91
    public void deliverResponse(T t) {
        o91.b<T> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(t);
        }
    }

    @DexIgnore
    @Override // com.fossil.m91
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            u91.f("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.m91
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @Override // com.fossil.m91
    @Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @Override // com.fossil.m91
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    @Override // com.fossil.m91
    public abstract o91<T> parseNetworkResponse(j91 j91);
}
