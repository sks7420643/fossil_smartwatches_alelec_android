package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i9 extends vx1<byte[], byte[]> {
    @DexIgnore
    public static /* final */ qx1<byte[]>[] b; // = {new c9(), new f9()};
    @DexIgnore
    public static /* final */ rx1<byte[]>[] c; // = new rx1[0];
    @DexIgnore
    public static /* final */ i9 d; // = new i9();

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<byte[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(byte[] bArr) {
        return bArr;
    }
}
