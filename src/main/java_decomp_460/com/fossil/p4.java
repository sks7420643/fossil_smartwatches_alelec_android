package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p4 {
    @DexIgnore
    public /* synthetic */ p4(kq7 kq7) {
    }

    @DexIgnore
    public final r4 a(int i) {
        switch (i) {
            case 10:
                return r4.BOND_NONE;
            case 11:
                return r4.BONDING;
            case 12:
                return r4.BONDED;
            default:
                return r4.BOND_NONE;
        }
    }
}
