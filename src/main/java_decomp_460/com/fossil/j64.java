package com.fossil;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.h72;
import com.fossil.pc2;
import com.google.firebase.components.ComponentDiscoveryService;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j64 {
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static /* final */ Executor j; // = new d();
    @DexIgnore
    public static /* final */ Map<String, j64> k; // = new zi0();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1719a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ l64 c;
    @DexIgnore
    public /* final */ i74 d;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ p74<lh4> g;
    @DexIgnore
    public /* final */ List<b> h; // = new CopyOnWriteArrayList();

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(14)
    public static class c implements h72.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static AtomicReference<c> f1720a; // = new AtomicReference<>();

        @DexIgnore
        public static void c(Context context) {
            if (mf2.a() && (context.getApplicationContext() instanceof Application)) {
                Application application = (Application) context.getApplicationContext();
                if (f1720a.get() == null) {
                    c cVar = new c();
                    if (f1720a.compareAndSet(null, cVar)) {
                        h72.c(application);
                        h72.b().a(cVar);
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.h72.a
        public void a(boolean z) {
            synchronized (j64.i) {
                Iterator it = new ArrayList(j64.k.values()).iterator();
                while (it.hasNext()) {
                    j64 j64 = (j64) it.next();
                    if (j64.e.get()) {
                        j64.t(z);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Executor {
        @DexIgnore
        public static /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(24)
    public static class e extends BroadcastReceiver {
        @DexIgnore
        public static AtomicReference<e> b; // = new AtomicReference<>();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f1721a;

        @DexIgnore
        public e(Context context) {
            this.f1721a = context;
        }

        @DexIgnore
        public static void b(Context context) {
            if (b.get() == null) {
                e eVar = new e(context);
                if (b.compareAndSet(null, eVar)) {
                    context.registerReceiver(eVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        @DexIgnore
        public void c() {
            this.f1721a.unregisterReceiver(this);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            synchronized (j64.i) {
                for (j64 j64 : j64.k.values()) {
                    j64.l();
                }
            }
            c();
        }
    }

    @DexIgnore
    public j64(Context context, String str, l64 l64) {
        new CopyOnWriteArrayList();
        rc2.k(context);
        this.f1719a = context;
        rc2.g(str);
        this.b = str;
        rc2.k(l64);
        this.c = l64;
        List<e74> a2 = c74.b(context, ComponentDiscoveryService.class).a();
        String a3 = qi4.a();
        this.d = new i74(j, a2, a74.n(context, Context.class, new Class[0]), a74.n(this, j64.class, new Class[0]), a74.n(l64, l64.class, new Class[0]), si4.a("fire-android", ""), si4.a("fire-core", "19.3.0"), a3 != null ? si4.a("kotlin", a3) : null, oi4.b(), ie4.b());
        this.g = new p74<>(i64.a(this, context));
    }

    @DexIgnore
    public static j64 h() {
        j64 j64;
        synchronized (i) {
            j64 = k.get("[DEFAULT]");
            if (j64 == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + nf2.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return j64;
    }

    @DexIgnore
    public static j64 m(Context context) {
        synchronized (i) {
            if (k.containsKey("[DEFAULT]")) {
                return h();
            }
            l64 a2 = l64.a(context);
            if (a2 == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            return n(context, a2);
        }
    }

    @DexIgnore
    public static j64 n(Context context, l64 l64) {
        return o(context, l64, "[DEFAULT]");
    }

    @DexIgnore
    public static j64 o(Context context, l64 l64, String str) {
        j64 j64;
        c.c(context);
        String s = s(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (i) {
            boolean z = !k.containsKey(s);
            rc2.o(z, "FirebaseApp name " + s + " already exists!");
            rc2.l(context, "Application context cannot be null.");
            j64 = new j64(context, s, l64);
            k.put(s, j64);
        }
        j64.l();
        return j64;
    }

    @DexIgnore
    public static /* synthetic */ lh4 r(j64 j64, Context context) {
        return new lh4(context, j64.k(), (fe4) j64.d.get(fe4.class));
    }

    @DexIgnore
    public static String s(String str) {
        return str.trim();
    }

    @DexIgnore
    public final void e() {
        rc2.o(!this.f.get(), "FirebaseApp was deleted");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof j64)) {
            return false;
        }
        return this.b.equals(((j64) obj).i());
    }

    @DexIgnore
    public <T> T f(Class<T> cls) {
        e();
        return (T) this.d.get(cls);
    }

    @DexIgnore
    public Context g() {
        e();
        return this.f1719a;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String i() {
        e();
        return this.b;
    }

    @DexIgnore
    public l64 j() {
        e();
        return this.c;
    }

    @DexIgnore
    public String k() {
        return cf2.a(i().getBytes(Charset.defaultCharset())) + g78.ANY_NON_NULL_MARKER + cf2.a(j().c().getBytes(Charset.defaultCharset()));
    }

    @DexIgnore
    public final void l() {
        if (!xm0.a(this.f1719a)) {
            e.b(this.f1719a);
        } else {
            this.d.d(q());
        }
    }

    @DexIgnore
    public boolean p() {
        e();
        return this.g.get().b();
    }

    @DexIgnore
    public boolean q() {
        return "[DEFAULT]".equals(i());
    }

    @DexIgnore
    public final void t(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (b bVar : this.h) {
            bVar.a(z);
        }
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("name", this.b);
        c2.a("options", this.c);
        return c2.toString();
    }
}
