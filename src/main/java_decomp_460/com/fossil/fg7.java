package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.net.URI;
import java.util.Iterator;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg7 {
    @DexIgnore
    public static boolean A; // = true;
    @DexIgnore
    public static volatile String B; // = "pingma.qq.com:80";
    @DexIgnore
    public static volatile String C; // = "http://pingma.qq.com:80/mstat/report";
    @DexIgnore
    public static int D; // = 20;
    @DexIgnore
    public static int E; // = 0;
    @DexIgnore
    public static boolean F; // = false;
    @DexIgnore
    public static String G; // = null;
    @DexIgnore
    public static oi7 H; // = null;
    @DexIgnore
    public static boolean I; // = true;
    @DexIgnore
    public static int J; // = 0;
    @DexIgnore
    public static long K; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int L; // = 512;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static th7 f1127a; // = ei7.p();
    @DexIgnore
    public static ni7 b; // = new ni7(2);
    @DexIgnore
    public static ni7 c; // = new ni7(1);
    @DexIgnore
    public static gg7 d; // = gg7.APP_LAUNCH;
    @DexIgnore
    public static boolean e; // = false;
    @DexIgnore
    public static boolean f; // = true;
    @DexIgnore
    public static int g; // = 30000;
    @DexIgnore
    public static int h; // = 100000;
    @DexIgnore
    public static int i; // = 30;
    @DexIgnore
    public static int j; // = 10;
    @DexIgnore
    public static int k; // = 100;
    @DexIgnore
    public static int l; // = 30;
    @DexIgnore
    public static int m; // = 1;
    @DexIgnore
    public static String n; // = "__HIBERNATE__";
    @DexIgnore
    public static String o; // = "__HIBERNATE__TIME";
    @DexIgnore
    public static String p; // = "__MTA_KILL__";
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static String s; // = "mta_channel";
    @DexIgnore
    public static String t; // = "";
    @DexIgnore
    public static int u; // = 180;
    @DexIgnore
    public static boolean v; // = false;
    @DexIgnore
    public static int w; // = 100;
    @DexIgnore
    public static long x; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int y; // = 1024;
    @DexIgnore
    public static boolean z; // = true;

    @DexIgnore
    public static int A() {
        return y;
    }

    @DexIgnore
    public static int B() {
        return j;
    }

    @DexIgnore
    public static int C() {
        return h;
    }

    @DexIgnore
    public static int D() {
        return m;
    }

    @DexIgnore
    public static String E(Context context) {
        return ii7.d(context, "mta.acc.qq", t);
    }

    @DexIgnore
    public static int F() {
        return u;
    }

    @DexIgnore
    public static int G() {
        return g;
    }

    @DexIgnore
    public static String H() {
        return C;
    }

    @DexIgnore
    public static gg7 I() {
        return d;
    }

    @DexIgnore
    public static boolean J() {
        return A;
    }

    @DexIgnore
    public static boolean K() {
        return e;
    }

    @DexIgnore
    public static boolean L() {
        return F;
    }

    @DexIgnore
    public static boolean M() {
        return f;
    }

    @DexIgnore
    public static void N(Context context, String str) {
        th7 th7;
        String str2;
        if (context == null) {
            th7 = f1127a;
            str2 = "ctx in StatConfig.setAppKey() is null";
        } else if (str == null || str.length() > 256) {
            th7 = f1127a;
            str2 = "appkey in StatConfig.setAppKey() is null or exceed 256 bytes";
        } else {
            if (q == null) {
                q = b(context);
            }
            if (l(str) || l(ei7.z(context))) {
                h(context, q);
                return;
            }
            return;
        }
        th7.f(str2);
    }

    @DexIgnore
    public static void O(boolean z2) {
        z = z2;
    }

    @DexIgnore
    public static void P(boolean z2) {
        f = z2;
        if (!z2) {
            f1127a.m("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    @DexIgnore
    public static void Q(Context context, String str) {
        if (str.length() > 128) {
            f1127a.f("the length of installChannel can not exceed the range of 128 bytes.");
            return;
        }
        r = str;
        ii7.g(context, s, str);
    }

    @DexIgnore
    public static void R(String str) {
        if (str.length() > 128) {
            f1127a.f("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            r = str;
        }
    }

    @DexIgnore
    public static void S(int i2) {
        if (!k(i2, 1, DateTimeConstants.MINUTES_PER_WEEK)) {
            f1127a.f("setSendPeriodMinutes can not exceed the range of [1, 7*24*60] minutes.");
        } else {
            u = i2;
        }
    }

    @DexIgnore
    public static void T(String str) {
        if (str == null || str.length() == 0) {
            f1127a.f("statReportUrl cannot be null or empty.");
            return;
        }
        C = str;
        try {
            B = new URI(C).getHost();
        } catch (Exception e2) {
            f1127a.l(e2);
        }
        if (K()) {
            th7 th7 = f1127a;
            th7.h("url:" + C + ", domain:" + B);
        }
    }

    @DexIgnore
    public static void U(gg7 gg7) {
        d = gg7;
        if (gg7 != gg7.PERIOD) {
            ig7.s = 0;
        }
        if (K()) {
            th7 th7 = f1127a;
            th7.b("Change to statSendStrategy: " + gg7);
        }
    }

    @DexIgnore
    public static int a() {
        return i;
    }

    @DexIgnore
    public static String b(Context context) {
        return ji7.b(ii7.d(context, "_mta_ky_tag_", null));
    }

    @DexIgnore
    public static String c(String str, String str2) {
        try {
            String string = c.b.getString(str);
            return string != null ? string : str2;
        } catch (Throwable th) {
            th7 th7 = f1127a;
            th7.l("can't find custom key:" + str);
            return str2;
        }
    }

    @DexIgnore
    public static void d(int i2) {
        synchronized (fg7.class) {
        }
    }

    @DexIgnore
    public static void e(long j2) {
        ii7.f(qi7.a(), n, j2);
        P(false);
        f1127a.m("MTA is disable for current SDK version");
    }

    @DexIgnore
    public static void f(Context context, ni7 ni7) {
        int i2 = ni7.f2529a;
        if (i2 == c.f2529a) {
            c = ni7;
            j(ni7.b);
            if (!c.b.isNull("iplist")) {
                tg7.a(context).d(c.b.getString("iplist"));
            }
        } else if (i2 == b.f2529a) {
            b = ni7;
        }
    }

    @DexIgnore
    public static void g(Context context, ni7 ni7, JSONObject jSONObject) {
        boolean z2 = false;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    int i2 = jSONObject.getInt(next);
                    boolean z3 = ni7.d != i2 ? true : z2;
                    ni7.d = i2;
                    z2 = z3;
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        ni7.b = new JSONObject(string);
                    }
                } else if (next.equalsIgnoreCase("m")) {
                    ni7.c = jSONObject.getString("m");
                }
            }
            if (z2) {
                gh7 b2 = gh7.b(qi7.a());
                if (b2 != null) {
                    b2.n(ni7);
                }
                if (ni7.f2529a == c.f2529a) {
                    j(ni7.b);
                    q(ni7.b);
                }
            }
            f(context, ni7);
        } catch (JSONException e2) {
            f1127a.e(e2);
        }
    }

    @DexIgnore
    public static void h(Context context, String str) {
        if (str != null) {
            ii7.g(context, "_mta_ky_tag_", ji7.g(str));
        }
    }

    @DexIgnore
    public static void i(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2;
        ni7 ni7;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(c.f2529a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    ni7 = c;
                } else if (next.equalsIgnoreCase(Integer.toString(b.f2529a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    ni7 = b;
                } else if (next.equalsIgnoreCase("rs")) {
                    gg7 statReportStrategy = gg7.getStatReportStrategy(jSONObject.getInt(next));
                    if (statReportStrategy != null) {
                        d = statReportStrategy;
                        if (K()) {
                            th7 th7 = f1127a;
                            th7.b("Change to ReportStrategy:" + statReportStrategy.name());
                        }
                    }
                } else {
                    return;
                }
                g(context, ni7, jSONObject2);
            }
        } catch (JSONException e2) {
            f1127a.e(e2);
        }
    }

    @DexIgnore
    public static void j(JSONObject jSONObject) {
        try {
            gg7 statReportStrategy = gg7.getStatReportStrategy(jSONObject.getInt("rs"));
            if (statReportStrategy != null) {
                U(statReportStrategy);
            }
        } catch (JSONException e2) {
            if (K()) {
                f1127a.h("rs not found.");
            }
        }
    }

    @DexIgnore
    public static boolean k(int i2, int i3, int i4) {
        return i2 >= i3 && i2 <= i4;
    }

    @DexIgnore
    public static boolean l(String str) {
        if (str == null) {
            return false;
        }
        String str2 = q;
        if (str2 == null) {
            q = str;
            return true;
        } else if (str2.contains(str)) {
            return false;
        } else {
            q += "|" + str;
            return true;
        }
    }

    @DexIgnore
    public static boolean m(JSONObject jSONObject, String str, String str2) {
        if (!jSONObject.isNull(str)) {
            String optString = jSONObject.optString(str);
            return ei7.t(str2) && ei7.t(optString) && str2.equalsIgnoreCase(optString);
        }
    }

    @DexIgnore
    public static void n() {
        E++;
    }

    @DexIgnore
    public static void o(int i2) {
        if (i2 >= 0) {
            E = i2;
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x00f3: SGET  (r4v5 int) =  android.os.Build.VERSION.SDK_INT int)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x011f: SGET  (r4v7 int) =  android.os.Build.VERSION.SDK_INT int)] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0034 A[Catch:{ Exception -> 0x01ab }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void p(android.content.Context r8, org.json.JSONObject r9) {
        /*
        // Method dump skipped, instructions count: 453
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fg7.p(android.content.Context, org.json.JSONObject):void");
    }

    @DexIgnore
    public static void q(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.length() != 0) {
            try {
                p(qi7.a(), jSONObject);
                String string = jSONObject.getString(n);
                if (K()) {
                    th7 th7 = f1127a;
                    th7.b("hibernateVer:" + string + ", current version:2.0.3");
                }
                long o2 = ei7.o(string);
                if (ei7.o("2.0.3") <= o2) {
                    e(o2);
                }
            } catch (JSONException e2) {
                f1127a.b("__HIBERNATE__ not found.");
            }
        }
    }

    @DexIgnore
    public static int r() {
        return E;
    }

    @DexIgnore
    public static String s(Context context) {
        String str;
        synchronized (fg7.class) {
            try {
                if (q != null) {
                    str = q;
                } else {
                    if (context != null && q == null) {
                        q = ei7.z(context);
                    }
                    if (q == null || q.trim().length() == 0) {
                        f1127a.f("AppKey can not be null or empty, please read Developer's Guide first!");
                    }
                    str = q;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static oi7 t() {
        return H;
    }

    @DexIgnore
    public static String u(Context context) {
        if (context == null) {
            f1127a.f("Context for getCustomUid is null.");
            return null;
        }
        if (G == null) {
            G = ii7.d(context, "MTA_CUSTOM_UID", "");
        }
        return G;
    }

    @DexIgnore
    public static String v(Context context) {
        String str;
        synchronized (fg7.class) {
            try {
                if (r != null) {
                    str = r;
                } else {
                    String d2 = ii7.d(context, s, "");
                    r = d2;
                    if (d2 == null || d2.trim().length() == 0) {
                        r = ei7.A(context);
                    }
                    if (r == null || r.trim().length() == 0) {
                        f1127a.l("installChannel can not be null or empty, please read Developer's Guide first!");
                    }
                    str = r;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static String w(Context context) {
        return context != null ? re7.a(context).d().a() : "0";
    }

    @DexIgnore
    public static int x() {
        return l;
    }

    @DexIgnore
    public static int y() {
        return D;
    }

    @DexIgnore
    public static int z() {
        return k;
    }
}
