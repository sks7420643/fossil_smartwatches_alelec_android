package com.fossil;

import android.app.Notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3343a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Notification c;

    @DexIgnore
    public t01(int i, Notification notification, int i2) {
        this.f3343a = i;
        this.c = notification;
        this.b = i2;
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public Notification b() {
        return this.c;
    }

    @DexIgnore
    public int c() {
        return this.f3343a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || t01.class != obj.getClass()) {
            return false;
        }
        t01 t01 = (t01) obj;
        if (this.f3343a == t01.f3343a && this.b == t01.b) {
            return this.c.equals(t01.c);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.f3343a * 31) + this.b) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ForegroundInfo{mNotificationId=" + this.f3343a + ", mForegroundServiceType=" + this.b + ", mNotification=" + this.c + '}';
    }
}
