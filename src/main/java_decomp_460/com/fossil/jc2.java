package com.fossil;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jc2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends ql2 implements jc2 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jc2$a$a")
        /* renamed from: com.fossil.jc2$a$a  reason: collision with other inner class name */
        public static final class C0128a extends rl2 implements jc2 {
            @DexIgnore
            public C0128a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            @Override // com.fossil.jc2
            public final Account m() throws RemoteException {
                Parcel e = e(2, d());
                Account account = (Account) sl2.b(e, Account.CREATOR);
                e.recycle();
                return account;
            }
        }

        @DexIgnore
        public static jc2 e(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            return queryLocalInterface instanceof jc2 ? (jc2) queryLocalInterface : new C0128a(iBinder);
        }
    }

    @DexIgnore
    Account m() throws RemoteException;
}
