package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ double f950a;

    /*
    static {
        double d = 1.0d;
        if (Build.VERSION.SDK_INT >= 17) {
            d = 1.0d / Math.pow(10.0d, 6.0d);
        }
        f950a = d;
    }
    */

    @DexIgnore
    public static double a(long j) {
        return ((double) (b() - j)) * f950a;
    }

    @DexIgnore
    @TargetApi(17)
    public static long b() {
        return Build.VERSION.SDK_INT >= 17 ? SystemClock.elapsedRealtimeNanos() : SystemClock.uptimeMillis();
    }
}
