package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it5 implements Factory<ht5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ it5 f1666a; // = new it5();
    }

    @DexIgnore
    public static it5 a() {
        return a.f1666a;
    }

    @DexIgnore
    public static ht5 c() {
        return new ht5();
    }

    @DexIgnore
    /* renamed from: b */
    public ht5 get() {
        return c();
    }
}
