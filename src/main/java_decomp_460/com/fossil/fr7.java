package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fr7 {
    @DexIgnore
    public gs7 a(nq7 nq7) {
        return nq7;
    }

    @DexIgnore
    public es7 b(Class cls) {
        return new iq7(cls);
    }

    @DexIgnore
    public fs7 c(Class cls, String str) {
        return new vq7(cls, str);
    }

    @DexIgnore
    public is7 d(rq7 rq7) {
        return rq7;
    }

    @DexIgnore
    public js7 e(sq7 sq7) {
        return sq7;
    }

    @DexIgnore
    public ls7 f(wq7 wq7) {
        return wq7;
    }

    @DexIgnore
    public ms7 g(xq7 xq7) {
        return xq7;
    }

    @DexIgnore
    public String h(mq7 mq7) {
        String obj = mq7.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }

    @DexIgnore
    public String i(qq7 qq7) {
        return h(qq7);
    }
}
