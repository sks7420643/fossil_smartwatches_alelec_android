package com.fossil;

import com.fossil.e88;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a88 extends e88.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f218a; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements e88<w18, w18> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f219a; // = new a();

        @DexIgnore
        /* renamed from: b */
        public w18 a(w18 w18) throws IOException {
            try {
                return u88.a(w18);
            } finally {
                w18.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements e88<RequestBody, RequestBody> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f220a; // = new b();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.e88
        public /* bridge */ /* synthetic */ RequestBody a(RequestBody requestBody) throws IOException {
            RequestBody requestBody2 = requestBody;
            b(requestBody2);
            return requestBody2;
        }

        @DexIgnore
        public RequestBody b(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements e88<w18, w18> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ c f221a; // = new c();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.e88
        public /* bridge */ /* synthetic */ w18 a(w18 w18) throws IOException {
            w18 w182 = w18;
            b(w182);
            return w182;
        }

        @DexIgnore
        public w18 b(w18 w18) {
            return w18;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements e88<Object, String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ d f222a; // = new d();

        @DexIgnore
        /* renamed from: b */
        public String a(Object obj) {
            return obj.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements e88<w18, tl7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e f223a; // = new e();

        @DexIgnore
        /* renamed from: b */
        public tl7 a(w18 w18) {
            w18.close();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements e88<w18, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ f f224a; // = new f();

        @DexIgnore
        /* renamed from: b */
        public Void a(w18 w18) {
            w18.close();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.e88.a
    public e88<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(u88.i(type))) {
            return b.f220a;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.e88.a
    public e88<w18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == w18.class) {
            return u88.m(annotationArr, ba8.class) ? c.f221a : a.f219a;
        }
        if (type == Void.class) {
            return f.f224a;
        }
        if (this.f218a && type == tl7.class) {
            try {
                return e.f223a;
            } catch (NoClassDefFoundError e2) {
                this.f218a = false;
            }
        }
        return null;
    }
}
