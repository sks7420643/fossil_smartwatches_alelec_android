package com.fossil;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z08 {
    @DexIgnore
    public static /* final */ z08 n;
    @DexIgnore
    public static /* final */ z08 o;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f4403a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public String m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f4404a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public z08 a() {
            return new z08(this);
        }

        @DexIgnore
        public a b(int i, TimeUnit timeUnit) {
            if (i >= 0) {
                long seconds = timeUnit.toSeconds((long) i);
                this.d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
                return this;
            }
            throw new IllegalArgumentException("maxStale < 0: " + i);
        }

        @DexIgnore
        public a c() {
            this.f4404a = true;
            return this;
        }

        @DexIgnore
        public a d() {
            this.b = true;
            return this;
        }

        @DexIgnore
        public a e() {
            this.f = true;
            return this;
        }
    }

    /*
    static {
        a aVar = new a();
        aVar.c();
        n = aVar.a();
        a aVar2 = new a();
        aVar2.e();
        aVar2.b(Integer.MAX_VALUE, TimeUnit.SECONDS);
        o = aVar2.a();
    }
    */

    @DexIgnore
    public z08(a aVar) {
        this.f4403a = aVar.f4404a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = -1;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = aVar.d;
        this.i = aVar.e;
        this.j = aVar.f;
        this.k = aVar.g;
        this.l = aVar.h;
    }

    @DexIgnore
    public z08(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, boolean z8, String str) {
        this.f4403a = z;
        this.b = z2;
        this.c = i2;
        this.d = i3;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = i4;
        this.i = i5;
        this.j = z6;
        this.k = z7;
        this.l = z8;
        this.m = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.z08 k(com.fossil.p18 r26) {
        /*
        // Method dump skipped, instructions count: 475
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z08.k(com.fossil.p18):com.fossil.z08");
    }

    @DexIgnore
    public final String a() {
        StringBuilder sb = new StringBuilder();
        if (this.f4403a) {
            sb.append("no-cache, ");
        }
        if (this.b) {
            sb.append("no-store, ");
        }
        if (this.c != -1) {
            sb.append("max-age=");
            sb.append(this.c);
            sb.append(", ");
        }
        if (this.d != -1) {
            sb.append("s-maxage=");
            sb.append(this.d);
            sb.append(", ");
        }
        if (this.e) {
            sb.append("private, ");
        }
        if (this.f) {
            sb.append("public, ");
        }
        if (this.g) {
            sb.append("must-revalidate, ");
        }
        if (this.h != -1) {
            sb.append("max-stale=");
            sb.append(this.h);
            sb.append(", ");
        }
        if (this.i != -1) {
            sb.append("min-fresh=");
            sb.append(this.i);
            sb.append(", ");
        }
        if (this.j) {
            sb.append("only-if-cached, ");
        }
        if (this.k) {
            sb.append("no-transform, ");
        }
        if (this.l) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    @DexIgnore
    public boolean b() {
        return this.e;
    }

    @DexIgnore
    public boolean c() {
        return this.f;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public int e() {
        return this.h;
    }

    @DexIgnore
    public int f() {
        return this.i;
    }

    @DexIgnore
    public boolean g() {
        return this.g;
    }

    @DexIgnore
    public boolean h() {
        return this.f4403a;
    }

    @DexIgnore
    public boolean i() {
        return this.b;
    }

    @DexIgnore
    public boolean j() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        String str = this.m;
        if (str != null) {
            return str;
        }
        String a2 = a();
        this.m = a2;
        return a2;
    }
}
