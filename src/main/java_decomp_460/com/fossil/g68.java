package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g68 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 4029025366392702726L;

    @DexIgnore
    public g68() {
    }

    @DexIgnore
    public g68(String str) {
        super(str);
    }

    @DexIgnore
    public g68(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public g68(Throwable th) {
        super(th);
    }
}
