package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.fossil.m91;
import com.fossil.o91;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ga1 extends m91<Bitmap> {
    @DexIgnore
    public static /* final */ float DEFAULT_IMAGE_BACKOFF_MULT; // = 2.0f;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_MAX_RETRIES; // = 2;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_TIMEOUT_MS; // = 1000;
    @DexIgnore
    public static /* final */ Object sDecodeLock; // = new Object();
    @DexIgnore
    public /* final */ Bitmap.Config mDecodeConfig;
    @DexIgnore
    public o91.b<Bitmap> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ int mMaxHeight;
    @DexIgnore
    public /* final */ int mMaxWidth;
    @DexIgnore
    public /* final */ ImageView.ScaleType mScaleType;

    @DexIgnore
    @Deprecated
    public ga1(String str, o91.b<Bitmap> bVar, int i, int i2, Bitmap.Config config, o91.a aVar) {
        this(str, bVar, i, i2, ImageView.ScaleType.CENTER_INSIDE, config, aVar);
    }

    @DexIgnore
    public ga1(String str, o91.b<Bitmap> bVar, int i, int i2, ImageView.ScaleType scaleType, Bitmap.Config config, o91.a aVar) {
        super(0, str, aVar);
        this.mLock = new Object();
        setRetryPolicy(new d91(1000, 2, 2.0f));
        this.mListener = bVar;
        this.mDecodeConfig = config;
        this.mMaxWidth = i;
        this.mMaxHeight = i2;
        this.mScaleType = scaleType;
    }

    @DexIgnore
    private o91<Bitmap> doParse(j91 j91) {
        Bitmap decodeByteArray;
        byte[] bArr = j91.b;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (this.mMaxWidth == 0 && this.mMaxHeight == 0) {
            options.inPreferredConfig = this.mDecodeConfig;
            decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int resizedDimension = getResizedDimension(this.mMaxWidth, this.mMaxHeight, i, i2, this.mScaleType);
            int resizedDimension2 = getResizedDimension(this.mMaxHeight, this.mMaxWidth, i2, i, this.mScaleType);
            options.inJustDecodeBounds = false;
            options.inSampleSize = findBestSampleSize(i, i2, resizedDimension, resizedDimension2);
            decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (decodeByteArray != null && (decodeByteArray.getWidth() > resizedDimension || decodeByteArray.getHeight() > resizedDimension2)) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeByteArray, resizedDimension, resizedDimension2, true);
                decodeByteArray.recycle();
                decodeByteArray = createScaledBitmap;
            }
        }
        return decodeByteArray == null ? o91.a(new l91(j91)) : o91.c(decodeByteArray, ba1.c(j91));
    }

    @DexIgnore
    public static int findBestSampleSize(int i, int i2, int i3, int i4) {
        float f = 1.0f;
        while (true) {
            float f2 = 2.0f * f;
            if (((double) f2) > Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4))) {
                return (int) f;
            }
            f = f2;
        }
    }

    @DexIgnore
    public static int getResizedDimension(int i, int i2, int i3, int i4, ImageView.ScaleType scaleType) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (scaleType == ImageView.ScaleType.FIT_XY) {
            return i == 0 ? i3 : i;
        }
        if (i == 0) {
            return (int) ((((double) i2) / ((double) i4)) * ((double) i3));
        }
        if (i2 == 0) {
            return i;
        }
        double d = ((double) i4) / ((double) i3);
        if (scaleType == ImageView.ScaleType.CENTER_CROP) {
            double d2 = (double) i2;
            return ((double) i) * d < d2 ? (int) (d2 / d) : i;
        }
        double d3 = (double) i2;
        return ((double) i) * d > d3 ? (int) (d3 / d) : i;
    }

    @DexIgnore
    @Override // com.fossil.m91
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public void deliverResponse(Bitmap bitmap) {
        o91.b<Bitmap> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(bitmap);
        }
    }

    @DexIgnore
    @Override // com.fossil.m91
    public m91.c getPriority() {
        return m91.c.LOW;
    }

    @DexIgnore
    @Override // com.fossil.m91
    public o91<Bitmap> parseNetworkResponse(j91 j91) {
        o91<Bitmap> a2;
        synchronized (sDecodeLock) {
            try {
                a2 = doParse(j91);
            } catch (OutOfMemoryError e) {
                u91.c("Caught OOM for %d byte image, url=%s", Integer.valueOf(j91.b.length), getUrl());
                a2 = o91.a(new l91(e));
            }
        }
        return a2;
    }
}
