package com.fossil;

import com.fossil.af1;
import com.fossil.sc1;
import com.fossil.wb1;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pc1 implements sc1, wb1.a<Object> {
    @DexIgnore
    public /* final */ List<mb1> b;
    @DexIgnore
    public /* final */ tc1<?> c;
    @DexIgnore
    public /* final */ sc1.a d;
    @DexIgnore
    public int e;
    @DexIgnore
    public mb1 f;
    @DexIgnore
    public List<af1<File, ?>> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public volatile af1.a<?> i;
    @DexIgnore
    public File j;

    @DexIgnore
    public pc1(tc1<?> tc1, sc1.a aVar) {
        this(tc1.c(), tc1, aVar);
    }

    @DexIgnore
    public pc1(List<mb1> list, tc1<?> tc1, sc1.a aVar) {
        this.e = -1;
        this.b = list;
        this.c = tc1;
        this.d = aVar;
    }

    @DexIgnore
    public final boolean a() {
        return this.h < this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.wb1.a
    public void b(Exception exc) {
        this.d.a(this.f, exc, this.i.c, gb1.DATA_DISK_CACHE);
    }

    @DexIgnore
    @Override // com.fossil.sc1
    public boolean c() {
        boolean z = false;
        while (true) {
            if (this.g == null || !a()) {
                int i2 = this.e + 1;
                this.e = i2;
                if (i2 >= this.b.size()) {
                    return false;
                }
                mb1 mb1 = this.b.get(this.e);
                File b2 = this.c.d().b(new qc1(mb1, this.c.o()));
                this.j = b2;
                if (b2 != null) {
                    this.f = mb1;
                    this.g = this.c.j(b2);
                    this.h = 0;
                }
            } else {
                this.i = null;
                while (!z && a()) {
                    List<af1<File, ?>> list = this.g;
                    int i3 = this.h;
                    this.h = i3 + 1;
                    this.i = list.get(i3).b(this.j, this.c.s(), this.c.f(), this.c.k());
                    if (this.i != null && this.c.t(this.i.c.getDataClass())) {
                        this.i.c.d(this.c.l(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.sc1
    public void cancel() {
        af1.a<?> aVar = this.i;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.wb1.a
    public void e(Object obj) {
        this.d.e(this.f, obj, this.i.c, gb1.DATA_DISK_CACHE, this.f);
    }
}
