package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ob7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public hb7 f2666a;
    @DexIgnore
    public List<gb7> b;

    @DexIgnore
    public ob7(hb7 hb7, List<gb7> list) {
        pq7.c(list, "movingObjects");
        this.f2666a = hb7;
        this.b = list;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ob7(hb7 hb7, List list, int i, kq7 kq7) {
        this(hb7, (i & 2) != 0 ? new ArrayList() : list);
    }

    @DexIgnore
    public final List<gb7> a() {
        return this.b;
    }

    @DexIgnore
    public final hb7 b() {
        return this.f2666a;
    }

    @DexIgnore
    public final void c(List<gb7> list) {
        pq7.c(list, "<set-?>");
        this.b = list;
    }

    @DexIgnore
    public final void d(hb7 hb7) {
        this.f2666a = hb7;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        ob7 ob7 = (ob7) (!(obj instanceof ob7) ? null : obj);
        if (ob7 == null) {
            return false;
        }
        return pq7.a(this.f2666a, ob7.f2666a) && cc7.a(this.b, ob7.b);
    }

    @DexIgnore
    public int hashCode() {
        hb7 hb7 = this.f2666a;
        return ((hb7 != null ? hb7.hashCode() : 0) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "WFThemeData(wfBackground=" + this.f2666a + ", movingObjects=" + this.b + ")";
    }
}
