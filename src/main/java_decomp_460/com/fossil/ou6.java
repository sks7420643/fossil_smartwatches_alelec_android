package com.fossil;

import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.uirenew.customview.ExpandableTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ou6 extends FrameLayout implements View.OnClickListener {
    @DexIgnore
    public a b;
    @DexIgnore
    public ExpandableTextView c;
    @DexIgnore
    public InAppNotification d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void X5(InAppNotification inAppNotification);
    }

    @DexIgnore
    public void a() {
        ((ViewGroup) ((Activity) getContext()).findViewById(16908290)).removeView(this);
        a aVar = this.b;
        if (aVar != null) {
            aVar.X5(this.d);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int id = view.getId();
        if (id == 2131362158) {
            this.c.h();
        } else if (id == 2131362209 || id == 2131363483) {
            a();
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
