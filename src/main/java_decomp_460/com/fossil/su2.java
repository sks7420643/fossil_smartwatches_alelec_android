package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su2 extends e13<su2, a> implements o23 {
    @DexIgnore
    public static /* final */ su2 zzf;
    @DexIgnore
    public static volatile z23<su2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<su2, a> implements o23 {
        @DexIgnore
        public a() {
            super(su2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(ou2 ou2) {
            this();
        }
    }

    /*
    static {
        su2 su2 = new su2();
        zzf = su2;
        e13.u(su2.class, su2);
    }
    */

    @DexIgnore
    public final String C() {
        return this.zzd;
    }

    @DexIgnore
    public final String D() {
        return this.zze;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (ou2.f2725a[i - 1]) {
            case 1:
                return new su2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1008\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                z23<su2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (su2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new e13.c(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
