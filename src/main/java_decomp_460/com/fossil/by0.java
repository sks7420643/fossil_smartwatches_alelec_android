package com.fossil;

import android.animation.TypeEvaluator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class by0 implements TypeEvaluator<float[]> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float[] f523a;

    @DexIgnore
    public by0(float[] fArr) {
        this.f523a = fArr;
    }

    @DexIgnore
    /* renamed from: a */
    public float[] evaluate(float f, float[] fArr, float[] fArr2) {
        float[] fArr3 = this.f523a;
        if (fArr3 == null) {
            fArr3 = new float[fArr.length];
        }
        for (int i = 0; i < fArr3.length; i++) {
            float f2 = fArr[i];
            fArr3[i] = f2 + ((fArr2[i] - f2) * f);
        }
        return fArr3;
    }
}
