package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Size;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.qa8;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na8 implements qa8 {
    @DexIgnore
    public static /* final */ ja8 b; // = new ja8();
    @DexIgnore
    public static ia8 c; // = new ia8();
    @DexIgnore
    public static /* final */ String[] d; // = {"bucket_id", "bucket_display_name"};
    @DexIgnore
    public static /* final */ na8 e; // = new na8();

    @DexIgnore
    public static /* synthetic */ Uri x(na8 na8, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        return na8.v(str, i, z);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public void a(Context context, ka8 ka8, byte[] bArr) {
        pq7.c(context, "context");
        pq7.c(ka8, "asset");
        pq7.c(bArr, "byteArray");
        c.c(context, ka8, bArr, true);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public boolean b(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        return qa8.b.b(this, context, str);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public List<ma8> c(Context context, int i, long j, la8 la8) {
        pq7.c(context, "context");
        pq7.c(la8, "option");
        ArrayList arrayList = new ArrayList();
        ArrayList<String> arrayList2 = new ArrayList<>();
        String q = q(i, la8, arrayList2);
        arrayList2.add(String.valueOf(j));
        String str = "bucket_id IS NOT NULL " + q + " AND date_added <= ? " + y(Integer.valueOf(i));
        ContentResolver contentResolver = context.getContentResolver();
        Uri n = n();
        String[] strArr = d;
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(n, strArr, str, (String[]) array, null);
            if (query != null) {
                pq7.b(query, "context.contentResolver.\u2026           ?: return list");
                HashMap hashMap = new HashMap();
                HashMap hashMap2 = new HashMap();
                while (query.moveToNext()) {
                    String string = query.getString(0);
                    if (hashMap.containsKey(string)) {
                        pq7.b(string, "galleryId");
                        Object obj = hashMap2.get(string);
                        if (obj != null) {
                            hashMap2.put(string, Integer.valueOf(((Number) obj).intValue() + 1));
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        String string2 = query.getString(1);
                        pq7.b(string, "galleryId");
                        pq7.b(string2, "galleryName");
                        hashMap.put(string, string2);
                        hashMap2.put(string, 1);
                    }
                }
                for (Map.Entry entry : hashMap.entrySet()) {
                    String str2 = (String) entry.getKey();
                    String str3 = (String) entry.getValue();
                    Object obj2 = hashMap2.get(str2);
                    if (obj2 != null) {
                        pq7.b(obj2, "countMap[id]!!");
                        arrayList.add(new ma8(str2, str3, ((Number) obj2).intValue(), i, false));
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                query.close();
            }
            return arrayList;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public List<String> d(Context context, List<String> list) {
        pq7.c(context, "context");
        pq7.c(list, "ids");
        return qa8.b.a(this, context, list);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public List<ka8> e(Context context, String str, int i, int i2, int i3, long j, la8 la8) {
        String str2;
        pq7.c(context, "context");
        pq7.c(str, "gId");
        pq7.c(la8, "option");
        ja8 ja8 = b;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri n = n();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String q = q(i3, la8, arrayList2);
        String y = y(Integer.valueOf(i3));
        arrayList2.add(String.valueOf(j));
        Object[] array = em7.D(dm7.s(dm7.s(qa8.f2953a.b(), qa8.f2953a.c()), qa8.f2953a.d())).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + q + " AND date_added <= ? " + y;
            } else {
                str2 = "bucket_id = ? " + q + " AND date_added <= ? " + y;
            }
            String str3 = "datetaken DESC LIMIT " + (i2 - i) + " OFFSET " + i;
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(n, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return hm7.e();
                }
                pq7.b(query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    ka8 ka8 = new ka8(u(query, FieldType.FOREIGN_ID_FIELD_SUFFIX), u(query, "_data"), i3 == 1 ? 0 : s(query, "duration"), s(query, "datetaken"), r(query, "width"), r(query, "height"), t(r(query, MessengerShareContentUtility.MEDIA_TYPE)), u(query, "_display_name"), s(query, "date_modified"));
                    arrayList.add(ka8);
                    ja8.c(ka8);
                }
                query.close();
                return arrayList;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a7, code lost:
        com.fossil.so7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00aa, code lost:
        throw r2;
     */
    @DexIgnore
    @Override // com.fossil.qa8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] f(android.content.Context r5, com.fossil.ka8 r6, boolean r7) {
        /*
            r4 = this;
            java.lang.String r0 = "context"
            com.fossil.pq7.c(r5, r0)
            java.lang.String r0 = "asset"
            com.fossil.pq7.c(r6, r0)
            com.fossil.ia8 r0 = com.fossil.na8.c
            java.lang.String r1 = r6.e()
            java.lang.String r2 = r6.b()
            r3 = 1
            java.io.File r0 = r0.b(r5, r1, r2, r3)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x003c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "the origin bytes come from "
            r1.append(r2)
            java.lang.String r2 = r0.getAbsolutePath()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.fossil.xa8.b(r1)
            byte[] r0 = com.fossil.ap7.a(r0)
        L_0x003b:
            return r0
        L_0x003c:
            android.net.Uri r0 = r4.w(r6, r7)
            android.content.ContentResolver r1 = r5.getContentResolver()
            java.io.InputStream r1 = r1.openInputStream(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "the cache file no exists, will read from MediaStore: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.fossil.xa8.b(r0)
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            if (r1 == 0) goto L_0x0070
            byte[] r2 = com.fossil.ro7.c(r1)     // Catch:{ all -> 0x00a4 }
            r0.write(r2)     // Catch:{ all -> 0x00a4 }
            com.fossil.tl7 r2 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x00a4 }
            r2 = 0
            com.fossil.so7.a(r1, r2)
        L_0x0070:
            byte[] r0 = r0.toByteArray()
            boolean r1 = com.fossil.xa8.f4085a
            if (r1 == 0) goto L_0x009e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "The asset "
            r1.append(r2)
            java.lang.String r2 = r6.e()
            r1.append(r2)
            java.lang.String r2 = " origin byte length : "
            r1.append(r2)
            java.lang.String r2 = "byteArray"
            com.fossil.pq7.b(r0, r2)
            int r2 = r0.length
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.fossil.xa8.b(r1)
        L_0x009e:
            java.lang.String r1 = "byteArray"
            com.fossil.pq7.b(r0, r1)
            goto L_0x003b
        L_0x00a4:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a6 }
        L_0x00a6:
            r2 = move-exception
            com.fossil.so7.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.na8.f(android.content.Context, com.fossil.ka8, boolean):byte[]");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public Bitmap g(Context context, String str, int i, int i2, Integer num) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        if (num == null) {
            return null;
        }
        return context.getContentResolver().loadThumbnail(x(this, str, num.intValue(), false, 4, null), new Size(i, i2), null);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public String h(Context context, String str, boolean z) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        ka8 j = j(context, str);
        if (j != null) {
            return c.a(context, str, j.b(), j.j(), z).getPath();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0087, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0088, code lost:
        com.fossil.so7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008f, code lost:
        com.fossil.so7.a(r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0092, code lost:
        throw r1;
     */
    @DexIgnore
    @Override // com.fossil.qa8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.ka8 i(android.content.Context r10, byte[] r11, java.lang.String r12, java.lang.String r13) {
        /*
            r9 = this;
            r0 = 0
            java.lang.String r1 = "context"
            com.fossil.pq7.c(r10, r1)
            java.lang.String r1 = "image"
            com.fossil.pq7.c(r11, r1)
            java.lang.String r1 = "title"
            com.fossil.pq7.c(r12, r1)
            java.lang.String r1 = "desc"
            com.fossil.pq7.c(r13, r1)
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream
            r1.<init>(r11)
            android.content.ContentResolver r2 = r10.getContentResolver()
            long r4 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, float:1.401E-42)
            long r6 = (long) r3
            long r4 = r4 / r6
            java.lang.String r3 = java.net.URLConnection.guessContentTypeFromStream(r1)
            android.net.Uri r6 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r7 = new android.content.ContentValues
            r7.<init>()
            java.lang.String r8 = "_display_name"
            r7.put(r8, r12)
            java.lang.String r8 = "mime_type"
            r7.put(r8, r3)
            java.lang.String r3 = "title"
            r7.put(r3, r12)
            java.lang.String r3 = "description"
            r7.put(r3, r13)
            java.lang.String r3 = "date_added"
            java.lang.Long r8 = java.lang.Long.valueOf(r4)
            r7.put(r3, r8)
            java.lang.String r3 = "date_modified"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r7.put(r3, r4)
            android.net.Uri r3 = r2.insert(r6, r7)
            if (r3 == 0) goto L_0x0084
            java.lang.String r4 = "cr.insert(uri, values) ?: return null"
            com.fossil.pq7.b(r3, r4)
            java.io.OutputStream r4 = r2.openOutputStream(r3)
            if (r4 == 0) goto L_0x0075
            r5 = 0
            r6 = 2
            r7 = 0
            com.fossil.ro7.b(r1, r4, r5, r6, r7)     // Catch:{ all -> 0x0085 }
            r5 = 0
            com.fossil.so7.a(r1, r5)     // Catch:{ all -> 0x008c }
            com.fossil.so7.a(r4, r0)
        L_0x0075:
            long r4 = android.content.ContentUris.parseId(r3)
            r2.notifyChange(r3, r0)
            java.lang.String r0 = java.lang.String.valueOf(r4)
            com.fossil.ka8 r0 = r9.j(r10, r0)
        L_0x0084:
            return r0
        L_0x0085:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r2 = move-exception
            com.fossil.so7.a(r1, r0)
            throw r2
        L_0x008c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008e }
        L_0x008e:
            r1 = move-exception
            com.fossil.so7.a(r4, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.na8.i(android.content.Context, byte[], java.lang.String, java.lang.String):com.fossil.ka8");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e8, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00e9, code lost:
        com.fossil.so7.a(r16, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ee, code lost:
        throw r3;
     */
    @DexIgnore
    @Override // com.fossil.qa8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.ka8 j(android.content.Context r18, java.lang.String r19) {
        /*
        // Method dump skipped, instructions count: 250
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.na8.j(android.content.Context, java.lang.String):com.fossil.ka8");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0082, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0083, code lost:
        com.fossil.so7.a(r10, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0086, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0089, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008a, code lost:
        com.fossil.so7.a(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008d, code lost:
        throw r1;
     */
    @DexIgnore
    @Override // com.fossil.qa8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.ka8 k(android.content.Context r9, java.io.InputStream r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            r0 = 0
            java.lang.String r1 = "context"
            com.fossil.pq7.c(r9, r1)
            java.lang.String r1 = "inputStream"
            com.fossil.pq7.c(r10, r1)
            java.lang.String r1 = "title"
            com.fossil.pq7.c(r11, r1)
            java.lang.String r1 = "desc"
            com.fossil.pq7.c(r12, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, float:1.401E-42)
            long r4 = (long) r4
            long r2 = r2 / r4
            java.lang.String r4 = java.net.URLConnection.guessContentTypeFromStream(r10)
            android.net.Uri r5 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "_display_name"
            r6.put(r7, r11)
            java.lang.String r7 = "mime_type"
            r6.put(r7, r4)
            java.lang.String r4 = "title"
            r6.put(r4, r11)
            java.lang.String r4 = "description"
            r6.put(r4, r12)
            java.lang.String r4 = "date_added"
            java.lang.Long r7 = java.lang.Long.valueOf(r2)
            r6.put(r4, r7)
            java.lang.String r4 = "date_modified"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r6.put(r4, r2)
            android.net.Uri r2 = r1.insert(r5, r6)
            if (r2 == 0) goto L_0x007f
            java.lang.String r3 = "cr.insert(uri, values) ?: return null"
            com.fossil.pq7.b(r2, r3)
            java.io.OutputStream r3 = r1.openOutputStream(r2)
            if (r3 == 0) goto L_0x0070
            r4 = 0
            r5 = 2
            r6 = 0
            com.fossil.ro7.b(r10, r3, r4, r5, r6)     // Catch:{ all -> 0x0080 }
            r4 = 0
            com.fossil.so7.a(r10, r4)     // Catch:{ all -> 0x0087 }
            com.fossil.so7.a(r3, r0)
        L_0x0070:
            long r4 = android.content.ContentUris.parseId(r2)
            r1.notifyChange(r2, r0)
            java.lang.String r0 = java.lang.String.valueOf(r4)
            com.fossil.ka8 r0 = r8.j(r9, r0)
        L_0x007f:
            return r0
        L_0x0080:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r1 = move-exception
            com.fossil.so7.a(r10, r0)
            throw r1
        L_0x0087:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0089 }
        L_0x0089:
            r1 = move-exception
            com.fossil.so7.a(r3, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.na8.k(android.content.Context, java.io.InputStream, java.lang.String, java.lang.String):com.fossil.ka8");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public void l() {
        b.a();
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public eq0 m(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        try {
            ka8 j = j(context, str);
            if (j != null) {
                Uri requireOriginal = MediaStore.setRequireOriginal(j.j() == 1 ? Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, j.e()) : Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, j.e()));
                pq7.b(requireOriginal, "MediaStore.setRequireOriginal(uri)");
                InputStream openInputStream = context.getContentResolver().openInputStream(requireOriginal);
                if (openInputStream != null) {
                    pq7.b(openInputStream, "context.contentResolver.\u2026iginalUri) ?: return null");
                    return new eq0(openInputStream);
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public Uri n() {
        return qa8.b.c(this);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public ma8 o(Context context, String str, int i, long j, la8 la8) {
        pq7.c(context, "context");
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        Uri n = n();
        String[] a2 = qa8.f2953a.a();
        String str2 = "";
        boolean a3 = pq7.a(str, "");
        ArrayList<String> arrayList = new ArrayList<>();
        String q = q(i, la8, arrayList);
        arrayList.add(String.valueOf(j));
        if (!a3) {
            arrayList.add(str);
            str2 = "AND bucket_id = ?";
        }
        String str3 = "bucket_id IS NOT NULL " + q + " AND date_added <= ? " + str2 + ' ' + y(null);
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(n, a2, str3, (String[]) array, null);
            if (query != null) {
                pq7.b(query, "context.contentResolver.\u2026           ?: return null");
                if (query.moveToNext()) {
                    String string = query.getString(1);
                    pq7.b(string, "cursor.getString(1)");
                    return new ma8(str, string, query.getCount(), i, a3);
                }
                query.close();
            }
            return null;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public List<ka8> p(Context context, String str, int i, int i2, int i3, long j, la8 la8, ja8 ja8) {
        String str2;
        pq7.c(context, "context");
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        if (ja8 == null) {
            ja8 = b;
        }
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri n = n();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String q = q(i3, la8, arrayList2);
        String y = y(Integer.valueOf(i3));
        arrayList2.add(String.valueOf(j));
        Object[] array = em7.D(dm7.s(dm7.s(qa8.f2953a.b(), qa8.f2953a.c()), qa8.f2953a.d())).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + q + " AND date_added <= ? " + y;
            } else {
                str2 = "bucket_id = ? " + q + " AND date_added <= ? " + y;
            }
            String str3 = "datetaken DESC LIMIT " + i2 + " OFFSET " + (i2 * i);
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(n, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return hm7.e();
                }
                pq7.b(query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    ka8 ka8 = new ka8(u(query, FieldType.FOREIGN_ID_FIELD_SUFFIX), u(query, "_data"), i3 == 1 ? 0 : s(query, "duration"), s(query, "datetaken"), r(query, "width"), r(query, "height"), t(r(query, MessengerShareContentUtility.MEDIA_TYPE)), u(query, "_display_name"), s(query, "date_modified"));
                    arrayList.add(ka8);
                    ja8.c(ka8);
                }
                query.close();
                return arrayList;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public String q(int i, la8 la8, ArrayList<String> arrayList) {
        pq7.c(la8, "filterOptions");
        pq7.c(arrayList, "args");
        return qa8.b.e(this, i, la8, arrayList);
    }

    @DexIgnore
    public int r(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getInt");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.g(this, cursor, str);
    }

    @DexIgnore
    public long s(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getLong");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.h(this, cursor, str);
    }

    @DexIgnore
    public int t(int i) {
        return qa8.b.i(this, i);
    }

    @DexIgnore
    public String u(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getString");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.j(this, cursor, str);
    }

    @DexIgnore
    public final Uri v(String str, int i, boolean z) {
        Uri withAppendedPath = i == 1 ? Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, str) : Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, str);
        if (z) {
            withAppendedPath = MediaStore.setRequireOriginal(withAppendedPath);
        }
        pq7.b(withAppendedPath, "uri");
        return withAppendedPath;
    }

    @DexIgnore
    public final Uri w(ka8 ka8, boolean z) {
        return v(ka8.e(), ka8.j(), z);
    }

    @DexIgnore
    public String y(Integer num) {
        return qa8.b.k(this, num);
    }
}
