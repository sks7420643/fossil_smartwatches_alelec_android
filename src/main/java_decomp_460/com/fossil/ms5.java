package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms5 implements MembersInjector<ls5> {
    @DexIgnore
    public static void a(ls5 ls5, DeviceRepository deviceRepository) {
        ls5.l = deviceRepository;
    }

    @DexIgnore
    public static void b(ls5 ls5, gu5 gu5) {
        ls5.s = gu5;
    }

    @DexIgnore
    public static void c(ls5 ls5, oq4 oq4) {
        ls5.m = oq4;
    }

    @DexIgnore
    public static void d(ls5 ls5, on5 on5) {
        ls5.k = on5;
    }

    @DexIgnore
    public static void e(ls5 ls5, UserRepository userRepository) {
        ls5.j = userRepository;
    }
}
