package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class lc4 implements wy1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lc4 f2174a; // = new lc4();

    @DexIgnore
    public static wy1 a() {
        return f2174a;
    }

    @DexIgnore
    @Override // com.fossil.wy1
    public Object apply(Object obj) {
        return mc4.b.E((ta4) obj).getBytes(Charset.forName("UTF-8"));
    }
}
