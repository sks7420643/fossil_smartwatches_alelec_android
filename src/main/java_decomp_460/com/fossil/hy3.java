package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy3 implements Comparable<hy3>, Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hy3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ Calendar b;
    @DexIgnore
    public /* final */ String c; // = ny3.n().format(this.b.getTime());
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e; // = this.b.get(1);
    @DexIgnore
    public /* final */ int f; // = this.b.getMaximum(7);
    @DexIgnore
    public /* final */ int g; // = this.b.getActualMaximum(5);
    @DexIgnore
    public /* final */ long h; // = this.b.getTimeInMillis();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hy3> {
        @DexIgnore
        /* renamed from: a */
        public hy3 createFromParcel(Parcel parcel) {
            return hy3.b(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* renamed from: b */
        public hy3[] newArray(int i) {
            return new hy3[i];
        }
    }

    @DexIgnore
    public hy3(Calendar calendar) {
        calendar.set(5, 1);
        Calendar d2 = ny3.d(calendar);
        this.b = d2;
        this.d = d2.get(2);
    }

    @DexIgnore
    public static hy3 b(int i, int i2) {
        Calendar k = ny3.k();
        k.set(1, i);
        k.set(2, i2);
        return new hy3(k);
    }

    @DexIgnore
    public static hy3 c(long j) {
        Calendar k = ny3.k();
        k.setTimeInMillis(j);
        return new hy3(k);
    }

    @DexIgnore
    public static hy3 n() {
        return new hy3(ny3.i());
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(hy3 hy3) {
        return this.b.compareTo(hy3.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int e() {
        int firstDayOfWeek = this.b.get(7) - this.b.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.f : firstDayOfWeek;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hy3)) {
            return false;
        }
        hy3 hy3 = (hy3) obj;
        return this.d == hy3.d && this.e == hy3.e;
    }

    @DexIgnore
    public long f(int i) {
        Calendar d2 = ny3.d(this.b);
        d2.set(5, i);
        return d2.getTimeInMillis();
    }

    @DexIgnore
    public String h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.d), Integer.valueOf(this.e)});
    }

    @DexIgnore
    public long i() {
        return this.b.getTimeInMillis();
    }

    @DexIgnore
    public hy3 k(int i) {
        Calendar d2 = ny3.d(this.b);
        d2.add(2, i);
        return new hy3(d2);
    }

    @DexIgnore
    public int m(hy3 hy3) {
        if (this.b instanceof GregorianCalendar) {
            return ((hy3.e - this.e) * 12) + (hy3.d - this.d);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.e);
        parcel.writeInt(this.d);
    }
}
