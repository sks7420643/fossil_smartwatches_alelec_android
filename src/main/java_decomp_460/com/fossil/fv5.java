package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv5 extends iq4<b, c, a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1216a;

        @DexIgnore
        public a(int i, String str) {
            pq7.c(str, "errorMessage");
            this.f1216a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f1216a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1217a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public b(String str, String str2, String str3) {
            pq7.c(str, Constants.SERVICE);
            pq7.c(str2, "token");
            pq7.c(str3, "clientId");
            this.f1217a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.f1217a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
        @DexIgnore
        public c(MFUser.Auth auth) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase", f = "LoginSocialUseCase.kt", l = {23}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ fv5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(fv5 fv5, qn7 qn7) {
            super(qn7);
            this.this$0 = fv5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = fv5.class.getSimpleName();
        pq7.b(simpleName, "LoginSocialUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public fv5(UserRepository userRepository) {
        pq7.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.fv5.b r10, com.fossil.qn7<java.lang.Object> r11) {
        /*
            r9 = this;
            r8 = 600(0x258, float:8.41E-43)
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.fossil.fv5.d
            if (r0 == 0) goto L_0x0045
            r0 = r11
            com.fossil.fv5$d r0 = (com.fossil.fv5.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0045
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r1.label
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x0054
            if (r0 != r7) goto L_0x004c
            java.lang.Object r0 = r1.L$1
            com.fossil.fv5$b r0 = (com.fossil.fv5.b) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.fv5 r0 = (com.fossil.fv5) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0030:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x008a
            com.fossil.fv5$c r1 = new com.fossil.fv5$c
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.MFUser$Auth r0 = (com.portfolio.platform.data.model.MFUser.Auth) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            com.fossil.fv5$d r0 = new com.fossil.fv5$d
            r0.<init>(r9, r11)
            r1 = r0
            goto L_0x0016
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.fv5.e
            java.lang.String r5 = "running UseCase"
            r0.d(r2, r5)
            if (r10 != 0) goto L_0x006e
            com.fossil.fv5$a r0 = new com.fossil.fv5$a
            java.lang.String r1 = ""
            r0.<init>(r8, r1)
            goto L_0x0044
        L_0x006e:
            com.portfolio.platform.data.source.UserRepository r0 = r9.d
            java.lang.String r2 = r10.b()
            java.lang.String r5 = r10.c()
            java.lang.String r6 = r10.a()
            r1.L$0 = r9
            r1.L$1 = r10
            r1.label = r7
            java.lang.Object r0 = r0.loginWithSocial(r2, r5, r6, r1)
            if (r0 != r4) goto L_0x0030
            r0 = r4
            goto L_0x0044
        L_0x008a:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00c8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.fv5.e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Inside .run failed with http code="
            r4.append(r5)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00d1
            java.lang.String r0 = r0.getMessage()
            if (r0 == 0) goto L_0x00d1
        L_0x00c0:
            com.fossil.fv5$a r1 = new com.fossil.fv5$a
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x0044
        L_0x00c8:
            com.fossil.fv5$a r0 = new com.fossil.fv5$a
            java.lang.String r1 = ""
            r0.<init>(r8, r1)
            goto L_0x0044
        L_0x00d1:
            r0 = r3
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fv5.k(com.fossil.fv5$b, com.fossil.qn7):java.lang.Object");
    }
}
