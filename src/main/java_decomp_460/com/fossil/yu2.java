package com.fossil;

import com.fossil.e13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu2 extends e13<yu2, a> implements o23 {
    @DexIgnore
    public static /* final */ yu2 zzj;
    @DexIgnore
    public static volatile z23<yu2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public float zzg;
    @DexIgnore
    public double zzh;
    @DexIgnore
    public m13<yu2> zzi; // = e13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<yu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(yu2.zzj);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a B(a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).W((yu2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final a C(Iterable<? extends yu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).L(iterable);
            return this;
        }

        @DexIgnore
        public final a E(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).M(str);
            return this;
        }

        @DexIgnore
        public final a G() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).i0();
            return this;
        }

        @DexIgnore
        public final a H(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).R(str);
            return this;
        }

        @DexIgnore
        public final a I() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).j0();
            return this;
        }

        @DexIgnore
        public final int J() {
            return ((yu2) this.c).e0();
        }

        @DexIgnore
        public final a K() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).l0();
            return this;
        }

        @DexIgnore
        public final a x() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).h0();
            return this;
        }

        @DexIgnore
        public final a y(double d) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).C(d);
            return this;
        }

        @DexIgnore
        public final a z(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((yu2) this.c).D(j);
            return this;
        }
    }

    /*
    static {
        yu2 yu2 = new yu2();
        zzj = yu2;
        e13.u(yu2.class, yu2);
    }
    */

    @DexIgnore
    public static a f0() {
        return (a) zzj.w();
    }

    @DexIgnore
    public final void C(double d) {
        this.zzc |= 16;
        this.zzh = d;
    }

    @DexIgnore
    public final void D(long j) {
        this.zzc |= 4;
        this.zzf = j;
    }

    @DexIgnore
    public final void L(Iterable<? extends yu2> iterable) {
        k0();
        nz2.a(iterable, this.zzi);
    }

    @DexIgnore
    public final void M(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    @DexIgnore
    public final boolean N() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final String O() {
        return this.zzd;
    }

    @DexIgnore
    public final void R(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final boolean T() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String U() {
        return this.zze;
    }

    @DexIgnore
    public final void W(yu2 yu2) {
        yu2.getClass();
        k0();
        this.zzi.add(yu2);
    }

    @DexIgnore
    public final boolean X() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long Y() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean Z() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final float a0() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean b0() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final double c0() {
        return this.zzh;
    }

    @DexIgnore
    public final List<yu2> d0() {
        return this.zzi;
    }

    @DexIgnore
    public final int e0() {
        return this.zzi.size();
    }

    @DexIgnore
    public final void h0() {
        this.zzc &= -3;
        this.zze = zzj.zze;
    }

    @DexIgnore
    public final void i0() {
        this.zzc &= -5;
        this.zzf = 0;
    }

    @DexIgnore
    public final void j0() {
        this.zzc &= -17;
        this.zzh = 0.0d;
    }

    @DexIgnore
    public final void k0() {
        m13<yu2> m13 = this.zzi;
        if (!m13.zza()) {
            this.zzi = e13.q(m13);
        }
    }

    @DexIgnore
    public final void l0() {
        this.zzi = e13.B();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new yu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001\u1008\u0000\u0002\u1008\u0001\u0003\u1002\u0002\u0004\u1001\u0003\u0005\u1000\u0004\u0006\u001b", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi", yu2.class});
            case 4:
                return zzj;
            case 5:
                z23<yu2> z232 = zzk;
                if (z232 != null) {
                    return z232;
                }
                synchronized (yu2.class) {
                    try {
                        z23 = zzk;
                        if (z23 == null) {
                            z23 = new e13.c(zzj);
                            zzk = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
