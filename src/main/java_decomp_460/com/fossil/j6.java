package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j6 extends m5 {
    @DexIgnore
    public /* final */ byte[] m;

    @DexIgnore
    public j6(n6 n6Var, byte[] bArr, n4 n4Var) {
        super(v5.i, n6Var, n4Var);
        this.m = bArr;
    }

    @DexIgnore
    @Override // com.fossil.m5, com.fossil.u5
    public JSONObject b(boolean z) {
        JSONObject b = super.b(z);
        if (z) {
            byte[] bArr = this.m;
            if (bArr.length < 100) {
                g80.k(b, jd0.S0, dy1.e(bArr, null, 1, null));
                return b;
            }
        }
        g80.k(b, jd0.T0, Integer.valueOf(this.m.length));
        return b;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.y(this.l, this.m);
        this.k = true;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return (h7Var instanceof l7) && ((l7) h7Var).b == this.l;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.f2462a;
    }
}
