package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ls2 extends IInterface {
    @DexIgnore
    boolean I1(ls2 ls2) throws RemoteException;

    @DexIgnore
    void K0() throws RemoteException;

    @DexIgnore
    void X0(rg2 rg2) throws RemoteException;

    @DexIgnore
    int a() throws RemoteException;

    @DexIgnore
    void e2(String str) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    LatLng getPosition() throws RemoteException;

    @DexIgnore
    void remove() throws RemoteException;

    @DexIgnore
    void setAlpha(float f) throws RemoteException;

    @DexIgnore
    void setAnchor(float f, float f2) throws RemoteException;

    @DexIgnore
    void setDraggable(boolean z) throws RemoteException;

    @DexIgnore
    void setFlat(boolean z) throws RemoteException;

    @DexIgnore
    void setInfoWindowAnchor(float f, float f2) throws RemoteException;

    @DexIgnore
    void setPosition(LatLng latLng) throws RemoteException;

    @DexIgnore
    void setRotation(float f) throws RemoteException;

    @DexIgnore
    void setTitle(String str) throws RemoteException;

    @DexIgnore
    void setVisible(boolean z) throws RemoteException;

    @DexIgnore
    void setZIndex(float f) throws RemoteException;

    @DexIgnore
    void t() throws RemoteException;

    @DexIgnore
    boolean y1() throws RemoteException;
}
