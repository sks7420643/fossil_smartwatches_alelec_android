package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ jp1[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yt1 createFromParcel(Parcel parcel) {
            return new yt1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yt1[] newArray(int i) {
            return new yt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ yt1(Parcel parcel, kq7 kq7) {
        super((jq1) parcel.readParcelable(rq1.class.getClassLoader()), (nt1) parcel.readParcelable(nt1.class.getClassLoader()));
        Object[] createTypedArray = parcel.createTypedArray(jp1.CREATOR);
        if (createTypedArray != null) {
            this.d = (jp1[]) createTypedArray;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public yt1(nt1 nt1) throws IllegalArgumentException {
        this(null, nt1, new jp1[0]);
    }

    @DexIgnore
    public yt1(rq1 rq1, nt1 nt1) throws IllegalArgumentException {
        this(rq1, nt1, new jp1[0]);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yt1(rq1 rq1, nt1 nt1, jp1[] jp1Arr) throws IllegalArgumentException {
        super(rq1, nt1);
        boolean z = true;
        this.d = jp1Arr;
        if (jp1Arr.length < 1 && getDeviceMessage() == null) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException("weatherInfoArray must have at least 1 elements.".toString());
        }
    }

    @DexIgnore
    public yt1(rq1 rq1, jp1[] jp1Arr) throws IllegalArgumentException {
        this(rq1, null, jp1Arr);
    }

    @DexIgnore
    public yt1(jp1[] jp1Arr) throws IllegalArgumentException {
        this(null, null, jp1Arr);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(super.a(), jd0.f2, px1.a(this.d));
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (!(this.d.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                int min = Math.min(this.d.length, 3);
                for (int i = 0; i < min; i++) {
                    jSONArray.put(this.d[i].a());
                }
                jSONObject.put("weatherApp._.config.locations", jSONArray);
            } else {
                JSONObject jSONObject2 = new JSONObject();
                nt1 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
                jSONObject.put("weatherApp._.config.locations", jSONObject2);
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        pq7.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(yt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !Arrays.equals(this.d, ((yt1) obj).d);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherWatchAppData");
    }

    @DexIgnore
    public final jp1[] getWeatherInfoArray() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        return (super.hashCode() * 31) + Arrays.hashCode(this.d);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.d, i);
        }
    }
}
