package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs extends ss {
    @DexIgnore
    public UUID[] A; // = new UUID[0];
    @DexIgnore
    public n6[] B; // = new n6[0];
    @DexIgnore
    public long C; // = ButtonService.CONNECT_TIMEOUT;

    @DexIgnore
    public qs(k5 k5Var) {
        super(hs.e, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(g80.k(g80.k(super.A(), jd0.o1, ey1.a(this.y.H())), jd0.f5, Integer.valueOf(this.y.A.getType())), jd0.g1, g80.j(this.A)), jd0.h1, g80.g(this.B));
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new d6(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void g(u5 u5Var) {
        d6 d6Var = (d6) u5Var;
        this.A = d6Var.k;
        this.B = d6Var.l;
        this.g.add(new hw(0, null, null, g80.k(g80.k(new JSONObject(), jd0.g1, g80.j(this.A)), jd0.h1, g80.g(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.rs, com.fossil.fs
    public void v(u5 u5Var) {
        JSONObject jSONObject;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null)) {
            g80.k(jSONObject, jd0.k, ey1.a(lw.b));
        }
        if (this.v.d == lw.b) {
            g(u5Var);
        }
        m(mw.a(this.v, null, null, (this.A.length == 0) ^ true ? lw.b : lw.s, null, null, 27));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.o1, ey1.a(this.y.H())), jd0.f5, Integer.valueOf(this.y.A.getType()));
    }
}
