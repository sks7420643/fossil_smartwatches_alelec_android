package com.fossil;

import com.fossil.e13;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lu2 extends e13<lu2, b> implements o23 {
    @DexIgnore
    public static /* final */ lu2 zzh;
    @DexIgnore
    public static volatile z23<lu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public m13<String> zzg; // = e13.B();

    @DexIgnore
    public enum a implements g13 {
        UNKNOWN_MATCH_TYPE(0),
        REGEXP(1),
        BEGINS_WITH(2),
        ENDS_WITH(3),
        PARTIAL(4),
        EXACT(5),
        IN_LIST(6);
        
        @DexIgnore
        public /* final */ int zzi;

        @DexIgnore
        public a(int i) {
            this.zzi = i;
        }

        @DexIgnore
        public static a zza(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_MATCH_TYPE;
                case 1:
                    return REGEXP;
                case 2:
                    return BEGINS_WITH;
                case 3:
                    return ENDS_WITH;
                case 4:
                    return PARTIAL;
                case 5:
                    return EXACT;
                case 6:
                    return IN_LIST;
                default:
                    return null;
            }
        }

        @DexIgnore
        public static i13 zzb() {
            return nu2.f2571a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.g13
        public final int zza() {
            return this.zzi;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends e13.a<lu2, b> implements o23 {
        @DexIgnore
        public b() {
            super(lu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ b(fu2 fu2) {
            this();
        }
    }

    /*
    static {
        lu2 lu2 = new lu2();
        zzh = lu2;
        e13.u(lu2.class, lu2);
    }
    */

    @DexIgnore
    public static lu2 L() {
        return zzh;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final a D() {
        a zza = a.zza(this.zzd);
        return zza == null ? a.UNKNOWN_MATCH_TYPE : zza;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean I() {
        return this.zzf;
    }

    @DexIgnore
    public final List<String> J() {
        return this.zzg;
    }

    @DexIgnore
    public final int K() {
        return this.zzg.size();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (fu2.f1206a[i - 1]) {
            case 1:
                return new lu2();
            case 2:
                return new b(null);
            case 3:
                i13 zzb = a.zzb();
                return e13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u1008\u0001\u0003\u1007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", zzb, "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                z23<lu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (lu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new e13.c(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
