package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r68 implements HttpEntity {
    @DexIgnore
    public static /* final */ char[] f; // = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    @DexIgnore
    public /* final */ n68 b;
    @DexIgnore
    public /* final */ Header c;
    @DexIgnore
    public long d;
    @DexIgnore
    public volatile boolean e;

    @DexIgnore
    public r68() {
        this(o68.STRICT, null, null);
    }

    @DexIgnore
    public r68(o68 o68, String str, Charset charset) {
        str = str == null ? c() : str;
        this.b = new n68("form-data", charset, str, o68 == null ? o68.STRICT : o68);
        this.c = new BasicHeader("Content-Type", d(str, charset));
        this.e = true;
    }

    @DexIgnore
    public void a(String str, t68 t68) {
        b(new l68(str, t68));
    }

    @DexIgnore
    public void b(l68 l68) {
        this.b.a(l68);
        this.e = true;
    }

    @DexIgnore
    public String c() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11);
        for (int i = 0; i < nextInt + 30; i++) {
            char[] cArr = f;
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    @DexIgnore
    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    @DexIgnore
    public String d(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        sb.append(str);
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    @DexIgnore
    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    @DexIgnore
    public Header getContentEncoding() {
        return null;
    }

    @DexIgnore
    public long getContentLength() {
        if (this.e) {
            this.d = this.b.f();
            this.e = false;
        }
        return this.d;
    }

    @DexIgnore
    public Header getContentType() {
        return this.c;
    }

    @DexIgnore
    public boolean isChunked() {
        return !isRepeatable();
    }

    @DexIgnore
    public boolean isRepeatable() {
        for (l68 l68 : this.b.d()) {
            if (l68.e().getContentLength() < 0) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isStreaming() {
        return !isRepeatable();
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        this.b.l(outputStream);
    }
}
