package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dd6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ue6 f768a;
    @DexIgnore
    public /* final */ qd6 b;
    @DexIgnore
    public /* final */ zf6 c;
    @DexIgnore
    public /* final */ hi6 d;
    @DexIgnore
    public /* final */ lj6 e;
    @DexIgnore
    public /* final */ dh6 f;

    @DexIgnore
    public dd6(ue6 ue6, qd6 qd6, zf6 zf6, hi6 hi6, lj6 lj6, dh6 dh6) {
        pq7.c(ue6, "mActivityView");
        pq7.c(qd6, "mActiveTimeView");
        pq7.c(zf6, "mCaloriesView");
        pq7.c(hi6, "mHeartrateView");
        pq7.c(lj6, "mSleepView");
        pq7.c(dh6, "mGoalTrackingView");
        this.f768a = ue6;
        this.b = qd6;
        this.c = zf6;
        this.d = hi6;
        this.e = lj6;
        this.f = dh6;
    }

    @DexIgnore
    public final qd6 a() {
        return this.b;
    }

    @DexIgnore
    public final ue6 b() {
        return this.f768a;
    }

    @DexIgnore
    public final zf6 c() {
        return this.c;
    }

    @DexIgnore
    public final dh6 d() {
        return this.f;
    }

    @DexIgnore
    public final hi6 e() {
        return this.d;
    }

    @DexIgnore
    public final lj6 f() {
        return this.e;
    }
}
