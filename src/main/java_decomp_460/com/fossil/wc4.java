package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3917a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore
    public wc4(String str, String str2, String str3, String str4, String str5, String str6, boolean z, int i, int i2) {
        this.f3917a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str6;
        this.f = z;
        this.g = i;
        this.h = i2;
    }

    @DexIgnore
    public wc4(String str, String str2, String str3, String str4, boolean z) {
        this(str, str2, str3, str4, null, null, z, 0, 0);
    }
}
