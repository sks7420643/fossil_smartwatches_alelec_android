package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ rn1 c;
    @DexIgnore
    public /* final */ mn1 d;
    @DexIgnore
    public /* final */ pn1 e;
    @DexIgnore
    public /* final */ sn1 f;
    @DexIgnore
    public /* final */ on1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<an1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final an1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                rn1 a2 = rn1.d.a((i >> 0) & 1);
                mn1 a3 = mn1.d.a((i >> 1) & 1);
                pn1 a4 = pn1.d.a((i >> 2) & 1);
                sn1 a5 = sn1.d.a((i >> 3) & 1);
                on1 a6 = on1.d.a((i >> 4) & 1);
                if (a2 != null && a3 != null && a4 != null && a5 != null && a6 != null) {
                    return new an1(a2, a3, a4, a5, a6);
                }
                throw new IllegalArgumentException("Invalid data properties");
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public an1 b(Parcel parcel) {
            return new an1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public an1 createFromParcel(Parcel parcel) {
            return new an1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public an1[] newArray(int i) {
            return new an1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ an1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.c = rn1.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                pq7.b(readString2, "parcel.readString()!!");
                this.d = mn1.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    pq7.b(readString3, "parcel.readString()!!");
                    this.e = pn1.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        pq7.b(readString4, "parcel.readString()!!");
                        this.f = sn1.valueOf(readString4);
                        String readString5 = parcel.readString();
                        if (readString5 != null) {
                            pq7.b(readString5, "parcel.readString()!!");
                            this.g = on1.valueOf(readString5);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public an1(rn1 rn1, mn1 mn1, pn1 pn1, sn1 sn1, on1 on1) {
        super(zm1.DISPLAY_UNIT);
        this.c = rn1;
        this.d = mn1;
        this.e = pn1;
        this.f = sn1;
        this.g = on1;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((this.c.a() << 0) | 0 | (this.d.a() << 1) | (this.e.a() << 2) | (this.f.a() << 3) | (this.g.a() << 4)).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(g80.k(g80.k(g80.k(g80.k(jSONObject, jd0.T1, ey1.a(this.c)), jd0.U1, ey1.a(this.d)), jd0.V1, ey1.a(this.e)), jd0.W1, ey1.a(this.f)), jd0.X1, ey1.a(this.g));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(an1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            an1 an1 = (an1) obj;
            if (this.c != an1.c) {
                return false;
            }
            if (this.d != an1.d) {
                return false;
            }
            if (this.e != an1.e) {
                return false;
            }
            if (this.f != an1.f) {
                return false;
            }
            return this.g == an1.g;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DisplayUnitConfig");
    }

    @DexIgnore
    public final mn1 getCaloriesUnit() {
        return this.d;
    }

    @DexIgnore
    public final on1 getDateFormat() {
        return this.g;
    }

    @DexIgnore
    public final pn1 getDistanceUnit() {
        return this.e;
    }

    @DexIgnore
    public final rn1 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public final sn1 getTimeFormat() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + this.f.hashCode()) * 31) + this.g.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }
}
