package com.fossil;

import com.fossil.ix1;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha extends qx1<fv1[]> {
    @DexIgnore
    public static /* final */ ha b; // = new ha();

    @DexIgnore
    public ha() {
        super(new ry1(2, 0));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [short, java.lang.Object] */
    @Override // com.fossil.qx1
    public byte[] a(short s, fv1[] fv1Arr) {
        fv1[] fv1Arr2 = fv1Arr;
        byte[] d = b(fv1Arr2);
        byte[] array = ByteBuffer.allocate(d.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put((byte) c().getMajor()).put((byte) c().getMinor()).putShort(0).putShort((short) e(fv1Arr2)).putInt(d.length).put(d).putInt((int) ix1.f1688a.b(d, ix1.a.CRC32C)).array();
        pq7.b(array, "result.array()");
        return array;
    }

    @DexIgnore
    /* renamed from: d */
    public byte[] b(fv1[] fv1Arr) {
        int e = e(fv1Arr);
        ArrayList arrayList = new ArrayList(fv1Arr.length);
        for (fv1 fv1 : fv1Arr) {
            arrayList.add(fv1.getReplyMessageGroup());
        }
        List<ev1> C = pm7.C(arrayList);
        ByteBuffer order = ByteBuffer.allocate(e(fv1Arr)).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) 2).put((byte) 11);
        int i = 0;
        for (ev1 ev1 : C) {
            ArrayList<fv1> arrayList2 = new ArrayList();
            for (fv1 fv12 : fv1Arr) {
                if (pq7.a(fv12.getReplyMessageGroup(), ev1)) {
                    arrayList2.add(fv12);
                }
            }
            int i2 = 0;
            for (fv1 fv13 : arrayList2) {
                i2 = fv13.getNotificationType().a() | i2;
            }
            order.putShort((short) i2).put((byte) ev1.getReplyMessages().length).putInt(e + 12 + i).putInt(ev1.a().length);
            i = ev1.a().length + i;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(order.array());
        for (ev1 ev12 : C) {
            byteArrayOutputStream.write(ev12.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "entriesDataInByte.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final int e(fv1[] fv1Arr) {
        ArrayList arrayList = new ArrayList(fv1Arr.length);
        for (fv1 fv1 : fv1Arr) {
            arrayList.add(fv1.getReplyMessageGroup());
        }
        return (pm7.C(arrayList).size() * 11) + 2;
    }
}
