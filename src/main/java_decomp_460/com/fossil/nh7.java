package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nh7 implements pi7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ List f2524a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ gh7 c;

    @DexIgnore
    public nh7(gh7 gh7, List list, boolean z) {
        this.c = gh7;
        this.f2524a = list;
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void a() {
        ig7.n();
        this.c.r(this.f2524a, this.b, true);
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void b() {
        ig7.p();
        this.c.p(this.f2524a, 1, this.b, true);
    }
}
