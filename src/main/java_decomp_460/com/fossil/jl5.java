package com.fossil;

import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ PortfolioApp f1776a; // = PortfolioApp.h0.c();
    @DexIgnore
    public static /* final */ jl5 b; // = new jl5();

    @DexIgnore
    public static /* synthetic */ Spannable b(jl5 jl5, String str, String str2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 2131099703;
        }
        return jl5.a(str, str2, i);
    }

    @DexIgnore
    public final Spannable a(String str, String str2, int i) {
        int G;
        pq7.c(str, "text");
        pq7.c(str2, MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
        SpannableString spannableString = new SpannableString(str2);
        if (i != 0 && (G = wt7.G(str2, str, 0, false, 6, null)) >= 0) {
            spannableString.setSpan(new StyleSpan(1), G, str.length() + G, 33);
            spannableString.setSpan(new ForegroundColorSpan(gl0.d(PortfolioApp.h0.c(), i)), G, str.length() + G, 33);
        }
        return spannableString;
    }

    @DexIgnore
    public final String c(Integer num, String str) {
        pq7.c(str, "errorMessage");
        if (num != null && num.intValue() == 601) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886794);
            pq7.b(c, "LanguageHelper.getString\u2026ourInternetConnectionAnd)");
            return c;
        } else if (num != null && num.intValue() == 408) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886832);
            pq7.b(c2, "LanguageHelper.getString\u2026asAProblemProcessingThat)");
            return c2;
        } else if ((num != null && num.intValue() == 504) || ((num != null && num.intValue() == 503) || (num != null && num.intValue() == 500))) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886882);
            pq7.b(c3, "LanguageHelper.getString\u2026tlyUndergoingMaintenance)");
            return c3;
        } else if (num != null && num.intValue() == 429) {
            String c4 = um5.c(PortfolioApp.h0.c(), 2131886987);
            pq7.b(c4, "LanguageHelper.getString\u2026_PleaseTryAgainAfterAFew)");
            return c4;
        } else {
            if (TextUtils.isEmpty(str)) {
                str = um5.c(PortfolioApp.h0.c(), 2131886832);
            }
            pq7.b(str, "if (TextUtils.isEmpty(er\u2026   errorMessage\n        }");
            return str;
        }
    }

    @DexIgnore
    public final String d() {
        String f = dk5.g.c().f();
        hr7 hr7 = hr7.f1520a;
        String format = String.format("%s/%s (Linux; U; Android %s; %s; Build/%s)", Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q(), f, Build.VERSION.RELEASE, Build.MODEL, f}, 5));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String e(Integer num, String str) {
        pq7.c(str, "errorMes");
        if (num != null && num.intValue() == 400701) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886292);
            pq7.b(c, "LanguageHelper.getString\u2026UsersFriendRequestsAreAt)");
            return c;
        } else if ((num != null && num.intValue() == 400700) || (num != null && num.intValue() == 400707)) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886293);
            pq7.b(c2, "LanguageHelper.getString\u2026LimitReachedPleaseReview)");
            return c2;
        } else if (num != null && num.intValue() == 404001) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886299);
            pq7.b(c3, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
            return c3;
        } else if (num != null && num.intValue() == 400709) {
            String c4 = um5.c(PortfolioApp.h0.c(), 2131886302);
            pq7.b(c4, "LanguageHelper.getString\u2026t__YouAreNoLongerFriends)");
            return c4;
        } else if (num != null && num.intValue() == 404701) {
            String c5 = um5.c(PortfolioApp.h0.c(), 2131886304);
            pq7.b(c5, "LanguageHelper.getString\u2026ge_Friends_not_available)");
            return c5;
        } else if (num == null || num.intValue() != 400705) {
            return c(num, str);
        } else {
            String c6 = um5.c(PortfolioApp.h0.c(), 2131887490);
            pq7.b(c6, "LanguageHelper.getString\u2026_friend_to_blocked_users)");
            return c6;
        }
    }

    @DexIgnore
    public final ArrayList<String> f() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.add(um5.c(PortfolioApp.h0.c(), 2131886667));
        arrayList.add(um5.c(PortfolioApp.h0.c(), 2131886670));
        arrayList.add(um5.c(PortfolioApp.h0.c(), 2131886669));
        arrayList.add(um5.c(PortfolioApp.h0.c(), 2131886671));
        arrayList.add(um5.c(PortfolioApp.h0.c(), 2131886668));
        return arrayList;
    }

    @DexIgnore
    public final SpannableString g(String str, String str2, float f) {
        pq7.c(str, "bigText");
        pq7.c(str2, "smallText");
        SpannableString spannableString = new SpannableString(str + str2);
        spannableString.setSpan(new RelativeSizeSpan(f), str.length(), str.length() + str2.length(), 0);
        return spannableString;
    }

    @DexIgnore
    public final String h(int i) {
        switch (i) {
            case 1:
                String c = um5.c(PortfolioApp.h0.c(), 2131886137);
                pq7.b(c, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__S)");
                return c;
            case 2:
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886136);
                pq7.b(c2, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__M)");
                return c2;
            case 3:
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886139);
                pq7.b(c3, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__T)");
                return c3;
            case 4:
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886141);
                pq7.b(c4, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__W)");
                return c4;
            case 5:
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886140);
                pq7.b(c5, "LanguageHelper.getString\u2026RepeatEnabled_Label__T_1)");
                return c5;
            case 6:
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886135);
                pq7.b(c6, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__F)");
                return c6;
            case 7:
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886138);
                pq7.b(c7, "LanguageHelper.getString\u2026RepeatEnabled_Label__S_1)");
                return c7;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String i(int i) {
        switch (i) {
            case 1:
                String c = um5.c(PortfolioApp.h0.c(), 2131886684);
                pq7.b(c, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sun)");
                return c;
            case 2:
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886681);
                pq7.b(c2, "LanguageHelper.getString\u2026ain_StepsToday_Text__Mon)");
                return c2;
            case 3:
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886687);
                pq7.b(c3, "LanguageHelper.getString\u2026in_StepsToday_Text__Tues)");
                return c3;
            case 4:
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886688);
                pq7.b(c4, "LanguageHelper.getString\u2026ain_StepsToday_Text__Wed)");
                return c4;
            case 5:
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886685);
                pq7.b(c5, "LanguageHelper.getString\u2026in_StepsToday_Text__Thur)");
                return c5;
            case 6:
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886680);
                pq7.b(c6, "LanguageHelper.getString\u2026ain_StepsToday_Text__Fri)");
                return c6;
            case 7:
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886683);
                pq7.b(c7, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sat)");
                return c7;
            default:
                return "";
        }
    }

    @DexIgnore
    public final char j(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final String k(String str) {
        pq7.c(str, LogBuilder.KEY_TIME);
        String string = PortfolioApp.h0.c().getString(2131886080);
        pq7.b(string, "PortfolioApp.instance.getString(R.string.AM)");
        if (vt7.i(str, string, false, 2, null)) {
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, str.length() - 2);
            pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(um5.c(PortfolioApp.h0.c(), 2131886763));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        String substring2 = str.substring(0, str.length() - 2);
        pq7.b(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb2.append(substring2);
        sb2.append(um5.c(PortfolioApp.h0.c(), 2131886766));
        return sb2.toString();
    }

    @DexIgnore
    public final String l(int i, float f) {
        float h = dl5.h(((float) 100) * f, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getSleepDaySummaryText", "roundedPercent : " + h);
        StringBuilder sb = new StringBuilder();
        hr7 hr7 = hr7.f1520a;
        String string = PortfolioApp.h0.c().getString(i);
        pq7.b(string, "PortfolioApp.instance.getString(stringId)");
        String format = String.format(string, Arrays.copyOf(new Object[]{String.valueOf(h)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(" %");
        return sb.toString();
    }

    @DexIgnore
    public final String m(int i) {
        String e = dl5.e(i);
        pq7.b(e, "NumberHelper.formatNumber(steps)");
        return e;
    }

    @DexIgnore
    public final String n(long j) {
        String format;
        if (j == 0) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886313);
            pq7.b(c, "LanguageHelper.getString\u2026erBoard_Label__NotSynced)");
            return c;
        }
        if (xy4.f4212a.b() - j < 3600000) {
            long b2 = (xy4.f4212a.b() - j) / 60000;
            if (b2 == 0) {
                format = um5.c(PortfolioApp.h0.c(), 2131886314);
            } else {
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886205);
                pq7.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInMins)");
                String format2 = String.format(c2, Arrays.copyOf(new Object[]{String.valueOf(b2)}, 1));
                pq7.b(format2, "java.lang.String.format(format, *args)");
                hr7 hr72 = hr7.f1520a;
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886315);
                pq7.b(c3, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c3, Arrays.copyOf(new Object[]{format2}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            }
        } else {
            long b3 = (xy4.f4212a.b() - j) / 3600000;
            long j2 = (long) 24;
            if (b3 < j2) {
                hr7 hr73 = hr7.f1520a;
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886203);
                pq7.b(c4, "LanguageHelper.getString\u2026enge_Label__StartInHours)");
                String format3 = String.format(c4, Arrays.copyOf(new Object[]{String.valueOf(b3)}, 1));
                pq7.b(format3, "java.lang.String.format(format, *args)");
                hr7 hr74 = hr7.f1520a;
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886315);
                pq7.b(c5, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c5, Arrays.copyOf(new Object[]{format3}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            } else {
                hr7 hr75 = hr7.f1520a;
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886202);
                pq7.b(c6, "LanguageHelper.getString\u2026lenge_Label__StartInDays)");
                String format4 = String.format(c6, Arrays.copyOf(new Object[]{String.valueOf(b3 / j2)}, 1));
                pq7.b(format4, "java.lang.String.format(format, *args)");
                hr7 hr76 = hr7.f1520a;
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886315);
                pq7.b(c7, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c7, Arrays.copyOf(new Object[]{format4}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            }
        }
        pq7.b(format, "if ((ExactTime.milliseco\u2026)\n            }\n        }");
        return format;
    }

    @DexIgnore
    public final CharSequence o(int i) {
        String d = dl5.d(i / 60);
        pq7.b(d, "NumberHelper.formatBigNumber(hours)");
        String c = um5.c(f1776a, 2131887116);
        pq7.b(c, "LanguageHelper.getString\u2026_SetGoalsSleep_Label__Hr)");
        String c2 = um5.c(f1776a, 2131887117);
        pq7.b(c2, "LanguageHelper.getString\u2026SetGoalsSleep_Label__Min)");
        CharSequence concat = TextUtils.concat(g(d, c, 0.7f), g(' ' + dl5.d(i % 60), c2, 0.7f));
        pq7.b(concat, "TextUtils.concat(activeH\u2026ing, remainMinutesString)");
        return concat;
    }

    @DexIgnore
    public final String p(int i) {
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(PortfolioApp.h0.c(), 2131886326);
        pq7.b(c, "LanguageHelper.getString\u2026r_List_Subtitle__Friends)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String q(int i) {
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(PortfolioApp.h0.c(), 2131886327);
        pq7.b(c, "LanguageHelper.getString\u2026r_List_Subtitle__Members)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String r(int i) {
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(PortfolioApp.h0.c(), 2131886328);
        pq7.b(c, "LanguageHelper.getString\u2026itle__PendingInvitations)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String s(int i) {
        int i2 = i / 3600;
        int i3 = i - ((i2 * 60) * 60);
        int i4 = i3 / 60;
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(PortfolioApp.h0.c(), 2131887314);
        pq7.b(c, "LanguageHelper.getString\u2026_time_hour_minute_second)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2), dl5.a(i4), dl5.a(i3 - (i4 * 60))}, 3));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }
}
