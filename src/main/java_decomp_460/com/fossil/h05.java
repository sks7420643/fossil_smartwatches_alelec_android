package com.fossil;

import android.text.TextUtils;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.helper.GsonConvertDateTime;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Resting> {
    }

    @DexIgnore
    public final Resting a(String str) {
        Resting resting;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        zi4 zi4 = new zi4();
        zi4.f(DateTime.class, new GsonConvertDateTime());
        try {
            resting = (Resting) zi4.d().l(str, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toResting()", String.valueOf(tl7.f3441a));
            resting = null;
        }
        return resting;
    }

    @DexIgnore
    public final String b(Resting resting) {
        if (resting == null) {
            return null;
        }
        try {
            zi4 zi4 = new zi4();
            zi4.f(DateTime.class, new GsonConvertDateTime());
            return zi4.d().t(resting);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toString()", String.valueOf(tl7.f3441a));
            return null;
        }
    }
}
