package com.fossil;

import java.util.List;
import java.util.ServiceLoader;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ List<CoroutineExceptionHandler> f997a; // = at7.t(ys7.b(ServiceLoader.load(CoroutineExceptionHandler.class, CoroutineExceptionHandler.class.getClassLoader()).iterator()));

    @DexIgnore
    public static final void a(tn7 tn7, Throwable th) {
        for (CoroutineExceptionHandler coroutineExceptionHandler : f997a) {
            try {
                coroutineExceptionHandler.handleException(tn7, th);
            } catch (Throwable th2) {
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, fv7.b(th, th2));
            }
        }
        Thread currentThread2 = Thread.currentThread();
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, th);
    }
}
