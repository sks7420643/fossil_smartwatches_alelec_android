package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp3 extends lm3 {
    @DexIgnore
    public /* final */ aq3 c;
    @DexIgnore
    public cl3 d;
    @DexIgnore
    public volatile Boolean e;
    @DexIgnore
    public /* final */ ng3 f;
    @DexIgnore
    public /* final */ vq3 g;
    @DexIgnore
    public /* final */ List<Runnable> h; // = new ArrayList();
    @DexIgnore
    public /* final */ ng3 i;

    @DexIgnore
    public fp3(pm3 pm3) {
        super(pm3);
        this.g = new vq3(pm3.zzm());
        this.c = new aq3(this);
        this.f = new ip3(this, pm3);
        this.i = new sp3(this, pm3);
    }

    @DexIgnore
    @Override // com.fossil.lm3
    public final boolean A() {
        return false;
    }

    @DexIgnore
    public final or3 D(boolean z) {
        b();
        return q().B(z ? d().O() : null);
    }

    @DexIgnore
    public final void E(ComponentName componentName) {
        h();
        if (this.d != null) {
            this.d = null;
            d().N().b("Disconnected from device MeasurementService", componentName);
            h();
            Z();
        }
    }

    @DexIgnore
    public final void F(Bundle bundle) {
        h();
        x();
        R(new qp3(this, bundle, D(false)));
    }

    @DexIgnore
    public final void G(u93 u93) {
        h();
        x();
        R(new kp3(this, D(false), u93));
    }

    @DexIgnore
    public final void H(u93 u93, vg3 vg3, String str) {
        h();
        x();
        if (k().t(h62.f1430a) != 0) {
            d().I().a("Not bundling data. Service unavailable or out of date");
            k().T(u93, new byte[0]);
            return;
        }
        R(new pp3(this, vg3, str, u93));
    }

    @DexIgnore
    public final void I(u93 u93, String str, String str2) {
        h();
        x();
        R(new vp3(this, str, str2, D(false), u93));
    }

    @DexIgnore
    public final void J(u93 u93, String str, String str2, boolean z) {
        h();
        x();
        R(new xp3(this, str, str2, z, D(false), u93));
    }

    @DexIgnore
    public final void K(vg3 vg3, String str) {
        rc2.k(vg3);
        h();
        x();
        boolean d0 = d0();
        R(new up3(this, d0, d0 && t().E(vg3), vg3, D(true), str));
    }

    @DexIgnore
    public final void L(cl3 cl3) {
        h();
        rc2.k(cl3);
        this.d = cl3;
        e0();
        h0();
    }

    @DexIgnore
    public final void M(cl3 cl3, zc2 zc2, or3 or3) {
        List<zc2> C;
        h();
        f();
        x();
        boolean d0 = d0();
        int i2 = 100;
        for (int i3 = 0; i3 < 1001 && i2 == 100; i3++) {
            ArrayList arrayList = new ArrayList();
            if (!d0 || (C = t().C(100)) == null) {
                i2 = 0;
            } else {
                arrayList.addAll(C);
                i2 = C.size();
            }
            if (zc2 != null && i2 < 100) {
                arrayList.add(zc2);
            }
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                zc2 zc22 = (zc2) obj;
                if (zc22 instanceof vg3) {
                    try {
                        cl3.E1((vg3) zc22, or3);
                    } catch (RemoteException e2) {
                        d().F().b("Failed to send event to the service", e2);
                    }
                } else if (zc22 instanceof fr3) {
                    try {
                        cl3.u2((fr3) zc22, or3);
                    } catch (RemoteException e3) {
                        d().F().b("Failed to send user property to the service", e3);
                    }
                } else if (zc22 instanceof xr3) {
                    try {
                        cl3.s((xr3) zc22, or3);
                    } catch (RemoteException e4) {
                        d().F().b("Failed to send conditional user property to the service", e4);
                    }
                } else {
                    d().F().a("Discarding data. Unrecognized parcel type.");
                }
            }
        }
    }

    @DexIgnore
    public final void N(xo3 xo3) {
        h();
        x();
        R(new np3(this, xo3));
    }

    @DexIgnore
    public final void P(fr3 fr3) {
        h();
        x();
        R(new hp3(this, d0() && t().F(fr3), fr3, D(true)));
    }

    @DexIgnore
    public final void Q(xr3 xr3) {
        rc2.k(xr3);
        h();
        x();
        b();
        R(new tp3(this, true, t().G(xr3), new xr3(xr3), D(true), xr3));
    }

    @DexIgnore
    public final void R(Runnable runnable) throws IllegalStateException {
        h();
        if (V()) {
            runnable.run();
        } else if (((long) this.h.size()) >= 1000) {
            d().F().a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.i.c(60000);
            Z();
        }
    }

    @DexIgnore
    public final void S(AtomicReference<String> atomicReference) {
        h();
        x();
        R(new lp3(this, atomicReference, D(false)));
    }

    @DexIgnore
    public final void T(AtomicReference<List<xr3>> atomicReference, String str, String str2, String str3) {
        h();
        x();
        R(new wp3(this, atomicReference, str, str2, str3, D(false)));
    }

    @DexIgnore
    public final void U(AtomicReference<List<fr3>> atomicReference, String str, String str2, String str3, boolean z) {
        h();
        x();
        R(new yp3(this, atomicReference, str, str2, str3, z, D(false)));
    }

    @DexIgnore
    public final boolean V() {
        h();
        x();
        return this.d != null;
    }

    @DexIgnore
    public final void W() {
        h();
        x();
        R(new rp3(this, D(true)));
    }

    @DexIgnore
    public final void X() {
        h();
        f();
        x();
        or3 D = D(false);
        if (d0()) {
            t().H();
        }
        R(new jp3(this, D));
    }

    @DexIgnore
    public final void Y() {
        h();
        x();
        or3 D = D(true);
        t().I();
        R(new op3(this, D));
    }

    @DexIgnore
    public final void Z() {
        h();
        x();
        if (!V()) {
            if (f0()) {
                this.c.f();
            } else if (!m().Q()) {
                b();
                List<ResolveInfo> queryIntentServices = e().getPackageManager().queryIntentServices(new Intent().setClassName(e(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    Intent intent = new Intent("com.google.android.gms.measurement.START");
                    Context e2 = e();
                    b();
                    intent.setComponent(new ComponentName(e2, "com.google.android.gms.measurement.AppMeasurementService"));
                    this.c.b(intent);
                    return;
                }
                d().F().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    @DexIgnore
    public final Boolean a0() {
        return this.e;
    }

    @DexIgnore
    public final void b0() {
        h();
        x();
        this.c.a();
        try {
            ve2.b().c(e(), this.c);
        } catch (IllegalArgumentException | IllegalStateException e2) {
        }
        this.d = null;
    }

    @DexIgnore
    public final boolean c0() {
        h();
        x();
        return !f0() || k().J0() >= 200900;
    }

    @DexIgnore
    public final boolean d0() {
        b();
        return true;
    }

    @DexIgnore
    public final void e0() {
        h();
        this.g.a();
        this.f.c(xg3.J.a(null).longValue());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0109  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean f0() {
        /*
        // Method dump skipped, instructions count: 271
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fp3.f0():boolean");
    }

    @DexIgnore
    public final void g0() {
        h();
        if (V()) {
            d().N().a("Inactivity, disconnecting from the service");
            b0();
        }
    }

    @DexIgnore
    public final void h0() {
        h();
        d().N().b("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable runnable : this.h) {
            try {
                runnable.run();
            } catch (Exception e2) {
                d().F().b("Task exception while flushing queue", e2);
            }
        }
        this.h.clear();
        this.i.e();
    }
}
