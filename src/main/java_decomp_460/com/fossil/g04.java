package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g04 {
    @DexIgnore
    public static /* final */ yz3 m; // = new e04(0.5f);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public zz3 f1244a;
    @DexIgnore
    public zz3 b;
    @DexIgnore
    public zz3 c;
    @DexIgnore
    public zz3 d;
    @DexIgnore
    public yz3 e;
    @DexIgnore
    public yz3 f;
    @DexIgnore
    public yz3 g;
    @DexIgnore
    public yz3 h;
    @DexIgnore
    public b04 i;
    @DexIgnore
    public b04 j;
    @DexIgnore
    public b04 k;
    @DexIgnore
    public b04 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public zz3 f1245a; // = d04.b();
        @DexIgnore
        public zz3 b; // = d04.b();
        @DexIgnore
        public zz3 c; // = d04.b();
        @DexIgnore
        public zz3 d; // = d04.b();
        @DexIgnore
        public yz3 e; // = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public yz3 f; // = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public yz3 g; // = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public yz3 h; // = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public b04 i; // = d04.c();
        @DexIgnore
        public b04 j; // = d04.c();
        @DexIgnore
        public b04 k; // = d04.c();
        @DexIgnore
        public b04 l; // = d04.c();

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(g04 g04) {
            this.f1245a = g04.f1244a;
            this.b = g04.b;
            this.c = g04.c;
            this.d = g04.d;
            this.e = g04.e;
            this.f = g04.f;
            this.g = g04.g;
            this.h = g04.h;
            this.i = g04.i;
            this.j = g04.j;
            this.k = g04.k;
            this.l = g04.l;
        }

        @DexIgnore
        public static float n(zz3 zz3) {
            if (zz3 instanceof f04) {
                return ((f04) zz3).f1015a;
            }
            if (zz3 instanceof a04) {
                return ((a04) zz3).f175a;
            }
            return -1.0f;
        }

        @DexIgnore
        public b A(float f2) {
            this.e = new wz3(f2);
            return this;
        }

        @DexIgnore
        public b B(yz3 yz3) {
            this.e = yz3;
            return this;
        }

        @DexIgnore
        public b C(int i2, yz3 yz3) {
            D(d04.a(i2));
            F(yz3);
            return this;
        }

        @DexIgnore
        public b D(zz3 zz3) {
            this.b = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                E(n);
            }
            return this;
        }

        @DexIgnore
        public b E(float f2) {
            this.f = new wz3(f2);
            return this;
        }

        @DexIgnore
        public b F(yz3 yz3) {
            this.f = yz3;
            return this;
        }

        @DexIgnore
        public g04 m() {
            return new g04(this);
        }

        @DexIgnore
        public b o(float f2) {
            A(f2);
            E(f2);
            v(f2);
            r(f2);
            return this;
        }

        @DexIgnore
        public b p(int i2, yz3 yz3) {
            q(d04.a(i2));
            s(yz3);
            return this;
        }

        @DexIgnore
        public b q(zz3 zz3) {
            this.d = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                r(n);
            }
            return this;
        }

        @DexIgnore
        public b r(float f2) {
            this.h = new wz3(f2);
            return this;
        }

        @DexIgnore
        public b s(yz3 yz3) {
            this.h = yz3;
            return this;
        }

        @DexIgnore
        public b t(int i2, yz3 yz3) {
            u(d04.a(i2));
            w(yz3);
            return this;
        }

        @DexIgnore
        public b u(zz3 zz3) {
            this.c = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                v(n);
            }
            return this;
        }

        @DexIgnore
        public b v(float f2) {
            this.g = new wz3(f2);
            return this;
        }

        @DexIgnore
        public b w(yz3 yz3) {
            this.g = yz3;
            return this;
        }

        @DexIgnore
        public b x(b04 b04) {
            this.i = b04;
            return this;
        }

        @DexIgnore
        public b y(int i2, yz3 yz3) {
            z(d04.a(i2));
            B(yz3);
            return this;
        }

        @DexIgnore
        public b z(zz3 zz3) {
            this.f1245a = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                A(n);
            }
            return this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        yz3 a(yz3 yz3);
    }

    @DexIgnore
    public g04() {
        this.f1244a = d04.b();
        this.b = d04.b();
        this.c = d04.b();
        this.d = d04.b();
        this.e = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = new wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = d04.c();
        this.j = d04.c();
        this.k = d04.c();
        this.l = d04.c();
    }

    @DexIgnore
    public g04(b bVar) {
        this.f1244a = bVar.f1245a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
    }

    @DexIgnore
    public static b a() {
        return new b();
    }

    @DexIgnore
    public static b b(Context context, int i2, int i3) {
        return c(context, i2, i3, 0);
    }

    @DexIgnore
    public static b c(Context context, int i2, int i3, int i4) {
        return d(context, i2, i3, new wz3((float) i4));
    }

    @DexIgnore
    public static b d(Context context, int i2, int i3, yz3 yz3) {
        if (i3 != 0) {
            context = new ContextThemeWrapper(context, i2);
        } else {
            i3 = i2;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i3, tw3.ShapeAppearance);
        try {
            int i4 = obtainStyledAttributes.getInt(tw3.ShapeAppearance_cornerFamily, 0);
            int i5 = obtainStyledAttributes.getInt(tw3.ShapeAppearance_cornerFamilyTopLeft, i4);
            int i6 = obtainStyledAttributes.getInt(tw3.ShapeAppearance_cornerFamilyTopRight, i4);
            int i7 = obtainStyledAttributes.getInt(tw3.ShapeAppearance_cornerFamilyBottomRight, i4);
            int i8 = obtainStyledAttributes.getInt(tw3.ShapeAppearance_cornerFamilyBottomLeft, i4);
            yz3 m2 = m(obtainStyledAttributes, tw3.ShapeAppearance_cornerSize, yz3);
            yz3 m3 = m(obtainStyledAttributes, tw3.ShapeAppearance_cornerSizeTopLeft, m2);
            yz3 m4 = m(obtainStyledAttributes, tw3.ShapeAppearance_cornerSizeTopRight, m2);
            yz3 m5 = m(obtainStyledAttributes, tw3.ShapeAppearance_cornerSizeBottomRight, m2);
            yz3 m6 = m(obtainStyledAttributes, tw3.ShapeAppearance_cornerSizeBottomLeft, m2);
            b bVar = new b();
            bVar.y(i5, m3);
            bVar.C(i6, m4);
            bVar.t(i7, m5);
            bVar.p(i8, m6);
            return bVar;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public static b e(Context context, AttributeSet attributeSet, int i2, int i3) {
        return f(context, attributeSet, i2, i3, 0);
    }

    @DexIgnore
    public static b f(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        return g(context, attributeSet, i2, i3, new wz3((float) i4));
    }

    @DexIgnore
    public static b g(Context context, AttributeSet attributeSet, int i2, int i3, yz3 yz3) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, tw3.MaterialShape, i2, i3);
        int resourceId = obtainStyledAttributes.getResourceId(tw3.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(tw3.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return d(context, resourceId, resourceId2, yz3);
    }

    @DexIgnore
    public static yz3 m(TypedArray typedArray, int i2, yz3 yz3) {
        TypedValue peekValue = typedArray.peekValue(i2);
        if (peekValue == null) {
            return yz3;
        }
        int i3 = peekValue.type;
        return i3 == 5 ? new wz3((float) TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics())) : i3 == 6 ? new e04(peekValue.getFraction(1.0f, 1.0f)) : yz3;
    }

    @DexIgnore
    public b04 h() {
        return this.k;
    }

    @DexIgnore
    public zz3 i() {
        return this.d;
    }

    @DexIgnore
    public yz3 j() {
        return this.h;
    }

    @DexIgnore
    public zz3 k() {
        return this.c;
    }

    @DexIgnore
    public yz3 l() {
        return this.g;
    }

    @DexIgnore
    public b04 n() {
        return this.l;
    }

    @DexIgnore
    public b04 o() {
        return this.j;
    }

    @DexIgnore
    public b04 p() {
        return this.i;
    }

    @DexIgnore
    public zz3 q() {
        return this.f1244a;
    }

    @DexIgnore
    public yz3 r() {
        return this.e;
    }

    @DexIgnore
    public zz3 s() {
        return this.b;
    }

    @DexIgnore
    public yz3 t() {
        return this.f;
    }

    @DexIgnore
    public boolean u(RectF rectF) {
        boolean z = this.l.getClass().equals(b04.class) && this.j.getClass().equals(b04.class) && this.i.getClass().equals(b04.class) && this.k.getClass().equals(b04.class);
        float a2 = this.e.a(rectF);
        return z && ((this.f.a(rectF) > a2 ? 1 : (this.f.a(rectF) == a2 ? 0 : -1)) == 0 && (this.h.a(rectF) > a2 ? 1 : (this.h.a(rectF) == a2 ? 0 : -1)) == 0 && (this.g.a(rectF) > a2 ? 1 : (this.g.a(rectF) == a2 ? 0 : -1)) == 0) && ((this.b instanceof f04) && (this.f1244a instanceof f04) && (this.c instanceof f04) && (this.d instanceof f04));
    }

    @DexIgnore
    public b v() {
        return new b(this);
    }

    @DexIgnore
    public g04 w(float f2) {
        b v = v();
        v.o(f2);
        return v.m();
    }

    @DexIgnore
    public g04 x(c cVar) {
        b v = v();
        v.B(cVar.a(r()));
        v.F(cVar.a(t()));
        v.s(cVar.a(j()));
        v.w(cVar.a(l()));
        return v.m();
    }
}
