package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bh1 implements qb1<Uri, Drawable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f428a;

    @DexIgnore
    public bh1(Context context) {
        this.f428a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Drawable> b(Uri uri, int i, int i2, ob1 ob1) {
        Context d = d(uri, uri.getAuthority());
        return ah1.f(yg1.b(this.f428a, d, g(d, uri)));
    }

    @DexIgnore
    public final Context d(Uri uri, String str) {
        if (str.equals(this.f428a.getPackageName())) {
            return this.f428a;
        }
        try {
            return this.f428a.createPackageContext(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            if (str.contains(this.f428a.getPackageName())) {
                return this.f428a;
            }
            throw new IllegalArgumentException("Failed to obtain context or unrecognized Uri format for: " + uri, e);
        }
    }

    @DexIgnore
    public final int e(Uri uri) {
        try {
            return Integer.parseInt(uri.getPathSegments().get(0));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Unrecognized Uri format: " + uri, e);
        }
    }

    @DexIgnore
    public final int f(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        String authority = uri.getAuthority();
        String str = pathSegments.get(0);
        String str2 = pathSegments.get(1);
        int identifier = context.getResources().getIdentifier(str2, str, authority);
        int identifier2 = identifier == 0 ? Resources.getSystem().getIdentifier(str2, str, "android") : identifier;
        if (identifier2 != 0) {
            return identifier2;
        }
        throw new IllegalArgumentException("Failed to find resource id for: " + uri);
    }

    @DexIgnore
    public final int g(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 2) {
            return f(context, uri);
        }
        if (pathSegments.size() == 1) {
            return e(uri);
        }
        throw new IllegalArgumentException("Unrecognized Uri format: " + uri);
    }

    @DexIgnore
    /* renamed from: h */
    public boolean a(Uri uri, ob1 ob1) {
        return uri.getScheme().equals("android.resource");
    }
}
