package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum o8 {
    ALARM((byte) 0),
    REMINDER((byte) 1);
    
    @DexIgnore
    public static /* final */ n8 f; // = new n8(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public o8(byte b2) {
        this.b = (byte) b2;
    }
}
