package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ k5 b;
    @DexIgnore
    public /* final */ /* synthetic */ g7 c;
    @DexIgnore
    public /* final */ /* synthetic */ UUID[] d;
    @DexIgnore
    public /* final */ /* synthetic */ n6[] e;

    @DexIgnore
    public s4(k5 k5Var, g7 g7Var, UUID[] uuidArr, n6[] n6VarArr) {
        this.b = k5Var;
        this.c = g7Var;
        this.d = uuidArr;
        this.e = n6VarArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.g.f();
        this.b.z.g.c(new c7(this.c, this.d, this.e));
    }
}
