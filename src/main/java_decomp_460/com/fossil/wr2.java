package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr2 implements Parcelable.Creator<vr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vr2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        tr2 tr2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l == 2) {
                tr2 = (tr2) ad2.e(parcel, t, tr2.CREATOR);
            } else if (l == 3) {
                iBinder2 = ad2.u(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                iBinder = ad2.u(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new vr2(i, tr2, iBinder2, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vr2[] newArray(int i) {
        return new vr2[i];
    }
}
