package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui extends qq7 implements rp7<nr, Boolean> {
    @DexIgnore
    public static /* final */ ui b; // = new ui();

    @DexIgnore
    public ui() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public Boolean invoke(nr nrVar) {
        nr nrVar2 = nrVar;
        zq zqVar = nrVar2.c;
        return Boolean.valueOf(zqVar == zq.INTERRUPTED || zqVar == zq.CONNECTION_DROPPED || zqVar == zq.BLUETOOTH_OFF || nrVar2.d.e.d.b == f7.GATT_NULL);
    }
}
