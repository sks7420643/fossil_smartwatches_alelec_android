package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Class<?> f3428a; // = a();
    @DexIgnore
    public static /* final */ Field b; // = b();
    @DexIgnore
    public static /* final */ Field c; // = f();
    @DexIgnore
    public static /* final */ Method d; // = d(f3428a);
    @DexIgnore
    public static /* final */ Method e; // = c(f3428a);
    @DexIgnore
    public static /* final */ Method f; // = e(f3428a);
    @DexIgnore
    public static /* final */ Handler g; // = new Handler(Looper.getMainLooper());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ d b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public a(d dVar, Object obj) {
            this.b = dVar;
            this.c = obj;
        }

        @DexIgnore
        public void run() {
            this.b.b = this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Application b;
        @DexIgnore
        public /* final */ /* synthetic */ d c;

        @DexIgnore
        public b(Application application, d dVar) {
            this.b = application;
            this.c = dVar;
        }

        @DexIgnore
        public void run() {
            this.b.unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public c(Object obj, Object obj2) {
            this.b = obj;
            this.c = obj2;
        }

        @DexIgnore
        public void run() {
            try {
                if (tk0.d != null) {
                    tk0.d.invoke(this.b, this.c, Boolean.FALSE, "AppCompat recreation");
                    return;
                }
                tk0.e.invoke(this.b, this.c, Boolean.FALSE);
            } catch (RuntimeException e) {
                if (e.getClass() == RuntimeException.class && e.getMessage() != null && e.getMessage().startsWith("Unable to stop")) {
                    throw e;
                }
            } catch (Throwable th) {
                Log.e("ActivityRecreator", "Exception while invoking performStopActivity", th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public Object b;
        @DexIgnore
        public Activity c;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public boolean f; // = false;

        @DexIgnore
        public d(Activity activity) {
            this.c = activity;
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            if (this.c == activity) {
                this.c = null;
                this.e = true;
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            if (this.e && !this.f && !this.d && tk0.h(this.b, activity)) {
                this.f = true;
                this.b = null;
            }
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            if (this.c == activity) {
                this.d = true;
            }
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("android.app.ActivityThread");
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Field b() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mMainThread");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Method c(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Method d(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE, String.class);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Method e(Class<?> cls) {
        if (g() && cls != null) {
            try {
                Method declaredMethod = cls.getDeclaredMethod("requestRelaunchActivity", IBinder.class, List.class, List.class, Integer.TYPE, Boolean.TYPE, Configuration.class, Configuration.class, Boolean.TYPE, Boolean.TYPE);
                declaredMethod.setAccessible(true);
                return declaredMethod;
            } catch (Throwable th) {
            }
        }
        return null;
    }

    @DexIgnore
    public static Field f() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mToken");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static boolean g() {
        int i = Build.VERSION.SDK_INT;
        return i == 26 || i == 27;
    }

    @DexIgnore
    public static boolean h(Object obj, Activity activity) {
        try {
            Object obj2 = c.get(activity);
            if (obj2 != obj) {
                return false;
            }
            g.postAtFrontOfQueue(new c(b.get(activity), obj2));
            return true;
        } catch (Throwable th) {
            Log.e("ActivityRecreator", "Exception while fetching field values", th);
            return false;
        }
    }

    @DexIgnore
    public static boolean i(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
            return true;
        } else if (g() && f == null) {
            return false;
        } else {
            if (e == null && d == null) {
                return false;
            }
            try {
                Object obj = c.get(activity);
                if (obj == null) {
                    return false;
                }
                Object obj2 = b.get(activity);
                if (obj2 == null) {
                    return false;
                }
                Application application = activity.getApplication();
                d dVar = new d(activity);
                application.registerActivityLifecycleCallbacks(dVar);
                g.post(new a(dVar, obj));
                try {
                    if (g()) {
                        f.invoke(obj2, obj, null, null, 0, Boolean.FALSE, null, null, Boolean.FALSE, Boolean.FALSE);
                    } else {
                        activity.recreate();
                    }
                    return true;
                } finally {
                    g.post(new b(application, dVar));
                }
            } catch (Throwable th) {
                return false;
            }
        }
    }
}
