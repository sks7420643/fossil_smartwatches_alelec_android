package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rq4 {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ a G;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Properties f3143a;
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String A() {
            return rq4.f;
        }

        @DexIgnore
        public final String B() {
            return rq4.h;
        }

        @DexIgnore
        public final String C() {
            return rq4.i;
        }

        @DexIgnore
        public final String D() {
            return rq4.F;
        }

        @DexIgnore
        public final String E() {
            return rq4.E;
        }

        @DexIgnore
        public final String F() {
            return rq4.u;
        }

        @DexIgnore
        public final String a() {
            return rq4.b;
        }

        @DexIgnore
        public final String b() {
            return rq4.D;
        }

        @DexIgnore
        public final String c() {
            return rq4.C;
        }

        @DexIgnore
        public final String d() {
            return rq4.c;
        }

        @DexIgnore
        public final String e() {
            return rq4.v;
        }

        @DexIgnore
        public final String f() {
            return rq4.B;
        }

        @DexIgnore
        public final String g() {
            return rq4.A;
        }

        @DexIgnore
        public final String h() {
            return rq4.w;
        }

        @DexIgnore
        public final String i() {
            return rq4.n;
        }

        @DexIgnore
        public final String j() {
            return rq4.o;
        }

        @DexIgnore
        public final String k() {
            return rq4.p;
        }

        @DexIgnore
        public final String l() {
            return rq4.j;
        }

        @DexIgnore
        public final String m() {
            return rq4.k;
        }

        @DexIgnore
        public final String n() {
            return rq4.l;
        }

        @DexIgnore
        public final String o() {
            return rq4.q;
        }

        @DexIgnore
        public final String p() {
            return rq4.m;
        }

        @DexIgnore
        public final Properties q(Context context) {
            pq7.c(context, "context");
            Properties properties = new Properties();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open("production.properties"), "UTF-8"));
                properties.load(bufferedReader);
                bufferedReader.close();
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("XXX", "Exception when load properties=" + e);
            }
            return properties;
        }

        @DexIgnore
        public final String r() {
            return rq4.z;
        }

        @DexIgnore
        public final String s() {
            return rq4.y;
        }

        @DexIgnore
        public final String t() {
            return rq4.s;
        }

        @DexIgnore
        public final String u() {
            return rq4.r;
        }

        @DexIgnore
        public final String v() {
            return rq4.x;
        }

        @DexIgnore
        public final String w() {
            return rq4.t;
        }

        @DexIgnore
        public final String x() {
            return rq4.e;
        }

        @DexIgnore
        public final String y() {
            return rq4.d;
        }

        @DexIgnore
        public final String z() {
            return rq4.g;
        }
    }

    /*
    static {
        a aVar = new a(null);
        G = aVar;
        Properties q2 = aVar.q(PortfolioApp.h0.c());
        f3143a = q2;
        String property = q2.getProperty("APP_CODE");
        pq7.b(property, "config.getProperty(\"APP_CODE\")");
        b = property;
        pq7.b(f3143a.getProperty("AUTH_ID"), "config.getProperty(\"AUTH_ID\")");
        pq7.b(f3143a.getProperty("AUTH_HOST"), "config.getProperty(\"AUTH_HOST\")");
        String property2 = f3143a.getProperty("BRAND_ID");
        pq7.b(property2, "config.getProperty(\"BRAND_ID\")");
        c = property2;
        String property3 = f3143a.getProperty("WATCH_FACE_GALLERY_STAGING");
        pq7.b(property3, "config.getProperty(\"WATCH_FACE_GALLERY_STAGING\")");
        d = property3;
        String property4 = f3143a.getProperty("WATCH_FACE_GALLERY_PRODUCTION");
        pq7.b(property4, "config.getProperty(\"WATC\u2026FACE_GALLERY_PRODUCTION\")");
        e = property4;
        String property5 = f3143a.getProperty("WATCH_FACE_QUICK_VIEW_STAGING");
        pq7.b(property5, "config.getProperty(\"WATC\u2026FACE_QUICK_VIEW_STAGING\")");
        f = property5;
        String property6 = f3143a.getProperty("WATCH_FACE_QUICK_VIEW_PRODUCTION");
        pq7.b(property6, "config.getProperty(\"WATC\u2026E_QUICK_VIEW_PRODUCTION\")");
        g = property6;
        pq7.b(f3143a.getProperty("UA_REDIRECT"), "config.getProperty(\"UA_REDIRECT\")");
        String property7 = f3143a.getProperty("WEIBO_REDIRECT_URL");
        pq7.b(property7, "config.getProperty(\"WEIBO_REDIRECT_URL\")");
        h = property7;
        String property8 = f3143a.getProperty("WEIBO_SCOPE");
        pq7.b(property8, "config.getProperty(\"WEIBO_SCOPE\")");
        i = property8;
        String property9 = f3143a.getProperty("MISFIT_API_BASE_URL_STAGING");
        pq7.b(property9, "config.getProperty(\"MISFIT_API_BASE_URL_STAGING\")");
        j = property9;
        String property10 = f3143a.getProperty("MISFIT_API_BASE_URL_STAGING_V2");
        pq7.b(property10, "config.getProperty(\"MISF\u2026API_BASE_URL_STAGING_V2\")");
        k = property10;
        String property11 = f3143a.getProperty("MISFIT_API_BASE_URL_STAGING_V2DOT1");
        pq7.b(property11, "config.getProperty(\"MISF\u2026BASE_URL_STAGING_V2DOT1\")");
        l = property11;
        String property12 = f3143a.getProperty("MISFIT_WEB_BASE_URL_STAGING");
        pq7.b(property12, "config.getProperty(\"MISFIT_WEB_BASE_URL_STAGING\")");
        m = property12;
        String property13 = f3143a.getProperty("MISFIT_API_BASE_URL_PRODUCTION");
        pq7.b(property13, "config.getProperty(\"MISF\u2026API_BASE_URL_PRODUCTION\")");
        n = property13;
        String property14 = f3143a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2");
        pq7.b(property14, "config.getProperty(\"MISF\u2026_BASE_URL_PRODUCTION_V2\")");
        o = property14;
        String property15 = f3143a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2DOT1");
        pq7.b(property15, "config.getProperty(\"MISF\u2026E_URL_PRODUCTION_V2DOT1\")");
        p = property15;
        String property16 = f3143a.getProperty("MISFIT_WEB_BASE_URL_PRODUCTION");
        pq7.b(property16, "config.getProperty(\"MISF\u2026WEB_BASE_URL_PRODUCTION\")");
        q = property16;
        pq7.b(f3143a.getProperty("EMAIL_MAGIC_STAGING_STAGING"), "config.getProperty(\"EMAIL_MAGIC_STAGING_STAGING\")");
        pq7.b(f3143a.getProperty("EMAIL_MAGIC_STAGING_PRODUCTION"), "config.getProperty(\"EMAI\u2026AGIC_STAGING_PRODUCTION\")");
        pq7.b(f3143a.getProperty("EMAIL_MAGIC_PRODUCTION_STAGING"), "config.getProperty(\"EMAI\u2026AGIC_PRODUCTION_STAGING\")");
        pq7.b(f3143a.getProperty("EMAIL_MAGIC_PRODUCTION_PRODUCTION"), "config.getProperty(\"EMAI\u2026C_PRODUCTION_PRODUCTION\")");
        String property17 = f3143a.getProperty("SDK_ENDPOINT_STAGING");
        pq7.b(property17, "config.getProperty(\"SDK_ENDPOINT_STAGING\")");
        r = property17;
        String property18 = f3143a.getProperty("SDK_ENDPOINT_PRODUCTION");
        pq7.b(property18, "config.getProperty(\"SDK_ENDPOINT_PRODUCTION\")");
        s = property18;
        String property19 = f3143a.getProperty("LIST_MICRO_APP_NOT_SUPPORTED");
        pq7.b(property19, "config.getProperty(\"LIST_MICRO_APP_NOT_SUPPORTED\")");
        t = property19;
        String property20 = f3143a.getProperty("ZENDESK_URL");
        pq7.b(property20, "config.getProperty(\"ZENDESK_URL\")");
        u = property20;
        String property21 = f3143a.getProperty("CLOUD_LOG_BASE_URL");
        pq7.b(property21, "config.getProperty(\"CLOUD_LOG_BASE_URL\")");
        v = property21;
        String property22 = f3143a.getProperty("LOG_BRAND_NAME");
        pq7.b(property22, "config.getProperty(\"LOG_BRAND_NAME\")");
        w = property22;
        String property23 = f3143a.getProperty("SDK_V2_LOG_END_POINT");
        pq7.b(property23, "config.getProperty(\"SDK_V2_LOG_END_POINT\")");
        x = property23;
        String property24 = f3143a.getProperty("SDK_API_BASE_URL_STAGING");
        pq7.b(property24, "config.getProperty(\"SDK_API_BASE_URL_STAGING\")");
        y = property24;
        String property25 = f3143a.getProperty("SDK_API_BASE_URL_PRODUCTION");
        pq7.b(property25, "config.getProperty(\"SDK_API_BASE_URL_PRODUCTION\")");
        z = property25;
        String property26 = f3143a.getProperty("GOOGLE_PROXY_BASE_URL_STAGING");
        pq7.b(property26, "config.getProperty(\"GOOG\u2026_PROXY_BASE_URL_STAGING\")");
        A = property26;
        String property27 = f3143a.getProperty("GOOGLE_PROXY_BASE_URL_PRODUCTION");
        pq7.b(property27, "config.getProperty(\"GOOG\u2026OXY_BASE_URL_PRODUCTION\")");
        B = property27;
        String property28 = f3143a.getProperty("APPLE_AUTHORIZATION_STAGING_URL");
        pq7.b(property28, "config.getProperty(\"APPL\u2026THORIZATION_STAGING_URL\")");
        C = property28;
        String property29 = f3143a.getProperty("APPLE_AUTHORIZATION_PRODUCTION_URL");
        pq7.b(property29, "config.getProperty(\"APPL\u2026RIZATION_PRODUCTION_URL\")");
        D = property29;
        String property30 = f3143a.getProperty("WHAT_NEW_STAGING_URL");
        if (property30 == null) {
            property30 = "";
        }
        E = property30;
        String property31 = f3143a.getProperty("WHAT_NEW_PRODUCTION_URL");
        if (property31 == null) {
            property31 = "";
        }
        F = property31;
    }
    */
}
