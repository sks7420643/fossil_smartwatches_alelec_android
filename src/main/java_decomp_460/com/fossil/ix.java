package com.fossil;

import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Hashtable<String, hx> f1683a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ix b; // = new ix();

    @DexIgnore
    public final hx a(String str) {
        hx hxVar;
        synchronized (f1683a) {
            hxVar = f1683a.get(str);
            if (hxVar == null) {
                hxVar = new hx();
            }
            f1683a.put(str, hxVar);
        }
        return hxVar;
    }

    @DexIgnore
    public final void b(String str, byte[] bArr) {
        synchronized (f1683a) {
            b.a(str).f1550a = bArr;
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final void c(String str, byte[] bArr, byte[] bArr2) {
        synchronized (f1683a) {
            b.a(str).b(bArr, bArr2);
            tl7 tl7 = tl7.f3441a;
        }
    }
}
