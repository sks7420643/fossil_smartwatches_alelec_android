package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3756a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Bundle d;
    }

    @DexIgnore
    public static boolean a(Context context, a aVar) {
        String str;
        if (context == null) {
            str = "send fail, invalid argument";
        } else if (af7.a(aVar.b)) {
            str = "send fail, action is null";
        } else {
            String str2 = null;
            if (!af7.a(aVar.f3756a)) {
                str2 = aVar.f3756a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(aVar.b);
            Bundle bundle = aVar.d;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", aVar.c);
            intent.putExtra("_mmessage_checksum", we7.a(aVar.c, 587268097, packageName));
            context.sendBroadcast(intent, str2);
            ye7.e("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str2);
            return true;
        }
        ye7.b("MicroMsg.SDK.MMessage", str);
        return false;
    }
}
