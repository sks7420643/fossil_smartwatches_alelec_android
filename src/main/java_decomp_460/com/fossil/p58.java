package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p58 implements Cloneable, Serializable {
    @DexIgnore
    public static /* final */ int UNINITIALIZED; // = -1;
    @DexIgnore
    public static /* final */ int UNLIMITED_VALUES; // = -2;
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public String argName;
    @DexIgnore
    public String description;
    @DexIgnore
    public String longOpt;
    @DexIgnore
    public int numberOfArgs;
    @DexIgnore
    public String opt;
    @DexIgnore
    public boolean optionalArg;
    @DexIgnore
    public boolean required;
    @DexIgnore
    public Object type;
    @DexIgnore
    public List values;
    @DexIgnore
    public char valuesep;

    @DexIgnore
    public p58(String str, String str2) throws IllegalArgumentException {
        this(str, null, false, str2);
    }

    @DexIgnore
    public p58(String str, String str2, boolean z, String str3) throws IllegalArgumentException {
        this.argName = "arg";
        this.numberOfArgs = -1;
        this.values = new ArrayList();
        r58.c(str);
        this.opt = str;
        this.longOpt = str2;
        if (z) {
            this.numberOfArgs = 1;
        }
        this.description = str3;
    }

    @DexIgnore
    public p58(String str, boolean z, String str2) throws IllegalArgumentException {
        this(str, null, z, str2);
    }

    @DexIgnore
    public boolean addValue(String str) {
        throw new UnsupportedOperationException("The addValue method is not intended for client use. Subclasses should use the addValueForProcessing method instead. ");
    }

    @DexIgnore
    public void addValueForProcessing(String str) {
        if (this.numberOfArgs != -1) {
            g(str);
            return;
        }
        throw new RuntimeException("NO_ARGS_ALLOWED");
    }

    @DexIgnore
    public void clearValues() {
        this.values.clear();
    }

    @DexIgnore
    @Override // java.lang.Object
    public Object clone() {
        try {
            p58 p58 = (p58) super.clone();
            p58.values = new ArrayList(this.values);
            return p58;
        } catch (CloneNotSupportedException e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("A CloneNotSupportedException was thrown: ");
            stringBuffer.append(e.getMessage());
            throw new RuntimeException(stringBuffer.toString());
        }
    }

    @DexIgnore
    public final void d(String str) {
        if (this.numberOfArgs <= 0 || this.values.size() <= this.numberOfArgs - 1) {
            this.values.add(str);
            return;
        }
        throw new RuntimeException("Cannot add value, list full.");
    }

    @DexIgnore
    public final boolean e() {
        return this.values.isEmpty();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p58.class != obj.getClass()) {
            return false;
        }
        p58 p58 = (p58) obj;
        String str = this.opt;
        if (str == null ? p58.opt != null : !str.equals(p58.opt)) {
            return false;
        }
        String str2 = this.longOpt;
        String str3 = p58.longOpt;
        if (str2 != null) {
            if (str2.equals(str3)) {
                return true;
            }
        } else if (str3 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void g(String str) {
        if (hasValueSeparator()) {
            char valueSeparator = getValueSeparator();
            int indexOf = str.indexOf(valueSeparator);
            while (indexOf != -1 && this.values.size() != this.numberOfArgs - 1) {
                d(str.substring(0, indexOf));
                str = str.substring(indexOf + 1);
                indexOf = str.indexOf(valueSeparator);
            }
        }
        d(str);
    }

    @DexIgnore
    public String getArgName() {
        return this.argName;
    }

    @DexIgnore
    public int getArgs() {
        return this.numberOfArgs;
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public int getId() {
        return getKey().charAt(0);
    }

    @DexIgnore
    public String getKey() {
        String str = this.opt;
        return str == null ? this.longOpt : str;
    }

    @DexIgnore
    public String getLongOpt() {
        return this.longOpt;
    }

    @DexIgnore
    public String getOpt() {
        return this.opt;
    }

    @DexIgnore
    public Object getType() {
        return this.type;
    }

    @DexIgnore
    public String getValue() {
        if (e()) {
            return null;
        }
        return (String) this.values.get(0);
    }

    @DexIgnore
    public String getValue(int i) throws IndexOutOfBoundsException {
        if (e()) {
            return null;
        }
        return (String) this.values.get(i);
    }

    @DexIgnore
    public String getValue(String str) {
        String value = getValue();
        return value != null ? value : str;
    }

    @DexIgnore
    public char getValueSeparator() {
        return this.valuesep;
    }

    @DexIgnore
    public String[] getValues() {
        if (e()) {
            return null;
        }
        List list = this.values;
        return (String[]) list.toArray(new String[list.size()]);
    }

    @DexIgnore
    public List getValuesList() {
        return this.values;
    }

    @DexIgnore
    public boolean hasArg() {
        int i = this.numberOfArgs;
        return i > 0 || i == -2;
    }

    @DexIgnore
    public boolean hasArgName() {
        String str = this.argName;
        return str != null && str.length() > 0;
    }

    @DexIgnore
    public boolean hasArgs() {
        int i = this.numberOfArgs;
        return i > 1 || i == -2;
    }

    @DexIgnore
    public boolean hasLongOpt() {
        return this.longOpt != null;
    }

    @DexIgnore
    public boolean hasOptionalArg() {
        return this.optionalArg;
    }

    @DexIgnore
    public boolean hasValueSeparator() {
        return this.valuesep > 0;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.opt;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.longOpt;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public boolean isRequired() {
        return this.required;
    }

    @DexIgnore
    public void setArgName(String str) {
        this.argName = str;
    }

    @DexIgnore
    public void setArgs(int i) {
        this.numberOfArgs = i;
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setLongOpt(String str) {
        this.longOpt = str;
    }

    @DexIgnore
    public void setOptionalArg(boolean z) {
        this.optionalArg = z;
    }

    @DexIgnore
    public void setRequired(boolean z) {
        this.required = z;
    }

    @DexIgnore
    public void setType(Object obj) {
        this.type = obj;
    }

    @DexIgnore
    public void setValueSeparator(char c) {
        this.valuesep = (char) c;
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[ option: ");
        stringBuffer.append(this.opt);
        if (this.longOpt != null) {
            stringBuffer.append(" ");
            stringBuffer.append(this.longOpt);
        }
        stringBuffer.append(" ");
        if (hasArgs()) {
            stringBuffer.append("[ARG...]");
        } else if (hasArg()) {
            stringBuffer.append(" [ARG]");
        }
        stringBuffer.append(" :: ");
        stringBuffer.append(this.description);
        if (this.type != null) {
            stringBuffer.append(" :: ");
            stringBuffer.append(this.type);
        }
        stringBuffer.append(" ]");
        return stringBuffer.toString();
    }
}
