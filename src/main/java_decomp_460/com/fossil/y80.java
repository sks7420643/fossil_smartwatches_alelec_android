package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y80 extends z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ y80 i; // = new y80();

    @DexIgnore
    public y80() {
        super("raw_minute_data", 102400, 20971520, "raw_minute_data", "raw_minute_data", new zw1("", "", ""), 1800, new b90(), ld0.d, false);
    }

    @DexIgnore
    @Override // com.fossil.z80
    public long e() {
        return h;
    }
}
