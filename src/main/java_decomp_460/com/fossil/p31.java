package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.o31;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnknownNullness"})
public interface p31 {
    @DexIgnore
    int a(f11 f11, String... strArr);

    @DexIgnore
    void b(String str);

    @DexIgnore
    List<o31> c();

    @DexIgnore
    int d(String str, long j);

    @DexIgnore
    List<o31.a> e(String str);

    @DexIgnore
    List<o31> f(long j);

    @DexIgnore
    List<o31> g(int i);

    @DexIgnore
    void h(o31 o31);

    @DexIgnore
    List<o31> i();

    @DexIgnore
    void j(String str, r01 r01);

    @DexIgnore
    List<o31> k();

    @DexIgnore
    List<String> l();

    @DexIgnore
    List<String> m(String str);

    @DexIgnore
    f11 n(String str);

    @DexIgnore
    o31 o(String str);

    @DexIgnore
    int p(String str);

    @DexIgnore
    List<r01> q(String str);

    @DexIgnore
    int r(String str);

    @DexIgnore
    void s(String str, long j);

    @DexIgnore
    int t();
}
