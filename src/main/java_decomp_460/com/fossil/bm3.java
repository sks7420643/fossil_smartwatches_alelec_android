package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f452a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ xl3 e;

    @DexIgnore
    public bm3(xl3 xl3, String str, long j) {
        this.e = xl3;
        rc2.g(str);
        this.f452a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.B().getLong(this.f452a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void b(long j) {
        SharedPreferences.Editor edit = this.e.B().edit();
        edit.putLong(this.f452a, j);
        edit.apply();
        this.d = j;
    }
}
