package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg7 extends ng7 {
    @DexIgnore
    public vh7 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public sg7(Context context, int i, JSONObject jSONObject, jg7 jg7) {
        super(context, i, jg7);
        this.m = new vh7(context);
        this.n = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public og7 a() {
        return og7.b;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public boolean b(JSONObject jSONObject) {
        uh7 uh7 = this.d;
        if (uh7 != null) {
            jSONObject.put("ut", uh7.e());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (ei7.U(this.j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.b(jSONObject, null);
        return true;
    }
}
