package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ld0 extends Enum<ld0> {
    @DexIgnore
    public static /* final */ ld0 b;
    @DexIgnore
    public static /* final */ ld0 c;
    @DexIgnore
    public static /* final */ ld0 d;
    @DexIgnore
    public static /* final */ ld0 e;
    @DexIgnore
    public static /* final */ ld0 f;
    @DexIgnore
    public static /* final */ /* synthetic */ ld0[] g;

    /*
    static {
        ld0 ld0 = new ld0("MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE", 0);
        b = ld0;
        ld0 ld02 = new ld0("SDK_LOG_PREFERENCE", 1);
        c = ld02;
        ld0 ld03 = new ld0("MINUTE_DATA_REFERENCE", 2);
        d = ld03;
        ld0 ld04 = new ld0("HARDWARE_LOG_REFERENCE", 3);
        e = ld04;
        ld0 ld05 = new ld0("GPS_REFERENCE", 4);
        f = ld05;
        g = new ld0[]{ld0, ld02, ld03, ld04, ld05, new ld0("TEXT_ENCRYPTION_PREFERENCE", 5)};
    }
    */

    @DexIgnore
    public ld0(String str, int i) {
    }

    @DexIgnore
    public static ld0 valueOf(String str) {
        return (ld0) Enum.valueOf(ld0.class, str);
    }

    @DexIgnore
    public static ld0[] values() {
        return (ld0[]) g.clone();
    }
}
