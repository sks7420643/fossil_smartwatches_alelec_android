package com.fossil;

import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y56 extends tq4<b, c, a> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tq4.a {
        @DexIgnore
        public a(String str) {
            pq7.c(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<ContactGroup> f4245a;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.contact.ContactGroup> */
        /* JADX WARN: Multi-variable type inference failed */
        public c(List<? extends ContactGroup> list) {
            pq7.c(list, "contactGroups");
            this.f4245a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.f4245a;
        }
    }

    /*
    static {
        String simpleName = y56.class.getSimpleName();
        pq7.b(simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    /* renamed from: f */
    public void a(b bVar) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            b().onSuccess(new c(allContactGroups));
        } else {
            b().a(new a("Get all contact group failed"));
        }
    }
}
