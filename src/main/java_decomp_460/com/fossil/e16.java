package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e16 implements Factory<d16> {
    @DexIgnore
    public static d16 a(y06 y06, uq4 uq4, d26 d26, f26 f26, g26 g26, v36 v36, on5 on5, NotificationSettingsDao notificationSettingsDao, mt5 mt5, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new d16(y06, uq4, d26, f26, g26, v36, on5, notificationSettingsDao, mt5, quickResponseRepository, notificationSettingsDatabase);
    }
}
