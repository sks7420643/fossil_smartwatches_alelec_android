package com.fossil;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt0 implements mt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MediaSessionManager.RemoteUserInfo f2565a;

    @DexIgnore
    public nt0(String str, int i, int i2) {
        this.f2565a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof nt0)) {
            return false;
        }
        return this.f2565a.equals(((nt0) obj).f2565a);
    }

    @DexIgnore
    public int hashCode() {
        return kn0.b(this.f2565a);
    }
}
