package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj7 {
    @DexIgnore
    public static /* final */ int abc_action_bar_title_item; // = 2131558400;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_container; // = 2131558401;
    @DexIgnore
    public static /* final */ int abc_action_menu_item_layout; // = 2131558402;
    @DexIgnore
    public static /* final */ int abc_action_menu_layout; // = 2131558403;
    @DexIgnore
    public static /* final */ int abc_action_mode_bar; // = 2131558404;
    @DexIgnore
    public static /* final */ int abc_action_mode_close_item_material; // = 2131558405;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view; // = 2131558406;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_list_item; // = 2131558407;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_bar_material; // = 2131558408;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_material; // = 2131558409;
    @DexIgnore
    public static /* final */ int abc_dialog_title_material; // = 2131558412;
    @DexIgnore
    public static /* final */ int abc_expanded_menu_layout; // = 2131558413;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_checkbox; // = 2131558414;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_icon; // = 2131558415;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_layout; // = 2131558416;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_radio; // = 2131558417;
    @DexIgnore
    public static /* final */ int abc_popup_menu_item_layout; // = 2131558419;
    @DexIgnore
    public static /* final */ int abc_screen_content_include; // = 2131558420;
    @DexIgnore
    public static /* final */ int abc_screen_simple; // = 2131558421;
    @DexIgnore
    public static /* final */ int abc_screen_simple_overlay_action_mode; // = 2131558422;
    @DexIgnore
    public static /* final */ int abc_screen_toolbar; // = 2131558423;
    @DexIgnore
    public static /* final */ int abc_search_dropdown_item_icons_2line; // = 2131558424;
    @DexIgnore
    public static /* final */ int abc_search_view; // = 2131558425;
    @DexIgnore
    public static /* final */ int abc_select_dialog_material; // = 2131558426;
    @DexIgnore
    public static /* final */ int belvedere_dialog; // = 2131558440;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row; // = 2131558441;
    @DexIgnore
    public static /* final */ int notification_media_action; // = 2131558774;
    @DexIgnore
    public static /* final */ int notification_media_cancel_action; // = 2131558775;
    @DexIgnore
    public static /* final */ int notification_template_big_media; // = 2131558776;
    @DexIgnore
    public static /* final */ int notification_template_big_media_narrow; // = 2131558778;
    @DexIgnore
    public static /* final */ int notification_template_media; // = 2131558783;
    @DexIgnore
    public static /* final */ int notification_template_part_chronometer; // = 2131558785;
    @DexIgnore
    public static /* final */ int notification_template_part_time; // = 2131558786;
    @DexIgnore
    public static /* final */ int select_dialog_item_material; // = 2131558816;
    @DexIgnore
    public static /* final */ int select_dialog_multichoice_material; // = 2131558817;
    @DexIgnore
    public static /* final */ int select_dialog_singlechoice_material; // = 2131558818;
    @DexIgnore
    public static /* final */ int support_simple_spinner_dropdown_item; // = 2131558820;
}
