package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zs2 {
    @DexIgnore
    public static volatile zs2 i; // = null;
    @DexIgnore
    public static Boolean j; // = null;
    @DexIgnore
    public static String k; // = "allow_remote_dynamite";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4521a;
    @DexIgnore
    public /* final */ ef2 b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ fg3 d;
    @DexIgnore
    public List<Pair<sn3, c>> e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public t93 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class a implements Runnable {
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public a(zs2 zs2) {
            this(true);
        }

        @DexIgnore
        public a(boolean z) {
            this.b = zs2.this.b.b();
            this.c = zs2.this.b.c();
            this.d = z;
        }

        @DexIgnore
        public abstract void a() throws RemoteException;

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void run() {
            if (zs2.this.g) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e2) {
                zs2.this.o(e2, false, this.d);
                b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
            zs2.this.k(new zt2(this, activity, bundle));
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
            zs2.this.k(new eu2(this, activity));
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            zs2.this.k(new au2(this, activity));
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            zs2.this.k(new bu2(this, activity));
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            r93 r93 = new r93();
            zs2.this.k(new cu2(this, activity, r93));
            Bundle n = r93.n(50);
            if (n != null) {
                bundle.putAll(n);
            }
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
            zs2.this.k(new yt2(this, activity));
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
            zs2.this.k(new du2(this, activity));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ts2 {
        @DexIgnore
        public /* final */ sn3 b;

        @DexIgnore
        public c(sn3 sn3) {
            this.b = sn3;
        }

        @DexIgnore
        @Override // com.fossil.us2
        public final void C1(String str, String str2, Bundle bundle, long j) {
            this.b.a(str, str2, bundle, j);
        }

        @DexIgnore
        @Override // com.fossil.us2
        public final int zza() {
            return System.identityHashCode(this.b);
        }
    }

    @DexIgnore
    public zs2(Context context, String str, String str2, String str3, Bundle bundle) {
        boolean z = true;
        if (str == null || !K(str2, str3)) {
            this.f4521a = "FA";
        } else {
            this.f4521a = str;
        }
        this.b = hf2.d();
        this.c = c13.a().a(new it2(this), q93.f2941a);
        this.d = new fg3(this);
        if (!(!Q(context) || X())) {
            this.g = true;
            Log.w(this.f4521a, "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection.");
            return;
        }
        if (!K(str2, str3)) {
            if (str2 == null || str3 == null) {
                if ((str3 != null ? false : z) ^ (str2 == null)) {
                    Log.w(this.f4521a, "Specified origin or custom app id is null. Both parameters will be ignored.");
                }
            } else {
                Log.v(this.f4521a, "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy");
            }
        }
        k(new ct2(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            Log.w(this.f4521a, "Unable to register lifecycle notifications. Application null.");
        } else {
            application.registerActivityLifecycleCallbacks(new b());
        }
    }

    @DexIgnore
    public static boolean K(String str, String str2) {
        return (str2 == null || str == null || X()) ? false : true;
    }

    @DexIgnore
    public static boolean Q(Context context) {
        try {
            return yo3.a(context, "google_app_id") != null;
        } catch (IllegalStateException e2) {
        }
    }

    @DexIgnore
    public static int R(Context context) {
        return DynamiteModule.c(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static int T(Context context) {
        return DynamiteModule.a(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static void V(Context context) {
        synchronized (zs2.class) {
            try {
                if (j != null) {
                    try {
                    } catch (Throwable th) {
                        throw th;
                    }
                } else if (x(context, "app_measurement_internal_disable_startup_flags")) {
                    j = Boolean.FALSE;
                } else {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
                    j = Boolean.valueOf(sharedPreferences.getBoolean(k, false));
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.remove(k);
                    edit.apply();
                }
            } catch (Exception e2) {
                Log.e("FA", "Exception reading flag from SharedPreferences.", e2);
                j = Boolean.FALSE;
            }
        }
    }

    @DexIgnore
    public static boolean X() {
        try {
            Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public static zs2 a(Context context) {
        return b(context, null, null, null, null);
    }

    @DexIgnore
    public static zs2 b(Context context, String str, String str2, String str3, Bundle bundle) {
        rc2.k(context);
        if (i == null) {
            synchronized (zs2.class) {
                try {
                    if (i == null) {
                        i = new zs2(context, str, str2, str3, bundle);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return i;
    }

    @DexIgnore
    public static boolean x(Context context, String str) {
        rc2.g(str);
        try {
            ApplicationInfo c2 = ag2.a(context).c(context.getPackageName(), 128);
            if (!(c2 == null || c2.metaData == null)) {
                return c2.metaData.getBoolean(str);
            }
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return false;
    }

    @DexIgnore
    public final List<Bundle> B(String str, String str2) {
        r93 r93 = new r93();
        k(new dt2(this, str, str2, r93));
        List<Bundle> list = (List) r93.e(r93.n(5000), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public final void D(String str) {
        k(new jt2(this, str));
    }

    @DexIgnore
    public final void E(String str, String str2, Bundle bundle) {
        k(new et2(this, str, str2, bundle));
    }

    @DexIgnore
    public final void F(boolean z) {
        k(new ut2(this, z));
    }

    @DexIgnore
    public final String I() {
        r93 r93 = new r93();
        k(new kt2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final void J(String str) {
        k(new lt2(this, str));
    }

    @DexIgnore
    public final int M(String str) {
        r93 r93 = new r93();
        k(new tt2(this, str, r93));
        Integer num = (Integer) r93.e(r93.n(ButtonService.CONNECT_TIMEOUT), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    @DexIgnore
    public final String N() {
        r93 r93 = new r93();
        k(new nt2(this, r93));
        return r93.i(50);
    }

    @DexIgnore
    public final long P() {
        r93 r93 = new r93();
        k(new mt2(this, r93));
        Long l = (Long) r93.e(r93.n(500), Long.class);
        if (l != null) {
            return l.longValue();
        }
        long nextLong = new Random(System.nanoTime() ^ this.b.b()).nextLong();
        int i2 = this.f + 1;
        this.f = i2;
        return nextLong + ((long) i2);
    }

    @DexIgnore
    public final String S() {
        r93 r93 = new r93();
        k(new pt2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final String U() {
        r93 r93 = new r93();
        k(new ot2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final t93 c(Context context, boolean z) {
        DynamiteModule.b bVar;
        if (z) {
            try {
                bVar = DynamiteModule.l;
            } catch (DynamiteModule.a e2) {
                o(e2, true, false);
                return null;
            }
        } else {
            bVar = DynamiteModule.j;
        }
        return s93.asInterface(DynamiteModule.e(context, bVar, ModuleDescriptor.MODULE_ID).d("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
    }

    @DexIgnore
    public final fg3 e() {
        return this.d;
    }

    @DexIgnore
    public final Map<String, Object> g(String str, String str2, boolean z) {
        r93 r93 = new r93();
        k(new st2(this, str, str2, z, r93));
        Bundle n = r93.n(5000);
        if (n == null || n.size() == 0) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(n.size());
        for (String str3 : n.keySet()) {
            Object obj = n.get(str3);
            if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                hashMap.put(str3, obj);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void h(int i2, String str, Object obj, Object obj2, Object obj3) {
        k(new rt2(this, false, 5, str, obj, null, null));
    }

    @DexIgnore
    public final void i(Activity activity, String str, String str2) {
        k(new ft2(this, activity, str, str2));
    }

    @DexIgnore
    public final void j(Bundle bundle) {
        k(new bt2(this, bundle));
    }

    @DexIgnore
    public final void k(a aVar) {
        this.c.execute(aVar);
    }

    @DexIgnore
    public final void n(sn3 sn3) {
        rc2.k(sn3);
        k(new vt2(this, sn3));
    }

    @DexIgnore
    public final void o(Exception exc, boolean z, boolean z2) {
        this.g |= z;
        if (z) {
            Log.w(this.f4521a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            h(5, "Error with data collection. Data lost.", exc, null, null);
        }
        Log.w(this.f4521a, "Error with data collection. Data lost.", exc);
    }

    @DexIgnore
    public final void p(String str) {
        k(new gt2(this, str));
    }

    @DexIgnore
    public final void q(String str, Bundle bundle) {
        t(null, str, bundle, false, true, null);
    }

    @DexIgnore
    public final void r(String str, String str2) {
        v(null, str, str2, false);
    }

    @DexIgnore
    public final void s(String str, String str2, Bundle bundle) {
        t(str, str2, bundle, true, true, null);
    }

    @DexIgnore
    public final void t(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l) {
        k(new xt2(this, l, str, str2, bundle, z, z2));
    }

    @DexIgnore
    public final void u(String str, String str2, Object obj) {
        v(str, str2, obj, true);
    }

    @DexIgnore
    public final void v(String str, String str2, Object obj, boolean z) {
        k(new wt2(this, str, str2, obj, z));
    }

    @DexIgnore
    public final void w(boolean z) {
        k(new ht2(this, z));
    }
}
