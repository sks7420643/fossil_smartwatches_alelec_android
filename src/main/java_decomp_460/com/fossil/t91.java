package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t91 extends Exception {
    @DexIgnore
    public /* final */ j91 networkResponse;
    @DexIgnore
    public long networkTimeMs;

    @DexIgnore
    public t91() {
        this.networkResponse = null;
    }

    @DexIgnore
    public t91(j91 j91) {
        this.networkResponse = j91;
    }

    @DexIgnore
    public t91(String str) {
        super(str);
        this.networkResponse = null;
    }

    @DexIgnore
    public t91(String str, Throwable th) {
        super(str, th);
        this.networkResponse = null;
    }

    @DexIgnore
    public t91(Throwable th) {
        super(th);
        this.networkResponse = null;
    }

    @DexIgnore
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }

    @DexIgnore
    public void setNetworkTimeMs(long j) {
        this.networkTimeMs = j;
    }
}
