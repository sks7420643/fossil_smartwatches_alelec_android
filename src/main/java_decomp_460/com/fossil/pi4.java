package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pi4 {
    @DexIgnore
    public static volatile pi4 b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<ri4> f2835a; // = new HashSet();

    @DexIgnore
    public static pi4 a() {
        pi4 pi4 = b;
        if (pi4 == null) {
            synchronized (pi4.class) {
                try {
                    pi4 = b;
                    if (pi4 == null) {
                        pi4 = new pi4();
                        b = pi4;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return pi4;
    }

    @DexIgnore
    public Set<ri4> b() {
        Set<ri4> unmodifiableSet;
        synchronized (this.f2835a) {
            unmodifiableSet = Collections.unmodifiableSet(this.f2835a);
        }
        return unmodifiableSet;
    }
}
