package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p96 implements Factory<o96> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<k97> f2805a;

    @DexIgnore
    public p96(Provider<k97> provider) {
        this.f2805a = provider;
    }

    @DexIgnore
    public static p96 a(Provider<k97> provider) {
        return new p96(provider);
    }

    @DexIgnore
    public static o96 c(k97 k97) {
        return new o96(k97);
    }

    @DexIgnore
    /* renamed from: b */
    public o96 get() {
        return c(this.f2805a.get());
    }
}
