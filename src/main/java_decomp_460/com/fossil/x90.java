package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class x90 extends Enum<x90> {
    @DexIgnore
    public static /* final */ x90 c;
    @DexIgnore
    public static /* final */ x90 d;
    @DexIgnore
    public static /* final */ /* synthetic */ x90[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        x90 x90 = new x90("CONFIGURATION_FILE", 0, (byte) 0);
        c = x90;
        x90 x902 = new x90("ELEMENT_CONFIGURATION_FILE", 1, (byte) 1);
        x90 x903 = new x90("LAUNCH_FILE", 2, (byte) 2);
        x90 x904 = new x90("LAUNCH_ELEMENT_FILE", 3, (byte) 3);
        x90 x905 = new x90("DECLARATION_FILE", 4, (byte) 4);
        x90 x906 = new x90("DECLARATION_ELEMENT_FILE", 5, (byte) 5);
        x90 x907 = new x90("CUSTOMIZATION_FILE", 6, (byte) 6);
        x90 x908 = new x90("CUSTOMIZATION_ELEMENT_FAME", 7, (byte) 7);
        x90 x909 = new x90("REMOTE_ACTIVITY", 8, (byte) 8);
        d = x909;
        e = new x90[]{x90, x902, x903, x904, x905, x906, x907, x908, x909};
    }
    */

    @DexIgnore
    public x90(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static x90 valueOf(String str) {
        return (x90) Enum.valueOf(x90.class, str);
    }

    @DexIgnore
    public static x90[] values() {
        return (x90[]) e.clone();
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
