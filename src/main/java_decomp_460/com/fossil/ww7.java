package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww7 extends ex7<xw7> {
    @DexIgnore
    public /* final */ rp7<Throwable, tl7> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rp7<? super java.lang.Throwable, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public ww7(xw7 xw7, rp7<? super Throwable, tl7> rp7) {
        super(xw7);
        this.f = rp7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        w(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "InvokeOnCompletion[" + ov7.a(this) + '@' + ov7.b(this) + ']';
    }

    @DexIgnore
    @Override // com.fossil.zu7
    public void w(Throwable th) {
        this.f.invoke(th);
    }
}
