package com.fossil;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class uf4 implements ft3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ vf4 f3579a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public uf4(vf4 vf4, Pair pair) {
        this.f3579a = vf4;
        this.b = pair;
    }

    @DexIgnore
    @Override // com.fossil.ft3
    public final Object then(nt3 nt3) {
        this.f3579a.b(this.b, nt3);
        return nt3;
    }
}
