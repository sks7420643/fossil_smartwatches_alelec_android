package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q97 implements Factory<p97> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<k97> f2946a;

    @DexIgnore
    public q97(Provider<k97> provider) {
        this.f2946a = provider;
    }

    @DexIgnore
    public static q97 a(Provider<k97> provider) {
        return new q97(provider);
    }

    @DexIgnore
    public static p97 c(k97 k97) {
        return new p97(k97);
    }

    @DexIgnore
    /* renamed from: b */
    public p97 get() {
        return c(this.f2946a.get());
    }
}
