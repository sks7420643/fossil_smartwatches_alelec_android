package com.fossil;

import android.os.Bundle;
import com.fossil.fc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w82 implements fc2.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ t82 f3896a;

    @DexIgnore
    public w82(t82 t82) {
        this.f3896a = t82;
    }

    @DexIgnore
    @Override // com.fossil.fc2.a
    public final boolean c() {
        return this.f3896a.n();
    }

    @DexIgnore
    @Override // com.fossil.fc2.a
    public final Bundle y() {
        return null;
    }
}
