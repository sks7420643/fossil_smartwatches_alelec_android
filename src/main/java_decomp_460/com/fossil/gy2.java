package com.fossil;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gy2<E> extends fy2<E> implements az2<E>, NavigableSet<E> {
    @DexIgnore
    public transient gy2<E> d;
    @DexIgnore
    public /* final */ transient Comparator<? super E> zza;

    @DexIgnore
    public gy2(Comparator<? super E> comparator) {
        this.zza = comparator;
    }

    @DexIgnore
    public static <E> vy2<E> zza(Comparator<? super E> comparator) {
        return ly2.zza.equals(comparator) ? (vy2<E>) vy2.zzb : new vy2<>(sx2.zza(), comparator);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) iy2.a((gy2) tailSet(e, true), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.az2
    public Comparator<? super E> comparator() {
        return this.zza;
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet descendingSet() {
        gy2<E> gy2 = this.d;
        if (gy2 != null) {
            return gy2;
        }
        gy2<E> zzi = zzi();
        this.d = zzi;
        zzi.d = this;
        return zzi;
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return (E) ((cz2) iterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) hy2.a((cz2) ((gy2) headSet(e, true)).descendingIterator(), null);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet headSet(Object obj, boolean z) {
        sw2.b(obj);
        return zza((gy2<E>) obj, z);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet headSet(Object obj) {
        return (gy2) headSet(obj, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) iy2.a((gy2) tailSet(e, false), null);
    }

    @DexIgnore
    @Override // com.fossil.tx2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.util.NavigableSet, com.fossil.ay2, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return iterator();
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return (E) ((cz2) descendingIterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) hy2.a((cz2) ((gy2) headSet(e, false)).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet subSet(Object obj, boolean z, Object obj2, boolean z2) {
        sw2.b(obj);
        sw2.b(obj2);
        if (this.zza.compare(obj, obj2) <= 0) {
            return zza(obj, z, obj2, z2);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet subSet(Object obj, Object obj2) {
        return (gy2) subSet(obj, true, obj2, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet tailSet(Object obj, boolean z) {
        sw2.b(obj);
        return zzb((gy2<E>) obj, z);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet tailSet(Object obj) {
        return (gy2) tailSet(obj, true);
    }

    @DexIgnore
    public final int zza(Object obj, Object obj2) {
        return this.zza.compare(obj, obj2);
    }

    @DexIgnore
    public abstract gy2<E> zza(E e, boolean z);

    @DexIgnore
    public abstract gy2<E> zza(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    public abstract gy2<E> zzb(E e, boolean z);

    @DexIgnore
    public abstract gy2<E> zzi();

    @DexIgnore
    /* renamed from: zzj */
    public abstract cz2<E> descendingIterator();
}
