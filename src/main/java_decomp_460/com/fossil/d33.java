package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d33 implements k23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ m23 f726a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public d33(m23 m23, String str, Object[] objArr) {
        this.f726a = m23;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < '\ud800') {
            this.d = charAt;
            return;
        }
        int i = charAt & '\u1fff';
        int i2 = 13;
        int i3 = 1;
        while (true) {
            char charAt2 = str.charAt(i3);
            if (charAt2 >= '\ud800') {
                i |= (charAt2 & '\u1fff') << i2;
                i2 += 13;
                i3++;
            } else {
                this.d = (charAt2 << i2) | i;
                return;
            }
        }
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object[] b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.k23
    public final int zza() {
        return (this.d & 1) == 1 ? e13.f.i : e13.f.j;
    }

    @DexIgnore
    @Override // com.fossil.k23
    public final boolean zzb() {
        return (this.d & 2) == 2;
    }

    @DexIgnore
    @Override // com.fossil.k23
    public final m23 zzc() {
        return this.f726a;
    }
}
