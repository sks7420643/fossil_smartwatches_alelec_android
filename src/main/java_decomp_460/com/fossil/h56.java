package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h56 implements Factory<d56> {
    @DexIgnore
    public static d56 a(f56 f56) {
        d56 c = f56.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
