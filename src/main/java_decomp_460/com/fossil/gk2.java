package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public SharedPreferences f1318a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public /* final */ Map<String, Object> c;

    @DexIgnore
    public gk2(Context context) {
        this(context, new ok2());
    }

    @DexIgnore
    public gk2(Context context, ok2 ok2) {
        this.c = new zi0();
        this.b = context;
        this.f1318a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        File file = new File(gl0.i(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    zj2.d(this.b, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("InstanceID/Store", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        return this.f1318a.getAll().isEmpty();
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this) {
            SharedPreferences.Editor edit = this.f1318a.edit();
            for (String str2 : this.f1318a.getAll().keySet()) {
                if (str2.startsWith(str)) {
                    edit.remove(str2);
                }
            }
            edit.commit();
        }
    }

    @DexIgnore
    public final void c(String str) {
        synchronized (this) {
            this.c.remove(str);
        }
        ok2.a(this.b, str);
        b(String.valueOf(str).concat("|"));
    }

    @DexIgnore
    public final void d() {
        synchronized (this) {
            this.c.clear();
            ok2.b(this.b);
            this.f1318a.edit().clear().commit();
        }
    }
}
