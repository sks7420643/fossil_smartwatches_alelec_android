package com.fossil;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ y18 f2618a;
    @DexIgnore
    public /* final */ d18 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public o18(y18 y18, d18 d18, List<Certificate> list, List<Certificate> list2) {
        this.f2618a = y18;
        this.b = d18;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static o18 b(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            d18 a2 = d18.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                y18 forJavaName = y18.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException e) {
                    certificateArr = null;
                }
                List u = certificateArr != null ? b28.u(certificateArr) : Collections.emptyList();
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                return new o18(forJavaName, a2, u, localCertificates != null ? b28.u(localCertificates) : Collections.emptyList());
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public static o18 c(y18 y18, d18 d18, List<Certificate> list, List<Certificate> list2) {
        if (y18 == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (d18 != null) {
            return new o18(y18, d18, b28.t(list), b28.t(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public d18 a() {
        return this.b;
    }

    @DexIgnore
    public List<Certificate> d() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof o18)) {
            return false;
        }
        o18 o18 = (o18) obj;
        return this.f2618a.equals(o18.f2618a) && this.b.equals(o18.b) && this.c.equals(o18.c) && this.d.equals(o18.d);
    }

    @DexIgnore
    public y18 f() {
        return this.f2618a;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.f2618a.hashCode() + 527) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }
}
