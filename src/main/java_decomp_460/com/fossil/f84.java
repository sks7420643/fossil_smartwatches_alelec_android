package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f84 implements b84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ m64 f1071a;

    @DexIgnore
    public f84(m64 m64) {
        this.f1071a = m64;
    }

    @DexIgnore
    @Override // com.fossil.b84
    public void a(String str, Bundle bundle) {
        this.f1071a.a("clx", str, bundle);
    }
}
