package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import com.facebook.GraphRequest;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.je4;
import com.fossil.jh4;
import com.fossil.kh4;
import com.fossil.vg4;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ih4 {
    @DexIgnore
    public static /* final */ Pattern d; // = Pattern.compile("[0-9]+s");
    @DexIgnore
    public static /* final */ Charset e; // = Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1628a;
    @DexIgnore
    public /* final */ ti4 b;
    @DexIgnore
    public /* final */ je4 c;

    @DexIgnore
    public ih4(Context context, ti4 ti4, je4 je4) {
        this.f1628a = context;
        this.b = ti4;
        this.c = je4;
    }

    @DexIgnore
    public static String a(String str, String str2, String str3) {
        String str4;
        if (TextUtils.isEmpty(str)) {
            str4 = "";
        } else {
            str4 = ", " + str;
        }
        return String.format("Firebase options used while communicating with Firebase server APIs: %s, %s%s", str2, str3, str4);
    }

    @DexIgnore
    public static JSONObject b(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("fid", str);
            jSONObject.put("appId", str2);
            jSONObject.put("authVersion", "FIS_v2");
            jSONObject.put("sdkVersion", "a:16.3.2");
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public static JSONObject c() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sdkVersion", "a:16.3.2");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Constants.INSTALLTION, jSONObject);
            return jSONObject2;
        } catch (JSONException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public static byte[] h(JSONObject jSONObject) throws IOException {
        return jSONObject.toString().getBytes("UTF-8");
    }

    @DexIgnore
    public static void i() {
        Log.e("Firebase-Installations", "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase.");
    }

    @DexIgnore
    public static void j(HttpURLConnection httpURLConnection, String str, String str2, String str3) {
        String n = n(httpURLConnection);
        if (!TextUtils.isEmpty(n)) {
            Log.w("Firebase-Installations", n);
            Log.w("Firebase-Installations", a(str, str2, str3));
        }
    }

    @DexIgnore
    public static long l(String str) {
        rc2.b(d.matcher(str).matches(), "Invalid Expiration Timestamp.");
        if (str == null || str.length() == 0) {
            return 0;
        }
        return Long.parseLong(str.substring(0, str.length() - 1));
    }

    @DexIgnore
    public static String n(HttpURLConnection httpURLConnection) {
        String str = null;
        InputStream errorStream = httpURLConnection.getErrorStream();
        if (errorStream != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(errorStream, e));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append('\n');
                }
                str = String.format("Error when communicating with the Firebase Installations server API. HTTP response: [%d %s: %s]", Integer.valueOf(httpURLConnection.getResponseCode()), httpURLConnection.getResponseMessage(), sb);
                try {
                    bufferedReader.close();
                } catch (IOException e2) {
                }
            } catch (IOException e3) {
                try {
                    bufferedReader.close();
                } catch (IOException e4) {
                }
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (IOException e5) {
                }
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static void r(URLConnection uRLConnection, byte[] bArr) throws IOException {
        OutputStream outputStream = uRLConnection.getOutputStream();
        if (outputStream != null) {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
            try {
                gZIPOutputStream.write(bArr);
            } finally {
                try {
                    gZIPOutputStream.close();
                    outputStream.close();
                } catch (IOException e2) {
                }
            }
        } else {
            throw new IOException("Cannot send request to FIS servers. No OutputStream available.");
        }
    }

    @DexIgnore
    public jh4 d(String str, String str2, String str3, String str4, String str5) throws IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations", str3)));
        while (i <= 1) {
            HttpURLConnection k = k(url, str);
            try {
                k.setRequestMethod("POST");
                k.setDoOutput(true);
                if (str5 != null) {
                    k.addRequestProperty("x-goog-fis-android-iid-migration-auth", str5);
                }
                p(k, str2, str4);
                int responseCode = k.getResponseCode();
                if (responseCode == 200) {
                    jh4 m = m(k);
                    k.disconnect();
                    return m;
                }
                j(k, str4, str, str3);
                if (responseCode == 429 || (responseCode >= 500 && responseCode < 600)) {
                    i++;
                    k.disconnect();
                } else {
                    i();
                    jh4.a a2 = jh4.a();
                    a2.e(jh4.b.BAD_CONFIG);
                    return a2.a();
                }
            } finally {
                k.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public void e(String str, String str2, String str3, String str4) throws k64, IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations/%s", str3, str2)));
        while (i <= 1) {
            HttpURLConnection k = k(url, str);
            try {
                k.setRequestMethod("DELETE");
                k.addRequestProperty("Authorization", "FIS_v2 " + str4);
                int responseCode = k.getResponseCode();
                if (responseCode == 200 || responseCode == 401 || responseCode == 404) {
                    k.disconnect();
                    return;
                }
                j(k, null, str, str3);
                if (responseCode == 429 || (responseCode >= 500 && responseCode < 600)) {
                    i++;
                } else {
                    i();
                    throw new vg4("Bad config while trying to delete FID", vg4.a.BAD_CONFIG);
                }
            } finally {
                k.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public kh4 f(String str, String str2, String str3, String str4) throws IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations/%s/authTokens:generate", str3, str2)));
        while (i <= 1) {
            HttpURLConnection k = k(url, str);
            try {
                k.setRequestMethod("POST");
                k.addRequestProperty("Authorization", "FIS_v2 " + str4);
                q(k);
                int responseCode = k.getResponseCode();
                if (responseCode == 200) {
                    kh4 o = o(k);
                    k.disconnect();
                    return o;
                }
                j(k, null, str, str3);
                if (responseCode == 401 || responseCode == 404) {
                    kh4.a a2 = kh4.a();
                    a2.b(kh4.b.AUTH_ERROR);
                    kh4 a3 = a2.a();
                    k.disconnect();
                    return a3;
                } else if (responseCode == 429 || (responseCode >= 500 && responseCode < 600)) {
                    i++;
                    k.disconnect();
                } else {
                    i();
                    kh4.a a4 = kh4.a();
                    a4.b(kh4.b.BAD_CONFIG);
                    return a4.a();
                }
            } finally {
                k.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public final String g() {
        try {
            byte[] a2 = af2.a(this.f1628a, this.f1628a.getPackageName());
            if (a2 != null) {
                return jf2.b(a2, false);
            }
            Log.e("ContentValues", "Could not get fingerprint hash for package: " + this.f1628a.getPackageName());
            return null;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("ContentValues", "No such package: " + this.f1628a.getPackageName(), e2);
            return null;
        }
    }

    @DexIgnore
    public final HttpURLConnection k(URL url, String str) throws IOException {
        je4.a a2;
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.addRequestProperty("Content-Type", com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        httpURLConnection.addRequestProperty(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        httpURLConnection.addRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.addRequestProperty(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, "no-cache");
        httpURLConnection.addRequestProperty("X-Android-Package", this.f1628a.getPackageName());
        je4 je4 = this.c;
        if (!(je4 == null || this.b == null || (a2 = je4.a("fire-installations-id")) == je4.a.NONE)) {
            httpURLConnection.addRequestProperty("x-firebase-client", this.b.a());
            httpURLConnection.addRequestProperty("x-firebase-client-log-type", Integer.toString(a2.getCode()));
        }
        httpURLConnection.addRequestProperty("X-Android-Cert", g());
        httpURLConnection.addRequestProperty("x-goog-api-key", str);
        return httpURLConnection;
    }

    @DexIgnore
    public final jh4 m(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, e));
        kh4.a a2 = kh4.a();
        jh4.a a3 = jh4.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("name")) {
                a3.f(jsonReader.nextString());
            } else if (nextName.equals("fid")) {
                a3.c(jsonReader.nextString());
            } else if (nextName.equals(Constants.PROFILE_KEY_REFRESH_TOKEN)) {
                a3.d(jsonReader.nextString());
            } else if (nextName.equals("authToken")) {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    String nextName2 = jsonReader.nextName();
                    if (nextName2.equals("token")) {
                        a2.c(jsonReader.nextString());
                    } else if (nextName2.equals("expiresIn")) {
                        a2.d(l(jsonReader.nextString()));
                    } else {
                        jsonReader.skipValue();
                    }
                }
                a3.b(a2.a());
                jsonReader.endObject();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        a3.e(jh4.b.OK);
        return a3.a();
    }

    @DexIgnore
    public final kh4 o(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, e));
        kh4.a a2 = kh4.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("token")) {
                a2.c(jsonReader.nextString());
            } else if (nextName.equals("expiresIn")) {
                a2.d(l(jsonReader.nextString()));
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        a2.b(kh4.b.OK);
        return a2.a();
    }

    @DexIgnore
    public final void p(HttpURLConnection httpURLConnection, String str, String str2) throws IOException {
        r(httpURLConnection, h(b(str, str2)));
    }

    @DexIgnore
    public final void q(HttpURLConnection httpURLConnection) throws IOException {
        r(httpURLConnection, h(c()));
    }
}
