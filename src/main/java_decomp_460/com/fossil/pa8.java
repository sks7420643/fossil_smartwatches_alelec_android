package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.qa8;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa8 implements qa8 {
    @DexIgnore
    public static /* final */ Uri b; // = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    @DexIgnore
    public static /* final */ Uri c; // = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    @DexIgnore
    public static /* final */ ja8 d; // = new ja8();
    @DexIgnore
    public static /* final */ String[] e; // = {"longitude", "latitude"};
    @DexIgnore
    public static /* final */ pa8 f; // = new pa8();

    @DexIgnore
    @Override // com.fossil.qa8
    public void a(Context context, ka8 ka8, byte[] bArr) {
        pq7.c(context, "context");
        pq7.c(ka8, "asset");
        pq7.c(bArr, "byteArray");
        throw new bl7("An operation is not implemented: not implemented");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public boolean b(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        return qa8.b.b(this, context, str);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public List<ma8> c(Context context, int i, long j, la8 la8) {
        pq7.c(context, "context");
        pq7.c(la8, "option");
        ArrayList arrayList = new ArrayList();
        Uri n = n();
        String[] strArr = (String[]) dm7.s(qa8.f2953a.a(), new String[]{"count(1)"});
        ArrayList<String> arrayList2 = new ArrayList<>();
        String q = q(i, la8, arrayList2);
        arrayList2.add(String.valueOf(j));
        String str = "bucket_id IS NOT NULL " + q + " AND datetaken <= ? " + na8.e.y(Integer.valueOf(i)) + ") GROUP BY (bucket_id";
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(n, strArr, str, (String[]) array, null);
            if (query == null) {
                return hm7.e();
            }
            pq7.b(query, "context.contentResolver.\u2026    ?: return emptyList()");
            while (query.moveToNext()) {
                String string = query.getString(0);
                String string2 = query.getString(1);
                int i2 = query.getInt(2);
                pq7.b(string, "id");
                pq7.b(string2, "name");
                arrayList.add(new ma8(string, string2, i2, 0, false, 16, null));
            }
            query.close();
            return arrayList;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public List<String> d(Context context, List<String> list) {
        pq7.c(context, "context");
        pq7.c(list, "ids");
        return qa8.b.a(this, context, list);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public List<ka8> e(Context context, String str, int i, int i2, int i3, long j, la8 la8) {
        String str2;
        pq7.c(context, "context");
        pq7.c(str, "gId");
        pq7.c(la8, "option");
        ja8 ja8 = d;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri n = n();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String q = q(i3, la8, arrayList2);
        arrayList2.add(String.valueOf(j));
        String y = na8.e.y(Integer.valueOf(i3));
        Object[] array = em7.D(dm7.s(dm7.s(dm7.s(qa8.f2953a.b(), qa8.f2953a.c()), qa8.f2953a.d()), e)).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + q + " AND datetaken <= ? " + y;
            } else {
                str2 = "bucket_id = ? " + q + " AND datetaken <= ? " + y;
            }
            String str3 = "datetaken DESC LIMIT " + (i2 - i) + " OFFSET " + i;
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(n, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return hm7.e();
                }
                pq7.b(query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String v = v(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String v2 = v(query, "_data");
                    long t = t(query, "datetaken");
                    int s = s(query, MessengerShareContentUtility.MEDIA_TYPE);
                    long t2 = i3 == 1 ? 0 : t(query, "duration");
                    int s2 = s(query, "width");
                    int s3 = s(query, "height");
                    String name = new File(v2).getName();
                    long t3 = t(query, "date_modified");
                    double r = r(query, "latitude");
                    double r2 = r(query, "longitude");
                    int u = u(s);
                    pq7.b(name, "displayName");
                    ka8 ka8 = new ka8(v, v2, t2, t, s2, s3, u, name, t3);
                    if (r != 0.0d) {
                        ka8.l(Double.valueOf(r));
                    }
                    if (r2 != 0.0d) {
                        ka8.m(Double.valueOf(r2));
                    }
                    arrayList.add(ka8);
                    ja8.c(ka8);
                }
                query.close();
                return arrayList;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public byte[] f(Context context, ka8 ka8, boolean z) {
        pq7.c(context, "context");
        pq7.c(ka8, "asset");
        throw new bl7("An operation is not implemented: not implemented");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public Bitmap g(Context context, String str, int i, int i2, Integer num) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        throw new bl7("An operation is not implemented: not implemented");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public String h(Context context, String str, boolean z) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        ka8 j = j(context, str);
        if (j != null) {
            return j.i();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public ka8 i(Context context, byte[] bArr, String str, String str2) {
        pq7.c(context, "context");
        pq7.c(bArr, "image");
        pq7.c(str, "title");
        pq7.c(str2, Constants.DESC);
        return j(context, String.valueOf(ContentUris.parseId(Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), BitmapFactory.decodeByteArray(bArr, 0, bArr.length), str, str2)))));
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public ka8 j(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        ka8 b2 = d.b(str);
        if (b2 != null) {
            return b2;
        }
        Object[] array = em7.D(dm7.s(dm7.s(dm7.s(qa8.f2953a.b(), qa8.f2953a.c()), e), qa8.f2953a.d())).toArray(new String[0]);
        if (array != null) {
            Cursor query = context.getContentResolver().query(n(), (String[]) array, "_id = ?", new String[]{str}, null);
            if (query != null) {
                pq7.b(query, "context.contentResolver.\u2026           ?: return null");
                if (query.moveToNext()) {
                    String v = v(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String v2 = v(query, "_data");
                    long t = t(query, "datetaken");
                    int s = s(query, MessengerShareContentUtility.MEDIA_TYPE);
                    long t2 = s == 1 ? 0 : t(query, "duration");
                    int s2 = s(query, "width");
                    int s3 = s(query, "height");
                    String name = new File(v2).getName();
                    long t3 = t(query, "date_modified");
                    double r = r(query, "latitude");
                    double r2 = r(query, "longitude");
                    int u = u(s);
                    pq7.b(name, "displayName");
                    ka8 ka8 = new ka8(v, v2, t2, t, s2, s3, u, name, t3);
                    if (r != 0.0d) {
                        ka8.l(Double.valueOf(r));
                    }
                    if (r2 != 0.0d) {
                        ka8.m(Double.valueOf(r2));
                    }
                    ka8.l(Double.valueOf(r));
                    ka8.m(Double.valueOf(r2));
                    d.c(ka8);
                    query.close();
                    return ka8;
                }
                query.close();
            }
            return null;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x009e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x009f, code lost:
        com.fossil.so7.a(r10, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a2, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00a6, code lost:
        com.fossil.so7.a(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a9, code lost:
        throw r1;
     */
    @DexIgnore
    @Override // com.fossil.qa8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.ka8 k(android.content.Context r9, java.io.InputStream r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            r1 = 0
            java.lang.String r0 = "context"
            com.fossil.pq7.c(r9, r0)
            java.lang.String r0 = "inputStream"
            com.fossil.pq7.c(r10, r0)
            java.lang.String r0 = "title"
            com.fossil.pq7.c(r11, r0)
            java.lang.String r0 = "desc"
            com.fossil.pq7.c(r12, r0)
            android.content.ContentResolver r2 = r9.getContentResolver()
            long r4 = java.lang.System.currentTimeMillis()
            r0 = 1000(0x3e8, float:1.401E-42)
            long r6 = (long) r0
            long r4 = r4 / r6
            java.lang.String r0 = java.net.URLConnection.guessContentTypeFromStream(r10)
            if (r0 != 0) goto L_0x0041
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "video/"
            r0.append(r3)
            java.io.File r3 = new java.io.File
            r3.<init>(r11)
            java.lang.String r3 = com.fossil.cp7.f(r3)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
        L_0x0041:
            android.net.Uri r3 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "_display_name"
            r6.put(r7, r11)
            java.lang.String r7 = "mime_type"
            r6.put(r7, r0)
            java.lang.String r0 = "title"
            r6.put(r0, r11)
            java.lang.String r0 = "description"
            r6.put(r0, r12)
            java.lang.String r0 = "date_added"
            java.lang.Long r7 = java.lang.Long.valueOf(r4)
            r6.put(r0, r7)
            java.lang.String r0 = "date_modified"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r6.put(r0, r4)
            android.net.Uri r0 = r2.insert(r3, r6)
            if (r0 == 0) goto L_0x00aa
            java.lang.String r3 = "cr.insert(uri, values) ?: return null"
            com.fossil.pq7.b(r0, r3)
            java.io.OutputStream r3 = r2.openOutputStream(r0)
            if (r3 == 0) goto L_0x008c
            r4 = 0
            r5 = 2
            r6 = 0
            com.fossil.ro7.b(r10, r3, r4, r5, r6)     // Catch:{ all -> 0x009c }
            r4 = 0
            com.fossil.so7.a(r10, r4)     // Catch:{ all -> 0x00a3 }
            com.fossil.so7.a(r3, r1)
        L_0x008c:
            long r4 = android.content.ContentUris.parseId(r0)
            r2.notifyChange(r0, r1)
            java.lang.String r0 = java.lang.String.valueOf(r4)
            com.fossil.ka8 r0 = r8.j(r9, r0)
        L_0x009b:
            return r0
        L_0x009c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x009e }
        L_0x009e:
            r1 = move-exception
            com.fossil.so7.a(r10, r0)
            throw r1
        L_0x00a3:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a5 }
        L_0x00a5:
            r1 = move-exception
            com.fossil.so7.a(r3, r0)
            throw r1
        L_0x00aa:
            r0 = r1
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pa8.k(android.content.Context, java.io.InputStream, java.lang.String, java.lang.String):com.fossil.ka8");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public void l() {
        d.a();
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public eq0 m(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "id");
        ka8 j = j(context, str);
        if (j != null) {
            return new eq0(j.i());
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public Uri n() {
        return qa8.b.c(this);
    }

    @DexIgnore
    @Override // com.fossil.qa8
    public ma8 o(Context context, String str, int i, long j, la8 la8) {
        pq7.c(context, "context");
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        Uri n = n();
        String[] strArr = (String[]) dm7.s(qa8.f2953a.a(), new String[]{"count(1)"});
        ArrayList<String> arrayList = new ArrayList<>();
        String q = q(i, la8, arrayList);
        arrayList.add(String.valueOf(j));
        String str2 = "";
        if (!pq7.a(str, "")) {
            arrayList.add(str);
            str2 = "AND bucket_id = ?";
        }
        String str3 = "bucket_id IS NOT NULL " + q + " AND datetaken <= ? " + str2 + ' ' + na8.e.y(null) + ") GROUP BY (bucket_id";
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(n, strArr, str3, (String[]) array, null);
            if (query == null) {
                return null;
            }
            pq7.b(query, "context.contentResolver.\u2026           ?: return null");
            if (query.moveToNext()) {
                String string = query.getString(0);
                String string2 = query.getString(1);
                int i2 = query.getInt(2);
                query.close();
                pq7.b(string, "id");
                pq7.b(string2, "name");
                return new ma8(string, string2, i2, 0, false, 16, null);
            }
            query.close();
            return null;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.qa8
    @SuppressLint({"Recycle"})
    public List<ka8> p(Context context, String str, int i, int i2, int i3, long j, la8 la8, ja8 ja8) {
        String str2;
        pq7.c(context, "context");
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        if (ja8 == null) {
            ja8 = d;
        }
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri n = n();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String q = q(i3, la8, arrayList2);
        arrayList2.add(String.valueOf(j));
        String y = na8.e.y(Integer.valueOf(i3));
        Object[] array = em7.D(dm7.s(dm7.s(dm7.s(qa8.f2953a.b(), qa8.f2953a.c()), qa8.f2953a.d()), e)).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + q + " AND datetaken <= ? " + y;
            } else {
                str2 = "bucket_id = ? " + q + " AND datetaken <= ? " + y;
            }
            String str3 = "datetaken DESC LIMIT " + i2 + " OFFSET " + (i2 * i);
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(n, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return hm7.e();
                }
                pq7.b(query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String v = v(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String v2 = v(query, "_data");
                    long t = t(query, "datetaken");
                    long t2 = t(query, "date_modified");
                    int s = s(query, MessengerShareContentUtility.MEDIA_TYPE);
                    long t3 = i3 == 1 ? 0 : t(query, "duration");
                    int s2 = s(query, "width");
                    int s3 = s(query, "height");
                    String name = new File(v2).getName();
                    double r = r(query, "latitude");
                    double r2 = r(query, "longitude");
                    int u = u(s);
                    pq7.b(name, "displayName");
                    ka8 ka8 = new ka8(v, v2, t3, t, s2, s3, u, name, t2);
                    if (r != 0.0d) {
                        ka8.l(Double.valueOf(r));
                    }
                    if (r2 != 0.0d) {
                        ka8.m(Double.valueOf(r2));
                    }
                    arrayList.add(ka8);
                    ja8.c(ka8);
                }
                query.close();
                return arrayList;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public String q(int i, la8 la8, ArrayList<String> arrayList) {
        pq7.c(la8, "filterOptions");
        pq7.c(arrayList, "args");
        return qa8.b.e(this, i, la8, arrayList);
    }

    @DexIgnore
    public double r(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getDouble");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.f(this, cursor, str);
    }

    @DexIgnore
    public int s(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getInt");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.g(this, cursor, str);
    }

    @DexIgnore
    public long t(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getLong");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.h(this, cursor, str);
    }

    @DexIgnore
    public int u(int i) {
        return qa8.b.i(this, i);
    }

    @DexIgnore
    public String v(Cursor cursor, String str) {
        pq7.c(cursor, "$this$getString");
        pq7.c(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return qa8.b.j(this, cursor, str);
    }
}
