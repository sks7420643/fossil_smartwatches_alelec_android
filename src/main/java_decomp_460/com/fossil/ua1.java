package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.xb1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ua1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ cf1 f3557a;
    @DexIgnore
    public /* final */ si1 b;
    @DexIgnore
    public /* final */ wi1 c;
    @DexIgnore
    public /* final */ xi1 d;
    @DexIgnore
    public /* final */ yb1 e;
    @DexIgnore
    public /* final */ uh1 f;
    @DexIgnore
    public /* final */ ti1 g;
    @DexIgnore
    public /* final */ vi1 h; // = new vi1();
    @DexIgnore
    public /* final */ ui1 i; // = new ui1();
    @DexIgnore
    public /* final */ mn0<List<Throwable>> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RuntimeException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a {
        @DexIgnore
        public b() {
            super("Failed to find image header parser.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends a {
        @DexIgnore
        public c(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }

        @DexIgnore
        public c(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        @DexIgnore
        public <M> c(M m, List<af1<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + ((Object) m));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends a {
        @DexIgnore
        public d(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a {
        @DexIgnore
        public e(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    @DexIgnore
    public ua1() {
        mn0<List<Throwable>> e2 = kk1.e();
        this.j = e2;
        this.f3557a = new cf1(e2);
        this.b = new si1();
        this.c = new wi1();
        this.d = new xi1();
        this.e = new yb1();
        this.f = new uh1();
        this.g = new ti1();
        r(Arrays.asList("Gif", "Bitmap", "BitmapDrawable"));
    }

    @DexIgnore
    public <Data> ua1 a(Class<Data> cls, jb1<Data> jb1) {
        this.b.a(cls, jb1);
        return this;
    }

    @DexIgnore
    public <TResource> ua1 b(Class<TResource> cls, rb1<TResource> rb1) {
        this.d.a(cls, rb1);
        return this;
    }

    @DexIgnore
    public <Data, TResource> ua1 c(Class<Data> cls, Class<TResource> cls2, qb1<Data, TResource> qb1) {
        e("legacy_append", cls, cls2, qb1);
        return this;
    }

    @DexIgnore
    public <Model, Data> ua1 d(Class<Model> cls, Class<Data> cls2, bf1<Model, Data> bf1) {
        this.f3557a.a(cls, cls2, bf1);
        return this;
    }

    @DexIgnore
    public <Data, TResource> ua1 e(String str, Class<Data> cls, Class<TResource> cls2, qb1<Data, TResource> qb1) {
        this.c.a(str, qb1, cls, cls2);
        return this;
    }

    @DexIgnore
    public final <Data, TResource, Transcode> List<vc1<Data, TResource, Transcode>> f(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.c.d(cls, cls2)) {
            for (Class cls5 : this.f.b(cls4, cls3)) {
                arrayList.add(new vc1(cls, cls4, cls5, this.c.b(cls, cls4), this.f.a(cls4, cls5), this.j));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ImageHeaderParser> g() {
        List<ImageHeaderParser> b2 = this.g.b();
        if (!b2.isEmpty()) {
            return b2;
        }
        throw new b();
    }

    @DexIgnore
    public <Data, TResource, Transcode> gd1<Data, TResource, Transcode> h(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        gd1<Data, TResource, Transcode> gd1 = null;
        gd1<Data, TResource, Transcode> a2 = this.i.a(cls, cls2, cls3);
        if (this.i.c(a2)) {
            return null;
        }
        if (a2 != null) {
            return a2;
        }
        List<vc1<Data, TResource, Transcode>> f2 = f(cls, cls2, cls3);
        if (!f2.isEmpty()) {
            gd1 = new gd1<>(cls, cls2, cls3, f2, this.j);
        }
        this.i.d(cls, cls2, cls3, gd1);
        return gd1;
    }

    @DexIgnore
    public <Model> List<af1<Model, ?>> i(Model model) {
        return this.f3557a.d(model);
    }

    @DexIgnore
    public <Model, TResource, Transcode> List<Class<?>> j(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2, cls3);
        if (a2 != null) {
            return a2;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<?> cls4 : this.f3557a.c(cls)) {
            for (Class cls5 : this.c.d(cls4, cls2)) {
                if (!this.f.b(cls5, cls3).isEmpty() && !arrayList.contains(cls5)) {
                    arrayList.add(cls5);
                }
            }
        }
        this.h.b(cls, cls2, cls3, Collections.unmodifiableList(arrayList));
        return arrayList;
    }

    @DexIgnore
    public <X> rb1<X> k(id1<X> id1) throws d {
        rb1<X> b2 = this.d.b(id1.d());
        if (b2 != null) {
            return b2;
        }
        throw new d(id1.d());
    }

    @DexIgnore
    public <X> xb1<X> l(X x) {
        return this.e.a(x);
    }

    @DexIgnore
    public <X> jb1<X> m(X x) throws e {
        jb1<X> b2 = this.b.b(x.getClass());
        if (b2 != null) {
            return b2;
        }
        throw new e(x.getClass());
    }

    @DexIgnore
    public boolean n(id1<?> id1) {
        return this.d.b(id1.d()) != null;
    }

    @DexIgnore
    public ua1 o(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    @DexIgnore
    public ua1 p(xb1.a<?> aVar) {
        this.e.b(aVar);
        return this;
    }

    @DexIgnore
    public <TResource, Transcode> ua1 q(Class<TResource> cls, Class<Transcode> cls2, th1<TResource, Transcode> th1) {
        this.f.c(cls, cls2, th1);
        return this;
    }

    @DexIgnore
    public final ua1 r(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.e(arrayList);
        return this;
    }
}
