package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<qe3> CREATOR; // = new gf3();
    @DexIgnore
    public /* final */ List<LatLng> b;
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public float e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public ce3 i;
    @DexIgnore
    public ce3 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public List<me3> l;

    @DexIgnore
    public qe3() {
        this.c = 10.0f;
        this.d = -16777216;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = true;
        this.g = false;
        this.h = false;
        this.i = new be3();
        this.j = new be3();
        this.k = 0;
        this.l = null;
        this.b = new ArrayList();
    }

    @DexIgnore
    public qe3(List list, float f2, int i2, float f3, boolean z, boolean z2, boolean z3, ce3 ce3, ce3 ce32, int i3, List<me3> list2) {
        this.c = 10.0f;
        this.d = -16777216;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = true;
        this.g = false;
        this.h = false;
        this.i = new be3();
        this.j = new be3();
        this.k = 0;
        this.l = null;
        this.b = list;
        this.c = f2;
        this.d = i2;
        this.e = f3;
        this.f = z;
        this.g = z2;
        this.h = z3;
        if (ce3 != null) {
            this.i = ce3;
        }
        if (ce32 != null) {
            this.j = ce32;
        }
        this.k = i3;
        this.l = list2;
    }

    @DexIgnore
    public final qe3 A(boolean z) {
        this.g = z;
        return this;
    }

    @DexIgnore
    public final qe3 A0(float f2) {
        this.c = f2;
        return this;
    }

    @DexIgnore
    public final qe3 B0(float f2) {
        this.e = f2;
        return this;
    }

    @DexIgnore
    public final int D() {
        return this.d;
    }

    @DexIgnore
    public final ce3 F() {
        return this.j;
    }

    @DexIgnore
    public final int L() {
        return this.k;
    }

    @DexIgnore
    public final qe3 c(Iterable<LatLng> iterable) {
        for (LatLng latLng : iterable) {
            this.b.add(latLng);
        }
        return this;
    }

    @DexIgnore
    public final qe3 f(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final qe3 h(int i2) {
        this.d = i2;
        return this;
    }

    @DexIgnore
    public final qe3 k(ce3 ce3) {
        rc2.l(ce3, "endCap must not be null");
        this.j = ce3;
        return this;
    }

    @DexIgnore
    public final List<me3> o0() {
        return this.l;
    }

    @DexIgnore
    public final List<LatLng> p0() {
        return this.b;
    }

    @DexIgnore
    public final ce3 q0() {
        return this.i;
    }

    @DexIgnore
    public final float r0() {
        return this.c;
    }

    @DexIgnore
    public final float s0() {
        return this.e;
    }

    @DexIgnore
    public final boolean t0() {
        return this.h;
    }

    @DexIgnore
    public final boolean u0() {
        return this.g;
    }

    @DexIgnore
    public final boolean v0() {
        return this.f;
    }

    @DexIgnore
    public final qe3 w0(int i2) {
        this.k = i2;
        return this;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.y(parcel, 2, p0(), false);
        bd2.j(parcel, 3, r0());
        bd2.n(parcel, 4, D());
        bd2.j(parcel, 5, s0());
        bd2.c(parcel, 6, v0());
        bd2.c(parcel, 7, u0());
        bd2.c(parcel, 8, t0());
        bd2.t(parcel, 9, q0(), i2, false);
        bd2.t(parcel, 10, F(), i2, false);
        bd2.n(parcel, 11, L());
        bd2.y(parcel, 12, o0(), false);
        bd2.b(parcel, a2);
    }

    @DexIgnore
    public final qe3 x0(List<me3> list) {
        this.l = list;
        return this;
    }

    @DexIgnore
    public final qe3 y0(ce3 ce3) {
        rc2.l(ce3, "startCap must not be null");
        this.i = ce3;
        return this;
    }

    @DexIgnore
    public final qe3 z0(boolean z) {
        this.f = z;
        return this;
    }
}
