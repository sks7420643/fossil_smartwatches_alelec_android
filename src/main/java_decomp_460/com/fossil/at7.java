package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class at7 extends zs7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterable<T>, jr7 {
        @DexIgnore
        public /* final */ /* synthetic */ ts7 b;

        @DexIgnore
        public a(ts7 ts7) {
            this.b = ts7;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return this.b.iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<T, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'boolean' to match base method */
        @Override // com.fossil.rp7
        public final Boolean invoke(T t) {
            return t == null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements vp7<T, R, cl7<? extends T, ? extends R>> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(2);
        }

        @DexIgnore
        @Override // com.fossil.vp7
        public final cl7<T, R> invoke(T t, R r) {
            return hl7.a(t, r);
        }
    }

    @DexIgnore
    public static final <T> Iterable<T> e(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$asIterable");
        return new a(ts7);
    }

    @DexIgnore
    public static final <T> boolean f(ts7<? extends T> ts7, T t) {
        pq7.c(ts7, "$this$contains");
        return k(ts7, t) >= 0;
    }

    @DexIgnore
    public static final <T> int g(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$count");
        Iterator<? extends T> it = ts7.iterator();
        int i = 0;
        while (it.hasNext()) {
            it.next();
            i++;
            if (i < 0) {
                hm7.k();
                throw null;
            }
        }
        return i;
    }

    @DexIgnore
    public static final <T> ts7<T> h(ts7<? extends T> ts7, rp7<? super T, Boolean> rp7) {
        pq7.c(ts7, "$this$filter");
        pq7.c(rp7, "predicate");
        return new qs7(ts7, true, rp7);
    }

    @DexIgnore
    public static final <T> ts7<T> i(ts7<? extends T> ts7, rp7<? super T, Boolean> rp7) {
        pq7.c(ts7, "$this$filterNot");
        pq7.c(rp7, "predicate");
        return new qs7(ts7, false, rp7);
    }

    @DexIgnore
    public static final <T> ts7<T> j(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$filterNotNull");
        ts7<T> i = i(ts7, b.INSTANCE);
        if (i != null) {
            return i;
        }
        throw new il7("null cannot be cast to non-null type kotlin.sequences.Sequence<T>");
    }

    @DexIgnore
    public static final <T> int k(ts7<? extends T> ts7, T t) {
        pq7.c(ts7, "$this$indexOf");
        int i = 0;
        for (Object obj : ts7) {
            if (i < 0) {
                hm7.l();
                throw null;
            } else if (pq7.a(t, obj)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T, A extends Appendable> A l(ts7<? extends T> ts7, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, rp7<? super T, ? extends CharSequence> rp7) {
        pq7.c(ts7, "$this$joinTo");
        pq7.c(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        pq7.c(charSequence, "separator");
        pq7.c(charSequence2, "prefix");
        pq7.c(charSequence3, "postfix");
        pq7.c(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object obj : ts7) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            nt7.a(a2, obj, rp7);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static final <T> String m(ts7<? extends T> ts7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, rp7<? super T, ? extends CharSequence> rp7) {
        pq7.c(ts7, "$this$joinToString");
        pq7.c(charSequence, "separator");
        pq7.c(charSequence2, "prefix");
        pq7.c(charSequence3, "postfix");
        pq7.c(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        l(ts7, sb, charSequence, charSequence2, charSequence3, i, charSequence4, rp7);
        String sb2 = sb.toString();
        pq7.b(sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }

    @DexIgnore
    public static /* synthetic */ String n(ts7 ts7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, rp7 rp7, int i2, Object obj) {
        String str = (i2 & 1) != 0 ? ", " : charSequence;
        CharSequence charSequence5 = "";
        String str2 = (i2 & 2) != 0 ? "" : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        return m(ts7, str, str2, charSequence5, (i2 & 8) != 0 ? -1 : i, (i2 & 16) != 0 ? "..." : charSequence4, (i2 & 32) != 0 ? null : rp7);
    }

    @DexIgnore
    public static final <T, R> ts7<R> o(ts7<? extends T> ts7, rp7<? super T, ? extends R> rp7) {
        pq7.c(ts7, "$this$map");
        pq7.c(rp7, "transform");
        return new bt7(ts7, rp7);
    }

    @DexIgnore
    public static final <T, R> ts7<R> p(ts7<? extends T> ts7, rp7<? super T, ? extends R> rp7) {
        pq7.c(ts7, "$this$mapNotNull");
        pq7.c(rp7, "transform");
        return j(new bt7(ts7, rp7));
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T q(com.fossil.ts7<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$max"
            com.fossil.pq7.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 >= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.at7.q(com.fossil.ts7):java.lang.Comparable");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T r(com.fossil.ts7<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$min"
            com.fossil.pq7.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 <= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.at7.r(com.fossil.ts7):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C s(ts7<? extends T> ts7, C c2) {
        pq7.c(ts7, "$this$toCollection");
        pq7.c(c2, ShareConstants.DESTINATION);
        Iterator<? extends T> it = ts7.iterator();
        while (it.hasNext()) {
            c2.add(it.next());
        }
        return c2;
    }

    @DexIgnore
    public static final <T> List<T> t(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$toList");
        return hm7.j(u(ts7));
    }

    @DexIgnore
    public static final <T> List<T> u(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$toMutableList");
        ArrayList arrayList = new ArrayList();
        s(ts7, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T, R> ts7<cl7<T, R>> v(ts7<? extends T> ts7, ts7<? extends R> ts72) {
        pq7.c(ts7, "$this$zip");
        pq7.c(ts72, FacebookRequestErrorClassification.KEY_OTHER);
        return new ss7(ts7, ts72, c.INSTANCE);
    }
}
