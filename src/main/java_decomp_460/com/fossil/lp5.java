package com.fossil;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lp5 extends BaseProvider {
    @DexIgnore
    boolean b();

    @DexIgnore
    List<MicroAppSetting> c();

    @DexIgnore
    List<MicroAppSetting> getPendingMicroAppSettings();

    @DexIgnore
    boolean h(MicroAppSetting microAppSetting);

    @DexIgnore
    void l(String str, int i);

    @DexIgnore
    MicroAppSetting n(String str);
}
