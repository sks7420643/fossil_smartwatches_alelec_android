package com.fossil;

import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww2 {
    @DexIgnore
    public static <T> xw2<T> a(xw2<T> xw2) {
        return ((xw2 instanceof yw2) || (xw2 instanceof zw2)) ? xw2 : xw2 instanceof Serializable ? new zw2(xw2) : new yw2(xw2);
    }

    @DexIgnore
    public static <T> xw2<T> b(@NullableDecl T t) {
        return new bx2(t);
    }
}
