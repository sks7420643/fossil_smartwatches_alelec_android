package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq1 extends lq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gq1 createFromParcel(Parcel parcel) {
            return new gq1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gq1[] newArray(int i) {
            return new gq1[i];
        }
    }

    @DexIgnore
    public gq1(byte b, bv1 bv1) {
        super(np1.COMMUTE_TIME_ETA_MICRO_APP, b, bv1);
    }

    @DexIgnore
    public /* synthetic */ gq1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }
}
