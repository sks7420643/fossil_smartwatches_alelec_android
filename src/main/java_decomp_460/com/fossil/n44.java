package com.fossil;

import com.fossil.b34;
import com.fossil.c34;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n44<K, V> extends t24<K, V> {
    @DexIgnore
    public static /* final */ n44<Object, Object> EMPTY; // = new n44<>(null, null, a34.EMPTY_ENTRY_ARRAY, 0, 0);
    @DexIgnore
    public static /* final */ double MAX_LOAD_FACTOR; // = 1.2d;
    @DexIgnore
    public /* final */ transient b34<K, V>[] f;
    @DexIgnore
    public /* final */ transient b34<K, V>[] g;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] h;
    @DexIgnore
    public /* final */ transient int i;
    @DexIgnore
    public /* final */ transient int j;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient t24<V, K> k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends t24<V, K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends c34<V, K> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n44$b$a$a")
            /* renamed from: com.fossil.n44$b$a$a  reason: collision with other inner class name */
            public class C0165a extends s24<Map.Entry<V, K>> {
                @DexIgnore
                public C0165a() {
                }

                @DexIgnore
                @Override // com.fossil.s24
                public u24<Map.Entry<V, K>> delegateCollection() {
                    return a.this;
                }

                @DexIgnore
                @Override // java.util.List
                public Map.Entry<V, K> get(int i) {
                    Map.Entry entry = n44.this.h[i];
                    return x34.e(entry.getValue(), entry.getKey());
                }
            }

            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.h34
            public y24<Map.Entry<V, K>> createAsList() {
                return new C0165a();
            }

            @DexIgnore
            @Override // com.fossil.c34, com.fossil.h34
            public int hashCode() {
                return n44.this.j;
            }

            @DexIgnore
            @Override // com.fossil.c34, com.fossil.h34
            public boolean isHashCodeFast() {
                return true;
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
            public h54<Map.Entry<V, K>> iterator() {
                return asList().iterator();
            }

            @DexIgnore
            @Override // com.fossil.c34
            public a34<V, K> map() {
                return b.this;
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.a34
        public h34<Map.Entry<V, K>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        @Override // com.fossil.a34, java.util.Map
        public K get(Object obj) {
            if (!(obj == null || n44.this.g == null)) {
                for (b34 b34 = n44.this.g[r24.b(obj.hashCode()) & n44.this.i]; b34 != null; b34 = b34.getNextInValueBucket()) {
                    if (obj.equals(b34.getValue())) {
                        return (K) b34.getKey();
                    }
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.t24, com.fossil.t24
        public t24<K, V> inverse() {
            return n44.this;
        }

        @DexIgnore
        @Override // com.fossil.a34
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return inverse().size();
        }

        @DexIgnore
        @Override // com.fossil.a34, com.fossil.t24
        public Object writeReplace() {
            return new c(n44.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ t24<K, V> forward;

        @DexIgnore
        public c(t24<K, V> t24) {
            this.forward = t24;
        }

        @DexIgnore
        public Object readResolve() {
            return this.forward.inverse();
        }
    }

    @DexIgnore
    public n44(b34<K, V>[] b34Arr, b34<K, V>[] b34Arr2, Map.Entry<K, V>[] entryArr, int i2, int i3) {
        this.f = b34Arr;
        this.g = b34Arr2;
        this.h = entryArr;
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public static void b(Object obj, Map.Entry<?, ?> entry, b34<?, ?> b34) {
        while (b34 != null) {
            a34.checkNoConflict(!obj.equals(b34.getValue()), "value", entry, b34);
            b34 = b34.getNextInValueBucket();
        }
    }

    @DexIgnore
    public static <K, V> n44<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> n44<K, V> fromEntryArray(int i2, Map.Entry<K, V>[] entryArr) {
        i14.p(i2, entryArr.length);
        int a2 = r24.a(i2, 1.2d);
        int i3 = a2 - 1;
        b34[] createEntryArray = b34.createEntryArray(a2);
        b34[] createEntryArray2 = b34.createEntryArray(a2);
        Map.Entry<K, V>[] createEntryArray3 = i2 == entryArr.length ? entryArr : b34.createEntryArray(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            Map.Entry<K, V> entry = entryArr[i5];
            K key = entry.getKey();
            V value = entry.getValue();
            a24.a(key, value);
            int hashCode = key.hashCode();
            int hashCode2 = value.hashCode();
            int b2 = r24.b(hashCode) & i3;
            int b3 = r24.b(hashCode2) & i3;
            b34 b34 = createEntryArray[b2];
            p44.checkNoConflictInKeyBucket(key, entry, b34);
            b34 b342 = createEntryArray2[b3];
            b(value, entry, b342);
            b34 b343 = (b342 == null && b34 == null) ? (entry instanceof b34) && ((b34) entry).isReusable() ? (b34) entry : new b34(key, value) : new b34.a(key, value, b34, b342);
            createEntryArray[b2] = b343;
            createEntryArray2[b3] = b343;
            createEntryArray3[i5] = b343;
            i4 += hashCode ^ hashCode2;
        }
        return new n44<>(createEntryArray, createEntryArray2, createEntryArray3, i3, i4);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? h34.of() : new c34.b(this, this.h);
    }

    @DexIgnore
    @Override // com.fossil.a34, java.util.Map
    public V get(Object obj) {
        b34<K, V>[] b34Arr = this.f;
        if (b34Arr == null) {
            return null;
        }
        return (V) p44.get(obj, b34Arr, this.i);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public int hashCode() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.t24, com.fossil.t24
    public t24<V, K> inverse() {
        if (isEmpty()) {
            return t24.of();
        }
        t24<V, K> t24 = this.k;
        if (t24 != null) {
            return t24;
        }
        b bVar = new b();
        this.k = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.h.length;
    }
}
