package com.fossil;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ho7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f1501a; // = new a(null, null, null);
    @DexIgnore
    public static a b;
    @DexIgnore
    public static /* final */ ho7 c; // = new ho7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f1502a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public a(Method method, Method method2, Method method3) {
            this.f1502a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    @DexIgnore
    public final a a(zn7 zn7) {
        try {
            a aVar = new a(Class.class.getDeclaredMethod("getModule", new Class[0]), zn7.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), zn7.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", new Class[0]));
            b = aVar;
            return aVar;
        } catch (Exception e) {
            a aVar2 = f1501a;
            b = aVar2;
            return aVar2;
        }
    }

    @DexIgnore
    public final String b(zn7 zn7) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        pq7.c(zn7, "continuation");
        a aVar = b;
        if (aVar == null) {
            aVar = a(zn7);
        }
        if (aVar == f1501a || (method = aVar.f1502a) == null || (invoke = method.invoke(zn7.getClass(), new Object[0])) == null || (method2 = aVar.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = aVar.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        if (!(invoke3 instanceof String)) {
            invoke3 = null;
        }
        return (String) invoke3;
    }
}
