package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import com.fossil.yo0;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rn0 {
    @DexIgnore
    public static /* final */ View.AccessibilityDelegate c; // = new View.AccessibilityDelegate();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ View.AccessibilityDelegate f3130a;
    @DexIgnore
    public /* final */ View.AccessibilityDelegate b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends View.AccessibilityDelegate {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rn0 f3131a;

        @DexIgnore
        public a(rn0 rn0) {
            this.f3131a = rn0;
        }

        @DexIgnore
        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.f3131a.a(view, accessibilityEvent);
        }

        @DexIgnore
        public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
            zo0 b = this.f3131a.b(view);
            if (b != null) {
                return (AccessibilityNodeProvider) b.d();
            }
            return null;
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f3131a.f(view, accessibilityEvent);
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            yo0 E0 = yo0.E0(accessibilityNodeInfo);
            E0.v0(mo0.T(view));
            E0.n0(mo0.O(view));
            E0.s0(mo0.o(view));
            this.f3131a.g(view, E0);
            E0.f(accessibilityNodeInfo.getText(), view);
            List<yo0.a> c = rn0.c(view);
            for (int i = 0; i < c.size(); i++) {
                E0.b(c.get(i));
            }
        }

        @DexIgnore
        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f3131a.h(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.f3131a.i(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            return this.f3131a.j(view, i, bundle);
        }

        @DexIgnore
        public void sendAccessibilityEvent(View view, int i) {
            this.f3131a.l(view, i);
        }

        @DexIgnore
        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.f3131a.m(view, accessibilityEvent);
        }
    }

    @DexIgnore
    public rn0() {
        this(c);
    }

    @DexIgnore
    public rn0(View.AccessibilityDelegate accessibilityDelegate) {
        this.f3130a = accessibilityDelegate;
        this.b = new a(this);
    }

    @DexIgnore
    public static List<yo0.a> c(View view) {
        List<yo0.a> list = (List) view.getTag(pk0.tag_accessibility_actions);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.f3130a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public zo0 b(View view) {
        AccessibilityNodeProvider accessibilityNodeProvider;
        if (Build.VERSION.SDK_INT < 16 || (accessibilityNodeProvider = this.f3130a.getAccessibilityNodeProvider(view)) == null) {
            return null;
        }
        return new zo0(accessibilityNodeProvider);
    }

    @DexIgnore
    public View.AccessibilityDelegate d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e(ClickableSpan clickableSpan, View view) {
        if (clickableSpan == null) {
            return false;
        }
        ClickableSpan[] q = yo0.q(view.createAccessibilityNodeInfo().getText());
        int i = 0;
        while (q != null && i < q.length) {
            if (clickableSpan.equals(q[i])) {
                return true;
            }
            i++;
        }
        return false;
    }

    @DexIgnore
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        this.f3130a.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void g(View view, yo0 yo0) {
        this.f3130a.onInitializeAccessibilityNodeInfo(view, yo0.D0());
    }

    @DexIgnore
    public void h(View view, AccessibilityEvent accessibilityEvent) {
        this.f3130a.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f3130a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    @DexIgnore
    public boolean j(View view, int i, Bundle bundle) {
        boolean z;
        List<yo0.a> c2 = c(view);
        int i2 = 0;
        while (true) {
            if (i2 >= c2.size()) {
                z = false;
                break;
            }
            yo0.a aVar = c2.get(i2);
            if (aVar.b() == i) {
                z = aVar.d(view, bundle);
                break;
            }
            i2++;
        }
        if (!z && Build.VERSION.SDK_INT >= 16) {
            z = this.f3130a.performAccessibilityAction(view, i, bundle);
        }
        return (z || i != pk0.accessibility_action_clickable_span) ? z : k(bundle.getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1), view);
    }

    @DexIgnore
    public final boolean k(int i, View view) {
        WeakReference weakReference;
        SparseArray sparseArray = (SparseArray) view.getTag(pk0.tag_accessibility_clickable_spans);
        if (!(sparseArray == null || (weakReference = (WeakReference) sparseArray.get(i)) == null)) {
            ClickableSpan clickableSpan = (ClickableSpan) weakReference.get();
            if (e(clickableSpan, view)) {
                clickableSpan.onClick(view);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void l(View view, int i) {
        this.f3130a.sendAccessibilityEvent(view, i);
    }

    @DexIgnore
    public void m(View view, AccessibilityEvent accessibilityEvent) {
        this.f3130a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
}
