package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hj4 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -4086729973971783390L;

    @DexIgnore
    public hj4(String str) {
        super(str);
    }

    @DexIgnore
    public hj4(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public hj4(Throwable th) {
        super(th);
    }
}
