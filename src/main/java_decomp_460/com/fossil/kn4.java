package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kn4 implements ql4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ en4 f1935a; // = new en4();

    @DexIgnore
    public static String b(String str) {
        int length = str.length();
        if (length == 11) {
            int i = 0;
            for (int i2 = 0; i2 < 11; i2++) {
                i += (i2 % 2 == 0 ? 3 : 1) * (str.charAt(i2) - '0');
            }
            str = str + ((1000 - i) % 10);
        } else if (length != 12) {
            throw new IllegalArgumentException("Requested contents should be 11 or 12 digits long, but got " + str.length());
        }
        return "0" + str;
    }

    @DexIgnore
    @Override // com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        if (kl4 == kl4.UPC_A) {
            return this.f1935a.a(b(str), kl4.EAN_13, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC-A, but got " + kl4);
    }
}
