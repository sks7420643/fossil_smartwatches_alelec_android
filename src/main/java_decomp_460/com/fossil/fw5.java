package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Category f1231a;
    @DexIgnore
    public int b;
    @DexIgnore
    public ArrayList<Category> c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public FlexibleButton f1232a;
        @DexIgnore
        public /* final */ /* synthetic */ fw5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fw5$a$a")
        /* renamed from: com.fossil.fw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0092a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0092a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CategoriesAdapter", "onItemClick pos=" + this.b.getAdapterPosition() + " currentPos=" + this.b.b.b);
                    a aVar = this.b;
                    aVar.b.b = aVar.getAdapterPosition();
                    if (this.b.b.b != 0) {
                        b k = this.b.b.k();
                        if (k != null) {
                            Object obj = this.b.b.c.get(this.b.b.b);
                            pq7.b(obj, "mData[mSelectedIndex]");
                            k.a((Category) obj);
                            return;
                        }
                        return;
                    }
                    b k2 = this.b.b.k();
                    if (k2 != null) {
                        k2.b();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fw5 fw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.b = fw5;
            View findViewById = view.findViewById(2131362379);
            pq7.b(findViewById, "view.findViewById(R.id.ftv_category)");
            FlexibleButton flexibleButton = (FlexibleButton) findViewById;
            this.f1232a = flexibleButton;
            flexibleButton.setOnClickListener(new View$OnClickListenerC0092a(this));
        }

        @DexIgnore
        public final void a(Category category, int i) {
            pq7.c(category, "category");
            Context context = this.f1232a.getContext();
            FlexibleButton flexibleButton = this.f1232a;
            flexibleButton.setCompoundDrawablePadding((int) p47.a(6, flexibleButton.getContext()));
            if (TextUtils.isEmpty(category.getId())) {
                FlexibleButton flexibleButton2 = this.f1232a;
                pq7.b(context, "context");
                flexibleButton2.setText(context.getResources().getString(2131886996));
            } else {
                String d = um5.d(PortfolioApp.h0.c(), category.getName(), category.getEnglishName());
                if (i == 0) {
                    this.f1232a.setText(d);
                } else {
                    pq7.b(d, "name");
                    if (d != null) {
                        String upperCase = d.toUpperCase();
                        pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        this.f1232a.setText(upperCase);
                    } else {
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            if (i == 0) {
                this.f1232a.d("flexible_button_search");
                Drawable f = gl0.f(context, 2131231050);
                if (f != null) {
                    f.setTintList(ColorStateList.valueOf(gl0.d(context, 2131099967)));
                }
                this.f1232a.setCompoundDrawablesRelativeWithIntrinsicBounds(f, (Drawable) null, (Drawable) null, (Drawable) null);
            } else if (i == this.b.b) {
                this.f1232a.setSelected(true);
                this.f1232a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.f1232a.d("flexible_button_primary");
            } else {
                this.f1232a.setSelected(false);
                this.f1232a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.f1232a.d("flexible_button_secondary");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Category category);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore
    public fw5(ArrayList<Category> arrayList, b bVar) {
        pq7.c(arrayList, "mData");
        this.c = arrayList;
        this.d = bVar;
        this.f1231a = new Category("Search", "Search", "Customization_Complications_Elements_Input__Search", "", "", -1);
        this.b = -1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fw5(ArrayList arrayList, b bVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final int j(String str) {
        T t;
        pq7.c(str, "category");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final b k() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            Category category = this.c.get(i);
            pq7.b(category, "mData[position]");
            aVar.a(category, i);
        }
    }

    @DexIgnore
    /* renamed from: m */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558667, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026_category, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void n(List<Category> list) {
        pq7.c(list, "data");
        this.c.clear();
        this.c.addAll(list);
        this.c.add(0, this.f1231a);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(b bVar) {
        pq7.c(bVar, "listener");
        this.d = bVar;
    }

    @DexIgnore
    public final void p(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CategoriesAdapter", "setSelectedCategory pos=" + i);
        if (i < getItemCount()) {
            this.b = i;
            notifyDataSetChanged();
        }
    }
}
