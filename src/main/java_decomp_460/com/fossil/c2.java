package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c2 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ b2 CREATOR; // = new b2(null);
    @DexIgnore
    public /* final */ lt b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c2(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = r4.readString()
            if (r0 == 0) goto L_0x0020
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.lt r1 = com.fossil.lt.valueOf(r0)
            byte r2 = r4.readByte()
            int r0 = r4.readInt()
            if (r0 == 0) goto L_0x001e
            r0 = 1
        L_0x001a:
            r3.<init>(r1, r2, r0)
            return
        L_0x001e:
            r0 = 0
            goto L_0x001a
        L_0x0020:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.c2.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public c2(lt ltVar, byte b2, boolean z) {
        this.d = true;
        this.b = ltVar;
        this.c = (byte) b2;
        this.d = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ c2(lt ltVar, byte b2, boolean z, int i) {
        this(ltVar, b2, (i & 4) != 0 ? true : z);
    }

    @DexIgnore
    public byte[] a() {
        return new byte[0];
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.H, ey1.a(this.b)), jd0.z1, Short.valueOf(hy1.p(this.c)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
