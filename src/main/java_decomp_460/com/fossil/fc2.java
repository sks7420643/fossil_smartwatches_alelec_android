package com.fossil;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.r62;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fc2 implements Handler.Callback {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f1103a;
    @DexIgnore
    public /* final */ ArrayList<r62.b> b; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<r62.b> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<r62.c> d; // = new ArrayList<>();
    @DexIgnore
    public volatile boolean e; // = false;
    @DexIgnore
    public /* final */ AtomicInteger f; // = new AtomicInteger(0);
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ Object i; // = new Object();

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean c();

        @DexIgnore
        Bundle y();
    }

    @DexIgnore
    public fc2(Looper looper, a aVar) {
        this.f1103a = aVar;
        this.h = new ol2(looper, this);
    }

    @DexIgnore
    public final void a() {
        this.e = false;
        this.f.incrementAndGet();
    }

    @DexIgnore
    public final void b() {
        this.e = true;
    }

    @DexIgnore
    public final void c(z52 z52) {
        rc2.e(this.h, "onConnectionFailure must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            ArrayList arrayList = new ArrayList(this.d);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                r62.c cVar = (r62.c) obj;
                if (this.e && this.f.get() == i2) {
                    if (this.d.contains(cVar)) {
                        cVar.n(z52);
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void d(Bundle bundle) {
        boolean z = true;
        rc2.e(this.h, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.i) {
            rc2.n(!this.g);
            this.h.removeMessages(1);
            this.g = true;
            if (this.c.size() != 0) {
                z = false;
            }
            rc2.n(z);
            ArrayList arrayList = new ArrayList(this.b);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                r62.b bVar = (r62.b) obj;
                if (!this.e || !this.f1103a.c() || this.f.get() != i2) {
                    break;
                } else if (!this.c.contains(bVar)) {
                    bVar.e(bundle);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    @DexIgnore
    public final void e(int i2) {
        rc2.e(this.h, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            this.g = true;
            ArrayList arrayList = new ArrayList(this.b);
            int i3 = this.f.get();
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                r62.b bVar = (r62.b) obj;
                if (!this.e || this.f.get() != i3) {
                    break;
                } else if (this.b.contains(bVar)) {
                    bVar.d(i2);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    @DexIgnore
    public final void f(r62.b bVar) {
        rc2.k(bVar);
        synchronized (this.i) {
            if (this.b.contains(bVar)) {
                String valueOf = String.valueOf(bVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.b.add(bVar);
            }
        }
        if (this.f1103a.c()) {
            Handler handler = this.h;
            handler.sendMessage(handler.obtainMessage(1, bVar));
        }
    }

    @DexIgnore
    public final void g(r62.c cVar) {
        rc2.k(cVar);
        synchronized (this.i) {
            if (this.d.contains(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.d.add(cVar);
            }
        }
    }

    @DexIgnore
    public final void h(r62.c cVar) {
        rc2.k(cVar);
        synchronized (this.i) {
            if (!this.d.remove(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 1) {
            r62.b bVar = (r62.b) message.obj;
            synchronized (this.i) {
                if (this.e && this.f1103a.c() && this.b.contains(bVar)) {
                    bVar.e(this.f1103a.y());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i2);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }
}
