package com.fossil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx4 extends pv5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<r25> g;
    @DexIgnore
    public po4 h;
    @DexIgnore
    public /* final */ List<Fragment> i; // = new ArrayList();
    @DexIgnore
    public int j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return dx4.m;
        }

        @DexIgnore
        public final dx4 b() {
            return new dx4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ r25 f847a;
        @DexIgnore
        public /* final */ /* synthetic */ dx4 b;

        @DexIgnore
        public b(r25 r25, dx4 dx4, yw4 yw4) {
            this.f847a = r25;
            this.b = dx4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            super.c(i);
            this.b.j = i;
            this.f847a.q.setCurrentPage(i);
            this.b.k = i != 0 ? i != 1 ? i != 2 ? "" : "bc_challenge_create" : "bc_challenge_list" : "bc_challenge_tab";
            ck5 g = ck5.f.g();
            String N6 = this.b.N6();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m(N6, activity);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
    }

    /*
    static {
        String simpleName = dx4.class.getSimpleName();
        pq7.b(simpleName, "BCChallengeTabFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public final String N6() {
        return this.k;
    }

    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public final void O6(CustomPageIndicator customPageIndicator, int i2) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(0, 2131230943);
        hashMap.put(Integer.valueOf(this.i.size() - 1), 2131230817);
        customPageIndicator.i(hashMap);
        customPageIndicator.setNumberOfPage(i2);
    }

    @DexIgnore
    public final void P6() {
        FLogger.INSTANCE.getLocal().e(m, "Inside .initTabs");
        Fragment Z = getChildFragmentManager().Z(ox4.s.a());
        Fragment Z2 = getChildFragmentManager().Z(ux4.s.a());
        Fragment Z3 = getChildFragmentManager().Z(ix4.l.a());
        if (Z == null) {
            Z = ox4.s.b();
        }
        if (Z2 == null) {
            Z2 = ux4.s.b();
        }
        if (Z3 == null) {
            Z3 = ix4.l.b();
        }
        this.i.clear();
        this.i.add(Z);
        this.i.add(Z2);
        this.i.add(Z3);
        yw4 yw4 = new yw4(this.i, this);
        g37<r25> g37 = this.g;
        if (g37 != null) {
            r25 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.s;
                pq7.b(viewPager2, "this");
                viewPager2.setAdapter(yw4);
                viewPager2.setOffscreenPageLimit(this.i.size());
                a2.s.g(new b(a2, this, yw4));
                CustomPageIndicator customPageIndicator = a2.q;
                pq7.b(customPageIndicator, "indicator");
                O6(customPageIndicator, this.i.size());
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.i.get(i2));
        g37<r25> g37 = this.g;
        if (g37 != null) {
            r25 a2 = g37.a();
            if (!(a2 == null || (viewPager2 = a2.s) == null)) {
                viewPager2.j(i2, true);
            }
            this.k = "bc_challenge_tab";
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        T t;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 11) {
            if (i2 == 13 && i3 == -1) {
                Q6(0);
                int intExtra = intent != null ? intent.getIntExtra("index_extra", -1) : -1;
                Iterator<T> it = this.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (next instanceof ux4) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 instanceof ux4)) {
                    t2 = null;
                }
                ux4 ux4 = (ux4) t2;
                if (ux4 != null) {
                    ux4.U6(intExtra);
                }
            }
        } else if (i3 == -1) {
            Q6(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().k1().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(fx4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            fx4 fx4 = (fx4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        r25 r25 = (r25) aq0.f(layoutInflater, 2131558505, viewGroup, false, A6());
        this.g = new g37<>(this, r25);
        pq7.b(r25, "binding");
        return r25.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ck5 g2 = ck5.f.g();
        String str = this.k;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        P6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
