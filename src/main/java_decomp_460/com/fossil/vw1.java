package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vw1 implements Serializable {
    TOP_PRESS,
    TOP_HOLD,
    TOP_SHORT_PRESS_RELEASE,
    TOP_LONG_PRESS_RELEASE,
    TOP_SINGLE_CLICK,
    TOP_DOUBLE_CLICK,
    MIDDLE_PRESS,
    MIDDLE_HOLD,
    MIDDLE_SHORT_PRESS_RELEASE,
    MIDDLE_LONG_PRESS_RELEASE,
    MIDDLE_SINGLE_CLICK,
    MIDDLE_DOUBLE_CLICK,
    BOTTOM_PRESS,
    BOTTOM_HOLD,
    BOTTOM_SHORT_PRESS_RELEASE,
    BOTTOM_LONG_PRESS_RELEASE,
    BOTTOM_SINGLE_CLICK,
    BOTTOM_DOUBLE_CLICK;
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final vw1 a(String str) {
            vw1[] values = vw1.values();
            for (vw1 vw1 : values) {
                if (pq7.a(ey1.a(vw1), str)) {
                    return vw1;
                }
            }
            return null;
        }
    }
}
