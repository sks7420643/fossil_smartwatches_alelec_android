package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y61 extends x61 {
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public y61(boolean z) {
        super(null);
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.x61
    public boolean a(f81 f81) {
        pq7.c(f81, "size");
        return this.b;
    }
}
