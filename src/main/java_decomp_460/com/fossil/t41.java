package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t41<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = p41.b();
    @DexIgnore
    public static volatile g j;
    @DexIgnore
    public static t41<?> k; // = new t41<>((Object) null);
    @DexIgnore
    public static t41<Boolean> l; // = new t41<>(Boolean.TRUE);
    @DexIgnore
    public static t41<Boolean> m; // = new t41<>(Boolean.FALSE);
    @DexIgnore
    public static t41<?> n; // = new t41<>(true);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f3357a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public v41 g;
    @DexIgnore
    public List<r41<TResult, Void>> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements r41<TResult, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ u41 f3358a;
        @DexIgnore
        public /* final */ /* synthetic */ r41 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ q41 d;

        @DexIgnore
        public a(t41 t41, u41 u41, r41 r41, Executor executor, q41 q41) {
            this.f3358a = u41;
            this.b = r41;
            this.c = executor;
            this.d = q41;
        }

        @DexIgnore
        /* renamed from: a */
        public Void then(t41<TResult> t41) {
            t41.e(this.f3358a, this.b, t41, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements r41<TResult, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ u41 f3359a;
        @DexIgnore
        public /* final */ /* synthetic */ r41 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ q41 d;

        @DexIgnore
        public b(t41 t41, u41 u41, r41 r41, Executor executor, q41 q41) {
            this.f3359a = u41;
            this.b = r41;
            this.c = executor;
            this.d = q41;
        }

        @DexIgnore
        /* renamed from: a */
        public Void then(t41<TResult> t41) {
            t41.d(this.f3359a, this.b, t41, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements r41<TResult, t41<TContinuationResult>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ q41 f3360a;
        @DexIgnore
        public /* final */ /* synthetic */ r41 b;

        @DexIgnore
        public c(t41 t41, q41 q41, r41 r41) {
            this.f3360a = q41;
            this.b = r41;
        }

        @DexIgnore
        /* renamed from: a */
        public t41<TContinuationResult> then(t41<TResult> t41) {
            q41 q41 = this.f3360a;
            return (q41 == null || !q41.a()) ? t41.r() ? t41.k(t41.m()) : t41.p() ? t41.c() : t41.f(this.b) : t41.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ q41 b;
        @DexIgnore
        public /* final */ /* synthetic */ u41 c;
        @DexIgnore
        public /* final */ /* synthetic */ r41 d;
        @DexIgnore
        public /* final */ /* synthetic */ t41 e;

        @DexIgnore
        public d(q41 q41, u41 u41, r41 r41, t41 t41) {
            this.b = q41;
            this.c = u41;
            this.d = r41;
            this.e = t41;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: com.fossil.u41 */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            q41 q41 = this.b;
            if (q41 == null || !q41.a()) {
                try {
                    this.c.d(this.d.then(this.e));
                } catch (CancellationException e2) {
                    this.c.b();
                } catch (Exception e3) {
                    this.c.c(e3);
                }
            } else {
                this.c.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ q41 b;
        @DexIgnore
        public /* final */ /* synthetic */ u41 c;
        @DexIgnore
        public /* final */ /* synthetic */ r41 d;
        @DexIgnore
        public /* final */ /* synthetic */ t41 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements r41<TContinuationResult, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            /* renamed from: a */
            public Void then(t41<TContinuationResult> t41) {
                q41 q41 = e.this.b;
                if (q41 != null && q41.a()) {
                    e.this.c.b();
                } else if (t41.p()) {
                    e.this.c.b();
                } else if (t41.r()) {
                    e.this.c.c(t41.m());
                } else {
                    e.this.c.d(t41.n());
                }
                return null;
            }
        }

        @DexIgnore
        public e(q41 q41, u41 u41, r41 r41, t41 t41) {
            this.b = q41;
            this.c = u41;
            this.d = r41;
            this.e = t41;
        }

        @DexIgnore
        public void run() {
            q41 q41 = this.b;
            if (q41 == null || !q41.a()) {
                try {
                    t41 t41 = (t41) this.d.then(this.e);
                    if (t41 == null) {
                        this.c.d(null);
                    } else {
                        t41.f(new a());
                    }
                } catch (CancellationException e2) {
                    this.c.b();
                } catch (Exception e3) {
                    this.c.c(e3);
                }
            } else {
                this.c.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends u41<TResult> {
        @DexIgnore
        public f(t41 t41) {
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(t41<?> t41, w41 w41);
    }

    /*
    static {
        p41.a();
        m41.c();
    }
    */

    @DexIgnore
    public t41() {
    }

    @DexIgnore
    public t41(TResult tresult) {
        x(tresult);
    }

    @DexIgnore
    public t41(boolean z) {
        if (z) {
            v();
        } else {
            x(null);
        }
    }

    @DexIgnore
    public static <TResult> t41<TResult> c() {
        return (t41<TResult>) n;
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(u41<TContinuationResult> u41, r41<TResult, t41<TContinuationResult>> r41, t41<TResult> t41, Executor executor, q41 q41) {
        try {
            executor.execute(new e(q41, u41, r41, t41));
        } catch (Exception e2) {
            u41.c(new s41(e2));
        }
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void e(u41<TContinuationResult> u41, r41<TResult, TContinuationResult> r41, t41<TResult> t41, Executor executor, q41 q41) {
        try {
            executor.execute(new d(q41, u41, r41, t41));
        } catch (Exception e2) {
            u41.c(new s41(e2));
        }
    }

    @DexIgnore
    public static <TResult> t41<TResult>.f j() {
        t41 t41 = new t41();
        t41.getClass();
        return new f(t41);
    }

    @DexIgnore
    public static <TResult> t41<TResult> k(Exception exc) {
        u41 u41 = new u41();
        u41.c(exc);
        return u41.a();
    }

    @DexIgnore
    public static <TResult> t41<TResult> l(TResult tresult) {
        if (tresult == null) {
            return (t41<TResult>) k;
        }
        if (tresult instanceof Boolean) {
            return tresult.booleanValue() ? (t41<TResult>) l : (t41<TResult>) m;
        }
        u41 u41 = new u41();
        u41.d(tresult);
        return u41.a();
    }

    @DexIgnore
    public static g o() {
        return j;
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> f(r41<TResult, TContinuationResult> r41) {
        return g(r41, i, null);
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> g(r41<TResult, TContinuationResult> r41, Executor executor, q41 q41) {
        boolean q;
        u41 u41 = new u41();
        synchronized (this.f3357a) {
            q = q();
            if (!q) {
                this.h.add(new a(this, u41, r41, executor, q41));
            }
        }
        if (q) {
            e(u41, r41, this, executor, q41);
        }
        return u41.a();
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> h(r41<TResult, t41<TContinuationResult>> r41, Executor executor) {
        return i(r41, executor, null);
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> i(r41<TResult, t41<TContinuationResult>> r41, Executor executor, q41 q41) {
        boolean q;
        u41 u41 = new u41();
        synchronized (this.f3357a) {
            q = q();
            if (!q) {
                this.h.add(new b(this, u41, r41, executor, q41));
            }
        }
        if (q) {
            d(u41, r41, this, executor, q41);
        }
        return u41.a();
    }

    @DexIgnore
    public Exception m() {
        Exception exc;
        synchronized (this.f3357a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult n() {
        TResult tresult;
        synchronized (this.f3357a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public boolean p() {
        boolean z;
        synchronized (this.f3357a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean q() {
        boolean z;
        synchronized (this.f3357a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean r() {
        boolean z;
        synchronized (this.f3357a) {
            z = m() != null;
        }
        return z;
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> s(r41<TResult, TContinuationResult> r41) {
        return t(r41, i, null);
    }

    @DexIgnore
    public <TContinuationResult> t41<TContinuationResult> t(r41<TResult, TContinuationResult> r41, Executor executor, q41 q41) {
        return h(new c(this, q41, r41), executor);
    }

    @DexIgnore
    public final void u() {
        synchronized (this.f3357a) {
            for (r41<TResult, Void> r41 : this.h) {
                try {
                    r41.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean v() {
        synchronized (this.f3357a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.f3357a.notifyAll();
            u();
            return true;
        }
    }

    @DexIgnore
    public boolean w(Exception exc) {
        synchronized (this.f3357a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.f3357a.notifyAll();
            u();
            if (!this.f && o() != null) {
                this.g = new v41(this);
            }
            return true;
        }
    }

    @DexIgnore
    public boolean x(TResult tresult) {
        synchronized (this.f3357a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.f3357a.notifyAll();
            u();
            return true;
        }
    }
}
