package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w21 {
    @DexIgnore
    public static w21 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public q21 f3867a;
    @DexIgnore
    public r21 b;
    @DexIgnore
    public u21 c;
    @DexIgnore
    public v21 d;

    @DexIgnore
    public w21(Context context, k41 k41) {
        Context applicationContext = context.getApplicationContext();
        this.f3867a = new q21(applicationContext, k41);
        this.b = new r21(applicationContext, k41);
        this.c = new u21(applicationContext, k41);
        this.d = new v21(applicationContext, k41);
    }

    @DexIgnore
    public static w21 c(Context context, k41 k41) {
        w21 w21;
        synchronized (w21.class) {
            try {
                if (e == null) {
                    e = new w21(context, k41);
                }
                w21 = e;
            } catch (Throwable th) {
                throw th;
            }
        }
        return w21;
    }

    @DexIgnore
    public q21 a() {
        return this.f3867a;
    }

    @DexIgnore
    public r21 b() {
        return this.b;
    }

    @DexIgnore
    public u21 d() {
        return this.c;
    }

    @DexIgnore
    public v21 e() {
        return this.d;
    }
}
