package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t77 implements Factory<s77> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<m77> f3375a;
    @DexIgnore
    public /* final */ Provider<r77> b;

    @DexIgnore
    public t77(Provider<m77> provider, Provider<r77> provider2) {
        this.f3375a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static t77 a(Provider<m77> provider, Provider<r77> provider2) {
        return new t77(provider, provider2);
    }

    @DexIgnore
    public static s77 c(m77 m77, r77 r77) {
        return new s77(m77, r77);
    }

    @DexIgnore
    /* renamed from: b */
    public s77 get() {
        return c(this.f3375a.get(), this.b.get());
    }
}
