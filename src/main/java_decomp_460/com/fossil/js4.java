package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js4 {
    @rj4("challengeId")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1799a;
    @DexIgnore
    @rj4("challengeName")
    public String b;
    @DexIgnore
    @rj4("players")
    public List<ms4> c;

    @DexIgnore
    public final String a() {
        return this.f1799a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final List<ms4> c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof js4) {
                js4 js4 = (js4) obj;
                if (!pq7.a(this.f1799a, js4.f1799a) || !pq7.a(this.b, js4.b) || !pq7.a(this.c, js4.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1799a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        List<ms4> list = this.c;
        if (list != null) {
            i = list.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCChallengePlayer(challengeId=" + this.f1799a + ", challengeName=" + this.b + ", players=" + this.c + ")";
    }
}
