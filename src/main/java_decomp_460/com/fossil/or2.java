package com.fossil;

import android.os.RemoteException;
import com.fossil.ha3;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or2 extends ha3.a<ka3> {
    @DexIgnore
    public /* final */ /* synthetic */ ia3 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public or2(nr2 nr2, r62 r62, ia3 ia3, String str) {
        super(r62);
        this.s = ia3;
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ z62 f(Status status) {
        return new ka3(status);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.m62$b] */
    @Override // com.fossil.i72
    public final /* synthetic */ void u(fr2 fr2) throws RemoteException {
        fr2.y0(this.s, this, this.t);
    }
}
