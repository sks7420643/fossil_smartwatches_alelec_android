package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ie1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(id1<?> id1);
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    id1<?> b(mb1 mb1, id1<?> id1);

    @DexIgnore
    id1<?> c(mb1 mb1);

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    void e(a aVar);
}
