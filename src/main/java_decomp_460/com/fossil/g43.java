package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g43 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ i43 f1260a; // = ((!(e43.m() && e43.r()) || qz2.b()) ? new h43() : new j43());

    @DexIgnore
    public static int d(CharSequence charSequence) {
        int i;
        int i2 = 0;
        int length = charSequence.length();
        int i3 = 0;
        while (i3 < length && charSequence.charAt(i3) < '\u0080') {
            i3++;
        }
        int i4 = length;
        while (true) {
            if (i3 >= length) {
                i = i4;
                break;
            }
            char charAt = charSequence.charAt(i3);
            if (charAt < '\u0800') {
                i4 += ('\u007f' - charAt) >>> 31;
                i3++;
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt2 = charSequence.charAt(i3);
                    if (charAt2 < '\u0800') {
                        i2 += ('\u007f' - charAt2) >>> 31;
                    } else {
                        i2 += 2;
                        if ('\ud800' <= charAt2 && charAt2 <= '\udfff') {
                            if (Character.codePointAt(charSequence, i3) >= 65536) {
                                i3++;
                            } else {
                                throw new k43(i3, length2);
                            }
                        }
                    }
                    i3++;
                }
                i = i2 + i4;
            }
        }
        if (i >= length) {
            return i;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(((long) i) + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static int e(CharSequence charSequence, byte[] bArr, int i, int i2) {
        return f1260a.b(charSequence, bArr, i, i2);
    }

    @DexIgnore
    public static boolean f(byte[] bArr) {
        return f1260a.d(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static boolean g(byte[] bArr, int i, int i2) {
        return f1260a.d(bArr, i, i2);
    }

    @DexIgnore
    public static int h(int i) {
        if (i > -12) {
            return -1;
        }
        return i;
    }

    @DexIgnore
    public static int i(int i, int i2) {
        if (i > -12 || i2 > -65) {
            return -1;
        }
        return (i2 << 8) ^ i;
    }

    @DexIgnore
    public static int j(int i, int i2, int i3) {
        if (i > -12 || i2 > -65 || i3 > -65) {
            return -1;
        }
        return ((i2 << 8) ^ i) ^ (i3 << 16);
    }

    @DexIgnore
    public static String k(byte[] bArr, int i, int i2) throws l13 {
        return f1260a.c(bArr, i, i2);
    }

    @DexIgnore
    public static int m(byte[] bArr, int i, int i2) {
        byte b = bArr[i - 1];
        int i3 = i2 - i;
        if (i3 == 0) {
            return h(b);
        }
        if (i3 == 1) {
            return i(b, bArr[i]);
        }
        if (i3 == 2) {
            return j(b, bArr[i], bArr[i + 1]);
        }
        throw new AssertionError();
    }
}
