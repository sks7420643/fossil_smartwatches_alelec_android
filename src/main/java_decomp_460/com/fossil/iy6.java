package com.fossil;

import com.misfit.frameworks.buttonservice.model.ShineDevice;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface iy6 extends rv5<hy6> {
    @DexIgnore
    Object D0();  // void declaration

    @DexIgnore
    void F2(String str, boolean z);

    @DexIgnore
    void G3(List<String> list);

    @DexIgnore
    void K2(int i, String str);

    @DexIgnore
    void P3(String str, boolean z, boolean z2);

    @DexIgnore
    Object Z0();  // void declaration

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void b4(boolean z);

    @DexIgnore
    Object d0();  // void declaration

    @DexIgnore
    Object e0();  // void declaration

    @DexIgnore
    void h4(int i, String str, String str2);

    @DexIgnore
    void i2(String str);

    @DexIgnore
    Object l();  // void declaration

    @DexIgnore
    void l4(String str);

    @DexIgnore
    void o4(List<cl7<ShineDevice, String>> list);

    @DexIgnore
    void s6(String str);

    @DexIgnore
    void t2(List<cl7<ShineDevice, String>> list);

    @DexIgnore
    void v2(String str, boolean z);

    @DexIgnore
    void v3(String str);

    @DexIgnore
    Object x4();  // void declaration

    @DexIgnore
    void y4(boolean z);
}
