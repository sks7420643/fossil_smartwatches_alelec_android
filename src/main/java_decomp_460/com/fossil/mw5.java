package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw5 extends du0<GoalTrackingData, a> {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f2430a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ mw5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mw5$a$a")
        /* renamed from: com.fossil.mw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0159a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0159a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                if (this.b.d.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    a aVar = this.b;
                    GoalTrackingData j = mw5.j(aVar.d, aVar.getAdapterPosition());
                    if (j != null && (bVar = this.b.d.d) != null) {
                        pq7.b(j, "it1");
                        bVar.Z2(j);
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(mw5 mw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = mw5;
            this.f2430a = (FlexibleTextView) view.findViewById(2131362543);
            this.b = (FlexibleTextView) view.findViewById(2131362493);
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362403);
            this.c = flexibleTextView;
            flexibleTextView.setOnClickListener(new View$OnClickListenerC0159a(this));
        }

        @DexIgnore
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            pq7.c(goalTrackingData, "item");
            if (this.d.c == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + dl5.b(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = dl5.b(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                pq7.b(str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            FlexibleTextView flexibleTextView = this.f2430a;
            pq7.b(flexibleTextView, "mTvTime");
            hr7 hr7 = hr7.f1520a;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887558);
            pq7.b(c2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{lk5.p(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str}, 2));
            pq7.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView.setText(format);
            FlexibleTextView flexibleTextView2 = this.b;
            pq7.b(flexibleTextView2, "mTvNoTime");
            flexibleTextView2.setVisibility(8);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void Z2(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mw5(b bVar, nq4 nq4) {
        super(nq4);
        pq7.c(nq4, "goalTrackingDataDiff");
        this.d = bVar;
        TimeZone timeZone = TimeZone.getDefault();
        pq7.b(timeZone, "TimeZone.getDefault()");
        this.c = lk5.g0(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData j(mw5 mw5, int i) {
        return (GoalTrackingData) mw5.getItem(i);
    }

    @DexIgnore
    /* renamed from: m */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) getItem(i);
        if (goalTrackingData != null) {
            aVar.a(goalTrackingData);
        }
    }

    @DexIgnore
    /* renamed from: n */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558710, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new a(this, inflate);
    }
}
