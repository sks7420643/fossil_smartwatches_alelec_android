package com.fossil;

import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px1 {
    @DexIgnore
    public static final JSONArray a(ox1[] ox1Arr) {
        pq7.c(ox1Arr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (ox1 ox1 : ox1Arr) {
            jSONArray.put(ox1.toJSONObject());
        }
        return jSONArray;
    }
}
