package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jg0 {

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean e();

        @DexIgnore
        void f(eg0 eg0, int i);

        @DexIgnore
        eg0 getItemData();
    }

    @DexIgnore
    void b(cg0 cg0);
}
