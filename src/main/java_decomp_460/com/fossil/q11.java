package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static ax0 f2908a; // = new a(1, 2);
    @DexIgnore
    public static ax0 b; // = new b(3, 4);
    @DexIgnore
    public static ax0 c; // = new c(4, 5);
    @DexIgnore
    public static ax0 d; // = new d(6, 7);
    @DexIgnore
    public static ax0 e; // = new e(7, 8);
    @DexIgnore
    public static ax0 f; // = new f(8, 9);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ax0 {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            lx0.execSQL("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            lx0.execSQL("DROP TABLE IF EXISTS alarmInfo");
            lx0.execSQL("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ax0 {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            if (Build.VERSION.SDK_INT >= 23) {
                lx0.execSQL("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ax0 {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ax0 {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ax0 {
        @DexIgnore
        public e(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends ax0 {
        @DexIgnore
        public f(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends ax0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f2909a;

        @DexIgnore
        public g(Context context, int i, int i2) {
            super(i, i2);
            this.f2909a = context;
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            if (this.endVersion >= 10) {
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
                return;
            }
            this.f2909a.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends ax0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f2910a;

        @DexIgnore
        public h(Context context) {
            super(9, 10);
            this.f2910a = context;
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            z31.b(this.f2910a, lx0);
            x31.a(this.f2910a, lx0);
        }
    }
}
