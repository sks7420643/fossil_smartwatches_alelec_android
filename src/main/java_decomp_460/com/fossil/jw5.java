package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CustomizeWidget f1822a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<MicroApp> c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public CustomizeWidget f1823a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public MicroApp e;
        @DexIgnore
        public /* final */ /* synthetic */ jw5 f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jw5$a$a")
        /* renamed from: com.fossil.jw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0132a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0132a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mData[adapterPosition]");
                    l.a((MicroApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mData[adapterPosition]");
                    l.b((MicroApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1824a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(a aVar) {
                this.f1824a = aVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                pq7.c(customizeWidget, "view");
                this.f1824a.f.f1822a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jw5 jw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.f = jw5;
            this.c = view.findViewById(2131362728);
            this.d = view.findViewById(2131362767);
            View findViewById = view.findViewById(2131363551);
            pq7.b(findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.f1823a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363424);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.f1823a.setOnClickListener(new View$OnClickListenerC0132a(this));
            this.d.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(MicroApp microApp) {
            String str;
            pq7.c(microApp, "microApp");
            this.e = microApp;
            if (microApp != null) {
                this.f1823a.S(microApp.getId());
                this.b.setText(um5.d(PortfolioApp.h0.c(), microApp.getNameKey(), microApp.getName()));
            }
            this.f1823a.setSelectedWc(pq7.a(microApp.getId(), this.f.b));
            View view = this.d;
            pq7.b(view, "ivWarning");
            view.setVisibility(!bl5.c.e(microApp.getId()) ? 0 : 8);
            if (pq7.a(microApp.getId(), this.f.b)) {
                View view2 = this.c;
                pq7.b(view2, "ivIndicator");
                view2.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230956));
            } else {
                View view3 = this.c;
                pq7.b(view3, "ivIndicator");
                view3.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230957));
            }
            CustomizeWidget customizeWidget = this.f1823a;
            Intent intent = new Intent();
            MicroApp microApp2 = this.e;
            if (microApp2 == null || (str = microApp2.getId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            pq7.b(putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.X(customizeWidget, "WATCH_APP", putExtra, null, new c(this), 4, null);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(MicroApp microApp);

        @DexIgnore
        void b(MicroApp microApp);
    }

    @DexIgnore
    public jw5(ArrayList<MicroApp> arrayList, b bVar) {
        pq7.c(arrayList, "mData");
        this.c = arrayList;
        this.d = bVar;
        this.b = "empty";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jw5(ArrayList arrayList, b bVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d("MicroAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.f1822a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final int k(String str) {
        T t;
        pq7.c(str, "microAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final b l() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: m */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            MicroApp microApp = this.c.get(i);
            pq7.b(microApp, "mData[position]");
            aVar.a(microApp);
        }
    }

    @DexIgnore
    /* renamed from: n */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558724, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void o(List<MicroApp> list) {
        pq7.c(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void p(b bVar) {
        pq7.c(bVar, "listener");
        this.d = bVar;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "microAppId");
        this.b = str;
        notifyDataSetChanged();
    }
}
