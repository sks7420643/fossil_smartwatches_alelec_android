package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z17 implements Factory<y17> {
    @DexIgnore
    public static y17 a(ct0 ct0, DeviceRepository deviceRepository, on5 on5, v17 v17, cu5 cu5, fu5 fu5, au5 au5, gu5 gu5, PortfolioApp portfolioApp) {
        return new y17(ct0, deviceRepository, on5, v17, cu5, fu5, au5, gu5, portfolioApp);
    }
}
