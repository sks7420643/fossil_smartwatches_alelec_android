package com.fossil;

import android.app.job.JobInfo;
import com.fossil.s12;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v12 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public t32 f3695a;
        @DexIgnore
        public Map<vy1, b> b; // = new HashMap();

        @DexIgnore
        public a a(vy1 vy1, b bVar) {
            this.b.put(vy1, bVar);
            return this;
        }

        @DexIgnore
        public v12 b() {
            if (this.f3695a == null) {
                throw new NullPointerException("missing required property: clock");
            } else if (this.b.keySet().size() >= vy1.values().length) {
                Map<vy1, b> map = this.b;
                this.b = new HashMap();
                return v12.c(this.f3695a, map);
            } else {
                throw new IllegalStateException("Not all priorities have been configured");
            }
        }

        @DexIgnore
        public a c(t32 t32) {
            this.f3695a = t32;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(long j);

            @DexIgnore
            public abstract a c(Set<c> set);

            @DexIgnore
            public abstract a d(long j);
        }

        @DexIgnore
        public static a a() {
            s12.b bVar = new s12.b();
            bVar.c(Collections.emptySet());
            return bVar;
        }

        @DexIgnore
        public abstract long b();

        @DexIgnore
        public abstract Set<c> c();

        @DexIgnore
        public abstract long d();
    }

    @DexIgnore
    public enum c {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    @DexIgnore
    public static a a() {
        return new a();
    }

    @DexIgnore
    public static v12 c(t32 t32, Map<vy1, b> map) {
        return new r12(t32, map);
    }

    @DexIgnore
    public static v12 e(t32 t32) {
        a a2 = a();
        vy1 vy1 = vy1.DEFAULT;
        b.a a3 = b.a();
        a3.b(30000);
        a3.d(LogBuilder.MAX_INTERVAL);
        a2.a(vy1, a3.a());
        vy1 vy12 = vy1.HIGHEST;
        b.a a4 = b.a();
        a4.b(1000);
        a4.d(LogBuilder.MAX_INTERVAL);
        a2.a(vy12, a4.a());
        vy1 vy13 = vy1.VERY_LOW;
        b.a a5 = b.a();
        a5.b(LogBuilder.MAX_INTERVAL);
        a5.d(LogBuilder.MAX_INTERVAL);
        a5.c(h(c.NETWORK_UNMETERED, c.DEVICE_IDLE));
        a2.a(vy13, a5.a());
        a2.c(t32);
        return a2.b();
    }

    @DexIgnore
    public static <T> Set<T> h(T... tArr) {
        return Collections.unmodifiableSet(new HashSet(Arrays.asList(tArr)));
    }

    @DexIgnore
    public JobInfo.Builder b(JobInfo.Builder builder, vy1 vy1, long j, int i) {
        builder.setMinimumLatency(f(vy1, j, i));
        i(builder, g().get(vy1).c());
        return builder;
    }

    @DexIgnore
    public abstract t32 d();

    @DexIgnore
    public long f(vy1 vy1, long j, int i) {
        long a2 = d().a();
        b bVar = g().get(vy1);
        return Math.min(Math.max(((long) Math.pow(2.0d, (double) (i - 1))) * bVar.b(), j - a2), bVar.d());
    }

    @DexIgnore
    public abstract Map<vy1, b> g();

    @DexIgnore
    public final void i(JobInfo.Builder builder, Set<c> set) {
        if (set.contains(c.NETWORK_UNMETERED)) {
            builder.setRequiredNetworkType(2);
        } else {
            builder.setRequiredNetworkType(1);
        }
        if (set.contains(c.DEVICE_CHARGING)) {
            builder.setRequiresCharging(true);
        }
        if (set.contains(c.DEVICE_IDLE)) {
            builder.setRequiresDeviceIdle(true);
        }
    }
}
