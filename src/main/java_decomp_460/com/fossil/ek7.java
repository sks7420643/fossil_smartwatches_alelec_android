package com.fossil;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ek7 extends AppCompatActivity implements dk7 {
    @DexIgnore
    public ck7<Object> b;

    @DexIgnore
    @Override // com.fossil.dk7
    public vj7<Object> c() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        uj7.a(this);
        super.onCreate(bundle);
    }
}
