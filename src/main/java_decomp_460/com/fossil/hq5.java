package com.fossil;

import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq5<T> extends iq5<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1516a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public hq5(int i, ServerError serverError, Throwable th, String str, T t) {
        super(null);
        this.f1516a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
        this.e = t;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ hq5(int i, ServerError serverError, Throwable th, String str, Object obj, int i2, kq7 kq7) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str, (i2 & 16) == 0 ? obj : null);
    }

    @DexIgnore
    public final int a() {
        return this.f1516a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof hq5) {
                hq5 hq5 = (hq5) obj;
                if (this.f1516a != hq5.f1516a || !pq7.a(this.b, hq5.b) || !pq7.a(this.c, hq5.c) || !pq7.a(this.d, hq5.d) || !pq7.a(this.e, hq5.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.f1516a;
        ServerError serverError = this.b;
        int hashCode = serverError != null ? serverError.hashCode() : 0;
        Throwable th = this.c;
        int hashCode2 = th != null ? th.hashCode() : 0;
        String str = this.d;
        int hashCode3 = str != null ? str.hashCode() : 0;
        T t = this.e;
        if (t != null) {
            i = t.hashCode();
        }
        return ((((((hashCode + (i2 * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.f1516a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ", data=" + ((Object) this.e) + ")";
    }
}
