package com.fossil;

import com.fossil.hn1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bb extends nq7 implements rp7<byte[], hn1> {
    @DexIgnore
    public bb(hn1.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final fs7 getOwner() {
        return er7.b(hn1.b.class);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/VibeStrengthConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public hn1 invoke(byte[] bArr) {
        return ((hn1.b) this.receiver).a(bArr);
    }
}
