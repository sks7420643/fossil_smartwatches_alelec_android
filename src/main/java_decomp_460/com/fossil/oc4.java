package com.fossil;

import com.facebook.AccessToken;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oc4 implements tc4 {
    @DexIgnore
    public static wc4 b(JSONObject jSONObject) throws JSONException {
        return new wc4(jSONObject.getString("status"), jSONObject.getString("url"), jSONObject.getString("reports_url"), jSONObject.getString("ndk_reports_url"), jSONObject.optBoolean("update_required", false));
    }

    @DexIgnore
    public static xc4 c(JSONObject jSONObject) {
        return new xc4(jSONObject.optBoolean("collect_reports", true));
    }

    @DexIgnore
    public static yc4 d(JSONObject jSONObject) {
        return new yc4(jSONObject.optInt("max_custom_exception_events", 8), 4);
    }

    @DexIgnore
    public static zc4 e(b94 b94) {
        JSONObject jSONObject = new JSONObject();
        return new ad4(f(b94, 3600, jSONObject), null, d(jSONObject), c(jSONObject), 0, 3600);
    }

    @DexIgnore
    public static long f(b94 b94, long j, JSONObject jSONObject) {
        return jSONObject.has(AccessToken.EXPIRES_AT_KEY) ? jSONObject.optLong(AccessToken.EXPIRES_AT_KEY) : b94.a() + (1000 * j);
    }

    @DexIgnore
    @Override // com.fossil.tc4
    public ad4 a(b94 b94, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new ad4(f(b94, (long) optInt2, jSONObject), b(jSONObject.getJSONObject("app")), d(jSONObject.getJSONObject(Constants.SESSION)), c(jSONObject.getJSONObject("features")), optInt, optInt2);
    }
}
