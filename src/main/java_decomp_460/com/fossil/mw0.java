package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mw0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<LiveData> f2428a; // = Collections.newSetFromMap(new IdentityHashMap());
    @DexIgnore
    public /* final */ qw0 b;

    @DexIgnore
    public mw0(qw0 qw0) {
        this.b = qw0;
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new uw0(this.b, this, z, callable, strArr);
    }

    @DexIgnore
    public void b(LiveData liveData) {
        this.f2428a.add(liveData);
    }

    @DexIgnore
    public void c(LiveData liveData) {
        this.f2428a.remove(liveData);
    }
}
