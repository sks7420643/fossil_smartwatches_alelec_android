package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb6 implements Factory<mb6> {
    @DexIgnore
    public static mb6 a(PortfolioApp portfolioApp, lb6 lb6, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, ib6 ib6, on5 on5) {
        return new mb6(portfolioApp, lb6, microAppRepository, hybridPresetRepository, ib6, on5);
    }
}
