package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.r16;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a26 extends u16 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public r16 f;
    @DexIgnore
    public /* final */ v16 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ls0<r16.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a26 f186a;

        @DexIgnore
        public a(a26 a26) {
            this.f186a = a26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r16.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a26.h;
            local.d(str, "NotificationSettingChanged value = " + aVar);
            this.f186a.e = aVar.b();
            this.f186a.g.e2(aVar.a());
        }
    }

    /*
    static {
        String simpleName = a26.class.getSimpleName();
        pq7.b(simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public a26(v16 v16) {
        pq7.c(v16, "mView");
        this.g = v16;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "start: isCall = " + this.e);
        r16 r16 = this.f;
        if (r16 != null) {
            MutableLiveData<r16.a> a2 = r16.a();
            v16 v16 = this.g;
            if (v16 != null) {
                a2.h((w16) v16, new a(this));
                this.g.t(s(this.e));
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        pq7.n("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
        r16 r16 = this.f;
        if (r16 != null) {
            MutableLiveData<r16.a> a2 = r16.a();
            v16 v16 = this.g;
            if (v16 != null) {
                a2.n((w16) v16);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        pq7.n("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u16
    public void n(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i);
        r16.a aVar = new r16.a(i, this.e);
        r16 r16 = this.f;
        if (r16 != null) {
            r16.a().l(aVar);
        } else {
            pq7.n("mNotificationSettingViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.u16
    public void o(r16 r16) {
        pq7.c(r16, "viewModel");
        this.f = r16;
    }

    @DexIgnore
    public final String s(boolean z) {
        if (z) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886094);
            pq7.b(c, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return c;
        }
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886095);
        pq7.b(c2, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return c2;
    }

    @DexIgnore
    public void t() {
        this.g.M5(this);
    }
}
