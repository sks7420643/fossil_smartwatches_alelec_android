package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yt7 extends xt7 {
    @DexIgnore
    public static final char w0(CharSequence charSequence) {
        pq7.c(charSequence, "$this$first");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(0);
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }

    @DexIgnore
    public static final char x0(CharSequence charSequence) {
        pq7.c(charSequence, "$this$last");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(wt7.A(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
}
