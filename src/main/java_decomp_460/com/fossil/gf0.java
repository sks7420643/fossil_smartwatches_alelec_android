package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public final class gf0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ThreadLocal<TypedValue> f1298a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ WeakHashMap<Context, SparseArray<a>> b; // = new WeakHashMap<>(0);
    @DexIgnore
    public static /* final */ Object c; // = new Object();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ColorStateList f1299a;
        @DexIgnore
        public /* final */ Configuration b;

        @DexIgnore
        public a(ColorStateList colorStateList, Configuration configuration) {
            this.f1299a = colorStateList;
            this.b = configuration;
        }
    }

    @DexIgnore
    public static void a(Context context, int i, ColorStateList colorStateList) {
        synchronized (c) {
            SparseArray<a> sparseArray = b.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray<>();
                b.put(context, sparseArray);
            }
            sparseArray.append(i, new a(colorStateList, context.getResources().getConfiguration()));
        }
    }

    @DexIgnore
    public static ColorStateList b(Context context, int i) {
        a aVar;
        synchronized (c) {
            SparseArray<a> sparseArray = b.get(context);
            if (!(sparseArray == null || sparseArray.size() <= 0 || (aVar = sparseArray.get(i)) == null)) {
                if (aVar.b.equals(context.getResources().getConfiguration())) {
                    return aVar.f1299a;
                }
                sparseArray.remove(i);
            }
            return null;
        }
    }

    @DexIgnore
    public static ColorStateList c(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList b2 = b(context, i);
        if (b2 != null) {
            return b2;
        }
        ColorStateList f = f(context, i);
        if (f == null) {
            return gl0.e(context, i);
        }
        a(context, i, f);
        return f;
    }

    @DexIgnore
    public static Drawable d(Context context, int i) {
        return jh0.h().j(context, i);
    }

    @DexIgnore
    public static TypedValue e() {
        TypedValue typedValue = f1298a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f1298a.set(typedValue2);
        return typedValue2;
    }

    @DexIgnore
    public static ColorStateList f(Context context, int i) {
        if (g(context, i)) {
            return null;
        }
        Resources resources = context.getResources();
        try {
            return il0.a(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    @DexIgnore
    public static boolean g(Context context, int i) {
        Resources resources = context.getResources();
        TypedValue e = e();
        resources.getValue(i, e, true);
        int i2 = e.type;
        return i2 >= 28 && i2 <= 31;
    }
}
