package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo4 implements Factory<bk5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3801a;
    @DexIgnore
    public /* final */ Provider<on5> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> d;

    @DexIgnore
    public vo4(uo4 uo4, Provider<on5> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        this.f3801a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static vo4 a(uo4 uo4, Provider<on5> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        return new vo4(uo4, provider, provider2, provider3);
    }

    @DexIgnore
    public static bk5 c(uo4 uo4, on5 on5, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        bk5 b2 = uo4.b(on5, userRepository, alarmsRepository);
        lk7.c(b2, "Cannot return null from a non-@Nullable @Provides method");
        return b2;
    }

    @DexIgnore
    /* renamed from: b */
    public bk5 get() {
        return c(this.f3801a, this.b.get(), this.c.get(), this.d.get());
    }
}
