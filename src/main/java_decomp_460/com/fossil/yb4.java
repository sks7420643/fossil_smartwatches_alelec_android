package com.fossil;

import android.app.ActivityManager;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yb4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ vb4 f4297a; // = vb4.c("0");
    @DexIgnore
    public static /* final */ vb4 b; // = vb4.c("Unity");

    @DexIgnore
    public static void A(xb4 xb4, vb4 vb4) throws Exception {
        if (vb4 != null) {
            xb4.d0(6, 2);
            xb4.X(h(vb4));
            xb4.F(1, vb4);
        }
    }

    @DexIgnore
    public static void B(xb4 xb4, String str, String str2, boolean z) throws Exception {
        vb4 c = vb4.c(str);
        vb4 c2 = vb4.c(str2);
        xb4.d0(8, 2);
        xb4.X(m(c, c2, z));
        xb4.H(1, 3);
        xb4.F(2, c);
        xb4.F(3, c2);
        xb4.C(4, z);
    }

    @DexIgnore
    public static void C(xb4 xb4, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        vb4 c = vb4.c(str);
        vb4 o = o(str2);
        vb4 o2 = o(str3);
        int c2 = xb4.c(1, c) + 0;
        if (str2 != null) {
            c2 += xb4.c(2, o);
        }
        if (str3 != null) {
            c2 += xb4.c(3, o2);
        }
        xb4.d0(6, 2);
        xb4.X(c2);
        xb4.F(1, c);
        if (str2 != null) {
            xb4.F(2, o);
        }
        if (str3 != null) {
            xb4.F(3, o2);
        }
    }

    @DexIgnore
    public static void D(xb4 xb4, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) throws Exception {
        xb4.d0(1, 2);
        xb4.X(n(thread, stackTraceElementArr, i, z));
        xb4.F(1, vb4.c(thread.getName()));
        xb4.e0(2, i);
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            q(xb4, 3, stackTraceElement, z);
        }
    }

    @DexIgnore
    public static int a(vb4 vb4, vb4 vb42) {
        int u = xb4.u(1, 0) + 0 + xb4.u(2, 0) + xb4.c(3, vb4);
        return vb42 != null ? u + xb4.c(4, vb42) : u;
    }

    @DexIgnore
    public static int b(String str, String str2) {
        int c = xb4.c(1, vb4.c(str));
        if (str2 == null) {
            str2 = "";
        }
        return c + xb4.c(2, vb4.c(str2));
    }

    @DexIgnore
    public static int c(ld4 ld4, int i, int i2) {
        int i3 = 0;
        int c = xb4.c(1, vb4.c(ld4.b)) + 0;
        String str = ld4.f2182a;
        if (str != null) {
            c += xb4.c(3, vb4.c(str));
        }
        for (StackTraceElement stackTraceElement : ld4.c) {
            int i4 = i(stackTraceElement, true);
            c += i4 + xb4.r(4) + xb4.l(i4);
        }
        ld4 ld42 = ld4.d;
        if (ld42 == null) {
            return c;
        }
        if (i < i2) {
            int c2 = c(ld42, i + 1, i2);
            return c + c2 + xb4.r(6) + xb4.l(c2);
        }
        while (ld42 != null) {
            ld42 = ld42.d;
            i3++;
        }
        return c + xb4.s(7, i3);
    }

    @DexIgnore
    public static int d() {
        return xb4.c(1, f4297a) + 0 + xb4.c(2, f4297a) + xb4.u(3, 0);
    }

    @DexIgnore
    public static int e(ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, vb4 vb4, vb4 vb42) {
        int n = n(thread, stackTraceElementArr, 4, true);
        int r = n + xb4.r(1) + xb4.l(n) + 0;
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            int n2 = n(threadArr[i2], list.get(i2), 0, false);
            r += n2 + xb4.r(1) + xb4.l(n2);
        }
        int c = c(ld4, 1, i);
        int r2 = xb4.r(2);
        int l = xb4.l(c);
        int d = d();
        int r3 = xb4.r(3);
        int l2 = xb4.l(d);
        int a2 = a(vb4, vb42);
        return c + r2 + l + r + r3 + l2 + d + xb4.r(3) + xb4.l(a2) + a2;
    }

    @DexIgnore
    public static int f(ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, vb4 vb4, vb4 vb42, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int i3;
        int e = e(ld4, thread, stackTraceElementArr, threadArr, list, i, vb4, vb42);
        int r = e + xb4.r(1) + xb4.l(e) + 0;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                int b2 = b(entry.getKey(), entry.getValue());
                r = b2 + xb4.r(2) + xb4.l(b2) + r;
            }
            i3 = r;
        } else {
            i3 = r;
        }
        return (runningAppProcessInfo != null ? xb4.a(3, runningAppProcessInfo.importance != 100) + i3 : i3) + xb4.s(4, i2);
    }

    @DexIgnore
    public static int g(Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = xb4.i(1, f.floatValue()) + 0;
        }
        return i3 + xb4.o(2, i) + xb4.a(3, z) + xb4.s(4, i2) + xb4.u(5, j) + xb4.u(6, j2);
    }

    @DexIgnore
    public static int h(vb4 vb4) {
        return xb4.c(1, vb4);
    }

    @DexIgnore
    public static int i(StackTraceElement stackTraceElement, boolean z) {
        int u = stackTraceElement.isNativeMethod() ? xb4.u(1, (long) Math.max(stackTraceElement.getLineNumber(), 0)) : xb4.u(1, 0);
        int c = u + 0 + xb4.c(2, vb4.c(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            c += xb4.c(3, vb4.c(stackTraceElement.getFileName()));
        }
        return xb4.s(5, z ? 2 : 0) + ((stackTraceElement.isNativeMethod() || stackTraceElement.getLineNumber() <= 0) ? c : c + xb4.u(4, (long) stackTraceElement.getLineNumber()));
    }

    @DexIgnore
    public static int j(vb4 vb4, vb4 vb42, vb4 vb43, vb4 vb44, int i, vb4 vb45) {
        int c = xb4.c(1, vb4) + 0 + xb4.c(2, vb42) + xb4.c(3, vb43) + xb4.c(6, vb44);
        if (vb45 != null) {
            c = c + xb4.c(8, b) + xb4.c(9, vb45);
        }
        return c + xb4.g(10, i);
    }

    @DexIgnore
    public static int k(int i, vb4 vb4, int i2, long j, long j2, boolean z, int i3, vb4 vb42, vb4 vb43) {
        int g = xb4.g(3, i);
        int i4 = 0;
        int c = vb4 == null ? 0 : xb4.c(4, vb4);
        int s = xb4.s(5, i2);
        int u = xb4.u(6, j);
        int u2 = xb4.u(7, j2);
        int a2 = xb4.a(10, z);
        int s2 = xb4.s(12, i3);
        int c2 = vb42 == null ? 0 : xb4.c(13, vb42);
        if (vb43 != null) {
            i4 = xb4.c(14, vb43);
        }
        return c + g + 0 + s + u + u2 + a2 + s2 + c2 + i4;
    }

    @DexIgnore
    public static int l(long j, String str, ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, vb4 vb4, vb4 vb42, Float f, int i3, boolean z, long j2, long j3, vb4 vb43) {
        int u = xb4.u(1, j);
        int c = xb4.c(2, vb4.c(str));
        int f2 = f(ld4, thread, stackTraceElementArr, threadArr, list, i, vb4, vb42, map, runningAppProcessInfo, i2);
        int r = xb4.r(3);
        int l = xb4.l(f2);
        int g = g(f, i3, z, i2, j2, j3);
        int r2 = g + xb4.r(5) + xb4.l(g) + u + 0 + c + r + l + f2;
        if (vb43 == null) {
            return r2;
        }
        int h = h(vb43);
        return r2 + h + xb4.r(6) + xb4.l(h);
    }

    @DexIgnore
    public static int m(vb4 vb4, vb4 vb42, boolean z) {
        return xb4.g(1, 3) + 0 + xb4.c(2, vb4) + xb4.c(3, vb42) + xb4.a(4, z);
    }

    @DexIgnore
    public static int n(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int s = xb4.s(2, i) + xb4.c(1, vb4.c(thread.getName()));
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            int i2 = i(stackTraceElement, z);
            s += i2 + xb4.r(3) + xb4.l(i2);
        }
        return s;
    }

    @DexIgnore
    public static vb4 o(String str) {
        if (str == null) {
            return null;
        }
        return vb4.c(str);
    }

    @DexIgnore
    public static void p(xb4 xb4, String str, String str2, long j) throws Exception {
        xb4.F(1, vb4.c(str2));
        xb4.F(2, vb4.c(str));
        xb4.g0(3, j);
    }

    @DexIgnore
    public static void q(xb4 xb4, int i, StackTraceElement stackTraceElement, boolean z) throws Exception {
        int i2 = 4;
        xb4.d0(i, 2);
        xb4.X(i(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            xb4.g0(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            xb4.g0(1, 0);
        }
        xb4.F(2, vb4.c(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            xb4.F(3, vb4.c(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            xb4.g0(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i2 = 0;
        }
        xb4.e0(5, i2);
    }

    @DexIgnore
    public static void r(xb4 xb4, String str, String str2, String str3, String str4, int i, String str5) throws Exception {
        vb4 c = vb4.c(str);
        vb4 c2 = vb4.c(str2);
        vb4 c3 = vb4.c(str3);
        vb4 c4 = vb4.c(str4);
        vb4 c5 = str5 != null ? vb4.c(str5) : null;
        xb4.d0(7, 2);
        xb4.X(j(c, c2, c3, c4, i, c5));
        xb4.F(1, c);
        xb4.F(2, c2);
        xb4.F(3, c3);
        xb4.F(6, c4);
        if (c5 != null) {
            xb4.F(8, b);
            xb4.F(9, c5);
        }
        xb4.H(10, i);
    }

    @DexIgnore
    public static void s(xb4 xb4, String str) throws Exception {
        vb4 c = vb4.c(str);
        xb4.d0(7, 2);
        int c2 = xb4.c(2, c);
        xb4.X(xb4.r(5) + xb4.l(c2) + c2);
        xb4.d0(5, 2);
        xb4.X(c2);
        xb4.F(2, c);
    }

    @DexIgnore
    public static void t(xb4 xb4, int i, String str, int i2, long j, long j2, boolean z, int i3, String str2, String str3) throws Exception {
        vb4 o = o(str);
        vb4 o2 = o(str3);
        vb4 o3 = o(str2);
        xb4.d0(9, 2);
        xb4.X(k(i, o, i2, j, j2, z, i3, o3, o2));
        xb4.H(3, i);
        xb4.F(4, o);
        xb4.e0(5, i2);
        xb4.g0(6, j);
        xb4.g0(7, j2);
        xb4.C(10, z);
        xb4.e0(12, i3);
        if (o3 != null) {
            xb4.F(13, o3);
        }
        if (o2 != null) {
            xb4.F(14, o2);
        }
    }

    @DexIgnore
    public static void u(xb4 xb4, long j, String str, ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, byte[] bArr, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, String str2, String str3, Float f, int i3, boolean z, long j2, long j3) throws Exception {
        vb4 c = vb4.c(str2);
        vb4 vb4 = null;
        vb4 c2 = str3 == null ? null : vb4.c(str3.replace(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, ""));
        if (bArr != null) {
            vb4 = vb4.a(bArr);
        } else {
            x74.f().b("No log data to include with this event.");
        }
        xb4.d0(10, 2);
        xb4.X(l(j, str, ld4, thread, stackTraceElementArr, threadArr, list, i, map, runningAppProcessInfo, i2, c, c2, f, i3, z, j2, j3, vb4));
        xb4.g0(1, j);
        xb4.F(2, vb4.c(str));
        v(xb4, ld4, thread, stackTraceElementArr, threadArr, list, i, c, c2, map, runningAppProcessInfo, i2);
        z(xb4, f, i3, z, i2, j2, j3);
        A(xb4, vb4);
    }

    @DexIgnore
    public static void v(xb4 xb4, ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, vb4 vb4, vb4 vb42, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) throws Exception {
        xb4.d0(3, 2);
        xb4.X(f(ld4, thread, stackTraceElementArr, threadArr, list, i, vb4, vb42, map, runningAppProcessInfo, i2));
        x(xb4, ld4, thread, stackTraceElementArr, threadArr, list, i, vb4, vb42);
        if (map != null && !map.isEmpty()) {
            w(xb4, map);
        }
        if (runningAppProcessInfo != null) {
            xb4.C(3, runningAppProcessInfo.importance != 100);
        }
        xb4.e0(4, i2);
    }

    @DexIgnore
    public static void w(xb4 xb4, Map<String, String> map) throws Exception {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xb4.d0(2, 2);
            xb4.X(b(entry.getKey(), entry.getValue()));
            xb4.F(1, vb4.c(entry.getKey()));
            String value = entry.getValue();
            if (value == null) {
                value = "";
            }
            xb4.F(2, vb4.c(value));
        }
    }

    @DexIgnore
    public static void x(xb4 xb4, ld4 ld4, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, vb4 vb4, vb4 vb42) throws Exception {
        xb4.d0(1, 2);
        xb4.X(e(ld4, thread, stackTraceElementArr, threadArr, list, i, vb4, vb42));
        D(xb4, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            D(xb4, threadArr[i2], list.get(i2), 0, false);
        }
        y(xb4, ld4, 1, i, 2);
        xb4.d0(3, 2);
        xb4.X(d());
        xb4.F(1, f4297a);
        xb4.F(2, f4297a);
        xb4.g0(3, 0);
        xb4.d0(4, 2);
        xb4.X(a(vb4, vb42));
        xb4.g0(1, 0);
        xb4.g0(2, 0);
        xb4.F(3, vb4);
        if (vb42 != null) {
            xb4.F(4, vb42);
        }
    }

    @DexIgnore
    public static void y(xb4 xb4, ld4 ld4, int i, int i2, int i3) throws Exception {
        int i4 = 0;
        xb4.d0(i3, 2);
        xb4.X(c(ld4, 1, i2));
        xb4.F(1, vb4.c(ld4.b));
        String str = ld4.f2182a;
        if (str != null) {
            xb4.F(3, vb4.c(str));
        }
        for (StackTraceElement stackTraceElement : ld4.c) {
            q(xb4, 4, stackTraceElement, true);
        }
        ld4 ld42 = ld4.d;
        if (ld42 == null) {
            return;
        }
        if (i < i2) {
            y(xb4, ld42, i + 1, i2, 6);
            return;
        }
        while (ld42 != null) {
            ld42 = ld42.d;
            i4++;
        }
        xb4.e0(7, i4);
    }

    @DexIgnore
    public static void z(xb4 xb4, Float f, int i, boolean z, int i2, long j, long j2) throws Exception {
        xb4.d0(5, 2);
        xb4.X(g(f, i, z, i2, j, j2));
        if (f != null) {
            xb4.L(1, f.floatValue());
        }
        xb4.b0(2, i);
        xb4.C(3, z);
        xb4.e0(4, i2);
        xb4.g0(5, j);
        xb4.g0(6, j2);
    }
}
