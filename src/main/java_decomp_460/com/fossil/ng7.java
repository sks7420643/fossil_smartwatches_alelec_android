package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ng7 {
    @DexIgnore
    public static String l;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2518a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public uh7 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public jg7 k; // = null;

    @DexIgnore
    public ng7(Context context, int i2, jg7 jg7) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = fg7.v(context);
        this.h = ei7.F(context);
        this.f2518a = fg7.s(context);
        if (jg7 != null) {
            this.k = jg7;
            if (ei7.t(jg7.a())) {
                this.f2518a = jg7.a();
            }
            if (ei7.t(jg7.b())) {
                this.g = jg7.b();
            }
            if (ei7.t(jg7.c())) {
                this.h = jg7.c();
            }
            this.i = jg7.d();
        }
        this.f = fg7.u(context);
        this.d = gh7.b(context).v(context);
        og7 a2 = a();
        og7 og7 = og7.i;
        this.e = a2 != og7 ? ei7.O(context).intValue() : -og7.a();
        if (!se7.g(l)) {
            String w = fg7.w(context);
            l = w;
            if (!ei7.t(w)) {
                l = "0";
            }
        }
    }

    @DexIgnore
    public abstract og7 a();

    @DexIgnore
    public abstract boolean b(JSONObject jSONObject);

    @DexIgnore
    public boolean c(JSONObject jSONObject) {
        try {
            ji7.d(jSONObject, "ky", this.f2518a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.c());
                ji7.d(jSONObject, "mc", this.d.d());
                int e2 = this.d.e();
                jSONObject.put("ut", e2);
                if (e2 == 0 && ei7.S(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            ji7.d(jSONObject, "cui", this.f);
            if (a() != og7.b) {
                ji7.d(jSONObject, "av", this.h);
                ji7.d(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            ji7.d(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", ei7.e(this.j, false));
            return b(jSONObject);
        } catch (Throwable th) {
            return false;
        }
    }

    @DexIgnore
    public long d() {
        return this.b;
    }

    @DexIgnore
    public jg7 e() {
        return this.k;
    }

    @DexIgnore
    public Context f() {
        return this.j;
    }

    @DexIgnore
    public boolean g() {
        return this.i;
    }

    @DexIgnore
    public String h() {
        try {
            JSONObject jSONObject = new JSONObject();
            c(jSONObject);
            return jSONObject.toString();
        } catch (Throwable th) {
            return "";
        }
    }
}
