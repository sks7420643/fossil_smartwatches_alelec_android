package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yn4 {
    L(1),
    M(0),
    Q(3),
    H(2);
    
    @DexIgnore
    public static /* final */ yn4[] b;
    @DexIgnore
    public /* final */ int bits;

    /*
    static {
        yn4 yn4;
        yn4 yn42 = L;
        b = new yn4[]{M, yn42, yn4, Q};
    }
    */

    @DexIgnore
    public yn4(int i) {
        this.bits = i;
    }

    @DexIgnore
    public static yn4 forBits(int i) {
        if (i >= 0) {
            yn4[] yn4Arr = b;
            if (i < yn4Arr.length) {
                return yn4Arr[i];
            }
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int getBits() {
        return this.bits;
    }
}
