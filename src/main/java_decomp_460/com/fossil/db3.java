package com.fossil;

import android.os.RemoteException;
import com.fossil.p72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db3 extends y72<fr2, fa3> {
    @DexIgnore
    public /* final */ /* synthetic */ ea3 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public db3(ea3 ea3, p72.a aVar) {
        super(aVar);
        this.b = ea3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.m62$b, com.fossil.ot3] */
    @Override // com.fossil.y72
    public final /* synthetic */ void b(fr2 fr2, ot3 ot3) throws RemoteException {
        try {
            fr2.z0(a(), this.b.w(ot3));
        } catch (RuntimeException e) {
            ot3.d(e);
        }
    }
}
