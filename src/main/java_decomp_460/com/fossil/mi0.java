package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.td0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mi0 extends Service {
    @DexIgnore
    public td0.a b; // = new a(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends td0.a {
        @DexIgnore
        public a(mi0 mi0) {
        }

        @DexIgnore
        @Override // com.fossil.td0
        public void G0(rd0 rd0, Bundle bundle) throws RemoteException {
            rd0.G2(bundle);
        }

        @DexIgnore
        @Override // com.fossil.td0
        public void g1(rd0 rd0, String str, Bundle bundle) throws RemoteException {
            rd0.A2(str, bundle);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b;
    }
}
