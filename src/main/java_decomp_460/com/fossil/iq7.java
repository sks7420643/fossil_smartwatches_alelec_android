package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq7 implements es7<Object>, hq7 {
    @DexIgnore
    public static /* final */ Map<Class<? extends uk7<?>>, Integer> c;
    @DexIgnore
    public static /* final */ HashMap<String, String> d;
    @DexIgnore
    public static /* final */ HashMap<String, String> e;
    @DexIgnore
    public static /* final */ HashMap<String, String> f;
    @DexIgnore
    public static /* final */ Map<String, String> g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public /* final */ Class<?> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x003f, code lost:
            if (r0 != null) goto L_0x0041;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.String a(java.lang.Class<?> r6) {
            /*
                r5 = this;
                r4 = 2
                r1 = 0
                java.lang.String r0 = "jClass"
                com.fossil.pq7.c(r6, r0)
                boolean r0 = r6.isAnonymousClass()
                java.lang.String r2 = "Array"
                if (r0 == 0) goto L_0x0011
                r0 = r1
            L_0x0010:
                return r0
            L_0x0011:
                boolean r0 = r6.isLocalClass()
                if (r0 == 0) goto L_0x0076
                java.lang.String r2 = r6.getSimpleName()
                java.lang.reflect.Method r0 = r6.getEnclosingMethod()
                if (r0 == 0) goto L_0x004f
                java.lang.String r3 = "name"
                com.fossil.pq7.b(r2, r3)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r0 = r0.getName()
                r3.append(r0)
                java.lang.String r0 = "$"
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                java.lang.String r0 = com.fossil.wt7.j0(r2, r0, r1, r4, r1)
                if (r0 == 0) goto L_0x004f
            L_0x0041:
                if (r0 != 0) goto L_0x0010
                java.lang.String r0 = "name"
                com.fossil.pq7.b(r2, r0)
                r0 = 36
                java.lang.String r0 = com.fossil.wt7.i0(r2, r0, r1, r4, r1)
                goto L_0x0010
            L_0x004f:
                java.lang.reflect.Constructor r0 = r6.getEnclosingConstructor()
                if (r0 == 0) goto L_0x0074
                java.lang.String r3 = "name"
                com.fossil.pq7.b(r2, r3)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r0 = r0.getName()
                r3.append(r0)
                java.lang.String r0 = "$"
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                java.lang.String r0 = com.fossil.wt7.j0(r2, r0, r1, r4, r1)
                goto L_0x0041
            L_0x0074:
                r0 = r1
                goto L_0x0041
            L_0x0076:
                boolean r0 = r6.isArray()
                if (r0 == 0) goto L_0x00b2
                java.lang.Class r0 = r6.getComponentType()
                java.lang.String r3 = "componentType"
                com.fossil.pq7.b(r0, r3)
                boolean r3 = r0.isPrimitive()
                if (r3 == 0) goto L_0x00c8
                java.util.Map r3 = com.fossil.iq7.c()
                java.lang.String r0 = r0.getName()
                java.lang.Object r0 = r3.get(r0)
                java.lang.String r0 = (java.lang.String) r0
                if (r0 == 0) goto L_0x00c8
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r1.append(r0)
                java.lang.String r0 = "Array"
                r1.append(r0)
                java.lang.String r1 = r1.toString()
                r0 = r1
            L_0x00ad:
                if (r0 != 0) goto L_0x0010
                r0 = r2
                goto L_0x0010
            L_0x00b2:
                java.util.Map r0 = com.fossil.iq7.c()
                java.lang.String r1 = r6.getName()
                java.lang.Object r0 = r0.get(r1)
                java.lang.String r0 = (java.lang.String) r0
                if (r0 != 0) goto L_0x0010
                java.lang.String r0 = r6.getSimpleName()
                goto L_0x0010
            L_0x00c8:
                r0 = r1
                goto L_0x00ad
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iq7.a.a(java.lang.Class):java.lang.String");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v40, resolved type: java.util.HashMap<java.lang.String, java.lang.String> */
    /* JADX WARN: Multi-variable type inference failed */
    /*
    static {
        int i = 0;
        List h2 = hm7.h(gp7.class, rp7.class, vp7.class, wp7.class, xp7.class, yp7.class, zp7.class, aq7.class, bq7.class, cq7.class, hp7.class, ip7.class, jp7.class, kp7.class, lp7.class, mp7.class, np7.class, op7.class, pp7.class, qp7.class, sp7.class, tp7.class, up7.class);
        ArrayList arrayList = new ArrayList(im7.m(h2, 10));
        for (Object obj : h2) {
            if (i >= 0) {
                arrayList.add(hl7.a((Class) obj, Integer.valueOf(i)));
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
        c = zm7.n(arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("boolean", "kotlin.Boolean");
        hashMap.put(LegacyTokenHelper.TYPE_CHAR, "kotlin.Char");
        hashMap.put(LegacyTokenHelper.TYPE_BYTE, "kotlin.Byte");
        hashMap.put(LegacyTokenHelper.TYPE_SHORT, "kotlin.Short");
        hashMap.put(LegacyTokenHelper.TYPE_INTEGER, "kotlin.Int");
        hashMap.put(LegacyTokenHelper.TYPE_FLOAT, "kotlin.Float");
        hashMap.put(LegacyTokenHelper.TYPE_LONG, "kotlin.Long");
        hashMap.put(LegacyTokenHelper.TYPE_DOUBLE, "kotlin.Double");
        d = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        e = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(d);
        hashMap3.putAll(e);
        Collection<String> values = d.values();
        pq7.b(values, "primitiveFqNames.values");
        for (T t : values) {
            StringBuilder sb = new StringBuilder();
            sb.append("kotlin.jvm.internal.");
            pq7.b(t, "kotlinName");
            sb.append(wt7.l0(t, '.', null, 2, null));
            sb.append("CompanionObject");
            String sb2 = sb.toString();
            cl7 a2 = hl7.a(sb2, ((String) t) + ".Companion");
            hashMap3.put(a2.getFirst(), a2.getSecond());
        }
        for (Map.Entry<Class<? extends uk7<?>>, Integer> entry : c.entrySet()) {
            int intValue = entry.getValue().intValue();
            String name = entry.getKey().getName();
            hashMap3.put(name, "kotlin.Function" + intValue);
        }
        f = hashMap3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(ym7.b(hashMap3.size()));
        for (Map.Entry entry2 : hashMap3.entrySet()) {
            linkedHashMap.put(entry2.getKey(), wt7.l0((String) entry2.getValue(), '.', null, 2, null));
        }
        g = linkedHashMap;
    }
    */

    @DexIgnore
    public iq7(Class<?> cls) {
        pq7.c(cls, "jClass");
        this.b = cls;
    }

    @DexIgnore
    @Override // com.fossil.es7
    public String a() {
        return h.a(b());
    }

    @DexIgnore
    @Override // com.fossil.hq7
    public Class<?> b() {
        return this.b;
    }

    @DexIgnore
    public final Void d() {
        throw new fp7();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof iq7) && pq7.a(ep7.b(this), ep7.b((es7) obj));
    }

    @DexIgnore
    @Override // com.fossil.cs7
    public List<Annotation> getAnnotations() {
        d();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return ep7.b(this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return b().toString() + " (Kotlin reflection is not available)";
    }
}
