package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fz3 extends SparseArray<Parcelable> implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fz3> CREATOR; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.ClassLoaderCreator<fz3> {
        @DexIgnore
        /* renamed from: a */
        public fz3 createFromParcel(Parcel parcel) {
            return new fz3(parcel, null);
        }

        @DexIgnore
        /* renamed from: b */
        public fz3 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new fz3(parcel, classLoader);
        }

        @DexIgnore
        /* renamed from: c */
        public fz3[] newArray(int i) {
            return new fz3[i];
        }
    }

    @DexIgnore
    public fz3() {
    }

    @DexIgnore
    public fz3(Parcel parcel, ClassLoader classLoader) {
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = keyAt(i2);
            parcelableArr[i2] = (Parcelable) valueAt(i2);
        }
        parcel.writeInt(size);
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }
}
