package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd3 extends as2 implements ac3 {
    @DexIgnore
    public pd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapFragmentDelegate");
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final rg2 C0(rg2 rg2, rg2 rg22, Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        es2.c(d, rg22);
        es2.d(d, bundle);
        Parcel e = e(4, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void l(pc3 pc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, pc3);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void o() throws RemoteException {
        i(7, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.d(d, bundle);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onDestroy() throws RemoteException {
        i(8, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onLowMemory() throws RemoteException {
        i(9, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onPause() throws RemoteException {
        i(6, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onResume() throws RemoteException {
        i(5, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.d(d, bundle);
        Parcel e = e(10, d);
        if (e.readInt() != 0) {
            bundle.readFromParcel(e);
        }
        e.recycle();
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onStart() throws RemoteException {
        i(15, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void onStop() throws RemoteException {
        i(16, d());
    }

    @DexIgnore
    @Override // com.fossil.ac3
    public final void s0(rg2 rg2, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        es2.d(d, googleMapOptions);
        es2.d(d, bundle);
        i(2, d);
    }
}
