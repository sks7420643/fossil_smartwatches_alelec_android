package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb extends vx1<tl7, zk1> {
    @DexIgnore
    public static /* final */ qx1<tl7>[] b; // = new qx1[0];
    @DexIgnore
    public static /* final */ rx1<zk1>[] c; // = {new ab(ob.DEVICE_INFO.c), new cb(ob.DEVICE_INFO.c)};
    @DexIgnore
    public static /* final */ eb d; // = new eb();

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<tl7>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<zk1>[] c() {
        return c;
    }

    @DexIgnore
    public final zk1 h(byte[] bArr) {
        return zk1.u.c(dm7.k(bArr, 12, bArr.length - 4));
    }
}
