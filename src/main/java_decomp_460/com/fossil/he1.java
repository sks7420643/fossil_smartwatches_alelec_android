package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.ie1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class he1 extends fk1<mb1, id1<?>> implements ie1 {
    @DexIgnore
    public ie1.a d;

    @DexIgnore
    public he1(long j) {
        super(j);
    }

    @DexIgnore
    @Override // com.fossil.ie1
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            d();
        } else if (i >= 20 || i == 15) {
            m(h() / 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ie1
    public /* bridge */ /* synthetic */ id1 b(mb1 mb1, id1 id1) {
        return (id1) super.k(mb1, id1);
    }

    @DexIgnore
    @Override // com.fossil.ie1
    public /* bridge */ /* synthetic */ id1 c(mb1 mb1) {
        return (id1) super.l(mb1);
    }

    @DexIgnore
    @Override // com.fossil.ie1
    public void e(ie1.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    /* renamed from: n */
    public int i(id1<?> id1) {
        return id1 == null ? super.i(null) : id1.c();
    }

    @DexIgnore
    /* renamed from: o */
    public void j(mb1 mb1, id1<?> id1) {
        ie1.a aVar = this.d;
        if (aVar != null && id1 != null) {
            aVar.a(id1);
        }
    }
}
