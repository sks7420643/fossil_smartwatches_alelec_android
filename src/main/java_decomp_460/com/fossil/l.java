package com.fossil;

import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l extends wk1 {
    @DexIgnore
    public /* final */ al1[] b;

    @DexIgnore
    public l(al1[] al1Arr) {
        this.b = al1Arr;
    }

    @DexIgnore
    @Override // com.fossil.wk1
    public boolean a(e60 e60) {
        if (this.b.length == 0) {
            return true;
        }
        for (al1 al1 : this.b) {
            if (al1 == e60.u.getDeviceType()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(new JSONObject(), jd0.H1, "device_type");
        jd0 jd0 = jd0.J1;
        al1[] al1Arr = this.b;
        JSONArray jSONArray = new JSONArray();
        for (al1 al1 : al1Arr) {
            jSONArray.put(ey1.a(al1));
        }
        return g80.k(k, jd0, jSONArray);
    }
}
