package com.fossil;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y21 implements g21, k11 {
    @DexIgnore
    public static /* final */ String m; // = x01.f("SystemFgDispatcher");
    @DexIgnore
    public Context b;
    @DexIgnore
    public s11 c;
    @DexIgnore
    public /* final */ k41 d;
    @DexIgnore
    public /* final */ Object e; // = new Object();
    @DexIgnore
    public String f;
    @DexIgnore
    public t01 g;
    @DexIgnore
    public /* final */ Map<String, t01> h;
    @DexIgnore
    public /* final */ Map<String, o31> i;
    @DexIgnore
    public /* final */ Set<o31> j;
    @DexIgnore
    public /* final */ h21 k;
    @DexIgnore
    public b l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WorkDatabase b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public a(WorkDatabase workDatabase, String str) {
            this.b = workDatabase;
            this.c = str;
        }

        @DexIgnore
        public void run() {
            o31 o = this.b.j().o(this.c);
            if (o != null && o.b()) {
                synchronized (y21.this.e) {
                    y21.this.i.put(this.c, o);
                    y21.this.j.add(o);
                    y21.this.k.d(y21.this.j);
                }
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void b(int i, int i2, Notification notification);

        @DexIgnore
        void c(int i, Notification notification);

        @DexIgnore
        void d(int i);

        @DexIgnore
        Object stop();  // void declaration
    }

    @DexIgnore
    public y21(Context context) {
        this.b = context;
        s11 l2 = s11.l(this.b);
        this.c = l2;
        this.d = l2.q();
        this.f = null;
        this.g = null;
        this.h = new LinkedHashMap();
        this.j = new HashSet();
        this.i = new HashMap();
        this.k = new h21(this.b, this.d, this);
        this.c.n().b(this);
    }

    @DexIgnore
    public final void a(Intent intent) {
        x01.c().d(m, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
            this.c.g(UUID.fromString(stringExtra));
        }
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void b(List<String> list) {
        if (!list.isEmpty()) {
            for (String str : list) {
                x01.c().a(m, String.format("Constraints unmet for WorkSpec %s", str), new Throwable[0]);
                this.c.x(str);
            }
        }
    }

    @DexIgnore
    public final void c(Intent intent) {
        int i2 = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        x01.c().a(m, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (!(notification == null || this.l == null)) {
            this.h.put(stringExtra, new t01(intExtra, notification, intExtra2));
            if (TextUtils.isEmpty(this.f)) {
                this.f = stringExtra;
                this.l.b(intExtra, intExtra2, notification);
                return;
            }
            this.l.c(intExtra, notification);
            if (intExtra2 != 0 && Build.VERSION.SDK_INT >= 29) {
                for (Map.Entry<String, t01> entry : this.h.entrySet()) {
                    i2 = entry.getValue().a() | i2;
                }
                t01 t01 = this.h.get(this.f);
                if (t01 != null) {
                    this.l.b(t01.c(), i2, t01.b());
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k11
    public void d(String str, boolean z) {
        b bVar;
        Map.Entry<String, t01> entry;
        synchronized (this.e) {
            o31 remove = this.i.remove(str);
            if (remove != null ? this.j.remove(remove) : false) {
                this.k.d(this.j);
            }
        }
        this.g = this.h.remove(str);
        if (!str.equals(this.f)) {
            t01 t01 = this.g;
            if (t01 != null && (bVar = this.l) != null) {
                bVar.d(t01.c());
            }
        } else if (this.h.size() > 0) {
            Iterator<Map.Entry<String, t01>> it = this.h.entrySet().iterator();
            Map.Entry<String, t01> next = it.next();
            while (true) {
                entry = next;
                if (!it.hasNext()) {
                    break;
                }
                next = it.next();
            }
            this.f = entry.getKey();
            if (this.l != null) {
                t01 value = entry.getValue();
                this.l.b(value.c(), value.a(), value.b());
                this.l.d(value.c());
            }
        }
    }

    @DexIgnore
    public final void e(Intent intent) {
        x01.c().d(m, String.format("Started foreground service %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        this.d.b(new a(this.c.p(), stringExtra));
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void f(List<String> list) {
    }

    @DexIgnore
    public void g() {
        x01.c().d(m, "Stopping foreground service", new Throwable[0]);
        b bVar = this.l;
        if (bVar != null) {
            t01 t01 = this.g;
            if (t01 != null) {
                bVar.d(t01.c());
                this.g = null;
            }
            this.l.stop();
        }
    }

    @DexIgnore
    public void h() {
        this.l = null;
        synchronized (this.e) {
            this.k.e();
        }
        this.c.n().h(this);
    }

    @DexIgnore
    public void i(Intent intent) {
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            e(intent);
            c(intent);
        } else if ("ACTION_NOTIFY".equals(action)) {
            c(intent);
        } else if ("ACTION_CANCEL_WORK".equals(action)) {
            a(intent);
        }
    }

    @DexIgnore
    public void j(b bVar) {
        if (this.l != null) {
            x01.c().b(m, "A callback already exists.", new Throwable[0]);
        } else {
            this.l = bVar;
        }
    }
}
