package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.tk1;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k5 implements Parcelable {
    @DexIgnore
    public static /* final */ t4 CREATOR; // = new t4(null);
    @DexIgnore
    public /* final */ BluetoothDevice A;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public BluetoothGatt c;
    @DexIgnore
    public long d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public v4 f;
    @DexIgnore
    public /* final */ BluetoothGattCallback g;
    @DexIgnore
    public l4 h;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<bs> i;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<ds> j;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<d5> k;
    @DexIgnore
    public /* final */ HashMap<n6, p6> l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public /* final */ BroadcastReceiver t;
    @DexIgnore
    public /* final */ BroadcastReceiver u;
    @DexIgnore
    public /* final */ BroadcastReceiver v;
    @DexIgnore
    public f5 w;
    @DexIgnore
    public /* final */ String x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ n4 z;

    @DexIgnore
    public /* synthetic */ k5(BluetoothDevice bluetoothDevice, kq7 kq7) {
        this.A = bluetoothDevice;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.g = new k6(this);
            this.h = new l4(this);
            this.i = new CopyOnWriteArraySet<>();
            this.j = new CopyOnWriteArraySet<>();
            this.k = new CopyOnWriteArraySet<>();
            this.l = new HashMap<>();
            this.m = 20;
            this.s = 20;
            this.t = new m7(this);
            this.u = new q7(this);
            this.v = new u7(this);
            this.w = f5.DISCONNECTED;
            String address = this.A.getAddress();
            pq7.b(address, "bluetoothDevice.address");
            this.x = address;
            this.z = new n4();
            BroadcastReceiver broadcastReceiver = this.t;
            Context a2 = id0.i.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"}[i2]);
                }
                ct0.b(a2).c(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = this.u;
            Context a3 = id0.i.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED"}[i3]);
                }
                ct0.b(a3).c(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = this.v;
            Context a4 = id0.i.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(new String[]{"com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED"}[i4]);
                }
                ct0.b(a4).c(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void A() {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new s4(this, new g7(f7.BLUETOOTH_OFF, 0, 2), new UUID[0], new n6[0]));
            return;
        }
        BluetoothGatt bluetoothGatt = this.c;
        if (bluetoothGatt == null) {
            this.b.post(new s4(this, new g7(f7.GATT_NULL, 0, 2), new UUID[0], new n6[0]));
        } else if (true != bluetoothGatt.discoverServices()) {
            this.e = true;
            this.b.post(new s4(this, new g7(f7.START_FAIL, 0, 2), new UUID[0], new n6[0]));
        }
    }

    @DexIgnore
    public final String C() {
        String name = this.A.getName();
        return name != null ? name : "";
    }

    @DexIgnore
    public final b5 D() {
        return b5.g.a(l80.d.h(this.A));
    }

    @DexIgnore
    public final boolean E() {
        if (!q3.f.a()) {
            return false;
        }
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            return false;
        }
        try {
            ky1 ky12 = ky1.DEBUG;
            Method method = this.A.getClass().getMethod("removeBond", new Class[0]);
            pq7.b(method, "bluetoothDevice.javaClass.getMethod(\"removeBond\")");
            Object invoke = method.invoke(this.A, new Object[0]);
            if (invoke != null) {
                return ((Boolean) invoke).booleanValue();
            }
            throw new il7("null cannot be cast to non-null type kotlin.Boolean");
        } catch (Exception e2) {
            d90.i.i(e2);
            return false;
        }
    }

    @DexIgnore
    public final void F() {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new a5(this, new g7(f7.BLUETOOTH_OFF, 0, 2), 0));
            return;
        }
        g7 g7Var = new g7(f7.SUCCESS, 0, 2);
        BluetoothGatt bluetoothGatt = this.c;
        if (bluetoothGatt == null) {
            g7Var = new g7(f7.GATT_NULL, 0, 2);
        } else if (true != bluetoothGatt.readRemoteRssi()) {
            g7Var = new g7(f7.START_FAIL, 0, 2);
        }
        if (g7Var.b != f7.SUCCESS) {
            this.b.post(new a5(this, g7Var, 0));
        }
    }

    @DexIgnore
    public final r4 H() {
        return r4.f.a(this.A.getBondState());
    }

    @DexIgnore
    public final void I() {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new c5(this, new g7(f7.BLUETOOTH_OFF, 0, 2), H(), H()));
        } else if (r4.BOND_NONE == H()) {
            this.b.post(new c5(this, new g7(f7.SUCCESS, 0, 2), H(), H()));
        } else if (!E()) {
            this.b.post(new c5(this, new g7(f7.START_FAIL, 0, 2), H(), H()));
        }
    }

    @DexIgnore
    public final void a() {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            g7 g7Var = new g7(f7.BLUETOOTH_OFF, 0, 2);
            b5 b5Var = b5.DISCONNECTED;
            this.b.post(new i4(this, g7Var, b5Var, b5Var));
            return;
        }
        int i2 = h5.c[D().ordinal()];
        if (i2 == 1) {
            g7 a2 = g7.d.a(l80.d.a(this.A));
            if (a2.b != f7.SUCCESS) {
                b5 b5Var2 = b5.DISCONNECTED;
                this.b.post(new i4(this, a2, b5Var2, b5Var2));
            }
        } else if (i2 == 3) {
            g7 g7Var2 = new g7(f7.SUCCESS, 0, 2);
            b5 b5Var3 = b5.CONNECTED;
            this.b.post(new i4(this, g7Var2, b5Var3, b5Var3));
        }
    }

    @DexIgnore
    public final void b(int i2) {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new e5(this, new g7(f7.BLUETOOTH_OFF, 0, 2), 0));
        } else if (i2 == this.m) {
            this.b.post(new e5(this, new g7(f7.SUCCESS, 0, 2), this.m));
        } else {
            BluetoothGatt bluetoothGatt = this.c;
            if (bluetoothGatt == null) {
                this.b.post(new e5(this, new g7(f7.GATT_NULL, 0, 2), this.m));
            } else if (true != bluetoothGatt.requestMtu(i2)) {
                this.e = true;
                this.b.post(new e5(this, new g7(f7.START_FAIL, 0, 2), this.m));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0016, code lost:
        if (r7 != 3) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(int r7, int r8) {
        /*
        // Method dump skipped, instructions count: 235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k5.c(int, int):void");
    }

    @DexIgnore
    public final void d(int i2, f5 f5Var) {
        Iterator<ds> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new k4(it.next(), f5Var, i2));
            } catch (Exception e2) {
                d90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void e(v4 v4Var) {
        a90 a90 = new a90("connection_state_changed", v80.h, this.x, "", "", v4Var.f3712a == 0, null, null, null, g80.k(g80.k(new JSONObject(), jd0.w0, Integer.valueOf(v4Var.f3712a)), jd0.B0, ey1.a(w(v4Var.b))), 448);
        Long l2 = (Long) pm7.G(v4Var.c);
        if (l2 != null) {
            long longValue = l2.longValue();
            a90.b = longValue;
            g80.k(a90.n, jd0.X, Double.valueOf(hy1.f(longValue)));
            JSONObject jSONObject = a90.n;
            jd0 jd0 = jd0.Y;
            Long l3 = (Long) pm7.Q(v4Var.c);
            if (l3 != null) {
                longValue = l3.longValue();
            }
            g80.k(jSONObject, jd0, Double.valueOf(hy1.f(longValue)));
            JSONArray jSONArray = new JSONArray();
            Iterator<T> it = v4Var.c.iterator();
            while (it.hasNext()) {
                jSONArray.put(hy1.f(it.next().longValue()));
            }
            g80.k(a90.n, jd0.i5, jSONArray);
            d90.i.d(a90);
        }
    }

    @DexIgnore
    public final void f(b5 b5Var, b5 b5Var2) {
        this.b.post(new q4(this, new g7(f7.SUCCESS, 0, 2), b5Var, b5Var2));
        this.b.post(new i4(this, new g7(f7.SUCCESS, 0, 2), b5Var, b5Var2));
        Iterator<T> it = this.k.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new u4(it.next(), this, b5Var, b5Var2));
            } catch (Exception e2) {
                d90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void g(f5 f5Var) {
        Iterator<T> it = this.k.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new w4(it.next(), this, f5Var));
            } catch (Exception e2) {
                d90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void m(u5 u5Var) {
        u5Var.f = new s7(this, u5Var);
        u5Var.g = new t7(this, u5Var);
        this.h.b(u5Var);
    }

    @DexIgnore
    public final void n(u5 u5Var, r5 r5Var) {
        if (u5Var != null) {
            l4 l4Var = this.h;
            if (!pq7.a(u5Var, l4Var.b)) {
                u5Var.a(j4.b);
                l4Var.f2144a.remove(u5Var);
            }
            u5Var.e(r5Var);
        }
    }

    @DexIgnore
    public final void p(n6 n6Var) {
        g7 g7Var;
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new y4(this, new g7(f7.BLUETOOTH_OFF, 0, 2), n6Var, new byte[0]));
            return;
        }
        g7 g7Var2 = new g7(f7.SUCCESS, 0, 2);
        if (this.c == null) {
            g7Var = new g7(f7.GATT_NULL, 0, 2);
        } else {
            p6 p6Var = this.l.get(n6Var);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6Var != null ? p6Var.f2785a : null;
            if (bluetoothGattCharacteristic == null) {
                g7Var = new g7(f7.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.c;
                if (bluetoothGatt == null || true != bluetoothGatt.readCharacteristic(bluetoothGattCharacteristic)) {
                    this.e = true;
                    g7Var = new g7(f7.START_FAIL, 0, 2);
                } else {
                    g7Var = g7Var2;
                }
            }
        }
        if (g7Var.b != f7.SUCCESS) {
            this.b.post(new y4(this, g7Var, n6Var, new byte[0]));
        }
    }

    @DexIgnore
    public final void q(n6 n6Var, byte[] bArr) {
        ky1 ky1 = ky1.DEBUG;
        n6Var.name();
        dy1.e(bArr, null, 1, null);
        Iterator<ds> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new v7(it.next(), n6Var, bArr));
            } catch (Exception e2) {
                d90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void r(tk1.c cVar) {
        if (cVar != tk1.c.ENABLED) {
            this.e = true;
            c(0, x6.k.b);
        }
    }

    @DexIgnore
    public final void s(List<? extends BluetoothGattService> list) {
        for (BluetoothGattService bluetoothGattService : list) {
            for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
                o6 o6Var = p6.b;
                UUID uuid = bluetoothGattCharacteristic.getUuid();
                pq7.b(uuid, "characteristic.uuid");
                n6 a2 = o6Var.a(uuid);
                if (a2 != n6.UNKNOWN) {
                    this.l.put(a2, new p6(bluetoothGattCharacteristic));
                }
            }
        }
    }

    @DexIgnore
    public final void t(boolean z2) {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new g4(this, new g7(f7.BLUETOOTH_OFF, 0, 2), f5.DISCONNECTED));
            return;
        }
        int i2 = h5.f1429a[this.w.ordinal()];
        if (i2 == 1) {
            c(1, 0);
            BluetoothGatt bluetoothGatt = this.c;
            if (bluetoothGatt == null) {
                ky1 ky12 = ky1.DEBUG;
                BluetoothGatt connectGatt = Build.VERSION.SDK_INT >= 23 ? this.A.connectGatt(id0.i.a(), z2, this.g, 2) : this.A.connectGatt(id0.i.a(), z2, this.g);
                this.c = connectGatt;
                if (connectGatt == null) {
                    ky1 ky13 = ky1.DEBUG;
                    c(0, x6.f.b);
                    return;
                }
                this.d = System.currentTimeMillis();
                ky1 ky14 = ky1.DEBUG;
            } else if (this.e || true != bluetoothGatt.connect()) {
                ky1 ky15 = ky1.DEBUG;
                m(new x5(this.z));
                c(0, x6.f.b);
            } else {
                this.d = System.currentTimeMillis();
                ky1 ky16 = ky1.DEBUG;
            }
        } else if (i2 != 3) {
        } else {
            if (this.c != null) {
                this.b.post(new g4(this, new g7(f7.SUCCESS, 0, 2), f5.CONNECTED));
            } else {
                c(0, x6.c.b);
            }
        }
    }

    @DexIgnore
    public final boolean u(n6 n6Var, boolean z2) {
        BluetoothGatt bluetoothGatt;
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
        } else {
            p6 p6Var = this.l.get(n6Var);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6Var != null ? p6Var.f2785a : null;
            if (!(bluetoothGattCharacteristic == null || (bluetoothGatt = this.c) == null || true != bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, z2))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean v(BluetoothGatt bluetoothGatt) {
        Object obj;
        boolean z2 = false;
        if (!q3.f.a()) {
            obj = null;
        } else if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            obj = "Peripheral.refreshDeviceCache: Bluetooth Off.";
        } else {
            try {
                ky1 ky12 = ky1.DEBUG;
                Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                pq7.b(method, "gatt.javaClass.getMethod(\"refresh\")");
                Object invoke = method.invoke(bluetoothGatt, new Object[0]);
                if (invoke != null) {
                    z2 = ((Boolean) invoke).booleanValue();
                    obj = null;
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.Boolean");
                }
            } catch (Exception e2) {
                obj = fy1.a(e2);
                d90.i.i(e2);
            }
        }
        d90.i.d(new a90("refresh_device_cache", v80.e, this.x, "", e.a("UUID.randomUUID().toString()"), z2, null, null, null, g80.k(new JSONObject(), jd0.P0, obj != null ? obj : JSONObject.NULL), 448));
        return z2;
    }

    @DexIgnore
    public final f5 w(int i2) {
        return i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? f5.DISCONNECTED : f5.DISCONNECTING : f5.CONNECTED : f5.CONNECTING : f5.DISCONNECTED;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.A, i2);
        }
    }

    @DexIgnore
    public final void x() {
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new m4(this, new g7(f7.BLUETOOTH_OFF, 0, 2), H(), H()));
        } else if (r4.BONDED == H()) {
            this.b.post(new m4(this, new g7(f7.SUCCESS, 0, 2), H(), H()));
        } else if (!this.A.createBond()) {
            this.b.post(new m4(this, new g7(f7.START_FAIL, 0, 2), H(), H()));
        }
    }

    @DexIgnore
    public final void y(n6 n6Var, byte[] bArr) {
        g7 g7Var;
        if (tk1.k.w() != tk1.c.ENABLED) {
            ky1 ky1 = ky1.DEBUG;
            this.b.post(new g5(this, new g7(f7.BLUETOOTH_OFF, 0, 2), n6Var, new byte[0]));
            return;
        }
        g7 g7Var2 = new g7(f7.SUCCESS, 0, 2);
        if (this.c == null) {
            g7Var = new g7(f7.GATT_NULL, 0, 2);
        } else {
            p6 p6Var = this.l.get(n6Var);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6Var != null ? p6Var.f2785a : null;
            if (bluetoothGattCharacteristic != null) {
                bluetoothGattCharacteristic.setValue(bArr);
            }
            if (bluetoothGattCharacteristic == null) {
                g7Var = new g7(f7.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.c;
                if (bluetoothGatt == null || true != bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                    this.e = true;
                    g7Var = new g7(f7.START_FAIL, 0, 2);
                } else {
                    g7Var = g7Var2;
                }
            }
        }
        ky1 ky12 = ky1.DEBUG;
        int length = bArr.length;
        dy1.e(bArr, null, 1, null);
        if (g7Var.b != f7.SUCCESS) {
            this.b.post(new g5(this, g7Var, n6Var, new byte[0]));
        }
    }

    @DexIgnore
    public final void z(boolean z2) {
        this.y = z2;
    }
}
