package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n46 implements Factory<m46> {
    @DexIgnore
    public static m46 a(LoaderManager loaderManager, h46 h46, int i, uq4 uq4, y56 y56, l56 l56, k66 k66, lt5 lt5) {
        return new m46(loaderManager, h46, i, uq4, y56, l56, k66, lt5);
    }
}
