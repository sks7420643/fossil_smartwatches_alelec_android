package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a76 implements Factory<z66> {
    @DexIgnore
    public static z66 a(y66 y66, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, jb6 jb6, UserRepository userRepository, zm5 zm5, FileRepository fileRepository, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository2, DianaWatchFaceRepository dianaWatchFaceRepository, PortfolioApp portfolioApp, uo5 uo5, s77 s77, yb7 yb7) {
        return new z66(y66, watchAppRepository, complicationRepository, jb6, userRepository, zm5, fileRepository, customizeRealDataRepository, userRepository2, dianaWatchFaceRepository, portfolioApp, uo5, s77, yb7);
    }
}
