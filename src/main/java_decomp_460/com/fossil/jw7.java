package com.fossil;

import com.fossil.iw7;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jw7 extends hw7 {
    @DexIgnore
    public abstract Thread t0();

    @DexIgnore
    public final void u0(long j, iw7.c cVar) {
        if (nv7.a()) {
            if (!(this != pv7.i)) {
                throw new AssertionError();
            }
        }
        pv7.i.F0(j, cVar);
    }

    @DexIgnore
    public final void v0() {
        Thread t0 = t0();
        if (Thread.currentThread() != t0) {
            xx7 a2 = yx7.a();
            if (a2 != null) {
                a2.e(t0);
            } else {
                LockSupport.unpark(t0);
            }
        }
    }
}
