package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y02 implements Factory<x02> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Context> f4223a;
    @DexIgnore
    public /* final */ Provider<t32> b;
    @DexIgnore
    public /* final */ Provider<t32> c;

    @DexIgnore
    public y02(Provider<Context> provider, Provider<t32> provider2, Provider<t32> provider3) {
        this.f4223a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static y02 a(Provider<Context> provider, Provider<t32> provider2, Provider<t32> provider3) {
        return new y02(provider, provider2, provider3);
    }

    @DexIgnore
    /* renamed from: b */
    public x02 get() {
        return new x02(this.f4223a.get(), this.b.get(), this.c.get());
    }
}
