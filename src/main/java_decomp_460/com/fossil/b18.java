package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b18 {
    @DexIgnore
    void onFailure(a18 a18, IOException iOException);

    @DexIgnore
    void onResponse(a18 a18, Response response) throws IOException;
}
