package com.fossil;

import com.fossil.zu0;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw5 extends zu0.d<ActivitySummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        pq7.c(activitySummary, "oldItem");
        pq7.c(activitySummary2, "newItem");
        return pq7.a(activitySummary, activitySummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        pq7.c(activitySummary, "oldItem");
        pq7.c(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
