package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p22 implements Factory<l22> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ p22 f2769a; // = new p22();

    @DexIgnore
    public static p22 a() {
        return f2769a;
    }

    @DexIgnore
    public static l22 c() {
        l22 c = m22.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    /* renamed from: b */
    public l22 get() {
        return c();
    }
}
