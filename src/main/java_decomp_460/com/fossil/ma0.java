package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma0 extends va0 {
    @DexIgnore
    public static /* final */ la0 CREATOR; // = new la0(null);
    @DexIgnore
    public /* final */ double c;

    @DexIgnore
    public ma0(double d) {
        super(aa0.DELAY);
        this.c = d;
    }

    @DexIgnore
    public /* synthetic */ ma0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readDouble();
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort((short) ((int) ((this.c * ((double) 1000)) / 100.0d)));
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ma0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((ma0) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.DelayInstr");
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int hashCode() {
        return (int) this.c;
    }

    @DexIgnore
    @Override // com.fossil.va0, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.W3, Double.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.c);
        }
    }
}
