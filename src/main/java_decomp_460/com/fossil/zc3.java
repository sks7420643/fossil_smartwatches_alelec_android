package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zc3 extends ds2 implements yc3 {
    @DexIgnore
    public zc3() {
        super("com.google.android.gms.maps.internal.IOnPolylineClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c0(bs2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
