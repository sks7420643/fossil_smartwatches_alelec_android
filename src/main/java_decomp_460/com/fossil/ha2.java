package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha2 implements ia2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ ea2 f1455a;

    @DexIgnore
    public ha2(ea2 ea2) {
        this.f1455a = ea2;
    }

    @DexIgnore
    @Override // com.fossil.ia2
    public final void a(BasePendingResult<?> basePendingResult) {
        this.f1455a.f901a.remove(basePendingResult);
        basePendingResult.r();
    }
}
