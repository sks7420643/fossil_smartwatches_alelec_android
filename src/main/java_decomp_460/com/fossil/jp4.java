package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp4 implements Factory<uk5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1787a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<no4> c;
    @DexIgnore
    public /* final */ Provider<on5> d;

    @DexIgnore
    public jp4(uo4 uo4, Provider<Context> provider, Provider<no4> provider2, Provider<on5> provider3) {
        this.f1787a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static jp4 a(uo4 uo4, Provider<Context> provider, Provider<no4> provider2, Provider<on5> provider3) {
        return new jp4(uo4, provider, provider2, provider3);
    }

    @DexIgnore
    public static uk5 c(uo4 uo4, Context context, no4 no4, on5 on5) {
        uk5 q = uo4.q(context, no4, on5);
        lk7.c(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    /* renamed from: b */
    public uk5 get() {
        return c(this.f1787a, this.b.get(), this.c.get(), this.d.get());
    }
}
