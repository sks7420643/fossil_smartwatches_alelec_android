package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x64 implements b74 {
    @DexIgnore
    @Override // com.fossil.b74
    public <T> Set<T> c(Class<T> cls) {
        return b(cls).get();
    }

    @DexIgnore
    @Override // com.fossil.b74
    public <T> T get(Class<T> cls) {
        mg4<T> a2 = a(cls);
        if (a2 == null) {
            return null;
        }
        return a2.get();
    }
}
