package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lc2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends ql2 implements lc2 {
        @DexIgnore
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @DexIgnore
        @Override // com.fossil.ql2
        public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                i1(parcel.readInt(), parcel.readStrongBinder(), (Bundle) sl2.b(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                W0(parcel.readInt(), (Bundle) sl2.b(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                d2(parcel.readInt(), parcel.readStrongBinder(), (ee2) sl2.b(parcel, ee2.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    @DexIgnore
    void W0(int i, Bundle bundle) throws RemoteException;

    @DexIgnore
    void d2(int i, IBinder iBinder, ee2 ee2) throws RemoteException;

    @DexIgnore
    void i1(int i, IBinder iBinder, Bundle bundle) throws RemoteException;
}
