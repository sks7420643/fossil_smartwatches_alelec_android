package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.plugins.FilesDumperPlugin;
import com.fossil.ta4;
import com.fossil.wearables.fsl.countdown.CountDown;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Explore;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y94 implements wd4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wd4 f4262a; // = new y94();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements sd4<ta4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f4263a; // = new a();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.b bVar, td4 td4) throws IOException {
            td4.f("key", bVar.b());
            td4.f("value", bVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements sd4<ta4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f4264a; // = new b();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4 ta4, td4 td4) throws IOException {
            td4.f("sdkVersion", ta4.i());
            td4.f("gmpAppId", ta4.e());
            td4.c("platform", ta4.h());
            td4.f("installationUuid", ta4.f());
            td4.f("buildVersion", ta4.c());
            td4.f("displayVersion", ta4.d());
            td4.f(Constants.SESSION, ta4.j());
            td4.f("ndkPayload", ta4.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sd4<ta4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ c f4265a; // = new c();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.c cVar, td4 td4) throws IOException {
            td4.f(FilesDumperPlugin.NAME, cVar.b());
            td4.f("orgId", cVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements sd4<ta4.c.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ d f4266a; // = new d();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.c.b bVar, td4 td4) throws IOException {
            td4.f("filename", bVar.c());
            td4.f("contents", bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements sd4<ta4.d.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e f4267a; // = new e();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.a aVar, td4 td4) throws IOException {
            td4.f("identifier", aVar.c());
            td4.f("version", aVar.f());
            td4.f("displayVersion", aVar.b());
            td4.f("organization", aVar.e());
            td4.f("installationUuid", aVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements sd4<ta4.d.a.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ f f4268a; // = new f();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.a.b bVar, td4 td4) throws IOException {
            td4.f("clsId", bVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements sd4<ta4.d.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ g f4269a; // = new g();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.c cVar, td4 td4) throws IOException {
            td4.c("arch", cVar.b());
            td4.f(DeviceRequestsHelper.DEVICE_INFO_MODEL, cVar.f());
            td4.c("cores", cVar.c());
            td4.b("ram", cVar.h());
            td4.b("diskSpace", cVar.d());
            td4.a("simulator", cVar.j());
            td4.c("state", cVar.i());
            td4.f("manufacturer", cVar.e());
            td4.f("modelClass", cVar.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements sd4<ta4.d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ h f4270a; // = new h();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d dVar, td4 td4) throws IOException {
            td4.f("generator", dVar.f());
            td4.f("identifier", dVar.i());
            td4.b("startedAt", dVar.k());
            td4.f(CountDown.COLUMN_ENDED_AT, dVar.d());
            td4.a("crashed", dVar.m());
            td4.f("app", dVar.b());
            td4.f("user", dVar.l());
            td4.f("os", dVar.j());
            td4.f("device", dVar.c());
            td4.f("events", dVar.e());
            td4.c("generatorType", dVar.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements sd4<ta4.d.AbstractC0224d.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ i f4271a; // = new i();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a aVar, td4 td4) throws IOException {
            td4.f("execution", aVar.d());
            td4.f("customAttributes", aVar.c());
            td4.f(Explore.COLUMN_BACKGROUND, aVar.b());
            td4.c("uiOrientation", aVar.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements sd4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ j f4272a; // = new j();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b.AbstractC0226a aVar, td4 td4) throws IOException {
            td4.b("baseAddress", aVar.b());
            td4.b("size", aVar.d());
            td4.f("name", aVar.c());
            td4.f("uuid", aVar.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements sd4<ta4.d.AbstractC0224d.a.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ k f4273a; // = new k();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b bVar, td4 td4) throws IOException {
            td4.f("threads", bVar.e());
            td4.f("exception", bVar.c());
            td4.f("signal", bVar.d());
            td4.f("binaries", bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements sd4<ta4.d.AbstractC0224d.a.b.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ l f4274a; // = new l();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b.c cVar, td4 td4) throws IOException {
            td4.f("type", cVar.f());
            td4.f("reason", cVar.e());
            td4.f("frames", cVar.c());
            td4.f("causedBy", cVar.b());
            td4.c("overflowCount", cVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements sd4<ta4.d.AbstractC0224d.a.b.AbstractC0230d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ m f4275a; // = new m();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b.AbstractC0230d dVar, td4 td4) throws IOException {
            td4.f("name", dVar.d());
            td4.f("code", dVar.c());
            td4.b("address", dVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements sd4<ta4.d.AbstractC0224d.a.b.e> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ n f4276a; // = new n();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b.e eVar, td4 td4) throws IOException {
            td4.f("name", eVar.d());
            td4.c("importance", eVar.c());
            td4.f("frames", eVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements sd4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ o f4277a; // = new o();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.a.b.e.AbstractC0233b bVar, td4 td4) throws IOException {
            td4.b("pc", bVar.e());
            td4.f("symbol", bVar.f());
            td4.f("file", bVar.b());
            td4.b(Constants.JSON_KEY_OFFSET, bVar.d());
            td4.c("importance", bVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements sd4<ta4.d.AbstractC0224d.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ p f4278a; // = new p();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.c cVar, td4 td4) throws IOException {
            td4.f(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, cVar.b());
            td4.c("batteryVelocity", cVar.c());
            td4.a("proximityOn", cVar.g());
            td4.c("orientation", cVar.e());
            td4.b("ramUsed", cVar.f());
            td4.b("diskUsed", cVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements sd4<ta4.d.AbstractC0224d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ q f4279a; // = new q();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d dVar, td4 td4) throws IOException {
            td4.b("timestamp", dVar.e());
            td4.f("type", dVar.f());
            td4.f("app", dVar.b());
            td4.f("device", dVar.c());
            td4.f("log", dVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements sd4<ta4.d.AbstractC0224d.AbstractC0235d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ r f4280a; // = new r();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.AbstractC0224d.AbstractC0235d dVar, td4 td4) throws IOException {
            td4.f("content", dVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements sd4<ta4.d.e> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ s f4281a; // = new s();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.e eVar, td4 td4) throws IOException {
            td4.c("platform", eVar.c());
            td4.f("version", eVar.d());
            td4.f("buildVersion", eVar.b());
            td4.a("jailbroken", eVar.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t implements sd4<ta4.d.f> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ t f4282a; // = new t();

        @DexIgnore
        /* renamed from: b */
        public void a(ta4.d.f fVar, td4 td4) throws IOException {
            td4.f("identifier", fVar.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.wd4
    public void a(xd4<?> xd4) {
        xd4.a(ta4.class, b.f4264a);
        xd4.a(z94.class, b.f4264a);
        xd4.a(ta4.d.class, h.f4270a);
        xd4.a(da4.class, h.f4270a);
        xd4.a(ta4.d.a.class, e.f4267a);
        xd4.a(ea4.class, e.f4267a);
        xd4.a(ta4.d.a.b.class, f.f4268a);
        xd4.a(fa4.class, f.f4268a);
        xd4.a(ta4.d.f.class, t.f4282a);
        xd4.a(sa4.class, t.f4282a);
        xd4.a(ta4.d.e.class, s.f4281a);
        xd4.a(ra4.class, s.f4281a);
        xd4.a(ta4.d.c.class, g.f4269a);
        xd4.a(ga4.class, g.f4269a);
        xd4.a(ta4.d.AbstractC0224d.class, q.f4279a);
        xd4.a(ha4.class, q.f4279a);
        xd4.a(ta4.d.AbstractC0224d.a.class, i.f4271a);
        xd4.a(ia4.class, i.f4271a);
        xd4.a(ta4.d.AbstractC0224d.a.b.class, k.f4273a);
        xd4.a(ja4.class, k.f4273a);
        xd4.a(ta4.d.AbstractC0224d.a.b.e.class, n.f4276a);
        xd4.a(na4.class, n.f4276a);
        xd4.a(ta4.d.AbstractC0224d.a.b.e.AbstractC0233b.class, o.f4277a);
        xd4.a(oa4.class, o.f4277a);
        xd4.a(ta4.d.AbstractC0224d.a.b.c.class, l.f4274a);
        xd4.a(la4.class, l.f4274a);
        xd4.a(ta4.d.AbstractC0224d.a.b.AbstractC0230d.class, m.f4275a);
        xd4.a(ma4.class, m.f4275a);
        xd4.a(ta4.d.AbstractC0224d.a.b.AbstractC0226a.class, j.f4272a);
        xd4.a(ka4.class, j.f4272a);
        xd4.a(ta4.b.class, a.f4263a);
        xd4.a(aa4.class, a.f4263a);
        xd4.a(ta4.d.AbstractC0224d.c.class, p.f4278a);
        xd4.a(pa4.class, p.f4278a);
        xd4.a(ta4.d.AbstractC0224d.AbstractC0235d.class, r.f4280a);
        xd4.a(qa4.class, r.f4280a);
        xd4.a(ta4.c.class, c.f4265a);
        xd4.a(ba4.class, c.f4265a);
        xd4.a(ta4.c.b.class, d.f4266a);
        xd4.a(ca4.class, d.f4266a);
    }
}
