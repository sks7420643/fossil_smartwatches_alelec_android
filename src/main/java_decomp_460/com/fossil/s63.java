package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s63 implements p63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3208a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.service.database_return_empty_collection", true);

    @DexIgnore
    @Override // com.fossil.p63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.p63
    public final boolean zzb() {
        return f3208a.o().booleanValue();
    }
}
