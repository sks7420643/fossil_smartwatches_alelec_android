package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class mw4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2429a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[vy4.values().length];
        f2429a = iArr;
        iArr[vy4.EDIT.ordinal()] = 1;
        f2429a[vy4.ADD_FRIENDS.ordinal()] = 2;
        f2429a[vy4.LEAVE.ordinal()] = 3;
        int[] iArr2 = new int[iz4.values().length];
        b = iArr2;
        iArr2[iz4.JOIN_TOO_MUCH.ordinal()] = 1;
        b[iz4.NEED_ACTIVE_DEVICE.ordinal()] = 2;
        b[iz4.SUGGEST_FIND_FRIEND.ordinal()] = 3;
    }
    */
}
