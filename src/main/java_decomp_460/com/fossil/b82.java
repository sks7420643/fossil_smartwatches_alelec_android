package com.fossil;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b82 extends ra2 {
    @DexIgnore
    public /* final */ aj0<g72<?>> g; // = new aj0<>();
    @DexIgnore
    public l72 h;

    @DexIgnore
    public b82(o72 o72) {
        super(o72);
        this.b.b1("ConnectionlessLifecycleHelper", this);
    }

    @DexIgnore
    public static void q(Activity activity, l72 l72, g72<?> g72) {
        o72 c = LifecycleCallback.c(activity);
        b82 b82 = (b82) c.S2("ConnectionlessLifecycleHelper", b82.class);
        if (b82 == null) {
            b82 = new b82(c);
        }
        b82.h = l72;
        rc2.l(g72, "ApiKey cannot be null");
        b82.g.add(g72);
        l72.l(b82);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void h() {
        super.h();
        s();
    }

    @DexIgnore
    @Override // com.fossil.ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        s();
    }

    @DexIgnore
    @Override // com.fossil.ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void k() {
        super.k();
        this.h.q(this);
    }

    @DexIgnore
    @Override // com.fossil.ra2
    public final void m(z52 z52, int i) {
        this.h.h(z52, i);
    }

    @DexIgnore
    @Override // com.fossil.ra2
    public final void o() {
        this.h.D();
    }

    @DexIgnore
    public final aj0<g72<?>> r() {
        return this.g;
    }

    @DexIgnore
    public final void s() {
        if (!this.g.isEmpty()) {
            this.h.l(this);
        }
    }
}
