package com.fossil;

import com.fossil.gn1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fb extends nq7 implements rp7<byte[], gn1> {
    @DexIgnore
    public fb(gn1.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final fs7 getOwner() {
        return er7.b(gn1.a.class);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/TimeConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public gn1 invoke(byte[] bArr) {
        return ((gn1.a) this.receiver).a(bArr);
    }
}
