package com.fossil;

import android.util.Log;
import com.fossil.m62;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb2 implements ht3<Map<g72<?>, String>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public t72 f910a;
    @DexIgnore
    public /* final */ /* synthetic */ db2 b;

    @DexIgnore
    public eb2(db2 db2, t72 t72) {
        this.b = db2;
        this.f910a = t72;
    }

    @DexIgnore
    public final void a() {
        this.f910a.onComplete();
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3<Map<g72<?>, String>> nt3) {
        db2.p(this.b).lock();
        try {
            if (!db2.z(this.b)) {
                this.f910a.onComplete();
                return;
            }
            if (nt3.q()) {
                db2.x(this.b, new zi0(db2.K(this.b).size()));
                for (bb2 bb2 : db2.K(this.b).values()) {
                    db2.E(this.b).put(bb2.a(), z52.f);
                }
            } else if (nt3.l() instanceof o62) {
                o62 o62 = (o62) nt3.l();
                if (db2.C(this.b)) {
                    db2.x(this.b, new zi0(db2.K(this.b).size()));
                    for (bb2 bb22 : db2.K(this.b).values()) {
                        g72 a2 = bb22.a();
                        z52 connectionResult = o62.getConnectionResult((q62<? extends m62.d>) bb22);
                        if (db2.r(this.b, bb22, connectionResult)) {
                            db2.E(this.b).put(a2, new z52(16));
                        } else {
                            db2.E(this.b).put(a2, connectionResult);
                        }
                    }
                } else {
                    db2.x(this.b, o62.zaj());
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", nt3.l());
                db2.x(this.b, Collections.emptyMap());
            }
            if (this.b.c()) {
                db2.B(this.b).putAll(db2.E(this.b));
                if (db2.D(this.b) == null) {
                    db2.G(this.b);
                    db2.H(this.b);
                    db2.J(this.b).signalAll();
                }
            }
            this.f910a.onComplete();
            db2.p(this.b).unlock();
        } finally {
            db2.p(this.b).unlock();
        }
    }
}
