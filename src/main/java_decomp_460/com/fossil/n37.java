package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n37 implements MembersInjector<l37> {
    @DexIgnore
    public static void a(l37 l37, DeviceRepository deviceRepository) {
        l37.h = deviceRepository;
    }

    @DexIgnore
    public static void b(l37 l37, NotificationSettingsDatabase notificationSettingsDatabase) {
        l37.i = notificationSettingsDatabase;
    }
}
