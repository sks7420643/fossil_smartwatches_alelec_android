package com.fossil;

import com.fossil.u24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a34<K, V> implements Map<K, V>, Serializable {
    @DexIgnore
    public static /* final */ Map.Entry<?, ?>[] EMPTY_ENTRY_ARRAY; // = new Map.Entry[0];
    @DexIgnore
    @LazyInit
    public transient h34<Map.Entry<K, V>> b;
    @DexIgnore
    @LazyInit
    public transient h34<K> c;
    @DexIgnore
    @LazyInit
    public transient u24<V> d;
    @DexIgnore
    @LazyInit
    public transient i34<K, V> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends h54<K> {
        @DexIgnore
        public /* final */ /* synthetic */ h54 b;

        @DexIgnore
        public a(a34 a34, h54 h54) {
            this.b = h54;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public K next() {
            return (K) ((Map.Entry) this.b.next()).getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Comparator<? super V> f188a;
        @DexIgnore
        public b34<K, V>[] b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b() {
            this(4);
        }

        @DexIgnore
        public b(int i) {
            this.b = new b34[i];
            this.c = 0;
            this.d = false;
        }

        @DexIgnore
        public a34<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return a34.of();
            }
            if (i == 1) {
                return a34.of((Object) this.b[0].getKey(), (Object) this.b[0].getValue());
            }
            if (this.f188a != null) {
                if (this.d) {
                    this.b = (b34[]) h44.a(this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, i44.from(this.f188a).onResultOf(x34.s()));
            }
            this.d = this.c == this.b.length;
            return p44.fromEntryArray(this.c, this.b);
        }

        @DexIgnore
        public final void b(int i) {
            b34<K, V>[] b34Arr = this.b;
            if (i > b34Arr.length) {
                this.b = (b34[]) h44.a(b34Arr, u24.b.d(b34Arr.length, i));
                this.d = false;
            }
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> c(K k, V v) {
            b(this.c + 1);
            b34<K, V> entryOf = a34.entryOf(k, v);
            b34<K, V>[] b34Arr = this.b;
            int i = this.c;
            this.c = i + 1;
            b34Arr[i] = entryOf;
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.a34$b<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public b<K, V> d(Map.Entry<? extends K, ? extends V> entry) {
            return c(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> e(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            if (iterable instanceof Collection) {
                b(((Collection) iterable).size() + this.c);
            }
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                d(entry);
            }
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> f(Map<? extends K, ? extends V> map) {
            return e(map.entrySet());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends a34<K, V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends c34<K, V> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
            public h54<Map.Entry<K, V>> iterator() {
                return c.this.entryIterator();
            }

            @DexIgnore
            @Override // com.fossil.c34
            public a34<K, V> map() {
                return c.this;
            }
        }

        @DexIgnore
        @Override // com.fossil.a34
        public h34<Map.Entry<K, V>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        public abstract h54<Map.Entry<K, V>> entryIterator();

        @DexIgnore
        @Override // com.fossil.a34, com.fossil.a34, java.util.Map
        public /* bridge */ /* synthetic */ Set entrySet() {
            return a34.super.entrySet();
        }

        @DexIgnore
        @Override // com.fossil.a34, com.fossil.a34, java.util.Map
        public /* bridge */ /* synthetic */ Set keySet() {
            return a34.super.keySet();
        }

        @DexIgnore
        @Override // com.fossil.a34, com.fossil.a34, java.util.Map
        public /* bridge */ /* synthetic */ Collection values() {
            return a34.super.values();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends c<K, h34<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h54<Map.Entry<K, h34<V>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a34$d$a$a")
            /* renamed from: com.fossil.a34$d$a$a  reason: collision with other inner class name */
            public class C0007a extends t14<K, h34<V>> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry b;

                @DexIgnore
                public C0007a(a aVar, Map.Entry entry) {
                    this.b = entry;
                }

                @DexIgnore
                /* renamed from: a */
                public h34<V> getValue() {
                    return h34.of(this.b.getValue());
                }

                @DexIgnore
                @Override // java.util.Map.Entry, com.fossil.t14
                public K getKey() {
                    return (K) this.b.getKey();
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.b = it;
            }

            @DexIgnore
            /* renamed from: a */
            public Map.Entry<K, h34<V>> next() {
                return new C0007a(this, (Map.Entry) this.b.next());
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }
        }

        @DexIgnore
        public d() {
        }

        @DexIgnore
        public /* synthetic */ d(a34 a34, a aVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.a34
        public boolean containsKey(Object obj) {
            return a34.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.a34.c
        public h54<Map.Entry<K, h34<V>>> entryIterator() {
            return new a(this, a34.this.entrySet().iterator());
        }

        @DexIgnore
        @Override // com.fossil.a34, java.util.Map
        public h34<V> get(Object obj) {
            Object obj2 = a34.this.get(obj);
            if (obj2 == null) {
                return null;
            }
            return h34.of(obj2);
        }

        @DexIgnore
        @Override // com.fossil.a34
        public int hashCode() {
            return a34.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.a34
        public boolean isHashCodeFast() {
            return a34.this.isHashCodeFast();
        }

        @DexIgnore
        @Override // com.fossil.a34
        public boolean isPartialView() {
            return a34.this.isPartialView();
        }

        @DexIgnore
        @Override // com.fossil.a34.c, com.fossil.a34, com.fossil.a34, java.util.Map
        public h34<K> keySet() {
            return a34.this.keySet();
        }

        @DexIgnore
        public int size() {
            return a34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] keys;
        @DexIgnore
        public /* final */ Object[] values;

        @DexIgnore
        public e(a34<?, ?> a34) {
            this.keys = new Object[a34.size()];
            this.values = new Object[a34.size()];
            Iterator it = a34.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }

        @DexIgnore
        public Object createMap(b<Object, Object> bVar) {
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return bVar.a();
                }
                bVar.c(objArr[i], this.values[i]);
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new b<>(this.keys.length));
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> a34<K, V> a(EnumMap<K, ? extends V> enumMap) {
        EnumMap enumMap2 = new EnumMap((EnumMap) enumMap);
        for (Map.Entry<K, V> entry : enumMap2.entrySet()) {
            a24.a(entry.getKey(), entry.getValue());
        }
        return w24.asImmutable(enumMap2);
    }

    @DexIgnore
    public static <K, V> b<K, V> builder() {
        return new b<>();
    }

    @DexIgnore
    public static void checkNoConflict(boolean z, String str, Map.Entry<?, ?> entry, Map.Entry<?, ?> entry2) {
        if (!z) {
            throw new IllegalArgumentException("Multiple entries with same " + str + ": " + entry + " and " + entry2);
        }
    }

    @DexIgnore
    public static <K, V> a34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) o34.i(iterable, EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return p44.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static <K, V> a34<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if ((map instanceof a34) && !(map instanceof k34)) {
            a34<K, V> a34 = (a34) map;
            if (!a34.isPartialView()) {
                return a34;
            }
        } else if (map instanceof EnumMap) {
            return a((EnumMap) map);
        }
        return copyOf(map.entrySet());
    }

    @DexIgnore
    public static <K, V> b34<K, V> entryOf(K k, V v) {
        return new b34<>(k, v);
    }

    @DexIgnore
    public static <K, V> a34<K, V> of() {
        return t24.of();
    }

    @DexIgnore
    public static <K, V> a34<K, V> of(K k, V v) {
        return t24.of((Object) k, (Object) v);
    }

    @DexIgnore
    public static <K, V> a34<K, V> of(K k, V v, K k2, V v2) {
        return p44.fromEntries(entryOf(k, v), entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> a34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return p44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> a34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return p44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> a34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return p44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5));
    }

    @DexIgnore
    public i34<K, V> asMultimap() {
        if (isEmpty()) {
            return i34.of();
        }
        i34<K, V> i34 = this.e;
        if (i34 != null) {
            return i34;
        }
        i34<K, V> i342 = new i34<>(new d(this, null), size(), null);
        this.e = i342;
        return i342;
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    @DexIgnore
    public abstract h34<Map.Entry<K, V>> createEntrySet();

    @DexIgnore
    public h34<K> createKeySet() {
        return isEmpty() ? h34.of() : new d34(this);
    }

    @DexIgnore
    public u24<V> createValues() {
        return new e34(this);
    }

    @DexIgnore
    @Override // java.util.Map
    public h34<Map.Entry<K, V>> entrySet() {
        h34<Map.Entry<K, V>> h34 = this.b;
        if (h34 != null) {
            return h34;
        }
        h34<Map.Entry<K, V>> createEntrySet = createEntrySet();
        this.b = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return x34.d(this, obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public abstract V get(Object obj);

    @DexIgnore
    public int hashCode() {
        return x44.b(entrySet());
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    public h54<K> keyIterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // java.util.Map
    public h34<K> keySet() {
        h34<K> h34 = this.c;
        if (h34 != null) {
            return h34;
        }
        h34<K> createKeySet = createKeySet();
        this.c = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return x34.p(this);
    }

    @DexIgnore
    @Override // java.util.Map
    public u24<V> values() {
        u24<V> u24 = this.d;
        if (u24 != null) {
            return u24;
        }
        u24<V> createValues = createValues();
        this.d = createValues;
        return createValues;
    }

    @DexIgnore
    public Object writeReplace() {
        return new e(this);
    }
}
