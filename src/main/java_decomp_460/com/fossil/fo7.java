package com.fossil;

import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo7 {
    @DexIgnore
    public static final void a(int i, int i2) {
        if (i2 > i) {
            throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
        }
    }

    @DexIgnore
    public static final eo7 b(zn7 zn7) {
        return (eo7) zn7.getClass().getAnnotation(eo7.class);
    }

    @DexIgnore
    public static final int c(zn7 zn7) {
        try {
            Field declaredField = zn7.getClass().getDeclaredField("label");
            pq7.b(declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(zn7);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception e) {
            return -1;
        }
    }

    @DexIgnore
    public static final StackTraceElement d(zn7 zn7) {
        String str;
        pq7.c(zn7, "$this$getStackTraceElementImpl");
        eo7 b = b(zn7);
        if (b == null) {
            return null;
        }
        a(1, b.v());
        int c = c(zn7);
        int i = c < 0 ? -1 : b.l()[c];
        String b2 = ho7.c.b(zn7);
        if (b2 == null) {
            str = b.c();
        } else {
            str = b2 + '/' + b.c();
        }
        return new StackTraceElement(str, b.m(), b.f(), i);
    }
}
