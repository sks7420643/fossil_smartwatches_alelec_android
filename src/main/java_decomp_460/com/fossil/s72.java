package com.fossil;

import android.os.RemoteException;
import com.fossil.m62;
import com.fossil.m62.b;
import com.fossil.p72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s72<A extends m62.b, L> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ p72<L> f3212a;
    @DexIgnore
    public /* final */ b62[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public s72(p72<L> p72) {
        this.f3212a = p72;
    }

    @DexIgnore
    public void a() {
        this.f3212a.a();
    }

    @DexIgnore
    public p72.a<L> b() {
        return this.f3212a.b();
    }

    @DexIgnore
    public b62[] c() {
        return this.b;
    }

    @DexIgnore
    public abstract void d(A a2, ot3<Void> ot3) throws RemoteException;

    @DexIgnore
    public final boolean e() {
        return this.c;
    }
}
