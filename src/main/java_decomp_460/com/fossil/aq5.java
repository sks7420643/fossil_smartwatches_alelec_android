package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.vr5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aq5 extends xp5 {
    @DexIgnore
    public static /* final */ String i; // = aq5.class.getSimpleName();
    @DexIgnore
    public /* final */ HashMap<String, Long> f; // = new HashMap<>();
    @DexIgnore
    public vr5 g;
    @DexIgnore
    public on5 h;

    @DexIgnore
    @Override // com.fossil.xp5
    public void b(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    @Override // com.fossil.xp5
    public void c(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onMissedCall : " + str);
        if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.d0.J()) && !TextUtils.isEmpty(str)) {
            this.g.U(str, date, vr5.g.PICKED);
        }
    }

    @DexIgnore
    @Override // com.fossil.xp5
    public void d(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onIncomingCallStarted : " + str);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.d0.J())) {
            j(str);
        } else {
            i(str, date);
        }
    }

    @DexIgnore
    @Override // com.fossil.xp5
    public void e(Context context, String str, Date date) {
    }

    @DexIgnore
    @Override // com.fossil.xp5
    public void f(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    @Override // com.fossil.xp5
    public void g(Context context, String str, Date date) {
    }

    @DexIgnore
    public final void h() {
        synchronized (this.f) {
            long currentTimeMillis = System.currentTimeMillis();
            int size = this.f.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Clean past call. Size = " + size);
            LinkedList<String> linkedList = new LinkedList();
            for (Map.Entry<String, Long> entry : this.f.entrySet()) {
                String key = entry.getKey();
                if (currentTimeMillis - entry.getValue().longValue() > 900000) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = i;
                    local2.d(str2, "Adding key to remove - key = " + key);
                    linkedList.add(key);
                }
            }
            for (String str3 : linkedList) {
                FLogger.INSTANCE.getLocal().d(i, "Dumping old call");
                this.f.remove(str3);
            }
        }
    }

    @DexIgnore
    public final void i(String str, Date date) {
        if (!TextUtils.isEmpty(str)) {
            this.g.U(str, date, vr5.g.RINGING);
        }
    }

    @DexIgnore
    public final void j(String str) {
        String d = p47.d(str);
        boolean Z = this.h.Z();
        synchronized (this.f) {
            this.f.put(d, Long.valueOf(System.currentTimeMillis()));
        }
        if (Z) {
            FLogger.INSTANCE.getLocal().d(i, "Phone Receiver - blocked by DND mode");
            return;
        }
        en5.i.a().h(new NotificationInfo(NotificationSource.CALL, d, "", ""));
        h();
    }
}
