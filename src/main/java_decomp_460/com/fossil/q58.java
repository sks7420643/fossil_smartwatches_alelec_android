package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q58 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map optionMap; // = new HashMap();
    @DexIgnore
    public boolean required;
    @DexIgnore
    public String selected;

    @DexIgnore
    public q58 addOption(p58 p58) {
        this.optionMap.put(p58.getKey(), p58);
        return this;
    }

    @DexIgnore
    public Collection getNames() {
        return this.optionMap.keySet();
    }

    @DexIgnore
    public Collection getOptions() {
        return this.optionMap.values();
    }

    @DexIgnore
    public String getSelected() {
        return this.selected;
    }

    @DexIgnore
    public boolean isRequired() {
        return this.required;
    }

    @DexIgnore
    public void setRequired(boolean z) {
        this.required = z;
    }

    @DexIgnore
    public void setSelected(p58 p58) throws h58 {
        String str = this.selected;
        if (str == null || str.equals(p58.getOpt())) {
            this.selected = p58.getOpt();
            return;
        }
        throw new h58(this, p58);
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = getOptions().iterator();
        stringBuffer.append("[");
        while (it.hasNext()) {
            p58 p58 = (p58) it.next();
            if (p58.getOpt() != null) {
                stringBuffer.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                stringBuffer.append(p58.getOpt());
            } else {
                stringBuffer.append("--");
                stringBuffer.append(p58.getLongOpt());
            }
            stringBuffer.append(" ");
            stringBuffer.append(p58.getDescription());
            if (it.hasNext()) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
