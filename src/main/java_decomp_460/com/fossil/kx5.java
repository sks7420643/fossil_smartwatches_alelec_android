package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx5 extends jx5<b> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public /* final */ ArrayList<String> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> j; // = new ArrayList<>();
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public a m;
    @DexIgnore
    public AlphabetIndexer s;
    @DexIgnore
    public /* final */ List<j06> t;
    @DexIgnore
    public /* final */ int u;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(j06 j06);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2109a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ ce5 h;
        @DexIgnore
        public /* final */ /* synthetic */ kx5 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kx5 kx5, ce5 ce5) {
            super(ce5.n());
            pq7.c(ce5, "binding");
            this.i = kx5;
            this.h = ce5;
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            String d3 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                this.h.s.setBackgroundColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d3)) {
                this.h.x.setBackgroundColor(Color.parseColor(d3));
            }
            this.h.q.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final j06 b() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.f2109a);
            contact.setPhotoThumbUri(this.b);
            j06 j06 = new j06(contact, null, 2, null);
            if (this.f == 1) {
                j06.setHasPhoneNumber(true);
                j06.setPhoneNumber(this.c);
            } else {
                j06.setHasPhoneNumber(false);
            }
            Contact contact2 = j06.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
            }
            Contact contact3 = j06.getContact();
            if (contact3 != null) {
                contact3.setUseCall(true);
            }
            j06.setFavorites(this.g);
            j06.setAdded(true);
            j06.setCurrentHandGroup(this.i.u);
            return j06;
        }

        @DexIgnore
        public final void c() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.t.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((j06) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 == -1) {
                    this.i.t.add(b());
                    this.i.notifyItemChanged(adapterPosition);
                } else if (((j06) this.i.t.get(i2)).getCurrentHandGroup() != this.i.u) {
                    a aVar = this.i.m;
                    if (aVar != null) {
                        aVar.a((j06) this.i.t.get(i2));
                    }
                } else {
                    this.i.t.remove(i2);
                    this.i.notifyItemChanged(adapterPosition);
                }
            }
        }

        @DexIgnore
        public final void d(Cursor cursor, int i2) {
            boolean z;
            Object obj;
            boolean z2;
            boolean z3;
            pq7.c(cursor, "cursor");
            cursor.moveToPosition(i2);
            FLogger.INSTANCE.getLocal().d(kx5.v, ".Inside renderData, cursor move position=" + i2);
            this.f2109a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            String d2 = p47.d(this.c);
            if (this.i.j.contains(Integer.valueOf(i2)) || (this.i.l < i2 && this.i.k == this.e && this.i.i.contains(d2))) {
                if (!this.i.j.contains(Integer.valueOf(i2))) {
                    this.i.j.add(Integer.valueOf(i2));
                }
                e(8);
                z = false;
            } else {
                if (i2 > this.i.l) {
                    this.i.l = i2;
                }
                if (this.i.k != this.e) {
                    this.i.i.clear();
                    this.i.k = this.e;
                }
                this.i.i.add(d2);
                e(0);
                if (!cursor.moveToPrevious() || cursor.getInt(cursor.getColumnIndex("contact_id")) != this.e) {
                    z3 = false;
                } else {
                    FlexibleCheckBox flexibleCheckBox = this.h.q;
                    pq7.b(flexibleCheckBox, "binding.accbSelect");
                    flexibleCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    pq7.b(flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                    z3 = true;
                }
                cursor.moveToNext();
                z = z3;
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            pq7.b(flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.f2109a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                pq7.b(flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
            }
            Iterator it = this.i.t.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                Object next = it.next();
                Contact contact = ((j06) next).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    obj = next;
                    break;
                }
            }
            j06 j06 = (j06) obj;
            if (j06 == null) {
                FlexibleCheckBox flexibleCheckBox2 = this.h.q;
                pq7.b(flexibleCheckBox2, "binding.accbSelect");
                flexibleCheckBox2.setChecked(false);
            } else if (j06.getCurrentHandGroup() == 0 || j06.getCurrentHandGroup() == this.i.u) {
                FlexibleCheckBox flexibleCheckBox3 = this.h.q;
                pq7.b(flexibleCheckBox3, "binding.accbSelect");
                flexibleCheckBox3.setChecked(true);
            } else {
                FlexibleTextView flexibleTextView4 = this.h.v;
                pq7.b(flexibleTextView4, "binding.pickContactPhone");
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886157);
                pq7.b(c2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(j06.getCurrentHandGroup())}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                FlexibleCheckBox flexibleCheckBox4 = this.h.q;
                pq7.b(flexibleCheckBox4, "binding.accbSelect");
                flexibleCheckBox4.setChecked(false);
                if (z) {
                    e(8);
                }
            }
            FLogger.INSTANCE.getLocal().d(kx5.v, "Inside renderData, contactId = " + this.e + ", displayName = " + this.f2109a + ", hasPhoneNumber = " + this.f + ", phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView5 = this.h.t;
                pq7.b(flexibleTextView5, "binding.ftvAlphabet");
                flexibleTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView6 = this.h.t;
                pq7.b(flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setText(Character.toString(jl5.b.j(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView7 = this.h.t;
            pq7.b(flexibleTextView7, "binding.ftvAlphabet");
            flexibleTextView7.setVisibility(8);
        }

        @DexIgnore
        public final void e(int i2) {
            FlexibleCheckBox flexibleCheckBox = this.h.q;
            pq7.b(flexibleCheckBox, "binding.accbSelect");
            flexibleCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            pq7.b(constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            pq7.b(flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.u;
            pq7.b(constraintLayout2, "binding.llTextContainer");
            constraintLayout2.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            pq7.b(flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            pq7.b(flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            pq7.b(view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout3 = this.h.s;
            pq7.b(constraintLayout3, "binding.clRoot");
            constraintLayout3.setVisibility(i2);
        }
    }

    /*
    static {
        String simpleName = kx5.class.getSimpleName();
        pq7.b(simpleName, "HybridCursorContactSearc\u2026er::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kx5(Cursor cursor, List<j06> list, int i2) {
        super(cursor);
        pq7.c(list, "mContactWrapperList");
        this.t = list;
        this.u = i2;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.s;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.jx5
    public Cursor l(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        AlphabetIndexer alphabetIndexer = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        this.s = alphabetIndexer;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.i.clear();
            this.j.clear();
            this.k = -1;
            this.l = -1;
            return super.l(cursor);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void w(String str) {
        pq7.c(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = v;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    /* renamed from: x */
    public void i(b bVar, Cursor cursor, int i2) {
        pq7.c(bVar, "holder");
        if (cursor != null) {
            bVar.d(cursor, i2);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: y */
    public b onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        ce5 z = ce5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemContactHybridBinding\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final void z(a aVar) {
        pq7.c(aVar, "itemClickListener");
        this.m = aVar;
    }
}
