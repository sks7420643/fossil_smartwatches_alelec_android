package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import android.support.v4.media.session.MediaSessionCompat;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class it0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1662a;
        @DexIgnore
        public /* final */ Bundle b;

        @DexIgnore
        public a(String str, Bundle bundle) {
            this.f1662a = str;
            this.b = bundle;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends MediaBrowserService {
        @DexIgnore
        public /* final */ d b;

        @DexIgnore
        public b(Context context, d dVar) {
            attachBaseContext(context);
            this.b = dVar;
        }

        @DexIgnore
        public MediaBrowserService.BrowserRoot onGetRoot(String str, int i, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            a f = this.b.f(str, i, bundle == null ? null : new Bundle(bundle));
            if (f == null) {
                return null;
            }
            return new MediaBrowserService.BrowserRoot(f.f1662a, f.b);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result) {
            this.b.c(str, new c<>(result));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public MediaBrowserService.Result f1663a;

        @DexIgnore
        public c(MediaBrowserService.Result result) {
            this.f1663a = result;
        }

        @DexIgnore
        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: android.service.media.MediaBrowserService$Result */
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: android.service.media.MediaBrowserService$Result */
        /* JADX WARN: Multi-variable type inference failed */
        public void b(T t) {
            if (t instanceof List) {
                this.f1663a.sendResult(a(t));
            } else if (t instanceof Parcel) {
                T t2 = t;
                t2.setDataPosition(0);
                this.f1663a.sendResult(MediaBrowser.MediaItem.CREATOR.createFromParcel(t2));
                t2.recycle();
            } else {
                this.f1663a.sendResult(null);
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void c(String str, c<List<Parcel>> cVar);

        @DexIgnore
        a f(String str, int i, Bundle bundle);
    }

    @DexIgnore
    public static Object a(Context context, d dVar) {
        return new b(context, dVar);
    }

    @DexIgnore
    public static IBinder b(Object obj, Intent intent) {
        return ((MediaBrowserService) obj).onBind(intent);
    }

    @DexIgnore
    public static void c(Object obj) {
        ((MediaBrowserService) obj).onCreate();
    }
}
