package com.fossil;

import com.fossil.m64;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Set<String> f3085a; // = new HashSet();
    @DexIgnore
    public m64.b b;
    @DexIgnore
    public fg3 c;
    @DexIgnore
    public q64 d;

    @DexIgnore
    public r64(fg3 fg3, m64.b bVar) {
        this.b = bVar;
        this.c = fg3;
        q64 q64 = new q64(this);
        this.d = q64;
        this.c.b(q64);
    }
}
