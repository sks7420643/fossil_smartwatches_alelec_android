package com.fossil;

import com.fossil.h02;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xz1 extends h02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4215a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ vy1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends h02.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f4216a;
        @DexIgnore
        public byte[] b;
        @DexIgnore
        public vy1 c;

        @DexIgnore
        @Override // com.fossil.h02.a
        public h02 a() {
            String str = "";
            if (this.f4216a == null) {
                str = " backendName";
            }
            if (this.c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new xz1(this.f4216a, this.b, this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.h02.a
        public h02.a b(String str) {
            if (str != null) {
                this.f4216a = str;
                return this;
            }
            throw new NullPointerException("Null backendName");
        }

        @DexIgnore
        @Override // com.fossil.h02.a
        public h02.a c(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.h02.a
        public h02.a d(vy1 vy1) {
            if (vy1 != null) {
                this.c = vy1;
                return this;
            }
            throw new NullPointerException("Null priority");
        }
    }

    @DexIgnore
    public xz1(String str, byte[] bArr, vy1 vy1) {
        this.f4215a = str;
        this.b = bArr;
        this.c = vy1;
    }

    @DexIgnore
    @Override // com.fossil.h02
    public String b() {
        return this.f4215a;
    }

    @DexIgnore
    @Override // com.fossil.h02
    public byte[] c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.h02
    public vy1 d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h02)) {
            return false;
        }
        h02 h02 = (h02) obj;
        if (this.f4215a.equals(h02.b())) {
            if (Arrays.equals(this.b, h02 instanceof xz1 ? ((xz1) h02).b : h02.c()) && this.c.equals(h02.d())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.f4215a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b)) * 1000003) ^ this.c.hashCode();
    }
}
