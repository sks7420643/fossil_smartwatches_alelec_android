package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg3 implements Parcelable.Creator<ug3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ug3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 2) {
                ad2.B(parcel, t);
            } else {
                bundle = ad2.a(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ug3(bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ug3[] newArray(int i) {
        return new ug3[i];
    }
}
