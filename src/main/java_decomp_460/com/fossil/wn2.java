package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn2 extends tn2 implements vn2 {
    @DexIgnore
    public wn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    @Override // com.fossil.vn2
    public final void V(dj2 dj2) throws RemoteException {
        Parcel d = d();
        qo2.b(d, dj2);
        i(1, d);
    }
}
