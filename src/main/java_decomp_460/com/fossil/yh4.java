package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yh4 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zh4 f4318a;

    @DexIgnore
    public yh4(zh4 zh4) {
        this.f4318a = zh4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.f4318a.a();
    }
}
