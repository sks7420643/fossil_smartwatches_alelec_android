package com.fossil;

import android.content.Context;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz4 implements RecyclerView.p {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f858a;
    @DexIgnore
    public GestureDetector b;
    @DexIgnore
    public /* final */ b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            pq7.c(motionEvent, "e");
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i);
    }

    @DexIgnore
    public dz4(Context context, b bVar) {
        pq7.c(context, "context");
        this.c = bVar;
        this.b = new GestureDetector(context, new a());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
        pq7.c(recyclerView, "view");
        pq7.c(motionEvent, "motionEvent");
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
        pq7.c(recyclerView, "view");
        pq7.c(motionEvent, "e");
        View findChildViewUnder = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        if (findChildViewUnder == null || this.c == null || !this.b.onTouchEvent(motionEvent) || SystemClock.elapsedRealtime() - this.f858a < 1000) {
            return false;
        }
        this.f858a = SystemClock.elapsedRealtime();
        b bVar = this.c;
        View rootView = findChildViewUnder.getRootView();
        pq7.b(rootView, "childView.rootView");
        bVar.a(rootView, recyclerView.getChildAdapterPosition(findChildViewUnder));
        return false;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void e(boolean z) {
    }
}
