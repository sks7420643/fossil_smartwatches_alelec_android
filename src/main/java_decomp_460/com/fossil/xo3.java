package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4149a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public xo3(String str, String str2, long j) {
        this(str, str2, j, false, 0);
    }

    @DexIgnore
    public xo3(String str, String str2, long j, boolean z, long j2) {
        this.f4149a = str;
        this.b = str2;
        this.c = j;
        this.d = false;
        this.e = z;
        this.f = j2;
    }
}
