package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hs6 implements Factory<gs6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f1521a;

    @DexIgnore
    public hs6(Provider<ThemeRepository> provider) {
        this.f1521a = provider;
    }

    @DexIgnore
    public static hs6 a(Provider<ThemeRepository> provider) {
        return new hs6(provider);
    }

    @DexIgnore
    public static gs6 c(ThemeRepository themeRepository) {
        return new gs6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public gs6 get() {
        return c(this.f1521a.get());
    }
}
