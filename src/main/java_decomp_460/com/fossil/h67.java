package com.fossil;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h67 extends RecyclerView.l {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1434a;

    @DexIgnore
    public h67() {
        Resources system = Resources.getSystem();
        pq7.b(system, "Resources.getSystem()");
        this.f1434a = system.getDisplayMetrics().widthPixels;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        pq7.c(rect, "outRect");
        pq7.c(view, "view");
        pq7.c(recyclerView, "parent");
        pq7.c(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        RecyclerView.g adapter = recyclerView.getAdapter();
        if (adapter != null) {
            pq7.b(adapter, "parent.adapter!!");
            int itemCount = adapter.getItemCount();
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            if (itemCount > 0) {
                int width = recyclerView.getWidth();
                int width2 = view.getWidth();
                if (width == 0) {
                    width = this.f1434a;
                }
                if (width2 == 0) {
                    width2 = this.f1434a / 2;
                }
                if (itemCount == 1 || childAdapterPosition == 0) {
                    rect.set(Math.max(0, (width - width2) / 2), 0, 0, 0);
                } else if (childAdapterPosition == itemCount - 1) {
                    rect.set(0, 0, Math.max(0, (width - width2) / 2), 0);
                }
            }
        } else {
            pq7.i();
            throw null;
        }
    }
}
