package com.fossil;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq2 extends jb3 {
    @DexIgnore
    public /* final */ p72<fa3> b;

    @DexIgnore
    public zq2(p72<fa3> p72) {
        this.b = p72;
    }

    @DexIgnore
    @Override // com.fossil.ib3
    public final void S1(LocationResult locationResult) {
        this.b.c(new ar2(this, locationResult));
    }

    @DexIgnore
    public final void i() {
        synchronized (this) {
            this.b.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.ib3
    public final void j1(LocationAvailability locationAvailability) {
        this.b.c(new br2(this, locationAvailability));
    }
}
