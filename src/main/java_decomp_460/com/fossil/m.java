package com.fossil;

import java.util.regex.Pattern;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m extends wk1 {
    @DexIgnore
    public Pattern b;

    @DexIgnore
    public m(String str) throws IllegalArgumentException {
        Pattern compile = Pattern.compile(str);
        pq7.b(compile, "Pattern.compile(serialNumberRegex)");
        this.b = compile;
    }

    @DexIgnore
    @Override // com.fossil.wk1
    public boolean a(e60 e60) {
        return this.b.matcher(e60.u.getSerialNumber()).matches();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.H1, "serial_number_pattern"), jd0.K1, this.b.toString());
    }
}
