package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import com.fossil.jt0;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static Field f2074a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jt0.a {
        @DexIgnore
        public a(Context context, c cVar) {
            super(context, cVar);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((c) this.b).e(str, new b(result), bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public MediaBrowserService.Result f2075a;

        @DexIgnore
        public b(MediaBrowserService.Result result) {
            this.f2075a = result;
        }

        @DexIgnore
        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }

        @DexIgnore
        public void b(List<Parcel> list, int i) {
            try {
                kt0.f2074a.setInt(this.f2075a, i);
            } catch (IllegalAccessException e) {
                Log.w("MBSCompatApi26", e);
            }
            this.f2075a.sendResult(a(list));
        }
    }

    @DexIgnore
    public interface c extends jt0.b {
        @DexIgnore
        void e(String str, b bVar, Bundle bundle);
    }

    /*
    static {
        try {
            Field declaredField = MediaBrowserService.Result.class.getDeclaredField("mFlags");
            f2074a = declaredField;
            declaredField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.w("MBSCompatApi26", e);
        }
    }
    */

    @DexIgnore
    public static Object a(Context context, c cVar) {
        return new a(context, cVar);
    }
}
