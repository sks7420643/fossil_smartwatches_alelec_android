package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class xo extends Enum<xo> {
    @DexIgnore
    public static /* final */ xo b;
    @DexIgnore
    public static /* final */ /* synthetic */ xo[] c;

    /*
    static {
        xo xoVar = new xo("NOT_AUTHENTICATED", 0);
        b = xoVar;
        c = new xo[]{xoVar, new xo("AUTHENTICATING", 1), new xo("AUTHENTICATED", 2)};
    }
    */

    @DexIgnore
    public xo(String str, int i) {
    }

    @DexIgnore
    public static xo valueOf(String str) {
        return (xo) Enum.valueOf(xo.class, str);
    }

    @DexIgnore
    public static xo[] values() {
        return (xo[]) c.clone();
    }
}
