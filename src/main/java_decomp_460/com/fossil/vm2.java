package com.fossil;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm2 extends WeakReference<Throwable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3782a;

    @DexIgnore
    public vm2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f3782a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == vm2.class) {
            if (this == obj) {
                return true;
            }
            vm2 vm2 = (vm2) obj;
            return this.f3782a == vm2.f3782a && get() == vm2.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.f3782a;
    }
}
