package com.fossil;

import com.fossil.js7;
import com.fossil.ms7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sq7 extends uq7 implements js7 {
    @DexIgnore
    public sq7() {
    }

    @DexIgnore
    public sq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public ds7 computeReflected() {
        er7.e(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get(T t);

    @DexIgnore
    @Override // com.fossil.ms7
    public Object getDelegate(Object obj) {
        return ((js7) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.yq7, com.fossil.ms7, com.fossil.uq7
    public ms7.a getGetter() {
        return ((js7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.js7, com.fossil.uq7
    public js7.a getSetter() {
        return ((js7) getReflected()).getSetter();
    }

    @DexIgnore
    @Override // com.fossil.rp7
    public Object invoke(Object obj) {
        return get(obj);
    }

    @DexIgnore
    public abstract /* synthetic */ void set(T t, R r);
}
