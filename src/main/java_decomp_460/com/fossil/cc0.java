package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cc0 extends ox1 implements Parcelable, nx1 {
    @DexIgnore
    public static /* final */ bc0 CREATOR; // = new bc0(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    public cc0(String str, byte[] bArr) {
        this.b = str;
        this.c = bArr;
    }

    @DexIgnore
    @Override // java.lang.Object
    public nx1 clone() {
        String str = this.b;
        byte[] bArr = this.c;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new cc0(str, copyOf);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.d(gy1.d(new JSONObject(), jd0.P4, this.b), jd0.U0, Long.valueOf(ix1.f1688a.b(this.c, ix1.a.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeByteArray(this.c);
    }
}
