package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.lifecycle.LifecycleCoroutineDispatcher;
import com.fossil.h81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g71 {
    @DexIgnore
    public static /* final */ Bitmap.Config[] b; // = (Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888});

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ x61 f1268a; // = x61.f4043a.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ a c; // = new a(l61.b, bw7.c().S());
        @DexIgnore
        public static /* final */ C0095a d; // = new C0095a(null);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Lifecycle f1269a;
        @DexIgnore
        public /* final */ dv7 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.g71$a$a")
        /* renamed from: com.fossil.g71$a$a  reason: collision with other inner class name */
        public static final class C0095a {
            @DexIgnore
            public C0095a() {
            }

            @DexIgnore
            public /* synthetic */ C0095a(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final a a() {
                return a.c;
            }
        }

        @DexIgnore
        public a(Lifecycle lifecycle, dv7 dv7) {
            pq7.c(lifecycle, "lifecycle");
            pq7.c(dv7, "mainDispatcher");
            this.f1269a = lifecycle;
            this.b = dv7;
        }

        @DexIgnore
        public final Lifecycle b() {
            return this.f1269a;
        }

        @DexIgnore
        public final dv7 c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1269a, aVar.f1269a) || !pq7.a(this.b, aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Lifecycle lifecycle = this.f1269a;
            int hashCode = lifecycle != null ? lifecycle.hashCode() : 0;
            dv7 dv7 = this.b;
            if (dv7 != null) {
                i = dv7.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "LifecycleInfo(lifecycle=" + this.f1269a + ", mainDispatcher=" + this.b + ")";
        }
    }

    @DexIgnore
    public final boolean a(x71 x71) {
        boolean z;
        View view = null;
        pq7.c(x71, "request");
        int i = h71.f1436a[x71.r().ordinal()];
        if (i == 1) {
            z = false;
        } else if (i == 2) {
            z = true;
        } else if (i == 3) {
            j81 u = x71.u();
            if (!(u instanceof k81)) {
                u = null;
            }
            k81 k81 = (k81) u;
            if (k81 != null) {
                view = k81.getView();
            }
            if (view instanceof ImageView) {
                return true;
            }
            return x71.t() == null && !(x71.u() instanceof k81);
        } else {
            throw new al7();
        }
        return z;
    }

    @DexIgnore
    public final Lifecycle b(t71 t71) {
        if (t71.A() != null) {
            return t71.A();
        }
        if (!(t71.u() instanceof k81)) {
            return t81.b(t71.x());
        }
        Context context = ((k81) t71.u()).getView().getContext();
        pq7.b(context, "target.view.context");
        return t81.b(context);
    }

    @DexIgnore
    public final boolean c(x71 x71, Bitmap.Config config) {
        pq7.c(x71, "request");
        pq7.c(config, "requestedConfig");
        if (!w81.m(config)) {
            return true;
        }
        if (!x71.b()) {
            return false;
        }
        j81 u = x71.u();
        if (u instanceof k81) {
            View view = ((k81) u).getView();
            if (view.isAttachedToWindow() && !view.isHardwareAccelerated()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean d(x71 x71, f81 f81) {
        return c(x71, x71.d()) && this.f1268a.a(f81);
    }

    @DexIgnore
    public final boolean e(x71 x71) {
        return x71.v().isEmpty() || em7.B(b, x71.d());
    }

    @DexIgnore
    public final a f(x71 x71) {
        pq7.c(x71, "request");
        if (x71 instanceof t71) {
            Lifecycle b2 = b((t71) x71);
            return b2 != null ? new a(b2, LifecycleCoroutineDispatcher.f.a(bw7.c().S(), b2)) : a.d.a();
        }
        throw new al7();
    }

    @DexIgnore
    public final x51 g(x71 x71, f81 f81, e81 e81, boolean z) {
        boolean z2 = true;
        pq7.c(x71, "request");
        pq7.c(f81, "size");
        pq7.c(e81, "scale");
        Bitmap.Config d = e(x71) && d(x71, f81) ? x71.d() : Bitmap.Config.ARGB_8888;
        s71 o = z ? x71.o() : s71.DISABLED;
        if (!x71.c() || !x71.v().isEmpty() || d == Bitmap.Config.ALPHA_8) {
            z2 = false;
        }
        return new x51(d, x71.e(), e81, a(x71), z2, x71.k(), x71.p(), o, x71.g());
    }

    @DexIgnore
    public final e81 h(x71 x71, g81 g81) {
        pq7.c(x71, "request");
        pq7.c(g81, "sizeResolver");
        e81 s = x71.s();
        if (s != null) {
            return s;
        }
        if (g81 instanceof h81) {
            View view = ((h81) g81).getView();
            if (view instanceof ImageView) {
                return w81.j((ImageView) view);
            }
        }
        j81 u = x71.u();
        if (u instanceof k81) {
            View view2 = ((k81) u).getView();
            if (view2 instanceof ImageView) {
                return w81.j((ImageView) view2);
            }
        }
        return e81.FILL;
    }

    @DexIgnore
    public final g81 i(x71 x71, Context context) {
        pq7.c(x71, "request");
        pq7.c(context, "context");
        g81 t = x71.t();
        j81 u = x71.u();
        return t != null ? t : u instanceof k81 ? h81.a.b(h81.f1446a, ((k81) u).getView(), false, 2, null) : new b81(context);
    }
}
