package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cq extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C;
    @DexIgnore
    public /* final */ fs D;

    @DexIgnore
    public cq(k5 k5Var, i60 i60, yp ypVar, fs fsVar) {
        super(k5Var, i60, ypVar, null, false, 24);
        this.D = fsVar;
        ArrayList<ow> arrayList = this.i;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        fs fsVar2 = this.D;
        if (!(fsVar2 instanceof ht) && !(fsVar2 instanceof ts)) {
            if (fsVar2 instanceof mu) {
                linkedHashSet.add(ow.DEVICE_CONFIG);
            } else if (fsVar2 instanceof fv) {
                linkedHashSet.add(ow.FILE_CONFIG);
                linkedHashSet.add(ow.TRANSFER_DATA);
            } else if (fsVar2 instanceof zs) {
                linkedHashSet.add(ow.TRANSFER_DATA);
            } else if (fsVar2 instanceof dv) {
                linkedHashSet.add(ow.FILE_CONFIG);
            } else if ((fsVar2 instanceof tv) || (fsVar2 instanceof pv)) {
                linkedHashSet.add(ow.FILE_CONFIG);
                linkedHashSet.add(ow.TRANSFER_DATA);
            } else if (fsVar2 instanceof bt) {
                linkedHashSet.add(ow.AUTHENTICATION);
            } else if ((fsVar2 instanceof us) || (fsVar2 instanceof ws) || (fsVar2 instanceof ls)) {
                linkedHashSet.add(ow.ASYNC);
            } else if (fsVar2 instanceof ks) {
                linkedHashSet.add(((ks) fsVar2).L.a());
            } else if (fsVar2 instanceof ps) {
                linkedHashSet.add(((ps) fsVar2).K().a());
                linkedHashSet.add(((ps) this.D).N().a());
            } else {
                linkedHashSet.add(ow.DEVICE_INFORMATION);
            }
        }
        this.C = by1.a(arrayList, new ArrayList(linkedHashSet));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public final void B() {
        lp.i(this, this.D, new po(this), cp.b, null, new pp(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return gy1.c(super.C(), this.D.z());
    }

    @DexIgnore
    public void G(fs fsVar) {
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
