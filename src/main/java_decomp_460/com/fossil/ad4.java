package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ad4 implements zc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ wc4 f252a;
    @DexIgnore
    public /* final */ yc4 b;
    @DexIgnore
    public /* final */ xc4 c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public ad4(long j, wc4 wc4, yc4 yc4, xc4 xc4, int i, int i2) {
        this.d = j;
        this.f252a = wc4;
        this.b = yc4;
        this.c = xc4;
    }

    @DexIgnore
    @Override // com.fossil.zc4
    public xc4 a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.zc4
    public yc4 b() {
        return this.b;
    }

    @DexIgnore
    public wc4 c() {
        return this.f252a;
    }

    @DexIgnore
    public long d() {
        return this.d;
    }

    @DexIgnore
    public boolean e(long j) {
        return this.d < j;
    }
}
