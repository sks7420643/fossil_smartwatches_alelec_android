package com.fossil;

import com.fossil.iq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy6 implements iq4.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3333a;
    @DexIgnore
    public String b;

    @DexIgnore
    public sy6(int i, String str) {
        pq7.c(str, "errorMessage");
        this.f3333a = i;
        this.b = str;
    }

    @DexIgnore
    public final int a() {
        return this.f3333a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }
}
