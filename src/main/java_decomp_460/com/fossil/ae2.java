package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ae2 implements Parcelable.Creator<sc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sc2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        GoogleSignInAccount googleSignInAccount = null;
        Account account = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i2 = ad2.v(parcel, t);
            } else if (l == 2) {
                account = (Account) ad2.e(parcel, t, Account.CREATOR);
            } else if (l == 3) {
                i = ad2.v(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                googleSignInAccount = (GoogleSignInAccount) ad2.e(parcel, t, GoogleSignInAccount.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new sc2(i2, account, i, googleSignInAccount);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sc2[] newArray(int i) {
        return new sc2[i];
    }
}
