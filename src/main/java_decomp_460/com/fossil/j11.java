package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j11 implements d11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Handler f1702a; // = qm0.a(Looper.getMainLooper());

    @DexIgnore
    @Override // com.fossil.d11
    public void a(long j, Runnable runnable) {
        this.f1702a.postDelayed(runnable, j);
    }

    @DexIgnore
    @Override // com.fossil.d11
    public void b(Runnable runnable) {
        this.f1702a.removeCallbacks(runnable);
    }
}
