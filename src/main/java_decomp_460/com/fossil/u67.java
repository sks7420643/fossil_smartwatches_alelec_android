package com.fossil;

import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u67 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ yk7 f3519a; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ u67 b; // = new u67();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<Float> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        /* Return type fixed from 'float' to match base method */
        @Override // com.fossil.gp7
        public final Float invoke() {
            Resources system = Resources.getSystem();
            pq7.b(system, "Resources.getSystem()");
            return system.getDisplayMetrics().density;
        }
    }

    @DexIgnore
    public final float a() {
        return ((Number) f3519a.getValue()).floatValue();
    }
}
