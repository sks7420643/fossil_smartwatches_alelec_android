package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o93 implements p93 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2653a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.upload.file_lock_state_check", false);

    @DexIgnore
    @Override // com.fossil.p93
    public final boolean zza() {
        return f2653a.o().booleanValue();
    }
}
