package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp1 extends nu1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public kp1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    pq7.b(createByteArray, "parcel.createByteArray()!!");
                    return new kp1(readString, createByteArray, b.values()[parcel.readInt()]);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public kp1[] newArray(int i) {
            return new kp1[i];
        }
    }

    @DexIgnore
    public enum b {
        VERTICAL("vertical"),
        HORIZONTAL(MessengerShareContentUtility.IMAGE_RATIO_HORIZONTAL);
        
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str) {
            this.b = str;
        }
    }

    @DexIgnore
    public kp1(String str, byte[] bArr, b bVar) {
        super(str, bArr);
        this.e = bVar;
    }

    @DexIgnore
    public final b e() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.nu1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.N5, ey1.a(this.e));
    }

    @DexIgnore
    @Override // com.fossil.nu1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
    }
}
