package com.fossil;

import com.fossil.mx0;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ww0 implements mx0.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4004a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ mx0.c c;

    @DexIgnore
    public ww0(String str, File file, mx0.c cVar) {
        this.f4004a = str;
        this.b = file;
        this.c = cVar;
    }

    @DexIgnore
    @Override // com.fossil.mx0.c
    public mx0 create(mx0.b bVar) {
        return new vw0(bVar.f2432a, this.f4004a, this.b, bVar.c.f2431a, this.c.create(bVar));
    }
}
