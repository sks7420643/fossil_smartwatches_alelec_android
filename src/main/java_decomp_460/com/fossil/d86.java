package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d86 implements Factory<s76> {
    @DexIgnore
    public static s76 a(c86 c86) {
        s76 a2 = c86.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
