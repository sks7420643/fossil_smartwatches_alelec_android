package com.fossil;

import com.fossil.s18;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m88<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m88<Iterable<T>> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: d */
        public void a(o88 o88, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T t : iterable) {
                    m88.this.a(o88, t);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends m88<Object> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.m88 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.m88
        public void a(o88 o88, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    m88.this.a(o88, Array.get(obj, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2318a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ e88<T, RequestBody> c;

        @DexIgnore
        public c(Method method, int i, e88<T, RequestBody> e88) {
            this.f2318a = method;
            this.b = i;
            this.c = e88;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) {
            if (t != null) {
                try {
                    o88.j(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.f2318a;
                    int i = this.b;
                    throw u88.q(method, e, i, "Unable to convert " + ((Object) t) + " to RequestBody", new Object[0]);
                }
            } else {
                throw u88.p(this.f2318a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2319a;
        @DexIgnore
        public /* final */ e88<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public d(String str, e88<T, String> e88, boolean z) {
            u88.b(str, "name == null");
            this.f2319a = str;
            this.b = e88;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.a(this.f2319a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> extends m88<Map<String, T>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2320a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ e88<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public e(Method method, int i, e88<T, String> e88, boolean z) {
            this.f2320a = method;
            this.b = i;
            this.c = e88;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: d */
        public void a(o88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                o88.a(key, a2, this.d);
                            } else {
                                Method method = this.f2320a;
                                int i = this.b;
                                throw u88.p(method, i, "Field map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.f2320a;
                            int i2 = this.b;
                            throw u88.p(method2, i2, "Field map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw u88.p(this.f2320a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw u88.p(this.f2320a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2321a;
        @DexIgnore
        public /* final */ e88<T, String> b;

        @DexIgnore
        public f(String str, e88<T, String> e88) {
            u88.b(str, "name == null");
            this.f2321a = str;
            this.b = e88;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.b(this.f2321a, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2322a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ p18 c;
        @DexIgnore
        public /* final */ e88<T, RequestBody> d;

        @DexIgnore
        public g(Method method, int i, p18 p18, e88<T, RequestBody> e88) {
            this.f2322a = method;
            this.b = i;
            this.c = p18;
            this.d = e88;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) {
            if (t != null) {
                try {
                    o88.c(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.f2322a;
                    int i = this.b;
                    throw u88.p(method, i, "Unable to convert " + ((Object) t) + " to RequestBody", e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends m88<Map<String, T>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2323a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ e88<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public h(Method method, int i, e88<T, RequestBody> e88, String str) {
            this.f2323a = method;
            this.b = i;
            this.c = e88;
            this.d = str;
        }

        @DexIgnore
        /* renamed from: d */
        public void a(o88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            o88.c(p18.g("Content-Disposition", "form-data; name=\"" + key + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.f2323a;
                            int i = this.b;
                            throw u88.p(method, i, "Part map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw u88.p(this.f2323a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw u88.p(this.f2323a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2324a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ e88<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public i(Method method, int i, String str, e88<T, String> e88, boolean z) {
            this.f2324a = method;
            this.b = i;
            u88.b(str, "name == null");
            this.c = str;
            this.d = e88;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) throws IOException {
            if (t != null) {
                o88.e(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.f2324a;
            int i = this.b;
            throw u88.p(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2325a;
        @DexIgnore
        public /* final */ e88<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public j(String str, e88<T, String> e88, boolean z) {
            u88.b(str, "name == null");
            this.f2325a = str;
            this.b = e88;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.f(this.f2325a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> extends m88<Map<String, T>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2326a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ e88<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public k(Method method, int i, e88<T, String> e88, boolean z) {
            this.f2326a = method;
            this.b = i;
            this.c = e88;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: d */
        public void a(o88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                o88.f(key, a2, this.d);
                            } else {
                                Method method = this.f2326a;
                                int i = this.b;
                                throw u88.p(method, i, "Query map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.f2326a;
                            int i2 = this.b;
                            throw u88.p(method2, i2, "Query map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw u88.p(this.f2326a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw u88.p(this.f2326a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> extends m88<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ e88<T, String> f2327a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(e88<T, String> e88, boolean z) {
            this.f2327a = e88;
            this.b = z;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, T t) throws IOException {
            if (t != null) {
                o88.f(this.f2327a.a(t), null, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends m88<s18.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ m f2328a; // = new m();

        @DexIgnore
        /* renamed from: d */
        public void a(o88 o88, s18.b bVar) {
            if (bVar != null) {
                o88.d(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends m88<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f2329a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public n(Method method, int i) {
            this.f2329a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.m88
        public void a(o88 o88, Object obj) {
            if (obj != null) {
                o88.k(obj);
                return;
            }
            throw u88.p(this.f2329a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract void a(o88 o88, T t) throws IOException;

    @DexIgnore
    public final m88<Object> b() {
        return new b();
    }

    @DexIgnore
    public final m88<Iterable<T>> c() {
        return new a();
    }
}
