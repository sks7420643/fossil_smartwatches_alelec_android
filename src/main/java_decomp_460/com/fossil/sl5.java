package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3278a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Map<String, String> c; // = new HashMap();
    @DexIgnore
    public ck5 d;

    @DexIgnore
    public sl5(ck5 ck5, String str) {
        pq7.c(ck5, "analyticsHelper");
        pq7.c(str, "eventName");
        this.d = ck5;
        this.f3278a = str;
    }

    @DexIgnore
    public final sl5 a(String str, String str2) {
        pq7.c(str, "paramName");
        pq7.c(str2, "paramValue");
        Map<String, String> map = this.c;
        if (map != null) {
            map.put(str, str2);
            return this;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void b() {
        String str;
        Map<String, String> map = this.c;
        if (map == null) {
            pq7.i();
            throw null;
        } else if (!map.isEmpty() || (str = this.b) == null) {
            ck5 ck5 = this.d;
            if (ck5 != null) {
                ck5.l(this.f3278a, this.c);
            } else {
                pq7.i();
                throw null;
            }
        } else if (str != null) {
            ck5 ck52 = this.d;
            if (ck52 != null) {
                String str2 = this.f3278a;
                if (str != null) {
                    ck52.j(str2, str);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            ck5 ck53 = this.d;
            if (ck53 != null) {
                ck53.i(this.f3278a);
            } else {
                pq7.i();
                throw null;
            }
        }
    }
}
