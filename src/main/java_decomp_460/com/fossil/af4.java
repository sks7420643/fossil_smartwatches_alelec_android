package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class af4 implements ee4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FirebaseInstanceId.a f262a;

    @DexIgnore
    public af4(FirebaseInstanceId.a aVar) {
        this.f262a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ee4
    public final void a(de4 de4) {
        this.f262a.d(de4);
    }
}
