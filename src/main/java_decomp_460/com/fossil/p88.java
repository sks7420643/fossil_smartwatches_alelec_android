package com.fossil;

import com.fossil.m88;
import com.fossil.p18;
import com.fossil.s18;
import com.fossil.v18;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit.RestMethodInfo;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p88 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Method f2802a;
    @DexIgnore
    public /* final */ q18 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ p18 e;
    @DexIgnore
    public /* final */ r18 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ m88<?>[] j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Pattern x; // = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        @DexIgnore
        public static /* final */ Pattern y; // = Pattern.compile(RestMethodInfo.PARAM);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Retrofit f2803a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Annotation[] c;
        @DexIgnore
        public /* final */ Annotation[][] d;
        @DexIgnore
        public /* final */ Type[] e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q;
        @DexIgnore
        public String r;
        @DexIgnore
        public p18 s;
        @DexIgnore
        public r18 t;
        @DexIgnore
        public Set<String> u;
        @DexIgnore
        public m88<?>[] v;
        @DexIgnore
        public boolean w;

        @DexIgnore
        public a(Retrofit retrofit3, Method method) {
            this.f2803a = retrofit3;
            this.b = method;
            this.c = method.getAnnotations();
            this.e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        @DexIgnore
        public static Class<?> a(Class<?> cls) {
            return Boolean.TYPE == cls ? Boolean.class : Byte.TYPE == cls ? Byte.class : Character.TYPE == cls ? Character.class : Double.TYPE == cls ? Double.class : Float.TYPE == cls ? Float.class : Integer.TYPE == cls ? Integer.class : Long.TYPE == cls ? Long.class : Short.TYPE == cls ? Short.class : cls;
        }

        @DexIgnore
        public static Set<String> h(String str) {
            Matcher matcher = x.matcher(str);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (matcher.find()) {
                linkedHashSet.add(matcher.group(1));
            }
            return linkedHashSet;
        }

        @DexIgnore
        public p88 b() {
            for (Annotation annotation : this.c) {
                e(annotation);
            }
            if (this.n != null) {
                if (!this.o) {
                    if (this.q) {
                        throw u88.n(this.b, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    } else if (this.p) {
                        throw u88.n(this.b, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    }
                }
                int length = this.d.length;
                this.v = new m88[length];
                int i2 = 0;
                while (i2 < length) {
                    this.v[i2] = f(i2, this.e[i2], this.d[i2], i2 == length + -1);
                    i2++;
                }
                if (this.r == null && !this.m) {
                    throw u88.n(this.b, "Missing either @%s URL or @Url parameter.", this.n);
                } else if (!this.p && !this.q && !this.o && this.h) {
                    throw u88.n(this.b, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                } else if (this.p && !this.f) {
                    throw u88.n(this.b, "Form-encoded method must contain at least one @Field.", new Object[0]);
                } else if (!this.q || this.g) {
                    return new p88(this);
                } else {
                    throw u88.n(this.b, "Multipart method must contain at least one @Part.", new Object[0]);
                }
            } else {
                throw u88.n(this.b, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            }
        }

        @DexIgnore
        public final p18 c(String[] strArr) {
            p18.a aVar = new p18.a();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw u88.n(this.b, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if ("Content-Type".equalsIgnoreCase(substring)) {
                    try {
                        this.t = r18.c(trim);
                    } catch (IllegalArgumentException e2) {
                        throw u88.o(this.b, e2, "Malformed content type: %s", trim);
                    }
                } else {
                    aVar.a(substring, trim);
                }
            }
            return aVar.e();
        }

        @DexIgnore
        public final void d(String str, String str2, boolean z) {
            String str3 = this.n;
            if (str3 == null) {
                this.n = str;
                this.o = z;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (x.matcher(substring).find()) {
                            throw u88.n(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.r = str2;
                    this.u = h(str2);
                    return;
                }
                return;
            }
            throw u88.n(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        @DexIgnore
        public final void e(Annotation annotation) {
            if (annotation instanceof j98) {
                d("DELETE", ((j98) annotation).value(), false);
            } else if (annotation instanceof m98) {
                d("GET", ((m98) annotation).value(), false);
            } else if (annotation instanceof n98) {
                d("HEAD", ((n98) annotation).value(), false);
            } else if (annotation instanceof s98) {
                d("PATCH", ((s98) annotation).value(), true);
            } else if (annotation instanceof t98) {
                d("POST", ((t98) annotation).value(), true);
            } else if (annotation instanceof u98) {
                d("PUT", ((u98) annotation).value(), true);
            } else if (annotation instanceof r98) {
                d("OPTIONS", ((r98) annotation).value(), false);
            } else if (annotation instanceof o98) {
                o98 o98 = (o98) annotation;
                d(o98.method(), o98.path(), o98.hasBody());
            } else if (annotation instanceof q98) {
                String[] value = ((q98) annotation).value();
                if (value.length != 0) {
                    this.s = c(value);
                    return;
                }
                throw u88.n(this.b, "@Headers annotation is empty.", new Object[0]);
            }
        }

        @DexIgnore
        public final m88<?> f(int i2, Type type, Annotation[] annotationArr, boolean z) {
            m88<?> m88;
            if (annotationArr != null) {
                m88 = null;
                for (Annotation annotation : annotationArr) {
                    m88<?> g2 = g(i2, type, annotationArr, annotation);
                    if (g2 != null) {
                        if (m88 == null) {
                            m88 = g2;
                        } else {
                            throw u88.p(this.b, i2, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                        }
                    }
                }
            } else {
                m88 = null;
            }
            if (m88 != null) {
                return m88;
            }
            if (z) {
                try {
                    if (u88.i(type) == qn7.class) {
                        this.w = true;
                        return null;
                    }
                } catch (NoClassDefFoundError e2) {
                }
            }
            throw u88.p(this.b, i2, "No Retrofit annotation found.", new Object[0]);
        }

        @DexIgnore
        public final m88<?> g(int i2, Type type, Annotation[] annotationArr, Annotation annotation) {
            if (annotation instanceof ca8) {
                j(i2, type);
                if (this.m) {
                    throw u88.p(this.b, i2, "Multiple @Url method annotations found.", new Object[0]);
                } else if (this.i) {
                    throw u88.p(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.j) {
                    throw u88.p(this.b, i2, "A @Url parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw u88.p(this.b, i2, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw u88.p(this.b, i2, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.r == null) {
                    this.m = true;
                    if (type == q18.class || type == String.class || type == URI.class || ((type instanceof Class) && "android.net.Uri".equals(((Class) type).getName()))) {
                        return new m88.n(this.b, i2);
                    }
                    throw u88.p(this.b, i2, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                } else {
                    throw u88.p(this.b, i2, "@Url cannot be used with @%s URL", this.n);
                }
            } else if (annotation instanceof x98) {
                j(i2, type);
                if (this.j) {
                    throw u88.p(this.b, i2, "A @Path parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw u88.p(this.b, i2, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw u88.p(this.b, i2, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.m) {
                    throw u88.p(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.r != null) {
                    this.i = true;
                    x98 x98 = (x98) annotation;
                    String value = x98.value();
                    i(i2, value);
                    return new m88.i(this.b, i2, value, this.f2803a.j(type, annotationArr), x98.encoded());
                } else {
                    throw u88.p(this.b, i2, "@Path can only be used with relative url on @%s", this.n);
                }
            } else if (annotation instanceof y98) {
                j(i2, type);
                y98 y98 = (y98) annotation;
                String value2 = y98.value();
                boolean encoded = y98.encoded();
                Class<?> i3 = u88.i(type);
                this.j = true;
                if (Iterable.class.isAssignableFrom(i3)) {
                    if (type instanceof ParameterizedType) {
                        return new m88.j(value2, this.f2803a.j(u88.h(0, (ParameterizedType) type), annotationArr), encoded).c();
                    }
                    Method method = this.b;
                    throw u88.p(method, i2, i3.getSimpleName() + " must include generic type (e.g., " + i3.getSimpleName() + "<String>)", new Object[0]);
                } else if (!i3.isArray()) {
                    return new m88.j(value2, this.f2803a.j(type, annotationArr), encoded);
                } else {
                    return new m88.j(value2, this.f2803a.j(a(i3.getComponentType()), annotationArr), encoded).b();
                }
            } else if (annotation instanceof aa8) {
                j(i2, type);
                boolean encoded2 = ((aa8) annotation).encoded();
                Class<?> i4 = u88.i(type);
                this.k = true;
                if (Iterable.class.isAssignableFrom(i4)) {
                    if (type instanceof ParameterizedType) {
                        return new m88.l(this.f2803a.j(u88.h(0, (ParameterizedType) type), annotationArr), encoded2).c();
                    }
                    Method method2 = this.b;
                    throw u88.p(method2, i2, i4.getSimpleName() + " must include generic type (e.g., " + i4.getSimpleName() + "<String>)", new Object[0]);
                } else if (!i4.isArray()) {
                    return new m88.l(this.f2803a.j(type, annotationArr), encoded2);
                } else {
                    return new m88.l(this.f2803a.j(a(i4.getComponentType()), annotationArr), encoded2).b();
                }
            } else if (annotation instanceof z98) {
                j(i2, type);
                Class<?> i5 = u88.i(type);
                this.l = true;
                if (Map.class.isAssignableFrom(i5)) {
                    Type j2 = u88.j(type, i5, Map.class);
                    if (j2 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) j2;
                        Type h2 = u88.h(0, parameterizedType);
                        if (String.class == h2) {
                            return new m88.k(this.b, i2, this.f2803a.j(u88.h(1, parameterizedType), annotationArr), ((z98) annotation).encoded());
                        }
                        Method method3 = this.b;
                        throw u88.p(method3, i2, "@QueryMap keys must be of type String: " + h2, new Object[0]);
                    }
                    throw u88.p(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw u88.p(this.b, i2, "@QueryMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof p98) {
                j(i2, type);
                String value3 = ((p98) annotation).value();
                Class<?> i6 = u88.i(type);
                if (Iterable.class.isAssignableFrom(i6)) {
                    if (type instanceof ParameterizedType) {
                        return new m88.f(value3, this.f2803a.j(u88.h(0, (ParameterizedType) type), annotationArr)).c();
                    }
                    Method method4 = this.b;
                    throw u88.p(method4, i2, i6.getSimpleName() + " must include generic type (e.g., " + i6.getSimpleName() + "<String>)", new Object[0]);
                } else if (!i6.isArray()) {
                    return new m88.f(value3, this.f2803a.j(type, annotationArr));
                } else {
                    return new m88.f(value3, this.f2803a.j(a(i6.getComponentType()), annotationArr)).b();
                }
            } else if (annotation instanceof k98) {
                j(i2, type);
                if (this.p) {
                    k98 k98 = (k98) annotation;
                    String value4 = k98.value();
                    boolean encoded3 = k98.encoded();
                    this.f = true;
                    Class<?> i7 = u88.i(type);
                    if (Iterable.class.isAssignableFrom(i7)) {
                        if (type instanceof ParameterizedType) {
                            return new m88.d(value4, this.f2803a.j(u88.h(0, (ParameterizedType) type), annotationArr), encoded3).c();
                        }
                        Method method5 = this.b;
                        throw u88.p(method5, i2, i7.getSimpleName() + " must include generic type (e.g., " + i7.getSimpleName() + "<String>)", new Object[0]);
                    } else if (!i7.isArray()) {
                        return new m88.d(value4, this.f2803a.j(type, annotationArr), encoded3);
                    } else {
                        return new m88.d(value4, this.f2803a.j(a(i7.getComponentType()), annotationArr), encoded3).b();
                    }
                } else {
                    throw u88.p(this.b, i2, "@Field parameters can only be used with form encoding.", new Object[0]);
                }
            } else if (annotation instanceof l98) {
                j(i2, type);
                if (this.p) {
                    Class<?> i8 = u88.i(type);
                    if (Map.class.isAssignableFrom(i8)) {
                        Type j3 = u88.j(type, i8, Map.class);
                        if (j3 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType2 = (ParameterizedType) j3;
                            Type h3 = u88.h(0, parameterizedType2);
                            if (String.class == h3) {
                                e88 j4 = this.f2803a.j(u88.h(1, parameterizedType2), annotationArr);
                                this.f = true;
                                return new m88.e(this.b, i2, j4, ((l98) annotation).encoded());
                            }
                            Method method6 = this.b;
                            throw u88.p(method6, i2, "@FieldMap keys must be of type String: " + h3, new Object[0]);
                        }
                        throw u88.p(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw u88.p(this.b, i2, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                throw u88.p(this.b, i2, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof v98) {
                j(i2, type);
                if (this.q) {
                    v98 v98 = (v98) annotation;
                    this.g = true;
                    String value5 = v98.value();
                    Class<?> i9 = u88.i(type);
                    if (!value5.isEmpty()) {
                        p18 g2 = p18.g("Content-Disposition", "form-data; name=\"" + value5 + "\"", "Content-Transfer-Encoding", v98.encoding());
                        if (Iterable.class.isAssignableFrom(i9)) {
                            if (type instanceof ParameterizedType) {
                                Type h4 = u88.h(0, (ParameterizedType) type);
                                if (!s18.b.class.isAssignableFrom(u88.i(h4))) {
                                    return new m88.g(this.b, i2, g2, this.f2803a.h(h4, annotationArr, this.c)).c();
                                }
                                throw u88.p(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                            }
                            Method method7 = this.b;
                            throw u88.p(method7, i2, i9.getSimpleName() + " must include generic type (e.g., " + i9.getSimpleName() + "<String>)", new Object[0]);
                        } else if (i9.isArray()) {
                            Class<?> a2 = a(i9.getComponentType());
                            if (!s18.b.class.isAssignableFrom(a2)) {
                                return new m88.g(this.b, i2, g2, this.f2803a.h(a2, annotationArr, this.c)).b();
                            }
                            throw u88.p(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        } else if (!s18.b.class.isAssignableFrom(i9)) {
                            return new m88.g(this.b, i2, g2, this.f2803a.h(type, annotationArr, this.c));
                        } else {
                            throw u88.p(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                    } else if (Iterable.class.isAssignableFrom(i9)) {
                        if (!(type instanceof ParameterizedType)) {
                            Method method8 = this.b;
                            throw u88.p(method8, i2, i9.getSimpleName() + " must include generic type (e.g., " + i9.getSimpleName() + "<String>)", new Object[0]);
                        } else if (s18.b.class.isAssignableFrom(u88.i(u88.h(0, (ParameterizedType) type)))) {
                            return m88.m.f2328a.c();
                        } else {
                            throw u88.p(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        }
                    } else if (i9.isArray()) {
                        if (s18.b.class.isAssignableFrom(i9.getComponentType())) {
                            return m88.m.f2328a.b();
                        }
                        throw u88.p(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    } else if (s18.b.class.isAssignableFrom(i9)) {
                        return m88.m.f2328a;
                    } else {
                        throw u88.p(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    }
                } else {
                    throw u88.p(this.b, i2, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                }
            } else if (annotation instanceof w98) {
                j(i2, type);
                if (this.q) {
                    this.g = true;
                    Class<?> i10 = u88.i(type);
                    if (Map.class.isAssignableFrom(i10)) {
                        Type j5 = u88.j(type, i10, Map.class);
                        if (j5 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType3 = (ParameterizedType) j5;
                            Type h5 = u88.h(0, parameterizedType3);
                            if (String.class == h5) {
                                Type h6 = u88.h(1, parameterizedType3);
                                if (!s18.b.class.isAssignableFrom(u88.i(h6))) {
                                    return new m88.h(this.b, i2, this.f2803a.h(h6, annotationArr, this.c), ((w98) annotation).encoding());
                                }
                                throw u88.p(this.b, i2, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                            }
                            Method method9 = this.b;
                            throw u88.p(method9, i2, "@PartMap keys must be of type String: " + h5, new Object[0]);
                        }
                        throw u88.p(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw u88.p(this.b, i2, "@PartMap parameter type must be Map.", new Object[0]);
                }
                throw u88.p(this.b, i2, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
            } else if (!(annotation instanceof i98)) {
                return null;
            } else {
                j(i2, type);
                if (this.p || this.q) {
                    throw u88.p(this.b, i2, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                } else if (!this.h) {
                    try {
                        e88 h7 = this.f2803a.h(type, annotationArr, this.c);
                        this.h = true;
                        return new m88.c(this.b, i2, h7);
                    } catch (RuntimeException e2) {
                        throw u88.q(this.b, e2, i2, "Unable to create @Body converter for %s", type);
                    }
                } else {
                    throw u88.p(this.b, i2, "Multiple @Body method annotations found.", new Object[0]);
                }
            }
        }

        @DexIgnore
        public final void i(int i2, String str) {
            if (!y.matcher(str).matches()) {
                throw u88.p(this.b, i2, "@Path parameter name must match %s. Found: %s", x.pattern(), str);
            } else if (!this.u.contains(str)) {
                throw u88.p(this.b, i2, "URL \"%s\" does not contain \"{%s}\".", this.r, str);
            }
        }

        @DexIgnore
        public final void j(int i2, Type type) {
            if (u88.k(type)) {
                throw u88.p(this.b, i2, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }
    }

    @DexIgnore
    public p88(a aVar) {
        this.f2802a = aVar.b;
        this.b = aVar.f2803a.c;
        this.c = aVar.n;
        this.d = aVar.r;
        this.e = aVar.s;
        this.f = aVar.t;
        this.g = aVar.o;
        this.h = aVar.p;
        this.i = aVar.q;
        this.j = aVar.v;
        this.k = aVar.w;
    }

    @DexIgnore
    public static p88 b(Retrofit retrofit3, Method method) {
        return new a(retrofit3, method).b();
    }

    @DexIgnore
    public v18 a(Object[] objArr) throws IOException {
        m88<?>[] m88Arr = this.j;
        int length = objArr.length;
        if (length == m88Arr.length) {
            o88 o88 = new o88(this.c, this.b, this.d, this.e, this.f, this.g, this.h, this.i);
            int i2 = this.k ? length - 1 : length;
            ArrayList arrayList = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add(objArr[i3]);
                m88Arr[i3].a(o88, objArr[i3]);
            }
            v18.a i4 = o88.i();
            i4.j(i88.class, new i88(this.f2802a, arrayList));
            return i4.b();
        }
        throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + m88Arr.length + ")");
    }
}
