package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl3 extends in3 {
    @DexIgnore
    public char c; // = ((char) 0);
    @DexIgnore
    public long d; // = -1;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ nl3 f; // = new nl3(this, 6, false, false);
    @DexIgnore
    public /* final */ nl3 g; // = new nl3(this, 6, true, false);
    @DexIgnore
    public /* final */ nl3 h; // = new nl3(this, 6, false, true);
    @DexIgnore
    public /* final */ nl3 i; // = new nl3(this, 5, false, false);
    @DexIgnore
    public /* final */ nl3 j; // = new nl3(this, 5, true, false);
    @DexIgnore
    public /* final */ nl3 k; // = new nl3(this, 5, false, true);
    @DexIgnore
    public /* final */ nl3 l; // = new nl3(this, 4, false, false);
    @DexIgnore
    public /* final */ nl3 m; // = new nl3(this, 3, false, false);
    @DexIgnore
    public /* final */ nl3 n; // = new nl3(this, 2, false, false);

    @DexIgnore
    public kl3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public static String E(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(0, lastIndexOf) : str;
    }

    @DexIgnore
    public static Object w(String str) {
        if (str == null) {
            return null;
        }
        return new ml3(str);
    }

    @DexIgnore
    public static String x(boolean z, Object obj) {
        String className;
        if (obj == null) {
            return "";
        }
        Object valueOf = obj instanceof Integer ? Long.valueOf((long) ((Integer) obj).intValue()) : obj;
        if (valueOf instanceof Long) {
            if (!z) {
                return String.valueOf(valueOf);
            }
            Long l2 = (Long) valueOf;
            if (Math.abs(l2.longValue()) < 100) {
                return String.valueOf(valueOf);
            }
            String str = String.valueOf(valueOf).charAt(0) == '-' ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : "";
            String valueOf2 = String.valueOf(Math.abs(l2.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf2.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf2.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str.length() + 43 + str.length());
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (valueOf instanceof Boolean) {
            return String.valueOf(valueOf);
        } else {
            if (!(valueOf instanceof Throwable)) {
                return valueOf instanceof ml3 ? ml3.a((ml3) valueOf) : z ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : String.valueOf(valueOf);
            }
            Throwable th = (Throwable) valueOf;
            StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
            String E = E(pm3.class.getCanonicalName());
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                StackTraceElement stackTraceElement = stackTrace[i2];
                if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && E(className).equals(E)) {
                    sb2.append(": ");
                    sb2.append(stackTraceElement);
                    break;
                }
                i2++;
            }
            return sb2.toString();
        }
    }

    @DexIgnore
    public static String y(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = "";
        }
        String x = x(z, obj);
        String x2 = x(z, obj2);
        String x3 = x(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(x)) {
            sb.append(str2);
            sb.append(x);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(x2)) {
            sb.append(str2);
            sb.append(x2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(x3)) {
            sb.append(str2);
            sb.append(x3);
        }
        return sb.toString();
    }

    @DexIgnore
    public final void A(int i2, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        int i3 = 0;
        if (!z && B(i2)) {
            z(i2, y(false, str, obj, obj2, obj3));
        }
        if (!z2 && i2 >= 5) {
            rc2.k(str);
            im3 D = this.f1780a.D();
            if (D == null) {
                z(6, "Scheduler not set. Not logging error/warn");
            } else if (!D.s()) {
                z(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i2 >= 0) {
                    i3 = i2;
                }
                if (i3 >= 9) {
                    i3 = 8;
                }
                D.y(new jl3(this, i3, str, obj, obj2, obj3));
            }
        }
    }

    @DexIgnore
    public final boolean B(int i2) {
        return Log.isLoggable(C(), i2);
    }

    @DexIgnore
    public final String C() {
        String str;
        String str2;
        synchronized (this) {
            if (this.e == null) {
                if (this.f1780a.L() != null) {
                    str2 = this.f1780a.L();
                } else {
                    m().b();
                    str2 = "FA";
                }
                this.e = str2;
            }
            str = this.e;
        }
        return str;
    }

    @DexIgnore
    public final nl3 F() {
        return this.f;
    }

    @DexIgnore
    public final nl3 G() {
        return this.g;
    }

    @DexIgnore
    public final nl3 H() {
        return this.h;
    }

    @DexIgnore
    public final nl3 I() {
        return this.i;
    }

    @DexIgnore
    public final nl3 J() {
        return this.j;
    }

    @DexIgnore
    public final nl3 K() {
        return this.k;
    }

    @DexIgnore
    public final nl3 L() {
        return this.l;
    }

    @DexIgnore
    public final nl3 M() {
        return this.m;
    }

    @DexIgnore
    public final nl3 N() {
        return this.n;
    }

    @DexIgnore
    public final String O() {
        Pair<String, Long> a2 = l().d.a();
        if (a2 == null || a2 == xl3.D) {
            return null;
        }
        String valueOf = String.valueOf(a2.second);
        String str = (String) a2.first;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.in3
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final void z(int i2, String str) {
        Log.println(i2, C(), str);
    }
}
