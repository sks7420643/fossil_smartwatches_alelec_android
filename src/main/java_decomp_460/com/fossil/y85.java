package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y85 extends x85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L;
    @DexIgnore
    public long J;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        L = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        L.put(2131362546, 2);
        L.put(2131362797, 3);
        L.put(2131362363, 4);
        L.put(2131363459, 5);
        L.put(2131362798, 6);
        L.put(2131362365, 7);
        L.put(2131363460, 8);
        L.put(2131362819, 9);
        L.put(2131362350, 10);
        L.put(2131362767, 11);
        L.put(2131363165, 12);
        L.put(2131362506, 13);
        L.put(2131363461, 14);
        L.put(2131362436, 15);
        L.put(2131362779, 16);
        L.put(2131361966, 17);
        L.put(2131363037, 18);
    }
    */

    @DexIgnore
    public y85(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 19, K, L));
    }

    @DexIgnore
    public y85(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[17], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (RTLImageView) objArr[11], (ConstraintLayout) objArr[16], (LinearLayout) objArr[3], (LinearLayout) objArr[6], (LinearLayout) objArr[9], (ConstraintLayout) objArr[0], (RecyclerView) objArr[18], (FlexibleSwitchCompat) objArr[12], (View) objArr[5], (View) objArr[8], (View) objArr[14]);
        this.J = -1;
        this.D.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.J != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.J = 1;
        }
        w();
    }
}
