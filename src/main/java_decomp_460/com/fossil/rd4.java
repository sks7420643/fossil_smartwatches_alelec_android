package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rd4 extends RuntimeException {
    @DexIgnore
    public rd4(String str) {
        super(str);
    }

    @DexIgnore
    public rd4(String str, Exception exc) {
        super(str, exc);
    }
}
