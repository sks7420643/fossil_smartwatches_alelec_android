package com.fossil;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ph1 implements th1<Bitmap, byte[]> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Bitmap.CompressFormat f2830a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public ph1() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    public ph1(Bitmap.CompressFormat compressFormat, int i) {
        this.f2830a = compressFormat;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.th1
    public id1<byte[]> a(id1<Bitmap> id1, ob1 ob1) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        id1.get().compress(this.f2830a, this.b, byteArrayOutputStream);
        id1.b();
        return new xg1(byteArrayOutputStream.toByteArray());
    }
}
