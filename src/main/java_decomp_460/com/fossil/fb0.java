package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb0 extends va0 {
    @DexIgnore
    public static /* final */ eb0 CREATOR; // = new eb0(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ fb0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c);
        order.put(this.d ? (byte) 1 : 0);
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(fb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            fb0 fb0 = (fb0) obj;
            if (this.d != fb0.d) {
                return false;
            }
            return this.c == fb0.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.SwitchActivityInstr");
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int hashCode() {
        return (this.c * 31) + Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.va0, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.P3, Byte.valueOf(this.c)), jd0.T3, Boolean.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
