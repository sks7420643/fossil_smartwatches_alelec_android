package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm4 implements nm4 {
    @DexIgnore
    public static char b(char c, char c2) {
        if (qm4.f(c) && qm4.f(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + 130);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }

    @DexIgnore
    @Override // com.fossil.nm4
    public void a(om4 om4) {
        if (qm4.a(om4.d(), om4.f) >= 2) {
            om4.r(b(om4.d().charAt(om4.f), om4.d().charAt(om4.f + 1)));
            om4.f += 2;
            return;
        }
        char c = om4.c();
        int n = qm4.n(om4.d(), om4.f, c());
        if (n != c()) {
            if (n == 1) {
                om4.r('\u00e6');
                om4.o(1);
            } else if (n == 2) {
                om4.r('\u00ef');
                om4.o(2);
            } else if (n == 3) {
                om4.r('\u00ee');
                om4.o(3);
            } else if (n == 4) {
                om4.r('\u00f0');
                om4.o(4);
            } else if (n == 5) {
                om4.r('\u00e7');
                om4.o(5);
            } else {
                throw new IllegalStateException("Illegal mode: " + n);
            }
        } else if (qm4.g(c)) {
            om4.r('\u00eb');
            om4.r((char) ((c - '\u0080') + 1));
            om4.f++;
        } else {
            om4.r((char) (c + 1));
            om4.f++;
        }
    }

    @DexIgnore
    public int c() {
        return 0;
    }
}
