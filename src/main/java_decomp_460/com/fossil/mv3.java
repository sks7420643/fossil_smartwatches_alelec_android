package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mv3 extends ba3 implements lv3 {
    @DexIgnore
    public mv3() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    @Override // com.fossil.ba3
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                O((DataHolder) ca3.a(parcel, DataHolder.CREATOR));
                break;
            case 2:
                a1((nv3) ca3.a(parcel, nv3.CREATOR));
                break;
            case 3:
                i0((pv3) ca3.a(parcel, pv3.CREATOR));
                break;
            case 4:
                T1((pv3) ca3.a(parcel, pv3.CREATOR));
                break;
            case 5:
                U2(parcel.createTypedArrayList(pv3.CREATOR));
                break;
            case 6:
                k2((uv3) ca3.a(parcel, uv3.CREATOR));
                break;
            case 7:
                r((ev3) ca3.a(parcel, ev3.CREATOR));
                break;
            case 8:
                t0((av3) ca3.a(parcel, av3.CREATOR));
                break;
            case 9:
                L0((sv3) ca3.a(parcel, sv3.CREATOR));
                break;
            default:
                return false;
        }
        return true;
    }
}
