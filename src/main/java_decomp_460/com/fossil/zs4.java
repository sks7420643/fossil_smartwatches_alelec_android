package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs4 implements ys4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f4522a;
    @DexIgnore
    public /* final */ jw0<xs4> b;
    @DexIgnore
    public /* final */ xw0 c;
    @DexIgnore
    public /* final */ xw0 d;
    @DexIgnore
    public /* final */ xw0 e;
    @DexIgnore
    public /* final */ xw0 f;
    @DexIgnore
    public /* final */ xw0 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<xs4> {
        @DexIgnore
        public a(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, xs4 xs4) {
            if (xs4.d() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, xs4.d());
            }
            if (xs4.i() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, xs4.i());
            }
            if (xs4.b() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, xs4.b());
            }
            if (xs4.e() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, xs4.e());
            }
            if (xs4.g() == null) {
                px0.bindNull(5);
            } else {
                px0.bindLong(5, (long) xs4.g().intValue());
            }
            if (xs4.h() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, xs4.h());
            }
            px0.bindLong(7, xs4.a() ? 1 : 0);
            px0.bindLong(8, (long) xs4.f());
            px0.bindLong(9, (long) xs4.c());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `friend` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`confirmation`,`pin`,`friendType`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE friend SET confirmation = ? AND friendType = ? AND pin = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM friend WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xw0 {
        @DexIgnore
        public d(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 2 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends xw0 {
        @DexIgnore
        public e(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends xw0 {
        @DexIgnore
        public f(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = -1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends xw0 {
        @DexIgnore
        public g(zs4 zs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM friend";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Callable<List<xs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f4523a;

        @DexIgnore
        public h(tw0 tw0) {
            this.f4523a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public List<xs4> call() throws Exception {
            Cursor b2 = ex0.b(zs4.this.f4522a, this.f4523a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "socialId");
                int c3 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
                int c4 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
                int c5 = dx0.c(b2, "points");
                int c6 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
                int c7 = dx0.c(b2, "confirmation");
                int c8 = dx0.c(b2, "pin");
                int c9 = dx0.c(b2, "friendType");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new xs4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.isNull(c5) ? null : Integer.valueOf(b2.getInt(c5)), b2.getString(c6), b2.getInt(c7) != 0, b2.getInt(c8), b2.getInt(c9)));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f4523a.m();
        }
    }

    @DexIgnore
    public zs4(qw0 qw0) {
        this.f4522a = qw0;
        this.b = new a(this, qw0);
        new b(this, qw0);
        this.c = new c(this, qw0);
        this.d = new d(this, qw0);
        this.e = new e(this, qw0);
        this.f = new f(this, qw0);
        this.g = new g(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public void a() {
        this.f4522a.assertNotSuspendingTransaction();
        px0 acquire = this.g.acquire();
        this.f4522a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
        } finally {
            this.f4522a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public int b(String str) {
        this.f4522a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f4522a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f4522a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public LiveData<List<xs4>> c() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType <> 3 ORDER BY friendType DESC, firstName COLLATE NOCASE ASC", 0);
        nw0 invalidationTracker = this.f4522a.getInvalidationTracker();
        h hVar = new h(f2);
        return invalidationTracker.d(new String[]{"friend"}, false, hVar);
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> d() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType = 2 AND pin = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public int e() {
        int i = 0;
        tw0 f2 = tw0.f("SELECT COUNT(*) FROM friend WHERE friendType = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i = b2.getInt(0);
            }
            return i;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> f() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType = 1 AND pin = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public void g() {
        this.f4522a.assertNotSuspendingTransaction();
        px0 acquire = this.e.acquire();
        this.f4522a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
        } finally {
            this.f4522a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public void h() {
        this.f4522a.assertNotSuspendingTransaction();
        px0 acquire = this.d.acquire();
        this.f4522a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
        } finally {
            this.f4522a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> i() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE pin = 2", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public Long[] insert(List<xs4> list) {
        this.f4522a.assertNotSuspendingTransaction();
        this.f4522a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f4522a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f4522a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public void j() {
        this.f4522a.assertNotSuspendingTransaction();
        px0 acquire = this.f.acquire();
        this.f4522a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
        } finally {
            this.f4522a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> k() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType = 0 AND pin = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> l() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType = 0 OR friendType = 1 OR friendType = -1 AND pin = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public int m(String[] strArr) {
        this.f4522a.assertNotSuspendingTransaction();
        StringBuilder b2 = hx0.b();
        b2.append("DELETE FROM friend WHERE id in (");
        hx0.a(b2, strArr.length);
        b2.append(")");
        px0 compileStatement = this.f4522a.compileStatement(b2.toString());
        int i = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindString(i, str);
            }
            i++;
        }
        this.f4522a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.f4522a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f4522a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public List<xs4> n() {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE friendType = -1 AND pin = 0", 0);
        this.f4522a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public long o(xs4 xs4) {
        this.f4522a.assertNotSuspendingTransaction();
        this.f4522a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(xs4);
            this.f4522a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f4522a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.ys4
    public xs4 p(String str) {
        tw0 f2 = tw0.f("SELECT * FROM friend WHERE id =? LIMIT 1", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f4522a.assertNotSuspendingTransaction();
        xs4 xs4 = null;
        Integer num = null;
        Cursor b2 = ex0.b(this.f4522a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "confirmation");
            int c9 = dx0.c(b2, "pin");
            int c10 = dx0.c(b2, "friendType");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                String string4 = b2.getString(c5);
                if (!b2.isNull(c6)) {
                    num = Integer.valueOf(b2.getInt(c6));
                }
                xs4 = new xs4(string, string2, string3, string4, num, b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10));
            }
            return xs4;
        } finally {
            b2.close();
            f2.m();
        }
    }
}
