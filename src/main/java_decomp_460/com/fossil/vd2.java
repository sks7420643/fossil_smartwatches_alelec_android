package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vd2 extends hl2 implements oc2 {
    @DexIgnore
    public vd2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.oc2
    public final rg2 P0(rg2 rg2, uc2 uc2) throws RemoteException {
        Parcel d = d();
        il2.c(d, rg2);
        il2.d(d, uc2);
        Parcel e = e(2, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
