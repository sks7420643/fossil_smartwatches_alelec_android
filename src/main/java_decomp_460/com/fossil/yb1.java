package com.fossil;

import com.fossil.xb1;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yb1 {
    @DexIgnore
    public static /* final */ xb1.a<?> b; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Class<?>, xb1.a<?>> f4290a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements xb1.a<Object> {
        @DexIgnore
        @Override // com.fossil.xb1.a
        public xb1<Object> a(Object obj) {
            return new b(obj);
        }

        @DexIgnore
        @Override // com.fossil.xb1.a
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements xb1<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f4291a;

        @DexIgnore
        public b(Object obj) {
            this.f4291a = obj;
        }

        @DexIgnore
        @Override // com.fossil.xb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.xb1
        public Object b() {
            return this.f4291a;
        }
    }

    @DexIgnore
    public <T> xb1<T> a(T t) {
        xb1<T> xb1;
        synchronized (this) {
            ik1.d(t);
            xb1.a<?> aVar = this.f4290a.get(t.getClass());
            if (aVar == null) {
                Iterator<xb1.a<?>> it = this.f4290a.values().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    xb1.a<?> next = it.next();
                    if (next.getDataClass().isAssignableFrom(t.getClass())) {
                        aVar = next;
                        break;
                    }
                }
            }
            if (aVar == null) {
                aVar = b;
            }
            xb1 = (xb1<T>) aVar.a(t);
        }
        return xb1;
    }

    @DexIgnore
    public void b(xb1.a<?> aVar) {
        synchronized (this) {
            this.f4290a.put(aVar.getDataClass(), aVar);
        }
    }
}
