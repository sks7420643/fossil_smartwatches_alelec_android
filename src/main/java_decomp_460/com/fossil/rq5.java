package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rq5 implements Factory<qq5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f3144a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public rq5(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<on5> provider3) {
        this.f3144a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static rq5 a(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<on5> provider3) {
        return new rq5(provider, provider2, provider3);
    }

    @DexIgnore
    public static qq5 c(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, on5 on5) {
        return new qq5(portfolioApp, authApiGuestService, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public qq5 get() {
        return c(this.f3144a.get(), this.b.get(), this.c.get());
    }
}
