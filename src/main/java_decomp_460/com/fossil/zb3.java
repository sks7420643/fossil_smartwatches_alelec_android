package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zb3 extends IInterface {
    @DexIgnore
    void B(vd3 vd3) throws RemoteException;

    @DexIgnore
    void D2(rc3 rc3) throws RemoteException;

    @DexIgnore
    fc3 F1() throws RemoteException;

    @DexIgnore
    void H(jc3 jc3) throws RemoteException;

    @DexIgnore
    void H2(nc3 nc3) throws RemoteException;

    @DexIgnore
    void I0(dd3 dd3, rg2 rg2) throws RemoteException;

    @DexIgnore
    void J(LatLngBounds latLngBounds) throws RemoteException;

    @DexIgnore
    void J0(hc3 hc3) throws RemoteException;

    @DexIgnore
    boolean N1() throws RemoteException;

    @DexIgnore
    ls2 N2(le3 le3) throws RemoteException;

    @DexIgnore
    is2 Q(ee3 ee3) throws RemoteException;

    @DexIgnore
    void R(yc3 yc3) throws RemoteException;

    @DexIgnore
    boolean R0() throws RemoteException;

    @DexIgnore
    void R2(tc3 tc3) throws RemoteException;

    @DexIgnore
    void S2(wc3 wc3) throws RemoteException;

    @DexIgnore
    void U0(rg2 rg2, kd3 kd3) throws RemoteException;

    @DexIgnore
    void X(lc3 lc3) throws RemoteException;

    @DexIgnore
    void Y0(float f) throws RemoteException;

    @DexIgnore
    void a0(int i, int i2, int i3, int i4) throws RemoteException;

    @DexIgnore
    void a2(td3 td3) throws RemoteException;

    @DexIgnore
    cc3 b2() throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    void e1(float f) throws RemoteException;

    @DexIgnore
    void h2(xd3 xd3) throws RemoteException;

    @DexIgnore
    void j2(rg2 rg2) throws RemoteException;

    @DexIgnore
    void o0(rg2 rg2) throws RemoteException;

    @DexIgnore
    CameraPosition p0() throws RemoteException;

    @DexIgnore
    void setBuildingsEnabled(boolean z) throws RemoteException;

    @DexIgnore
    boolean setIndoorEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMapType(int i) throws RemoteException;

    @DexIgnore
    void setMyLocationEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setTrafficEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void u1() throws RemoteException;

    @DexIgnore
    boolean v0(je3 je3) throws RemoteException;

    @DexIgnore
    os2 w1(oe3 oe3) throws RemoteException;

    @DexIgnore
    void x1(rd3 rd3) throws RemoteException;

    @DexIgnore
    rs2 x2(qe3 qe3) throws RemoteException;

    @DexIgnore
    float z() throws RemoteException;

    @DexIgnore
    float z2() throws RemoteException;
}
