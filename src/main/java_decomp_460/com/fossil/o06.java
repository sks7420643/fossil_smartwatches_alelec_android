package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ex5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o06 extends qv5 implements n06, t47.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public m06 h;
    @DexIgnore
    public g37<v85> i;
    @DexIgnore
    public w16 j;
    @DexIgnore
    public ex5 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return o06.m;
        }

        @DexIgnore
        public final o06 b() {
            return new o06();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ex5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o06 f2608a;

        @DexIgnore
        public b(o06 o06) {
            this.f2608a = o06;
        }

        @DexIgnore
        @Override // com.fossil.ex5.b
        public void a(i06 i06, boolean z) {
            pq7.c(i06, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = o06.s.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = i06.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            o06.L6(this.f2608a).p(i06, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o06 b;

        @DexIgnore
        public c(o06 o06) {
            this.b = o06;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(o06.s.a(), "press on close button");
            o06.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o06 f2609a;

        @DexIgnore
        public d(o06 o06) {
            this.f2609a = o06;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            o06.L6(this.f2609a).q(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ o06 b;
        @DexIgnore
        public /* final */ /* synthetic */ v85 c;

        @DexIgnore
        public e(o06 o06, v85 v85) {
            this.b = o06;
            this.c = v85;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.c.u;
            pq7.b(rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 == 0 ? 4 : 0);
            o06.K6(this.b).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ v85 b;

        @DexIgnore
        public f(v85 v85) {
            this.b = v85;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                v85 v85 = this.b;
                pq7.b(v85, "binding");
                v85.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ v85 b;

        @DexIgnore
        public g(v85 v85) {
            this.b = v85;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.r.setText("");
        }
    }

    /*
    static {
        String simpleName = o06.class.getSimpleName();
        pq7.b(simpleName, "NotificationAppsFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ex5 K6(o06 o06) {
        ex5 ex5 = o06.k;
        if (ex5 != null) {
            return ex5;
        }
        pq7.n("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ m06 L6(o06 o06) {
        m06 m06 = o06.h;
        if (m06 != null) {
            return m06;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d(m, "onActivityBackPressed -");
        m06 m06 = this.h;
        if (m06 != null) {
            m06.n();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(m06 m06) {
        pq7.c(m06, "presenter");
        this.h = m06;
    }

    @DexIgnore
    @Override // com.fossil.n06
    public void Q0(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<v85> g37 = this.i;
        if (g37 != null) {
            v85 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373) {
                m06 m06 = this.h;
                if (m06 != null) {
                    m06.o();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i2 == 2131362290) {
                m06 m062 = this.h;
                if (m062 != null) {
                    m062.n();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (i2 != 2131362385) {
                if (i2 == 2131362686) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.B;
                FragmentActivity requireActivity = requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n06
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.n06
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        v85 v85 = (v85) aq0.f(layoutInflater, 2131558588, viewGroup, false, A6());
        v85.v.setOnClickListener(new c(this));
        v85.y.setOnCheckedChangeListener(new d(this));
        v85.r.addTextChangedListener(new e(this, v85));
        v85.r.setOnFocusChangeListener(new f(v85));
        v85.u.setOnClickListener(new g(v85));
        ex5 ex5 = new ex5();
        ex5.o(new b(this));
        this.k = ex5;
        w16 w16 = (w16) getChildFragmentManager().Z(w16.w.a());
        this.j = w16;
        if (w16 == null) {
            this.j = w16.w.b();
        }
        RecyclerView recyclerView = v85.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        ex5 ex52 = this.k;
        if (ex52 != null) {
            recyclerView.setAdapter(ex52);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((yu0) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                ro4 M = PortfolioApp.h0.c().M();
                w16 w162 = this.j;
                if (w162 != null) {
                    M.U1(new y16(w162)).a(this);
                    this.i = new g37<>(this, v85);
                    E6("app_notification_view");
                    pq7.b(v85, "binding");
                    return v85.n();
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        pq7.n("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        m06 m06 = this.h;
        if (m06 != null) {
            m06.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        m06 m06 = this.h;
        if (m06 != null) {
            m06.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n06
    public void s2(List<i06> list) {
        pq7.c(list, "listAppWrapper");
        ex5 ex5 = this.k;
        if (ex5 != null) {
            ex5.n(list);
        } else {
            pq7.n("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.n06
    public void u() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            TroubleshootingActivity.a.c(aVar, requireContext, PortfolioApp.h0.c().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
