package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn1 extends ym1 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c(null);
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ b f;
    @DexIgnore
    public /* final */ b g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ b i;
    @DexIgnore
    public /* final */ b j;
    @DexIgnore
    public /* final */ b k;
    @DexIgnore
    public /* final */ b l;
    @DexIgnore
    public /* final */ short m;
    @DexIgnore
    public /* final */ short n;

    @DexIgnore
    public enum a {
        DAILY((byte) 0),
        EXTENDED((byte) 1),
        TIME_ONLY((byte) 2),
        CUSTOM((byte) 3);
        
        @DexIgnore
        public static /* final */ C0058a d; // = new C0058a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dn1$a$a")
        /* renamed from: com.fossil.dn1$a$a  reason: collision with other inner class name */
        public static final class C0058a {
            @DexIgnore
            public /* synthetic */ C0058a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore
    public enum b {
        OFF((byte) 0),
        ON((byte) 1);
        
        @DexIgnore
        public static /* final */ a d; // = new a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public b(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<dn1> {
        @DexIgnore
        public /* synthetic */ c(kq7 kq7) {
        }

        @DexIgnore
        public final dn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 14) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new dn1(a.d.a(order.get(0)), b.d.a(order.get(1)), b.d.a(order.get(2)), b.d.a(order.get(3)), b.d.a(order.get(4)), b.d.a(order.get(5)), b.d.a(order.get(6)), b.d.a(order.get(7)), b.d.a(order.get(8)), b.d.a(order.get(9)), order.getShort(10), order.getShort(12));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 14"));
        }

        @DexIgnore
        public dn1 b(Parcel parcel) {
            return new dn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dn1 createFromParcel(Parcel parcel) {
            return new dn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dn1[] newArray(int i) {
            return new dn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ dn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = a.d.a(parcel.readByte());
        this.d = b.d.a(parcel.readByte());
        this.e = b.d.a(parcel.readByte());
        this.f = b.d.a(parcel.readByte());
        this.g = b.d.a(parcel.readByte());
        this.h = b.d.a(parcel.readByte());
        this.i = b.d.a(parcel.readByte());
        this.j = b.d.a(parcel.readByte());
        this.k = b.d.a(parcel.readByte());
        this.l = b.d.a(parcel.readByte());
        this.m = (short) ((short) parcel.readInt());
        this.n = (short) ((short) parcel.readInt());
    }

    @DexIgnore
    public dn1(a aVar, b bVar, b bVar2, b bVar3, b bVar4, b bVar5, b bVar6, b bVar7, b bVar8, b bVar9, short s, short s2) {
        super(zm1.HELLAS_BATTERY);
        this.c = aVar;
        this.d = bVar;
        this.e = bVar2;
        this.f = bVar3;
        this.g = bVar4;
        this.h = bVar5;
        this.i = bVar6;
        this.j = bVar7;
        this.k = bVar8;
        this.l = bVar9;
        this.m = (short) s;
        this.n = (short) s2;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(14).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).put(this.d.a()).put(this.e.a()).put(this.f.a()).put(this.g.a()).put(this.h.a()).put(this.i.a()).put(this.j.a()).put(this.k.a()).put(this.l.a()).putShort(this.m).putShort(this.n).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.m5, ey1.a(this.c));
            g80.k(jSONObject, jd0.n5, ey1.a(this.d));
            g80.k(jSONObject, jd0.o5, ey1.a(this.e));
            g80.k(jSONObject, jd0.p5, ey1.a(this.f));
            g80.k(jSONObject, jd0.q5, ey1.a(this.g));
            g80.k(jSONObject, jd0.r5, ey1.a(this.h));
            g80.k(jSONObject, jd0.s5, ey1.a(this.i));
            g80.k(jSONObject, jd0.t5, ey1.a(this.j));
            g80.k(jSONObject, jd0.u5, ey1.a(this.k));
            g80.k(jSONObject, jd0.v5, ey1.a(this.l));
            g80.k(jSONObject, jd0.w5, Short.valueOf(this.m));
            g80.k(jSONObject, jd0.x5, Short.valueOf(this.n));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(dn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            dn1 dn1 = (dn1) obj;
            if (this.c != dn1.c) {
                return false;
            }
            if (this.d != dn1.d) {
                return false;
            }
            if (this.e != dn1.e) {
                return false;
            }
            if (this.f != dn1.f) {
                return false;
            }
            if (this.g != dn1.g) {
                return false;
            }
            if (this.h != dn1.h) {
                return false;
            }
            if (this.i != dn1.i) {
                return false;
            }
            if (this.j != dn1.j) {
                return false;
            }
            if (this.k != dn1.k) {
                return false;
            }
            if (this.l != dn1.l) {
                return false;
            }
            if (this.m != dn1.m) {
                return false;
            }
            return this.n == dn1.n;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HellasBatteryConfig");
    }

    @DexIgnore
    public final b getAlwaysOnScreenState() {
        return this.e;
    }

    @DexIgnore
    public final a getBatteryMode() {
        return this.c;
    }

    @DexIgnore
    public final short getBleFromMinuteOffset() {
        return this.m;
    }

    @DexIgnore
    public final short getBleToMinuteOffset() {
        return this.n;
    }

    @DexIgnore
    public final b getGoogleDetectState() {
        return this.l;
    }

    @DexIgnore
    public final b getLocationState() {
        return this.h;
    }

    @DexIgnore
    public final b getNfcState() {
        return this.d;
    }

    @DexIgnore
    public final b getSpeakerState() {
        return this.j;
    }

    @DexIgnore
    public final b getTiltToWakeState() {
        return this.g;
    }

    @DexIgnore
    public final b getTouchToWakeState() {
        return this.f;
    }

    @DexIgnore
    public final b getVibrationState() {
        return this.i;
    }

    @DexIgnore
    public final b getWifiState() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        return (((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + Short.valueOf(this.m).hashCode()) * 31) + Short.valueOf(this.n).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.h.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.i.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.j.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.k.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.l.a());
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.m));
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.n));
        }
    }
}
