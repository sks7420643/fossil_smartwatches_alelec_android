package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dg5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FrameLayout f786a;
    @DexIgnore
    public /* final */ TextView b;

    @DexIgnore
    public dg5(FrameLayout frameLayout, TextView textView) {
        this.f786a = frameLayout;
        this.b = textView;
    }

    @DexIgnore
    public static dg5 a(View view) {
        TextView textView = (TextView) view.findViewById(2131362963);
        if (textView != null) {
            return new dg5((FrameLayout) view, textView);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(2131362963)));
    }

    @DexIgnore
    public static dg5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558723, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public FrameLayout b() {
        return this.f786a;
    }
}
