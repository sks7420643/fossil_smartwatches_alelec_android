package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xm5 {
    @DexIgnore
    public static /* final */ String b; // = ("Localization_" + xm5.class.getSimpleName());

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Map<String, String> f4140a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public a(xm5 xm5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public b(xm5 xm5) {
        }
    }

    @DexIgnore
    public void a() {
        Map<String, String> map = this.f4140a;
        if (map != null) {
            map.clear();
        }
    }

    @DexIgnore
    public Map<String, String> b() {
        return this.f4140a;
    }

    @DexIgnore
    public String c(String str) {
        return (this.f4140a == null || TextUtils.isEmpty(str)) ? "" : this.f4140a.get(str);
    }

    @DexIgnore
    public void d(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "parseJSONFile() called with: filePath = [" + str + "], isDownloaded = [" + z + "]");
        try {
            Gson gson = new Gson();
            if (!z) {
                InputStream open = PortfolioApp.d0.getAssets().open(str);
                InputStreamReader inputStreamReader = new InputStreamReader(open, "UTF-8");
                try {
                    this.f4140a.putAll((Map) gson.j(inputStreamReader, new a(this).getType()));
                    inputStreamReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    inputStreamReader.close();
                } catch (Throwable th) {
                    inputStreamReader.close();
                    open.close();
                    throw th;
                }
                open.close();
                return;
            }
            FileReader fileReader = new FileReader(new File(str));
            try {
                this.f4140a.putAll((Map) gson.j(fileReader, new b(this).getType()));
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (Throwable th2) {
                fileReader.close();
                throw th2;
            }
            fileReader.close();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.e(str3, "parseJSONFile failed exception=" + e3);
        }
    }
}
