package com.fossil;

import com.fossil.c44;
import com.fossil.v44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import com.misfit.frameworks.common.constants.Constants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f34<K, V> extends u14<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient a34<K, ? extends u24<V>> map;
    @DexIgnore
    public /* final */ transient int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends f34<K, V>.f {
        @DexIgnore
        public a(f34 f34) {
            super(f34, null);
        }

        @DexIgnore
        /* renamed from: b */
        public Map.Entry<K, V> a(K k, V v) {
            return x34.e(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends f34<K, V>.f {
        @DexIgnore
        public b(f34 f34) {
            super(f34, null);
        }

        @DexIgnore
        @Override // com.fossil.f34.f
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public y34<K, V> f1046a;
        @DexIgnore
        public Comparator<? super K> b;
        @DexIgnore
        public Comparator<? super V> c;

        @DexIgnore
        public c() {
            this(a44.a().a().c());
        }

        @DexIgnore
        public c(y34<K, V> y34) {
            this.f1046a = y34;
        }

        @DexIgnore
        public f34<K, V> a() {
            if (this.c != null) {
                Iterator<Collection<V>> it = this.f1046a.asMap().values().iterator();
                while (it.hasNext()) {
                    Collections.sort((List) it.next(), this.c);
                }
            }
            if (this.b != null) {
                s34<K, V> c2 = a44.a().a().c();
                for (Map.Entry entry : i44.from(this.b).onKeys().immutableSortedCopy(this.f1046a.asMap().entrySet())) {
                    c2.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                this.f1046a = c2;
            }
            return f34.copyOf(this.f1046a);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public c<K, V> b(K k, V v) {
            a24.a(k, v);
            this.f1046a.put(k, v);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.f34$c<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public c<K, V> c(Map.Entry<? extends K, ? extends V> entry) {
            return b(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public c<K, V> d(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                c(entry);
            }
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends u24<Map.Entry<K, V>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ f34<K, V> multimap;

        @DexIgnore
        public d(f34<K, V> f34) {
            this.multimap = f34;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.multimap.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return this.multimap.isPartialView();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.u24, com.fossil.u24, java.lang.Iterable
        public h54<Map.Entry<K, V>> iterator() {
            return this.multimap.entryIterator();
        }

        @DexIgnore
        public int size() {
            return this.multimap.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ v44.b<f34> f1047a; // = v44.a(f34.class, Constants.MAP);
        @DexIgnore
        public static /* final */ v44.b<f34> b; // = v44.a(f34.class, "size");
        @DexIgnore
        public static /* final */ v44.b<i34> c; // = v44.a(i34.class, "emptySet");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f<T> extends h54<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> b;
        @DexIgnore
        public K c;
        @DexIgnore
        public Iterator<V> d;

        @DexIgnore
        public f() {
            this.b = f34.this.asMap().entrySet().iterator();
            this.c = null;
            this.d = p34.h();
        }

        @DexIgnore
        public /* synthetic */ f(f34 f34, a aVar) {
            this();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() || this.d.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.d.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getKey();
                this.d = next.getValue().iterator();
            }
            return a(this.c, this.d.next());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends g34<K> {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.u24, com.fossil.g34
        public boolean contains(Object obj) {
            return f34.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.g34
        public int count(Object obj) {
            Collection collection = (Collection) f34.this.map.get(obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.c44, com.fossil.g34
        public Set<K> elementSet() {
            return f34.this.keySet();
        }

        @DexIgnore
        @Override // com.fossil.g34
        public c44.a<K> getEntry(int i) {
            Map.Entry<K, ? extends u24<V>> entry = f34.this.map.entrySet().asList().get(i);
            return d44.d(entry.getKey(), ((Collection) entry.getValue()).size());
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return f34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<K, V> extends u24<V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ transient f34<K, V> b;

        @DexIgnore
        public h(f34<K, V> f34) {
            this.b = f34;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            return this.b.containsValue(obj);
        }

        @DexIgnore
        @Override // com.fossil.u24
        public int copyIntoArray(Object[] objArr, int i) {
            Iterator it = this.b.map.values().iterator();
            while (it.hasNext()) {
                i = ((u24) it.next()).copyIntoArray(objArr, i);
            }
            return i;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.u24, com.fossil.u24, java.lang.Iterable
        public h54<V> iterator() {
            return this.b.valueIterator();
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }
    }

    @DexIgnore
    public f34(a34<K, ? extends u24<V>> a34, int i) {
        this.map = a34;
        this.size = i;
    }

    @DexIgnore
    public static <K, V> c<K, V> builder() {
        return new c<>();
    }

    @DexIgnore
    public static <K, V> f34<K, V> copyOf(y34<? extends K, ? extends V> y34) {
        if (y34 instanceof f34) {
            f34<K, V> f34 = (f34) y34;
            if (!f34.isPartialView()) {
                return f34;
            }
        }
        return z24.copyOf((y34) y34);
    }

    @DexIgnore
    public static <K, V> f34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return z24.copyOf((Iterable) iterable);
    }

    @DexIgnore
    public static <K, V> f34<K, V> of() {
        return z24.of();
    }

    @DexIgnore
    public static <K, V> f34<K, V> of(K k, V v) {
        return z24.of((Object) k, (Object) v);
    }

    @DexIgnore
    public static <K, V> f34<K, V> of(K k, V v, K k2, V v2) {
        return z24.of((Object) k, (Object) v, (Object) k2, (Object) v2);
    }

    @DexIgnore
    public static <K, V> f34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3);
    }

    @DexIgnore
    public static <K, V> f34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4);
    }

    @DexIgnore
    public static <K, V> f34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4, (Object) k5, (Object) v5);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.a34<K, ? extends com.fossil.u24<V>>, com.fossil.a34<K, java.util.Collection<V>> */
    @Override // com.fossil.u14, com.fossil.y34
    public a34<K, Collection<V>> asMap() {
        return (a34<K, ? extends u24<V>>) this.map;
    }

    @DexIgnore
    @Override // com.fossil.y34
    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public /* bridge */ /* synthetic */ boolean containsEntry(Object obj, Object obj2) {
        return super.containsEntry(obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.y34
    public boolean containsKey(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public boolean containsValue(Object obj) {
        return obj != null && super.containsValue(obj);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Map<K, Collection<V>> createAsMap() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // com.fossil.u14
    public u24<Map.Entry<K, V>> createEntries() {
        return new d(this);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public g34<K> createKeys() {
        return new g();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public u24<V> createValues() {
        return new h(this);
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public u24<Map.Entry<K, V>> entries() {
        return (u24) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public h54<Map.Entry<K, V>> entryIterator() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.y34
    public abstract u24<V> get(K k);

    @DexIgnore
    @Override // com.fossil.u14
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    public abstract f34<V, K> inverse();

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.map.isPartialView();
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public h34<K> keySet() {
        return this.map.keySet();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public g34<K> keys() {
        return (g34) super.keys();
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u14
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(y34<? extends K, ? extends V> y34) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean remove(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public u24<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u14
    @CanIgnoreReturnValue
    @Deprecated
    public u24<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.y34
    public int size() {
        return this.size;
    }

    @DexIgnore
    @Override // com.fossil.u14
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public h54<V> valueIterator() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public u24<V> values() {
        return (u24) super.values();
    }
}
