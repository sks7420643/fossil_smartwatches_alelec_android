package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pn1 {
    KM(0),
    MILE(1);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final pn1 a(int i) {
            pn1[] values = pn1.values();
            for (pn1 pn1 : values) {
                if (pn1.a() == i) {
                    return pn1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public pn1(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
