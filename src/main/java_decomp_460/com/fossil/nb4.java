package com.fossil;

import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nb4 implements FileFilter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2496a;

    @DexIgnore
    public nb4(String str) {
        this.f2496a = str;
    }

    @DexIgnore
    public static FileFilter a(String str) {
        return new nb4(str);
    }

    @DexIgnore
    public boolean accept(File file) {
        return sb4.t(this.f2496a, file);
    }
}
