package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.s87;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s97 extends pv5 {
    @DexIgnore
    public po4 g;
    @DexIgnore
    public u97 h;
    @DexIgnore
    public zg5 i;
    @DexIgnore
    public w97 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<s87.b, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ s97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(s97 s97) {
            super(1);
            this.this$0 = s97;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(s87.b bVar) {
            invoke(bVar);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(s87.b bVar) {
            pq7.c(bVar, "it");
            gc7 e = hc7.c.e(this.this$0);
            if (e != null) {
                e.a(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s97 f3223a;

        @DexIgnore
        public b(s97 s97) {
            this.f3223a = s97;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            s97.K6(this.f3223a).j(t);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s97 f3224a;

        @DexIgnore
        public c(s97 s97) {
            this.f3224a = s97;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                bool.booleanValue();
                s97.K6(this.f3224a).notifyDataSetChanged();
                gc7 e = hc7.c.e(this.f3224a);
                if (e != null) {
                    e.F();
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ w97 K6(s97 s97) {
        w97 w97 = s97.j;
        if (w97 != null) {
            return w97;
        }
        pq7.n("adapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceStickerFragment";
    }

    @DexIgnore
    public final void L6() {
        this.j = new w97(new a(this));
        zg5 zg5 = this.i;
        if (zg5 != null) {
            RecyclerView recyclerView = zg5.r;
            pq7.b(recyclerView, "binding.rvStickerPreview");
            w97 w97 = this.j;
            if (w97 != null) {
                recyclerView.setAdapter(w97);
            } else {
                pq7.n("adapter");
                throw null;
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void M6() {
        u97 u97 = this.h;
        if (u97 != null) {
            LiveData<List<s87.b>> t = u97.t();
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
            t.h(viewLifecycleOwner, new b(this));
            u97 u972 = this.h;
            if (u972 != null) {
                LiveData<u37<Boolean>> s = u972.s();
                LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                pq7.b(viewLifecycleOwner2, "viewLifecycleOwner");
                s.h(viewLifecycleOwner2, new c(this));
                return;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().d1().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            po4 po4 = this.g;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchFaceEditActivity, po4).a(u97.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026kerViewModel::class.java)");
                this.h = (u97) a2;
                String d = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    zg5 zg5 = this.i;
                    if (zg5 != null) {
                        zg5.q.setBackgroundColor(Color.parseColor(d));
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
                L6();
                M6();
                return;
            }
            pq7.n("viewModelFactory");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        zg5 z = zg5.z(layoutInflater, viewGroup, false);
        pq7.b(z, "WatchFaceStickerFragment\u2026flater, container, false)");
        this.i = z;
        if (z != null) {
            View n = z.n();
            pq7.b(n, "binding.root");
            return n;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
