package com.fossil;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a58 extends Closeable, Flushable {
    @DexIgnore
    void K(i48 i48, long j) throws IOException;

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close() throws IOException;

    @DexIgnore
    d58 e();

    @DexIgnore
    @Override // java.io.Flushable
    void flush() throws IOException;
}
