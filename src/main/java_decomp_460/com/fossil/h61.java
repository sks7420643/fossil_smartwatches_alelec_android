package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.a18;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h61 extends g61<Uri> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h61(a18.a aVar) {
        super(aVar);
        pq7.c(aVar, "callFactory");
    }

    @DexIgnore
    /* renamed from: g */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        return pq7.a(uri.getScheme(), "http") || pq7.a(uri.getScheme(), Utility.URL_SCHEME);
    }

    @DexIgnore
    /* renamed from: h */
    public String b(Uri uri) {
        pq7.c(uri, "data");
        String uri2 = uri.toString();
        pq7.b(uri2, "data.toString()");
        return uri2;
    }

    @DexIgnore
    /* renamed from: i */
    public q18 f(Uri uri) {
        pq7.c(uri, "$this$toHttpUrl");
        q18 l = q18.l(uri.toString());
        pq7.b(l, "HttpUrl.get(toString())");
        return l;
    }
}
