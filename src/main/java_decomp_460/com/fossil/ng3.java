package com.fossil;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ng3 {
    @DexIgnore
    public static volatile Handler d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ln3 f2515a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public ng3(ln3 ln3) {
        rc2.k(ln3);
        this.f2515a = ln3;
        this.b = new qg3(this, ln3);
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public final void c(long j) {
        e();
        if (j >= 0) {
            this.c = this.f2515a.zzm().b();
            if (!f().postDelayed(this.b, j)) {
                this.f2515a.d().F().b("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.c != 0;
    }

    @DexIgnore
    public final void e() {
        this.c = 0;
        f().removeCallbacks(this.b);
    }

    @DexIgnore
    public final Handler f() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (ng3.class) {
            try {
                if (d == null) {
                    d = new e93(this.f2515a.e().getMainLooper());
                }
                handler = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return handler;
    }
}
