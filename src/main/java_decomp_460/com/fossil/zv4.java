package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv4 extends pv5 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<l35> g;
    @DexIgnore
    public bw4 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public dw4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return zv4.l;
        }

        @DexIgnore
        public final zv4 b() {
            return new zv4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zv4 b;

        @DexIgnore
        public b(zv4 zv4) {
            this.b = zv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l35 f4543a;
        @DexIgnore
        public /* final */ /* synthetic */ zv4 b;

        @DexIgnore
        public c(l35 l35, zv4 zv4) {
            this.f4543a = l35;
            this.b = zv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f4543a.r;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            zv4.N6(this.b).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zv4 f4544a;

        @DexIgnore
        public d(zv4 zv4) {
            this.f4544a = zv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            l35 l35 = (l35) zv4.K6(this.f4544a).a();
            if (l35 != null) {
                SwipeRefreshLayout swipeRefreshLayout = l35.w;
                pq7.b(swipeRefreshLayout, "swipe");
                pq7.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zv4 f4545a;

        @DexIgnore
        public e(zv4 zv4) {
            this.f4545a = zv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            l35 l35 = (l35) zv4.K6(this.f4545a).a();
            if (l35 != null) {
                pq7.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = l35.q;
                    pq7.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = l35.u;
                    pq7.b(recyclerView, "rcvNotification");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = l35.q;
                pq7.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = l35.u;
                pq7.b(recyclerView2, "rcvNotification");
                recyclerView2.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<List<? extends dt4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zv4 f4546a;

        @DexIgnore
        public f(zv4 zv4) {
            this.f4546a = zv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<dt4> list) {
            dw4 dw4 = this.f4546a.j;
            if (dw4 != null) {
                pq7.b(list, "it");
                dw4.i(list);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zv4 f4547a;

        @DexIgnore
        public g(zv4 zv4) {
            this.f4547a = zv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            boolean booleanValue = cl7.getFirst().booleanValue();
            ServerError serverError = (ServerError) cl7.getSecond();
            l35 l35 = (l35) zv4.K6(this.f4547a).a();
            if (l35 != null) {
                FlexibleTextView flexibleTextView = l35.q;
                pq7.b(flexibleTextView, "ftvEmpty");
                flexibleTextView.setVisibility(8);
                if (booleanValue) {
                    FlexibleTextView flexibleTextView2 = l35.r;
                    pq7.b(flexibleTextView2, "ftvError");
                    flexibleTextView2.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView3 = l35.r;
                pq7.b(flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(8);
                FlexibleTextView flexibleTextView4 = l35.r;
                pq7.b(flexibleTextView4, "ftvError");
                String c = um5.c(flexibleTextView4.getContext(), 2131886231);
                FragmentActivity activity = this.f4547a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }
    }

    /*
    static {
        String simpleName = zv4.class.getSimpleName();
        pq7.b(simpleName, "BCNotificationFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(zv4 zv4) {
        g37<l35> g37 = zv4.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ bw4 N6(zv4 zv4) {
        bw4 bw4 = zv4.h;
        if (bw4 != null) {
            return bw4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        g37<l35> g37 = this.g;
        if (g37 != null) {
            l35 a2 = g37.a();
            if (a2 != null) {
                this.j = new dw4();
                a2.s.setOnClickListener(new b(this));
                a2.w.setOnRefreshListener(new c(a2, this));
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.hasFixedSize();
                recyclerView.setAdapter(this.j);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        bw4 bw4 = this.h;
        if (bw4 != null) {
            bw4.k().h(getViewLifecycleOwner(), new d(this));
            bw4 bw42 = this.h;
            if (bw42 != null) {
                bw42.i().h(getViewLifecycleOwner(), new e(this));
                bw4 bw43 = this.h;
                if (bw43 != null) {
                    bw43.l().h(getViewLifecycleOwner(), new f(this));
                    bw4 bw44 = this.h;
                    if (bw44 != null) {
                        bw44.j().h(getViewLifecycleOwner(), new g(this));
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().W0().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(bw4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.h = (bw4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        l35 l35 = (l35) aq0.f(layoutInflater, 2131558515, viewGroup, false, A6());
        this.g = new g37<>(this, l35);
        pq7.b(l35, "binding");
        return l35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        O6();
        P6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
