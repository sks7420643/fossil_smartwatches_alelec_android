package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class va3 implements Parcelable.Creator<ka3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ka3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        la3 la3 = null;
        Status status = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                status = (Status) ad2.e(parcel, t, Status.CREATOR);
            } else if (l != 2) {
                ad2.B(parcel, t);
            } else {
                la3 = (la3) ad2.e(parcel, t, la3.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new ka3(status, la3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ka3[] newArray(int i) {
        return new ka3[i];
    }
}
