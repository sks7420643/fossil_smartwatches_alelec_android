package com.fossil;

import com.fossil.yk1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fh extends lp {
    @DexIgnore
    public static /* final */ ArrayList<n6> Q; // = hm7.c(n6.DC, n6.FTC, n6.FTD, n6.AUTHENTICATION, n6.ASYNC, n6.FTD_1);
    @DexIgnore
    public zk1 C;
    @DexIgnore
    public /* final */ ArrayList<ow> D; // = new ArrayList<>();
    @DexIgnore
    public long E; // = System.currentTimeMillis();
    @DexIgnore
    public long F;
    @DexIgnore
    public int G;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<n6> H;
    @DexIgnore
    public int I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ HashMap<ym, Object> P;

    @DexIgnore
    public fh(k5 k5Var, i60 i60, HashMap<ym, Object> hashMap, String str) {
        super(k5Var, i60, yp.c, str, false, 16);
        this.P = hashMap;
        this.C = new zk1(k5Var.C(), k5Var.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        gm7.b(hd0.y.u());
        hm7.h(n6.MODEL_NUMBER, n6.SERIAL_NUMBER, n6.FIRMWARE_VERSION, n6.DC, n6.FTC, n6.FTD, n6.AUTHENTICATION, n6.ASYNC);
        this.H = new CopyOnWriteArrayList<>();
    }

    @DexIgnore
    public static final /* synthetic */ void H(fh fhVar, long j) {
        if (fhVar.G < 2) {
            fhVar.b = null;
            fhVar.o.postDelayed(new eh(fhVar), j);
            return;
        }
        nr nrVar = fhVar.v;
        fhVar.K(nr.a(nrVar, null, zq.I.a(nrVar.d), null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        i60 i60 = this.x;
        if (i60.f1587a.v == yk1.c.CONNECTED) {
            this.C = i60.a();
            l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
            return;
        }
        this.E = System.currentTimeMillis();
        this.M = true;
        V();
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(g80.k(super.C(), jd0.o1, ey1.a(this.w.H())), jd0.f5, Integer.valueOf(this.w.A.getType()));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(g80.k(g80.k(super.E(), jd0.k2, Long.valueOf(this.F)), jd0.l2, Integer.valueOf(this.N)), jd0.m2, Integer.valueOf(this.O));
    }

    @DexIgnore
    public final void K(nr nrVar) {
        if (nrVar.c == zq.SUCCESS) {
            l(nrVar);
            return;
        }
        this.v = nrVar;
        this.O++;
        lp.i(this, new os(this.w), jq.b, xq.b, null, new lr(this), null, 40, null);
    }

    @DexIgnore
    public final void R() {
        lp.i(this, new bv(ob.ALL_FILE.b, this.w, 0, 4), wn.b, io.b, null, new vo(this), null, 40, null);
    }

    @DexIgnore
    public final void S() {
        lp.i(this, new uu(this.w), new fj(this), rj.b, null, new dk(this), null, 40, null);
    }

    @DexIgnore
    public final void T() {
        lp.i(this, new yu(cx1.f.f(), this.w), om.b, zm.b, null, new ln(this), null, 40, null);
    }

    @DexIgnore
    public final void U() {
        int i;
        ky1 ky1 = ky1.DEBUG;
        if (this.I >= this.H.size()) {
            Hashtable hashtable = new Hashtable();
            hashtable.put(ow.DEVICE_INFORMATION, Integer.MAX_VALUE);
            for (T t : this.H) {
                ow a2 = t.a();
                switch (m6.b[t.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        i = Integer.MAX_VALUE;
                        break;
                    case 8:
                    case 9:
                    case 10:
                        i = 1;
                        break;
                    default:
                        i = 0;
                        break;
                }
                Integer num = (Integer) hashtable.get(a2);
                if (num == null) {
                    num = 0;
                }
                pq7.b(num, "resourceQuotas[resourceType] ?: 0");
                hashtable.put(a2, Integer.valueOf(num.intValue() + i));
            }
            this.x.b().d(new nw(hashtable, null));
            lp.h(this, new ah(this.w, this.x, this.z), new pk(this), new cl(this), null, null, null, 56, null);
            return;
        }
        n6 n6Var = this.H.get(this.I);
        pq7.b(n6Var, "characteristicsToSubscri\u2026ibingCharacteristicIndex]");
        lp.i(this, new vs(n6Var, true, this.w), new xn(this), new jo(this), null, null, null, 56, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void V() {
        /*
            r14 = this;
            r12 = 0
            r4 = 0
            r3 = 0
            boolean r0 = r14.L
            if (r0 != 0) goto L_0x001a
            java.util.HashMap<com.fossil.ym, java.lang.Object> r0 = r14.P
            com.fossil.ym r1 = com.fossil.ym.AUTO_CONNECT
            java.lang.Object r0 = r0.get(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0042
            boolean r0 = r0.booleanValue()
        L_0x0018:
            if (r0 == 0) goto L_0x0044
        L_0x001a:
            r0 = 1
            r2 = r0
        L_0x001c:
            r14.L = r3
            java.util.HashMap<com.fossil.ym, java.lang.Object> r0 = r14.P
            com.fossil.ym r1 = com.fossil.ym.CONNECTION_TIME_OUT
            java.lang.Object r0 = r0.get(r1)
            java.lang.Long r0 = (java.lang.Long) r0
            if (r0 == 0) goto L_0x0046
            long r0 = r0.longValue()
        L_0x002e:
            com.fossil.q3 r5 = com.fossil.q3.f
            long r6 = r5.e(r2)
            int r5 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r5 != 0) goto L_0x0049
        L_0x0038:
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x0057
            com.fossil.nr r0 = r14.v
            r14.l(r0)
        L_0x0041:
            return
        L_0x0042:
            r0 = r3
            goto L_0x0018
        L_0x0044:
            r2 = r3
            goto L_0x001c
        L_0x0046:
            r0 = 30000(0x7530, double:1.4822E-319)
            goto L_0x002e
        L_0x0049:
            long r8 = java.lang.System.currentTimeMillis()
            long r10 = r14.E
            long r8 = r8 - r10
            long r0 = r0 - r8
            long r0 = java.lang.Math.min(r0, r6)
            r6 = r0
            goto L_0x0038
        L_0x0057:
            long r0 = java.lang.System.currentTimeMillis()
            r14.J = r0
            com.fossil.as r1 = new com.fossil.as
            com.fossil.k5 r0 = r14.w
            r1.<init>(r0, r2, r6)
            boolean r0 = r14.M
            r1.w = r0
            r14.M = r3
            int r0 = r14.N
            int r0 = r0 + 1
            r14.N = r0
            com.fossil.wo r2 = new com.fossil.wo
            r2.<init>(r14)
            com.fossil.yq r3 = new com.fossil.yq
            r3.<init>(r14)
            com.fossil.mr r5 = new com.fossil.mr
            r5.<init>(r14)
            com.fossil.cf r6 = com.fossil.cf.b
            r7 = 8
            r0 = r14
            r8 = r4
            com.fossil.lp.i(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fh.V():void");
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void e(f5 f5Var) {
        if (kn.f1934a[f5Var.ordinal()] == 1) {
            fs fsVar = this.b;
            if (fsVar == null || fsVar.t) {
                lp lpVar = this.n;
                if (lpVar == null || lpVar.t) {
                    k(zq.CONNECTION_DROPPED);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean q(lp lpVar) {
        return u(lpVar) || (lpVar.z().isEmpty() && this.y != lpVar.y);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean r(fs fsVar) {
        hs hsVar = fsVar != null ? fsVar.x : null;
        return hsVar != null && kn.c[hsVar.ordinal()] == 1;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.D;
    }
}
