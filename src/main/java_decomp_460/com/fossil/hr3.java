package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1518a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ Object e;

    @DexIgnore
    public hr3(String str, String str2, String str3, long j, Object obj) {
        rc2.g(str);
        rc2.g(str3);
        rc2.k(obj);
        this.f1518a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
