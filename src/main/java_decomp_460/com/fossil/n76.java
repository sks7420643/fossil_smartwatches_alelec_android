package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bw5;
import com.fossil.cr4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n76 extends pv5 implements s76, cr4.a {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<n35> g;
    @DexIgnore
    public r76 h;
    @DexIgnore
    public bw5 i;
    @DexIgnore
    public cr4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final n76 a() {
            return new n76();
        }

        @DexIgnore
        public final String b() {
            return n76.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n76 f2479a;
        @DexIgnore
        public /* final */ /* synthetic */ n35 b;

        @DexIgnore
        public b(n76 n76, n35 n35) {
            this.f2479a = n76;
            this.b = n35;
        }

        @DexIgnore
        @Override // com.fossil.bw5.b
        public void a(String str) {
            pq7.c(str, "address");
            this.b.q.setText((CharSequence) str, false);
            this.f2479a.P6(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public c(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public d(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public e(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public f(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ n35 c;

        @DexIgnore
        public g(View view, n35 n35) {
            this.b = view;
            this.c = n35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.D.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                n35 n35 = this.c;
                pq7.b(n35, "binding");
                n35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = n76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                pq7.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;
        @DexIgnore
        public /* final */ /* synthetic */ n35 c;

        @DexIgnore
        public h(n76 n76, n35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.getItem(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                pq7.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = n76.s.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                String spannableString = fullText.toString();
                pq7.b(spannableString, "primaryText.toString()");
                if (!TextUtils.isEmpty(spannableString)) {
                    this.b.P6(spannableString);
                    this.c.q.setText((CharSequence) spannableString, false);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;
        @DexIgnore
        public /* final */ /* synthetic */ n35 c;

        @DexIgnore
        public i(n76 n76, n35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            RTLImageView rTLImageView = this.c.t;
            if (!pq7.a(this.b.N6(), String.valueOf(editable))) {
                this.b.P6(null);
            }
            RTLImageView rTLImageView2 = this.c.t;
            pq7.b(rTLImageView2, "binding.closeIv");
            rTLImageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public j(n76 n76, n35 n35) {
            this.b = n76;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            pq7.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public k(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).v("travel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public l(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            n76 n76 = this.b;
            CommuteTimeSetting o = n76.L6(n76).o();
            if (o == null || (str = o.getAddress()) == null) {
                str = "";
            }
            aVar.a(n76, 0.0d, 0.0d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public m(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;
        @DexIgnore
        public /* final */ /* synthetic */ n35 c;

        @DexIgnore
        public n(n76 n76, n35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.c.q.setText((CharSequence) "", false);
            n76.L6(this.b).n();
            this.b.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n76 b;

        @DexIgnore
        public o(n76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            n76.L6(this.b).v("eta");
        }
    }

    /*
    static {
        String simpleName = n76.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ r76 L6(n76 n76) {
        r76 r76 = n76.h;
        if (r76 != null) {
            return r76;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cr4.a
    public void D1(boolean z) {
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.k = null;
                        S6();
                        return;
                    }
                }
                O6();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    @DexIgnore
    @Override // com.fossil.pv5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean F6() {
        /*
            r7 = this;
            r3 = 0
            com.fossil.g37<com.fossil.n35> r0 = r7.g
            if (r0 == 0) goto L_0x007c
            java.lang.Object r0 = r0.a()
            r4 = r0
            com.fossil.n35 r4 = (com.fossil.n35) r4
            if (r4 == 0) goto L_0x005e
            java.lang.String r0 = r7.k
            if (r0 == 0) goto L_0x0068
            if (r0 == 0) goto L_0x0060
            java.lang.CharSequence r0 = com.fossil.wt7.u0(r0)
            java.lang.String r1 = r0.toString()
            if (r1 == 0) goto L_0x0068
        L_0x001e:
            com.portfolio.platform.view.RTLImageView r0 = r4.z
            java.lang.String r2 = "it.ivArrivalTime"
            com.fossil.pq7.b(r0, r2)
            float r0 = r0.getAlpha()
            r2 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x006b
            java.lang.String r5 = "eta"
        L_0x0031:
            com.fossil.r76 r0 = r7.h
            if (r0 == 0) goto L_0x0076
            com.portfolio.platform.view.FlexibleAutoCompleteTextView r2 = r4.q
            java.lang.String r3 = "it.autocompletePlaces"
            com.fossil.pq7.b(r2, r3)
            android.text.Editable r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            if (r2 == 0) goto L_0x006e
            java.lang.CharSequence r2 = com.fossil.wt7.u0(r2)
            java.lang.String r2 = r2.toString()
            com.fossil.mh5 r3 = com.fossil.mh5.CAR
            com.portfolio.platform.view.FlexibleSwitchCompat r4 = r4.J
            java.lang.String r6 = "it.scAvoidTolls"
            com.fossil.pq7.b(r4, r6)
            boolean r4 = r4.isChecked()
            r0.u(r1, r2, r3, r4, r5)
        L_0x005e:
            r0 = 1
            return r0
        L_0x0060:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x0068:
            java.lang.String r1 = ""
            goto L_0x001e
        L_0x006b:
            java.lang.String r5 = "travel"
            goto L_0x0031
        L_0x006e:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x0076:
            java.lang.String r0 = "mPresenter"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x007c:
            java.lang.String r0 = "mBinding"
            com.fossil.pq7.n(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n76.F6():boolean");
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            cr4 cr4 = new cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            g37<n35> g37 = this.g;
            if (g37 != null) {
                n35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    pq7.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0, false);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    O6();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final String N6() {
        return this.k;
    }

    @DexIgnore
    public void O6() {
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                LinearLayout linearLayout = a2.G;
                pq7.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                ee5 ee5 = a2.x;
                pq7.b(ee5, "it.icHome");
                View n2 = ee5.n();
                pq7.b(n2, "it.icHome.root");
                n2.setVisibility(0);
                ee5 ee52 = a2.y;
                pq7.b(ee52, "it.icWork");
                View n3 = ee52.n();
                pq7.b(n3, "it.icWork.root");
                n3.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6(String str) {
        this.k = str;
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void Q4(String str) {
        ee5 ee5;
        pq7.c(str, "userWorkDefaultAddress");
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null && (ee5 = a2.y) != null) {
                FlexibleTextView flexibleTextView = ee5.r;
                pq7.b(flexibleTextView, "workButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) p47.a(12, requireContext());
                ee5.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = ee5.t;
                    pq7.b(imageButton, "workButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(gl0.d(PortfolioApp.h0.c(), 2131099968)));
                    ee5.t.setImageResource(2131231049);
                    return;
                }
                ImageButton imageButton2 = ee5.t;
                pq7.b(imageButton2, "workButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(gl0.d(PortfolioApp.h0.c(), 2131099809)));
                ee5.t.setImageResource(2131231049);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(r76 r76) {
        pq7.c(r76, "presenter");
        this.h = r76;
    }

    @DexIgnore
    public void R6(String str) {
        pq7.c(str, "address");
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.k = str;
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void S6() {
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.h0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.u;
                pq7.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.G;
                pq7.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                ee5 ee5 = a2.x;
                pq7.b(ee5, "it.icHome");
                View n2 = ee5.n();
                pq7.b(n2, "it.icHome.root");
                n2.setVisibility(8);
                ee5 ee52 = a2.y;
                pq7.b(ee52, "it.icWork");
                View n3 = ee52.n();
                pq7.b(n3, "it.icWork.root");
                n3.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void c2(String str, String str2) {
        pq7.c(str, "addressType");
        pq7.c(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                cr4 cr4 = this.j;
                if (cr4 != null) {
                    cr4.h(null);
                }
                CommuteTimeSettingsDefaultAddressActivity.B.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            cr4 cr42 = this.j;
            if (cr42 != null) {
                cr42.h(null);
            }
            CommuteTimeSettingsDefaultAddressActivity.B.a(this, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void h0(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        pq7.c(list, "recentSearchedList");
        O6();
        if (!list.isEmpty()) {
            bw5 bw5 = this.i;
            if (bw5 != null) {
                bw5.k(pm7.j0(list));
            }
            g37<n35> g37 = this.g;
            if (g37 != null) {
                n35 a2 = g37.a();
                if (a2 != null && (linearLayout2 = a2.G) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<n35> g372 = this.g;
        if (g372 != null) {
            n35 a3 = g372.a();
            if (a3 != null && (linearLayout = a3.G) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void k2(String str) {
        ee5 ee5;
        pq7.c(str, "userHomeDefaultAddress");
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null && (ee5 = a2.x) != null) {
                FlexibleTextView flexibleTextView = ee5.r;
                pq7.b(flexibleTextView, "homeButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) p47.a(12, requireContext());
                ee5.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = ee5.t;
                    pq7.b(imageButton, "homeButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(gl0.d(PortfolioApp.h0.c(), 2131099967)));
                    ee5.t.setImageResource(2131231049);
                    return;
                }
                ImageButton imageButton2 = ee5.t;
                pq7.b(imageButton2, "homeButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(gl0.d(PortfolioApp.h0.c(), 2131099809)));
                ee5.t.setImageResource(2131231049);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent == null) {
            return;
        }
        if (i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                R6(string);
            }
        } else if (i2 == 111) {
            String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
            k2(stringExtra != null ? stringExtra : "");
            r76 r76 = this.h;
            if (r76 != null) {
                if (stringExtra == null) {
                    stringExtra = "";
                }
                r76.r(stringExtra);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        } else if (i2 == 112) {
            String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
            Q4(stringExtra2 != null ? stringExtra2 : "");
            r76 r762 = this.h;
            if (r762 != null) {
                if (stringExtra2 == null) {
                    stringExtra2 = "";
                }
                r762.r(stringExtra2);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        n35 n35 = (n35) aq0.f(layoutInflater, 2131558516, viewGroup, false, A6());
        String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d2)) {
            n35.F.setBackgroundColor(Color.parseColor(d2));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = n35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(gl0.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new h(this, n35));
        flexibleAutoCompleteTextView.addTextChangedListener(new i(this, n35));
        flexibleAutoCompleteTextView.setOnKeyListener(new j(this, n35));
        n35.w.setOnClickListener(new m(this));
        ee5 ee5 = n35.x;
        ee5.u.setImageResource(2131231073);
        FlexibleTextView flexibleTextView = ee5.s;
        pq7.b(flexibleTextView, "it.ftvTitle");
        flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131886354));
        ee5.q.setOnClickListener(new c(this));
        ee5.t.setOnClickListener(new d(this));
        ee5 ee52 = n35.y;
        ee52.u.setImageResource(2131231074);
        FlexibleTextView flexibleTextView2 = ee52.s;
        pq7.b(flexibleTextView2, "it.ftvTitle");
        flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131886355));
        ee52.q.setOnClickListener(new e(this));
        ee52.t.setOnClickListener(new f(this));
        n35.t.setOnClickListener(new n(this, n35));
        bw5 bw5 = new bw5();
        bw5.l(new b(this, n35));
        this.i = bw5;
        RecyclerView recyclerView = n35.I;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        pq7.b(n35, "binding");
        View n2 = n35.n();
        pq7.b(n2, "binding.root");
        n2.getViewTreeObserver().addOnGlobalLayoutListener(new g(n2, n35));
        n35.z.setOnClickListener(new o(this));
        n35.B.setOnClickListener(new k(this));
        n35.A.setOnClickListener(new l(this));
        this.g = new g37<>(this, n35);
        return n35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        r76 r76 = this.h;
        if (r76 != null) {
            r76.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        r76 r76 = this.h;
        if (r76 != null) {
            r76.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void p6(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<n35> g37 = this.g;
        if (g37 != null) {
            n35 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.J) != null) {
                pq7.b(flexibleSwitchCompat, "it");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void s4(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(m, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    g37<n35> g37 = this.g;
                    if (g37 != null) {
                        n35 a2 = g37.a();
                        if (a2 != null) {
                            RTLImageView rTLImageView = a2.z;
                            pq7.b(rTLImageView, "it.ivArrivalTime");
                            rTLImageView.setAlpha(1.0f);
                            RTLImageView rTLImageView2 = a2.B;
                            pq7.b(rTLImageView2, "it.ivTravelTime");
                            rTLImageView2.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                g37<n35> g372 = this.g;
                if (g372 != null) {
                    n35 a3 = g372.a();
                    if (a3 != null) {
                        RTLImageView rTLImageView3 = a3.B;
                        pq7.b(rTLImageView3, "it.ivTravelTime");
                        rTLImageView3.setAlpha(1.0f);
                        RTLImageView rTLImageView4 = a3.z;
                        pq7.b(rTLImageView4, "it.ivArrivalTime");
                        rTLImageView4.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void w2(String str) {
        boolean z = true;
        pq7.c(str, "userCurrentAddress");
        if (isActive()) {
            g37<n35> g37 = this.g;
            if (g37 != null) {
                n35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    pq7.b(text, "it.autocompletePlaces.text");
                    if (wt7.u0(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText((CharSequence) str, false);
                            this.k = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.s76
    public void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            r76 r76 = this.h;
            if (r76 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", r76.o());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
