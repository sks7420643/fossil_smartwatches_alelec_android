package com.fossil;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gh7 {
    @DexIgnore
    public static th7 l; // = ei7.p();
    @DexIgnore
    public static Context m; // = null;
    @DexIgnore
    public static gh7 n; // = null;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ph7 f1309a; // = null;
    @DexIgnore
    public ph7 b; // = null;
    @DexIgnore
    public yh7 c; // = null;
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public volatile int f; // = 0;
    @DexIgnore
    public uh7 g; // = null;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public ConcurrentHashMap<ng7, String> i; // = null;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public HashMap<String, String> k; // = new HashMap<>();

    @DexIgnore
    public gh7(Context context) {
        try {
            this.c = new yh7();
            m = context.getApplicationContext();
            this.i = new ConcurrentHashMap<>();
            this.d = ei7.N(context);
            this.e = "pri_" + ei7.N(context);
            this.f1309a = new ph7(m, this.d);
            this.b = new ph7(m, this.e);
            s(true);
            s(false);
            F();
            v(m);
            D();
            J();
        } catch (Throwable th) {
            l.e(th);
        }
    }

    @DexIgnore
    public static gh7 b(Context context) {
        if (n == null) {
            synchronized (gh7.class) {
                try {
                    if (n == null) {
                        n = new gh7(context);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return n;
    }

    @DexIgnore
    public static gh7 u() {
        return n;
    }

    @DexIgnore
    public final SQLiteDatabase A(boolean z) {
        return (!z ? this.f1309a : this.b).getWritableDatabase();
    }

    @DexIgnore
    public void B() {
        if (fg7.M()) {
            try {
                this.c.a(new jh7(this));
            } catch (Throwable th) {
                l.e(th);
            }
        }
    }

    @DexIgnore
    public final SQLiteDatabase C(boolean z) {
        return (!z ? this.f1309a : this.b).getReadableDatabase();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004a A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void D() {
        /*
            r9 = this;
            r8 = 0
            com.fossil.ph7 r0 = r9.f1309a     // Catch:{ all -> 0x005b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ all -> 0x005b }
            java.lang.String r1 = "config"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x005b }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x004e
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ all -> 0x0042 }
            r2 = 1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ all -> 0x0042 }
            r3 = 2
            r1.getString(r3)     // Catch:{ all -> 0x0042 }
            r3 = 3
            int r3 = r1.getInt(r3)     // Catch:{ all -> 0x0042 }
            com.fossil.ni7 r4 = new com.fossil.ni7     // Catch:{ all -> 0x0042 }
            r4.<init>(r0)     // Catch:{ all -> 0x0042 }
            r4.f2529a = r0     // Catch:{ all -> 0x0042 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ all -> 0x0042 }
            r0.<init>(r2)     // Catch:{ all -> 0x0042 }
            r4.b = r0     // Catch:{ all -> 0x0042 }
            r4.d = r3     // Catch:{ all -> 0x0042 }
            android.content.Context r0 = com.fossil.gh7.m     // Catch:{ all -> 0x0042 }
            com.fossil.fg7.f(r0, r4)     // Catch:{ all -> 0x0042 }
            goto L_0x0013
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x0054 }
            r2.e(r0)     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x004d
            r1.close()
        L_0x004d:
            return
        L_0x004e:
            if (r1 == 0) goto L_0x004d
            r1.close()
            goto L_0x004d
        L_0x0054:
            r0 = move-exception
            if (r1 == 0) goto L_0x005a
            r1.close()
        L_0x005a:
            throw r0
        L_0x005b:
            r0 = move-exception
            r1 = r8
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.D():void");
    }

    @DexIgnore
    public final void F() {
        this.f = G() + H();
    }

    @DexIgnore
    public final int G() {
        return (int) DatabaseUtils.queryNumEntries(this.f1309a.getReadableDatabase(), "events");
    }

    @DexIgnore
    public final int H() {
        return (int) DatabaseUtils.queryNumEntries(this.b.getReadableDatabase(), "events");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc A[SYNTHETIC, Splitter:B:29:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00db  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void I() {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.I():void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void J() {
        /*
            r9 = this;
            r8 = 0
            com.fossil.ph7 r0 = r9.f1309a     // Catch:{ all -> 0x0042 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = "keyvalues"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0042 }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0035
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r9.k     // Catch:{ all -> 0x0029 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ all -> 0x0029 }
            r3 = 1
            java.lang.String r3 = r1.getString(r3)     // Catch:{ all -> 0x0029 }
            r0.put(r2, r3)     // Catch:{ all -> 0x0029 }
            goto L_0x0013
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x003b }
            r2.e(r0)     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0034
            r1.close()
        L_0x0034:
            return
        L_0x0035:
            if (r1 == 0) goto L_0x0034
            r1.close()
            goto L_0x0034
        L_0x003b:
            r0 = move-exception
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            throw r0
        L_0x0042:
            r0 = move-exception
            r1 = r8
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.J():void");
    }

    @DexIgnore
    public int a() {
        return this.f;
    }

    @DexIgnore
    public final String c(List<qh7> list) {
        StringBuilder sb = new StringBuilder(list.size() * 3);
        sb.append("event_id in (");
        int size = list.size();
        int i2 = 0;
        for (qh7 qh7 : list) {
            sb.append(qh7.f2983a);
            if (i2 != size - 1) {
                sb.append(",");
            }
            i2++;
        }
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public void d(int i2) {
        this.c.a(new oh7(this, i2));
    }

    @DexIgnore
    public final void e(int i2, boolean z) {
        synchronized (this) {
            try {
                if (this.f > 0 && i2 > 0 && !ig7.g()) {
                    if (fg7.K()) {
                        th7 th7 = l;
                        th7.h("Load " + this.f + " unsent events");
                    }
                    ArrayList arrayList = new ArrayList(i2);
                    z(arrayList, i2, z);
                    if (arrayList.size() > 0) {
                        if (fg7.K()) {
                            th7 th72 = l;
                            th72.h("Peek " + arrayList.size() + " unsent events.");
                        }
                        o(arrayList, 2, z);
                        qi7.f(m).g(arrayList, new nh7(this, arrayList, z));
                    }
                }
            } catch (Throwable th) {
                l.e(th);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f(com.fossil.ng7 r7, com.fossil.pi7 r8, boolean r9) {
        /*
            r6 = this;
            r1 = 0
            android.database.sqlite.SQLiteDatabase r1 = r6.A(r9)     // Catch:{ all -> 0x00bb }
            r1.beginTransaction()     // Catch:{ all -> 0x00ef }
            if (r9 != 0) goto L_0x002d
            int r0 = r6.f     // Catch:{ all -> 0x00ef }
            int r2 = com.fossil.fg7.C()     // Catch:{ all -> 0x00ef }
            if (r0 <= r2) goto L_0x002d
            com.fossil.th7 r0 = com.fossil.gh7.l     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = "Too many events stored in db."
            r0.m(r2)     // Catch:{ all -> 0x00ef }
            int r0 = r6.f     // Catch:{ all -> 0x00ef }
            com.fossil.ph7 r2 = r6.f1309a     // Catch:{ all -> 0x00ef }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ all -> 0x00ef }
            java.lang.String r3 = "events"
            java.lang.String r4 = "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)"
            r5 = 0
            int r2 = r2.delete(r3, r4, r5)     // Catch:{ all -> 0x00ef }
            int r0 = r0 - r2
            r6.f = r0     // Catch:{ all -> 0x00ef }
        L_0x002d:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x00ef }
            r0.<init>()     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = r7.h()     // Catch:{ all -> 0x00ef }
            boolean r3 = com.fossil.fg7.K()     // Catch:{ all -> 0x00ef }
            if (r3 == 0) goto L_0x004f
            com.fossil.th7 r3 = com.fossil.gh7.l     // Catch:{ all -> 0x00ef }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ef }
            java.lang.String r5 = "insert 1 event, content:"
            r4.<init>(r5)     // Catch:{ all -> 0x00ef }
            r4.append(r2)     // Catch:{ all -> 0x00ef }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00ef }
            r3.h(r4)     // Catch:{ all -> 0x00ef }
        L_0x004f:
            java.lang.String r3 = "content"
            java.lang.String r2 = com.fossil.ji7.g(r2)     // Catch:{ all -> 0x00ef }
            r0.put(r3, r2)     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = "send_count"
            java.lang.String r3 = "0"
            r0.put(r2, r3)     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = "status"
            r3 = 1
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ all -> 0x00ef }
            r0.put(r2, r3)     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = "timestamp"
            long r4 = r7.d()     // Catch:{ all -> 0x00ef }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x00ef }
            r0.put(r2, r3)     // Catch:{ all -> 0x00ef }
            java.lang.String r2 = "events"
            r3 = 0
            long r2 = r1.insert(r2, r3, r0)     // Catch:{ all -> 0x00ef }
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x00ef }
            if (r1 == 0) goto L_0x0085
            r1.endTransaction()     // Catch:{ all -> 0x00b4 }
        L_0x0085:
            r0 = 0
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c9
            int r0 = r6.f
            int r0 = r0 + 1
            r6.f = r0
            boolean r0 = com.fossil.fg7.K()
            if (r0 == 0) goto L_0x00ae
            com.fossil.th7 r0 = com.fossil.gh7.l
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "directStoreEvent insert event to db, event:"
            r1.<init>(r2)
            java.lang.String r2 = r7.h()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
        L_0x00ae:
            if (r8 == 0) goto L_0x00b3
            r8.a()
        L_0x00b3:
            return
        L_0x00b4:
            r0 = move-exception
            com.fossil.th7 r1 = com.fossil.gh7.l
            r1.e(r0)
            goto L_0x0085
        L_0x00bb:
            r0 = move-exception
        L_0x00bc:
            r2 = -1
            com.fossil.th7 r4 = com.fossil.gh7.l     // Catch:{ all -> 0x00e1 }
            r4.e(r0)     // Catch:{ all -> 0x00e1 }
            if (r1 == 0) goto L_0x0085
            r1.endTransaction()
            goto L_0x0085
        L_0x00c9:
            com.fossil.th7 r0 = com.fossil.gh7.l
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Failed to store event:"
            r1.<init>(r2)
            java.lang.String r2 = r7.h()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.f(r1)
            goto L_0x00b3
        L_0x00e1:
            r0 = move-exception
            if (r1 == 0) goto L_0x00e7
            r1.endTransaction()     // Catch:{ all -> 0x00e8 }
        L_0x00e7:
            throw r0
        L_0x00e8:
            r1 = move-exception
            com.fossil.th7 r2 = com.fossil.gh7.l
            r2.e(r1)
            goto L_0x00e7
        L_0x00ef:
            r0 = move-exception
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.f(com.fossil.ng7, com.fossil.pi7, boolean):void");
    }

    @DexIgnore
    public void g(ng7 ng7, pi7 pi7, boolean z, boolean z2) {
        yh7 yh7 = this.c;
        if (yh7 != null) {
            yh7.a(new kh7(this, ng7, pi7, z, z2));
        }
    }

    @DexIgnore
    public void n(ni7 ni7) {
        if (ni7 != null) {
            this.c.a(new lh7(this, ni7));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b0 A[SYNTHETIC, Splitter:B:32:0x00b0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void o(java.util.List<com.fossil.qh7> r7, int r8, boolean r9) {
        /*
            r6 = this;
            r2 = 0
            monitor-enter(r6)
            int r0 = r7.size()     // Catch:{ all -> 0x00d5 }
            if (r0 != 0) goto L_0x000a
            monitor-exit(r6)
        L_0x0009:
            return
        L_0x000a:
            int r3 = r6.t(r9)
            android.database.sqlite.SQLiteDatabase r1 = r6.A(r9)     // Catch:{ all -> 0x00bf }
            r0 = 2
            if (r8 != r0) goto L_0x0073
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "update events set status="
            r0.<init>(r3)     // Catch:{ all -> 0x00a8 }
            r0.append(r8)     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = ", send_count=send_count+1  where "
            r0.append(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = r6.c(r7)     // Catch:{ all -> 0x00a8 }
            r0.append(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a8 }
        L_0x002f:
            boolean r3 = com.fossil.fg7.K()     // Catch:{ all -> 0x00a8 }
            if (r3 == 0) goto L_0x0048
            com.fossil.th7 r3 = com.fossil.gh7.l     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r5 = "update sql:"
            r4.<init>(r5)     // Catch:{ all -> 0x00a8 }
            r4.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00a8 }
            r3.h(r4)     // Catch:{ all -> 0x00a8 }
        L_0x0048:
            r1.beginTransaction()     // Catch:{ all -> 0x00a8 }
            r1.execSQL(r0)     // Catch:{ all -> 0x00a8 }
            if (r2 == 0) goto L_0x0069
            com.fossil.th7 r0 = com.fossil.gh7.l     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = "update for delete sql:"
            r3.<init>(r4)     // Catch:{ all -> 0x00a8 }
            r3.append(r2)     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00a8 }
            r0.h(r3)     // Catch:{ all -> 0x00a8 }
            r1.execSQL(r2)     // Catch:{ all -> 0x00a8 }
            r6.F()     // Catch:{ all -> 0x00a8 }
        L_0x0069:
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x00cb
            r1.endTransaction()     // Catch:{ all -> 0x00b6 }
            monitor-exit(r6)
            goto L_0x0009
        L_0x0073:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = "update events set status="
            r0.<init>(r4)
            r0.append(r8)
            java.lang.String r4 = " where "
            r0.append(r4)
            java.lang.String r4 = r6.c(r7)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            int r4 = r6.h
            int r4 = r4 % 3
            if (r4 != 0) goto L_0x00a1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "delete from events where send_count>"
            r2.<init>(r4)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x00a1:
            int r3 = r6.h
            int r3 = r3 + 1
            r6.h = r3
            goto L_0x002f
        L_0x00a8:
            r0 = move-exception
        L_0x00a9:
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x00ce }
            r2.e(r0)     // Catch:{ all -> 0x00ce }
            if (r1 == 0) goto L_0x00cb
            r1.endTransaction()     // Catch:{ all -> 0x00c2 }
            monitor-exit(r6)
            goto L_0x0009
        L_0x00b6:
            r0 = move-exception
            com.fossil.th7 r1 = com.fossil.gh7.l
            r1.e(r0)
            monitor-exit(r6)
            goto L_0x0009
        L_0x00bf:
            r0 = move-exception
            r1 = r2
            goto L_0x00a9
        L_0x00c2:
            r0 = move-exception
            com.fossil.th7 r1 = com.fossil.gh7.l
            r1.e(r0)
            monitor-exit(r6)
            goto L_0x0009
        L_0x00cb:
            monitor-exit(r6)
            goto L_0x0009
        L_0x00ce:
            r0 = move-exception
            if (r1 == 0) goto L_0x00d4
            r1.endTransaction()     // Catch:{ all -> 0x00d8 }
        L_0x00d4:
            throw r0
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x00d8:
            r1 = move-exception
            com.fossil.th7 r2 = com.fossil.gh7.l
            r2.e(r1)
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.o(java.util.List, int, boolean):void");
    }

    @DexIgnore
    public void p(List<qh7> list, int i2, boolean z, boolean z2) {
        yh7 yh7 = this.c;
        if (yh7 != null) {
            yh7.a(new hh7(this, list, i2, z, z2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d0 A[SYNTHETIC, Splitter:B:35:0x00d0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void q(java.util.List<com.fossil.qh7> r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 245
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.q(java.util.List, boolean):void");
    }

    @DexIgnore
    public void r(List<qh7> list, boolean z, boolean z2) {
        yh7 yh7 = this.c;
        if (yh7 != null) {
            yh7.a(new ih7(this, list, z, z2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void s(boolean r9) {
        /*
            r8 = this;
            android.database.sqlite.SQLiteDatabase r1 = r8.A(r9)     // Catch:{ all -> 0x0058 }
            r1.beginTransaction()     // Catch:{ all -> 0x0073 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x0073 }
            r0.<init>()     // Catch:{ all -> 0x0073 }
            java.lang.String r2 = "status"
            r3 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0073 }
            r0.put(r2, r3)     // Catch:{ all -> 0x0073 }
            java.lang.String r2 = "events"
            java.lang.String r3 = "status=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0073 }
            r5 = 0
            r6 = 2
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ all -> 0x0073 }
            r4[r5] = r6     // Catch:{ all -> 0x0073 }
            int r0 = r1.update(r2, r0, r3, r4)     // Catch:{ all -> 0x0073 }
            boolean r2 = com.fossil.fg7.K()     // Catch:{ all -> 0x0073 }
            if (r2 == 0) goto L_0x0048
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
            java.lang.String r4 = "update "
            r3.<init>(r4)     // Catch:{ all -> 0x0073 }
            r3.append(r0)     // Catch:{ all -> 0x0073 }
            java.lang.String r0 = " unsent events."
            r3.append(r0)     // Catch:{ all -> 0x0073 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0073 }
            r2.h(r0)     // Catch:{ all -> 0x0073 }
        L_0x0048:
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x0073 }
            if (r1 == 0) goto L_0x0050
            r1.endTransaction()     // Catch:{ all -> 0x0051 }
        L_0x0050:
            return
        L_0x0051:
            r0 = move-exception
            com.fossil.th7 r1 = com.fossil.gh7.l
            r1.e(r0)
            goto L_0x0050
        L_0x0058:
            r0 = move-exception
            r1 = 0
        L_0x005a:
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x0065 }
            r2.e(r0)     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0050
            r1.endTransaction()
            goto L_0x0050
        L_0x0065:
            r0 = move-exception
            if (r1 == 0) goto L_0x006b
            r1.endTransaction()     // Catch:{ all -> 0x006c }
        L_0x006b:
            throw r0
        L_0x006c:
            r1 = move-exception
            com.fossil.th7 r2 = com.fossil.gh7.l
            r2.e(r1)
            goto L_0x006b
        L_0x0073:
            r0 = move-exception
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.s(boolean):void");
    }

    @DexIgnore
    public final int t(boolean z) {
        return !z ? fg7.B() : fg7.z();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01aa A[SYNTHETIC, Splitter:B:66:0x01aa] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x021b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.uh7 v(android.content.Context r15) {
        /*
        // Method dump skipped, instructions count: 549
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.v(android.content.Context):com.fossil.uh7");
    }

    @DexIgnore
    public final void w(int i2, boolean z) {
        int G = i2 == -1 ? !z ? G() : H() : i2;
        if (G > 0) {
            int F = fg7.F() * 60 * fg7.D();
            if (G > F && F > 0) {
                G = F;
            }
            int a2 = fg7.a();
            int i3 = G / a2;
            int i4 = G % a2;
            if (fg7.K()) {
                th7 th7 = l;
                th7.h("sentStoreEventsByDb sendNumbers=" + G + ",important=" + z + ",maxSendNumPerFor1Period=" + F + ",maxCount=" + i3 + ",restNumbers=" + i4);
            }
            for (int i5 = 0; i5 < i3; i5++) {
                e(a2, z);
            }
            if (i4 > 0) {
                e(i4, z);
            }
        }
    }

    @DexIgnore
    public final void x(ng7 ng7, pi7 pi7, boolean z, boolean z2) {
        synchronized (this) {
            if (fg7.C() > 0) {
                if (fg7.J <= 0 || z || z2) {
                    f(ng7, pi7, z);
                } else if (fg7.J > 0) {
                    if (fg7.K()) {
                        th7 th7 = l;
                        th7.h("cacheEventsInMemory.size():" + this.i.size() + ",numEventsCachedInMemory:" + fg7.J + ",numStoredEvents:" + this.f);
                        th7 th72 = l;
                        StringBuilder sb = new StringBuilder("cache event:");
                        sb.append(ng7.h());
                        th72.h(sb.toString());
                    }
                    this.i.put(ng7, "");
                    if (this.i.size() >= fg7.J) {
                        I();
                    }
                    if (pi7 != null) {
                        if (this.i.size() > 0) {
                            I();
                        }
                        pi7.a();
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00df A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void y(com.fossil.ni7 r14) {
        /*
        // Method dump skipped, instructions count: 267
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.y(com.fossil.ni7):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0082 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void z(java.util.List<com.fossil.qh7> r11, int r12, boolean r13) {
        /*
            r10 = this;
            r9 = 0
            android.database.sqlite.SQLiteDatabase r0 = r10.C(r13)     // Catch:{ all -> 0x0093 }
            r1 = 1
            java.lang.String r5 = java.lang.Integer.toString(r1)     // Catch:{ all -> 0x0093 }
            java.lang.String r8 = java.lang.Integer.toString(r12)     // Catch:{ all -> 0x0093 }
            java.lang.String r1 = "events"
            r2 = 0
            java.lang.String r3 = "status=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0093 }
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x0093 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0093 }
        L_0x0020:
            boolean r0 = r7.moveToNext()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0086
            r0 = 0
            long r2 = r7.getLong(r0)     // Catch:{ all -> 0x0079 }
            r0 = 1
            java.lang.String r4 = r7.getString(r0)     // Catch:{ all -> 0x0079 }
            boolean r0 = com.fossil.fg7.v     // Catch:{ all -> 0x0079 }
            if (r0 != 0) goto L_0x0038
            java.lang.String r4 = com.fossil.ji7.b(r4)     // Catch:{ all -> 0x0079 }
        L_0x0038:
            r0 = 2
            int r5 = r7.getInt(r0)     // Catch:{ all -> 0x0079 }
            r0 = 3
            int r6 = r7.getInt(r0)     // Catch:{ all -> 0x0079 }
            com.fossil.qh7 r1 = new com.fossil.qh7     // Catch:{ all -> 0x0079 }
            r1.<init>(r2, r4, r5, r6)     // Catch:{ all -> 0x0079 }
            boolean r0 = com.fossil.fg7.K()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0075
            com.fossil.th7 r0 = com.fossil.gh7.l     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            java.lang.String r5 = "peek event, id="
            r4.<init>(r5)     // Catch:{ all -> 0x0079 }
            r4.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = ",send_count="
            r4.append(r2)     // Catch:{ all -> 0x0079 }
            r4.append(r6)     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = ",timestamp="
            r4.append(r2)     // Catch:{ all -> 0x0079 }
            r2 = 4
            long r2 = r7.getLong(r2)     // Catch:{ all -> 0x0079 }
            r4.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = r4.toString()     // Catch:{ all -> 0x0079 }
            r0.h(r2)     // Catch:{ all -> 0x0079 }
        L_0x0075:
            r11.add(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x0020
        L_0x0079:
            r0 = move-exception
            r1 = r7
        L_0x007b:
            com.fossil.th7 r2 = com.fossil.gh7.l     // Catch:{ all -> 0x008c }
            r2.e(r0)     // Catch:{ all -> 0x008c }
            if (r1 == 0) goto L_0x0085
            r1.close()
        L_0x0085:
            return
        L_0x0086:
            if (r7 == 0) goto L_0x0085
            r7.close()
            goto L_0x0085
        L_0x008c:
            r0 = move-exception
            if (r1 == 0) goto L_0x0092
            r1.close()
        L_0x0092:
            throw r0
        L_0x0093:
            r0 = move-exception
            r1 = r9
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh7.z(java.util.List, int, boolean):void");
    }
}
