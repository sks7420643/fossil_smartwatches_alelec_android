package com.fossil;

import android.os.SystemClock;
import android.text.TextUtils;
import com.fossil.a91;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z91 implements a91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, a> f4434a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f4435a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public /* final */ long e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public /* final */ List<f91> h;

        @DexIgnore
        public a(String str, a91.a aVar) {
            this(str, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, a(aVar));
            this.f4435a = (long) aVar.f225a.length;
        }

        @DexIgnore
        public a(String str, String str2, long j, long j2, long j3, long j4, List<f91> list) {
            this.b = str;
            this.c = "".equals(str2) ? null : str2;
            this.d = j;
            this.e = j2;
            this.f = j3;
            this.g = j4;
            this.h = list;
        }

        @DexIgnore
        public static List<f91> a(a91.a aVar) {
            List<f91> list = aVar.h;
            return list != null ? list : ba1.f(aVar.g);
        }

        @DexIgnore
        public static a b(b bVar) throws IOException {
            if (z91.l(bVar) == 538247942) {
                return new a(z91.n(bVar), z91.n(bVar), z91.m(bVar), z91.m(bVar), z91.m(bVar), z91.m(bVar), z91.k(bVar));
            }
            throw new IOException();
        }

        @DexIgnore
        public a91.a c(byte[] bArr) {
            a91.a aVar = new a91.a();
            aVar.f225a = bArr;
            aVar.b = this.c;
            aVar.c = this.d;
            aVar.d = this.e;
            aVar.e = this.f;
            aVar.f = this.g;
            aVar.g = ba1.g(this.h);
            aVar.h = Collections.unmodifiableList(this.h);
            return aVar;
        }

        @DexIgnore
        public boolean d(OutputStream outputStream) {
            try {
                z91.s(outputStream, 538247942);
                z91.u(outputStream, this.b);
                z91.u(outputStream, this.c == null ? "" : this.c);
                z91.t(outputStream, this.d);
                z91.t(outputStream, this.e);
                z91.t(outputStream, this.f);
                z91.t(outputStream, this.g);
                z91.r(this.h, outputStream);
                outputStream.flush();
                return true;
            } catch (IOException e2) {
                u91.b("%s", e2.toString());
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends FilterInputStream {
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public long c;

        @DexIgnore
        public b(InputStream inputStream, long j) {
            super(inputStream);
            this.b = j;
        }

        @DexIgnore
        public long a() {
            return this.b - this.c;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.c++;
            }
            return read;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.c += (long) read;
            }
            return read;
        }
    }

    @DexIgnore
    public z91(File file) {
        this(file, FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    public z91(File file, int i) {
        this.f4434a = new LinkedHashMap(16, 0.75f, true);
        this.b = 0;
        this.c = file;
        this.d = i;
    }

    @DexIgnore
    public static int j(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    @DexIgnore
    public static List<f91> k(b bVar) throws IOException {
        int l = l(bVar);
        if (l >= 0) {
            List<f91> emptyList = l == 0 ? Collections.emptyList() : new ArrayList<>();
            for (int i = 0; i < l; i++) {
                emptyList.add(new f91(n(bVar).intern(), n(bVar).intern()));
            }
            return emptyList;
        }
        throw new IOException("readHeaderList size=" + l);
    }

    @DexIgnore
    public static int l(InputStream inputStream) throws IOException {
        return (j(inputStream) << 0) | 0 | (j(inputStream) << 8) | (j(inputStream) << 16) | (j(inputStream) << 24);
    }

    @DexIgnore
    public static long m(InputStream inputStream) throws IOException {
        return ((((long) j(inputStream)) & 255) << 0) | 0 | ((((long) j(inputStream)) & 255) << 8) | ((((long) j(inputStream)) & 255) << 16) | ((((long) j(inputStream)) & 255) << 24) | ((((long) j(inputStream)) & 255) << 32) | ((((long) j(inputStream)) & 255) << 40) | ((((long) j(inputStream)) & 255) << 48) | ((((long) j(inputStream)) & 255) << 56);
    }

    @DexIgnore
    public static String n(b bVar) throws IOException {
        return new String(q(bVar, m(bVar)), "UTF-8");
    }

    @DexIgnore
    public static byte[] q(b bVar, long j) throws IOException {
        long a2 = bVar.a();
        if (j >= 0 && j <= a2) {
            int i = (int) j;
            if (((long) i) == j) {
                byte[] bArr = new byte[i];
                new DataInputStream(bVar).readFully(bArr);
                return bArr;
            }
        }
        throw new IOException("streamToBytes length=" + j + ", maxLength=" + a2);
    }

    @DexIgnore
    public static void r(List<f91> list, OutputStream outputStream) throws IOException {
        if (list != null) {
            s(outputStream, list.size());
            for (f91 f91 : list) {
                u(outputStream, f91.a());
                u(outputStream, f91.b());
            }
            return;
        }
        s(outputStream, 0);
    }

    @DexIgnore
    public static void s(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    @DexIgnore
    public static void t(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    @DexIgnore
    public static void u(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        t(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    @DexIgnore
    @Override // com.fossil.a91
    public void a() {
        synchronized (this) {
            if (!this.c.exists()) {
                if (!this.c.mkdirs()) {
                    u91.c("Unable to create cache dir %s", this.c.getAbsolutePath());
                }
                return;
            }
            File[] listFiles = this.c.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    try {
                        long length = file.length();
                        b bVar = new b(new BufferedInputStream(d(file)), length);
                        try {
                            a b2 = a.b(bVar);
                            b2.f4435a = length;
                            i(b2.b, b2);
                        } finally {
                            bVar.close();
                        }
                    } catch (IOException e) {
                        file.delete();
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a91
    public a91.a b(String str) {
        synchronized (this) {
            a aVar = this.f4434a.get(str);
            if (aVar == null) {
                return null;
            }
            File f = f(str);
            try {
                b bVar = new b(new BufferedInputStream(d(f)), f.length());
                try {
                    a b2 = a.b(bVar);
                    if (!TextUtils.equals(str, b2.b)) {
                        u91.b("%s: key=%s, found=%s", f.getAbsolutePath(), str, b2.b);
                        p(str);
                        return null;
                    }
                    a91.a c2 = aVar.c(q(bVar, bVar.a()));
                    bVar.close();
                    return c2;
                } finally {
                    bVar.close();
                }
            } catch (IOException e) {
                u91.b("%s: %s", f.getAbsolutePath(), e.toString());
                o(str);
                return null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a91
    public void c(String str, a91.a aVar) {
        synchronized (this) {
            h(aVar.f225a.length);
            File f = f(str);
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(e(f));
                a aVar2 = new a(str, aVar);
                if (aVar2.d(bufferedOutputStream)) {
                    bufferedOutputStream.write(aVar.f225a);
                    bufferedOutputStream.close();
                    i(str, aVar2);
                } else {
                    bufferedOutputStream.close();
                    u91.b("Failed to write header for %s", f.getAbsolutePath());
                    throw new IOException();
                }
            } catch (IOException e) {
                if (!f.delete()) {
                    u91.b("Could not clean up file %s", f.getAbsolutePath());
                }
            }
        }
    }

    @DexIgnore
    public InputStream d(File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @DexIgnore
    public OutputStream e(File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }

    @DexIgnore
    public File f(String str) {
        return new File(this.c, g(str));
    }

    @DexIgnore
    public final String g(String str) {
        int length = str.length() / 2;
        int hashCode = str.substring(0, length).hashCode();
        return String.valueOf(hashCode) + String.valueOf(str.substring(length).hashCode());
    }

    @DexIgnore
    public final void h(int i) {
        long j = (long) i;
        if (this.b + j >= ((long) this.d)) {
            if (u91.b) {
                u91.e("Pruning old cache entries.", new Object[0]);
            }
            long j2 = this.b;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, a>> it = this.f4434a.entrySet().iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = i2;
                    break;
                }
                a value = it.next().getValue();
                if (f(value.b).delete()) {
                    this.b -= value.f4435a;
                } else {
                    String str = value.b;
                    u91.b("Could not delete cache entry for key=%s, filename=%s", str, g(str));
                }
                it.remove();
                i2++;
                if (((float) (this.b + j)) < ((float) this.d) * 0.9f) {
                    break;
                }
            }
            if (u91.b) {
                u91.e("pruned %d files, %d bytes, %d ms", Integer.valueOf(i2), Long.valueOf(this.b - j2), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            }
        }
    }

    @DexIgnore
    public final void i(String str, a aVar) {
        if (!this.f4434a.containsKey(str)) {
            this.b += aVar.f4435a;
        } else {
            this.b = (aVar.f4435a - this.f4434a.get(str).f4435a) + this.b;
        }
        this.f4434a.put(str, aVar);
    }

    @DexIgnore
    public void o(String str) {
        synchronized (this) {
            boolean delete = f(str).delete();
            p(str);
            if (!delete) {
                u91.b("Could not delete cache entry for key=%s, filename=%s", str, g(str));
            }
        }
    }

    @DexIgnore
    public final void p(String str) {
        a remove = this.f4434a.remove(str);
        if (remove != null) {
            this.b -= remove.f4435a;
        }
    }
}
