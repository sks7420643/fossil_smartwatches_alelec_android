package com.fossil;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ TextPaint f1559a; // = new TextPaint(1);
    @DexIgnore
    public /* final */ rz3 b; // = new a();
    @DexIgnore
    public float c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public WeakReference<b> e; // = new WeakReference<>(null);
    @DexIgnore
    public pz3 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends rz3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.rz3
        public void a(int i) {
            hz3.this.d = true;
            b bVar = (b) hz3.this.e.get();
            if (bVar != null) {
                bVar.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.rz3
        public void b(Typeface typeface, boolean z) {
            if (!z) {
                hz3.this.d = true;
                b bVar = (b) hz3.this.e.get();
                if (bVar != null) {
                    bVar.a();
                }
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        int[] getState();

        @DexIgnore
        boolean onStateChange(int[] iArr);
    }

    @DexIgnore
    public hz3(b bVar) {
        g(bVar);
    }

    @DexIgnore
    public final float c(CharSequence charSequence) {
        return charSequence == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.f1559a.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public pz3 d() {
        return this.f;
    }

    @DexIgnore
    public TextPaint e() {
        return this.f1559a;
    }

    @DexIgnore
    public float f(String str) {
        if (!this.d) {
            return this.c;
        }
        float c2 = c(str);
        this.c = c2;
        this.d = false;
        return c2;
    }

    @DexIgnore
    public void g(b bVar) {
        this.e = new WeakReference<>(bVar);
    }

    @DexIgnore
    public void h(pz3 pz3, Context context) {
        if (this.f != pz3) {
            this.f = pz3;
            if (pz3 != null) {
                pz3.j(context, this.f1559a, this.b);
                b bVar = this.e.get();
                if (bVar != null) {
                    this.f1559a.drawableState = bVar.getState();
                }
                pz3.i(context, this.f1559a, this.b);
                this.d = true;
            }
            b bVar2 = this.e.get();
            if (bVar2 != null) {
                bVar2.a();
                bVar2.onStateChange(bVar2.getState());
            }
        }
    }

    @DexIgnore
    public void i(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void j(Context context) {
        this.f.i(context, this.f1559a, this.b);
    }
}
