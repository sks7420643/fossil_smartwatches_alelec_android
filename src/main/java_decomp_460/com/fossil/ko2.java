package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko2 extends tn2 implements lo2 {
    @DexIgnore
    public ko2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    @Override // com.fossil.lo2
    public final void V2(wi2 wi2) throws RemoteException {
        Parcel d = d();
        qo2.b(d, wi2);
        e(3, d);
    }
}
