package com.fossil;

import android.os.Process;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicLong f2938a; // = new AtomicLong(0);
    @DexIgnore
    public static String b;

    @DexIgnore
    public q84(h94 h94) {
        byte[] bArr = new byte[10];
        e(bArr);
        d(bArr);
        c(bArr);
        String H = r84.H(h94.a());
        String z = r84.z(bArr);
        b = String.format(Locale.US, "%s-%s-%s-%s", z.substring(0, 12), z.substring(12, 16), z.subSequence(16, 20), H.substring(0, 12)).toUpperCase(Locale.US);
    }

    @DexIgnore
    public static byte[] a(long j) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt((int) j);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.position(0);
        return allocate.array();
    }

    @DexIgnore
    public static byte[] b(long j) {
        ByteBuffer allocate = ByteBuffer.allocate(2);
        allocate.putShort((short) ((int) j));
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.position(0);
        return allocate.array();
    }

    @DexIgnore
    public final void c(byte[] bArr) {
        byte[] b2 = b((long) Integer.valueOf(Process.myPid()).shortValue());
        bArr[8] = (byte) b2[0];
        bArr[9] = (byte) b2[1];
    }

    @DexIgnore
    public final void d(byte[] bArr) {
        byte[] b2 = b(f2938a.incrementAndGet());
        bArr[6] = (byte) b2[0];
        bArr[7] = (byte) b2[1];
    }

    @DexIgnore
    public final void e(byte[] bArr) {
        long time = new Date().getTime();
        byte[] a2 = a(time / 1000);
        bArr[0] = (byte) a2[0];
        bArr[1] = (byte) a2[1];
        bArr[2] = (byte) a2[2];
        bArr[3] = (byte) a2[3];
        byte[] b2 = b(time % 1000);
        bArr[4] = (byte) b2[0];
        bArr[5] = (byte) b2[1];
    }

    @DexIgnore
    public String toString() {
        return b;
    }
}
