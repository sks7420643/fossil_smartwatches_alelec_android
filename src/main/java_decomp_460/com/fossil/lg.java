package com.fossil;

import com.fossil.ix1;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.AUTHENTICATION));
    @DexIgnore
    public Boolean D;
    @DexIgnore
    public /* final */ byte[] E; // = new byte[8];
    @DexIgnore
    public /* final */ byte[] F;
    @DexIgnore
    public /* final */ byte[] G;

    @DexIgnore
    public lg(k5 k5Var, i60 i60, byte[] bArr) {
        super(k5Var, i60, yp.U, null, false, 24);
        this.G = bArr;
        byte[] copyOf = Arrays.copyOf(this.G, 16);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        this.F = copyOf;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        if (this.G.length < 16) {
            k(zq.INVALID_PARAMETER);
            return;
        }
        new SecureRandom().nextBytes(this.E);
        lp.i(this, new ft(this.w, hd0.y.b(), this.E), new jf(this), new xf(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.t2, Long.valueOf(ix1.f1688a.b(this.G, ix1.a.CRC32)));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(super.E(), jd0.B3, this.D);
    }

    @DexIgnore
    public final void H(byte[] bArr) {
        zq zqVar;
        if (bArr.length != 16) {
            zqVar = zq.INVALID_DATA_LENGTH;
        } else {
            byte[] a2 = rt.g.a(hd0.y.b(), lx1.f2275a.a(ji.K.b(), this.F, ji.K.a(), bArr));
            if (a2.length != 16) {
                zqVar = zq.INVALID_DATA_LENGTH;
            } else {
                if (Arrays.equals(this.E, dm7.k(a2, 8, 16))) {
                    this.D = Boolean.TRUE;
                }
                zqVar = zq.SUCCESS;
            }
        }
        l(nr.a(this.v, null, zqVar, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        Boolean bool = this.D;
        return Boolean.valueOf(bool != null ? bool.booleanValue() : false);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
