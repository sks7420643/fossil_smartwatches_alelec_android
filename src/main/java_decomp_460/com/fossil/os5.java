package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.g47;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class os5 extends pv5 implements t47.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public BlockingQueue<uh5> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<uh5> {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        /* renamed from: a */
        public final int compare(uh5 uh5, uh5 uh52) {
            return uh5.ordinal() - uh52.ordinal();
        }
    }

    /*
    static {
        String simpleName = os5.class.getSimpleName();
        pq7.b(simpleName, "BasePermissionFragment::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public final void K6(uh5 uh5) {
        BlockingQueue<uh5> blockingQueue = this.g;
        if (blockingQueue == null) {
            pq7.n("mPermissionQueue");
            throw null;
        } else if (!blockingQueue.contains(uh5)) {
            BlockingQueue<uh5> blockingQueue2 = this.g;
            if (blockingQueue2 != null) {
                blockingQueue2.offer(uh5);
            } else {
                pq7.n("mPermissionQueue");
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean L6() {
        boolean c = g47.f1261a.c(PortfolioApp.h0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenBackgroundLocationPermission() - isBackgroundLocationPermissionGranted = " + c);
        if (!c) {
            X6();
        }
        return !c;
    }

    @DexIgnore
    public final void M(uh5... uh5Arr) {
        boolean M6;
        pq7.c(uh5Arr, "permissionCodes");
        for (uh5 uh5 : uh5Arr) {
            K6(uh5);
        }
        BlockingQueue<uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            uh5 peek = blockingQueue.peek();
            FLogger.INSTANCE.getLocal().d(i, "processPermissionPopups() - permissionErrorCode = " + peek);
            if (peek == null) {
                W6();
                return;
            }
            switch (ns5.f2562a[peek.ordinal()]) {
                case 1:
                    M6 = M6();
                    break;
                case 2:
                case 3:
                    M6 = N6();
                    break;
                case 4:
                case 5:
                    M6 = O6();
                    break;
                case 6:
                    M6 = L6();
                    break;
                default:
                    throw new al7();
            }
            if (!M6) {
                BlockingQueue<uh5> blockingQueue2 = this.g;
                if (blockingQueue2 != null) {
                    blockingQueue2.remove(peek);
                    M(new uh5[0]);
                    return;
                }
                pq7.n("mPermissionQueue");
                throw null;
            }
            return;
        }
        pq7.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final boolean M6() {
        boolean e = g47.f1261a.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenBluetoothPermission() - isBluetoothEnabled = " + e);
        if (!e) {
            Y6();
        }
        return !e;
    }

    @DexIgnore
    public final boolean N6() {
        boolean d = g47.f1261a.d(PortfolioApp.h0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenLocationPermission() - isLocationPermissionGranted = " + d);
        if (!d) {
            Z6();
        }
        return !d;
    }

    @DexIgnore
    public final boolean O6() {
        boolean g2 = g47.f1261a.g();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenLocationService() - isLocationOpen = " + g2);
        if (!g2) {
            a7();
        }
        return !g2;
    }

    @DexIgnore
    public final void P6() {
        startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
    }

    @DexIgnore
    public final void Q6() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (pq7.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131362442) {
                V6();
                R6();
            } else if (i2 != 2131363373) {
                U6();
            } else {
                g47.a aVar = g47.f1261a;
                FragmentActivity requireActivity = requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.r(requireActivity, 0);
            }
        } else if (pq7.a(str, s37.c.f())) {
            if (i2 == 2131362442) {
                V6();
                R6();
            } else if (i2 != 2131363373) {
                U6();
            } else {
                g47.a aVar2 = g47.f1261a;
                FragmentActivity requireActivity2 = requireActivity();
                pq7.b(requireActivity2, "requireActivity()");
                aVar2.q(requireActivity2, 1);
            }
        } else if (pq7.a(str, "REQUEST_OPEN_LOCATION_SERVICE")) {
            if (i2 != 2131362442) {
                T6();
                return;
            }
            V6();
            Q6();
        } else if (!pq7.a(str, "BLUETOOTH_OFF")) {
        } else {
            if (i2 != 2131362442) {
                S6();
                return;
            }
            V6();
            P6();
        }
    }

    @DexIgnore
    public final void R6() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.h0.c().getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public abstract void S6();

    @DexIgnore
    public abstract void T6();

    @DexIgnore
    public abstract void U6();

    @DexIgnore
    public abstract void V6();

    @DexIgnore
    public abstract void W6();

    @DexIgnore
    public final void X6() {
        BlockingQueue<uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != uh5.BACKGROUND_LOCATION_PERMISSION_OFF) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.k0(childFragmentManager);
                return;
            }
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.i0(childFragmentManager2);
            return;
        }
        pq7.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.k(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Z6() {
        BlockingQueue<uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != uh5.LOCATION_PERMISSION_FEATURE_OFF) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.k0(childFragmentManager);
                return;
            }
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.j0(childFragmentManager2);
            return;
        }
        pq7.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void a7() {
        BlockingQueue<uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationService() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != uh5.LOCATION_SERVICE_FEATURE_OFF) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.T(childFragmentManager);
                return;
            }
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.S(childFragmentManager2);
            return;
        }
        pq7.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = new PriorityBlockingQueue(5, a.b);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.rk0.b, com.fossil.pv5, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        boolean z = true;
        pq7.c(strArr, "permissions");
        pq7.c(iArr, "grantResults");
        if (i2 != 0) {
            super.onRequestPermissionsResult(i2, strArr, iArr);
            return;
        }
        if (!(!(iArr.length == 0)) || iArr[0] != 0) {
            z = false;
        }
        if (z) {
            M(new uh5[0]);
        } else {
            U6();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
