package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.ig0;
import com.fossil.jg0;
import com.fossil.sn0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rg0 extends xf0 implements sn0.a {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ SparseBooleanArray C; // = new SparseBooleanArray();
    @DexIgnore
    public e D;
    @DexIgnore
    public a E;
    @DexIgnore
    public c F;
    @DexIgnore
    public b G;
    @DexIgnore
    public /* final */ f H; // = new f();
    @DexIgnore
    public int I;
    @DexIgnore
    public d k;
    @DexIgnore
    public Drawable l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hg0 {
        @DexIgnore
        public a(Context context, ng0 ng0, View view) {
            super(context, ng0, view, false, le0.actionOverflowMenuStyle);
            if (!((eg0) ng0.getItem()).l()) {
                View view2 = rg0.this.k;
                f(view2 == null ? (View) rg0.this.i : view2);
            }
            j(rg0.this.H);
        }

        @DexIgnore
        @Override // com.fossil.hg0
        public void e() {
            rg0 rg0 = rg0.this;
            rg0.E = null;
            rg0.I = 0;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ActionMenuItemView.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.appcompat.view.menu.ActionMenuItemView.b
        public lg0 a() {
            a aVar = rg0.this.E;
            if (aVar != null) {
                return aVar.c();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public e b;

        @DexIgnore
        public c(e eVar) {
            this.b = eVar;
        }

        @DexIgnore
        public void run() {
            if (rg0.this.d != null) {
                rg0.this.d.d();
            }
            View view = (View) rg0.this.i;
            if (!(view == null || view.getWindowToken() == null || !this.b.m())) {
                rg0.this.D = this.b;
            }
            rg0.this.F = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AppCompatImageView implements ActionMenuView.a {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends gh0 {
            @DexIgnore
            public a(View view, rg0 rg0) {
                super(view);
            }

            @DexIgnore
            @Override // com.fossil.gh0
            public lg0 b() {
                e eVar = rg0.this.D;
                if (eVar == null) {
                    return null;
                }
                return eVar.c();
            }

            @DexIgnore
            @Override // com.fossil.gh0
            public boolean c() {
                rg0.this.N();
                return true;
            }

            @DexIgnore
            @Override // com.fossil.gh0
            public boolean d() {
                rg0 rg0 = rg0.this;
                if (rg0.F != null) {
                    return false;
                }
                rg0.E();
                return true;
            }
        }

        @DexIgnore
        public d(Context context) {
            super(context, null, le0.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            vh0.a(this, getContentDescription());
            setOnTouchListener(new a(this, rg0.this));
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean c() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                rg0.this.N();
            }
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                am0.l(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends hg0 {
        @DexIgnore
        public e(Context context, cg0 cg0, View view, boolean z) {
            super(context, cg0, view, z, le0.actionOverflowMenuStyle);
            h(8388613);
            j(rg0.this.H);
        }

        @DexIgnore
        @Override // com.fossil.hg0
        public void e() {
            if (rg0.this.d != null) {
                rg0.this.d.close();
            }
            rg0.this.D = null;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements ig0.a {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public void b(cg0 cg0, boolean z) {
            if (cg0 instanceof ng0) {
                cg0.F().e(false);
            }
            ig0.a p = rg0.this.p();
            if (p != null) {
                p.b(cg0, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public boolean c(cg0 cg0) {
            if (cg0 == rg0.this.d) {
                return false;
            }
            rg0.this.I = ((ng0) cg0).getItem().getItemId();
            ig0.a p = rg0.this.p();
            return p != null ? p.c(cg0) : false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"BanParcelableUsage"})
    public static class g implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<g> CREATOR; // = new a();
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Parcelable.Creator<g> {
            @DexIgnore
            /* renamed from: a */
            public g createFromParcel(Parcel parcel) {
                return new g(parcel);
            }

            @DexIgnore
            /* renamed from: b */
            public g[] newArray(int i) {
                return new g[i];
            }
        }

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public g(Parcel parcel) {
            this.b = parcel.readInt();
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    public rg0(Context context) {
        super(context, re0.abc_action_menu_layout, re0.abc_action_menu_item_layout);
    }

    @DexIgnore
    public boolean B() {
        return E() | F();
    }

    @DexIgnore
    public final View C(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.i;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof jg0.a) && ((jg0.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public Drawable D() {
        d dVar = this.k;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.m) {
            return this.l;
        }
        return null;
    }

    @DexIgnore
    public boolean E() {
        jg0 jg0;
        c cVar = this.F;
        if (cVar == null || (jg0 = this.i) == null) {
            e eVar = this.D;
            if (eVar == null) {
                return false;
            }
            eVar.b();
            return true;
        }
        ((View) jg0).removeCallbacks(cVar);
        this.F = null;
        return true;
    }

    @DexIgnore
    public boolean F() {
        a aVar = this.E;
        if (aVar == null) {
            return false;
        }
        aVar.b();
        return true;
    }

    @DexIgnore
    public boolean G() {
        return this.F != null || H();
    }

    @DexIgnore
    public boolean H() {
        e eVar = this.D;
        return eVar != null && eVar.d();
    }

    @DexIgnore
    public void I(Configuration configuration) {
        if (!this.x) {
            this.w = of0.b(this.c).d();
        }
        cg0 cg0 = this.d;
        if (cg0 != null) {
            cg0.M(true);
        }
    }

    @DexIgnore
    public void J(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    public void K(ActionMenuView actionMenuView) {
        this.i = actionMenuView;
        actionMenuView.b(this.d);
    }

    @DexIgnore
    public void L(Drawable drawable) {
        d dVar = this.k;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.m = true;
        this.l = drawable;
    }

    @DexIgnore
    public void M(boolean z2) {
        this.s = z2;
        this.t = true;
    }

    @DexIgnore
    public boolean N() {
        cg0 cg0;
        if (!this.s || H() || (cg0 = this.d) == null || this.i == null || this.F != null || cg0.B().isEmpty()) {
            return false;
        }
        c cVar = new c(new e(this.c, this.d, this.k, true));
        this.F = cVar;
        ((View) this.i).post(cVar);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sn0.a
    public void a(boolean z2) {
        if (z2) {
            super.k(null);
            return;
        }
        cg0 cg0 = this.d;
        if (cg0 != null) {
            cg0.e(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.xf0, com.fossil.ig0
    public void b(cg0 cg0, boolean z2) {
        B();
        super.b(cg0, z2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008d  */
    @Override // com.fossil.xf0, com.fossil.ig0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(boolean r7) {
        /*
            r6 = this;
            r1 = 1
            r2 = 0
            super.c(r7)
            com.fossil.jg0 r0 = r6.i
            android.view.View r0 = (android.view.View) r0
            r0.requestLayout()
            com.fossil.cg0 r0 = r6.d
            if (r0 == 0) goto L_0x002e
            java.util.ArrayList r4 = r0.u()
            int r5 = r4.size()
            r3 = r2
        L_0x0019:
            if (r3 >= r5) goto L_0x002e
            java.lang.Object r0 = r4.get(r3)
            com.fossil.eg0 r0 = (com.fossil.eg0) r0
            com.fossil.sn0 r0 = r0.b()
            if (r0 == 0) goto L_0x002a
            r0.setSubUiVisibilityListener(r6)
        L_0x002a:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0019
        L_0x002e:
            com.fossil.cg0 r0 = r6.d
            if (r0 == 0) goto L_0x0087
            java.util.ArrayList r0 = r0.B()
        L_0x0036:
            boolean r3 = r6.s
            if (r3 == 0) goto L_0x00a1
            if (r0 == 0) goto L_0x00a1
            int r3 = r0.size()
            if (r3 != r1) goto L_0x0089
            java.lang.Object r0 = r0.get(r2)
            com.fossil.eg0 r0 = (com.fossil.eg0) r0
            boolean r0 = r0.isActionViewExpanded()
            r0 = r0 ^ 1
        L_0x004e:
            if (r0 == 0) goto L_0x008d
            com.fossil.rg0$d r0 = r6.k
            if (r0 != 0) goto L_0x005d
            com.fossil.rg0$d r0 = new com.fossil.rg0$d
            android.content.Context r1 = r6.b
            r0.<init>(r1)
            r6.k = r0
        L_0x005d:
            com.fossil.rg0$d r0 = r6.k
            android.view.ViewParent r0 = r0.getParent()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            com.fossil.jg0 r1 = r6.i
            if (r0 == r1) goto L_0x007d
            if (r0 == 0) goto L_0x0070
            com.fossil.rg0$d r1 = r6.k
            r0.removeView(r1)
        L_0x0070:
            com.fossil.jg0 r0 = r6.i
            androidx.appcompat.widget.ActionMenuView r0 = (androidx.appcompat.widget.ActionMenuView) r0
            com.fossil.rg0$d r1 = r6.k
            androidx.appcompat.widget.ActionMenuView$LayoutParams r2 = r0.i()
            r0.addView(r1, r2)
        L_0x007d:
            com.fossil.jg0 r0 = r6.i
            androidx.appcompat.widget.ActionMenuView r0 = (androidx.appcompat.widget.ActionMenuView) r0
            boolean r1 = r6.s
            r0.setOverflowReserved(r1)
            return
        L_0x0087:
            r0 = 0
            goto L_0x0036
        L_0x0089:
            if (r3 <= 0) goto L_0x00a1
            r0 = r1
            goto L_0x004e
        L_0x008d:
            com.fossil.rg0$d r0 = r6.k
            if (r0 == 0) goto L_0x007d
            android.view.ViewParent r1 = r0.getParent()
            com.fossil.jg0 r0 = r6.i
            if (r1 != r0) goto L_0x007d
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            com.fossil.rg0$d r1 = r6.k
            r0.removeView(r1)
            goto L_0x007d
        L_0x00a1:
            r0 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rg0.c(boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean d() {
        int i;
        ArrayList<eg0> arrayList;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z2;
        int i6;
        int i7;
        int i8;
        int i9;
        cg0 cg0 = this.d;
        if (cg0 != null) {
            ArrayList<eg0> G2 = cg0.G();
            i = G2.size();
            arrayList = G2;
        } else {
            i = 0;
            arrayList = null;
        }
        int i10 = this.w;
        int i11 = this.v;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.i;
        boolean z3 = false;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        while (i14 < i) {
            eg0 eg0 = arrayList.get(i14);
            if (eg0.o()) {
                i12++;
            } else if (eg0.n()) {
                i13++;
            } else {
                z3 = true;
            }
            i14++;
            i10 = (!this.A || !eg0.isActionViewExpanded()) ? i10 : 0;
        }
        if (this.s && (z3 || i13 + i12 > i10)) {
            i10--;
        }
        int i15 = i10 - i12;
        SparseBooleanArray sparseBooleanArray = this.C;
        sparseBooleanArray.clear();
        if (this.y) {
            int i16 = this.B;
            i2 = i11 / i16;
            i3 = i16 + ((i11 % i16) / i2);
        } else {
            i2 = 0;
            i3 = 0;
        }
        int i17 = 0;
        int i18 = 0;
        while (i18 < i) {
            eg0 eg02 = arrayList.get(i18);
            if (eg02.o()) {
                View q = q(eg02, null, viewGroup);
                if (this.y) {
                    i2 -= ActionMenuView.o(q, i3, i2, makeMeasureSpec, 0);
                } else {
                    q.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i4 = q.getMeasuredWidth();
                int i19 = i11 - i4;
                if (i17 != 0) {
                    i4 = i17;
                }
                int groupId = eg02.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                eg02.u(true);
                i5 = i19;
                i9 = i2;
            } else if (eg02.n()) {
                int groupId2 = eg02.getGroupId();
                boolean z4 = sparseBooleanArray.get(groupId2);
                boolean z5 = (i15 > 0 || z4) && i11 > 0 && (!this.y || i2 > 0);
                if (z5) {
                    View q2 = q(eg02, null, viewGroup);
                    if (this.y) {
                        int o = ActionMenuView.o(q2, i3, i2, makeMeasureSpec, 0);
                        i6 = i2 - o;
                        if (o == 0) {
                            z5 = false;
                        }
                    } else {
                        q2.measure(makeMeasureSpec, makeMeasureSpec);
                        i6 = i2;
                    }
                    int measuredWidth = q2.getMeasuredWidth();
                    i11 -= measuredWidth;
                    if (i17 == 0) {
                        i17 = measuredWidth;
                    }
                    z2 = z5 & (!this.y ? i11 + i17 > 0 : i11 >= 0);
                    i7 = i17;
                } else {
                    z2 = z5;
                    i6 = i2;
                    i7 = i17;
                }
                if (z2 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i8 = i15;
                } else if (z4) {
                    sparseBooleanArray.put(groupId2, false);
                    int i20 = i15;
                    for (int i21 = 0; i21 < i18; i21++) {
                        eg0 eg03 = arrayList.get(i21);
                        if (eg03.getGroupId() == groupId2) {
                            if (eg03.l()) {
                                i20++;
                            }
                            eg03.u(false);
                        }
                    }
                    i8 = i20;
                } else {
                    i8 = i15;
                }
                if (z2) {
                    i8--;
                }
                eg02.u(z2);
                i5 = i11;
                i9 = i6;
                i15 = i8;
                i4 = i7;
            } else {
                eg02.u(false);
                i4 = i17;
                i5 = i11;
                i18++;
                i17 = i4;
                i11 = i5;
            }
            i2 = i9;
            i18++;
            i17 = i4;
            i11 = i5;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.xf0, com.fossil.ig0
    public void h(Context context, cg0 cg0) {
        super.h(context, cg0);
        Resources resources = context.getResources();
        of0 b2 = of0.b(context);
        if (!this.t) {
            this.s = b2.h();
        }
        if (!this.z) {
            this.u = b2.c();
        }
        if (!this.x) {
            this.w = b2.d();
        }
        int i = this.u;
        if (this.s) {
            if (this.k == null) {
                d dVar = new d(this.b);
                this.k = dVar;
                if (this.m) {
                    dVar.setImageDrawable(this.l);
                    this.l = null;
                    this.m = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.k.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.k.getMeasuredWidth();
        } else {
            this.k = null;
        }
        this.v = i;
        this.B = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void i(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof g) && (i = ((g) parcelable).b) > 0 && (findItem = this.d.findItem(i)) != null) {
            k((ng0) findItem.getSubMenu());
        }
    }

    @DexIgnore
    @Override // com.fossil.xf0, com.fossil.ig0
    public boolean k(ng0 ng0) {
        boolean z2;
        if (!ng0.hasVisibleItems()) {
            return false;
        }
        ng0 ng02 = ng0;
        while (ng02.i0() != this.d) {
            ng02 = (ng0) ng02.i0();
        }
        View C2 = C(ng02.getItem());
        if (C2 == null) {
            return false;
        }
        this.I = ng0.getItem().getItemId();
        int size = ng0.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                z2 = false;
                break;
            }
            MenuItem item = ng0.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        a aVar = new a(this.c, ng0, C2);
        this.E = aVar;
        aVar.g(z2);
        this.E.k();
        super.k(ng0);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public Parcelable l() {
        g gVar = new g();
        gVar.b = this.I;
        return gVar;
    }

    @DexIgnore
    @Override // com.fossil.xf0
    public void m(eg0 eg0, jg0.a aVar) {
        aVar.f(eg0, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.i);
        if (this.G == null) {
            this.G = new b();
        }
        actionMenuItemView.setPopupCallback(this.G);
    }

    @DexIgnore
    @Override // com.fossil.xf0
    public boolean o(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.k) {
            return false;
        }
        return super.o(viewGroup, i);
    }

    @DexIgnore
    @Override // com.fossil.xf0
    public View q(eg0 eg0, View view, ViewGroup viewGroup) {
        View actionView = eg0.getActionView();
        if (actionView == null || eg0.j()) {
            actionView = super.q(eg0, view, viewGroup);
        }
        actionView.setVisibility(eg0.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    @Override // com.fossil.xf0
    public jg0 r(ViewGroup viewGroup) {
        jg0 jg0 = this.i;
        jg0 r = super.r(viewGroup);
        if (jg0 != r) {
            ((ActionMenuView) r).setPresenter(this);
        }
        return r;
    }

    @DexIgnore
    @Override // com.fossil.xf0
    public boolean t(int i, eg0 eg0) {
        return eg0.l();
    }
}
