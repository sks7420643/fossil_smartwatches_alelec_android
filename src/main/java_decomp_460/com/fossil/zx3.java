package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zx3<S> extends Parcelable {
    @DexIgnore
    View G(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, wx3 wx3, ky3<S> ky3);

    @DexIgnore
    int Q(Context context);

    @DexIgnore
    boolean T();

    @DexIgnore
    Collection<Long> X();

    @DexIgnore
    S b0();

    @DexIgnore
    void i0(long j);

    @DexIgnore
    String j(Context context);

    @DexIgnore
    Collection<ln0<Long, Long>> o();
}
