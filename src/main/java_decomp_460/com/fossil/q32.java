package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q32 implements Factory<p32> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Context> f2920a;
    @DexIgnore
    public /* final */ Provider<String> b;
    @DexIgnore
    public /* final */ Provider<Integer> c;

    @DexIgnore
    public q32(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        this.f2920a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static q32 a(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        return new q32(provider, provider2, provider3);
    }

    @DexIgnore
    /* renamed from: b */
    public p32 get() {
        return new p32(this.f2920a.get(), this.b.get(), this.c.get().intValue());
    }
}
