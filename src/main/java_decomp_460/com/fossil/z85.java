package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ LinearLayout B;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ RecyclerView D;
    @DexIgnore
    public /* final */ View E;
    @DexIgnore
    public /* final */ View F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    public z85(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, ImageView imageView3, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView4, ImageView imageView5, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ConstraintLayout constraintLayout2, RecyclerView recyclerView, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = imageView2;
        this.u = imageView3;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = imageView4;
        this.y = imageView5;
        this.z = linearLayout;
        this.A = linearLayout2;
        this.B = linearLayout3;
        this.C = constraintLayout2;
        this.D = recyclerView;
        this.E = view2;
        this.F = view3;
        this.G = view4;
    }
}
