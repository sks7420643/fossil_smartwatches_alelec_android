package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve6 extends pv5 implements ue6, zw5, aw5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<l55> g;
    @DexIgnore
    public te6 h;
    @DexIgnore
    public uw5 i;
    @DexIgnore
    public ActivityOverviewFragment j;
    @DexIgnore
    public f67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ve6.m;
        }

        @DexIgnore
        public final ve6 b() {
            return new ve6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends f67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ ve6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, ve6 ve6, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = ve6;
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void b(int i) {
            ve6.K6(this.f).p();
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void c(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = ve6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "DashboardActivityFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ te6 K6(ve6 ve6) {
        te6 te6 = ve6.h;
        if (te6 != null) {
            return te6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return m;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ue6
    public void I1(ai5 ai5) {
        RecyclerView recyclerView;
        pq7.c(ai5, "distanceUnit");
        l55 M6 = M6();
        RecyclerView.m layoutManager = (M6 == null || (recyclerView = M6.q) == null) ? null : recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            uw5 uw5 = this.i;
            if (uw5 != null) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                uw5.y(ai5, linearLayoutManager.a2(), linearLayoutManager.d2());
                return;
            }
            pq7.n("mDashboardActivitiesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final l55 M6() {
        g37<l55> g37 = this.g;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(te6 te6) {
        pq7.c(te6, "presenter");
        this.h = te6;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void Q(Date date) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onDayClicked - date=" + date);
        Context context = getContext();
        if (context != null) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.C;
            pq7.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        l55 M6;
        RecyclerView recyclerView;
        View view;
        if (z) {
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
            }
            if (isVisible() && this.g != null && (M6 = M6()) != null && (recyclerView = M6.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    recyclerView.smoothScrollToPosition(0);
                    f67 f67 = this.k;
                    if (f67 != null) {
                        f67.d();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.ue6
    public void d() {
        f67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<l55> g37 = new g37<>(this, (l55) aq0.f(layoutInflater, 2131558543, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            l55 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(m, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        te6 te6 = this.h;
        if (te6 != null) {
            te6.o();
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        te6 te6 = this.h;
        if (te6 != null) {
            te6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        te6 te6 = this.h;
        if (te6 != null) {
            te6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ActivityOverviewFragment activityOverviewFragment = (ActivityOverviewFragment) getChildFragmentManager().Z("ActivityOverviewFragment");
        this.j = activityOverviewFragment;
        if (activityOverviewFragment == null) {
            this.j = new ActivityOverviewFragment();
        }
        sw5 sw5 = new sw5();
        PortfolioApp c = PortfolioApp.h0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        ActivityOverviewFragment activityOverviewFragment2 = this.j;
        if (activityOverviewFragment2 != null) {
            this.i = new uw5(sw5, c, this, childFragmentManager, activityOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            l55 M6 = M6();
            if (!(M6 == null || (recyclerView2 = M6.q) == null)) {
                pq7.b(recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                uw5 uw5 = this.i;
                if (uw5 != null) {
                    recyclerView2.setAdapter(uw5);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        b bVar = new b(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.k = bVar;
                        if (bVar != null) {
                            recyclerView2.addOnScrollListener(bVar);
                            recyclerView2.setItemViewCacheSize(0);
                            bd6 bd6 = new bd6(linearLayoutManager.q2());
                            Drawable f = gl0.f(recyclerView2.getContext(), 2131230856);
                            if (f != null) {
                                pq7.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                bd6.h(f);
                                recyclerView2.addItemDecoration(bd6);
                                te6 te6 = this.h;
                                if (te6 != null) {
                                    te6.n();
                                } else {
                                    pq7.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    pq7.n("mDashboardActivitiesAdapter");
                    throw null;
                }
            }
            l55 M62 = M6();
            if (!(M62 == null || (recyclerView = M62.q) == null)) {
                pq7.b(recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof pv0) {
                    ((pv0) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            E6("steps_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ts0 a2 = vs0.e(activity).a(y67.class);
                pq7.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                y67 y67 = (y67) a2;
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ue6
    public void q(cu0<ActivitySummary> cu0) {
        uw5 uw5 = this.i;
        if (uw5 != null) {
            uw5.x(cu0);
        } else {
            pq7.n("mDashboardActivitiesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void q0(Date date, Date date2) {
        pq7.c(date, "startWeekDate");
        pq7.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
