package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fw5;
import com.fossil.jw5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub6 extends pv5 implements jc6, t47.g {
    @DexIgnore
    public g37<p85> g;
    @DexIgnore
    public ic6 h;
    @DexIgnore
    public fw5 i;
    @DexIgnore
    public jw5 j;
    @DexIgnore
    public po4 k;
    @DexIgnore
    public sb6 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements jw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ub6 f3561a;

        @DexIgnore
        public a(ub6 ub6) {
            this.f3561a = ub6;
        }

        @DexIgnore
        @Override // com.fossil.jw5.b
        public void a(MicroApp microApp) {
            pq7.c(microApp, "microApp");
            ub6.K6(this.f3561a).q(microApp.getId());
        }

        @DexIgnore
        @Override // com.fossil.jw5.b
        public void b(MicroApp microApp) {
            pq7.c(microApp, "microApp");
            jn5.c(jn5.b, this.f3561a.getContext(), hl5.f1493a.d(microApp.getId()), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ub6 f3562a;

        @DexIgnore
        public b(ub6 ub6) {
            this.f3562a = ub6;
        }

        @DexIgnore
        @Override // com.fossil.fw5.b
        public void a(Category category) {
            pq7.c(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppFragment", "onItemClicked category=" + category);
            ub6.K6(this.f3562a).p(category);
        }

        @DexIgnore
        @Override // com.fossil.fw5.b
        public void b() {
            ub6.K6(this.f3562a).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ub6 b;

        @DexIgnore
        public c(ub6 ub6) {
            this.b = ub6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ub6.K6(this.b).o();
        }
    }

    @DexIgnore
    public static final /* synthetic */ ic6 K6(ub6 ub6) {
        ic6 ic6 = ub6.h;
        if (ic6 != null) {
            return ic6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void A1(MicroApp microApp) {
        if (microApp != null) {
            jw5 jw5 = this.j;
            if (jw5 != null) {
                jw5.q(microApp.getId());
                N6(microApp);
                return;
            }
            pq7.n("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "MicroAppFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final void L6() {
        if (isActive()) {
            jw5 jw5 = this.j;
            if (jw5 != null) {
                jw5.j();
            } else {
                pq7.n("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(ic6 ic6) {
        pq7.c(ic6, "presenter");
        this.h = ic6;
    }

    @DexIgnore
    public final void N6(MicroApp microApp) {
        g37<p85> g37 = this.g;
        if (g37 != null) {
            p85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.y;
                pq7.b(flexibleTextView, "binding.tvSelectedMicroApp");
                flexibleTextView.setText(um5.d(PortfolioApp.h0.c(), microApp.getNameKey(), microApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.u;
                pq7.b(flexibleTextView2, "binding.tvMicroAppDetail");
                flexibleTextView2.setText(um5.d(PortfolioApp.h0.c(), microApp.getDescriptionKey(), microApp.getDescription()));
                jw5 jw5 = this.j;
                if (jw5 != null) {
                    int k2 = jw5.k(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + k2);
                    if (k2 >= 0) {
                        a2.t.scrollToPosition(k2);
                        return;
                    }
                    return;
                }
                pq7.n("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        pq7.c(str, "tag");
        if (pq7.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363373 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    ((ls5) activity3).x();
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (pq7.a(str, s37.c.f())) {
            if (i2 == 2131363373 && (activity2 = getActivity()) != null) {
                if (!v78.d(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    g47.f1261a.t(this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    ((ls5) activity2).x();
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (pq7.a(str, "LOCATION_PERMISSION_TAG")) {
            if (i2 == 2131363373 && (activity = getActivity()) != null) {
                if (activity != null) {
                    ((ls5) activity).x();
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (pq7.a(str, InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363373) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void S(List<Category> list) {
        pq7.c(list, "categories");
        fw5 fw5 = this.i;
        if (fw5 != null) {
            fw5.n(list);
        } else {
            pq7.n("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void U5(List<MicroApp> list) {
        pq7.c(list, "microApps");
        jw5 jw5 = this.j;
        if (jw5 != null) {
            jw5.o(list);
        } else {
            pq7.n("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void a0(String str, String str2, String str3) {
        pq7.c(str, "topMicroApp");
        pq7.c(str2, "middleMicroApp");
        pq7.c(str3, "bottomMicroApp");
        SearchMicroAppActivity.B.a(this, str, str2, str3);
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void a5(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void d1(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void f0(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void g0(boolean z, String str, String str2, String str3) {
        pq7.c(str, "microAppId");
        pq7.c(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        g37<p85> g37 = this.g;
        if (g37 != null) {
            p85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                pq7.b(flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView2 = a2.w;
                    pq7.b(flexibleTextView2, "it.tvMicroAppSetting");
                    flexibleTextView2.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView3 = a2.w;
                        pq7.b(flexibleTextView3, "it.tvMicroAppSetting");
                        flexibleTextView3.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView4 = a2.w;
                    pq7.b(flexibleTextView4, "it.tvMicroAppSetting");
                    flexibleTextView4.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView5 = a2.w;
                pq7.b(flexibleTextView5, "it.tvMicroAppSetting");
                flexibleTextView5.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void g3(String str) {
        pq7.c(str, "content");
        g37<p85> g37 = this.g;
        if (g37 != null) {
            p85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.tvMicroAppDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            po4 po4 = this.k;
            if (po4 != null) {
                ts0 a2 = vs0.f(hybridCustomizeEditActivity, po4).a(sb6.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                sb6 sb6 = (sb6) a2;
                this.l = sb6;
                ic6 ic6 = this.h;
                if (ic6 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (sb6 != null) {
                    ic6.r(sb6);
                } else {
                    pq7.n("mShareViewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        Ringtone ringtone;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "onActivityResult requestCode " + i2 + " resultCode " + i3);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 != 104) {
                    if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                        ic6 ic6 = this.h;
                        if (ic6 != null) {
                            ic6.s(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), jj5.a(commuteTimeSetting));
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    }
                } else if (intent != null && i3 == -1 && (ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE")) != null) {
                    ic6 ic62 = this.h;
                    if (ic62 != null) {
                        ic62.s(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), jj5.a(ringtone));
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    ic6 ic63 = this.h;
                    if (ic63 != null) {
                        pq7.b(stringExtra, "selectedMicroAppId");
                        ic63.q(stringExtra);
                        return;
                    }
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            ic6 ic64 = this.h;
            if (ic64 != null) {
                ic64.s(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), jj5.a(secondTimezoneSetting));
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        p85 p85 = (p85) aq0.f(layoutInflater, 2131558585, viewGroup, false, A6());
        PortfolioApp.h0.c().M().g1(new kc6(this)).a(this);
        this.g = new g37<>(this, p85);
        pq7.b(p85, "binding");
        return p85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ic6 ic6 = this.h;
        if (ic6 != null) {
            ic6.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ic6 ic6 = this.h;
        if (ic6 != null) {
            ic6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        jw5 jw5 = new jw5(null, null, 3, null);
        jw5.p(new a(this));
        this.j = jw5;
        fw5 fw5 = new fw5(null, null, 3, null);
        fw5.o(new b(this));
        this.i = fw5;
        g37<p85> g37 = this.g;
        if (g37 != null) {
            p85 a2 = g37.a();
            if (a2 != null) {
                String d = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    a2.r.setBackgroundColor(Color.parseColor(d));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                fw5 fw52 = this.i;
                if (fw52 != null) {
                    recyclerView.setAdapter(fw52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    jw5 jw52 = this.j;
                    if (jw52 != null) {
                        recyclerView2.setAdapter(jw52);
                        a2.w.setOnClickListener(new c(this));
                        return;
                    }
                    pq7.n("mMicroAppAdapter");
                    throw null;
                }
                pq7.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jc6
    public void u0(String str) {
        pq7.c(str, "category");
        g37<p85> g37 = this.g;
        if (g37 != null) {
            p85 a2 = g37.a();
            if (a2 != null) {
                fw5 fw5 = this.i;
                if (fw5 != null) {
                    int j2 = fw5.j(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "scrollToCategory category=" + str + " scrollTo " + j2);
                    if (j2 >= 0) {
                        fw5 fw52 = this.i;
                        if (fw52 != null) {
                            fw52.p(j2);
                            a2.s.smoothScrollToPosition(j2);
                            return;
                        }
                        pq7.n("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                pq7.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
