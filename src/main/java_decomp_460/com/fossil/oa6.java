package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa6 implements Factory<na6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f2662a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public oa6(Provider<on5> provider, Provider<UserRepository> provider2) {
        this.f2662a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static oa6 a(Provider<on5> provider, Provider<UserRepository> provider2) {
        return new oa6(provider, provider2);
    }

    @DexIgnore
    public static na6 c(on5 on5, UserRepository userRepository) {
        return new na6(on5, userRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public na6 get() {
        return c(this.f2662a.get(), this.b.get());
    }
}
