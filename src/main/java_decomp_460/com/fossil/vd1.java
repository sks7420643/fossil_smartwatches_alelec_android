package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vd1 implements nd1<int[]> {
    @DexIgnore
    @Override // com.fossil.nd1
    public int a() {
        return 4;
    }

    @DexIgnore
    /* renamed from: c */
    public int b(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    /* renamed from: d */
    public int[] newArray(int i) {
        return new int[i];
    }

    @DexIgnore
    @Override // com.fossil.nd1
    public String getTag() {
        return "IntegerArrayPool";
    }
}
