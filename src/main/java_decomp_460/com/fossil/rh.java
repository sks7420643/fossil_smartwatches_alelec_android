package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rh extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ fh b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rh(fh fhVar) {
        super(1);
        this.b = fhVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        lp1 Q = ((tu) fsVar).Q();
        WorkoutSessionManager workoutSessionManager = this.b.x.f1587a.t;
        if (workoutSessionManager == null || workoutSessionManager.getCurrentSessionId() != Q.getSessionId()) {
            i60 i60 = this.b.x;
            i60.f1587a.u0(WorkoutSessionManager.initialize(i60.a().getSerialNumber(), Q.getSessionId(), Q.getWorkoutState(), Q.getDurationInSecond()));
        }
        return tl7.f3441a;
    }
}
