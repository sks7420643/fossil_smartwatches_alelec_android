package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj6 extends pv5 implements fj6 {
    @DexIgnore
    public g37<h75> g;
    @DexIgnore
    public ej6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* renamed from: K6 */
    public void M5(ej6 ej6) {
        pq7.c(ej6, "presenter");
        this.h = ej6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        g37<h75> g37 = new g37<>(this, (h75) aq0.f(layoutInflater, 2131558567, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            h75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        ej6 ej6 = this.h;
        if (ej6 != null) {
            ej6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        ej6 ej6 = this.h;
        if (ej6 != null) {
            ej6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fj6
    public void q5(List<Integer> list, List<String> list2) {
        WeekHeartRateChart weekHeartRateChart;
        pq7.c(list, "data");
        pq7.c(list2, "listWeekDays");
        g37<h75> g37 = this.g;
        if (g37 != null) {
            h75 a2 = g37.a();
            if (a2 != null && (weekHeartRateChart = a2.q) != null) {
                weekHeartRateChart.setListWeekDays(list2);
                weekHeartRateChart.m(list);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
