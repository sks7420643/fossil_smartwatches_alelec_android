package com.fossil;

import android.os.Process;
import com.fossil.a91;
import com.fossil.m91;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b91 extends Thread {
    @DexIgnore
    public static /* final */ boolean h; // = u91.b;
    @DexIgnore
    public /* final */ BlockingQueue<m91<?>> b;
    @DexIgnore
    public /* final */ BlockingQueue<m91<?>> c;
    @DexIgnore
    public /* final */ a91 d;
    @DexIgnore
    public /* final */ p91 e;
    @DexIgnore
    public volatile boolean f; // = false;
    @DexIgnore
    public /* final */ b g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ m91 b;

        @DexIgnore
        public a(m91 m91) {
            this.b = m91;
        }

        @DexIgnore
        public void run() {
            try {
                b91.this.c.put(this.b);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements m91.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Map<String, List<m91<?>>> f403a; // = new HashMap();
        @DexIgnore
        public /* final */ b91 b;

        @DexIgnore
        public b(b91 b91) {
            this.b = b91;
        }

        @DexIgnore
        @Override // com.fossil.m91.b
        public void a(m91<?> m91) {
            synchronized (this) {
                String cacheKey = m91.getCacheKey();
                List<m91<?>> remove = this.f403a.remove(cacheKey);
                if (remove != null && !remove.isEmpty()) {
                    if (u91.b) {
                        u91.e("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), cacheKey);
                    }
                    m91<?> remove2 = remove.remove(0);
                    this.f403a.put(cacheKey, remove);
                    remove2.setNetworkRequestCompleteListener(this);
                    try {
                        this.b.c.put(remove2);
                    } catch (InterruptedException e) {
                        u91.c("Couldn't add request to queue. %s", e.toString());
                        Thread.currentThread().interrupt();
                        this.b.e();
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.m91.b
        public void b(m91<?> m91, o91<?> o91) {
            List<m91<?>> remove;
            a91.a aVar = o91.b;
            if (aVar == null || aVar.a()) {
                a(m91);
                return;
            }
            String cacheKey = m91.getCacheKey();
            synchronized (this) {
                remove = this.f403a.remove(cacheKey);
            }
            if (remove != null) {
                if (u91.b) {
                    u91.e("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                }
                for (m91<?> m912 : remove) {
                    this.b.e.a(m912, o91);
                }
            }
        }

        @DexIgnore
        public final boolean d(m91<?> m91) {
            synchronized (this) {
                String cacheKey = m91.getCacheKey();
                if (this.f403a.containsKey(cacheKey)) {
                    List<m91<?>> list = this.f403a.get(cacheKey);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    m91.addMarker("waiting-for-response");
                    list.add(m91);
                    this.f403a.put(cacheKey, list);
                    if (u91.b) {
                        u91.b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                    }
                    return true;
                }
                this.f403a.put(cacheKey, null);
                m91.setNetworkRequestCompleteListener(this);
                if (u91.b) {
                    u91.b("new request, sending to network %s", cacheKey);
                }
                return false;
            }
        }
    }

    @DexIgnore
    public b91(BlockingQueue<m91<?>> blockingQueue, BlockingQueue<m91<?>> blockingQueue2, a91 a91, p91 p91) {
        this.b = blockingQueue;
        this.c = blockingQueue2;
        this.d = a91;
        this.e = p91;
        this.g = new b(this);
    }

    @DexIgnore
    public final void c() throws InterruptedException {
        d(this.b.take());
    }

    @DexIgnore
    public void d(m91<?> m91) throws InterruptedException {
        m91.addMarker("cache-queue-take");
        if (m91.isCanceled()) {
            m91.finish("cache-discard-canceled");
            return;
        }
        a91.a b2 = this.d.b(m91.getCacheKey());
        if (b2 == null) {
            m91.addMarker("cache-miss");
            if (!this.g.d(m91)) {
                this.c.put(m91);
            }
        } else if (b2.a()) {
            m91.addMarker("cache-hit-expired");
            m91.setCacheEntry(b2);
            if (!this.g.d(m91)) {
                this.c.put(m91);
            }
        } else {
            m91.addMarker("cache-hit");
            o91<?> parseNetworkResponse = m91.parseNetworkResponse(new j91(b2.f225a, b2.g));
            m91.addMarker("cache-hit-parsed");
            if (!b2.b()) {
                this.e.a(m91, parseNetworkResponse);
                return;
            }
            m91.addMarker("cache-hit-refresh-needed");
            m91.setCacheEntry(b2);
            parseNetworkResponse.d = true;
            if (!this.g.d(m91)) {
                this.e.b(m91, parseNetworkResponse, new a(m91));
            } else {
                this.e.a(m91, parseNetworkResponse);
            }
        }
    }

    @DexIgnore
    public void e() {
        this.f = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        if (h) {
            u91.e("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.d.a();
        while (true) {
            try {
                c();
            } catch (InterruptedException e2) {
                if (this.f) {
                    Thread.currentThread().interrupt();
                    return;
                }
                u91.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }
}
