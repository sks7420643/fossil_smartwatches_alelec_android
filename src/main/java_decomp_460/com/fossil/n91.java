package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AtomicInteger f2483a;
    @DexIgnore
    public /* final */ Set<m91<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<m91<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<m91<?>> d;
    @DexIgnore
    public /* final */ a91 e;
    @DexIgnore
    public /* final */ g91 f;
    @DexIgnore
    public /* final */ p91 g;
    @DexIgnore
    public /* final */ h91[] h;
    @DexIgnore
    public b91 i;
    @DexIgnore
    public /* final */ List<a> j;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(m91<T> m91);
    }

    @DexIgnore
    public n91(a91 a91, g91 g91) {
        this(a91, g91, 4);
    }

    @DexIgnore
    public n91(a91 a91, g91 g91, int i2) {
        this(a91, g91, i2, new e91(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public n91(a91 a91, g91 g91, int i2, p91 p91) {
        this.f2483a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = a91;
        this.f = g91;
        this.h = new h91[i2];
        this.g = p91;
    }

    @DexIgnore
    public <T> m91<T> a(m91<T> m91) {
        m91.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(m91);
        }
        m91.setSequence(c());
        m91.addMarker("add-to-queue");
        if (!m91.shouldCache()) {
            this.d.add(m91);
        } else {
            this.c.add(m91);
        }
        return m91;
    }

    @DexIgnore
    public <T> void b(m91<T> m91) {
        synchronized (this.b) {
            this.b.remove(m91);
        }
        synchronized (this.j) {
            for (a aVar : this.j) {
                aVar.a(m91);
            }
        }
    }

    @DexIgnore
    public int c() {
        return this.f2483a.incrementAndGet();
    }

    @DexIgnore
    public void d() {
        e();
        b91 b91 = new b91(this.c, this.d, this.e, this.g);
        this.i = b91;
        b91.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            h91 h91 = new h91(this.d, this.f, this.e, this.g);
            this.h[i2] = h91;
            h91.start();
        }
    }

    @DexIgnore
    public void e() {
        b91 b91 = this.i;
        if (b91 != null) {
            b91.e();
        }
        h91[] h91Arr = this.h;
        for (h91 h91 : h91Arr) {
            if (h91 != null) {
                h91.e();
            }
        }
    }
}
