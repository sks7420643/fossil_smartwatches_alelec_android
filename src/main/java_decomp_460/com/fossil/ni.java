package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ni extends lp {
    @DexIgnore
    public /* final */ HashMap<dr, Object> C;

    @DexIgnore
    public ni(k5 k5Var, i60 i60, HashMap<dr, Object> hashMap) {
        super(k5Var, i60, yp.i0, null, false, 24);
        this.C = hashMap;
    }

    @DexIgnore
    public static final /* synthetic */ void G(ni niVar) {
        k5 k5Var = niVar.w;
        Long l = (Long) niVar.C.get(dr.CONNECTION_TIME_OUT);
        lp.i(niVar, new yr(k5Var, l != null ? l.longValue() : 60000), rr.b, we.b, null, new kf(niVar), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.h(this, new mk(this.w, this.x, this.z), new yf(this), new mg(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I(nr nrVar) {
        if (nrVar.c == zq.SUCCESS) {
            l(nrVar);
            return;
        }
        this.v = nrVar;
        lp.i(this, new ms(this.w), zg.b, mh.b, null, new zh(this), null, 40, null);
    }
}
