package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k73 implements h73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1869a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.ga.ga_app_id", false);

    @DexIgnore
    @Override // com.fossil.h73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.h73
    public final boolean zzb() {
        return f1869a.o().booleanValue();
    }
}
