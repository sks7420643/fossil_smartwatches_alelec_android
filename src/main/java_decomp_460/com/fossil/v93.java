package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v93 extends ss2 implements t93 {
    @DexIgnore
    public v93(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(23, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.c(d, bundle);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(24, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void generateEventId(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(22, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getAppInstanceId(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(20, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getCachedAppInstanceId(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getConditionalUserProperties(String str, String str2, u93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.b(d, u93);
        i(10, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getCurrentScreenClass(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(17, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getCurrentScreenName(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(16, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getGmpAppId(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(21, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getMaxUserProperties(String str, u93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        qt2.b(d, u93);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getTestFlag(u93 u93, int i) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        d.writeInt(i);
        i(38, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void getUserProperties(String str, String str2, boolean z, u93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.d(d, z);
        qt2.b(d, u93);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void initForTests(Map map) throws RemoteException {
        Parcel d = d();
        d.writeMap(map);
        i(37, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void initialize(rg2 rg2, xs2 xs2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        qt2.c(d, xs2);
        d.writeLong(j);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void isDataCollectionEnabled(u93 u93) throws RemoteException {
        Parcel d = d();
        qt2.b(d, u93);
        i(40, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.c(d, bundle);
        qt2.d(d, z);
        qt2.d(d, z2);
        d.writeLong(j);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void logEventAndBundle(String str, String str2, Bundle bundle, u93 u93, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.c(d, bundle);
        qt2.b(d, u93);
        d.writeLong(j);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void logHealthData(int i, String str, rg2 rg2, rg2 rg22, rg2 rg23) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        d.writeString(str);
        qt2.b(d, rg2);
        qt2.b(d, rg22);
        qt2.b(d, rg23);
        i(33, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityCreated(rg2 rg2, Bundle bundle, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        qt2.c(d, bundle);
        d.writeLong(j);
        i(27, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityDestroyed(rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeLong(j);
        i(28, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityPaused(rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeLong(j);
        i(29, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityResumed(rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeLong(j);
        i(30, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivitySaveInstanceState(rg2 rg2, u93 u93, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        qt2.b(d, u93);
        d.writeLong(j);
        i(31, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityStarted(rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeLong(j);
        i(25, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void onActivityStopped(rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeLong(j);
        i(26, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void performAction(Bundle bundle, u93 u93, long j) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        qt2.b(d, u93);
        d.writeLong(j);
        i(32, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void registerOnMeasurementEventListener(us2 us2) throws RemoteException {
        Parcel d = d();
        qt2.b(d, us2);
        i(35, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void resetAnalyticsData(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        d.writeLong(j);
        i(8, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setCurrentScreen(rg2 rg2, String str, String str2, long j) throws RemoteException {
        Parcel d = d();
        qt2.b(d, rg2);
        d.writeString(str);
        d.writeString(str2);
        d.writeLong(j);
        i(15, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setDataCollectionEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        qt2.d(d, z);
        i(39, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setDefaultEventParameters(Bundle bundle) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        i(42, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setEventInterceptor(us2 us2) throws RemoteException {
        Parcel d = d();
        qt2.b(d, us2);
        i(34, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setInstanceIdProvider(vs2 vs2) throws RemoteException {
        Parcel d = d();
        qt2.b(d, vs2);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setMeasurementEnabled(boolean z, long j) throws RemoteException {
        Parcel d = d();
        qt2.d(d, z);
        d.writeLong(j);
        i(11, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setMinimumSessionDuration(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(13, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setSessionTimeoutDuration(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(14, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setUserId(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void setUserProperty(String str, String str2, rg2 rg2, boolean z, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.b(d, rg2);
        qt2.d(d, z);
        d.writeLong(j);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.t93
    public final void unregisterOnMeasurementEventListener(us2 us2) throws RemoteException {
        Parcel d = d();
        qt2.b(d, us2);
        i(36, d);
    }
}
