package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s10 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ f20 b;
    @DexIgnore
    public /* final */ /* synthetic */ lp c;
    @DexIgnore
    public /* final */ /* synthetic */ float d;

    @DexIgnore
    public s10(f20 f20, lp lpVar, float f) {
        this.b = f20;
        this.c = lpVar;
        this.d = f;
    }

    @DexIgnore
    public final void run() {
        e60.p0(this.b.c, ky1.DEBUG, ey1.a(this.c.y), "Progress: %.4f.", Float.valueOf(this.d));
        this.b.b.y(this.d);
    }
}
