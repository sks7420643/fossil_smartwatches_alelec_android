package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji6 extends gi6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<DailyHeartRateSummary> f;
    @DexIgnore
    public /* final */ hi6 g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository h;
    @DexIgnore
    public /* final */ FitnessDataRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ no4 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter$initDataSource$1", f = "DashboardHeartRatePresenter.kt", l = {55, 61}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ji6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ji6$a$a")
        /* renamed from: com.fossil.ji6$a$a  reason: collision with other inner class name */
        public static final class C0130a implements fl5.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1765a;

            @DexIgnore
            public C0130a(a aVar) {
                this.f1765a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.fl5.a
            public final void e(fl5.g gVar) {
                pq7.c(gVar, "report");
                MFLogger.d("DashboardHeartRatePresenter", "onStatusChange status=" + gVar);
                if (gVar.b()) {
                    this.f1765a.this$0.g.d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<cu0<DailyHeartRateSummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1766a;

            @DexIgnore
            public b(a aVar) {
                this.f1766a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<DailyHeartRateSummary> cu0) {
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                MFLogger.d("DashboardHeartRatePresenter", sb.toString());
                if (cu0 != null) {
                    this.f1766a.this$0.g.r3(cu0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter$initDataSource$1$user$1", f = "DashboardHeartRatePresenter.kt", l = {55}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.j;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ji6 ji6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ji6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0067  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                r4 = 0
                r9 = 2
                r3 = 1
                java.lang.Object r8 = com.fossil.yn7.d()
                int r0 = r10.label
                if (r0 == 0) goto L_0x00a2
                if (r0 == r3) goto L_0x0059
                if (r0 != r9) goto L_0x0051
                java.lang.Object r0 = r10.L$4
                com.fossil.ji6 r0 = (com.fossil.ji6) r0
                java.lang.Object r1 = r10.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r10.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r11)
                r2 = r0
                r1 = r11
            L_0x0028:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.fossil.ji6.x(r2, r0)
                com.fossil.ji6 r0 = r10.this$0
                com.fossil.hi6 r0 = com.fossil.ji6.w(r0)
                com.fossil.ji6 r1 = r10.this$0
                com.portfolio.platform.data.Listing r1 = com.fossil.ji6.t(r1)
                if (r1 == 0) goto L_0x004e
                androidx.lifecycle.LiveData r1 = r1.getPagedList()
                if (r1 == 0) goto L_0x004e
                if (r0 == 0) goto L_0x00c1
                com.fossil.ii6 r0 = (com.fossil.ii6) r0
                com.fossil.ji6$a$b r2 = new com.fossil.ji6$a$b
                r2.<init>(r10)
                r1.h(r0, r2)
            L_0x004e:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0050:
                return r0
            L_0x0051:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0059:
                java.lang.Object r0 = r10.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r11)
                r6 = r0
                r1 = r11
            L_0x0062:
                r5 = r1
                com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
                if (r5 == 0) goto L_0x004e
                java.lang.String r0 = r5.getCreatedAt()
                if (r0 == 0) goto L_0x00c9
                java.util.Date r2 = com.fossil.lk5.q0(r0)
                com.fossil.ji6 r7 = r10.this$0
                com.portfolio.platform.data.source.HeartRateSummaryRepository r0 = com.fossil.ji6.u(r7)
                com.fossil.ji6 r1 = r10.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r1 = com.fossil.ji6.s(r1)
                java.lang.String r3 = "createdDate"
                com.fossil.pq7.b(r2, r3)
                com.fossil.ji6 r3 = r10.this$0
                com.fossil.no4 r3 = com.fossil.ji6.r(r3)
                com.fossil.ji6$a$a r4 = new com.fossil.ji6$a$a
                r4.<init>(r10)
                r10.L$0 = r6
                r10.L$1 = r5
                r10.L$2 = r5
                r10.L$3 = r2
                r10.L$4 = r7
                r10.label = r9
                r5 = r10
                java.lang.Object r1 = r0.getSummariesPaging(r1, r2, r3, r4, r5)
                if (r1 != r8) goto L_0x00be
                r0 = r8
                goto L_0x0050
            L_0x00a2:
                com.fossil.el7.b(r11)
                com.fossil.iv7 r0 = r10.p$
                com.fossil.ji6 r1 = r10.this$0
                com.fossil.dv7 r1 = com.fossil.ji6.q(r1)
                com.fossil.ji6$a$c r2 = new com.fossil.ji6$a$c
                r2.<init>(r10, r4)
                r10.L$0 = r0
                r10.label = r3
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r10)
                if (r1 != r8) goto L_0x00cd
                r0 = r8
                goto L_0x0050
            L_0x00be:
                r2 = r7
                goto L_0x0028
            L_0x00c1:
                com.fossil.il7 r0 = new com.fossil.il7
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment"
                r0.<init>(r1)
                throw r0
            L_0x00c9:
                com.fossil.pq7.i()
                throw r4
            L_0x00cd:
                r6 = r0
                goto L_0x0062
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ji6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public ji6(hi6 hi6, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, no4 no4) {
        pq7.c(hi6, "mView");
        pq7.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(no4, "mAppExecutors");
        this.g = hi6;
        this.h = heartRateSummaryRepository;
        this.i = fitnessDataRepository;
        this.j = userRepository;
        this.k = no4;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        Boolean p0 = lk5.p0(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardHeartRatePresenter", "start isDateTodayDate " + p0 + " listingPage " + this.f);
        if (!p0.booleanValue()) {
            this.e = new Date();
            Listing<DailyHeartRateSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardHeartRatePresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.gi6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.gi6
    public void o() {
        LiveData<cu0<DailyHeartRateSummary>> pagedList;
        try {
            hi6 hi6 = this.g;
            Listing<DailyHeartRateSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (hi6 != null) {
                    pagedList.n((ii6) hi6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("DashboardHeartRatePresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.gi6
    public void p() {
        gp7<tl7> retry;
        MFLogger.d("DashboardHeartRatePresenter", "retry all failed request");
        Listing<DailyHeartRateSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public void y() {
        this.g.M5(this);
    }
}
