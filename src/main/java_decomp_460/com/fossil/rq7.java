package com.fossil;

import com.fossil.is7;
import com.fossil.ls7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rq7 extends uq7 implements is7 {
    @DexIgnore
    public rq7() {
    }

    @DexIgnore
    public rq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public ds7 computeReflected() {
        er7.d(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get();

    @DexIgnore
    @Override // com.fossil.ls7
    public Object getDelegate() {
        return ((is7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.yq7, com.fossil.ls7, com.fossil.uq7
    public ls7.a getGetter() {
        return ((is7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.is7, com.fossil.uq7
    public is7.a getSetter() {
        return ((is7) getReflected()).getSetter();
    }

    @DexIgnore
    @Override // com.fossil.gp7
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public abstract /* synthetic */ void set(R r);
}
