package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp4 implements Factory<un5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1162a;

    @DexIgnore
    public fp4(uo4 uo4) {
        this.f1162a = uo4;
    }

    @DexIgnore
    public static fp4 a(uo4 uo4) {
        return new fp4(uo4);
    }

    @DexIgnore
    public static un5 c(uo4 uo4) {
        un5 m = uo4.m();
        lk7.c(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    /* renamed from: b */
    public un5 get() {
        return c(this.f1162a);
    }
}
