package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw7 extends CancellationException implements bv7<yw7> {
    @DexIgnore
    public /* final */ xw7 job;

    @DexIgnore
    public yw7(String str, Throwable th, xw7 xw7) {
        super(str);
        this.job = xw7;
        if (th != null) {
            initCause(th);
        }
    }

    @DexIgnore
    @Override // com.fossil.bv7
    public yw7 createCopy() {
        if (!nv7.c()) {
            return null;
        }
        String message = getMessage();
        if (message != null) {
            return new yw7(message, this, this.job);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof yw7) {
                yw7 yw7 = (yw7) obj;
                if (!pq7.a(yw7.getMessage(), getMessage()) || !pq7.a(yw7.job, this.job) || !pq7.a(yw7.getCause(), getCause())) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        return nv7.c() ? super.fillInStackTrace() : this;
    }

    @DexIgnore
    public int hashCode() {
        String message = getMessage();
        if (message != null) {
            int hashCode = message.hashCode();
            int hashCode2 = this.job.hashCode();
            Throwable cause = getCause();
            return (cause != null ? cause.hashCode() : 0) + (((hashCode * 31) + hashCode2) * 31);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "; job=" + this.job;
    }
}
