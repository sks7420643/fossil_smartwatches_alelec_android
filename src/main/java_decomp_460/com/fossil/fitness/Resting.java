package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Resting implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Resting> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ byte mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<Resting> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Resting createFromParcel(Parcel parcel) {
            return new Resting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Resting[] newArray(int i) {
            return new Resting[i];
        }
    }

    @DexIgnore
    public Resting(int i, int i2, byte b) {
        this.mTimestamp = i;
        this.mTimezoneOffsetInSecond = i2;
        this.mValue = (byte) b;
    }

    @DexIgnore
    public Resting(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mValue = parcel.readByte();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Resting)) {
            return false;
        }
        Resting resting = (Resting) obj;
        return this.mTimestamp == resting.mTimestamp && this.mTimezoneOffsetInSecond == resting.mTimezoneOffsetInSecond && this.mValue == resting.mValue;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public byte getValue() {
        return this.mValue;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.mTimestamp + 527) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mValue;
    }

    @DexIgnore
    public String toString() {
        return "Resting{mTimestamp=" + this.mTimestamp + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mValue=" + ((int) this.mValue) + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeByte(this.mValue);
    }
}
