package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class GpsDataEncoder {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends GpsDataEncoder {
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native ArrayList<GpsDataPoint> decode(byte[] bArr);

        @DexIgnore
        public static native byte[] encode(ArrayList<GpsDataPoint> arrayList);

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }
    }

    @DexIgnore
    public static ArrayList<GpsDataPoint> decode(byte[] bArr) {
        return CppProxy.decode(bArr);
    }

    @DexIgnore
    public static byte[] encode(ArrayList<GpsDataPoint> arrayList) {
        return CppProxy.encode(arrayList);
    }
}
