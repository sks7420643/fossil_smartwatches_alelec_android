package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BinaryFile implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<BinaryFile> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ byte[] mData;
    @DexIgnore
    public /* final */ int mSynctime;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<BinaryFile> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BinaryFile createFromParcel(Parcel parcel) {
            return new BinaryFile(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BinaryFile[] newArray(int i) {
            return new BinaryFile[i];
        }
    }

    @DexIgnore
    public BinaryFile(Parcel parcel) {
        this.mData = parcel.createByteArray();
        this.mSynctime = parcel.readInt();
    }

    @DexIgnore
    public BinaryFile(byte[] bArr, int i) {
        this.mData = bArr;
        this.mSynctime = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof BinaryFile)) {
            return false;
        }
        BinaryFile binaryFile = (BinaryFile) obj;
        return Arrays.equals(this.mData, binaryFile.mData) && this.mSynctime == binaryFile.mSynctime;
    }

    @DexIgnore
    public byte[] getData() {
        return this.mData;
    }

    @DexIgnore
    public int getSynctime() {
        return this.mSynctime;
    }

    @DexIgnore
    public int hashCode() {
        return ((Arrays.hashCode(this.mData) + 527) * 31) + this.mSynctime;
    }

    @DexIgnore
    public String toString() {
        return "BinaryFile{mData=" + this.mData + ",mSynctime=" + this.mSynctime + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.mData);
        parcel.writeInt(this.mSynctime);
    }
}
