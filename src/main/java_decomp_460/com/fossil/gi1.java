package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.util.Log;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class gi1 extends Fragment {
    @DexIgnore
    public /* final */ wh1 b;
    @DexIgnore
    public /* final */ ii1 c;
    @DexIgnore
    public /* final */ Set<gi1> d;
    @DexIgnore
    public wa1 e;
    @DexIgnore
    public gi1 f;
    @DexIgnore
    public Fragment g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ii1 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ii1
        public Set<wa1> a() {
            Set<gi1> b = gi1.this.b();
            HashSet hashSet = new HashSet(b.size());
            for (gi1 gi1 : b) {
                if (gi1.e() != null) {
                    hashSet.add(gi1.e());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + gi1.this + "}";
        }
    }

    @DexIgnore
    public gi1() {
        this(new wh1());
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public gi1(wh1 wh1) {
        this.c = new a();
        this.d = new HashSet();
        this.b = wh1;
    }

    @DexIgnore
    public final void a(gi1 gi1) {
        this.d.add(gi1);
    }

    @DexIgnore
    @TargetApi(17)
    public Set<gi1> b() {
        if (equals(this.f)) {
            return Collections.unmodifiableSet(this.d);
        }
        if (this.f == null || Build.VERSION.SDK_INT < 17) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet();
        for (gi1 gi1 : this.f.b()) {
            if (g(gi1.getParentFragment())) {
                hashSet.add(gi1);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public wh1 c() {
        return this.b;
    }

    @DexIgnore
    @TargetApi(17)
    public final Fragment d() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.g;
    }

    @DexIgnore
    public wa1 e() {
        return this.e;
    }

    @DexIgnore
    public ii1 f() {
        return this.c;
    }

    @DexIgnore
    @TargetApi(17)
    public final boolean g(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void h(Activity activity) {
        l();
        gi1 p = oa1.c(activity).k().p(activity);
        this.f = p;
        if (!equals(p)) {
            this.f.a(this);
        }
    }

    @DexIgnore
    public final void i(gi1 gi1) {
        this.d.remove(gi1);
    }

    @DexIgnore
    public void j(Fragment fragment) {
        this.g = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            h(fragment.getActivity());
        }
    }

    @DexIgnore
    public void k(wa1 wa1) {
        this.e = wa1;
    }

    @DexIgnore
    public final void l() {
        gi1 gi1 = this.f;
        if (gi1 != null) {
            gi1.i(this);
            this.f = null;
        }
    }

    @DexIgnore
    @Override // android.app.Fragment
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            h(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.b.c();
        l();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        l();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.b.d();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.b.e();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{parent=" + d() + "}";
    }
}
