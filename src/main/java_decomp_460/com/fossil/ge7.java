package com.fossil;

import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ge7 extends ee7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ be7 f1297a;
    @DexIgnore
    public /* final */ MethodChannel.Result b;
    @DexIgnore
    public /* final */ Boolean c;

    @DexIgnore
    public ge7(MethodChannel.Result result, be7 be7, Boolean bool) {
        this.b = result;
        this.f1297a = be7;
        this.c = bool;
    }

    @DexIgnore
    @Override // com.fossil.ie7
    public <T> T a(String str) {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ee7, com.fossil.ie7
    public be7 b() {
        return this.f1297a;
    }

    @DexIgnore
    @Override // com.fossil.ee7, com.fossil.ie7
    public Boolean d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.je7
    public void error(String str, String str2, Object obj) {
        this.b.error(str, str2, obj);
    }

    @DexIgnore
    @Override // com.fossil.je7
    public void success(Object obj) {
        this.b.success(obj);
    }
}
