package com.fossil;

import com.fossil.qb3;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg3 extends oc3 {
    @DexIgnore
    public /* final */ /* synthetic */ qb3.i b;

    @DexIgnore
    public cg3(qb3 qb3, qb3.i iVar) {
        this.b = iVar;
    }

    @DexIgnore
    @Override // com.fossil.nc3
    public final void onMapLongClick(LatLng latLng) {
        this.b.onMapLongClick(latLng);
    }
}
