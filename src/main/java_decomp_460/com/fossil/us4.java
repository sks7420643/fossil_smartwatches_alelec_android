package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us4 {
    @DexIgnore
    public static final boolean a(ts4 ts4) {
        pq7.c(ts4, "$this$validate");
        if (pq7.a(ts4.k(), "activity_best_result")) {
            if ((ts4.e().length() > 0) && ts4.e().length() <= 32 && ts4.b() >= 3600 && ts4.b() <= 259200) {
                return true;
            }
        }
        if (pq7.a(ts4.k(), "activity_reach_goal")) {
            if ((ts4.e().length() > 0) && ts4.e().length() <= 32 && ts4.i() >= 1000 && ts4.i() <= 300000) {
                return true;
            }
        }
        return false;
    }
}
