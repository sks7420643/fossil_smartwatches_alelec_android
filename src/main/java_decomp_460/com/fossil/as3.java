package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class as3 implements Parcelable.Creator<xr3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xr3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        vg3 vg3 = null;
        vg3 vg32 = null;
        vg3 vg33 = null;
        String str = null;
        fr3 fr3 = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    str3 = ad2.f(parcel, t);
                    break;
                case 3:
                    str2 = ad2.f(parcel, t);
                    break;
                case 4:
                    fr3 = (fr3) ad2.e(parcel, t, fr3.CREATOR);
                    break;
                case 5:
                    j3 = ad2.y(parcel, t);
                    break;
                case 6:
                    z = ad2.m(parcel, t);
                    break;
                case 7:
                    str = ad2.f(parcel, t);
                    break;
                case 8:
                    vg33 = (vg3) ad2.e(parcel, t, vg3.CREATOR);
                    break;
                case 9:
                    j2 = ad2.y(parcel, t);
                    break;
                case 10:
                    vg32 = (vg3) ad2.e(parcel, t, vg3.CREATOR);
                    break;
                case 11:
                    j = ad2.y(parcel, t);
                    break;
                case 12:
                    vg3 = (vg3) ad2.e(parcel, t, vg3.CREATOR);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new xr3(str3, str2, fr3, j3, z, str, vg33, j2, vg32, j, vg3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xr3[] newArray(int i) {
        return new xr3[i];
    }
}
