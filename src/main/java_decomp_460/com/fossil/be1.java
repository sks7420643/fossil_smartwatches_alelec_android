package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface be1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        be1 build();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(File file);
    }

    @DexIgnore
    void a(mb1 mb1, b bVar);

    @DexIgnore
    File b(mb1 mb1);
}
