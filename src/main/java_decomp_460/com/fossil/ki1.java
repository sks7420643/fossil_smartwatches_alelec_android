package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ki1 extends Fragment {
    @DexIgnore
    public /* final */ wh1 b;
    @DexIgnore
    public /* final */ ii1 c;
    @DexIgnore
    public /* final */ Set<ki1> d;
    @DexIgnore
    public ki1 e;
    @DexIgnore
    public wa1 f;
    @DexIgnore
    public Fragment g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ii1 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ii1
        public Set<wa1> a() {
            Set<ki1> w6 = ki1.this.w6();
            HashSet hashSet = new HashSet(w6.size());
            for (ki1 ki1 : w6) {
                if (ki1.z6() != null) {
                    hashSet.add(ki1.z6());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + ki1.this + "}";
        }
    }

    @DexIgnore
    public ki1() {
        this(new wh1());
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public ki1(wh1 wh1) {
        this.c = new a();
        this.d = new HashSet();
        this.b = wh1;
    }

    @DexIgnore
    public static FragmentManager B6(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    @DexIgnore
    public ii1 A6() {
        return this.c;
    }

    @DexIgnore
    public final boolean C6(Fragment fragment) {
        Fragment y6 = y6();
        while (true) {
            Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(y6)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void D6(Context context, FragmentManager fragmentManager) {
        H6();
        ki1 r = oa1.c(context).k().r(context, fragmentManager);
        this.e = r;
        if (!equals(r)) {
            this.e.v6(this);
        }
    }

    @DexIgnore
    public final void E6(ki1 ki1) {
        this.d.remove(ki1);
    }

    @DexIgnore
    public void F6(Fragment fragment) {
        FragmentManager B6;
        this.g = fragment;
        if (fragment != null && fragment.getContext() != null && (B6 = B6(fragment)) != null) {
            D6(fragment.getContext(), B6);
        }
    }

    @DexIgnore
    public void G6(wa1 wa1) {
        this.f = wa1;
    }

    @DexIgnore
    public final void H6() {
        ki1 ki1 = this.e;
        if (ki1 != null) {
            ki1.E6(this);
            this.e = null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentManager B6 = B6(this);
        if (B6 != null) {
            try {
                D6(getContext(), B6);
            } catch (IllegalStateException e2) {
                if (Log.isLoggable("SupportRMFragment", 5)) {
                    Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
                }
            }
        } else if (Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.b.c();
        H6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.g = null;
        H6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        this.b.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        this.b.e();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public String toString() {
        return super.toString() + "{parent=" + y6() + "}";
    }

    @DexIgnore
    public final void v6(ki1 ki1) {
        this.d.add(ki1);
    }

    @DexIgnore
    public Set<ki1> w6() {
        ki1 ki1 = this.e;
        if (ki1 == null) {
            return Collections.emptySet();
        }
        if (equals(ki1)) {
            return Collections.unmodifiableSet(this.d);
        }
        HashSet hashSet = new HashSet();
        for (ki1 ki12 : this.e.w6()) {
            if (C6(ki12.y6())) {
                hashSet.add(ki12);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public wh1 x6() {
        return this.b;
    }

    @DexIgnore
    public final Fragment y6() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.g;
    }

    @DexIgnore
    public wa1 z6() {
        return this.f;
    }
}
