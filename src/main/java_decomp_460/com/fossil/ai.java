package com.fossil;

import com.fossil.ix1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ oi b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(oi oiVar) {
        super(1);
        this.b = oiVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        oi oiVar = this.b;
        lv lvVar = (lv) fsVar;
        long j = lvVar.M;
        long j2 = lvVar.N;
        long j3 = lvVar.O;
        if (0 == j2) {
            oiVar.D = oiVar.E;
            oiVar.l(nr.a(oiVar.v, null, zq.SUCCESS, null, null, 13));
        } else if (j != oiVar.E) {
            oiVar.l(nr.a(oiVar.v, null, zq.FLOW_BROKEN, null, null, 13));
        } else {
            oiVar.F = j + j2;
            long a2 = ix1.f1688a.a(oiVar.H.f, (int) j, (int) j2, ix1.a.CRC32);
            ky1 ky1 = ky1.DEBUG;
            if (j3 == a2) {
                long j4 = oiVar.F;
                long j5 = oiVar.G;
                if (j4 == j5) {
                    oiVar.D = j4;
                    oiVar.l(nr.a(oiVar.v, null, zq.SUCCESS, null, null, 13));
                } else {
                    oiVar.F = Math.min(((j4 - oiVar.E) / ((long) 2)) + j4, j5);
                    oiVar.E = j4;
                    lp.j(oiVar, hs.p, null, 2, null);
                }
            } else {
                oiVar.F = (oiVar.F + oiVar.E) / ((long) 2);
                lp.j(oiVar, hs.p, null, 2, null);
            }
        }
        return tl7.f3441a;
    }
}
