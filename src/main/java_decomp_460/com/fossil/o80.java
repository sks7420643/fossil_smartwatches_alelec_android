package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o80<T> implements Comparator<T> {
    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return mn7.c(Long.valueOf(t2.lastModified()), Long.valueOf(t.lastModified()));
    }
}
