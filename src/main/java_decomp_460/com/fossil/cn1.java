package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn1 extends ym1 implements Parcelable {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b(null);
    @DexIgnore
    public /* final */ a c;

    @DexIgnore
    public enum a {
        CONTINUOUS((byte) 0),
        LOW_POWER((byte) 1),
        DISABLE((byte) 2);
        
        @DexIgnore
        public static /* final */ C0033a d; // = new C0033a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cn1$a$a")
        /* renamed from: com.fossil.cn1$a$a  reason: collision with other inner class name */
        public static final class C0033a {
            @DexIgnore
            public /* synthetic */ C0033a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<cn1> {
        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
        }

        @DexIgnore
        public final cn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new cn1(a.d.a(bArr[0]));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public cn1 b(Parcel parcel) {
            return new cn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cn1 createFromParcel(Parcel parcel) {
            return new cn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cn1[] newArray(int i) {
            return new cn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ cn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.c = a.valueOf(readString);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public cn1(a aVar) {
        super(zm1.HEART_RATE_MODE);
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public String c() {
        return ey1.a(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(cn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((cn1) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
    }

    @DexIgnore
    public final a getHeartRateMode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
