package com.fossil;

import com.fossil.gu0;
import com.fossil.xt0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lu0<A, B> extends gu0<B> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gu0<A> f2248a;
    @DexIgnore
    public /* final */ gi0<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gu0.b<A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gu0.b f2249a;

        @DexIgnore
        public a(gu0.b bVar) {
            this.f2249a = bVar;
        }

        @DexIgnore
        @Override // com.fossil.gu0.b
        public void a(List<A> list, int i, int i2) {
            this.f2249a.a(xt0.convert(lu0.this.b, list), i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gu0.e<A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gu0.e f2250a;

        @DexIgnore
        public b(gu0.e eVar) {
            this.f2250a = eVar;
        }

        @DexIgnore
        @Override // com.fossil.gu0.e
        public void a(List<A> list) {
            this.f2250a.a(xt0.convert(lu0.this.b, list));
        }
    }

    @DexIgnore
    public lu0(gu0<A> gu0, gi0<List<A>, List<B>> gi0) {
        this.f2248a = gu0;
        this.b = gi0;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void addInvalidatedCallback(xt0.c cVar) {
        this.f2248a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void invalidate() {
        this.f2248a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        return this.f2248a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.gu0
    public void loadInitial(gu0.d dVar, gu0.b<B> bVar) {
        this.f2248a.loadInitial(dVar, new a(bVar));
    }

    @DexIgnore
    @Override // com.fossil.gu0
    public void loadRange(gu0.g gVar, gu0.e<B> eVar) {
        this.f2248a.loadRange(gVar, new b(eVar));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void removeInvalidatedCallback(xt0.c cVar) {
        this.f2248a.removeInvalidatedCallback(cVar);
    }
}
