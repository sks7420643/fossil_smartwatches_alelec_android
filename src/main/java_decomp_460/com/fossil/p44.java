package com.fossil;

import com.fossil.b34;
import com.fossil.c34;
import com.fossil.h34;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p44<K, V> extends a34<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] f;
    @DexIgnore
    public /* final */ transient b34<K, V>[] g;
    @DexIgnore
    public /* final */ transient int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends h34.b<K> {
        @DexIgnore
        @Weak
        public /* final */ p44<K, V> map;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.p44$a$a")
        /* renamed from: com.fossil.p44$a$a  reason: collision with other inner class name */
        public static class C0189a<K> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ a34<K, ?> map;

            @DexIgnore
            public C0189a(a34<K, ?> a34) {
                this.map = a34;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.keySet();
            }
        }

        @DexIgnore
        public a(p44<K, V> p44) {
            this.map = p44;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            return this.map.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.h34.b
        public K get(int i) {
            return (K) this.map.f[i].getKey();
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.u24, com.fossil.h34
        public Object writeReplace() {
            return new C0189a(this.map);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends y24<V> {
        @DexIgnore
        @Weak
        public /* final */ p44<K, V> map;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a<V> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ a34<?, V> map;

            @DexIgnore
            public a(a34<?, V> a34) {
                this.map = a34;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.values();
            }
        }

        @DexIgnore
        public b(p44<K, V> p44) {
            this.map = p44;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) this.map.f[i].getValue();
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.u24, com.fossil.y24
        public Object writeReplace() {
            return new a(this.map);
        }
    }

    @DexIgnore
    public p44(Map.Entry<K, V>[] entryArr, b34<K, V>[] b34Arr, int i) {
        this.f = entryArr;
        this.g = b34Arr;
        this.h = i;
    }

    @DexIgnore
    public static void checkNoConflictInKeyBucket(Object obj, Map.Entry<?, ?> entry, b34<?, ?> b34) {
        while (b34 != null) {
            a34.checkNoConflict(!obj.equals(b34.getKey()), "key", entry, b34);
            b34 = b34.getNextInKeyBucket();
        }
    }

    @DexIgnore
    public static <K, V> p44<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> p44<K, V> fromEntryArray(int i, Map.Entry<K, V>[] entryArr) {
        i14.p(i, entryArr.length);
        b34[] createEntryArray = i == entryArr.length ? entryArr : b34.createEntryArray(i);
        int a2 = r24.a(i, 1.2d);
        b34[] createEntryArray2 = b34.createEntryArray(a2);
        int i2 = a2 - 1;
        for (int i3 = 0; i3 < i; i3++) {
            Map.Entry<K, V> entry = entryArr[i3];
            K key = entry.getKey();
            V value = entry.getValue();
            a24.a(key, value);
            int b2 = r24.b(key.hashCode()) & i2;
            b34 b34 = createEntryArray2[b2];
            b34 b342 = b34 == null ? (entry instanceof b34) && ((b34) entry).isReusable() ? (b34) entry : new b34(key, value) : new b34.b(key, value, b34);
            createEntryArray2[b2] = b342;
            createEntryArray[i3] = b342;
            checkNoConflictInKeyBucket(key, b342, b34);
        }
        return new p44<>(createEntryArray, createEntryArray2, i2);
    }

    @DexIgnore
    public static <V> V get(Object obj, b34<?, V>[] b34Arr, int i) {
        if (obj == null) {
            return null;
        }
        for (b34<?, V> b34 = b34Arr[r24.b(obj.hashCode()) & i]; b34 != null; b34 = b34.getNextInKeyBucket()) {
            if (obj.equals(b34.getKey())) {
                return b34.getValue();
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<Map.Entry<K, V>> createEntrySet() {
        return new c34.b(this, this.f);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<K> createKeySet() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public u24<V> createValues() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.a34, java.util.Map
    public V get(Object obj) {
        return (V) get(obj, this.g, this.h);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.f.length;
    }
}
