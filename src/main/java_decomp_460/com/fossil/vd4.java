package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vd4 {
    @DexIgnore
    vd4 d(String str) throws IOException;

    @DexIgnore
    vd4 e(boolean z) throws IOException;
}
