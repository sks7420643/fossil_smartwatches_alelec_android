package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i36 extends u47 implements h36 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<va5> l;
    @DexIgnore
    public g36 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return i36.t;
        }

        @DexIgnore
        public final i36 b() {
            return new i36();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i36 f1576a;
        @DexIgnore
        public /* final */ /* synthetic */ va5 b;

        @DexIgnore
        public b(i36 i36, va5 va5) {
            this.f1576a = i36;
            this.b = va5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            g36 A6 = i36.A6(this.f1576a);
            NumberPicker numberPicker2 = this.b.u;
            pq7.b(numberPicker2, "binding.numberPicker");
            A6.o(numberPicker2.getValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i36 b;

        @DexIgnore
        public c(i36 i36) {
            this.b = i36;
        }

        @DexIgnore
        public final void onClick(View view) {
            i36.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ va5 b;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 c;

        @DexIgnore
        public d(va5 va5, dr7 dr7) {
            this.b = va5;
            this.c = dr7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.q;
            pq7.b(constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        va5 va5 = this.b;
                        pq7.b(va5, "it");
                        View n = va5.n();
                        pq7.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new il7("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = i36.class.getSimpleName();
        pq7.b(simpleName, "RemindTimeFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g36 A6(i36 i36) {
        g36 g36 = i36.m;
        if (g36 != null) {
            return g36;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6(va5 va5) {
        ArrayList arrayList = new ArrayList();
        ur7 l2 = bs7.l(new wr7(20, 120), 20);
        int a2 = l2.a();
        int b2 = l2.b();
        int c2 = l2.c();
        if (c2 < 0 ? a2 >= b2 : a2 <= b2) {
            while (true) {
                String g = ll5.g(a2);
                pq7.b(g, "timeString");
                arrayList.add(g);
                if (a2 == b2) {
                    break;
                }
                a2 += c2;
            }
        }
        NumberPicker numberPicker = va5.u;
        pq7.b(numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = va5.u;
        pq7.b(numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = va5.u;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            va5.u.setOnValueChangedListener(new b(this, va5));
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* renamed from: D6 */
    public void M5(g36 g36) {
        pq7.c(g36, "presenter");
        this.m = g36;
    }

    @DexIgnore
    @Override // com.fossil.h36
    public void E(int i) {
        NumberPicker numberPicker;
        g37<va5> g37 = this.l;
        if (g37 != null) {
            va5 a2 = g37.a();
            if (a2 != null && (numberPicker = a2.u) != null) {
                pq7.b(numberPicker, "numberPicker");
                numberPicker.setValue(i / 20);
                g36 g36 = this.m;
                if (g36 != null) {
                    g36.o(numberPicker.getValue());
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.h36
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        va5 va5 = (va5) aq0.f(layoutInflater, 2131558615, viewGroup, false, this.k);
        String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d2)) {
            va5.q.setBackgroundColor(Color.parseColor(d2));
        }
        va5.r.setOnClickListener(new c(this));
        pq7.b(va5, "binding");
        C6(va5);
        this.l = new g37<>(this, va5);
        return va5.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        g36 g36 = this.m;
        if (g36 != null) {
            g36.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        g36 g36 = this.m;
        if (g36 != null) {
            g36.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<va5> g37 = this.l;
        if (g37 != null) {
            va5 a2 = g37.a();
            if (a2 != null) {
                dr7 dr7 = new dr7();
                dr7.element = null;
                dr7.element = (T) new d(a2, dr7);
                pq7.b(a2, "it");
                View n = a2.n();
                pq7.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(dr7.element);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
