package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lb0 implements Parcelable.Creator<mb0> {
    @DexIgnore
    public /* synthetic */ lb0(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public mb0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (pq7.a(readString, ib0.class.getName())) {
            return ib0.CREATOR.a(parcel);
        }
        if (pq7.a(readString, ob0.class.getName())) {
            return ob0.CREATOR.a(parcel);
        }
        if (pq7.a(readString, kb0.class.getName())) {
            return kb0.CREATOR.a(parcel);
        }
        return null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public mb0[] newArray(int i) {
        return new mb0[i];
    }
}
