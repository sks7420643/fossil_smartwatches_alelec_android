package com.fossil;

import android.webkit.MimeTypeMap;
import com.fossil.e61;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f61 implements e61<File> {
    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, File file, f81 f81, x51 x51, qn7<? super d61> qn7) {
        return new k61(s48.d(s48.k(file)), MimeTypeMap.getSingleton().getMimeTypeFromExtension(cp7.f(file)), q51.DISK);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(File file) {
        pq7.c(file, "data");
        return e61.a.a(this, file);
    }

    @DexIgnore
    /* renamed from: f */
    public String b(File file) {
        pq7.c(file, "data");
        return file.getPath() + ':' + file.lastModified();
    }
}
