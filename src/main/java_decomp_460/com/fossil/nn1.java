package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum nn1 {
    DC(n6.DC),
    FTC(n6.FTC),
    FTD(n6.FTD),
    AUT(n6.AUTHENTICATION),
    ASYNC(n6.ASYNC),
    FTD_1(n6.FTD_1),
    HEART_RATE(n6.HEART_RATE);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ n6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }
    }

    @DexIgnore
    public nn1(n6 n6Var) {
        this.b = n6Var;
    }

    @DexIgnore
    public final n6 a() {
        return this.b;
    }
}
