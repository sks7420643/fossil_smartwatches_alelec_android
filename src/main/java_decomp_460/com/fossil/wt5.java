package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt5 implements Factory<vt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DeviceRepository> f3996a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<uo5> f;

    @DexIgnore
    public wt5(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5, Provider<uo5> provider6) {
        this.f3996a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static wt5 a(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5, Provider<uo5> provider6) {
        return new wt5(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static vt5 c(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository, uo5 uo5) {
        return new vt5(deviceRepository, hybridPresetRepository, dianaPresetRepository, portfolioApp, watchFaceRepository, uo5);
    }

    @DexIgnore
    /* renamed from: b */
    public vt5 get() {
        return c(this.f3996a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }
}
