package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z14<F, T> extends i44<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ b14<F, ? extends T> function;
    @DexIgnore
    public /* final */ i44<T> ordering;

    @DexIgnore
    public z14(b14<F, ? extends T> b14, i44<T> i44) {
        i14.l(b14);
        this.function = b14;
        i14.l(i44);
        this.ordering = i44;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.i44<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.i44, java.util.Comparator
    public int compare(F f, F f2) {
        return this.ordering.compare(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z14)) {
            return false;
        }
        z14 z14 = (z14) obj;
        return this.function.equals(z14.function) && this.ordering.equals(z14.ordering);
    }

    @DexIgnore
    public int hashCode() {
        return f14.b(this.function, this.ordering);
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".onResultOf(" + this.function + ")";
    }
}
