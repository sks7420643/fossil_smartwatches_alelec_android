package com.fossil;

import com.fossil.f17;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v17 extends gq4<u17> {
    @DexIgnore
    void E1(Double d, Double d2);

    @DexIgnore
    void I2(long j);

    @DexIgnore
    void L2(String str);

    @DexIgnore
    void W0(int i);

    @DexIgnore
    void f3(boolean z, boolean z2);

    @DexIgnore
    Object j1();  // void declaration

    @DexIgnore
    void o(int i, String str);

    @DexIgnore
    Object r6();  // void declaration

    @DexIgnore
    void w3(f17.d dVar);
}
