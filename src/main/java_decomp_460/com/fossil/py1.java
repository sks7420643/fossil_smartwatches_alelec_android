package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class py1<V, E> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public V f2890a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<V, tl7>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<rp7<E, tl7>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<rp7<tl7, tl7>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<rp7<E, tl7>> g; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $error$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rp7 rp7, qn7 qn7, Object obj) {
            super(2, qn7);
            this.$action = rp7;
            this.$error$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$action, qn7, this.$error$inlined);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.$action.invoke(this.$error$inlined);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.common.task.Promise$finally$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $actionOnFinal;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ py1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(py1 py1, rp7 rp7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = py1;
            this.$actionOnFinal = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$actionOnFinal, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.f.add(this.$actionOnFinal);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.common.task.Promise$onError$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $actionOnError;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ py1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(py1 py1, rp7 rp7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = py1;
            this.$actionOnError = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$actionOnError, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.e.add(this.$actionOnError);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.common.task.Promise$onSuccess$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $actionOnSuccess;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ py1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(py1 py1, rp7 rp7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = py1;
            this.$actionOnSuccess = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$actionOnSuccess, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.d.add(this.$actionOnSuccess);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $error$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(rp7 rp7, qn7 qn7, Object obj) {
            super(2, qn7);
            this.$action = rp7;
            this.$error$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$action, qn7, this.$error$inlined);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.$action.invoke(this.$error$inlined);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $result$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(rp7 rp7, qn7 qn7, Object obj) {
            super(2, qn7);
            this.$action = rp7;
            this.$result$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.$action, qn7, this.$result$inlined);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.$action.invoke(this.$result$inlined);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void d(E e2) {
        synchronized (this) {
            pq7.c(e2, "error");
            if (!g() && !this.c) {
                this.c = true;
                if (!this.g.isEmpty()) {
                    Iterator<T> it = this.g.iterator();
                    while (it.hasNext()) {
                        try {
                            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(it.next(), null, e2), 3, null);
                        } catch (Exception e3) {
                        }
                    }
                    this.c = false;
                } else {
                    n(e2);
                }
            }
        }
    }

    @DexIgnore
    public py1<V, E> e(rp7<? super tl7, tl7> rp7) {
        pq7.c(rp7, "actionOnFinal");
        if (g()) {
            try {
                rp7.invoke(tl7.f3441a);
            } catch (Exception e2) {
            }
        } else {
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new b(this, rp7, null), 3, null);
        }
        return this;
    }

    @DexIgnore
    public final V f() {
        return this.f2890a;
    }

    @DexIgnore
    public final boolean g() {
        return i() || h();
    }

    @DexIgnore
    public final boolean h() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean i() {
        return this.f2890a != null;
    }

    @DexIgnore
    public void j() {
        synchronized (this) {
            Iterator<T> it = this.f.iterator();
            while (it.hasNext()) {
                try {
                    it.next().invoke(tl7.f3441a);
                } catch (Exception e2) {
                }
            }
            this.d.clear();
            this.e.clear();
            this.f.clear();
            this.g.clear();
        }
    }

    @DexIgnore
    public py1<V, E> k(rp7<? super E, tl7> rp7) {
        pq7.c(rp7, "actionOnCancel");
        if (!g()) {
            this.g.add(rp7);
        }
        return this;
    }

    @DexIgnore
    public py1<V, E> l(rp7<? super E, tl7> rp7) {
        pq7.c(rp7, "actionOnError");
        if (!g()) {
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new c(this, rp7, null), 3, null);
        } else if (h()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    rp7.invoke(e2);
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e3) {
            }
        }
        return this;
    }

    @DexIgnore
    public py1<V, E> m(rp7<? super V, tl7> rp7) {
        pq7.c(rp7, "actionOnSuccess");
        if (!g()) {
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new d(this, rp7, null), 3, null);
        } else if (i()) {
            try {
                V v = this.f2890a;
                if (v != null) {
                    rp7.invoke(v);
                } else {
                    pq7.i();
                    throw null;
                }
            } catch (Exception e2) {
            }
        }
        return this;
    }

    @DexIgnore
    public final void n(E e2) {
        synchronized (this) {
            pq7.c(e2, "error");
            if (!g()) {
                this.b = e2;
                this.f2890a = this.f2890a;
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    try {
                        xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new e(it.next(), null, e2), 3, null);
                    } catch (Exception e3) {
                    }
                }
                j();
            }
        }
    }

    @DexIgnore
    public final void o(V v) {
        synchronized (this) {
            pq7.c(v, Constants.RESULT);
            if (!g()) {
                this.f2890a = v;
                this.b = null;
                Iterator<T> it = this.d.iterator();
                while (it.hasNext()) {
                    try {
                        xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new f(it.next(), null, v), 3, null);
                    } catch (Exception e2) {
                    }
                }
                j();
            }
        }
    }
}
