package com.fossil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ byte[] f3744a;
    @DexIgnore
    public volatile int b; // = 0;

    @DexIgnore
    public vb4(byte[] bArr) {
        this.f3744a = bArr;
    }

    @DexIgnore
    public static vb4 a(byte[] bArr) {
        return b(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static vb4 b(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new vb4(bArr2);
    }

    @DexIgnore
    public static vb4 c(String str) {
        try {
            return new vb4(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    @DexIgnore
    public void d(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.f3744a, i, bArr, i2, i3);
    }

    @DexIgnore
    public InputStream e() {
        return new ByteArrayInputStream(this.f3744a);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof vb4)) {
            return false;
        }
        byte[] bArr = this.f3744a;
        int length = bArr.length;
        byte[] bArr2 = ((vb4) obj).f3744a;
        if (length != bArr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int f() {
        return this.f3744a.length;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        if (i == 0) {
            byte[] bArr = this.f3744a;
            int length = bArr.length;
            i = length;
            for (byte b2 : bArr) {
                i = (i * 31) + b2;
            }
            if (i == 0) {
                i = 1;
            }
            this.b = i;
        }
        return i;
    }
}
