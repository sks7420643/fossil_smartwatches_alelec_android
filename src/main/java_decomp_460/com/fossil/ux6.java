package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.n04;
import com.fossil.t47;
import com.fossil.x37;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux6 extends ey6 implements gq4, t47.g {
    @DexIgnore
    public static /* final */ String w; // = "STATE";
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public hy6 h;
    @DexIgnore
    public qy6 i;
    @DexIgnore
    public g37<ub5> j;
    @DexIgnore
    public gr4 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public int m;
    @DexIgnore
    public /* final */ String s; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String t; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String u; // = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ux6 a(String str, boolean z, int i) {
            pq7.c(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putString("SERIAL", str);
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putInt(ux6.w, i);
            ux6 ux6 = new ux6();
            ux6.setArguments(bundle);
            return ux6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ux6 b;

        @DexIgnore
        public b(ux6 ux6, String str) {
            this.b = ux6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ub5 f3670a;
        @DexIgnore
        public /* final */ /* synthetic */ ux6 b;

        @DexIgnore
        public c(ub5 ub5, ux6 ux6, String str) {
            this.f3670a = ub5;
            this.b = ux6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.t)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "set icon color " + this.b.t);
                int parseColor = Color.parseColor(this.b.t);
                TabLayout.g v = this.f3670a.A.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.s) && this.b.m != i) {
                int parseColor2 = Color.parseColor(this.b.s);
                TabLayout.g v2 = this.f3670a.A.v(this.b.m);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.m = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ux6 b;

        @DexIgnore
        public d(ux6 ux6, String str) {
            this.b = ux6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().B();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ux6 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public e(ux6 ux6, String str) {
            this.b = ux6;
            this.c = str;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                Context requireContext = this.b.requireContext();
                pq7.b(requireContext, "requireContext()");
                TroubleshootingActivity.a.c(aVar, requireContext, this.c, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux6 f3671a;

        @DexIgnore
        public f(ux6 ux6) {
            this.f3671a = ux6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f3671a.s) && !TextUtils.isEmpty(this.f3671a.t)) {
                int parseColor = Color.parseColor(this.f3671a.s);
                int parseColor2 = Color.parseColor(this.f3671a.t);
                gVar.o(2131230966);
                if (i == this.f3671a.m) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    public final void A(boolean z) {
        if (isActive()) {
            g37<ub5> g37 = this.j;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    pq7.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    pq7.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    pq7.b(flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    pq7.b(flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    pq7.b(flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setProgress(1000);
                    FlexibleTextView flexibleTextView2 = a2.y;
                    pq7.b(flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887045));
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.q;
                pq7.b(constraintLayout3, "it.clUpdateFwFail");
                constraintLayout3.setVisibility(0);
                ConstraintLayout constraintLayout4 = a2.r;
                pq7.b(constraintLayout4, "it.clUpdatingFw");
                constraintLayout4.setVisibility(8);
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final hy6 P6() {
        hy6 hy6 = this.h;
        if (hy6 != null) {
            return hy6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(hy6 hy6) {
        pq7.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.l);
            if (i2 == 2131362290) {
                hy6 hy6 = this.h;
                if (hy6 != null) {
                    hy6.B();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131362385 && getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.B;
                FragmentActivity requireActivity = requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore
    public void R6() {
        DashBar dashBar;
        g37<ub5> g37 = this.j;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.c(dashBar, this.l, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        g37<ub5> g37 = this.j;
        if (g37 != null) {
            ub5 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                g37<ub5> g372 = this.j;
                if (g372 != null) {
                    ub5 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new n04(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V2() {
        if (isActive()) {
            g37<ub5> g37 = this.j;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    pq7.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    pq7.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void X(List<? extends Explore> list) {
        pq7.c(list, "data");
        gr4 gr4 = this.k;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            pq7.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        g37<ub5> g37 = this.j;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void f() {
        DashBar dashBar;
        g37<ub5> g37 = this.j;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.i(dashBar, this.l, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ub5 ub5 = (ub5) aq0.f(layoutInflater, 2131558632, viewGroup, false, A6());
        this.j = new g37<>(this, ub5);
        pq7.b(ub5, "binding");
        return ub5.n();
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qy6 qy6 = this.i;
        if (qy6 != null) {
            qy6.e();
        } else {
            pq7.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qy6 qy6 = this.i;
        if (qy6 != null) {
            qy6.d();
        } else {
            pq7.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String string;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.l = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        Bundle arguments2 = getArguments();
        String str = (arguments2 == null || (string = arguments2.getString("SERIAL", "")) == null) ? "" : string;
        Bundle arguments3 = getArguments();
        int i2 = arguments3 != null ? arguments3.getInt(w) : 0;
        this.i = new qy6(str, this);
        this.k = new gr4(new ArrayList());
        g37<ub5> g37 = this.j;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                pq7.b(constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                pq7.b(constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                pq7.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                pq7.b(flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                pq7.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this, str));
                ViewPager2 viewPager2 = a2.F;
                pq7.b(viewPager2, "binding.rvpTutorial");
                gr4 gr4 = this.k;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (a2.F.getChildAt(0) != null) {
                        View childAt = a2.F.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    if (!TextUtils.isEmpty(this.u)) {
                        TabLayout tabLayout = a2.A;
                        pq7.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                    }
                    S6();
                    a2.F.g(new c(a2, this, str));
                    a2.t.setOnClickListener(new d(this, str));
                    a2.w.setOnClickListener(new e(this, str));
                } else {
                    pq7.n("mAdapterUpdateFirmware");
                    throw null;
                }
            }
            if (i2 == 1) {
                t3();
            } else if (i2 == 2) {
                A(true);
            } else if (i2 == 3) {
                A(false);
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t3() {
        if (isActive()) {
            g37<ub5> g37 = this.j;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 != null) {
                    qy6 qy6 = this.i;
                    if (qy6 != null) {
                        qy6.f();
                        ConstraintLayout constraintLayout = a2.q;
                        pq7.b(constraintLayout, "it.clUpdateFwFail");
                        constraintLayout.setVisibility(8);
                        ConstraintLayout constraintLayout2 = a2.r;
                        pq7.b(constraintLayout2, "it.clUpdatingFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleButton flexibleButton = a2.s;
                        pq7.b(flexibleButton, "it.fbContinue");
                        flexibleButton.setVisibility(0);
                        FlexibleTextView flexibleTextView = a2.z;
                        pq7.b(flexibleTextView, "it.ftvUpdateWarning");
                        flexibleTextView.setVisibility(4);
                        FlexibleProgressBar flexibleProgressBar = a2.D;
                        pq7.b(flexibleProgressBar, "it.progressUpdate");
                        flexibleProgressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "it.ftvCountdownTime");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.y;
                        pq7.b(flexibleTextView3, "it.ftvUpdate");
                        flexibleTextView3.setText(um5.c(PortfolioApp.h0.c(), 2131886783));
                        return;
                    }
                    pq7.n("mSubPresenter");
                    throw null;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
