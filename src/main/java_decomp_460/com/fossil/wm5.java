package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wm5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3973a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.f3973a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public void d(gj4 gj4) {
        if (gj4.s("platform")) {
            gj4.p("platform").f();
        }
        if (gj4.s("data")) {
            this.f3973a = gj4.p("data").d().p("url").f();
        }
        if (gj4.s("metadata")) {
            this.b = gj4.p("metadata").d().p("checksum").f();
        }
        if (gj4.s("metadata")) {
            this.c = gj4.p("metadata").d().p("appVersion").f();
        }
        if (gj4.s("createdAt")) {
            gj4.p("createdAt").f();
        }
        if (gj4.s("updatedAt")) {
            this.d = gj4.p("updatedAt").f();
        }
        if (gj4.s("objectId")) {
            gj4.p("objectId").f();
        }
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.f3973a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }
}
