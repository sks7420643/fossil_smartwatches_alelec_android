package com.fossil;

import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3712a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LinkedHashSet<Long> c;

    @DexIgnore
    public v4(int i, int i2, LinkedHashSet<Long> linkedHashSet) {
        this.f3712a = i;
        this.b = i2;
        this.c = linkedHashSet;
    }
}
