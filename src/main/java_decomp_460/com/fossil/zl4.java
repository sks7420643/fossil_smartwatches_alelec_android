package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zl4 {
    @DexIgnore
    public static /* final */ zl4 b; // = new xl4(null, 0, 0);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zl4 f4490a;

    @DexIgnore
    public zl4(zl4 zl4) {
        this.f4490a = zl4;
    }

    @DexIgnore
    public final zl4 a(int i, int i2) {
        return new xl4(this, i, i2);
    }

    @DexIgnore
    public final zl4 b(int i, int i2) {
        return new ul4(this, i, i2);
    }

    @DexIgnore
    public abstract void c(am4 am4, byte[] bArr);

    @DexIgnore
    public final zl4 d() {
        return this.f4490a;
    }
}
