package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx4 implements Factory<fx4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ gx4 f1389a; // = new gx4();
    }

    @DexIgnore
    public static gx4 a() {
        return a.f1389a;
    }

    @DexIgnore
    public static fx4 c() {
        return new fx4();
    }

    @DexIgnore
    /* renamed from: b */
    public fx4 get() {
        return c();
    }
}
