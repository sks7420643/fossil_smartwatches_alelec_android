package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import com.facebook.stetho.server.http.HttpHeaders;
import com.squareup.picasso.Downloader;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wd7 implements Downloader {
    @DexIgnore
    public static volatile Object b;
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> d; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3922a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        /* renamed from: a */
        public StringBuilder initialValue() {
            return new StringBuilder();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static Object a(Context context) throws IOException {
            File f = xd7.f(context);
            HttpResponseCache installed = HttpResponseCache.getInstalled();
            return installed == null ? HttpResponseCache.install(f, xd7.a(f)) : installed;
        }
    }

    @DexIgnore
    public wd7(Context context) {
        this.f3922a = context.getApplicationContext();
    }

    @DexIgnore
    public static void a(Context context) {
        if (b == null) {
            try {
                synchronized (c) {
                    if (b == null) {
                        b = b.a(context);
                    }
                }
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public HttpURLConnection b(Uri uri) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(uri.toString()).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(20000);
        return httpURLConnection;
    }

    @DexIgnore
    @Override // com.squareup.picasso.Downloader
    public Downloader.Response load(Uri uri, int i) throws IOException {
        String sb;
        if (Build.VERSION.SDK_INT >= 14) {
            a(this.f3922a);
        }
        HttpURLConnection b2 = b(uri);
        b2.setUseCaches(true);
        if (i != 0) {
            if (kd7.isOfflineOnly(i)) {
                sb = "only-if-cached,max-age=2147483647";
            } else {
                StringBuilder sb2 = d.get();
                sb2.setLength(0);
                if (!kd7.shouldReadFromDiskCache(i)) {
                    sb2.append("no-cache");
                }
                if (!kd7.shouldWriteToDiskCache(i)) {
                    if (sb2.length() > 0) {
                        sb2.append(',');
                    }
                    sb2.append("no-store");
                }
                sb = sb2.toString();
            }
            b2.setRequestProperty(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, sb);
        }
        int responseCode = b2.getResponseCode();
        if (responseCode < 300) {
            long headerFieldInt = (long) b2.getHeaderFieldInt(HttpHeaders.CONTENT_LENGTH, -1);
            return new Downloader.Response(b2.getInputStream(), xd7.w(b2.getHeaderField("X-Android-Response-Source")), headerFieldInt);
        }
        b2.disconnect();
        throw new Downloader.a(responseCode + " " + b2.getResponseMessage(), i, responseCode);
    }
}
