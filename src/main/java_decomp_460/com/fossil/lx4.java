package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Object> f2276a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<iz4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ on5 c;
    @DexIgnore
    public /* final */ tt4 d;
    @DexIgnore
    public /* final */ zt4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1", f = "BCCreateSubTabViewModel.kt", l = {33, 38}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lx4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx4$a$a")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$challenge$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.lx4$a$a  reason: collision with other inner class name */
        public static final class C0147a extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0147a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0147a aVar = new C0147a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
                return ((C0147a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.d.g(new String[]{"running", "waiting"}, new Date());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$countFriend$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return ao7.e(this.this$0.this$0.e.e());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lx4 lx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0055  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00c4  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 0
                r7 = 2
                r6 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x007a
                if (r0 == r6) goto L_0x0047
                if (r0 != r7) goto L_0x003f
                java.lang.Object r0 = r9.L$1
                com.fossil.ps4 r0 = (com.fossil.ps4) r0
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
                r0 = r10
            L_0x001b:
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                if (r0 >= r6) goto L_0x00a9
                com.fossil.lx4 r0 = r9.this$0
                com.fossil.on5 r0 = com.fossil.lx4.c(r0)
                boolean r0 = r0.V()
                if (r0 == 0) goto L_0x0094
                com.fossil.lx4 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.lx4.d(r0)
                java.lang.Boolean r1 = com.fossil.ao7.a(r6)
                r0.l(r1)
            L_0x003c:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x003e:
                return r0
            L_0x003f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0047:
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0050:
                r0 = r1
                com.fossil.ps4 r0 = (com.fossil.ps4) r0
                if (r0 != 0) goto L_0x00c4
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                boolean r1 = r1.L()
                if (r1 == 0) goto L_0x00b7
                com.fossil.dv7 r4 = com.fossil.bw7.b()
                com.fossil.lx4$a$b r5 = new com.fossil.lx4$a$b
                r5.<init>(r9, r8)
                r9.L$0 = r2
                r9.L$1 = r0
                r9.Z$0 = r1
                r9.label = r7
                java.lang.Object r0 = com.fossil.eu7.g(r4, r5, r9)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x003e
            L_0x007a:
                com.fossil.el7.b(r10)
                com.fossil.iv7 r0 = r9.p$
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.lx4$a$a r2 = new com.fossil.lx4$a$a
                r2.<init>(r9, r8)
                r9.L$0 = r0
                r9.label = r6
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r9)
                if (r1 != r3) goto L_0x00d1
                r0 = r3
                goto L_0x003e
            L_0x0094:
                com.fossil.lx4 r0 = r9.this$0
                com.fossil.on5 r0 = com.fossil.lx4.c(r0)
                r0.j1()
                com.fossil.lx4 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.lx4.e(r0)
                com.fossil.iz4 r1 = com.fossil.iz4.SUGGEST_FIND_FRIEND
                r0.l(r1)
                goto L_0x003c
            L_0x00a9:
                com.fossil.lx4 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.lx4.d(r0)
                java.lang.Boolean r1 = com.fossil.ao7.a(r6)
                r0.l(r1)
                goto L_0x003c
            L_0x00b7:
                com.fossil.lx4 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.lx4.e(r0)
                com.fossil.iz4 r1 = com.fossil.iz4.NEED_ACTIVE_DEVICE
                r0.l(r1)
                goto L_0x003c
            L_0x00c4:
                com.fossil.lx4 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.lx4.e(r0)
                com.fossil.iz4 r1 = com.fossil.iz4.JOIN_TOO_MUCH
                r0.l(r1)
                goto L_0x003c
            L_0x00d1:
                r2 = r0
                goto L_0x0050
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.lx4.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public lx4(on5 on5, tt4 tt4, zt4 zt4) {
        pq7.c(on5, "shared");
        pq7.c(tt4, "challengeRepository");
        pq7.c(zt4, "friendRepository");
        this.c = on5;
        this.d = tt4;
        this.e = zt4;
    }

    @DexIgnore
    public final LiveData<Object> f() {
        return this.f2276a;
    }

    @DexIgnore
    public final LiveData<iz4> g() {
        return this.b;
    }

    @DexIgnore
    public final void h() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }
}
