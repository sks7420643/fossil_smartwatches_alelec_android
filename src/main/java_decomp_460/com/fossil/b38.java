package com.fossil;

import com.fossil.p18;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b38 implements s28 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ OkHttpClient f393a;
    @DexIgnore
    public /* final */ p28 b;
    @DexIgnore
    public /* final */ k48 c;
    @DexIgnore
    public /* final */ j48 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements c58 {
        @DexIgnore
        public /* final */ o48 b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public long d;

        @DexIgnore
        public b() {
            this.b = new o48(b38.this.c.e());
            this.d = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            b38 b38 = b38.this;
            int i = b38.e;
            if (i != 6) {
                if (i == 5) {
                    b38.g(this.b);
                    b38 b382 = b38.this;
                    b382.e = 6;
                    p28 p28 = b382.b;
                    if (p28 != null) {
                        p28.r(!z, b382, this.d, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + b38.this.e);
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public long d0(i48 i48, long j) throws IOException {
            try {
                long d0 = b38.this.c.d0(i48, j);
                if (d0 > 0) {
                    this.d += d0;
                }
                return d0;
            } catch (IOException e2) {
                a(false, e2);
                throw e2;
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public d58 e() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements a58 {
        @DexIgnore
        public /* final */ o48 b; // = new o48(b38.this.d.e());
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.a58
        public void K(i48 i48, long j) throws IOException {
            if (this.c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                b38.this.d.O(j);
                b38.this.d.E("\r\n");
                b38.this.d.K(i48, j);
                b38.this.d.E("\r\n");
            }
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            synchronized (this) {
                if (!this.c) {
                    this.c = true;
                    b38.this.d.E("0\r\n\r\n");
                    b38.this.g(this.b);
                    b38.this.e = 3;
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.a58
        public d58 e() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Flushable
        public void flush() throws IOException {
            synchronized (this) {
                if (!this.c) {
                    b38.this.d.flush();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public /* final */ q18 f;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public boolean h; // = true;

        @DexIgnore
        public d(q18 q18) {
            super();
            this.f = q18;
        }

        @DexIgnore
        public final void b() throws IOException {
            if (this.g != -1) {
                b38.this.c.U();
            }
            try {
                this.g = b38.this.c.m0();
                String trim = b38.this.c.U().trim();
                if (this.g < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.g + trim + "\"");
                } else if (this.g == 0) {
                    this.h = false;
                    u28.g(b38.this.f393a.o(), this.f, b38.this.n());
                    a(true, null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (this.h && !b28.p(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.c58, com.fossil.b38.b
        public long d0(i48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.c) {
                throw new IllegalStateException("closed");
            } else if (!this.h) {
                return -1;
            } else {
                long j2 = this.g;
                if (j2 == 0 || j2 == -1) {
                    b();
                    if (!this.h) {
                        return -1;
                    }
                }
                long d0 = super.d0(i48, Math.min(j, this.g));
                if (d0 != -1) {
                    this.g -= d0;
                    return d0;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements a58 {
        @DexIgnore
        public /* final */ o48 b; // = new o48(b38.this.d.e());
        @DexIgnore
        public boolean c;
        @DexIgnore
        public long d;

        @DexIgnore
        public e(long j) {
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.a58
        public void K(i48 i48, long j) throws IOException {
            if (!this.c) {
                b28.f(i48.p0(), 0, j);
                if (j <= this.d) {
                    b38.this.d.K(i48, j);
                    this.d -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.d + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                this.c = true;
                if (this.d <= 0) {
                    b38.this.g(this.b);
                    b38.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        @Override // com.fossil.a58
        public d58 e() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Flushable
        public void flush() throws IOException {
            if (!this.c) {
                b38.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long f;

        @DexIgnore
        public f(b38 b38, long j) throws IOException {
            super();
            this.f = j;
            if (j == 0) {
                a(true, null);
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (this.f != 0 && !b28.p(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.c58, com.fossil.b38.b
        public long d0(i48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!this.c) {
                long j2 = this.f;
                if (j2 == 0) {
                    return -1;
                }
                long d0 = super.d0(i48, Math.min(j2, j));
                if (d0 != -1) {
                    long j3 = this.f - d0;
                    this.f = j3;
                    if (j3 == 0) {
                        a(true, null);
                    }
                    return d0;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean f;

        @DexIgnore
        public g(b38 b38) {
            super();
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (!this.f) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.c58, com.fossil.b38.b
        public long d0(i48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.c) {
                throw new IllegalStateException("closed");
            } else if (this.f) {
                return -1;
            } else {
                long d0 = super.d0(i48, j);
                if (d0 != -1) {
                    return d0;
                }
                this.f = true;
                a(true, null);
                return -1;
            }
        }
    }

    @DexIgnore
    public b38(OkHttpClient okHttpClient, p28 p28, k48 k48, j48 j48) {
        this.f393a = okHttpClient;
        this.b = p28;
        this.c = k48;
        this.d = j48;
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void b(v18 v18) throws IOException {
        o(v18.e(), y28.a(v18, this.b.d().q().b().type()));
    }

    @DexIgnore
    @Override // com.fossil.s28
    public w18 c(Response response) throws IOException {
        p28 p28 = this.b;
        p28.f.q(p28.e);
        String j = response.j("Content-Type");
        if (!u28.c(response)) {
            return new x28(j, 0, s48.d(k(0)));
        }
        if ("chunked".equalsIgnoreCase(response.j("Transfer-Encoding"))) {
            return new x28(j, -1, s48.d(i(response.G().j())));
        }
        long b2 = u28.b(response);
        return b2 != -1 ? new x28(j, b2, s48.d(k(b2))) : new x28(j, -1, s48.d(l()));
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void cancel() {
        l28 d2 = this.b.d();
        if (d2 != null) {
            d2.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.s28
    public Response.a d(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                a38 a2 = a38.a(m());
                Response.a aVar = new Response.a();
                aVar.n(a2.f190a);
                aVar.g(a2.b);
                aVar.k(a2.c);
                aVar.j(n());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void e() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    @Override // com.fossil.s28
    public a58 f(v18 v18, long j) {
        if ("chunked".equalsIgnoreCase(v18.c("Transfer-Encoding"))) {
            return h();
        }
        if (j != -1) {
            return j(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void g(o48 o48) {
        d58 i = o48.i();
        o48.j(d58.d);
        i.a();
        i.b();
    }

    @DexIgnore
    public a58 h() {
        if (this.e == 1) {
            this.e = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public c58 i(q18 q18) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new d(q18);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public a58 j(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public c58 k(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new f(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public c58 l() throws IOException {
        if (this.e == 4) {
            p28 p28 = this.b;
            if (p28 != null) {
                this.e = 5;
                p28.j();
                return new g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String m() throws IOException {
        String z = this.c.z(this.f);
        this.f -= (long) z.length();
        return z;
    }

    @DexIgnore
    public p18 n() throws IOException {
        p18.a aVar = new p18.a();
        while (true) {
            String m = m();
            if (m.length() == 0) {
                return aVar.e();
            }
            z18.f4406a.a(aVar, m);
        }
    }

    @DexIgnore
    public void o(p18 p18, String str) throws IOException {
        if (this.e == 0) {
            this.d.E(str).E("\r\n");
            int h = p18.h();
            for (int i = 0; i < h; i++) {
                this.d.E(p18.e(i)).E(": ").E(p18.i(i)).E("\r\n");
            }
            this.d.E("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }
}
