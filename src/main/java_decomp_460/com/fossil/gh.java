package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ lp b;
    @DexIgnore
    public /* final */ /* synthetic */ rp7 c;
    @DexIgnore
    public /* final */ /* synthetic */ rp7 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gh(lp lpVar, rp7 rp7, rp7 rp72) {
        super(1);
        this.b = lpVar;
        this.c = rp7;
        this.d = rp72;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        fs fsVar2 = fsVar;
        if (((Boolean) this.c.invoke(fsVar2.v)).booleanValue()) {
            this.b.o(fsVar2.v);
        } else {
            this.d.invoke(fsVar2);
        }
        return tl7.f3441a;
    }
}
