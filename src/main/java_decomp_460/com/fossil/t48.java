package com.fossil;

import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t48 implements a58 {
    @DexIgnore
    public /* final */ OutputStream b;
    @DexIgnore
    public /* final */ d58 c;

    @DexIgnore
    public t48(OutputStream outputStream, d58 d58) {
        pq7.c(outputStream, "out");
        pq7.c(d58, "timeout");
        this.b = outputStream;
        this.c = d58;
    }

    @DexIgnore
    @Override // com.fossil.a58
    public void K(i48 i48, long j) {
        pq7.c(i48, "source");
        f48.b(i48.p0(), 0, j);
        while (j > 0) {
            this.c.f();
            x48 x48 = i48.b;
            if (x48 != null) {
                int min = (int) Math.min(j, (long) (x48.c - x48.b));
                this.b.write(x48.f4040a, x48.b, min);
                x48.b += min;
                long j2 = (long) min;
                j -= j2;
                i48.o0(i48.p0() - j2);
                if (x48.b == x48.c) {
                    i48.b = x48.b();
                    y48.c.a(x48);
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.a58
    public d58 e() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.a58, java.io.Flushable
    public void flush() {
        this.b.flush();
    }

    @DexIgnore
    public String toString() {
        return "sink(" + this.b + ')';
    }
}
