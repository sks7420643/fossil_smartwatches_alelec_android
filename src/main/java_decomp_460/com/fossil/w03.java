package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class w03 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3863a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[l43.values().length];
        b = iArr;
        try {
            iArr[l43.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            b[l43.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            b[l43.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            b[l43.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            b[l43.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            b[l43.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            b[l43.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            b[l43.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            b[l43.GROUP.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            b[l43.MESSAGE.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            b[l43.STRING.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            b[l43.BYTES.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            b[l43.UINT32.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            b[l43.SFIXED32.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            b[l43.SFIXED64.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            b[l43.SINT32.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            b[l43.SINT64.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            b[l43.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
        int[] iArr2 = new int[s43.values().length];
        f3863a = iArr2;
        try {
            iArr2[s43.INT.ordinal()] = 1;
        } catch (NoSuchFieldError e19) {
        }
        try {
            f3863a[s43.LONG.ordinal()] = 2;
        } catch (NoSuchFieldError e20) {
        }
        try {
            f3863a[s43.FLOAT.ordinal()] = 3;
        } catch (NoSuchFieldError e21) {
        }
        try {
            f3863a[s43.DOUBLE.ordinal()] = 4;
        } catch (NoSuchFieldError e22) {
        }
        try {
            f3863a[s43.BOOLEAN.ordinal()] = 5;
        } catch (NoSuchFieldError e23) {
        }
        try {
            f3863a[s43.STRING.ordinal()] = 6;
        } catch (NoSuchFieldError e24) {
        }
        try {
            f3863a[s43.BYTE_STRING.ordinal()] = 7;
        } catch (NoSuchFieldError e25) {
        }
        try {
            f3863a[s43.ENUM.ordinal()] = 8;
        } catch (NoSuchFieldError e26) {
        }
        try {
            f3863a[s43.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError e27) {
        }
    }
    */
}
