package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.os.PowerManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.ResumeWorkoutInfoData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.StartWorkoutInfoData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fs5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Location f1186a;
    @DexIgnore
    public /* final */ PowerManager b;
    @DexIgnore
    public a c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public Handler e;
    @DexIgnore
    public Runnable f;
    @DexIgnore
    public /* final */ u08 g;
    @DexIgnore
    public /* final */ iv7 h;
    @DexIgnore
    public /* final */ b i;
    @DexIgnore
    public /* final */ PortfolioApp j;
    @DexIgnore
    public /* final */ LocationSource k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements LocationSource.LocationListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fs5 f1187a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(fs5 fs5) {
            this.f1187a = fs5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            pq7.c(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutTetherGpsManager", "onLocationChanged location=" + location);
            if (this.f1187a.f1186a == null) {
                long currentTimeMillis = System.currentTimeMillis() - location.getTime();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WorkoutTetherGpsManager", "delta of first location from android " + currentTimeMillis);
                if (currentTimeMillis > ((long) 2000)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("WorkoutTetherGpsManager", "Ignore this outdated location " + location);
                    return;
                }
                this.f1187a.f1186a = location;
            }
            this.f1187a.j.P1(this.f1187a.j.J(), location);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fs5 f1188a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(fs5 fs5) {
            this.f1188a = fs5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (pq7.a(intent != null ? intent.getAction() : null, "android.os.action.POWER_SAVE_MODE_CHANGED")) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "mPowerSavingModeChangedReceiver");
                this.f1188a.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ fs5 b;

        @DexIgnore
        public c(fs5 fs5) {
            this.b = fs5;
        }

        @DexIgnore
        public final void run() {
            this.b.t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.workout.WorkoutTetherGpsManager$startLocationObserver$1", f = "WorkoutTetherGpsManager.kt", l = {261}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(fs5 fs5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fs5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            u08 u08;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                u08 = this.this$0.g;
                this.L$0 = iv7;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                u08 = (u08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                boolean isObservingLocation = this.this$0.k.isObservingLocation(this.this$0.c, false);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherGpsManager", "startLocationObserver isObserving " + isObservingLocation);
                this.this$0.e.removeCallbacksAndMessages(null);
                if (!isObservingLocation) {
                    this.this$0.f1186a = null;
                    FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "startLocationObserver start observing location");
                    this.this$0.k.observerLocation(this.this$0.j, this.this$0.c, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                tl7 tl7 = tl7.f3441a;
                u08.b(null);
                return tl7.f3441a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore
    public fs5(PortfolioApp portfolioApp, LocationSource locationSource) {
        pq7.c(portfolioApp, "mPortfolioApp");
        pq7.c(locationSource, "mLocationSource");
        this.j = portfolioApp;
        this.k = locationSource;
        Object systemService = PortfolioApp.h0.c().getSystemService("power");
        if (systemService != null) {
            this.b = (PowerManager) systemService;
            this.c = new a(this);
            this.e = new Handler();
            this.f = new c(this);
            this.g = w08.b(false, 1, null);
            this.h = jv7.a(bw7.c().plus(ux7.b(null, 1, null)));
            this.i = new b(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.os.PowerManager");
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "autoResumeWorkoutTracking ");
        this.d = true;
        q();
        m();
        n();
    }

    @DexIgnore
    public final String j(boolean z, boolean z2) {
        boolean m = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_BACKGROUND);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp c2 = PortfolioApp.h0.c();
        String str = "";
        if (!z || !m) {
            str = " " + c2.getResources().getString(2131886730);
        }
        if (!z2) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + c2.getResources().getString(2131886731);
        }
        if (isPowerSaveMode) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + c2.getResources().getString(2131886732);
        }
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "watchContent " + str);
        if (!(str.length() > 0)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(' ');
        hr7 hr7 = hr7.f1520a;
        String string = c2.getResources().getString(2131886733);
        pq7.b(string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        String Q = c2.Q();
        if (Q != null) {
            String upperCase = Q.toUpperCase();
            pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String format = String.format(string, Arrays.copyOf(new Object[]{upperCase}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            sb.append(format);
            return sb.toString();
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final cl7<String, String> k() {
        String str;
        String str2;
        boolean m = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_FINE);
        boolean m2 = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_BACKGROUND);
        boolean m3 = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_SERVICE);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp c2 = PortfolioApp.h0.c();
        hr7 hr7 = hr7.f1520a;
        String string = c2.getResources().getString(2131886733);
        pq7.b(string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        String Q = c2.Q();
        if (Q != null) {
            String upperCase = Q.toUpperCase();
            pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String format = String.format(string, Arrays.copyOf(new Object[]{upperCase}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "postWorkoutNotification isFinePermissionGranted " + m + " isBackgroundLocationGranted " + m2 + " isLocationTurnOn " + m3 + " isPowerSaving " + isPowerSaveMode);
            if (!m || !m2) {
                str = c2.getResources().getString(2131886730);
                pq7.b(str, "context.resources.getStr\u2026LocationPermissionAlways)");
                str2 = str + ' ' + format;
            } else if (m && m2 && !m3) {
                str = c2.getResources().getString(2131886731);
                pq7.b(str, "context.resources.getStr\u2026__TurnOnLocationServices)");
                str2 = str + "  " + format;
            } else if (!m || !m2 || !m3 || !isPowerSaveMode) {
                str = c2.getResources().getString(2131886722);
                pq7.b(str, "context.resources.getStr\u2026Text__GpsWorkoutTracking)");
                str2 = c2.getResources().getString(2131886720);
                pq7.b(str2, "context.resources.getStr\u2026rkoutSessionLocationData)");
            } else {
                str = c2.getResources().getString(2131886732);
                pq7.b(str, "context.resources.getStr\u2026__TurnOffPowerSavingMode)");
                str2 = str + ' ' + format;
            }
            return new cl7<>(str, str2);
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean l() {
        return this.d;
    }

    @DexIgnore
    public final void m() {
        cl7<String, String> k2 = k();
        eo5.c.e(this.j, k2.getFirst(), k2.getSecond());
    }

    @DexIgnore
    public final void n() {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            PortfolioApp.h0.c().registerReceiver(this.i, intentFilter);
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public final void o(mq1 mq1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "resumeWorkoutTracking " + mq1);
        if (mq1 != null) {
            if (mq1.isRequiredGPS()) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "resumeWorkoutTracking gpsRequired");
                i();
            }
            PortfolioApp.h0.c().g1(new ResumeWorkoutInfoData(mq1, "", eu1.SUCCESS), PortfolioApp.h0.c().J());
        }
    }

    @DexIgnore
    public final void p() {
        boolean isObservingLocation = this.k.isObservingLocation(this.c, false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "scheduleStopGpsListener isObserving " + isObservingLocation);
        this.e.removeCallbacksAndMessages(null);
        if (isObservingLocation) {
            this.e.postDelayed(this.f, 3600000);
        }
    }

    @DexIgnore
    public final xw7 q() {
        return gu7.d(this.h, null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final void r(String str, pq1 pq1) {
        boolean z = true;
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "startWorkoutTracking serial " + str + " request " + pq1);
        if (pq1 == null || pq1.isRequiredGPS()) {
            u();
            n();
            boolean m = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_FINE);
            boolean m2 = jn5.b.m(PortfolioApp.h0.c(), jn5.c.LOCATION_SERVICE);
            this.d = true;
            if (m && m2) {
                q();
            }
            m();
            String j2 = j(m, m2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WorkoutTetherGpsManager", "startWorkoutTracking watchErrorContent " + j2);
            if (j2.length() <= 0) {
                z = false;
            }
            if (z) {
                PortfolioApp c2 = PortfolioApp.h0.c();
                if (pq1 != null) {
                    c2.g1(new StartWorkoutInfoData(pq1, j2, eu1.ERROR), str);
                    return;
                }
                return;
            }
            PortfolioApp c3 = PortfolioApp.h0.c();
            if (pq1 != null) {
                c3.g1(new StartWorkoutInfoData(pq1, "", eu1.SUCCESS), str);
                return;
            }
            return;
        }
        PortfolioApp.h0.c().g1(new StartWorkoutInfoData(pq1, "", eu1.SUCCESS), str);
        s();
    }

    @DexIgnore
    public final void s() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopLocationObserver");
        this.f1186a = null;
        eo5.c.d(PortfolioApp.h0.c());
        this.k.unObserverLocation(this.c, false);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopWorkoutTracking");
        this.d = false;
        s();
        u();
    }

    @DexIgnore
    public final void u() {
        try {
            PortfolioApp.h0.c().unregisterReceiver(this.i);
        } catch (Exception e2) {
        }
    }
}
