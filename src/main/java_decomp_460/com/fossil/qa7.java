package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ew5;
import com.fossil.ka7;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa7 extends qv5 implements ew5.a, ka7.a, t47.g {
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public ug5 i;
    @DexIgnore
    public sa7 j;
    @DexIgnore
    public oa7 k;
    @DexIgnore
    public ka7 l;
    @DexIgnore
    public ew5 m;
    @DexIgnore
    public List<String> s; // = new ArrayList();
    @DexIgnore
    public String t;
    @DexIgnore
    public int u;
    @DexIgnore
    public String v; // = "";
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final qa7 a(String str, String str2) {
            pq7.c(str, "watchFaceId");
            pq7.c(str2, "type");
            qa7 qa7 = new qa7();
            qa7.setArguments(nm0.a(hl7.a("WATCH_FACE_ID", str), hl7.a("WATCH_FACE_ID_TYPE", str2)));
            return qa7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qa7 f2951a;

        @DexIgnore
        public b(qa7 qa7) {
            this.f2951a = qa7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            this.f2951a.t = t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qa7 f2952a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements Comparator<T> {
            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return mn7.c(t2.c().getCreatedAt(), t.c().getCreatedAt());
            }
        }

        @DexIgnore
        public c(qa7 qa7) {
            this.f2952a = qa7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            List<ka7.b> b0 = pm7.b0(t, new a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "watchFace changes " + b0);
            if (b0.isEmpty()) {
                this.f2952a.s.clear();
                return;
            }
            ka7 ka7 = this.f2952a.l;
            if (ka7 != null) {
                ka7.k(b0);
            }
        }
    }

    @DexIgnore
    public final void N6(ka7.b bVar) {
        WatchFacePreviewView watchFacePreviewView;
        if (bVar == null) {
            ug5 ug5 = this.i;
            if (ug5 != null && (watchFacePreviewView = ug5.g) != null) {
                watchFacePreviewView.U();
                return;
            }
            return;
        }
        DianaWatchFaceUser c2 = bVar.c();
        if (c2.getPreviewURL().length() > 0) {
            ug5 ug52 = this.i;
            if (ug52 != null) {
                ug52.g.P(c2.getPreviewURL());
            } else {
                pq7.i();
                throw null;
            }
        } else {
            ug5 ug53 = this.i;
            if (ug53 != null) {
                ug53.g.Q(bVar.b(), bVar.a());
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == -466555268 && str.equals("DELETE_MY_FACE") && i2 == 2131363373) {
            String stringExtra = intent != null ? intent.getStringExtra("WATCH_FACE_ID") : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "delete watch face id " + stringExtra);
            oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.o(stringExtra);
            } else {
                pq7.n("mSharedViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ew5.a
    public void a4(int i2) {
        ka7 ka7 = this.l;
        if (ka7 == null) {
            return;
        }
        if (i2 >= 0) {
            ka7.b bVar = (ka7.b) ka7.getCurrentList().get(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "show watch face " + bVar);
            this.t = bVar.c().getId();
            N6(bVar);
            int size = ka7.getCurrentList().size();
            boolean z = true;
            if (i2 != size - 1) {
                z = false;
            }
            oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.t(bVar.c().getId());
                oa7 oa72 = this.k;
                if (oa72 != null) {
                    oa72.u(z);
                } else {
                    pq7.n("mSharedViewModel");
                    throw null;
                }
            } else {
                pq7.n("mSharedViewModel");
                throw null;
            }
        } else {
            N6(null);
        }
    }

    @DexIgnore
    @Override // com.fossil.ka7.a
    public void i6(String str) {
        pq7.c(str, "watchFaceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MyFaceFragment", "onWatchFaceApply id " + str + " currentId " + this.t);
        oa7 oa7 = this.k;
        if (oa7 != null) {
            String str2 = this.t;
            if (str2 == null) {
                str2 = str;
            }
            oa7.n(str2);
            b77.f401a.a(str);
            return;
        }
        pq7.n("mSharedViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ug5 c2 = ug5.c(layoutInflater, viewGroup, false);
        this.i = c2;
        if (c2 != null) {
            return c2.b();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String str;
        String str2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.h0.c().M().q0().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = new ViewModelProvider(this, po4).a(sa7.class);
            pq7.b(a2, "ViewModelProvider(this, \u2026aceViewModel::class.java)");
            this.j = (sa7) a2;
            FragmentActivity requireActivity = requireActivity();
            if (requireActivity != null) {
                this.k = ((WatchFaceListActivity) requireActivity).M();
                this.l = new ka7(this);
                ug5 ug5 = this.i;
                if (ug5 != null) {
                    RecyclerView recyclerView = ug5.e;
                    if (ug5 != null) {
                        pq7.b(recyclerView, "mBinding!!.rvFaces");
                        recyclerView.setAdapter(this.l);
                        ug5 ug52 = this.i;
                        if (ug52 != null) {
                            RecyclerView recyclerView2 = ug52.e;
                            pq7.b(recyclerView2, "mBinding!!.rvFaces");
                            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
                            ew5 ew5 = new ew5(this);
                            this.m = ew5;
                            ug5 ug53 = this.i;
                            if (ug53 != null) {
                                ew5.b(ug53.e);
                                Bundle arguments = getArguments();
                                if (arguments == null || (str = arguments.getString("WATCH_FACE_ID")) == null) {
                                    str = "";
                                }
                                this.v = str;
                                Bundle arguments2 = getArguments();
                                if (arguments2 == null || (str2 = arguments2.getString("WATCH_FACE_ID_TYPE")) == null) {
                                    str2 = "";
                                }
                                this.w = str2;
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                local.d("MyFaceFragment", "onViewCreated watchFaceId " + this.v + " watchFaceIdType " + this.w);
                                oa7 oa7 = this.k;
                                if (oa7 != null) {
                                    LiveData<String> q = oa7.q();
                                    LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                                    pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
                                    q.h(viewLifecycleOwner, new b(this));
                                    sa7 sa7 = this.j;
                                    if (sa7 != null) {
                                        LiveData<List<ka7.b>> A = sa7.A();
                                        LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                                        pq7.b(viewLifecycleOwner2, "viewLifecycleOwner");
                                        A.h(viewLifecycleOwner2, new c(this));
                                        return;
                                    }
                                    pq7.n("mViewModel");
                                    throw null;
                                }
                                pq7.n("mSharedViewModel");
                                throw null;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.faces.WatchFaceListActivity");
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ka7.a
    public void s3(List<ka7.b> list) {
        int i2;
        T t2;
        T t3;
        T t4;
        T t5;
        T t6;
        boolean z = false;
        pq7.c(list, "sortedList");
        FLogger.INSTANCE.getLocal().d("MyFaceFragment", "onCurrentListChanged");
        if (!TextUtils.isEmpty(this.v)) {
            if (pq7.a(this.w, "ORDER_ID")) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t6 = null;
                        break;
                    }
                    T next = it.next();
                    DianaWatchFaceOrder order = next.c().getOrder();
                    if (pq7.a(order != null ? order.getOrderId() : null, this.v)) {
                        t6 = next;
                        break;
                    }
                }
                t4 = t6;
            } else if (pq7.a(this.w, "ITEM_ID")) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t5 = null;
                        break;
                    }
                    T next2 = it2.next();
                    DianaWatchFaceOrder order2 = next2.c().getOrder();
                    if (pq7.a(order2 != null ? order2.getWatchFaceId() : null, this.v)) {
                        t5 = next2;
                        break;
                    }
                }
                t4 = t5;
            } else {
                Iterator<T> it3 = list.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (pq7.a(next3.c().getId(), this.v)) {
                        t3 = next3;
                        break;
                    }
                }
                t4 = t3;
            }
            this.v = "";
            i2 = pm7.K(list, t4);
        } else if (!TextUtils.isEmpty(this.t)) {
            Iterator<T> it4 = list.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    t2 = null;
                    break;
                }
                T next4 = it4.next();
                if (pq7.a(next4.c().getId(), this.t)) {
                    t2 = next4;
                    break;
                }
            }
            i2 = pm7.K(list, t2);
            if (i2 < 0 && (!list.isEmpty())) {
                i2 = pm7.K(this.s, this.t);
                FLogger.INSTANCE.getLocal().d("MyFaceFragment", "deletePosition " + i2 + " sortedList.size " + list.size());
                if (i2 > list.size() - 1) {
                    i2--;
                }
            }
        } else {
            i2 = 0;
        }
        this.u = i2;
        this.s.clear();
        List<String> list2 = this.s;
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it5 = list.iterator();
        while (it5.hasNext()) {
            arrayList.add(it5.next().c().getId());
        }
        list2.addAll(pm7.j0(arrayList));
        FLogger.INSTANCE.getLocal().d("MyFaceFragment", "pos " + this.u + " currentId " + this.t + " mWatchFaceIds " + this.s);
        int i3 = this.u;
        if (i3 >= 0) {
            ka7.b bVar = list.get(i3);
            this.t = bVar.c().getId();
            oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.t(bVar.c().getId());
                FLogger.INSTANCE.getLocal().d("MyFaceFragment", "show watch face " + bVar);
                N6(list.get(this.u));
                if (this.u == list.size() - 1) {
                    z = true;
                }
                oa7 oa72 = this.k;
                if (oa72 != null) {
                    oa72.u(z);
                    FLogger.INSTANCE.getLocal().d("MyFaceFragment", "onCurrentListChanged scrollToPosition to " + this.u);
                    ug5 ug5 = this.i;
                    if (ug5 != null) {
                        ug5.e.scrollToPosition(this.u);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mSharedViewModel");
                    throw null;
                }
            } else {
                pq7.n("mSharedViewModel");
                throw null;
            }
        } else {
            N6(null);
        }
    }

    @DexIgnore
    @Override // com.fossil.ka7.a
    public void u6(String str) {
        pq7.c(str, "watchFaceId");
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.m(childFragmentManager, str);
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
