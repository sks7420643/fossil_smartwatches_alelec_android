package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft7 implements ts7<wr7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CharSequence f1203a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ vp7<CharSequence, Integer, cl7<Integer, Integer>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<wr7>, jr7 {
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public wr7 e;
        @DexIgnore
        public int f;
        @DexIgnore
        public /* final */ /* synthetic */ ft7 g;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(ft7 ft7) {
            this.g = ft7;
            int i = bs7.i(ft7.b, 0, ft7.f1203a.length());
            this.c = i;
            this.d = i;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
            if (r0 < r6.g.c) goto L_0x0023;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
                r6 = this;
                r2 = 1
                r3 = 0
                r5 = -1
                int r0 = r6.d
                if (r0 >= 0) goto L_0x000d
                r6.b = r3
                r0 = 0
                r6.e = r0
            L_0x000c:
                return
            L_0x000d:
                com.fossil.ft7 r0 = r6.g
                int r0 = com.fossil.ft7.c(r0)
                if (r0 <= 0) goto L_0x0023
                int r0 = r6.f
                int r0 = r0 + 1
                r6.f = r0
                com.fossil.ft7 r1 = r6.g
                int r1 = com.fossil.ft7.c(r1)
                if (r0 >= r1) goto L_0x0031
            L_0x0023:
                int r0 = r6.d
                com.fossil.ft7 r1 = r6.g
                java.lang.CharSequence r1 = com.fossil.ft7.b(r1)
                int r1 = r1.length()
                if (r0 <= r1) goto L_0x0049
            L_0x0031:
                com.fossil.wr7 r0 = new com.fossil.wr7
                int r1 = r6.c
                com.fossil.ft7 r3 = r6.g
                java.lang.CharSequence r3 = com.fossil.ft7.b(r3)
                int r3 = com.fossil.wt7.A(r3)
                r0.<init>(r1, r3)
                r6.e = r0
                r6.d = r5
            L_0x0046:
                r6.b = r2
                goto L_0x000c
            L_0x0049:
                com.fossil.ft7 r0 = r6.g
                com.fossil.vp7 r0 = com.fossil.ft7.a(r0)
                com.fossil.ft7 r1 = r6.g
                java.lang.CharSequence r1 = com.fossil.ft7.b(r1)
                int r4 = r6.d
                java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
                java.lang.Object r0 = r0.invoke(r1, r4)
                com.fossil.cl7 r0 = (com.fossil.cl7) r0
                if (r0 != 0) goto L_0x0079
                com.fossil.wr7 r0 = new com.fossil.wr7
                int r1 = r6.c
                com.fossil.ft7 r3 = r6.g
                java.lang.CharSequence r3 = com.fossil.ft7.b(r3)
                int r3 = com.fossil.wt7.A(r3)
                r0.<init>(r1, r3)
                r6.e = r0
                r6.d = r5
                goto L_0x0046
            L_0x0079:
                java.lang.Object r1 = r0.component1()
                java.lang.Number r1 = (java.lang.Number) r1
                int r1 = r1.intValue()
                java.lang.Object r0 = r0.component2()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.c
                com.fossil.wr7 r4 = com.fossil.bs7.m(r4, r1)
                r6.e = r4
                int r1 = r1 + r0
                r6.c = r1
                if (r0 != 0) goto L_0x009f
                r0 = r2
            L_0x009b:
                int r0 = r0 + r1
                r6.d = r0
                goto L_0x0046
            L_0x009f:
                r0 = r3
                goto L_0x009b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ft7.a.a():void");
        }

        @DexIgnore
        /* renamed from: b */
        public wr7 next() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                wr7 wr7 = this.e;
                if (wr7 != null) {
                    this.e = null;
                    this.b = -1;
                    return wr7;
                }
                throw new il7("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.vp7<? super java.lang.CharSequence, ? super java.lang.Integer, com.fossil.cl7<java.lang.Integer, java.lang.Integer>> */
    /* JADX WARN: Multi-variable type inference failed */
    public ft7(CharSequence charSequence, int i, int i2, vp7<? super CharSequence, ? super Integer, cl7<Integer, Integer>> vp7) {
        pq7.c(charSequence, "input");
        pq7.c(vp7, "getNextMatch");
        this.f1203a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = vp7;
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<wr7> iterator() {
        return new a(this);
    }
}
