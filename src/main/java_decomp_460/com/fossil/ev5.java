package com.fossil;

import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ vn5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ev5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<ls5> f991a;

        @DexIgnore
        public b(WeakReference<ls5> weakReference) {
            pq7.c(weakReference, "activityContext");
            this.f991a = weakReference;
        }

        @DexIgnore
        public final WeakReference<ls5> a() {
            return this.f991a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f992a;
        @DexIgnore
        public /* final */ z52 b;

        @DexIgnore
        public c(int i, z52 z52) {
            this.f992a = i;
            this.b = z52;
        }

        @DexIgnore
        public final int a() {
            return this.f992a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SignUpSocialAuth f993a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            this.f993a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.f993a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements yn5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ev5 f994a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(ev5 ev5) {
            this.f994a = ev5;
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ev5.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.f994a.j(new d(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void b(int i, z52 z52, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ev5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + z52);
            this.f994a.i(new c(i, z52));
        }
    }

    /*
    static {
        String simpleName = ev5.class.getSimpleName();
        pq7.b(simpleName, "LoginGoogleUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ev5(vn5 vn5) {
        pq7.c(vn5, "mLoginGoogleManager");
        this.d = vn5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* renamed from: n */
    public Object k(b bVar, qn7<Object> qn7) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.h0.c().p0()) {
                return new c(601, null);
            }
            vn5 vn5 = this.d;
            WeakReference<ls5> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                vn5.g(a2, new e(this));
                return tl7.f3441a;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, null);
        }
    }
}
