package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qj7 {
    @DexIgnore
    @SafeVarargs
    public static <T> List<T> a(List<T>... listArr) {
        if (listArr == null || listArr.length == 0) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = new CopyOnWriteArrayList(listArr).iterator();
        while (it.hasNext()) {
            List list = (List) it.next();
            if (g(list)) {
                Iterator it2 = new CopyOnWriteArrayList(list).iterator();
                while (it2.hasNext()) {
                    arrayList.add(it2.next());
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static <T> List<T> b(List<T> list) {
        if (list == null) {
            return new ArrayList();
        }
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList(list);
        ArrayList arrayList = new ArrayList(copyOnWriteArrayList.size());
        Iterator it = copyOnWriteArrayList.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public static <K, V> Map<K, V> c(Map<K, V> map) {
        if (map == null) {
            return new HashMap();
        }
        Map<? extends K, ? extends V> synchronizedMap = Collections.synchronizedMap(map);
        HashMap hashMap = new HashMap();
        hashMap.putAll(synchronizedMap);
        return hashMap;
    }

    @DexIgnore
    public static <T> List<T> d(List<T> list) {
        return e(list) ? new ArrayList() : list;
    }

    @DexIgnore
    public static <T> boolean e(Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }

    @DexIgnore
    public static <T> boolean f(T[] tArr) {
        return tArr == null || tArr.length == 0;
    }

    @DexIgnore
    public static <T> boolean g(Collection<T> collection) {
        return !e(collection);
    }

    @DexIgnore
    public static <T> boolean h(T[] tArr) {
        return !f(tArr);
    }

    @DexIgnore
    public static <T> List<T> i(List<T> list) {
        return Collections.unmodifiableList(d(list));
    }
}
