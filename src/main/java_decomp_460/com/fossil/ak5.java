package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ak5 implements vi4 {
    @DexIgnore
    @Override // com.fossil.vi4
    public boolean a(wi4 wi4) {
        pq7.c(wi4, "f");
        return wi4.a(zj5.class) != null;
    }

    @DexIgnore
    @Override // com.fossil.vi4
    public boolean b(Class<?> cls) {
        pq7.c(cls, "clazz");
        return false;
    }
}
