package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dx3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f846a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;

    @DexIgnore
    public dx3(int i, float f, float f2) {
        this.f846a = i;
        this.b = f;
        this.c = f2;
    }
}
