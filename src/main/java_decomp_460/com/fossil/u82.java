package com.fossil;

import android.os.Bundle;
import com.fossil.m62;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u82 implements d92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c92 f3523a;

    @DexIgnore
    public u82(c92 c92) {
        this.f3523a = c92;
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void b() {
        this.f3523a.r();
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void f() {
        for (m62.f fVar : this.f3523a.g.values()) {
            fVar.a();
        }
        this.f3523a.t.q = Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void i(z52 z52, m62<?> m62, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final <A extends m62.b, T extends i72<? extends z62, A>> T j(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t) {
        this.f3523a.t.i.add(t);
        return t;
    }
}
