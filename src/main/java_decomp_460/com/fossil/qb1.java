package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qb1<T, Z> {
    @DexIgnore
    boolean a(T t, ob1 ob1) throws IOException;

    @DexIgnore
    id1<Z> b(T t, int i, int i2, ob1 ob1) throws IOException;
}
