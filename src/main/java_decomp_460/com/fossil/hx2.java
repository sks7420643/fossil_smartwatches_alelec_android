package com.fossil;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx2<K, V> extends AbstractMap<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    @NullableDecl
    public transient Object b;
    @DexIgnore
    public transient int c; // = fz2.a(3, 1, 1073741823);
    @DexIgnore
    public transient int d;
    @DexIgnore
    @NullableDecl
    public transient Set<K> e;
    @DexIgnore
    @NullableDecl
    public transient Set<Map.Entry<K, V>> f;
    @DexIgnore
    @NullableDecl
    public transient Collection<V> g;
    @DexIgnore
    @NullableDecl
    public transient int[] zza;
    @DexIgnore
    @NullableDecl
    public transient Object[] zzb;
    @DexIgnore
    @NullableDecl
    public transient Object[] zzc;

    @DexIgnore
    public hx2() {
        sw2.f(true, "Expected size must be >= 0");
    }

    @DexIgnore
    public static int zzb(int i, int i2) {
        return i - 1;
    }

    @DexIgnore
    public static /* synthetic */ int zzd(hx2 hx2) {
        int i = hx2.d;
        hx2.d = i - 1;
        return i;
    }

    @DexIgnore
    public final int a(int i, int i2, int i3, int i4) {
        Object d2 = ox2.d(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            ox2.e(d2, i3 & i5, i4 + 1);
        }
        Object obj = this.b;
        int[] iArr = this.zza;
        for (int i6 = 0; i6 <= i; i6++) {
            int b2 = ox2.b(obj, i6);
            while (b2 != 0) {
                int i7 = b2 - 1;
                int i8 = iArr[i7];
                int i9 = (i & i8) | i6;
                int i10 = i9 & i5;
                int b3 = ox2.b(d2, i10);
                ox2.e(d2, i10, b2);
                iArr[i7] = ox2.a(i9, b3, i5);
                b2 = i8 & i;
            }
        }
        this.b = d2;
        e(i5);
        return i5;
    }

    @DexIgnore
    public final int b(@NullableDecl Object obj) {
        if (zza()) {
            return -1;
        }
        int b2 = qx2.b(obj);
        int f2 = f();
        int b3 = ox2.b(this.b, b2 & f2);
        if (b3 == 0) {
            return -1;
        }
        do {
            int i = b3 - 1;
            int i2 = this.zza[i];
            if ((i2 & f2) == (b2 & f2) && qw2.a(obj, this.zzb[i])) {
                return i;
            }
            b3 = i2 & f2;
        } while (b3 != 0);
        return -1;
    }

    @DexIgnore
    @NullableDecl
    public final Object c(@NullableDecl Object obj) {
        if (zza()) {
            return h;
        }
        int f2 = f();
        int c2 = ox2.c(obj, null, f2, this.b, this.zza, this.zzb, null);
        if (c2 == -1) {
            return h;
        }
        Object obj2 = this.zzc[c2];
        zza(c2, f2);
        this.d--;
        zzc();
        return obj2;
    }

    @DexIgnore
    public final void clear() {
        if (!zza()) {
            zzc();
            Map<K, V> zzb2 = zzb();
            if (zzb2 != null) {
                this.c = fz2.a(size(), 3, 1073741823);
                zzb2.clear();
                this.b = null;
                this.d = 0;
                return;
            }
            Arrays.fill(this.zzb, 0, this.d, (Object) null);
            Arrays.fill(this.zzc, 0, this.d, (Object) null);
            Object obj = this.b;
            if (obj instanceof byte[]) {
                Arrays.fill((byte[]) obj, (byte) 0);
            } else if (obj instanceof short[]) {
                Arrays.fill((short[]) obj, (short) 0);
            } else {
                Arrays.fill((int[]) obj, 0);
            }
            Arrays.fill(this.zza, 0, this.d, 0);
            this.d = 0;
        }
    }

    @DexIgnore
    public final boolean containsKey(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.containsKey(obj) : b(obj) != -1;
    }

    @DexIgnore
    public final boolean containsValue(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.containsValue(obj);
        }
        for (int i = 0; i < this.d; i++) {
            if (qw2.a(obj, this.zzc[i])) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void e(int i) {
        this.c = ox2.a(this.c, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.f;
        if (set != null) {
            return set;
        }
        lx2 lx2 = new lx2(this);
        this.f = lx2;
        return lx2;
    }

    @DexIgnore
    public final int f() {
        return (1 << (this.c & 31)) - 1;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final V get(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.get(obj);
        }
        int b2 = b(obj);
        if (b2 == -1) {
            return null;
        }
        return (V) this.zzc[b2];
    }

    @DexIgnore
    public final boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Set<K> keySet() {
        Set<K> set = this.e;
        if (set != null) {
            return set;
        }
        nx2 nx2 = new nx2(this);
        this.e = nx2;
        return nx2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v6, resolved type: java.util.LinkedHashMap */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    @NullableDecl
    public final V put(@NullableDecl K k, @NullableDecl V v) {
        int i;
        int i2;
        int i3;
        int min;
        if (zza()) {
            sw2.h(zza(), "Arrays already allocated");
            int i4 = this.c;
            int max = Math.max(i4 + 1, 2);
            int highestOneBit = Integer.highestOneBit(max);
            if (max > ((int) (((double) highestOneBit) * 1.0d)) && (highestOneBit = highestOneBit << 1) <= 0) {
                highestOneBit = 1073741824;
            }
            int max2 = Math.max(4, highestOneBit);
            this.b = ox2.d(max2);
            e(max2 - 1);
            this.zza = new int[i4];
            this.zzb = new Object[i4];
            this.zzc = new Object[i4];
        }
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.put(k, v);
        }
        int[] iArr = this.zza;
        Object[] objArr = this.zzb;
        Object[] objArr2 = this.zzc;
        int i5 = this.d;
        int i6 = i5 + 1;
        int b2 = qx2.b(k);
        int f2 = f();
        int i7 = b2 & f2;
        int b3 = ox2.b(this.b, i7);
        if (b3 != 0) {
            int i8 = 0;
            do {
                i = b3 - 1;
                i2 = iArr[i];
                if ((i2 & f2) != (b2 & f2) || !qw2.a(k, objArr[i])) {
                    b3 = i2 & f2;
                    i8++;
                } else {
                    V v2 = (V) objArr2[i];
                    objArr2[i] = v;
                    return v2;
                }
            } while (b3 != 0);
            if (i8 >= 9) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(f() + 1, 1.0f);
                int zzd = zzd();
                while (zzd >= 0) {
                    linkedHashMap.put(this.zzb[zzd], this.zzc[zzd]);
                    zzd = zza(zzd);
                }
                this.b = linkedHashMap;
                this.zza = null;
                this.zzb = null;
                this.zzc = null;
                zzc();
                return (V) linkedHashMap.put(k, v);
            } else if (i6 > f2) {
                i3 = a(f2, ox2.f(f2), b2, i5);
            } else {
                iArr[i] = ox2.a(i2, i6, f2);
                i3 = f2;
            }
        } else if (i6 > f2) {
            i3 = a(f2, ox2.f(f2), b2, i5);
        } else {
            ox2.e(this.b, i7, i6);
            i3 = f2;
        }
        int length = this.zza.length;
        if (i6 > length && (min = Math.min(1073741823, (Math.max(1, length >>> 1) + length) | 1)) != length) {
            this.zza = Arrays.copyOf(this.zza, min);
            this.zzb = Arrays.copyOf(this.zzb, min);
            this.zzc = Arrays.copyOf(this.zzc, min);
        }
        this.zza[i5] = ox2.a(b2, 0, i3);
        this.zzb[i5] = k;
        this.zzc[i5] = v;
        this.d = i6;
        zzc();
        return null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @NullableDecl
    public final V remove(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.remove(obj);
        }
        V v = (V) c(obj);
        if (v == h) {
            return null;
        }
        return v;
    }

    @DexIgnore
    public final int size() {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.size() : this.d;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Collection<V> values() {
        Collection<V> collection = this.g;
        if (collection != null) {
            return collection;
        }
        px2 px2 = new px2(this);
        this.g = px2;
        return px2;
    }

    @DexIgnore
    public final int zza(int i) {
        int i2 = i + 1;
        if (i2 < this.d) {
            return i2;
        }
        return -1;
    }

    @DexIgnore
    public final void zza(int i, int i2) {
        int i3;
        int[] iArr;
        int i4;
        int size = size() - 1;
        if (i < size) {
            Object[] objArr = this.zzb;
            Object obj = objArr[size];
            objArr[i] = obj;
            Object[] objArr2 = this.zzc;
            objArr2[i] = objArr2[size];
            objArr[size] = null;
            objArr2[size] = null;
            int[] iArr2 = this.zza;
            iArr2[i] = iArr2[size];
            iArr2[size] = 0;
            int b2 = qx2.b(obj) & i2;
            int b3 = ox2.b(this.b, b2);
            int i5 = size + 1;
            if (b3 == i5) {
                ox2.e(this.b, b2, i + 1);
                return;
            }
            do {
                i3 = b3 - 1;
                iArr = this.zza;
                i4 = iArr[i3];
                b3 = i4 & i2;
            } while (b3 != i5);
            iArr[i3] = ox2.a(i4, i + 1, i2);
            return;
        }
        this.zzb[i] = null;
        this.zzc[i] = null;
        this.zza[i] = 0;
    }

    @DexIgnore
    public final boolean zza() {
        return this.b == null;
    }

    @DexIgnore
    @NullableDecl
    public final Map<K, V> zzb() {
        Object obj = this.b;
        if (obj instanceof Map) {
            return (Map) obj;
        }
        return null;
    }

    @DexIgnore
    public final void zzc() {
        this.c += 32;
    }

    @DexIgnore
    public final int zzd() {
        return isEmpty() ? -1 : 0;
    }

    @DexIgnore
    public final Iterator<K> zze() {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.keySet().iterator() : new gx2(this);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> zzf() {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.entrySet().iterator() : new jx2(this);
    }

    @DexIgnore
    public final Iterator<V> zzg() {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.values().iterator() : new ix2(this);
    }
}
