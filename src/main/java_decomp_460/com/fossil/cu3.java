package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ bu3 c;

    @DexIgnore
    public cu3(bu3 bu3, nt3 nt3) {
        this.c = bu3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (bu3.b(this.c)) {
            if (bu3.c(this.c) != null) {
                bu3.c(this.c).onFailure(this.b.l());
            }
        }
    }
}
