package com.fossil;

import com.fossil.g02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz1 extends g02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ h02 f4021a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ uy1<?> c;
    @DexIgnore
    public /* final */ wy1<?, byte[]> d;
    @DexIgnore
    public /* final */ ty1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends g02.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public h02 f4022a;
        @DexIgnore
        public String b;
        @DexIgnore
        public uy1<?> c;
        @DexIgnore
        public wy1<?, byte[]> d;
        @DexIgnore
        public ty1 e;

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02 a() {
            String str = "";
            if (this.f4022a == null) {
                str = " transportContext";
            }
            if (this.b == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " event";
            }
            if (this.d == null) {
                str = str + " transformer";
            }
            if (this.e == null) {
                str = str + " encoding";
            }
            if (str.isEmpty()) {
                return new wz1(this.f4022a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02.a b(ty1 ty1) {
            if (ty1 != null) {
                this.e = ty1;
                return this;
            }
            throw new NullPointerException("Null encoding");
        }

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02.a c(uy1<?> uy1) {
            if (uy1 != null) {
                this.c = uy1;
                return this;
            }
            throw new NullPointerException("Null event");
        }

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02.a d(wy1<?, byte[]> wy1) {
            if (wy1 != null) {
                this.d = wy1;
                return this;
            }
            throw new NullPointerException("Null transformer");
        }

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02.a e(h02 h02) {
            if (h02 != null) {
                this.f4022a = h02;
                return this;
            }
            throw new NullPointerException("Null transportContext");
        }

        @DexIgnore
        @Override // com.fossil.g02.a
        public g02.a f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }
    }

    @DexIgnore
    public wz1(h02 h02, String str, uy1<?> uy1, wy1<?, byte[]> wy1, ty1 ty1) {
        this.f4021a = h02;
        this.b = str;
        this.c = uy1;
        this.d = wy1;
        this.e = ty1;
    }

    @DexIgnore
    @Override // com.fossil.g02
    public ty1 b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.g02
    public uy1<?> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.g02
    public wy1<?, byte[]> e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g02)) {
            return false;
        }
        g02 g02 = (g02) obj;
        return this.f4021a.equals(g02.f()) && this.b.equals(g02.g()) && this.c.equals(g02.c()) && this.d.equals(g02.e()) && this.e.equals(g02.b());
    }

    @DexIgnore
    @Override // com.fossil.g02
    public h02 f() {
        return this.f4021a;
    }

    @DexIgnore
    @Override // com.fossil.g02
    public String g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((this.f4021a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SendRequest{transportContext=" + this.f4021a + ", transportName=" + this.b + ", event=" + this.c + ", transformer=" + this.d + ", encoding=" + this.e + "}";
    }
}
