package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, String> f3435a; // = new HashMap();
    @DexIgnore
    public /* final */ ck5 b;

    @DexIgnore
    public tl5(ck5 ck5) {
        pq7.c(ck5, "mAnalyticsInstance");
        this.b = ck5;
    }

    @DexIgnore
    public final tl5 a(String str, String str2) {
        pq7.c(str, "propertyName");
        pq7.c(str2, "propertyValue");
        this.f3435a.put(str, str2);
        return this;
    }

    @DexIgnore
    public final void b() {
        this.b.r(this.f3435a);
    }
}
