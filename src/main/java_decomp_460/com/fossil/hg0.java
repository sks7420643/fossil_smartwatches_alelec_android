package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.fossil.ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hg0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1475a;
    @DexIgnore
    public /* final */ cg0 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public View f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public ig0.a i;
    @DexIgnore
    public gg0 j;
    @DexIgnore
    public PopupWindow.OnDismissListener k;
    @DexIgnore
    public /* final */ PopupWindow.OnDismissListener l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements PopupWindow.OnDismissListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onDismiss() {
            hg0.this.e();
        }
    }

    @DexIgnore
    public hg0(Context context, cg0 cg0, View view, boolean z, int i2) {
        this(context, cg0, view, z, i2, 0);
    }

    @DexIgnore
    public hg0(Context context, cg0 cg0, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new a();
        this.f1475a = context;
        this.b = cg0;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public final gg0 a() {
        Display defaultDisplay = ((WindowManager) this.f1475a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        gg0 zf0 = Math.min(point.x, point.y) >= this.f1475a.getResources().getDimensionPixelSize(oe0.abc_cascading_menus_min_smallest_width) ? new zf0(this.f1475a, this.f, this.d, this.e, this.c) : new mg0(this.f1475a, this.b, this.f, this.d, this.e, this.c);
        zf0.m(this.b);
        zf0.v(this.l);
        zf0.q(this.f);
        zf0.g(this.i);
        zf0.s(this.h);
        zf0.t(this.g);
        return zf0;
    }

    @DexIgnore
    public void b() {
        if (d()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    public gg0 c() {
        if (this.j == null) {
            this.j = a();
        }
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        gg0 gg0 = this.j;
        return gg0 != null && gg0.a();
    }

    @DexIgnore
    public void e() {
        this.j = null;
        PopupWindow.OnDismissListener onDismissListener = this.k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public void f(View view) {
        this.f = view;
    }

    @DexIgnore
    public void g(boolean z) {
        this.h = z;
        gg0 gg0 = this.j;
        if (gg0 != null) {
            gg0.s(z);
        }
    }

    @DexIgnore
    public void h(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void i(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    @DexIgnore
    public void j(ig0.a aVar) {
        this.i = aVar;
        gg0 gg0 = this.j;
        if (gg0 != null) {
            gg0.g(aVar);
        }
    }

    @DexIgnore
    public void k() {
        if (!m()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    public final void l(int i2, int i3, boolean z, boolean z2) {
        gg0 c2 = c();
        c2.w(z2);
        if (z) {
            if ((wn0.b(this.g, mo0.z(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            c2.u(i2);
            c2.x(i3);
            int i4 = (int) ((this.f1475a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c2.r(new Rect(i2 - i4, i3 - i4, i2 + i4, i4 + i3));
        }
        c2.show();
    }

    @DexIgnore
    public boolean m() {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        l(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public boolean n(int i2, int i3) {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        l(i2, i3, true, true);
        return true;
    }
}
