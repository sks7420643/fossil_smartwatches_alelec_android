package com.fossil;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h04 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i04[] f1405a; // = new i04[4];
    @DexIgnore
    public /* final */ Matrix[] b; // = new Matrix[4];
    @DexIgnore
    public /* final */ Matrix[] c; // = new Matrix[4];
    @DexIgnore
    public /* final */ PointF d; // = new PointF();
    @DexIgnore
    public /* final */ i04 e; // = new i04();
    @DexIgnore
    public /* final */ float[] f; // = new float[2];
    @DexIgnore
    public /* final */ float[] g; // = new float[2];

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(i04 i04, Matrix matrix, int i);

        @DexIgnore
        void b(i04 i04, Matrix matrix, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ g04 f1406a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ RectF c;
        @DexIgnore
        public /* final */ a d;
        @DexIgnore
        public /* final */ float e;

        @DexIgnore
        public b(g04 g04, float f, RectF rectF, a aVar, Path path) {
            this.d = aVar;
            this.f1406a = g04;
            this.e = f;
            this.c = rectF;
            this.b = path;
        }
    }

    @DexIgnore
    public h04() {
        for (int i = 0; i < 4; i++) {
            this.f1405a[i] = new i04();
            this.b[i] = new Matrix();
            this.c[i] = new Matrix();
        }
    }

    @DexIgnore
    public final float a(int i) {
        return (float) ((i + 1) * 90);
    }

    @DexIgnore
    public final void b(b bVar, int i) {
        this.f[0] = this.f1405a[i].j();
        this.f[1] = this.f1405a[i].k();
        this.b[i].mapPoints(this.f);
        if (i == 0) {
            Path path = bVar.b;
            float[] fArr = this.f;
            path.moveTo(fArr[0], fArr[1]);
        } else {
            Path path2 = bVar.b;
            float[] fArr2 = this.f;
            path2.lineTo(fArr2[0], fArr2[1]);
        }
        this.f1405a[i].d(this.b[i], bVar.b);
        a aVar = bVar.d;
        if (aVar != null) {
            aVar.a(this.f1405a[i], this.b[i], i);
        }
    }

    @DexIgnore
    public final void c(b bVar, int i) {
        int i2 = (i + 1) % 4;
        this.f[0] = this.f1405a[i].h();
        this.f[1] = this.f1405a[i].i();
        this.b[i].mapPoints(this.f);
        this.g[0] = this.f1405a[i2].j();
        this.g[1] = this.f1405a[i2].k();
        this.b[i2].mapPoints(this.g);
        float[] fArr = this.f;
        float f2 = fArr[0];
        float[] fArr2 = this.g;
        float max = Math.max(((float) Math.hypot((double) (f2 - fArr2[0]), (double) (fArr[1] - fArr2[1]))) - 0.001f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float i3 = i(bVar.c, i);
        this.e.m(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        j(i, bVar.f1406a).d(max, i3, bVar.e, this.e);
        this.e.d(this.c[i], bVar.b);
        a aVar = bVar.d;
        if (aVar != null) {
            aVar.b(this.e, this.c[i], i);
        }
    }

    @DexIgnore
    public void d(g04 g04, float f2, RectF rectF, Path path) {
        e(g04, f2, rectF, null, path);
    }

    @DexIgnore
    public void e(g04 g04, float f2, RectF rectF, a aVar, Path path) {
        path.rewind();
        b bVar = new b(g04, f2, rectF, aVar, path);
        for (int i = 0; i < 4; i++) {
            k(bVar, i);
            l(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            b(bVar, i2);
            c(bVar, i2);
        }
        path.close();
    }

    @DexIgnore
    public final void f(int i, RectF rectF, PointF pointF) {
        if (i == 1) {
            pointF.set(rectF.right, rectF.bottom);
        } else if (i == 2) {
            pointF.set(rectF.left, rectF.bottom);
        } else if (i != 3) {
            pointF.set(rectF.right, rectF.top);
        } else {
            pointF.set(rectF.left, rectF.top);
        }
    }

    @DexIgnore
    public final yz3 g(int i, g04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.t() : g04.r() : g04.j() : g04.l();
    }

    @DexIgnore
    public final zz3 h(int i, g04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.s() : g04.q() : g04.i() : g04.k();
    }

    @DexIgnore
    public final float i(RectF rectF, int i) {
        float[] fArr = this.f;
        i04[] i04Arr = this.f1405a;
        fArr[0] = i04Arr[i].c;
        fArr[1] = i04Arr[i].d;
        this.b[i].mapPoints(fArr);
        return (i == 1 || i == 3) ? Math.abs(rectF.centerX() - this.f[0]) : Math.abs(rectF.centerY() - this.f[1]);
    }

    @DexIgnore
    public final b04 j(int i, g04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.o() : g04.p() : g04.n() : g04.h();
    }

    @DexIgnore
    public final void k(b bVar, int i) {
        h(i, bVar.f1406a).b(this.f1405a[i], 90.0f, bVar.e, bVar.c, g(i, bVar.f1406a));
        float a2 = a(i);
        this.b[i].reset();
        f(i, bVar.c, this.d);
        Matrix matrix = this.b[i];
        PointF pointF = this.d;
        matrix.setTranslate(pointF.x, pointF.y);
        this.b[i].preRotate(a2);
    }

    @DexIgnore
    public final void l(int i) {
        this.f[0] = this.f1405a[i].h();
        this.f[1] = this.f1405a[i].i();
        this.b[i].mapPoints(this.f);
        float a2 = a(i);
        this.c[i].reset();
        Matrix matrix = this.c[i];
        float[] fArr = this.f;
        matrix.setTranslate(fArr[0], fArr[1]);
        this.c[i].preRotate(a2);
    }
}
