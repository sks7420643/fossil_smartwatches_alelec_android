package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl7 implements Comparable<nl7> {
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public /* synthetic */ nl7(long j) {
        this.b = j;
    }

    @DexIgnore
    public static final /* synthetic */ nl7 a(long j) {
        return new nl7(j);
    }

    @DexIgnore
    public static int c(long j, long j2) {
        return ul7.b(j, j2);
    }

    @DexIgnore
    public static long e(long j) {
        return j;
    }

    @DexIgnore
    public static boolean f(long j, Object obj) {
        return (obj instanceof nl7) && j == ((nl7) obj).j();
    }

    @DexIgnore
    public static int h(long j) {
        return (int) ((j >>> 32) ^ j);
    }

    @DexIgnore
    public static String i(long j) {
        return ul7.c(j);
    }

    @DexIgnore
    public final int b(long j) {
        return c(this.b, j);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(nl7 nl7) {
        return b(nl7.j());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return f(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public final /* synthetic */ long j() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return i(this.b);
    }
}
