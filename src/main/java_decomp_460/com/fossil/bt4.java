package com.fossil;

import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt4 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f505a;
    @DexIgnore
    @rj4("challengeType")
    public String b;
    @DexIgnore
    @rj4("name")
    public String c;
    @DexIgnore
    @rj4("description")
    public String d;
    @DexIgnore
    @rj4("owner")
    public ht4 e;
    @DexIgnore
    @rj4("numberOfPlayers")
    public Integer f;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_START_TIME)
    public Date g;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_END_TIME)
    public Date h;
    @DexIgnore
    @rj4("target")
    public Integer i;
    @DexIgnore
    @rj4("duration")
    public Integer j;
    @DexIgnore
    @rj4(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String k;
    @DexIgnore
    @rj4("version")
    public String l;
    @DexIgnore
    @rj4("status")
    public String m;
    @DexIgnore
    @rj4("players")
    public List<ms4> n;
    @DexIgnore
    public ms4 o;
    @DexIgnore
    public ms4 p;
    @DexIgnore
    @rj4("createdAt")
    public Date q;
    @DexIgnore
    @rj4("updatedAt")
    public Date r;
    @DexIgnore
    @rj4("completedTime")
    public Date s;
    @DexIgnore
    public boolean t;

    @DexIgnore
    public bt4(String str, String str2, String str3, String str4, ht4 ht4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, List<ms4> list, ms4 ms4, ms4 ms42, Date date3, Date date4, Date date5, boolean z) {
        pq7.c(str, "id");
        pq7.c(list, "players");
        this.f505a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = ht4;
        this.f = num;
        this.g = date;
        this.h = date2;
        this.i = num2;
        this.j = num3;
        this.k = str5;
        this.l = str6;
        this.m = str7;
        this.n = list;
        this.o = ms4;
        this.p = ms42;
        this.q = date3;
        this.r = date4;
        this.s = date5;
        this.t = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bt4(String str, String str2, String str3, String str4, ht4 ht4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, List list, ms4 ms4, ms4 ms42, Date date3, Date date4, Date date5, boolean z, int i2, kq7 kq7) {
        this(str, str2, str3, str4, ht4, num, date, date2, num2, num3, str5, str6, str7, list, ms4, ms42, date3, date4, date5, (524288 & i2) != 0 ? false : z);
    }

    @DexIgnore
    public final Date a() {
        return this.s;
    }

    @DexIgnore
    public final Date b() {
        return this.q;
    }

    @DexIgnore
    public final ms4 c() {
        return this.p;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final Integer e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof bt4) {
                bt4 bt4 = (bt4) obj;
                if (!pq7.a(this.f505a, bt4.f505a) || !pq7.a(this.b, bt4.b) || !pq7.a(this.c, bt4.c) || !pq7.a(this.d, bt4.d) || !pq7.a(this.e, bt4.e) || !pq7.a(this.f, bt4.f) || !pq7.a(this.g, bt4.g) || !pq7.a(this.h, bt4.h) || !pq7.a(this.i, bt4.i) || !pq7.a(this.j, bt4.j) || !pq7.a(this.k, bt4.k) || !pq7.a(this.l, bt4.l) || !pq7.a(this.m, bt4.m) || !pq7.a(this.n, bt4.n) || !pq7.a(this.o, bt4.o) || !pq7.a(this.p, bt4.p) || !pq7.a(this.q, bt4.q) || !pq7.a(this.r, bt4.r) || !pq7.a(this.s, bt4.s) || this.t != bt4.t) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Date f() {
        return this.h;
    }

    @DexIgnore
    public final String g() {
        return this.f505a;
    }

    @DexIgnore
    public final String h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.f505a;
        int i2 = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        ht4 ht4 = this.e;
        int hashCode5 = ht4 != null ? ht4.hashCode() : 0;
        Integer num = this.f;
        int hashCode6 = num != null ? num.hashCode() : 0;
        Date date = this.g;
        int hashCode7 = date != null ? date.hashCode() : 0;
        Date date2 = this.h;
        int hashCode8 = date2 != null ? date2.hashCode() : 0;
        Integer num2 = this.i;
        int hashCode9 = num2 != null ? num2.hashCode() : 0;
        Integer num3 = this.j;
        int hashCode10 = num3 != null ? num3.hashCode() : 0;
        String str5 = this.k;
        int hashCode11 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.l;
        int hashCode12 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.m;
        int hashCode13 = str7 != null ? str7.hashCode() : 0;
        List<ms4> list = this.n;
        int hashCode14 = list != null ? list.hashCode() : 0;
        ms4 ms4 = this.o;
        int hashCode15 = ms4 != null ? ms4.hashCode() : 0;
        ms4 ms42 = this.p;
        int hashCode16 = ms42 != null ? ms42.hashCode() : 0;
        Date date3 = this.q;
        int hashCode17 = date3 != null ? date3.hashCode() : 0;
        Date date4 = this.r;
        int hashCode18 = date4 != null ? date4.hashCode() : 0;
        Date date5 = this.s;
        if (date5 != null) {
            i2 = date5.hashCode();
        }
        int i3 = this.t ? 1 : 0;
        if (i3 != 0) {
            i3 = 1;
        }
        return (((((((((((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + hashCode17) * 31) + hashCode18) * 31) + i2) * 31) + i3;
    }

    @DexIgnore
    public final Integer i() {
        return this.f;
    }

    @DexIgnore
    public final ht4 j() {
        return this.e;
    }

    @DexIgnore
    public final List<ms4> k() {
        return this.n;
    }

    @DexIgnore
    public final String l() {
        return this.k;
    }

    @DexIgnore
    public final Date m() {
        return this.g;
    }

    @DexIgnore
    public final String n() {
        return this.m;
    }

    @DexIgnore
    public final Integer o() {
        return this.i;
    }

    @DexIgnore
    public final ms4 p() {
        return this.o;
    }

    @DexIgnore
    public final String q() {
        return this.b;
    }

    @DexIgnore
    public final Date r() {
        return this.r;
    }

    @DexIgnore
    public final String s() {
        return this.l;
    }

    @DexIgnore
    public final boolean t() {
        return this.t;
    }

    @DexIgnore
    public String toString() {
        return "HistoryChallenge(id=" + this.f505a + ", type=" + this.b + ", name=" + this.c + ", des=" + this.d + ", owner=" + this.e + ", numberOfPlayers=" + this.f + ", startTime=" + this.g + ", endTime=" + this.h + ", target=" + this.i + ", duration=" + this.j + ", privacy=" + this.k + ", version=" + this.l + ", status=" + this.m + ", players=" + this.n + ", topPlayer=" + this.o + ", currentPlayer=" + this.p + ", createdAt=" + this.q + ", updatedAt=" + this.r + ", completedAt=" + this.s + ", isFirst=" + this.t + ")";
    }

    @DexIgnore
    public final void u(List<ms4> list) {
        pq7.c(list, "<set-?>");
        this.n = list;
    }
}
