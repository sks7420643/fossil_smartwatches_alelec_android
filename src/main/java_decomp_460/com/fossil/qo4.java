package com.fossil;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo4 implements Factory<po4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Map<Class<? extends ts0>, Provider<ts0>>> f3003a;

    @DexIgnore
    public qo4(Provider<Map<Class<? extends ts0>, Provider<ts0>>> provider) {
        this.f3003a = provider;
    }

    @DexIgnore
    public static qo4 a(Provider<Map<Class<? extends ts0>, Provider<ts0>>> provider) {
        return new qo4(provider);
    }

    @DexIgnore
    public static po4 c(Map<Class<? extends ts0>, Provider<ts0>> map) {
        return new po4(map);
    }

    @DexIgnore
    /* renamed from: b */
    public po4 get() {
        return c(this.f3003a.get());
    }
}
