package com.fossil;

import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp4 implements Factory<SecureApiService2Dot1> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3450a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public tp4(uo4 uo4, Provider<on5> provider) {
        this.f3450a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static tp4 a(uo4 uo4, Provider<on5> provider) {
        return new tp4(uo4, provider);
    }

    @DexIgnore
    public static SecureApiService2Dot1 c(uo4 uo4, on5 on5) {
        SecureApiService2Dot1 A = uo4.A(on5);
        lk7.c(A, "Cannot return null from a non-@Nullable @Provides method");
        return A;
    }

    @DexIgnore
    /* renamed from: b */
    public SecureApiService2Dot1 get() {
        return c(this.f3450a, this.b.get());
    }
}
