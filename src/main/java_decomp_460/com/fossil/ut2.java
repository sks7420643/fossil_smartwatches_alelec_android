package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut2(zs2 zs2, boolean z) {
        super(zs2);
        this.g = zs2;
        this.f = z;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.g.h.setDataCollectionEnabled(this.f);
    }
}
