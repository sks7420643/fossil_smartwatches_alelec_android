package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dj extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ af b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dj(af afVar) {
        super(1);
        this.b = afVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        af afVar = this.b;
        long j = ((wv) fsVar).L;
        afVar.I = 0;
        afVar.H = 0;
        afVar.J = 0;
        afVar.G = j;
        int i = (j > afVar.D ? 1 : (j == afVar.D ? 0 : -1));
        if (i > 0 || j <= 0) {
            afVar.G = 0;
            afVar.T();
        } else if (i == 0) {
            afVar.U();
        } else {
            afVar.H = j;
            afVar.V();
        }
        return tl7.f3441a;
    }
}
