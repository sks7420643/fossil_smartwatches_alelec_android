package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol4 implements ql4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f2693a;

        /*
        static {
            int[] iArr = new int[kl4.values().length];
            f2693a = iArr;
            try {
                iArr[kl4.EAN_8.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2693a[kl4.UPC_E.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2693a[kl4.EAN_13.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f2693a[kl4.UPC_A.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f2693a[kl4.QR_CODE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f2693a[kl4.CODE_39.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f2693a[kl4.CODE_93.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f2693a[kl4.CODE_128.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f2693a[kl4.ITF.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f2693a[kl4.PDF_417.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f2693a[kl4.CODABAR.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f2693a[kl4.DATA_MATRIX.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f2693a[kl4.AZTEC.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
        }
        */
    }

    @DexIgnore
    @Override // com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        ql4 fn4;
        switch (a.f2693a[kl4.ordinal()]) {
            case 1:
                fn4 = new fn4();
                break;
            case 2:
                fn4 = new on4();
                break;
            case 3:
                fn4 = new en4();
                break;
            case 4:
                fn4 = new kn4();
                break;
            case 5:
                fn4 = new xn4();
                break;
            case 6:
                fn4 = new an4();
                break;
            case 7:
                fn4 = new cn4();
                break;
            case 8:
                fn4 = new ym4();
                break;
            case 9:
                fn4 = new hn4();
                break;
            case 10:
                fn4 = new pn4();
                break;
            case 11:
                fn4 = new wm4();
                break;
            case 12:
                fn4 = new gm4();
                break;
            case 13:
                fn4 = new sl4();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + kl4);
        }
        return fn4.a(str, kl4, i, i2, map);
    }
}
