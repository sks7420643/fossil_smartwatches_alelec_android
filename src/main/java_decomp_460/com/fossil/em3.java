package com.fossil;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ bw2 b;
    @DexIgnore
    public /* final */ /* synthetic */ ServiceConnection c;
    @DexIgnore
    public /* final */ /* synthetic */ fm3 d;

    @DexIgnore
    public em3(fm3 fm3, bw2 bw2, ServiceConnection serviceConnection) {
        this.d = fm3;
        this.b = bw2;
        this.c = serviceConnection;
    }

    @DexIgnore
    public final void run() {
        fm3 fm3 = this.d;
        cm3 cm3 = fm3.b;
        String str = fm3.f1155a;
        bw2 bw2 = this.b;
        ServiceConnection serviceConnection = this.c;
        Bundle a2 = cm3.a(str, bw2);
        cm3.f629a.c().h();
        if (a2 != null) {
            long j = a2.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                cm3.f629a.d().I().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a2.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    cm3.f629a.d().F().a("No referrer defined in Install Referrer response");
                } else {
                    cm3.f629a.d().N().b("InstallReferrer API result", string);
                    kr3 F = cm3.f629a.F();
                    String valueOf = String.valueOf(string);
                    Bundle z = F.z(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (z == null) {
                        cm3.f629a.d().F().a("No campaign params defined in Install Referrer result");
                    } else {
                        String string2 = z.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a2.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                cm3.f629a.d().F().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                z.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == cm3.f629a.z().k.a()) {
                            cm3.f629a.b();
                            cm3.f629a.d().N().a("Install Referrer campaign has already been logged");
                        } else if (!k63.a() || !cm3.f629a.w().s(xg3.B0) || cm3.f629a.o()) {
                            cm3.f629a.z().k.b(j);
                            cm3.f629a.b();
                            cm3.f629a.d().N().b("Logging Install Referrer campaign from sdk with ", "referrer API");
                            z.putString("_cis", "referrer API");
                            cm3.f629a.E().Q("auto", "_cmp", z);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            ve2.b().c(cm3.f629a.e(), serviceConnection);
        }
    }
}
