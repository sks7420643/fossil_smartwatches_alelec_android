package com.fossil;

import com.fossil.e13;
import com.fossil.e13.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e13<MessageType extends e13<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends nz2<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, e13<?, ?>> zzd; // = new ConcurrentHashMap();
    @DexIgnore
    public w33 zzb; // = w33.a();
    @DexIgnore
    public int zzc; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<MessageType extends e13<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends mz2<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType b;
        @DexIgnore
        public MessageType c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public a(MessageType messagetype) {
            this.b = messagetype;
            this.c = (MessageType) ((e13) messagetype.r(f.d, null, null));
        }

        @DexIgnore
        public static void s(MessageType messagetype, MessageType messagetype2) {
            b33.a().c(messagetype).zzb(messagetype, messagetype2);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.e13$a */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) this.b.r(f.e, null, null);
            aVar.r((e13) b());
            return aVar;
        }

        @DexIgnore
        @Override // com.fossil.o23
        public final /* synthetic */ m23 d() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.o23
        public final boolean j() {
            return e13.v(this.c, false);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.e13$a<MessageType extends com.fossil.e13<MessageType, BuilderType>, BuilderType extends com.fossil.e13$a<MessageType, BuilderType>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.mz2
        public final /* synthetic */ mz2 o(nz2 nz2) {
            r((e13) nz2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.mz2
        public final /* synthetic */ mz2 p(byte[] bArr, int i, int i2) throws l13 {
            t(bArr, 0, i2, q03.a());
            return this;
        }

        @DexIgnore
        @Override // com.fossil.mz2
        public final /* synthetic */ mz2 q(byte[] bArr, int i, int i2, q03 q03) throws l13 {
            t(bArr, 0, i2, q03);
            return this;
        }

        @DexIgnore
        public final BuilderType r(MessageType messagetype) {
            if (this.d) {
                u();
                this.d = false;
            }
            s(this.c, messagetype);
            return this;
        }

        @DexIgnore
        public final BuilderType t(byte[] bArr, int i, int i2, q03 q03) throws l13 {
            if (this.d) {
                u();
                this.d = false;
            }
            try {
                b33.a().c(this.c).a(this.c, bArr, 0, i2 + 0, new sz2(q03));
                return this;
            } catch (l13 e) {
                throw e;
            } catch (IndexOutOfBoundsException e2) {
                throw l13.zza();
            } catch (IOException e3) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e3);
            }
        }

        @DexIgnore
        public void u() {
            MessageType messagetype = (MessageType) ((e13) this.c.r(f.d, null, null));
            s(messagetype, this.c);
            this.c = messagetype;
        }

        @DexIgnore
        /* renamed from: v */
        public MessageType b() {
            if (this.d) {
                return this.c;
            }
            MessageType messagetype = this.c;
            b33.a().c(messagetype).zzc(messagetype);
            this.d = true;
            return this.c;
        }

        @DexIgnore
        /* renamed from: w */
        public final MessageType h() {
            MessageType messagetype = (MessageType) ((e13) b());
            if (messagetype.j()) {
                return messagetype;
            }
            throw new u33(messagetype);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<MessageType extends b<MessageType, BuilderType>, BuilderType> extends e13<MessageType, BuilderType> implements o23 {
        @DexIgnore
        public t03<e> zzc; // = t03.c();

        @DexIgnore
        public final t03<e> C() {
            if (this.zzc.o()) {
                this.zzc = (t03) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T extends e13<T, ?>> extends oz2<T> {
        @DexIgnore
        public c(T t) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<ContainingType extends m23, Type> extends o03<ContainingType, Type> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements v03<e> {
        @DexIgnore
        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final p23 d(p23 p23, m23 m23) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final v23 g(v23 v23, v23 v232) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final int zza() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final l43 zzb() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final s43 zzc() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final boolean zzd() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.v03
        public final boolean zze() {
            throw new NoSuchMethodError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ int f868a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {1, 2, 3, 4, 5, 6, 7};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore
    public static <E> m13<E> B() {
        return a33.d();
    }

    @DexIgnore
    public static <T extends e13<?, ?>> T o(Class<T> cls) {
        e13<?, ?> e13 = zzd.get(cls);
        if (e13 == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                e13 = (T) zzd.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (e13 == null) {
            e13 = (T) ((e13) ((e13) e43.c(cls)).r(f.f, null, null));
            if (e13 != null) {
                zzd.put(cls, e13);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) e13;
    }

    @DexIgnore
    public static j13 p(j13 j13) {
        int size = j13.size();
        return j13.C(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static <E> m13<E> q(m13<E> m13) {
        int size = m13.size();
        return m13.zza(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static Object s(m23 m23, String str, Object[] objArr) {
        return new d33(m23, str, objArr);
    }

    @DexIgnore
    public static Object t(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static <T extends e13<?, ?>> void u(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    @DexIgnore
    public static final <T extends e13<T, ?>> boolean v(T t, boolean z) {
        byte byteValue = ((Byte) t.r(f.f868a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzd2 = b33.a().c(t).zzd(t);
        if (z) {
            t.r(f.b, zzd2 ? t : null, null);
        }
        return zzd2;
    }

    @DexIgnore
    public static k13 y() {
        return f13.c();
    }

    @DexIgnore
    public static j13 z() {
        return z13.c();
    }

    @DexIgnore
    @Override // com.fossil.o23
    public final /* synthetic */ m23 d() {
        return (e13) r(f.f, null, null);
    }

    @DexIgnore
    @Override // com.fossil.m23
    public final /* synthetic */ p23 e() {
        a aVar = (a) r(f.e, null, null);
        aVar.r(this);
        return aVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return b33.a().c(this).zza(this, (e13) obj);
    }

    @DexIgnore
    @Override // com.fossil.nz2
    public final int f() {
        return this.zzc;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        int zza = b33.a().c(this).zza(this);
        this.zza = zza;
        return zza;
    }

    @DexIgnore
    @Override // com.fossil.m23
    public final /* synthetic */ p23 i() {
        return (a) r(f.e, null, null);
    }

    @DexIgnore
    @Override // com.fossil.o23
    public final boolean j() {
        return v(this, true);
    }

    @DexIgnore
    @Override // com.fossil.m23
    public final int l() {
        if (this.zzc == -1) {
            this.zzc = b33.a().c(this).zzb(this);
        }
        return this.zzc;
    }

    @DexIgnore
    @Override // com.fossil.nz2
    public final void m(int i) {
        this.zzc = i;
    }

    @DexIgnore
    @Override // com.fossil.m23
    public final void n(l03 l03) throws IOException {
        b33.a().c(this).b(this, n03.g(l03));
    }

    @DexIgnore
    public abstract Object r(int i, Object obj, Object obj2);

    @DexIgnore
    public String toString() {
        return r23.a(this, super.toString());
    }

    @DexIgnore
    public final <MessageType extends e13<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> BuilderType w() {
        return (BuilderType) ((a) r(f.e, null, null));
    }

    @DexIgnore
    public final BuilderType x() {
        BuilderType buildertype = (BuilderType) ((a) r(f.e, null, null));
        buildertype.r(this);
        return buildertype;
    }
}
