package com.fossil;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lg0 {
    @DexIgnore
    boolean a();

    @DexIgnore
    Object dismiss();  // void declaration

    @DexIgnore
    ListView j();

    @DexIgnore
    Object show();  // void declaration
}
