package com.fossil;

import androidx.fragment.app.Fragment;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ly3<S> extends Fragment {
    @DexIgnore
    public /* final */ LinkedHashSet<ky3<S>> b; // = new LinkedHashSet<>();

    @DexIgnore
    public boolean v6(ky3<S> ky3) {
        return this.b.add(ky3);
    }

    @DexIgnore
    public void w6() {
        this.b.clear();
    }
}
