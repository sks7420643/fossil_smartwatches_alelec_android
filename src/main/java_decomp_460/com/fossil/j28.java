package com.fossil;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j28 implements Interceptor {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ OkHttpClient f1706a;

    @DexIgnore
    public j28(OkHttpClient okHttpClient) {
        this.f1706a = okHttpClient;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        w28 w28 = (w28) chain;
        v18 c = w28.c();
        p28 k = w28.k();
        return w28.j(c, k, k.i(this.f1706a, chain, !c.g().equals("GET")), k.d());
    }
}
