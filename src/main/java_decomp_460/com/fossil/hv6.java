package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hv6 implements Factory<gv6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f1543a;
    @DexIgnore
    public /* final */ Provider<du5> b;
    @DexIgnore
    public /* final */ Provider<au5> c;

    @DexIgnore
    public hv6(Provider<PortfolioApp> provider, Provider<du5> provider2, Provider<au5> provider3) {
        this.f1543a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static hv6 a(Provider<PortfolioApp> provider, Provider<du5> provider2, Provider<au5> provider3) {
        return new hv6(provider, provider2, provider3);
    }

    @DexIgnore
    public static gv6 c(PortfolioApp portfolioApp, du5 du5, au5 au5) {
        return new gv6(portfolioApp, du5, au5);
    }

    @DexIgnore
    /* renamed from: b */
    public gv6 get() {
        return c(this.f1543a.get(), this.b.get(), this.c.get());
    }
}
