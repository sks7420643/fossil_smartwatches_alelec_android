package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i83 implements f83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1592a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.config.string.always_update_disk_on_set", true);

    @DexIgnore
    @Override // com.fossil.f83
    public final boolean zza() {
        return f1592a.o().booleanValue();
    }
}
