package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.m47;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp6 extends pv5 implements yp6, View.OnClickListener, t47.g {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<v55> g;
    @DexIgnore
    public xp6 h;
    @DexIgnore
    public Integer i;
    @DexIgnore
    public /* final */ String j; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final kp6 a() {
            return new kp6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp6 b;

        @DexIgnore
        public b(kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp6 b;

        @DexIgnore
        public c(kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.r(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp6 b;

        @DexIgnore
        public d(kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (wr4.f3989a.a().n()) {
                String a2 = m47.a(m47.c.REPAIR_CENTER, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = kp6.l;
                local.d(str, "Support mail = " + a2);
                kp6 kp6 = this.b;
                pq7.b(a2, "url");
                kp6.O6(a2);
                return;
            }
            this.b.M6().p();
            this.b.M6().q("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = kp6.class.getSimpleName();
        pq7.b(simpleName, "DeleteAccountFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public final xp6 M6() {
        xp6 xp6 = this.h;
        if (xp6 != null) {
            return xp6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(xp6 xp6) {
        pq7.c(xp6, "presenter");
        i14.l(xp6);
        pq7.b(xp6, "Preconditions.checkNotNull(presenter)");
        this.h = xp6;
    }

    @DexIgnore
    public final void O6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), l);
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = l;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((ls5) activity).R5(str, i2, intent);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i2 == 2131363373) {
                xp6 xp6 = this.h;
                if (xp6 != null) {
                    xp6.n();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void a6() {
        B6().t();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(l, "deleteUser - successfully");
                WelcomeActivity.B.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void j0(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        pq7.c(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void o(int i2, String str) {
        pq7.c(str, "message");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            xp6 xp6 = this.h;
            if (xp6 != null) {
                xp6.o();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        pq7.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        v55 v55 = (v55) aq0.f(LayoutInflater.from(getContext()), 2131558548, null, false, A6());
        this.g = new g37<>(this, v55);
        pq7.b(v55, "binding");
        return v55.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        xp6 xp6 = this.h;
        if (xp6 != null) {
            xp6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        xp6 xp6 = this.h;
        if (xp6 != null) {
            xp6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        if (!TextUtils.isEmpty(this.j)) {
            this.i = Integer.valueOf(Color.parseColor(this.j));
        }
        g37<v55> g37 = this.g;
        if (g37 != null) {
            v55 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                String q = vt7.q(um5.c(PortfolioApp.h0.c(), 2131887094).toString(), "contact_our_support_team", "", false, 4, null);
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "binding.tvDescription");
                flexibleTextView.setText(Html.fromHtml(q));
                Integer num = this.i;
                if (num != null) {
                    a2.u.setLinkTextColor(num.intValue());
                }
                a2.u.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
