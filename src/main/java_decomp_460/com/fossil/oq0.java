package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oq0 implements LayoutInflater.Factory2 {
    @DexIgnore
    public /* final */ FragmentManager b;

    @DexIgnore
    public oq0(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    @DexIgnore
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        Fragment fragment = null;
        if (FragmentContainerView.class.getName().equals(str)) {
            return new FragmentContainerView(context, attributeSet, this.b);
        }
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, FacebookAppLinkResolver.APP_LINK_TARGET_CLASS_KEY);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hq0.Fragment);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(hq0.Fragment_android_name);
        }
        int resourceId = obtainStyledAttributes.getResourceId(hq0.Fragment_android_id, -1);
        String string = obtainStyledAttributes.getString(hq0.Fragment_android_tag);
        obtainStyledAttributes.recycle();
        if (attributeValue == null || !nq0.isFragmentClass(context.getClassLoader(), attributeValue)) {
            return null;
        }
        int id = view != null ? view.getId() : 0;
        if (id == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + attributeValue);
        }
        if (resourceId != -1) {
            fragment = this.b.Y(resourceId);
        }
        if (fragment == null && string != null) {
            fragment = this.b.Z(string);
        }
        if (fragment == null && id != -1) {
            fragment = this.b.Y(id);
        }
        if (FragmentManager.s0(2)) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + attributeValue + " existing=" + fragment);
        }
        if (fragment == null) {
            Fragment instantiate = this.b.h0().instantiate(context.getClassLoader(), attributeValue);
            instantiate.mFromLayout = true;
            instantiate.mFragmentId = resourceId != 0 ? resourceId : id;
            instantiate.mContainerId = id;
            instantiate.mTag = string;
            instantiate.mInLayout = true;
            FragmentManager fragmentManager = this.b;
            instantiate.mFragmentManager = fragmentManager;
            FragmentHostCallback<?> fragmentHostCallback = fragmentManager.o;
            instantiate.mHost = fragmentHostCallback;
            instantiate.onInflate(fragmentHostCallback.e(), attributeSet, instantiate.mSavedFragmentState);
            this.b.d(instantiate);
            this.b.D0(instantiate);
            fragment = instantiate;
        } else if (!fragment.mInLayout) {
            fragment.mInLayout = true;
            FragmentHostCallback<?> fragmentHostCallback2 = this.b.o;
            fragment.mHost = fragmentHostCallback2;
            fragment.onInflate(fragmentHostCallback2.e(), attributeSet, fragment.mSavedFragmentState);
        } else {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + attributeValue);
        }
        FragmentManager fragmentManager2 = this.b;
        if (fragmentManager2.n >= 1 || !fragment.mFromLayout) {
            this.b.D0(fragment);
        } else {
            fragmentManager2.E0(fragment, 1);
        }
        View view2 = fragment.mView;
        if (view2 != null) {
            if (resourceId != 0) {
                view2.setId(resourceId);
            }
            if (fragment.mView.getTag() == null) {
                fragment.mView.setTag(string);
            }
            return fragment.mView;
        }
        throw new IllegalStateException("Fragment " + attributeValue + " did not create a view.");
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
