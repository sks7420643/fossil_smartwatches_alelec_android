package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dg extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ eh b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dg(eh ehVar) {
        super(1);
        this.b = ehVar;
    }

    @DexIgnore
    public final void a(fs fsVar) {
        qs qsVar = (qs) fsVar;
        List d0 = em7.d0(qsVar.A);
        List d02 = em7.d0(qsVar.B);
        pm7.N(d0, null, null, null, 0, null, pf.b, 31, null);
        pm7.N(d02, null, null, null, 0, null, bf.b, 31, null);
        fh fhVar = this.b.b;
        k5 k5Var = fhVar.w;
        ky1 ky1 = ky1.DEBUG;
        String str = fhVar.f2227a;
        fhVar.H.clear();
        CopyOnWriteArrayList<n6> copyOnWriteArrayList = fhVar.H;
        ArrayList arrayList = new ArrayList();
        for (Object obj : d02) {
            if (fh.Q.contains((n6) obj)) {
                arrayList.add(obj);
            }
        }
        copyOnWriteArrayList.addAll(arrayList);
        this.b.b.U();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(fs fsVar) {
        a(fsVar);
        return tl7.f3441a;
    }
}
