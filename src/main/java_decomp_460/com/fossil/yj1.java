package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj1 implements mb1 {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public yj1(Object obj) {
        ik1.d(obj);
        this.b = obj;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(mb1.f2349a));
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (obj instanceof yj1) {
            return this.b.equals(((yj1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
