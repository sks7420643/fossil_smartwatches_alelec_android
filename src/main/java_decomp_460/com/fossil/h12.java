package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class h12 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i12 f1409a;
    @DexIgnore
    public /* final */ h02 b;
    @DexIgnore
    public /* final */ c02 c;

    @DexIgnore
    public h12(i12 i12, h02 h02, c02 c02) {
        this.f1409a = i12;
        this.b = h02;
        this.c = c02;
    }

    @DexIgnore
    public static s32.a b(i12 i12, h02 h02, c02 c02) {
        return new h12(i12, h02, c02);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return i12.b(this.f1409a, this.b, this.c);
    }
}
