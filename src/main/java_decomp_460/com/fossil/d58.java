package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d58 {
    @DexIgnore
    public static /* final */ d58 d; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f739a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends d58 {
        @DexIgnore
        @Override // com.fossil.d58
        public d58 d(long j) {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.d58
        public void f() {
        }

        @DexIgnore
        @Override // com.fossil.d58
        public d58 g(long j, TimeUnit timeUnit) {
            pq7.c(timeUnit, Constants.PROFILE_KEY_UNIT);
            return this;
        }
    }

    @DexIgnore
    public d58 a() {
        this.f739a = false;
        return this;
    }

    @DexIgnore
    public d58 b() {
        this.c = 0;
        return this;
    }

    @DexIgnore
    public long c() {
        if (this.f739a) {
            return this.b;
        }
        throw new IllegalStateException("No deadline".toString());
    }

    @DexIgnore
    public d58 d(long j) {
        this.f739a = true;
        this.b = j;
        return this;
    }

    @DexIgnore
    public boolean e() {
        return this.f739a;
    }

    @DexIgnore
    public void f() throws IOException {
        if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        } else if (this.f739a && this.b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    @DexIgnore
    public d58 g(long j, TimeUnit timeUnit) {
        pq7.c(timeUnit, Constants.PROFILE_KEY_UNIT);
        if (j >= 0) {
            this.c = timeUnit.toNanos(j);
            return this;
        }
        throw new IllegalArgumentException(("timeout < 0: " + j).toString());
    }

    @DexIgnore
    public long h() {
        return this.c;
    }
}
