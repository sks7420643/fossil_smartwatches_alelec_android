package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm extends zj {
    @DexIgnore
    public /* final */ tu1[] T;

    @DexIgnore
    public sm(k5 k5Var, i60 i60, tu1[] tu1Arr) {
        super(k5Var, i60, yp.h0, true, ke.b.b(k5Var.x, ob.MICRO_APP), new byte[0], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = tu1Arr;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.a4, px1.a(this.T));
    }

    @DexIgnore
    @Override // com.fossil.zj, com.fossil.ro
    public byte[] M() {
        ry1 microAppVersion = this.x.a().getMicroAppVersion();
        ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.MICRO_APP.b));
        if (ry1 == null) {
            ry1 = hd0.y.d();
        }
        pq7.b(ry1, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
        t90 t90 = new t90(this.T, microAppVersion);
        byte a2 = x90.c.a();
        byte[] a3 = t90.a(t90.f);
        byte[] a4 = dy1.a(dy1.a(dy1.a(new byte[]{(byte) t90.c.getMajor(), (byte) t90.c.getMinor(), a2}, a3), t90.a(t90.d)), t90.a(t90.e));
        ByteBuffer order = ByteBuffer.allocate(a4.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        order.put(a4);
        order.putInt((int) ix1.f1688a.b(a4, ix1.a.CRC32));
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        try {
            return i9.d.a(this.D, ry1, array);
        } catch (sx1 e) {
            return new byte[0];
        }
    }
}
