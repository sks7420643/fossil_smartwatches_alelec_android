package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ fp1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public xt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(qq1.class.getClassLoader());
            if (readParcelable != null) {
                qq1 qq1 = (qq1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(nt1.class.getClassLoader());
                if (readParcelable2 != null) {
                    nt1 nt1 = (nt1) readParcelable2;
                    Parcelable readParcelable3 = parcel.readParcelable(fp1.class.getClassLoader());
                    if (readParcelable3 != null) {
                        return new xt1(qq1, nt1, (fp1) readParcelable3);
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public xt1[] newArray(int i) {
            return new xt1[i];
        }
    }

    @DexIgnore
    public xt1(fp1 fp1) {
        this(null, null, fp1);
    }

    @DexIgnore
    public xt1(qq1 qq1, fp1 fp1) {
        super(qq1, null);
        this.d = fp1;
    }

    @DexIgnore
    public xt1(qq1 qq1, nt1 nt1, fp1 fp1) {
        super(qq1, nt1);
        this.d = fp1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return gy1.c(super.a(), this.d.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("weatherInfo", this.d.a());
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(xt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((xt1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherComplicationData");
    }

    @DexIgnore
    public final fp1 getWeatherInfo() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
