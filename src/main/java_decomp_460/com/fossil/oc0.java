package com.fossil;

import com.fossil.imagefilters.Format;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class oc0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2667a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[Format.values().length];
        f2667a = iArr;
        iArr[Format.RAW.ordinal()] = 1;
        f2667a[Format.RLE.ordinal()] = 2;
        int[] iArr2 = new int[vb0.values().length];
        b = iArr2;
        iArr2[vb0.IMAGE.ordinal()] = 1;
        b[vb0.COMP.ordinal()] = 2;
        b[vb0.TEXT.ordinal()] = 3;
    }
    */
}
