package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl7<T> implements yk7<T>, Serializable {
    @DexIgnore
    public volatile Object _value;
    @DexIgnore
    public gp7<? extends T> initializer;
    @DexIgnore
    public /* final */ Object lock;

    @DexIgnore
    public fl7(gp7<? extends T> gp7, Object obj) {
        pq7.c(gp7, "initializer");
        this.initializer = gp7;
        this._value = pl7.f2845a;
        this.lock = obj == null ? this : obj;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fl7(gp7 gp7, Object obj, int i, kq7 kq7) {
        this(gp7, (i & 2) != 0 ? null : obj);
    }

    @DexIgnore
    private final Object writeReplace() {
        return new vk7(getValue());
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0011 */
    @Override // com.fossil.yk7
    public T getValue() {
        Object obj = (T) this._value;
        if (obj == pl7.f2845a) {
            synchronized (this.lock) {
                Object obj2 = this._value;
                obj = obj2;
                if (obj2 == pl7.f2845a) {
                    gp7<? extends T> gp7 = this.initializer;
                    if (gp7 != null) {
                        Object invoke = gp7.invoke();
                        this._value = invoke;
                        this.initializer = null;
                        obj = invoke;
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }
        return (T) obj;
    }

    @DexIgnore
    public boolean isInitialized() {
        return this._value != pl7.f2845a;
    }

    @DexIgnore
    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }
}
