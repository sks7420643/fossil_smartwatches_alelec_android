package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1086a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public f91(String str, String str2) {
        this.f1086a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.f1086a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f91.class != obj.getClass()) {
            return false;
        }
        f91 f91 = (f91) obj;
        return TextUtils.equals(this.f1086a, f91.f1086a) && TextUtils.equals(this.b, f91.b);
    }

    @DexIgnore
    public int hashCode() {
        return (this.f1086a.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Header[name=" + this.f1086a + ",value=" + this.b + "]";
    }
}
