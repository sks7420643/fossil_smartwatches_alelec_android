package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class zf4 implements ht3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bg4 f4464a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ScheduledFuture c;

    @DexIgnore
    public zf4(bg4 bg4, String str, ScheduledFuture scheduledFuture) {
        this.f4464a = bg4;
        this.b = str;
        this.c = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3 nt3) {
        this.f4464a.g(this.b, this.c, nt3);
    }
}
