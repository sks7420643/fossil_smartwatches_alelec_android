package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a98 implements e88<w18, Character> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a98 f230a; // = new a98();

    @DexIgnore
    /* renamed from: b */
    public Character a(w18 w18) throws IOException {
        String string = w18.string();
        if (string.length() == 1) {
            return Character.valueOf(string.charAt(0));
        }
        throw new IOException("Expected body of length 1 for Character conversion but was " + string.length());
    }
}
