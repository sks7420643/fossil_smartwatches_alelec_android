package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tr2> CREATOR; // = new ur2();
    @DexIgnore
    public static /* final */ List<zb2> e; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ za3 f; // = new za3();
    @DexIgnore
    public za3 b;
    @DexIgnore
    public List<zb2> c;
    @DexIgnore
    public String d;

    @DexIgnore
    public tr2(za3 za3, List<zb2> list, String str) {
        this.b = za3;
        this.c = list;
        this.d = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof tr2)) {
            return false;
        }
        tr2 tr2 = (tr2) obj;
        return pc2.a(this.b, tr2.b) && pc2.a(this.c, tr2.c) && pc2.a(this.d, tr2.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, this.b, i, false);
        bd2.y(parcel, 2, this.c, false);
        bd2.u(parcel, 3, this.d, false);
        bd2.b(parcel, a2);
    }
}
