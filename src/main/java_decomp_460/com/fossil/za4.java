package com.fossil;

import android.util.JsonReader;
import com.fossil.cb4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class za4 implements cb4.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ za4 f4444a; // = new za4();

    @DexIgnore
    public static cb4.a b() {
        return f4444a;
    }

    @DexIgnore
    @Override // com.fossil.cb4.a
    public Object a(JsonReader jsonReader) {
        return cb4.p(jsonReader);
    }
}
