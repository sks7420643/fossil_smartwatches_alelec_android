package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv6 implements MembersInjector<av6> {
    @DexIgnore
    public static void A(av6 av6, SleepSummariesRepository sleepSummariesRepository) {
        av6.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void B(av6 av6, SummariesRepository summariesRepository) {
        av6.C = summariesRepository;
    }

    @DexIgnore
    public static void C(av6 av6, uq4 uq4) {
        av6.q = uq4;
    }

    @DexIgnore
    public static void D(av6 av6, UserRepository userRepository) {
        av6.l = userRepository;
    }

    @DexIgnore
    public static void E(av6 av6, WatchLocalizationRepository watchLocalizationRepository) {
        av6.I = watchLocalizationRepository;
    }

    @DexIgnore
    public static void F(av6 av6, WorkoutSettingRepository workoutSettingRepository) {
        av6.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void G(av6 av6, xn5 xn5) {
        av6.w = xn5;
    }

    @DexIgnore
    public static void H(av6 av6) {
        av6.c0();
    }

    @DexIgnore
    public static void I(av6 av6, hu4 hu4) {
        av6.K = hu4;
    }

    @DexIgnore
    public static void a(av6 av6, vt4 vt4) {
        av6.L = vt4;
    }

    @DexIgnore
    public static void b(av6 av6, pr4 pr4) {
        av6.M = pr4;
    }

    @DexIgnore
    public static void c(av6 av6, AlarmsRepository alarmsRepository) {
        av6.J = alarmsRepository;
    }

    @DexIgnore
    public static void d(av6 av6, ck5 ck5) {
        av6.B = ck5;
    }

    @DexIgnore
    public static void e(av6 av6, o27 o27) {
        av6.A = o27;
    }

    @DexIgnore
    public static void f(av6 av6, DeviceRepository deviceRepository) {
        av6.m = deviceRepository;
    }

    @DexIgnore
    public static void g(av6 av6, mj5 mj5) {
        av6.k = mj5;
    }

    @DexIgnore
    public static void h(av6 av6, av5 av5) {
        av6.i = av5;
    }

    @DexIgnore
    public static void i(av6 av6, ru5 ru5) {
        av6.o = ru5;
    }

    @DexIgnore
    public static void j(av6 av6, hu5 hu5) {
        av6.F = hu5;
    }

    @DexIgnore
    public static void k(av6 av6, ju5 ju5) {
        av6.u = ju5;
    }

    @DexIgnore
    public static void l(av6 av6, iu5 iu5) {
        av6.G = iu5;
    }

    @DexIgnore
    public static void m(av6 av6, ku5 ku5) {
        av6.t = ku5;
    }

    @DexIgnore
    public static void n(av6 av6, tu5 tu5) {
        av6.r = tu5;
    }

    @DexIgnore
    public static void o(av6 av6, uu5 uu5) {
        av6.s = uu5;
    }

    @DexIgnore
    public static void p(av6 av6, su5 su5) {
        av6.p = su5;
    }

    @DexIgnore
    public static void q(av6 av6, v27 v27) {
        av6.H = v27;
    }

    @DexIgnore
    public static void r(av6 av6, GoalTrackingRepository goalTrackingRepository) {
        av6.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void s(av6 av6, cv5 cv5) {
        av6.g = cv5;
    }

    @DexIgnore
    public static void t(av6 av6, dv5 dv5) {
        av6.v = dv5;
    }

    @DexIgnore
    public static void u(av6 av6, ev5 ev5) {
        av6.x = ev5;
    }

    @DexIgnore
    public static void v(av6 av6, fv5 fv5) {
        av6.h = fv5;
    }

    @DexIgnore
    public static void w(av6 av6, gv5 gv5) {
        av6.z = gv5;
    }

    @DexIgnore
    public static void x(av6 av6, hv5 hv5) {
        av6.y = hv5;
    }

    @DexIgnore
    public static void y(av6 av6, ht5 ht5) {
        av6.j = ht5;
    }

    @DexIgnore
    public static void z(av6 av6, on5 on5) {
        av6.n = on5;
    }
}
