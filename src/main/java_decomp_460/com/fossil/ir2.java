package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ir2> CREATOR; // = new jr2();
    @DexIgnore
    public static /* final */ List<zb2> i; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest b;
    @DexIgnore
    public List<zb2> c;
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public String h;

    @DexIgnore
    public ir2(LocationRequest locationRequest, List<zb2> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.b = locationRequest;
        this.c = list;
        this.d = str;
        this.e = z;
        this.f = z2;
        this.g = z3;
        this.h = str2;
    }

    @DexIgnore
    @Deprecated
    public static ir2 c(LocationRequest locationRequest) {
        return new ir2(locationRequest, i, null, false, false, false, null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof ir2)) {
            return false;
        }
        ir2 ir2 = (ir2) obj;
        return pc2.a(this.b, ir2.b) && pc2.a(this.c, ir2.c) && pc2.a(this.d, ir2.d) && this.e == ir2.e && this.f == ir2.f && this.g == ir2.g && pc2.a(this.h, ir2.h);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        if (this.d != null) {
            sb.append(" tag=");
            sb.append(this.d);
        }
        if (this.h != null) {
            sb.append(" moduleId=");
            sb.append(this.h);
        }
        sb.append(" hideAppOps=");
        sb.append(this.e);
        sb.append(" clients=");
        sb.append(this.c);
        sb.append(" forceCoarseLocation=");
        sb.append(this.f);
        if (this.g) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, this.b, i2, false);
        bd2.y(parcel, 5, this.c, false);
        bd2.u(parcel, 6, this.d, false);
        bd2.c(parcel, 7, this.e);
        bd2.c(parcel, 8, this.f);
        bd2.c(parcel, 9, this.g);
        bd2.u(parcel, 10, this.h, false);
        bd2.b(parcel, a2);
    }
}
