package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr4 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3146a;
    @DexIgnore
    @rj4("variantKey")
    public String b;

    @DexIgnore
    public rr4(String str, String str2) {
        pq7.c(str, "id");
        this.f3146a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.f3146a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof rr4) {
                rr4 rr4 = (rr4) obj;
                if (!pq7.a(this.f3146a, rr4.f3146a) || !pq7.a(this.b, rr4.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f3146a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Flag(id=" + this.f3146a + ", variantKey=" + this.b + ")";
    }
}
