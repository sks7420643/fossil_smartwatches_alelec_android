package com.fossil;

import android.view.View;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wy0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, Object> f4017a; // = new HashMap();
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ ArrayList<Transition> c; // = new ArrayList<>();

    @DexIgnore
    @Deprecated
    public wy0() {
    }

    @DexIgnore
    public wy0(View view) {
        this.b = view;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof wy0) {
            wy0 wy0 = (wy0) obj;
            return this.b == wy0.b && this.f4017a.equals(wy0.f4017a);
        }
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.f4017a.hashCode();
    }

    @DexIgnore
    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + "\n") + "    values:";
        for (String str2 : this.f4017a.keySet()) {
            str = str + "    " + str2 + ": " + this.f4017a.get(str2) + "\n";
        }
        return str;
    }
}
