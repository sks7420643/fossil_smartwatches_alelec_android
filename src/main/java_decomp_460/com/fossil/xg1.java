package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xg1 implements id1<byte[]> {
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public xg1(byte[] bArr) {
        ik1.d(bArr);
        this.b = bArr;
    }

    @DexIgnore
    /* renamed from: a */
    public byte[] get() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return this.b.length;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<byte[]> d() {
        return byte[].class;
    }
}
