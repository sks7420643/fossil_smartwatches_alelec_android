package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static Boolean f1616a;
    @DexIgnore
    public static Boolean b;
    @DexIgnore
    public static Boolean c;

    @DexIgnore
    public static boolean a() {
        return "user".equals(Build.TYPE);
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean b(Context context) {
        return c(context.getPackageManager());
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean c(PackageManager packageManager) {
        if (f1616a == null) {
            f1616a = Boolean.valueOf(mf2.g() && packageManager.hasSystemFeature("android.hardware.type.watch"));
        }
        return f1616a.booleanValue();
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean d(Context context) {
        return b(context) && (!mf2.i() || (e(context) && !mf2.j()));
    }

    @DexIgnore
    @TargetApi(21)
    public static boolean e(Context context) {
        if (b == null) {
            b = Boolean.valueOf(mf2.h() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    @DexIgnore
    public static boolean f(Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }
}
