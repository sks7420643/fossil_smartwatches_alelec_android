package com.fossil;

import com.fossil.a18;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i61 extends g61<q18> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i61(a18.a aVar) {
        super(aVar);
        pq7.c(aVar, "callFactory");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.g61
    public /* bridge */ /* synthetic */ q18 f(q18 q18) {
        q18 q182 = q18;
        h(q182);
        return q182;
    }

    @DexIgnore
    /* renamed from: g */
    public String b(q18 q18) {
        pq7.c(q18, "data");
        String q182 = q18.toString();
        pq7.b(q182, "data.toString()");
        return q182;
    }

    @DexIgnore
    public q18 h(q18 q18) {
        pq7.c(q18, "$this$toHttpUrl");
        return q18;
    }
}
