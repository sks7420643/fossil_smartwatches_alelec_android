package com.fossil;

import android.os.Build;
import android.os.CancellationSignal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f2695a;
    @DexIgnore
    public a b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object onCancel();  // void declaration
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r0 == null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (android.os.Build.VERSION.SDK_INT < 16) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        ((android.os.CancellationSignal) r0).cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r3.d = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0030, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0031, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r3.d = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r1 == null) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r1.onCancel();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.f2695a     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r3)     // Catch:{ all -> 0x003d }
        L_0x0006:
            return
        L_0x0007:
            r0 = 1
            r3.f2695a = r0     // Catch:{ all -> 0x003d }
            r0 = 1
            r3.d = r0     // Catch:{ all -> 0x003d }
            com.fossil.om0$a r1 = r3.b     // Catch:{ all -> 0x003d }
            java.lang.Object r0 = r3.c     // Catch:{ all -> 0x003d }
            monitor-exit(r3)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x0017
            r1.onCancel()     // Catch:{ all -> 0x0030 }
        L_0x0017:
            if (r0 == 0) goto L_0x0024
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0030 }
            r2 = 16
            if (r1 < r2) goto L_0x0024
            android.os.CancellationSignal r0 = (android.os.CancellationSignal) r0     // Catch:{ all -> 0x0030 }
            r0.cancel()     // Catch:{ all -> 0x0030 }
        L_0x0024:
            monitor-enter(r3)
            r0 = 0
            r3.d = r0     // Catch:{ all -> 0x002d }
            r3.notifyAll()     // Catch:{ all -> 0x002d }
            monitor-exit(r3)     // Catch:{ all -> 0x002d }
            goto L_0x0006
        L_0x002d:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002d }
            throw r0
        L_0x0030:
            r0 = move-exception
            monitor-enter(r3)
            r1 = 0
            r3.d = r1     // Catch:{ all -> 0x003a }
            r3.notifyAll()     // Catch:{ all -> 0x003a }
            monitor-exit(r3)     // Catch:{ all -> 0x003a }
            throw r0
        L_0x003a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x003d:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.om0.a():void");
    }

    @DexIgnore
    public Object b() {
        Object obj;
        if (Build.VERSION.SDK_INT < 16) {
            return null;
        }
        synchronized (this) {
            if (this.c == null) {
                CancellationSignal cancellationSignal = new CancellationSignal();
                this.c = cancellationSignal;
                if (this.f2695a) {
                    cancellationSignal.cancel();
                }
            }
            obj = this.c;
        }
        return obj;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this) {
            z = this.f2695a;
        }
        return z;
    }

    @DexIgnore
    public void d(a aVar) {
        synchronized (this) {
            f();
            if (this.b != aVar) {
                this.b = aVar;
                if (this.f2695a && aVar != null) {
                    aVar.onCancel();
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        if (c()) {
            throw new vm0();
        }
    }

    @DexIgnore
    public final void f() {
        while (this.d) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }
}
