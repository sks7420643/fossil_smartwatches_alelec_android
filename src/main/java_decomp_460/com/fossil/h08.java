package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h08 extends i08 {
    @DexIgnore
    public static /* final */ dv7 h;
    @DexIgnore
    public static /* final */ h08 i;

    /*
    static {
        h08 h08 = new h08();
        i = h08;
        h = h08.T(wz7.f("kotlinx.coroutines.io.parallelism", bs7.d(64, wz7.a()), 0, 0, 12, null));
    }
    */

    @DexIgnore
    public h08() {
        super(0, 0, null, 7, null);
    }

    @DexIgnore
    public final dv7 b0() {
        return h;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public String toString() {
        return "DefaultDispatcher";
    }
}
