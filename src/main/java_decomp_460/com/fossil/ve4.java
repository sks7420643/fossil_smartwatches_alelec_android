package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ve4 implements ft3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FirebaseInstanceId f3755a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public ve4(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.f3755a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    @Override // com.fossil.ft3
    public final Object then(nt3 nt3) {
        return this.f3755a.C(this.b, this.c, nt3);
    }
}
