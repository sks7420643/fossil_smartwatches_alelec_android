package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f97 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1090a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public f97(String str, String str2) {
        pq7.c(str, "id");
        pq7.c(str2, "imageBase64");
        this.f1090a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.f1090a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof f97) {
                f97 f97 = (f97) obj;
                if (!pq7.a(this.f1090a, f97.f1090a) || !pq7.a(this.b, f97.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1090a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WFBackgroundPhotoDraft(id=" + this.f1090a + ", imageBase64=" + this.b + ")";
    }
}
