package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n63 implements o63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2472a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.engagement_time_main_thread", true);

    @DexIgnore
    @Override // com.fossil.o63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.o63
    public final boolean zzb() {
        return f2472a.o().booleanValue();
    }
}
