package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ is2 f775a;

    @DexIgnore
    public de3(is2 is2) {
        rc2.k(is2);
        this.f775a = is2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.f775a.getId();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.f775a.remove();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void c(LatLng latLng) {
        try {
            this.f775a.setCenter(latLng);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        try {
            this.f775a.b(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void e(int i) {
        try {
            this.f775a.setFillColor(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof de3)) {
            return false;
        }
        try {
            return this.f775a.b0(((de3) obj).f775a);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void f(double d) {
        try {
            this.f775a.setRadius(d);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.f775a.setStrokeColor(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void h(float f) {
        try {
            this.f775a.setStrokeWidth(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.f775a.a();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.f775a.setVisible(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void j(float f) {
        try {
            this.f775a.setZIndex(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
