package com.fossil;

import com.fossil.ec4;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dc4 implements ec4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f767a;

    @DexIgnore
    public dc4(File file) {
        this.f767a = file;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public Map<String, String> a() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public String b() {
        return this.f767a.getName();
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public File c() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public File[] d() {
        return this.f767a.listFiles();
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public String e() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public ec4.a getType() {
        return ec4.a.NATIVE;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public void remove() {
        File[] d = d();
        for (File file : d) {
            x74.f().b("Removing native report file at " + file.getPath());
            file.delete();
        }
        x74.f().b("Removing native report directory at " + this.f767a);
        this.f767a.delete();
    }
}
