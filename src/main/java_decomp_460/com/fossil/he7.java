package com.fossil;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class he7 extends de7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MethodCall f1466a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements je7 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ MethodChannel.Result f1467a;

        @DexIgnore
        public a(he7 he7, MethodChannel.Result result) {
            this.f1467a = result;
        }

        @DexIgnore
        @Override // com.fossil.je7
        public void error(String str, String str2, Object obj) {
            this.f1467a.error(str, str2, obj);
        }

        @DexIgnore
        @Override // com.fossil.je7
        public void success(Object obj) {
            this.f1467a.success(obj);
        }
    }

    @DexIgnore
    public he7(MethodCall methodCall, MethodChannel.Result result) {
        this.f1466a = methodCall;
        this.b = new a(this, result);
    }

    @DexIgnore
    @Override // com.fossil.ie7
    public <T> T a(String str) {
        return (T) this.f1466a.argument(str);
    }

    @DexIgnore
    @Override // com.fossil.de7
    public je7 i() {
        return this.b;
    }
}
