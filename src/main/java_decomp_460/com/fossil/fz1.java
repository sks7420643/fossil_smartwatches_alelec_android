package com.fossil;

import com.fossil.lz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz1 extends lz1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lz1.b f1239a;
    @DexIgnore
    public /* final */ bz1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends lz1.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public lz1.b f1240a;
        @DexIgnore
        public bz1 b;

        @DexIgnore
        @Override // com.fossil.lz1.a
        public lz1.a a(bz1 bz1) {
            this.b = bz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lz1.a
        public lz1.a b(lz1.b bVar) {
            this.f1240a = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lz1.a
        public lz1 c() {
            return new fz1(this.f1240a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ fz1(lz1.b bVar, bz1 bz1, a aVar) {
        this.f1239a = bVar;
        this.b = bz1;
    }

    @DexIgnore
    @Override // com.fossil.lz1
    public bz1 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.lz1
    public lz1.b c() {
        return this.f1239a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof lz1)) {
            return false;
        }
        lz1.b bVar = this.f1239a;
        if (bVar != null ? bVar.equals(((fz1) obj).f1239a) : ((fz1) obj).f1239a == null) {
            bz1 bz1 = this.b;
            if (bz1 == null) {
                if (((fz1) obj).b == null) {
                    z = true;
                    return z;
                }
            } else if (bz1.equals(((fz1) obj).b)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        lz1.b bVar = this.f1239a;
        int hashCode = bVar == null ? 0 : bVar.hashCode();
        bz1 bz1 = this.b;
        if (bz1 != null) {
            i = bz1.hashCode();
        }
        return ((hashCode ^ 1000003) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "ClientInfo{clientType=" + this.f1239a + ", androidClientInfo=" + this.b + "}";
    }
}
