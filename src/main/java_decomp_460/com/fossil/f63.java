package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f63 implements xw2<i63> {
    @DexIgnore
    public static f63 c; // = new f63();
    @DexIgnore
    public /* final */ xw2<i63> b;

    @DexIgnore
    public f63() {
        this(ww2.b(new h63()));
    }

    @DexIgnore
    public f63(xw2<i63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((i63) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ i63 zza() {
        return this.b.zza();
    }
}
