package com.fossil;

import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import com.fossil.vd0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wd0 {

    @DexIgnore
    public interface a extends vd0.d {
        @DexIgnore
        void a(String str, Bundle bundle);

        @DexIgnore
        void b(String str, List<?> list, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends vd0.e<T> {
        @DexIgnore
        public b(T t) {
            super(t);
        }

        @DexIgnore
        @Override // android.media.browse.MediaBrowser.SubscriptionCallback
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) this.f3752a).b(str, list, bundle);
        }

        @DexIgnore
        public void onError(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) this.f3752a).a(str, bundle);
        }
    }

    @DexIgnore
    public static Object a(a aVar) {
        return new b(aVar);
    }
}
