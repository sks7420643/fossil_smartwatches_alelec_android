package com.fossil;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr3 extends jn3 {
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public hg3 c; // = ig3.f1618a;
    @DexIgnore
    public Boolean d;

    @DexIgnore
    public zr3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public static long M() {
        return xg3.D.a(null).longValue();
    }

    @DexIgnore
    public static long N() {
        return xg3.d.a(null).longValue();
    }

    @DexIgnore
    public final Boolean A(String str) {
        rc2.g(str);
        Bundle R = R();
        if (R == null) {
            d().F().a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (R.containsKey(str)) {
            return Boolean.valueOf(R.getBoolean(str));
        } else {
            return null;
        }
    }

    @DexIgnore
    public final boolean B(String str, zk3<Boolean> zk3) {
        return y(str, zk3);
    }

    @DexIgnore
    public final long C() {
        b();
        return 31000;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a A[SYNTHETIC, Splitter:B:8:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> D(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            com.fossil.rc2.g(r5)
            android.os.Bundle r1 = r4.R()
            if (r1 != 0) goto L_0x001b
            com.fossil.kl3 r1 = r4.d()
            com.fossil.nl3 r1 = r1.F()
            java.lang.String r2 = "Failed to load metadata: Metadata bundle is null"
            r1.a(r2)
        L_0x0017:
            r1 = r0
        L_0x0018:
            if (r1 != 0) goto L_0x002a
        L_0x001a:
            return r0
        L_0x001b:
            boolean r2 = r1.containsKey(r5)
            if (r2 == 0) goto L_0x0017
            int r1 = r1.getInt(r5)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            goto L_0x0018
        L_0x002a:
            android.content.Context r2 = r4.e()     // Catch:{ NotFoundException -> 0x0041 }
            android.content.res.Resources r2 = r2.getResources()     // Catch:{ NotFoundException -> 0x0041 }
            int r1 = r1.intValue()     // Catch:{ NotFoundException -> 0x0041 }
            java.lang.String[] r1 = r2.getStringArray(r1)     // Catch:{ NotFoundException -> 0x0041 }
            if (r1 == 0) goto L_0x001a
            java.util.List r0 = java.util.Arrays.asList(r1)     // Catch:{ NotFoundException -> 0x0041 }
            goto L_0x001a
        L_0x0041:
            r1 = move-exception
            com.fossil.kl3 r2 = r4.d()
            com.fossil.nl3 r2 = r2.F()
            java.lang.String r3 = "Failed to load string array from metadata: resource not found"
            r2.b(r3, r1)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zr3.D(java.lang.String):java.util.List");
    }

    @DexIgnore
    public final boolean E() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = e().getApplicationInfo();
                    String a2 = nf2.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.d = Boolean.valueOf(str != null && str.equals(a2));
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        d().F().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    @DexIgnore
    public final boolean F(String str) {
        return "1".equals(this.c.a(str, "gaia_collection_enabled"));
    }

    @DexIgnore
    public final boolean G() {
        b();
        Boolean A = A("firebase_analytics_collection_deactivated");
        return A != null && A.booleanValue();
    }

    @DexIgnore
    public final boolean H(String str) {
        return "1".equals(this.c.a(str, "measurement.event_sampling_enabled"));
    }

    @DexIgnore
    public final Boolean I() {
        f();
        Boolean A = A("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(A == null || A.booleanValue());
    }

    @DexIgnore
    public final boolean J(String str) {
        return y(str, xg3.K);
    }

    @DexIgnore
    public final Boolean K() {
        f();
        if (!v73.a() || !s(xg3.C0)) {
            return Boolean.TRUE;
        }
        Boolean A = A("google_analytics_automatic_screen_reporting_enabled");
        return Boolean.valueOf(A == null || A.booleanValue());
    }

    @DexIgnore
    public final String L(String str) {
        zk3<String> zk3 = xg3.L;
        return str == null ? zk3.a(null) : zk3.a(this.c.a(str, zk3.b()));
    }

    @DexIgnore
    public final String O() {
        return a("debug.firebase.analytics.app", "");
    }

    @DexIgnore
    public final String P() {
        return a("debug.deferred.deeplink", "");
    }

    @DexIgnore
    public final boolean Q() {
        if (this.b == null) {
            Boolean A = A("app_measurement_lite");
            this.b = A;
            if (A == null) {
                this.b = Boolean.FALSE;
            }
        }
        return this.b.booleanValue() || !this.f1780a.M();
    }

    @DexIgnore
    public final Bundle R() {
        try {
            if (e().getPackageManager() == null) {
                d().F().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo c2 = ag2.a(e()).c(e().getPackageName(), 128);
            if (c2 != null) {
                return c2.metaData;
            }
            d().F().a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            d().F().b("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    @DexIgnore
    public final String a(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
        } catch (ClassNotFoundException e) {
            d().F().b("Could not find SystemProperties class", e);
        } catch (NoSuchMethodException e2) {
            d().F().b("Could not find SystemProperties.get() method", e2);
        } catch (IllegalAccessException e3) {
            d().F().b("Could not access SystemProperties.get()", e3);
        } catch (InvocationTargetException e4) {
            d().F().b("SystemProperties.get() threw an exception", e4);
        }
        return str2;
    }

    @DexIgnore
    public final int n(String str) {
        return o(str, xg3.I, 25, 100);
    }

    @DexIgnore
    public final int o(String str, zk3<Integer> zk3, int i, int i2) {
        return Math.max(Math.min(u(str, zk3), i2), i);
    }

    @DexIgnore
    public final long p(String str, zk3<Long> zk3) {
        if (str == null) {
            return zk3.a(null).longValue();
        }
        String a2 = this.c.a(str, zk3.b());
        if (TextUtils.isEmpty(a2)) {
            return zk3.a(null).longValue();
        }
        try {
            return zk3.a(Long.valueOf(Long.parseLong(a2))).longValue();
        } catch (NumberFormatException e) {
            return zk3.a(null).longValue();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002e, code lost:
        if (android.text.TextUtils.isEmpty(r0) != false) goto L_0x0030;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0089  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String q(com.fossil.ll3 r7) {
        /*
            r6 = this;
            r4 = 0
            android.net.Uri$Builder r2 = new android.net.Uri$Builder
            r2.<init>()
            java.lang.String r0 = r7.A()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0091
            boolean r0 = com.fossil.i73.a()
            if (r0 == 0) goto L_0x0030
            com.fossil.zr3 r0 = r6.m()
            java.lang.String r1 = r7.t()
            com.fossil.zk3<java.lang.Boolean> r3 = com.fossil.xg3.o0
            boolean r0 = r0.y(r1, r3)
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = r7.G()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0091
        L_0x0030:
            java.lang.String r0 = r7.D()
            r1 = r0
        L_0x0035:
            com.fossil.zk3<java.lang.String> r0 = com.fossil.xg3.e
            java.lang.Object r0 = r0.a(r4)
            java.lang.String r0 = (java.lang.String) r0
            android.net.Uri$Builder r3 = r2.scheme(r0)
            com.fossil.zk3<java.lang.String> r0 = com.fossil.xg3.f
            java.lang.Object r0 = r0.a(r4)
            java.lang.String r0 = (java.lang.String) r0
            android.net.Uri$Builder r3 = r3.encodedAuthority(r0)
            java.lang.String r0 = java.lang.String.valueOf(r1)
            int r1 = r0.length()
            if (r1 == 0) goto L_0x0089
            java.lang.String r1 = "config/app/"
            java.lang.String r0 = r1.concat(r0)
        L_0x005d:
            android.net.Uri$Builder r0 = r3.path(r0)
            java.lang.String r1 = "app_instance_id"
            java.lang.String r3 = r7.x()
            android.net.Uri$Builder r0 = r0.appendQueryParameter(r1, r3)
            java.lang.String r1 = "platform"
            java.lang.String r3 = "android"
            android.net.Uri$Builder r0 = r0.appendQueryParameter(r1, r3)
            java.lang.String r1 = "gmp_version"
            long r4 = r6.C()
            java.lang.String r3 = java.lang.String.valueOf(r4)
            r0.appendQueryParameter(r1, r3)
            android.net.Uri r0 = r2.build()
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0089:
            java.lang.String r0 = new java.lang.String
            java.lang.String r1 = "config/app/"
            r0.<init>(r1)
            goto L_0x005d
        L_0x0091:
            r1 = r0
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zr3.q(com.fossil.ll3):java.lang.String");
    }

    @DexIgnore
    public final void r(hg3 hg3) {
        this.c = hg3;
    }

    @DexIgnore
    public final boolean s(zk3<Boolean> zk3) {
        return y(null, zk3);
    }

    @DexIgnore
    public final int t(String str) {
        if (!h53.a() || !y(null, xg3.K0)) {
            return 500;
        }
        return o(str, xg3.H, 500, 2000);
    }

    @DexIgnore
    public final int u(String str, zk3<Integer> zk3) {
        if (str == null) {
            return zk3.a(null).intValue();
        }
        String a2 = this.c.a(str, zk3.b());
        if (TextUtils.isEmpty(a2)) {
            return zk3.a(null).intValue();
        }
        try {
            return zk3.a(Integer.valueOf(Integer.parseInt(a2))).intValue();
        } catch (NumberFormatException e) {
            return zk3.a(null).intValue();
        }
    }

    @DexIgnore
    public final double v(String str, zk3<Double> zk3) {
        if (str == null) {
            return zk3.a(null).doubleValue();
        }
        String a2 = this.c.a(str, zk3.b());
        if (TextUtils.isEmpty(a2)) {
            return zk3.a(null).doubleValue();
        }
        try {
            return zk3.a(Double.valueOf(Double.parseDouble(a2))).doubleValue();
        } catch (NumberFormatException e) {
            return zk3.a(null).doubleValue();
        }
    }

    @DexIgnore
    public final int w(String str) {
        return u(str, xg3.o);
    }

    @DexIgnore
    public final int x(String str) {
        if (!h53.a() || !y(null, xg3.K0)) {
            return 25;
        }
        return o(str, xg3.G, 25, 100);
    }

    @DexIgnore
    public final boolean y(String str, zk3<Boolean> zk3) {
        if (str == null) {
            return zk3.a(null).booleanValue();
        }
        String a2 = this.c.a(str, zk3.b());
        return TextUtils.isEmpty(a2) ? zk3.a(null).booleanValue() : zk3.a(Boolean.valueOf(Boolean.parseBoolean(a2))).booleanValue();
    }

    @DexIgnore
    public final int z() {
        return (!h53.a() || !m().y(null, xg3.L0) || k().J0() < 201500) ? 25 : 100;
    }
}
