package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx7<T> extends sv7<T> {
    @DexIgnore
    public /* final */ qn7<tl7> e;

    @DexIgnore
    public hx7(tn7 tn7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7) {
        super(tn7, false);
        this.e = xn7.b(vp7, this, this);
    }

    @DexIgnore
    @Override // com.fossil.au7
    public void y0() {
        d08.a(this.e, this);
    }
}
