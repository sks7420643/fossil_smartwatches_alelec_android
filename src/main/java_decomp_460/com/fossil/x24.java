package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.lang.Enum;
import java.util.Collection;
import java.util.EnumSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x24<E extends Enum<E>> extends h34<E> {
    @DexIgnore
    public /* final */ transient EnumSet<E> c;
    @DexIgnore
    @LazyInit
    public transient int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E extends Enum<E>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumSet<E> delegate;

        @DexIgnore
        public b(EnumSet<E> enumSet) {
            this.delegate = enumSet;
        }

        @DexIgnore
        public Object readResolve() {
            return new x24(this.delegate.clone());
        }
    }

    @DexIgnore
    public x24(EnumSet<E> enumSet) {
        this.c = enumSet;
    }

    @DexIgnore
    public static h34 asImmutable(EnumSet enumSet) {
        int size = enumSet.size();
        return size != 0 ? size != 1 ? new x24(enumSet) : h34.of(o34.f(enumSet)) : h34.of();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        return this.c.contains(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof x24) {
            collection = ((x24) collection).c;
        }
        return this.c.containsAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.h34
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof x24) {
            obj = ((x24) obj).c;
        }
        return this.c.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.h34
    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = this.c.hashCode();
        this.d = hashCode;
        return hashCode;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.c.isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.h34
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
    public h54<E> iterator() {
        return p34.x(this.c.iterator());
    }

    @DexIgnore
    public int size() {
        return this.c.size();
    }

    @DexIgnore
    public String toString() {
        return this.c.toString();
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.h34
    public Object writeReplace() {
        return new b(this.c);
    }
}
