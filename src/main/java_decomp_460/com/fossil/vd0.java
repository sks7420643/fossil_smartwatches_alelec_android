package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vd0 {

    @DexIgnore
    public interface a {
        @DexIgnore
        Object c();  // void declaration

        @DexIgnore
        Object d();  // void declaration

        @DexIgnore
        Object e();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends MediaBrowser.ConnectionCallback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ T f3751a;

        @DexIgnore
        public b(T t) {
            this.f3751a = t;
        }

        @DexIgnore
        public void onConnected() {
            this.f3751a.d();
        }

        @DexIgnore
        public void onConnectionFailed() {
            this.f3751a.e();
        }

        @DexIgnore
        public void onConnectionSuspended() {
            this.f3751a.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public static Object a(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getDescription();
        }

        @DexIgnore
        public static int b(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getFlags();
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void c(String str, List<?> list);

        @DexIgnore
        void d(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<T extends d> extends MediaBrowser.SubscriptionCallback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ T f3752a;

        @DexIgnore
        public e(T t) {
            this.f3752a = t;
        }

        @DexIgnore
        @Override // android.media.browse.MediaBrowser.SubscriptionCallback
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list) {
            this.f3752a.c(str, list);
        }

        @DexIgnore
        public void onError(String str) {
            this.f3752a.d(str);
        }
    }

    @DexIgnore
    public static void a(Object obj) {
        ((MediaBrowser) obj).connect();
    }

    @DexIgnore
    public static Object b(Context context, ComponentName componentName, Object obj, Bundle bundle) {
        return new MediaBrowser(context, componentName, (MediaBrowser.ConnectionCallback) obj, bundle);
    }

    @DexIgnore
    public static Object c(a aVar) {
        return new b(aVar);
    }

    @DexIgnore
    public static Object d(d dVar) {
        return new e(dVar);
    }

    @DexIgnore
    public static void e(Object obj) {
        ((MediaBrowser) obj).disconnect();
    }

    @DexIgnore
    public static Bundle f(Object obj) {
        return ((MediaBrowser) obj).getExtras();
    }

    @DexIgnore
    public static Object g(Object obj) {
        return ((MediaBrowser) obj).getSessionToken();
    }
}
