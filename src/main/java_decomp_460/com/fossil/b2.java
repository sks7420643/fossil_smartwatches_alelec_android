package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b2 implements Parcelable.Creator<c2> {
    @DexIgnore
    public /* synthetic */ b2(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public c2 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            lt valueOf = lt.valueOf(readString);
            parcel.setDataPosition(0);
            switch (a2.f184a[valueOf.ordinal()]) {
                case 1:
                    return q2.CREATOR.a(parcel);
                case 2:
                    return o2.CREATOR.a(parcel);
                case 3:
                    return k2.CREATOR.a(parcel);
                case 4:
                    return y1.CREATOR.a(parcel);
                case 5:
                    return u2.CREATOR.a(parcel);
                case 6:
                    return g2.CREATOR.b(parcel);
                case 7:
                    return w2.CREATOR.a(parcel);
                case 8:
                    return s2.CREATOR.b(parcel);
                case 9:
                    return z1.CREATOR.a(parcel);
                case 10:
                    return e2.CREATOR.a(parcel);
                case 11:
                    return new i2(parcel);
                case 12:
                    return new m2(parcel);
                default:
                    throw new al7();
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public c2[] newArray(int i) {
        return new c2[i];
    }
}
