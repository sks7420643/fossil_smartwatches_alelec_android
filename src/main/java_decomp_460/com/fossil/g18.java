package com.fossil;

import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g18 {
    @DexIgnore
    public static /* final */ d18[] e; // = {d18.q, d18.r, d18.s, d18.t, d18.u, d18.k, d18.m, d18.l, d18.n, d18.p, d18.o};
    @DexIgnore
    public static /* final */ d18[] f; // = {d18.q, d18.r, d18.s, d18.t, d18.u, d18.k, d18.m, d18.l, d18.n, d18.p, d18.o, d18.i, d18.j, d18.g, d18.h, d18.e, d18.f, d18.d};
    @DexIgnore
    public static /* final */ g18 g;
    @DexIgnore
    public static /* final */ g18 h; // = new a(false).a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f1247a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ String[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1248a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a(g18 g18) {
            this.f1248a = g18.f1247a;
            this.b = g18.c;
            this.c = g18.d;
            this.d = g18.b;
        }

        @DexIgnore
        public a(boolean z) {
            this.f1248a = z;
        }

        @DexIgnore
        public g18 a() {
            return new g18(this);
        }

        @DexIgnore
        public a b(String... strArr) {
            if (!this.f1248a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        @DexIgnore
        public a c(d18... d18Arr) {
            if (this.f1248a) {
                String[] strArr = new String[d18Arr.length];
                for (int i = 0; i < d18Arr.length; i++) {
                    strArr[i] = d18Arr[i].f721a;
                }
                b(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public a d(boolean z) {
            if (this.f1248a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public a e(String... strArr) {
            if (!this.f1248a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        @DexIgnore
        public a f(y18... y18Arr) {
            if (this.f1248a) {
                String[] strArr = new String[y18Arr.length];
                for (int i = 0; i < y18Arr.length; i++) {
                    strArr[i] = y18Arr[i].javaName;
                }
                e(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }
    }

    /*
    static {
        a aVar = new a(true);
        aVar.c(e);
        aVar.f(y18.TLS_1_3, y18.TLS_1_2);
        aVar.d(true);
        aVar.a();
        a aVar2 = new a(true);
        aVar2.c(f);
        aVar2.f(y18.TLS_1_3, y18.TLS_1_2, y18.TLS_1_1, y18.TLS_1_0);
        aVar2.d(true);
        g = aVar2.a();
        a aVar3 = new a(true);
        aVar3.c(f);
        aVar3.f(y18.TLS_1_0);
        aVar3.d(true);
        aVar3.a();
    }
    */

    @DexIgnore
    public g18(a aVar) {
        this.f1247a = aVar.f1248a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.b = aVar.d;
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        g18 e2 = e(sSLSocket, z);
        String[] strArr = e2.d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = e2.c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    @DexIgnore
    public List<d18> b() {
        String[] strArr = this.c;
        if (strArr != null) {
            return d18.b(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean c(SSLSocket sSLSocket) {
        if (!this.f1247a) {
            return false;
        }
        String[] strArr = this.d;
        if (strArr != null && !b28.B(b28.p, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.c;
        return strArr2 == null || b28.B(d18.b, strArr2, sSLSocket.getEnabledCipherSuites());
    }

    @DexIgnore
    public boolean d() {
        return this.f1247a;
    }

    @DexIgnore
    public final g18 e(SSLSocket sSLSocket, boolean z) {
        String[] z2 = this.c != null ? b28.z(d18.b, sSLSocket.getEnabledCipherSuites(), this.c) : sSLSocket.getEnabledCipherSuites();
        String[] z3 = this.d != null ? b28.z(b28.p, sSLSocket.getEnabledProtocols(), this.d) : sSLSocket.getEnabledProtocols();
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int w = b28.w(d18.b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && w != -1) {
            z2 = b28.i(z2, supportedCipherSuites[w]);
        }
        a aVar = new a(this);
        aVar.b(z2);
        aVar.e(z3);
        return aVar.a();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof g18)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        g18 g18 = (g18) obj;
        boolean z = this.f1247a;
        if (z == g18.f1247a) {
            return !z || (Arrays.equals(this.c, g18.c) && Arrays.equals(this.d, g18.d) && this.b == g18.b);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.b;
    }

    @DexIgnore
    public List<y18> g() {
        String[] strArr = this.d;
        if (strArr != null) {
            return y18.forJavaNames(strArr);
        }
        return null;
    }

    @DexIgnore
    public int hashCode() {
        if (this.f1247a) {
            return ((((Arrays.hashCode(this.c) + 527) * 31) + Arrays.hashCode(this.d)) * 31) + (!this.b ? 1 : 0);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        if (!this.f1247a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.c != null ? b().toString() : "[all enabled]";
        if (this.d != null) {
            str = g().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.b + ")";
    }
}
