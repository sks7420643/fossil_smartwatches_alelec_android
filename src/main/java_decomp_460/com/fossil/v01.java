package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3690a; // = x01.f("InputMerger");

    @DexIgnore
    public static v01 a(String str) {
        try {
            return (v01) Class.forName(str).newInstance();
        } catch (Exception e) {
            x01 c = x01.c();
            String str2 = f3690a;
            c.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract r01 b(List<r01> list);
}
