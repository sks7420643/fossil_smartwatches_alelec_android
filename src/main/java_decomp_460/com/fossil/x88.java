package com.fossil;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x88<T> implements e88<T, RequestBody> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ x88<Object> f4076a; // = new x88<>();
    @DexIgnore
    public static /* final */ r18 b; // = r18.d("text/plain; charset=UTF-8");

    @DexIgnore
    /* renamed from: b */
    public RequestBody a(T t) throws IOException {
        return RequestBody.d(b, String.valueOf(t));
    }
}
