package com.fossil;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz2 extends WeakReference<Throwable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1698a;

    @DexIgnore
    public iz2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f1698a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == iz2.class) {
            if (this == obj) {
                return true;
            }
            iz2 iz2 = (iz2) obj;
            return this.f1698a == iz2.f1698a && get() == iz2.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.f1698a;
    }
}
