package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.kk7;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.RingStyleRepository_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository_Factory;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FileRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.QuickResponseRepository_Factory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.ThemeRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.UserSettingDao;
import com.portfolio.platform.data.source.UserSettingDatabase;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource_Factory;
import com.portfolio.platform.deeplink.DeepLinkActivity;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListViewModel;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq4 implements ro4 {
    @DexIgnore
    public Provider<AlarmsRepository> A;
    @DexIgnore
    public Provider<tt4> A0;
    @DexIgnore
    public Provider<br5> A1;
    @DexIgnore
    public Provider<du5> A2;
    @DexIgnore
    public Provider<po4> A3;
    @DexIgnore
    public Provider<bk5> B;
    @DexIgnore
    public Provider<ft4> B0;
    @DexIgnore
    public Provider<FitnessDataRepository> B1;
    @DexIgnore
    public Provider<au5> B2;
    @DexIgnore
    public Provider<NotificationSettingsDao> B3;
    @DexIgnore
    public Provider<sk5> C;
    @DexIgnore
    public Provider<bu4> C0;
    @DexIgnore
    public Provider<uk5> C1;
    @DexIgnore
    public Provider<gv6> C2;
    @DexIgnore
    public Provider<ServerSettingDataSource> C3;
    @DexIgnore
    public Provider<SummariesRepository> D;
    @DexIgnore
    public Provider<cu4> D0;
    @DexIgnore
    public Provider<ThirdPartyRepository> D1;
    @DexIgnore
    public Provider<ax4> D2;
    @DexIgnore
    public Provider<ServerSettingDataSource> D3;
    @DexIgnore
    public Provider<SleepSummariesRepository> E;
    @DexIgnore
    public Provider<du4> E0;
    @DexIgnore
    public Provider<PushPendingDataWorker.b> E1;
    @DexIgnore
    public Provider<pv4> E2;
    @DexIgnore
    public Provider<n47> E3;
    @DexIgnore
    public Provider<GuestApiService> F;
    @DexIgnore
    public Provider<FileDatabase> F0;
    @DexIgnore
    public Provider<WatchFaceRemoteDataSource> F1;
    @DexIgnore
    public Provider<cv4> F2;
    @DexIgnore
    public Provider<no4> G;
    @DexIgnore
    public Provider<FileDao> G0;
    @DexIgnore
    public Provider<WatchFaceRepository> G1;
    @DexIgnore
    public Provider<ey4> G2;
    @DexIgnore
    public Provider<NotificationsDataSource> H;
    @DexIgnore
    public Provider<cn5> H0;
    @DexIgnore
    public Provider<bo5> H1;
    @DexIgnore
    public Provider<bw4> H2;
    @DexIgnore
    public Provider<NotificationsRepository> I;
    @DexIgnore
    public Provider<FileRepository> I0;
    @DexIgnore
    public Provider<AddressDatabase> I1;
    @DexIgnore
    public Provider<vw4> I2;
    @DexIgnore
    public Provider<DeviceDatabase> J;
    @DexIgnore
    public Provider<vt4> J0;
    @DexIgnore
    public Provider<AddressDao> J1;
    @DexIgnore
    public Provider<mu4> J2;
    @DexIgnore
    public Provider<DeviceDao> K;
    @DexIgnore
    public Provider<SecureApiService> K0;
    @DexIgnore
    public Provider<LocationSource> K1;
    @DexIgnore
    public Provider<xu4> K2;
    @DexIgnore
    public Provider<SkuDao> L;
    @DexIgnore
    public Provider<so5> L0;
    @DexIgnore
    public Provider<fs5> L1;
    @DexIgnore
    public Provider<lx4> L2;
    @DexIgnore
    public Provider<ApiService2Dot1> M;
    @DexIgnore
    public Provider<uo5> M0;
    @DexIgnore
    public Provider<aq5> M1;
    @DexIgnore
    public Provider<wx4> M2;
    @DexIgnore
    public Provider<SecureApiService2Dot1> N;
    @DexIgnore
    public Provider<j97> N0;
    @DexIgnore
    public Provider<dq5> N1;
    @DexIgnore
    public Provider<qx4> N2;
    @DexIgnore
    public Provider<DeviceRemoteDataSource> O;
    @DexIgnore
    public Provider<k97> O0;
    @DexIgnore
    public Provider<yr4> O1;
    @DexIgnore
    public Provider<ow4> O2;
    @DexIgnore
    public Provider<DeviceRepository> P;
    @DexIgnore
    public Provider<ck5> P0;
    @DexIgnore
    public Provider<a37> P1;
    @DexIgnore
    public Provider<uv4> P2;
    @DexIgnore
    public Provider<ContentResolver> Q;
    @DexIgnore
    public Provider<CategoryDatabase> Q0;
    @DexIgnore
    public Provider<or5> Q1;
    @DexIgnore
    public Provider<iw4> Q2;
    @DexIgnore
    public Provider<uq4> R;
    @DexIgnore
    public Provider<CategoryDao> R0;
    @DexIgnore
    public Provider<hn5> R1;
    @DexIgnore
    public Provider<iv4> R2;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> S;
    @DexIgnore
    public Provider<CategoryRemoteDataSource> S0;
    @DexIgnore
    public Provider<rl5> S1;
    @DexIgnore
    public Provider<my4> S2;
    @DexIgnore
    public Provider<HybridPresetDao> T;
    @DexIgnore
    public Provider<CategoryRepository> T0;
    @DexIgnore
    public Provider<yr5> T1;
    @DexIgnore
    public Provider<mt5> T2;
    @DexIgnore
    public Provider<HybridPresetRemoteDataSource> U;
    @DexIgnore
    public Provider<WatchAppRepository> U0;
    @DexIgnore
    public Provider<un5> U1;
    @DexIgnore
    public Provider<n26> U2;
    @DexIgnore
    public Provider<HybridPresetRepository> V;
    @DexIgnore
    public Provider<ComplicationRepository> V0;
    @DexIgnore
    public Provider<xn5> V1;
    @DexIgnore
    public Provider<lt6> V2;
    @DexIgnore
    public Provider<ActivitiesRepository> W;
    @DexIgnore
    public Provider<DianaAppSettingRepository> W0;
    @DexIgnore
    public Provider<wn5> W1;
    @DexIgnore
    public Provider<gs6> W2;
    @DexIgnore
    public Provider<ShortcutApiService> X;
    @DexIgnore
    public Provider<DianaWatchFaceRemoteDataSource> X0;
    @DexIgnore
    public Provider<MicroAppLastSettingRepository> X1;
    @DexIgnore
    public Provider<as6> X2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Y;
    @DexIgnore
    public Provider<DianaWatchFaceRepository> Y0;
    @DexIgnore
    public Provider<oq4> Y1;
    @DexIgnore
    public Provider<zs6> Y2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Z;
    @DexIgnore
    public Provider<RingStyleRemoteDataSource> Z0;
    @DexIgnore
    public Provider<FirmwareFileRepository> Z1;
    @DexIgnore
    public Provider<ur6> Z2;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1946a;
    @DexIgnore
    public Provider<MicroAppSettingRepository> a0;
    @DexIgnore
    public Provider<RingStyleRepository> a1;
    @DexIgnore
    public Provider<pu5> a2;
    @DexIgnore
    public Provider<or6> a3;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<SleepSessionsRepository> b0;
    @DexIgnore
    public Provider<WatchLocalizationRepository> b1;
    @DexIgnore
    public Provider<InAppNotificationDatabase> b2;
    @DexIgnore
    public Provider<ir6> b3;
    @DexIgnore
    public Provider<on5> c;
    @DexIgnore
    public Provider<GoalTrackingRepository> c0;
    @DexIgnore
    public Provider<r77> c1;
    @DexIgnore
    public Provider<InAppNotificationDao> c2;
    @DexIgnore
    public Provider<ns6> c3;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<vn5> d0;
    @DexIgnore
    public Provider<s77> d1;
    @DexIgnore
    public Provider<InAppNotificationRepository> d2;
    @DexIgnore
    public Provider<ts6> d3;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> e0;
    @DexIgnore
    public Provider<yo5> e1;
    @DexIgnore
    public Provider<pu6> e2;
    @DexIgnore
    public Provider<ft6> e3;
    @DexIgnore
    public Provider<QuickResponseDatabase> f;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> f0;
    @DexIgnore
    public Provider<zo5> f1;
    @DexIgnore
    public Provider<GoogleApiService> f2;
    @DexIgnore
    public Provider<st6> f3;
    @DexIgnore
    public Provider<QuickResponseMessageDao> g;
    @DexIgnore
    public Provider<MicroAppDao> g0;
    @DexIgnore
    public Provider<wy6> g1;
    @DexIgnore
    public Provider<DownloadServiceApi> g2;
    @DexIgnore
    public Provider<j26> g3;
    @DexIgnore
    public Provider<QuickResponseSenderDao> h;
    @DexIgnore
    public Provider<MicroAppRemoteDataSource> h0;
    @DexIgnore
    public Provider<yy6> h1;
    @DexIgnore
    public Provider<ct0> h2;
    @DexIgnore
    public Provider<kn6> h3;
    @DexIgnore
    public Provider<QuickResponseRepository> i;
    @DexIgnore
    public Provider<MicroAppRepository> i0;
    @DexIgnore
    public Provider<dt5> i1;
    @DexIgnore
    public Provider<pl5> i2;
    @DexIgnore
    public Provider<pn6> i3;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> j;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> j0;
    @DexIgnore
    public Provider<q27> j1;
    @DexIgnore
    public Provider<lo4> j2;
    @DexIgnore
    public Provider<jt5> j3;
    @DexIgnore
    public Provider<vr5> k;
    @DexIgnore
    public Provider<HeartRateSampleRepository> k0;
    @DexIgnore
    public Provider<d37> k1;
    @DexIgnore
    public Provider<k76> k2;
    @DexIgnore
    public Provider<hp6> k3;
    @DexIgnore
    public Provider<AuthApiGuestService> l;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> l0;
    @DexIgnore
    public Provider<yt5> l1;
    @DexIgnore
    public Provider<sb6> l2;
    @DexIgnore
    public Provider<wb7> l3;
    @DexIgnore
    public Provider<qq5> m;
    @DexIgnore
    public Provider<WorkoutSessionRepository> m0;
    @DexIgnore
    public Provider<WorkoutSettingRemoteDataSource> m1;
    @DexIgnore
    public Provider<xu5> m2;
    @DexIgnore
    public Provider<WatchFaceGalleryViewModel> m3;
    @DexIgnore
    public Provider<uq5> n;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> n0;
    @DexIgnore
    public Provider<WorkoutSettingRepository> n1;
    @DexIgnore
    public Provider<vu5> n2;
    @DexIgnore
    public Provider<zm5> n3;
    @DexIgnore
    public Provider<ApiServiceV2> o;
    @DexIgnore
    public Provider<BuddyChallengeDatabase> o0;
    @DexIgnore
    public Provider<at5> o1;
    @DexIgnore
    public Provider<bp6> o2;
    @DexIgnore
    public Provider<a87> o3;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> p;
    @DexIgnore
    public Provider<jt4> p0;
    @DexIgnore
    public Provider<mj5> p1;
    @DexIgnore
    public Provider<o96> p2;
    @DexIgnore
    public Provider<h87> p3;
    @DexIgnore
    public Provider<DianaPresetRepository> q;
    @DexIgnore
    public Provider<fu4> q0;
    @DexIgnore
    public Provider<AppSettingsDatabase> q1;
    @DexIgnore
    public Provider<s96> q2;
    @DexIgnore
    public Provider<m87> q3;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> r;
    @DexIgnore
    public Provider<gu4> r0;
    @DexIgnore
    public Provider<lr4> r1;
    @DexIgnore
    public Provider<ot5> r2;
    @DexIgnore
    public Provider<p97> r3;
    @DexIgnore
    public Provider<CustomizeRealDataDao> s;
    @DexIgnore
    public Provider<hu4> s0;
    @DexIgnore
    public Provider<nr4> s1;
    @DexIgnore
    public Provider<vt5> s2;
    @DexIgnore
    public Provider<u97> s3;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> t;
    @DexIgnore
    public Provider<ys4> t0;
    @DexIgnore
    public Provider<or4> t1;
    @DexIgnore
    public Provider<qt5> t2;
    @DexIgnore
    public Provider<ba7> t3;
    @DexIgnore
    public Provider<AuthApiUserService> u;
    @DexIgnore
    public Provider<xt4> u0;
    @DexIgnore
    public Provider<pr4> u1;
    @DexIgnore
    public Provider<f17> u2;
    @DexIgnore
    public Provider<WatchFaceListViewModel> u3;
    @DexIgnore
    public Provider<UserRemoteDataSource> v;
    @DexIgnore
    public Provider<yt4> v0;
    @DexIgnore
    public Provider<dr5> v1;
    @DexIgnore
    public Provider<na6> v2;
    @DexIgnore
    public Provider<sa7> v3;
    @DexIgnore
    public Provider<UserSettingDatabase> w;
    @DexIgnore
    public Provider<zt4> w0;
    @DexIgnore
    public Provider<ThemeDatabase> w1;
    @DexIgnore
    public Provider<ka6> w2;
    @DexIgnore
    public Provider<hh5> w3;
    @DexIgnore
    public Provider<UserSettingDao> x;
    @DexIgnore
    public Provider<qs4> x0;
    @DexIgnore
    public Provider<ThemeDao> x1;
    @DexIgnore
    public Provider<cr6> x2;
    @DexIgnore
    public Provider<do5> x3;
    @DexIgnore
    public Provider<UserRepository> y;
    @DexIgnore
    public Provider<rt4> y0;
    @DexIgnore
    public Provider<ThemeRepository> y1;
    @DexIgnore
    public Provider<fu6> y2;
    @DexIgnore
    public Provider<lv6> y3;
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> z;
    @DexIgnore
    public Provider<st4> z0;
    @DexIgnore
    public Provider<ApplicationEventListener> z1;
    @DexIgnore
    public Provider<mu6> z2;
    @DexIgnore
    public Provider<Map<Class<? extends ts0>, Provider<ts0>>> z3;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements j17 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ m17 f1947a;

        @DexIgnore
        public a0(m17 m17) {
            this.f1947a = m17;
        }

        @DexIgnore
        @Override // com.fossil.j17
        public void a(CalibrationActivity calibrationActivity) {
            c(calibrationActivity);
        }

        @DexIgnore
        public final o17 b() {
            o17 a2 = q17.a((PortfolioApp) kq4.this.d.get(), n17.a(this.f1947a), (UserRepository) kq4.this.y.get(), (ct0) kq4.this.h2.get(), (pl5) kq4.this.i2.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity c(CalibrationActivity calibrationActivity) {
            ms5.e(calibrationActivity, (UserRepository) kq4.this.y.get());
            ms5.d(calibrationActivity, (on5) kq4.this.c.get());
            ms5.a(calibrationActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(calibrationActivity, (oq4) kq4.this.Y1.get());
            ms5.b(calibrationActivity, new gu5());
            i17.a(calibrationActivity, b());
            return calibrationActivity;
        }

        @DexIgnore
        public final o17 d(o17 o17) {
            r17.a(o17);
            return o17;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements e07 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ f07 f1948a;

        @DexIgnore
        public a1(f07 f07) {
            this.f1948a = f07;
        }

        @DexIgnore
        @Override // com.fossil.e07
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            d(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final b07 b() {
            b07 a2 = c07.a(g07.a(this.f1948a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final c37 c() {
            return new c37((UserRepository) kq4.this.y.get());
        }

        @DexIgnore
        public final EmailOtpVerificationActivity d(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            ms5.e(emailOtpVerificationActivity, (UserRepository) kq4.this.y.get());
            ms5.d(emailOtpVerificationActivity, (on5) kq4.this.c.get());
            ms5.a(emailOtpVerificationActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(emailOtpVerificationActivity, (oq4) kq4.this.Y1.get());
            ms5.b(emailOtpVerificationActivity, new gu5());
            xz6.a(emailOtpVerificationActivity, b());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final b07 e(b07 b07) {
            d07.a(b07, kq4.this.G4());
            d07.b(b07, c());
            d07.c(b07);
            return b07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a2 implements t16 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ y16 f1949a;

        @DexIgnore
        public a2(y16 y16) {
            this.f1949a = y16;
        }

        @DexIgnore
        @Override // com.fossil.t16
        public void a(o06 o06) {
        }

        @DexIgnore
        @Override // com.fossil.t16
        public void b(z06 z06) {
            e(z06);
        }

        @DexIgnore
        @Override // com.fossil.t16
        public void c(w16 w16) {
            f(w16);
        }

        @DexIgnore
        public final a26 d() {
            a26 a2 = b26.a(z16.a(this.f1949a));
            g(a2);
            return a2;
        }

        @DexIgnore
        public final z06 e(z06 z06) {
            a16.a(z06, d());
            a16.b(z06, (po4) kq4.this.A3.get());
            return z06;
        }

        @DexIgnore
        public final w16 f(w16 w16) {
            x16.a(w16, (po4) kq4.this.A3.get());
            return w16;
        }

        @DexIgnore
        public final a26 g(a26 a26) {
            c26.a(a26);
            return a26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a3 implements u96 {
        @DexIgnore
        public a3(z96 z96) {
        }

        @DexIgnore
        @Override // com.fossil.u96
        public void a(x96 x96) {
            b(x96);
        }

        @DexIgnore
        public final x96 b(x96 x96) {
            y96.a(x96, (po4) kq4.this.A3.get());
            return x96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements fo6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ jo6 f1951a;

        @DexIgnore
        public b(jo6 jo6) {
            this.f1951a = jo6;
        }

        @DexIgnore
        @Override // com.fossil.fo6
        public void a(AboutActivity aboutActivity) {
            c(aboutActivity);
        }

        @DexIgnore
        public final lo6 b() {
            lo6 a2 = mo6.a(ko6.a(this.f1951a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity c(AboutActivity aboutActivity) {
            ms5.e(aboutActivity, (UserRepository) kq4.this.y.get());
            ms5.d(aboutActivity, (on5) kq4.this.c.get());
            ms5.a(aboutActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(aboutActivity, (oq4) kq4.this.Y1.get());
            ms5.b(aboutActivity, new gu5());
            eo6.a(aboutActivity, b());
            return aboutActivity;
        }

        @DexIgnore
        public final lo6 d(lo6 lo6) {
            no6.a(lo6);
            return lo6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements nl6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rl6 f1952a;

        @DexIgnore
        public b0(rl6 rl6) {
            this.f1952a = rl6;
        }

        @DexIgnore
        @Override // com.fossil.nl6
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            c(caloriesDetailActivity);
        }

        @DexIgnore
        public final tl6 b() {
            tl6 a2 = ul6.a(sl6.a(this.f1952a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (UserRepository) kq4.this.y.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (FileRepository) kq4.this.I0.get(), (no4) kq4.this.G.get(), (PortfolioApp) kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity c(CaloriesDetailActivity caloriesDetailActivity) {
            ms5.e(caloriesDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(caloriesDetailActivity, (on5) kq4.this.c.get());
            ms5.a(caloriesDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(caloriesDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(caloriesDetailActivity, new gu5());
            ml6.a(caloriesDetailActivity, b());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public final tl6 d(tl6 tl6) {
            vl6.a(tl6);
            return tl6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements ov6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rv6 f1953a;

        @DexIgnore
        public b1(rv6 rv6) {
            this.f1953a = rv6;
        }

        @DexIgnore
        @Override // com.fossil.ov6
        public void a(ExploreWatchActivity exploreWatchActivity) {
            c(exploreWatchActivity);
        }

        @DexIgnore
        public final tv6 b() {
            tv6 a2 = uv6.a(sv6.a(this.f1953a), kq4.this.a4(), (DeviceRepository) kq4.this.P.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity c(ExploreWatchActivity exploreWatchActivity) {
            ms5.e(exploreWatchActivity, (UserRepository) kq4.this.y.get());
            ms5.d(exploreWatchActivity, (on5) kq4.this.c.get());
            ms5.a(exploreWatchActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(exploreWatchActivity, (oq4) kq4.this.Y1.get());
            ms5.b(exploreWatchActivity, new gu5());
            nv6.a(exploreWatchActivity, b());
            return exploreWatchActivity;
        }

        @DexIgnore
        public final tv6 d(tv6 tv6) {
            vv6.a(tv6);
            return tv6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b2 implements w26 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ b36 f1954a;

        @DexIgnore
        public b2(b36 b36) {
            this.f1954a = b36;
        }

        @DexIgnore
        @Override // com.fossil.w26
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            c(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final d36 b() {
            d36 a2 = e36.a(c36.a(this.f1954a), (on5) kq4.this.c.get(), (RemindersSettingsDatabase) kq4.this.n0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity c(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            ms5.e(notificationWatchRemindersActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationWatchRemindersActivity, (on5) kq4.this.c.get());
            ms5.a(notificationWatchRemindersActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationWatchRemindersActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationWatchRemindersActivity, new gu5());
            v26.a(notificationWatchRemindersActivity, b());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public final d36 d(d36 d36) {
            f36.a(d36);
            return d36;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b3 implements e87 {
        @DexIgnore
        public b3() {
        }

        @DexIgnore
        @Override // com.fossil.e87
        public void a(f87 f87) {
            b(f87);
        }

        @DexIgnore
        public final f87 b(f87 f87) {
            g87.a(f87, (po4) kq4.this.A3.get());
            return f87;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements qk6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ uk6 f1956a;

        @DexIgnore
        public c(uk6 uk6) {
            this.f1956a = uk6;
        }

        @DexIgnore
        @Override // com.fossil.qk6
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            c(activeTimeDetailActivity);
        }

        @DexIgnore
        public final wk6 b() {
            wk6 a2 = xk6.a(vk6.a(this.f1956a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (UserRepository) kq4.this.y.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (FileRepository) kq4.this.I0.get(), (no4) kq4.this.G.get(), (PortfolioApp) kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity c(ActiveTimeDetailActivity activeTimeDetailActivity) {
            ms5.e(activeTimeDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(activeTimeDetailActivity, (on5) kq4.this.c.get());
            ms5.a(activeTimeDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(activeTimeDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(activeTimeDetailActivity, new gu5());
            pk6.a(activeTimeDetailActivity, b());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public final wk6 d(wk6 wk6) {
            yk6.a(wk6);
            return wk6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements eg6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ mg6 f1957a;

        @DexIgnore
        public c0(mg6 mg6) {
            this.f1957a = mg6;
        }

        @DexIgnore
        @Override // com.fossil.eg6
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            f(caloriesOverviewFragment);
        }

        @DexIgnore
        public final ig6 b() {
            ig6 a2 = jg6.a(ng6.a(this.f1957a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (PortfolioApp) kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final tg6 c() {
            tg6 a2 = ug6.a(og6.a(this.f1957a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final zg6 d() {
            zg6 a2 = ah6.a(pg6.a(this.f1957a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final ig6 e(ig6 ig6) {
            kg6.a(ig6);
            return ig6;
        }

        @DexIgnore
        public final CaloriesOverviewFragment f(CaloriesOverviewFragment caloriesOverviewFragment) {
            lg6.a(caloriesOverviewFragment, b());
            lg6.c(caloriesOverviewFragment, d());
            lg6.b(caloriesOverviewFragment, c());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final tg6 g(tg6 tg6) {
            vg6.a(tg6);
            return tg6;
        }

        @DexIgnore
        public final zg6 h(zg6 zg6) {
            bh6.a(zg6);
            return zg6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements t17 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ w17 f1958a;

        @DexIgnore
        public c1(w17 w17) {
            this.f1958a = w17;
        }

        @DexIgnore
        @Override // com.fossil.t17
        public void a(FindDeviceActivity findDeviceActivity) {
            e(findDeviceActivity);
        }

        @DexIgnore
        public final y17 b() {
            y17 a2 = z17.a((ct0) kq4.this.h2.get(), (DeviceRepository) kq4.this.P.get(), (on5) kq4.this.c.get(), x17.a(this.f1958a), new cu5(), d(), c(), new gu5(), (PortfolioApp) kq4.this.d.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final au5 c() {
            return new au5((GoogleApiService) kq4.this.f2.get());
        }

        @DexIgnore
        public final fu5 d() {
            return new fu5((lo4) kq4.this.j2.get());
        }

        @DexIgnore
        public final FindDeviceActivity e(FindDeviceActivity findDeviceActivity) {
            ms5.e(findDeviceActivity, (UserRepository) kq4.this.y.get());
            ms5.d(findDeviceActivity, (on5) kq4.this.c.get());
            ms5.a(findDeviceActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(findDeviceActivity, (oq4) kq4.this.Y1.get());
            ms5.b(findDeviceActivity, new gu5());
            s17.a(findDeviceActivity, b());
            return findDeviceActivity;
        }

        @DexIgnore
        public final y17 f(y17 y17) {
            a27.a(y17);
            return y17;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c2 implements gw6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ jw6 f1959a;

        @DexIgnore
        public c2(jw6 jw6) {
            this.f1959a = jw6;
        }

        @DexIgnore
        @Override // com.fossil.gw6
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            d(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final u27 b() {
            return new u27((SummariesRepository) kq4.this.D.get(), (SleepSummariesRepository) kq4.this.E.get(), (SleepSessionsRepository) kq4.this.b0.get());
        }

        @DexIgnore
        public final lw6 c() {
            lw6 a2 = mw6.a(kw6.a(this.f1959a), (UserRepository) kq4.this.y.get(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity d(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            ms5.e(onboardingHeightWeightActivity, (UserRepository) kq4.this.y.get());
            ms5.d(onboardingHeightWeightActivity, (on5) kq4.this.c.get());
            ms5.a(onboardingHeightWeightActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(onboardingHeightWeightActivity, (oq4) kq4.this.Y1.get());
            ms5.b(onboardingHeightWeightActivity, new gu5());
            fw6.a(onboardingHeightWeightActivity, c());
            return onboardingHeightWeightActivity;
        }

        @DexIgnore
        public final lw6 e(lw6 lw6) {
            nw6.a(lw6);
            return lw6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c3 implements w77 {
        @DexIgnore
        public c3() {
        }

        @DexIgnore
        @Override // com.fossil.w77
        public void a(x77 x77) {
            b(x77);
        }

        @DexIgnore
        public final x77 b(x77 x77) {
            z77.a(x77, (po4) kq4.this.A3.get());
            return x77;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements vd6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ de6 f1961a;

        @DexIgnore
        public d(de6 de6) {
            this.f1961a = de6;
        }

        @DexIgnore
        @Override // com.fossil.vd6
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            f(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final zd6 b() {
            zd6 a2 = ae6.a(ee6.a(this.f1961a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (PortfolioApp) kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ke6 c() {
            ke6 a2 = le6.a(fe6.a(this.f1961a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final qe6 d() {
            qe6 a2 = re6.a(ge6.a(this.f1961a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final zd6 e(zd6 zd6) {
            be6.a(zd6);
            return zd6;
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment f(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            ce6.a(activeTimeOverviewFragment, b());
            ce6.c(activeTimeOverviewFragment, d());
            ce6.b(activeTimeOverviewFragment, c());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final ke6 g(ke6 ke6) {
            me6.a(ke6);
            return ke6;
        }

        @DexIgnore
        public final qe6 h(qe6 qe6) {
            se6.a(qe6);
            return qe6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements q76 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c86 f1962a;

        @DexIgnore
        public d0(c86 c86) {
            this.f1962a = c86;
        }

        @DexIgnore
        @Override // com.fossil.q76
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            c(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final e86 b() {
            e86 a2 = f86.a(d86.a(this.f1962a), (on5) kq4.this.c.get(), (UserRepository) kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity c(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            ms5.e(commuteTimeSettingsActivity, (UserRepository) kq4.this.y.get());
            ms5.d(commuteTimeSettingsActivity, (on5) kq4.this.c.get());
            ms5.a(commuteTimeSettingsActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(commuteTimeSettingsActivity, (oq4) kq4.this.Y1.get());
            ms5.b(commuteTimeSettingsActivity, new gu5());
            p76.a(commuteTimeSettingsActivity, b());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public final e86 d(e86 e86) {
            g86.a(e86);
            return e86;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements xv6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ aw6 f1963a;

        @DexIgnore
        public d1(aw6 aw6) {
            this.f1963a = aw6;
        }

        @DexIgnore
        @Override // com.fossil.xv6
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            d(forgotPasswordActivity);
        }

        @DexIgnore
        public final cw6 b() {
            cw6 a2 = dw6.a(bw6.a(this.f1963a), c());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final iv5 c() {
            return new iv5((AuthApiGuestService) kq4.this.l.get());
        }

        @DexIgnore
        public final ForgotPasswordActivity d(ForgotPasswordActivity forgotPasswordActivity) {
            ms5.e(forgotPasswordActivity, (UserRepository) kq4.this.y.get());
            ms5.d(forgotPasswordActivity, (on5) kq4.this.c.get());
            ms5.a(forgotPasswordActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(forgotPasswordActivity, (oq4) kq4.this.Y1.get());
            ms5.b(forgotPasswordActivity, new gu5());
            wv6.a(forgotPasswordActivity, b());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final cw6 e(cw6 cw6) {
            ew6.a(cw6);
            return cw6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d2 implements gy6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ jy6 f1964a;

        @DexIgnore
        public d2(jy6 jy6) {
            this.f1964a = jy6;
        }

        @DexIgnore
        @Override // com.fossil.gy6
        public void a(PairingActivity pairingActivity) {
            g(pairingActivity);
        }

        @DexIgnore
        public final ct5 b() {
            return new ct5((PortfolioApp) kq4.this.d.get(), (GuestApiService) kq4.this.F.get());
        }

        @DexIgnore
        public final LabelRemoteDataSource c() {
            return new LabelRemoteDataSource((ApiServiceV2) kq4.this.o.get());
        }

        @DexIgnore
        public final LabelRepository d() {
            return new LabelRepository((Context) kq4.this.b.get(), c());
        }

        @DexIgnore
        public final ft5 e() {
            return new ft5((DeviceRepository) kq4.this.P.get(), b(), (on5) kq4.this.c.get(), (UserRepository) kq4.this.y.get(), kq4.this.Y3(), kq4.this.a4(), d(), (FileRepository) kq4.this.I0.get());
        }

        @DexIgnore
        public final ly6 f() {
            ly6 a2 = ny6.a(ky6.a(this.f1964a), e(), (DeviceRepository) kq4.this.P.get(), kq4.this.a4(), (or5) kq4.this.Q1.get(), kq4.this.J4(), (on5) kq4.this.c.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final PairingActivity g(PairingActivity pairingActivity) {
            ms5.e(pairingActivity, (UserRepository) kq4.this.y.get());
            ms5.d(pairingActivity, (on5) kq4.this.c.get());
            ms5.a(pairingActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(pairingActivity, (oq4) kq4.this.Y1.get());
            ms5.b(pairingActivity, new gu5());
            fy6.a(pairingActivity, f());
            return pairingActivity;
        }

        @DexIgnore
        public final ly6 h(ly6 ly6) {
            oy6.a(ly6);
            return ly6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d3 implements wa7 {
        @DexIgnore
        public d3() {
        }

        @DexIgnore
        @Override // com.fossil.wa7
        public void a(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            b(watchFaceGalleryFragment);
        }

        @DexIgnore
        public final WatchFaceGalleryFragment b(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            xa7.a(watchFaceGalleryFragment, (po4) kq4.this.A3.get());
            return watchFaceGalleryFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements al6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ el6 f1966a;

        @DexIgnore
        public e(el6 el6) {
            this.f1966a = el6;
        }

        @DexIgnore
        @Override // com.fossil.al6
        public void a(ActivityDetailActivity activityDetailActivity) {
            c(activityDetailActivity);
        }

        @DexIgnore
        public final gl6 b() {
            gl6 a2 = hl6.a(fl6.a(this.f1966a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (UserRepository) kq4.this.y.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (FileRepository) kq4.this.I0.get(), (no4) kq4.this.G.get(), (PortfolioApp) kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity c(ActivityDetailActivity activityDetailActivity) {
            ms5.e(activityDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(activityDetailActivity, (on5) kq4.this.c.get());
            ms5.a(activityDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(activityDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(activityDetailActivity, new gu5());
            zk6.a(activityDetailActivity, b());
            return activityDetailActivity;
        }

        @DexIgnore
        public final gl6 d(gl6 gl6) {
            il6.a(gl6);
            return gl6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements u76 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ x76 f1967a;

        @DexIgnore
        public e0(x76 x76) {
            this.f1967a = x76;
        }

        @DexIgnore
        @Override // com.fossil.u76
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            c(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final z76 b() {
            z76 a2 = a86.a(y76.a(this.f1967a), (on5) kq4.this.c.get(), (UserRepository) kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity c(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            ms5.e(commuteTimeSettingsDefaultAddressActivity, (UserRepository) kq4.this.y.get());
            ms5.d(commuteTimeSettingsDefaultAddressActivity, (on5) kq4.this.c.get());
            ms5.a(commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(commuteTimeSettingsDefaultAddressActivity, (oq4) kq4.this.Y1.get());
            ms5.b(commuteTimeSettingsDefaultAddressActivity, new gu5());
            t76.a(commuteTimeSettingsDefaultAddressActivity, b());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public final z76 d(z76 z76) {
            b86.a(z76);
            return z76;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements xl6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ bm6 f1968a;

        @DexIgnore
        public e1(bm6 bm6) {
            this.f1968a = bm6;
        }

        @DexIgnore
        @Override // com.fossil.xl6
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            c(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final dm6 b() {
            dm6 a2 = em6.a(cm6.a(this.f1968a), (GoalTrackingRepository) kq4.this.c0.get(), (no4) kq4.this.G.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity c(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            ms5.e(goalTrackingDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(goalTrackingDetailActivity, (on5) kq4.this.c.get());
            ms5.a(goalTrackingDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(goalTrackingDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(goalTrackingDetailActivity, new gu5());
            wl6.a(goalTrackingDetailActivity, b());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public final dm6 d(dm6 dm6) {
            fm6.a(dm6);
            return dm6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e2 implements wx6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ zx6 f1969a;

        @DexIgnore
        public e2(zx6 zx6) {
            this.f1969a = zx6;
        }

        @DexIgnore
        @Override // com.fossil.wx6
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            c(pairingInstructionsActivity);
        }

        @DexIgnore
        public final by6 b() {
            by6 a2 = cy6.a(ay6.a(this.f1969a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity c(PairingInstructionsActivity pairingInstructionsActivity) {
            ms5.e(pairingInstructionsActivity, (UserRepository) kq4.this.y.get());
            ms5.d(pairingInstructionsActivity, (on5) kq4.this.c.get());
            ms5.a(pairingInstructionsActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(pairingInstructionsActivity, (oq4) kq4.this.Y1.get());
            ms5.b(pairingInstructionsActivity, new gu5());
            vx6.a(pairingInstructionsActivity, b());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public final by6 d(by6 by6) {
            dy6.a(by6);
            return by6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e3 implements la7 {
        @DexIgnore
        public e3() {
        }

        @DexIgnore
        @Override // com.fossil.la7
        public void a(qa7 qa7) {
            c(qa7);
        }

        @DexIgnore
        @Override // com.fossil.la7
        public void b(ma7 ma7) {
            d(ma7);
        }

        @DexIgnore
        public final qa7 c(qa7 qa7) {
            ra7.a(qa7, (po4) kq4.this.A3.get());
            return qa7;
        }

        @DexIgnore
        public final ma7 d(ma7 ma7) {
            na7.a(ma7, (po4) kq4.this.A3.get());
            return ma7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements ze6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ if6 f1971a;

        @DexIgnore
        public f(if6 if6) {
            this.f1971a = if6;
        }

        @DexIgnore
        @Override // com.fossil.ze6
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            f(activityOverviewFragment);
        }

        @DexIgnore
        public final ef6 b() {
            ef6 a2 = ff6.a(jf6.a(this.f1971a), (SummariesRepository) kq4.this.D.get(), (ActivitiesRepository) kq4.this.W.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (PortfolioApp) kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final pf6 c() {
            pf6 a2 = qf6.a(kf6.a(this.f1971a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final vf6 d() {
            vf6 a2 = wf6.a(lf6.a(this.f1971a), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (PortfolioApp) kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final ef6 e(ef6 ef6) {
            gf6.a(ef6);
            return ef6;
        }

        @DexIgnore
        public final ActivityOverviewFragment f(ActivityOverviewFragment activityOverviewFragment) {
            hf6.a(activityOverviewFragment, b());
            hf6.c(activityOverviewFragment, d());
            hf6.b(activityOverviewFragment, c());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final pf6 g(pf6 pf6) {
            rf6.a(pf6);
            return pf6;
        }

        @DexIgnore
        public final vf6 h(vf6 vf6) {
            xf6.a(vf6);
            return vf6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements ja6 {
        @DexIgnore
        public f0() {
        }

        @DexIgnore
        @Override // com.fossil.ja6
        public void a(da6 da6) {
            b(da6);
        }

        @DexIgnore
        public final da6 b(da6 da6) {
            fa6.a(da6, (po4) kq4.this.A3.get());
            return da6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements ih6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ qh6 f1973a;

        @DexIgnore
        public f1(qh6 qh6) {
            this.f1973a = qh6;
        }

        @DexIgnore
        @Override // com.fossil.ih6
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            f(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final mh6 b() {
            mh6 a2 = nh6.a(rh6.a(this.f1973a), (on5) kq4.this.c.get(), (GoalTrackingRepository) kq4.this.c0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final xh6 c() {
            xh6 a2 = yh6.a(sh6.a(this.f1973a), (UserRepository) kq4.this.y.get(), (on5) kq4.this.c.get(), (GoalTrackingRepository) kq4.this.c0.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final di6 d() {
            di6 a2 = ei6.a(th6.a(this.f1973a), (UserRepository) kq4.this.y.get(), (on5) kq4.this.c.get(), (GoalTrackingRepository) kq4.this.c0.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final mh6 e(mh6 mh6) {
            oh6.a(mh6);
            return mh6;
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment f(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            ph6.a(goalTrackingOverviewFragment, b());
            ph6.c(goalTrackingOverviewFragment, d());
            ph6.b(goalTrackingOverviewFragment, c());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final xh6 g(xh6 xh6) {
            zh6.a(xh6);
            return xh6;
        }

        @DexIgnore
        public final di6 h(di6 di6) {
            fi6.a(di6);
            return di6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f2 implements bz6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ gz6 f1974a;

        @DexIgnore
        public f2(gz6 gz6) {
            this.f1974a = gz6;
        }

        @DexIgnore
        @Override // com.fossil.bz6
        public void a(PermissionActivity permissionActivity) {
            c(permissionActivity);
        }

        @DexIgnore
        public final jz6 b() {
            jz6 a2 = kz6.a(iz6.a(this.f1974a), hz6.a(this.f1974a), (on5) kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity c(PermissionActivity permissionActivity) {
            ms5.e(permissionActivity, (UserRepository) kq4.this.y.get());
            ms5.d(permissionActivity, (on5) kq4.this.c.get());
            ms5.a(permissionActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(permissionActivity, (oq4) kq4.this.Y1.get());
            ms5.b(permissionActivity, new gu5());
            az6.a(permissionActivity, b());
            return permissionActivity;
        }

        @DexIgnore
        public final jz6 d(jz6 jz6) {
            lz6.a(jz6);
            return jz6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f3 implements e97 {
        @DexIgnore
        public f3() {
        }

        @DexIgnore
        @Override // com.fossil.e97
        public void a(n97 n97) {
            b(n97);
        }

        @DexIgnore
        public final n97 b(n97 n97) {
            o97.a(n97, (po4) kq4.this.A3.get());
            return n97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements mx5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ qx5 f1976a;

        @DexIgnore
        public g(qx5 qx5) {
            this.f1976a = qx5;
        }

        @DexIgnore
        @Override // com.fossil.mx5
        public void a(AlarmActivity alarmActivity) {
            e(alarmActivity);
        }

        @DexIgnore
        public final ux5 b() {
            ux5 a2 = vx5.a(tx5.a(this.f1976a), sx5.a(this.f1976a), rx5.a(this.f1976a), this.f1976a.a(), d(), (bk5) kq4.this.B.get(), c(), (UserRepository) kq4.this.y.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final xx5 c() {
            return new xx5((AlarmsRepository) kq4.this.A.get());
        }

        @DexIgnore
        public final yx5 d() {
            return new yx5((PortfolioApp) kq4.this.d.get(), (AlarmsRepository) kq4.this.A.get());
        }

        @DexIgnore
        public final AlarmActivity e(AlarmActivity alarmActivity) {
            ms5.e(alarmActivity, (UserRepository) kq4.this.y.get());
            ms5.d(alarmActivity, (on5) kq4.this.c.get());
            ms5.a(alarmActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(alarmActivity, (oq4) kq4.this.Y1.get());
            ms5.b(alarmActivity, new gu5());
            lx5.a(alarmActivity, b());
            return alarmActivity;
        }

        @DexIgnore
        public final ux5 f(ux5 ux5) {
            wx5.a(ux5);
            return ux5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements ma6 {
        @DexIgnore
        public g0() {
        }

        @DexIgnore
        @Override // com.fossil.ma6
        public void a(ga6 ga6) {
            b(ga6);
        }

        @DexIgnore
        public final ga6 b(ga6 ga6) {
            ha6.a(ga6, (po4) kq4.this.A3.get());
            return ga6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements hm6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ lm6 f1978a;

        @DexIgnore
        public g1(lm6 lm6) {
            this.f1978a = lm6;
        }

        @DexIgnore
        @Override // com.fossil.hm6
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            c(heartRateDetailActivity);
        }

        @DexIgnore
        public final nm6 b() {
            nm6 a2 = om6.a(mm6.a(this.f1978a), (HeartRateSummaryRepository) kq4.this.l0.get(), (HeartRateSampleRepository) kq4.this.k0.get(), (UserRepository) kq4.this.y.get(), (WorkoutSessionRepository) kq4.this.m0.get(), (FileRepository) kq4.this.I0.get(), (no4) kq4.this.G.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity c(HeartRateDetailActivity heartRateDetailActivity) {
            ms5.e(heartRateDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(heartRateDetailActivity, (on5) kq4.this.c.get());
            ms5.a(heartRateDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(heartRateDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(heartRateDetailActivity, new gu5());
            gm6.a(heartRateDetailActivity, b());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public final nm6 d(nm6 nm6) {
            pm6.a(nm6);
            return nm6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g2 implements vt6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ yt6 f1979a;

        @DexIgnore
        public g2(yt6 yt6) {
            this.f1979a = yt6;
        }

        @DexIgnore
        @Override // com.fossil.vt6
        public void a(PreferredUnitActivity preferredUnitActivity) {
            c(preferredUnitActivity);
        }

        @DexIgnore
        public final au6 b() {
            au6 a2 = bu6.a(zt6.a(this.f1979a), (UserRepository) kq4.this.y.get(), kq4.this.Q4());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity c(PreferredUnitActivity preferredUnitActivity) {
            ms5.e(preferredUnitActivity, (UserRepository) kq4.this.y.get());
            ms5.d(preferredUnitActivity, (on5) kq4.this.c.get());
            ms5.a(preferredUnitActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(preferredUnitActivity, (oq4) kq4.this.Y1.get());
            ms5.b(preferredUnitActivity, new gu5());
            ut6.a(preferredUnitActivity, b());
            return preferredUnitActivity;
        }

        @DexIgnore
        public final au6 d(au6 au6) {
            cu6.a(au6);
            return au6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g3 implements j87 {
        @DexIgnore
        public g3() {
        }

        @DexIgnore
        @Override // com.fossil.j87
        public void a(k87 k87) {
            b(k87);
        }

        @DexIgnore
        public final k87 b(k87 k87) {
            l87.a(k87, (po4) kq4.this.A3.get());
            return k87;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h implements cx4 {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        @Override // com.fossil.cx4
        public void a(dx4 dx4) {
            b(dx4);
        }

        @DexIgnore
        public final dx4 b(dx4 dx4) {
            ex4.a(dx4, (po4) kq4.this.A3.get());
            return dx4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements b96 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ f96 f1982a;

        @DexIgnore
        public h0(f96 f96) {
            this.f1982a = f96;
        }

        @DexIgnore
        @Override // com.fossil.b96
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            c(complicationSearchActivity);
        }

        @DexIgnore
        public final h96 b() {
            h96 a2 = i96.a(g96.a(this.f1982a), kq4.this.T3(), (on5) kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity c(ComplicationSearchActivity complicationSearchActivity) {
            ms5.e(complicationSearchActivity, (UserRepository) kq4.this.y.get());
            ms5.d(complicationSearchActivity, (on5) kq4.this.c.get());
            ms5.a(complicationSearchActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(complicationSearchActivity, (oq4) kq4.this.Y1.get());
            ms5.b(complicationSearchActivity, new gu5());
            a96.a(complicationSearchActivity, b());
            return complicationSearchActivity;
        }

        @DexIgnore
        public final h96 d(h96 h96) {
            j96.a(h96);
            return h96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements mi6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ui6 f1983a;

        @DexIgnore
        public h1(ui6 ui6) {
            this.f1983a = ui6;
        }

        @DexIgnore
        @Override // com.fossil.mi6
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            f(heartRateOverviewFragment);
        }

        @DexIgnore
        public final qi6 b() {
            qi6 a2 = ri6.a(vi6.a(this.f1983a), (HeartRateSampleRepository) kq4.this.k0.get(), (WorkoutSessionRepository) kq4.this.m0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final bj6 c() {
            bj6 a2 = cj6.a(wi6.a(this.f1983a), (UserRepository) kq4.this.y.get(), (HeartRateSummaryRepository) kq4.this.l0.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final hj6 d() {
            hj6 a2 = ij6.a(xi6.a(this.f1983a), (UserRepository) kq4.this.y.get(), (HeartRateSummaryRepository) kq4.this.l0.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final qi6 e(qi6 qi6) {
            si6.a(qi6);
            return qi6;
        }

        @DexIgnore
        public final HeartRateOverviewFragment f(HeartRateOverviewFragment heartRateOverviewFragment) {
            ti6.a(heartRateOverviewFragment, b());
            ti6.c(heartRateOverviewFragment, d());
            ti6.b(heartRateOverviewFragment, c());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public final bj6 g(bj6 bj6) {
            dj6.a(bj6);
            return bj6;
        }

        @DexIgnore
        public final hj6 h(hj6 hj6) {
            jj6.a(hj6);
            return hj6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h2 implements pq6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ tq6 f1984a;

        @DexIgnore
        public h2(tq6 tq6) {
            this.f1984a = tq6;
        }

        @DexIgnore
        @Override // com.fossil.pq6
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            d(profileChangePasswordActivity);
        }

        @DexIgnore
        public final nu5 b() {
            return new nu5((AuthApiUserService) kq4.this.u.get());
        }

        @DexIgnore
        public final vq6 c() {
            vq6 a2 = wq6.a(uq6.a(this.f1984a), b(), (uq4) kq4.this.R.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileChangePasswordActivity d(ProfileChangePasswordActivity profileChangePasswordActivity) {
            ms5.e(profileChangePasswordActivity, (UserRepository) kq4.this.y.get());
            ms5.d(profileChangePasswordActivity, (on5) kq4.this.c.get());
            ms5.a(profileChangePasswordActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(profileChangePasswordActivity, (oq4) kq4.this.Y1.get());
            ms5.b(profileChangePasswordActivity, new gu5());
            oq6.a(profileChangePasswordActivity, c());
            return profileChangePasswordActivity;
        }

        @DexIgnore
        public final vq6 e(vq6 vq6) {
            xq6.a(vq6);
            return vq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h3 implements r97 {
        @DexIgnore
        public h3() {
        }

        @DexIgnore
        @Override // com.fossil.r97
        public void a(s97 s97) {
            b(s97);
        }

        @DexIgnore
        public final s97 b(s97 s97) {
            t97.a(s97, (po4) kq4.this.A3.get());
            return s97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements ju4 {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        @Override // com.fossil.ju4
        public void a(ku4 ku4) {
            b(ku4);
        }

        @DexIgnore
        public final ku4 b(ku4 ku4) {
            lu4.a(ku4, (po4) kq4.this.A3.get());
            return ku4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements o66 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ p66 f1987a;

        @DexIgnore
        public i0(p66 p66) {
            this.f1987a = p66;
        }

        @DexIgnore
        @Override // com.fossil.o66
        public void a(f76 f76) {
            c(f76);
        }

        @DexIgnore
        public final aa6 b() {
            aa6 a2 = ba6.a(q66.a(this.f1987a), kq4.this.Q3(), (on5) kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final f76 c(f76 f76) {
            g76.a(f76, b());
            g76.b(f76, (po4) kq4.this.A3.get());
            return f76;
        }

        @DexIgnore
        public final aa6 d(aa6 aa6) {
            ca6.a(aa6);
            return aa6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements mp6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ qp6 f1988a;

        @DexIgnore
        public i1(qp6 qp6) {
            this.f1988a = qp6;
        }

        @DexIgnore
        @Override // com.fossil.mp6
        public void a(HelpActivity helpActivity) {
            d(helpActivity);
        }

        @DexIgnore
        public final ou5 b() {
            return new ou5((UserRepository) kq4.this.y.get(), (DeviceRepository) kq4.this.P.get(), (on5) kq4.this.c.get());
        }

        @DexIgnore
        public final sp6 c() {
            sp6 a2 = tp6.a(rp6.a(this.f1988a), (DeviceRepository) kq4.this.P.get(), b(), (ck5) kq4.this.P0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final HelpActivity d(HelpActivity helpActivity) {
            ms5.e(helpActivity, (UserRepository) kq4.this.y.get());
            ms5.d(helpActivity, (on5) kq4.this.c.get());
            ms5.a(helpActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(helpActivity, (oq4) kq4.this.Y1.get());
            ms5.b(helpActivity, new gu5());
            lp6.a(helpActivity, c());
            return helpActivity;
        }

        @DexIgnore
        public final sp6 e(sp6 sp6) {
            up6.a(sp6);
            return sp6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i2 implements dp6 {
        @DexIgnore
        public i2() {
        }

        @DexIgnore
        @Override // com.fossil.dp6
        public void a(ep6 ep6) {
            b(ep6);
        }

        @DexIgnore
        public final ep6 b(ep6 ep6) {
            gp6.a(ep6, (po4) kq4.this.A3.get());
            return ep6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i3 implements y97 {
        @DexIgnore
        public i3() {
        }

        @DexIgnore
        @Override // com.fossil.y97
        public void a(z97 z97) {
            b(z97);
        }

        @DexIgnore
        public final z97 b(z97 z97) {
            aa7.a(z97, (po4) kq4.this.A3.get());
            return z97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements ou4 {
        @DexIgnore
        public j() {
        }

        @DexIgnore
        @Override // com.fossil.ou4
        public void a(pu4 pu4) {
            b(pu4);
        }

        @DexIgnore
        public final pu4 b(pu4 pu4) {
            qu4.a(pu4, (po4) kq4.this.A3.get());
            return pu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements ay5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dy5 f1992a;

        @DexIgnore
        public j0(dy5 dy5) {
            this.f1992a = dy5;
        }

        @DexIgnore
        @Override // com.fossil.ay5
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            c(connectedAppsActivity);
        }

        @DexIgnore
        public final fy5 b() {
            fy5 a2 = gy5.a(ey5.a(this.f1992a), (UserRepository) kq4.this.y.get(), (PortfolioApp) kq4.this.d.get(), kq4.this.w4());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity c(ConnectedAppsActivity connectedAppsActivity) {
            ms5.e(connectedAppsActivity, (UserRepository) kq4.this.y.get());
            ms5.d(connectedAppsActivity, (on5) kq4.this.c.get());
            ms5.a(connectedAppsActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(connectedAppsActivity, (oq4) kq4.this.Y1.get());
            ms5.b(connectedAppsActivity, new gu5());
            zx5.a(connectedAppsActivity, b());
            return connectedAppsActivity;
        }

        @DexIgnore
        public final fy5 d(fy5 fy5) {
            hy5.a(fy5);
            return fy5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements yz5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d06 f1993a;

        @DexIgnore
        public j1(d06 d06) {
            this.f1993a = d06;
        }

        @DexIgnore
        @Override // com.fossil.yz5
        public void a(b06 b06) {
            d(b06);
        }

        @DexIgnore
        public final s36 b() {
            s36 a2 = t36.a(e06.a(this.f1993a), (DNDSettingsDatabase) kq4.this.e.get());
            c(a2);
            return a2;
        }

        @DexIgnore
        public final s36 c(s36 s36) {
            u36.a(s36);
            return s36;
        }

        @DexIgnore
        public final b06 d(b06 b06) {
            c06.a(b06, b());
            return b06;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j2 implements fq6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ jq6 f1994a;

        @DexIgnore
        public j2(jq6 jq6) {
            this.f1994a = jq6;
        }

        @DexIgnore
        @Override // com.fossil.fq6
        public void a(ProfileOptInActivity profileOptInActivity) {
            c(profileOptInActivity);
        }

        @DexIgnore
        public final lq6 b() {
            lq6 a2 = mq6.a(kq6.a(this.f1994a), kq4.this.Q4(), (UserRepository) kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity c(ProfileOptInActivity profileOptInActivity) {
            ms5.e(profileOptInActivity, (UserRepository) kq4.this.y.get());
            ms5.d(profileOptInActivity, (on5) kq4.this.c.get());
            ms5.a(profileOptInActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(profileOptInActivity, (oq4) kq4.this.Y1.get());
            ms5.b(profileOptInActivity, new gu5());
            eq6.a(profileOptInActivity, b());
            return profileOptInActivity;
        }

        @DexIgnore
        public final lq6 d(lq6 lq6) {
            nq6.a(lq6);
            return lq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j3 implements da7 {
        @DexIgnore
        public j3() {
        }

        @DexIgnore
        @Override // com.fossil.da7
        public void a(ea7 ea7) {
            b(ea7);
        }

        @DexIgnore
        public final ea7 b(ea7 ea7) {
            fa7.a(ea7, (po4) kq4.this.A3.get());
            return ea7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k implements hx4 {
        @DexIgnore
        public k() {
        }

        @DexIgnore
        @Override // com.fossil.hx4
        public void a(ix4 ix4) {
            b(ix4);
        }

        @DexIgnore
        public final ix4 b(ix4 ix4) {
            kx4.a(ix4, (po4) kq4.this.A3.get());
            return ix4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements er6 {
        @DexIgnore
        public k0(hr6 hr6) {
        }

        @DexIgnore
        @Override // com.fossil.er6
        public void a(fr6 fr6) {
            b(fr6);
        }

        @DexIgnore
        public final fr6 b(fr6 fr6) {
            gr6.a(fr6, (po4) kq4.this.A3.get());
            return fr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements xy5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ hz5 f1998a;

        @DexIgnore
        public k1(hz5 hz5) {
            this.f1998a = hz5;
        }

        @DexIgnore
        @Override // com.fossil.xy5
        public void a(HomeActivity homeActivity) {
            f(homeActivity);
        }

        @DexIgnore
        public final bv5 b() {
            return new bv5(e());
        }

        @DexIgnore
        public final ou5 c() {
            return new ou5((UserRepository) kq4.this.y.get(), (DeviceRepository) kq4.this.P.get(), (on5) kq4.this.c.get());
        }

        @DexIgnore
        public final jz5 d() {
            jz5 a2 = kz5.a(iz5.a(this.f1998a), (on5) kq4.this.c.get(), (DeviceRepository) kq4.this.P.get(), (PortfolioApp) kq4.this.d.get(), kq4.this.P4(), (uq4) kq4.this.R.get(), kq4.this.a4(), b(), (n47) kq4.this.E3.get(), e(), c(), (pr4) kq4.this.u1.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository e() {
            return new ServerSettingRepository((ServerSettingDataSource) kq4.this.C3.get(), (ServerSettingDataSource) kq4.this.D3.get());
        }

        @DexIgnore
        public final HomeActivity f(HomeActivity homeActivity) {
            ms5.e(homeActivity, (UserRepository) kq4.this.y.get());
            ms5.d(homeActivity, (on5) kq4.this.c.get());
            ms5.a(homeActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(homeActivity, (oq4) kq4.this.Y1.get());
            ms5.b(homeActivity, new gu5());
            wy5.a(homeActivity, d());
            return homeActivity;
        }

        @DexIgnore
        public final jz5 g(jz5 jz5) {
            lz5.a(jz5);
            return jz5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k2 implements xw6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ax6 f1999a;

        @DexIgnore
        public k2(ax6 ax6) {
            this.f1999a = ax6;
        }

        @DexIgnore
        @Override // com.fossil.xw6
        public void a(ProfileSetupActivity profileSetupActivity) {
            e(profileSetupActivity);
        }

        @DexIgnore
        public final u27 b() {
            return new u27((SummariesRepository) kq4.this.D.get(), (SleepSummariesRepository) kq4.this.E.get(), (SleepSessionsRepository) kq4.this.b0.get());
        }

        @DexIgnore
        public final cx6 c() {
            cx6 a2 = dx6.a(bx6.a(this.f1999a), b(), (UserRepository) kq4.this.y.get(), (DeviceRepository) kq4.this.P.get(), d(), (pr4) kq4.this.u1.get(), (on5) kq4.this.c.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) kq4.this.C3.get(), (ServerSettingDataSource) kq4.this.D3.get());
        }

        @DexIgnore
        public final ProfileSetupActivity e(ProfileSetupActivity profileSetupActivity) {
            ms5.e(profileSetupActivity, (UserRepository) kq4.this.y.get());
            ms5.d(profileSetupActivity, (on5) kq4.this.c.get());
            ms5.a(profileSetupActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(profileSetupActivity, (oq4) kq4.this.Y1.get());
            ms5.b(profileSetupActivity, new gu5());
            ww6.a(profileSetupActivity, c());
            return profileSetupActivity;
        }

        @DexIgnore
        public final cx6 f(cx6 cx6) {
            ex6.c(cx6, kq4.this.L4());
            ex6.d(cx6, kq4.this.M4());
            ex6.b(cx6, kq4.this.u4());
            ex6.a(cx6, (ck5) kq4.this.P0.get());
            ex6.e(cx6);
            return cx6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k3 implements c17 {
        @DexIgnore
        public k3() {
        }

        @DexIgnore
        @Override // com.fossil.c17
        public void a(d17 d17) {
            b(d17);
        }

        @DexIgnore
        public final d17 b(d17 d17) {
            e17.a(d17, (po4) kq4.this.A3.get());
            return d17;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l implements ev4 {
        @DexIgnore
        public l() {
        }

        @DexIgnore
        @Override // com.fossil.ev4
        public void a(av4 av4) {
            b(av4);
        }

        @DexIgnore
        public final av4 b(av4 av4) {
            bv4.a(av4, (po4) kq4.this.A3.get());
            return av4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements kr6 {
        @DexIgnore
        public l0(nr6 nr6) {
        }

        @DexIgnore
        @Override // com.fossil.kr6
        public void a(lr6 lr6) {
            b(lr6);
        }

        @DexIgnore
        public final lr6 b(lr6 lr6) {
            mr6.a(lr6, (po4) kq4.this.A3.get());
            return lr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements nz5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ oz5 f2003a;

        @DexIgnore
        public l1(oz5 oz5) {
            this.f2003a = oz5;
        }

        @DexIgnore
        @Override // com.fossil.nz5
        public void a(ez5 ez5) {
            r(ez5);
        }

        @DexIgnore
        public final a46 b() {
            a46 a2 = b46.a(pz5.a(this.f2003a), (bk5) kq4.this.B.get(), j(), (AlarmsRepository) kq4.this.A.get(), (on5) kq4.this.c.get());
            n(a2);
            return a2;
        }

        @DexIgnore
        public final f06 c() {
            f06 a2 = g06.a(qz5.a(this.f2003a), (uq4) kq4.this.R.get(), (bk5) kq4.this.B.get(), new v36(), new d26(), i(), (NotificationSettingsDatabase) kq4.this.j.get(), j(), (AlarmsRepository) kq4.this.A.get(), (on5) kq4.this.c.get(), (DNDSettingsDatabase) kq4.this.e.get());
            o(a2);
            return a2;
        }

        @DexIgnore
        public final md6 d() {
            md6 a2 = nd6.a(rz5.a(this.f2003a), (PortfolioApp) kq4.this.d.get(), (DeviceRepository) kq4.this.P.get(), kq4.this.a4(), (SummariesRepository) kq4.this.D.get(), (GoalTrackingRepository) kq4.this.c0.get(), (SleepSummariesRepository) kq4.this.E.get(), (on5) kq4.this.c.get(), (HeartRateSampleRepository) kq4.this.k0.get(), (tt4) kq4.this.A0.get());
            p(a2);
            return a2;
        }

        @DexIgnore
        public final z66 e() {
            z66 a2 = a76.a(sz5.a(this.f2003a), kq4.this.U4(), kq4.this.T3(), m(), (UserRepository) kq4.this.y.get(), kq4.this.V3(), (FileRepository) kq4.this.I0.get(), (CustomizeRealDataRepository) kq4.this.t.get(), (UserRepository) kq4.this.y.get(), kq4.this.e4(), (PortfolioApp) kq4.this.d.get(), (uo5) kq4.this.M0.get(), (s77) kq4.this.d1.get(), l());
            q(a2);
            return a2;
        }

        @DexIgnore
        public final mb6 f() {
            mb6 a2 = nb6.a((PortfolioApp) kq4.this.d.get(), tz5.a(this.f2003a), (MicroAppRepository) kq4.this.i0.get(), (HybridPresetRepository) kq4.this.V.get(), k(), (on5) kq4.this.c.get());
            s(a2);
            return a2;
        }

        @DexIgnore
        public final bo6 g() {
            bo6 a2 = co6.a(uz5.a(this.f2003a), (PortfolioApp) kq4.this.d.get(), kq4.this.u4(), kq4.this.Q4(), (DeviceRepository) kq4.this.P.get(), (UserRepository) kq4.this.y.get(), (SummariesRepository) kq4.this.D.get(), (SleepSummariesRepository) kq4.this.E.get(), kq4.this.Z3(), (hu4) kq4.this.s0.get(), (tt4) kq4.this.A0.get(), (on5) kq4.this.c.get());
            t(a2);
            return a2;
        }

        @DexIgnore
        public final un6 h() {
            un6 a2 = vn6.a(vz5.a(this.f2003a));
            u(a2);
            return a2;
        }

        @DexIgnore
        public final u06 i() {
            return new u06((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final yx5 j() {
            return new yx5((PortfolioApp) kq4.this.d.get(), (AlarmsRepository) kq4.this.A.get());
        }

        @DexIgnore
        public final ib6 k() {
            return new ib6((DeviceRepository) kq4.this.P.get(), (HybridPresetRepository) kq4.this.V.get(), (MicroAppRepository) kq4.this.i0.get(), kq4.this.F4());
        }

        @DexIgnore
        public final yb7 l() {
            return new yb7((uo5) kq4.this.M0.get(), (FileRepository) kq4.this.I0.get(), (on5) kq4.this.c.get(), (s77) kq4.this.d1.get(), kq4.this.b4());
        }

        @DexIgnore
        public final jb6 m() {
            return new jb6((uo5) kq4.this.M0.get(), (FileRepository) kq4.this.I0.get(), kq4.this.b4(), kq4.this.b4());
        }

        @DexIgnore
        public final a46 n(a46 a46) {
            c46.a(a46);
            return a46;
        }

        @DexIgnore
        public final f06 o(f06 f06) {
            h06.a(f06);
            return f06;
        }

        @DexIgnore
        public final md6 p(md6 md6) {
            od6.a(md6);
            return md6;
        }

        @DexIgnore
        public final z66 q(z66 z66) {
            b76.a(z66);
            return z66;
        }

        @DexIgnore
        public final ez5 r(ez5 ez5) {
            fz5.c(ez5, d());
            fz5.d(ez5, e());
            fz5.e(ez5, f());
            fz5.f(ez5, g());
            fz5.b(ez5, c());
            fz5.a(ez5, b());
            fz5.g(ez5, h());
            return ez5;
        }

        @DexIgnore
        public final mb6 s(mb6 mb6) {
            ob6.a(mb6);
            return mb6;
        }

        @DexIgnore
        public final bo6 t(bo6 bo6) {
            do6.a(bo6);
            return bo6;
        }

        @DexIgnore
        public final un6 u(un6 un6) {
            wn6.a(un6);
            return un6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l2 implements i26 {
        @DexIgnore
        public l2() {
        }

        @DexIgnore
        @Override // com.fossil.i26
        public void a(l26 l26) {
            b(l26);
        }

        @DexIgnore
        public final l26 b(l26 l26) {
            m26.a(l26, (po4) kq4.this.A3.get());
            return l26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l3 implements qa6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ta6 f2005a;

        @DexIgnore
        public l3(ta6 ta6) {
            this.f2005a = ta6;
        }

        @DexIgnore
        @Override // com.fossil.qa6
        public void a(WeatherSettingActivity weatherSettingActivity) {
            c(weatherSettingActivity);
        }

        @DexIgnore
        public final va6 b() {
            va6 a2 = wa6.a(ua6.a(this.f2005a), (GoogleApiService) kq4.this.f2.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity c(WeatherSettingActivity weatherSettingActivity) {
            ms5.e(weatherSettingActivity, (UserRepository) kq4.this.y.get());
            ms5.d(weatherSettingActivity, (on5) kq4.this.c.get());
            ms5.a(weatherSettingActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(weatherSettingActivity, (oq4) kq4.this.Y1.get());
            ms5.b(weatherSettingActivity, new gu5());
            pa6.a(weatherSettingActivity, b());
            return weatherSettingActivity;
        }

        @DexIgnore
        public final va6 d(va6 va6) {
            xa6.a(va6);
            return va6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m implements nx4 {
        @DexIgnore
        public m() {
        }

        @DexIgnore
        @Override // com.fossil.nx4
        public void a(ox4 ox4) {
            b(ox4);
        }

        @DexIgnore
        public final ox4 b(ox4 ox4) {
            px4.a(ox4, (po4) kq4.this.A3.get());
            return ox4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements qr6 {
        @DexIgnore
        public m0(tr6 tr6) {
        }

        @DexIgnore
        @Override // com.fossil.qr6
        public void a(rr6 rr6) {
            b(rr6);
        }

        @DexIgnore
        public final rr6 b(rr6 rr6) {
            sr6.a(rr6, (po4) kq4.this.A3.get());
            return rr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements xb6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ cc6 f2008a;

        @DexIgnore
        public m1(cc6 cc6) {
            this.f2008a = cc6;
        }

        @DexIgnore
        @Override // com.fossil.xb6
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            d(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final ec6 b() {
            ec6 a2 = fc6.a(dc6.a(this.f2008a), c());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ib6 c() {
            return new ib6((DeviceRepository) kq4.this.P.get(), (HybridPresetRepository) kq4.this.V.get(), (MicroAppRepository) kq4.this.i0.get(), kq4.this.F4());
        }

        @DexIgnore
        public final HybridCustomizeEditActivity d(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            ms5.e(hybridCustomizeEditActivity, (UserRepository) kq4.this.y.get());
            ms5.d(hybridCustomizeEditActivity, (on5) kq4.this.c.get());
            ms5.a(hybridCustomizeEditActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(hybridCustomizeEditActivity, (oq4) kq4.this.Y1.get());
            ms5.b(hybridCustomizeEditActivity, new gu5());
            wb6.a(hybridCustomizeEditActivity, b());
            wb6.b(hybridCustomizeEditActivity, (po4) kq4.this.A3.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final ec6 e(ec6 ec6) {
            gc6.a(ec6);
            return ec6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m2 implements m36 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ n36 f2009a;

        @DexIgnore
        public m2(n36 n36) {
            this.f2009a = n36;
        }

        @DexIgnore
        @Override // com.fossil.m36
        public void a(z26 z26) {
            e(z26);
        }

        @DexIgnore
        public final s26 b() {
            s26 a2 = t26.a(o36.a(this.f2009a), (RemindersSettingsDatabase) kq4.this.n0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final j36 c() {
            j36 a2 = k36.a(p36.a(this.f2009a), (RemindersSettingsDatabase) kq4.this.n0.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final s26 d(s26 s26) {
            u26.a(s26);
            return s26;
        }

        @DexIgnore
        public final z26 e(z26 z26) {
            a36.a(z26, b());
            a36.b(z26, c());
            return z26;
        }

        @DexIgnore
        public final j36 f(j36 j36) {
            l36.a(j36);
            return j36;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m3 implements e27 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ j27 f2010a;

        @DexIgnore
        public m3(j27 j27) {
            this.f2010a = j27;
        }

        @DexIgnore
        @Override // com.fossil.e27
        public void a(WelcomeActivity welcomeActivity) {
            c(welcomeActivity);
        }

        @DexIgnore
        public final i27 b() {
            i27 a2 = l27.a(k27.a(this.f2010a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity c(WelcomeActivity welcomeActivity) {
            ms5.e(welcomeActivity, (UserRepository) kq4.this.y.get());
            ms5.d(welcomeActivity, (on5) kq4.this.c.get());
            ms5.a(welcomeActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(welcomeActivity, (oq4) kq4.this.Y1.get());
            ms5.b(welcomeActivity, new gu5());
            d27.a(welcomeActivity, b());
            return welcomeActivity;
        }

        @DexIgnore
        public final i27 d(i27 i27) {
            m27.a(i27);
            return i27;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n implements rw4 {
        @DexIgnore
        public n() {
        }

        @DexIgnore
        @Override // com.fossil.rw4
        public void a(sw4 sw4) {
            b(sw4);
        }

        @DexIgnore
        public final sw4 b(sw4 sw4) {
            uw4.a(sw4, (po4) kq4.this.A3.get());
            return sw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements wr6 {
        @DexIgnore
        public n0(zr6 zr6) {
        }

        @DexIgnore
        @Override // com.fossil.wr6
        public void a(xr6 xr6) {
            b(xr6);
        }

        @DexIgnore
        public final xr6 b(xr6 xr6) {
            yr6.a(xr6, (po4) kq4.this.A3.get());
            return xr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements pb6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ qb6 f2013a;

        @DexIgnore
        public n1(qb6 qb6) {
            this.f2013a = qb6;
        }

        @DexIgnore
        @Override // com.fossil.pb6
        public void a(ac6 ac6) {
            c(ac6);
        }

        @DexIgnore
        public final lc6 b() {
            lc6 a2 = mc6.a(rb6.a(this.f2013a), kq4.this.Q3());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ac6 c(ac6 ac6) {
            bc6.a(ac6, b());
            bc6.b(ac6, (po4) kq4.this.A3.get());
            return ac6;
        }

        @DexIgnore
        public final lc6 d(lc6 lc6) {
            nc6.a(lc6);
            return lc6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n2 implements po6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ to6 f2014a;

        @DexIgnore
        public n2(to6 to6) {
            this.f2014a = to6;
        }

        @DexIgnore
        @Override // com.fossil.po6
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            c(replaceBatteryActivity);
        }

        @DexIgnore
        public final vo6 b() {
            vo6 a2 = wo6.a(uo6.a(this.f2014a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity c(ReplaceBatteryActivity replaceBatteryActivity) {
            ms5.e(replaceBatteryActivity, (UserRepository) kq4.this.y.get());
            ms5.d(replaceBatteryActivity, (on5) kq4.this.c.get());
            ms5.a(replaceBatteryActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(replaceBatteryActivity, (oq4) kq4.this.Y1.get());
            ms5.b(replaceBatteryActivity, new gu5());
            oo6.a(replaceBatteryActivity, b());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public final vo6 d(vo6 vo6) {
            xo6.a(vo6);
            return vo6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n3 implements en6 {
        @DexIgnore
        public n3() {
        }

        @DexIgnore
        @Override // com.fossil.en6
        public void a(fn6 fn6) {
            b(fn6);
        }

        @DexIgnore
        public final fn6 b(fn6 fn6) {
            hn6.a(fn6, (po4) kq4.this.A3.get());
            return fn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements gy4 {
        @DexIgnore
        public o() {
        }

        @DexIgnore
        @Override // com.fossil.gy4
        public void a(by4 by4) {
            b(by4);
        }

        @DexIgnore
        public final by4 b(by4 by4) {
            dy4.a(by4, (po4) kq4.this.A3.get());
            return by4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements cs6 {
        @DexIgnore
        public o0(fs6 fs6) {
        }

        @DexIgnore
        @Override // com.fossil.cs6
        public void a(ds6 ds6) {
            b(ds6);
        }

        @DexIgnore
        public final ds6 b(ds6 ds6) {
            es6.a(ds6, (po4) kq4.this.A3.get());
            return ds6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements tu6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xu6 f2018a;

        @DexIgnore
        public o1(xu6 xu6) {
            this.f2018a = xu6;
        }

        @DexIgnore
        @Override // com.fossil.tu6
        public void a(LoginActivity loginActivity) {
            c(loginActivity);
        }

        @DexIgnore
        public final av6 b() {
            av6 a2 = bv6.a(zu6.a(this.f2018a), yu6.a(this.f2018a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity c(LoginActivity loginActivity) {
            ms5.e(loginActivity, (UserRepository) kq4.this.y.get());
            ms5.d(loginActivity, (on5) kq4.this.c.get());
            ms5.a(loginActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(loginActivity, (oq4) kq4.this.Y1.get());
            ms5.b(loginActivity, new gu5());
            su6.a(loginActivity, (un5) kq4.this.U1.get());
            su6.b(loginActivity, (vn5) kq4.this.d0.get());
            su6.d(loginActivity, (xn5) kq4.this.V1.get());
            su6.c(loginActivity, (wn5) kq4.this.W1.get());
            su6.e(loginActivity, b());
            return loginActivity;
        }

        @DexIgnore
        public final av6 d(av6 av6) {
            cv6.s(av6, kq4.this.y4());
            cv6.v(av6, kq4.this.B4());
            cv6.h(av6, kq4.this.f4());
            cv6.y(av6, new ht5());
            cv6.g(av6, kq4.this.a4());
            cv6.D(av6, (UserRepository) kq4.this.y.get());
            cv6.f(av6, (DeviceRepository) kq4.this.P.get());
            cv6.z(av6, (on5) kq4.this.c.get());
            cv6.i(av6, kq4.this.h4());
            cv6.p(av6, kq4.this.o4());
            cv6.C(av6, (uq4) kq4.this.R.get());
            cv6.n(av6, kq4.this.m4());
            cv6.o(av6, kq4.this.n4());
            cv6.m(av6, kq4.this.l4());
            cv6.k(av6, kq4.this.j4());
            cv6.t(av6, kq4.this.z4());
            cv6.G(av6, (xn5) kq4.this.V1.get());
            cv6.u(av6, kq4.this.A4());
            cv6.x(av6, kq4.this.D4());
            cv6.w(av6, kq4.this.C4());
            cv6.e(av6, kq4.this.S3());
            cv6.d(av6, (ck5) kq4.this.P0.get());
            cv6.B(av6, (SummariesRepository) kq4.this.D.get());
            cv6.A(av6, (SleepSummariesRepository) kq4.this.E.get());
            cv6.r(av6, (GoalTrackingRepository) kq4.this.c0.get());
            cv6.j(av6, kq4.this.i4());
            cv6.l(av6, kq4.this.k4());
            cv6.q(av6, kq4.this.t4());
            cv6.E(av6, kq4.this.X4());
            cv6.c(av6, (AlarmsRepository) kq4.this.A.get());
            cv6.I(av6, (hu4) kq4.this.s0.get());
            cv6.a(av6, (vt4) kq4.this.J0.get());
            cv6.b(av6, (pr4) kq4.this.u1.get());
            cv6.F(av6, kq4.this.Z4());
            cv6.H(av6);
            return av6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o2 implements pc6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ tc6 f2019a;

        @DexIgnore
        public o2(tc6 tc6) {
            this.f2019a = tc6;
        }

        @DexIgnore
        @Override // com.fossil.pc6
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            c(searchMicroAppActivity);
        }

        @DexIgnore
        public final vc6 b() {
            vc6 a2 = wc6.a(uc6.a(this.f2019a), (MicroAppRepository) kq4.this.i0.get(), (on5) kq4.this.c.get(), (PortfolioApp) kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity c(SearchMicroAppActivity searchMicroAppActivity) {
            ms5.e(searchMicroAppActivity, (UserRepository) kq4.this.y.get());
            ms5.d(searchMicroAppActivity, (on5) kq4.this.c.get());
            ms5.a(searchMicroAppActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(searchMicroAppActivity, (oq4) kq4.this.Y1.get());
            ms5.b(searchMicroAppActivity, new gu5());
            oc6.a(searchMicroAppActivity, b());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public final vc6 d(vc6 vc6) {
            xc6.a(vc6);
            return vc6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o3 implements mn6 {
        @DexIgnore
        public o3() {
        }

        @DexIgnore
        @Override // com.fossil.mn6
        public void a(nn6 nn6) {
            b(nn6);
        }

        @DexIgnore
        public final nn6 b(nn6 nn6) {
            on6.a(nn6, (po4) kq4.this.A3.get());
            return nn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p implements jy4 {
        @DexIgnore
        public p() {
        }

        @DexIgnore
        @Override // com.fossil.jy4
        public void a(ky4 ky4) {
            b(ky4);
        }

        @DexIgnore
        public final ky4 b(ky4 ky4) {
            ly4.a(ky4, (po4) kq4.this.A3.get());
            return ky4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements js6 {
        @DexIgnore
        public p0(ms6 ms6) {
        }

        @DexIgnore
        @Override // com.fossil.js6
        public void a(ks6 ks6) {
            b(ks6);
        }

        @DexIgnore
        public final ks6 b(ks6 ks6) {
            ls6.a(ks6, (po4) kq4.this.A3.get());
            return ks6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements dv6 {
        @DexIgnore
        public p1() {
        }

        @DexIgnore
        @Override // com.fossil.dv6
        public void a(ev6 ev6) {
            b(ev6);
        }

        @DexIgnore
        public final ev6 b(ev6 ev6) {
            fv6.a(ev6, (po4) kq4.this.A3.get());
            return ev6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p2 implements i86 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ m86 f2024a;

        @DexIgnore
        public p2(m86 m86) {
            this.f2024a = m86;
        }

        @DexIgnore
        @Override // com.fossil.i86
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            c(searchRingPhoneActivity);
        }

        @DexIgnore
        public final o86 b() {
            o86 a2 = p86.a(n86.a(this.f2024a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity c(SearchRingPhoneActivity searchRingPhoneActivity) {
            ms5.e(searchRingPhoneActivity, (UserRepository) kq4.this.y.get());
            ms5.d(searchRingPhoneActivity, (on5) kq4.this.c.get());
            ms5.a(searchRingPhoneActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(searchRingPhoneActivity, (oq4) kq4.this.Y1.get());
            ms5.b(searchRingPhoneActivity, new gu5());
            h86.a(searchRingPhoneActivity, b());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public final o86 d(o86 o86) {
            q86.a(o86);
            return o86;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p3 implements iu6 {
        @DexIgnore
        public p3() {
        }

        @DexIgnore
        @Override // com.fossil.iu6
        public void a(ju6 ju6) {
            b(ju6);
        }

        @DexIgnore
        public final ju6 b(ju6 ju6) {
            lu6.a(ju6, (po4) kq4.this.A3.get());
            return ju6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements uu4 {
        @DexIgnore
        public q() {
        }

        @DexIgnore
        @Override // com.fossil.uu4
        public void a(vu4 vu4) {
            b(vu4);
        }

        @DexIgnore
        public final vu4 b(vu4 vu4) {
            wu4.a(vu4, (po4) kq4.this.A3.get());
            return vu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements ps6 {
        @DexIgnore
        public q0(ss6 ss6) {
        }

        @DexIgnore
        @Override // com.fossil.ps6
        public void a(qs6 qs6) {
            b(qs6);
        }

        @DexIgnore
        public final qs6 b(qs6 qs6) {
            rs6.a(qs6, (po4) kq4.this.A3.get());
            return qs6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements hc6 {
        @DexIgnore
        public q1(kc6 kc6) {
        }

        @DexIgnore
        @Override // com.fossil.hc6
        public void a(ub6 ub6) {
            b(ub6);
        }

        @DexIgnore
        public final ub6 b(ub6 ub6) {
            vb6.a(ub6, (po4) kq4.this.A3.get());
            return ub6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q2 implements s86 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ v86 f2029a;

        @DexIgnore
        public q2(v86 v86) {
            this.f2029a = v86;
        }

        @DexIgnore
        @Override // com.fossil.s86
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            c(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final x86 b() {
            x86 a2 = y86.a(w86.a(this.f2029a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity c(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            ms5.e(searchSecondTimezoneActivity, (UserRepository) kq4.this.y.get());
            ms5.d(searchSecondTimezoneActivity, (on5) kq4.this.c.get());
            ms5.a(searchSecondTimezoneActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(searchSecondTimezoneActivity, (oq4) kq4.this.Y1.get());
            ms5.b(searchSecondTimezoneActivity, new gu5());
            r86.a(searchSecondTimezoneActivity, b());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public final x86 d(x86 x86) {
            z86.a(x86);
            return x86;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r implements fv4 {
        @DexIgnore
        public r() {
        }

        @DexIgnore
        @Override // com.fossil.fv4
        public void a(gv4 gv4) {
            b(gv4);
        }

        @DexIgnore
        public final gv4 b(gv4 gv4) {
            hv4.a(gv4, (po4) kq4.this.A3.get());
            return gv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements vs6 {
        @DexIgnore
        public r0(ys6 ys6) {
        }

        @DexIgnore
        @Override // com.fossil.vs6
        public void a(ws6 ws6) {
            b(ws6);
        }

        @DexIgnore
        public final ws6 b(ws6 ws6) {
            xs6.a(ws6, (po4) kq4.this.A3.get());
            return ws6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements iv6 {
        @DexIgnore
        public r1() {
        }

        @DexIgnore
        @Override // com.fossil.iv6
        public void a(jv6 jv6) {
            b(jv6);
        }

        @DexIgnore
        public final jv6 b(jv6 jv6) {
            kv6.a(jv6, (po4) kq4.this.A3.get());
            return jv6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r2 implements nz6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rz6 f2033a;

        @DexIgnore
        public r2(rz6 rz6) {
            this.f2033a = rz6;
        }

        @DexIgnore
        @Override // com.fossil.nz6
        public void a(SignUpActivity signUpActivity) {
            c(signUpActivity);
        }

        @DexIgnore
        public final uz6 b() {
            uz6 a2 = vz6.a(tz6.a(this.f2033a), sz6.a(this.f2033a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity c(SignUpActivity signUpActivity) {
            ms5.e(signUpActivity, (UserRepository) kq4.this.y.get());
            ms5.d(signUpActivity, (on5) kq4.this.c.get());
            ms5.a(signUpActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(signUpActivity, (oq4) kq4.this.Y1.get());
            ms5.b(signUpActivity, new gu5());
            mz6.e(signUpActivity, b());
            mz6.a(signUpActivity, (un5) kq4.this.U1.get());
            mz6.b(signUpActivity, (vn5) kq4.this.d0.get());
            mz6.d(signUpActivity, (xn5) kq4.this.V1.get());
            mz6.c(signUpActivity, (wn5) kq4.this.W1.get());
            return signUpActivity;
        }

        @DexIgnore
        public final uz6 d(uz6 uz6) {
            wz6.u(uz6, kq4.this.z4());
            wz6.y(uz6, (xn5) kq4.this.V1.get());
            wz6.v(uz6, kq4.this.A4());
            wz6.z(uz6, kq4.this.D4());
            wz6.x(uz6, kq4.this.C4());
            wz6.w(uz6, kq4.this.B4());
            wz6.H(uz6, (UserRepository) kq4.this.y.get());
            wz6.g(uz6, (DeviceRepository) kq4.this.P.get());
            wz6.G(uz6, (uq4) kq4.this.R.get());
            wz6.o(uz6, kq4.this.m4());
            wz6.p(uz6, kq4.this.n4());
            wz6.j(uz6, kq4.this.h4());
            wz6.q(uz6, kq4.this.o4());
            wz6.n(uz6, kq4.this.l4());
            wz6.l(uz6, kq4.this.j4());
            wz6.c(uz6, (AlarmsRepository) kq4.this.A.get());
            wz6.A(uz6, new ht5());
            wz6.h(uz6, kq4.this.a4());
            wz6.i(uz6, kq4.this.f4());
            wz6.D(uz6, (on5) kq4.this.c.get());
            wz6.e(uz6, kq4.this.R3());
            wz6.f(uz6, kq4.this.S3());
            wz6.d(uz6, (ck5) kq4.this.P0.get());
            wz6.s(uz6, kq4.this.u4());
            wz6.F(uz6, (SummariesRepository) kq4.this.D.get());
            wz6.E(uz6, (SleepSummariesRepository) kq4.this.E.get());
            wz6.t(uz6, (GoalTrackingRepository) kq4.this.c0.get());
            wz6.k(uz6, kq4.this.i4());
            wz6.m(uz6, kq4.this.k4());
            wz6.B(uz6, kq4.this.G4());
            wz6.r(uz6, kq4.this.t4());
            wz6.I(uz6, kq4.this.X4());
            wz6.C(uz6, (on5) kq4.this.c.get());
            wz6.a(uz6, (vt4) kq4.this.J0.get());
            wz6.b(uz6, (pr4) kq4.this.u1.get());
            wz6.J(uz6, kq4.this.Z4());
            wz6.K(uz6);
            return uz6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s implements mv4 {
        @DexIgnore
        public s() {
        }

        @DexIgnore
        @Override // com.fossil.mv4
        public void a(nv4 nv4) {
            b(nv4);
        }

        @DexIgnore
        public final nv4 b(nv4 nv4) {
            ov4.a(nv4, (po4) kq4.this.A3.get());
            return nv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements bt6 {
        @DexIgnore
        public s0(et6 et6) {
        }

        @DexIgnore
        @Override // com.fossil.bt6
        public void a(ct6 ct6) {
            b(ct6);
        }

        @DexIgnore
        public final ct6 b(ct6 ct6) {
            dt6.a(ct6, (po4) kq4.this.A3.get());
            return ct6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements l06 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ p06 f2036a;

        @DexIgnore
        public s1(p06 p06) {
            this.f2036a = p06;
        }

        @DexIgnore
        @Override // com.fossil.l06
        public void a(NotificationAppsActivity notificationAppsActivity) {
            d(notificationAppsActivity);
        }

        @DexIgnore
        public final r06 b() {
            r06 a2 = s06.a(q06.a(this.f2036a), (uq4) kq4.this.R.get(), new v36(), new d26(), c(), (on5) kq4.this.c.get(), (NotificationSettingsDatabase) kq4.this.j.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final u06 c() {
            return new u06((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationAppsActivity d(NotificationAppsActivity notificationAppsActivity) {
            ms5.e(notificationAppsActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationAppsActivity, (on5) kq4.this.c.get());
            ms5.a(notificationAppsActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationAppsActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationAppsActivity, new gu5());
            k06.a(notificationAppsActivity, b());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final r06 e(r06 r06) {
            t06.a(r06);
            return r06;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s2 implements rm6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ vm6 f2037a;

        @DexIgnore
        public s2(vm6 vm6) {
            this.f2037a = vm6;
        }

        @DexIgnore
        @Override // com.fossil.rm6
        public void a(SleepDetailActivity sleepDetailActivity) {
            c(sleepDetailActivity);
        }

        @DexIgnore
        public final xm6 b() {
            xm6 a2 = ym6.a(wm6.a(this.f2037a), (SleepSummariesRepository) kq4.this.E.get(), (SleepSessionsRepository) kq4.this.b0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity c(SleepDetailActivity sleepDetailActivity) {
            ms5.e(sleepDetailActivity, (UserRepository) kq4.this.y.get());
            ms5.d(sleepDetailActivity, (on5) kq4.this.c.get());
            ms5.a(sleepDetailActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(sleepDetailActivity, (oq4) kq4.this.Y1.get());
            ms5.b(sleepDetailActivity, new gu5());
            qm6.a(sleepDetailActivity, b());
            return sleepDetailActivity;
        }

        @DexIgnore
        public final xm6 d(xm6 xm6) {
            zm6.a(xm6);
            return xm6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t implements rv4 {
        @DexIgnore
        public t() {
        }

        @DexIgnore
        @Override // com.fossil.rv4
        public void a(sv4 sv4) {
            b(sv4);
        }

        @DexIgnore
        public final sv4 b(sv4 sv4) {
            tv4.a(sv4, (po4) kq4.this.A3.get());
            return sv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements ht6 {
        @DexIgnore
        public t0(kt6 kt6) {
        }

        @DexIgnore
        @Override // com.fossil.ht6
        public void a(it6 it6) {
            b(it6);
        }

        @DexIgnore
        public final it6 b(it6 it6) {
            jt6.a(it6, (po4) kq4.this.A3.get());
            return it6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements w06 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ b16 f2040a;

        @DexIgnore
        public t1(b16 b16) {
            this.f2040a = b16;
        }

        @DexIgnore
        @Override // com.fossil.w06
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            e(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final d16 b() {
            d16 a2 = e16.a(c16.a(this.f2040a), (uq4) kq4.this.R.get(), new d26(), c(), d(), new v36(), (on5) kq4.this.c.get(), (NotificationSettingsDao) kq4.this.B3.get(), kq4.this.K4(), (QuickResponseRepository) kq4.this.i.get(), (NotificationSettingsDatabase) kq4.this.j.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final f26 c() {
            return new f26((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final g26 d() {
            return new g26((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity e(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            ms5.e(notificationCallsAndMessagesActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationCallsAndMessagesActivity, (on5) kq4.this.c.get());
            ms5.a(notificationCallsAndMessagesActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationCallsAndMessagesActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationCallsAndMessagesActivity, new gu5());
            v06.a(notificationCallsAndMessagesActivity, b());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final d16 f(d16 d16) {
            f16.a(d16);
            return d16;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t2 implements qj6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ yj6 f2041a;

        @DexIgnore
        public t2(yj6 yj6) {
            this.f2041a = yj6;
        }

        @DexIgnore
        @Override // com.fossil.qj6
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            f(sleepOverviewFragment);
        }

        @DexIgnore
        public final uj6 b() {
            uj6 a2 = vj6.a(zj6.a(this.f2041a), (SleepSummariesRepository) kq4.this.E.get(), (SleepSessionsRepository) kq4.this.b0.get(), (PortfolioApp) kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final fk6 c() {
            fk6 a2 = gk6.a(ak6.a(this.f2041a), (UserRepository) kq4.this.y.get(), (SleepSummariesRepository) kq4.this.E.get(), (PortfolioApp) kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final lk6 d() {
            lk6 a2 = mk6.a(bk6.a(this.f2041a), (UserRepository) kq4.this.y.get(), (SleepSummariesRepository) kq4.this.E.get(), (PortfolioApp) kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final uj6 e(uj6 uj6) {
            wj6.a(uj6);
            return uj6;
        }

        @DexIgnore
        public final SleepOverviewFragment f(SleepOverviewFragment sleepOverviewFragment) {
            xj6.a(sleepOverviewFragment, b());
            xj6.c(sleepOverviewFragment, d());
            xj6.b(sleepOverviewFragment, c());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public final fk6 g(fk6 fk6) {
            hk6.a(fk6);
            return fk6;
        }

        @DexIgnore
        public final lk6 h(lk6 lk6) {
            nk6.a(lk6);
            return lk6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements yv4 {
        @DexIgnore
        public u() {
        }

        @DexIgnore
        @Override // com.fossil.yv4
        public void a(zv4 zv4) {
            b(zv4);
        }

        @DexIgnore
        public final zv4 b(zv4 zv4) {
            aw4.a(zv4, (po4) kq4.this.A3.get());
            return zv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements yc6 {
        @DexIgnore
        public u0() {
        }

        @DexIgnore
        @Override // com.fossil.yc6
        public void a(s66 s66) {
            b(s66);
        }

        @DexIgnore
        public final s66 b(s66 s66) {
            t66.a(s66, (po4) kq4.this.A3.get());
            return s66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements f46 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ j46 f2044a;

        @DexIgnore
        public u1(j46 j46) {
            this.f2044a = j46;
        }

        @DexIgnore
        @Override // com.fossil.f46
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            f(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final l56 b() {
            return new l56((Context) kq4.this.b.get());
        }

        @DexIgnore
        public final m46 c() {
            m46 a2 = n46.a(k46.a(this.f2044a), l46.a(this.f2044a), this.f2044a.a(), (uq4) kq4.this.R.get(), new y56(), b(), d(), e());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final k66 d() {
            return new k66((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final lt5 e() {
            return new lt5((NotificationsRepository) kq4.this.I.get(), (DeviceRepository) kq4.this.P.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity f(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            ms5.e(notificationContactsAndAppsAssignedActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationContactsAndAppsAssignedActivity, (on5) kq4.this.c.get());
            ms5.a(notificationContactsAndAppsAssignedActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationContactsAndAppsAssignedActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationContactsAndAppsAssignedActivity, new gu5());
            e46.a(notificationContactsAndAppsAssignedActivity, c());
            return notificationContactsAndAppsAssignedActivity;
        }

        @DexIgnore
        public final m46 g(m46 m46) {
            o46.a(m46);
            return m46;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u2 implements h07 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ k07 f2045a;

        @DexIgnore
        public u2(k07 k07) {
            this.f2045a = k07;
        }

        @DexIgnore
        @Override // com.fossil.h07
        public void a(SplashScreenActivity splashScreenActivity) {
            d(splashScreenActivity);
        }

        @DexIgnore
        public final m07 b() {
            m07 a2 = n07.a(l07.a(this.f2045a), (UserRepository) kq4.this.y.get(), (do5) kq4.this.x3.get(), kq4.this.a4(), (ThemeRepository) kq4.this.y1.get());
            c(a2);
            return a2;
        }

        @DexIgnore
        public final m07 c(m07 m07) {
            o07.a(m07);
            return m07;
        }

        @DexIgnore
        public final SplashScreenActivity d(SplashScreenActivity splashScreenActivity) {
            ms5.e(splashScreenActivity, (UserRepository) kq4.this.y.get());
            ms5.d(splashScreenActivity, (on5) kq4.this.c.get());
            ms5.a(splashScreenActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(splashScreenActivity, (oq4) kq4.this.Y1.get());
            ms5.b(splashScreenActivity, new gu5());
            p07.a(splashScreenActivity, b());
            return splashScreenActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v implements ew4 {
        @DexIgnore
        public v() {
        }

        @DexIgnore
        @Override // com.fossil.ew4
        public void a(fw4 fw4) {
            b(fw4);
        }

        @DexIgnore
        public final fw4 b(fw4 fw4) {
            gw4.a(fw4, (po4) kq4.this.A3.get());
            return fw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements cd6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ dd6 f2047a;

        @DexIgnore
        public v0(dd6 dd6) {
            this.f2047a = dd6;
        }

        @DexIgnore
        @Override // com.fossil.cd6
        public void a(az5 az5) {
            n(az5);
        }

        @DexIgnore
        public final sd6 b() {
            sd6 a2 = td6.a(ed6.a(this.f2047a), (SummariesRepository) kq4.this.D.get(), kq4.this.p4(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final we6 c() {
            we6 a2 = xe6.a(fd6.a(this.f2047a), (SummariesRepository) kq4.this.D.get(), kq4.this.p4(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            i(a2);
            return a2;
        }

        @DexIgnore
        public final bg6 d() {
            bg6 a2 = cg6.a(gd6.a(this.f2047a), (SummariesRepository) kq4.this.D.get(), kq4.this.p4(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            j(a2);
            return a2;
        }

        @DexIgnore
        public final fh6 e() {
            fh6 a2 = gh6.a(hd6.a(this.f2047a), (GoalTrackingRepository) kq4.this.c0.get(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            k(a2);
            return a2;
        }

        @DexIgnore
        public final ji6 f() {
            ji6 a2 = ki6.a(id6.a(this.f2047a), (HeartRateSummaryRepository) kq4.this.l0.get(), kq4.this.p4(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            l(a2);
            return a2;
        }

        @DexIgnore
        public final nj6 g() {
            nj6 a2 = oj6.a(jd6.a(this.f2047a), (SleepSummariesRepository) kq4.this.E.get(), (SleepSessionsRepository) kq4.this.b0.get(), kq4.this.p4(), (UserRepository) kq4.this.y.get(), (no4) kq4.this.G.get());
            m(a2);
            return a2;
        }

        @DexIgnore
        public final sd6 h(sd6 sd6) {
            ud6.a(sd6);
            return sd6;
        }

        @DexIgnore
        public final we6 i(we6 we6) {
            ye6.a(we6);
            return we6;
        }

        @DexIgnore
        public final bg6 j(bg6 bg6) {
            dg6.a(bg6);
            return bg6;
        }

        @DexIgnore
        public final fh6 k(fh6 fh6) {
            hh6.a(fh6);
            return fh6;
        }

        @DexIgnore
        public final ji6 l(ji6 ji6) {
            li6.a(ji6);
            return ji6;
        }

        @DexIgnore
        public final nj6 m(nj6 nj6) {
            pj6.a(nj6);
            return nj6;
        }

        @DexIgnore
        public final az5 n(az5 az5) {
            cz5.b(az5, c());
            cz5.a(az5, b());
            cz5.c(az5, d());
            cz5.e(az5, f());
            cz5.f(az5, g());
            cz5.d(az5, e());
            return az5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements h16 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ l16 f2048a;

        @DexIgnore
        public v1(l16 l16) {
            this.f2048a = l16;
        }

        @DexIgnore
        @Override // com.fossil.h16
        public void a(NotificationContactsActivity notificationContactsActivity) {
            d(notificationContactsActivity);
        }

        @DexIgnore
        public final o16 b() {
            o16 a2 = p16.a(n16.a(this.f2048a), (uq4) kq4.this.R.get(), new d26(), c(), m16.a(this.f2048a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final g26 c() {
            return new g26((NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationContactsActivity d(NotificationContactsActivity notificationContactsActivity) {
            ms5.e(notificationContactsActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationContactsActivity, (on5) kq4.this.c.get());
            ms5.a(notificationContactsActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationContactsActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationContactsActivity, new gu5());
            g16.a(notificationContactsActivity, b());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final o16 e(o16 o16) {
            q16.a(o16);
            return o16;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v2 implements yq6 {
        @DexIgnore
        public v2(br6 br6) {
        }

        @DexIgnore
        @Override // com.fossil.yq6
        public void a(zq6 zq6) {
            b(zq6);
        }

        @DexIgnore
        public final zq6 b(zq6 zq6) {
            ar6.a(zq6, (po4) kq4.this.A3.get());
            return zq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w implements tx4 {
        @DexIgnore
        public w() {
        }

        @DexIgnore
        @Override // com.fossil.tx4
        public void a(ux4 ux4) {
            b(ux4);
        }

        @DexIgnore
        public final ux4 b(ux4 ux4) {
            vx4.a(ux4, (po4) kq4.this.A3.get());
            return ux4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements gh5 {
        @DexIgnore
        public w0() {
        }

        @DexIgnore
        @Override // com.fossil.gh5
        public void a(DeepLinkActivity deepLinkActivity) {
            b(deepLinkActivity);
        }

        @DexIgnore
        public final DeepLinkActivity b(DeepLinkActivity deepLinkActivity) {
            ms5.e(deepLinkActivity, (UserRepository) kq4.this.y.get());
            ms5.d(deepLinkActivity, (on5) kq4.this.c.get());
            ms5.a(deepLinkActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(deepLinkActivity, (oq4) kq4.this.Y1.get());
            ms5.b(deepLinkActivity, new gu5());
            fh5.a(deepLinkActivity, (po4) kq4.this.A3.get());
            return deepLinkActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements q46 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ u46 f2052a;

        @DexIgnore
        public w1(u46 u46) {
            this.f2052a = u46;
        }

        @DexIgnore
        @Override // com.fossil.q46
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            d(notificationDialLandingActivity);
        }

        @DexIgnore
        public final x46 b() {
            x46 a2 = y46.a(w46.a(this.f2052a), c(), v46.a(this.f2052a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader c() {
            return NotificationsLoader_Factory.newInstance((Context) kq4.this.b.get(), (NotificationsRepository) kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationDialLandingActivity d(NotificationDialLandingActivity notificationDialLandingActivity) {
            ms5.e(notificationDialLandingActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationDialLandingActivity, (on5) kq4.this.c.get());
            ms5.a(notificationDialLandingActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationDialLandingActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationDialLandingActivity, new gu5());
            p46.a(notificationDialLandingActivity, b());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final x46 e(x46 x46) {
            z46.a(x46);
            return x46;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w2 implements r07 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ v07 f2053a;

        @DexIgnore
        public w2(v07 v07) {
            this.f2053a = v07;
        }

        @DexIgnore
        @Override // com.fossil.r07
        public void a(TroubleshootingActivity troubleshootingActivity) {
            c(troubleshootingActivity);
        }

        @DexIgnore
        public final x07 b() {
            x07 a2 = y07.a(w07.a(this.f2053a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity c(TroubleshootingActivity troubleshootingActivity) {
            ms5.e(troubleshootingActivity, (UserRepository) kq4.this.y.get());
            ms5.d(troubleshootingActivity, (on5) kq4.this.c.get());
            ms5.a(troubleshootingActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(troubleshootingActivity, (oq4) kq4.this.Y1.get());
            ms5.b(troubleshootingActivity, new gu5());
            q07.a(troubleshootingActivity, b());
            return troubleshootingActivity;
        }

        @DexIgnore
        public final x07 d(x07 x07) {
            z07.a(x07);
            return x07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x implements kw4 {
        @DexIgnore
        public x() {
        }

        @DexIgnore
        @Override // com.fossil.kw4
        public void a(lw4 lw4) {
            b(lw4);
        }

        @DexIgnore
        public final lw4 b(lw4 lw4) {
            nw4.a(lw4, (po4) kq4.this.A3.get());
            return lw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements wp6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ zp6 f2055a;

        @DexIgnore
        public x0(zp6 zp6) {
            this.f2055a = zp6;
        }

        @DexIgnore
        @Override // com.fossil.wp6
        public void a(DeleteAccountActivity deleteAccountActivity) {
            d(deleteAccountActivity);
        }

        @DexIgnore
        public final bq6 b() {
            bq6 a2 = cq6.a(aq6.a(this.f2055a), (DeviceRepository) kq4.this.P.get(), (ck5) kq4.this.P0.get(), (UserRepository) kq4.this.y.get(), c(), kq4.this.Z3());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ou5 c() {
            return new ou5((UserRepository) kq4.this.y.get(), (DeviceRepository) kq4.this.P.get(), (on5) kq4.this.c.get());
        }

        @DexIgnore
        public final DeleteAccountActivity d(DeleteAccountActivity deleteAccountActivity) {
            ms5.e(deleteAccountActivity, (UserRepository) kq4.this.y.get());
            ms5.d(deleteAccountActivity, (on5) kq4.this.c.get());
            ms5.a(deleteAccountActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(deleteAccountActivity, (oq4) kq4.this.Y1.get());
            ms5.b(deleteAccountActivity, new gu5());
            vp6.a(deleteAccountActivity, b());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final bq6 e(bq6 bq6) {
            dq6.a(bq6);
            return bq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements b56 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ f56 f2056a;

        @DexIgnore
        public x1(f56 f56) {
            this.f2056a = f56;
        }

        @DexIgnore
        @Override // com.fossil.b56
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            d(notificationHybridAppActivity);
        }

        @DexIgnore
        public final l56 b() {
            return new l56((Context) kq4.this.b.get());
        }

        @DexIgnore
        public final i56 c() {
            i56 a2 = j56.a(h56.a(this.f2056a), this.f2056a.a(), g56.a(this.f2056a), (uq4) kq4.this.R.get(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridAppActivity d(NotificationHybridAppActivity notificationHybridAppActivity) {
            ms5.e(notificationHybridAppActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationHybridAppActivity, (on5) kq4.this.c.get());
            ms5.a(notificationHybridAppActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationHybridAppActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationHybridAppActivity, new gu5());
            a56.a(notificationHybridAppActivity, c());
            return notificationHybridAppActivity;
        }

        @DexIgnore
        public final i56 e(i56 i56) {
            k56.a(i56);
            return i56;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x2 implements ow6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rw6 f2057a;

        @DexIgnore
        public x2(rw6 rw6) {
            this.f2057a = rw6;
        }

        @DexIgnore
        @Override // com.fossil.ow6
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            d(updateFirmwareActivity);
        }

        @DexIgnore
        public final ct5 b() {
            return new ct5((PortfolioApp) kq4.this.d.get(), (GuestApiService) kq4.this.F.get());
        }

        @DexIgnore
        public final tw6 c() {
            tw6 a2 = uw6.a(sw6.a(this.f2057a), (DeviceRepository) kq4.this.P.get(), (UserRepository) kq4.this.y.get(), kq4.this.a4(), (on5) kq4.this.c.get(), kq4.this.P4(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final UpdateFirmwareActivity d(UpdateFirmwareActivity updateFirmwareActivity) {
            ms5.e(updateFirmwareActivity, (UserRepository) kq4.this.y.get());
            ms5.d(updateFirmwareActivity, (on5) kq4.this.c.get());
            ms5.a(updateFirmwareActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(updateFirmwareActivity, (oq4) kq4.this.Y1.get());
            ms5.b(updateFirmwareActivity, new gu5());
            nx6.a(updateFirmwareActivity, c());
            return updateFirmwareActivity;
        }

        @DexIgnore
        public final tw6 e(tw6 tw6) {
            vw6.a(tw6);
            return tw6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y implements fx6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ix6 f2058a;

        @DexIgnore
        public y(ix6 ix6) {
            this.f2058a = ix6;
        }

        @DexIgnore
        @Override // com.fossil.fx6
        public void a(yo6 yo6) {
            e(yo6);
        }

        @DexIgnore
        @Override // com.fossil.fx6
        public void b(xv5 xv5) {
            f(xv5);
        }

        @DexIgnore
        public final kx6 c() {
            kx6 a2 = lx6.a(jx6.a(this.f2058a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final kx6 d(kx6 kx6) {
            mx6.a(kx6);
            return kx6;
        }

        @DexIgnore
        public final yo6 e(yo6 yo6) {
            ap6.b(yo6, (po4) kq4.this.A3.get());
            ap6.a(yo6, c());
            return yo6;
        }

        @DexIgnore
        public final xv5 f(xv5 xv5) {
            zv5.a(xv5, c());
            return xv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements u66 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ v66 f2059a;

        @DexIgnore
        public y0(v66 v66) {
            this.f2059a = v66;
        }

        @DexIgnore
        @Override // com.fossil.u66
        public void a(WatchAppEditActivity watchAppEditActivity) {
            d(watchAppEditActivity);
        }

        @DexIgnore
        public final jb6 b() {
            return new jb6((uo5) kq4.this.M0.get(), (FileRepository) kq4.this.I0.get(), kq4.this.b4(), kq4.this.b4());
        }

        @DexIgnore
        public final h76 c() {
            h76 a2 = i76.a(w66.a(this.f2059a), b(), (on5) kq4.this.c.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppEditActivity d(WatchAppEditActivity watchAppEditActivity) {
            ms5.e(watchAppEditActivity, (UserRepository) kq4.this.y.get());
            ms5.d(watchAppEditActivity, (on5) kq4.this.c.get());
            ms5.a(watchAppEditActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(watchAppEditActivity, (oq4) kq4.this.Y1.get());
            ms5.b(watchAppEditActivity, new gu5());
            c76.a(watchAppEditActivity, c());
            c76.b(watchAppEditActivity, (po4) kq4.this.A3.get());
            return watchAppEditActivity;
        }

        @DexIgnore
        public final h76 e(h76 h76) {
            j76.a(h76);
            return h76;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements n56 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ r56 f2060a;

        @DexIgnore
        public y1(r56 r56) {
            this.f2060a = r56;
        }

        @DexIgnore
        @Override // com.fossil.n56
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            c(notificationHybridContactActivity);
        }

        @DexIgnore
        public final v56 b() {
            v56 a2 = w56.a(u56.a(this.f2060a), this.f2060a.b(), s56.a(this.f2060a), t56.a(this.f2060a), (uq4) kq4.this.R.get(), new y56());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity c(NotificationHybridContactActivity notificationHybridContactActivity) {
            ms5.e(notificationHybridContactActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationHybridContactActivity, (on5) kq4.this.c.get());
            ms5.a(notificationHybridContactActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationHybridContactActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationHybridContactActivity, new gu5());
            m56.a(notificationHybridContactActivity, b());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public final v56 d(v56 v56) {
            x56.a(v56);
            return v56;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y2 implements ot6 {
        @DexIgnore
        public y2(rt6 rt6) {
        }

        @DexIgnore
        @Override // com.fossil.ot6
        public void a(pt6 pt6) {
            b(pt6);
        }

        @DexIgnore
        public final pt6 b(pt6 pt6) {
            qt6.a(pt6, (po4) kq4.this.A3.get());
            return pt6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public uo4 f2062a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public MicroAppSettingRepositoryModule c;
        @DexIgnore
        public RepositoriesModule d;
        @DexIgnore
        public NotificationsRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public z() {
        }

        @DexIgnore
        public z a(uo4 uo4) {
            lk7.b(uo4);
            this.f2062a = uo4;
            return this;
        }

        @DexIgnore
        public ro4 b() {
            lk7.a(this.f2062a, uo4.class);
            if (this.b == null) {
                this.b = new PortfolioDatabaseModule();
            }
            if (this.c == null) {
                this.c = new MicroAppSettingRepositoryModule();
            }
            if (this.d == null) {
                this.d = new RepositoriesModule();
            }
            if (this.e == null) {
                this.e = new NotificationsRepositoryModule();
            }
            if (this.f == null) {
                this.f = new UAppSystemVersionRepositoryModule();
            }
            return new kq4(this.f2062a, this.b, this.c, this.d, this.e, this.f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements l96 {
        @DexIgnore
        public z0() {
        }

        @DexIgnore
        @Override // com.fossil.l96
        public void a(m96 m96) {
            b(m96);
        }

        @DexIgnore
        public final m96 b(m96 m96) {
            n96.a(m96, (po4) kq4.this.A3.get());
            return m96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z1 implements a66 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ e66 f2064a;

        @DexIgnore
        public z1(e66 e66) {
            this.f2064a = e66;
        }

        @DexIgnore
        @Override // com.fossil.a66
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            c(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final h66 b() {
            h66 a2 = i66.a(g66.a(this.f2064a), this.f2064a.b(), f66.a(this.f2064a), (uq4) kq4.this.R.get(), new y56());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity c(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            ms5.e(notificationHybridEveryoneActivity, (UserRepository) kq4.this.y.get());
            ms5.d(notificationHybridEveryoneActivity, (on5) kq4.this.c.get());
            ms5.a(notificationHybridEveryoneActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(notificationHybridEveryoneActivity, (oq4) kq4.this.Y1.get());
            ms5.b(notificationHybridEveryoneActivity, new gu5());
            z56.a(notificationHybridEveryoneActivity, b());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public final h66 d(h66 h66) {
            j66.a(h66);
            return h66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z2 implements za6 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ db6 f2065a;

        @DexIgnore
        public z2(db6 db6) {
            this.f2065a = db6;
        }

        @DexIgnore
        @Override // com.fossil.za6
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            c(watchAppSearchActivity);
        }

        @DexIgnore
        public final fb6 b() {
            fb6 a2 = gb6.a(eb6.a(this.f2065a), kq4.this.U4(), (on5) kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity c(WatchAppSearchActivity watchAppSearchActivity) {
            ms5.e(watchAppSearchActivity, (UserRepository) kq4.this.y.get());
            ms5.d(watchAppSearchActivity, (on5) kq4.this.c.get());
            ms5.a(watchAppSearchActivity, (DeviceRepository) kq4.this.P.get());
            ms5.c(watchAppSearchActivity, (oq4) kq4.this.Y1.get());
            ms5.b(watchAppSearchActivity, new gu5());
            ya6.a(watchAppSearchActivity, b());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public final fb6 d(fb6 fb6) {
            hb6.a(fb6);
            return fb6;
        }
    }

    @DexIgnore
    public kq4(uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.f1946a = uo4;
        a5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        b5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        c5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static z O3() {
        return new z();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void A(DebugActivity debugActivity) {
        o5(debugActivity);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void A0(NetworkChangedReceiver networkChangedReceiver) {
        y5(networkChangedReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void A1(FossilNotificationListenerService fossilNotificationListenerService) {
        t5(fossilNotificationListenerService);
    }

    @DexIgnore
    public final ev5 A4() {
        return new ev5(this.d0.get());
    }

    @DexIgnore
    public final PortfolioApp A5(PortfolioApp portfolioApp) {
        qq4.D(portfolioApp, this.c.get());
        qq4.v(portfolioApp, this.D.get());
        qq4.u(portfolioApp, this.E.get());
        qq4.b(portfolioApp, this.B.get());
        qq4.p(portfolioApp, this.F.get());
        qq4.f(portfolioApp, this.G.get());
        qq4.d(portfolioApp, this.o.get());
        qq4.g(portfolioApp, U3());
        qq4.x(portfolioApp, this.R.get());
        qq4.k(portfolioApp, Z3());
        qq4.c(portfolioApp, this.P0.get());
        qq4.e(portfolioApp, this.z1.get());
        qq4.l(portfolioApp, this.P.get());
        qq4.n(portfolioApp, this.C.get());
        qq4.t(portfolioApp, this.A1.get());
        qq4.m(portfolioApp, this.q.get());
        qq4.h(portfolioApp, X3());
        qq4.A(portfolioApp, X4());
        qq4.i(portfolioApp, this.H1.get());
        qq4.y(portfolioApp, this.y.get());
        qq4.E(portfolioApp, W4());
        qq4.s(portfolioApp, this.i.get());
        qq4.C(portfolioApp, Z4());
        qq4.z(portfolioApp, T4());
        qq4.B(portfolioApp, this.L1.get());
        qq4.r(portfolioApp, this.M1.get());
        qq4.q(portfolioApp, this.N1.get());
        qq4.w(portfolioApp, this.y1.get());
        qq4.j(portfolioApp, Y3());
        qq4.o(portfolioApp, q4());
        qq4.a(portfolioApp, this.O1.get());
        return portfolioApp;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public w06 B(b16 b16) {
        lk7.b(b16);
        return new t1(b16);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ou4 B0() {
        return new j();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public hx4 B1() {
        return new k();
    }

    @DexIgnore
    public final fv5 B4() {
        return new fv5(this.y.get());
    }

    @DexIgnore
    public final cx6 B5(cx6 cx6) {
        ex6.c(cx6, L4());
        ex6.d(cx6, M4());
        ex6.b(cx6, u4());
        ex6.a(cx6, this.P0.get());
        ex6.e(cx6);
        return cx6;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public wr6 C(zr6 zr6) {
        lk7.b(zr6);
        return new n0(zr6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public fv4 C0() {
        return new r();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public fx6 C1(ix6 ix6) {
        lk7.b(ix6);
        return new y(ix6);
    }

    @DexIgnore
    public final gv5 C4() {
        return new gv5(this.W1.get());
    }

    @DexIgnore
    public final uz6 C5(uz6 uz6) {
        wz6.u(uz6, z4());
        wz6.y(uz6, this.V1.get());
        wz6.v(uz6, A4());
        wz6.z(uz6, D4());
        wz6.x(uz6, C4());
        wz6.w(uz6, B4());
        wz6.H(uz6, this.y.get());
        wz6.g(uz6, this.P.get());
        wz6.G(uz6, this.R.get());
        wz6.o(uz6, m4());
        wz6.p(uz6, n4());
        wz6.j(uz6, h4());
        wz6.q(uz6, o4());
        wz6.n(uz6, l4());
        wz6.l(uz6, j4());
        wz6.c(uz6, this.A.get());
        wz6.A(uz6, new ht5());
        wz6.h(uz6, a4());
        wz6.i(uz6, f4());
        wz6.D(uz6, this.c.get());
        wz6.e(uz6, R3());
        wz6.f(uz6, S3());
        wz6.d(uz6, this.P0.get());
        wz6.s(uz6, u4());
        wz6.F(uz6, this.D.get());
        wz6.E(uz6, this.E.get());
        wz6.t(uz6, this.c0.get());
        wz6.k(uz6, i4());
        wz6.m(uz6, k4());
        wz6.B(uz6, G4());
        wz6.r(uz6, t4());
        wz6.I(uz6, X4());
        wz6.C(uz6, this.c.get());
        wz6.a(uz6, this.J0.get());
        wz6.b(uz6, this.u1.get());
        wz6.J(uz6, Z4());
        wz6.K(uz6);
        return uz6;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public qr6 D(tr6 tr6) {
        lk7.b(tr6);
        return new m0(tr6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public bt6 D0(et6 et6) {
        lk7.b(et6);
        return new s0(et6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public rv4 D1() {
        return new t();
    }

    @DexIgnore
    public final hv5 D4() {
        return new hv5(this.V1.get());
    }

    @DexIgnore
    public final qn5 D5(qn5 qn5) {
        rn5.a(qn5, this.y1.get());
        return qn5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public vd6 E(de6 de6) {
        lk7.b(de6);
        return new d(de6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ew4 E0() {
        return new v();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public wx6 E1(zx6 zx6) {
        lk7.b(zx6);
        return new e2(zx6);
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<jc7<? extends ListenableWorker>>> E4() {
        return a34.of(PushPendingDataWorker.class, this.E1);
    }

    @DexIgnore
    public final TimeChangeReceiver E5(TimeChangeReceiver timeChangeReceiver) {
        lc7.a(timeChangeReceiver, this.B.get());
        lc7.b(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public nx4 F() {
        return new m();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public xw6 F0(ax6 ax6) {
        lk7.b(ax6);
        return new k2(ax6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public dp6 F1() {
        return new i2();
    }

    @DexIgnore
    public final MicroAppLastSettingRepository F4() {
        return new MicroAppLastSettingRepository(this.j0.get());
    }

    @DexIgnore
    public final go5 F5(go5 go5) {
        ho5.c(go5, this.c.get());
        ho5.b(go5, W3());
        ho5.a(go5, this.B.get());
        return go5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public pc6 G(tc6 tc6) {
        lk7.b(tc6);
        return new o2(tc6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public e87 G0() {
        return new b3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public wp6 G1(zp6 zp6) {
        lk7.b(zp6);
        return new x0(zp6);
    }

    @DexIgnore
    public final z27 G4() {
        return new z27(this.y.get());
    }

    @DexIgnore
    public final URLRequestTaskHelper G5(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.o.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public r07 H(v07 v07) {
        lk7.b(v07);
        return new w2(v07);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void H0(PortfolioApp portfolioApp) {
        A5(portfolioApp);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public e07 H1(f07 f07) {
        lk7.b(f07);
        return new a1(f07);
    }

    @DexIgnore
    public final RingStyleRemoteDataSource H4() {
        return new RingStyleRemoteDataSource(this.o.get());
    }

    @DexIgnore
    public final n47 H5(n47 n47) {
        o47.a(n47, this.c.get());
        return n47;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void I(n47 n47) {
        H5(n47);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public yz5 I0(d06 d06) {
        lk7.b(d06);
        return new j1(d06);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void I1(sn5 sn5) {
        K5(sn5);
    }

    @DexIgnore
    public final RingStyleRepository I4() {
        return new RingStyleRepository(H4(), this.I0.get());
    }

    @DexIgnore
    public final cs5 I5(cs5 cs5) {
        es5.d(cs5, this.d.get());
        es5.a(cs5, this.o.get());
        es5.c(cs5, this.K1.get());
        es5.f(cs5, this.y.get());
        es5.b(cs5, b4());
        es5.e(cs5, this.c.get());
        return cs5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public s86 J(v86 v86) {
        lk7.b(v86);
        return new q2(v86);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void J0(TimeChangeReceiver timeChangeReceiver) {
        E5(timeChangeReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ma6 J1() {
        return new g0();
    }

    @DexIgnore
    public final a37 J4() {
        return new a37(new v36(), new d26(), this.j.get(), this.I.get(), this.P.get(), this.c.get());
    }

    @DexIgnore
    public final pl5 J5(pl5 pl5) {
        ql5.a(pl5, this.c.get());
        return pl5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public rm6 K(vm6 vm6) {
        lk7.b(vm6);
        return new s2(vm6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public c17 K0() {
        return new k3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void K1(dk5 dk5) {
        e5(dk5);
    }

    @DexIgnore
    public final mt5 K4() {
        return new mt5(this.i.get());
    }

    @DexIgnore
    public final sn5 K5(sn5 sn5) {
        tn5.f(sn5, this.d.get());
        tn5.a(sn5, this.o.get());
        tn5.e(sn5, this.K1.get());
        tn5.g(sn5, this.y.get());
        tn5.b(sn5, this.t.get());
        tn5.c(sn5, b4());
        tn5.d(sn5, this.f2.get());
        return sn5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public o66 L(p66 p66) {
        lk7.b(p66);
        return new i0(p66);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ow6 L0(rw6 rw6) {
        lk7.b(rw6);
        return new x2(rw6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public mx5 L1(qx5 qx5) {
        lk7.b(qx5);
        return new g(qx5);
    }

    @DexIgnore
    public final jv5 L4() {
        return new jv5(this.y.get(), this.c.get());
    }

    @DexIgnore
    public final WorkoutDetailActivity L5(WorkoutDetailActivity workoutDetailActivity) {
        dn6.a(workoutDetailActivity, this.m0.get());
        return workoutDetailActivity;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public xb6 M(cc6 cc6) {
        lk7.b(cc6);
        return new m1(cc6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public qk6 M0(uk6 uk6) {
        lk7.b(uk6);
        return new c(uk6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ja6 M1() {
        return new f0();
    }

    @DexIgnore
    public final kv5 M4() {
        return new kv5(this.y.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public mp6 N(qp6 qp6) {
        lk7.b(qp6);
        return new i1(qp6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public tu6 N0(xu6 xu6) {
        lk7.b(xu6);
        return new o1(xu6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public jy4 N1() {
        return new p();
    }

    @DexIgnore
    public final lu5 N4() {
        return mu5.a(this.I.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void O(NotificationReceiver notificationReceiver) {
        z5(notificationReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public pb6 O0(qb6 qb6) {
        lk7.b(qb6);
        return new n1(qb6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void O1(qn5 qn5) {
        D5(qn5);
    }

    @DexIgnore
    public final xt5 O4() {
        return new xt5(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public b96 P(f96 f96) {
        lk7.b(f96);
        return new h0(f96);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ih6 P0(qh6 qh6) {
        lk7.b(qh6);
        return new f1(qh6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public q46 P1(u46 u46) {
        lk7.b(u46);
        return new w1(u46);
    }

    @DexIgnore
    public final CategoryRemoteDataSource P3() {
        return new CategoryRemoteDataSource(this.o.get());
    }

    @DexIgnore
    public final yt5 P4() {
        return new yt5(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void Q(p37 p37) {
        q5(p37);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public gy6 Q0(jy6 jy6) {
        lk7.b(jy6);
        return new d2(jy6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public gy4 Q1() {
        return new o();
    }

    @DexIgnore
    public final CategoryRepository Q3() {
        return new CategoryRepository(this.R0.get(), P3());
    }

    @DexIgnore
    public final xu5 Q4() {
        return new xu5(this.y.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void R(en5 en5) {
        u5(en5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public po6 R0(to6 to6) {
        lk7.b(to6);
        return new n2(to6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public nz6 R1(rz6 rz6) {
        lk7.b(rz6);
        return new r2(rz6);
    }

    @DexIgnore
    public final n27 R3() {
        return new n27(this.y.get());
    }

    @DexIgnore
    public final d37 R4() {
        return new d37(this.P.get(), this.y.get(), Y3(), this.d.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public t17 S(w17 w17) {
        lk7.b(w17);
        return new c1(w17);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void S0(av6 av6) {
        v5(av6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public gh5 S1() {
        return new w0();
    }

    @DexIgnore
    public final o27 S3() {
        return new o27(this.y.get());
    }

    @DexIgnore
    public final WatchAppDataRemoteDataSource S4() {
        return new WatchAppDataRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public rw4 T() {
        return new n();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public q76 T0(c86 c86) {
        lk7.b(c86);
        return new d0(c86);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public u96 T1(z96 z96) {
        lk7.b(z96);
        return new a3(z96);
    }

    @DexIgnore
    public final ComplicationRepository T3() {
        return new ComplicationRepository(this.f0.get(), this.d.get());
    }

    @DexIgnore
    public final WatchAppDataRepository T4() {
        return new WatchAppDataRepository(S4(), this.I0.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public iu6 U() {
        return new p3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public yc6 U0() {
        return new u0();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public t16 U1(y16 y16) {
        lk7.b(y16);
        return new a2(y16);
    }

    @DexIgnore
    public final l37 U3() {
        l37 a4 = m37.a(this.d.get(), this.I.get(), this.P.get(), this.j.get(), this.G.get(), this.Q.get(), N4(), this.c.get());
        n5(a4);
        return a4;
    }

    @DexIgnore
    public final WatchAppRepository U4() {
        return new WatchAppRepository(this.e0.get(), this.d.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public mv4 V() {
        return new s();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public hm6 V0(lm6 lm6) {
        lk7.b(lm6);
        return new g1(lm6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void V1(WorkoutDetailActivity workoutDetailActivity) {
        L5(workoutDetailActivity);
    }

    @DexIgnore
    public final zm5 V3() {
        return new zm5(this.d.get(), this.P.get(), b4());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource V4() {
        return new WatchFaceRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public en6 W() {
        return new n3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public yv4 W0() {
        return new u();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void W1(CloudImageHelper cloudImageHelper) {
        k5(cloudImageHelper);
    }

    @DexIgnore
    public final p27 W3() {
        return new p27(this.q.get(), this.t.get());
    }

    @DexIgnore
    public final WatchFaceRepository W4() {
        return new WatchFaceRepository(this.b.get(), V4(), this.I0.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public qa6 X(ta6 ta6) {
        lk7.b(ta6);
        return new l3(ta6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public y97 X0() {
        return new i3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public qj6 X1(yj6 yj6) {
        lk7.b(yj6);
        return new t2(yj6);
    }

    @DexIgnore
    public final ic7 X3() {
        return new ic7(E4());
    }

    @DexIgnore
    public final WatchLocalizationRepository X4() {
        return new WatchLocalizationRepository(this.o.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public er6 Y(hr6 hr6) {
        lk7.b(hr6);
        return new k0(hr6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public kw4 Y0() {
        return new x();
    }

    @DexIgnore
    public final q27 Y3() {
        return new q27(this.c.get());
    }

    @DexIgnore
    public final WorkoutSettingRemoteDataSource Y4() {
        return new WorkoutSettingRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void Z(MFDeviceService mFDeviceService) {
        w5(mFDeviceService);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public cs6 Z0(fs6 fs6) {
        lk7.b(fs6);
        return new o0(fs6);
    }

    @DexIgnore
    public final zu5 Z3() {
        return new zu5(this.y.get(), this.A.get(), this.c.get(), this.V.get(), this.W.get(), this.D.get(), this.a0.get(), this.I.get(), this.P.get(), this.b0.get(), this.c0.get(), this.d0.get(), w4(), this.E.get(), this.q.get(), U4(), T3(), this.j.get(), this.e.get(), this.i0.get(), F4(), p4(), this.k0.get(), this.l0.get(), this.m0.get(), this.n0.get(), this.s0.get(), this.w0.get(), this.A0.get(), this.E0.get(), this.I0.get(), this.i.get(), W4(), I4(), this.J0.get(), Z4(), b4(), e4(), T4(), this.M0.get(), this.O0.get());
    }

    @DexIgnore
    public final WorkoutSettingRepository Z4() {
        return new WorkoutSettingRepository(Y4());
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public js6 a(ms6 ms6) {
        lk7.b(ms6);
        return new p0(ms6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public mn6 a0() {
        return new o3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public da7 a1() {
        return new j3();
    }

    @DexIgnore
    public final mj5 a4() {
        return new mj5(r4(), s4(), x4(), c4());
    }

    @DexIgnore
    public final void a5(uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        Provider<Context> a4 = ik7.a(zo4.a(uo4));
        this.b = a4;
        this.c = ik7.a(up4.a(uo4, a4));
        Provider<PortfolioApp> a5 = ik7.a(bp4.a(uo4));
        this.d = a5;
        this.e = ik7.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(portfolioDatabaseModule, a5));
        Provider<QuickResponseDatabase> a6 = ik7.a(PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.f = a6;
        this.g = ik7.a(PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory.create(portfolioDatabaseModule, a6));
        Provider<QuickResponseSenderDao> a7 = ik7.a(PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory.create(portfolioDatabaseModule, this.f));
        this.h = a7;
        this.i = ik7.a(QuickResponseRepository_Factory.create(this.g, a7));
        Provider<NotificationSettingsDatabase> a8 = ik7.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.j = a8;
        this.k = ik7.a(xr5.a(this.c, this.e, this.i, a8));
        Provider<AuthApiGuestService> a9 = ik7.a(cp4.a(uo4, this.c));
        this.l = a9;
        this.m = ik7.a(rq5.a(this.d, a9, this.c));
        Provider<uq5> a10 = ik7.a(vq5.a(this.l, this.c));
        this.n = a10;
        Provider<ApiServiceV2> a11 = ik7.a(yo4.a(uo4, this.m, a10));
        this.o = a11;
        Provider<DianaPresetRemoteDataSource> a12 = ik7.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(repositoriesModule, a11));
        this.p = a12;
        this.q = ik7.a(DianaPresetRepository_Factory.create(a12));
        Provider<CustomizeRealDataDatabase> a13 = ik7.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.r = a13;
        Provider<CustomizeRealDataDao> a14 = ik7.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(portfolioDatabaseModule, a13));
        this.s = a14;
        this.t = ik7.a(CustomizeRealDataRepository_Factory.create(a14));
        Provider<AuthApiUserService> a15 = ik7.a(dp4.a(uo4, this.m, this.n));
        this.u = a15;
        this.v = UserRemoteDataSource_Factory.create(this.o, this.l, a15);
        Provider<UserSettingDatabase> a16 = ik7.a(PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.w = a16;
        Provider<UserSettingDao> a17 = ik7.a(PortfolioDatabaseModule_ProvideUserSettingDaoFactory.create(portfolioDatabaseModule, a16));
        this.x = a17;
        this.y = ik7.a(UserRepository_Factory.create(this.v, a17, this.c));
        Provider<AlarmsRemoteDataSource> a18 = ik7.a(AlarmsRemoteDataSource_Factory.create(this.o));
        this.z = a18;
        Provider<AlarmsRepository> a19 = ik7.a(AlarmsRepository_Factory.create(a18));
        this.A = a19;
        this.B = ik7.a(vo4.a(uo4, this.c, this.y, a19));
        Provider<sk5> a20 = ik7.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(portfolioDatabaseModule, this.c));
        this.C = a20;
        this.D = ik7.a(SummariesRepository_Factory.create(this.o, a20));
        this.E = ik7.a(SleepSummariesRepository_Factory.create(this.o));
        this.F = ik7.a(kp4.a(uo4, this.c));
        this.G = ik7.a(oo4.a());
        Provider<NotificationsDataSource> a21 = ik7.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(notificationsRepositoryModule));
        this.H = a21;
        this.I = ik7.a(NotificationsRepository_Factory.create(a21));
        Provider<DeviceDatabase> a22 = ik7.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.J = a22;
        this.K = ik7.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(portfolioDatabaseModule, a22));
        this.L = ik7.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(portfolioDatabaseModule, this.J));
        this.M = ik7.a(xo4.a(uo4, this.m, this.n));
        Provider<SecureApiService2Dot1> a23 = ik7.a(tp4.a(uo4, this.c));
        this.N = a23;
        DeviceRemoteDataSource_Factory create = DeviceRemoteDataSource_Factory.create(this.o, this.M, a23);
        this.O = create;
        this.P = ik7.a(DeviceRepository_Factory.create(this.K, this.L, create));
        this.Q = ik7.a(ep4.a(uo4));
        this.R = ik7.a(wp4.a(uo4));
        Provider<HybridCustomizeDatabase> a24 = ik7.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.S = a24;
        this.T = ik7.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(portfolioDatabaseModule, a24));
        HybridPresetRemoteDataSource_Factory create2 = HybridPresetRemoteDataSource_Factory.create(this.o);
        this.U = create2;
        this.V = ik7.a(HybridPresetRepository_Factory.create(this.T, create2));
        this.W = ik7.a(ActivitiesRepository_Factory.create(this.o, this.y, this.C));
        Provider<ShortcutApiService> a25 = ik7.a(vp4.a(uo4, this.m, this.n));
        this.X = a25;
        this.Y = ik7.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(microAppSettingRepositoryModule, a25, this.G));
        Provider<MicroAppSettingDataSource> a26 = ik7.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(microAppSettingRepositoryModule));
        this.Z = a26;
        this.a0 = ik7.a(MicroAppSettingRepository_Factory.create(this.Y, a26, this.G));
        this.b0 = ik7.a(SleepSessionsRepository_Factory.create(this.y, this.o));
        this.c0 = ik7.a(GoalTrackingRepository_Factory.create(this.y, this.c, this.o));
        this.d0 = ik7.a(op4.a(uo4));
        this.e0 = ik7.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.f0 = ik7.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.g0 = ik7.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(portfolioDatabaseModule, this.S));
        MicroAppRemoteDataSource_Factory create3 = MicroAppRemoteDataSource_Factory.create(this.o);
        this.h0 = create3;
        this.i0 = ik7.a(MicroAppRepository_Factory.create(this.g0, create3, this.d));
        this.j0 = ik7.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(portfolioDatabaseModule, this.S));
        this.k0 = ik7.a(HeartRateSampleRepository_Factory.create(this.o));
        this.l0 = ik7.a(HeartRateSummaryRepository_Factory.create(this.o));
        this.m0 = ik7.a(WorkoutSessionRepository_Factory.create(this.o));
        this.n0 = ik7.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        Provider<BuddyChallengeDatabase> a27 = ik7.a(PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.o0 = a27;
        Provider<jt4> a28 = ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory.create(portfolioDatabaseModule, a27));
        this.p0 = a28;
        this.q0 = ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory.create(portfolioDatabaseModule, a28));
        Provider<gu4> a29 = ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.r0 = a29;
        this.s0 = ik7.a(iu4.a(this.q0, a29, this.c));
        Provider<ys4> a30 = ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.t0 = a30;
        this.u0 = ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory.create(portfolioDatabaseModule, a30));
        Provider<yt4> a31 = ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.v0 = a31;
        this.w0 = ik7.a(au4.a(this.u0, a31, this.c));
        Provider<qs4> a32 = ik7.a(PortfolioDatabaseModule_ProvidesChallengeDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.x0 = a32;
        this.y0 = ik7.a(PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory.create(portfolioDatabaseModule, a32));
        Provider<st4> a33 = ik7.a(PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory.create(portfolioDatabaseModule, this.o));
        this.z0 = a33;
        this.A0 = ik7.a(ut4.a(this.y0, a33, this.c));
        Provider<ft4> a34 = ik7.a(PortfolioDatabaseModule_ProvidesNotificationDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.B0 = a34;
        this.C0 = ik7.a(PortfolioDatabaseModule_ProvidesNotificationLocalFactory.create(portfolioDatabaseModule, a34));
        Provider<cu4> a35 = ik7.a(PortfolioDatabaseModule_ProvidesNotificationRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.D0 = a35;
        this.E0 = ik7.a(eu4.a(this.C0, a35));
        Provider<FileDatabase> a36 = ik7.a(PortfolioDatabaseModule_ProvideFileDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.F0 = a36;
        this.G0 = ik7.a(PortfolioDatabaseModule_ProvideFileDaoFactory.create(portfolioDatabaseModule, a36));
        Provider<cn5> a37 = ik7.a(gp4.a(uo4));
        this.H0 = a37;
        this.I0 = ik7.a(FileRepository_Factory.create(this.G0, a37, this.d));
        this.J0 = ik7.a(wt4.a(this.c, this.o));
        Provider<SecureApiService> a38 = ik7.a(sp4.a(uo4, this.c));
        this.K0 = a38;
        to5 a39 = to5.a(this.o, a38);
        this.L0 = a39;
        this.M0 = ik7.a(vo5.a(a39));
        Provider<j97> a40 = ik7.a(PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.N0 = a40;
        this.O0 = ik7.a(l97.a(a40));
        this.P0 = ik7.a(wo4.a(uo4));
        Provider<CategoryDatabase> a41 = ik7.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.Q0 = a41;
        this.R0 = ik7.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(portfolioDatabaseModule, a41));
        CategoryRemoteDataSource_Factory create4 = CategoryRemoteDataSource_Factory.create(this.o);
        this.S0 = create4;
        this.T0 = CategoryRepository_Factory.create(this.R0, create4);
        this.U0 = WatchAppRepository_Factory.create(this.e0, this.d);
        this.V0 = ComplicationRepository_Factory.create(this.f0, this.d);
        this.W0 = DianaAppSettingRepository_Factory.create(this.o);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public nl6 b(rl6 rl6) {
        lk7.b(rl6);
        return new b0(rl6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public kr6 b0(nr6 nr6) {
        lk7.b(nr6);
        return new l0(nr6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public tx4 b1() {
        return new w();
    }

    @DexIgnore
    public final DianaAppSettingRepository b4() {
        return new DianaAppSettingRepository(this.o.get());
    }

    @DexIgnore
    public final void b5(uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        DianaWatchFaceRemoteDataSource_Factory create = DianaWatchFaceRemoteDataSource_Factory.create(this.o, this.K0);
        this.X0 = create;
        this.Y0 = DianaWatchFaceRepository_Factory.create(this.I0, create);
        RingStyleRemoteDataSource_Factory create2 = RingStyleRemoteDataSource_Factory.create(this.o);
        this.Z0 = create2;
        this.a1 = RingStyleRepository_Factory.create(create2, this.I0);
        this.b1 = WatchLocalizationRepository_Factory.create(this.o, this.c);
        this.c1 = ik7.a(PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.d1 = ik7.a(t77.a(n77.a(), this.c1));
        Provider<yo5> a4 = ik7.a(PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.e1 = a4;
        this.f1 = ik7.a(ap5.a(a4));
        this.g1 = xy6.a(this.U0, this.V0, this.W0, this.T0, this.Y0, this.a1, w36.a(), e26.a(), this.j, this.c, this.b1, this.A, this.d1, this.I0, this.M0, this.f1, this.y, this.O0);
        this.h1 = zy6.a(this.V, this.i0, this.P, this.I, this.T0, this.A);
        this.i1 = et5.a(this.i0, this.c, this.P, this.d, this.V, this.I, this.P0, this.A);
        r27 a5 = r27.a(this.c);
        this.j1 = a5;
        this.k1 = e37.a(this.P, this.y, a5, this.d);
        this.l1 = zt5.a(this.P, this.c);
        WorkoutSettingRemoteDataSource_Factory create3 = WorkoutSettingRemoteDataSource_Factory.create(this.o);
        this.m1 = create3;
        this.n1 = WorkoutSettingRepository_Factory.create(create3);
        bt5 a6 = bt5.a(this.c, w36.a(), this.d, this.P, this.I0, this.M0, this.j, e26.a(), this.k1, this.l1, this.P0, this.A, this.W0, this.n1);
        this.o1 = a6;
        this.p1 = nj5.a(this.g1, this.h1, this.i1, a6);
        Provider<AppSettingsDatabase> a7 = ik7.a(PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.q1 = a7;
        Provider<lr4> a8 = ik7.a(PortfolioDatabaseModule_ProvidesFlagDaoFactory.create(portfolioDatabaseModule, a7));
        this.r1 = a8;
        this.s1 = ik7.a(PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory.create(portfolioDatabaseModule, a8));
        Provider<or4> a9 = ik7.a(PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory.create(portfolioDatabaseModule, this.o));
        this.t1 = a9;
        this.u1 = ik7.a(qr4.a(this.s1, a9));
        this.v1 = ik7.a(er5.a(this.d, this.A0, this.w0, this.c));
        Provider<ThemeDatabase> a10 = ik7.a(PortfolioDatabaseModule_ProvideThemeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.w1 = a10;
        Provider<ThemeDao> a11 = ik7.a(PortfolioDatabaseModule_ProvideThemeDaoFactory.create(portfolioDatabaseModule, a10));
        this.x1 = a11;
        Provider<ThemeRepository> a12 = ik7.a(ThemeRepository_Factory.create(a11, this.d));
        this.y1 = a12;
        this.z1 = ik7.a(ap4.a(uo4, this.d, this.c, this.V, this.T0, this.U0, this.V0, this.i0, this.q, this.P, this.y, this.A, this.p1, this.Y0, this.b1, this.a1, this.s0, this.w0, this.A0, this.I0, this.J0, this.n1, this.u1, this.v1, a12, this.d1, this.M0, this.f1, this.O0, this.W0));
        this.A1 = ik7.a(cr5.a(this.y));
        this.B1 = FitnessDataRepository_Factory.create(this.o);
        jp4 a13 = jp4.a(uo4, this.b, this.G, this.c);
        this.C1 = a13;
        Provider<ThirdPartyRepository> a14 = ik7.a(ThirdPartyRepository_Factory.create(a13, this.W, this.d));
        this.D1 = a14;
        this.E1 = kc7.a(this.W, this.D, this.b0, this.E, this.c0, this.k0, this.l0, this.B1, this.A, this.c, this.V, a14, this.A0, this.w0, this.I0, this.y, this.n1, this.J0, this.Y0, this.d);
        WatchFaceRemoteDataSource_Factory create4 = WatchFaceRemoteDataSource_Factory.create(this.o);
        this.F1 = create4;
        WatchFaceRepository_Factory create5 = WatchFaceRepository_Factory.create(this.b, create4, this.I0);
        this.G1 = create5;
        this.H1 = ik7.a(co5.a(this.q, this.c, create5));
        Provider<AddressDatabase> a15 = ik7.a(PortfolioDatabaseModule_ProvideAddressDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.I1 = a15;
        Provider<AddressDao> a16 = ik7.a(PortfolioDatabaseModule_ProvideAddressDaoFactory.create(portfolioDatabaseModule, a15));
        this.J1 = a16;
        Provider<LocationSource> a17 = ik7.a(LocationSource_Factory.create(a16));
        this.K1 = a17;
        this.L1 = ik7.a(gs5.a(this.d, a17));
        this.M1 = ik7.a(bq5.a(this.k, this.c));
        this.N1 = ik7.a(eq5.a(this.k, this.c));
        this.O1 = ik7.a(zr4.a(this.P0, this.A0));
        this.P1 = b37.a(w36.a(), e26.a(), this.j, this.I, this.P, this.c);
        Provider<or5> a18 = ik7.a(pr5.a());
        this.Q1 = a18;
        this.R1 = ik7.a(mp4.a(uo4, this.V, this.v1, this.i, this.A, this.c, this.P1, a18, this.L1));
        this.S1 = ik7.a(zp4.a(uo4, this.P, this.d));
        this.T1 = ik7.a(zr5.a());
        this.U1 = ik7.a(fp4.a(uo4));
        this.V1 = ik7.a(bq4.a(uo4));
        this.W1 = ik7.a(aq4.a(uo4));
        ik7.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(uAppSystemVersionRepositoryModule));
        MicroAppLastSettingRepository_Factory create6 = MicroAppLastSettingRepository_Factory.create(this.j0);
        this.X1 = create6;
        this.Y1 = ik7.a(rp4.a(uo4, this.c, this.y, this.h1, this.I, this.d, this.c0, this.K, this.S, create6, this.p1, this.u1, this.q, this.G1, this.I0, this.O0, this.M0, this.d1, this.P, this.W0, this.Y0));
        this.Z1 = ik7.a(hp4.a(uo4));
        this.a2 = ik7.a(qu5.a());
        Provider<InAppNotificationDatabase> a19 = ik7.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.b2 = a19;
        Provider<InAppNotificationDao> a20 = ik7.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(portfolioDatabaseModule, a19));
        this.c2 = a20;
        Provider<InAppNotificationRepository> a21 = ik7.a(InAppNotificationRepository_Factory.create(a20));
        this.d2 = a21;
        this.e2 = ik7.a(lp4.a(uo4, a21));
        this.f2 = ik7.a(ip4.a(uo4, this.m, this.n));
        this.g2 = ik7.a(cq4.a(uo4));
        this.h2 = ik7.a(np4.a(uo4));
        this.i2 = ik7.a(yp4.a(uo4));
        this.j2 = ik7.a(pp4.a(uo4));
        this.k2 = l76.a(this.M0, this.W0, this.U0, this.c);
        this.l2 = tb6.a(this.V, this.X1, this.i0);
        this.m2 = yu5.a(this.y);
        wu5 a22 = wu5.a(this.y);
        this.n2 = a22;
        this.o2 = cp6.a(this.m2, a22);
        this.p2 = p96.a(this.O0);
        this.q2 = t96.a(this.O0, this.a1);
        this.r2 = pt5.a(this.P, this.c);
        this.s2 = wt5.a(this.P, this.V, this.q, this.d, this.G1, this.M0);
        rt5 a23 = rt5.a(this.y, this.P, this.p1, this.d, this.c);
        this.t2 = a23;
        this.u2 = h17.a(this.P, this.r2, this.s2, this.p1, this.c, a23, it5.a(), this.A0, this.d);
        this.v2 = oa6.a(this.c, this.y);
        this.w2 = la6.a(this.c);
        this.x2 = dr6.a(this.y1);
        gu6 a24 = gu6.a(this.n1);
        this.y2 = a24;
        this.z2 = nu6.a(a24, this.n1);
        this.A2 = eu5.a(this.K1, this.d);
        bu5 a25 = bu5.a(this.f2);
        this.B2 = a25;
        this.C2 = hv6.a(this.d, this.A2, a25);
        this.D2 = bx4.a(this.c, this.s0);
        this.E2 = qv4.a(this.s0);
        this.F2 = dv4.a(this.s0, this.J0, this.c);
        this.G2 = fy4.a(this.c, this.w0);
        this.H2 = cw4.a(this.E0);
        this.I2 = ww4.a(this.w0, this.A0);
        this.J2 = nu4.a(this.c, this.A0);
        this.K2 = yu4.a(this.w0, this.A0, this.c);
        this.L2 = mx4.a(this.c, this.A0, this.w0);
        this.M2 = xx4.a(this.A0);
        this.N2 = rx4.a(this.A0, this.c);
        this.O2 = qw4.a(this.A0, this.w0, this.c);
        this.P2 = vv4.a(this.A0, this.w0);
        this.Q2 = jw4.a(this.A0, this.c);
        this.R2 = jv4.a(this.A0);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public xl6 c(bm6 bm6) {
        lk7.b(bm6);
        return new e1(bm6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void c0(cx6 cx6) {
        B5(cx6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void c1(cs5 cs5) {
        I5(cs5);
    }

    @DexIgnore
    public final at5 c4() {
        return new at5(this.c.get(), new v36(), this.d.get(), this.P.get(), this.I0.get(), this.M0.get(), this.j.get(), new d26(), R4(), P4(), this.P0.get(), this.A.get(), b4(), Z4());
    }

    @DexIgnore
    public final void c5(uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.S2 = ny4.a(this.A0);
        nt5 a4 = nt5.a(this.i);
        this.T2 = a4;
        this.U2 = o26.a(this.i, a4);
        this.V2 = mt6.a(this.y1);
        this.W2 = hs6.a(this.y1);
        this.X2 = bs6.a(this.y1);
        this.Y2 = at6.a(this.y1);
        this.Z2 = vr6.a(this.y1);
        this.a3 = pr6.a(this.y1);
        this.b3 = jr6.a(this.y1);
        this.c3 = os6.a(this.y1);
        this.d3 = us6.a(this.y1);
        this.e3 = gt6.a(this.y1);
        this.f3 = tt6.a(this.y1);
        this.g3 = k26.a(this.i);
        this.h3 = ln6.a(this.m0);
        this.i3 = qn6.a(this.m0);
        kt5 a5 = kt5.a(this.d, this.y, this.x);
        this.j3 = a5;
        this.k3 = jp6.a(this.D, this.E, this.c0, this.y, a5);
        xb7 a6 = xb7.a(this.Y0, this.I0);
        this.l3 = a6;
        this.m3 = ya7.a(this.y, this.Y0, a6);
        an5 a7 = an5.a(this.d, this.P, this.W0);
        this.n3 = a7;
        this.o3 = c87.a(a7, this.y, this.t, this.M0, this.I0, this.d1, this.W0, this.Y0);
        this.p3 = i87.a(this.V0, this.W0);
        this.q3 = n87.a(this.d1);
        this.r3 = q97.a(this.O0);
        this.s3 = v97.a(this.d1, this.I0);
        this.t3 = ca7.a(this.d1, this.I0);
        this.u3 = pa7.a(this.Y0, this.I0, this.M0, this.l3);
        this.v3 = ta7.a(this.d, this.Y0, this.I0, this.d1, this.y, this.n3, this.t);
        this.w3 = ih5.a(this.y);
        Provider<do5> a8 = ik7.a(qp4.a(uo4, this.c, this.P, this.q, this.Y1));
        this.x3 = a8;
        this.y3 = mv6.a(a8, this.p1, this.c);
        kk7.b b4 = kk7.b(59);
        b4.c(k76.class, this.k2);
        b4.c(sb6.class, this.l2);
        b4.c(bp6.class, this.o2);
        b4.c(o96.class, this.p2);
        b4.c(s96.class, this.q2);
        b4.c(f17.class, this.u2);
        b4.c(na6.class, this.v2);
        b4.c(ka6.class, this.w2);
        b4.c(zc6.class, ad6.a());
        b4.c(r16.class, s16.a());
        b4.c(cr6.class, this.x2);
        b4.c(mu6.class, this.z2);
        b4.c(gv6.class, this.C2);
        b4.c(ax4.class, this.D2);
        b4.c(pv4.class, this.E2);
        b4.c(cv4.class, this.F2);
        b4.c(fx4.class, gx4.a());
        b4.c(ey4.class, this.G2);
        b4.c(bw4.class, this.H2);
        b4.c(vw4.class, this.I2);
        b4.c(ru4.class, su4.a());
        b4.c(mu4.class, this.J2);
        b4.c(xu4.class, this.K2);
        b4.c(lx4.class, this.L2);
        b4.c(wx4.class, this.M2);
        b4.c(qx4.class, this.N2);
        b4.c(ow4.class, this.O2);
        b4.c(uv4.class, this.P2);
        b4.c(iw4.class, this.Q2);
        b4.c(iv4.class, this.R2);
        b4.c(my4.class, this.S2);
        b4.c(n26.class, this.U2);
        b4.c(lt6.class, this.V2);
        b4.c(gs6.class, this.W2);
        b4.c(as6.class, this.X2);
        b4.c(zs6.class, this.Y2);
        b4.c(ur6.class, this.Z2);
        b4.c(or6.class, this.a3);
        b4.c(ir6.class, this.b3);
        b4.c(ns6.class, this.c3);
        b4.c(ts6.class, this.d3);
        b4.c(ft6.class, this.e3);
        b4.c(st6.class, this.f3);
        b4.c(j26.class, this.g3);
        b4.c(kn6.class, this.h3);
        b4.c(pn6.class, this.i3);
        b4.c(hp6.class, this.k3);
        b4.c(WatchFaceGalleryViewModel.class, this.m3);
        b4.c(a87.class, this.o3);
        b4.c(h87.class, this.p3);
        b4.c(m87.class, this.q3);
        b4.c(p97.class, this.r3);
        b4.c(u97.class, this.s3);
        b4.c(ba7.class, this.t3);
        b4.c(WatchFaceListViewModel.class, this.u3);
        b4.c(sa7.class, this.v3);
        b4.c(ga7.class, ha7.a());
        b4.c(hh5.class, this.w3);
        b4.c(lv6.class, this.y3);
        kk7 b5 = b4.b();
        this.z3 = b5;
        this.A3 = ik7.a(qo4.a(b5));
        this.B3 = ik7.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(portfolioDatabaseModule, this.j));
        this.C3 = ik7.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(repositoriesModule));
        this.D3 = ik7.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.E3 = ik7.a(xp4.a(uo4));
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ot6 d(rt6 rt6) {
        lk7.b(rt6);
        return new y2(rt6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ju4 d0() {
        return new i();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public r97 d1() {
        return new h3();
    }

    @DexIgnore
    public final DianaWatchFaceRemoteDataSource d4() {
        return new DianaWatchFaceRemoteDataSource(this.o.get(), this.K0.get());
    }

    @DexIgnore
    public final AlarmReceiver d5(AlarmReceiver alarmReceiver) {
        tp5.e(alarmReceiver, this.y.get());
        tp5.d(alarmReceiver, this.c.get());
        tp5.c(alarmReceiver, this.P.get());
        tp5.a(alarmReceiver, this.B.get());
        tp5.b(alarmReceiver, this.A.get());
        return alarmReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public vs6 e(ys6 ys6) {
        lk7.b(ys6);
        return new r0(ys6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public j87 e0() {
        return new g3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public j17 e1(m17 m17) {
        lk7.b(m17);
        return new a0(m17);
    }

    @DexIgnore
    public final DianaWatchFaceRepository e4() {
        return new DianaWatchFaceRepository(this.I0.get(), d4());
    }

    @DexIgnore
    public final dk5 e5(dk5 dk5) {
        ek5.c(dk5, this.c.get());
        ek5.b(dk5, this.P.get());
        ek5.d(dk5, this.y.get());
        ek5.a(dk5, Y3());
        return dk5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ps6 f(ss6 ss6) {
        lk7.b(ss6);
        return new q0(ss6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void f0(ComplicationWeatherService complicationWeatherService) {
        m5(complicationWeatherService);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public za6 f1(db6 db6) {
        lk7.b(db6);
        return new z2(db6);
    }

    @DexIgnore
    public final av5 f4() {
        return new av5(this.y.get());
    }

    @DexIgnore
    public final up5 f5(up5 up5) {
        vp5.e(up5, this.I.get());
        vp5.f(up5, this.c.get());
        vp5.a(up5, a4());
        vp5.c(up5, new v36());
        vp5.b(up5, new d26());
        vp5.d(up5, this.j.get());
        return up5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public fq6 g(jq6 jq6) {
        lk7.b(jq6);
        return new j2(jq6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void g0(pl5 pl5) {
        J5(pl5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public hc6 g1(kc6 kc6) {
        lk7.b(kc6);
        return new q1(kc6);
    }

    @DexIgnore
    public final s27 g4() {
        return new s27(this.c.get());
    }

    @DexIgnore
    public final AppPackageRemoveReceiver g5(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        wp5.a(appPackageRemoveReceiver, this.I.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ay5 h(dy5 dy5) {
        lk7.b(dy5);
        return new j0(dy5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ze6 h0(if6 if6) {
        lk7.b(if6);
        return new f(if6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public fo6 h1(jo6 jo6) {
        lk7.b(jo6);
        return new b(jo6);
    }

    @DexIgnore
    public final ru5 h4() {
        return new ru5(this.W.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final BCNotificationActionReceiver h5(BCNotificationActionReceiver bCNotificationActionReceiver) {
        ry4.b(bCNotificationActionReceiver, this.w0.get());
        ry4.a(bCNotificationActionReceiver, this.A0.get());
        ry4.c(bCNotificationActionReceiver, this.c.get());
        return bCNotificationActionReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void i(AlarmReceiver alarmReceiver) {
        d5(alarmReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public gw6 i0(jw6 jw6) {
        lk7.b(jw6);
        return new c2(jw6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void i1(cn5 cn5) {
        r5(cn5);
    }

    @DexIgnore
    public final hu5 i4() {
        return new hu5(this.c0.get(), this.y.get());
    }

    @DexIgnore
    public final ls5 i5(ls5 ls5) {
        ms5.e(ls5, this.y.get());
        ms5.d(ls5, this.c.get());
        ms5.a(ls5, this.P.get());
        ms5.c(ls5, this.Y1.get());
        ms5.b(ls5, new gu5());
        return ls5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public b56 j(f56 f56) {
        lk7.b(f56);
        return new x1(f56);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public uu4 j0() {
        return new q();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public m36 j1(n36 n36) {
        lk7.b(n36);
        return new m2(n36);
    }

    @DexIgnore
    public final ju5 j4() {
        return new ju5(this.l0.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final BootReceiver j5(BootReceiver bootReceiver) {
        yp5.a(bootReceiver, this.B.get());
        return bootReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public al6 k(el6 el6) {
        lk7.b(el6);
        return new e(el6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public i86 k0(m86 m86) {
        lk7.b(m86);
        return new p2(m86);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public cx4 k1() {
        return new h();
    }

    @DexIgnore
    public final iu5 k4() {
        return new iu5(this.c0.get(), this.y.get());
    }

    @DexIgnore
    public final CloudImageHelper k5(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.G.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void l(up5 up5) {
        f5(up5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void l0(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        s5(fossilFirebaseMessagingService);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void l1(x27 x27) {
        x5(x27);
    }

    @DexIgnore
    public final ku5 l4() {
        return new ku5(this.k0.get(), p4(), this.y.get());
    }

    @DexIgnore
    public final CommuteTimeService l5(CommuteTimeService commuteTimeService) {
        lr5.a(commuteTimeService, this.a2.get());
        return commuteTimeService;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public f46 m(j46 j46) {
        lk7.b(j46);
        return new u1(j46);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public u76 m0(x76 x76) {
        lk7.b(x76);
        return new e0(x76);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public h16 m1(l16 l16) {
        lk7.b(l16);
        return new v1(l16);
    }

    @DexIgnore
    public final tu5 m4() {
        return new tu5(this.b0.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final ComplicationWeatherService m5(ComplicationWeatherService complicationWeatherService) {
        gr5.a(complicationWeatherService, this.K1.get());
        ir5.b(complicationWeatherService, v4());
        ir5.e(complicationWeatherService, this.R.get());
        ir5.d(complicationWeatherService, this.c.get());
        ir5.c(complicationWeatherService, this.d.get());
        ir5.a(complicationWeatherService, this.t.get());
        ir5.f(complicationWeatherService, this.y.get());
        return complicationWeatherService;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void n(nk5 nk5) {
        p5(nk5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public n56 n0(r56 r56) {
        lk7.b(r56);
        return new y1(r56);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ov6 n1(rv6 rv6) {
        lk7.b(rv6);
        return new b1(rv6);
    }

    @DexIgnore
    public final uu5 n4() {
        return new uu5(this.E.get(), this.y.get(), this.b0.get(), p4());
    }

    @DexIgnore
    public final l37 n5(l37 l37) {
        n37.a(l37, this.P.get());
        n37.b(l37, this.j.get());
        return l37;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public u66 o(v66 v66) {
        lk7.b(v66);
        return new y0(v66);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ev4 o0() {
        return new l();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void o1(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public final su5 o4() {
        return new su5(this.D.get(), p4(), this.y.get(), this.W.get());
    }

    @DexIgnore
    public final DebugActivity o5(DebugActivity debugActivity) {
        ms5.e(debugActivity, this.y.get());
        ms5.d(debugActivity, this.c.get());
        ms5.a(debugActivity, this.P.get());
        ms5.c(debugActivity, this.Y1.get());
        ms5.b(debugActivity, new gu5());
        rs5.g(debugActivity, this.c.get());
        rs5.e(debugActivity, O4());
        rs5.c(debugActivity, this.Z1.get());
        rs5.d(debugActivity, this.F.get());
        rs5.b(debugActivity, this.q.get());
        rs5.f(debugActivity, this.A1.get());
        rs5.a(debugActivity, a4());
        return debugActivity;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void p(URLRequestTaskHelper uRLRequestTaskHelper) {
        G5(uRLRequestTaskHelper);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public w26 p0(b36 b36) {
        lk7.b(b36);
        return new b2(b36);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public nz5 p1(oz5 oz5) {
        lk7.b(oz5);
        return new l1(oz5);
    }

    @DexIgnore
    public final FitnessDataRepository p4() {
        return new FitnessDataRepository(this.o.get());
    }

    @DexIgnore
    public final nk5 p5(nk5 nk5) {
        ok5.b(nk5, this.c.get());
        ok5.a(nk5, this.P.get());
        return nk5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public a66 q(e66 e66) {
        lk7.b(e66);
        return new z1(e66);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public la7 q0() {
        return new e3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public w77 q1() {
        return new c3();
    }

    @DexIgnore
    public final t27 q4() {
        return new t27(g4());
    }

    @DexIgnore
    public final p37 q5(p37 p37) {
        q37.a(p37, this.P.get());
        q37.d(p37, this.c.get());
        q37.e(p37, this.y.get());
        q37.c(p37, this.F.get());
        q37.b(p37, this.Z1.get());
        return p37;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public h07 r(k07 k07) {
        lk7.b(k07);
        return new u2(k07);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public xv6 r0(aw6 aw6) {
        lk7.b(aw6);
        return new d1(aw6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void r1(BCNotificationActionReceiver bCNotificationActionReceiver) {
        h5(bCNotificationActionReceiver);
    }

    @DexIgnore
    public final wy6 r4() {
        return new wy6(U4(), T3(), b4(), Q3(), e4(), I4(), new v36(), new d26(), this.j.get(), this.c.get(), X4(), this.A.get(), this.d1.get(), this.I0.get(), this.M0.get(), this.f1.get(), this.y.get(), this.O0.get());
    }

    @DexIgnore
    public final cn5 r5(cn5 cn5) {
        dn5.a(cn5, this.g2.get());
        return cn5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void s(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        g5(appPackageRemoveReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public wa7 s0() {
        return new d3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public eg6 s1(mg6 mg6) {
        lk7.b(mg6);
        return new c0(mg6);
    }

    @DexIgnore
    public final yy6 s4() {
        return new yy6(this.V.get(), this.i0.get(), this.P.get(), this.I.get(), Q3(), this.A.get());
    }

    @DexIgnore
    public final FossilFirebaseMessagingService s5(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        jr5.d(fossilFirebaseMessagingService, this.e2.get());
        jr5.c(fossilFirebaseMessagingService, this.v1.get());
        jr5.e(fossilFirebaseMessagingService, this.c.get());
        jr5.a(fossilFirebaseMessagingService, this.J0.get());
        jr5.b(fossilFirebaseMessagingService, this.u1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public mi6 t(ui6 ui6) {
        lk7.b(ui6);
        return new h1(ui6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public cd6 t0(dd6 dd6) {
        lk7.b(dd6);
        return new v0(dd6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void t1(CommuteTimeService commuteTimeService) {
        l5(commuteTimeService);
    }

    @DexIgnore
    public final v27 t4() {
        return new v27(g4(), this.P.get());
    }

    @DexIgnore
    public final FossilNotificationListenerService t5(FossilNotificationListenerService fossilNotificationListenerService) {
        xq5.d(fossilNotificationListenerService, this.Q1.get());
        xq5.b(fossilNotificationListenerService, this.k.get());
        xq5.c(fossilNotificationListenerService, this.T1.get());
        xq5.a(fossilNotificationListenerService, this.G.get());
        xq5.e(fossilNotificationListenerService, this.c.get());
        xq5.f(fossilNotificationListenerService, this.y.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public bz6 u(gz6 gz6) {
        lk7.b(gz6);
        return new f2(gz6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public ht6 u0(kt6 kt6) {
        lk7.b(kt6);
        return new t0(kt6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void u1(ls5 ls5) {
        i5(ls5);
    }

    @DexIgnore
    public final vu5 u4() {
        return new vu5(this.y.get());
    }

    @DexIgnore
    public final en5 u5(en5 en5) {
        gn5.a(en5, this.P.get());
        gn5.b(en5, this.c.get());
        return en5;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public yq6 v(br6 br6) {
        lk7.b(br6);
        return new v2(br6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public l96 v0() {
        return new z0();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public iv6 v1() {
        return new r1();
    }

    @DexIgnore
    public final w27 v4() {
        return new w27(this.o.get());
    }

    @DexIgnore
    public final av6 v5(av6 av6) {
        cv6.s(av6, y4());
        cv6.v(av6, B4());
        cv6.h(av6, f4());
        cv6.y(av6, new ht5());
        cv6.g(av6, a4());
        cv6.D(av6, this.y.get());
        cv6.f(av6, this.P.get());
        cv6.z(av6, this.c.get());
        cv6.i(av6, h4());
        cv6.p(av6, o4());
        cv6.C(av6, this.R.get());
        cv6.n(av6, m4());
        cv6.o(av6, n4());
        cv6.m(av6, l4());
        cv6.k(av6, j4());
        cv6.t(av6, z4());
        cv6.G(av6, this.V1.get());
        cv6.u(av6, A4());
        cv6.x(av6, D4());
        cv6.w(av6, C4());
        cv6.e(av6, S3());
        cv6.d(av6, this.P0.get());
        cv6.B(av6, this.D.get());
        cv6.A(av6, this.E.get());
        cv6.r(av6, this.c0.get());
        cv6.j(av6, i4());
        cv6.l(av6, k4());
        cv6.q(av6, t4());
        cv6.E(av6, X4());
        cv6.c(av6, this.A.get());
        cv6.I(av6, this.s0.get());
        cv6.a(av6, this.J0.get());
        cv6.b(av6, this.u1.get());
        cv6.F(av6, Z4());
        cv6.H(av6);
        return av6;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public e97 w() {
        return new f3();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void w0(go5 go5) {
        F5(go5);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public e27 w1(j27 j27) {
        lk7.b(j27);
        return new m3(j27);
    }

    @DexIgnore
    public final uk5 w4() {
        return jp4.c(this.f1946a, this.b.get(), this.G.get(), this.c.get());
    }

    @DexIgnore
    public final MFDeviceService w5(MFDeviceService mFDeviceService) {
        zq5.q(mFDeviceService, this.c.get());
        zq5.f(mFDeviceService, this.P.get());
        zq5.a(mFDeviceService, this.W.get());
        zq5.t(mFDeviceService, this.D.get());
        zq5.r(mFDeviceService, this.b0.get());
        zq5.s(mFDeviceService, this.E.get());
        zq5.v(mFDeviceService, this.y.get());
        zq5.p(mFDeviceService, this.V.get());
        zq5.n(mFDeviceService, this.i0.get());
        zq5.k(mFDeviceService, this.k0.get());
        zq5.l(mFDeviceService, this.l0.get());
        zq5.y(mFDeviceService, this.m0.get());
        zq5.h(mFDeviceService, p4());
        zq5.j(mFDeviceService, this.c0.get());
        zq5.d(mFDeviceService, this.G.get());
        zq5.b(mFDeviceService, this.P0.get());
        zq5.c(mFDeviceService, this.d.get());
        zq5.m(mFDeviceService, this.R1.get());
        zq5.u(mFDeviceService, this.D1.get());
        zq5.g(mFDeviceService, g4());
        zq5.w(mFDeviceService, R4());
        zq5.i(mFDeviceService, this.C.get());
        zq5.x(mFDeviceService, this.S1.get());
        zq5.e(mFDeviceService, this.v1.get());
        zq5.z(mFDeviceService, this.L1.get());
        zq5.o(mFDeviceService, this.Q1.get());
        return mFDeviceService;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public dv6 x() {
        return new p1();
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void x0(mn5 mn5) {
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public vt6 x1(yt6 yt6) {
        lk7.b(yt6);
        return new g2(yt6);
    }

    @DexIgnore
    public final dt5 x4() {
        return new dt5(this.i0.get(), this.c.get(), this.P.get(), this.d.get(), this.V.get(), this.I.get(), this.P0.get(), this.A.get());
    }

    @DexIgnore
    public final x27 x5(x27 x27) {
        y27.a(x27, this.W.get());
        return x27;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public pq6 y(tq6 tq6) {
        lk7.b(tq6);
        return new h2(tq6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public l06 y0(p06 p06) {
        lk7.b(p06);
        return new s1(p06);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public i26 y1() {
        return new l2();
    }

    @DexIgnore
    public final cv5 y4() {
        return new cv5(this.y.get());
    }

    @DexIgnore
    public final NetworkChangedReceiver y5(NetworkChangedReceiver networkChangedReceiver) {
        zp5.a(networkChangedReceiver, this.y.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void z(BootReceiver bootReceiver) {
        j5(bootReceiver);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public void z0(uz6 uz6) {
        C5(uz6);
    }

    @DexIgnore
    @Override // com.fossil.ro4
    public xy5 z1(hz5 hz5) {
        lk7.b(hz5);
        return new k1(hz5);
    }

    @DexIgnore
    public final dv5 z4() {
        return new dv5(this.U1.get());
    }

    @DexIgnore
    public final NotificationReceiver z5(NotificationReceiver notificationReceiver) {
        fo5.b(notificationReceiver, this.c.get());
        fo5.a(notificationReceiver, a4());
        fo5.c(notificationReceiver, this.y.get());
        return notificationReceiver;
    }
}
