package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ye extends qq7 implements vp7<fs, Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ bi b;
    @DexIgnore
    public /* final */ /* synthetic */ j0 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ye(bi biVar, j0 j0Var) {
        super(2);
        this.b = biVar;
        this.c = j0Var;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(fs fsVar, Float f) {
        float floatValue = f.floatValue();
        if (this.b.J > 0) {
            bi biVar = this.b;
            floatValue = ((floatValue * ((float) (this.c.g - this.b.N))) + ((float) (biVar.K + biVar.N))) / ((float) this.b.J);
        }
        if (Math.abs(floatValue - this.b.L) > this.b.U || floatValue == 1.0f) {
            bi biVar2 = this.b;
            biVar2.L = floatValue;
            biVar2.d(floatValue);
        }
        return tl7.f3441a;
    }
}
