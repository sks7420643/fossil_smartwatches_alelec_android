package com.fossil;

import com.fossil.a34;
import java.io.Serializable;
import java.lang.Enum;
import java.util.EnumMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w24<K extends Enum<K>, V> extends a34.c<K, V> {
    @DexIgnore
    public /* final */ transient EnumMap<K, V> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K extends Enum<K>, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumMap<K, V> delegate;

        @DexIgnore
        public b(EnumMap<K, V> enumMap) {
            this.delegate = enumMap;
        }

        @DexIgnore
        public Object readResolve() {
            return new w24(this.delegate);
        }
    }

    @DexIgnore
    public w24(EnumMap<K, V> enumMap) {
        this.f = enumMap;
        i14.d(!enumMap.isEmpty());
    }

    @DexIgnore
    public static <K extends Enum<K>, V> a34<K, V> asImmutable(EnumMap<K, V> enumMap) {
        int size = enumMap.size();
        if (size == 0) {
            return a34.of();
        }
        if (size != 1) {
            return new w24(enumMap);
        }
        Map.Entry entry = (Map.Entry) o34.f(enumMap.entrySet());
        return a34.of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean containsKey(Object obj) {
        return this.f.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.a34.c
    public h54<Map.Entry<K, V>> entryIterator() {
        return x34.r(this.f.entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof w24) {
            obj = ((w24) obj).f;
        }
        return this.f.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.a34, java.util.Map
    public V get(Object obj) {
        return this.f.get(obj);
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h54<K> keyIterator() {
        return p34.x(this.f.keySet().iterator());
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    @Override // com.fossil.a34
    public Object writeReplace() {
        return new b(this.f);
    }
}
