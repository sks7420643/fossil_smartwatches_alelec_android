package com.fossil;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g01 extends RecyclerView.ViewHolder {
    @DexIgnore
    public g01(FrameLayout frameLayout) {
        super(frameLayout);
    }

    @DexIgnore
    public static g01 a(ViewGroup viewGroup) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        frameLayout.setId(mo0.j());
        frameLayout.setSaveEnabled(false);
        return new g01(frameLayout);
    }

    @DexIgnore
    public FrameLayout b() {
        return (FrameLayout) this.itemView;
    }
}
