package com.fossil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t13 extends rz2<String> implements w13, RandomAccess {
    @DexIgnore
    public static /* final */ t13 d;
    @DexIgnore
    public /* final */ List<Object> c;

    /*
    static {
        t13 t13 = new t13();
        d = t13;
        t13.zzb();
    }
    */

    @DexIgnore
    public t13() {
        this(10);
    }

    @DexIgnore
    public t13(int i) {
        this(new ArrayList(i));
    }

    @DexIgnore
    public t13(ArrayList<Object> arrayList) {
        this.c = arrayList;
    }

    @DexIgnore
    public static String b(Object obj) {
        return obj instanceof String ? (String) obj : obj instanceof xz2 ? ((xz2) obj).zzb() : h13.i((byte[]) obj);
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final void V(xz2 xz2) {
        a();
        this.c.add(xz2);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.c.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList, com.fossil.rz2
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof w13) {
            collection = ((w13) collection).zzd();
        }
        boolean addAll = this.c.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.rz2
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    @Override // com.fossil.rz2
    public final void clear() {
        a();
        this.c.clear();
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        Object obj = this.c.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof xz2) {
            xz2 xz2 = (xz2) obj;
            String zzb = xz2.zzb();
            if (xz2.zzc()) {
                this.c.set(i, zzb);
            }
            return zzb;
        }
        byte[] bArr = (byte[]) obj;
        String i2 = h13.i(bArr);
        if (h13.h(bArr)) {
            this.c.set(i, i2);
        }
        return i2;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.c.remove(i);
        ((AbstractList) this).modCount++;
        return b(remove);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return b(this.c.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.c.size();
    }

    @DexIgnore
    @Override // com.fossil.m13
    public final /* synthetic */ m13 zza(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.c);
            return new t13(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final Object zzb(int i) {
        return this.c.get(i);
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final List<?> zzd() {
        return Collections.unmodifiableList(this.c);
    }

    @DexIgnore
    @Override // com.fossil.w13
    public final w13 zze() {
        return zza() ? new y33(this) : this;
    }
}
