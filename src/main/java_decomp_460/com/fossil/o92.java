package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o92 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f2652a;
    @DexIgnore
    public /* final */ q92 b;

    @DexIgnore
    public o92(q92 q92) {
        this.b = q92;
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            if (this.f2652a != null) {
                this.f2652a.unregisterReceiver(this);
            }
            this.f2652a = null;
        }
    }

    @DexIgnore
    public final void b(Context context) {
        this.f2652a = context;
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.b.a();
            a();
        }
    }
}
