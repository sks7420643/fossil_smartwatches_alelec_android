package com.fossil;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import androidx.core.app.SharedElementCallback;
import com.portfolio.platform.PortfolioApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hs5 extends SharedElementCallback {
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Intent d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore
    public hs5(Intent intent, PortfolioApp portfolioApp) {
        pq7.c(intent, "intent");
        pq7.c(portfolioApp, "mApp");
        this.d = intent;
        this.e = portfolioApp;
        Resources resources = portfolioApp.getResources();
        this.b = (float) resources.getDimensionPixelSize(2131165728);
        this.c = (float) resources.getDimensionPixelSize(2131165724);
    }

    @DexIgnore
    @Override // androidx.core.app.SharedElementCallback
    public void f(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        if (list2 != null) {
            for (View view : list2) {
                view.setTag(2131363184, null);
            }
            Integer valueOf = list != null ? Integer.valueOf(list.indexOf(this.e.getString(2131887600))) : null;
            if (valueOf != null) {
                Object obj = list2.get(valueOf.intValue());
                if (obj != null) {
                    TextView textView = (TextView) obj;
                    int measuredWidth = textView.getMeasuredWidth();
                    int measuredHeight = textView.getMeasuredHeight();
                    textView.setTextSize(0, this.c);
                    textView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                    int measuredWidth2 = textView.getMeasuredWidth();
                    int i = (measuredWidth2 - measuredWidth) / 2;
                    int measuredHeight2 = (textView.getMeasuredHeight() - measuredHeight) / 2;
                    textView.layout(textView.getLeft() - i, textView.getTop() - measuredHeight2, i + textView.getRight(), measuredHeight2 + textView.getBottom());
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }

    @DexIgnore
    @Override // androidx.core.app.SharedElementCallback
    public void g(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                String str = list.get(i);
                if (this.d.hasExtra(str) && list2 != null) {
                    ((View) list2.get(i)).setTag(2131363184, this.d.getBundleExtra(str));
                }
            }
            int indexOf = list.indexOf(this.e.getString(2131887600));
            if (list2 != null) {
                Object obj = list2.get(indexOf);
                if (obj != null) {
                    ((TextView) obj).setTextSize(0, this.b);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }
}
