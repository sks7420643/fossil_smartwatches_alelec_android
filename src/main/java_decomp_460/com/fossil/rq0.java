package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class rq0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rq0> CREATOR; // = new a();
    @DexIgnore
    public ArrayList<uq0> b;
    @DexIgnore
    public ArrayList<String> c;
    @DexIgnore
    public jq0[] d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rq0> {
        @DexIgnore
        /* renamed from: a */
        public rq0 createFromParcel(Parcel parcel) {
            return new rq0(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public rq0[] newArray(int i) {
            return new rq0[i];
        }
    }

    @DexIgnore
    public rq0() {
    }

    @DexIgnore
    public rq0(Parcel parcel) {
        this.b = parcel.createTypedArrayList(uq0.CREATOR);
        this.c = parcel.createStringArrayList();
        this.d = (jq0[]) parcel.createTypedArray(jq0.CREATOR);
        this.e = parcel.readInt();
        this.f = parcel.readString();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.b);
        parcel.writeStringList(this.c);
        parcel.writeTypedArray(this.d, i);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
    }
}
