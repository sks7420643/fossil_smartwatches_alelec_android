package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv2 extends gw2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2100a;
    @DexIgnore
    public /* final */ xw2<tw2<tv2>> b;

    @DexIgnore
    public kv2(Context context, xw2<tw2<tv2>> xw2) {
        if (context != null) {
            this.f2100a = context;
            this.b = xw2;
            return;
        }
        throw new NullPointerException("Null context");
    }

    @DexIgnore
    @Override // com.fossil.gw2
    public final Context a() {
        return this.f2100a;
    }

    @DexIgnore
    @Override // com.fossil.gw2
    public final xw2<tw2<tv2>> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof gw2) {
            gw2 gw2 = (gw2) obj;
            if (this.f2100a.equals(gw2.a())) {
                xw2<tw2<tv2>> xw2 = this.b;
                if (xw2 == null) {
                    return gw2.b() == null;
                }
                if (xw2.equals(gw2.b())) {
                    return true;
                }
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = this.f2100a.hashCode();
        xw2<tw2<tv2>> xw2 = this.b;
        return (xw2 == null ? 0 : xw2.hashCode()) ^ ((hashCode ^ 1000003) * 1000003);
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.f2100a);
        String valueOf2 = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 46 + String.valueOf(valueOf2).length());
        sb.append("FlagsContext{context=");
        sb.append(valueOf);
        sb.append(", hermeticFileOverrides=");
        sb.append(valueOf2);
        sb.append("}");
        return sb.toString();
    }
}
