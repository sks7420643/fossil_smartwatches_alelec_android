package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gn7 implements Iterator<jl7>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final jl7 next() {
        return jl7.a(b());
    }

    @DexIgnore
    public abstract byte b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
