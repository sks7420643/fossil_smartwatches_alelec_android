package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h0 implements Parcelable.Creator<j0> {
    @DexIgnore
    public /* synthetic */ h0(kq7 kq7) {
    }

    @DexIgnore
    public final byte a(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(1);
    }

    @DexIgnore
    public final byte b(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public j0 createFromParcel(Parcel parcel) {
        boolean z = false;
        String readString = parcel.readString();
        if (readString == null) {
            readString = "";
        }
        byte readByte = parcel.readByte();
        byte readByte2 = parcel.readByte();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray == null) {
            createByteArray = new byte[0];
        }
        long readLong = parcel.readLong();
        long readLong2 = parcel.readLong();
        long readLong3 = parcel.readLong();
        if (parcel.readInt() != 0) {
            z = true;
        }
        return new j0(readString, readByte, readByte2, createByteArray, readLong, readLong2, readLong3, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public j0[] newArray(int i) {
        return new j0[i];
    }
}
