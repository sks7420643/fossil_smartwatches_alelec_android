package com.fossil;

import android.util.Log;
import com.fossil.l72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n92 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ z52 b;
    @DexIgnore
    public /* final */ /* synthetic */ l72.c c;

    @DexIgnore
    public n92(l72.c cVar, z52 z52) {
        this.c = cVar;
        this.b = z52;
    }

    @DexIgnore
    public final void run() {
        l72.a aVar = (l72.a) l72.this.i.get(this.c.b);
        if (aVar != null) {
            if (this.b.A()) {
                this.c.e = true;
                if (this.c.f2153a.v()) {
                    this.c.g();
                    return;
                }
                try {
                    this.c.f2153a.i(null, this.c.f2153a.h());
                } catch (SecurityException e) {
                    Log.e("GoogleApiManager", "Failed to get service from broker. ", e);
                    aVar.n(new z52(10));
                }
            } else {
                aVar.n(this.b);
            }
        }
    }
}
