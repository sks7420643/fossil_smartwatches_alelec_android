package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.fossil.yi0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class si0 extends ui0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements yi0.a {
        @DexIgnore
        public a(si0 si0) {
        }

        @DexIgnore
        @Override // com.fossil.yi0.a
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    @Override // com.fossil.wi0, com.fossil.ui0
    public void j() {
        yi0.r = new a(this);
    }
}
