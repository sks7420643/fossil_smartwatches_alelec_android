package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv5 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ on5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SignUpSocialAuth f2102a;

        @DexIgnore
        public a(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "socialAuth");
            this.f2102a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.f2102a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2103a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMesagge");
            this.f2103a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f2103a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase", f = "SignUpSocialUseCase.kt", l = {23, 26}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ kv5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kv5 kv5, qn7 qn7) {
            super(qn7);
            this.this$0 = kv5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public kv5(UserRepository userRepository, on5 on5) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SignUpSocialUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.kv5.a r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 283
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kv5.k(com.fossil.kv5$a, com.fossil.qn7):java.lang.Object");
    }
}
