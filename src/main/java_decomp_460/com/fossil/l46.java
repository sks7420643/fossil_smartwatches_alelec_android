package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l46 implements Factory<h46> {
    @DexIgnore
    public static h46 a(j46 j46) {
        h46 c = j46.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
