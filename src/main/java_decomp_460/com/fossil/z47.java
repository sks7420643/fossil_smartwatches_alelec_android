package com.fossil;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z47 extends MetricAffectingSpan {
    @DexIgnore
    public Typeface b;

    @DexIgnore
    public z47(Typeface typeface) {
        pq7.c(typeface, "typeface");
        this.b = typeface;
    }

    @DexIgnore
    public final void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
    }

    @DexIgnore
    public void updateDrawState(TextPaint textPaint) {
        if (textPaint != null) {
            Typeface typeface = this.b;
            if (typeface != null) {
                a(textPaint, typeface);
            } else {
                pq7.n("typeface");
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public void updateMeasureState(TextPaint textPaint) {
        pq7.c(textPaint, "textPaint");
        Typeface typeface = this.b;
        if (typeface != null) {
            a(textPaint, typeface);
        } else {
            pq7.n("typeface");
            throw null;
        }
    }
}
