package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tw1 extends lw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tw1 createFromParcel(Parcel parcel) {
            return new tw1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tw1[] newArray(int i) {
            return new tw1[i];
        }
    }

    @DexIgnore
    public tw1(Parcel parcel) {
        super(parcel);
        k();
    }

    @DexIgnore
    public tw1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        k();
    }

    @DexIgnore
    @Override // com.fossil.lw1
    private final void k() {
        if (!(getThemeClassifier() == ec0.GOAL_RINGS)) {
            throw new IllegalArgumentException("Incorrect theme classifier.".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.iw1, com.fossil.lw1, java.lang.Object
    public final tw1 clone() {
        ry1 clone = h().clone();
        jw1 jw1 = g().b;
        ry1 ry1 = new ry1(g().c.getMajor(), 0);
        boolean z = g().d;
        byte[] bArr = g().e;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        tw1 tw1 = new tw1(clone, new yb0(jw1, ry1, z, copyOf), (cc0[]) f().clone(), (cc0[]) b().clone(), (cc0[]) d().clone(), (cc0[]) e().clone(), (cc0[]) c().clone(), (cc0[]) a().clone(), (cc0[]) i().clone());
        tw1.f()[0] = new cc0(g80.e(0, 1), tw1.f()[0].c);
        return tw1;
    }

    @DexIgnore
    @Override // com.fossil.lw1
    public sw1 edit() {
        return new sw1(this, new ry1(getPackageVersion().getMajor(), (getPackageVersion().getMinor() + 1) % (hy1.c(fq7.f1179a) + 1)));
    }
}
