package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy1 {
    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        pq7.c(bArr, "$this$concatenate");
        pq7.c(bArr2, FacebookRequestErrorClassification.KEY_OTHER);
        if (bArr2.length == 0) {
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
            return copyOf;
        }
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    @DexIgnore
    public static final byte[][] b(byte[] bArr, int i) {
        pq7.c(bArr, "$this$divide");
        if (i <= 0) {
            return new byte[][]{bArr};
        }
        ArrayList arrayList = new ArrayList();
        ur7 l = bs7.l(bs7.m(0, bArr.length), i);
        int a2 = l.a();
        int b = l.b();
        int c = l.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                arrayList.add(dm7.k(bArr, a2, Math.min(a2 + i, bArr.length)));
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public static final byte[] c(byte[] bArr, int i) {
        pq7.c(bArr, "$this$increaseAsNumber");
        long o = hy1.o(i);
        for (int length = bArr.length - 1; length >= 0; length--) {
            long p = ((long) hy1.p(bArr[length])) + o;
            long j = (long) 256;
            o = p / j;
            bArr[length] = (byte) ((byte) ((int) (p % j)));
            if (o == 0) {
                break;
            }
        }
        return bArr;
    }

    @DexIgnore
    public static final String d(byte[] bArr, String str) {
        int i = 0;
        pq7.c(bArr, "$this$toHexString");
        pq7.c(str, "separator");
        if (bArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        while (i < bArr.length - 1) {
            sb.append(hy1.j(bArr[i], null, 1, null));
            sb.append(str);
            i++;
        }
        if (i == bArr.length - 1) {
            sb.append(hy1.j(bArr[i], null, 1, null));
        }
        String sb2 = sb.toString();
        pq7.b(sb2, "sb.toString()");
        return sb2;
    }

    @DexIgnore
    public static /* synthetic */ String e(byte[] bArr, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        return d(bArr, str);
    }
}
