package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mu extends ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ n6 I;
    @DexIgnore
    public /* final */ n6 J;
    @DexIgnore
    public /* final */ byte[] K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ mu(fu fuVar, hs hsVar, k5 k5Var, int i, int i2) {
        super(hsVar, k5Var, (i2 & 8) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(fuVar.d.length + 2).put(fuVar.b.a()).put(fuVar.c.a()).put(fuVar.d).array();
        pq7.b(array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(fuVar.g.length + 2).put(fuVar.e.a()).put(fuVar.f.a()).put(fuVar.g).array();
        pq7.b(array2, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.H = array2;
        n6 n6Var = n6.DC;
        this.I = n6Var;
        this.J = n6Var;
        this.K = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final mt E(byte b) {
        return hu.g.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final long a(o7 o7Var) {
        return 0;
    }
}
