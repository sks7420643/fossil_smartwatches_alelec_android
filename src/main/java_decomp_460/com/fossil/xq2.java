package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xq2 extends gr2 implements wq2 {
    @DexIgnore
    public xq2() {
        super("com.google.android.gms.location.internal.ISettingsCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        E0((ka3) qr2.a(parcel, ka3.CREATOR));
        return true;
    }
}
