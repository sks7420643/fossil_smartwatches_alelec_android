package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r2 implements Parcelable.Creator<s2> {
    @DexIgnore
    public /* synthetic */ r2(kq7 kq7) {
    }

    @DexIgnore
    public final s2 a(byte b, byte[] bArr) {
        return new s2(b, new bv1(bArr));
    }

    @DexIgnore
    public s2 b(Parcel parcel) {
        return new s2(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public s2 createFromParcel(Parcel parcel) {
        return new s2(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public s2[] newArray(int i) {
        return new s2[i];
    }
}
