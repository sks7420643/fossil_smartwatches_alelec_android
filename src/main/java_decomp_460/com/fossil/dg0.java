package com.fossil;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.fossil.ig0;
import com.fossil.ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dg0 implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, ig0.a {
    @DexIgnore
    public cg0 b;
    @DexIgnore
    public ve0 c;
    @DexIgnore
    public ag0 d;
    @DexIgnore
    public ig0.a e;

    @DexIgnore
    public dg0(cg0 cg0) {
        this.b = cg0;
    }

    @DexIgnore
    public void a() {
        ve0 ve0 = this.c;
        if (ve0 != null) {
            ve0.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0.a
    public void b(cg0 cg0, boolean z) {
        if (z || cg0 == this.b) {
            a();
        }
        ig0.a aVar = this.e;
        if (aVar != null) {
            aVar.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0.a
    public boolean c(cg0 cg0) {
        ig0.a aVar = this.e;
        if (aVar != null) {
            return aVar.c(cg0);
        }
        return false;
    }

    @DexIgnore
    public void d(IBinder iBinder) {
        cg0 cg0 = this.b;
        ve0.a aVar = new ve0.a(cg0.w());
        ag0 ag0 = new ag0(aVar.b(), re0.abc_list_menu_item_layout);
        this.d = ag0;
        ag0.g(this);
        this.b.b(this.d);
        aVar.c(this.d.a(), this);
        View A = cg0.A();
        if (A != null) {
            aVar.e(A);
        } else {
            aVar.f(cg0.y());
            aVar.o(cg0.z());
        }
        aVar.j(this);
        ve0 a2 = aVar.a();
        this.c = a2;
        a2.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.N((eg0) this.d.a().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        this.d.b(this.b, true);
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.e(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i, keyEvent, 0);
    }
}
