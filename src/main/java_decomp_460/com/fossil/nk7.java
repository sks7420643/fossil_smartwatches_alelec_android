package com.fossil;

import io.flutter.embedding.engine.systemchannels.KeyEventChannel;
import io.flutter.plugin.common.BasicMessageChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nk7 implements BasicMessageChannel.Reply {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ KeyEventChannel f2540a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    public /* synthetic */ nk7(KeyEventChannel keyEventChannel, long j) {
        this.f2540a = keyEventChannel;
        this.b = j;
    }

    @DexIgnore
    @Override // io.flutter.plugin.common.BasicMessageChannel.Reply
    public final void reply(Object obj) {
        this.f2540a.a(this.b, obj);
    }
}
