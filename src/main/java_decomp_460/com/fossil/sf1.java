package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sf1<T> implements id1<T> {
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public sf1(T t) {
        ik1.d(t);
        this.b = t;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.id1
    public final int c() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<T> d() {
        return (Class<T>) this.b.getClass();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public final T get() {
        return this.b;
    }
}
