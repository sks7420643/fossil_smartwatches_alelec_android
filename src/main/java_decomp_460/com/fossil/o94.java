package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class o94 implements ft3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ q94 f2654a;

    @DexIgnore
    public o94(q94 q94) {
        this.f2654a = q94;
    }

    @DexIgnore
    public static ft3 a(q94 q94) {
        return new o94(q94);
    }

    @DexIgnore
    @Override // com.fossil.ft3
    public Object then(nt3 nt3) {
        return Boolean.valueOf(this.f2654a.i(nt3));
    }
}
