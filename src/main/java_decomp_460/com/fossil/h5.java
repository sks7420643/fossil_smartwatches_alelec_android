package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class h5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1429a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;

    /*
    static {
        int[] iArr = new int[f5.values().length];
        f1429a = iArr;
        iArr[f5.DISCONNECTED.ordinal()] = 1;
        f1429a[f5.CONNECTING.ordinal()] = 2;
        f1429a[f5.CONNECTED.ordinal()] = 3;
        f1429a[f5.DISCONNECTING.ordinal()] = 4;
        int[] iArr2 = new int[f5.values().length];
        b = iArr2;
        iArr2[f5.DISCONNECTED.ordinal()] = 1;
        b[f5.CONNECTED.ordinal()] = 2;
        b[f5.CONNECTING.ordinal()] = 3;
        b[f5.DISCONNECTING.ordinal()] = 4;
        int[] iArr3 = new int[b5.values().length];
        c = iArr3;
        iArr3[b5.DISCONNECTED.ordinal()] = 1;
        c[b5.CONNECTING.ordinal()] = 2;
        c[b5.CONNECTED.ordinal()] = 3;
        c[b5.DISCONNECTING.ordinal()] = 4;
        int[] iArr4 = new int[b5.values().length];
        d = iArr4;
        iArr4[b5.DISCONNECTED.ordinal()] = 1;
        d[b5.CONNECTING.ordinal()] = 2;
        d[b5.CONNECTED.ordinal()] = 3;
        d[b5.DISCONNECTING.ordinal()] = 4;
    }
    */
}
