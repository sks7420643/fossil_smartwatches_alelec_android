package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1146a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[qh5.values().length];
        f1146a = iArr;
        iArr[qh5.FEMALE.ordinal()] = 1;
        f1146a[qh5.MALE.ordinal()] = 2;
        f1146a[qh5.OTHER.ordinal()] = 3;
        int[] iArr2 = new int[qh5.values().length];
        b = iArr2;
        iArr2[qh5.FEMALE.ordinal()] = 1;
        b[qh5.MALE.ordinal()] = 2;
        b[qh5.OTHER.ordinal()] = 3;
    }
    */
}
