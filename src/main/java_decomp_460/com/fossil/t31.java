package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t31 implements s31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f3355a;
    @DexIgnore
    public /* final */ jw0<r31> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<r31> {
        @DexIgnore
        public a(t31 t31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, r31 r31) {
            String str = r31.f3076a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            String str2 = r31.b;
            if (str2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, str2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public t31(qw0 qw0) {
        this.f3355a = qw0;
        this.b = new a(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.s31
    public void a(r31 r31) {
        this.f3355a.assertNotSuspendingTransaction();
        this.f3355a.beginTransaction();
        try {
            this.b.insert((jw0<r31>) r31);
            this.f3355a.setTransactionSuccessful();
        } finally {
            this.f3355a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.s31
    public List<String> b(String str) {
        tw0 f = tw0.f("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f3355a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3355a, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }
}
