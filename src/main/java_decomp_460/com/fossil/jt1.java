package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ry1 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(gq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                gq1 gq1 = (gq1) readParcelable;
                nt1 nt1 = (nt1) parcel.readParcelable(nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new jt1(gq1, nt1, (ry1) readParcelable2, parcel.readInt(), parcel.readInt());
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jt1[] newArray(int i) {
            return new jt1[i];
        }
    }

    @DexIgnore
    public jt1(gq1 gq1, nt1 nt1, ry1 ry1, int i, int i2) {
        super(gq1, nt1);
        this.e = i;
        this.f = i2;
        this.d = ry1;
    }

    @DexIgnore
    public jt1(gq1 gq1, ry1 ry1, int i, int i2) {
        super(gq1, null);
        this.e = i;
        this.f = i2;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(g80.k(g80.k(super.a(), jd0.t3, this.d.toString()), jd0.A, Integer.valueOf(this.e)), jd0.B, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        try {
            i9 i9Var = i9.d;
            jq1 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9Var.a(s, ry1, new ib0(((lq1) deviceRequest).c(), new ry1(this.d.getMajor(), this.d.getMinor()), this.e, this.f).a());
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sx1 e2) {
            d90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(jt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            jt1 jt1 = (jt1) obj;
            if (!pq7.a(this.d, jt1.d)) {
                return false;
            }
            if (this.e != jt1.e) {
                return false;
            }
            return this.f == jt1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData");
    }

    @DexIgnore
    public final int getHour() {
        return this.e;
    }

    @DexIgnore
    public final ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    public final int getMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Integer.valueOf(this.e).hashCode()) * 31) + Integer.valueOf(this.f).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
