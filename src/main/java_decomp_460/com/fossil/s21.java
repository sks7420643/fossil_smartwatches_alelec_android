package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s21<T> extends t21<T> {
    @DexIgnore
    public static /* final */ String h; // = x01.f("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                s21.this.h(context, intent);
            }
        }
    }

    @DexIgnore
    public s21(Context context, k41 k41) {
        super(context, k41);
    }

    @DexIgnore
    @Override // com.fossil.t21
    public void e() {
        x01.c().a(h, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.registerReceiver(this.g, g());
    }

    @DexIgnore
    @Override // com.fossil.t21
    public void f() {
        x01.c().a(h, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter g();

    @DexIgnore
    public abstract void h(Context context, Intent intent);
}
