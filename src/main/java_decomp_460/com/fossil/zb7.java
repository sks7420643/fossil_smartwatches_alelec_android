package com.fossil;

import android.view.View;
import com.fossil.w67;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zb7 {
    @DexIgnore
    w67.c getType();

    @DexIgnore
    View getView();
}
