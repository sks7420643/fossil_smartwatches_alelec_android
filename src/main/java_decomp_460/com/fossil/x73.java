package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x73 implements y73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f4046a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f4046a = hw2.d("measurement.sdk.screen.manual_screen_view_logging", true);
        b = hw2.d("measurement.sdk.screen.disabling_automatic_reporting", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.y73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.y73
    public final boolean zzb() {
        return f4046a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.y73
    public final boolean zzc() {
        return b.o().booleanValue();
    }
}
