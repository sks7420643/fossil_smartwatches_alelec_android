package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os3 extends zc2 implements z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<os3> CREATOR; // = new qs3();
    @DexIgnore
    public /* final */ List<String> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public os3(List<String> list, String str) {
        this.b = list;
        this.c = str;
    }

    @DexIgnore
    @Override // com.fossil.z62
    public final Status a() {
        return this.c != null ? Status.f : Status.j;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.w(parcel, 1, this.b, false);
        bd2.u(parcel, 2, this.c, false);
        bd2.b(parcel, a2);
    }
}
