package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cj1 {

    @DexIgnore
    public enum a {
        RUNNING(false),
        PAUSED(false),
        CLEARED(false),
        SUCCESS(true),
        FAILED(true);
        
        @DexIgnore
        public /* final */ boolean isComplete;

        @DexIgnore
        public a(boolean z) {
            this.isComplete = z;
        }

        @DexIgnore
        public boolean isComplete() {
            return this.isComplete;
        }
    }

    @DexIgnore
    void a(bj1 bj1);

    @DexIgnore
    boolean b();

    @DexIgnore
    cj1 c();

    @DexIgnore
    boolean d(bj1 bj1);

    @DexIgnore
    boolean e(bj1 bj1);

    @DexIgnore
    void i(bj1 bj1);

    @DexIgnore
    boolean k(bj1 bj1);
}
