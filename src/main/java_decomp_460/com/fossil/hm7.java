package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hm7 extends gm7 {
    @DexIgnore
    public static final <T> ArrayList<T> c(T... tArr) {
        pq7.c(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new am7(tArr, true));
    }

    @DexIgnore
    public static final <T> Collection<T> d(T[] tArr) {
        pq7.c(tArr, "$this$asCollection");
        return new am7(tArr, false);
    }

    @DexIgnore
    public static final <T> List<T> e() {
        return rm7.INSTANCE;
    }

    @DexIgnore
    public static final wr7 f(Collection<?> collection) {
        pq7.c(collection, "$this$indices");
        return new wr7(0, collection.size() - 1);
    }

    @DexIgnore
    public static final <T> int g(List<? extends T> list) {
        pq7.c(list, "$this$lastIndex");
        return list.size() - 1;
    }

    @DexIgnore
    public static final <T> List<T> h(T... tArr) {
        pq7.c(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length > 0 ? dm7.d(tArr) : e();
    }

    @DexIgnore
    public static final <T> List<T> i(T... tArr) {
        pq7.c(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new am7(tArr, true));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> j(List<? extends T> list) {
        pq7.c(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        return size != 0 ? size != 1 ? list : gm7.b(list.get(0)) : e();
    }

    @DexIgnore
    public static final void k() {
        throw new ArithmeticException("Count overflow has happened.");
    }

    @DexIgnore
    public static final void l() {
        throw new ArithmeticException("Index overflow has happened.");
    }
}
