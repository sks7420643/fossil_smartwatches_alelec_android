package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c6 extends u5 {
    @DexIgnore
    public /* final */ n5 k; // = n5.HIGH;
    @DexIgnore
    public b5 l; // = b5.DISCONNECTED;

    @DexIgnore
    public c6(n4 n4Var) {
        super(v5.n, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        int i = h5.d[k5Var.D().ordinal()];
        if (i == 1) {
            g7 g7Var = new g7(f7.SUCCESS, 0, 2);
            b5 b5Var = b5.DISCONNECTED;
            k5Var.b.post(new q4(k5Var, g7Var, b5Var, b5Var));
        } else if (i == 2 || i == 3) {
            g7 a2 = g7.d.a(l80.d.g(k5Var.A));
            if (a2.b != f7.SUCCESS) {
                k5Var.b.post(new q4(k5Var, a2, b5.CONNECTED, b5.DISCONNECTED));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        k(h7Var);
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = this.l == b5.DISCONNECTED ? s5.a(this.e, null, r5.b, null, 5) : s5.a(this.e, null, r5.e, null, 5);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public n5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return (h7Var instanceof y6) && ((y6) h7Var).b == b5.DISCONNECTED;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.l;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.l = ((y6) h7Var).b;
    }
}
