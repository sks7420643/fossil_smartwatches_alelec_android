package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k21<T> implements e21<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<String> f1855a; // = new ArrayList();
    @DexIgnore
    public T b;
    @DexIgnore
    public t21<T> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(List<String> list);

        @DexIgnore
        void b(List<String> list);
    }

    @DexIgnore
    public k21(t21<T> t21) {
        this.c = t21;
    }

    @DexIgnore
    @Override // com.fossil.e21
    public void a(T t) {
        this.b = t;
        h(this.d, t);
    }

    @DexIgnore
    public abstract boolean b(o31 o31);

    @DexIgnore
    public abstract boolean c(T t);

    @DexIgnore
    public boolean d(String str) {
        T t = this.b;
        return t != null && c(t) && this.f1855a.contains(str);
    }

    @DexIgnore
    public void e(Iterable<o31> iterable) {
        this.f1855a.clear();
        for (o31 o31 : iterable) {
            if (b(o31)) {
                this.f1855a.add(o31.f2626a);
            }
        }
        if (this.f1855a.isEmpty()) {
            this.c.c(this);
        } else {
            this.c.a(this);
        }
        h(this.d, this.b);
    }

    @DexIgnore
    public void f() {
        if (!this.f1855a.isEmpty()) {
            this.f1855a.clear();
            this.c.c(this);
        }
    }

    @DexIgnore
    public void g(a aVar) {
        if (this.d != aVar) {
            this.d = aVar;
            h(aVar, this.b);
        }
    }

    @DexIgnore
    public final void h(a aVar, T t) {
        if (!this.f1855a.isEmpty() && aVar != null) {
            if (t == null || c(t)) {
                aVar.b(this.f1855a);
            } else {
                aVar.a(this.f1855a);
            }
        }
    }
}
