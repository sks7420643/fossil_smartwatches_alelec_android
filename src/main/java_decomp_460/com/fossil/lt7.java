package com.fossil;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt7 {
    @DexIgnore
    public static final ht7 e(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new it7(matcher, charSequence);
    }

    @DexIgnore
    public static final ht7 f(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new it7(matcher, charSequence);
    }

    @DexIgnore
    public static final wr7 g(MatchResult matchResult) {
        return bs7.m(matchResult.start(), matchResult.end());
    }

    @DexIgnore
    public static final int h(Iterable<? extends gt7> iterable) {
        int i = 0;
        for (gt7 gt7 : iterable) {
            i = gt7.getValue() | i;
        }
        return i;
    }
}
