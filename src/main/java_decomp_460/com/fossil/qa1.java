package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.fossil.oa1;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qa1 extends ContextWrapper {
    @DexIgnore
    public static /* final */ xa1<?, ?> k; // = new na1();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ od1 f2947a;
    @DexIgnore
    public /* final */ ua1 b;
    @DexIgnore
    public /* final */ oj1 c;
    @DexIgnore
    public /* final */ oa1.a d;
    @DexIgnore
    public /* final */ List<ej1<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, xa1<?, ?>> f;
    @DexIgnore
    public /* final */ xc1 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public fj1 j;

    @DexIgnore
    public qa1(Context context, od1 od1, ua1 ua1, oj1 oj1, oa1.a aVar, Map<Class<?>, xa1<?, ?>> map, List<ej1<Object>> list, xc1 xc1, boolean z, int i2) {
        super(context.getApplicationContext());
        this.f2947a = od1;
        this.b = ua1;
        this.c = oj1;
        this.d = aVar;
        this.e = list;
        this.f = map;
        this.g = xc1;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <X> rj1<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public od1 b() {
        return this.f2947a;
    }

    @DexIgnore
    public List<ej1<Object>> c() {
        return this.e;
    }

    @DexIgnore
    public fj1 d() {
        fj1 fj1;
        synchronized (this) {
            if (this.j == null) {
                this.j = (fj1) this.d.build().V();
            }
            fj1 = this.j;
        }
        return fj1;
    }

    @DexIgnore
    public <T> xa1<?, T> e(Class<T> cls) {
        xa1<?, T> xa1 = (xa1<?, T>) this.f.get(cls);
        if (xa1 == null) {
            xa1<?, ?> xa12 = xa1;
            for (Map.Entry<Class<?>, xa1<?, ?>> entry : this.f.entrySet()) {
                if (entry.getKey().isAssignableFrom(cls)) {
                    xa12 = entry.getValue();
                }
            }
            xa1 = xa12;
        }
        return xa1 == null ? (xa1<?, T>) k : xa1;
    }

    @DexIgnore
    public xc1 f() {
        return this.g;
    }

    @DexIgnore
    public int g() {
        return this.i;
    }

    @DexIgnore
    public ua1 h() {
        return this.b;
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }
}
