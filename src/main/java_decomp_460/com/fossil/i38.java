package com.fossil;

import com.fossil.j38;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i38 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService E; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), b28.G("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ Socket A;
    @DexIgnore
    public /* final */ l38 B;
    @DexIgnore
    public /* final */ l C;
    @DexIgnore
    public /* final */ Set<Integer> D; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ j c;
    @DexIgnore
    public /* final */ Map<Integer, k38> d; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ ScheduledExecutorService i;
    @DexIgnore
    public /* final */ ExecutorService j;
    @DexIgnore
    public /* final */ n38 k;
    @DexIgnore
    public long l; // = 0;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public long s; // = 0;
    @DexIgnore
    public long t; // = 0;
    @DexIgnore
    public long u; // = 0;
    @DexIgnore
    public long v; // = 0;
    @DexIgnore
    public long w; // = 0;
    @DexIgnore
    public long x;
    @DexIgnore
    public o38 y; // = new o38();
    @DexIgnore
    public /* final */ o38 z; // = new o38();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ d38 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, d38 d38) {
            super(str, objArr);
            this.c = i;
            this.d = d38;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            try {
                i38.this.t0(this.c, this.d);
            } catch (IOException e2) {
                i38.this.C();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.c = i;
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            try {
                i38.this.B.D(this.c, this.d);
            } catch (IOException e2) {
                i38.this.C();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends a28 {
        @DexIgnore
        public c(String str, Object... objArr) {
            super(str, objArr);
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            i38.this.s0(false, 2, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.c = i;
            this.d = list;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            if (i38.this.k.a(this.c, this.d)) {
                try {
                    i38.this.B.A(this.c, d38.CANCEL);
                    synchronized (i38.this) {
                        i38.this.D.remove(Integer.valueOf(this.c));
                    }
                } catch (IOException e2) {
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i, List list, boolean z) {
            super(str, objArr);
            this.c = i;
            this.d = list;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            boolean b = i38.this.k.b(this.c, this.d, this.e);
            if (b) {
                try {
                    i38.this.B.A(this.c, d38.CANCEL);
                } catch (IOException e2) {
                    return;
                }
            }
            if (b || this.e) {
                synchronized (i38.this) {
                    i38.this.D.remove(Integer.valueOf(this.c));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ i48 d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i, i48 i48, int i2, boolean z) {
            super(str, objArr);
            this.c = i;
            this.d = i48;
            this.e = i2;
            this.f = z;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            try {
                boolean d2 = i38.this.k.d(this.c, this.d, this.e, this.f);
                if (d2) {
                    i38.this.B.A(this.c, d38.CANCEL);
                }
                if (d2 || this.f) {
                    synchronized (i38.this) {
                        i38.this.D.remove(Integer.valueOf(this.c));
                    }
                }
            } catch (IOException e2) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends a28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ d38 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, Object[] objArr, int i, d38 d38) {
            super(str, objArr);
            this.c = i;
            this.d = d38;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            i38.this.k.c(this.c, this.d);
            synchronized (i38.this) {
                i38.this.D.remove(Integer.valueOf(this.c));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Socket f1578a;
        @DexIgnore
        public String b;
        @DexIgnore
        public k48 c;
        @DexIgnore
        public j48 d;
        @DexIgnore
        public j e; // = j.f1579a;
        @DexIgnore
        public n38 f; // = n38.f2461a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public h(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public i38 a() {
            return new i38(this);
        }

        @DexIgnore
        public h b(j jVar) {
            this.e = jVar;
            return this;
        }

        @DexIgnore
        public h c(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public h d(Socket socket, String str, k48 k48, j48 j48) {
            this.f1578a = socket;
            this.b = str;
            this.c = k48;
            this.d = j48;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i extends a28 {
        @DexIgnore
        public i() {
            super("OkHttp %s ping", i38.this.e);
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            boolean z;
            synchronized (i38.this) {
                if (i38.this.m < i38.this.l) {
                    z = true;
                } else {
                    i38.h(i38.this);
                    z = false;
                }
            }
            if (z) {
                i38.this.C();
            } else {
                i38.this.s0(false, 1, 0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ j f1579a; // = new a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends j {
            @DexIgnore
            @Override // com.fossil.i38.j
            public void c(k38 k38) throws IOException {
                k38.f(d38.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public void b(i38 i38) {
        }

        @DexIgnore
        public abstract void c(k38 k38) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends a28 {
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        public k(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", i38.this.e, Integer.valueOf(i), Integer.valueOf(i2));
            this.c = z;
            this.d = i;
            this.e = i2;
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            i38.this.s0(this.c, this.d, this.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends a28 implements j38.b {
        @DexIgnore
        public /* final */ j38 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends a28 {
            @DexIgnore
            public /* final */ /* synthetic */ k38 c;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, k38 k38) {
                super(str, objArr);
                this.c = k38;
            }

            @DexIgnore
            @Override // com.fossil.a28
            public void k() {
                try {
                    i38.this.c.c(this.c);
                } catch (IOException e) {
                    w38 j = w38.j();
                    j.q(4, "Http2Connection.Listener failure for " + i38.this.e, e);
                    try {
                        this.c.f(d38.PROTOCOL_ERROR);
                    } catch (IOException e2) {
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends a28 {
            @DexIgnore
            public /* final */ /* synthetic */ boolean c;
            @DexIgnore
            public /* final */ /* synthetic */ o38 d;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(String str, Object[] objArr, boolean z, o38 o38) {
                super(str, objArr);
                this.c = z;
                this.d = o38;
            }

            @DexIgnore
            @Override // com.fossil.a28
            public void k() {
                l.this.l(this.c, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends a28 {
            @DexIgnore
            public c(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            @Override // com.fossil.a28
            public void k() {
                i38 i38 = i38.this;
                i38.c.b(i38);
            }
        }

        @DexIgnore
        public l(j38 j38) {
            super("OkHttp %s", i38.this.e);
            this.c = j38;
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void b(boolean z, o38 o38) {
            try {
                i38.this.i.execute(new b("OkHttp %s ACK Settings", new Object[]{i38.this.e}, z, o38));
            } catch (RejectedExecutionException e) {
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0079, code lost:
            r0.q(r12);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x007c, code lost:
            if (r9 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x007e, code lost:
            r0.p();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        @DexIgnore
        @Override // com.fossil.j38.b
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void c(boolean r9, int r10, int r11, java.util.List<com.fossil.e38> r12) {
            /*
                r8 = this;
                com.fossil.i38 r0 = com.fossil.i38.this
                boolean r0 = r0.X(r10)
                if (r0 == 0) goto L_0x000e
                com.fossil.i38 r0 = com.fossil.i38.this
                r0.S(r10, r12, r9)
            L_0x000d:
                return
            L_0x000e:
                com.fossil.i38 r6 = com.fossil.i38.this
                monitor-enter(r6)
                com.fossil.i38 r0 = com.fossil.i38.this     // Catch:{ all -> 0x0023 }
                com.fossil.k38 r0 = r0.D(r10)     // Catch:{ all -> 0x0023 }
                if (r0 != 0) goto L_0x0078
                com.fossil.i38 r0 = com.fossil.i38.this     // Catch:{ all -> 0x0023 }
                boolean r0 = com.fossil.i38.j(r0)     // Catch:{ all -> 0x0023 }
                if (r0 == 0) goto L_0x0026
                monitor-exit(r6)     // Catch:{ all -> 0x0023 }
                goto L_0x000d
            L_0x0023:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0023 }
                throw r0
            L_0x0026:
                com.fossil.i38 r0 = com.fossil.i38.this
                int r0 = r0.f
                if (r10 > r0) goto L_0x002e
                monitor-exit(r6)
                goto L_0x000d
            L_0x002e:
                int r0 = r10 % 2
                com.fossil.i38 r1 = com.fossil.i38.this
                int r1 = r1.g
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x003a
                monitor-exit(r6)
                goto L_0x000d
            L_0x003a:
                com.fossil.p18 r5 = com.fossil.b28.H(r12)
                com.fossil.k38 r0 = new com.fossil.k38
                com.fossil.i38 r2 = com.fossil.i38.this
                r3 = 0
                r1 = r10
                r4 = r9
                r0.<init>(r1, r2, r3, r4, r5)
                com.fossil.i38 r1 = com.fossil.i38.this
                r1.f = r10
                com.fossil.i38 r1 = com.fossil.i38.this
                java.util.Map<java.lang.Integer, com.fossil.k38> r1 = r1.d
                java.lang.Integer r2 = java.lang.Integer.valueOf(r10)
                r1.put(r2, r0)
                java.util.concurrent.ExecutorService r1 = com.fossil.i38.l()
                com.fossil.i38$l$a r2 = new com.fossil.i38$l$a
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]
                r5 = 0
                com.fossil.i38 r7 = com.fossil.i38.this
                java.lang.String r7 = r7.e
                r4[r5] = r7
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r10)
                r4[r5] = r7
                r2.<init>(r3, r4, r0)
                r1.execute(r2)
                monitor-exit(r6)
                goto L_0x000d
            L_0x0078:
                monitor-exit(r6)
                r0.q(r12)
                if (r9 == 0) goto L_0x000d
                r0.p()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.i38.l.c(boolean, int, int, java.util.List):void");
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void d(int i, long j) {
            if (i == 0) {
                synchronized (i38.this) {
                    i38.this.x += j;
                    i38.this.notifyAll();
                }
                return;
            }
            k38 D = i38.this.D(i);
            if (D != null) {
                synchronized (D) {
                    D.c(j);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void e(boolean z, int i, k48 k48, int i2) throws IOException {
            if (i38.this.X(i)) {
                i38.this.P(i, k48, i2, z);
                return;
            }
            k38 D = i38.this.D(i);
            if (D == null) {
                i38.this.u0(i, d38.PROTOCOL_ERROR);
                long j = (long) i2;
                i38.this.q0(j);
                k48.skip(j);
                return;
            }
            D.o(k48, i2);
            if (z) {
                D.p();
            }
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void f(boolean z, int i, int i2) {
            if (z) {
                synchronized (i38.this) {
                    if (i == 1) {
                        i38.c(i38.this);
                    } else if (i == 2) {
                        i38.o(i38.this);
                    } else if (i == 3) {
                        i38.A(i38.this);
                        i38.this.notifyAll();
                    }
                }
                return;
            }
            try {
                i38.this.i.execute(new k(true, i, i2));
            } catch (RejectedExecutionException e) {
            }
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void g(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void h(int i, d38 d38) {
            if (i38.this.X(i)) {
                i38.this.V(i, d38);
                return;
            }
            k38 b0 = i38.this.b0(i);
            if (b0 != null) {
                b0.r(d38);
            }
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void i(int i, int i2, List<e38> list) {
            i38.this.T(i2, list);
        }

        @DexIgnore
        @Override // com.fossil.j38.b
        public void j(int i, d38 d38, l48 l48) {
            k38[] k38Arr;
            l48.size();
            synchronized (i38.this) {
                k38Arr = (k38[]) i38.this.d.values().toArray(new k38[i38.this.d.size()]);
                i38.this.h = true;
            }
            for (k38 k38 : k38Arr) {
                if (k38.i() > i && k38.l()) {
                    k38.r(d38.REFUSED_STREAM);
                    i38.this.b0(k38.i());
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.a28
        public void k() {
            d38 d38;
            d38 d382;
            i38 i38;
            d38 d383 = d38.INTERNAL_ERROR;
            try {
                this.c.c(this);
                do {
                } while (this.c.b(false, this));
                d38 = d38.NO_ERROR;
                try {
                    d382 = d38.CANCEL;
                    try {
                        i38 = i38.this;
                    } catch (IOException e) {
                    }
                } catch (IOException e2) {
                    try {
                        d38 = d38.PROTOCOL_ERROR;
                        d382 = d38.PROTOCOL_ERROR;
                        i38 = i38.this;
                        i38.B(d38, d382);
                        b28.g(this.c);
                    } catch (Throwable th) {
                        th = th;
                        try {
                            i38.this.B(d38, d383);
                        } catch (IOException e3) {
                        }
                        b28.g(this.c);
                        throw th;
                    }
                }
            } catch (IOException e4) {
                d38 = d383;
                d38 = d38.PROTOCOL_ERROR;
                d382 = d38.PROTOCOL_ERROR;
                i38 = i38.this;
                i38.B(d38, d382);
                b28.g(this.c);
            } catch (Throwable th2) {
                th = th2;
                d38 = d383;
                i38.this.B(d38, d383);
                b28.g(this.c);
                throw th;
            }
            i38.B(d38, d382);
            b28.g(this.c);
        }

        @DexIgnore
        public void l(boolean z, o38 o38) {
            long j;
            k38[] k38Arr;
            synchronized (i38.this.B) {
                synchronized (i38.this) {
                    int d2 = i38.this.z.d();
                    if (z) {
                        i38.this.z.a();
                    }
                    i38.this.z.h(o38);
                    int d3 = i38.this.z.d();
                    if (d3 == -1 || d3 == d2) {
                        j = 0;
                        k38Arr = null;
                    } else {
                        j = (long) (d3 - d2);
                        k38Arr = !i38.this.d.isEmpty() ? (k38[]) i38.this.d.values().toArray(new k38[i38.this.d.size()]) : null;
                    }
                }
                try {
                    i38.this.B.a(i38.this.z);
                } catch (IOException e) {
                    i38.this.C();
                }
            }
            if (k38Arr != null) {
                for (k38 k38 : k38Arr) {
                    synchronized (k38) {
                        k38.c(j);
                    }
                }
            }
            i38.E.execute(new c("OkHttp %s settings", i38.this.e));
        }
    }

    @DexIgnore
    public i38(h hVar) {
        this.k = hVar.f;
        boolean z2 = hVar.g;
        this.b = z2;
        this.c = hVar.e;
        int i2 = z2 ? 1 : 2;
        this.g = i2;
        if (hVar.g) {
            this.g = i2 + 2;
        }
        if (hVar.g) {
            this.y.i(7, 16777216);
        }
        this.e = hVar.b;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, b28.G(b28.r("OkHttp %s Writer", this.e), false));
        this.i = scheduledThreadPoolExecutor;
        if (hVar.h != 0) {
            i iVar = new i();
            int i3 = hVar.h;
            scheduledThreadPoolExecutor.scheduleAtFixedRate(iVar, (long) i3, (long) i3, TimeUnit.MILLISECONDS);
        }
        this.j = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), b28.G(b28.r("OkHttp %s Push Observer", this.e), true));
        this.z.i(7, 65535);
        this.z.i(5, 16384);
        this.x = (long) this.z.d();
        this.A = hVar.f1578a;
        this.B = new l38(hVar.d, this.b);
        this.C = new l(new j38(hVar.c, this.b));
    }

    @DexIgnore
    public static /* synthetic */ long A(i38 i38) {
        long j2 = i38.u;
        i38.u = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long c(i38 i38) {
        long j2 = i38.m;
        i38.m = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long h(i38 i38) {
        long j2 = i38.l;
        i38.l = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long o(i38 i38) {
        long j2 = i38.t;
        i38.t = 1 + j2;
        return j2;
    }

    @DexIgnore
    public void B(d38 d38, d38 d382) throws IOException {
        k38[] k38Arr;
        try {
            i0(d38);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.d.isEmpty()) {
                this.d.clear();
                k38Arr = (k38[]) this.d.values().toArray(new k38[this.d.size()]);
            } else {
                k38Arr = null;
            }
        }
        if (k38Arr != null) {
            IOException iOException = e;
            for (k38 k38 : k38Arr) {
                try {
                    k38.f(d382);
                } catch (IOException e3) {
                    if (iOException != null) {
                        iOException = e3;
                    }
                }
            }
            e = iOException;
        }
        try {
            this.B.close();
            e = e;
        } catch (IOException e4) {
            e = e4;
            if (e != null) {
                e = e;
            }
        }
        try {
            this.A.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.i.shutdown();
        this.j.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public final void C() {
        try {
            B(d38.PROTOCOL_ERROR, d38.PROTOCOL_ERROR);
        } catch (IOException e2) {
        }
    }

    @DexIgnore
    public k38 D(int i2) {
        k38 k38;
        synchronized (this) {
            k38 = this.d.get(Integer.valueOf(i2));
        }
        return k38;
    }

    @DexIgnore
    public boolean F(long j2) {
        synchronized (this) {
            if (this.h) {
                return false;
            }
            return this.t >= this.s || j2 < this.v;
        }
    }

    @DexIgnore
    public int G() {
        int e2;
        synchronized (this) {
            e2 = this.z.e(Integer.MAX_VALUE);
        }
        return e2;
    }

    @DexIgnore
    public final k38 L(int i2, List<e38> list, boolean z2) throws IOException {
        int i3;
        k38 k38;
        boolean z3;
        boolean z4 = !z2;
        synchronized (this.B) {
            synchronized (this) {
                if (this.g > 1073741823) {
                    i0(d38.REFUSED_STREAM);
                }
                if (!this.h) {
                    i3 = this.g;
                    this.g += 2;
                    k38 = new k38(i3, this, z4, false, null);
                    z3 = !z2 || this.x == 0 || k38.b == 0;
                    if (k38.m()) {
                        this.d.put(Integer.valueOf(i3), k38);
                    }
                } else {
                    throw new c38();
                }
            }
            if (i2 == 0) {
                this.B.C(z4, i3, i2, list);
            } else if (!this.b) {
                this.B.o(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (z3) {
            this.B.flush();
        }
        return k38;
    }

    @DexIgnore
    public k38 M(List<e38> list, boolean z2) throws IOException {
        return L(0, list, z2);
    }

    @DexIgnore
    public void P(int i2, k48 k48, int i3, boolean z2) throws IOException {
        i48 i48 = new i48();
        long j2 = (long) i3;
        k48.j0(j2);
        k48.d0(i48, j2);
        if (i48.p0() == j2) {
            Q(new f("OkHttp %s Push Data[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, i48, i3, z2));
            return;
        }
        throw new IOException(i48.p0() + " != " + i3);
    }

    @DexIgnore
    public final void Q(a28 a28) {
        synchronized (this) {
            if (!this.h) {
                this.j.execute(a28);
            }
        }
    }

    @DexIgnore
    public void S(int i2, List<e38> list, boolean z2) {
        try {
            Q(new e("OkHttp %s Push Headers[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, list, z2));
        } catch (RejectedExecutionException e2) {
        }
    }

    @DexIgnore
    public void T(int i2, List<e38> list) {
        synchronized (this) {
            if (this.D.contains(Integer.valueOf(i2))) {
                u0(i2, d38.PROTOCOL_ERROR);
                return;
            }
            this.D.add(Integer.valueOf(i2));
            try {
                Q(new d("OkHttp %s Push Request[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException e2) {
            }
        }
    }

    @DexIgnore
    public void V(int i2, d38 d38) {
        Q(new g("OkHttp %s Push Reset[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, d38));
    }

    @DexIgnore
    public boolean X(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public k38 b0(int i2) {
        k38 remove;
        synchronized (this) {
            remove = this.d.remove(Integer.valueOf(i2));
            notifyAll();
        }
        return remove;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        B(d38.NO_ERROR, d38.CANCEL);
    }

    @DexIgnore
    public void flush() throws IOException {
        this.B.flush();
    }

    @DexIgnore
    public void g0() {
        synchronized (this) {
            if (this.t >= this.s) {
                this.s++;
                this.v = System.nanoTime() + 1000000000;
                try {
                    this.i.execute(new c("OkHttp %s ping", this.e));
                } catch (RejectedExecutionException e2) {
                }
            }
        }
    }

    @DexIgnore
    public void i0(d38 d38) throws IOException {
        synchronized (this.B) {
            synchronized (this) {
                if (!this.h) {
                    this.h = true;
                    this.B.j(this.f, d38, b28.f386a);
                }
            }
        }
    }

    @DexIgnore
    public void o0() throws IOException {
        p0(true);
    }

    @DexIgnore
    public void p0(boolean z2) throws IOException {
        if (z2) {
            this.B.b();
            this.B.B(this.y);
            int d2 = this.y.d();
            if (d2 != 65535) {
                this.B.D(0, (long) (d2 - 65535));
            }
        }
        new Thread(this.C).start();
    }

    @DexIgnore
    public void q0(long j2) {
        synchronized (this) {
            long j3 = this.w + j2;
            this.w = j3;
            if (j3 >= ((long) (this.y.d() / 2))) {
                v0(0, this.w);
                this.w = 0;
            }
        }
    }

    @DexIgnore
    public void r0(int i2, boolean z2, i48 i48, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.B.c(z2, i2, i48, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.x <= 0) {
                    try {
                        if (this.d.containsKey(Integer.valueOf(i2))) {
                            wait();
                        } else {
                            throw new IOException("stream closed");
                        }
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.x), this.B.l());
                j3 = (long) min;
                this.x -= j3;
            }
            j2 -= j3;
            this.B.c(z2 && j2 == 0, i2, i48, min);
        }
    }

    @DexIgnore
    public void s0(boolean z2, int i2, int i3) {
        try {
            this.B.m(z2, i2, i3);
        } catch (IOException e2) {
            C();
        }
    }

    @DexIgnore
    public void t0(int i2, d38 d38) throws IOException {
        this.B.A(i2, d38);
    }

    @DexIgnore
    public void u0(int i2, d38 d38) {
        try {
            this.i.execute(new a("OkHttp %s stream %d", new Object[]{this.e, Integer.valueOf(i2)}, i2, d38));
        } catch (RejectedExecutionException e2) {
        }
    }

    @DexIgnore
    public void v0(int i2, long j2) {
        try {
            this.i.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.e, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException e2) {
        }
    }
}
