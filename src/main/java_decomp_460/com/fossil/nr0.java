package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr0<T> extends js0<T> {
    @DexIgnore
    public jr0<T> l;
    @DexIgnore
    public sr0 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ nr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(nr0 nr0) {
            super(0);
            this.this$0 = nr0;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            this.this$0.l = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.CoroutineLiveData", f = "CoroutineLiveData.kt", l = {234}, m = "clearSource$lifecycle_livedata_ktx_release")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(nr0 nr0, qn7 qn7) {
            super(qn7);
            this.this$0 = nr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.CoroutineLiveData", f = "CoroutineLiveData.kt", l = {227, 228}, m = "emitSource$lifecycle_livedata_ktx_release")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(nr0 nr0, qn7 qn7) {
            super(qn7);
            this.this$0 = nr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore
    public nr0(tn7 tn7, long j, vp7<? super hs0<T>, ? super qn7<? super tl7>, ? extends Object> vp7) {
        pq7.c(tn7, "context");
        pq7.c(vp7, "block");
        this.l = new jr0<>(this, vp7, j, jv7.a(bw7.c().S().plus(tn7).plus(ux7.a((xw7) tn7.get(xw7.r)))), new a(this));
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData, com.fossil.js0
    public void j() {
        super.j();
        jr0<T> jr0 = this.l;
        if (jr0 != null) {
            jr0.h();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData, com.fossil.js0
    public void k() {
        super.k();
        jr0<T> jr0 = this.l;
        if (jr0 != null) {
            jr0.g();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.nr0.b
            if (r0 == 0) goto L_0x0030
            r0 = r6
            com.fossil.nr0$b r0 = (com.fossil.nr0.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0030
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003e
            if (r3 != r4) goto L_0x0036
            java.lang.Object r0 = r0.L$0
            com.fossil.nr0 r0 = (com.fossil.nr0) r0
            com.fossil.el7.b(r1)
            r5 = r0
        L_0x0027:
            r0 = r1
            com.fossil.tl7 r0 = (com.fossil.tl7) r0
        L_0x002a:
            r0 = 0
            r5.m = r0
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x002f:
            return r0
        L_0x0030:
            com.fossil.nr0$b r0 = new com.fossil.nr0$b
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            com.fossil.el7.b(r1)
            com.fossil.sr0 r1 = r5.m
            if (r1 == 0) goto L_0x002a
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.b(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nr0.s(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object t(androidx.lifecycle.LiveData<T> r8, com.fossil.qn7<? super com.fossil.dw7> r9) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.nr0.c
            if (r0 == 0) goto L_0x0036
            r0 = r9
            com.fossil.nr0$c r0 = (com.fossil.nr0.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0036
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r3 = r2.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x005f
            if (r0 == r5) goto L_0x0045
            if (r0 != r6) goto L_0x003d
            java.lang.Object r0 = r2.L$1
            androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
            java.lang.Object r0 = r2.L$0
            com.fossil.nr0 r0 = (com.fossil.nr0) r0
            com.fossil.el7.b(r3)
            r1 = r3
            r7 = r0
        L_0x0030:
            r0 = r1
            com.fossil.sr0 r0 = (com.fossil.sr0) r0
            r7.m = r0
        L_0x0035:
            return r0
        L_0x0036:
            com.fossil.nr0$c r0 = new com.fossil.nr0$c
            r0.<init>(r7, r9)
            r2 = r0
            goto L_0x0015
        L_0x003d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0045:
            java.lang.Object r0 = r2.L$1
            androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
            java.lang.Object r1 = r2.L$0
            com.fossil.nr0 r1 = (com.fossil.nr0) r1
            com.fossil.el7.b(r3)
            r7 = r1
        L_0x0051:
            r2.L$0 = r7
            r2.L$1 = r0
            r2.label = r6
            java.lang.Object r1 = com.fossil.or0.a(r7, r0, r2)
            if (r1 != r4) goto L_0x0030
            r0 = r4
            goto L_0x0035
        L_0x005f:
            com.fossil.el7.b(r3)
            r2.L$0 = r7
            r2.L$1 = r8
            r2.label = r5
            java.lang.Object r0 = r7.s(r2)
            if (r0 != r4) goto L_0x0070
            r0 = r4
            goto L_0x0035
        L_0x0070:
            r0 = r8
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nr0.t(androidx.lifecycle.LiveData, com.fossil.qn7):java.lang.Object");
    }
}
