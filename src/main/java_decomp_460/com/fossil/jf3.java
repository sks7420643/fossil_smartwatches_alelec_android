package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf3 implements Parcelable.Creator<ve3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ve3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 2) {
                ad2.B(parcel, t);
            } else {
                i = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ve3(i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ve3[] newArray(int i) {
        return new ve3[i];
    }
}
