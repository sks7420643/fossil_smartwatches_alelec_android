package com.fossil;

import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c41 implements Runnable {
    @DexIgnore
    public static /* final */ String e; // = x01.f("StopWorkRunnable");
    @DexIgnore
    public /* final */ s11 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public c41(s11 s11, String str, boolean z) {
        this.b = s11;
        this.c = str;
        this.d = z;
    }

    @DexIgnore
    public void run() {
        boolean n;
        WorkDatabase p = this.b.p();
        m11 n2 = this.b.n();
        p31 j = p.j();
        p.beginTransaction();
        try {
            boolean g = n2.g(this.c);
            if (this.d) {
                n = this.b.n().m(this.c);
            } else {
                if (!g && j.n(this.c) == f11.RUNNING) {
                    j.a(f11.ENQUEUED, this.c);
                }
                n = this.b.n().n(this.c);
            }
            x01.c().a(e, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.c, Boolean.valueOf(n)), new Throwable[0]);
            p.setTransactionSuccessful();
        } finally {
            p.endTransaction();
        }
    }
}
