package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dy3;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oy3 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ dy3<?> f2750a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public a(int i) {
            this.b = i;
        }

        @DexIgnore
        public void onClick(View view) {
            oy3.this.f2750a.O6(hy3.b(this.b, oy3.this.f2750a.I6().d));
            oy3.this.f2750a.P6(dy3.k.DAY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f2751a;

        @DexIgnore
        public b(TextView textView) {
            super(textView);
            this.f2751a = textView;
        }
    }

    @DexIgnore
    public oy3(dy3<?> dy3) {
        this.f2750a = dy3;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f2750a.G6().k();
    }

    @DexIgnore
    public final View.OnClickListener h(int i) {
        return new a(i);
    }

    @DexIgnore
    public int i(int i) {
        return i - this.f2750a.G6().i().e;
    }

    @DexIgnore
    public int j(int i) {
        return this.f2750a.G6().i().e + i;
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(b bVar, int i) {
        int j = j(i);
        String string = bVar.f2751a.getContext().getString(rw3.mtrl_picker_navigate_to_year_description);
        bVar.f2751a.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(j)));
        bVar.f2751a.setContentDescription(String.format(string, Integer.valueOf(j)));
        yx3 H6 = this.f2750a.H6();
        Calendar i2 = ny3.i();
        xx3 xx3 = i2.get(1) == j ? H6.f : H6.d;
        xx3 xx32 = xx3;
        for (Long l : this.f2750a.J6().X()) {
            i2.setTimeInMillis(l.longValue());
            if (i2.get(1) == j) {
                xx32 = H6.e;
            }
        }
        xx32.d(bVar.f2751a);
        bVar.f2751a.setOnClickListener(h(j));
    }

    @DexIgnore
    /* renamed from: l */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(pw3.mtrl_calendar_year, viewGroup, false));
    }
}
