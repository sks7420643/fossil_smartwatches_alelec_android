package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface nd1<T> {
    @DexIgnore
    int a();

    @DexIgnore
    int b(T t);

    @DexIgnore
    String getTag();

    @DexIgnore
    T newArray(int i);
}
