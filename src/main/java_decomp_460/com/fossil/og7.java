package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class og7 extends Enum<og7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ og7 f2678a; // = new og7("PAGE_VIEW", 0, 1);
    @DexIgnore
    public static /* final */ og7 b; // = new og7("SESSION_ENV", 1, 2);
    @DexIgnore
    public static /* final */ og7 c; // = new og7("ERROR", 2, 3);
    @DexIgnore
    public static /* final */ og7 d; // = new og7("CUSTOM", 3, 1000);
    @DexIgnore
    public static /* final */ og7 e; // = new og7("ADDITION", 4, 1001);
    @DexIgnore
    public static /* final */ og7 f; // = new og7("MONITOR_STAT", 5, 1002);
    @DexIgnore
    public static /* final */ og7 g; // = new og7("MTA_GAME_USER", 6, 1003);
    @DexIgnore
    public static /* final */ og7 h; // = new og7("NETWORK_MONITOR", 7, 1004);
    @DexIgnore
    public static /* final */ og7 i; // = new og7("NETWORK_DETECTOR", 8, 1005);
    @DexIgnore
    public int j;

    @DexIgnore
    public og7(String str, int i2, int i3) {
        this.j = i3;
    }

    @DexIgnore
    public final int a() {
        return this.j;
    }
}
