package com.fossil;

import android.os.LocaleList;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um0 implements tm0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LocaleList f3612a;

    @DexIgnore
    public um0(LocaleList localeList) {
        this.f3612a = localeList;
    }

    @DexIgnore
    @Override // com.fossil.tm0
    public Object a() {
        return this.f3612a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this.f3612a.equals(((tm0) obj).a());
    }

    @DexIgnore
    @Override // com.fossil.tm0
    public Locale get(int i) {
        return this.f3612a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3612a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.f3612a.toString();
    }
}
