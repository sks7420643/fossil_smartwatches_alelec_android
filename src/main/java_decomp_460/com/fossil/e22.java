package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class e22 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f22 f871a;

    @DexIgnore
    public e22(f22 f22) {
        this.f871a = f22;
    }

    @DexIgnore
    public static s32.a b(f22 f22) {
        return new e22(f22);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return f22.b(this.f871a);
    }
}
