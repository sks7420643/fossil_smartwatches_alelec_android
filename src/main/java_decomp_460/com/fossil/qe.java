package com.fossil;

import java.util.ArrayList;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ nd0<lp> f2965a; // = new nd0<>(le.b);
    @DexIgnore
    public /* final */ nd0<lp> b; // = new nd0<>(me.b);
    @DexIgnore
    public Hashtable<lp, el> c; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public nw e;

    @DexIgnore
    public qe(nw nwVar) {
        this.e = nwVar;
    }

    @DexIgnore
    public final ArrayList<lp> a() {
        ArrayList<lp> arrayList;
        synchronized (this.d) {
            arrayList = new ArrayList<>();
            arrayList.addAll(this.f2965a);
            arrayList.addAll(this.b);
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(zq zqVar, rp7<? super lp, Boolean> rp7) {
        for (lp lpVar : this.b) {
            pq7.b(lpVar, "phase");
            if (rp7.invoke(lpVar).booleanValue()) {
                lpVar.k(zqVar);
            }
        }
    }

    @DexIgnore
    public final void c(zq zqVar, yp[] ypVarArr) {
        oe oeVar = new oe(ypVarArr);
        b(zqVar, oeVar);
        for (lp lpVar : this.f2965a) {
            pq7.b(lpVar, "phase");
            if (oeVar.invoke(lpVar).booleanValue()) {
                lpVar.k(zqVar);
                this.f2965a.remove(lpVar);
            }
        }
    }

    @DexIgnore
    public final void d(nw nwVar) {
        synchronized (this.d) {
            this.e = nwVar;
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final void e(zq zqVar, yp[] ypVarArr) {
        b(zqVar, new pe(ypVarArr));
    }

    @DexIgnore
    public final lp[] f() {
        lp[] lpVarArr;
        synchronized (this.d) {
            lp[] array = this.b.toArray(new lp[0]);
            if (array != null) {
                lpVarArr = array;
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return lpVarArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0078, code lost:
        if (r15.e.a(r0) != false) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x014a, code lost:
        if (r15.e.a(r0) != false) goto L_0x007a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g() {
        /*
        // Method dump skipped, instructions count: 410
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qe.g():void");
    }
}
