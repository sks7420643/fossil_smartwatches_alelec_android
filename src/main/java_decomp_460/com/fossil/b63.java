package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b63 implements c63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f398a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.client.firebase_feature_rollout.v1.enable", true);

    @DexIgnore
    @Override // com.fossil.c63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.c63
    public final boolean zzb() {
        return f398a.o().booleanValue();
    }
}
