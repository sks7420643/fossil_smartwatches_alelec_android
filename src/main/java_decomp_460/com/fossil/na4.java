package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na4 extends ta4.d.AbstractC0224d.a.b.e {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2491a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.b.e.AbstractC0232a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2492a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> c;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0232a
        public ta4.d.AbstractC0224d.a.b.e a() {
            String str = "";
            if (this.f2492a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " importance";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (str.isEmpty()) {
                return new na4(this.f2492a, this.b.intValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0232a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0232a b(ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> ua4) {
            if (ua4 != null) {
                this.c = ua4;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0232a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0232a c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e.AbstractC0232a
        public ta4.d.AbstractC0224d.a.b.e.AbstractC0232a d(String str) {
            if (str != null) {
                this.f2492a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }
    }

    @DexIgnore
    public na4(String str, int i, ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> ua4) {
        this.f2491a = str;
        this.b = i;
        this.c = ua4;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e
    public ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e
    public int c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.e
    public String d() {
        return this.f2491a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a.b.e)) {
            return false;
        }
        ta4.d.AbstractC0224d.a.b.e eVar = (ta4.d.AbstractC0224d.a.b.e) obj;
        return this.f2491a.equals(eVar.d()) && this.b == eVar.c() && this.c.equals(eVar.b());
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.f2491a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Thread{name=" + this.f2491a + ", importance=" + this.b + ", frames=" + this.c + "}";
    }
}
