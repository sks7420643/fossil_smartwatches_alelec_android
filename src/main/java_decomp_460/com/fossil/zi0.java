package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zi0<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    @DexIgnore
    public fj0<K, V> i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends fj0<K, V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void a() {
            zi0.this.clear();
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public Object b(int i, int i2) {
            return zi0.this.c[(i << 1) + i2];
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public Map<K, V> c() {
            return zi0.this;
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int d() {
            return zi0.this.d;
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int e(Object obj) {
            return zi0.this.g(obj);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int f(Object obj) {
            return zi0.this.i(obj);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void g(K k, V v) {
            zi0.this.put(k, v);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void h(int i) {
            zi0.this.l(i);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public V i(int i, V v) {
            return (V) zi0.this.m(i, v);
        }
    }

    @DexIgnore
    public zi0() {
    }

    @DexIgnore
    public zi0(int i2) {
        super(i2);
    }

    @DexIgnore
    public zi0(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        return o().l();
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<K> keySet() {
        return o().m();
    }

    @DexIgnore
    public final fj0<K, V> o() {
        if (this.i == null) {
            this.i = new a();
        }
        return this.i;
    }

    @DexIgnore
    public boolean p(Collection<?> collection) {
        return fj0.p(this, collection);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.zi0<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        c(this.d + map.size());
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map
    public Collection<V> values() {
        return o().n();
    }
}
