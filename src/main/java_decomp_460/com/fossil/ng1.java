package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ng1 implements qb1<InputStream, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xf1 f2514a; // = new xf1();

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(InputStream inputStream, int i, int i2, ob1 ob1) throws IOException {
        return this.f2514a.b(ImageDecoder.createSource(zj1.b(inputStream)), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(InputStream inputStream, ob1 ob1) throws IOException {
        return true;
    }
}
