package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ys7 extends xs7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ts7<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Iterator f4363a;

        @DexIgnore
        public a(Iterator it) {
            this.f4363a = it;
        }

        @DexIgnore
        @Override // com.fossil.ts7
        public Iterator<T> iterator() {
            return this.f4363a;
        }
    }

    @DexIgnore
    public static final <T> ts7<T> b(Iterator<? extends T> it) {
        pq7.c(it, "$this$asSequence");
        return c(new a(it));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.ts7<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> ts7<T> c(ts7<? extends T> ts7) {
        pq7.c(ts7, "$this$constrainOnce");
        return ts7 instanceof ps7 ? ts7 : new ps7(ts7);
    }

    @DexIgnore
    public static final <T> ts7<T> d(gp7<? extends T> gp7, rp7<? super T, ? extends T> rp7) {
        pq7.c(gp7, "seedFunction");
        pq7.c(rp7, "nextFunction");
        return new rs7(gp7, rp7);
    }
}
