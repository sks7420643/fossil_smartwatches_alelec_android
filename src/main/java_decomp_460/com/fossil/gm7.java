package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gm7 {
    @DexIgnore
    public static final <T> Object[] a(T[] tArr, boolean z) {
        pq7.c(tArr, "$this$copyToArrayOfAny");
        if (z && pq7.a(tArr.getClass(), Object[].class)) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length, Object[].class);
        pq7.b(copyOf, "java.util.Arrays.copyOf(\u2026 Array<Any?>::class.java)");
        return copyOf;
    }

    @DexIgnore
    public static final <T> List<T> b(T t) {
        List<T> singletonList = Collections.singletonList(t);
        pq7.b(singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }
}
