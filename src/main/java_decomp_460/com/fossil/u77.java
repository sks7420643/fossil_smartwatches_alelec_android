package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface u77 {
    @DexIgnore
    void a(l77 l77);

    @DexIgnore
    List<p77> b(l77 l77);

    @DexIgnore
    p77 c(l77 l77, String str);

    @DexIgnore
    Long[] insert(List<p77> list);
}
