package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CompoundButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CompoundButton f3405a;
    @DexIgnore
    public ColorStateList b; // = null;
    @DexIgnore
    public PorterDuff.Mode c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public tg0(CompoundButton compoundButton) {
        this.f3405a = compoundButton;
    }

    @DexIgnore
    public void a() {
        Drawable a2 = ep0.a(this.f3405a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = am0.r(a2).mutate();
            if (this.d) {
                am0.o(mutate, this.b);
            }
            if (this.e) {
                am0.p(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f3405a.getDrawableState());
            }
            this.f3405a.setButtonDrawable(mutate);
        }
    }

    @DexIgnore
    public int b(int i) {
        Drawable a2;
        return (Build.VERSION.SDK_INT >= 17 || (a2 = ep0.a(this.f3405a)) == null) ? i : i + a2.getIntrinsicWidth();
    }

    @DexIgnore
    public ColorStateList c() {
        return this.b;
    }

    @DexIgnore
    public PorterDuff.Mode d() {
        return this.c;
    }

    @DexIgnore
    public void e(AttributeSet attributeSet, int i) {
        int n;
        int n2;
        boolean z = false;
        th0 v = th0.v(this.f3405a.getContext(), attributeSet, ue0.CompoundButton, i, 0);
        CompoundButton compoundButton = this.f3405a;
        mo0.j0(compoundButton, compoundButton.getContext(), ue0.CompoundButton, attributeSet, v.r(), i, 0);
        try {
            if (v.s(ue0.CompoundButton_buttonCompat) && (n2 = v.n(ue0.CompoundButton_buttonCompat, 0)) != 0) {
                try {
                    this.f3405a.setButtonDrawable(gf0.d(this.f3405a.getContext(), n2));
                    z = true;
                } catch (Resources.NotFoundException e2) {
                }
            }
            if (!z && v.s(ue0.CompoundButton_android_button) && (n = v.n(ue0.CompoundButton_android_button, 0)) != 0) {
                this.f3405a.setButtonDrawable(gf0.d(this.f3405a.getContext(), n));
            }
            if (v.s(ue0.CompoundButton_buttonTint)) {
                ep0.c(this.f3405a, v.c(ue0.CompoundButton_buttonTint));
            }
            if (v.s(ue0.CompoundButton_buttonTintMode)) {
                ep0.d(this.f3405a, dh0.e(v.k(ue0.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            v.w();
        }
    }

    @DexIgnore
    public void f() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        a();
    }

    @DexIgnore
    public void g(ColorStateList colorStateList) {
        this.b = colorStateList;
        this.d = true;
        a();
    }

    @DexIgnore
    public void h(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        a();
    }
}
