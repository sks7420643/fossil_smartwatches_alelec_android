package com.fossil;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ jk2 c;

    @DexIgnore
    public kk2(jk2 jk2, Intent intent) {
        this.c = jk2;
        this.b = intent;
    }

    @DexIgnore
    public final void run() {
        String action = this.b.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        this.c.a();
    }
}
