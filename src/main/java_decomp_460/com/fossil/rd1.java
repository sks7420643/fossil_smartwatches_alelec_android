package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rd1 {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    Bitmap e(int i, int i2, Bitmap.Config config);
}
