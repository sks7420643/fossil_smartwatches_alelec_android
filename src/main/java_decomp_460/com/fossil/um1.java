package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int g; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<um1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final um1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new um1(hy1.n(order.getShort(0)), hy1.n(order.getShort(2)), hy1.n(order.getShort(4)), hy1.n(order.getShort(6)));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public um1 b(Parcel parcel) {
            return new um1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public um1 createFromParcel(Parcel parcel) {
            return new um1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public um1[] newArray(int i) {
            return new um1[i];
        }
    }

    @DexIgnore
    public um1(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        super(zm1.DAILY_SLEEP);
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
        d();
    }

    @DexIgnore
    public /* synthetic */ um1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).putShort((short) this.f).array();
        pq7.b(array, "ByteBuffer.allocate(Dail\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(g80.k(g80.k(g80.k(jSONObject, jd0.P1, Integer.valueOf(this.c)), jd0.Q1, Integer.valueOf(this.d)), jd0.R1, Integer.valueOf(this.e)), jd0.S1, Integer.valueOf(this.f));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        int i = g;
        int i2 = this.c;
        if (i2 >= 0 && i >= i2) {
            int i3 = g;
            int i4 = this.d;
            if (i4 >= 0 && i3 >= i4) {
                int i5 = g;
                int i6 = this.e;
                if (i6 >= 0 && i5 >= i6) {
                    int i7 = g;
                    int i8 = this.f;
                    if (i8 < 0 || i7 < i8) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder e2 = e.e("deepSleepInMinute (");
                        e2.append(this.f);
                        e2.append(") is out of range ");
                        e2.append("[0, ");
                        throw new IllegalArgumentException(e.b(e2, g, "]."));
                    }
                    return;
                }
                StringBuilder e3 = e.e("lightSleepInMinute (");
                e3.append(this.e);
                e3.append(") is out of range ");
                e3.append("[0, ");
                throw new IllegalArgumentException(e.b(e3, g, "]."));
            }
            StringBuilder e4 = e.e("awakeInMinute (");
            e4.append(this.d);
            e4.append(") is out of range ");
            e4.append("[0, ");
            throw new IllegalArgumentException(e.b(e4, g, "]."));
        }
        StringBuilder e5 = e.e("totalSleepInMinute (");
        e5.append(this.c);
        e5.append(") is out of range ");
        e5.append("[0, ");
        throw new IllegalArgumentException(e.b(e5, g, "]."));
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(um1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            um1 um1 = (um1) obj;
            if (this.c != um1.c) {
                return false;
            }
            if (this.d != um1.d) {
                return false;
            }
            if (this.e != um1.e) {
                return false;
            }
            return this.f == um1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailySleepConfig");
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.d;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.f;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.e;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (((((this.c * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
