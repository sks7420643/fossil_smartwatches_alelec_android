package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi4 {
    @DexIgnore
    public static /* final */ Pattern d; // = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1483a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public hi4(String str, String str2) {
        this.f1483a = d(str2, str);
        this.b = str;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("!");
        sb.append(str2);
        this.c = sb.toString();
    }

    @DexIgnore
    public static hi4 a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] split = str.split("!", -1);
        if (split.length == 2) {
            return new hi4(split[0], split[1]);
        }
        return null;
    }

    @DexIgnore
    public static String d(String str, String str2) {
        if (str != null && str.startsWith("/topics/")) {
            Log.w("FirebaseMessaging", String.format("Format /topics/topic-name is deprecated. Only 'topic-name' should be used in %s.", str2));
            str = str.substring(8);
        }
        if (str != null && d.matcher(str).matches()) {
            return str;
        }
        throw new IllegalArgumentException(String.format("Invalid topic name: %s does not match the allowed format %s.", str, "[a-zA-Z0-9-_.~%]{1,900}"));
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.f1483a;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof hi4)) {
            return false;
        }
        hi4 hi4 = (hi4) obj;
        return this.f1483a.equals(hi4.f1483a) && this.b.equals(hi4.b);
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(this.b, this.f1483a);
    }
}
