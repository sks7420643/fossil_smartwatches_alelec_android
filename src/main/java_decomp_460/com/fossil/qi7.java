package com.fossil;

import android.content.Context;
import com.facebook.internal.ServerProtocol;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qi7 {
    @DexIgnore
    public static th7 e; // = ei7.p();
    @DexIgnore
    public static qi7 f; // = null;
    @DexIgnore
    public static Context g; // = null;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public DefaultHttpClient f2991a; // = null;
    @DexIgnore
    public yh7 b; // = null;
    @DexIgnore
    public StringBuilder c; // = new StringBuilder(4096);
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public qi7(Context context) {
        try {
            g = context.getApplicationContext();
            this.d = System.currentTimeMillis() / 1000;
            this.b = new yh7();
            if (fg7.K()) {
                try {
                    Logger.getLogger("org.apache.http.wire").setLevel(Level.FINER);
                    Logger.getLogger("org.apache.http.headers").setLevel(Level.FINER);
                    System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
                    System.setProperty("org.apache.commons.logging.simplelog.showdatetime", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                    System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");
                } catch (Throwable th) {
                }
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            this.f2991a = defaultHttpClient;
            defaultHttpClient.setKeepAliveStrategy(new ri7(this));
        } catch (Throwable th2) {
            e.e(th2);
        }
    }

    @DexIgnore
    public static Context a() {
        return g;
    }

    @DexIgnore
    public static void b(Context context) {
        g = context.getApplicationContext();
    }

    @DexIgnore
    public static qi7 f(Context context) {
        if (f == null) {
            synchronized (qi7.class) {
                try {
                    if (f == null) {
                        f = new qi7(context);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void c(ng7 ng7, pi7 pi7) {
        g(Arrays.asList(ng7.h()), pi7);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02fc, code lost:
        if (r12 != null) goto L_0x02d6;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(java.util.List<?> r11, com.fossil.pi7 r12) {
        /*
        // Method dump skipped, instructions count: 779
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qi7.d(java.util.List, com.fossil.pi7):void");
    }

    @DexIgnore
    public final void e(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("mid");
            if (se7.g(optString)) {
                if (fg7.K()) {
                    th7 th7 = e;
                    th7.h("update mid:" + optString);
                }
                re7.a(g).b(optString);
            }
            if (!jSONObject.isNull("cfg")) {
                fg7.i(g, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (fg7.K()) {
                    th7 th72 = e;
                    th72.h("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                ei7.V(g);
                ei7.l(g, currentTimeMillis);
            }
        } catch (Throwable th) {
            e.l(th);
        }
    }

    @DexIgnore
    public void g(List<?> list, pi7 pi7) {
        yh7 yh7 = this.b;
        if (yh7 != null) {
            yh7.a(new si7(this, list, pi7));
        }
    }
}
