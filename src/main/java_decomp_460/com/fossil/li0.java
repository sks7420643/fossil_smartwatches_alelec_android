package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class li0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd0 f2198a;

    @DexIgnore
    public li0(rd0 rd0) {
        this.f2198a = rd0;
    }

    @DexIgnore
    public IBinder a() {
        return this.f2198a.asBinder();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof li0)) {
            return false;
        }
        return ((li0) obj).a().equals(this.f2198a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }
}
