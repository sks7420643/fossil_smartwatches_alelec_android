package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class jg4 implements ht3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ScheduledFuture f1757a;

    @DexIgnore
    public jg4(ScheduledFuture scheduledFuture) {
        this.f1757a = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3 nt3) {
        this.f1757a.cancel(false);
    }
}
