package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wy2<E> extends ay2<E> {
    @DexIgnore
    public static /* final */ wy2<Object> zza; // = new wy2<>(new Object[0], 0, null, 0, 0);
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient Object[] e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public /* final */ transient int g;
    @DexIgnore
    public /* final */ transient int h;

    @DexIgnore
    public wy2(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.d = objArr;
        this.e = objArr2;
        this.f = i2;
        this.g = i;
        this.h = i3;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean contains(@NullableDecl Object obj) {
        Object[] objArr = this.e;
        if (obj == null || objArr == null) {
            return false;
        }
        int b = qx2.b(obj);
        while (true) {
            int i = b & this.f;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            b = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final int hashCode() {
        return this.g;
    }

    @DexIgnore
    public final int size() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.d, 0, objArr, i, this.h);
        return this.h + i;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    /* renamed from: zzb */
    public final cz2<E> iterator() {
        return (cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final sx2<E> zzd() {
        return sx2.zza(this.d, this.h);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final Object[] zze() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzg() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return false;
    }
}
