package com.fossil;

import android.os.RemoteException;
import com.fossil.l72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka2 extends v92<Void> {
    @DexIgnore
    public /* final */ u92 c;

    @DexIgnore
    public ka2(u92 u92, ot3<Void> ot3) {
        super(3, ot3);
        this.c = u92;
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final /* bridge */ /* synthetic */ void d(a82 a82, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final b62[] g(l72.a<?> aVar) {
        return this.c.f3556a.c();
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final boolean h(l72.a<?> aVar) {
        return this.c.f3556a.e();
    }

    @DexIgnore
    @Override // com.fossil.v92
    public final void i(l72.a<?> aVar) throws RemoteException {
        this.c.f3556a.d(aVar.R(), this.b);
        if (this.c.f3556a.b() != null) {
            aVar.A().put(this.c.f3556a.b(), this.c);
        }
    }
}
