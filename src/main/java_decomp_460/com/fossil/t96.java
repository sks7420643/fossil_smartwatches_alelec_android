package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t96 implements Factory<s96> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<k97> f3384a;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> b;

    @DexIgnore
    public t96(Provider<k97> provider, Provider<RingStyleRepository> provider2) {
        this.f3384a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static t96 a(Provider<k97> provider, Provider<RingStyleRepository> provider2) {
        return new t96(provider, provider2);
    }

    @DexIgnore
    public static s96 c(k97 k97, RingStyleRepository ringStyleRepository) {
        return new s96(k97, ringStyleRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public s96 get() {
        return c(this.f3384a.get(), this.b.get());
    }
}
