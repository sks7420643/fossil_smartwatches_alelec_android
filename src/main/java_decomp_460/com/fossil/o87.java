package com.fossil;

import android.graphics.Color;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum o87 {
    BLACK(Color.parseColor("#000000")),
    WHITE(Color.parseColor("#FFFFFF")),
    LIGHT_GRAY(Color.parseColor("#AAAAAA")),
    DARK_GRAY(Color.parseColor("#555555"));
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public static /* final */ o87 c; // = WHITE;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final o87 a() {
            return o87.c;
        }
    }

    @DexIgnore
    public o87(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final nb7 toWFThemeColour() {
        int i = p87.f2801a[ordinal()];
        if (i == 1) {
            return nb7.BLACK;
        }
        if (i == 2) {
            return nb7.WHITE;
        }
        if (i == 3) {
            return nb7.LIGHT_GREY;
        }
        if (i == 4) {
            return nb7.DARK_GREY;
        }
        throw new al7();
    }
}
