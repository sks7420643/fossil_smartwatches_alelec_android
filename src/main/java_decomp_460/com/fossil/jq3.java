package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq3 extends lm3 {
    @DexIgnore
    public Handler c;
    @DexIgnore
    public /* final */ sq3 d; // = new sq3(this);
    @DexIgnore
    public /* final */ qq3 e; // = new qq3(this);
    @DexIgnore
    public /* final */ pq3 f; // = new pq3(this);

    @DexIgnore
    public jq3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    @Override // com.fossil.lm3
    public final boolean A() {
        return false;
    }

    @DexIgnore
    public final long B(long j) {
        return this.e.g(j);
    }

    @DexIgnore
    public final boolean E(boolean z, boolean z2, long j) {
        return this.e.d(z, z2, j);
    }

    @DexIgnore
    public final void F() {
        h();
        if (this.c == null) {
            this.c = new e93(Looper.getMainLooper());
        }
    }

    @DexIgnore
    public final void H(long j) {
        h();
        F();
        d().N().b("Activity resumed, time", Long.valueOf(j));
        if (m().s(xg3.D0)) {
            if (m().K().booleanValue() || l().w.b()) {
                this.e.b(j);
            }
            this.f.a();
        } else {
            this.f.a();
            if (m().K().booleanValue()) {
                this.e.b(j);
            }
        }
        sq3 sq3 = this.d;
        sq3.f3287a.h();
        if (sq3.f3287a.f1780a.o()) {
            if (!sq3.f3287a.m().s(xg3.D0)) {
                sq3.f3287a.l().w.a(false);
            }
            sq3.b(sq3.f3287a.zzm().b(), false);
        }
    }

    @DexIgnore
    public final void J(long j) {
        h();
        F();
        d().N().b("Activity paused, time", Long.valueOf(j));
        this.f.b(j);
        if (m().K().booleanValue()) {
            this.e.f(j);
        }
        sq3 sq3 = this.d;
        if (!sq3.f3287a.m().s(xg3.D0)) {
            sq3.f3287a.l().w.a(true);
        }
    }
}
