package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l5 extends qq7 implements rp7<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ l5 b; // = new l5();

    @DexIgnore
    public l5() {
        super(1);
    }

    @DexIgnore
    /* renamed from: a */
    public final String invoke(BluetoothGattService bluetoothGattService) {
        StringBuilder sb = new StringBuilder();
        pq7.b(bluetoothGattService, Constants.SERVICE);
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        pq7.b(characteristics, "service.characteristics");
        sb.append(pm7.N(characteristics, "\n", null, null, 0, null, j5.b, 30, null));
        return sb.toString();
    }
}
