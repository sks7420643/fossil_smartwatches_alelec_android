package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pd1 implements yd1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f2817a; // = new b();
    @DexIgnore
    public /* final */ ud1<a, Bitmap> b; // = new ud1<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements zd1 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ b f2818a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public Bitmap.Config d;

        @DexIgnore
        public a(b bVar) {
            this.f2818a = bVar;
        }

        @DexIgnore
        @Override // com.fossil.zd1
        public void a() {
            this.f2818a.c(this);
        }

        @DexIgnore
        public void b(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b;
            int i2 = this.c;
            Bitmap.Config config = this.d;
            return (config != null ? config.hashCode() : 0) + (((i * 31) + i2) * 31);
        }

        @DexIgnore
        public String toString() {
            return pd1.f(this.b, this.c, this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends qd1<a> {
        @DexIgnore
        /* renamed from: d */
        public a a() {
            return new a(this);
        }

        @DexIgnore
        public a e(int i, int i2, Bitmap.Config config) {
            a aVar = (a) b();
            aVar.b(i, i2, config);
            return aVar;
        }
    }

    @DexIgnore
    public static String f(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    @DexIgnore
    public static String g(Bitmap bitmap) {
        return f(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public String a(int i, int i2, Bitmap.Config config) {
        return f(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public void b(Bitmap bitmap) {
        this.b.d(this.f2817a.e(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.f2817a.e(i, i2, config));
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public String d(Bitmap bitmap) {
        return g(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public int e(Bitmap bitmap) {
        return jk1.h(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.yd1
    public Bitmap removeLast() {
        return this.b.f();
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }
}
