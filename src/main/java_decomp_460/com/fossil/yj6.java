package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ sj6 f4326a;
    @DexIgnore
    public /* final */ jk6 b;
    @DexIgnore
    public /* final */ dk6 c;

    @DexIgnore
    public yj6(sj6 sj6, jk6 jk6, dk6 dk6) {
        pq7.c(sj6, "mSleepOverviewDayView");
        pq7.c(jk6, "mSleepOverviewWeekView");
        pq7.c(dk6, "mSleepOverviewMonthView");
        this.f4326a = sj6;
        this.b = jk6;
        this.c = dk6;
    }

    @DexIgnore
    public final sj6 a() {
        return this.f4326a;
    }

    @DexIgnore
    public final dk6 b() {
        return this.c;
    }

    @DexIgnore
    public final jk6 c() {
        return this.b;
    }
}
