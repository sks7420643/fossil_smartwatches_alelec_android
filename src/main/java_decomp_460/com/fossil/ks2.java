package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks2 extends as2 implements is2 {
    @DexIgnore
    public ks2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.ICircleDelegate");
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final int a() throws RemoteException {
        Parcel e = e(18, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void b(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final boolean b0(is2 is2) throws RemoteException {
        Parcel d = d();
        es2.c(d, is2);
        Parcel e = e(17, d);
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final String getId() throws RemoteException {
        Parcel e = e(2, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void remove() throws RemoteException {
        i(1, d());
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setCenter(LatLng latLng) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLng);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setFillColor(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(11, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setRadius(double d) throws RemoteException {
        Parcel d2 = d();
        d2.writeDouble(d);
        i(5, d2);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setStrokeColor(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setStrokeWidth(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(15, d);
    }

    @DexIgnore
    @Override // com.fossil.is2
    public final void setZIndex(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(13, d);
    }
}
