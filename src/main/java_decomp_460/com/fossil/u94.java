package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface u94 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    String b();

    @DexIgnore
    byte[] c();

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    void e(long j, String str);
}
