package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp4 implements Factory<oq4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3142a;
    @DexIgnore
    public /* final */ Provider<on5> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<yy6> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> f;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> g;
    @DexIgnore
    public /* final */ Provider<DeviceDao> h;
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> i;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> j;
    @DexIgnore
    public /* final */ Provider<mj5> k;
    @DexIgnore
    public /* final */ Provider<pr4> l;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> m;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<FileRepository> o;
    @DexIgnore
    public /* final */ Provider<k97> p;
    @DexIgnore
    public /* final */ Provider<uo5> q;
    @DexIgnore
    public /* final */ Provider<s77> r;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> s;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> t;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> u;

    @DexIgnore
    public rp4(uo4 uo4, Provider<on5> provider, Provider<UserRepository> provider2, Provider<yy6> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<mj5> provider10, Provider<pr4> provider11, Provider<DianaPresetRepository> provider12, Provider<WatchFaceRepository> provider13, Provider<FileRepository> provider14, Provider<k97> provider15, Provider<uo5> provider16, Provider<s77> provider17, Provider<DeviceRepository> provider18, Provider<DianaAppSettingRepository> provider19, Provider<DianaWatchFaceRepository> provider20) {
        this.f3142a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
        this.s = provider18;
        this.t = provider19;
        this.u = provider20;
    }

    @DexIgnore
    public static rp4 a(uo4 uo4, Provider<on5> provider, Provider<UserRepository> provider2, Provider<yy6> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<mj5> provider10, Provider<pr4> provider11, Provider<DianaPresetRepository> provider12, Provider<WatchFaceRepository> provider13, Provider<FileRepository> provider14, Provider<k97> provider15, Provider<uo5> provider16, Provider<s77> provider17, Provider<DeviceRepository> provider18, Provider<DianaAppSettingRepository> provider19, Provider<DianaWatchFaceRepository> provider20) {
        return new rp4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    public static oq4 c(uo4 uo4, on5 on5, UserRepository userRepository, yy6 yy6, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, mj5 mj5, pr4 pr4, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, k97 k97, uo5 uo5, s77 s77, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        oq4 y = uo4.y(on5, userRepository, yy6, notificationsRepository, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, mj5, pr4, dianaPresetRepository, watchFaceRepository, fileRepository, k97, uo5, s77, deviceRepository, dianaAppSettingRepository, dianaWatchFaceRepository);
        lk7.c(y, "Cannot return null from a non-@Nullable @Provides method");
        return y;
    }

    @DexIgnore
    /* renamed from: b */
    public oq4 get() {
        return c(this.f3142a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get(), this.s.get(), this.t.get(), this.u.get());
    }
}
