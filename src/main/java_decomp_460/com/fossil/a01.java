package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.rl0;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a01 extends zz0 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode k; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public h c;
    @DexIgnore
    public PorterDuffColorFilter d;
    @DexIgnore
    public ColorFilter e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ float[] h;
    @DexIgnore
    public /* final */ Matrix i;
    @DexIgnore
    public /* final */ Rect j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(b bVar) {
            super(bVar);
        }

        @DexIgnore
        @Override // com.fossil.a01.f
        public boolean c() {
            return true;
        }

        @DexIgnore
        public void e(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (ol0.j(xmlPullParser, "pathData")) {
                TypedArray k = ol0.k(resources, theme, attributeSet, sz0.d);
                f(k, xmlPullParser);
                k.recycle();
            }
        }

        @DexIgnore
        public final void f(TypedArray typedArray, XmlPullParser xmlPullParser) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.b = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f171a = rl0.d(string2);
            }
            this.c = ol0.g(typedArray, xmlPullParser, "fillType", 2, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f {
        @DexIgnore
        public int[] e;
        @DexIgnore
        public jl0 f;
        @DexIgnore
        public float g; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public jl0 h;
        @DexIgnore
        public float i; // = 1.0f;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float l; // = 1.0f;
        @DexIgnore
        public float m; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public Paint.Cap n; // = Paint.Cap.BUTT;
        @DexIgnore
        public Paint.Join o; // = Paint.Join.MITER;
        @DexIgnore
        public float p; // = 4.0f;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public c(c cVar) {
            super(cVar);
            this.e = cVar.e;
            this.f = cVar.f;
            this.g = cVar.g;
            this.i = cVar.i;
            this.h = cVar.h;
            this.c = cVar.c;
            this.j = cVar.j;
            this.k = cVar.k;
            this.l = cVar.l;
            this.m = cVar.m;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
        }

        @DexIgnore
        @Override // com.fossil.a01.e
        public boolean a() {
            return this.h.i() || this.f.i();
        }

        @DexIgnore
        @Override // com.fossil.a01.e
        public boolean b(int[] iArr) {
            return this.h.j(iArr) | this.f.j(iArr);
        }

        @DexIgnore
        public final Paint.Cap e(int i2, Paint.Cap cap) {
            return i2 != 0 ? i2 != 1 ? i2 != 2 ? cap : Paint.Cap.SQUARE : Paint.Cap.ROUND : Paint.Cap.BUTT;
        }

        @DexIgnore
        public final Paint.Join f(int i2, Paint.Join join) {
            return i2 != 0 ? i2 != 1 ? i2 != 2 ? join : Paint.Join.BEVEL : Paint.Join.ROUND : Paint.Join.MITER;
        }

        @DexIgnore
        public void g(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray k2 = ol0.k(resources, theme, attributeSet, sz0.c);
            h(k2, xmlPullParser, theme);
            k2.recycle();
        }

        @DexIgnore
        public float getFillAlpha() {
            return this.j;
        }

        @DexIgnore
        public int getFillColor() {
            return this.h.e();
        }

        @DexIgnore
        public float getStrokeAlpha() {
            return this.i;
        }

        @DexIgnore
        public int getStrokeColor() {
            return this.f.e();
        }

        @DexIgnore
        public float getStrokeWidth() {
            return this.g;
        }

        @DexIgnore
        public float getTrimPathEnd() {
            return this.l;
        }

        @DexIgnore
        public float getTrimPathOffset() {
            return this.m;
        }

        @DexIgnore
        public float getTrimPathStart() {
            return this.k;
        }

        @DexIgnore
        public final void h(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) {
            this.e = null;
            if (ol0.j(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.b = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f171a = rl0.d(string2);
                }
                this.h = ol0.e(typedArray, xmlPullParser, theme, "fillColor", 1, 0);
                this.j = ol0.f(typedArray, xmlPullParser, "fillAlpha", 12, this.j);
                this.n = e(ol0.g(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
                this.o = f(ol0.g(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
                this.p = ol0.f(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
                this.f = ol0.e(typedArray, xmlPullParser, theme, "strokeColor", 3, 0);
                this.i = ol0.f(typedArray, xmlPullParser, "strokeAlpha", 11, this.i);
                this.g = ol0.f(typedArray, xmlPullParser, "strokeWidth", 4, this.g);
                this.l = ol0.f(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
                this.m = ol0.f(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
                this.k = ol0.f(typedArray, xmlPullParser, "trimPathStart", 5, this.k);
                this.c = ol0.g(typedArray, xmlPullParser, "fillType", 13, this.c);
            }
        }

        @DexIgnore
        public void setFillAlpha(float f2) {
            this.j = f2;
        }

        @DexIgnore
        public void setFillColor(int i2) {
            this.h.k(i2);
        }

        @DexIgnore
        public void setStrokeAlpha(float f2) {
            this.i = f2;
        }

        @DexIgnore
        public void setStrokeColor(int i2) {
            this.f.k(i2);
        }

        @DexIgnore
        public void setStrokeWidth(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public void setTrimPathEnd(float f2) {
            this.l = f2;
        }

        @DexIgnore
        public void setTrimPathOffset(float f2) {
            this.m = f2;
        }

        @DexIgnore
        public void setTrimPathStart(float f2) {
            this.k = f2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Matrix f170a; // = new Matrix();
        @DexIgnore
        public /* final */ ArrayList<e> b; // = new ArrayList<>();
        @DexIgnore
        public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float d; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float e; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float f; // = 1.0f;
        @DexIgnore
        public float g; // = 1.0f;
        @DexIgnore
        public float h; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float i; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public /* final */ Matrix j; // = new Matrix();
        @DexIgnore
        public int k;
        @DexIgnore
        public int[] l;
        @DexIgnore
        public String m; // = null;

        @DexIgnore
        public d() {
            super();
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r7v0, types: [com.fossil.zi0<java.lang.String, java.lang.Object>, com.fossil.zi0, androidx.collection.SimpleArrayMap] */
        /* JADX WARN: Type inference failed for: r1v10, types: [com.fossil.a01$c] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public d(com.fossil.a01.d r6, com.fossil.zi0<java.lang.String, java.lang.Object> r7) {
            /*
                r5 = this;
                r3 = 0
                r2 = 1065353216(0x3f800000, float:1.0)
                r1 = 0
                r5.<init>()
                android.graphics.Matrix r0 = new android.graphics.Matrix
                r0.<init>()
                r5.f170a = r0
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                r5.b = r0
                r5.c = r1
                r5.d = r1
                r5.e = r1
                r5.f = r2
                r5.g = r2
                r5.h = r1
                r5.i = r1
                android.graphics.Matrix r0 = new android.graphics.Matrix
                r0.<init>()
                r5.j = r0
                r5.m = r3
                float r0 = r6.c
                r5.c = r0
                float r0 = r6.d
                r5.d = r0
                float r0 = r6.e
                r5.e = r0
                float r0 = r6.f
                r5.f = r0
                float r0 = r6.g
                r5.g = r0
                float r0 = r6.h
                r5.h = r0
                float r0 = r6.i
                r5.i = r0
                int[] r0 = r6.l
                r5.l = r0
                java.lang.String r0 = r6.m
                r5.m = r0
                int r1 = r6.k
                r5.k = r1
                if (r0 == 0) goto L_0x0059
                r7.put(r0, r5)
            L_0x0059:
                android.graphics.Matrix r0 = r5.j
                android.graphics.Matrix r1 = r6.j
                r0.set(r1)
                java.util.ArrayList<com.fossil.a01$e> r3 = r6.b
                r0 = 0
                r2 = r0
            L_0x0064:
                int r0 = r3.size()
                if (r2 >= r0) goto L_0x00b0
                java.lang.Object r0 = r3.get(r2)
                boolean r1 = r0 instanceof com.fossil.a01.d
                if (r1 == 0) goto L_0x0082
                com.fossil.a01$d r0 = (com.fossil.a01.d) r0
                java.util.ArrayList<com.fossil.a01$e> r1 = r5.b
                com.fossil.a01$d r4 = new com.fossil.a01$d
                r4.<init>(r0, r7)
                r1.add(r4)
            L_0x007e:
                int r0 = r2 + 1
                r2 = r0
                goto L_0x0064
            L_0x0082:
                boolean r1 = r0 instanceof com.fossil.a01.c
                if (r1 == 0) goto L_0x009b
                com.fossil.a01$c r1 = new com.fossil.a01$c
                com.fossil.a01$c r0 = (com.fossil.a01.c) r0
                r1.<init>(r0)
                r0 = r1
            L_0x008e:
                java.util.ArrayList<com.fossil.a01$e> r1 = r5.b
                r1.add(r0)
                java.lang.String r1 = r0.b
                if (r1 == 0) goto L_0x007e
                r7.put(r1, r0)
                goto L_0x007e
            L_0x009b:
                boolean r1 = r0 instanceof com.fossil.a01.b
                if (r1 == 0) goto L_0x00a8
                com.fossil.a01$b r1 = new com.fossil.a01$b
                com.fossil.a01$b r0 = (com.fossil.a01.b) r0
                r1.<init>(r0)
                r0 = r1
                goto L_0x008e
            L_0x00a8:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "Unknown object in the tree!"
                r0.<init>(r1)
                throw r0
            L_0x00b0:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a01.d.<init>(com.fossil.a01$d, com.fossil.zi0):void");
        }

        @DexIgnore
        @Override // com.fossil.a01.e
        public boolean a() {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.a01.e
        public boolean b(int[] iArr) {
            boolean z = false;
            int i2 = 0;
            while (i2 < this.b.size()) {
                i2++;
                z = this.b.get(i2).b(iArr) | z;
            }
            return z;
        }

        @DexIgnore
        public void c(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray k2 = ol0.k(resources, theme, attributeSet, sz0.b);
            e(k2, xmlPullParser);
            k2.recycle();
        }

        @DexIgnore
        public final void d() {
            this.j.reset();
            this.j.postTranslate(-this.d, -this.e);
            this.j.postScale(this.f, this.g);
            this.j.postRotate(this.c, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.j.postTranslate(this.h + this.d, this.i + this.e);
        }

        @DexIgnore
        public final void e(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.c = ol0.f(typedArray, xmlPullParser, "rotation", 5, this.c);
            this.d = typedArray.getFloat(1, this.d);
            this.e = typedArray.getFloat(2, this.e);
            this.f = ol0.f(typedArray, xmlPullParser, "scaleX", 3, this.f);
            this.g = ol0.f(typedArray, xmlPullParser, "scaleY", 4, this.g);
            this.h = ol0.f(typedArray, xmlPullParser, "translateX", 6, this.h);
            this.i = ol0.f(typedArray, xmlPullParser, "translateY", 7, this.i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            d();
        }

        @DexIgnore
        public String getGroupName() {
            return this.m;
        }

        @DexIgnore
        public Matrix getLocalMatrix() {
            return this.j;
        }

        @DexIgnore
        public float getPivotX() {
            return this.d;
        }

        @DexIgnore
        public float getPivotY() {
            return this.e;
        }

        @DexIgnore
        public float getRotation() {
            return this.c;
        }

        @DexIgnore
        public float getScaleX() {
            return this.f;
        }

        @DexIgnore
        public float getScaleY() {
            return this.g;
        }

        @DexIgnore
        public float getTranslateX() {
            return this.h;
        }

        @DexIgnore
        public float getTranslateY() {
            return this.i;
        }

        @DexIgnore
        public void setPivotX(float f2) {
            if (f2 != this.d) {
                this.d = f2;
                d();
            }
        }

        @DexIgnore
        public void setPivotY(float f2) {
            if (f2 != this.e) {
                this.e = f2;
                d();
            }
        }

        @DexIgnore
        public void setRotation(float f2) {
            if (f2 != this.c) {
                this.c = f2;
                d();
            }
        }

        @DexIgnore
        public void setScaleX(float f2) {
            if (f2 != this.f) {
                this.f = f2;
                d();
            }
        }

        @DexIgnore
        public void setScaleY(float f2) {
            if (f2 != this.g) {
                this.g = f2;
                d();
            }
        }

        @DexIgnore
        public void setTranslateX(float f2) {
            if (f2 != this.h) {
                this.h = f2;
                d();
            }
        }

        @DexIgnore
        public void setTranslateY(float f2) {
            if (f2 != this.i) {
                this.i = f2;
                d();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean b(int[] iArr) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f extends e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public rl0.b[] f171a; // = null;
        @DexIgnore
        public String b;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public int d;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        public f(f fVar) {
            super();
            this.b = fVar.b;
            this.d = fVar.d;
            this.f171a = rl0.f(fVar.f171a);
        }

        @DexIgnore
        public boolean c() {
            return false;
        }

        @DexIgnore
        public void d(Path path) {
            path.reset();
            rl0.b[] bVarArr = this.f171a;
            if (bVarArr != null) {
                rl0.b.e(bVarArr, path);
            }
        }

        @DexIgnore
        public rl0.b[] getPathData() {
            return this.f171a;
        }

        @DexIgnore
        public String getPathName() {
            return this.b;
        }

        @DexIgnore
        public void setPathData(rl0.b[] bVarArr) {
            if (!rl0.b(this.f171a, bVarArr)) {
                this.f171a = rl0.f(bVarArr);
            } else {
                rl0.j(this.f171a, bVarArr);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public static /* final */ Matrix q; // = new Matrix();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Path f172a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ Matrix c;
        @DexIgnore
        public Paint d;
        @DexIgnore
        public Paint e;
        @DexIgnore
        public PathMeasure f;
        @DexIgnore
        public int g;
        @DexIgnore
        public /* final */ d h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public float k;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m;
        @DexIgnore
        public String n;
        @DexIgnore
        public Boolean o;
        @DexIgnore
        public /* final */ zi0<String, Object> p;

        @DexIgnore
        public g() {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new zi0<>();
            this.h = new d();
            this.f172a = new Path();
            this.b = new Path();
        }

        @DexIgnore
        public g(g gVar) {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            zi0<String, Object> zi0 = new zi0<>();
            this.p = zi0;
            this.h = new d(gVar.h, zi0);
            this.f172a = new Path(gVar.f172a);
            this.b = new Path(gVar.b);
            this.i = gVar.i;
            this.j = gVar.j;
            this.k = gVar.k;
            this.l = gVar.l;
            this.g = gVar.g;
            this.m = gVar.m;
            this.n = gVar.n;
            String str = gVar.n;
            if (str != null) {
                this.p.put(str, this);
            }
            this.o = gVar.o;
        }

        @DexIgnore
        public static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        @DexIgnore
        public void b(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            c(this.h, q, canvas, i2, i3, colorFilter);
        }

        @DexIgnore
        public final void c(d dVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            dVar.f170a.set(matrix);
            dVar.f170a.preConcat(dVar.j);
            canvas.save();
            for (int i4 = 0; i4 < dVar.b.size(); i4++) {
                e eVar = dVar.b.get(i4);
                if (eVar instanceof d) {
                    c((d) eVar, dVar.f170a, canvas, i2, i3, colorFilter);
                } else if (eVar instanceof f) {
                    d(dVar, (f) eVar, canvas, i2, i3, colorFilter);
                }
            }
            canvas.restore();
        }

        @DexIgnore
        public final void d(d dVar, f fVar, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.k;
            float f3 = ((float) i3) / this.l;
            float min = Math.min(f2, f3);
            Matrix matrix = dVar.f170a;
            this.c.set(matrix);
            this.c.postScale(f2, f3);
            float e2 = e(matrix);
            if (e2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                fVar.d(this.f172a);
                Path path = this.f172a;
                this.b.reset();
                if (fVar.c()) {
                    this.b.setFillType(fVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    this.b.addPath(path, this.c);
                    canvas.clipPath(this.b);
                    return;
                }
                c cVar = (c) fVar;
                if (!(cVar.k == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && cVar.l == 1.0f)) {
                    float f4 = cVar.k;
                    float f5 = cVar.m;
                    float f6 = cVar.l;
                    if (this.f == null) {
                        this.f = new PathMeasure();
                    }
                    this.f.setPath(this.f172a, false);
                    float length = this.f.getLength();
                    float f7 = ((f4 + f5) % 1.0f) * length;
                    float f8 = ((f5 + f6) % 1.0f) * length;
                    path.reset();
                    if (f7 > f8) {
                        this.f.getSegment(f7, length, path, true);
                        this.f.getSegment(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f8, path, true);
                    } else {
                        this.f.getSegment(f7, f8, path, true);
                    }
                    path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                this.b.addPath(path, this.c);
                if (cVar.h.l()) {
                    jl0 jl0 = cVar.h;
                    if (this.e == null) {
                        Paint paint = new Paint(1);
                        this.e = paint;
                        paint.setStyle(Paint.Style.FILL);
                    }
                    Paint paint2 = this.e;
                    if (jl0.h()) {
                        Shader f9 = jl0.f();
                        f9.setLocalMatrix(this.c);
                        paint2.setShader(f9);
                        paint2.setAlpha(Math.round(cVar.j * 255.0f));
                    } else {
                        paint2.setShader(null);
                        paint2.setAlpha(255);
                        paint2.setColor(a01.a(jl0.e(), cVar.j));
                    }
                    paint2.setColorFilter(colorFilter);
                    this.b.setFillType(cVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.b, paint2);
                }
                if (cVar.f.l()) {
                    jl0 jl02 = cVar.f;
                    if (this.d == null) {
                        Paint paint3 = new Paint(1);
                        this.d = paint3;
                        paint3.setStyle(Paint.Style.STROKE);
                    }
                    Paint paint4 = this.d;
                    Paint.Join join = cVar.o;
                    if (join != null) {
                        paint4.setStrokeJoin(join);
                    }
                    Paint.Cap cap = cVar.n;
                    if (cap != null) {
                        paint4.setStrokeCap(cap);
                    }
                    paint4.setStrokeMiter(cVar.p);
                    if (jl02.h()) {
                        Shader f10 = jl02.f();
                        f10.setLocalMatrix(this.c);
                        paint4.setShader(f10);
                        paint4.setAlpha(Math.round(cVar.i * 255.0f));
                    } else {
                        paint4.setShader(null);
                        paint4.setAlpha(255);
                        paint4.setColor(a01.a(jl02.e(), cVar.i));
                    }
                    paint4.setColorFilter(colorFilter);
                    paint4.setStrokeWidth(cVar.g * e2 * min);
                    canvas.drawPath(this.b, paint4);
                }
            }
        }

        @DexIgnore
        public final float e(Matrix matrix) {
            float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float hypot = (float) Math.hypot((double) fArr[2], (double) fArr[3]);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), hypot);
            return max > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? Math.abs(a2) / max : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }

        @DexIgnore
        public boolean f() {
            if (this.o == null) {
                this.o = Boolean.valueOf(this.h.a());
            }
            return this.o.booleanValue();
        }

        @DexIgnore
        public boolean g(int[] iArr) {
            return this.h.b(iArr);
        }

        @DexIgnore
        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        @DexIgnore
        public int getRootAlpha() {
            return this.m;
        }

        @DexIgnore
        public void setAlpha(float f2) {
            setRootAlpha((int) (255.0f * f2));
        }

        @DexIgnore
        public void setRootAlpha(int i2) {
            this.m = i2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f173a;
        @DexIgnore
        public g b;
        @DexIgnore
        public ColorStateList c;
        @DexIgnore
        public PorterDuff.Mode d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public ColorStateList g;
        @DexIgnore
        public PorterDuff.Mode h;
        @DexIgnore
        public int i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public Paint l;

        @DexIgnore
        public h() {
            this.c = null;
            this.d = a01.k;
            this.b = new g();
        }

        @DexIgnore
        public h(h hVar) {
            this.c = null;
            this.d = a01.k;
            if (hVar != null) {
                this.f173a = hVar.f173a;
                g gVar = new g(hVar.b);
                this.b = gVar;
                if (hVar.b.e != null) {
                    gVar.e = new Paint(hVar.b.e);
                }
                if (hVar.b.d != null) {
                    this.b.d = new Paint(hVar.b.d);
                }
                this.c = hVar.c;
                this.d = hVar.d;
                this.e = hVar.e;
            }
        }

        @DexIgnore
        public boolean a(int i2, int i3) {
            return i2 == this.f.getWidth() && i3 == this.f.getHeight();
        }

        @DexIgnore
        public boolean b() {
            return !this.k && this.g == this.c && this.h == this.d && this.j == this.e && this.i == this.b.getRootAlpha();
        }

        @DexIgnore
        public void c(int i2, int i3) {
            if (this.f == null || !a(i2, i3)) {
                this.f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.k = true;
            }
        }

        @DexIgnore
        public void d(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f, (Rect) null, rect, e(colorFilter));
        }

        @DexIgnore
        public Paint e(ColorFilter colorFilter) {
            if (!f() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                Paint paint = new Paint();
                this.l = paint;
                paint.setFilterBitmap(true);
            }
            this.l.setAlpha(this.b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        @DexIgnore
        public boolean f() {
            return this.b.getRootAlpha() < 255;
        }

        @DexIgnore
        public boolean g() {
            return this.b.f();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f173a;
        }

        @DexIgnore
        public boolean h(int[] iArr) {
            boolean g2 = this.b.g(iArr);
            this.k |= g2;
            return g2;
        }

        @DexIgnore
        public void i() {
            this.g = this.c;
            this.h = this.d;
            this.i = this.b.getRootAlpha();
            this.j = this.e;
            this.k = false;
        }

        @DexIgnore
        public void j(int i2, int i3) {
            this.f.eraseColor(0);
            this.b.b(new Canvas(this.f), i2, i3, null);
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new a01(this);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new a01(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Drawable.ConstantState f174a;

        @DexIgnore
        public i(Drawable.ConstantState constantState) {
            this.f174a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.f174a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f174a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            a01 a01 = new a01();
            a01.b = (VectorDrawable) this.f174a.newDrawable();
            return a01;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            a01 a01 = new a01();
            a01.b = (VectorDrawable) this.f174a.newDrawable(resources);
            return a01;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            a01 a01 = new a01();
            a01.b = (VectorDrawable) this.f174a.newDrawable(resources, theme);
            return a01;
        }
    }

    @DexIgnore
    public a01() {
        this.g = true;
        this.h = new float[9];
        this.i = new Matrix();
        this.j = new Rect();
        this.c = new h();
    }

    @DexIgnore
    public a01(h hVar) {
        this.g = true;
        this.h = new float[9];
        this.i = new Matrix();
        this.j = new Rect();
        this.c = hVar;
        this.d = j(this.d, hVar.c, hVar.d);
    }

    @DexIgnore
    public static int a(int i2, float f2) {
        return (16777215 & i2) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }

    @DexIgnore
    public static a01 b(Resources resources, int i2, Resources.Theme theme) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            a01 a01 = new a01();
            a01.b = nl0.a(resources, i2, theme);
            new i(a01.b.getConstantState());
            return a01;
        }
        try {
            XmlResourceParser xml = resources.getXml(i2);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            do {
                next = xml.next();
                if (next == 2) {
                    break;
                }
            } while (next != 1);
            if (next == 2) {
                return c(resources, xml, asAttributeSet, theme);
            }
            throw new XmlPullParserException("No start tag found");
        } catch (XmlPullParserException e2) {
            Log.e("VectorDrawableCompat", "parser error", e2);
            return null;
        } catch (IOException e3) {
            Log.e("VectorDrawableCompat", "parser error", e3);
            return null;
        }
    }

    @DexIgnore
    public static a01 c(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        a01 a01 = new a01();
        a01.inflate(resources, xmlPullParser, attributeSet, theme);
        return a01;
    }

    @DexIgnore
    public static PorterDuff.Mode g(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.b;
        if (drawable == null) {
            return false;
        }
        am0.b(drawable);
        return false;
    }

    @DexIgnore
    public Object d(String str) {
        return this.c.b.p.get(str);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.j);
        if (this.j.width() > 0 && this.j.height() > 0) {
            ColorFilter colorFilter = this.e;
            if (colorFilter == null) {
                colorFilter = this.d;
            }
            canvas.getMatrix(this.i);
            this.i.getValues(this.h);
            float abs = Math.abs(this.h[0]);
            float abs2 = Math.abs(this.h[4]);
            float abs3 = Math.abs(this.h[1]);
            float abs4 = Math.abs(this.h[3]);
            if (!(abs3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && abs4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                abs2 = 1.0f;
                abs = 1.0f;
            }
            int min = Math.min(2048, (int) (abs * ((float) this.j.width())));
            int min2 = Math.min(2048, (int) (abs2 * ((float) this.j.height())));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                Rect rect = this.j;
                canvas.translate((float) rect.left, (float) rect.top);
                if (f()) {
                    canvas.translate((float) this.j.width(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.j.offsetTo(0, 0);
                this.c.c(min, min2);
                if (!this.g) {
                    this.c.j(min, min2);
                } else if (!this.c.b()) {
                    this.c.j(min, min2);
                    this.c.i();
                }
                this.c.d(canvas, colorFilter, this.j);
                canvas.restoreToCount(save);
            }
        }
    }

    @DexIgnore
    public final void e(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        boolean z;
        h hVar = this.c;
        g gVar = hVar.b;
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(gVar.h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth();
        boolean z2 = true;
        int i2 = eventType;
        while (i2 != 1 && (xmlPullParser.getDepth() >= depth + 1 || i2 != 3)) {
            if (i2 == 2) {
                String name = xmlPullParser.getName();
                d dVar = (d) arrayDeque.peek();
                if ("path".equals(name)) {
                    c cVar = new c();
                    cVar.g(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(cVar);
                    if (cVar.getPathName() != null) {
                        gVar.p.put(cVar.getPathName(), cVar);
                    }
                    z = false;
                    hVar.f173a = cVar.d | hVar.f173a;
                } else if ("clip-path".equals(name)) {
                    b bVar = new b();
                    bVar.e(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(bVar);
                    if (bVar.getPathName() != null) {
                        gVar.p.put(bVar.getPathName(), bVar);
                    }
                    hVar.f173a |= bVar.d;
                    z = z2;
                } else {
                    if ("group".equals(name)) {
                        d dVar2 = new d();
                        dVar2.c(resources, attributeSet, theme, xmlPullParser);
                        dVar.b.add(dVar2);
                        arrayDeque.push(dVar2);
                        if (dVar2.getGroupName() != null) {
                            gVar.p.put(dVar2.getGroupName(), dVar2);
                        }
                        hVar.f173a |= dVar2.k;
                        z = z2;
                    }
                    z = z2;
                }
            } else {
                if (i2 == 3 && "group".equals(xmlPullParser.getName())) {
                    arrayDeque.pop();
                    z = z2;
                }
                z = z2;
            }
            i2 = xmlPullParser.next();
            z2 = z;
        }
        if (z2) {
            throw new XmlPullParserException("no path defined");
        }
    }

    @DexIgnore
    public final boolean f() {
        return Build.VERSION.SDK_INT >= 17 && isAutoMirrored() && am0.f(this) == 1;
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.b;
        return drawable != null ? am0.d(drawable) : this.c.b.getRootAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getChangingConfigurations() : super.getChangingConfigurations() | this.c.getChangingConfigurations();
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        Drawable drawable = this.b;
        return drawable != null ? am0.e(drawable) : this.e;
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        if (this.b != null && Build.VERSION.SDK_INT >= 24) {
            return new i(this.b.getConstantState());
        }
        this.c.f173a = getChangingConfigurations();
        return this.c;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getIntrinsicHeight() : (int) this.c.b.j;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getIntrinsicWidth() : (int) this.c.b.i;
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.b;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    @DexIgnore
    public void h(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public final void i(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException {
        h hVar = this.c;
        g gVar = hVar.b;
        hVar.d = g(ol0.g(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList c2 = ol0.c(typedArray, xmlPullParser, theme, "tint", 1);
        if (c2 != null) {
            hVar.c = c2;
        }
        hVar.e = ol0.a(typedArray, xmlPullParser, "autoMirrored", 5, hVar.e);
        gVar.k = ol0.f(typedArray, xmlPullParser, "viewportWidth", 7, gVar.k);
        float f2 = ol0.f(typedArray, xmlPullParser, "viewportHeight", 8, gVar.l);
        gVar.l = f2;
        if (gVar.k <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            gVar.i = typedArray.getDimension(3, gVar.i);
            float dimension = typedArray.getDimension(2, gVar.j);
            gVar.j = dimension;
            if (gVar.i <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (dimension > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                gVar.setAlpha(ol0.f(typedArray, xmlPullParser, "alpha", 4, gVar.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    gVar.n = string;
                    gVar.p.put(string, gVar);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.g(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        h hVar = this.c;
        hVar.b = new g();
        TypedArray k2 = ol0.k(resources, theme, attributeSet, sz0.f3337a);
        i(k2, xmlPullParser, theme);
        k2.recycle();
        hVar.f173a = getChangingConfigurations();
        hVar.k = true;
        e(resources, xmlPullParser, attributeSet, theme);
        this.d = j(this.d, hVar.c, hVar.d);
    }

    @DexIgnore
    public void invalidateSelf() {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.b;
        return drawable != null ? am0.h(drawable) : this.c.e;
    }

    @DexIgnore
    public boolean isStateful() {
        h hVar;
        ColorStateList colorStateList;
        Drawable drawable = this.b;
        return drawable != null ? drawable.isStateful() : super.isStateful() || ((hVar = this.c) != null && (hVar.g() || ((colorStateList = this.c.c) != null && colorStateList.isStateful())));
    }

    @DexIgnore
    public PorterDuffColorFilter j(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.mutate();
        } else if (!this.f && super.mutate() == this) {
            this.c = new h(this.c);
            this.f = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = this.b;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        h hVar = this.c;
        ColorStateList colorStateList = hVar.c;
        if (!(colorStateList == null || (mode = hVar.d) == null)) {
            this.d = j(this.d, colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        if (!hVar.g() || !hVar.h(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.c.b.getRootAlpha() != i2) {
            this.c.b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.j(drawable, z);
        } else {
            this.c.e = z;
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.e = colorFilter;
        invalidateSelf();
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTint(int i2) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.n(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.o(drawable, colorStateList);
            return;
        }
        h hVar = this.c;
        if (hVar.c != colorStateList) {
            hVar.c = colorStateList;
            this.d = j(this.d, colorStateList, hVar.d);
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.p(drawable, mode);
            return;
        }
        h hVar = this.c;
        if (hVar.d != mode) {
            hVar.d = mode;
            this.d = j(this.d, hVar.c, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.setVisible(z, z2) : super.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }
}
