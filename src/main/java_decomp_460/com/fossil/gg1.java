package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fg1;
import com.fossil.mg1;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gg1 {
    @DexIgnore
    public static /* final */ nb1<hb1> f; // = nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", hb1.DEFAULT);
    @DexIgnore
    public static /* final */ nb1<pb1> g; // = nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace", pb1.SRGB);
    @DexIgnore
    public static /* final */ nb1<Boolean> h; // = nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", Boolean.FALSE);
    @DexIgnore
    public static /* final */ nb1<Boolean> i; // = nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", Boolean.FALSE);
    @DexIgnore
    public static /* final */ Set<String> j; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));
    @DexIgnore
    public static /* final */ b k; // = new a();
    @DexIgnore
    public static /* final */ Set<ImageHeaderParser.ImageType> l; // = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
    @DexIgnore
    public static /* final */ Queue<BitmapFactory.Options> m; // = jk1.f(0);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd1 f1302a;
    @DexIgnore
    public /* final */ DisplayMetrics b;
    @DexIgnore
    public /* final */ od1 c;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> d;
    @DexIgnore
    public /* final */ lg1 e; // = lg1.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        @Override // com.fossil.gg1.b
        public void a(rd1 rd1, Bitmap bitmap) {
        }

        @DexIgnore
        @Override // com.fossil.gg1.b
        public void b() {
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(rd1 rd1, Bitmap bitmap) throws IOException;

        @DexIgnore
        Object b();  // void declaration
    }

    /*
    static {
        nb1<fg1> nb1 = fg1.f;
    }
    */

    @DexIgnore
    public gg1(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, rd1 rd1, od1 od1) {
        this.d = list;
        ik1.d(displayMetrics);
        this.b = displayMetrics;
        ik1.d(rd1);
        this.f1302a = rd1;
        ik1.d(od1);
        this.c = od1;
    }

    @DexIgnore
    public static int a(double d2) {
        int l2 = l(d2);
        int x = x(((double) l2) * d2);
        return x(((double) x) * (d2 / ((double) (((float) x) / ((float) l2)))));
    }

    @DexIgnore
    public static void c(ImageHeaderParser.ImageType imageType, mg1 mg1, b bVar, rd1 rd1, fg1 fg1, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) throws IOException {
        int i7;
        int i8;
        int max;
        int i9;
        double d2;
        int i10;
        if (i3 > 0 && i4 > 0) {
            if (r(i2)) {
                i7 = i4;
                i8 = i3;
            } else {
                i7 = i3;
                i8 = i4;
            }
            float b2 = fg1.b(i7, i8, i5, i6);
            if (b2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                fg1.e a2 = fg1.a(i7, i8, i5, i6);
                if (a2 != null) {
                    float f2 = (float) i7;
                    float f3 = (float) i8;
                    int x = i7 / x((double) (b2 * f2));
                    int x2 = i8 / x((double) (b2 * f3));
                    int max2 = a2 == fg1.e.MEMORY ? Math.max(x, x2) : Math.min(x, x2);
                    if (Build.VERSION.SDK_INT > 23 || !j.contains(options.outMimeType)) {
                        max = Math.max(1, Integer.highestOneBit(max2));
                        if (a2 == fg1.e.MEMORY && ((float) max) < 1.0f / b2) {
                            max <<= 1;
                        }
                    } else {
                        max = 1;
                    }
                    options.inSampleSize = max;
                    if (imageType == ImageHeaderParser.ImageType.JPEG) {
                        float min = (float) Math.min(max, 8);
                        i9 = (int) Math.ceil((double) (f2 / min));
                        i10 = (int) Math.ceil((double) (f3 / min));
                        int i11 = max / 8;
                        if (i11 > 0) {
                            i9 /= i11;
                            i10 /= i11;
                        }
                    } else {
                        if (imageType == ImageHeaderParser.ImageType.PNG || imageType == ImageHeaderParser.ImageType.PNG_A) {
                            float f4 = (float) max;
                            i9 = (int) Math.floor((double) (f2 / f4));
                            d2 = Math.floor((double) (f3 / f4));
                        } else if (imageType == ImageHeaderParser.ImageType.WEBP || imageType == ImageHeaderParser.ImageType.WEBP_A) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                float f5 = (float) max;
                                i9 = Math.round(f2 / f5);
                                i10 = Math.round(f3 / f5);
                            } else {
                                float f6 = (float) max;
                                i9 = (int) Math.floor((double) (f2 / f6));
                                d2 = Math.floor((double) (f3 / f6));
                            }
                        } else if (i7 % max == 0 && i8 % max == 0) {
                            i9 = i7 / max;
                            i10 = i8 / max;
                        } else {
                            int[] m2 = m(mg1, options, bVar, rd1);
                            i9 = m2[0];
                            i10 = m2[1];
                        }
                        i10 = (int) d2;
                    }
                    double b3 = (double) fg1.b(i9, i10, i5, i6);
                    if (Build.VERSION.SDK_INT >= 19) {
                        options.inTargetDensity = a(b3);
                        options.inDensity = l(b3);
                    }
                    if (s(options)) {
                        options.inScaled = true;
                    } else {
                        options.inTargetDensity = 0;
                        options.inDensity = 0;
                    }
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculate scaling, source: [" + i3 + "x" + i4 + "], degreesToRotate: " + i2 + ", target: [" + i5 + "x" + i6 + "], power of two scaled: [" + i9 + "x" + i10 + "], exact scale factor: " + b2 + ", power of 2 sample size: " + max + ", adjusted scale factor: " + b3 + ", target density: " + options.inTargetDensity + ", density: " + options.inDensity);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("Cannot round with null rounding");
            }
            throw new IllegalArgumentException("Cannot scale with factor: " + b2 + " from: " + fg1 + ", source: [" + i3 + "x" + i4 + "], target: [" + i5 + "x" + i6 + "]");
        } else if (Log.isLoggable("Downsampler", 3)) {
            Log.d("Downsampler", "Unable to determine dimensions for: " + imageType + " with target [" + i5 + "x" + i6 + "]");
        }
    }

    @DexIgnore
    public static Bitmap i(mg1 mg1, BitmapFactory.Options options, b bVar, rd1 rd1) throws IOException {
        Bitmap i2;
        if (!options.inJustDecodeBounds) {
            bVar.b();
            mg1.c();
        }
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        String str = options.outMimeType;
        tg1.f().lock();
        try {
            i2 = mg1.b(options);
        } catch (IllegalArgumentException e2) {
            IOException u = u(e2, i3, i4, str, options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", u);
            }
            if (options.inBitmap != null) {
                try {
                    rd1.b(options.inBitmap);
                    options.inBitmap = null;
                    i2 = i(mg1, options, bVar, rd1);
                } catch (IOException e3) {
                    throw u;
                }
            } else {
                throw u;
            }
        } finally {
            tg1.f().unlock();
        }
        return i2;
    }

    @DexIgnore
    @TargetApi(19)
    public static String j(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    @DexIgnore
    public static BitmapFactory.Options k() {
        BitmapFactory.Options poll;
        synchronized (gg1.class) {
            try {
                synchronized (m) {
                    poll = m.poll();
                }
                if (poll == null) {
                    poll = new BitmapFactory.Options();
                    w(poll);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return poll;
    }

    @DexIgnore
    public static int l(double d2) {
        if (d2 > 1.0d) {
            d2 = 1.0d / d2;
        }
        return (int) Math.round(2.147483647E9d * d2);
    }

    @DexIgnore
    public static int[] m(mg1 mg1, BitmapFactory.Options options, b bVar, rd1 rd1) throws IOException {
        options.inJustDecodeBounds = true;
        i(mg1, options, bVar, rd1);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    @DexIgnore
    public static String n(BitmapFactory.Options options) {
        return j(options.inBitmap);
    }

    @DexIgnore
    public static boolean r(int i2) {
        return i2 == 90 || i2 == 270;
    }

    @DexIgnore
    public static boolean s(BitmapFactory.Options options) {
        int i2;
        int i3 = options.inTargetDensity;
        return i3 > 0 && (i2 = options.inDensity) > 0 && i3 != i2;
    }

    @DexIgnore
    public static void t(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        Log.v("Downsampler", "Decoded " + j(bitmap) + " from [" + i2 + "x" + i3 + "] " + str + " with inBitmap " + n(options) + " for [" + i4 + "x" + i5 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + ek1.a(j2));
    }

    @DexIgnore
    public static IOException u(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + n(options), illegalArgumentException);
    }

    @DexIgnore
    public static void v(BitmapFactory.Options options) {
        w(options);
        synchronized (m) {
            m.offer(options);
        }
    }

    @DexIgnore
    public static void w(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    @DexIgnore
    public static int x(double d2) {
        return (int) (0.5d + d2);
    }

    @DexIgnore
    @TargetApi(26)
    public static void y(BitmapFactory.Options options, rd1 rd1, int i2, int i3) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = rd1.e(i2, i3, config);
    }

    @DexIgnore
    public final void b(mg1 mg1, hb1 hb1, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        boolean z3;
        if (!this.e.e(i2, i3, options, z, z2)) {
            if (hb1 == hb1.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            try {
                z3 = mg1.d().hasAlpha();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + hb1, e2);
                    z3 = false;
                } else {
                    z3 = false;
                }
            }
            Bitmap.Config config = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            options.inPreferredConfig = config;
            if (config == Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    @DexIgnore
    public id1<Bitmap> d(ParcelFileDescriptor parcelFileDescriptor, int i2, int i3, ob1 ob1) throws IOException {
        return e(new mg1.b(parcelFileDescriptor, this.d, this.c), i2, i3, ob1, k);
    }

    @DexIgnore
    public final id1<Bitmap> e(mg1 mg1, int i2, int i3, ob1 ob1, b bVar) throws IOException {
        byte[] bArr = (byte[]) this.c.g(65536, byte[].class);
        BitmapFactory.Options k2 = k();
        k2.inTempStorage = bArr;
        hb1 hb1 = (hb1) ob1.c(f);
        pb1 pb1 = (pb1) ob1.c(g);
        try {
            return yf1.f(h(mg1, k2, (fg1) ob1.c(fg1.f), hb1, pb1, ob1.c(i) != null && ((Boolean) ob1.c(i)).booleanValue(), i2, i3, ((Boolean) ob1.c(h)).booleanValue(), bVar), this.f1302a);
        } finally {
            v(k2);
            this.c.f(bArr);
        }
    }

    @DexIgnore
    public id1<Bitmap> f(InputStream inputStream, int i2, int i3, ob1 ob1) throws IOException {
        return g(inputStream, i2, i3, ob1, k);
    }

    @DexIgnore
    public id1<Bitmap> g(InputStream inputStream, int i2, int i3, ob1 ob1, b bVar) throws IOException {
        return e(new mg1.a(inputStream, this.d, this.c), i2, i3, ob1, bVar);
    }

    @DexIgnore
    public final Bitmap h(mg1 mg1, BitmapFactory.Options options, fg1 fg1, hb1 hb1, pb1 pb1, boolean z, int i2, int i3, boolean z2, b bVar) throws IOException {
        ColorSpace colorSpace;
        long b2 = ek1.b();
        int[] m2 = m(mg1, options, bVar, this.f1302a);
        int i4 = m2[0];
        int i5 = m2[1];
        String str = options.outMimeType;
        boolean z3 = (i4 == -1 || i5 == -1) ? false : z;
        int a2 = mg1.a();
        int g2 = tg1.g(a2);
        boolean j2 = tg1.j(a2);
        int i6 = i2 == Integer.MIN_VALUE ? r(g2) ? i5 : i4 : i2;
        int i7 = i3 == Integer.MIN_VALUE ? r(g2) ? i4 : i5 : i3;
        ImageHeaderParser.ImageType d2 = mg1.d();
        c(d2, mg1, bVar, this.f1302a, fg1, g2, i4, i5, i6, i7, options);
        b(mg1, hb1, z3, j2, options, i6, i7);
        boolean z4 = Build.VERSION.SDK_INT >= 19;
        if ((options.inSampleSize == 1 || z4) && z(d2)) {
            if (i4 < 0 || i5 < 0 || !z2 || !z4) {
                float f2 = s(options) ? ((float) options.inTargetDensity) / ((float) options.inDensity) : 1.0f;
                int i8 = options.inSampleSize;
                float f3 = (float) i8;
                i6 = Math.round(((float) ((int) Math.ceil((double) (((float) i4) / f3)))) * f2);
                i7 = Math.round(((float) ((int) Math.ceil((double) (((float) i5) / f3)))) * f2);
                if (Log.isLoggable("Downsampler", 2)) {
                    Log.v("Downsampler", "Calculated target [" + i6 + "x" + i7 + "] for source [" + i4 + "x" + i5 + "], sampleSize: " + i8 + ", targetDensity: " + options.inTargetDensity + ", density: " + options.inDensity + ", density multiplier: " + f2);
                }
            }
            if (i6 > 0 && i7 > 0) {
                y(options, this.f1302a, i6, i7);
            }
        }
        int i9 = Build.VERSION.SDK_INT;
        if (i9 >= 28) {
            options.inPreferredColorSpace = ColorSpace.get(pb1 == pb1.DISPLAY_P3 && (colorSpace = options.outColorSpace) != null && colorSpace.isWideGamut() ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB);
        } else if (i9 >= 26) {
            options.inPreferredColorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
        }
        Bitmap i10 = i(mg1, options, bVar, this.f1302a);
        bVar.a(this.f1302a, i10);
        if (Log.isLoggable("Downsampler", 2)) {
            t(i4, i5, str, options, i10, i2, i3, b2);
        }
        Bitmap bitmap = null;
        if (i10 != null) {
            i10.setDensity(this.b.densityDpi);
            bitmap = tg1.k(this.f1302a, i10, a2);
            if (!i10.equals(bitmap)) {
                this.f1302a.b(i10);
            }
        }
        return bitmap;
    }

    @DexIgnore
    public boolean o(ParcelFileDescriptor parcelFileDescriptor) {
        return fc1.c();
    }

    @DexIgnore
    public boolean p(InputStream inputStream) {
        return true;
    }

    @DexIgnore
    public boolean q(ByteBuffer byteBuffer) {
        return true;
    }

    @DexIgnore
    public final boolean z(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return l.contains(imageType);
    }
}
