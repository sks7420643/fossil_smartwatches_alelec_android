package com.fossil;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur5<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<T> f3637a; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ Object b; // = new Object();

    @DexIgnore
    public final boolean a(T t) {
        boolean add;
        synchronized (this.b) {
            add = !this.f3637a.contains(t) ? this.f3637a.add(t) : false;
        }
        return add;
    }

    @DexIgnore
    public final T b(rp7<? super T, Boolean> rp7) {
        T t;
        pq7.c(rp7, "predicate");
        synchronized (this.b) {
            Iterator<T> it = this.f3637a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (rp7.invoke(next).booleanValue()) {
                    t = next;
                    break;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public final boolean c(T t) {
        boolean remove;
        synchronized (this.b) {
            remove = this.f3637a.remove(t);
        }
        return remove;
    }
}
