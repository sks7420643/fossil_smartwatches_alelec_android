package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ te f3398a; // = new te();

    /*
    static {
        new ve(24, 40, 0, 600);
        new ve(9, 12, 0, 72);
        new ve(80, 108, 4, 600);
    }
    */

    @DexIgnore
    public final boolean a(se seVar, ve veVar) {
        int i = veVar.b;
        int i2 = veVar.c;
        int i3 = seVar.b;
        return i <= i3 && i2 >= i3 && seVar.c == veVar.d && seVar.d >= veVar.e;
    }
}
