package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q35 extends p35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362592, 1);
        F.put(2131362546, 2);
        F.put(2131363433, 3);
        F.put(2131361903, 4);
        F.put(2131362775, 5);
        F.put(2131362133, 6);
        F.put(2131362787, 7);
        F.put(2131362741, 8);
        F.put(2131362783, 9);
        F.put(2131362359, 10);
        F.put(2131362820, 11);
        F.put(2131363050, 12);
    }
    */

    @DexIgnore
    public q35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 13, E, F));
    }

    @DexIgnore
    public q35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleAutoCompleteTextView) objArr[4], (ImageView) objArr[6], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (ImageView) objArr[8], (ConstraintLayout) objArr[5], (View) objArr[9], (ImageView) objArr[7], (LinearLayout) objArr[11], (ConstraintLayout) objArr[0], (RecyclerView) objArr[12], (View) objArr[3]);
        this.D = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.D != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.D = 1;
        }
        w();
    }
}
