package com.fossil;

import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c58 extends Closeable {
    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close() throws IOException;

    @DexIgnore
    long d0(i48 i48, long j) throws IOException;

    @DexIgnore
    d58 e();
}
