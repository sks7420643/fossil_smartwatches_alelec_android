package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z83 implements xw2<c93> {
    @DexIgnore
    public static z83 c; // = new z83();
    @DexIgnore
    public /* final */ xw2<c93> b;

    @DexIgnore
    public z83() {
        this(ww2.b(new b93()));
    }

    @DexIgnore
    public z83(xw2<c93> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((c93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ c93 zza() {
        return this.b.zza();
    }
}
