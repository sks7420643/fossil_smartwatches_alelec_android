package com.fossil;

import com.fossil.be1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ee1 implements be1.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f921a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        File a();
    }

    @DexIgnore
    public ee1(a aVar, long j) {
        this.f921a = j;
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.be1.a
    public be1 build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return fe1.c(a2, this.f921a);
        }
        return null;
    }
}
