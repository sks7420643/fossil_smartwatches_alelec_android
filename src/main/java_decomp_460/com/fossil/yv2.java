package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yv2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ zi0<String, Uri> f4381a; // = new zi0<>();

    @DexIgnore
    public static Uri a(String str) {
        Uri uri;
        synchronized (yv2.class) {
            try {
                uri = f4381a.get(str);
                if (uri == null) {
                    String valueOf = String.valueOf(Uri.encode(str));
                    uri = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                    f4381a.put(str, uri);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return uri;
    }
}
