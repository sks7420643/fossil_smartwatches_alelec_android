package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x80 extends z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ x80 i; // = new x80();

    @DexIgnore
    public x80() {
        super("raw_hardware_log", 102400, 20971520, "raw_hardware_log", "raw_hardware_log", new zw1("", "", ""), 1800, new b90(), ld0.e, false);
    }

    @DexIgnore
    @Override // com.fossil.z80
    public long e() {
        return h;
    }
}
