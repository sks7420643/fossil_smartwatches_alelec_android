package com.fossil;

import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im3 extends in3 {
    @DexIgnore
    public static /* final */ AtomicLong l; // = new AtomicLong(Long.MIN_VALUE);
    @DexIgnore
    public mm3 c;
    @DexIgnore
    public mm3 d;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<nm3<?>> e; // = new PriorityBlockingQueue<>();
    @DexIgnore
    public /* final */ BlockingQueue<nm3<?>> f; // = new LinkedBlockingQueue();
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler g; // = new km3(this, "Thread death: Uncaught exception on worker thread");
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler h; // = new km3(this, "Thread death: Uncaught exception on network thread");
    @DexIgnore
    public /* final */ Object i; // = new Object();
    @DexIgnore
    public /* final */ Semaphore j; // = new Semaphore(2);
    @DexIgnore
    public volatile boolean k;

    @DexIgnore
    public im3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public final <V> Future<V> A(Callable<V> callable) throws IllegalStateException {
        o();
        rc2.k(callable);
        nm3<?> nm3 = new nm3<>(this, (Callable<?>) callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            nm3.run();
        } else {
            x(nm3);
        }
        return nm3;
    }

    @DexIgnore
    public final void B(Runnable runnable) throws IllegalStateException {
        o();
        rc2.k(runnable);
        nm3<?> nm3 = new nm3<>(this, runnable, false, "Task exception on network thread");
        synchronized (this.i) {
            this.f.add(nm3);
            if (this.d == null) {
                mm3 mm3 = new mm3(this, "Measurement Network", this.f);
                this.d = mm3;
                mm3.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                this.d.a();
            }
        }
    }

    @DexIgnore
    public final boolean G() {
        return Thread.currentThread() == this.c;
    }

    @DexIgnore
    @Override // com.fossil.jn3
    public final void g() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    @DexIgnore
    @Override // com.fossil.jn3
    public final void h() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    @DexIgnore
    @Override // com.fossil.in3
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final <T> T u(AtomicReference<T> atomicReference, long j2, String str, Runnable runnable) {
        synchronized (atomicReference) {
            c().y(runnable);
            try {
                atomicReference.wait(j2);
            } catch (InterruptedException e2) {
                nl3 I = d().I();
                String valueOf = String.valueOf(str);
                I.a(valueOf.length() != 0 ? "Interrupted waiting for ".concat(valueOf) : new String("Interrupted waiting for "));
                return null;
            }
        }
        T t = atomicReference.get();
        if (t == null) {
            nl3 I2 = d().I();
            String valueOf2 = String.valueOf(str);
            I2.a(valueOf2.length() != 0 ? "Timed out waiting for ".concat(valueOf2) : new String("Timed out waiting for "));
        }
        return t;
    }

    @DexIgnore
    public final <V> Future<V> v(Callable<V> callable) throws IllegalStateException {
        o();
        rc2.k(callable);
        nm3<?> nm3 = new nm3<>(this, (Callable<?>) callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.e.isEmpty()) {
                d().I().a("Callable skipped the worker queue.");
            }
            nm3.run();
        } else {
            x(nm3);
        }
        return nm3;
    }

    @DexIgnore
    public final void x(nm3<?> nm3) {
        synchronized (this.i) {
            this.e.add(nm3);
            if (this.c == null) {
                mm3 mm3 = new mm3(this, "Measurement Worker", this.e);
                this.c = mm3;
                mm3.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }

    @DexIgnore
    public final void y(Runnable runnable) throws IllegalStateException {
        o();
        rc2.k(runnable);
        x(new nm3<>(this, runnable, false, "Task exception on worker thread"));
    }
}
