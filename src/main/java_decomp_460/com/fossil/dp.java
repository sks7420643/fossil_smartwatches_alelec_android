package com.fossil;

import com.fossil.fitness.FitnessData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dp(rq rqVar) {
        super(1);
        this.b = rqVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(lp lpVar) {
        rq rqVar = this.b;
        FitnessData[] fitnessDataArr = ((mi) lpVar).V;
        if (fitnessDataArr == null) {
            fitnessDataArr = new FitnessData[0];
        }
        rqVar.D = fitnessDataArr;
        if (q3.f.f(this.b.x.a())) {
            rq rqVar2 = this.b;
            rqVar2.l(nr.a(rqVar2.v, null, zq.SUCCESS, null, null, 13));
        } else {
            rq rqVar3 = this.b;
            if (rqVar3.G > 0) {
                lp.h(rqVar3, new cj(rqVar3.w, rqVar3.x, (HashMap) null, rqVar3.z, 4), new sn(rqVar3), new eo(rqVar3), new qo(rqVar3), null, null, 48, null);
            } else {
                rqVar3.l(nr.a(rqVar3.v, null, zq.SUCCESS, null, null, 13));
            }
        }
        return tl7.f3441a;
    }
}
