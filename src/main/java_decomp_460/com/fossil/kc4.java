package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kc4 implements zy1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ot3 f1899a;
    @DexIgnore
    public /* final */ z84 b;

    @DexIgnore
    public kc4(ot3 ot3, z84 z84) {
        this.f1899a = ot3;
        this.b = z84;
    }

    @DexIgnore
    public static zy1 b(ot3 ot3, z84 z84) {
        return new kc4(ot3, z84);
    }

    @DexIgnore
    @Override // com.fossil.zy1
    public void a(Exception exc) {
        mc4.b(this.f1899a, this.b, exc);
    }
}
