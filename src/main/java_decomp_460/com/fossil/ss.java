package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ss extends rs {
    @DexIgnore
    public ss(hs hsVar, k5 k5Var) {
        super(hsVar, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final String p(mw mwVar) {
        f7 f7Var = mwVar.e.d.b;
        f7 f7Var2 = f7.START_FAIL;
        return f7Var == f7Var2 ? ey1.a(f7Var2) : ey1.a(lw.b);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final void r(u5 u5Var) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject k;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        if (u5Var.e.d.b == f7.START_FAIL) {
            a90 a90 = this.f;
            if (a90 != null) {
                a90.j = false;
            }
            a90 a902 = this.f;
            if (!(a902 == null || (jSONObject2 = a902.n) == null || (k = g80.k(jSONObject2, jd0.k, ey1.a(x6.f))) == null)) {
                g80.k(k, jd0.P0, u5Var.e.toJSONObject());
            }
        } else {
            a90 a903 = this.f;
            if (a903 != null) {
                a903.j = true;
            }
            a90 a904 = this.f;
            if (!(a904 == null || (jSONObject = a904.n) == null)) {
                g80.k(jSONObject, jd0.k, ey1.a(lw.b));
            }
        }
        C();
        m(this.v);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final String u(mw mwVar) {
        lw lwVar = mwVar.d;
        lw lwVar2 = lw.s;
        if (lwVar == lwVar2) {
            return ey1.a(lwVar2);
        }
        g7 g7Var = mwVar.e.d;
        f7 f7Var = g7Var.b;
        f7 f7Var2 = f7.START_FAIL;
        return f7Var == f7Var2 ? ey1.a(f7Var2) : (lwVar == lw.q || lwVar == lw.r) ? ey1.a(lw.r) : ey1.a(x6.n.a(g7Var.c));
    }
}
