package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe7 extends qe7 {
    @DexIgnore
    public oe7(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f2970a).edit();
            edit.putString(se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
            edit.commit();
        }
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final String d() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from sharedPreferences");
            string = PreferenceManager.getDefaultSharedPreferences(this.f2970a).getString(se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), null);
        }
        return string;
    }
}
