package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uy2<K> extends ay2<K> {
    @DexIgnore
    public /* final */ transient wx2<K, ?> d;
    @DexIgnore
    public /* final */ transient sx2<K> e;

    @DexIgnore
    public uy2(wx2<K, ?> wx2, sx2<K> sx2) {
        this.d = wx2;
        this.e = sx2;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean contains(@NullableDecl Object obj) {
        return this.d.get(obj) != null;
    }

    @DexIgnore
    public final int size() {
        return this.d.size();
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    /* renamed from: zzb */
    public final cz2<K> iterator() {
        return (cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.tx2, com.fossil.ay2
    public final sx2<K> zzc() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return true;
    }
}
