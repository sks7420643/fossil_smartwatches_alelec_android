package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fu7 {
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
        if (r0 != null) goto L_0x003f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> T a(com.fossil.tn7 r4, com.fossil.vp7<? super com.fossil.iv7, ? super com.fossil.qn7<? super T>, ? extends java.lang.Object> r5) {
        /*
            r1 = 0
            java.lang.Thread r2 = java.lang.Thread.currentThread()
            com.fossil.rn7$b r0 = com.fossil.rn7.p
            com.fossil.tn7$b r0 = r4.get(r0)
            com.fossil.rn7 r0 = (com.fossil.rn7) r0
            if (r0 != 0) goto L_0x002e
            com.fossil.wx7 r0 = com.fossil.wx7.b
            com.fossil.hw7 r0 = r0.b()
            com.fossil.qw7 r1 = com.fossil.qw7.b
            com.fossil.tn7 r3 = r4.plus(r0)
            com.fossil.tn7 r1 = com.fossil.cv7.c(r1, r3)
        L_0x001f:
            com.fossil.cu7 r3 = new com.fossil.cu7
            r3.<init>(r1, r2, r0)
            com.fossil.lv7 r0 = com.fossil.lv7.DEFAULT
            r3.z0(r0, r3, r5)
            java.lang.Object r0 = r3.A0()
            return r0
        L_0x002e:
            boolean r3 = r0 instanceof com.fossil.hw7
            if (r3 != 0) goto L_0x0033
            r0 = r1
        L_0x0033:
            com.fossil.hw7 r0 = (com.fossil.hw7) r0
            if (r0 == 0) goto L_0x0046
            boolean r3 = r0.s0()
            if (r3 == 0) goto L_0x004d
        L_0x003d:
            if (r0 == 0) goto L_0x0046
        L_0x003f:
            com.fossil.qw7 r1 = com.fossil.qw7.b
            com.fossil.tn7 r1 = com.fossil.cv7.c(r1, r4)
            goto L_0x001f
        L_0x0046:
            com.fossil.wx7 r0 = com.fossil.wx7.b
            com.fossil.hw7 r0 = r0.a()
            goto L_0x003f
        L_0x004d:
            r0 = r1
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fu7.a(com.fossil.tn7, com.fossil.vp7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object b(tn7 tn7, vp7 vp7, int i, Object obj) {
        if ((i & 1) != 0) {
            tn7 = un7.INSTANCE;
        }
        return eu7.e(tn7, vp7);
    }
}
