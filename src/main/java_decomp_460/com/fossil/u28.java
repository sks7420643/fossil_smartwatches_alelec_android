package com.fossil;

import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.p18;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u28 {
    /*
    static {
        l48.encodeUtf8("\"\\");
        l48.encodeUtf8("\t ,=");
    }
    */

    @DexIgnore
    public static long a(p18 p18) {
        return j(p18.c(HttpHeaders.CONTENT_LENGTH));
    }

    @DexIgnore
    public static long b(Response response) {
        return a(response.l());
    }

    @DexIgnore
    public static boolean c(Response response) {
        if (response.G().g().equals("HEAD")) {
            return false;
        }
        int f = response.f();
        if ((f >= 100 && f < 200) || f == 204 || f == 304) {
            return b(response) != -1 || "chunked".equalsIgnoreCase(response.j("Transfer-Encoding"));
        }
        return true;
    }

    @DexIgnore
    public static boolean d(p18 p18) {
        return k(p18).contains(g78.ANY_MARKER);
    }

    @DexIgnore
    public static boolean e(Response response) {
        return d(response.l());
    }

    @DexIgnore
    public static int f(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException e) {
            return i;
        }
    }

    @DexIgnore
    public static void g(i18 i18, q18 q18, p18 p18) {
        if (i18 != i18.f1572a) {
            List<h18> f = h18.f(q18, p18);
            if (!f.isEmpty()) {
                i18.a(q18, f);
            }
        }
    }

    @DexIgnore
    public static int h(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static int i(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == '\t')) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static long j(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @DexIgnore
    public static Set<String> k(p18 p18) {
        Set<String> emptySet = Collections.emptySet();
        int h = p18.h();
        for (int i = 0; i < h; i++) {
            if ("Vary".equalsIgnoreCase(p18.e(i))) {
                String i2 = p18.i(i);
                if (emptySet.isEmpty()) {
                    emptySet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                String[] split = i2.split(",");
                for (String str : split) {
                    emptySet.add(str.trim());
                }
            }
        }
        return emptySet;
    }

    @DexIgnore
    public static Set<String> l(Response response) {
        return k(response.l());
    }

    @DexIgnore
    public static p18 m(p18 p18, p18 p182) {
        Set<String> k = k(p182);
        if (k.isEmpty()) {
            return new p18.a().e();
        }
        p18.a aVar = new p18.a();
        int h = p18.h();
        for (int i = 0; i < h; i++) {
            String e = p18.e(i);
            if (k.contains(e)) {
                aVar.a(e, p18.i(i));
            }
        }
        return aVar.e();
    }

    @DexIgnore
    public static p18 n(Response response) {
        return m(response.A().G().e(), response.l());
    }

    @DexIgnore
    public static boolean o(Response response, p18 p18, v18 v18) {
        for (String str : l(response)) {
            if (!b28.q(p18.j(str), v18.d(str))) {
                return false;
            }
        }
        return true;
    }
}
