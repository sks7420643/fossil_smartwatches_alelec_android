package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m48 implements a58 {
    @DexIgnore
    public /* final */ a58 b;

    @DexIgnore
    public m48(a58 a58) {
        pq7.c(a58, "delegate");
        this.b = a58;
    }

    @DexIgnore
    @Override // com.fossil.a58
    public void K(i48 i48, long j) throws IOException {
        pq7.c(i48, "source");
        this.b.K(i48, j);
    }

    @DexIgnore
    @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.a58
    public d58 e() {
        return this.b.e();
    }

    @DexIgnore
    @Override // com.fossil.a58, java.io.Flushable
    public void flush() throws IOException {
        this.b.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }
}
