package com.fossil;

import java.util.concurrent.Executor;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i12 implements k12 {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(m02.class.getName());

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ h22 f1571a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ t02 c;
    @DexIgnore
    public /* final */ k22 d;
    @DexIgnore
    public /* final */ s32 e;

    @DexIgnore
    public i12(Executor executor, t02 t02, h22 h22, k22 k22, s32 s32) {
        this.b = executor;
        this.c = t02;
        this.f1571a = h22;
        this.d = k22;
        this.e = s32;
    }

    @DexIgnore
    public static /* synthetic */ Object b(i12 i12, h02 h02, c02 c02) {
        i12.d.Y(h02, c02);
        i12.f1571a.a(h02, 1);
        return null;
    }

    @DexIgnore
    public static /* synthetic */ void c(i12 i12, h02 h02, zy1 zy1, c02 c02) {
        try {
            b12 b2 = i12.c.b(h02.b());
            if (b2 == null) {
                String format = String.format("Transport backend '%s' is not registered", h02.b());
                f.warning(format);
                zy1.a(new IllegalArgumentException(format));
                return;
            }
            i12.e.a(h12.b(i12, h02, b2.b(c02)));
            zy1.a(null);
        } catch (Exception e2) {
            Logger logger = f;
            logger.warning("Error scheduling event " + e2.getMessage());
            zy1.a(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.k12
    public void a(h02 h02, c02 c02, zy1 zy1) {
        this.b.execute(g12.a(this, h02, zy1, c02));
    }
}
