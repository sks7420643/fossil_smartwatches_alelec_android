package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zy0 extends ez0 implements bz0 {
    @DexIgnore
    public zy0(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static zy0 g(ViewGroup viewGroup) {
        return (zy0) ez0.e(viewGroup);
    }

    @DexIgnore
    @Override // com.fossil.bz0
    public void c(View view) {
        this.f1007a.b(view);
    }

    @DexIgnore
    @Override // com.fossil.bz0
    public void d(View view) {
        this.f1007a.g(view);
    }
}
