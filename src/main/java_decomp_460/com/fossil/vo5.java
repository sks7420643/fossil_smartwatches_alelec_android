package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo5 implements Factory<uo5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<so5> f3802a;

    @DexIgnore
    public vo5(Provider<so5> provider) {
        this.f3802a = provider;
    }

    @DexIgnore
    public static vo5 a(Provider<so5> provider) {
        return new vo5(provider);
    }

    @DexIgnore
    public static uo5 c(so5 so5) {
        return new uo5(so5);
    }

    @DexIgnore
    /* renamed from: b */
    public uo5 get() {
        return c(this.f3802a.get());
    }
}
