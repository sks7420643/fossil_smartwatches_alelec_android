package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro1 extends yo1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ro1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public ro1 a(Parcel parcel) {
            return new ro1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ro1 createFromParcel(Parcel parcel) {
            return new ro1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ro1[] newArray(int i) {
            return new ro1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ro1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public ro1(ww1 ww1) {
        super(ap1.COMMUTE, null, ww1, 2);
    }

    @DexIgnore
    public ro1(ww1 ww1, vw1 vw1) {
        super(ap1.COMMUTE, vw1, ww1);
    }

    @DexIgnore
    @Override // com.fossil.yo1
    public JSONObject a() {
        JSONObject put = super.a().put("commuteApp._.config.destinations", ay1.b(getDataConfig().getDestinations()));
        pq7.b(put, "super.getDataConfigJSONO\u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public final ww1 getDataConfig() {
        xw1 xw1 = this.d;
        if (xw1 != null) {
            return (ww1) xw1;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }
}
