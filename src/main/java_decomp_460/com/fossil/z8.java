package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class z8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f4430a;

    /*
    static {
        int[] iArr = new int[zm1.values().length];
        f4430a = iArr;
        iArr[zm1.BIOMETRIC_PROFILE.ordinal()] = 1;
        f4430a[zm1.DAILY_STEP.ordinal()] = 2;
        f4430a[zm1.DAILY_STEP_GOAL.ordinal()] = 3;
        f4430a[zm1.DAILY_CALORIE.ordinal()] = 4;
        f4430a[zm1.DAILY_CALORIE_GOAL.ordinal()] = 5;
        f4430a[zm1.DAILY_TOTAL_ACTIVE_MINUTE.ordinal()] = 6;
        f4430a[zm1.DAILY_ACTIVE_MINUTE_GOAL.ordinal()] = 7;
        f4430a[zm1.DAILY_DISTANCE.ordinal()] = 8;
        f4430a[zm1.INACTIVE_NUDGE.ordinal()] = 9;
        f4430a[zm1.VIBE_STRENGTH.ordinal()] = 10;
        f4430a[zm1.DO_NOT_DISTURB_SCHEDULE.ordinal()] = 11;
        f4430a[zm1.TIME.ordinal()] = 12;
        f4430a[zm1.BATTERY.ordinal()] = 13;
        f4430a[zm1.HEART_RATE_MODE.ordinal()] = 14;
        f4430a[zm1.DAILY_SLEEP.ordinal()] = 15;
        f4430a[zm1.DISPLAY_UNIT.ordinal()] = 16;
        f4430a[zm1.SECOND_TIMEZONE_OFFSET.ordinal()] = 17;
        f4430a[zm1.CURRENT_HEART_RATE.ordinal()] = 18;
        f4430a[zm1.HELLAS_BATTERY.ordinal()] = 19;
        f4430a[zm1.AUTO_WORKOUT_DETECTION.ordinal()] = 20;
        f4430a[zm1.CYCLING_CADENCE.ordinal()] = 21;
    }
    */
}
