package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq5 implements Factory<aq5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<vr5> f484a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public bq5(Provider<vr5> provider, Provider<on5> provider2) {
        this.f484a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static bq5 a(Provider<vr5> provider, Provider<on5> provider2) {
        return new bq5(provider, provider2);
    }

    @DexIgnore
    public static aq5 c() {
        return new aq5();
    }

    @DexIgnore
    /* renamed from: b */
    public aq5 get() {
        aq5 c = c();
        cq5.a(c, this.f484a.get());
        cq5.b(c, this.b.get());
        return c;
    }
}
