package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bv3 implements Parcelable.Creator<av3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ av3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        ArrayList arrayList = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                str = ad2.f(parcel, t);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                arrayList = ad2.j(parcel, t, pv3.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new av3(str, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ av3[] newArray(int i) {
        return new av3[i];
    }
}
