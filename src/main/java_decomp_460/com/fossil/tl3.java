package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl3 implements Runnable {
    @DexIgnore
    public /* final */ ql3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Throwable d;
    @DexIgnore
    public /* final */ byte[] e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Map<String, List<String>> g;

    @DexIgnore
    public tl3(String str, ql3 ql3, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        rc2.k(ql3);
        this.b = ql3;
        this.c = i;
        this.d = th;
        this.e = bArr;
        this.f = str;
        this.g = map;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.f, this.c, this.d, this.e, this.g);
    }
}
