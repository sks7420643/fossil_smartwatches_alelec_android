package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum eu {
    WORKOUT_SESSION_CONTROL(new byte[]{2});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public eu(byte[] bArr) {
        this.b = bArr;
    }
}
