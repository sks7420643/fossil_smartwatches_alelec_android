package com.fossil;

import android.text.TextUtils;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ym5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static xm5 f4335a; // = new xm5();
    @DexIgnore
    public static String b; // = ", ";

    @DexIgnore
    public static void a() {
        f4335a.a();
        tm5.d().b();
    }

    @DexIgnore
    public static String b(String str) {
        return TextUtils.isEmpty(str) ? "" : str.substring(str.lastIndexOf(File.separator) + 1);
    }

    @DexIgnore
    public static void c() {
        xm5 xm5 = f4335a;
        if (!(xm5 == null || xm5.b() == null)) {
            tm5 d = tm5.d();
            for (Map.Entry<String, String> entry : f4335a.b().entrySet()) {
                if (d.a(entry.getKey())) {
                    d.e(entry.getKey());
                }
                d.g(entry.getKey(), entry.getValue());
            }
        }
    }

    @DexIgnore
    public static void d(String str, boolean z) {
        f4335a.d(str, z);
    }

    @DexIgnore
    public static String e(int i) {
        return f68.c(i, "0123456789abcdef");
    }

    @DexIgnore
    public static String f(String str) {
        return f4335a.c(str);
    }
}
