package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ tn1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gp1 createFromParcel(Parcel parcel) {
            float readFloat = parcel.readFloat();
            float readFloat2 = parcel.readFloat();
            float readFloat3 = parcel.readFloat();
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                return new gp1(readFloat, readFloat2, readFloat3, readInt, tn1.valueOf(readString));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gp1[] newArray(int i) {
            return new gp1[i];
        }
    }

    @DexIgnore
    public gp1(float f2, float f3, float f4, int i, tn1 tn1) {
        this.b = hy1.e(f2, 2);
        this.c = hy1.e(f3, 2);
        this.d = hy1.e(f4, 2);
        this.e = i;
        this.f = tn1;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject put = new JSONObject().put("temp", Float.valueOf(this.b)).put("high", Float.valueOf(this.c)).put("low", Float.valueOf(this.d)).put("rain", this.e).put("cond_id", this.f.a());
        pq7.b(put, "JSONObject()\n           \u2026rrentWeatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(gp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            gp1 gp1 = (gp1) obj;
            if (this.b != gp1.b) {
                return false;
            }
            if (this.c != gp1.c) {
                return false;
            }
            if (this.d != gp1.d) {
                return false;
            }
            if (this.e != gp1.e) {
                return false;
            }
            return this.f == gp1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CurrentWeatherInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.e;
    }

    @DexIgnore
    public final float getCurrentTemperature() {
        return this.b;
    }

    @DexIgnore
    public final tn1 getCurrentWeatherCondition() {
        return this.f;
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.c;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        int hashCode3 = Float.valueOf(this.d).hashCode();
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + this.e) * 31) + this.f.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.Y1, Float.valueOf(this.b)), jd0.Z1, Float.valueOf(this.c)), jd0.a2, Float.valueOf(this.d)), jd0.r, Integer.valueOf(this.e)), jd0.t, ey1.a(this.f));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeFloat(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
    }
}
