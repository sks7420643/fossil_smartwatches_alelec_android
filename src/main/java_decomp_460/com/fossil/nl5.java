package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl5 {
    @DexIgnore
    public static final void a(View view, View.OnClickListener onClickListener) {
        pq7.c(view, "$this$setOnSingleClickListener");
        if (onClickListener != null) {
            view.setOnClickListener(new el5(onClickListener));
            if (onClickListener != null) {
                return;
            }
        }
        view.setOnClickListener(null);
        tl7 tl7 = tl7.f3441a;
    }
}
