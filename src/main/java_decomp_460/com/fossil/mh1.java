package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh1 implements qb1<bb1, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd1 f2380a;

    @DexIgnore
    public mh1(rd1 rd1) {
        this.f2380a = rd1;
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(bb1 bb1, int i, int i2, ob1 ob1) {
        return yf1.f(bb1.a(), this.f2380a);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(bb1 bb1, ob1 ob1) {
        return true;
    }
}
