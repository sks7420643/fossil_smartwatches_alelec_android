package com.fossil;

import android.database.Cursor;
import android.os.Build;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ix0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1684a;
    @DexIgnore
    public /* final */ Map<String, a> b;
    @DexIgnore
    public /* final */ Set<b> c;
    @DexIgnore
    public /* final */ Set<d> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1685a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ int g;

        @DexIgnore
        public a(String str, String str2, boolean z, int i, String str3, int i2) {
            this.f1685a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = a(str2);
            this.f = str3;
            this.g = i2;
        }

        @DexIgnore
        public static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (!upperCase.contains("BLOB")) {
                return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
            }
            return 5;
        }

        @DexIgnore
        public boolean b() {
            return this.e > 0;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            String str;
            String str2;
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.e != aVar.e) {
                    return false;
                }
            } else if (b() != aVar.b()) {
                return false;
            }
            if (!this.f1685a.equals(aVar.f1685a) || this.d != aVar.d) {
                return false;
            }
            if (this.g == 1 && aVar.g == 2 && (str2 = this.f) != null && !str2.equals(aVar.f)) {
                return false;
            }
            if (this.g == 2 && aVar.g == 1 && (str = aVar.f) != null && !str.equals(this.f)) {
                return false;
            }
            int i = this.g;
            if (i != 0 && i == aVar.g) {
                String str3 = this.f;
                if (str3 != null) {
                    if (!str3.equals(aVar.f)) {
                        return false;
                    }
                } else if (aVar.f != null) {
                    return false;
                }
            }
            if (this.c != aVar.c) {
                z = false;
            }
            return z;
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.f1685a.hashCode();
            return (((this.d ? 1231 : 1237) + (((hashCode * 31) + this.c) * 31)) * 31) + this.e;
        }

        @DexIgnore
        public String toString() {
            return "Column{name='" + this.f1685a + "', type='" + this.b + "', affinity='" + this.c + "', notNull=" + this.d + ", primaryKeyPosition=" + this.e + ", defaultValue='" + this.f + "'}";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1686a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ List<String> d;
        @DexIgnore
        public /* final */ List<String> e;

        @DexIgnore
        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.f1686a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (!this.f1686a.equals(bVar.f1686a) || !this.b.equals(bVar.b) || !this.c.equals(bVar.c) || !this.d.equals(bVar.d)) {
                return false;
            }
            return this.e.equals(bVar.e);
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.f1686a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ForeignKey{referenceTable='" + this.f1686a + "', onDelete='" + this.b + "', onUpdate='" + this.c + "', columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Comparable<c> {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;

        @DexIgnore
        public c(int i, int i2, String str, String str2) {
            this.b = i;
            this.c = i2;
            this.d = str;
            this.e = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = this.b - cVar.b;
            return i == 0 ? this.c - cVar.c : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1687a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ List<String> c;

        @DexIgnore
        public d(String str, boolean z, List<String> list) {
            this.f1687a = str;
            this.b = z;
            this.c = list;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.b != dVar.b || !this.c.equals(dVar.c)) {
                return false;
            }
            return this.f1687a.startsWith("index_") ? dVar.f1687a.startsWith("index_") : this.f1687a.equals(dVar.f1687a);
        }

        @DexIgnore
        public int hashCode() {
            return ((((this.f1687a.startsWith("index_") ? -1184239155 : this.f1687a.hashCode()) * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Index{name='" + this.f1687a + "', unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    @DexIgnore
    public ix0(String str, Map<String, a> map, Set<b> set, Set<d> set2) {
        this.f1684a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        this.d = set2 == null ? null : Collections.unmodifiableSet(set2);
    }

    @DexIgnore
    public static ix0 a(lx0 lx0, String str) {
        return new ix0(str, b(lx0, str), d(lx0, str), f(lx0, str));
    }

    @DexIgnore
    public static Map<String, a> b(lx0 lx0, String str) {
        Cursor query = lx0.query("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (query.getColumnCount() > 0) {
                int columnIndex = query.getColumnIndex("name");
                int columnIndex2 = query.getColumnIndex("type");
                int columnIndex3 = query.getColumnIndex("notnull");
                int columnIndex4 = query.getColumnIndex("pk");
                int columnIndex5 = query.getColumnIndex("dflt_value");
                while (query.moveToNext()) {
                    String string = query.getString(columnIndex);
                    hashMap.put(string, new a(string, query.getString(columnIndex2), query.getInt(columnIndex3) != 0, query.getInt(columnIndex4), query.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static List<c> c(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    public static Set<b> d(lx0 lx0, String str) {
        HashSet hashSet = new HashSet();
        Cursor query = lx0.query("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("id");
            int columnIndex2 = query.getColumnIndex("seq");
            int columnIndex3 = query.getColumnIndex("table");
            int columnIndex4 = query.getColumnIndex("on_delete");
            int columnIndex5 = query.getColumnIndex("on_update");
            List<c> c2 = c(query);
            int count = query.getCount();
            for (int i = 0; i < count; i++) {
                query.moveToPosition(i);
                if (query.getInt(columnIndex2) == 0) {
                    int i2 = query.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (c cVar : c2) {
                        if (cVar.b == i2) {
                            arrayList.add(cVar.d);
                            arrayList2.add(cVar.e);
                        }
                    }
                    hashSet.add(new b(query.getString(columnIndex3), query.getString(columnIndex4), query.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static d e(lx0 lx0, String str, boolean z) {
        Cursor query = lx0.query("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("seqno");
            int columnIndex2 = query.getColumnIndex("cid");
            int columnIndex3 = query.getColumnIndex("name");
            if (columnIndex == -1 || columnIndex2 == -1 || columnIndex3 == -1) {
                query.close();
                return null;
            }
            TreeMap treeMap = new TreeMap();
            while (query.moveToNext()) {
                if (query.getInt(columnIndex2) >= 0) {
                    treeMap.put(Integer.valueOf(query.getInt(columnIndex)), query.getString(columnIndex3));
                }
            }
            ArrayList arrayList = new ArrayList(treeMap.size());
            arrayList.addAll(treeMap.values());
            d dVar = new d(str, z, arrayList);
            query.close();
            return dVar;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    @DexIgnore
    public static Set<d> f(lx0 lx0, String str) {
        Cursor query = lx0.query("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("name");
            int columnIndex2 = query.getColumnIndex("origin");
            int columnIndex3 = query.getColumnIndex(DatabaseFieldConfigLoader.FIELD_NAME_UNIQUE);
            if (columnIndex == -1 || columnIndex2 == -1 || columnIndex3 == -1) {
                return null;
            }
            HashSet hashSet = new HashSet();
            while (query.moveToNext()) {
                if ("c".equals(query.getString(columnIndex2))) {
                    d e = e(lx0, query.getString(columnIndex), query.getInt(columnIndex3) == 1);
                    if (e == null) {
                        query.close();
                        return null;
                    }
                    hashSet.add(e);
                }
            }
            query.close();
            return hashSet;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Set<d> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || ix0.class != obj.getClass()) {
            return false;
        }
        ix0 ix0 = (ix0) obj;
        String str = this.f1684a;
        if (str == null ? ix0.f1684a != null : !str.equals(ix0.f1684a)) {
            return false;
        }
        Map<String, a> map = this.b;
        if (map == null ? ix0.b != null : !map.equals(ix0.b)) {
            return false;
        }
        Set<b> set2 = this.c;
        if (set2 == null ? ix0.c != null : !set2.equals(ix0.c)) {
            return false;
        }
        Set<d> set3 = this.d;
        if (set3 == null || (set = ix0.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1684a;
        int hashCode = str != null ? str.hashCode() : 0;
        Map<String, a> map = this.b;
        int hashCode2 = map != null ? map.hashCode() : 0;
        Set<b> set = this.c;
        if (set != null) {
            i = set.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "TableInfo{name='" + this.f1684a + "', columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }
}
