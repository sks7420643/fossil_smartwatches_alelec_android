package com.fossil;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import com.facebook.internal.Utility;
import com.fossil.rd7;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ld7 extends rd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Downloader f2183a;
    @DexIgnore
    public /* final */ td7 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore
    public ld7(Downloader downloader, td7 td7) {
        this.f2183a = downloader;
        this.b = td7;
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean c(pd7 pd7) {
        String scheme = pd7.d.getScheme();
        return "http".equals(scheme) || Utility.URL_SCHEME.equals(scheme);
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public int e() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        Downloader.Response load = this.f2183a.load(pd7.d, pd7.c);
        if (load == null) {
            return null;
        }
        Picasso.LoadedFrom loadedFrom = load.c ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap a2 = load.a();
        if (a2 != null) {
            return new rd7.a(a2, loadedFrom);
        }
        InputStream c = load.c();
        if (c == null) {
            return null;
        }
        if (loadedFrom == Picasso.LoadedFrom.DISK && load.b() == 0) {
            xd7.e(c);
            throw new a("Received response with 0 content-length header.");
        }
        if (loadedFrom == Picasso.LoadedFrom.NETWORK && load.b() > 0) {
            this.b.f(load.b());
        }
        return new rd7.a(c, loadedFrom);
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean h(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean i() {
        return true;
    }
}
