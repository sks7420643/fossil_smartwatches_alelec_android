package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Rect f4200a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ g04 f;

    @DexIgnore
    public xx3(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i, g04 g04, Rect rect) {
        pn0.c(rect.left);
        pn0.c(rect.top);
        pn0.c(rect.right);
        pn0.c(rect.bottom);
        this.f4200a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.e = i;
        this.f = g04;
    }

    @DexIgnore
    public static xx3 a(Context context, int i) {
        pn0.b(i != 0, "Cannot create a CalendarItemStyle with a styleResId of 0");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, tw3.MaterialCalendarItem);
        Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(tw3.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(tw3.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(tw3.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(tw3.MaterialCalendarItem_android_insetBottom, 0));
        ColorStateList a2 = oz3.a(context, obtainStyledAttributes, tw3.MaterialCalendarItem_itemFillColor);
        ColorStateList a3 = oz3.a(context, obtainStyledAttributes, tw3.MaterialCalendarItem_itemTextColor);
        ColorStateList a4 = oz3.a(context, obtainStyledAttributes, tw3.MaterialCalendarItem_itemStrokeColor);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(tw3.MaterialCalendarItem_itemStrokeWidth, 0);
        g04 m = g04.b(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(tw3.MaterialCalendarItem_itemShapeAppearanceOverlay, 0)).m();
        obtainStyledAttributes.recycle();
        return new xx3(a2, a3, a4, dimensionPixelSize, m, rect);
    }

    @DexIgnore
    public int b() {
        return this.f4200a.bottom;
    }

    @DexIgnore
    public int c() {
        return this.f4200a.top;
    }

    @DexIgnore
    public void d(TextView textView) {
        c04 c04 = new c04();
        c04 c042 = new c04();
        c04.setShapeAppearanceModel(this.f);
        c042.setShapeAppearanceModel(this.f);
        c04.V(this.c);
        c04.e0((float) this.e, this.d);
        textView.setTextColor(this.b);
        Drawable rippleDrawable = Build.VERSION.SDK_INT >= 21 ? new RippleDrawable(this.b.withAlpha(30), c04, c042) : c04;
        Rect rect = this.f4200a;
        mo0.o0(textView, new InsetDrawable(rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }
}
