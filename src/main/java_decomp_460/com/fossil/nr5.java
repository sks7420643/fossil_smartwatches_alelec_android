package com.fossil;

import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nr5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2558a;

    /*
    static {
        int[] iArr = new int[NotifyMusicEventResponse.MusicMediaAction.values().length];
        f2558a = iArr;
        iArr[NotifyMusicEventResponse.MusicMediaAction.PLAY.ordinal()] = 1;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.PAUSE.ordinal()] = 2;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.NEXT.ordinal()] = 4;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.PREVIOUS.ordinal()] = 5;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.VOLUME_UP.ordinal()] = 6;
        f2558a[NotifyMusicEventResponse.MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
    }
    */
}
