package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay7<U, T extends U> extends tz7<T> implements Runnable {
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public ay7(long j, qn7<? super U> qn7) {
        super(qn7.getContext(), qn7);
        this.f = j;
    }

    @DexIgnore
    @Override // com.fossil.au7, com.fossil.fx7
    public String Z() {
        return super.Z() + "(timeMillis=" + this.f + ')';
    }

    @DexIgnore
    public void run() {
        s(by7.a(this.f, this));
    }
}
