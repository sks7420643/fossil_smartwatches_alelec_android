package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.fossil.l72;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z82 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f4431a;

    @DexIgnore
    public z82(int i) {
        this.f4431a = i;
    }

    @DexIgnore
    public static Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (mf2.b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    @DexIgnore
    public abstract void b(Status status);

    @DexIgnore
    public abstract void c(l72.a<?> aVar) throws DeadObjectException;

    @DexIgnore
    public abstract void d(a82 a82, boolean z);

    @DexIgnore
    public abstract void e(Exception exc);
}
