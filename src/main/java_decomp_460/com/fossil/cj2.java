package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<cj2> CREATOR; // = new bj2();
    @DexIgnore
    public /* final */ DataSet b;
    @DexIgnore
    public /* final */ mo2 c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public cj2(DataSet dataSet, IBinder iBinder, boolean z) {
        this.b = dataSet;
        this.c = oo2.e(iBinder);
        this.d = z;
    }

    @DexIgnore
    public cj2(DataSet dataSet, mo2 mo2, boolean z) {
        this.b = dataSet;
        this.c = mo2;
        this.d = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return obj == this || ((obj instanceof cj2) && pc2.a(this.b, ((cj2) obj).b));
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(this.b);
    }

    @DexIgnore
    public final String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("dataSet", this.b);
        return c2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, this.b, i, false);
        mo2 mo2 = this.c;
        bd2.m(parcel, 2, mo2 == null ? null : mo2.asBinder(), false);
        bd2.c(parcel, 4, this.d);
        bd2.b(parcel, a2);
    }
}
