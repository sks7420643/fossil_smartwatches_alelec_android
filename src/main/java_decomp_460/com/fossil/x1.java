package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ v1 CREATOR; // = new v1(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d; // = ((TimeZone.getDefault().getOffset(this.e) / 1000) / 60);
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public x1(long j) {
        this.e = j;
        long j2 = (long) 1000;
        long j3 = j / j2;
        this.b = j3;
        this.c = j - (j2 * j3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.G2, Long.valueOf(this.b)), jd0.H2, Long.valueOf(this.c)), jd0.w2, Integer.valueOf(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.e);
    }
}
