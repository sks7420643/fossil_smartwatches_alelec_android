package com.fossil;

import com.fossil.e88;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class l88 extends e88.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ e88.a f2159a; // = new l88();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class a<T> implements e88<w18, Optional<T>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ e88<w18, T> f2160a;

        @DexIgnore
        public a(e88<w18, T> e88) {
            this.f2160a = e88;
        }

        @DexIgnore
        /* renamed from: b */
        public Optional<T> a(w18 w18) throws IOException {
            return Optional.ofNullable(this.f2160a.a(w18));
        }
    }

    @DexIgnore
    @Override // com.fossil.e88.a
    public e88<w18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (e88.a.b(type) != Optional.class) {
            return null;
        }
        return new a(retrofit3.i(e88.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
