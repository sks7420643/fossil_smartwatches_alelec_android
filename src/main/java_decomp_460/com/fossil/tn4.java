package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3444a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.d;
    }

    @DexIgnore
    public int c() {
        return this.f3444a;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }
}
