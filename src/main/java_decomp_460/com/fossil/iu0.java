package com.fossil;

import com.fossil.bu0;
import com.fossil.cu0;
import com.fossil.eu0;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iu0<T> extends cu0<T> implements eu0.a {
    @DexIgnore
    public /* final */ gu0<T> u;
    @DexIgnore
    public bu0.a<T> v; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bu0.a<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.bu0.a
        public void a(int i, bu0<T> bu0) {
            boolean z = true;
            if (bu0.c()) {
                iu0.this.m();
            } else if (iu0.this.t()) {
            } else {
                if (i == 0 || i == 3) {
                    List<T> list = bu0.f508a;
                    if (iu0.this.f.l() == 0) {
                        iu0 iu0 = iu0.this;
                        iu0.f.s(bu0.b, list, bu0.c, bu0.d, iu0.e.f656a, iu0);
                    } else {
                        iu0 iu02 = iu0.this;
                        iu02.f.F(bu0.d, list, iu02.g, iu02.e.d, iu02.i, iu02);
                    }
                    iu0 iu03 = iu0.this;
                    if (iu03.d != null) {
                        boolean z2 = iu03.f.size() == 0;
                        boolean z3 = !z2 && bu0.b == 0 && bu0.d == 0;
                        int size = iu0.this.size();
                        if (z2 || (!(i == 0 && bu0.c == 0) && (i != 3 || bu0.d + iu0.this.e.f656a < size))) {
                            z = false;
                        }
                        iu0.this.l(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(int i) {
            this.b = i;
        }

        @DexIgnore
        public void run() {
            if (!iu0.this.t()) {
                iu0 iu0 = iu0.this;
                int i = iu0.e.f656a;
                if (iu0.u.isInvalid()) {
                    iu0.this.m();
                    return;
                }
                int i2 = this.b * i;
                int min = Math.min(i, iu0.this.f.size() - i2);
                iu0 iu02 = iu0.this;
                iu02.u.dispatchLoadRange(3, i2, min, iu02.b, iu02.v);
            }
        }
    }

    @DexIgnore
    public iu0(gu0<T> gu0, Executor executor, Executor executor2, cu0.c<T> cVar, cu0.f fVar, int i) {
        super(new eu0(), executor, executor2, cVar, fVar);
        this.u = gu0;
        int i2 = this.e.f656a;
        this.g = i;
        if (gu0.isInvalid()) {
            m();
            return;
        }
        int max = Math.max(this.e.e / i2, 2) * i2;
        this.u.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.b, this.v);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void c(int i) {
        y(0, i);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void d(int i) {
        this.c.execute(new b(i));
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void e(int i, int i2) {
        x(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void f(int i, int i2) {
        z(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void g() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void h(int i, int i2) {
        x(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void i(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void o(cu0<T> cu0, cu0.e eVar) {
        int i;
        eu0<T> eu0 = cu0.f;
        if (eu0.isEmpty() || this.f.size() != eu0.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i2 = this.e.f656a;
        int h = this.f.h() / i2;
        int l = this.f.l();
        int i3 = 0;
        while (i3 < l) {
            int i4 = i3 + h;
            int i5 = 0;
            while (i5 < this.f.l()) {
                int i6 = i4 + i5;
                if (!this.f.p(i2, i6) || eu0.p(i2, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                eVar.a(i4 * i2, i2 * i5);
                i = (i5 - 1) + i3;
            } else {
                i = i3;
            }
            i3 = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public xt0<?, T> p() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public Object q() {
        return Integer.valueOf(this.g);
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public boolean s() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void w(int i) {
        eu0<T> eu0 = this.f;
        cu0.f fVar = this.e;
        eu0.b(i, fVar.b, fVar.f656a, this);
    }
}
