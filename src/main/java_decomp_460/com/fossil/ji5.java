package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ji5 {
    DEVICE("device"),
    MANUAL("manual");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ji5 a(String str) {
            ji5 ji5;
            pq7.c(str, "value");
            ji5[] values = ji5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ji5 = null;
                    break;
                }
                ji5 = values[i];
                String mValue = ji5.getMValue();
                String lowerCase = str.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (pq7.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return ji5 != null ? ji5 : ji5.DEVICE;
        }
    }

    @DexIgnore
    public ji5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
