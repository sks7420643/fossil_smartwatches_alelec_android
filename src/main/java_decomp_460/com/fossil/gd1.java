package com.fossil;

import com.fossil.vc1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gd1<Data, ResourceType, Transcode> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ mn0<List<Throwable>> f1293a;
    @DexIgnore
    public /* final */ List<? extends vc1<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public gd1(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<vc1<Data, ResourceType, Transcode>> list, mn0<List<Throwable>> mn0) {
        this.f1293a = mn0;
        ik1.c(list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public id1<Transcode> a(xb1<Data> xb1, ob1 ob1, int i, int i2, vc1.a<ResourceType> aVar) throws dd1 {
        List<Throwable> b2 = this.f1293a.b();
        ik1.d(b2);
        List<Throwable> list = b2;
        try {
            return b(xb1, ob1, i, i2, aVar, list);
        } finally {
            this.f1293a.a(list);
        }
    }

    @DexIgnore
    public final id1<Transcode> b(xb1<Data> xb1, ob1 ob1, int i, int i2, vc1.a<ResourceType> aVar, List<Throwable> list) throws dd1 {
        id1<Transcode> id1;
        int size = this.b.size();
        id1<Transcode> id12 = null;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                id1 = id12;
                break;
            }
            try {
                id1 = ((vc1) this.b.get(i3)).a(xb1, i, i2, ob1, aVar);
            } catch (dd1 e) {
                list.add(e);
                id1 = id12;
            }
            if (id1 != null) {
                break;
            }
            i3++;
            id12 = id1;
        }
        if (id1 != null) {
            return id1;
        }
        throw new dd1(this.c, new ArrayList(list));
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }
}
