package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gy7 extends hy7 implements tv7 {
    @DexIgnore
    public volatile gy7 _immediate;
    @DexIgnore
    public /* final */ gy7 c;
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements dw7 {
        @DexIgnore
        public /* final */ /* synthetic */ gy7 b;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable c;

        @DexIgnore
        public a(gy7 gy7, Runnable runnable) {
            this.b = gy7;
            this.c = runnable;
        }

        @DexIgnore
        @Override // com.fossil.dw7
        public void dispose() {
            this.b.d.removeCallbacks(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ gy7 b;
        @DexIgnore
        public /* final */ /* synthetic */ ku7 c;

        @DexIgnore
        public b(gy7 gy7, ku7 ku7) {
            this.b = gy7;
            this.c = ku7;
        }

        @DexIgnore
        public final void run() {
            this.c.f(this.b, tl7.f3441a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<Throwable, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable $block;
        @DexIgnore
        public /* final */ /* synthetic */ gy7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gy7 gy7, Runnable runnable) {
            super(1);
            this.this$0 = gy7;
            this.$block = runnable;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
            invoke(th);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.this$0.d.removeCallbacks(this.$block);
        }
    }

    @DexIgnore
    public gy7(Handler handler, String str) {
        this(handler, str, false);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gy7(Handler handler, String str, boolean z) {
        super(null);
        gy7 gy7 = null;
        this.d = handler;
        this.e = str;
        this.f = z;
        this._immediate = z ? this : gy7;
        gy7 gy72 = this._immediate;
        if (gy72 == null) {
            gy72 = new gy7(this.d, this.e, true);
            this._immediate = gy72;
        }
        this.c = gy72;
    }

    @DexIgnore
    @Override // com.fossil.tv7, com.fossil.hy7
    public dw7 G(long j, Runnable runnable) {
        this.d.postDelayed(runnable, bs7.h(j, 4611686018427387903L));
        return new a(this, runnable);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void M(tn7 tn7, Runnable runnable) {
        this.d.post(runnable);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public boolean Q(tn7 tn7) {
        return !this.f || (pq7.a(Looper.myLooper(), this.d.getLooper()) ^ true);
    }

    @DexIgnore
    /* renamed from: V */
    public gy7 S() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof gy7) && ((gy7) obj).d == this.d;
    }

    @DexIgnore
    @Override // com.fossil.tv7
    public void f(long j, ku7<? super tl7> ku7) {
        b bVar = new b(this, ku7);
        this.d.postDelayed(bVar, bs7.h(j, 4611686018427387903L));
        ku7.e(new c(this, bVar));
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(this.d);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public String toString() {
        String str = this.e;
        if (str == null) {
            return this.d.toString();
        }
        if (!this.f) {
            return str;
        }
        return this.e + " [immediate]";
    }
}
