package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a21 implements k11 {
    @DexIgnore
    public static /* final */ String l; // = x01.f("SystemAlarmDispatcher");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ k41 c;
    @DexIgnore
    public /* final */ g41 d;
    @DexIgnore
    public /* final */ m11 e;
    @DexIgnore
    public /* final */ s11 f;
    @DexIgnore
    public /* final */ x11 g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ List<Intent> i;
    @DexIgnore
    public Intent j;
    @DexIgnore
    public c k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            a21 a21;
            d dVar;
            synchronized (a21.this.i) {
                a21.this.j = a21.this.i.get(0);
            }
            Intent intent = a21.this.j;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = a21.this.j.getIntExtra("KEY_START_ID", 0);
                x01.c().a(a21.l, String.format("Processing command %s, %s", a21.this.j, Integer.valueOf(intExtra)), new Throwable[0]);
                PowerManager.WakeLock b2 = d41.b(a21.this.b, String.format("%s (%s)", action, Integer.valueOf(intExtra)));
                try {
                    x01.c().a(a21.l, String.format("Acquiring operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.acquire();
                    a21.this.g.p(a21.this.j, intExtra, a21.this);
                    x01.c().a(a21.l, String.format("Releasing operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.release();
                    a21 = a21.this;
                    dVar = new d(a21);
                } catch (Throwable th) {
                    x01.c().a(a21.l, String.format("Releasing operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.release();
                    a21 a212 = a21.this;
                    a212.k(new d(a212));
                    throw th;
                }
                a21.k(dVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ a21 b;
        @DexIgnore
        public /* final */ Intent c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public b(a21 a21, Intent intent, int i) {
            this.b = a21;
            this.c = intent;
            this.d = i;
        }

        @DexIgnore
        public void run() {
            this.b.a(this.c, this.d);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ a21 b;

        @DexIgnore
        public d(a21 a21) {
            this.b = a21;
        }

        @DexIgnore
        public void run() {
            this.b.c();
        }
    }

    @DexIgnore
    public a21(Context context) {
        this(context, null, null);
    }

    @DexIgnore
    public a21(Context context, m11 m11, s11 s11) {
        this.b = context.getApplicationContext();
        this.g = new x11(this.b);
        this.d = new g41();
        s11 = s11 == null ? s11.l(context) : s11;
        this.f = s11;
        this.e = m11 == null ? s11.n() : m11;
        this.c = this.f.q();
        this.e.b(this);
        this.i = new ArrayList();
        this.j = null;
        this.h = new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public boolean a(Intent intent, int i2) {
        boolean z = false;
        x01.c().a(l, String.format("Adding command %s (%s)", intent, Integer.valueOf(i2)), new Throwable[0]);
        b();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            x01.c().h(l, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && i("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.i) {
                if (!this.i.isEmpty()) {
                    z = true;
                }
                this.i.add(intent);
                if (!z) {
                    l();
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final void b() {
        if (this.h.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }

    @DexIgnore
    public void c() {
        x01.c().a(l, "Checking if commands are complete.", new Throwable[0]);
        b();
        synchronized (this.i) {
            if (this.j != null) {
                x01.c().a(l, String.format("Removing command %s", this.j), new Throwable[0]);
                if (this.i.remove(0).equals(this.j)) {
                    this.j = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            a41 c2 = this.c.c();
            if (!this.g.o() && this.i.isEmpty() && !c2.a()) {
                x01.c().a(l, "No more commands & intents.", new Throwable[0]);
                if (this.k != null) {
                    this.k.a();
                }
            } else if (!this.i.isEmpty()) {
                l();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k11
    public void d(String str, boolean z) {
        k(new b(this, x11.c(this.b, str, z), 0));
    }

    @DexIgnore
    public m11 e() {
        return this.e;
    }

    @DexIgnore
    public k41 f() {
        return this.c;
    }

    @DexIgnore
    public s11 g() {
        return this.f;
    }

    @DexIgnore
    public g41 h() {
        return this.d;
    }

    @DexIgnore
    public final boolean i(String str) {
        b();
        synchronized (this.i) {
            for (Intent intent : this.i) {
                if (str.equals(intent.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void j() {
        x01.c().a(l, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.e.h(this);
        this.d.a();
        this.k = null;
    }

    @DexIgnore
    public void k(Runnable runnable) {
        this.h.post(runnable);
    }

    @DexIgnore
    public final void l() {
        b();
        PowerManager.WakeLock b2 = d41.b(this.b, "ProcessCommand");
        try {
            b2.acquire();
            this.f.q().b(new a());
        } finally {
            b2.release();
        }
    }

    @DexIgnore
    public void m(c cVar) {
        if (this.k != null) {
            x01.c().b(l, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.k = cVar;
        }
    }
}
