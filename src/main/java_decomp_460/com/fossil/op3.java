package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class op3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ or3 b;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 c;

    @DexIgnore
    public op3(fp3 fp3, or3 or3) {
        this.c = fp3;
        this.b = or3;
    }

    @DexIgnore
    public final void run() {
        cl3 cl3 = this.c.d;
        if (cl3 == null) {
            this.c.d().F().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            cl3.r1(this.b);
            this.c.t().J();
            this.c.M(cl3, null, this.b);
            this.c.e0();
        } catch (RemoteException e) {
            this.c.d().F().b("Failed to send app launch to the service", e);
        }
    }
}
