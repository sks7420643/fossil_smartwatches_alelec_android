package com.fossil;

import com.fossil.h34;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d34<K, V> extends h34.b<K> {
    @DexIgnore
    @Weak
    public /* final */ a34<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ a34<K, ?> map;

        @DexIgnore
        public a(a34<K, ?> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.keySet();
        }
    }

    @DexIgnore
    public d34(a34<K, V> a34) {
        this.map = a34;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.h34.b
    public K get(int i) {
        return this.map.entrySet().asList().get(i).getKey();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, com.fossil.h34.b, com.fossil.h34.b, java.lang.Iterable
    public h54<K> iterator() {
        return this.map.keyIterator();
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.h34
    public Object writeReplace() {
        return new a(this.map);
    }
}
