package com.fossil;

import com.facebook.share.internal.VideoUploader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo7 implements ts7<File> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f3978a;
    @DexIgnore
    public /* final */ yo7 b;
    @DexIgnore
    public /* final */ rp7<File, Boolean> c;
    @DexIgnore
    public /* final */ rp7<File, tl7> d;
    @DexIgnore
    public /* final */ vp7<File, IOException, tl7> e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends c {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(File file) {
            super(file);
            pq7.c(file, "rootDir");
            if (vl7.f3781a) {
                boolean isDirectory = file.isDirectory();
                if (vl7.f3781a && !isDirectory) {
                    throw new AssertionError("rootDir must be verified to be directory beforehand.");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends yl7<File> {
        @DexIgnore
        public /* final */ ArrayDeque<c> d; // = new ArrayDeque<>();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends a {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public boolean e;
            @DexIgnore
            public /* final */ /* synthetic */ b f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, File file) {
                super(file);
                pq7.c(file, "rootDir");
                this.f = bVar;
            }

            @DexIgnore
            @Override // com.fossil.wo7.c
            public File b() {
                if (!this.e && this.c == null) {
                    rp7 rp7 = wo7.this.c;
                    if (rp7 != null && !((Boolean) rp7.invoke(a())).booleanValue()) {
                        return null;
                    }
                    File[] listFiles = a().listFiles();
                    this.c = listFiles;
                    if (listFiles == null) {
                        vp7 vp7 = wo7.this.e;
                        if (vp7 != null) {
                            tl7 tl7 = (tl7) vp7.invoke(a(), new qo7(a(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.e = true;
                    }
                }
                File[] fileArr = this.c;
                if (fileArr != null) {
                    int i = this.d;
                    if (fileArr == null) {
                        pq7.i();
                        throw null;
                    } else if (i < fileArr.length) {
                        if (fileArr != null) {
                            this.d = i + 1;
                            return fileArr[i];
                        }
                        pq7.i();
                        throw null;
                    }
                }
                if (!this.b) {
                    this.b = true;
                    return a();
                }
                rp7 rp72 = wo7.this.d;
                if (rp72 == null) {
                    return null;
                }
                tl7 tl72 = (tl7) rp72.invoke(a());
                return null;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wo7$b$b")
        /* renamed from: com.fossil.wo7$b$b  reason: collision with other inner class name */
        public final class C0276b extends c {
            @DexIgnore
            public boolean b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0276b(b bVar, File file) {
                super(file);
                pq7.c(file, "rootFile");
                if (vl7.f3781a) {
                    boolean isFile = file.isFile();
                    if (vl7.f3781a && !isFile) {
                        throw new AssertionError("rootFile must be verified to be file beforehand.");
                    }
                }
            }

            @DexIgnore
            @Override // com.fossil.wo7.c
            public File b() {
                if (this.b) {
                    return null;
                }
                this.b = true;
                return a();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class c extends a {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, File file) {
                super(file);
                pq7.c(file, "rootDir");
                this.e = bVar;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
                if (r0.length == 0) goto L_0x006d;
             */
            @DexIgnore
            @Override // com.fossil.wo7.c
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.io.File b() {
                /*
                    r8 = this;
                    r2 = 0
                    boolean r0 = r8.b
                    if (r0 != 0) goto L_0x0028
                    com.fossil.wo7$b r0 = r8.e
                    com.fossil.wo7 r0 = com.fossil.wo7.this
                    com.fossil.rp7 r0 = com.fossil.wo7.c(r0)
                    if (r0 == 0) goto L_0x0020
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x0020
                L_0x001f:
                    return r2
                L_0x0020:
                    r0 = 1
                    r8.b = r0
                    java.io.File r2 = r8.a()
                    goto L_0x001f
                L_0x0028:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x0033
                    int r1 = r8.d
                    if (r0 == 0) goto L_0x0097
                    int r0 = r0.length
                    if (r1 >= r0) goto L_0x0082
                L_0x0033:
                    java.io.File[] r0 = r8.c
                    if (r0 != 0) goto L_0x009f
                    java.io.File r0 = r8.a()
                    java.io.File[] r0 = r0.listFiles()
                    r8.c = r0
                    if (r0 != 0) goto L_0x0064
                    com.fossil.wo7$b r0 = r8.e
                    com.fossil.wo7 r0 = com.fossil.wo7.this
                    com.fossil.vp7 r6 = com.fossil.wo7.d(r0)
                    if (r6 == 0) goto L_0x0064
                    java.io.File r7 = r8.a()
                    com.fossil.qo7 r0 = new com.fossil.qo7
                    java.io.File r1 = r8.a()
                    java.lang.String r3 = "Cannot list files in a directory"
                    r4 = 2
                    r5 = r2
                    r0.<init>(r1, r2, r3, r4, r5)
                    java.lang.Object r0 = r6.invoke(r7, r0)
                    com.fossil.tl7 r0 = (com.fossil.tl7) r0
                L_0x0064:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x006d
                    if (r0 == 0) goto L_0x009b
                    int r0 = r0.length
                    if (r0 != 0) goto L_0x009f
                L_0x006d:
                    com.fossil.wo7$b r0 = r8.e
                    com.fossil.wo7 r0 = com.fossil.wo7.this
                    com.fossil.rp7 r0 = com.fossil.wo7.e(r0)
                    if (r0 == 0) goto L_0x001f
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    com.fossil.tl7 r0 = (com.fossil.tl7) r0
                    goto L_0x001f
                L_0x0082:
                    com.fossil.wo7$b r0 = r8.e
                    com.fossil.wo7 r0 = com.fossil.wo7.this
                    com.fossil.rp7 r0 = com.fossil.wo7.e(r0)
                    if (r0 == 0) goto L_0x001f
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    com.fossil.tl7 r0 = (com.fossil.tl7) r0
                    goto L_0x001f
                L_0x0097:
                    com.fossil.pq7.i()
                    throw r2
                L_0x009b:
                    com.fossil.pq7.i()
                    throw r2
                L_0x009f:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x00ad
                    int r1 = r8.d
                    int r2 = r1 + 1
                    r8.d = r2
                    r2 = r0[r1]
                    goto L_0x001f
                L_0x00ad:
                    com.fossil.pq7.i()
                    throw r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo7.b.c.b():java.io.File");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
            if (wo7.this.f3978a.isDirectory()) {
                this.d.push(f(wo7.this.f3978a));
            } else if (wo7.this.f3978a.isFile()) {
                this.d.push(new C0276b(this, wo7.this.f3978a));
            } else {
                b();
            }
        }

        @DexIgnore
        @Override // com.fossil.yl7
        public void a() {
            File g = g();
            if (g != null) {
                c(g);
            } else {
                b();
            }
        }

        @DexIgnore
        public final a f(File file) {
            int i = xo7.f4153a[wo7.this.b.ordinal()];
            if (i == 1) {
                return new c(this, file);
            }
            if (i == 2) {
                return new a(this, file);
            }
            throw new al7();
        }

        @DexIgnore
        public final File g() {
            File b;
            while (true) {
                c peek = this.d.peek();
                if (peek == null) {
                    return null;
                }
                b = peek.b();
                if (b == null) {
                    this.d.pop();
                } else if (!pq7.a(b, peek.a()) && b.isDirectory() && this.d.size() < wo7.this.f) {
                    this.d.push(f(b));
                }
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ File f3979a;

        @DexIgnore
        public c(File file) {
            pq7.c(file, "root");
            this.f3979a = file;
        }

        @DexIgnore
        public final File a() {
            return this.f3979a;
        }

        @DexIgnore
        public abstract File b();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public wo7(File file, yo7 yo7) {
        this(file, yo7, null, null, null, 0, 32, null);
        pq7.c(file, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        pq7.c(yo7, "direction");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.rp7<? super java.io.File, java.lang.Boolean> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.rp7<? super java.io.File, com.fossil.tl7> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.vp7<? super java.io.File, ? super java.io.IOException, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public wo7(File file, yo7 yo7, rp7<? super File, Boolean> rp7, rp7<? super File, tl7> rp72, vp7<? super File, ? super IOException, tl7> vp7, int i) {
        this.f3978a = file;
        this.b = yo7;
        this.c = rp7;
        this.d = rp72;
        this.e = vp7;
        this.f = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wo7(File file, yo7 yo7, rp7 rp7, rp7 rp72, vp7 vp7, int i, int i2, kq7 kq7) {
        this(file, (i2 & 2) != 0 ? yo7.TOP_DOWN : yo7, rp7, rp72, vp7, (i2 & 32) != 0 ? Integer.MAX_VALUE : i);
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<File> iterator() {
        return new b();
    }
}
