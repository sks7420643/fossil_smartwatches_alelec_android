package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue0 {
    @DexIgnore
    public static /* final */ int[] ActionBar; // = {2130968728, 2130968738, 2130968739, 2130968979, 2130968980, 2130968981, 2130968982, 2130968983, 2130968984, 2130969066, 2130969111, 2130969112, 2130969137, 2130969237, 2130969244, 2130969252, 2130969253, 2130969255, 2130969273, 2130969305, 2130969427, 2130969470, 2130969510, 2130969518, 2130969519, 2130969710, 2130969713, 2130969821, 2130969831};
    @DexIgnore
    public static /* final */ int[] ActionBarLayout; // = {16842931};
    @DexIgnore
    public static /* final */ int ActionBarLayout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundStacked; // = 2;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEnd; // = 3;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEndWithActions; // = 4;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetLeft; // = 5;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetRight; // = 6;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStart; // = 7;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStartWithNavigation; // = 8;
    @DexIgnore
    public static /* final */ int ActionBar_customNavigationLayout; // = 9;
    @DexIgnore
    public static /* final */ int ActionBar_displayOptions; // = 10;
    @DexIgnore
    public static /* final */ int ActionBar_divider; // = 11;
    @DexIgnore
    public static /* final */ int ActionBar_elevation; // = 12;
    @DexIgnore
    public static /* final */ int ActionBar_height; // = 13;
    @DexIgnore
    public static /* final */ int ActionBar_hideOnContentScroll; // = 14;
    @DexIgnore
    public static /* final */ int ActionBar_homeAsUpIndicator; // = 15;
    @DexIgnore
    public static /* final */ int ActionBar_homeLayout; // = 16;
    @DexIgnore
    public static /* final */ int ActionBar_icon; // = 17;
    @DexIgnore
    public static /* final */ int ActionBar_indeterminateProgressStyle; // = 18;
    @DexIgnore
    public static /* final */ int ActionBar_itemPadding; // = 19;
    @DexIgnore
    public static /* final */ int ActionBar_logo; // = 20;
    @DexIgnore
    public static /* final */ int ActionBar_navigationMode; // = 21;
    @DexIgnore
    public static /* final */ int ActionBar_popupTheme; // = 22;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarPadding; // = 23;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarStyle; // = 24;
    @DexIgnore
    public static /* final */ int ActionBar_subtitle; // = 25;
    @DexIgnore
    public static /* final */ int ActionBar_subtitleTextStyle; // = 26;
    @DexIgnore
    public static /* final */ int ActionBar_title; // = 27;
    @DexIgnore
    public static /* final */ int ActionBar_titleTextStyle; // = 28;
    @DexIgnore
    public static /* final */ int[] ActionMenuItemView; // = {16843071};
    @DexIgnore
    public static /* final */ int ActionMenuItemView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int[] ActionMenuView; // = new int[0];
    @DexIgnore
    public static /* final */ int[] ActionMode; // = {2130968728, 2130968738, 2130968924, 2130969237, 2130969713, 2130969831};
    @DexIgnore
    public static /* final */ int ActionMode_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionMode_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionMode_closeItemLayout; // = 2;
    @DexIgnore
    public static /* final */ int ActionMode_height; // = 3;
    @DexIgnore
    public static /* final */ int ActionMode_subtitleTextStyle; // = 4;
    @DexIgnore
    public static /* final */ int ActionMode_titleTextStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ActivityChooserView; // = {2130969160, 2130969284};
    @DexIgnore
    public static /* final */ int ActivityChooserView_expandActivityOverflowButtonDrawable; // = 0;
    @DexIgnore
    public static /* final */ int ActivityChooserView_initialActivityCount; // = 1;
    @DexIgnore
    public static /* final */ int[] AlertDialog; // = {16842994, 2130968848, 2130968849, 2130969412, 2130969413, 2130969463, 2130969621, 2130969623};
    @DexIgnore
    public static /* final */ int AlertDialog_android_layout; // = 0;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonIconDimen; // = 1;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonPanelSideLayout; // = 2;
    @DexIgnore
    public static /* final */ int AlertDialog_listItemLayout; // = 3;
    @DexIgnore
    public static /* final */ int AlertDialog_listLayout; // = 4;
    @DexIgnore
    public static /* final */ int AlertDialog_multiChoiceItemLayout; // = 5;
    @DexIgnore
    public static /* final */ int AlertDialog_showTitle; // = 6;
    @DexIgnore
    public static /* final */ int AlertDialog_singleChoiceItemLayout; // = 7;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableCompat; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableItem; // = {16842960, 16843161};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_drawable; // = 1;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_id; // = 0;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableTransition; // = {16843161, 16843849, 16843850, 16843851};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_fromId; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_reversible; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_toId; // = 1;
    @DexIgnore
    public static /* final */ int[] AppCompatImageView; // = {16843033, 2130969685, 2130969817, 2130969818};
    @DexIgnore
    public static /* final */ int AppCompatImageView_android_src; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatImageView_srcCompat; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatSeekBar; // = {16843074, 2130969813, 2130969814, 2130969815};
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_android_thumb; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMark; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatTextHelper; // = {16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667};
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableBottom; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableEnd; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableLeft; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableRight; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableStart; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableTop; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int[] AppCompatTextView; // = {16842804, 2130968719, 2130968720, 2130968721, 2130968722, 2130968723, 2130969118, 2130969119, 2130969120, 2130969121, 2130969123, 2130969124, 2130969125, 2130969126, 2130969185, 2130969197, 2130969205, 2130969323, 2130969405, 2130969758, 2130969796};
    @DexIgnore
    public static /* final */ int AppCompatTextView_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMaxTextSize; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMinTextSize; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizePresetSizes; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeStepGranularity; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeTextType; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableBottomCompat; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableEndCompat; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableLeftCompat; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableRightCompat; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableStartCompat; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTint; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTintMode; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTopCompat; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTextView_firstBaselineToTopHeight; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontFamily; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontVariationSettings; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lastBaselineToBottomHeight; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lineHeight; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textAllCaps; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textLocale; // = 20;
    @DexIgnore
    public static /* final */ int[] AppCompatTheme; // = {16842839, 16842926, 2130968623, 2130968624, 2130968625, 2130968626, 2130968627, 2130968628, 2130968629, 2130968630, 2130968631, 2130968632, 2130968633, 2130968634, 2130968635, 2130968637, 2130968638, 2130968639, 2130968640, 2130968641, 2130968642, 2130968643, 2130968644, 2130968645, 2130968646, 2130968647, 2130968648, 2130968649, 2130968650, 2130968651, 2130968652, 2130968653, 2130968660, 2130968700, 2130968701, 2130968702, 2130968703, 2130968717, 2130968822, 2130968841, 2130968842, 2130968843, 2130968844, 2130968845, 2130968851, 2130968852, 2130968887, 2130968894, 2130968931, 2130968932, 2130968933, 2130968934, 2130968935, 2130968936, 2130968937, 2130968944, 2130968945, 2130968952, 2130968991, 2130969108, 2130969109, 2130969110, 2130969113, 2130969115, 2130969128, 2130969129, 2130969132, 2130969133, 2130969134, 2130969252, 2130969271, 2130969408, 2130969409, 2130969410, 2130969411, 2130969414, 2130969415, 2130969416, 2130969417, 2130969418, 2130969419, 2130969420, 2130969421, 2130969422, 2130969498, 2130969499, 2130969500, 2130969509, 2130969511, 2130969534, 2130969538, 2130969539, 2130969540, 2130969585, 2130969586, 2130969587, 2130969588, 2130969682, 2130969683, 2130969720, 2130969769, 2130969771, 2130969772, 2130969773, 2130969775, 2130969776, 2130969777, 2130969778, 2130969786, 2130969787, 2130969833, 2130969834, 2130969835, 2130969836, 2130969863, 2130969946, 2130969947, 2130969948, 2130969949, 2130969950, 2130969951, 2130969952, 2130969953, 2130969954, 2130969955};
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarDivider; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarItemBackground; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarPopupTheme; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSize; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSplitStyle; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarStyle; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabBarStyle; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabStyle; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabTextStyle; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTheme; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarWidgetTheme; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionButtonStyle; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionDropDownStyle; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextAppearance; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextColor; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeBackground; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseButtonStyle; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseDrawable; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCopyDrawable; // = 20;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCutDrawable; // = 21;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeFindDrawable; // = 22;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePasteDrawable; // = 23;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePopupWindowStyle; // = 24;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSelectAllDrawable; // = 25;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeShareDrawable; // = 26;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSplitBackground; // = 27;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeStyle; // = 28;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeWebSearchDrawable; // = 29;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowButtonStyle; // = 30;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowMenuStyle; // = 31;
    @DexIgnore
    public static /* final */ int AppCompatTheme_activityChooserViewStyle; // = 32;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogButtonGroupStyle; // = 33;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogCenterButtons; // = 34;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogStyle; // = 35;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogTheme; // = 36;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowIsFloating; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTheme_autoCompleteTextViewStyle; // = 37;
    @DexIgnore
    public static /* final */ int AppCompatTheme_borderlessButtonStyle; // = 38;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarButtonStyle; // = 39;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNegativeButtonStyle; // = 40;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNeutralButtonStyle; // = 41;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarPositiveButtonStyle; // = 42;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarStyle; // = 43;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyle; // = 44;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyleSmall; // = 45;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkboxStyle; // = 46;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkedTextViewStyle; // = 47;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorAccent; // = 48;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorBackgroundFloating; // = 49;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorButtonNormal; // = 50;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlActivated; // = 51;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlHighlight; // = 52;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlNormal; // = 53;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorError; // = 54;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimary; // = 55;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimaryDark; // = 56;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorSwitchThumbNormal; // = 57;
    @DexIgnore
    public static /* final */ int AppCompatTheme_controlBackground; // = 58;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogCornerRadius; // = 59;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogPreferredPadding; // = 60;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogTheme; // = 61;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerHorizontal; // = 62;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerVertical; // = 63;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropDownListViewStyle; // = 64;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropdownListPreferredItemHeight; // = 65;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextBackground; // = 66;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextColor; // = 67;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextStyle; // = 68;
    @DexIgnore
    public static /* final */ int AppCompatTheme_homeAsUpIndicator; // = 69;
    @DexIgnore
    public static /* final */ int AppCompatTheme_imageButtonStyle; // = 70;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceBackgroundIndicator; // = 71;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceIndicatorMultipleAnimated; // = 72;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceIndicatorSingleAnimated; // = 73;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listDividerAlertDialog; // = 74;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listMenuViewStyle; // = 75;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPopupWindowStyle; // = 76;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeight; // = 77;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightLarge; // = 78;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightSmall; // = 79;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingEnd; // = 80;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingLeft; // = 81;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingRight; // = 82;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingStart; // = 83;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelBackground; // = 84;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListTheme; // = 85;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListWidth; // = 86;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupMenuStyle; // = 87;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupWindowStyle; // = 88;
    @DexIgnore
    public static /* final */ int AppCompatTheme_radioButtonStyle; // = 89;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyle; // = 90;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleIndicator; // = 91;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleSmall; // = 92;
    @DexIgnore
    public static /* final */ int AppCompatTheme_searchViewStyle; // = 93;
    @DexIgnore
    public static /* final */ int AppCompatTheme_seekBarStyle; // = 94;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackground; // = 95;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackgroundBorderless; // = 96;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerDropDownItemStyle; // = 97;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerStyle; // = 98;
    @DexIgnore
    public static /* final */ int AppCompatTheme_switchStyle; // = 99;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceLargePopupMenu; // = 100;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItem; // = 101;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSecondary; // = 102;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSmall; // = 103;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearancePopupMenuHeader; // = 104;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultSubtitle; // = 105;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultTitle; // = 106;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSmallPopupMenu; // = 107;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorAlertDialogListItem; // = 108;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorSearchUrl; // = 109;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarNavigationButtonStyle; // = 110;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarStyle; // = 111;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipForegroundColor; // = 112;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipFrameBackground; // = 113;
    @DexIgnore
    public static /* final */ int AppCompatTheme_viewInflaterClass; // = 114;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBar; // = 115;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBarOverlay; // = 116;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionModeOverlay; // = 117;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMajor; // = 118;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMinor; // = 119;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMajor; // = 120;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMinor; // = 121;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMajor; // = 122;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMinor; // = 123;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowNoTitle; // = 124;
    @DexIgnore
    public static /* final */ int[] ButtonBarLayout; // = {2130968706};
    @DexIgnore
    public static /* final */ int ButtonBarLayout_allowStacking; // = 0;
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, 2130968707};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] CompoundButton; // = {16843015, 2130968846, 2130968853, 2130968854};
    @DexIgnore
    public static /* final */ int CompoundButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonCompat; // = 1;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTint; // = 2;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] DrawerArrowToggle; // = {2130968714, 2130968715, 2130968747, 2130968930, 2130969122, 2130969213, 2130969681, 2130969809};
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowHeadLength; // = 0;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowShaftLength; // = 1;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_barLength; // = 2;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_color; // = 3;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_drawableSize; // = 4;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_gapBetweenBars; // = 5;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_spinBars; // = 6;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_thickness; // = 7;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {2130969198, 2130969199, 2130969200, 2130969201, 2130969202, 2130969203};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, 2130969196, 2130969204, 2130969205, 2130969206, 2130969843};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat; // = {16842927, 16842948, 16843046, 16843047, 16843048, 2130969112, 2130969114, 2130969458, 2130969618};
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat_Layout; // = {16842931, 16842996, 16842997, 16843137};
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_height; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_weight; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_width; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAligned; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAlignedChildIndex; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_orientation; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_weightSum; // = 4;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_divider; // = 5;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_dividerPadding; // = 6;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_measureWithLargestChild; // = 7;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_showDividers; // = 8;
    @DexIgnore
    public static /* final */ int[] ListPopupWindow; // = {16843436, 16843437};
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownHorizontalOffset; // = 0;
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownVerticalOffset; // = 1;
    @DexIgnore
    public static /* final */ int[] MenuGroup; // = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    @DexIgnore
    public static /* final */ int MenuGroup_android_checkableBehavior; // = 5;
    @DexIgnore
    public static /* final */ int MenuGroup_android_enabled; // = 0;
    @DexIgnore
    public static /* final */ int MenuGroup_android_id; // = 1;
    @DexIgnore
    public static /* final */ int MenuGroup_android_menuCategory; // = 3;
    @DexIgnore
    public static /* final */ int MenuGroup_android_orderInCategory; // = 4;
    @DexIgnore
    public static /* final */ int MenuGroup_android_visible; // = 2;
    @DexIgnore
    public static /* final */ int[] MenuItem; // = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, 2130968636, 2130968654, 2130968656, 2130968709, 2130968978, 2130969261, 2130969262, 2130969490, 2130969617, 2130969837};
    @DexIgnore
    public static /* final */ int MenuItem_actionLayout; // = 13;
    @DexIgnore
    public static /* final */ int MenuItem_actionProviderClass; // = 14;
    @DexIgnore
    public static /* final */ int MenuItem_actionViewClass; // = 15;
    @DexIgnore
    public static /* final */ int MenuItem_alphabeticModifiers; // = 16;
    @DexIgnore
    public static /* final */ int MenuItem_android_alphabeticShortcut; // = 9;
    @DexIgnore
    public static /* final */ int MenuItem_android_checkable; // = 11;
    @DexIgnore
    public static /* final */ int MenuItem_android_checked; // = 3;
    @DexIgnore
    public static /* final */ int MenuItem_android_enabled; // = 1;
    @DexIgnore
    public static /* final */ int MenuItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int MenuItem_android_id; // = 2;
    @DexIgnore
    public static /* final */ int MenuItem_android_menuCategory; // = 5;
    @DexIgnore
    public static /* final */ int MenuItem_android_numericShortcut; // = 10;
    @DexIgnore
    public static /* final */ int MenuItem_android_onClick; // = 12;
    @DexIgnore
    public static /* final */ int MenuItem_android_orderInCategory; // = 6;
    @DexIgnore
    public static /* final */ int MenuItem_android_title; // = 7;
    @DexIgnore
    public static /* final */ int MenuItem_android_titleCondensed; // = 8;
    @DexIgnore
    public static /* final */ int MenuItem_android_visible; // = 4;
    @DexIgnore
    public static /* final */ int MenuItem_contentDescription; // = 17;
    @DexIgnore
    public static /* final */ int MenuItem_iconTint; // = 18;
    @DexIgnore
    public static /* final */ int MenuItem_iconTintMode; // = 19;
    @DexIgnore
    public static /* final */ int MenuItem_numericModifiers; // = 20;
    @DexIgnore
    public static /* final */ int MenuItem_showAsAction; // = 21;
    @DexIgnore
    public static /* final */ int MenuItem_tooltipText; // = 22;
    @DexIgnore
    public static /* final */ int[] MenuView; // = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, 2130969512, 2130969708};
    @DexIgnore
    public static /* final */ int MenuView_android_headerBackground; // = 4;
    @DexIgnore
    public static /* final */ int MenuView_android_horizontalDivider; // = 2;
    @DexIgnore
    public static /* final */ int MenuView_android_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int MenuView_android_itemIconDisabledAlpha; // = 6;
    @DexIgnore
    public static /* final */ int MenuView_android_itemTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int MenuView_android_verticalDivider; // = 3;
    @DexIgnore
    public static /* final */ int MenuView_android_windowAnimationStyle; // = 0;
    @DexIgnore
    public static /* final */ int MenuView_preserveIconSpacing; // = 7;
    @DexIgnore
    public static /* final */ int MenuView_subMenuArrow; // = 8;
    @DexIgnore
    public static /* final */ int[] PopupWindow; // = {16843126, 16843465, 2130969491};
    @DexIgnore
    public static /* final */ int[] PopupWindowBackgroundState; // = {2130969694};
    @DexIgnore
    public static /* final */ int PopupWindowBackgroundState_state_above_anchor; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupBackground; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_overlapAnchor; // = 2;
    @DexIgnore
    public static /* final */ int[] RecycleListView; // = {2130969493, 2130969496};
    @DexIgnore
    public static /* final */ int RecycleListView_paddingBottomNoButtons; // = 0;
    @DexIgnore
    public static /* final */ int RecycleListView_paddingTopNoTitle; // = 1;
    @DexIgnore
    public static /* final */ int[] SearchView; // = {16842970, 16843039, 16843296, 16843364, 2130968917, 2130968965, 2130969097, 2130969215, 2130969267, 2130969328, 2130969532, 2130969533, 2130969582, 2130969583, 2130969709, 2130969714, 2130969879};
    @DexIgnore
    public static /* final */ int SearchView_android_focusable; // = 0;
    @DexIgnore
    public static /* final */ int SearchView_android_imeOptions; // = 3;
    @DexIgnore
    public static /* final */ int SearchView_android_inputType; // = 2;
    @DexIgnore
    public static /* final */ int SearchView_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int SearchView_closeIcon; // = 4;
    @DexIgnore
    public static /* final */ int SearchView_commitIcon; // = 5;
    @DexIgnore
    public static /* final */ int SearchView_defaultQueryHint; // = 6;
    @DexIgnore
    public static /* final */ int SearchView_goIcon; // = 7;
    @DexIgnore
    public static /* final */ int SearchView_iconifiedByDefault; // = 8;
    @DexIgnore
    public static /* final */ int SearchView_layout; // = 9;
    @DexIgnore
    public static /* final */ int SearchView_queryBackground; // = 10;
    @DexIgnore
    public static /* final */ int SearchView_queryHint; // = 11;
    @DexIgnore
    public static /* final */ int SearchView_searchHintIcon; // = 12;
    @DexIgnore
    public static /* final */ int SearchView_searchIcon; // = 13;
    @DexIgnore
    public static /* final */ int SearchView_submitBackground; // = 14;
    @DexIgnore
    public static /* final */ int SearchView_suggestionRowLayout; // = 15;
    @DexIgnore
    public static /* final */ int SearchView_voiceIcon; // = 16;
    @DexIgnore
    public static /* final */ int[] Spinner; // = {16842930, 16843126, 16843131, 16843362, 2130969510};
    @DexIgnore
    public static /* final */ int Spinner_android_dropDownWidth; // = 3;
    @DexIgnore
    public static /* final */ int Spinner_android_entries; // = 0;
    @DexIgnore
    public static /* final */ int Spinner_android_popupBackground; // = 1;
    @DexIgnore
    public static /* final */ int Spinner_android_prompt; // = 2;
    @DexIgnore
    public static /* final */ int Spinner_popupTheme; // = 4;
    @DexIgnore
    public static /* final */ int[] StateListDrawable; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int[] StateListDrawableItem; // = {16843161};
    @DexIgnore
    public static /* final */ int StateListDrawableItem_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] SwitchCompat; // = {16843044, 16843045, 16843074, 2130969620, 2130969684, 2130969718, 2130969719, 2130969721, 2130969810, 2130969811, 2130969812, 2130969839, 2130969840, 2130969841};
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOff; // = 1;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOn; // = 0;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_thumb; // = 2;
    @DexIgnore
    public static /* final */ int SwitchCompat_showText; // = 3;
    @DexIgnore
    public static /* final */ int SwitchCompat_splitTrack; // = 4;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchMinWidth; // = 5;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchPadding; // = 6;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchTextAppearance; // = 7;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTextPadding; // = 8;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTint; // = 9;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTintMode; // = 10;
    @DexIgnore
    public static /* final */ int SwitchCompat_track; // = 11;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTint; // = 12;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTintMode; // = 13;
    @DexIgnore
    public static /* final */ int[] TextAppearance; // = {16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, 16844165, 2130969197, 2130969205, 2130969758, 2130969796};
    @DexIgnore
    public static /* final */ int TextAppearance_android_fontFamily; // = 10;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowColor; // = 6;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDx; // = 7;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDy; // = 8;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowRadius; // = 9;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColor; // = 3;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorHint; // = 4;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorLink; // = 5;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textFontWeight; // = 11;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textSize; // = 0;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textStyle; // = 2;
    @DexIgnore
    public static /* final */ int TextAppearance_android_typeface; // = 1;
    @DexIgnore
    public static /* final */ int TextAppearance_fontFamily; // = 12;
    @DexIgnore
    public static /* final */ int TextAppearance_fontVariationSettings; // = 13;
    @DexIgnore
    public static /* final */ int TextAppearance_textAllCaps; // = 14;
    @DexIgnore
    public static /* final */ int TextAppearance_textLocale; // = 15;
    @DexIgnore
    public static /* final */ int[] Toolbar; // = {16842927, 16843072, 2130968847, 2130968926, 2130968927, 2130968979, 2130968980, 2130968981, 2130968982, 2130968983, 2130968984, 2130969427, 2130969428, 2130969453, 2130969459, 2130969468, 2130969469, 2130969510, 2130969710, 2130969711, 2130969712, 2130969821, 2130969823, 2130969824, 2130969825, 2130969826, 2130969827, 2130969828, 2130969829, 2130969830};
    @DexIgnore
    public static /* final */ int Toolbar_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int Toolbar_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int Toolbar_buttonGravity; // = 2;
    @DexIgnore
    public static /* final */ int Toolbar_collapseContentDescription; // = 3;
    @DexIgnore
    public static /* final */ int Toolbar_collapseIcon; // = 4;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEnd; // = 5;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEndWithActions; // = 6;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetLeft; // = 7;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetRight; // = 8;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStart; // = 9;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStartWithNavigation; // = 10;
    @DexIgnore
    public static /* final */ int Toolbar_logo; // = 11;
    @DexIgnore
    public static /* final */ int Toolbar_logoDescription; // = 12;
    @DexIgnore
    public static /* final */ int Toolbar_maxButtonHeight; // = 13;
    @DexIgnore
    public static /* final */ int Toolbar_menu; // = 14;
    @DexIgnore
    public static /* final */ int Toolbar_navigationContentDescription; // = 15;
    @DexIgnore
    public static /* final */ int Toolbar_navigationIcon; // = 16;
    @DexIgnore
    public static /* final */ int Toolbar_popupTheme; // = 17;
    @DexIgnore
    public static /* final */ int Toolbar_subtitle; // = 18;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextAppearance; // = 19;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextColor; // = 20;
    @DexIgnore
    public static /* final */ int Toolbar_title; // = 21;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargin; // = 22;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginBottom; // = 23;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginEnd; // = 24;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginStart; // = 25;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginTop; // = 26;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargins; // = 27;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextAppearance; // = 28;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextColor; // = 29;
    @DexIgnore
    public static /* final */ int[] View; // = {16842752, 16842970, 2130969494, 2130969495, 2130969807};
    @DexIgnore
    public static /* final */ int[] ViewBackgroundHelper; // = {16842964, 2130968740, 2130968741};
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_android_background; // = 0;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTint; // = 1;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] ViewStubCompat; // = {16842960, 16842994, 16842995};
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_id; // = 0;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_inflatedId; // = 2;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int View_android_focusable; // = 1;
    @DexIgnore
    public static /* final */ int View_android_theme; // = 0;
    @DexIgnore
    public static /* final */ int View_paddingEnd; // = 2;
    @DexIgnore
    public static /* final */ int View_paddingStart; // = 3;
    @DexIgnore
    public static /* final */ int View_theme; // = 4;
}
