package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.HorizontalScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public final class j67 extends HorizontalScrollView {
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public b c;
    @DexIgnore
    public Runnable d; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            j67 j67 = j67.this;
            if (currentTimeMillis - j67.b > 100) {
                j67.b = -1;
                j67.c.a();
                return;
            }
            j67.postDelayed(this, 100);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object onScrollChanged();  // void declaration
    }

    @DexIgnore
    public j67(Context context, b bVar) {
        super(context);
        this.c = bVar;
    }

    @DexIgnore
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        b bVar = this.c;
        if (bVar != null) {
            bVar.onScrollChanged();
            if (this.b == -1) {
                postDelayed(this.d, 100);
            }
            this.b = System.currentTimeMillis();
        }
    }
}
