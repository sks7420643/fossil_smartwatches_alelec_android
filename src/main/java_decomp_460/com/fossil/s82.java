package com.fossil;

import android.os.Bundle;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s82 implements r62.b, r62.c {
    @DexIgnore
    public /* final */ /* synthetic */ h82 b;

    @DexIgnore
    public s82(h82 h82) {
        this.b = h82;
    }

    @DexIgnore
    public /* synthetic */ s82(h82 h82, k82 k82) {
        this(h82);
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void e(Bundle bundle) {
        if (this.b.r.l()) {
            this.b.b.lock();
            try {
                if (this.b.k != null) {
                    this.b.k.e(new q82(this.b));
                    this.b.b.unlock();
                }
            } finally {
                this.b.b.unlock();
            }
        } else {
            this.b.k.e(new q82(this.b));
        }
    }

    @DexIgnore
    @Override // com.fossil.r72
    public final void n(z52 z52) {
        this.b.b.lock();
        try {
            if (this.b.C(z52)) {
                this.b.r();
                this.b.p();
            } else {
                this.b.D(z52);
            }
        } finally {
            this.b.b.unlock();
        }
    }
}
