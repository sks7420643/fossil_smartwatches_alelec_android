package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut4 implements Factory<tt4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<rt4> f3644a;
    @DexIgnore
    public /* final */ Provider<st4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public ut4(Provider<rt4> provider, Provider<st4> provider2, Provider<on5> provider3) {
        this.f3644a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static ut4 a(Provider<rt4> provider, Provider<st4> provider2, Provider<on5> provider3) {
        return new ut4(provider, provider2, provider3);
    }

    @DexIgnore
    public static tt4 c(rt4 rt4, st4 st4, on5 on5) {
        return new tt4(rt4, st4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public tt4 get() {
        return c(this.f3644a.get(), this.b.get(), this.c.get());
    }
}
