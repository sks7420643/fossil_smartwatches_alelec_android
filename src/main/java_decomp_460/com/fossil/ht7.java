package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ht7 {
    @DexIgnore
    wr7 a();

    @DexIgnore
    String getValue();

    @DexIgnore
    ht7 next();
}
