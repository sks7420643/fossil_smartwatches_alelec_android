package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.GraphRequest;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.bz1;
import com.fossil.c02;
import com.fossil.lz1;
import com.fossil.mz1;
import com.fossil.nz1;
import com.fossil.pz1;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.zendesk.sdk.network.Constants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz1 implements b12 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ pd4 f3495a;
    @DexIgnore
    public /* final */ ConnectivityManager b;
    @DexIgnore
    public /* final */ URL c; // = f(az1.c);
    @DexIgnore
    public /* final */ t32 d;
    @DexIgnore
    public /* final */ t32 e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ URL f3496a;
        @DexIgnore
        public /* final */ kz1 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public a(URL url, kz1 kz1, String str) {
            this.f3496a = url;
            this.b = kz1;
            this.c = str;
        }

        @DexIgnore
        public a a(URL url) {
            return new a(url, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3497a;
        @DexIgnore
        public /* final */ URL b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public b(int i, URL url, long j) {
            this.f3497a = i;
            this.b = url;
            this.c = j;
        }
    }

    @DexIgnore
    public tz1(Context context, t32 t32, t32 t322) {
        be4 be4 = new be4();
        be4.g(cz1.f695a);
        be4.h(true);
        this.f3495a = be4.f();
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.d = t322;
        this.e = t32;
        this.f = 40000;
    }

    @DexIgnore
    public static /* synthetic */ a c(a aVar, b bVar) {
        URL url = bVar.b;
        if (url == null) {
            return null;
        }
        c12.a("CctTransportBackend", "Following redirect to: %s", url);
        return aVar.a(bVar.b);
    }

    @DexIgnore
    public static URL f(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e2) {
            throw new IllegalArgumentException("Invalid url: " + str, e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.b12
    public v02 a(u02 u02) {
        String str;
        mz1.a a2;
        HashMap hashMap = new HashMap();
        for (c02 c02 : u02.b()) {
            String j = c02.j();
            if (!hashMap.containsKey(j)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(c02);
                hashMap.put(j, arrayList);
            } else {
                ((List) hashMap.get(j)).add(c02);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry entry : hashMap.entrySet()) {
            c02 c022 = (c02) ((List) entry.getValue()).get(0);
            nz1.a a3 = nz1.a();
            a3.d(qz1.zza);
            a3.b(this.e.a());
            a3.i(this.d.a());
            lz1.a a4 = lz1.a();
            a4.b(lz1.b.zzb);
            bz1.a a5 = bz1.a();
            a5.a(Integer.valueOf(c022.g("sdk-version")));
            a5.g(c022.b(DeviceRequestsHelper.DEVICE_INFO_MODEL));
            a5.e(c022.b("hardware"));
            a5.b(c022.b("device"));
            a5.i(c022.b("product"));
            a5.h(c022.b("os-uild"));
            a5.f(c022.b("manufacturer"));
            a5.d(c022.b("fingerprint"));
            a4.a(a5.c());
            a3.c(a4.c());
            try {
                a3.a(Integer.parseInt((String) entry.getKey()));
            } catch (NumberFormatException e2) {
                a3.j((String) entry.getKey());
            }
            ArrayList arrayList3 = new ArrayList();
            for (c02 c023 : (List) entry.getValue()) {
                b02 e3 = c023.e();
                ty1 b2 = e3.b();
                if (b2.equals(ty1.b("proto"))) {
                    a2 = mz1.b(e3.a());
                } else if (b2.equals(ty1.b("json"))) {
                    a2 = mz1.a(new String(e3.a(), Charset.forName("UTF-8")));
                } else {
                    c12.f("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", b2);
                }
                a2.a(c023.f());
                a2.e(c023.k());
                a2.f(c023.h("tz-offset"));
                pz1.a a6 = pz1.a();
                a6.b(pz1.c.zza(c023.g("net-type")));
                a6.a(pz1.b.zza(c023.g("mobile-subtype")));
                a2.b(a6.c());
                if (c023.d() != null) {
                    a2.c(c023.d());
                }
                arrayList3.add(a2.d());
            }
            a3.g(arrayList3);
            arrayList2.add(a3.h());
        }
        kz1 a7 = kz1.a(arrayList2);
        URL url = this.c;
        if (u02.c() != null) {
            try {
                az1 c2 = az1.c(u02.c());
                str = c2.d() != null ? c2.d() : null;
                if (c2.e() != null) {
                    url = f(c2.e());
                }
            } catch (IllegalArgumentException e4) {
                return v02.a();
            }
        } else {
            str = null;
        }
        try {
            b bVar = (b) e12.a(5, new a(url, a7, str), rz1.a(this), sz1.b());
            if (bVar.f3497a == 200) {
                return v02.d(bVar.c);
            }
            int i = bVar.f3497a;
            return (i >= 500 || i == 404) ? v02.e() : v02.a();
        } catch (IOException e5) {
            c12.c("CctTransportBackend", "Could not make request to the backend", e5);
            return v02.e();
        }
    }

    @DexIgnore
    @Override // com.fossil.b12
    public c02 b(c02 c02) {
        int subtype;
        NetworkInfo activeNetworkInfo = this.b.getActiveNetworkInfo();
        c02.a l = c02.l();
        l.a("sdk-version", Build.VERSION.SDK_INT);
        l.c(DeviceRequestsHelper.DEVICE_INFO_MODEL, Build.MODEL);
        l.c("hardware", Build.HARDWARE);
        l.c("device", Build.DEVICE);
        l.c("product", Build.PRODUCT);
        l.c("os-uild", Build.ID);
        l.c("manufacturer", Build.MANUFACTURER);
        l.c("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        l.b("tz-offset", (long) (TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        l.a("net-type", activeNetworkInfo == null ? pz1.c.zzs.zza() : activeNetworkInfo.getType());
        if (activeNetworkInfo == null) {
            subtype = pz1.b.zza.zza();
        } else {
            subtype = activeNetworkInfo.getSubtype();
            if (subtype == -1) {
                subtype = pz1.b.zzu.zza();
            } else if (pz1.b.zza(subtype) == null) {
                subtype = 0;
            }
        }
        l.a("mobile-subtype", subtype);
        return l.d();
    }

    @DexIgnore
    public final b d(a aVar) throws IOException {
        c12.a("CctTransportBackend", "Making request to: %s", aVar.f3496a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) aVar.f3496a.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.f);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("User-Agent", String.format("datatransport/%s android/", "2.3.0"));
        httpURLConnection.setRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
        httpURLConnection.setRequestProperty("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        String str = aVar.c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        try {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                try {
                    this.f3495a.a(aVar.b, new BufferedWriter(new OutputStreamWriter(gZIPOutputStream)));
                    gZIPOutputStream.close();
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    int responseCode = httpURLConnection.getResponseCode();
                    c12.e("CctTransportBackend", "Status Code: " + responseCode);
                    c12.e("CctTransportBackend", "Content-Type: " + httpURLConnection.getHeaderField("Content-Type"));
                    c12.e("CctTransportBackend", "Content-Encoding: " + httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER));
                    if (responseCode == 302 || responseCode == 301 || responseCode == 307) {
                        return new b(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0);
                    }
                    if (responseCode != 200) {
                        return new b(responseCode, null, 0);
                    }
                    InputStream inputStream = httpURLConnection.getInputStream();
                    try {
                        GZIPInputStream gZIPInputStream = DecompressionHelper.GZIP_ENCODING.equals(httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER)) ? new GZIPInputStream(inputStream) : inputStream;
                        try {
                            b bVar = new b(responseCode, null, oz1.b(new BufferedReader(new InputStreamReader(gZIPInputStream))).a());
                            if (gZIPInputStream != null) {
                                gZIPInputStream.close();
                            }
                            if (inputStream == null) {
                                return bVar;
                            }
                            inputStream.close();
                            return bVar;
                        } catch (Throwable th) {
                        }
                    } catch (Throwable th2) {
                    }
                } catch (Throwable th3) {
                }
                throw th;
                throw th;
                throw th;
                throw th;
            } catch (Throwable th4) {
            }
        } catch (ConnectException | UnknownHostException e2) {
            c12.c("CctTransportBackend", "Couldn't open connection, returning with 500", e2);
            return new b(500, null, 0);
        } catch (rd4 | IOException e3) {
            c12.c("CctTransportBackend", "Couldn't encode request, returning with 400", e3);
            return new b(MFNetworkReturnCode.BAD_REQUEST, null, 0);
        }
    }
}
