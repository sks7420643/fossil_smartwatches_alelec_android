package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ HashMap<cl7<String, lt>, Byte> f3915a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ wc b; // = new wc();

    @DexIgnore
    public final c2 a(String str, byte[] bArr) {
        if (bArr.length < 3) {
            m80.c.a("DeviceEventParser", "Invalid rawData.", new Object[0]);
            return null;
        }
        ot a2 = ot.e.a(bArr[0]);
        lt a3 = lt.p.a(bArr[1]);
        byte b2 = (byte) (bArr[2] & -1);
        if (a2 == null) {
            m80.c.a("DeviceEventParser", "Unknown command type.", new Object[0]);
        }
        if (a3 == null) {
            m80.c.a("DeviceEventParser", "Invalid event type.", new Object[0]);
            return null;
        }
        f3915a.put(new cl7<>(str, a3), Byte.valueOf(b2));
        byte[] k = dm7.k(bArr, 3, bArr.length);
        switch (vc.f3746a[a3.ordinal()]) {
            case 1:
                try {
                    Charset forName = Charset.forName("UTF-8");
                    pq7.b(forName, "Charset.forName(\"UTF-8\")");
                    JSONObject jSONObject = new JSONObject(new String(k, forName)).getJSONObject("req");
                    int i = jSONObject.getInt("id");
                    pq7.b(jSONObject, "requestJSON");
                    return new q2(b2, i, jSONObject);
                } catch (JSONException e) {
                    d90.i.i(e);
                    return null;
                }
            case 2:
                return new o2(b2);
            case 3:
                break;
            case 4:
                try {
                    int i2 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                    do1 a4 = do1.d.a(k[4]);
                    if (a4 == null) {
                        return null;
                    }
                    int i3 = vc.b[a4.ordinal()];
                    if (i3 == 1) {
                        return new y1(b2, i2, new ao1());
                    }
                    if (i3 == 2) {
                        return new y1(b2, i2, new fo1());
                    }
                    if (i3 == 3) {
                        return new y1(b2, i2, new eo1());
                    }
                    if (i3 == 4) {
                        byte b3 = k[5];
                        ByteBuffer order = ByteBuffer.wrap(k, 6, 4).order(ByteOrder.LITTLE_ENDIAN);
                        pq7.b(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
                        return new y1(b2, i2, new go1(b3, order.getInt()));
                    }
                    throw new al7();
                } catch (Exception e2) {
                    d90.i.i(e2);
                    return null;
                }
            case 5:
                try {
                    vn1 a5 = vn1.d.a(k[0]);
                    if (a5 != null) {
                        return new u2(b2, a5);
                    }
                } catch (IllegalArgumentException e3) {
                    d90.i.i(e3);
                    return null;
                }
                break;
            case 6:
                try {
                    return g2.CREATOR.a(b2, k);
                } catch (IllegalArgumentException e4) {
                    d90.i.i(e4);
                    return null;
                }
            case 7:
                return new w2(b2);
            case 8:
                try {
                    return s2.CREATOR.a(b2, k);
                } catch (IllegalArgumentException e5) {
                    d90.i.i(e5);
                    return null;
                }
            case 9:
                return new z1(b2);
            case 10:
                return new e2(b2);
            case 11:
                try {
                    cu1 a6 = cu1.d.a(k[0]);
                    if (a6 != null) {
                        return new i2(b2, a6, k[1]);
                    }
                } catch (Exception e6) {
                    d90.i.i(e6);
                    return null;
                }
                break;
            case 12:
                return b(b2, k);
            default:
                throw new al7();
        }
        return null;
    }

    @DexIgnore
    public final m2 b(byte b2, byte[] bArr) {
        int i;
        int i2;
        try {
            gu1 a2 = gu1.d.a(bArr[0]);
            fu1 a3 = fu1.d.a(bArr[1]);
            iu1 a4 = iu1.d.a(bArr[2]);
            byte[] k = dm7.k(bArr, 3, bArr.length);
            if (a3 == fu1.XOR) {
                ByteBuffer wrap = ByteBuffer.wrap(dm7.k(bArr, 6, 10));
                i2 = hy1.n(wrap.getShort(0));
                i = hy1.n(wrap.getShort(2));
            } else {
                i = 0;
                i2 = 0;
            }
            if (!(a2 == null || a3 == null || a4 == null)) {
                return new m2(b2, a2, a3, a4, k, i2, i);
            }
        } catch (Exception e) {
            d90.i.i(e);
        }
        return null;
    }

    @DexIgnore
    public final void c(String str) {
        synchronized (f3915a) {
            Set<Map.Entry<cl7<String, lt>, Byte>> entrySet = f3915a.entrySet();
            pq7.b(entrySet, "eventSequenceManager.entries");
            for (T t : entrySet) {
                if (pq7.a(str, (String) ((cl7) t.getKey()).getFirst())) {
                    AbstractMap abstractMap = f3915a;
                    Object key = t.getKey();
                    pq7.b(key, "entry.key");
                    abstractMap.put(key, (byte) -1);
                }
            }
            tl7 tl7 = tl7.f3441a;
        }
    }
}
