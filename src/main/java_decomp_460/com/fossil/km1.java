package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km1 extends dm1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<km1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public km1 a(Parcel parcel) {
            return new km1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public km1 createFromParcel(Parcel parcel) {
            return new km1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public km1[] newArray(int i) {
            return new km1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ km1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public km1(ct1 ct1) {
        super(fm1.SECOND_TIMEZONE, ct1, null, null, 12);
    }

    @DexIgnore
    public km1(ct1 ct1, dt1 dt1, et1 et1) {
        super(fm1.SECOND_TIMEZONE, ct1, dt1, et1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ km1(ct1 ct1, dt1 dt1, et1 et1, int i, kq7 kq7) {
        this(ct1, dt1, (i & 4) != 0 ? new et1(et1.CREATOR.a()) : et1);
    }

    @DexIgnore
    public final ct1 getDataConfig() {
        at1 at1 = this.c;
        if (at1 != null) {
            return (ct1) at1;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }
}
