package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public ug7(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    @DexIgnore
    public final void run() {
        try {
            ig7.u(this.b);
            gh7.b(this.b).d(this.c);
        } catch (Throwable th) {
            ig7.m.e(th);
            ig7.f(this.b, th);
        }
    }
}
