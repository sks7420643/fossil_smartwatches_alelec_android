package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h17 implements Factory<f17> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DeviceRepository> f1411a;
    @DexIgnore
    public /* final */ Provider<ot5> b;
    @DexIgnore
    public /* final */ Provider<vt5> c;
    @DexIgnore
    public /* final */ Provider<mj5> d;
    @DexIgnore
    public /* final */ Provider<on5> e;
    @DexIgnore
    public /* final */ Provider<qt5> f;
    @DexIgnore
    public /* final */ Provider<ht5> g;
    @DexIgnore
    public /* final */ Provider<tt4> h;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> i;

    @DexIgnore
    public h17(Provider<DeviceRepository> provider, Provider<ot5> provider2, Provider<vt5> provider3, Provider<mj5> provider4, Provider<on5> provider5, Provider<qt5> provider6, Provider<ht5> provider7, Provider<tt4> provider8, Provider<PortfolioApp> provider9) {
        this.f1411a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
    }

    @DexIgnore
    public static h17 a(Provider<DeviceRepository> provider, Provider<ot5> provider2, Provider<vt5> provider3, Provider<mj5> provider4, Provider<on5> provider5, Provider<qt5> provider6, Provider<ht5> provider7, Provider<tt4> provider8, Provider<PortfolioApp> provider9) {
        return new h17(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9);
    }

    @DexIgnore
    public static f17 c(DeviceRepository deviceRepository, ot5 ot5, vt5 vt5, mj5 mj5, on5 on5, qt5 qt5, ht5 ht5, tt4 tt4, PortfolioApp portfolioApp) {
        return new f17(deviceRepository, ot5, vt5, mj5, on5, qt5, ht5, tt4, portfolioApp);
    }

    @DexIgnore
    /* renamed from: b */
    public f17 get() {
        return c(this.f1411a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }
}
