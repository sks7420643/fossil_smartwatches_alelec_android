package com.fossil;

import com.fossil.iq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt5 implements iq4.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3466a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public tt5(int i, String str, boolean z) {
        pq7.c(str, "serial");
        this.f3466a = i;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.f3466a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }
}
