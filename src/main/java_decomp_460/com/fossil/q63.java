package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q63 implements xw2<p63> {
    @DexIgnore
    public static q63 c; // = new q63();
    @DexIgnore
    public /* final */ xw2<p63> b;

    @DexIgnore
    public q63() {
        this(ww2.b(new s63()));
    }

    @DexIgnore
    public q63(xw2<p63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((p63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((p63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ p63 zza() {
        return this.b.zza();
    }
}
