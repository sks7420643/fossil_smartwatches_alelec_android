package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ve5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ View s;

    @DexIgnore
    public ve5(Object obj, View view, int i, FlexibleTextView flexibleTextView, View view2, View view3) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = view2;
        this.s = view3;
    }

    @DexIgnore
    @Deprecated
    public static ve5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ve5) ViewDataBinding.p(layoutInflater, 2131558692, viewGroup, z, obj);
    }

    @DexIgnore
    public static ve5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, aq0.d());
    }
}
