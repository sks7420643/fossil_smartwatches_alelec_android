package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b93 implements c93 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f405a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.collection.synthetic_data_mitigation", false);

    @DexIgnore
    @Override // com.fossil.c93
    public final boolean zza() {
        return f405a.o().booleanValue();
    }
}
