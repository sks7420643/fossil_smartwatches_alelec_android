package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hv1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (pq7.a(readString, em1.class.getCanonicalName())) {
                return em1.CREATOR.a(parcel);
            }
            if (pq7.a(readString, zo1.class.getCanonicalName())) {
                return zo1.CREATOR.a(parcel);
            }
            if (pq7.a(readString, sl1.class.getCanonicalName())) {
                return sl1.CREATOR.b(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hv1[] newArray(int i) {
            return new hv1[i];
        }
    }

    @DexIgnore
    public hv1() {
        hd0.y.d();
    }

    @DexIgnore
    public hv1(Parcel parcel) {
        hd0.y.d();
        parcel.readString();
    }

    @DexIgnore
    public abstract JSONObject a();

    @DexIgnore
    public final void a(ry1 ry1) {
    }

    @DexIgnore
    public final JSONObject b() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", a()));
            pq7.b(put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            d90.i.i(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }
}
