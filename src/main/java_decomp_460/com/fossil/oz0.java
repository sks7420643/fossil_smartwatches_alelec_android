package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oz0 extends uy0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String[] f2754a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    public static int d(wy0 wy0, int i) {
        if (wy0 == null) {
            return -1;
        }
        int[] iArr = (int[]) wy0.f4017a.get("android:visibilityPropagation:center");
        if (iArr == null) {
            return -1;
        }
        return iArr[i];
    }

    @DexIgnore
    @Override // com.fossil.uy0
    public void a(wy0 wy0) {
        View view = wy0.b;
        Integer num = (Integer) wy0.f4017a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        wy0.f4017a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = (view.getHeight() / 2) + iArr[1];
        wy0.f4017a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    @Override // com.fossil.uy0
    public String[] b() {
        return f2754a;
    }

    @DexIgnore
    public int e(wy0 wy0) {
        if (wy0 == null) {
            return 8;
        }
        Integer num = (Integer) wy0.f4017a.get("android:visibilityPropagation:visibility");
        if (num == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int f(wy0 wy0) {
        return d(wy0, 0);
    }

    @DexIgnore
    public int g(wy0 wy0) {
        return d(wy0, 1);
    }
}
