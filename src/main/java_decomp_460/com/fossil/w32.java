package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w32 implements Factory<t32> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ w32 f3874a; // = new w32();

    @DexIgnore
    public static w32 a() {
        return f3874a;
    }

    @DexIgnore
    public static t32 c() {
        t32 b = u32.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    /* renamed from: b */
    public t32 get() {
        return c();
    }
}
