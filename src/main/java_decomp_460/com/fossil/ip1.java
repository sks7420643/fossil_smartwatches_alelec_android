package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ip1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ tn1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ip1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ip1 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            float readFloat = parcel.readFloat();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                return new ip1(readInt, readFloat, tn1.valueOf(readString));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ip1[] newArray(int i) {
            return new ip1[i];
        }
    }

    @DexIgnore
    public ip1(int i, float f, tn1 tn1) throws IllegalArgumentException {
        this.b = i;
        this.c = hy1.e(f, 2);
        this.d = tn1;
        int i2 = this.b;
        if (!(i2 >= 0 && 23 >= i2)) {
            throw new IllegalArgumentException(e.c(e.e("hourIn24Format("), this.b, ") is out of range ", "[0, 23]."));
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject put = new JSONObject().put(AppFilter.COLUMN_HOUR, this.b).put("temp", Float.valueOf(this.c)).put("cond_id", this.d.a());
        pq7.b(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ip1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ip1 ip1 = (ip1) obj;
            if (this.b != ip1.b) {
                return false;
            }
            if (this.c != ip1.c) {
                return false;
            }
            return this.d == ip1.d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherHourForecast");
    }

    @DexIgnore
    public final int getHourIn24Format() {
        return this.b;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.c;
    }

    @DexIgnore
    public final tn1 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        return (((i * 31) + Float.valueOf(this.c).hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.A, Integer.valueOf(this.b)), jd0.T1, Float.valueOf(this.c)), jd0.t, ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
