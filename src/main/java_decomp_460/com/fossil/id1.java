package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface id1<Z> {
    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    int c();

    @DexIgnore
    Class<Z> d();

    @DexIgnore
    Z get();
}
