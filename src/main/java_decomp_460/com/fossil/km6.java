package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.jl6;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km6 extends pv5 implements jm6, View.OnClickListener, jl6.a {
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public g37<z65> g;
    @DexIgnore
    public im6 h;
    @DexIgnore
    public Date i; // = new Date();
    @DexIgnore
    public jl6 j;
    @DexIgnore
    public /* final */ Calendar k; // = Calendar.getInstance();
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final km6 a(Date date) {
            pq7.c(date, "date");
            km6 km6 = new km6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            km6.setArguments(bundle);
            return km6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ boolean f1933a;
        @DexIgnore
        public /* final */ /* synthetic */ cu0 b;

        @DexIgnore
        public b(km6 km6, boolean z, cu0 cu0, ai5 ai5) {
            this.f1933a = z;
            this.b = cu0;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            pq7.c(appBarLayout, "appBarLayout");
            return this.f1933a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public km6() {
        String d = qn5.l.a().d("nonBrandSurface");
        this.m = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = qn5.l.a().d("backgroundDashboard");
        this.s = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("secondaryText");
        this.t = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("primaryText");
        this.u = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandNonReachGoal");
        this.v = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.w = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
    }

    @DexIgnore
    @Override // com.fossil.jm6
    public void A5(int i2, int i3) {
        g37<z65> g37 = this.g;
        if (g37 != null) {
            z65 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (i2 == 0 && i3 == 0) {
                FlexibleTextView flexibleTextView = a2.A;
                pq7.b(flexibleTextView, "it.ftvNoRecord");
                flexibleTextView.setVisibility(0);
                ConstraintLayout constraintLayout = a2.s;
                pq7.b(constraintLayout, "it.clContainer");
                constraintLayout.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.A;
            pq7.b(flexibleTextView2, "it.ftvNoRecord");
            flexibleTextView2.setVisibility(8);
            ConstraintLayout constraintLayout2 = a2.s;
            pq7.b(constraintLayout2, "it.clContainer");
            constraintLayout2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = a2.D;
            pq7.b(flexibleTextView3, "it.ftvRestingValue");
            flexibleTextView3.setText(String.valueOf(i2));
            FlexibleTextView flexibleTextView4 = a2.z;
            pq7.b(flexibleTextView4, "it.ftvMaxValue");
            flexibleTextView4.setText(String.valueOf(i3));
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jl6.a
    public void B2(WorkoutSession workoutSession) {
        String name;
        ai5 n;
        pq7.c(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            im6 im6 = this.h;
            if (im6 != null) {
                if (im6 == null || (n = im6.n()) == null || (name = n.name()) == null) {
                    name = ai5.METRIC.name();
                }
                WorkoutDetailActivity.a aVar = WorkoutDetailActivity.f;
                pq7.b(activity, "it");
                aVar.a(activity, workoutSession.getId(), name);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HeartRateDetailFragment";
    }

    @DexIgnore
    public final void K6(z65 z65) {
        z65.F.setOnClickListener(this);
        z65.G.setOnClickListener(this);
        z65.H.setOnClickListener(this);
        this.l = qn5.l.a().d("dianaHeartRateTab");
        jl6 jl6 = new jl6(jl6.c.HEART_RATE, ai5.IMPERIAL, new WorkoutSessionDifference(), this.l);
        this.j = jl6;
        if (jl6 != null) {
            jl6.u(this);
        }
        RecyclerView recyclerView = z65.L;
        pq7.b(recyclerView, "it");
        recyclerView.setAdapter(this.j);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable f = gl0.f(recyclerView.getContext(), 2131230857);
        if (f != null) {
            ok6 ok6 = new ok6(linearLayoutManager.q2(), false, false, 6, null);
            pq7.b(f, ResourceManager.DRAWABLE);
            ok6.h(f);
            recyclerView.addItemDecoration(ok6);
        }
        z65.J.setBackgroundColor(this.s);
        z65.r.setBackgroundColor(this.m);
        z65.D.setTextColor(this.u);
        z65.C.setTextColor(this.v);
        z65.z.setTextColor(this.u);
        z65.y.setTextColor(this.v);
        z65.x.setTextColor(this.t);
        z65.w.setTextColor(this.u);
        z65.t.setBackgroundColor(this.w);
        z65.v.setBackgroundColor(this.s);
    }

    @DexIgnore
    public final void L6() {
        TodayHeartRateChart todayHeartRateChart;
        g37<z65> g37 = this.g;
        if (g37 != null) {
            z65 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.o("maxHeartRate", "lowestHeartRate");
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(im6 im6) {
        pq7.c(im6, "presenter");
        this.h = im6;
        Lifecycle lifecycle = getLifecycle();
        im6 im62 = this.h;
        if (im62 == null) {
            pq7.n("mPresenter");
            throw null;
        } else if (im62 != null) {
            lifecycle.a(im62.o());
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.jm6
    public void b0(int i2, List<w57> list, List<gl7<Integer, cl7<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        pq7.c(list, "listTodayHeartRateModel");
        pq7.c(list2, "listTimeZoneChange");
        g37<z65> g37 = this.g;
        if (g37 != null) {
            z65 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.m(list);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jm6
    public void j(Date date, boolean z, boolean z2, boolean z3) {
        pq7.c(date, "date");
        this.i = date;
        Calendar calendar = this.k;
        pq7.b(calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.k.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.k);
        g37<z65> g37 = this.g;
        if (g37 != null) {
            z65 a2 = g37.a();
            if (a2 != null) {
                a2.q.r(true, true);
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.k.get(5)));
                if (z) {
                    ImageView imageView = a2.G;
                    pq7.b(imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.G;
                    pq7.b(imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.H;
                    pq7.b(imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.x;
                        pq7.b(flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(um5.c(getContext(), 2131886662));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.x;
                    pq7.b(flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(jl5.b.i(i2));
                    return;
                }
                ImageView imageView4 = a2.H;
                pq7.b(imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.x;
                pq7.b(flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(jl5.b.i(i2));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        MFLogger.d("HeartRateDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    im6 im6 = this.h;
                    if (im6 != null) {
                        im6.u();
                        return;
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                case 2131362735:
                    im6 im62 = this.h;
                    if (im62 != null) {
                        im62.t();
                        return;
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        z65 z65 = (z65) aq0.f(layoutInflater, 2131558563, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.i = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.i = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        pq7.b(z65, "binding");
        K6(z65);
        im6 im6 = this.h;
        if (im6 != null) {
            im6.p(this.i);
            this.g = new g37<>(this, z65);
            L6();
            g37<z65> g37 = this.g;
            if (g37 != null) {
                z65 a2 = g37.a();
                if (a2 != null) {
                    return a2.n();
                }
                return null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        im6 im6 = this.h;
        if (im6 != null) {
            im6.q();
            Lifecycle lifecycle = getLifecycle();
            im6 im62 = this.h;
            if (im62 != null) {
                lifecycle.c(im62.o());
                super.onDestroyView();
                v6();
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        im6 im6 = this.h;
        if (im6 != null) {
            im6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        L6();
        im6 im6 = this.h;
        if (im6 != null) {
            im6.s(this.i);
            im6 im62 = this.h;
            if (im62 != null) {
                im62.l();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        im6 im6 = this.h;
        if (im6 != null) {
            im6.r(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jm6
    public void s(boolean z, ai5 ai5, cu0<WorkoutSession> cu0) {
        pq7.c(ai5, "distanceUnit");
        pq7.c(cu0, "workoutSessions");
        g37<z65> g37 = this.g;
        if (g37 != null) {
            z65 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.J;
                    pq7.b(linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!cu0.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.B;
                        pq7.b(flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.L;
                        pq7.b(recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        jl6 jl6 = this.j;
                        if (jl6 != null) {
                            jl6.t(ai5, cu0);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.B;
                        pq7.b(flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.L;
                        pq7.b(recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        jl6 jl62 = this.j;
                        if (jl62 != null) {
                            jl62.t(ai5, cu0);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.J;
                    pq7.b(linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                pq7.b(appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.f();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new b(this, z, cu0, ai5));
                    eVar.o(behavior);
                    return;
                }
                throw new il7("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jl6.a
    public void t4(WorkoutSession workoutSession) {
        pq7.c(workoutSession, "workoutSession");
        nn6 b2 = nn6.v.b(workoutSession.getId());
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, nn6.v.a());
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
