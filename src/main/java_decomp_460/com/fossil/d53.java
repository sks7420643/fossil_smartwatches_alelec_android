package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d53 implements e53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f738a;
    @DexIgnore
    public static /* final */ xv2<Long> b;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f738a = hw2.d("measurement.sdk.attribution.cache", true);
        b = hw2.b("measurement.sdk.attribution.cache.ttl", 604800000);
    }
    */

    @DexIgnore
    @Override // com.fossil.e53
    public final boolean zza() {
        return f738a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.e53
    public final long zzb() {
        return b.o().longValue();
    }
}
