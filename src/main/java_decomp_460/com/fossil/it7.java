package com.fossil;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it7 implements ht7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Matcher f1668a;
    @DexIgnore
    public /* final */ CharSequence b;

    @DexIgnore
    public it7(Matcher matcher, CharSequence charSequence) {
        pq7.c(matcher, "matcher");
        pq7.c(charSequence, "input");
        this.f1668a = matcher;
        this.b = charSequence;
    }

    @DexIgnore
    @Override // com.fossil.ht7
    public wr7 a() {
        return lt7.g(b());
    }

    @DexIgnore
    public final MatchResult b() {
        return this.f1668a;
    }

    @DexIgnore
    @Override // com.fossil.ht7
    public String getValue() {
        String group = b().group();
        pq7.b(group, "matchResult.group()");
        return group;
    }

    @DexIgnore
    @Override // com.fossil.ht7
    public ht7 next() {
        int end = (b().end() == b().start() ? 1 : 0) + b().end();
        if (end > this.b.length()) {
            return null;
        }
        Matcher matcher = this.f1668a.pattern().matcher(this.b);
        pq7.b(matcher, "matcher.pattern().matcher(input)");
        return lt7.e(matcher, end, this.b);
    }
}
