package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su4 implements Factory<ru4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ su4 f3308a; // = new su4();
    }

    @DexIgnore
    public static su4 a() {
        return a.f3308a;
    }

    @DexIgnore
    public static ru4 c() {
        return new ru4();
    }

    @DexIgnore
    /* renamed from: b */
    public ru4 get() {
        return c();
    }
}
