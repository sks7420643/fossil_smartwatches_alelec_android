package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sd2 extends cc2 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ o72 c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;

    @DexIgnore
    public sd2(Intent intent, o72 o72, int i) {
        this.b = intent;
        this.c = o72;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.cc2
    public final void c() {
        Intent intent = this.b;
        if (intent != null) {
            this.c.startActivityForResult(intent, this.d);
        }
    }
}
