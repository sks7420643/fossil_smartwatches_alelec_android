package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz4 {
    @DexIgnore
    public final String a(DateTime dateTime) {
        String s0 = lk5.s0(DateTimeZone.UTC, dateTime);
        pq7.b(s0, "DateHelper.printServerDa\u2026t(DateTimeZone.UTC, date)");
        return vt7.o(s0, "Z", "+0000", true);
    }

    @DexIgnore
    public final DateTime b(String str) {
        try {
            return lk5.R(DateTimeZone.UTC, str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeUTCStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }
}
