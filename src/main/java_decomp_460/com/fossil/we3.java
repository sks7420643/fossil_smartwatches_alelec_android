package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<we3> CREATOR; // = new kf3();
    @DexIgnore
    public /* final */ LatLng b;
    @DexIgnore
    public /* final */ LatLng c;
    @DexIgnore
    public /* final */ LatLng d;
    @DexIgnore
    public /* final */ LatLng e;
    @DexIgnore
    public /* final */ LatLngBounds f;

    @DexIgnore
    public we3(LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.b = latLng;
        this.c = latLng2;
        this.d = latLng3;
        this.e = latLng4;
        this.f = latLngBounds;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof we3)) {
            return false;
        }
        we3 we3 = (we3) obj;
        return this.b.equals(we3.b) && this.c.equals(we3.c) && this.d.equals(we3.d) && this.e.equals(we3.e) && this.f.equals(we3.f);
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(this.b, this.c, this.d, this.e, this.f);
    }

    @DexIgnore
    public final String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("nearLeft", this.b);
        c2.a("nearRight", this.c);
        c2.a("farLeft", this.d);
        c2.a("farRight", this.e);
        c2.a("latLngBounds", this.f);
        return c2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 2, this.b, i, false);
        bd2.t(parcel, 3, this.c, i, false);
        bd2.t(parcel, 4, this.d, i, false);
        bd2.t(parcel, 5, this.e, i, false);
        bd2.t(parcel, 6, this.f, i, false);
        bd2.b(parcel, a2);
    }
}
