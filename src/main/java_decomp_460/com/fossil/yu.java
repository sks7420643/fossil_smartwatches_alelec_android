package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu extends mu {
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    public yu(long j, k5 k5Var) {
        super(fu.r, hs.F, k5Var, 0, 8);
        this.L = j;
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.mu, com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        pq7.b(array, "ByteBuffer.allocate(4).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A1, Long.valueOf(this.L));
    }
}
