package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dy3;
import com.google.android.material.datepicker.MaterialCalendarGridView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jy3 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ wx3 f1833a;
    @DexIgnore
    public /* final */ zx3<?> b;
    @DexIgnore
    public /* final */ dy3.l c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MaterialCalendarGridView b;

        @DexIgnore
        public a(MaterialCalendarGridView materialCalendarGridView) {
            this.b = materialCalendarGridView;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (this.b.getAdapter().j(i)) {
                jy3.this.c.a(this.b.getAdapter().getItem(i).longValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f1834a;
        @DexIgnore
        public /* final */ MaterialCalendarGridView b;

        @DexIgnore
        public b(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(nw3.month_title);
            this.f1834a = textView;
            mo0.m0(textView, true);
            this.b = (MaterialCalendarGridView) linearLayout.findViewById(nw3.month_grid);
            if (!z) {
                this.f1834a.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public jy3(Context context, zx3<?> zx3, wx3 wx3, dy3.l lVar) {
        hy3 i = wx3.i();
        hy3 f = wx3.f();
        hy3 h = wx3.h();
        if (i.compareTo(h) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (h.compareTo(f) <= 0) {
            this.d = (ey3.K6(context) ? dy3.K6(context) : 0) + (iy3.f * dy3.K6(context));
            this.f1833a = wx3;
            this.b = zx3;
            this.c = lVar;
            setHasStableIds(true);
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1833a.g();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return this.f1833a.i().k(i).i();
    }

    @DexIgnore
    public hy3 h(int i) {
        return this.f1833a.i().k(i);
    }

    @DexIgnore
    public CharSequence i(int i) {
        return h(i).h();
    }

    @DexIgnore
    public int j(hy3 hy3) {
        return this.f1833a.i().m(hy3);
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(b bVar, int i) {
        hy3 k = this.f1833a.i().k(i);
        bVar.f1834a.setText(k.h());
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) bVar.b.findViewById(nw3.month_grid);
        if (materialCalendarGridView.getAdapter() == null || !k.equals(materialCalendarGridView.getAdapter().b)) {
            iy3 iy3 = new iy3(k, this.b, this.f1833a);
            materialCalendarGridView.setNumColumns(k.f);
            materialCalendarGridView.setAdapter((ListAdapter) iy3);
        } else {
            materialCalendarGridView.getAdapter().notifyDataSetChanged();
        }
        materialCalendarGridView.setOnItemClickListener(new a(materialCalendarGridView));
    }

    @DexIgnore
    /* renamed from: l */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(pw3.mtrl_calendar_month_labeled, viewGroup, false);
        if (!ey3.K6(viewGroup.getContext())) {
            return new b(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.d));
        return new b(linearLayout, true);
    }
}
