package com.fossil;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wh1 implements di1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<ei1> f3936a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    @Override // com.fossil.di1
    public void a(ei1 ei1) {
        this.f3936a.add(ei1);
        if (this.c) {
            ei1.onDestroy();
        } else if (this.b) {
            ei1.onStart();
        } else {
            ei1.onStop();
        }
    }

    @DexIgnore
    @Override // com.fossil.di1
    public void b(ei1 ei1) {
        this.f3936a.remove(ei1);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (ei1 ei1 : jk1.j(this.f3936a)) {
            ei1.onDestroy();
        }
    }

    @DexIgnore
    public void d() {
        this.b = true;
        for (ei1 ei1 : jk1.j(this.f3936a)) {
            ei1.onStart();
        }
    }

    @DexIgnore
    public void e() {
        this.b = false;
        for (ei1 ei1 : jk1.j(this.f3936a)) {
            ei1.onStop();
        }
    }
}
