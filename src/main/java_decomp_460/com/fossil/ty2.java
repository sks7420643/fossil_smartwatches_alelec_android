package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty2 extends sx2<Object> {
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ transient int f;

    @DexIgnore
    public ty2(Object[] objArr, int i, int i2) {
        this.d = objArr;
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    @Override // java.util.List
    public final Object get(int i) {
        sw2.a(i, this.f);
        return this.d[(i * 2) + this.e];
    }

    @DexIgnore
    public final int size() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return true;
    }
}
