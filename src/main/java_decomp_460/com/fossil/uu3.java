package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uu3 extends tb2<Object> implements z62 {
    @DexIgnore
    public /* final */ Status e;

    @DexIgnore
    public uu3(DataHolder dataHolder) {
        super(dataHolder);
        this.e = new Status(dataHolder.k());
    }

    @DexIgnore
    @Override // com.fossil.z62
    public Status a() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.tb2
    public /* synthetic */ Object c(int i, int i2) {
        return new iv3(this.b, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.tb2
    public String f() {
        return "path";
    }
}
