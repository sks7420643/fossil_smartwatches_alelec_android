package com.fossil;

import com.fossil.vz1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public final a a(String str, int i) {
            e().put(str, String.valueOf(i));
            return this;
        }

        @DexIgnore
        public final a b(String str, long j) {
            e().put(str, String.valueOf(j));
            return this;
        }

        @DexIgnore
        public final a c(String str, String str2) {
            e().put(str, str2);
            return this;
        }

        @DexIgnore
        public abstract c02 d();

        @DexIgnore
        public abstract Map<String, String> e();

        @DexIgnore
        public abstract a f(Map<String, String> map);

        @DexIgnore
        public abstract a g(Integer num);

        @DexIgnore
        public abstract a h(b02 b02);

        @DexIgnore
        public abstract a i(long j);

        @DexIgnore
        public abstract a j(String str);

        @DexIgnore
        public abstract a k(long j);
    }

    @DexIgnore
    public static a a() {
        vz1.b bVar = new vz1.b();
        bVar.f(new HashMap());
        return bVar;
    }

    @DexIgnore
    public final String b(String str) {
        String str2 = c().get(str);
        return str2 == null ? "" : str2;
    }

    @DexIgnore
    public abstract Map<String, String> c();

    @DexIgnore
    public abstract Integer d();

    @DexIgnore
    public abstract b02 e();

    @DexIgnore
    public abstract long f();

    @DexIgnore
    public final int g(String str) {
        String str2 = c().get(str);
        if (str2 == null) {
            return 0;
        }
        return Integer.valueOf(str2).intValue();
    }

    @DexIgnore
    public final long h(String str) {
        String str2 = c().get(str);
        if (str2 == null) {
            return 0;
        }
        return Long.valueOf(str2).longValue();
    }

    @DexIgnore
    public final Map<String, String> i() {
        return Collections.unmodifiableMap(c());
    }

    @DexIgnore
    public abstract String j();

    @DexIgnore
    public abstract long k();

    @DexIgnore
    public a l() {
        vz1.b bVar = new vz1.b();
        bVar.j(j());
        bVar.g(d());
        bVar.h(e());
        bVar.i(f());
        bVar.k(k());
        bVar.f(new HashMap(c()));
        return bVar;
    }
}
