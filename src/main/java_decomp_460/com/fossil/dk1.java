package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Executor f797a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            runnable.run();
        }
    }

    @DexIgnore
    public static Executor a() {
        return b;
    }

    @DexIgnore
    public static Executor b() {
        return f797a;
    }
}
