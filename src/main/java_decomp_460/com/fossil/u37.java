package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u37<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f3511a;
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public u37(T t) {
        this.b = t;
    }

    @DexIgnore
    public final T a() {
        if (this.f3511a) {
            return null;
        }
        this.f3511a = true;
        return this.b;
    }
}
