package com.fossil;

import com.fossil.cu0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hu0<T> extends cu0<T> {
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ Object v;
    @DexIgnore
    public /* final */ xt0<?, T> w;

    @DexIgnore
    public hu0(cu0<T> cu0) {
        super(cu0.f.B(), cu0.b, cu0.c, null, cu0.e);
        this.w = cu0.p();
        this.u = cu0.s();
        this.g = cu0.g;
        this.v = cu0.q();
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void o(cu0<T> cu0, cu0.e eVar) {
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public xt0<?, T> p() {
        return this.w;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public Object q() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public boolean s() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public boolean t() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public boolean u() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void w(int i) {
    }
}
