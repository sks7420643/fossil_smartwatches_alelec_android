package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cj7 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<cj7> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Intent c;
    @DexIgnore
    public /* final */ fj7 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cj7> {
        @DexIgnore
        /* renamed from: a */
        public cj7 createFromParcel(Parcel parcel) {
            return new cj7(parcel, null);
        }

        @DexIgnore
        /* renamed from: b */
        public cj7[] newArray(int i) {
            return new cj7[i];
        }
    }

    @DexIgnore
    public cj7(Intent intent, int i, fj7 fj7) {
        this.c = intent;
        this.b = i;
        this.d = fj7;
    }

    @DexIgnore
    public cj7(Parcel parcel) {
        this.b = parcel.readInt();
        this.c = (Intent) parcel.readParcelable(cj7.class.getClassLoader());
        this.d = (fj7) parcel.readSerializable();
    }

    @DexIgnore
    public /* synthetic */ cj7(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public fj7 a() {
        return this.d;
    }

    @DexIgnore
    public void b(Activity activity) {
        activity.startActivityForResult(this.c, this.b);
    }

    @DexIgnore
    public void c(Fragment fragment) {
        fragment.startActivityForResult(this.c, this.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.c, i);
        parcel.writeSerializable(this.d);
    }
}
