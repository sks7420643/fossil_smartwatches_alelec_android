package com.fossil;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb2 extends Fragment implements o72 {
    @DexIgnore
    public static WeakHashMap<Activity, WeakReference<hb2>> e; // = new WeakHashMap<>();
    @DexIgnore
    public Map<String, LifecycleCallback> b; // = new zi0();
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public static hb2 b(Activity activity) {
        hb2 hb2;
        WeakReference<hb2> weakReference = e.get(activity);
        if (weakReference == null || (hb2 = weakReference.get()) == null) {
            try {
                hb2 = (hb2) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
                if (hb2 == null || hb2.isRemoving()) {
                    hb2 = new hb2();
                    activity.getFragmentManager().beginTransaction().add(hb2, "LifecycleFragmentImpl").commitAllowingStateLoss();
                }
                e.put(activity, new WeakReference<>(hb2));
            } catch (ClassCastException e2) {
                throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e2);
            }
        }
        return hb2;
    }

    @DexIgnore
    @Override // com.fossil.o72
    public final <T extends LifecycleCallback> T S2(String str, Class<T> cls) {
        return cls.cast(this.b.get(str));
    }

    @DexIgnore
    @Override // com.fossil.o72
    public final Activity X2() {
        return getActivity();
    }

    @DexIgnore
    @Override // com.fossil.o72
    public final void b1(String str, LifecycleCallback lifecycleCallback) {
        if (!this.b.containsKey(str)) {
            this.b.put(str, lifecycleCallback);
            if (this.c > 0) {
                new xl2(Looper.getMainLooper()).post(new gb2(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.e(i, i2, intent);
        }
    }

    @DexIgnore
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = 1;
        this.d = bundle;
        for (Map.Entry<String, LifecycleCallback> entry : this.b.entrySet()) {
            entry.getValue().f(bundle != null ? bundle.getBundle(entry.getKey()) : null);
        }
    }

    @DexIgnore
    public final void onDestroy() {
        super.onDestroy();
        this.c = 5;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.g();
        }
    }

    @DexIgnore
    public final void onResume() {
        super.onResume();
        this.c = 3;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.h();
        }
    }

    @DexIgnore
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry<String, LifecycleCallback> entry : this.b.entrySet()) {
                Bundle bundle2 = new Bundle();
                entry.getValue().i(bundle2);
                bundle.putBundle(entry.getKey(), bundle2);
            }
        }
    }

    @DexIgnore
    public final void onStart() {
        super.onStart();
        this.c = 2;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.j();
        }
    }

    @DexIgnore
    public final void onStop() {
        super.onStop();
        this.c = 4;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.k();
        }
    }
}
