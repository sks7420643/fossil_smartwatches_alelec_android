package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g03 implements c03 {
    @DexIgnore
    public g03() {
    }

    @DexIgnore
    public /* synthetic */ g03(wz2 wz2) {
        this();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
