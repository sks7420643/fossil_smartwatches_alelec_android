package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv4 extends ts0 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Pattern j; // = Pattern.compile("^[a-z0-9.]+$");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<b> f667a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<a> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ hu4 f;
    @DexIgnore
    public /* final */ vt4 g;
    @DexIgnore
    public /* final */ on5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f668a;
        @DexIgnore
        public String b;

        @DexIgnore
        public a(int i, String str) {
            this.f668a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f668a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (this.f668a != aVar.f668a || !pq7.a(this.b, aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f668a;
            String str = this.b;
            return (str != null ? str.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "Error(errorCode=" + this.f668a + ", errorMessage=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f669a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(boolean z, boolean z2) {
            this.f669a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.f669a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(this.f669a == bVar.f669a && this.b == bVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.f669a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (i2 * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "IdValidation(isCombineMet=" + this.f669a + ", isLengthMet=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1", f = "BCCreateSocialProfileViewModel.kt", l = {57, 65}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $socialId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cv4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$1", f = "BCCreateSocialProfileViewModel.kt", l = {66, 71}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r3 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x0074
                    if (r0 == r6) goto L_0x0024
                    if (r0 != r7) goto L_0x001c
                    java.lang.Object r0 = r8.L$1
                    com.fossil.kz4 r0 = (com.fossil.kz4) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                L_0x0019:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001b:
                    return r0
                L_0x001c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0024:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                    r2 = r0
                    r1 = r9
                L_0x002d:
                    r0 = r1
                    com.fossil.kz4 r0 = (com.fossil.kz4) r0
                    com.portfolio.platform.data.model.ServerError r1 = r0.a()
                    if (r1 == 0) goto L_0x0019
                    com.portfolio.platform.data.model.ServerError r1 = r0.a()
                    java.lang.Integer r1 = r1.getCode()
                    if (r1 == 0) goto L_0x0019
                    int r1 = r1.intValue()
                    if (r1 != 0) goto L_0x0019
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    java.lang.String r4 = com.fossil.cv4.e()
                    java.lang.String r5 = "reset device Id"
                    r1.e(r4, r5)
                    com.fossil.cv4$c r1 = r8.this$0
                    com.fossil.cv4 r1 = r1.this$0
                    com.fossil.on5 r1 = com.fossil.cv4.c(r1)
                    r1.Y1(r6)
                    com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                    com.portfolio.platform.PortfolioApp r1 = r1.c()
                    r8.L$0 = r2
                    r8.L$1 = r0
                    r8.label = r7
                    java.lang.Object r0 = r1.A(r8)
                    if (r0 != r3) goto L_0x0019
                    r0 = r3
                    goto L_0x001b
                L_0x0074:
                    com.fossil.el7.b(r9)
                    com.fossil.iv7 r0 = r8.p$
                    com.fossil.cv4$c r1 = r8.this$0
                    com.fossil.cv4 r1 = r1.this$0
                    com.fossil.vt4 r1 = com.fossil.cv4.b(r1)
                    r8.L$0 = r0
                    r8.label = r6
                    java.lang.Object r1 = r1.d(r8)
                    if (r1 != r3) goto L_0x008d
                    r0 = r3
                    goto L_0x001b
                L_0x008d:
                    r2 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cv4.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$result$1", f = "BCCreateSocialProfileViewModel.kt", l = {58}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<it4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<it4>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    hu4 hu4 = this.this$0.this$0.f;
                    String str = this.this$0.$socialId;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object a2 = hu4.a(str, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cv4 cv4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cv4;
            this.$socialId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$socialId, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0090  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r5 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0069
                if (r0 == r5) goto L_0x0025
                if (r0 != r6) goto L_0x001d
                java.lang.Object r0 = r8.L$1
                com.fossil.kz4 r0 = (com.fossil.kz4) r0
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x001a:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0025:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
                r2 = r0
                r1 = r9
            L_0x002e:
                r0 = r1
                com.fossil.kz4 r0 = (com.fossil.kz4) r0
                com.fossil.cv4 r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.fossil.cv4.f(r1)
                r4 = 0
                java.lang.Boolean r4 = com.fossil.ao7.a(r4)
                r1.l(r4)
                java.lang.Object r1 = r0.c()
                if (r1 == 0) goto L_0x0090
                com.fossil.cv4 r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.fossil.cv4.g(r1)
                java.lang.Boolean r4 = com.fossil.ao7.a(r5)
                r1.l(r4)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.cv4$c$a r4 = new com.fossil.cv4$c$a
                r4.<init>(r8, r7)
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r6
                java.lang.Object r0 = com.fossil.eu7.g(r1, r4, r8)
                if (r0 != r3) goto L_0x001a
                r0 = r3
                goto L_0x001c
            L_0x0069:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                com.fossil.cv4 r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.fossil.cv4.f(r1)
                java.lang.Boolean r2 = com.fossil.ao7.a(r5)
                r1.l(r2)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.cv4$c$b r2 = new com.fossil.cv4$c$b
                r2.<init>(r8, r7)
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
                if (r1 != r3) goto L_0x00b6
                r0 = r3
                goto L_0x001c
            L_0x0090:
                com.portfolio.platform.data.model.ServerError r1 = r0.a()
                if (r1 == 0) goto L_0x001a
                com.fossil.cv4 r1 = r8.this$0
                com.portfolio.platform.data.model.ServerError r2 = r0.a()
                java.lang.Integer r2 = r2.getCode()
                java.lang.String r3 = "result.error.code"
                com.fossil.pq7.b(r2, r3)
                int r2 = r2.intValue()
                com.portfolio.platform.data.model.ServerError r0 = r0.a()
                java.lang.String r0 = r0.getMessage()
                com.fossil.cv4.a(r1, r2, r0)
                goto L_0x001a
            L_0x00b6:
                r2 = r0
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.cv4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String name = cv4.class.getName();
        pq7.b(name, "BCCreateSocialProfileViewModel::class.java.name");
        i = name;
    }
    */

    @DexIgnore
    public cv4(hu4 hu4, vt4 vt4, on5 on5) {
        pq7.c(hu4, "socialProfileRepository");
        pq7.c(vt4, "fcmRepository");
        pq7.c(on5, "sharePrefs");
        this.f = hu4;
        this.g = vt4;
        this.h = on5;
    }

    @DexIgnore
    public static /* synthetic */ void k(cv4 cv4, boolean z, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        cv4.j(z, z2);
    }

    @DexIgnore
    public final void h(String str) {
        pq7.c(str, "socialId");
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void i(int i2, String str) {
        this.b.l(new a(i2, str));
    }

    @DexIgnore
    public final void j(boolean z, boolean z2) {
        this.f667a.l(new b(z, z2));
    }

    @DexIgnore
    public final LiveData<Boolean> l() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<a> m() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> n() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<b> p() {
        return this.f667a;
    }

    @DexIgnore
    public final void q(String str) {
        boolean z = true;
        pq7.c(str, "socialId");
        if (str.length() > 0) {
            int length = str.length();
            boolean z2 = 6 <= length && 24 >= length;
            boolean matches = j.matcher(str).matches();
            j(matches, z2);
            MutableLiveData<Boolean> mutableLiveData = this.d;
            if (!matches || !z2) {
                z = false;
            }
            mutableLiveData.l(Boolean.valueOf(z));
            return;
        }
        k(this, false, false, 3, null);
        this.d.l(Boolean.FALSE);
    }
}
