package com.fossil;

import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f4343a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaRecommendedPresetRemote$fetchRecommendedPreset$2", f = "DianaRecommendedPresetRemote.kt", l = {12}, m = "invokeSuspend")
    public static final class a extends ko7 implements rp7<qn7<? super q88<ApiResponse<no5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(yo5 yo5, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yo5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new a(this.this$0, this.$serial, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<no5>>> qn7) {
            return ((a) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4343a;
                String str = this.$serial;
                this.label = 1;
                Object fetchRecommendedDianaPreset = apiServiceV2.fetchRecommendedDianaPreset(str, this);
                return fetchRecommendedDianaPreset == d ? d : fetchRecommendedDianaPreset;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public yo5(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f4343a = apiServiceV2;
    }

    @DexIgnore
    public final Object b(String str, qn7<? super iq5<ApiResponse<no5>>> qn7) {
        return jq5.d(new a(this, str, null), qn7);
    }
}
