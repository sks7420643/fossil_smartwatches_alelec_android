package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.m64;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ w84 f3727a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a84 f3728a;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService b;
        @DexIgnore
        public /* final */ /* synthetic */ qc4 c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ w84 e;

        @DexIgnore
        public a(a84 a84, ExecutorService executorService, qc4 qc4, boolean z, w84 w84) {
            this.f3728a = a84;
            this.b = executorService;
            this.c = qc4;
            this.d = z;
            this.e = w84;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            this.f3728a.c(this.b, this.c);
            if (!this.d) {
                return null;
            }
            this.e.g(this.c);
            return null;
        }
    }

    @DexIgnore
    public v74(w84 w84) {
        this.f3727a = w84;
    }

    @DexIgnore
    public static v74 a() {
        v74 v74 = (v74) j64.h().f(v74.class);
        if (v74 != null) {
            return v74;
        }
        throw new NullPointerException("FirebaseCrashlytics component is not present.");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v7, types: [com.fossil.f84] */
    /* JADX WARN: Type inference failed for: r1v9, types: [com.fossil.t74] */
    /* JADX WARN: Type inference failed for: r5v7, types: [com.fossil.c84, com.fossil.e84] */
    /* JADX WARN: Type inference failed for: r6v5, types: [com.fossil.c84, com.fossil.d84] */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.v74 b(com.fossil.j64 r11, com.fossil.lg4 r12, com.fossil.w74 r13, com.fossil.m64 r14) {
        /*
            android.content.Context r8 = r11.g()
            com.fossil.h94 r2 = new com.fossil.h94
            java.lang.String r0 = r8.getPackageName()
            r2.<init>(r8, r0, r12)
            com.fossil.c94 r4 = new com.fossil.c94
            r4.<init>(r11)
            if (r13 != 0) goto L_0x0073
            com.fossil.y74 r3 = new com.fossil.y74
            r3.<init>()
        L_0x0019:
            com.fossil.a84 r9 = new com.fossil.a84
            r9.<init>(r11, r8, r2, r4)
            if (r14 == 0) goto L_0x0085
            com.fossil.x74 r0 = com.fossil.x74.f()
            java.lang.String r1 = "Firebase Analytics is available."
            r0.b(r1)
            com.fossil.f84 r0 = new com.fossil.f84
            r0.<init>(r14)
            com.fossil.t74 r1 = new com.fossil.t74
            r1.<init>()
            com.fossil.m64$a r5 = e(r14, r1)
            if (r5 == 0) goto L_0x0075
            com.fossil.x74 r5 = com.fossil.x74.f()
            java.lang.String r6 = "Firebase Analytics listener registered successfully."
            r5.b(r6)
            com.fossil.e84 r5 = new com.fossil.e84
            r5.<init>()
            com.fossil.d84 r6 = new com.fossil.d84
            r7 = 500(0x1f4, float:7.0E-43)
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS
            r6.<init>(r0, r7, r10)
            r1.d(r5)
            r1.e(r6)
        L_0x0056:
            com.fossil.w84 r0 = new com.fossil.w84
            java.lang.String r1 = "Crashlytics Exception Handler"
            java.util.concurrent.ExecutorService r7 = com.fossil.f94.c(r1)
            r1 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            boolean r1 = r9.h()
            if (r1 != 0) goto L_0x0099
            com.fossil.x74 r0 = com.fossil.x74.f()
            java.lang.String r1 = "Unable to start Crashlytics."
            r0.d(r1)
            r0 = 0
        L_0x0072:
            return r0
        L_0x0073:
            r3 = r13
            goto L_0x0019
        L_0x0075:
            com.fossil.x74 r1 = com.fossil.x74.f()
            java.lang.String r5 = "Firebase Analytics listener registration failed."
            r1.b(r5)
            com.fossil.j84 r5 = new com.fossil.j84
            r5.<init>()
            r6 = r0
            goto L_0x0056
        L_0x0085:
            com.fossil.x74 r0 = com.fossil.x74.f()
            java.lang.String r1 = "Firebase Analytics is unavailable."
            r0.b(r1)
            com.fossil.j84 r5 = new com.fossil.j84
            r5.<init>()
            com.fossil.g84 r6 = new com.fossil.g84
            r6.<init>()
            goto L_0x0056
        L_0x0099:
            java.lang.String r1 = "com.google.firebase.crashlytics.startup"
            java.util.concurrent.ExecutorService r3 = com.fossil.f94.c(r1)
            com.fossil.qc4 r4 = r9.l(r8, r11, r3)
            com.fossil.v74$a r1 = new com.fossil.v74$a
            boolean r5 = r0.n(r4)
            r2 = r9
            r6 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            com.fossil.qt3.c(r3, r1)
            com.fossil.v74 r1 = new com.fossil.v74
            r1.<init>(r0)
            r0 = r1
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.v74.b(com.fossil.j64, com.fossil.lg4, com.fossil.w74, com.fossil.m64):com.fossil.v74");
    }

    @DexIgnore
    public static m64.a e(m64 m64, t74 t74) {
        m64.a c = m64.c("clx", t74);
        if (c == null) {
            x74.f().b("Could not register AnalyticsConnectorListener with Crashlytics origin.");
            c = m64.c(CrashDumperPlugin.NAME, t74);
            if (c != null) {
                x74.f().i("A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version.");
            }
        }
        return c;
    }

    @DexIgnore
    public void c(String str, String str2) {
        this.f3727a.o(str, str2);
    }

    @DexIgnore
    public void d(String str) {
        this.f3727a.p(str);
    }
}
