package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import coil.memory.BaseRequestDelegate;
import coil.memory.RequestDelegate;
import coil.memory.ViewTargetRequestDelegate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a51 f3370a;
    @DexIgnore
    public /* final */ s61 b;

    @DexIgnore
    public t61(a51 a51, s61 s61) {
        pq7.c(a51, "imageLoader");
        pq7.c(s61, "referenceCounter");
        this.f3370a = a51;
        this.b = s61;
    }

    @DexIgnore
    public final RequestDelegate a(x71 x71, i71 i71, Lifecycle lifecycle, dv7 dv7, rv7<? extends Drawable> rv7) {
        pq7.c(x71, "request");
        pq7.c(i71, "targetDelegate");
        pq7.c(lifecycle, "lifecycle");
        pq7.c(dv7, "mainDispatcher");
        pq7.c(rv7, "deferred");
        if (x71 instanceof t71) {
            j81 u = x71.u();
            if (u instanceof k81) {
                ViewTargetRequestDelegate viewTargetRequestDelegate = new ViewTargetRequestDelegate(this.f3370a, (t71) x71, i71, lifecycle, dv7, rv7);
                lifecycle.a(viewTargetRequestDelegate);
                w81.i(((k81) u).getView()).c(viewTargetRequestDelegate);
                return viewTargetRequestDelegate;
            }
            BaseRequestDelegate baseRequestDelegate = new BaseRequestDelegate(lifecycle, dv7, rv7);
            lifecycle.a(baseRequestDelegate);
            return baseRequestDelegate;
        }
        throw new al7();
    }

    @DexIgnore
    public final i71 b(x71 x71) {
        pq7.c(x71, "request");
        if (x71 instanceof t71) {
            j81 u = x71.u();
            return u == null ? v61.f3723a : u instanceof i81 ? new e71((i81) u, this.b) : new a71(u, this.b);
        }
        throw new al7();
    }
}
