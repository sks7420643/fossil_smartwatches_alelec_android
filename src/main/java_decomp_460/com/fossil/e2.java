package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e2 extends c2 {
    @DexIgnore
    public static /* final */ d2 CREATOR; // = new d2(null);

    @DexIgnore
    public e2(byte b) {
        super(lt.AUTHENTICATION_REQUEST_EVENT, b, false, 4);
    }

    @DexIgnore
    public /* synthetic */ e2(Parcel parcel, kq7 kq7) {
        super(parcel);
    }
}
