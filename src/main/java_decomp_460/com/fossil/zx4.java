package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<Object> f4553a; // = new ArrayList();
    @DexIgnore
    public az4 b; // = new az4(1);
    @DexIgnore
    public yx4 c; // = new yx4(2);
    @DexIgnore
    public sx4 d; // = new sx4(3);

    @DexIgnore
    public final void g(int i) {
        Object obj = this.f4553a.get(i);
        if (!(obj instanceof mt4)) {
            obj = null;
        }
        mt4 mt4 = (mt4) obj;
        if (mt4 != null) {
            mt4.d(false);
        }
        notifyItemChanged(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4553a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.f4553a, i)) {
            return this.b.a();
        }
        if (this.c.b(this.f4553a, i)) {
            return this.c.a();
        }
        if (this.d.b(this.f4553a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    public final Object h(int i) {
        return this.f4553a.get(i);
    }

    @DexIgnore
    public final void i(int i) {
        int size = this.f4553a.size();
        int i2 = i - 1;
        int i3 = i + 1;
        if (i != -1 && i < size) {
            if (size == 2) {
                try {
                    this.f4553a.clear();
                    notifyDataSetChanged();
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("RecommendationChallengeAdapter", "remove : index: " + i + " - size: " + size);
                }
            } else if (i2 == 0 && i3 < size && (this.f4553a.get(i2) instanceof String) && (this.f4553a.get(i3) instanceof String)) {
                this.f4553a.remove(0);
                this.f4553a.remove(0);
                notifyItemRangeRemoved(0, 2);
            } else if (i2 <= -1 || !(this.f4553a.get(i2) instanceof String) || i3 != size) {
                this.f4553a.remove(i);
                notifyItemRemoved(i);
            } else {
                this.f4553a.remove(i);
                notifyItemRemoved(i);
                this.f4553a.remove(i2);
                notifyItemRemoved(i2);
            }
        }
    }

    @DexIgnore
    public final void j(List<? extends Object> list) {
        pq7.c(list, "newData");
        this.f4553a.clear();
        this.f4553a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void k(TimerViewObserver timerViewObserver) {
        pq7.c(timerViewObserver, "observer");
        this.c.e(timerViewObserver);
        this.d.e(timerViewObserver);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == this.b.a()) {
            this.b.c(this.f4553a, i, viewHolder);
        } else if (itemViewType == this.c.a()) {
            this.c.c(this.f4553a, i, viewHolder);
        } else if (itemViewType == this.d.a()) {
            this.d.c(this.f4553a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.d(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.d(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.d(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }
}
