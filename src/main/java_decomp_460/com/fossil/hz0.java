package com.fossil;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ nz0 f1555a;
    @DexIgnore
    public static /* final */ Property<View, Float> b; // = new a(Float.class, "translationAlpha");
    @DexIgnore
    public static /* final */ Property<View, Rect> c; // = new b(Rect.class, "clipBounds");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Property<View, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(hz0.c(view));
        }

        @DexIgnore
        /* renamed from: b */
        public void set(View view, Float f) {
            hz0.h(view, f.floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Property<View, Rect> {
        @DexIgnore
        public b(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Rect get(View view) {
            return mo0.s(view);
        }

        @DexIgnore
        /* renamed from: b */
        public void set(View view, Rect rect) {
            mo0.r0(view, rect);
        }
    }

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            f1555a = new mz0();
        } else if (i >= 23) {
            f1555a = new lz0();
        } else if (i >= 22) {
            f1555a = new kz0();
        } else if (i >= 21) {
            f1555a = new jz0();
        } else if (i >= 19) {
            f1555a = new iz0();
        } else {
            f1555a = new nz0();
        }
    }
    */

    @DexIgnore
    public static void a(View view) {
        f1555a.a(view);
    }

    @DexIgnore
    public static gz0 b(View view) {
        return Build.VERSION.SDK_INT >= 18 ? new fz0(view) : ez0.e(view);
    }

    @DexIgnore
    public static float c(View view) {
        return f1555a.c(view);
    }

    @DexIgnore
    public static rz0 d(View view) {
        return Build.VERSION.SDK_INT >= 18 ? new qz0(view) : new pz0(view.getWindowToken());
    }

    @DexIgnore
    public static void e(View view) {
        f1555a.d(view);
    }

    @DexIgnore
    public static void f(View view, Matrix matrix) {
        f1555a.e(view, matrix);
    }

    @DexIgnore
    public static void g(View view, int i, int i2, int i3, int i4) {
        f1555a.f(view, i, i2, i3, i4);
    }

    @DexIgnore
    public static void h(View view, float f) {
        f1555a.g(view, f);
    }

    @DexIgnore
    public static void i(View view, int i) {
        f1555a.h(view, i);
    }

    @DexIgnore
    public static void j(View view, Matrix matrix) {
        f1555a.i(view, matrix);
    }

    @DexIgnore
    public static void k(View view, Matrix matrix) {
        f1555a.j(view, matrix);
    }
}
