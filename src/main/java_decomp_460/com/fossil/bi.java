package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi extends mj {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ kb F;
    @DexIgnore
    public /* final */ ArrayList<byte[]> G;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<j0> H;
    @DexIgnore
    public /* final */ ArrayList<j0> I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public float L;
    @DexIgnore
    public int M;
    @DexIgnore
    public long N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public /* final */ boolean T;
    @DexIgnore
    public /* final */ float U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ bi(com.fossil.k5 r10, com.fossil.i60 r11, com.fossil.yp r12, short r13, boolean r14, java.util.HashMap r15, float r16, java.lang.String r17, int r18) {
        /*
            r9 = this;
            r1 = r18 & 4
            if (r1 == 0) goto L_0x00a6
            com.fossil.yp r4 = com.fossil.yp.m
        L_0x0006:
            r8 = 0
            r1 = r18 & 16
            if (r1 == 0) goto L_0x000c
            r14 = 0
        L_0x000c:
            r1 = r18 & 32
            if (r1 == 0) goto L_0x0015
            java.util.HashMap r15 = new java.util.HashMap
            r15.<init>()
        L_0x0015:
            r1 = r18 & 64
            if (r1 == 0) goto L_0x001c
            r16 = 981668463(0x3a83126f, float:0.001)
        L_0x001c:
            r0 = r18
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x00b3
            java.lang.String r1 = "UUID.randomUUID().toString()"
            java.lang.String r6 = com.fossil.e.a(r1)
        L_0x0028:
            r7 = 1
            r1 = r9
            r2 = r10
            r3 = r11
            r5 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7)
            r0 = r16
            r9.U = r0
            r9.E = r14
            com.fossil.kb r1 = new com.fossil.kb
            r1.<init>(r13)
            r9.F = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r9.G = r1
            java.util.concurrent.CopyOnWriteArrayList r1 = new java.util.concurrent.CopyOnWriteArrayList
            r1.<init>()
            r9.H = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r9.I = r1
            r1 = -1
            r9.M = r1
            com.fossil.hu1 r1 = com.fossil.hu1.SKIP_LIST
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00a9
            boolean r1 = r1.booleanValue()
        L_0x0063:
            r9.P = r1
            com.fossil.hu1 r1 = com.fossil.hu1.SKIP_ERASE
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00ab
            boolean r1 = r1.booleanValue()
        L_0x0073:
            r9.Q = r1
            com.fossil.hu1 r1 = com.fossil.hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00ad
            boolean r1 = r1.booleanValue()
        L_0x0083:
            r9.R = r1
            com.fossil.hu1 r1 = com.fossil.hu1.NUMBER_OF_FILE_REQUIRED
            java.lang.Object r1 = r15.get(r1)
            java.lang.Integer r1 = (java.lang.Integer) r1
            if (r1 == 0) goto L_0x00af
            int r1 = r1.intValue()
        L_0x0093:
            r9.S = r1
            com.fossil.hu1 r1 = com.fossil.hu1.ERASE_CACHE_FILE_BEFORE_GET
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00b1
            boolean r1 = r1.booleanValue()
        L_0x00a3:
            r9.T = r1
            return
        L_0x00a6:
            r4 = r12
            goto L_0x0006
        L_0x00a9:
            r1 = 0
            goto L_0x0063
        L_0x00ab:
            r1 = 0
            goto L_0x0073
        L_0x00ad:
            r1 = 0
            goto L_0x0083
        L_0x00af:
            r1 = 0
            goto L_0x0093
        L_0x00b1:
            r1 = r8
            goto L_0x00a3
        L_0x00b3:
            r6 = r17
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bi.<init>(com.fossil.k5, com.fossil.i60, com.fossil.yp, short, boolean, java.util.HashMap, float, java.lang.String, int):void");
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        if (this.T) {
            V();
        }
        if (!this.R) {
            v(new oh(this));
        }
        if (this.P) {
            this.H.add(new j0(this.w.x, this.D, 4294967295L, 0));
            Y();
            return;
        }
        lp.j(this, hs.v, null, 2, null);
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.mj
    public JSONObject C() {
        JSONObject put = super.C().put(ey1.a(hu1.SKIP_LIST), this.P).put(ey1.a(hu1.SKIP_ERASE), this.Q).put(ey1.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.R).put(ey1.a(hu1.NUMBER_OF_FILE_REQUIRED), this.S).put(ey1.a(hu1.ERASE_CACHE_FILE_BEFORE_GET), this.T);
        pq7.b(put, "super.optionDescription(\u2026 eraseCacheFileBeforeGet)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject put = super.E().put(ey1.a(hu1.SKIP_ERASE), this.Q);
        pq7.b(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        jd0 jd0 = jd0.P2;
        Object[] array = this.I.toArray(new j0[0]);
        if (array != null) {
            return g80.k(put, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final long G(j0 j0Var) {
        u.f3499a.c(j0Var.c, j0Var.d, j0Var.e);
        if (!(j0Var.f.length == 0)) {
            return u.f3499a.d(j0Var);
        }
        k5 k5Var = this.w;
        ky1 ky1 = ky1.DEBUG;
        String str = k5Var.x;
        byte b = j0Var.d;
        byte b2 = j0Var.e;
        return -1;
    }

    @DexIgnore
    public final j0 H(byte b, byte b2) {
        return (j0) pm7.I(u.f3499a.i(this.w.x, b, b2), 0);
    }

    @DexIgnore
    public final void L(oi oiVar) {
        long j = oiVar.D;
        this.N = j;
        j0 j0Var = oiVar.H;
        G(j0.a(j0Var, null, (byte) 0, (byte) 0, dm7.k(j0Var.f, 0, (int) j), 0, 0, 0, false, 119));
        long j2 = this.J;
        float f = j2 > 0 ? (((float) (this.K + this.N)) * 1.0f) / ((float) j2) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (Math.abs(f - this.L) > this.U || f == 1.0f) {
            this.L = f;
            d(f);
        }
        X();
    }

    @DexIgnore
    public void M(ArrayList<j0> arrayList) {
        l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
    }

    @DexIgnore
    public final void O(j0 j0Var) {
        lp.h(this, new oi(this.w, this.x, j0Var, this.z), new ag(this), new og(this), null, null, null, 56, null);
    }

    @DexIgnore
    public void Q(j0 j0Var) {
        ky1 ky1 = ky1.DEBUG;
        j0Var.toJSONString(2);
    }

    @DexIgnore
    public final void S(j0 j0Var) {
        if (j0Var.h != ix1.f1688a.b(j0Var.f, ix1.a.CRC32) || ((int) j0Var.g) != j0Var.f.length) {
            int i = this.O;
            if (i < 3) {
                this.O = i + 1;
                O(j0Var);
                return;
            }
            l(nr.a(this.v, null, zq.INVALID_FILE_CRC, null, null, 13));
        } else if (G(j0.a(j0Var, null, (byte) 0, (byte) 0, null, 0, 0, 0, true, 127)) < 0) {
            l(nr.a(this.v, null, zq.DATABASE_ERROR, null, null, 13));
        } else {
            this.K += j0Var.g;
            this.G.add(j0Var.f);
            Q(j0Var);
            if (this.Q) {
                Y();
            } else {
                lp.j(this, hs.x, null, 2, null);
            }
        }
    }

    @DexIgnore
    public final void V() {
        u.f3499a.b(this.w.x, this.F.b);
    }

    @DexIgnore
    public final j0 W() {
        return (j0) pm7.I(this.H, this.M);
    }

    @DexIgnore
    public final void X() {
        j0 W = W();
        if (W == null) {
            pq7.i();
            throw null;
        } else if (W.g > 0) {
            n(hs.w, new bh(this));
        } else if (this.Q) {
            Y();
        } else {
            lp.j(this, hs.x, null, 2, null);
        }
    }

    @DexIgnore
    public final void Y() {
        int i = this.M + 1;
        this.M = i;
        if (i < this.H.size()) {
            j0 W = W();
            if (W != null) {
                kb kbVar = new kb(W.b());
                j0 H2 = H(kbVar.b, kbVar.c);
                k5 k5Var = this.w;
                ky1 ky1 = ky1.DEBUG;
                String str = k5Var.x;
                if (H2 != null) {
                    H2.toString();
                }
                j0 W2 = W();
                if (W2 == null) {
                    pq7.i();
                    throw null;
                } else if (W2.g <= 0) {
                    Y();
                } else if (H2 != null) {
                    j0 W3 = W();
                    if (W3 != null) {
                        long j = W3.g;
                        j0 W4 = W();
                        if (W4 != null) {
                            S(j0.a(H2, null, (byte) 0, (byte) 0, null, j, W4.h, 0, false, 207));
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    this.N = 0;
                    X();
                }
            } else {
                pq7.i();
                throw null;
            }
        } else if (this.G.size() < this.S) {
            l(nr.a(this.v, null, zq.NOT_ENOUGH_FILE_TO_PROCESS, null, null, 13));
        } else {
            this.I.clear();
            this.I.addAll(u.f3499a.h(this.w.x, this.F.b));
            M(this.I);
        }
    }

    @DexIgnore
    public final void Z() {
        T t;
        boolean z;
        for (T t2 : u.f3499a.j(this.w.x, this.F.b)) {
            Iterator<T> it = this.H.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (t.b() == ByteBuffer.allocate(2).put(t2.d).put(t2.e).getShort(0)) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null && ix1.f1688a.b(t2.f, ix1.a.CRC32) != t2.h) {
                u.f3499a.c(this.w.x, t2.d, t2.e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public fs b(hs hsVar) {
        int i = gq.f1342a[hsVar.ordinal()];
        if (i == 1) {
            iv ivVar = new iv(this.D, this.w, 0, 4);
            ivVar.o(new mf(this));
            return ivVar;
        } else if (i == 2) {
            j0 W = W();
            if (W != null) {
                gv gvVar = new gv(W.b(), this.N, 4294967295L, this.w, 0, 16);
                ir irVar = new ir(this, W);
                if (!gvVar.t) {
                    gvVar.n.add(irVar);
                } else {
                    irVar.invoke(gvVar);
                }
                ye yeVar = new ye(this, W);
                if (!gvVar.t) {
                    gvVar.o.add(yeVar);
                }
                gvVar.s = w();
                return gvVar;
            }
            pq7.i();
            throw null;
        } else if (i != 3) {
            return null;
        } else {
            j0 W2 = W();
            if (W2 != null) {
                cv cvVar = new cv(W2.b(), this.w, 0, 4);
                cvVar.o(new uq(this));
                return cvVar;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public final boolean t() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        Object[] array = this.I.toArray(new j0[0]);
        if (array != null) {
            return array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
