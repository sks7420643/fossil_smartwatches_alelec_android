package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v73 implements xw2<y73> {
    @DexIgnore
    public static v73 c; // = new v73();
    @DexIgnore
    public /* final */ xw2<y73> b;

    @DexIgnore
    public v73() {
        this(ww2.b(new x73()));
    }

    @DexIgnore
    public v73(xw2<y73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((y73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((y73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((y73) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ y73 zza() {
        return this.b.zza();
    }
}
