package com.fossil;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nj2 {
    @DexIgnore
    public static nj2 b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, Map<String, Boolean>> f2531a; // = new zi0();

    @DexIgnore
    public nj2(Context context) {
    }

    @DexIgnore
    public static nj2 a(Context context) {
        nj2 nj2;
        synchronized (nj2.class) {
            try {
                if (b == null) {
                    b = new nj2(context.getApplicationContext());
                }
                nj2 = b;
            } catch (Throwable th) {
                throw th;
            }
        }
        return nj2;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        boolean z;
        synchronized (this) {
            Map<String, Boolean> map = this.f2531a.get(str2);
            if (map == null) {
                map = new zi0<>();
                this.f2531a.put(str2, map);
            }
            z = map.put(str, Boolean.FALSE) == null;
        }
        return z;
    }

    @DexIgnore
    public final void c(String str, String str2) {
        synchronized (this) {
            Map<String, Boolean> map = this.f2531a.get(str2);
            if (map != null) {
                if ((map.remove(str) != null) && map.isEmpty()) {
                    this.f2531a.remove(str2);
                }
            }
        }
    }

    @DexIgnore
    public final boolean d(String str) {
        boolean containsKey;
        synchronized (this) {
            containsKey = this.f2531a.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public final boolean e(String str, String str2) {
        synchronized (this) {
            Map<String, Boolean> map = this.f2531a.get(str2);
            if (map == null) {
                return false;
            }
            Boolean bool = map.get(str);
            if (bool == null) {
                return false;
            }
            return bool.booleanValue();
        }
    }
}
