package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x97 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<fb7> f4083a; // = new ArrayList();
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public /* final */ g77 c;

    @DexIgnore
    public x97(e77 e77) {
        pq7.c(e77, "templateListener");
        this.c = new g77(1, e77);
    }

    @DexIgnore
    public final void g(int i) {
        if (i != this.b) {
            this.c.a(i);
            notifyItemChanged(this.b);
            notifyItemChanged(i);
            this.b = i;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4083a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.c.c(this.f4083a, i)) {
            return this.c.b();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    public final void h(List<fb7> list) {
        pq7.c(list, "data");
        if (!pq7.a(this.f4083a, list)) {
            this.f4083a.clear();
            this.f4083a.addAll(list);
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        if (getItemViewType(i) == this.c.b()) {
            this.c.d(this.f4083a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == this.c.b()) {
            return this.c.e(viewGroup);
        }
        throw new IllegalArgumentException("No delegate for this viewtype : " + i);
    }
}
