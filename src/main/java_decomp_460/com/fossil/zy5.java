package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zy5 extends ps5<yy5> {
    @DexIgnore
    void A(boolean z);

    @DexIgnore
    void C1(Intent intent);

    @DexIgnore
    void L0(FossilDeviceSerialPatternUtil.DEVICE device, int i, boolean z);

    @DexIgnore
    Object O3();  // void declaration

    @DexIgnore
    Object T1();  // void declaration

    @DexIgnore
    void o0(int i);

    @DexIgnore
    void onActivityResult(int i, int i2, Intent intent);

    @DexIgnore
    void z3(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration);
}
