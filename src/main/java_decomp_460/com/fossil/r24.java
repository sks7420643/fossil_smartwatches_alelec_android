package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r24 {
    @DexIgnore
    public static int a(int i, double d) {
        int max = Math.max(i, 2);
        int highestOneBit = Integer.highestOneBit(max);
        if (max <= ((int) (((double) highestOneBit) * d))) {
            return highestOneBit;
        }
        int i2 = highestOneBit << 1;
        if (i2 > 0) {
            return i2;
        }
        return 1073741824;
    }

    @DexIgnore
    public static int b(int i) {
        return Integer.rotateLeft(-862048943 * i, 15) * 461845907;
    }

    @DexIgnore
    public static int c(Object obj) {
        return b(obj == null ? 0 : obj.hashCode());
    }
}
