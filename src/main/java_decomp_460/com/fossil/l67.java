package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l67 extends LinearLayout implements CustomSwipeRefreshLayout.b {
    @DexIgnore
    public ViewGroup b;
    @DexIgnore
    public ImageView c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleTextView e;
    @DexIgnore
    public FlexibleTextView f;
    @DexIgnore
    public FlexibleTextView g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ String j; // = qn5.l.a().d("nonBrandSurface");
    @DexIgnore
    public wa1 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l67 f2149a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(l67 l67) {
            this.f2149a = l67;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            va1<Drawable> t = this.f2149a.getMRequestManager$app_fossilRelease().t(str2);
            ImageView ivDevice$app_fossilRelease = this.f2149a.getIvDevice$app_fossilRelease();
            if (ivDevice$app_fossilRelease != null) {
                t.F0(ivDevice$app_fossilRelease);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l67(Context context) {
        super(context);
        pq7.c(context, "context");
        wa1 t = oa1.t(PortfolioApp.h0.c());
        pq7.b(t, "Glide.with(PortfolioApp.instance)");
        this.k = t;
        setWillNotDraw(false);
        b();
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.b
    public void a(CustomSwipeRefreshLayout.d dVar, CustomSwipeRefreshLayout.d dVar2) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        pq7.c(dVar, "currentState");
        pq7.c(dVar2, "lastState");
        int a2 = dVar.a();
        int a3 = dVar2.a();
        if (a2 != a3) {
            if (a2 == 0) {
                FlexibleTextView flexibleTextView3 = this.e;
                if (flexibleTextView3 != null) {
                    flexibleTextView3.setText(um5.c(getContext(), 2131886809));
                }
            } else if (a2 != 1) {
                if (a2 == 2) {
                    FlexibleTextView flexibleTextView4 = this.e;
                    if (flexibleTextView4 != null) {
                        flexibleTextView4.setText(um5.c(getContext(), 2131886812));
                    }
                } else if (a2 == 3 && (flexibleTextView2 = this.e) != null) {
                    flexibleTextView2.setText(um5.c(getContext(), 2131886782));
                }
            } else if (!(a3 == 1 || (flexibleTextView = this.e) == null)) {
                flexibleTextView.setText(um5.c(getContext(), 2131886810));
            }
            c(this.h, this.i);
        }
    }

    @DexIgnore
    public final void b() {
        View inflate = LayoutInflater.from(getContext()).inflate(2131558842, (ViewGroup) null);
        if (inflate != null) {
            this.b = (ViewGroup) inflate;
            ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            ViewGroup viewGroup = this.b;
            if (viewGroup != null) {
                this.d = (FlexibleTextView) viewGroup.findViewById(2131362343);
                this.e = (FlexibleTextView) viewGroup.findViewById(2131362351);
                this.f = (FlexibleTextView) viewGroup.findViewById(2131362346);
                this.g = (FlexibleTextView) viewGroup.findViewById(2131362348);
                this.c = (ImageView) viewGroup.findViewById(2131362653);
                String str = this.j;
                if (str != null) {
                    viewGroup.setBackgroundColor(Color.parseColor(str));
                }
                addView(viewGroup, layoutParams);
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @DexIgnore
    public final void c(String str, String str2) {
        String format;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeadViewCard", "updateStaticInfo - deviceSerial=" + str);
        this.h = str;
        this.i = str2;
        PortfolioApp c2 = PortfolioApp.h0.c();
        if (TextUtils.isEmpty(str)) {
            FlexibleTextView flexibleTextView = this.g;
            if (flexibleTextView != null) {
                flexibleTextView.setVisibility(0);
                return;
            }
            return;
        }
        FlexibleTextView flexibleTextView2 = this.g;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setVisibility(8);
        }
        FlexibleTextView flexibleTextView3 = this.d;
        if (flexibleTextView3 != null) {
            flexibleTextView3.setText(str2);
        }
        long C = new on5(c2).C(c2.J());
        if (C <= 0) {
            format = um5.c(c2, 2131887397);
            pq7.b(format, "LanguageHelper.getString\u2026 R.string.empty_hour_min)");
        } else {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTimeInMillis(C);
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                hr7 hr7 = hr7.f1520a;
                String c3 = um5.c(c2, 2131886691);
                pq7.b(c3, "LanguageHelper.getString\u2026ay_Title__TodayMonthDate)");
                format = String.format(c3, Arrays.copyOf(new Object[]{lk5.q(instance.getTime())}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            } else {
                Calendar instance2 = Calendar.getInstance();
                pq7.b(instance2, "Calendar.getInstance()");
                long timeInMillis = (instance2.getTimeInMillis() - C) / 3600000;
                long j2 = (long) 24;
                if (timeInMillis < j2) {
                    hr7 hr72 = hr7.f1520a;
                    String c4 = um5.c(c2, 2131887556);
                    pq7.b(c4, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    format = String.format(c4, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis)}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                } else {
                    hr7 hr73 = hr7.f1520a;
                    String c5 = um5.c(c2, 2131887556);
                    pq7.b(c5, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    format = String.format(c5, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis / j2)}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                }
            }
        }
        FlexibleTextView flexibleTextView4 = this.f;
        if (flexibleTextView4 != null) {
            hr7 hr74 = hr7.f1520a;
            String c6 = um5.c(c2, 2131887179);
            pq7.b(c6, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
            String format2 = String.format(c6, Arrays.copyOf(new Object[]{format}, 1));
            pq7.b(format2, "java.lang.String.format(format, *args)");
            flexibleTextView4.setText(format2);
        }
        FlexibleTextView flexibleTextView5 = this.f;
        if (flexibleTextView5 != null) {
            flexibleTextView5.setSelected(true);
        }
        CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
        if (str != null) {
            CloudImageHelper.ItemImage type = with.setSerialNumber(str).setSerialPrefix(nk5.o.m(str)).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.c;
            if (imageView != null) {
                type.setPlaceHolder(imageView, nk5.o.i(str, nk5.b.SMALL)).setImageCallback(new a(this)).download();
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final ImageView getIvDevice$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final wa1 getMRequestManager$app_fossilRelease() {
        return this.k;
    }

    @DexIgnore
    public final void setIvDevice$app_fossilRelease(ImageView imageView) {
        this.c = imageView;
    }

    @DexIgnore
    public final void setMRequestManager$app_fossilRelease(wa1 wa1) {
        pq7.c(wa1, "<set-?>");
        this.k = wa1;
    }
}
