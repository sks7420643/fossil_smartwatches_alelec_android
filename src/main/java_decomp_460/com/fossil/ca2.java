package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ z62 b;
    @DexIgnore
    public /* final */ /* synthetic */ da2 c;

    @DexIgnore
    public ca2(da2 da2, z62 z62) {
        this.c = da2;
        this.b = z62;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(Boolean.TRUE);
            this.c.g.sendMessage(this.c.g.obtainMessage(0, this.c.f755a.b(this.b)));
            BasePendingResult.p.set(Boolean.FALSE);
            da2 da2 = this.c;
            da2.c(this.b);
            r62 r62 = (r62) this.c.f.get();
            if (r62 != null) {
                r62.s(this.c);
            }
        } catch (RuntimeException e) {
            this.c.g.sendMessage(this.c.g.obtainMessage(1, e));
            BasePendingResult.p.set(Boolean.FALSE);
            da2 da22 = this.c;
            da2.c(this.b);
            r62 r622 = (r62) this.c.f.get();
            if (r622 != null) {
                r622.s(this.c);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(Boolean.FALSE);
            da2 da23 = this.c;
            da2.c(this.b);
            r62 r623 = (r62) this.c.f.get();
            if (r623 != null) {
                r623.s(this.c);
            }
            throw th;
        }
    }
}
