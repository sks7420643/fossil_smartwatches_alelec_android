package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao extends ro {
    @DexIgnore
    public /* final */ yn1 S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ao(k5 k5Var, i60 i60, yn1 yn1, short s, String str, int i) {
        super(k5Var, i60, yp.A, true, (i & 8) != 0 ? ke.b.b(k5Var.x, ob.MUSIC_CONTROL) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 160);
        this.S = yn1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.v, this.S.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ro
    public byte[] M() {
        try {
            aa aaVar = aa.d;
            short s = this.D;
            ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.MUSIC_CONTROL.b));
            if (ry1 == null) {
                ry1 = hd0.y.d();
            }
            return aaVar.a(s, ry1, this.S);
        } catch (sx1 e) {
            return new byte[0];
        }
    }
}
