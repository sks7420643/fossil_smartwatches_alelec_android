package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kb0 extends mb0 {
    @DexIgnore
    public static /* final */ jb0 CREATOR; // = new jb0(null);
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ kb0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    public kb0(bv1 bv1, ry1 ry1, boolean z) {
        super(bv1, ry1);
        this.d = z;
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public List<va0> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new za0());
        if (this.d) {
            arrayList.add(new ia0(new r90[]{new r90(ca0.c, w90.d, da0.POSITION, ea0.c, 0), new r90(ca0.c, w90.d, da0.POSITION, ea0.c, 0)}));
        }
        arrayList.add(new ga0(fa0.c));
        arrayList.add(new ka0());
        return arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(kb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((kb0) obj).d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse");
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public int hashCode() {
        return Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.mb0
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.w3, Integer.valueOf(this.d ? 1 : 0));
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
