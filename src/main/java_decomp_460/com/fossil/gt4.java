package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt4 implements ft4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f1355a;
    @DexIgnore
    public /* final */ jw0<dt4> b;
    @DexIgnore
    public /* final */ et4 c; // = new et4();
    @DexIgnore
    public /* final */ xw0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<dt4> {
        @DexIgnore
        public a(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, dt4 dt4) {
            if (dt4.e() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dt4.e());
            }
            if (dt4.i() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dt4.i());
            }
            if (dt4.a() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dt4.a());
            }
            String b = gt4.this.c.b(dt4.d());
            if (b == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, b);
            }
            String a2 = gt4.this.c.a(dt4.b());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            String c = gt4.this.c.c(dt4.f());
            if (c == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, c);
            }
            px0.bindLong(7, dt4.c() ? 1 : 0);
            px0.bindLong(8, (long) dt4.g());
            String b2 = gt4.this.c.b(dt4.h());
            if (b2 == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, b2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notification` (`id`,`titleKey`,`bodyKey`,`createdAt`,`challengeData`,`profileData`,`confirmChallenge`,`rank`,`reachGoalAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(gt4 gt4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM notification WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(gt4 gt4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM notification";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<List<dt4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f1357a;

        @DexIgnore
        public d(tw0 tw0) {
            this.f1357a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public List<dt4> call() throws Exception {
            Cursor b2 = ex0.b(gt4.this.f1355a, this.f1357a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "titleKey");
                int c3 = dx0.c(b2, "bodyKey");
                int c4 = dx0.c(b2, "createdAt");
                int c5 = dx0.c(b2, "challengeData");
                int c6 = dx0.c(b2, "profileData");
                int c7 = dx0.c(b2, "confirmChallenge");
                int c8 = dx0.c(b2, "rank");
                int c9 = dx0.c(b2, "reachGoalAt");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new dt4(b2.getString(c), b2.getString(c2), b2.getString(c3), gt4.this.c.f(b2.getString(c4)), gt4.this.c.d(b2.getString(c5)), gt4.this.c.e(b2.getString(c6)), b2.getInt(c7) != 0, b2.getInt(c8), gt4.this.c.f(b2.getString(c9))));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f1357a.m();
        }
    }

    @DexIgnore
    public gt4(qw0 qw0) {
        this.f1355a = qw0;
        this.b = new a(qw0);
        new b(this, qw0);
        this.d = new c(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.ft4
    public void a() {
        this.f1355a.assertNotSuspendingTransaction();
        px0 acquire = this.d.acquire();
        this.f1355a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f1355a.setTransactionSuccessful();
        } finally {
            this.f1355a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ft4
    public LiveData<List<dt4>> b() {
        tw0 f = tw0.f("SELECT * FROM notification ORDER BY createdAt DESC", 0);
        nw0 invalidationTracker = this.f1355a.getInvalidationTracker();
        d dVar = new d(f);
        return invalidationTracker.d(new String[]{"notification"}, false, dVar);
    }

    @DexIgnore
    @Override // com.fossil.ft4
    public Long[] insert(List<dt4> list) {
        this.f1355a.assertNotSuspendingTransaction();
        this.f1355a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f1355a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f1355a.endTransaction();
        }
    }
}
