package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v42 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static se2 f3714a; // = new se2("GoogleSignInCommon", new String[0]);

    @DexIgnore
    public static l42 a(Intent intent) {
        if (intent == null || (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount"))) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.f;
        }
        return new l42(googleSignInAccount, status);
    }

    @DexIgnore
    public static Intent b(Context context, GoogleSignInOptions googleSignInOptions) {
        f3714a.a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    @DexIgnore
    public static t62<Status> c(r62 r62, Context context, boolean z) {
        f3714a.a("Signing out", new Object[0]);
        d(context);
        return z ? u62.b(Status.f, r62) : r62.j(new w42(r62));
    }

    @DexIgnore
    public static void d(Context context) {
        b52.c(context).a();
        for (r62 r62 : r62.k()) {
            r62.p();
        }
        l72.b();
    }

    @DexIgnore
    public static Intent e(Context context, GoogleSignInOptions googleSignInOptions) {
        f3714a.a("getFallbackSignInIntent()", new Object[0]);
        Intent b = b(context, googleSignInOptions);
        b.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return b;
    }

    @DexIgnore
    public static t62<Status> f(r62 r62, Context context, boolean z) {
        f3714a.a("Revoking access", new Object[0]);
        String e = o42.b(context).e();
        d(context);
        return z ? r42.a(e) : r62.j(new y42(r62));
    }

    @DexIgnore
    public static Intent g(Context context, GoogleSignInOptions googleSignInOptions) {
        f3714a.a("getNoImplementationSignInIntent()", new Object[0]);
        Intent b = b(context, googleSignInOptions);
        b.setAction("com.google.android.gms.auth.NO_IMPL");
        return b;
    }
}
