package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us extends rs {
    @DexIgnore
    public /* final */ ho1 A;

    @DexIgnore
    public us(ho1 ho1, k5 k5Var) {
        super(hs.c0, k5Var);
        this.A = ho1;
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        n6 n6Var = n6.ASYNC;
        ho1 ho1 = this.A;
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(8).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(ot.c.b);
        order.put(lt.APP_NOTIFICATION_EVENT.b);
        order.putInt(ho1.getNotificationUid());
        order.put(ho1.getAction().a());
        order.put(ho1.getActionStatus().a());
        byte[] array = order.array();
        pq7.b(array, "appNotificationEventData.array()");
        return new j6(n6Var, array, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.e4, this.A.toJSONObject());
    }
}
