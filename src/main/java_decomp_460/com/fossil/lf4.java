package com.fossil;

import android.os.Handler;
import android.os.Message;
import com.fossil.kf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class lf4 implements Handler.Callback {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ kf4.b f2189a;

    @DexIgnore
    public lf4(kf4.b bVar) {
        this.f2189a = bVar;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.f2189a.h(message);
    }
}
