package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a74<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<Class<? super T>> f207a;
    @DexIgnore
    public /* final */ Set<k74> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ d74<T> e;
    @DexIgnore
    public /* final */ Set<Class<?>> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Set<Class<? super T>> f208a;
        @DexIgnore
        public /* final */ Set<k74> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public d74<T> e;
        @DexIgnore
        public Set<Class<?>> f;

        @DexIgnore
        @SafeVarargs
        public b(Class<T> cls, Class<? super T>... clsArr) {
            this.f208a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.d = 0;
            this.f = new HashSet();
            r74.c(cls, "Null interface");
            this.f208a.add(cls);
            for (Class<? super T> cls2 : clsArr) {
                r74.c(cls2, "Null interface");
            }
            Collections.addAll(this.f208a, clsArr);
        }

        @DexIgnore
        public b<T> b(k74 k74) {
            r74.c(k74, "Null dependency");
            i(k74.a());
            this.b.add(k74);
            return this;
        }

        @DexIgnore
        public b<T> c() {
            h(1);
            return this;
        }

        @DexIgnore
        public a74<T> d() {
            r74.d(this.e != null, "Missing required property: factory.");
            return new a74<>(new HashSet(this.f208a), new HashSet(this.b), this.c, this.d, this.e, this.f);
        }

        @DexIgnore
        public b<T> e() {
            h(2);
            return this;
        }

        @DexIgnore
        public b<T> f(d74<T> d74) {
            r74.c(d74, "Null factory");
            this.e = d74;
            return this;
        }

        @DexIgnore
        public final b<T> g() {
            this.d = 1;
            return this;
        }

        @DexIgnore
        public final b<T> h(int i) {
            r74.d(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public final void i(Class<?> cls) {
            r74.a(!this.f208a.contains(cls), "Components are not allowed to depend on interfaces they themselves provide.");
        }
    }

    @DexIgnore
    public a74(Set<Class<? super T>> set, Set<k74> set2, int i, int i2, d74<T> d74, Set<Class<?>> set3) {
        this.f207a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = d74;
        this.f = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0]);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> b<T> b(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr);
    }

    @DexIgnore
    public static <T> a74<T> g(T t, Class<T> cls) {
        b h = h(cls);
        h.f(z64.b(t));
        return h.d();
    }

    @DexIgnore
    public static <T> b<T> h(Class<T> cls) {
        b<T> a2 = a(cls);
        a2.g();
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ Object l(Object obj, b74 b74) {
        return obj;
    }

    @DexIgnore
    public static /* synthetic */ Object m(Object obj, b74 b74) {
        return obj;
    }

    @DexIgnore
    @SafeVarargs
    public static <T> a74<T> n(T t, Class<T> cls, Class<? super T>... clsArr) {
        b b2 = b(cls, clsArr);
        b2.f(y64.b(t));
        return b2.d();
    }

    @DexIgnore
    public Set<k74> c() {
        return this.b;
    }

    @DexIgnore
    public d74<T> d() {
        return this.e;
    }

    @DexIgnore
    public Set<Class<? super T>> e() {
        return this.f207a;
    }

    @DexIgnore
    public Set<Class<?>> f() {
        return this.f;
    }

    @DexIgnore
    public boolean i() {
        return this.c == 1;
    }

    @DexIgnore
    public boolean j() {
        return this.c == 2;
    }

    @DexIgnore
    public boolean k() {
        return this.d == 0;
    }

    @DexIgnore
    public String toString() {
        return "Component<" + Arrays.toString(this.f207a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }
}
