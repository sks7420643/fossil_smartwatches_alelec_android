package com.fossil;

import com.fossil.o91;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ha1 extends ia1<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ha1(int i, String str, JSONObject jSONObject, o91.b<JSONObject> bVar, o91.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ha1(String str, JSONObject jSONObject, o91.b<JSONObject> bVar, o91.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.m91, com.fossil.ia1
    public o91<JSONObject> parseNetworkResponse(j91 j91) {
        try {
            return o91.c(new JSONObject(new String(j91.b, ba1.d(j91.c, ia1.PROTOCOL_CHARSET))), ba1.c(j91));
        } catch (UnsupportedEncodingException e) {
            return o91.a(new l91(e));
        } catch (JSONException e2) {
            return o91.a(new l91(e2));
        }
    }
}
