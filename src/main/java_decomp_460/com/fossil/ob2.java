package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ob2 implements Parcelable.Creator<Status> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Status createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        PendingIntent pendingIntent = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l == 2) {
                str = ad2.f(parcel, t);
            } else if (l == 3) {
                pendingIntent = (PendingIntent) ad2.e(parcel, t, PendingIntent.CREATOR);
            } else if (l != 1000) {
                ad2.B(parcel, t);
            } else {
                i2 = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new Status(i2, i, str, pendingIntent);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Status[] newArray(int i) {
        return new Status[i];
    }
}
