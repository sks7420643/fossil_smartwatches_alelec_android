package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.nk5;
import com.fossil.oi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class df6 extends pv5 implements bf6 {
    @DexIgnore
    public g37<e25> g;
    @DexIgnore
    public af6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        public final void onClick(View view) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActivityOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.bf6
    public void G(mv5 mv5, ArrayList<String> arrayList) {
        e25 a2;
        OverviewDayChart overviewDayChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        g37<e25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(fd5 fd5, WorkoutSession workoutSession) {
        int i2;
        String str;
        if (fd5 != null) {
            View n = fd5.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            oi5.a aVar = oi5.Companion;
            cl7<Integer, Integer> a2 = aVar.a(aVar.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = um5.c(context, a2.getSecond().intValue());
            fd5.t.setImageResource(a2.getFirst().intValue());
            nk5.a aVar2 = nk5.o;
            af6 af6 = this.h;
            String d = aVar2.w(af6 != null ? af6.n() : null) ? qn5.l.a().d("dianaStepsTab") : qn5.l.a().d("hybridStepsTab");
            if (d != null) {
                fd5.t.setColorFilter(Color.parseColor(d));
            }
            FlexibleTextView flexibleTextView = fd5.r;
            pq7.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            mi5 editedType = workoutSession.getEditedType();
            if (editedType == null) {
                editedType = workoutSession.getWorkoutType();
            }
            FlexibleTextView flexibleTextView2 = fd5.s;
            pq7.b(flexibleTextView2, "it.ftvWorkoutValue");
            if (editedType != null && ((i2 = cf6.f608a[editedType.ordinal()]) == 1 || i2 == 2)) {
                str = "";
            } else {
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(context, 2131886682);
                pq7.b(c2, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
                Integer totalSteps = workoutSession.getTotalSteps();
                str = String.format(c2, Arrays.copyOf(new Object[]{dl5.c(totalSteps != null ? (float) totalSteps.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1)}, 1));
                pq7.b(str, "java.lang.String.format(format, *args)");
            }
            flexibleTextView2.setText(str);
            FlexibleTextView flexibleTextView3 = fd5.q;
            pq7.b(flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(lk5.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }

    @DexIgnore
    public final void L6() {
        e25 a2;
        OverviewDayChart overviewDayChart;
        g37<e25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            nk5.a aVar = nk5.o;
            af6 af6 = this.h;
            if (aVar.w(af6 != null ? af6.n() : null)) {
                overviewDayChart.D("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(af6 af6) {
        pq7.c(af6, "presenter");
        this.h = af6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        e25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onCreateView");
        e25 e25 = (e25) aq0.f(layoutInflater, 2131558498, viewGroup, false, A6());
        e25.r.setOnClickListener(a.b);
        this.g = new g37<>(this, e25);
        L6();
        g37<e25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onResume");
        L6();
        af6 af6 = this.h;
        if (af6 != null) {
            af6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onStop");
        af6 af6 = this.h;
        if (af6 != null) {
            af6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.bf6
    public void v(boolean z, List<WorkoutSession> list) {
        e25 a2;
        View n;
        View n2;
        pq7.c(list, "workoutSessions");
        g37<e25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                pq7.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    fd5 fd5 = a2.t;
                    if (fd5 != null && (n2 = fd5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                fd5 fd52 = a2.t;
                if (!(fd52 == null || (n = fd52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            pq7.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
