package com.fossil;

import android.content.Context;
import com.google.android.material.internal.CheckableImageButton;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s04 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public TextInputLayout f3188a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public CheckableImageButton c;

    @DexIgnore
    public s04(TextInputLayout textInputLayout) {
        this.f3188a = textInputLayout;
        this.b = textInputLayout.getContext();
        this.c = textInputLayout.getEndIconView();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public boolean b(int i) {
        return true;
    }

    @DexIgnore
    public boolean c() {
        return false;
    }
}
