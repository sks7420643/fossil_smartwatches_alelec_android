package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import com.fossil.hp6;
import com.fossil.hq4;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep6 extends qv5 implements t47.g {
    @DexIgnore
    public static /* final */ /* synthetic */ ks7[] t;
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public hp6 i;
    @DexIgnore
    public g37<na5> j;
    @DexIgnore
    public /* final */ InputMethodManager k;
    @DexIgnore
    public /* final */ or7 l;
    @DexIgnore
    public rh5 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ep6.u;
        }

        @DexIgnore
        public final ep6 b() {
            return new ep6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<u37<? extends hp6.e>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ep6 f968a;

        @DexIgnore
        public b(ep6 ep6) {
            this.f968a = ep6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(u37<? extends hp6.e> u37) {
            hp6.e eVar = (hp6.e) u37.a();
            if (eVar == null) {
                return;
            }
            if (eVar instanceof hp6.e.a) {
                ep6 ep6 = this.f968a;
                ep6.Y6(ep6.m);
            } else if (eVar instanceof hp6.e.c) {
                this.f968a.e7();
            } else if (eVar instanceof hp6.e.d) {
                hp6.e.d dVar = (hp6.e.d) eVar;
                this.f968a.l5(dVar.a(), dVar.b());
            } else if (eVar instanceof hp6.e.b) {
                this.f968a.W6();
            } else if (eVar instanceof hp6.e.C0114e) {
                ep6 ep62 = this.f968a;
                Object[] array = ((hp6.e.C0114e) eVar).a().toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    ep62.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ep6 f969a;

        @DexIgnore
        public c(ep6 ep6) {
            this.f969a = ep6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = ep6.v.a();
                local.d(a2, "loadingState start " + bVar.a() + " stop " + bVar.b());
                if (bVar.a()) {
                    this.f969a.b();
                }
                if (bVar.b()) {
                    this.f969a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ep6 c;

        @DexIgnore
        public d(na5 na5, ep6 ep6) {
            this.b = na5;
            this.c = ep6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                if (editable.length() > 0) {
                    int g = dl5.g(editable.toString());
                    if (g <= 9 && editable.length() > 1) {
                        this.b.w.setText(String.valueOf(g));
                    }
                    this.c.V6(g);
                    return;
                }
                this.b.w.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence != null) {
                if (charSequence.length() > 0) {
                    this.b.w.removeTextChangedListener(this);
                    String obj = charSequence.toString();
                    if (wt7.v(obj, ",", false, 2, null)) {
                        obj = new jt7(",").replace(obj, "");
                    }
                    this.b.w.setText(dl5.f(Integer.parseInt(obj)));
                    FlexibleEditText flexibleEditText = this.b.w;
                    pq7.b(flexibleEditText, "binding.fetGoalsValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        flexibleEditText.setSelection(text.length());
                        this.b.w.addTextChangedListener(this);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                this.b.w.setText(String.valueOf(0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ep6 c;

        @DexIgnore
        public e(na5 na5, ep6 ep6) {
            this.b = na5;
            this.c = ep6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            int i;
            int i2 = 16;
            if (editable != null) {
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.x.setText(String.valueOf(parseInt));
                    }
                    if (parseInt > 16) {
                        this.b.x.setText(String.valueOf(16));
                        s37 s37 = s37.c;
                        FragmentManager childFragmentManager = this.c.getChildFragmentManager();
                        pq7.b(childFragmentManager, "childFragmentManager");
                        s37.s0(childFragmentManager, this.c.m, 16);
                    } else {
                        i2 = parseInt;
                    }
                    ep6 ep6 = this.c;
                    FlexibleEditText flexibleEditText = this.b.y;
                    pq7.b(flexibleEditText, "binding.fetSleepMinuteValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        pq7.b(text, "binding.fetSleepMinuteValue.text!!");
                        if (text.length() > 0) {
                            FlexibleEditText flexibleEditText2 = this.b.y;
                            pq7.b(flexibleEditText2, "binding.fetSleepMinuteValue");
                            i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                        } else {
                            i = 0;
                        }
                        ep6.L6(this.c).P(ep6.i7(i, i2), this.c.m);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                this.b.x.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ep6 c;

        @DexIgnore
        public f(na5 na5, ep6 ep6) {
            this.b = na5;
            this.c = ep6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            int i;
            int i2 = 59;
            if (editable != null) {
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.y.setText(String.valueOf(parseInt));
                    }
                    if (this.c.m == rh5.TOTAL_SLEEP) {
                        if (parseInt > 59) {
                            this.b.y.setText(String.valueOf(59));
                        } else {
                            i2 = parseInt;
                        }
                        ep6 ep6 = this.c;
                        FlexibleEditText flexibleEditText = this.b.y;
                        pq7.b(flexibleEditText, "binding.fetSleepMinuteValue");
                        Editable text = flexibleEditText.getText();
                        if (text != null) {
                            pq7.b(text, "binding.fetSleepMinuteValue.text!!");
                            if (text.length() > 0) {
                                FlexibleEditText flexibleEditText2 = this.b.x;
                                pq7.b(flexibleEditText2, "binding.fetSleepHourValue");
                                i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                            } else {
                                i = 0;
                            }
                            parseInt = ep6.i7(i2, i);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    ep6.L6(this.c).P(parseInt, this.c.m);
                    return;
                }
                this.b.y.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ep6 c;

        @DexIgnore
        public g(na5 na5, ep6 ep6) {
            this.b = na5;
            this.c = ep6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (this.c.m == rh5.TOTAL_SLEEP) {
                ep6 ep6 = this.c;
                FlexibleEditText flexibleEditText = this.b.y;
                pq7.b(flexibleEditText, "binding.fetSleepMinuteValue");
                ep6.X6(z, flexibleEditText.isFocused());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ep6 c;

        @DexIgnore
        public h(na5 na5, ep6 ep6) {
            this.b = na5;
            this.c = ep6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            ep6 ep6 = this.c;
            FlexibleEditText flexibleEditText = this.b.x;
            pq7.b(flexibleEditText, "binding.fetSleepHourValue");
            ep6.X6(flexibleEditText.isFocused(), z);
            if (z) {
                FlexibleEditText flexibleEditText2 = this.b.x;
                pq7.b(flexibleEditText2, "binding.fetSleepHourValue");
                if (Integer.parseInt(String.valueOf(flexibleEditText2.getText())) == 16) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = this.c.getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.s0(childFragmentManager, rh5.TOTAL_SLEEP, 16);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<hp6.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ na5 f970a;

        @DexIgnore
        public i(na5 na5) {
            this.f970a = na5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hp6.c cVar) {
            this.f970a.u.setValue(cVar.i());
            this.f970a.v.setValue(cVar.d());
            this.f970a.s.setValue(cVar.f());
            this.f970a.t.setValue(cVar.h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<hp6.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ na5 f971a;

        @DexIgnore
        public j(na5 na5) {
            this.f971a = na5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hp6.c cVar) {
            FLogger.INSTANCE.getLocal().d("Brown", cVar.toString());
            this.f971a.u.setValue(cVar.i());
            this.f971a.v.setValue(cVar.f());
            this.f971a.s.setValue(cVar.h());
            this.f971a.t.setValue(cVar.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ep6 b;

        @DexIgnore
        public k(ep6 ep6) {
            this.b = ep6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ep6 b;

        @DexIgnore
        public l(ep6 ep6) {
            this.b = ep6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ep6 ep6 = this.b;
            if (view != null) {
                ep6.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ep6 b;

        @DexIgnore
        public m(ep6 ep6) {
            this.b = ep6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ep6 ep6 = this.b;
            if (view != null) {
                ep6.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ep6 b;

        @DexIgnore
        public n(ep6 ep6) {
            this.b = ep6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ep6 ep6 = this.b;
            if (view != null) {
                ep6.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ep6 b;

        @DexIgnore
        public o(ep6 ep6) {
            this.b = ep6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ep6 ep6 = this.b;
            if (view != null) {
                ep6.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ na5 b;

        @DexIgnore
        public p(na5 na5) {
            this.b = na5;
        }

        @DexIgnore
        public final void run() {
            this.b.J.fullScroll(130);
        }
    }

    /*
    static {
        tq7 tq7 = new tq7(er7.b(ep6.class), "mIsDiana", "getMIsDiana()Z");
        er7.e(tq7);
        t = new ks7[]{tq7};
        String simpleName = ep6.class.getSimpleName();
        pq7.b(simpleName, "ProfileGoalEditFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public ep6() {
        Object systemService = PortfolioApp.h0.c().getSystemService("input_method");
        if (systemService != null) {
            this.k = (InputMethodManager) systemService;
            this.l = mr7.f2413a.a();
            this.m = rh5.TOTAL_STEPS;
            return;
        }
        throw new il7("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public static final /* synthetic */ hp6 L6(ep6 ep6) {
        hp6 hp6 = ep6.i;
        if (hp6 != null) {
            return hp6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        if (getActivity() == null) {
            return true;
        }
        FragmentActivity requireActivity = requireActivity();
        pq7.b(requireActivity, "requireActivity()");
        if (requireActivity.isFinishing()) {
            return true;
        }
        FragmentActivity requireActivity2 = requireActivity();
        pq7.b(requireActivity2, "requireActivity()");
        if (requireActivity2.isDestroyed()) {
            return true;
        }
        hp6 hp6 = this.i;
        if (hp6 != null) {
            hp6.L();
            return true;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        View view = null;
        pq7.c(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -1375614559) {
                if (hashCode != 927377074) {
                    if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
                        if (i2 == 2131363373) {
                            FragmentActivity activity2 = getActivity();
                            if (activity2 != null) {
                                ((ls5) activity2).y();
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                        } else if (i2 == 2131363291) {
                            W6();
                        }
                    }
                } else if (str.equals("GOAL_EXCEED_VALUE") && i2 == 2131363373) {
                    FragmentActivity requireActivity = requireActivity();
                    pq7.b(requireActivity, "requireActivity()");
                    View currentFocus = requireActivity.getCurrentFocus();
                    if (currentFocus instanceof EditText) {
                        view = currentFocus;
                    }
                    h7((EditText) view);
                }
            } else if (!str.equals("UNSAVED_CHANGE")) {
            } else {
                if (i2 == 2131363373) {
                    hp6 hp6 = this.i;
                    if (hp6 != null) {
                        hp6.M();
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                } else if (i2 == 2131363291 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            }
        }
    }

    @DexIgnore
    public final boolean U6() {
        return ((Boolean) this.l.b(this, t[0])).booleanValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004f, code lost:
        if (r9 <= 999) goto L_0x0028;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void V6(int r9) {
        /*
            r8 = this;
            r1 = 999(0x3e7, float:1.4E-42)
            r2 = 480(0x1e0, float:6.73E-43)
            r3 = 1
            r4 = 0
            r7 = 0
            com.fossil.g37<com.fossil.na5> r0 = r8.j
            if (r0 == 0) goto L_0x007d
            java.lang.Object r0 = r0.a()
            com.fossil.na5 r0 = (com.fossil.na5) r0
            if (r0 == 0) goto L_0x004e
            com.fossil.rh5 r5 = r8.m
            int[] r6 = com.fossil.fp6.b
            int r5 = r5.ordinal()
            r5 = r6[r5]
            if (r5 == r3) goto L_0x005e
            r6 = 2
            if (r5 == r6) goto L_0x0057
            r6 = 3
            if (r5 == r6) goto L_0x0053
            r2 = 4
            if (r5 == r2) goto L_0x004f
        L_0x0028:
            r2 = r4
            r1 = r4
        L_0x002a:
            if (r2 == 0) goto L_0x006d
            com.fossil.hp6 r2 = r8.i
            if (r2 == 0) goto L_0x0067
            com.fossil.rh5 r3 = r8.m
            r2.P(r1, r3)
            com.portfolio.platform.view.FlexibleEditText r0 = r0.w
            java.lang.String r2 = com.fossil.dl5.f(r1)
            r0.setText(r2)
            com.fossil.s37 r0 = com.fossil.s37.c
            androidx.fragment.app.FragmentManager r2 = r8.getChildFragmentManager()
            java.lang.String r3 = "childFragmentManager"
            com.fossil.pq7.b(r2, r3)
            com.fossil.rh5 r3 = r8.m
            r0.s0(r2, r3, r1)
        L_0x004e:
            return
        L_0x004f:
            if (r9 <= r1) goto L_0x0028
        L_0x0051:
            r2 = r3
            goto L_0x002a
        L_0x0053:
            if (r9 <= r2) goto L_0x0028
            r1 = r2
            goto L_0x0051
        L_0x0057:
            r1 = 4800(0x12c0, float:6.726E-42)
            if (r9 <= r1) goto L_0x0028
            r1 = 4800(0x12c0, float:6.726E-42)
            goto L_0x0051
        L_0x005e:
            r1 = 50000(0xc350, float:7.0065E-41)
            if (r9 <= r1) goto L_0x0028
            r1 = 50000(0xc350, float:7.0065E-41)
            goto L_0x0051
        L_0x0067:
            java.lang.String r0 = "mViewModel"
            com.fossil.pq7.n(r0)
            throw r7
        L_0x006d:
            com.fossil.hp6 r0 = r8.i
            if (r0 == 0) goto L_0x0077
            com.fossil.rh5 r1 = r8.m
            r0.P(r9, r1)
            goto L_0x004e
        L_0x0077:
            java.lang.String r0 = "mViewModel"
            com.fossil.pq7.n(r0)
            throw r7
        L_0x007d:
            java.lang.String r0 = "mBinding"
            com.fossil.pq7.n(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ep6.V6(int):void");
    }

    @DexIgnore
    public final void W6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void X6(boolean z, boolean z2) {
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                String d2 = qn5.l.a().d("dianaSleepTab");
                String d3 = qn5.l.a().d("nonBrandCoolGray");
                int parseColor = Color.parseColor(d2);
                int parseColor2 = Color.parseColor(d3);
                if (z) {
                    a2.x.setTextColor(parseColor);
                    a2.y.setTextColor(parseColor2);
                } else if (z2) {
                    a2.x.setTextColor(parseColor2);
                    a2.y.setTextColor(parseColor);
                } else {
                    a2.x.setTextColor(parseColor);
                    a2.y.setTextColor(parseColor);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Y6(rh5 rh5) {
        boolean z = true;
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                if (rh5 != null) {
                    this.m = rh5;
                }
                CustomEditGoalView customEditGoalView = a2.u;
                pq7.b(customEditGoalView, "cegvTopLeft");
                customEditGoalView.setSelected(a2.u.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView2 = a2.v;
                pq7.b(customEditGoalView2, "cegvTopRight");
                customEditGoalView2.setSelected(a2.v.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView3 = a2.s;
                pq7.b(customEditGoalView3, "cegvBottomLeft");
                customEditGoalView3.setSelected(a2.s.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView4 = a2.t;
                pq7.b(customEditGoalView4, "cegvBottomRight");
                if (a2.t.getMGoalType() != rh5) {
                    z = false;
                }
                customEditGoalView4.setSelected(z);
                f7(rh5);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Z6() {
        hp6 hp6 = this.i;
        if (hp6 != null) {
            hp6.G().h(getViewLifecycleOwner(), new b(this));
            hp6 hp62 = this.i;
            if (hp62 != null) {
                hp62.j().h(getViewLifecycleOwner(), new c(this));
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(boolean z) {
        this.l.a(this, t[0], Boolean.valueOf(z));
    }

    @DexIgnore
    public final void b7() {
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                a2.w.addTextChangedListener(new d(a2, this));
                a2.x.addTextChangedListener(new e(a2, this));
                a2.y.addTextChangedListener(new f(a2, this));
                FlexibleEditText flexibleEditText = a2.x;
                pq7.b(flexibleEditText, "binding.fetSleepHourValue");
                flexibleEditText.setOnFocusChangeListener(new g(a2, this));
                FlexibleEditText flexibleEditText2 = a2.y;
                pq7.b(flexibleEditText2, "binding.fetSleepMinuteValue");
                flexibleEditText2.setOnFocusChangeListener(new h(a2, this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7() {
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                String d2 = qn5.l.a().d("nonBrandSurface");
                if (d2 != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d2));
                }
                if (U6()) {
                    a2.u.setType(rh5.TOTAL_STEPS);
                    a2.v.setType(rh5.ACTIVE_TIME);
                    a2.s.setType(rh5.CALORIES);
                    a2.t.setType(rh5.TOTAL_SLEEP);
                    hp6 hp6 = this.i;
                    if (hp6 != null) {
                        hp6.H().h(getViewLifecycleOwner(), new i(a2));
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                } else {
                    a2.u.setType(rh5.TOTAL_STEPS);
                    a2.v.setType(rh5.CALORIES);
                    a2.s.setType(rh5.TOTAL_SLEEP);
                    a2.t.setType(rh5.GOAL_TRACKING);
                    hp6 hp62 = this.i;
                    if (hp62 != null) {
                        hp62.H().h(getViewLifecycleOwner(), new j(a2));
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void d7() {
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                a2.F.setOnClickListener(new k(this));
                a2.u.setOnClickListener(new l(this));
                a2.v.setOnClickListener(new m(this));
                a2.s.setOnClickListener(new n(this));
                a2.t.setOnClickListener(new o(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e7() {
        if (isActive()) {
            a();
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
        }
    }

    @DexIgnore
    public final void f7(rh5 rh5) {
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null && rh5 != null) {
                int i2 = fp6.f1176a[rh5.ordinal()];
                if (i2 == 1) {
                    FlexibleTextView flexibleTextView = a2.A;
                    pq7.b(flexibleTextView, "ftvGoalTitle");
                    flexibleTextView.setText(getString(2131886630));
                    FlexibleTextView flexibleTextView2 = a2.z;
                    pq7.b(flexibleTextView2, "ftvDesc");
                    flexibleTextView2.setText(getString(2131887112));
                    String d2 = qn5.l.a().d("dianaActiveCaloriesTab");
                    if (d2 != null) {
                        a2.w.setTextColor(Color.parseColor(d2));
                    }
                    FlexibleEditText flexibleEditText = a2.w;
                    pq7.b(flexibleEditText, "fetGoalsValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf((U6() ? a2.s : a2.v).getValue()));
                    FlexibleTextView flexibleTextView3 = a2.B;
                    pq7.b(flexibleTextView3, "ftvGoalsUnit");
                    flexibleTextView3.setVisibility(8);
                    g7(false);
                } else if (i2 == 2) {
                    FlexibleTextView flexibleTextView4 = a2.A;
                    pq7.b(flexibleTextView4, "ftvGoalTitle");
                    flexibleTextView4.setText(getString(2131886638));
                    FlexibleTextView flexibleTextView5 = a2.z;
                    pq7.b(flexibleTextView5, "ftvDesc");
                    flexibleTextView5.setText(getString(2131887114));
                    String d3 = qn5.l.a().d("dianaActiveMinutesTab");
                    if (d3 != null) {
                        a2.w.setTextColor(Color.parseColor(d3));
                    }
                    FlexibleEditText flexibleEditText2 = a2.w;
                    pq7.b(flexibleEditText2, "fetGoalsValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(5)});
                    a2.w.setText(String.valueOf(a2.v.getValue()));
                    FlexibleTextView flexibleTextView6 = a2.B;
                    pq7.b(flexibleTextView6, "ftvGoalsUnit");
                    flexibleTextView6.setVisibility(8);
                    g7(false);
                } else if (i2 == 3) {
                    FlexibleTextView flexibleTextView7 = a2.A;
                    pq7.b(flexibleTextView7, "ftvGoalTitle");
                    flexibleTextView7.setText(getString(2131887124));
                    FlexibleTextView flexibleTextView8 = a2.z;
                    pq7.b(flexibleTextView8, "ftvDesc");
                    flexibleTextView8.setText(getString(2131887125));
                    String d4 = qn5.l.a().d("hybridStepsTab");
                    if (d4 != null) {
                        a2.w.setTextColor(Color.parseColor(d4));
                    }
                    FlexibleEditText flexibleEditText3 = a2.w;
                    pq7.b(flexibleEditText3, "fetGoalsValue");
                    flexibleEditText3.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf(a2.u.getValue()));
                    FlexibleTextView flexibleTextView9 = a2.B;
                    pq7.b(flexibleTextView9, "ftvGoalsUnit");
                    flexibleTextView9.setVisibility(8);
                    g7(false);
                } else if (i2 == 4) {
                    FlexibleTextView flexibleTextView10 = a2.A;
                    pq7.b(flexibleTextView10, "ftvGoalTitle");
                    flexibleTextView10.setText(getString(2131886767));
                    FlexibleTextView flexibleTextView11 = a2.z;
                    pq7.b(flexibleTextView11, "ftvDesc");
                    flexibleTextView11.setText(getString(2131887118));
                    String d5 = qn5.l.a().d("dianaSleepTab");
                    if (d5 != null) {
                        a2.w.setTextColor(Color.parseColor(d5));
                    }
                    g7(true);
                } else if (i2 == 5) {
                    FlexibleTextView flexibleTextView12 = a2.A;
                    pq7.b(flexibleTextView12, "ftvGoalTitle");
                    flexibleTextView12.setText(getString(2131886741));
                    FlexibleTextView flexibleTextView13 = a2.z;
                    pq7.b(flexibleTextView13, "ftvDesc");
                    flexibleTextView13.setText(getString(2131887130));
                    String d6 = qn5.l.a().d("hybridGoalTrackingTab");
                    if (d6 != null) {
                        a2.w.setTextColor(Color.parseColor(d6));
                    }
                    FlexibleEditText flexibleEditText4 = a2.w;
                    pq7.b(flexibleEditText4, "fetGoalsValue");
                    flexibleEditText4.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf(a2.t.getValue()));
                    FlexibleTextView flexibleTextView14 = a2.B;
                    pq7.b(flexibleTextView14, "ftvGoalsUnit");
                    flexibleTextView14.setVisibility(0);
                    FlexibleTextView flexibleTextView15 = a2.B;
                    pq7.b(flexibleTextView15, "ftvGoalsUnit");
                    flexibleTextView15.setText(getString(2131887129));
                    g7(false);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void g7(boolean z) {
        FLogger.INSTANCE.getLocal().d(u, "showSleepGoalEdit");
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.h0.c();
                if (z) {
                    LinearLayout linearLayout = a2.H;
                    pq7.b(linearLayout, "llSleepGoalValue");
                    linearLayout.setVisibility(0);
                    LinearLayout linearLayout2 = a2.G;
                    pq7.b(linearLayout2, "llGoalsValue");
                    linearLayout2.setVisibility(8);
                    a2.x.setTextColor(Color.parseColor(qn5.l.a().d("dianaSleepTab")));
                    a2.y.setTextColor(gl0.d(c2, 2131099764));
                    FlexibleEditText flexibleEditText = a2.x;
                    pq7.b(flexibleEditText, "fetSleepHourValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleEditText flexibleEditText2 = a2.y;
                    pq7.b(flexibleEditText2, "fetSleepMinuteValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleTextView flexibleTextView = a2.C;
                    pq7.b(flexibleTextView, "ftvSleepHourUnit");
                    flexibleTextView.setText(getString(2131887116));
                    FlexibleTextView flexibleTextView2 = a2.D;
                    pq7.b(flexibleTextView2, "ftvSleepMinuteUnit");
                    flexibleTextView2.setText(getString(2131887117));
                    int mValue = (U6() ? a2.t : a2.s).getMValue() / 60;
                    int mValue2 = (U6() ? a2.t : a2.s).getMValue();
                    a2.x.setText(String.valueOf(mValue));
                    a2.y.setText(String.valueOf(mValue2 % 60));
                    h7(a2.x);
                } else {
                    LinearLayout linearLayout3 = a2.H;
                    pq7.b(linearLayout3, "llSleepGoalValue");
                    linearLayout3.setVisibility(8);
                    LinearLayout linearLayout4 = a2.G;
                    pq7.b(linearLayout4, "llGoalsValue");
                    linearLayout4.setVisibility(0);
                    h7(a2.w);
                }
                a2.J.post(new p(a2));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h7(EditText editText) {
        if (editText != null) {
            gj5.b(editText, this.k);
        }
    }

    @DexIgnore
    public final int i7(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "updateSleepGoal minute: " + i2 + " hour: " + i3);
        g37<na5> g37 = this.j;
        if (g37 != null) {
            na5 a2 = g37.a();
            if (a2 == null) {
                return 0;
            }
            int i4 = (i3 * 60) + i2;
            if (i4 <= 960) {
                return i4;
            }
            a2.y.setText("0");
            a2.x.setText(String.valueOf(16));
            return 960;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void l5(int i2, String str) {
        if (isActive()) {
            a();
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        na5 na5 = (na5) aq0.f(layoutInflater, 2131558611, viewGroup, false, A6());
        PortfolioApp.h0.c().M().F1().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = new ViewModelProvider(this, po4).a(hp6.class);
            pq7.b(a2, "ViewModelProvider(this, \u2026ditViewModel::class.java)");
            this.i = (hp6) a2;
            this.j = new g37<>(this, na5);
            hp6 hp6 = this.i;
            if (hp6 != null) {
                a7(hp6.J());
                c7();
                d7();
                b7();
                Z6();
                LinearLayout linearLayout = na5.G;
                pq7.b(linearLayout, "binding.llGoalsValue");
                linearLayout.setVisibility(4);
                LinearLayout linearLayout2 = na5.H;
                pq7.b(linearLayout2, "binding.llSleepGoalValue");
                linearLayout2.setVisibility(4);
                E6("set_goal_view");
                pq7.b(na5, "binding");
                return na5.n();
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d(u, "onResume");
        super.onResume();
        vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        FLogger.INSTANCE.getLocal().d(u, "onStop");
        super.onStop();
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
