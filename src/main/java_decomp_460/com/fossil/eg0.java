package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.jg0;
import com.fossil.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg0 implements hm0 {
    @DexIgnore
    public View A;
    @DexIgnore
    public sn0 B;
    @DexIgnore
    public MenuItem.OnActionExpandListener C;
    @DexIgnore
    public boolean D; // = false;
    @DexIgnore
    public ContextMenu.ContextMenuInfo E;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f934a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public CharSequence e;
    @DexIgnore
    public CharSequence f;
    @DexIgnore
    public Intent g;
    @DexIgnore
    public char h;
    @DexIgnore
    public int i; // = 4096;
    @DexIgnore
    public char j;
    @DexIgnore
    public int k; // = 4096;
    @DexIgnore
    public Drawable l;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public cg0 n;
    @DexIgnore
    public ng0 o;
    @DexIgnore
    public Runnable p;
    @DexIgnore
    public MenuItem.OnMenuItemClickListener q;
    @DexIgnore
    public CharSequence r;
    @DexIgnore
    public CharSequence s;
    @DexIgnore
    public ColorStateList t; // = null;
    @DexIgnore
    public PorterDuff.Mode u; // = null;
    @DexIgnore
    public boolean v; // = false;
    @DexIgnore
    public boolean w; // = false;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public int y; // = 16;
    @DexIgnore
    public int z; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements sn0.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.sn0.b
        public void onActionProviderVisibilityChanged(boolean z) {
            eg0 eg0 = eg0.this;
            eg0.n.L(eg0);
        }
    }

    @DexIgnore
    public eg0(cg0 cg0, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.n = cg0;
        this.f934a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.z = i6;
    }

    @DexIgnore
    public static void d(StringBuilder sb, int i2, int i3, String str) {
        if ((i2 & i3) == i3) {
            sb.append(str);
        }
    }

    @DexIgnore
    public boolean A() {
        return this.n.J() && g() != 0;
    }

    @DexIgnore
    public boolean B() {
        return (this.z & 4) == 4;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 a(sn0 sn0) {
        sn0 sn02 = this.B;
        if (sn02 != null) {
            sn02.reset();
        }
        this.A = null;
        this.B = sn0;
        this.n.M(true);
        sn0 sn03 = this.B;
        if (sn03 != null) {
            sn03.setVisibilityListener(new a());
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public sn0 b() {
        return this.B;
    }

    @DexIgnore
    public void c() {
        this.n.K(this);
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean collapseActionView() {
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null) {
            return true;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
            return this.n.f(this);
        }
        return false;
    }

    @DexIgnore
    public final Drawable e(Drawable drawable) {
        if (drawable != null && this.x && (this.v || this.w)) {
            drawable = am0.r(drawable).mutate();
            if (this.v) {
                am0.o(drawable, this.t);
            }
            if (this.w) {
                am0.p(drawable, this.u);
            }
            this.x = false;
        }
        return drawable;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean expandActionView() {
        if (!j()) {
            return false;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionExpand(this)) {
            return this.n.m(this);
        }
        return false;
    }

    @DexIgnore
    public int f() {
        return this.d;
    }

    @DexIgnore
    public char g() {
        return this.n.I() ? this.j : this.h;
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public View getActionView() {
        View view = this.A;
        if (view != null) {
            return view;
        }
        sn0 sn0 = this.B;
        if (sn0 == null) {
            return null;
        }
        View onCreateActionView = sn0.onCreateActionView(this);
        this.A = onCreateActionView;
        return onCreateActionView;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public int getAlphabeticModifiers() {
        return this.k;
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public CharSequence getContentDescription() {
        return this.r;
    }

    @DexIgnore
    public int getGroupId() {
        return this.b;
    }

    @DexIgnore
    public Drawable getIcon() {
        Drawable drawable = this.l;
        if (drawable != null) {
            return e(drawable);
        }
        if (this.m == 0) {
            return null;
        }
        Drawable d2 = gf0.d(this.n.w(), this.m);
        this.m = 0;
        this.l = d2;
        return e(d2);
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public ColorStateList getIconTintList() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public PorterDuff.Mode getIconTintMode() {
        return this.u;
    }

    @DexIgnore
    public Intent getIntent() {
        return this.g;
    }

    @DexIgnore
    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f934a;
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public int getNumericModifiers() {
        return this.i;
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.h;
    }

    @DexIgnore
    public int getOrder() {
        return this.c;
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return this.o;
    }

    @DexIgnore
    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.e;
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f;
        if (charSequence == null) {
            charSequence = this.e;
        }
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public CharSequence getTooltipText() {
        return this.s;
    }

    @DexIgnore
    public String h() {
        char g2 = g();
        if (g2 == 0) {
            return "";
        }
        Resources resources = this.n.w().getResources();
        StringBuilder sb = new StringBuilder();
        if (ViewConfiguration.get(this.n.w()).hasPermanentMenuKey()) {
            sb.append(resources.getString(se0.abc_prepend_shortcut_label));
        }
        int i2 = this.n.I() ? this.k : this.i;
        d(sb, i2, 65536, resources.getString(se0.abc_menu_meta_shortcut_label));
        d(sb, i2, 4096, resources.getString(se0.abc_menu_ctrl_shortcut_label));
        d(sb, i2, 2, resources.getString(se0.abc_menu_alt_shortcut_label));
        d(sb, i2, 1, resources.getString(se0.abc_menu_shift_shortcut_label));
        d(sb, i2, 4, resources.getString(se0.abc_menu_sym_shortcut_label));
        d(sb, i2, 8, resources.getString(se0.abc_menu_function_shortcut_label));
        if (g2 == '\b') {
            sb.append(resources.getString(se0.abc_menu_delete_shortcut_label));
        } else if (g2 == '\n') {
            sb.append(resources.getString(se0.abc_menu_enter_shortcut_label));
        } else if (g2 != ' ') {
            sb.append(g2);
        } else {
            sb.append(resources.getString(se0.abc_menu_space_shortcut_label));
        }
        return sb.toString();
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return this.o != null;
    }

    @DexIgnore
    public CharSequence i(jg0.a aVar) {
        return (aVar == null || !aVar.e()) ? getTitle() : getTitleCondensed();
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean isActionViewExpanded() {
        return this.D;
    }

    @DexIgnore
    public boolean isCheckable() {
        return (this.y & 1) == 1;
    }

    @DexIgnore
    public boolean isChecked() {
        return (this.y & 2) == 2;
    }

    @DexIgnore
    public boolean isEnabled() {
        return (this.y & 16) != 0;
    }

    @DexIgnore
    public boolean isVisible() {
        sn0 sn0 = this.B;
        return (sn0 == null || !sn0.overridesItemVisibility()) ? (this.y & 8) == 0 : (this.y & 8) == 0 && this.B.isVisible();
    }

    @DexIgnore
    public boolean j() {
        sn0 sn0;
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null && (sn0 = this.B) != null) {
            this.A = sn0.onCreateActionView(this);
        }
        return this.A != null;
    }

    @DexIgnore
    public boolean k() {
        MenuItem.OnMenuItemClickListener onMenuItemClickListener = this.q;
        if (onMenuItemClickListener != null && onMenuItemClickListener.onMenuItemClick(this)) {
            return true;
        }
        cg0 cg0 = this.n;
        if (cg0.h(cg0, this)) {
            return true;
        }
        Runnable runnable = this.p;
        if (runnable != null) {
            runnable.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.n.w().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        sn0 sn0 = this.B;
        return sn0 != null && sn0.onPerformDefaultAction();
    }

    @DexIgnore
    public boolean l() {
        return (this.y & 32) == 32;
    }

    @DexIgnore
    public boolean m() {
        return (this.y & 4) != 0;
    }

    @DexIgnore
    public boolean n() {
        return (this.z & 1) == 1;
    }

    @DexIgnore
    public boolean o() {
        return (this.z & 2) == 2;
    }

    @DexIgnore
    public hm0 p(int i2) {
        Context w2 = this.n.w();
        q(LayoutInflater.from(w2).inflate(i2, (ViewGroup) new LinearLayout(w2), false));
        return this;
    }

    @DexIgnore
    public hm0 q(View view) {
        int i2;
        this.A = view;
        this.B = null;
        if (view != null && view.getId() == -1 && (i2 = this.f934a) > 0) {
            view.setId(i2);
        }
        this.n.K(this);
        return this;
    }

    @DexIgnore
    public void r(boolean z2) {
        this.D = z2;
        this.n.M(false);
    }

    @DexIgnore
    public void s(boolean z2) {
        int i2 = this.y;
        int i3 = (z2 ? 2 : 0) | (i2 & -3);
        this.y = i3;
        if (i2 != i3) {
            this.n.M(false);
        }
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    @DexIgnore
    @Override // com.fossil.hm0, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(int i2) {
        p(i2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(View view) {
        q(view);
        return this;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.j != c2) {
            this.j = Character.toLowerCase(c2);
            this.n.M(false);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        if (!(this.j == c2 && this.k == i2)) {
            this.j = Character.toLowerCase(c2);
            this.k = KeyEvent.normalizeMetaState(i2);
            this.n.M(false);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z2) {
        int i2 = this.y;
        int i3 = (i2 & -2) | (z2 ? 1 : 0);
        this.y = i3;
        if (i2 != i3) {
            this.n.M(false);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z2) {
        if ((this.y & 4) != 0) {
            this.n.X(this);
        } else {
            s(z2);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 setContentDescription(CharSequence charSequence) {
        this.r = charSequence;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.y |= 16;
        } else {
            this.y &= -17;
        }
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(int i2) {
        this.l = null;
        this.m = i2;
        this.x = true;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.m = 0;
        this.l = drawable;
        this.x = true;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.t = colorStateList;
        this.v = true;
        this.x = true;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.u = mode;
        this.w = true;
        this.x = true;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        if (this.h != c2) {
            this.h = (char) c2;
            this.n.M(false);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setNumericShortcut(char c2, int i2) {
        if (!(this.h == c2 && this.i == i2)) {
            this.h = (char) c2;
            this.i = KeyEvent.normalizeMetaState(i2);
            this.n.M(false);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.C = onActionExpandListener;
        return this;
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.q = onMenuItemClickListener;
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        this.h = (char) c2;
        this.j = Character.toLowerCase(c3);
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.h = (char) c2;
        this.i = KeyEvent.normalizeMetaState(i2);
        this.j = Character.toLowerCase(c3);
        this.k = KeyEvent.normalizeMetaState(i3);
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public void setShowAsAction(int i2) {
        int i3 = i2 & 3;
        if (i3 == 0 || i3 == 1 || i3 == 2) {
            this.z = i2;
            this.n.K(this);
            return;
        }
        throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public /* bridge */ /* synthetic */ MenuItem setShowAsActionFlags(int i2) {
        w(i2);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(int i2) {
        setTitle(this.n.w().getString(i2));
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.e = charSequence;
        this.n.M(false);
        ng0 ng0 = this.o;
        if (ng0 != null) {
            ng0.setHeaderTitle(charSequence);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f = charSequence;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 setTooltipText(CharSequence charSequence) {
        this.s = charSequence;
        this.n.M(false);
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z2) {
        if (y(z2)) {
            this.n.L(this);
        }
        return this;
    }

    @DexIgnore
    public void t(boolean z2) {
        this.y = (z2 ? 4 : 0) | (this.y & -5);
    }

    @DexIgnore
    public String toString() {
        CharSequence charSequence = this.e;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    @DexIgnore
    public void u(boolean z2) {
        if (z2) {
            this.y |= 32;
        } else {
            this.y &= -33;
        }
    }

    @DexIgnore
    public void v(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.E = contextMenuInfo;
    }

    @DexIgnore
    public hm0 w(int i2) {
        setShowAsAction(i2);
        return this;
    }

    @DexIgnore
    public void x(ng0 ng0) {
        this.o = ng0;
        ng0.setHeaderTitle(getTitle());
    }

    @DexIgnore
    public boolean y(boolean z2) {
        int i2 = this.y;
        int i3 = (z2 ? 0 : 8) | (i2 & -9);
        this.y = i3;
        return i2 != i3;
    }

    @DexIgnore
    public boolean z() {
        return this.n.C();
    }
}
