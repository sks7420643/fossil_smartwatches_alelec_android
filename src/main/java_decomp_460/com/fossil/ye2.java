package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ye2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static ye2 f4306a; // = new ye2();
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static ye2 a() {
        return f4306a;
    }

    @DexIgnore
    public static void d(Context context, WakeLockEvent wakeLockEvent) {
        try {
            context.startService(new Intent().setComponent(we2.f3925a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", wakeLockEvent));
        } catch (Exception e) {
            Log.wtf("WakeLockTracker", e);
        }
    }

    @DexIgnore
    public static boolean e() {
        if (b == null) {
            b = Boolean.FALSE;
        }
        return b.booleanValue();
    }

    @DexIgnore
    public void b(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list) {
        c(context, str, i, str2, str3, str4, i2, list, 0);
    }

    @DexIgnore
    public void c(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list, long j) {
        if (e()) {
            if (TextUtils.isEmpty(str)) {
                String valueOf = String.valueOf(str);
                Log.e("WakeLockTracker", valueOf.length() != 0 ? "missing wakeLock key. ".concat(valueOf) : new String("missing wakeLock key. "));
            } else if (7 == i || 8 == i || 10 == i || 11 == i) {
                d(context, new WakeLockEvent(System.currentTimeMillis(), i, str2, i2, xe2.b(list), str, SystemClock.elapsedRealtime(), vf2.a(context), str3, xe2.c(context.getPackageName()), vf2.b(context), j, str4, false));
            }
        }
    }
}
