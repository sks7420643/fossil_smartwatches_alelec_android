package com.fossil;

import com.google.firebase.crashlytics.CrashlyticsRegistrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class u74 implements d74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CrashlyticsRegistrar f3521a;

    @DexIgnore
    public u74(CrashlyticsRegistrar crashlyticsRegistrar) {
        this.f3521a = crashlyticsRegistrar;
    }

    @DexIgnore
    public static d74 b(CrashlyticsRegistrar crashlyticsRegistrar) {
        return new u74(crashlyticsRegistrar);
    }

    @DexIgnore
    @Override // com.fossil.d74
    public Object a(b74 b74) {
        return this.f3521a.b(b74);
    }
}
