package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y48 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static x48 f4242a;
    @DexIgnore
    public static long b;
    @DexIgnore
    public static /* final */ y48 c; // = new y48();

    @DexIgnore
    public final void a(x48 x48) {
        boolean z = false;
        pq7.c(x48, "segment");
        if (x48.f == null && x48.g == null) {
            z = true;
        }
        if (!z) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!x48.d) {
            synchronized (this) {
                long j = (long) 8192;
                if (b + j <= 65536) {
                    b += j;
                    x48.f = f4242a;
                    x48.c = 0;
                    x48.b = 0;
                    f4242a = x48;
                    tl7 tl7 = tl7.f3441a;
                }
            }
        }
    }

    @DexIgnore
    public final x48 b() {
        synchronized (this) {
            x48 x48 = f4242a;
            if (x48 == null) {
                return new x48();
            }
            f4242a = x48.f;
            x48.f = null;
            b -= (long) 8192;
            return x48;
        }
    }
}
