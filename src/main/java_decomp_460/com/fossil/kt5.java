package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt5 implements Factory<jt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f2078a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<UserSettingDao> c;

    @DexIgnore
    public kt5(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        this.f2078a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static kt5 a(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        return new kt5(provider, provider2, provider3);
    }

    @DexIgnore
    public static jt5 c(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        return new jt5(portfolioApp, userRepository, userSettingDao);
    }

    @DexIgnore
    /* renamed from: b */
    public jt5 get() {
        return c(this.f2078a.get(), this.b.get(), this.c.get());
    }
}
