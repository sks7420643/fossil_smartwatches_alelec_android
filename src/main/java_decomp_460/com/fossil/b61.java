package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.fossil.e61;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b61 implements e61<Drawable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f397a;
    @DexIgnore
    public /* final */ v51 b;

    @DexIgnore
    public b61(Context context, v51 v51) {
        pq7.c(context, "context");
        pq7.c(v51, "drawableDecoder");
        this.f397a = context;
        this.b = v51;
    }

    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, Drawable drawable, f81 f81, x51 x51, qn7<? super d61> qn7) {
        boolean o = w81.o(drawable);
        if (o) {
            Bitmap a2 = this.b.a(drawable, f81, x51.d());
            Resources resources = this.f397a.getResources();
            pq7.b(resources, "context.resources");
            drawable = new BitmapDrawable(resources, a2);
        }
        return new c61(drawable, o, q51.MEMORY);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Drawable drawable) {
        pq7.c(drawable, "data");
        return e61.a.a(this, drawable);
    }

    @DexIgnore
    /* renamed from: f */
    public String b(Drawable drawable) {
        pq7.c(drawable, "data");
        return null;
    }
}
