package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji2 implements Parcelable.Creator<DataPoint> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        uh2 uh2 = null;
        ai2[] ai2Arr = null;
        uh2 uh22 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    uh22 = (uh2) ad2.e(parcel, t, uh2.CREATOR);
                    break;
                case 2:
                default:
                    ad2.B(parcel, t);
                    break;
                case 3:
                    j4 = ad2.y(parcel, t);
                    break;
                case 4:
                    j3 = ad2.y(parcel, t);
                    break;
                case 5:
                    ai2Arr = (ai2[]) ad2.i(parcel, t, ai2.CREATOR);
                    break;
                case 6:
                    uh2 = (uh2) ad2.e(parcel, t, uh2.CREATOR);
                    break;
                case 7:
                    j2 = ad2.y(parcel, t);
                    break;
                case 8:
                    j = ad2.y(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new DataPoint(uh22, j4, j3, ai2Arr, uh2, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint[] newArray(int i) {
        return new DataPoint[i];
    }
}
