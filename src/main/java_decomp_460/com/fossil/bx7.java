package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx7 {
    @DexIgnore
    public static final uu7 a(xw7 xw7) {
        return dx7.a(xw7);
    }

    @DexIgnore
    public static final void c(tn7 tn7, CancellationException cancellationException) {
        dx7.c(tn7, cancellationException);
    }

    @DexIgnore
    public static final void e(ku7<?> ku7, Future<?> future) {
        cx7.a(ku7, future);
    }

    @DexIgnore
    public static final dw7 f(xw7 xw7, dw7 dw7) {
        return dx7.e(xw7, dw7);
    }

    @DexIgnore
    public static final void g(tn7 tn7) {
        dx7.f(tn7);
    }

    @DexIgnore
    public static final void h(xw7 xw7) {
        dx7.g(xw7);
    }
}
