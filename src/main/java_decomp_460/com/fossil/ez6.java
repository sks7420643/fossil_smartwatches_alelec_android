package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.jn5;
import com.fossil.lw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez6 extends pv5 implements dz6 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public boolean g;
    @DexIgnore
    public g37<da5> h;
    @DexIgnore
    public cz6 i;
    @DexIgnore
    public lw5 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ez6.l;
        }

        @DexIgnore
        public final ez6 b() {
            Bundle bundle = new Bundle();
            ez6 ez6 = new ez6();
            ez6.setArguments(bundle);
            return ez6;
        }

        @DexIgnore
        public final ez6 c(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("PMS_SKIP_ABLE", z);
            ez6 ez6 = new ez6();
            ez6.setArguments(bundle);
            return ez6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ez6 b;

        @DexIgnore
        public b(ez6 ez6) {
            this.b = ez6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ez6 b;

        @DexIgnore
        public c(ez6 ez6) {
            this.b = ez6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements lw5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ez6 f1012a;

        @DexIgnore
        public d(ez6 ez6) {
            this.f1012a = ez6;
        }

        @DexIgnore
        @Override // com.fossil.lw5.a
        public void a(PermissionData permissionData) {
            pq7.c(permissionData, "permissionData");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ez6.m.a();
            local.d(a2, "requestPermission " + permissionData);
            String type = permissionData.getType();
            int hashCode = type.hashCode();
            if (hashCode != -1168251815) {
                if (hashCode != -381825158 || !type.equals("PERMISSION_REQUEST_TYPE")) {
                    return;
                }
                if (!permissionData.getAndroidPermissionSet().isEmpty()) {
                    ez6 ez6 = this.f1012a;
                    Object[] array = permissionData.getAndroidPermissionSet().toArray(new String[0]);
                    if (array != null) {
                        ez6.requestPermissions((String[]) array, 111);
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
                jn5.c settingPermissionId = permissionData.getSettingPermissionId();
                if (settingPermissionId != null) {
                    int i = fz6.f1241a[settingPermissionId.ordinal()];
                    if (i == 1) {
                        this.f1012a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
                    } else if (i == 2) {
                        this.f1012a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                    } else if (i == 3) {
                        this.f1012a.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                    }
                }
            } else if (type.equals("PERMISSION_SETTING_TYPE")) {
                this.f1012a.requireActivity().finish();
            }
        }
    }

    /*
    static {
        String simpleName = ez6.class.getSimpleName();
        pq7.b(simpleName, "PermissionFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        M6(2);
        return true;
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(cz6 cz6) {
        pq7.c(cz6, "presenter");
        this.i = cz6;
    }

    @DexIgnore
    public void M6(int i2) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(i2);
            activity.finish();
        }
    }

    @DexIgnore
    public final void N6() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        StringBuilder sb = new StringBuilder();
        sb.append("package:");
        FragmentActivity activity = getActivity();
        sb.append(activity != null ? activity.getPackageName() : null);
        intent.setData(Uri.parse(sb.toString()));
        startActivity(intent);
    }

    @DexIgnore
    @Override // com.fossil.dz6
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.dz6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d5(java.util.List<com.portfolio.platform.data.model.PermissionData> r8) {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ez6.d5(java.util.List):void");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        g37<da5> g37 = new g37<>(this, (da5) aq0.f(layoutInflater, 2131558606, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            da5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        cz6 cz6 = this.i;
        if (cz6 != null) {
            cz6.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rk0.b, com.fossil.pv5, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        pq7.c(strArr, "permissions");
        pq7.c(iArr, "grantResults");
        super.onRequestPermissionsResult(i2, strArr, iArr);
        Integer[] w = dm7.w(iArr);
        int length = w.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            if (w[i3].intValue() == -1) {
                String str = strArr[i4];
                boolean shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale(str);
                cz6 cz6 = this.i;
                if (cz6 != null) {
                    boolean n = cz6.n(str);
                    FLogger.INSTANCE.getLocal().d(l, "permission " + str + " is denied, isDenyOnly " + shouldShowRequestPermissionRationale + " isFirstTime " + n + ' ');
                    if (shouldShowRequestPermissionRationale) {
                        return;
                    }
                    if (!n) {
                        N6();
                        return;
                    }
                    cz6 cz62 = this.i;
                    if (cz62 != null) {
                        cz62.o(str, false);
                        return;
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else {
                i3++;
                i4++;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(l, "onResume");
        cz6 cz6 = this.i;
        if (cz6 != null) {
            cz6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ImageView imageView;
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            boolean z = arguments.getBoolean("PMS_SKIP_ABLE", false);
            this.g = z;
            if (z) {
                g37<da5> g37 = this.h;
                if (g37 != null) {
                    da5 a2 = g37.a();
                    if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                        flexibleButton2.setVisibility(0);
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            }
        }
        g37<da5> g372 = this.h;
        if (g372 != null) {
            da5 a3 = g372.a();
            if (!(a3 == null || (flexibleButton = a3.q) == null)) {
                flexibleButton.setOnClickListener(new b(this));
            }
            g37<da5> g373 = this.h;
            if (g373 != null) {
                da5 a4 = g373.a();
                if (a4 != null && (imageView = a4.s) != null) {
                    imageView.setOnClickListener(new c(this));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
