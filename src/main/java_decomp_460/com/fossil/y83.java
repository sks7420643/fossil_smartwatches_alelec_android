package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y83 implements xw2<x83> {
    @DexIgnore
    public static y83 c; // = new y83();
    @DexIgnore
    public /* final */ xw2<x83> b;

    @DexIgnore
    public y83() {
        this(ww2.b(new a93()));
    }

    @DexIgnore
    public y83(xw2<x83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((x83) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((x83) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ x83 zza() {
        return this.b.zza();
    }
}
