package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cw0 extends bw0 {
    @DexIgnore
    public cw0(long j, RenderScript renderScript) {
        super(j, renderScript);
        if (j == 0) {
            throw new aw0("Loading of ScriptIntrinsic failed.");
        }
    }
}
