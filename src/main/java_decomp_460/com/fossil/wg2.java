package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg2 implements qg2.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ Activity f3933a;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ qg2 d;

    @DexIgnore
    public wg2(qg2 qg2, Activity activity, Bundle bundle, Bundle bundle2) {
        this.d = qg2;
        this.f3933a = activity;
        this.b = bundle;
        this.c = bundle2;
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final void a(sg2 sg2) {
        this.d.f2976a.p(this.f3933a, this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final int getState() {
        return 0;
    }
}
