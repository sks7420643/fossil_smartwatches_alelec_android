package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nw5;
import com.fossil.t47;
import com.fossil.x37;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx6 extends ey6 implements gq4, nw5.a, t47.g {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<x95> h;
    @DexIgnore
    public hy6 i;
    @DexIgnore
    public nw5 j;
    @DexIgnore
    public ew5 k;
    @DexIgnore
    public List<cl7<ShineDevice, String>> l; // = new ArrayList();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final qx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            qx6 qx6 = new qx6();
            qx6.setArguments(bundle);
            return qx6;
        }

        @DexIgnore
        public final String b() {
            String simpleName = qx6.class.getSimpleName();
            pq7.b(simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ x95 b;
        @DexIgnore
        public /* final */ /* synthetic */ qx6 c;

        @DexIgnore
        public b(x95 x95, qx6 qx6, dr7 dr7) {
            this.b = x95;
            this.c = qx6;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getActivity(), 2130772008);
            this.b.s.startAnimation(loadAnimation);
            this.b.w.startAnimation(loadAnimation);
            this.b.q.startAnimation(loadAnimation);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qx6 b;

        @DexIgnore
        public c(qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qx6.L6(this.b).r(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qx6 b;

        @DexIgnore
        public d(qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qx6.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qx6 b;

        @DexIgnore
        public e(qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (qx6.M6(this.b).w() < this.b.l.size()) {
                qx6 qx6 = this.b;
                qx6.O6((ShineDevice) ((cl7) qx6.l.get(qx6.M6(this.b).w())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ hy6 L6(qx6 qx6) {
        hy6 hy6 = qx6.i;
        if (hy6 != null) {
            return hy6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ew5 M6(qx6 qx6) {
        ew5 ew5 = qx6.k;
        if (ew5 != null) {
            return ew5;
        }
        pq7.n("mSnapHelper");
        throw null;
    }

    @DexIgnore
    public final void O6(ShineDevice shineDevice) {
        String serial = shineDevice.getSerial();
        pq7.b(serial, "device.serial");
        if (serial.length() > 0) {
            hy6 hy6 = this.i;
            if (hy6 != null) {
                hy6.A(shineDevice);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.v(childFragmentManager);
        }
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(hy6 hy6) {
        pq7.c(hy6, "presenter");
        this.i = hy6;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == -2084521848 && str.equals("DOWNLOAD") && i2 == 2131363373) {
            hy6 hy6 = this.i;
            if (hy6 != null) {
                List<cl7<ShineDevice, String>> list = this.l;
                ew5 ew5 = this.k;
                if (ew5 != null) {
                    hy6.u(list.get(ew5.w()).getFirst());
                } else {
                    pq7.n("mSnapHelper");
                    throw null;
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        dr7 dr7 = new dr7();
        dr7.element = null;
        if (z) {
            g37<x95> g37 = this.h;
            if (g37 != null) {
                x95 a2 = g37.a();
                if (!(a2 == null || getActivity() == null)) {
                    T t = (T) AnimationUtils.loadAnimation(getActivity(), 2130771999);
                    dr7.element = t;
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAnimationListener(new b(a2, this, dr7));
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        return dr7.element;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        x95 x95 = (x95) aq0.f(layoutInflater, 2131558603, viewGroup, false, A6());
        this.h = new g37<>(this, x95);
        pq7.b(x95, "binding");
        return x95.n();
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        FlexibleButton flexibleButton;
        FlexibleTextView flexibleTextView;
        RecyclerView recyclerView;
        FlexibleTextView flexibleTextView2;
        RTLImageView rTLImageView;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<x95> g37 = this.h;
        if (g37 != null) {
            x95 a2 = g37.a();
            if (!(a2 == null || (rTLImageView = a2.t) == null)) {
                rTLImageView.setOnClickListener(new c(this));
            }
            g37<x95> g372 = this.h;
            if (g372 != null) {
                x95 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView2 = a3.s) == null)) {
                    hr7 hr7 = hr7.f1520a;
                    Locale locale = Locale.US;
                    pq7.b(locale, "Locale.US");
                    String c2 = um5.c(getContext(), 2131886943);
                    pq7.b(c2, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format = String.format(locale, c2, Arrays.copyOf(new Object[]{Integer.valueOf(this.l.size())}, 1));
                    pq7.b(format, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView2.setText(format);
                }
                wa1 v = oa1.v(this);
                pq7.b(v, "Glide.with(this)");
                nw5 nw5 = new nw5(v, this);
                this.j = nw5;
                nw5.k(this.l);
                g37<x95> g373 = this.h;
                if (g373 != null) {
                    x95 a4 = g373.a();
                    if (!(a4 == null || (recyclerView = a4.w) == null)) {
                        pq7.b(recyclerView, "it");
                        recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.h0.c().getApplicationContext(), 0, false));
                        recyclerView.addItemDecoration(new h67());
                        recyclerView.setAdapter(this.j);
                        ew5 ew5 = new ew5(null, 1, null);
                        this.k = ew5;
                        ew5.b(recyclerView);
                    }
                    g37<x95> g374 = this.h;
                    if (g374 != null) {
                        x95 a5 = g374.a();
                        if (!(a5 == null || (flexibleTextView = a5.r) == null)) {
                            flexibleTextView.setOnClickListener(new d(this));
                        }
                        g37<x95> g375 = this.h;
                        if (g375 != null) {
                            x95 a6 = g375.a();
                            if (!(a6 == null || (flexibleButton = a6.q) == null)) {
                                flexibleButton.setOnClickListener(new e(this));
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                g37<x95> g376 = this.h;
                                if (g376 != null) {
                                    x95 a7 = g376.a();
                                    if (a7 != null && (dashBar = a7.u) != null) {
                                        x37.a aVar = x37.f4036a;
                                        pq7.b(dashBar, "this");
                                        aVar.b(dashBar, z, 500);
                                        return;
                                    }
                                    return;
                                }
                                pq7.n("mBinding");
                                throw null;
                            }
                            return;
                        }
                        pq7.n("mBinding");
                        throw null;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nw5.a
    public void p1(View view, nw5.b bVar, int i2) {
        RecyclerView recyclerView;
        pq7.c(view, "view");
        pq7.c(bVar, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b2 = s.b();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i2);
        sb.append(", mSnappedPos=");
        ew5 ew5 = this.k;
        if (ew5 != null) {
            sb.append(ew5.w());
            local.d(b2, sb.toString());
            ew5 ew52 = this.k;
            if (ew52 == null) {
                pq7.n("mSnapHelper");
                throw null;
            } else if (ew52.w() != i2) {
                g37<x95> g37 = this.h;
                if (g37 != null) {
                    x95 a2 = g37.a();
                    if (a2 != null && (recyclerView = a2.w) != null) {
                        ew5 ew53 = this.k;
                        if (ew53 != null) {
                            recyclerView.smoothScrollBy((i2 - ew53.w()) * view.getWidth(), view.getHeight());
                        } else {
                            pq7.n("mSnapHelper");
                            throw null;
                        }
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                List<cl7<ShineDevice, String>> list = this.l;
                ew5 ew54 = this.k;
                if (ew54 != null) {
                    O6(list.get(ew54.w()).getFirst());
                } else {
                    pq7.n("mSnapHelper");
                    throw null;
                }
            }
        } else {
            pq7.n("mSnapHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void t2(List<cl7<ShineDevice, String>> list) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        pq7.c(list, "shineDeviceList");
        this.l = list;
        nw5 nw5 = this.j;
        if (nw5 != null) {
            nw5.k(list);
            int size = this.l.size();
            if (size <= 1) {
                g37<x95> g37 = this.h;
                if (g37 != null) {
                    x95 a2 = g37.a();
                    if (a2 != null && (flexibleTextView2 = a2.s) != null) {
                        hr7 hr7 = hr7.f1520a;
                        Locale locale = Locale.US;
                        pq7.b(locale, "Locale.US");
                        String c2 = um5.c(getActivity(), 2131886943);
                        pq7.b(c2, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        String format = String.format(locale, c2, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                        pq7.b(format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView2.setText(format);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            g37<x95> g372 = this.h;
            if (g372 != null) {
                x95 a3 = g372.a();
                if (a3 != null && (flexibleTextView = a3.s) != null) {
                    hr7 hr72 = hr7.f1520a;
                    Locale locale2 = Locale.US;
                    pq7.b(locale2, "Locale.US");
                    String c3 = um5.c(getActivity(), 2131886943);
                    pq7.b(c3, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format2 = String.format(locale2, c3, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                    pq7.b(format2, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView.setText(format2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
