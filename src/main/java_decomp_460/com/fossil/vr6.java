package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr6 implements Factory<ur6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f3817a;

    @DexIgnore
    public vr6(Provider<ThemeRepository> provider) {
        this.f3817a = provider;
    }

    @DexIgnore
    public static vr6 a(Provider<ThemeRepository> provider) {
        return new vr6(provider);
    }

    @DexIgnore
    public static ur6 c(ThemeRepository themeRepository) {
        return new ur6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ur6 get() {
        return c(this.f3817a.get());
    }
}
