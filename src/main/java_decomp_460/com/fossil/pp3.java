package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ vg3 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ u93 d;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 e;

    @DexIgnore
    public pp3(fp3 fp3, vg3 vg3, String str, u93 u93) {
        this.e = fp3;
        this.b = vg3;
        this.c = str;
        this.d = u93;
    }

    @DexIgnore
    public final void run() {
        try {
            cl3 cl3 = this.e.d;
            if (cl3 == null) {
                this.e.d().F().a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            byte[] w2 = cl3.w2(this.b, this.c);
            this.e.e0();
            this.e.k().T(this.d, w2);
        } catch (RemoteException e2) {
            this.e.d().F().b("Failed to send event to the service to bundle", e2);
        } finally {
            this.e.k().T(this.d, null);
        }
    }
}
