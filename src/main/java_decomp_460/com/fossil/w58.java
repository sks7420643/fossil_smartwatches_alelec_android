package com.fossil;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w58 {
    @DexIgnore
    public static Class a(String str) throws t58 {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unable to find the class: ");
            stringBuffer.append(str);
            throw new t58(stringBuffer.toString());
        }
    }

    @DexIgnore
    public static Date b(String str) throws t58 {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @DexIgnore
    public static File c(String str) throws t58 {
        return new File(str);
    }

    @DexIgnore
    public static File[] d(String str) throws t58 {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @DexIgnore
    public static Number e(String str) throws t58 {
        try {
            return str.indexOf(46) != -1 ? Double.valueOf(str) : Long.valueOf(str);
        } catch (NumberFormatException e) {
            throw new t58(e.getMessage());
        }
    }

    @DexIgnore
    public static Object f(String str) throws t58 {
        try {
            try {
                return Class.forName(str).newInstance();
            } catch (Exception e) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(e.getClass().getName());
                stringBuffer.append("; Unable to create an instance of: ");
                stringBuffer.append(str);
                throw new t58(stringBuffer.toString());
            }
        } catch (ClassNotFoundException e2) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Unable to find the class: ");
            stringBuffer2.append(str);
            throw new t58(stringBuffer2.toString());
        }
    }

    @DexIgnore
    public static URL g(String str) throws t58 {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unable to parse the URL: ");
            stringBuffer.append(str);
            throw new t58(stringBuffer.toString());
        }
    }

    @DexIgnore
    public static Object h(String str, Class cls) throws t58 {
        if (v58.f3721a == cls) {
            return str;
        }
        if (v58.b == cls) {
            return f(str);
        }
        if (v58.c == cls) {
            return e(str);
        }
        if (v58.d == cls) {
            b(str);
            throw null;
        } else if (v58.e == cls) {
            return a(str);
        } else {
            if (v58.g == cls) {
                return c(str);
            }
            if (v58.f == cls) {
                return c(str);
            }
            if (v58.h == cls) {
                d(str);
                throw null;
            } else if (v58.i == cls) {
                return g(str);
            } else {
                return null;
            }
        }
    }

    @DexIgnore
    public static Object i(String str, Object obj) throws t58 {
        return h(str, (Class) obj);
    }
}
