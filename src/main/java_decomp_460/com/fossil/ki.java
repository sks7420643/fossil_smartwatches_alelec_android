package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ki extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ il b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ki(il ilVar) {
        super(1);
        this.b = ilVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(lp lpVar) {
        T t;
        ac0[] a0 = ((oj) lpVar).x();
        for (ac0 ac0 : a0) {
            if (ac0.b == jw1.WATCH_APP) {
                Iterator<T> it = this.b.E.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.getBundleId(), ac0.a())) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 == null) {
                    this.b.D.add(ac0);
                } else if (t2.getPackageCrc() == ac0.b()) {
                    this.b.E.remove(t2);
                    this.b.C.add(t2);
                }
            }
        }
        il ilVar = this.b;
        ilVar.G = 0.9f / ((float) ilVar.E.size());
        il.I(this.b);
        return tl7.f3441a;
    }
}
