package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String g;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public NotificationsRepository f3632a;
    @DexIgnore
    public on5 b;
    @DexIgnore
    public mj5 c;
    @DexIgnore
    public v36 d;
    @DexIgnore
    public d26 e;
    @DexIgnore
    public NotificationSettingsDatabase f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver$onReceive$1", f = "AppPackageInstallReceiver.kt", l = {67}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ up5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(up5 up5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = up5;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$packageName, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FLogger.INSTANCE.getLocal().d(up5.g, "new app installed, start full sync");
                AppFilter appFilter = new AppFilter();
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.ordinal());
                appFilter.setType(this.$packageName);
                this.this$0.b().saveAppFilter(appFilter);
                up5 up5 = this.this$0;
                this.L$0 = iv7;
                this.L$1 = appFilter;
                this.label = 1;
                if (up5.c(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                AppFilter appFilter2 = (AppFilter) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver", f = "AppPackageInstallReceiver.kt", l = {73}, m = "setRuleNotificationFilterToDevice")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ up5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(up5 up5, qn7 qn7) {
            super(qn7);
            this.this$0 = up5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    /*
    static {
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        pq7.b(simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public up5() {
        PortfolioApp.h0.c().M().l(this);
    }

    @DexIgnore
    public final NotificationsRepository b() {
        NotificationsRepository notificationsRepository = this.f3632a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        pq7.n("mNotificationsRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c(com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r9 = this;
            r8 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 0
            boolean r0 = r10 instanceof com.fossil.up5.b
            if (r0 == 0) goto L_0x004a
            r0 = r10
            com.fossil.up5$b r0 = (com.fossil.up5.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
            r5 = r0
        L_0x0015:
            java.lang.Object r1 = r5.result
            java.lang.Object r6 = com.fossil.yn7.d()
            int r0 = r5.label
            if (r0 == 0) goto L_0x0058
            if (r0 != r8) goto L_0x0050
            java.lang.Object r0 = r5.L$0
            com.fossil.up5 r0 = (com.fossil.up5) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0029:
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            java.util.List r0 = (java.util.List) r0
            long r2 = java.lang.System.currentTimeMillis()
            r1.<init>(r0, r2)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r2 = r2.J()
            r0.s1(r1, r2)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0049:
            return r0
        L_0x004a:
            com.fossil.up5$b r5 = new com.fossil.up5$b
            r5.<init>(r9, r10)
            goto L_0x0015
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.el7.b(r1)
            com.fossil.e47 r0 = com.fossil.e47.b
            com.fossil.v36 r1 = r9.d
            if (r1 == 0) goto L_0x008b
            com.fossil.d26 r2 = r9.e
            if (r2 == 0) goto L_0x0085
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r3 = r9.f
            if (r3 == 0) goto L_0x007f
            com.fossil.on5 r4 = r9.b
            if (r4 == 0) goto L_0x0079
            r5.L$0 = r9
            r5.label = r8
            java.lang.Object r0 = r0.c(r1, r2, r3, r4, r5)
            if (r0 != r6) goto L_0x0029
            r0 = r6
            goto L_0x0049
        L_0x0079:
            java.lang.String r0 = "mSharedPreferencesManager"
            com.fossil.pq7.n(r0)
            throw r7
        L_0x007f:
            java.lang.String r0 = "mNotificationSettingsDatabase"
            com.fossil.pq7.n(r0)
            throw r7
        L_0x0085:
            java.lang.String r0 = "mGetAllContactGroup"
            com.fossil.pq7.n(r0)
            throw r7
        L_0x008b:
            java.lang.String r0 = "mGetApps"
            com.fossil.pq7.n(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.up5.c(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Uri data;
        on5 on5 = this.b;
        if (on5 != null) {
            boolean W = on5.W();
            String J = PortfolioApp.h0.c().J();
            String encodedSchemeSpecificPart = (intent == null || (data = intent.getData()) == null) ? null : data.getEncodedSchemeSpecificPart();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceive action ");
            sb.append(intent != null ? intent.getAction() : null);
            sb.append(" new app install ");
            sb.append(encodedSchemeSpecificPart);
            sb.append(", isAllAppEnable ");
            sb.append(W);
            local.d(str, sb.toString());
            if (W && nk5.o.x(J) && !TextUtils.isEmpty(encodedSchemeSpecificPart)) {
                xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, encodedSchemeSpecificPart, null), 3, null);
                return;
            }
            return;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }
}
