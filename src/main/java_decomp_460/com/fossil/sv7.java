package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sv7<T> extends au7<T> implements rv7<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "kotlinx.coroutines.DeferredCoroutine", f = "Builders.common.kt", l = {99}, m = "await$suspendImpl")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sv7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sv7 sv7, qn7 qn7) {
            super(qn7);
            this.this$0 = sv7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return sv7.A0(this.this$0, this);
        }
    }

    @DexIgnore
    public sv7(tn7 tn7, boolean z) {
        super(tn7, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object A0(com.fossil.sv7 r5, com.fossil.qn7 r6) {
        /*
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.sv7.a
            if (r0 == 0) goto L_0x0028
            r0 = r6
            com.fossil.sv7$a r0 = (com.fossil.sv7.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0028
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0036
            if (r3 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.sv7 r0 = (com.fossil.sv7) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            return r0
        L_0x0028:
            com.fossil.sv7$a r0 = new com.fossil.sv7$a
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x002e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0036:
            com.fossil.el7.b(r1)
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r5.q(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sv7.A0(com.fossil.sv7, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.rv7
    public Object l(qn7<? super T> qn7) {
        return A0(this, qn7);
    }
}
