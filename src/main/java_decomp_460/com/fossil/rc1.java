package com.fossil;

import com.fossil.be1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rc1<DataType> implements be1.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jb1<DataType> f3098a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ ob1 c;

    @DexIgnore
    public rc1(jb1<DataType> jb1, DataType datatype, ob1 ob1) {
        this.f3098a = jb1;
        this.b = datatype;
        this.c = ob1;
    }

    @DexIgnore
    @Override // com.fossil.be1.b
    public boolean a(File file) {
        return this.f3098a.a(this.b, file, this.c);
    }
}
