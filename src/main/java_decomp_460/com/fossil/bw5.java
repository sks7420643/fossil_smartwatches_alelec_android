package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<String> f517a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f518a;
        @DexIgnore
        public /* final */ /* synthetic */ bw5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bw5$a$a")
        /* renamed from: com.fossil.bw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0024a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0024a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (this.b.getAdapterPosition() != -1 && (bVar = this.b.b.b) != null) {
                    List list = this.b.b.f517a;
                    if (list != null) {
                        bVar.a((String) list.get(adapterPosition));
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bw5 bw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.b = bw5;
            view.setOnClickListener(new View$OnClickListenerC0024a(this));
            View findViewById = view.findViewById(2131362783);
            if (findViewById != null) {
                View findViewById2 = view.findViewById(2131362103);
                if (findViewById2 != null) {
                    String d = qn5.l.a().d("nonBrandSeparatorLine");
                    String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                    if (!TextUtils.isEmpty(d)) {
                        findViewById.setBackgroundColor(Color.parseColor(d));
                    }
                    if (!TextUtils.isEmpty(d2)) {
                        findViewById2.setBackgroundColor(Color.parseColor(d2));
                    }
                    View findViewById3 = view.findViewById(2131363408);
                    if (findViewById3 != null) {
                        this.f518a = (TextView) findViewById3;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.f518a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<String> list = this.f517a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        TextView a2 = aVar.a();
        List<String> list = this.f517a;
        if (list != null) {
            a2.setText(list.get(i));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558657, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void k(List<String> list) {
        pq7.c(list, "addressSearchList");
        this.f517a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(b bVar) {
        pq7.c(bVar, "listener");
        this.b = bVar;
    }
}
