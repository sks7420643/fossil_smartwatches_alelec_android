package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv7 {
    @DexIgnore
    public static final Object a(long j, qn7<? super tl7> qn7) {
        if (j <= 0) {
            return tl7.f3441a;
        }
        lu7 lu7 = new lu7(xn7.c(qn7), 1);
        b(lu7.getContext()).f(j, lu7);
        Object t = lu7.t();
        if (t != yn7.d()) {
            return t;
        }
        go7.c(qn7);
        return t;
    }

    @DexIgnore
    public static final tv7 b(tn7 tn7) {
        tn7.b bVar = tn7.get(rn7.p);
        if (!(bVar instanceof tv7)) {
            bVar = null;
        }
        tv7 tv7 = (tv7) bVar;
        return tv7 != null ? tv7 : qv7.a();
    }
}
