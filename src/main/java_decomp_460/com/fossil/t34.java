package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t34 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<E> extends AbstractList<E> implements Serializable, RandomAccess {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ E first;
        @DexIgnore
        public /* final */ E[] rest;

        @DexIgnore
        public a(E e, E[] eArr) {
            this.first = e;
            i14.l(eArr);
            this.rest = eArr;
        }

        @DexIgnore
        @Override // java.util.List, java.util.AbstractList
        public E get(int i) {
            i14.j(i, size());
            return i == 0 ? this.first : this.rest[i - 1];
        }

        @DexIgnore
        public int size() {
            return t54.d(this.rest.length, 1);
        }
    }

    @DexIgnore
    public static <E> List<E> a(E e, E[] eArr) {
        return new a(e, eArr);
    }

    @DexIgnore
    public static boolean b(List<?> list, Object obj) {
        i14.l(list);
        if (obj == list) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        List list2 = (List) obj;
        int size = list.size();
        if (size != list2.size()) {
            return false;
        }
        if (!(list instanceof RandomAccess) || !(list2 instanceof RandomAccess)) {
            return p34.g(list.iterator(), list2.iterator());
        }
        for (int i = 0; i < size; i++) {
            if (!f14.a(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static int c(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return d(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (f14.a(obj, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    public static int d(List<?> list, Object obj) {
        int i = 0;
        int size = list.size();
        if (obj == null) {
            while (i < size) {
                if (list.get(i) == null) {
                    return i;
                }
                i++;
            }
        } else {
            while (i < size) {
                if (obj.equals(list.get(i))) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static int e(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return f(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (f14.a(obj, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    public static int f(List<?> list, Object obj) {
        if (obj == null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                if (list.get(size) == null) {
                    return size;
                }
            }
        } else {
            for (int size2 = list.size() - 1; size2 >= 0; size2--) {
                if (obj.equals(list.get(size2))) {
                    return size2;
                }
            }
        }
        return -1;
    }

    @DexIgnore
    public static <E> ArrayList<E> g() {
        return new ArrayList<>();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <E> ArrayList<E> h(Iterable<? extends E> iterable) {
        i14.l(iterable);
        return iterable instanceof Collection ? new ArrayList<>(b24.a(iterable)) : i(iterable.iterator());
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <E> ArrayList<E> i(Iterator<? extends E> it) {
        ArrayList<E> g = g();
        p34.a(g, it);
        return g;
    }
}
