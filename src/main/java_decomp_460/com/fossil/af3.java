package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class af3 implements Parcelable.Creator<LatLngBounds> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LatLngBounds createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                latLng2 = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                latLng = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new LatLngBounds(latLng2, latLng);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LatLngBounds[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
