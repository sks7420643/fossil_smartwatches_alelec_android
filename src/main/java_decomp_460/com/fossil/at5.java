package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.d37;
import com.fossil.iq4;
import com.fossil.wq5;
import com.fossil.yt5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at5 extends iq4<tt5, ut5, st5> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ on5 e;
    @DexIgnore
    public /* final */ v36 f;
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ FileRepository i;
    @DexIgnore
    public /* final */ uo5 j;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase k;
    @DexIgnore
    public /* final */ d26 l;
    @DexIgnore
    public /* final */ d37 m;
    @DexIgnore
    public /* final */ yt5 n;
    @DexIgnore
    public /* final */ ck5 o;
    @DexIgnore
    public /* final */ AlarmsRepository p;
    @DexIgnore
    public /* final */ DianaAppSettingRepository q;
    @DexIgnore
    public /* final */ WorkoutSettingRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return at5.s;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ at5 f321a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(at5 at5) {
            this.f321a = at5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, this.f321a.g.J(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(at5.t.a(), "sync success, remove device now");
                    wq5.d.j(this, CommunicateMode.SYNC);
                    this.f321a.j(new ut5());
                } else if (intExtra == 2 || intExtra == 4) {
                    wq5.d.j(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = at5.t.a();
                    local.d(a2, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.f321a.i(new st5(zh5.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.f321a.i(new st5(zh5.FAIL_DUE_TO_SYNC_FAIL, null));
                            return;
                        }
                    }
                    this.f321a.i(new st5(zh5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    FLogger.INSTANCE.getLocal().d(at5.t.a(), "sync pending due to workout");
                    this.f321a.i(new st5(zh5.FAIL_DUE_TO_PENDING_WORKOUT, null));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {278, Action.Selfie.SELFIE_END_ACTION, 306}, m = "removeBuddyChallengeOnAllPresets")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ at5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(at5 at5, qn7 qn7) {
            super(qn7);
            this.this$0 = at5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$3", f = "DianaSyncUseCase.kt", l = {306}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super kz4<List<mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $presets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ at5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(at5 at5, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = at5;
            this.$presets = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$presets, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<mo5>>> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                uo5 uo5 = this.this$0.j;
                List<jo5> f = lo5.f(this.$presets, this.this$0.i);
                this.L$0 = iv7;
                this.label = 1;
                Object r = uo5.r(f, this);
                return r == d ? d : r;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$presets$1", f = "DianaSyncUseCase.kt", l = {278}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super List<? extends mo5>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ at5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(at5 at5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = at5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$serial, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super List<? extends mo5>> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                uo5 uo5 = this.this$0.j;
                String str = this.$serial;
                this.L$0 = iv7;
                this.label = 1;
                Object o = uo5.o(str, this);
                return o == d ? d : o;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {110, 112, 113, 155, 165, DateTimeConstants.HOURS_PER_WEEK, 169, 176, 180}, m = "run")
    public static final class f extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ at5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(at5 at5, qn7 qn7) {
            super(qn7);
            this.this$0 = at5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$run$isAA$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public g(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return ao7.a(SoLibraryLoader.f().c(PortfolioApp.h0.c()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {240}, m = "setRuleNotificationFilterToDevice")
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ at5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(at5 at5, qn7 qn7) {
            super(qn7);
            this.this$0 = at5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements iq4.e<d37.c, d37.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ at5 f322a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ UserProfile e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements iq4.e<yt5.d, yt5.c> {
            @DexIgnore
            /* renamed from: b */
            public void a(yt5.c cVar) {
                pq7.c(cVar, "errorValue");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(yt5.d dVar) {
                pq7.c(dVar, "responseValue");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    i iVar = this.this$0;
                    iVar.f322a.w(iVar.c, iVar.d, iVar.e, iVar.b);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public i(at5 at5, String str, int i, boolean z, UserProfile userProfile) {
            this.f322a = at5;
            this.b = str;
            this.c = i;
            this.d = z;
            this.e = userProfile;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(d37.b bVar) {
            pq7.c(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = at5.t.a();
            local.d(a2, "verify secret key fail, stop sync " + bVar.a());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.b;
            String a3 = at5.t.a();
            remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + bVar.a());
            if (bVar.a() == cl1.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
                this.f322a.n.e(new yt5.b(this.b, true), new a());
            }
            this.f322a.s(this.e.getOriginalSyncMode(), this.b, 2);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(d37.c cVar) {
            pq7.c(cVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = at5.t.a();
            local.d(a2, "secret key " + cVar.a() + ", start sync");
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b, at5.t.a(), "[Sync] Verify secret key success, start sync");
            PortfolioApp.h0.c().I1(cVar.a(), this.b);
            xw7 unused = gu7.d(this.f322a.g(), null, null, new b(this, null), 3, null);
        }
    }

    /*
    static {
        String simpleName = at5.class.getSimpleName();
        pq7.b(simpleName, "DianaSyncUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public at5(on5 on5, v36 v36, PortfolioApp portfolioApp, DeviceRepository deviceRepository, FileRepository fileRepository, uo5 uo5, NotificationSettingsDatabase notificationSettingsDatabase, d26 d26, d37 d37, yt5 yt5, ck5 ck5, AlarmsRepository alarmsRepository, DianaAppSettingRepository dianaAppSettingRepository, WorkoutSettingRepository workoutSettingRepository) {
        pq7.c(on5, "mSharedPrefs");
        pq7.c(v36, "mGetApps");
        pq7.c(portfolioApp, "mApp");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(uo5, "mDianaPresetRepository");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        pq7.c(d26, "mGetAllContactGroup");
        pq7.c(d37, "mVerifySecretKeyUseCase");
        pq7.c(yt5, "mUpdateFirmwareUseCase");
        pq7.c(ck5, "mAnalyticsHelper");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        pq7.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.e = on5;
        this.f = v36;
        this.g = portfolioApp;
        this.h = deviceRepository;
        this.i = fileRepository;
        this.j = uo5;
        this.k = notificationSettingsDatabase;
        this.l = d26;
        this.m = d37;
        this.n = yt5;
        this.o = ck5;
        this.p = alarmsRepository;
        this.q = dianaAppSettingRepository;
        this.r = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return s;
    }

    @DexIgnore
    public final void s(int i2, String str, int i3) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String h2 = h();
        local.d(h2, "broadcastSyncStatus serial=" + str + ' ' + i3);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), i2);
        intent.putExtra("SERIAL", str);
        wq5 wq5 = wq5.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        wq5.h(communicateMode, new wq5.a(communicateMode, str, intent));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v26, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0134, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.fossil.at5.s, "removeBuddyChallengeOnAllPresets - start reset active watchApp");
        r4 = new com.google.gson.Gson();
        r12 = r9.q;
        r11.L$0 = r9;
        r11.L$1 = r8;
        r11.L$2 = r7;
        r11.L$3 = r13;
        r11.L$4 = r5;
        r11.L$5 = r14;
        r11.L$6 = r3;
        r11.L$7 = r2;
        r11.L$8 = r10;
        r11.L$9 = r6;
        r11.L$10 = r4;
        r11.L$11 = r6;
        r11.label = 2;
        r10 = r12.getAllDianaAppSettings("watch_app", r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x016b, code lost:
        if (r10 != r15) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x019f, code lost:
        r3 = r4;
        r12 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return r15;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(java.lang.String r19, com.fossil.qn7<? super com.fossil.tl7> r20) {
        /*
        // Method dump skipped, instructions count: 419
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.at5.t(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x03fa  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x03fe  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x05a3  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x05aa  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x05ba  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x05bd  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x05e3  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x05e9  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x05ee  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0066 A[Catch:{ Exception -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071 A[Catch:{ Exception -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0126 A[Catch:{ Exception -> 0x0262 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x017e A[Catch:{ Exception -> 0x05d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01ec  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01fb  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02b3 A[SYNTHETIC, Splitter:B:79:0x02b3] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02fe  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0337  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x036a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0397  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x039b  */
    /* renamed from: u */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.tt5 r20, com.fossil.qn7<java.lang.Object> r21) {
        /*
        // Method dump skipped, instructions count: 1546
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.at5.k(com.fossil.tt5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object v(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.at5.h
            if (r0 == 0) goto L_0x0049
            r0 = r9
            com.fossil.at5$h r0 = (com.fossil.at5.h) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0049
            int r1 = r1 + r3
            r0.label = r1
            r5 = r0
        L_0x0014:
            java.lang.Object r1 = r5.result
            java.lang.Object r6 = com.fossil.yn7.d()
            int r0 = r5.label
            if (r0 == 0) goto L_0x0057
            if (r0 != r7) goto L_0x004f
            java.lang.Object r0 = r5.L$0
            com.fossil.at5 r0 = (com.fossil.at5) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            java.util.List r0 = (java.util.List) r0
            long r2 = java.lang.System.currentTimeMillis()
            r1.<init>(r0, r2)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r2 = r2.J()
            r0.s1(r1, r2)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0048:
            return r0
        L_0x0049:
            com.fossil.at5$h r5 = new com.fossil.at5$h
            r5.<init>(r8, r9)
            goto L_0x0014
        L_0x004f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0057:
            com.fossil.el7.b(r1)
            com.fossil.e47 r0 = com.fossil.e47.b
            com.fossil.v36 r1 = r8.f
            com.fossil.d26 r2 = r8.l
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r3 = r8.k
            com.fossil.on5 r4 = r8.e
            r5.L$0 = r8
            r5.label = r7
            java.lang.Object r0 = r0.c(r1, r2, r3, r4, r5)
            if (r0 != r6) goto L_0x0028
            r0 = r6
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.at5.v(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void w(int i2, boolean z, UserProfile userProfile, String str) {
        pq7.c(userProfile, "userProfile");
        pq7.c(str, "serial");
        if (f() != null) {
            wq5.d.e(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String h2 = h();
        local.d(h2, ".startDeviceSync - syncMode=" + i2);
        if (!this.e.z0()) {
            SKUModel skuModelBySerialPrefix = this.h.getSkuModelBySerialPrefix(nk5.o.m(str));
            this.e.t1(Boolean.TRUE);
            this.o.o(i2, skuModelBySerialPrefix);
            ul5 c2 = ck5.f.c("sync_session");
            ck5.f.a("sync_session", c2);
            c2.i();
        }
        if (!PortfolioApp.h0.c().T1(str, userProfile)) {
            s(userProfile.getOriginalSyncMode(), str, 2);
        }
    }

    @DexIgnore
    public final void x(int i2, boolean z, UserProfile userProfile, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "verifySecretKey " + str);
        s(userProfile.getOriginalSyncMode(), str, 0);
        this.m.e(new d37.a(str), new i(this, str, i2, z, userProfile));
    }
}
