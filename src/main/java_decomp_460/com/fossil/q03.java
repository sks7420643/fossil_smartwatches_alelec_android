package com.fossil;

import com.fossil.e13;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q03 {
    @DexIgnore
    public static volatile q03 b;
    @DexIgnore
    public static volatile q03 c;
    @DexIgnore
    public static /* final */ q03 d; // = new q03(true);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<a, e13.d<?, ?>> f2904a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f2905a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Object obj, int i) {
            this.f2905a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.f2905a == aVar.f2905a && this.b == aVar.b;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.f2905a) * 65535) + this.b;
        }
    }

    @DexIgnore
    public q03() {
        this.f2904a = new HashMap();
    }

    @DexIgnore
    public q03(boolean z) {
        this.f2904a = Collections.emptyMap();
    }

    @DexIgnore
    public static q03 a() {
        q03 q03 = b;
        if (q03 == null) {
            synchronized (q03.class) {
                try {
                    q03 = b;
                    if (q03 == null) {
                        q03 = d;
                        b = q03;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return q03;
    }

    @DexIgnore
    public static q03 c() {
        q03 q03 = c;
        if (q03 == null) {
            synchronized (q03.class) {
                try {
                    q03 = c;
                    if (q03 == null) {
                        q03 = d13.b(q03.class);
                        c = q03;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return q03;
    }

    @DexIgnore
    public final <ContainingType extends m23> e13.d<ContainingType, ?> b(ContainingType containingtype, int i) {
        return (e13.d<ContainingType, ?>) this.f2904a.get(new a(containingtype, i));
    }
}
