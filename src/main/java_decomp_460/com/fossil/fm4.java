package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ dm4 f1156a;
    @DexIgnore
    public /* final */ List<em4> b;

    @DexIgnore
    public fm4(dm4 dm4) {
        this.f1156a = dm4;
        ArrayList arrayList = new ArrayList();
        this.b = arrayList;
        arrayList.add(new em4(dm4, new int[]{1}));
    }

    @DexIgnore
    public final em4 a(int i) {
        if (i >= this.b.size()) {
            List<em4> list = this.b;
            em4 em4 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                dm4 dm4 = this.f1156a;
                em4 = em4.g(new em4(dm4, new int[]{1, dm4.c((size - 1) + dm4.d())}));
                this.b.add(em4);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void b(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                em4 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] d = new em4(this.f1156a, iArr2).h(i, 1).b(a2)[1].d();
                int length2 = i - d.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(d, 0, iArr, length + length2, d.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
