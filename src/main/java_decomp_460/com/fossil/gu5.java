package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu5 extends iq4<d, e, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ c f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return gu5.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(gu5.h.a(), "Inside .onReceive");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal());
            if (gu5.this.o() && pq7.a(gu5.this.n(), stringExtra) && intExtra == CommunicateMode.READ_RSSI.ordinal()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = gu5.h.a();
                local.d(a2, "onReceive - blePhase: " + intExtra + " - deviceId: " + stringExtra + " - mIsExecuted: " + gu5.this.o());
                gu5.this.r(false);
                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal() || intent.getExtras() == null) {
                    FLogger.INSTANCE.getLocal().e(gu5.h.a(), "Inside .onReceive return error");
                    gu5.this.i(new b());
                    return;
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int i = extras.getInt("rssi", 0);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = gu5.h.a();
                    local2.d(a3, "Inside .onReceive return rssi=" + i);
                    gu5.this.j(new e(i));
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1367a;

        @DexIgnore
        public d(String str) {
            pq7.c(str, "deviceId");
            this.f1367a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f1367a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1368a;

        @DexIgnore
        public e(int i) {
            this.f1368a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f1368a;
        }
    }

    /*
    static {
        String simpleName = gu5.class.getSimpleName();
        pq7.b(simpleName, "GetRssi::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return g;
    }

    @DexIgnore
    public final String n() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        pq7.n("mDeviceId");
        throw null;
    }

    @DexIgnore
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    public final void p() {
        wq5.d.e(this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    /* renamed from: q */
    public Object k(d dVar, qn7<Object> qn7) {
        String str;
        String str2 = null;
        try {
            FLogger.INSTANCE.getLocal().d(g, "Inside .run");
            this.d = true;
            if (dVar == null || (str = dVar.a()) == null) {
                str = "";
            }
            this.e = str;
            if (PortfolioApp.h0.b() != null) {
                IButtonConnectivity b2 = PortfolioApp.h0.b();
                if (b2 != null) {
                    if (dVar != null) {
                        str2 = dVar.a();
                    }
                    return ao7.f(b2.deviceGetRssi(str2));
                }
                pq7.i();
                throw null;
            }
            FLogger.INSTANCE.getLocal().e(g, "Inside .run ButtonApi is null");
            return new b();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.e(str3, "Inside .run caught exception=" + e2);
            return new b();
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void s() {
        wq5.d.j(this.f, CommunicateMode.READ_RSSI);
    }
}
