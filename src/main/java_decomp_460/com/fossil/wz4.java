package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f4024a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new a().getType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<? extends Long>> {
    }

    @DexIgnore
    public final List<Long> a(String str) {
        pq7.c(str, "data");
        if (str.length() == 0) {
            return hm7.e();
        }
        try {
            Object l = this.f4024a.l(str, this.b);
            pq7.b(l, "mGson.fromJson(data, mType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListLong: ");
            e.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("GFitActiveTimeConverter", sb.toString());
            return hm7.e();
        }
    }

    @DexIgnore
    public final String b(List<Long> list) {
        pq7.c(list, "activeTimes");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String u = this.f4024a.u(list, this.b);
            pq7.b(u, "mGson.toJson(activeTimes, mType)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("GFitActiveTimeConverter", sb.toString());
            return "";
        }
    }
}
