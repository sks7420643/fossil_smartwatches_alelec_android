package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hd4 implements kd4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1464a;
    @DexIgnore
    public /* final */ kd4[] b;
    @DexIgnore
    public /* final */ id4 c;

    @DexIgnore
    public hd4(int i, kd4... kd4Arr) {
        this.f1464a = i;
        this.b = kd4Arr;
        this.c = new id4(i);
    }

    @DexIgnore
    @Override // com.fossil.kd4
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.f1464a) {
            return stackTraceElementArr;
        }
        kd4[] kd4Arr = this.b;
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (kd4 kd4 : kd4Arr) {
            if (stackTraceElementArr2.length <= this.f1464a) {
                break;
            }
            stackTraceElementArr2 = kd4.a(stackTraceElementArr);
        }
        if (stackTraceElementArr2.length > this.f1464a) {
            stackTraceElementArr2 = this.c.a(stackTraceElementArr2);
        }
        return stackTraceElementArr2;
    }
}
