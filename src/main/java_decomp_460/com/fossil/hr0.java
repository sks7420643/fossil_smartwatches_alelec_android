package com.fossil;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class hr0 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ SparseArray<PowerManager.WakeLock> f1517a; // = new SparseArray<>();
    @DexIgnore
    public static int b; // = 1;

    @DexIgnore
    public static boolean b(Intent intent) {
        int intExtra = intent.getIntExtra("androidx.contentpager.content.wakelockid", 0);
        if (intExtra == 0) {
            return false;
        }
        synchronized (f1517a) {
            PowerManager.WakeLock wakeLock = f1517a.get(intExtra);
            if (wakeLock != null) {
                wakeLock.release();
                f1517a.remove(intExtra);
                return true;
            }
            Log.w("WakefulBroadcastReceiv.", "No active wake lock id #" + intExtra);
            return true;
        }
    }

    @DexIgnore
    public static ComponentName c(Context context, Intent intent) {
        synchronized (f1517a) {
            int i = b;
            int i2 = b + 1;
            b = i2;
            if (i2 <= 0) {
                b = 1;
            }
            intent.putExtra("androidx.contentpager.content.wakelockid", i);
            ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "androidx.core:wake:" + startService.flattenToShortString());
            newWakeLock.setReferenceCounted(false);
            newWakeLock.acquire(60000);
            f1517a.put(i, newWakeLock);
            return startService;
        }
    }
}
