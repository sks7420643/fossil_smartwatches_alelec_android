package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String[] f2767a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<String> f2768a; // = new ArrayList(20);

        @DexIgnore
        public a a(String str, String str2) {
            p18.a(str);
            p18.b(str2, str);
            c(str, str2);
            return this;
        }

        @DexIgnore
        public a b(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                c(str.substring(0, indexOf), str.substring(indexOf + 1));
            } else if (str.startsWith(":")) {
                c("", str.substring(1));
            } else {
                c("", str);
            }
            return this;
        }

        @DexIgnore
        public a c(String str, String str2) {
            this.f2768a.add(str);
            this.f2768a.add(str2.trim());
            return this;
        }

        @DexIgnore
        public a d(String str, String str2) {
            p18.a(str);
            c(str, str2);
            return this;
        }

        @DexIgnore
        public p18 e() {
            return new p18(this);
        }

        @DexIgnore
        public String f(String str) {
            for (int size = this.f2768a.size() - 2; size >= 0; size -= 2) {
                if (str.equalsIgnoreCase(this.f2768a.get(size))) {
                    return this.f2768a.get(size + 1);
                }
            }
            return null;
        }

        @DexIgnore
        public a g(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f2768a.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase(this.f2768a.get(i2))) {
                    this.f2768a.remove(i2);
                    this.f2768a.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        @DexIgnore
        public a h(String str, String str2) {
            p18.a(str);
            p18.b(str2, str);
            g(str);
            c(str, str2);
            return this;
        }
    }

    @DexIgnore
    public p18(a aVar) {
        List<String> list = aVar.f2768a;
        this.f2767a = (String[]) list.toArray(new String[list.size()]);
    }

    @DexIgnore
    public p18(String[] strArr) {
        this.f2767a = strArr;
    }

    @DexIgnore
    public static void a(String str) {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (!str.isEmpty()) {
            int length = str.length();
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt <= ' ' || charAt >= '\u007f') {
                    throw new IllegalArgumentException(b28.r("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                }
            }
        } else {
            throw new IllegalArgumentException("name is empty");
        }
    }

    @DexIgnore
    public static void b(String str, String str2) {
        if (str != null) {
            int length = str.length();
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if ((charAt <= 31 && charAt != '\t') || charAt >= '\u007f') {
                    throw new IllegalArgumentException(b28.r("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt), Integer.valueOf(i), str2, str));
                }
            }
            return;
        }
        throw new NullPointerException("value for name " + str2 + " == null");
    }

    @DexIgnore
    public static String d(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    @DexIgnore
    public static p18 g(String... strArr) {
        if (strArr == null) {
            throw new NullPointerException("namesAndValues == null");
        } else if (strArr.length % 2 == 0) {
            String[] strArr2 = (String[]) strArr.clone();
            for (int i = 0; i < strArr2.length; i++) {
                if (strArr2[i] != null) {
                    strArr2[i] = strArr2[i].trim();
                } else {
                    throw new IllegalArgumentException("Headers cannot be null");
                }
            }
            for (int i2 = 0; i2 < strArr2.length; i2 += 2) {
                String str = strArr2[i2];
                String str2 = strArr2[i2 + 1];
                a(str);
                b(str2, str);
            }
            return new p18(strArr2);
        } else {
            throw new IllegalArgumentException("Expected alternating header names and values");
        }
    }

    @DexIgnore
    public String c(String str) {
        return d(this.f2767a, str);
    }

    @DexIgnore
    public String e(int i) {
        return this.f2767a[i * 2];
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof p18) && Arrays.equals(((p18) obj).f2767a, this.f2767a);
    }

    @DexIgnore
    public a f() {
        a aVar = new a();
        Collections.addAll(aVar.f2768a, this.f2767a);
        return aVar;
    }

    @DexIgnore
    public int h() {
        return this.f2767a.length / 2;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.f2767a);
    }

    @DexIgnore
    public String i(int i) {
        return this.f2767a[(i * 2) + 1];
    }

    @DexIgnore
    public List<String> j(String str) {
        int h = h();
        ArrayList arrayList = null;
        for (int i = 0; i < h; i++) {
            if (str.equalsIgnoreCase(e(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(i(i));
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int h = h();
        for (int i = 0; i < h; i++) {
            sb.append(e(i));
            sb.append(": ");
            sb.append(i(i));
            sb.append("\n");
        }
        return sb.toString();
    }
}
