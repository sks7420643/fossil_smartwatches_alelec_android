package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fv extends dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public byte[] N; // = new byte[0];
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public int R; // = -1;
    @DexIgnore
    public float S;
    @DexIgnore
    public n6 T;
    @DexIgnore
    public long U;
    @DexIgnore
    public /* final */ hw V;
    @DexIgnore
    public /* final */ gp7<tl7> W;

    @DexIgnore
    public fv(iu iuVar, short s, hs hsVar, k5 k5Var, int i) {
        super(iuVar, s, hsVar, k5Var, i);
        n6 n6Var = n6.UNKNOWN;
        this.T = n6Var;
        this.V = new hw(0, n6Var, new byte[0], null, 9);
        this.W = new ev(this, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final JSONObject F(byte[] bArr) {
        if (!this.Q) {
            JSONObject jSONObject = new JSONObject();
            if (bArr.length >= 4) {
                long o = hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                this.M = o;
                g80.k(jSONObject, jd0.F0, Long.valueOf(o));
                if (this.M == 0) {
                    m(mw.a(this.v, null, null, lw.b, null, null, 27));
                } else {
                    n6 a2 = bArr.length >= 5 ? n6.q.a(bArr[4]) : n6.FTD;
                    this.T = a2;
                    g80.k(jSONObject, jd0.G0, a2.b);
                    this.V.c = this.T;
                    if (this.s) {
                        ix.b.a(this.y.x).d(this.T);
                    }
                }
            } else {
                this.v = mw.a(this.v, null, null, lw.k, null, null, 27);
            }
            this.Q = true;
            this.E = this.v.d != lw.b;
            byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(iu.i.a()).putShort(this.L).array();
            pq7.b(array, "ByteBuffer.allocate(1 + \u2026                 .array()");
            this.H = array;
            return jSONObject;
        }
        B();
        JSONObject jSONObject2 = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        S(hy1.o(order.getInt(0)));
        Q(hy1.o(order.getInt(4)));
        g80.k(g80.k(jSONObject2, jd0.I, Long.valueOf(U())), jd0.J, Long.valueOf(T()));
        W();
        this.E = true;
        return jSONObject2;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final boolean G(o7 o7Var) {
        return o7Var.f2638a == this.T;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final void J(o7 o7Var) {
        byte[] b = this.s ? jx.b.b(this.y.x, this.T, o7Var.b) : o7Var.b;
        if (hy1.n(hy1.p((byte) (b[0] & 63))) == (this.R + 1) % 64) {
            if (((byte) (b[0] & ((byte) 128))) != ((byte) 0)) {
                f(5000);
                n(this.W);
            } else {
                n(this.p);
            }
            this.R++;
            R(dy1.a(V(), dm7.k(b, 1, b.length)));
            float length = (((float) V().length) * 1.0f) / ((float) this.M);
            if (length - this.S > 0.001f || length == 1.0f) {
                this.S = length;
                e(length);
            }
            if (this.U == 0) {
                k(this.V);
            }
            this.U = ((long) b.length) + this.U;
            g80.k(g80.k(g80.k(this.V.e, jd0.V0, Integer.valueOf(this.R + 1)), jd0.q3, Long.valueOf(this.U)), jd0.H0, Integer.valueOf(V().length));
            return;
        }
        this.v = mw.a(this.v, null, null, lw.h, null, null, 27);
        this.E = true;
    }

    @DexIgnore
    public void Q(long j) {
        this.P = j;
    }

    @DexIgnore
    public void R(byte[] bArr) {
        this.N = bArr;
    }

    @DexIgnore
    public void S(long j) {
        this.O = j;
    }

    @DexIgnore
    public long T() {
        return this.P;
    }

    @DexIgnore
    public long U() {
        return this.O;
    }

    @DexIgnore
    public byte[] V() {
        return this.N;
    }

    @DexIgnore
    public void W() {
    }
}
