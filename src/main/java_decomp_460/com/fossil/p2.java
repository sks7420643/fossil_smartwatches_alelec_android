package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p2 implements Parcelable.Creator<q2> {
    @DexIgnore
    public /* synthetic */ p2(kq7 kq7) {
    }

    @DexIgnore
    public q2 a(Parcel parcel) {
        return new q2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public q2 createFromParcel(Parcel parcel) {
        return new q2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public q2[] newArray(int i) {
        return new q2[i];
    }
}
