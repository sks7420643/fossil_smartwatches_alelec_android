package com.fossil;

import android.os.Build;
import androidx.fragment.app.Fragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y78<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public T f4255a;

    @DexIgnore
    public y78(T t) {
        this.f4255a = t;
    }

    @DexIgnore
    public static y78<Fragment> b(Fragment fragment) {
        return Build.VERSION.SDK_INT < 23 ? new x78(fragment) : new z78(fragment);
    }

    @DexIgnore
    public T a() {
        return this.f4255a;
    }

    @DexIgnore
    public boolean c(String str) {
        return !d(str);
    }

    @DexIgnore
    public abstract boolean d(String str);

    @DexIgnore
    public boolean e(List<String> list) {
        for (String str : list) {
            if (c(str)) {
                return true;
            }
        }
        return false;
    }
}
