package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx7 extends jz7 implements sw7 {
    @DexIgnore
    @Override // com.fossil.sw7
    public kx7 b() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return nv7.c() ? x("Active") : super.toString();
    }

    @DexIgnore
    public final String x(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object l = l();
        if (l != null) {
            boolean z = true;
            lz7 lz7 = (lz7) l;
            while (!pq7.a(lz7, this)) {
                if (lz7 instanceof ex7) {
                    ex7 ex7 = (ex7) lz7;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(ex7);
                }
                lz7 = lz7.m();
                z = z;
            }
            sb.append("]");
            String sb2 = sb.toString();
            pq7.b(sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
