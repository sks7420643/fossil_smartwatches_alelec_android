package com.fossil;

import android.database.Cursor;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class f32 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ f32 f1045a; // = new f32();

    @DexIgnore
    public static j32.b a() {
        return f1045a;
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.G((Cursor) obj);
    }
}
