package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it4 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1665a;
    @DexIgnore
    @rj4("socialId")
    public String b;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @rj4("points")
    public Integer e;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String f;
    @DexIgnore
    @rj4("createdAt")
    public String g;
    @DexIgnore
    @rj4("updatedAt")
    public String h;

    @DexIgnore
    public it4(String str, String str2, String str3, String str4, Integer num, String str5, String str6, String str7) {
        pq7.c(str, "id");
        pq7.c(str2, "socialId");
        this.f1665a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = num;
        this.f = str5;
        this.g = str6;
        this.h = str7;
    }

    @DexIgnore
    public final String a() {
        return this.g;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.f1665a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final Integer e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof it4) {
                it4 it4 = (it4) obj;
                if (!pq7.a(this.f1665a, it4.f1665a) || !pq7.a(this.b, it4.b) || !pq7.a(this.c, it4.c) || !pq7.a(this.d, it4.d) || !pq7.a(this.e, it4.e) || !pq7.a(this.f, it4.f) || !pq7.a(this.g, it4.g) || !pq7.a(this.h, it4.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.f;
    }

    @DexIgnore
    public final String g() {
        return this.b;
    }

    @DexIgnore
    public final String h() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1665a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        Integer num = this.e;
        int hashCode5 = num != null ? num.hashCode() : 0;
        String str5 = this.f;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.g;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.h;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Profile(id=" + this.f1665a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", points=" + this.e + ", profilePicture=" + this.f + ", createdAt=" + this.g + ", updatedAt=" + this.h + ")";
    }
}
