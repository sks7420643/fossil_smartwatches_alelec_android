package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e14 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f869a;
        @DexIgnore
        public /* final */ a b;
        @DexIgnore
        public a c;
        @DexIgnore
        public boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public String f870a;
            @DexIgnore
            public Object b;
            @DexIgnore
            public a c;

            @DexIgnore
            public a() {
            }
        }

        @DexIgnore
        public b(String str) {
            a aVar = new a();
            this.b = aVar;
            this.c = aVar;
            this.d = false;
            i14.l(str);
            this.f869a = str;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b a(String str, int i) {
            e(str, String.valueOf(i));
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b b(String str, Object obj) {
            e(str, obj);
            return this;
        }

        @DexIgnore
        public final a c() {
            a aVar = new a();
            this.c.c = aVar;
            this.c = aVar;
            return aVar;
        }

        @DexIgnore
        public final b d(Object obj) {
            c().b = obj;
            return this;
        }

        @DexIgnore
        public final b e(String str, Object obj) {
            a c2 = c();
            c2.b = obj;
            i14.l(str);
            c2.f870a = str;
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b f(Object obj) {
            d(obj);
            return this;
        }

        @DexIgnore
        public String toString() {
            boolean z = this.d;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.f869a);
            sb.append('{');
            String str = "";
            for (a aVar = this.b.c; aVar != null; aVar = aVar.c) {
                Object obj = aVar.b;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = aVar.f870a;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj == null || !obj.getClass().isArray()) {
                        sb.append(obj);
                    } else {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    @DexIgnore
    public static <T> T a(T t, T t2) {
        if (t != null) {
            return t;
        }
        i14.l(t2);
        return t2;
    }

    @DexIgnore
    public static b b(Object obj) {
        return new b(obj.getClass().getSimpleName());
    }
}
