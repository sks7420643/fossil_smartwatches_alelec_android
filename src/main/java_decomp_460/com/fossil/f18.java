package com.fossil;

import com.fossil.p28;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f18 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), b28.G("OkHttp ConnectionPool", true));

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1037a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<l28> d;
    @DexIgnore
    public /* final */ m28 e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            while (true) {
                long a2 = f18.this.a(System.nanoTime());
                if (a2 != -1) {
                    if (a2 > 0) {
                        long j = a2 / 1000000;
                        synchronized (f18.this) {
                            try {
                                f18.this.wait(j, (int) (a2 - (1000000 * j)));
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public f18() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public f18(int i, long j, TimeUnit timeUnit) {
        this.c = new a();
        this.d = new ArrayDeque();
        this.e = new m28();
        this.f1037a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            l28 l28 = null;
            long j2 = Long.MIN_VALUE;
            int i = 0;
            int i2 = 0;
            for (l28 l282 : this.d) {
                if (e(l282, j) > 0) {
                    i++;
                } else {
                    i2++;
                    long j3 = j - l282.o;
                    if (j3 > j2) {
                        j2 = j3;
                        l28 = l282;
                    }
                }
            }
            if (j2 >= this.b || i2 > this.f1037a) {
                this.d.remove(l28);
                b28.h(l28.r());
                return 0;
            } else if (i2 > 0) {
                return this.b - j2;
            } else if (i > 0) {
                return this.b;
            } else {
                this.f = false;
                return -1;
            }
        }
    }

    @DexIgnore
    public boolean b(l28 l28) {
        if (l28.k || this.f1037a == 0) {
            this.d.remove(l28);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public Socket c(x08 x08, p28 p28) {
        for (l28 l28 : this.d) {
            if (l28.m(x08, null) && l28.o() && l28 != p28.d()) {
                return p28.m(l28);
            }
        }
        return null;
    }

    @DexIgnore
    public l28 d(x08 x08, p28 p28, x18 x18) {
        for (l28 l28 : this.d) {
            if (l28.m(x08, x18)) {
                p28.a(l28, true);
                return l28;
            }
        }
        return null;
    }

    @DexIgnore
    public final int e(l28 l28, long j) {
        List<Reference<p28>> list = l28.n;
        int i = 0;
        while (i < list.size()) {
            Reference<p28> reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                w38.j().r("A connection to " + l28.q().a().l() + " was leaked. Did you forget to close a response body?", ((p28.a) reference).f2771a);
                list.remove(i);
                l28.k = true;
                if (list.isEmpty()) {
                    l28.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }

    @DexIgnore
    public void f(l28 l28) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(l28);
    }
}
