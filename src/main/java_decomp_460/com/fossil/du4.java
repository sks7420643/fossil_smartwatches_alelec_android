package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f834a;
    @DexIgnore
    public /* final */ bu4 b;
    @DexIgnore
    public /* final */ cu4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRepository", f = "NotificationRepository.kt", l = {20}, m = "fetchNotifications")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ du4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(du4 du4, qn7 qn7) {
            super(qn7);
            this.this$0 = du4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore
    public du4(bu4 bu4, cu4 cu4) {
        pq7.c(bu4, "local");
        pq7.c(cu4, "remote");
        this.b = bu4;
        this.c = cu4;
        String name = du4.class.getName();
        pq7.b(name, "NotificationRepository::class.java.name");
        this.f834a = name;
    }

    @DexIgnore
    public final void a() {
        this.b.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.dt4>>> r8) {
        /*
            r7 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            r2 = 0
            boolean r0 = r8 instanceof com.fossil.du4.a
            if (r0 == 0) goto L_0x006f
            r0 = r8
            com.fossil.du4$a r0 = (com.fossil.du4.a) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x006f
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x007d
            if (r4 != r5) goto L_0x0075
            java.lang.Object r0 = r0.L$0
            com.fossil.du4 r0 = (com.fossil.du4) r0
            com.fossil.el7.b(r1)
            r7 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x009e
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            if (r1 == 0) goto L_0x008f
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = r7.f834a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "notification: "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r3.e(r4, r5)
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0064
            com.fossil.bu4 r0 = r7.b
            java.util.List r3 = r1.get_items()
            r0.b(r3)
        L_0x0064:
            com.fossil.kz4 r0 = new com.fossil.kz4
            java.util.List r1 = r1.get_items()
            r3 = 2
            r0.<init>(r1, r2, r3, r2)
        L_0x006e:
            return r0
        L_0x006f:
            com.fossil.du4$a r0 = new com.fossil.du4$a
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0075:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007d:
            com.fossil.el7.b(r1)
            com.fossil.cu4 r1 = r7.c
            r0.L$0 = r7
            r0.label = r5
            r4 = 0
            java.lang.Object r1 = com.fossil.cu4.c(r1, r4, r0, r5, r2)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x006e
        L_0x008f:
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            java.lang.String r3 = "response is null"
            r1.<init>(r2, r3)
            r0.<init>(r1)
            goto L_0x006e
        L_0x009e:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00be
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r3 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00c4
            java.lang.String r0 = r0.getMessage()
        L_0x00b2:
            com.fossil.kz4 r1 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>(r3, r0)
            r1.<init>(r2)
            r0 = r1
            goto L_0x006e
        L_0x00be:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        L_0x00c4:
            r0 = r2
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.du4.b(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<dt4>> c() {
        return this.b.c();
    }
}
