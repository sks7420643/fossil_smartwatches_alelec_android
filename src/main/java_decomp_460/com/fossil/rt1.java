package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(mq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Re\u2026class.java.classLoader)!!");
                mq1 mq1 = (mq1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(nt1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new rt1(mq1, (nt1) readParcelable2);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rt1[] newArray(int i) {
            return new rt1[i];
        }
    }

    @DexIgnore
    public rt1(mq1 mq1, nt1 nt1) {
        super(mq1, nt1);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    d90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            pq7.i();
            throw null;
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
    }
}
