package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f57.b f1553a;
    @DexIgnore
    public /* final */ uy4 b; // = uy4.d.b();
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ne5 f1554a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ne5 ne5) {
            super(ne5.n());
            pq7.c(ne5, "binding");
            this.f1554a = ne5;
        }

        @DexIgnore
        public final void a(xs4 xs4, f57.b bVar, uy4 uy4) {
            int i = 8;
            pq7.c(xs4, "friend");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            ne5 ne5 = this.f1554a;
            String b = hz4.f1561a.b(xs4.b(), xs4.e(), xs4.i());
            FlexibleTextView flexibleTextView = ne5.v;
            pq7.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b);
            ImageView imageView = ne5.r;
            pq7.b(imageView, "ivAvatar");
            ty4.b(imageView, xs4.h(), b, bVar, uy4);
            int c = xs4.c();
            if (c == -1) {
                ImageView imageView2 = ne5.s;
                pq7.b(imageView2, "ivPending");
                imageView2.setVisibility(0);
                ImageView imageView3 = ne5.s;
                pq7.b(imageView3, "ivPending");
                imageView3.setImageDrawable(gl0.f(imageView3.getContext(), 2131231019));
                ProgressBar progressBar = ne5.u;
                pq7.b(progressBar, "prg");
                progressBar.setVisibility(8);
            } else if (c == 0) {
                ImageView imageView4 = ne5.s;
                pq7.b(imageView4, "ivPending");
                imageView4.setVisibility(8);
                ProgressBar progressBar2 = ne5.u;
                pq7.b(progressBar2, "prg");
                if (xs4.f() != 0) {
                    i = 0;
                }
                progressBar2.setVisibility(i);
            } else if (c == 1) {
                ImageView imageView5 = ne5.s;
                pq7.b(imageView5, "ivPending");
                imageView5.setVisibility(0);
                ImageView imageView6 = ne5.s;
                pq7.b(imageView6, "ivPending");
                imageView6.setImageDrawable(gl0.f(imageView6.getContext(), 2131231121));
                ProgressBar progressBar3 = ne5.u;
                pq7.b(progressBar3, "prg");
                progressBar3.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public hy4(int i) {
        this.c = i;
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.f1553a = f;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        Object obj = list.get(i);
        if (obj instanceof xs4) {
            xs4 xs4 = (xs4) obj;
            if (xs4.c() == 0 || xs4.c() == 1) {
                return true;
            }
            if (xs4.c() == -1) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof xs4)) {
            obj = null;
        }
        xs4 xs4 = (xs4) obj;
        if (viewHolder instanceof a) {
            aVar = viewHolder;
        }
        a aVar2 = aVar;
        if (xs4 != null && aVar2 != null) {
            aVar2.a(xs4, this.f1553a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        ne5 z = ne5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemFriendListBinding.in\u2026.context), parent, false)");
        return new a(z);
    }
}
