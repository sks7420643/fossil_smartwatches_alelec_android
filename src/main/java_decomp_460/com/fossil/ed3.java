package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ed3 extends ds2 implements dd3 {
    @DexIgnore
    public ed3() {
        super("com.google.android.gms.maps.internal.ISnapshotReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            onSnapshotReady((Bitmap) es2.b(parcel, Bitmap.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            q1(rg2.a.e(parcel.readStrongBinder()));
        }
        parcel2.writeNoException();
        return true;
    }
}
