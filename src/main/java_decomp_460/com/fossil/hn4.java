package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn4 extends jn4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int[] f1497a; // = {1, 1, 1, 1};
    @DexIgnore
    public static /* final */ int[] b; // = {3, 1, 1};

    @DexIgnore
    @Override // com.fossil.jn4, com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        if (kl4 == kl4.ITF) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode ITF, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.jn4
    public boolean[] c(String str) {
        int length = str.length();
        if (length % 2 != 0) {
            throw new IllegalArgumentException("The length of the input should be even");
        } else if (length <= 80) {
            boolean[] zArr = new boolean[((length * 9) + 9)];
            int b2 = jn4.b(zArr, 0, f1497a, true);
            for (int i = 0; i < length; i += 2) {
                int digit = Character.digit(str.charAt(i), 10);
                int digit2 = Character.digit(str.charAt(i + 1), 10);
                int[] iArr = new int[18];
                for (int i2 = 0; i2 < 5; i2++) {
                    int i3 = i2 * 2;
                    int[][] iArr2 = gn4.f1336a;
                    iArr[i3] = iArr2[digit][i2];
                    iArr[i3 + 1] = iArr2[digit2][i2];
                }
                b2 += jn4.b(zArr, b2, iArr, true);
            }
            jn4.b(zArr, b2, b, true);
            return zArr;
        } else {
            throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
        }
    }
}
