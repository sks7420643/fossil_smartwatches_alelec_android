package com.fossil;

import com.fossil.tn7;
import com.fossil.vx7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv7 extends nn7 implements vx7<String> {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tn7.c<gv7> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public gv7(long j) {
        super(c);
        this.b = j;
    }

    @DexIgnore
    public final long M() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: P */
    public void B(tn7 tn7, String str) {
        Thread.currentThread().setName(str);
    }

    @DexIgnore
    /* renamed from: Q */
    public String F(tn7 tn7) {
        String M;
        hv7 hv7 = (hv7) tn7.get(hv7.c);
        String str = (hv7 == null || (M = hv7.M()) == null) ? "coroutine" : M;
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        int L = wt7.L(name, " @", 0, false, 6, null);
        if (L < 0) {
            L = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + L + 10);
        if (name != null) {
            String substring = name.substring(0, L);
            pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(" @");
            sb.append(str);
            sb.append('#');
            sb.append(this.b);
            String sb2 = sb.toString();
            pq7.b(sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
            currentThread.setName(sb2);
            return name;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof gv7) && this.b == ((gv7) obj).b);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.nn7
    public <R> R fold(R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
        return (R) vx7.a.a(this, r, vp7);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.tn7.b, com.fossil.nn7
    public <E extends tn7.b> E get(tn7.c<E> cVar) {
        return (E) vx7.a.b(this, cVar);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.nn7
    public tn7 minusKey(tn7.c<?> cVar) {
        return vx7.a.c(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.nn7
    public tn7 plus(tn7 tn7) {
        return vx7.a.d(this, tn7);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.b + ')';
    }
}
