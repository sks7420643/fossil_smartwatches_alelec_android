package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.tq4;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wq4 implements vq4 {
    @DexIgnore
    public static /* final */ int c;
    @DexIgnore
    public static /* final */ int d;
    @DexIgnore
    public static /* final */ int e; // = ((c * 2) + 1);
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(Integer.MAX_VALUE);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Handler f3982a; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ ThreadPoolExecutor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicInteger f3983a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.f3983a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ tq4.d b;
        @DexIgnore
        public /* final */ /* synthetic */ tq4.a c;

        @DexIgnore
        public b(wq4 wq4, tq4.d dVar, tq4.a aVar) {
            this.b = dVar;
            this.c = aVar;
        }

        @DexIgnore
        public void run() {
            tq4.d dVar = this.b;
            if (dVar != null) {
                dVar.a(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ tq4.d b;
        @DexIgnore
        public /* final */ /* synthetic */ tq4.c c;

        @DexIgnore
        public c(wq4 wq4, tq4.d dVar, tq4.c cVar) {
            this.b = dVar;
            this.c = cVar;
        }

        @DexIgnore
        public void run() {
            tq4.d dVar = this.b;
            if (dVar != null) {
                dVar.onSuccess(this.c);
            }
        }
    }

    /*
    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        c = availableProcessors;
        d = Math.max(2, Math.min(availableProcessors - 1, 4));
    }
    */

    @DexIgnore
    public wq4() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(d, e, 30, TimeUnit.SECONDS, g, f);
        this.b = threadPoolExecutor;
        threadPoolExecutor.allowCoreThreadTimeOut(true);
    }

    @DexIgnore
    @Override // com.fossil.vq4
    public <P extends tq4.c, E extends tq4.a> void a(P p, tq4.d<P, E> dVar) {
        this.f3982a.post(new c(this, dVar, p));
    }

    @DexIgnore
    @Override // com.fossil.vq4
    public <P extends tq4.c, E extends tq4.a> void b(E e2, tq4.d<P, E> dVar) {
        this.f3982a.post(new b(this, dVar, e2));
    }

    @DexIgnore
    @Override // com.fossil.vq4
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }
}
