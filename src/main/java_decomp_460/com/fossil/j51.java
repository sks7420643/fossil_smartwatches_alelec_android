package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final a f1711a = a.f1712a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ a f1712a; // = new a();

        @DexIgnore
        public final j51 a() {
            int i = Build.VERSION.SDK_INT;
            return i >= 23 ? new l51() : i >= 19 ? new k51() : new i51();
        }
    }

    @DexIgnore
    String a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String d(Bitmap bitmap);

    @DexIgnore
    Bitmap removeLast();
}
