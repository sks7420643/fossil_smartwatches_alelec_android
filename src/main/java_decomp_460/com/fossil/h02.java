package com.fossil;

import android.util.Base64;
import com.fossil.xz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract h02 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(byte[] bArr);

        @DexIgnore
        public abstract a d(vy1 vy1);
    }

    @DexIgnore
    public static a a() {
        xz1.b bVar = new xz1.b();
        bVar.d(vy1.DEFAULT);
        return bVar;
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public abstract vy1 d();

    @DexIgnore
    public h02 e(vy1 vy1) {
        a a2 = a();
        a2.b(b());
        a2.d(vy1);
        a2.c(c());
        return a2.a();
    }

    @DexIgnore
    public final String toString() {
        return String.format("TransportContext(%s, %s, %s)", b(), d(), c() == null ? "" : Base64.encodeToString(c(), 2));
    }
}
