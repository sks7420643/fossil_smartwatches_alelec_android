package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu0 implements jv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ RecyclerView.g f3468a;

    @DexIgnore
    public tu0(RecyclerView.g gVar) {
        this.f3468a = gVar;
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void a(int i, int i2) {
        this.f3468a.notifyItemMoved(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void b(int i, int i2) {
        this.f3468a.notifyItemRangeInserted(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void c(int i, int i2) {
        this.f3468a.notifyItemRangeRemoved(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void d(int i, int i2, Object obj) {
        this.f3468a.notifyItemRangeChanged(i, i2, obj);
    }
}
