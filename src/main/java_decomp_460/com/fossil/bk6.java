package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bk6 implements Factory<jk6> {
    @DexIgnore
    public static jk6 a(yj6 yj6) {
        jk6 c = yj6.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
