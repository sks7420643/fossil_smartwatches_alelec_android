package com.fossil;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hs2 extends as2 implements fs2 {
    @DexIgnore
    public hs2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.fs2
    public final rg2 O1(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(5, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fs2
    public final rg2 g() throws RemoteException {
        Parcel e = e(4, d());
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fs2
    public final rg2 zza(Bitmap bitmap) throws RemoteException {
        Parcel d = d();
        es2.d(d, bitmap);
        Parcel e = e(6, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fs2
    public final rg2 zza(String str) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        Parcel e = e(2, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
