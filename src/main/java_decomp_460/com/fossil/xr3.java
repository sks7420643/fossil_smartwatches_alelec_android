package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xr3> CREATOR; // = new as3();
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public fr3 d;
    @DexIgnore
    public long e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;
    @DexIgnore
    public vg3 h;
    @DexIgnore
    public long i;
    @DexIgnore
    public vg3 j;
    @DexIgnore
    public long k;
    @DexIgnore
    public vg3 l;

    @DexIgnore
    public xr3(xr3 xr3) {
        rc2.k(xr3);
        this.b = xr3.b;
        this.c = xr3.c;
        this.d = xr3.d;
        this.e = xr3.e;
        this.f = xr3.f;
        this.g = xr3.g;
        this.h = xr3.h;
        this.i = xr3.i;
        this.j = xr3.j;
        this.k = xr3.k;
        this.l = xr3.l;
    }

    @DexIgnore
    public xr3(String str, String str2, fr3 fr3, long j2, boolean z, String str3, vg3 vg3, long j3, vg3 vg32, long j4, vg3 vg33) {
        this.b = str;
        this.c = str2;
        this.d = fr3;
        this.e = j2;
        this.f = z;
        this.g = str3;
        this.h = vg3;
        this.i = j3;
        this.j = vg32;
        this.k = j4;
        this.l = vg33;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 2, this.b, false);
        bd2.u(parcel, 3, this.c, false);
        bd2.t(parcel, 4, this.d, i2, false);
        bd2.r(parcel, 5, this.e);
        bd2.c(parcel, 6, this.f);
        bd2.u(parcel, 7, this.g, false);
        bd2.t(parcel, 8, this.h, i2, false);
        bd2.r(parcel, 9, this.i);
        bd2.t(parcel, 10, this.j, i2, false);
        bd2.r(parcel, 11, this.k);
        bd2.t(parcel, 12, this.l, i2, false);
        bd2.b(parcel, a2);
    }
}
