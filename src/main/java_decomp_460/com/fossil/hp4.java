package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp4 implements Factory<FirmwareFileRepository> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1504a;

    @DexIgnore
    public hp4(uo4 uo4) {
        this.f1504a = uo4;
    }

    @DexIgnore
    public static hp4 a(uo4 uo4) {
        return new hp4(uo4);
    }

    @DexIgnore
    public static FirmwareFileRepository c(uo4 uo4) {
        FirmwareFileRepository o = uo4.o();
        lk7.c(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    /* renamed from: b */
    public FirmwareFileRepository get() {
        return c(this.f1504a);
    }
}
