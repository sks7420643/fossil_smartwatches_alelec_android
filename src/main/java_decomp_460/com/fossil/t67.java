package com.fossil;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.s87;
import com.fossil.w67;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t67 extends r67 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public s87.c l;
    @DexIgnore
    public TextView m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final cl7<t67, ViewGroup.LayoutParams> a(WatchFaceEditorView watchFaceEditorView, s87.c cVar, int i) {
            pq7.c(watchFaceEditorView, "editorView");
            pq7.c(cVar, "textConfig");
            w87 b = cVar.b();
            float b2 = b != null ? b.b() : 2.0f;
            TextView textView = new TextView(watchFaceEditorView.getContext());
            textView.setText(cVar.i());
            textView.setTextColor(cVar.g().getValue());
            textView.setTextSize(cVar.h() / u67.b.a());
            textView.setTypeface(cVar.j());
            textView.setSingleLine(true);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            Context context = watchFaceEditorView.getContext();
            pq7.b(context, "editorView.context");
            t67 t67 = new t67(context, null);
            t67.C(textView, cVar, layoutParams, i);
            t67.setScaleX(b2);
            t67.setScaleY(b2);
            t67.setElementEventHandler(watchFaceEditorView);
            return hl7.a(t67, new FrameLayout.LayoutParams(-2, -2));
        }

        @DexIgnore
        public final cl7<t67, ViewGroup.LayoutParams> b(Context context, s87.c cVar) {
            pq7.c(context, "context");
            pq7.c(cVar, "textConfig");
            TextView textView = new TextView(context);
            textView.setText(cVar.i());
            textView.setTextColor(cVar.g().getValue());
            textView.setTextSize(cVar.h() / u67.b.a());
            textView.setTypeface(cVar.j());
            textView.setSingleLine(true);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            t67 t67 = new t67(context, null);
            t67.D(t67, textView, cVar, layoutParams, 0, 8, null);
            return hl7.a(t67, new FrameLayout.LayoutParams(-2, -2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ gp7 b;

        @DexIgnore
        public b(gp7 gp7) {
            this.b = gp7;
        }

        @DexIgnore
        public final void run() {
            gp7 gp7 = this.b;
            if (gp7 != null) {
                tl7 tl7 = (tl7) gp7.invoke();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ t67 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements gp7<tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar) {
                super(0);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.gp7
            public final void invoke() {
                this.this$0.b.m();
            }
        }

        @DexIgnore
        public c(t67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            if (this.b.A()) {
                this.b.y(new a(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ t67 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements gp7<tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar) {
                super(0);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.gp7
            public final void invoke() {
                this.this$0.b.m();
            }
        }

        @DexIgnore
        public d(t67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            if (this.b.A()) {
                this.b.y(new a(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ t67 b;

        @DexIgnore
        public e(t67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            this.b.m();
        }
    }

    @DexIgnore
    public t67(Context context) {
        super(context);
    }

    @DexIgnore
    public /* synthetic */ t67(Context context, kq7 kq7) {
        this(context);
    }

    @DexIgnore
    public static /* synthetic */ t67 D(t67 t67, TextView textView, s87.c cVar, ViewGroup.LayoutParams layoutParams, int i, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            i = 0;
        }
        t67.C(textView, cVar, layoutParams, i);
        return t67;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.t67 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void z(t67 t67, gp7 gp7, int i, Object obj) {
        if ((i & 1) != 0) {
            gp7 = null;
        }
        t67.y(gp7);
    }

    @DexIgnore
    public final boolean A() {
        float width = (float) getWidth();
        float scaleX = getScaleX();
        float height = (float) getHeight();
        float scaleX2 = getScaleX();
        w67.a handler = getHandler();
        return handler != null && w67.a.C0267a.a(handler, width * scaleX, height * scaleX2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 4, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x007e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void B(java.lang.String r7, android.graphics.Typeface r8, com.fossil.o87 r9) {
        /*
            r6 = this;
            r2 = 1
            r3 = 0
            r1 = 0
            if (r7 == 0) goto L_0x0095
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = r0.i()
        L_0x000d:
            boolean r0 = com.fossil.pq7.a(r0, r7)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0095
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x0091
            r0.setText(r7)
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x0023
            r0.o(r7)
        L_0x0023:
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x008d
            com.fossil.t67$c r4 = new com.fossil.t67$c
            r4.<init>(r6)
            r0.post(r4)
            r5 = r2
        L_0x0030:
            if (r8 == 0) goto L_0x00a1
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x0097
            android.graphics.Typeface r0 = r0.j()
        L_0x003a:
            boolean r0 = com.fossil.pq7.a(r0, r8)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x00a1
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x009d
            r0.setTypeface(r8)
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x0050
            r0.p(r8)
        L_0x0050:
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x0099
            com.fossil.t67$d r4 = new com.fossil.t67$d
            r4.<init>(r6)
            r0.post(r4)
            r4 = r2
        L_0x005d:
            if (r9 == 0) goto L_0x00ad
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x00a3
            com.fossil.o87 r0 = r0.g()
        L_0x0067:
            if (r0 == r9) goto L_0x00a9
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x00a5
            int r1 = r9.getValue()
            r0.setTextColor(r1)
            com.fossil.s87$c r0 = r6.l
            if (r0 == 0) goto L_0x00ab
            r0.k(r9)
            r0 = r2
        L_0x007c:
            if (r5 != 0) goto L_0x0082
            if (r4 != 0) goto L_0x0082
            if (r0 == 0) goto L_0x008a
        L_0x0082:
            com.fossil.t67$e r0 = new com.fossil.t67$e
            r0.<init>(r6)
            r6.post(r0)
        L_0x008a:
            return
        L_0x008b:
            r0 = r1
            goto L_0x000d
        L_0x008d:
            com.fossil.pq7.i()
            throw r1
        L_0x0091:
            com.fossil.pq7.i()
            throw r1
        L_0x0095:
            r5 = r3
            goto L_0x0030
        L_0x0097:
            r0 = r1
            goto L_0x003a
        L_0x0099:
            com.fossil.pq7.i()
            throw r1
        L_0x009d:
            com.fossil.pq7.i()
            throw r1
        L_0x00a1:
            r4 = r3
            goto L_0x005d
        L_0x00a3:
            r0 = r1
            goto L_0x0067
        L_0x00a5:
            com.fossil.pq7.i()
            throw r1
        L_0x00a9:
            r0 = r3
            goto L_0x007c
        L_0x00ab:
            r0 = r2
            goto L_0x007c
        L_0x00ad:
            r0 = r3
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t67.B(java.lang.String, android.graphics.Typeface, com.fossil.o87):void");
    }

    @DexIgnore
    public final t67 C(TextView textView, s87.c cVar, ViewGroup.LayoutParams layoutParams, int i) {
        addView(textView, layoutParams);
        this.m = textView;
        cVar.m(i);
        cVar.n(i);
        this.l = cVar;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public s87 c(boolean z) {
        int i = 0;
        s87.c cVar = this.l;
        if (cVar != null) {
            cVar.d(getMetric());
        }
        if (z) {
            w67.a handler = getHandler();
            int c2 = handler != null ? handler.c() : 0;
            s87.c cVar2 = this.l;
            if (cVar2 != null) {
                if (cVar2 != null) {
                    i = cVar2.a();
                }
                cVar2.n(i);
            }
            s87.c cVar3 = this.l;
            if (cVar3 != null) {
                cVar3.m(c2);
            }
        }
        s87.c cVar4 = this.l;
        if (cVar4 != null) {
            return cVar4;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public w67.c getType() {
        return w67.c.TEXT;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public void o() {
        w67.a handler;
        if (getTouchCount$app_fossilRelease() > 1 && (handler = getHandler()) != null) {
            handler.m(this);
        }
    }

    @DexIgnore
    public final void y(gp7<tl7> gp7) {
        w67.a handler = getHandler();
        WatchFaceEditorView.a g = handler != null ? handler.g() : null;
        if (g != null) {
            float a2 = g.a();
            float max = (float) Math.max(getWidth(), getHeight());
            float scaleX = getScaleX();
            TextView textView = this.m;
            if (textView != null) {
                float textSize = ((a2 * 0.9f) / (max * scaleX)) * textView.getTextSize();
                TextView textView2 = this.m;
                if (textView2 != null) {
                    textView2.setTextSize(textSize / u67.b.a());
                    s87.c cVar = this.l;
                    if (cVar != null) {
                        cVar.l(textSize);
                        post(new b(gp7));
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }
}
