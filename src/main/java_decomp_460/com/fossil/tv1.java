package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tv1 extends gw1 {
    @DexIgnore
    public tv1(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    public tv1(byte[] bArr) {
        this(g80.e(0, 1), bArr);
    }

    @DexIgnore
    @Override // com.fossil.fw1, com.fossil.fw1, com.fossil.gw1, com.fossil.gw1, com.fossil.gw1, java.lang.Object
    public tv1 clone() {
        String name = getName();
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new tv1(name, copyOf);
    }
}
