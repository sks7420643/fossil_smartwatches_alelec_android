package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it2 implements ThreadFactory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ThreadFactory f1664a; // = Executors.defaultThreadFactory();

    @DexIgnore
    public it2(zs2 zs2) {
    }

    @DexIgnore
    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.f1664a.newThread(runnable);
        newThread.setName("ScionFrontendApi");
        return newThread;
    }
}
