package com.fossil;

import com.fossil.tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu5 extends tq4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return nu5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2574a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            pq7.c(str, "oldPass");
            pq7.c(str2, "newPass");
            this.f2574a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.f2574a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2575a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.f2575a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f2575a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements tq4.c {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends sq5<gj4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nu5 f2576a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(nu5 nu5) {
            this.f2576a = nu5;
        }

        @DexIgnore
        @Override // com.fossil.sq5
        public void a(Call<gj4> call, ServerError serverError) {
            pq7.c(call, "call");
            pq7.c(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = nu5.f.a();
            local.d(a2, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            tq4.d b = this.f2576a.b();
            Integer code = serverError.getCode();
            pq7.b(code, "response.code");
            b.a(new c(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        @Override // com.fossil.sq5
        public void b(Call<gj4> call, Throwable th) {
            pq7.c(call, "call");
            pq7.c(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = nu5.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(a2, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.f2576a.b().a(new c(MFNetworkReturnCode.CLIENT_TIMEOUT, null));
            } else {
                this.f2576a.b().a(new c(601, null));
            }
        }

        @DexIgnore
        @Override // com.fossil.sq5
        public void c(Call<gj4> call, q88<gj4> q88) {
            pq7.c(call, "call");
            pq7.c(q88, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = nu5.f.a();
            local.d(a2, "changePassword onSuccessResponse response=" + q88);
            this.f2576a.b().onSuccess(new d());
        }
    }

    /*
    static {
        String simpleName = nu5.class.getSimpleName();
        pq7.b(simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public nu5(AuthApiUserService authApiUserService) {
        pq7.c(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexIgnore
    /* renamed from: h */
    public void a(b bVar) {
        pq7.c(bVar, "requestValues");
        if (!o37.b(PortfolioApp.h0.c())) {
            b().a(new c(601, ""));
            return;
        }
        gj4 gj4 = new gj4();
        try {
            gj4.n("oldPassword", bVar.b());
            gj4.n("newPassword", bVar.a());
        } catch (Exception e2) {
        }
        this.d.changePassword(gj4).D(new e(this));
    }
}
