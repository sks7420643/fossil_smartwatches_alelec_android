package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu {
    @DexIgnore
    public /* synthetic */ gu(kq7 kq7) {
    }

    @DexIgnore
    public final hu a(byte b) {
        hu huVar;
        hu[] values = hu.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                huVar = null;
                break;
            }
            huVar = values[i];
            if (huVar.c == b) {
                break;
            }
            i++;
        }
        return huVar != null ? huVar : hu.UNKNOWN;
    }
}
