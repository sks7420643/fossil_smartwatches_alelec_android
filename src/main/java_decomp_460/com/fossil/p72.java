package com.fossil;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p72<L> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c f2792a;
    @DexIgnore
    public volatile L b;
    @DexIgnore
    public volatile a<L> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<L> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ L f2793a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(L l, String str) {
            this.f2793a = l;
            this.b = str;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.f2793a == aVar.f2793a && this.b.equals(aVar.b);
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.f2793a) * 31) + this.b.hashCode();
        }
    }

    @DexIgnore
    public interface b<L> {
        @DexIgnore
        void a(L l);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends ol2 {
        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            rc2.a(z);
            p72.this.d((b) message.obj);
        }
    }

    @DexIgnore
    public p72(Looper looper, L l, String str) {
        this.f2792a = new c(looper);
        rc2.l(l, "Listener must not be null");
        this.b = l;
        rc2.g(str);
        this.c = new a<>(l, str);
    }

    @DexIgnore
    public final void a() {
        this.b = null;
        this.c = null;
    }

    @DexIgnore
    public final a<L> b() {
        return this.c;
    }

    @DexIgnore
    public final void c(b<? super L> bVar) {
        rc2.l(bVar, "Notifier must not be null");
        this.f2792a.sendMessage(this.f2792a.obtainMessage(1, bVar));
    }

    @DexIgnore
    public final void d(b<? super L> bVar) {
        L l = this.b;
        if (l == null) {
            bVar.b();
            return;
        }
        try {
            bVar.a(l);
        } catch (RuntimeException e) {
            bVar.b();
            throw e;
        }
    }
}
