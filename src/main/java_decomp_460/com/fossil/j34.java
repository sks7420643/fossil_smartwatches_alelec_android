package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j34<E> extends m44<E> implements b54<E> {
    @DexIgnore
    public j34(m34<E> m34, y24<E> y24) {
        super(m34, y24);
    }

    @DexIgnore
    @Override // com.fossil.b54
    public Comparator<? super E> comparator() {
        return delegateCollection().comparator();
    }

    @DexIgnore
    @Override // com.fossil.s24, com.fossil.u24, com.fossil.y24
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    @Override // com.fossil.m44, com.fossil.s24
    public m34<E> delegateCollection() {
        return (m34) super.delegateCollection();
    }

    @DexIgnore
    @Override // com.fossil.y24
    public int indexOf(Object obj) {
        int indexOf = delegateCollection().indexOf(obj);
        if (indexOf < 0 || !get(indexOf).equals(obj)) {
            return -1;
        }
        return indexOf;
    }

    @DexIgnore
    @Override // com.fossil.y24
    public int lastIndexOf(Object obj) {
        return indexOf(obj);
    }

    @DexIgnore
    @Override // com.fossil.y24
    public y24<E> subListUnchecked(int i, int i2) {
        return new s44(super.subListUnchecked(i, i2), comparator()).asList();
    }
}
