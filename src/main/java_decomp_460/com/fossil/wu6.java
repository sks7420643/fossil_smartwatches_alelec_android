package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu6 extends pv5 implements vu6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public uu6 g;
    @DexIgnore
    public g37<bb5> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return wu6.j;
        }

        @DexIgnore
        public final wu6 b() {
            return new wu6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ wu6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ bb5 b;

            @DexIgnore
            public a(bb5 bb5) {
                this.b = bb5;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.b.K;
                pq7.b(flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.b.L;
                pq7.b(flexibleButton, "it.tvSignup");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, wu6 wu6) {
            this.b = view;
            this.c = wu6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                bb5 bb5 = (bb5) wu6.K6(this.c).a();
                if (bb5 != null) {
                    try {
                        FlexibleTextView flexibleTextView = bb5.K;
                        pq7.b(flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = bb5.L;
                        pq7.b(flexibleButton, "it.tvSignup");
                        flexibleButton.setVisibility(8);
                        tl7 tl7 = tl7.f3441a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = wu6.k.a();
                        local.d(a2, "onCreateView - e=" + e);
                        tl7 tl72 = tl7.f3441a;
                    }
                }
            } else {
                bb5 bb52 = (bb5) wu6.K6(this.c).a();
                if (bb52 != null) {
                    try {
                        pq7.b(bb52, "it");
                        bb52.n().postDelayed(new a(bb52), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = wu6.k.a();
                        local2.d(a3, "onCreateView - e=" + e2);
                        tl7 tl73 = tl7.f3441a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public c(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().u();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public d(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.F;
                pq7.b(activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public e(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextView.OnEditorActionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ wu6 f3999a;

        @DexIgnore
        public f(wu6 wu6) {
            this.f3999a = wu6;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(wu6.k.a(), "Password DONE key, trigger login flow");
            this.f3999a.O6();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ bb5 b;
        @DexIgnore
        public /* final */ /* synthetic */ wu6 c;

        @DexIgnore
        public g(bb5 bb5, wu6 wu6) {
            this.b = bb5;
            this.c = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.w;
            pq7.b(flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.b.v;
            pq7.b(flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.c.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public h(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.b.N6().w(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public i(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.N6().v(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public j(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.b.N6().x(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public k(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public l(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public m(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public n(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wu6 b;

        @DexIgnore
        public o(wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().t();
        }
    }

    /*
    static {
        String simpleName = wu6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "LoginFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(wu6 wu6) {
        g37<bb5> g37 = wu6.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void C(int i2, String str) {
        pq7.c(str, "errorMessage");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void H5(String str) {
        pq7.c(str, Constants.EMAIL);
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.B;
            pq7.b(context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    public final uu6 N6() {
        uu6 uu6 = this.g;
        if (uu6 != null) {
            return uu6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void O0() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        g37<bb5> g37 = this.h;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(false);
            }
            g37<bb5> g372 = this.h;
            if (g372 != null) {
                bb5 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void O4(boolean z) {
        g37<bb5> g37 = this.h;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ConstraintLayout constraintLayout = a2.D;
                pq7.b(constraintLayout, "it.ivWeibo");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.C;
                pq7.b(constraintLayout2, "it.ivWechat");
                constraintLayout2.setVisibility(0);
                ImageView imageView = a2.B;
                pq7.b(imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.A;
                pq7.b(imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            ConstraintLayout constraintLayout3 = a2.D;
            pq7.b(constraintLayout3, "it.ivWeibo");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.C;
            pq7.b(constraintLayout4, "it.ivWechat");
            constraintLayout4.setVisibility(8);
            ImageView imageView3 = a2.B;
            pq7.b(imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.A;
            pq7.b(imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        g37<bb5> g37 = this.h;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (a2 != null) {
                uu6 uu6 = this.g;
                if (uu6 != null) {
                    FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                    pq7.b(flexibleTextInputEditText, "etEmail");
                    String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                    FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                    pq7.b(flexibleTextInputEditText2, "etPassword");
                    uu6.q(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void P1(boolean z, String str) {
        FlexibleTextInputLayout flexibleTextInputLayout;
        pq7.c(str, "errorMessage");
        if (isActive()) {
            g37<bb5> g37 = this.h;
            if (g37 != null) {
                bb5 a2 = g37.a();
                if (a2 != null && (flexibleTextInputLayout = a2.v) != null) {
                    pq7.b(flexibleTextInputLayout, "it");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    flexibleTextInputLayout.setError(str);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(uu6 uu6) {
        pq7.c(uu6, "presenter");
        this.g = uu6;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void R() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = h37.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.E;
            pq7.b(activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void W1() {
        if (isActive()) {
            g37<bb5> g37 = this.h;
            if (g37 != null) {
                bb5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    pq7.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setError(" ");
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.w;
                    pq7.b(flexibleTextInputLayout2, "it.inputPassword");
                    flexibleTextInputLayout2.setError(um5.c(PortfolioApp.h0.c(), 2131886912));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void c3() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        g37<bb5> g37 = this.h;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(true);
            }
            g37<bb5> g372 = this.h;
            if (g372 != null) {
                bb5 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.d("flexible_button_primary");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void e5(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.B;
            pq7.b(activity, "it");
            aVar.b(activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void i() {
        String string = getString(2131886911);
        pq7.b(string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            pq7.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(j, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            uu6 uu6 = this.g;
            if (uu6 != null) {
                pq7.b(signUpSocialAuth, "authCode");
                uu6.p(signUpSocialAuth);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        bb5 bb5 = (bb5) aq0.f(layoutInflater, 2131558621, viewGroup, false, A6());
        pq7.b(bb5, "binding");
        View n2 = bb5.n();
        pq7.b(n2, "binding.root");
        n2.getViewTreeObserver().addOnGlobalLayoutListener(new b(n2, this));
        g37<bb5> g37 = new g37<>(this, bb5);
        this.h = g37;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        uu6 uu6 = this.g;
        if (uu6 != null) {
            uu6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        uu6 uu6 = this.g;
        if (uu6 != null) {
            uu6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<bb5> g37 = this.h;
        if (g37 != null) {
            bb5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new g(a2, this));
                a2.t.addTextChangedListener(new h(this));
                a2.t.setOnFocusChangeListener(new i(this));
                a2.u.addTextChangedListener(new j(this));
                a2.z.setOnClickListener(new k(this));
                a2.A.setOnClickListener(new l(this));
                a2.B.setOnClickListener(new m(this));
                a2.x.setOnClickListener(new n(this));
                a2.C.setOnClickListener(new o(this));
                a2.D.setOnClickListener(new c(this));
                a2.L.setOnClickListener(new d(this));
                a2.J.setOnClickListener(new e(this));
                a2.u.setOnEditorActionListener(new f(this));
            }
            E6("login_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
