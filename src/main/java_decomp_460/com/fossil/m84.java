package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m84 extends z84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ta4 f2314a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public m84(ta4 ta4, String str) {
        if (ta4 != null) {
            this.f2314a = ta4;
            if (str != null) {
                this.b = str;
                return;
            }
            throw new NullPointerException("Null sessionId");
        }
        throw new NullPointerException("Null report");
    }

    @DexIgnore
    @Override // com.fossil.z84
    public ta4 b() {
        return this.f2314a;
    }

    @DexIgnore
    @Override // com.fossil.z84
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z84)) {
            return false;
        }
        z84 z84 = (z84) obj;
        return this.f2314a.equals(z84.b()) && this.b.equals(z84.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f2314a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CrashlyticsReportWithSessionId{report=" + this.f2314a + ", sessionId=" + this.b + "}";
    }
}
