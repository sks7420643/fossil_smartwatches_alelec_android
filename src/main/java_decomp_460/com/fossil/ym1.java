package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ym1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ zm1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ym1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ym1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                zm1 valueOf = zm1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (z8.f4430a[valueOf.ordinal()]) {
                    case 1:
                        return nm1.CREATOR.b(parcel);
                    case 2:
                        return vm1.CREATOR.b(parcel);
                    case 3:
                        return wm1.CREATOR.b(parcel);
                    case 4:
                        return rm1.CREATOR.b(parcel);
                    case 5:
                        return sm1.CREATOR.b(parcel);
                    case 6:
                        return xm1.CREATOR.b(parcel);
                    case 7:
                        return qm1.CREATOR.b(parcel);
                    case 8:
                        return tm1.CREATOR.b(parcel);
                    case 9:
                        return en1.CREATOR.b(parcel);
                    case 10:
                        return hn1.CREATOR.b(parcel);
                    case 11:
                        return bn1.CREATOR.b(parcel);
                    case 12:
                        return gn1.CREATOR.b(parcel);
                    case 13:
                        return mm1.CREATOR.b(parcel);
                    case 14:
                        return cn1.CREATOR.b(parcel);
                    case 15:
                        return um1.CREATOR.b(parcel);
                    case 16:
                        return an1.CREATOR.b(parcel);
                    case 17:
                        return fn1.CREATOR.b(parcel);
                    case 18:
                        return om1.CREATOR.b(parcel);
                    case 19:
                        return dn1.CREATOR.b(parcel);
                    case 20:
                        return jn1.CREATOR.b(parcel);
                    case 21:
                        return pm1.CREATOR.b(parcel);
                    default:
                        throw new al7();
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ym1[] newArray(int i) {
            return new ym1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ym1(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0013
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.zm1 r0 = com.fossil.zm1.valueOf(r0)
            r2.<init>(r0)
            return
        L_0x0013:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ym1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ym1(zm1 zm1) {
        this.b = zm1;
    }

    @DexIgnore
    public final byte[] a() {
        ByteBuffer allocate = ByteBuffer.allocate(b().length + 3);
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b.a()).put((byte) b().length).array();
        pq7.b(array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        byte[] array2 = allocate.put(array).put(b()).array();
        pq7.b(array2, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array2;
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public abstract Object c();

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((ym1) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DeviceConfigItem");
    }

    @DexIgnore
    public final zm1 getKey() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public final JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.y0, this.b.b());
            g80.k(jSONObject, jd0.z0, c());
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }
}
