package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ mo1 b;
    @DexIgnore
    public /* final */ ev1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public fv1 createFromParcel(Parcel parcel) {
            mo1 mo1 = mo1.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(ev1.class.getClassLoader());
            if (readParcelable != null) {
                return new fv1(mo1, (ev1) readParcelable);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fv1[] newArray(int i) {
            return new fv1[i];
        }
    }

    @DexIgnore
    public fv1(mo1 mo1, ev1 ev1) {
        this.b = mo1;
        this.c = ev1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(fv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            fv1 fv1 = (fv1) obj;
            if (this.b != fv1.b) {
                return false;
            }
            return !(pq7.a(this.c, fv1.c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageMapping");
    }

    @DexIgnore
    public final mo1 getNotificationType() {
        return this.b;
    }

    @DexIgnore
    public final ev1 getReplyMessageGroup() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.e, ey1.a(this.b)), jd0.h5, this.c.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeParcelable(this.c, i);
    }
}
