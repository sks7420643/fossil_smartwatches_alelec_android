package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fx5;
import com.fossil.t47;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z06 extends qv5 implements y06, t47.g {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public x06 h;
    @DexIgnore
    public g37<x85> i;
    @DexIgnore
    public fx5 j;
    @DexIgnore
    public w16 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public a26 m;
    @DexIgnore
    public r16 s;
    @DexIgnore
    public po4 t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return z06.v;
        }

        @DexIgnore
        public final z06 b() {
            return new z06();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fx5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z06 f4401a;

        @DexIgnore
        public b(z06 z06) {
            this.f4401a = z06;
        }

        @DexIgnore
        @Override // com.fossil.fx5.a
        public void a(ContactGroup contactGroup) {
            pq7.c(contactGroup, "contactGroup");
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f4401a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.c0(childFragmentManager, contactGroup);
        }

        @DexIgnore
        @Override // com.fossil.fx5.a
        public void b() {
            NotificationContactsActivity.B.a(this.f4401a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public c(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.A;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z06 f4402a;

        @DexIgnore
        public d(z06 z06) {
            this.f4402a = z06;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (this.f4402a.h == null) {
                return;
            }
            if (!z) {
                z06.L6(this.f4402a).r(false);
            } else if (!this.f4402a.O6()) {
                z06.L6(this.f4402a).u(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public e(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(z06.w.a(), "press on button back");
            z06.L6(this.b).v(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public f(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            z06.L6(this.b).w(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public g(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            z06.L6(this.b).w(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public h(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public i(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.A;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z06 b;

        @DexIgnore
        public j(z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            z06.L6(this.b).n();
        }
    }

    /*
    static {
        String simpleName = z06.class.getSimpleName();
        pq7.b(simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ x06 L6(z06 z06) {
        x06 x06 = z06.h;
        if (x06 != null) {
            return x06;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void C2(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        x06 x06 = this.h;
        if (x06 != null) {
            x06.r(z);
            g37<x85> g37 = this.i;
            if (g37 != null) {
                x85 a2 = g37.a();
                if (a2 != null && (flexibleSwitchCompat = a2.F) != null) {
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void D4(List<ContactGroup> list) {
        pq7.c(list, "mListContactGroup");
        fx5 fx5 = this.j;
        if (fx5 != null) {
            fx5.n(list);
        } else {
            pq7.n("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void E2() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.t0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            g37<x85> g37 = this.i;
            if (g37 != null) {
                x85 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.y) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<x85> g372 = this.i;
        if (g372 != null) {
            x85 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.y) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d(v, "onActivityBackPressed -");
        x06 x06 = this.h;
        if (x06 != null) {
            x06.v(true);
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public int L1() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        CharSequence charSequence = null;
        g37<x85> g37 = this.i;
        if (g37 != null) {
            x85 a2 = g37.a();
            if (pq7.a((a2 == null || (flexibleTextView2 = a2.r) == null) ? null : flexibleTextView2.getText(), um5.c(PortfolioApp.h0.c(), 2131886089))) {
                return 0;
            }
            g37<x85> g372 = this.i;
            if (g372 != null) {
                x85 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView = a3.r) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return pq7.a(charSequence, um5.c(PortfolioApp.h0.c(), 2131886090)) ? 1 : 2;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public int O2() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        CharSequence charSequence = null;
        g37<x85> g37 = this.i;
        if (g37 != null) {
            x85 a2 = g37.a();
            if (pq7.a((a2 == null || (flexibleTextView2 = a2.s) == null) ? null : flexibleTextView2.getText(), um5.c(PortfolioApp.h0.c(), 2131886089))) {
                return 0;
            }
            g37<x85> g372 = this.i;
            if (g372 != null) {
                x85 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView = a3.s) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return pq7.a(charSequence, um5.c(PortfolioApp.h0.c(), 2131886090)) ? 1 : 2;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public boolean O5() {
        return this.l;
    }

    @DexIgnore
    public final boolean O6() {
        x06 x06 = this.h;
        if (x06 != null) {
            return x06.o();
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(x06 x06) {
        pq7.c(x06, "presenter");
        this.h = x06;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        ContactGroup contactGroup;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode != -4398250) {
                if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373) {
                    x06 x06 = this.h;
                    if (x06 != null) {
                        x06.p();
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("SET TO WATCH FAIL")) {
            } else {
                if (i2 == 2131363291) {
                    C2(false);
                } else if (i2 == 2131363373) {
                    x06 x062 = this.h;
                    if (x062 != null) {
                        x062.u(true);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i2 == 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (contactGroup = (ContactGroup) extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE")) != null) {
                x06 x063 = this.h;
                if (x063 != null) {
                    x063.q(contactGroup);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void V4() {
        w16 w16;
        FLogger.INSTANCE.getLocal().d(v, "showNotificationSettingDialog");
        if (isActive() && (w16 = this.k) != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            w16.show(childFragmentManager, w16.w.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void d4() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.clearFlags(16);
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public List<ContactGroup> f5() {
        fx5 fx5 = this.j;
        if (fx5 != null) {
            return fx5.j();
        }
        pq7.n("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void h3(ContactGroup contactGroup) {
        pq7.c(contactGroup, "contactGroup");
        fx5 fx5 = this.j;
        if (fx5 != null) {
            fx5.m(contactGroup);
        } else {
            pq7.n("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void j6() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.setFlags(16, 16);
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void m4(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "settingsTypeName");
        g37<x85> g37 = this.i;
        if (g37 != null) {
            x85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ro4 M = PortfolioApp.h0.c().M();
        w16 w16 = this.k;
        if (w16 != null) {
            M.U1(new y16(w16)).b(this);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
                po4 po4 = this.t;
                if (po4 != null) {
                    ts0 a2 = vs0.f(notificationCallsAndMessagesActivity, po4).a(r16.class);
                    pq7.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    r16 r16 = (r16) a2;
                    this.s = r16;
                    x06 x06 = this.h;
                    if (x06 == null) {
                        pq7.n("mPresenter");
                        throw null;
                    } else if (r16 != null) {
                        x06.t(r16);
                    } else {
                        pq7.n("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModelFactory");
                    throw null;
                }
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == 1) {
            this.l = true;
            if (i2 == 112) {
                x06 x06 = this.h;
                if (x06 != null) {
                    x06.u(false);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else {
                x06 x062 = this.h;
                if (x062 != null) {
                    x062.v(false);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == 2 && i2 == 111) {
            x06 x063 = this.h;
            if (x063 != null) {
                x063.s();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        x85 x85 = (x85) aq0.f(layoutInflater, 2131558589, viewGroup, false, A6());
        String d2 = qn5.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230985);
            if (drawable != null) {
                drawable.setTint(parseColor);
                x85.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        w16 w16 = (w16) getChildFragmentManager().Z(w16.w.a());
        this.k = w16;
        if (w16 == null) {
            this.k = w16.w.b();
        }
        x85.C.setOnClickListener(new c(this));
        x85.F.setOnCheckedChangeListener(new d(this));
        x85.x.setOnClickListener(new e(this));
        x85.A.setOnClickListener(new f(this));
        x85.B.setOnClickListener(new g(this));
        x85.q.setOnClickListener(new h(this));
        FlexibleSwitchCompat flexibleSwitchCompat = x85.F;
        pq7.b(flexibleSwitchCompat, "binding.swQrEnabled");
        flexibleSwitchCompat.setChecked(O6());
        x85.C.setOnClickListener(new i(this));
        x85.y.setOnClickListener(new j(this));
        fx5 fx5 = new fx5();
        fx5.o(new b(this));
        this.j = fx5;
        RecyclerView recyclerView = x85.E;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        fx5 fx52 = this.j;
        if (fx52 != null) {
            recyclerView.setAdapter(fx52);
            recyclerView.setHasFixedSize(true);
            ro4 M = PortfolioApp.h0.c().M();
            w16 w162 = this.k;
            if (w162 != null) {
                M.U1(new y16(w162)).b(this);
                this.i = new g37<>(this, x85);
                E6("call_message_view");
                pq7.b(x85, "binding");
                return x85.n();
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        pq7.n("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        x06 x06 = this.h;
        if (x06 != null) {
            x06.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FlexibleSwitchCompat flexibleSwitchCompat;
        super.onResume();
        x06 x06 = this.h;
        if (x06 != null) {
            if (x06 != null) {
                x06.l();
                g37<x85> g37 = this.i;
                if (g37 != null) {
                    x85 a2 = g37.a();
                    if (!(a2 == null || (flexibleSwitchCompat = a2.F) == null)) {
                        flexibleSwitchCompat.setChecked(O6());
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.y06
    public void w1(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "settingsTypeName");
        g37<x85> g37 = this.i;
        if (g37 != null) {
            x85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.s) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }
}
