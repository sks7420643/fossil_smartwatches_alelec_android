package com.fossil;

import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public TextView f4465a;
    @DexIgnore
    public TextClassifier b;

    @DexIgnore
    public zg0(TextView textView) {
        pn0.d(textView);
        this.f4465a = textView;
    }

    @DexIgnore
    public TextClassifier a() {
        TextClassifier textClassifier = this.b;
        if (textClassifier != null) {
            return textClassifier;
        }
        TextClassificationManager textClassificationManager = (TextClassificationManager) this.f4465a.getContext().getSystemService(TextClassificationManager.class);
        return textClassificationManager != null ? textClassificationManager.getTextClassifier() : TextClassifier.NO_OP;
    }

    @DexIgnore
    public void b(TextClassifier textClassifier) {
        this.b = textClassifier;
    }
}
