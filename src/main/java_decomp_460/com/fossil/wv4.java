package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f57.b f4000a;
    @DexIgnore
    public /* final */ uy4 b; // = uy4.d.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ne5 f4001a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ne5 ne5) {
            super(ne5.n());
            pq7.c(ne5, "binding");
            this.f4001a = ne5;
        }

        @DexIgnore
        public final void a(at4 at4, f57.b bVar, uy4 uy4) {
            pq7.c(at4, "player");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            ne5 ne5 = this.f4001a;
            String c = pq7.a(PortfolioApp.h0.c().l0(), at4.c()) ? um5.c(PortfolioApp.h0.c(), 2131886250) : hz4.f1561a.b(at4.b(), at4.d(), at4.e());
            FlexibleTextView flexibleTextView = ne5.v;
            pq7.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(c);
            ImageView imageView = ne5.r;
            pq7.b(imageView, "ivAvatar");
            ty4.b(imageView, at4.a(), at4.b(), bVar, uy4);
        }
    }

    @DexIgnore
    public wv4(int i) {
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.f4000a = f;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        return list.get(i) instanceof at4;
    }

    @DexIgnore
    public void b(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        a aVar = (a) (!(viewHolder instanceof a) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof at4) {
            obj = obj2;
        }
        at4 at4 = (at4) obj;
        if (aVar != null && at4 != null) {
            aVar.a(at4, this.f4000a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder c(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        ne5 z = ne5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemFriendListBinding.in\u2026(inflater, parent, false)");
        return new a(z);
    }
}
