package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qi2 implements Parcelable.Creator<wh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wh2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        Boolean bool = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str = ad2.f(parcel, t);
            } else if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                bool = ad2.n(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new wh2(str, i, bool);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wh2[] newArray(int i) {
        return new wh2[i];
    }
}
