package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class va7 extends FragmentStateAdapter {
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ String k;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public va7(Fragment fragment, String str, String str2) {
        super(fragment);
        pq7.c(fragment, "fragment");
        pq7.c(str, "watchFaceId");
        pq7.c(str2, "watchFaceIdType");
        this.j = str;
        this.k = str2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return 2;
    }

    @DexIgnore
    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment i(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("Brown", "createFragment position=" + i);
        if (i == 0) {
            return qa7.y.a(this.j, this.k);
        }
        if (i == 1) {
            return ua7.j.a();
        }
        throw new IllegalArgumentException("Unknown fragment");
    }
}
