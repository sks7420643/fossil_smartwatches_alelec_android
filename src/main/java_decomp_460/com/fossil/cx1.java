package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.fitness.AlgorithmManager;
import com.fossil.fitness.DataCaching;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static long f678a; // = 1800000;
    @DexIgnore
    public static String b; // = "";
    @DexIgnore
    public static String c;
    @DexIgnore
    public static ft1 d;
    @DexIgnore
    public static nm1 e;
    @DexIgnore
    public static /* final */ cx1 f; // = new cx1();

    @DexIgnore
    public final nm1 a() {
        return e;
    }

    @DexIgnore
    public final boolean b() {
        return id0.i.a() != null;
    }

    @DexIgnore
    public final String c() {
        return c;
    }

    @DexIgnore
    public final long d(lu1 lu1) {
        int i = kd0.f1901a[lu1.ordinal()];
        if (i == 1) {
            return d90.i.g();
        }
        if (i == 2) {
            return x80.i.g();
        }
        if (i == 3) {
            return y80.i.g();
        }
        if (i == 4) {
            return w80.i.g();
        }
        throw new al7();
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final long f() {
        return f678a;
    }

    @DexIgnore
    public final String g() {
        return null;
    }

    @DexIgnore
    public final String h() {
        return null;
    }

    @DexIgnore
    public final ft1 i() {
        return d;
    }

    @DexIgnore
    public final String j() {
        return id0.i.l();
    }

    @DexIgnore
    public final void k(Context context) throws IllegalArgumentException, IllegalStateException {
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            System.loadLibrary("FitnessAlgorithm");
            System.loadLibrary("EllipticCurveCrypto");
            System.loadLibrary("EInkImageFilter");
            id0.i.c(1);
            id0.i.b(context);
            m80.c.a("SDKManager", "init: sdkVersion=%s", "5.13.5-production-release");
            mx1.j.s(context);
            l80.d.f(context);
            tk1.k.f(context);
            File file = new File(context.getFilesDir(), "msl");
            file.mkdirs();
            if (file.exists()) {
                AlgorithmManager.defaultManager().setDataCaching(new DataCaching(file.getAbsolutePath(), new byte[0]));
            }
            AlgorithmManager defaultManager = AlgorithmManager.defaultManager();
            pq7.b(defaultManager, "AlgorithmManager.defaultManager()");
            defaultManager.setDebugEnabled(false);
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void l(String str) {
        c = str;
    }

    @DexIgnore
    public final void m(String str) {
        b = str;
    }

    @DexIgnore
    public final void n(zw1 zw1) {
        id0.i.f(zw1);
    }

    @DexIgnore
    public final void o(ft1 ft1) {
        d = ft1;
    }

    @DexIgnore
    public final void p(String str) {
        id0.i.d(str);
        c90.e.a().c = str;
    }
}
