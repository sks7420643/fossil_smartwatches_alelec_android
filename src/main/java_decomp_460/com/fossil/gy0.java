package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class gy0 extends ViewGroup implements dy0 {
    @DexIgnore
    public ViewGroup b;
    @DexIgnore
    public View c;
    @DexIgnore
    public /* final */ View d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Matrix f;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener g; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            View view;
            mo0.b0(gy0.this);
            gy0 gy0 = gy0.this;
            ViewGroup viewGroup = gy0.b;
            if (viewGroup == null || (view = gy0.c) == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            mo0.b0(gy0.this.b);
            gy0 gy02 = gy0.this;
            gy02.b = null;
            gy02.c = null;
            return true;
        }
    }

    @DexIgnore
    public gy0(View view) {
        super(view.getContext());
        this.d = view;
        setWillNotDraw(false);
        setLayerType(2, null);
    }

    @DexIgnore
    public static gy0 b(View view, ViewGroup viewGroup, Matrix matrix) {
        int i;
        gy0 gy0;
        ey0 ey0;
        ey0 ey02;
        if (view.getParent() instanceof ViewGroup) {
            ey0 b2 = ey0.b(viewGroup);
            gy0 e2 = e(view);
            if (e2 == null || (ey02 = (ey0) e2.getParent()) == b2) {
                i = 0;
                gy0 = e2;
            } else {
                int i2 = e2.e;
                ey02.removeView(e2);
                gy0 = null;
                i = i2;
            }
            if (gy0 == null) {
                if (matrix == null) {
                    matrix = new Matrix();
                    c(view, viewGroup, matrix);
                }
                gy0 gy02 = new gy0(view);
                gy02.h(matrix);
                if (b2 == null) {
                    ey0 = new ey0(viewGroup);
                } else {
                    b2.g();
                    ey0 = b2;
                }
                d(viewGroup, ey0);
                d(viewGroup, gy02);
                ey0.a(gy02);
                gy02.e = i;
                gy0 = gy02;
            } else if (matrix != null) {
                gy0.h(matrix);
            }
            gy0.e++;
            return gy0;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }

    @DexIgnore
    public static void c(View view, ViewGroup viewGroup, Matrix matrix) {
        ViewGroup viewGroup2 = (ViewGroup) view.getParent();
        matrix.reset();
        hz0.j(viewGroup2, matrix);
        matrix.preTranslate((float) (-viewGroup2.getScrollX()), (float) (-viewGroup2.getScrollY()));
        hz0.k(viewGroup, matrix);
    }

    @DexIgnore
    public static void d(View view, View view2) {
        hz0.g(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }

    @DexIgnore
    public static gy0 e(View view) {
        return (gy0) view.getTag(ny0.ghost_view);
    }

    @DexIgnore
    public static void f(View view) {
        gy0 e2 = e(view);
        if (e2 != null) {
            int i = e2.e - 1;
            e2.e = i;
            if (i <= 0) {
                ((ey0) e2.getParent()).removeView(e2);
            }
        }
    }

    @DexIgnore
    public static void g(View view, gy0 gy0) {
        view.setTag(ny0.ghost_view, gy0);
    }

    @DexIgnore
    @Override // com.fossil.dy0
    public void a(ViewGroup viewGroup, View view) {
        this.b = viewGroup;
        this.c = view;
    }

    @DexIgnore
    public void h(Matrix matrix) {
        this.f = matrix;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        g(this.d, this);
        this.d.getViewTreeObserver().addOnPreDrawListener(this.g);
        hz0.i(this.d, 4);
        if (this.d.getParent() != null) {
            ((View) this.d.getParent()).invalidate();
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.d.getViewTreeObserver().removeOnPreDrawListener(this.g);
        hz0.i(this.d, 0);
        g(this.d, null);
        if (this.d.getParent() != null) {
            ((View) this.d.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        zx0.a(canvas, true);
        canvas.setMatrix(this.f);
        hz0.i(this.d, 0);
        this.d.invalidate();
        hz0.i(this.d, 4);
        drawChild(canvas, this.d, getDrawingTime());
        zx0.a(canvas, false);
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.dy0
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (e(this.d) == this) {
            hz0.i(this.d, i == 0 ? 4 : 0);
        }
    }
}
