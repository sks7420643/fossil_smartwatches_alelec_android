package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m35 extends l35 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        A.put(2131362666, 2);
        A.put(2131362783, 3);
        A.put(2131363169, 4);
        A.put(2131362982, 5);
        A.put(2131362425, 6);
        A.put(2131362428, 7);
    }
    */

    @DexIgnore
    public m35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public m35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[7], (RTLImageView) objArr[2], (View) objArr[3], (RecyclerView) objArr[5], (ConstraintLayout) objArr[0], (SwipeRefreshLayout) objArr[4], (FlexibleTextView) objArr[1]);
        this.y = -1;
        this.v.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
