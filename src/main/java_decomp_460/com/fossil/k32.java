package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k32 implements Factory<j32> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<t32> f1859a;
    @DexIgnore
    public /* final */ Provider<t32> b;
    @DexIgnore
    public /* final */ Provider<l22> c;
    @DexIgnore
    public /* final */ Provider<p32> d;

    @DexIgnore
    public k32(Provider<t32> provider, Provider<t32> provider2, Provider<l22> provider3, Provider<p32> provider4) {
        this.f1859a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static k32 a(Provider<t32> provider, Provider<t32> provider2, Provider<l22> provider3, Provider<p32> provider4) {
        return new k32(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    /* renamed from: b */
    public j32 get() {
        return new j32(this.f1859a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
