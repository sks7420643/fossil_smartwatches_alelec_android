package com.fossil;

import com.facebook.internal.FileLruCache;
import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn7<T> extends zl7<T> implements RandomAccess {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ Object[] f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends yl7<T> {
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public /* final */ /* synthetic */ bn7 f;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(bn7 bn7) {
            this.f = bn7;
            this.d = bn7.size();
            this.e = bn7.d;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.bn7$a */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.yl7
        public void a() {
            if (this.d == 0) {
                b();
                return;
            }
            c(this.f.f[this.e]);
            this.e = (this.e + 1) % this.f.c;
            this.d--;
        }
    }

    @DexIgnore
    public bn7(int i) {
        this(new Object[i], 0);
    }

    @DexIgnore
    public bn7(Object[] objArr, int i) {
        boolean z = true;
        pq7.c(objArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        this.f = objArr;
        if (i >= 0) {
            if (i > this.f.length ? false : z) {
                this.c = this.f.length;
                this.e = i;
                return;
            }
            throw new IllegalArgumentException(("ring buffer filled size: " + i + " cannot be larger than the buffer size: " + this.f.length).toString());
        }
        throw new IllegalArgumentException(("ring buffer filled size should not be negative but it is " + i).toString());
    }

    @DexIgnore
    @Override // com.fossil.wl7
    public int a() {
        return this.e;
    }

    @DexIgnore
    public final void f(T t) {
        if (!h()) {
            this.f[(this.d + size()) % this.c] = t;
            this.e = size() + 1;
            return;
        }
        throw new IllegalStateException("ring buffer is full");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.bn7<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final bn7<T> g(int i) {
        Object[] array;
        int i2 = this.c;
        int g = bs7.g(i2 + (i2 >> 1) + 1, i);
        if (this.d == 0) {
            array = Arrays.copyOf(this.f, g);
            pq7.b(array, "java.util.Arrays.copyOf(this, newSize)");
        } else {
            array = toArray(new Object[g]);
        }
        return new bn7<>(array, size());
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.zl7
    public T get(int i) {
        zl7.b.a(i, size());
        return (T) this.f[(this.d + i) % this.c];
    }

    @DexIgnore
    public final boolean h() {
        return size() == this.c;
    }

    @DexIgnore
    public final void i(int i) {
        boolean z = true;
        if (i >= 0) {
            if (i > size()) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(("n shouldn't be greater than the buffer size: n = " + i + ", size = " + size()).toString());
            } else if (i > 0) {
                int i2 = this.d;
                int i3 = (i2 + i) % this.c;
                if (i2 > i3) {
                    dm7.m(this.f, null, i2, this.c);
                    dm7.m(this.f, null, 0, i3);
                } else {
                    dm7.m(this.f, null, i2, i3);
                }
                this.d = i3;
                this.e = size() - i;
            }
        } else {
            throw new IllegalArgumentException(("n shouldn't be negative but it is " + i).toString());
        }
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, com.fossil.zl7, java.lang.Iterable
    public Iterator<T> iterator() {
        return new a(this);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.bn7<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.wl7
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v6, resolved type: T[] */
    /* JADX DEBUG: Multi-variable search result rejected for r6v7, resolved type: T[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.List, com.fossil.wl7, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        int i = 0;
        pq7.c(tArr, "array");
        if (tArr.length < size()) {
            tArr = (T[]) Arrays.copyOf(tArr, size());
            pq7.b(tArr, "java.util.Arrays.copyOf(this, newSize)");
        }
        int size = size();
        int i2 = this.d;
        int i3 = 0;
        while (i3 < size && i2 < this.c) {
            tArr[i3] = this.f[i2];
            i3++;
            i2++;
        }
        while (i3 < size) {
            tArr[i3] = this.f[i];
            i3++;
            i++;
        }
        if (tArr.length > size()) {
            tArr[size()] = null;
        }
        if (tArr != null) {
            return tArr;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
