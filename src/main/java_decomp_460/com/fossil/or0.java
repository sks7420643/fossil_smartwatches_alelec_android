package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super sr0>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $source;
        @DexIgnore
        public /* final */ /* synthetic */ js0 $this_addDisposableSource;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.or0$a$a")
        /* renamed from: com.fossil.or0$a$a */
        public static final class C0180a<T> implements ls0<S> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ a f2711a;

            @DexIgnore
            public C0180a(a aVar) {
                this.f2711a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ls0
            public final void onChanged(T t) {
                this.f2711a.$this_addDisposableSource.o(t);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(js0 js0, LiveData liveData, qn7 qn7) {
            super(2, qn7);
            this.$this_addDisposableSource = js0;
            this.$source = liveData;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$this_addDisposableSource, this.$source, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super sr0> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.$this_addDisposableSource.p(this.$source, new C0180a(this));
                return new sr0(this.$source, this.$this_addDisposableSource);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public static final <T> Object a(js0<T> js0, LiveData<T> liveData, qn7<? super sr0> qn7) {
        return eu7.g(bw7.c().S(), new a(js0, liveData, null), qn7);
    }

    @DexIgnore
    public static final <T> LiveData<T> b(tn7 tn7, long j, vp7<? super hs0<T>, ? super qn7<? super tl7>, ? extends Object> vp7) {
        pq7.c(tn7, "context");
        pq7.c(vp7, "block");
        return new nr0(tn7, j, vp7);
    }

    @DexIgnore
    public static /* synthetic */ LiveData c(tn7 tn7, long j, vp7 vp7, int i, Object obj) {
        if ((i & 1) != 0) {
            tn7 = un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            j = 5000;
        }
        return b(tn7, j, vp7);
    }
}
