package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j73 implements xw2<m73> {
    @DexIgnore
    public static j73 c; // = new j73();
    @DexIgnore
    public /* final */ xw2<m73> b;

    @DexIgnore
    public j73() {
        this(ww2.b(new l73()));
    }

    @DexIgnore
    public j73(xw2<m73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((m73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((m73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((m73) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((m73) c.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ m73 zza() {
        return this.b.zza();
    }
}
