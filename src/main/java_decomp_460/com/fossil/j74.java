package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j74 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ a74<?> f1722a;
        @DexIgnore
        public /* final */ Set<b> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<b> c; // = new HashSet();

        @DexIgnore
        public b(a74<?> a74) {
            this.f1722a = a74;
        }

        @DexIgnore
        public void a(b bVar) {
            this.b.add(bVar);
        }

        @DexIgnore
        public void b(b bVar) {
            this.c.add(bVar);
        }

        @DexIgnore
        public a74<?> c() {
            return this.f1722a;
        }

        @DexIgnore
        public Set<b> d() {
            return this.b;
        }

        @DexIgnore
        public boolean e() {
            return this.b.isEmpty();
        }

        @DexIgnore
        public boolean f() {
            return this.c.isEmpty();
        }

        @DexIgnore
        public void g(b bVar) {
            this.c.remove(bVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<?> f1723a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public c(Class<?> cls, boolean z) {
            this.f1723a = cls;
            this.b = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return cVar.f1723a.equals(this.f1723a) && cVar.b == this.b;
        }

        @DexIgnore
        public int hashCode() {
            return ((this.f1723a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.b).hashCode();
        }
    }

    @DexIgnore
    public static void a(List<a74<?>> list) {
        Set<b> c2 = c(list);
        Set<b> b2 = b(c2);
        int i = 0;
        while (!b2.isEmpty()) {
            b next = b2.iterator().next();
            b2.remove(next);
            int i2 = i + 1;
            for (b bVar : next.d()) {
                bVar.g(next);
                if (bVar.f()) {
                    b2.add(bVar);
                }
            }
            i = i2;
        }
        if (i != list.size()) {
            ArrayList arrayList = new ArrayList();
            for (b bVar2 : c2) {
                if (!bVar2.f() && !bVar2.e()) {
                    arrayList.add(bVar2.c());
                }
            }
            throw new l74(arrayList);
        }
    }

    @DexIgnore
    public static Set<b> b(Set<b> set) {
        HashSet hashSet = new HashSet();
        for (b bVar : set) {
            if (bVar.f()) {
                hashSet.add(bVar);
            }
        }
        return hashSet;
    }

    @DexIgnore
    public static Set<b> c(List<a74<?>> list) {
        Set<b> set;
        HashMap hashMap = new HashMap(list.size());
        for (a74<?> a74 : list) {
            b bVar = new b(a74);
            Iterator<Class<? super Object>> it = a74.e().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class<? super Object> next = it.next();
                    c cVar = new c(next, !a74.k());
                    if (!hashMap.containsKey(cVar)) {
                        hashMap.put(cVar, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(cVar);
                    if (set2.isEmpty() || cVar.b) {
                        set2.add(bVar);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", next));
                    }
                }
            }
        }
        for (Set<b> set3 : hashMap.values()) {
            for (b bVar2 : set3) {
                for (k74 k74 : bVar2.c().c()) {
                    if (k74.b() && (set = (Set) hashMap.get(new c(k74.a(), k74.d()))) != null) {
                        for (b bVar3 : set) {
                            bVar2.a(bVar3);
                            bVar3.b(bVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set set4 : hashMap.values()) {
            hashSet.addAll(set4);
        }
        return hashSet;
    }
}
