package com.fossil;

import com.fossil.t62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z72 implements t62.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ BasePendingResult f4429a;
    @DexIgnore
    public /* final */ /* synthetic */ a82 b;

    @DexIgnore
    public z72(a82 a82, BasePendingResult basePendingResult) {
        this.b = a82;
        this.f4429a = basePendingResult;
    }

    @DexIgnore
    @Override // com.fossil.t62.a
    public final void a(Status status) {
        a82.a(this.b).remove(this.f4429a);
    }
}
