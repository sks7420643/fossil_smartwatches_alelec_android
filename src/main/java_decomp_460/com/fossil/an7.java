package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class an7 extends zm7 {
    @DexIgnore
    public static final <K, V> List<cl7<K, V>> q(Map<? extends K, ? extends V> map) {
        pq7.c(map, "$this$toList");
        if (map.size() == 0) {
            return hm7.e();
        }
        Iterator<Map.Entry<? extends K, ? extends V>> it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return hm7.e();
        }
        Map.Entry<? extends K, ? extends V> next = it.next();
        if (!it.hasNext()) {
            return gm7.b(new cl7(next.getKey(), next.getValue()));
        }
        ArrayList arrayList = new ArrayList(map.size());
        arrayList.add(new cl7(next.getKey(), next.getValue()));
        do {
            Map.Entry<? extends K, ? extends V> next2 = it.next();
            arrayList.add(new cl7(next2.getKey(), next2.getValue()));
        } while (it.hasNext());
        return arrayList;
    }
}
