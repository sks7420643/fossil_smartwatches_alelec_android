package com.fossil;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j56 implements Factory<i56> {
    @DexIgnore
    public static i56 a(d56 d56, int i, ArrayList<String> arrayList, uq4 uq4, l56 l56) {
        return new i56(d56, i, arrayList, uq4, l56);
    }
}
