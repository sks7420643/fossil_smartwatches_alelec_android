package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq4 implements MembersInjector<PortfolioApp> {
    @DexIgnore
    public static void A(PortfolioApp portfolioApp, WatchLocalizationRepository watchLocalizationRepository) {
        portfolioApp.z = watchLocalizationRepository;
    }

    @DexIgnore
    public static void B(PortfolioApp portfolioApp, fs5 fs5) {
        portfolioApp.G = fs5;
    }

    @DexIgnore
    public static void C(PortfolioApp portfolioApp, WorkoutSettingRepository workoutSettingRepository) {
        portfolioApp.E = workoutSettingRepository;
    }

    @DexIgnore
    public static void D(PortfolioApp portfolioApp, on5 on5) {
        portfolioApp.d = on5;
    }

    @DexIgnore
    public static void E(PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        portfolioApp.C = watchFaceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, yr4 yr4) {
        portfolioApp.Z = yr4;
    }

    @DexIgnore
    public static void b(PortfolioApp portfolioApp, bk5 bk5) {
        portfolioApp.g = bk5;
    }

    @DexIgnore
    public static void c(PortfolioApp portfolioApp, ck5 ck5) {
        portfolioApp.s = ck5;
    }

    @DexIgnore
    public static void d(PortfolioApp portfolioApp, ApiServiceV2 apiServiceV2) {
        portfolioApp.j = apiServiceV2;
    }

    @DexIgnore
    public static void e(PortfolioApp portfolioApp, ApplicationEventListener applicationEventListener) {
        portfolioApp.t = applicationEventListener;
    }

    @DexIgnore
    public static void f(PortfolioApp portfolioApp, no4 no4) {
        portfolioApp.i = no4;
    }

    @DexIgnore
    public static void g(PortfolioApp portfolioApp, l37 l37) {
        portfolioApp.k = l37;
    }

    @DexIgnore
    public static void h(PortfolioApp portfolioApp, ic7 ic7) {
        portfolioApp.y = ic7;
    }

    @DexIgnore
    public static void i(PortfolioApp portfolioApp, bo5 bo5) {
        portfolioApp.A = bo5;
    }

    @DexIgnore
    public static void j(PortfolioApp portfolioApp, q27 q27) {
        portfolioApp.X = q27;
    }

    @DexIgnore
    public static void k(PortfolioApp portfolioApp, zu5 zu5) {
        portfolioApp.m = zu5;
    }

    @DexIgnore
    public static void l(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        portfolioApp.u = deviceRepository;
    }

    @DexIgnore
    public static void m(PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository) {
        portfolioApp.x = dianaPresetRepository;
    }

    @DexIgnore
    public static void n(PortfolioApp portfolioApp, sk5 sk5) {
        portfolioApp.v = sk5;
    }

    @DexIgnore
    public static void o(PortfolioApp portfolioApp, t27 t27) {
        portfolioApp.Y = t27;
    }

    @DexIgnore
    public static void p(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        portfolioApp.h = guestApiService;
    }

    @DexIgnore
    public static void q(PortfolioApp portfolioApp, dq5 dq5) {
        portfolioApp.V = dq5;
    }

    @DexIgnore
    public static void r(PortfolioApp portfolioApp, aq5 aq5) {
        portfolioApp.U = aq5;
    }

    @DexIgnore
    public static void s(PortfolioApp portfolioApp, QuickResponseRepository quickResponseRepository) {
        portfolioApp.D = quickResponseRepository;
    }

    @DexIgnore
    public static void t(PortfolioApp portfolioApp, br5 br5) {
        portfolioApp.w = br5;
    }

    @DexIgnore
    public static void u(PortfolioApp portfolioApp, SleepSummariesRepository sleepSummariesRepository) {
        portfolioApp.f = sleepSummariesRepository;
    }

    @DexIgnore
    public static void v(PortfolioApp portfolioApp, SummariesRepository summariesRepository) {
        portfolioApp.e = summariesRepository;
    }

    @DexIgnore
    public static void w(PortfolioApp portfolioApp, ThemeRepository themeRepository) {
        portfolioApp.W = themeRepository;
    }

    @DexIgnore
    public static void x(PortfolioApp portfolioApp, uq4 uq4) {
        portfolioApp.l = uq4;
    }

    @DexIgnore
    public static void y(PortfolioApp portfolioApp, UserRepository userRepository) {
        portfolioApp.B = userRepository;
    }

    @DexIgnore
    public static void z(PortfolioApp portfolioApp, WatchAppDataRepository watchAppDataRepository) {
        portfolioApp.F = watchAppDataRepository;
    }
}
