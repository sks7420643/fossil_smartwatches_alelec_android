package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab3 implements Parcelable.Creator<za3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ za3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                z = ad2.m(parcel, t);
            } else if (l == 2) {
                j = ad2.y(parcel, t);
            } else if (l == 3) {
                f = ad2.r(parcel, t);
            } else if (l == 4) {
                j2 = ad2.y(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                i = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new za3(z, j, f, j2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ za3[] newArray(int i) {
        return new za3[i];
    }
}
