package com.fossil;

import com.fossil.a18;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k88<T> implements Call<T> {
    @DexIgnore
    public /* final */ p88 b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ a18.a d;
    @DexIgnore
    public /* final */ e88<w18, T> e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public a18 g;
    @DexIgnore
    public Throwable h;
    @DexIgnore
    public boolean i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b18 {
        @DexIgnore
        public /* final */ /* synthetic */ c88 b;

        @DexIgnore
        public a(c88 c88) {
            this.b = c88;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.b.onFailure(k88.this, th);
            } catch (Throwable th2) {
                u88.t(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        @Override // com.fossil.b18
        public void onFailure(a18 a18, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        @Override // com.fossil.b18
        public void onResponse(a18 a18, Response response) {
            try {
                try {
                    this.b.onResponse(k88.this, k88.this.g(response));
                } catch (Throwable th) {
                    u88.t(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                u88.t(th2);
                a(th2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends w18 {
        @DexIgnore
        public /* final */ w18 b;
        @DexIgnore
        public /* final */ k48 c;
        @DexIgnore
        public IOException d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends n48 {
            @DexIgnore
            public a(c58 c58) {
                super(c58);
            }

            @DexIgnore
            @Override // com.fossil.n48, com.fossil.c58
            public long d0(i48 i48, long j) throws IOException {
                try {
                    return super.d0(i48, j);
                } catch (IOException e) {
                    b.this.d = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public b(w18 w18) {
            this.b = w18;
            this.c = s48.d(new a(w18.source()));
        }

        @DexIgnore
        public void a() throws IOException {
            IOException iOException = this.d;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        @Override // com.fossil.w18, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.b.close();
        }

        @DexIgnore
        @Override // com.fossil.w18
        public long contentLength() {
            return this.b.contentLength();
        }

        @DexIgnore
        @Override // com.fossil.w18
        public r18 contentType() {
            return this.b.contentType();
        }

        @DexIgnore
        @Override // com.fossil.w18
        public k48 source() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends w18 {
        @DexIgnore
        public /* final */ r18 b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public c(r18 r18, long j) {
            this.b = r18;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public long contentLength() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public r18 contentType() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.w18
        public k48 source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public k88(p88 p88, Object[] objArr, a18.a aVar, e88<w18, T> e88) {
        this.b = p88;
        this.c = objArr;
        this.d = aVar;
        this.e = e88;
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void D(c88<T> c88) {
        Throwable th;
        a18 a18;
        u88.b(c88, "callback == null");
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                a18 a182 = this.g;
                th = this.h;
                if (a182 == null && th == null) {
                    try {
                        a18 = e();
                        this.g = a18;
                    } catch (Throwable th2) {
                        th = th2;
                        u88.t(th);
                        this.h = th;
                        a18 = a182;
                    }
                } else {
                    a18 = a182;
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            c88.onFailure(this, th);
            return;
        }
        if (this.f) {
            a18.cancel();
        }
        a18.m(new a(c88));
    }

    @DexIgnore
    @Override // retrofit2.Call
    public q88<T> a() throws IOException {
        a18 a18;
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                if (this.h == null) {
                    a18 = this.g;
                    if (a18 == null) {
                        try {
                            a18 = e();
                            this.g = a18;
                        } catch (IOException | Error | RuntimeException e2) {
                            u88.t(e2);
                            this.h = e2;
                            throw e2;
                        }
                    }
                } else if (this.h instanceof IOException) {
                    throw ((IOException) this.h);
                } else if (this.h instanceof RuntimeException) {
                    throw ((RuntimeException) this.h);
                } else {
                    throw ((Error) this.h);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.f) {
            a18.cancel();
        }
        return g(a18.a());
    }

    @DexIgnore
    @Override // retrofit2.Call
    public v18 c() {
        v18 c2;
        synchronized (this) {
            a18 a18 = this.g;
            if (a18 != null) {
                c2 = a18.c();
            } else if (this.h == null) {
                try {
                    a18 e2 = e();
                    this.g = e2;
                    c2 = e2.c();
                } catch (Error | RuntimeException e3) {
                    u88.t(e3);
                    this.h = e3;
                    throw e3;
                } catch (IOException e4) {
                    this.h = e4;
                    throw new RuntimeException("Unable to create request.", e4);
                }
            } else if (this.h instanceof IOException) {
                throw new RuntimeException("Unable to create request.", this.h);
            } else if (this.h instanceof RuntimeException) {
                throw ((RuntimeException) this.h);
            } else {
                throw ((Error) this.h);
            }
        }
        return c2;
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void cancel() {
        a18 a18;
        this.f = true;
        synchronized (this) {
            a18 = this.g;
        }
        if (a18 != null) {
            a18.cancel();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public k88<T> clone() {
        return new k88<>(this.b, this.c, this.d, this.e);
    }

    @DexIgnore
    public final a18 e() throws IOException {
        a18 d2 = this.d.d(this.b.a(this.c));
        if (d2 != null) {
            return d2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    @Override // retrofit2.Call
    public boolean f() {
        boolean z = true;
        if (!this.f) {
            synchronized (this) {
                if (this.g == null || !this.g.f()) {
                    z = false;
                }
            }
        }
        return z;
    }

    @DexIgnore
    public q88<T> g(Response response) throws IOException {
        w18 a2 = response.a();
        Response.a B = response.B();
        B.b(new c(a2.contentType(), a2.contentLength()));
        Response c2 = B.c();
        int f2 = c2.f();
        if (f2 < 200 || f2 >= 300) {
            try {
                return q88.c(u88.a(a2), c2);
            } finally {
                a2.close();
            }
        } else if (f2 == 204 || f2 == 205) {
            a2.close();
            return q88.h(null, c2);
        } else {
            b bVar = new b(a2);
            try {
                return q88.h(this.e.a(bVar), c2);
            } catch (RuntimeException e2) {
                bVar.a();
                throw e2;
            }
        }
    }
}
