package com.fossil;

import java.util.Comparator;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c54 {
    @DexIgnore
    public static <E> Comparator<? super E> a(SortedSet<E> sortedSet) {
        Comparator<? super E> comparator = sortedSet.comparator();
        return comparator == null ? i44.natural() : comparator;
    }

    @DexIgnore
    public static boolean b(Comparator<?> comparator, Iterable<?> iterable) {
        Comparator comparator2;
        i14.l(comparator);
        i14.l(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = a((SortedSet) iterable);
        } else if (!(iterable instanceof b54)) {
            return false;
        } else {
            comparator2 = ((b54) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}
