package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y52 implements ServiceConnection {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f4244a; // = false;
    @DexIgnore
    public /* final */ BlockingQueue<IBinder> b; // = new LinkedBlockingQueue();

    @DexIgnore
    public IBinder a(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        rc2.j("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (!this.f4244a) {
            this.f4244a = true;
            IBinder poll = this.b.poll(j, timeUnit);
            if (poll != null) {
                return poll;
            }
            throw new TimeoutException("Timed out waiting for the service connection");
        }
        throw new IllegalStateException("Cannot call get on this connection more than once");
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.b.add(iBinder);
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
    }
}
