package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u43 implements xw2<t43> {
    @DexIgnore
    public static u43 c; // = new u43();
    @DexIgnore
    public /* final */ xw2<t43> b;

    @DexIgnore
    public u43() {
        this(ww2.b(new w43()));
    }

    @DexIgnore
    public u43(xw2<t43> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((t43) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ t43 zza() {
        return this.b.zza();
    }
}
