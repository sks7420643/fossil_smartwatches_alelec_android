package com.fossil;

import androidx.loader.app.LoaderManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r56 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ p56 f3082a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<j06> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public r56(p56 p56, int i, ArrayList<j06> arrayList, LoaderManager loaderManager) {
        pq7.c(p56, "mView");
        pq7.c(loaderManager, "mLoaderManager");
        this.f3082a = p56;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<j06> a() {
        ArrayList<j06> arrayList = this.c;
        return arrayList != null ? arrayList : new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final p56 d() {
        return this.f3082a;
    }
}
