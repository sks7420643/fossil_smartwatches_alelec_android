package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx4 implements Factory<ax4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f522a;
    @DexIgnore
    public /* final */ Provider<hu4> b;

    @DexIgnore
    public bx4(Provider<on5> provider, Provider<hu4> provider2) {
        this.f522a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static bx4 a(Provider<on5> provider, Provider<hu4> provider2) {
        return new bx4(provider, provider2);
    }

    @DexIgnore
    public static ax4 c(on5 on5, hu4 hu4) {
        return new ax4(on5, hu4);
    }

    @DexIgnore
    /* renamed from: b */
    public ax4 get() {
        return c(this.f522a.get(), this.b.get());
    }
}
