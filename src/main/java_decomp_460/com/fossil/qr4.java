package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr4 implements Factory<pr4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<nr4> f3009a;
    @DexIgnore
    public /* final */ Provider<or4> b;

    @DexIgnore
    public qr4(Provider<nr4> provider, Provider<or4> provider2) {
        this.f3009a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static qr4 a(Provider<nr4> provider, Provider<or4> provider2) {
        return new qr4(provider, provider2);
    }

    @DexIgnore
    public static pr4 c(nr4 nr4, or4 or4) {
        return new pr4(nr4, or4);
    }

    @DexIgnore
    /* renamed from: b */
    public pr4 get() {
        return c(this.f3009a.get(), this.b.get());
    }
}
