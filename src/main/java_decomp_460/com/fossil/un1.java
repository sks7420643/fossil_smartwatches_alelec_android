package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum un1 {
    SUNDAY((byte) 1, "Sun"),
    MONDAY((byte) 2, "Mon"),
    TUESDAY((byte) 4, "Tue"),
    WEDNESDAY((byte) 8, "Wed"),
    THURSDAY(DateTimeFieldType.CLOCKHOUR_OF_DAY, "Thu"),
    FRIDAY((byte) 32, "Fri"),
    SATURDAY((byte) 64, "Sat");
    
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final Set<un1> a(byte b) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            un1[] values = un1.values();
            for (un1 un1 : values) {
                if (((byte) (un1.b() & b)) == un1.b()) {
                    linkedHashSet.add(un1);
                }
            }
            return linkedHashSet;
        }

        @DexIgnore
        public final un1[] b(String[] strArr) {
            un1 un1;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                un1[] values = un1.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        un1 = null;
                        break;
                    }
                    un1 = values[i];
                    if (pq7.a(ey1.a(un1), str) || pq7.a(un1.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (un1 != null) {
                    arrayList.add(un1);
                }
            }
            Object[] array = arrayList.toArray(new un1[0]);
            if (array != null) {
                return (un1[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public un1(byte b2, String str) {
        this.b = (byte) b2;
        this.c = str;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final byte b() {
        return this.b;
    }
}
