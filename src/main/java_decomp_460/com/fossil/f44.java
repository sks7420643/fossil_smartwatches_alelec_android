package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f44<T> extends i44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ i44<? super T> ordering;

    @DexIgnore
    public f44(i44<? super T> i44) {
        this.ordering = i44;
    }

    @DexIgnore
    @Override // com.fossil.i44, java.util.Comparator
    public int compare(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return this.ordering.compare(t, t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof f44) {
            return this.ordering.equals(((f44) obj).ordering);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.ordering.hashCode() ^ 957692532;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends T> i44<S> nullsFirst() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends T> i44<S> nullsLast() {
        return this.ordering.nullsLast();
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends T> i44<S> reverse() {
        return this.ordering.reverse().nullsLast();
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".nullsFirst()";
    }
}
