package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 d;

    @DexIgnore
    public qp3(fp3 fp3, Bundle bundle, or3 or3) {
        this.d = fp3;
        this.b = bundle;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        cl3 cl3 = this.d.d;
        if (cl3 == null) {
            this.d.d().F().a("Failed to send default event parameters to service");
            return;
        }
        try {
            cl3.n2(this.b, this.c);
        } catch (RemoteException e) {
            this.d.d().F().b("Failed to send default event parameters to service", e);
        }
    }
}
