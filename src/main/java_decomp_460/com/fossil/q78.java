package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q78 implements d78 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f2936a; // = false;
    @DexIgnore
    public /* final */ Map<String, p78> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<k78> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    @Override // com.fossil.d78
    public e78 a(String str) {
        p78 p78;
        synchronized (this) {
            p78 = this.b.get(str);
            if (p78 == null) {
                p78 = new p78(str, this.c, this.f2936a);
                this.b.put(str, p78);
            }
        }
        return p78;
    }

    @DexIgnore
    public void b() {
        this.b.clear();
        this.c.clear();
    }

    @DexIgnore
    public LinkedBlockingQueue<k78> c() {
        return this.c;
    }

    @DexIgnore
    public List<p78> d() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void e() {
        this.f2936a = true;
    }
}
