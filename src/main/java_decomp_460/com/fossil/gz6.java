package com.fossil;

import com.portfolio.platform.data.model.PermissionData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ dz6 f1403a;
    @DexIgnore
    public /* final */ List<PermissionData> b;

    @DexIgnore
    public gz6(dz6 dz6, List<PermissionData> list) {
        pq7.c(dz6, "mView");
        pq7.c(list, "mListPerms");
        this.f1403a = dz6;
        this.b = list;
    }

    @DexIgnore
    public final List<PermissionData> a() {
        return this.b;
    }

    @DexIgnore
    public final dz6 b() {
        return this.f1403a;
    }
}
