package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d21 implements n11 {
    @DexIgnore
    public static /* final */ String f; // = x01.f("SystemJobScheduler");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ JobScheduler c;
    @DexIgnore
    public /* final */ s11 d;
    @DexIgnore
    public /* final */ c21 e;

    @DexIgnore
    public d21(Context context, s11 s11) {
        this(context, s11, (JobScheduler) context.getSystemService("jobscheduler"), new c21(context));
    }

    @DexIgnore
    public d21(Context context, s11 s11, JobScheduler jobScheduler, c21 c21) {
        this.b = context;
        this.d = s11;
        this.c = jobScheduler;
        this.e = c21;
    }

    @DexIgnore
    public static void b(Context context) {
        List<JobInfo> h;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (!(jobScheduler == null || (h = h(context, jobScheduler)) == null || h.isEmpty())) {
            for (JobInfo jobInfo : h) {
                f(jobScheduler, jobInfo.getId());
            }
        }
    }

    @DexIgnore
    public static void d(Context context) {
        List<JobInfo> h;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (!(jobScheduler == null || (h = h(context, jobScheduler)) == null || h.isEmpty())) {
            for (JobInfo jobInfo : h) {
                if (i(jobInfo) == null) {
                    f(jobScheduler, jobInfo.getId());
                }
            }
        }
    }

    @DexIgnore
    public static void f(JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            x01.c().b(f, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", Integer.valueOf(i)), th);
        }
    }

    @DexIgnore
    public static List<Integer> g(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> h = h(context, jobScheduler);
        if (h == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo jobInfo : h) {
            if (str.equals(i(jobInfo))) {
                arrayList.add(Integer.valueOf(jobInfo.getId()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<JobInfo> h(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            x01.c().b(f, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo jobInfo : list) {
            if (componentName.equals(jobInfo.getService())) {
                arrayList.add(jobInfo);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String i(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras != null) {
            try {
                if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                    return extras.getString("EXTRA_WORK_SPEC_ID");
                }
            } catch (NullPointerException e2) {
            }
        }
        return null;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.n11
    public void a(o31... o31Arr) {
        List<Integer> g;
        WorkDatabase p = this.d.p();
        x31 x31 = new x31(p);
        for (o31 o31 : o31Arr) {
            p.beginTransaction();
            try {
                o31 o = p.j().o(o31.f2626a);
                if (o == null) {
                    x01.c().h(f, "Skipping scheduling " + o31.f2626a + " because it's no longer in the DB", new Throwable[0]);
                    p.setTransactionSuccessful();
                } else if (o.b != f11.ENQUEUED) {
                    x01.c().h(f, "Skipping scheduling " + o31.f2626a + " because it is no longer enqueued", new Throwable[0]);
                    p.setTransactionSuccessful();
                } else {
                    f31 b2 = p.g().b(o31.f2626a);
                    int d2 = b2 != null ? b2.b : x31.d(this.d.j().f(), this.d.j().d());
                    if (b2 == null) {
                        this.d.p().g().a(new f31(o31.f2626a, d2));
                    }
                    j(o31, d2);
                    if (Build.VERSION.SDK_INT == 23 && (g = g(this.b, this.c, o31.f2626a)) != null) {
                        int indexOf = g.indexOf(Integer.valueOf(d2));
                        if (indexOf >= 0) {
                            g.remove(indexOf);
                        }
                        j(o31, !g.isEmpty() ? g.get(0).intValue() : x31.d(this.d.j().f(), this.d.j().d()));
                    }
                    p.setTransactionSuccessful();
                }
                p.endTransaction();
            } catch (Throwable th) {
                p.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n11
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.n11
    public void e(String str) {
        List<Integer> g = g(this.b, this.c, str);
        if (!(g == null || g.isEmpty())) {
            for (Integer num : g) {
                f(this.c, num.intValue());
            }
            this.d.p().g().c(str);
        }
    }

    @DexIgnore
    public void j(o31 o31, int i) {
        JobInfo a2 = this.e.a(o31, i);
        x01.c().a(f, String.format("Scheduling work ID %s Job ID %s", o31.f2626a, Integer.valueOf(i)), new Throwable[0]);
        try {
            this.c.schedule(a2);
        } catch (IllegalStateException e2) {
            List<JobInfo> h = h(this.b, this.c);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", Integer.valueOf(h != null ? h.size() : 0), Integer.valueOf(this.d.p().j().i().size()), Integer.valueOf(this.d.j().e()));
            x01.c().b(f, format, new Throwable[0]);
            throw new IllegalStateException(format, e2);
        } catch (Throwable th) {
            x01.c().b(f, String.format("Unable to schedule %s", o31), th);
        }
    }
}
