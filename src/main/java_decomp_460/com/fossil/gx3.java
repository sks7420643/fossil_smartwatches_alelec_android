package com.fossil;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gx3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int[] f1388a; // = {16843848};

    @DexIgnore
    public static void a(View view) {
        view.setOutlineProvider(ViewOutlineProvider.BOUNDS);
    }

    @DexIgnore
    public static void b(View view, float f) {
        int integer = view.getResources().getInteger(ow3.app_bar_elevation_anim_duration);
        StateListAnimator stateListAnimator = new StateListAnimator();
        int i = jw3.state_liftable;
        long j = (long) integer;
        ObjectAnimator duration = ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(j);
        stateListAnimator.addState(new int[]{16842766, i, -jw3.state_lifted}, duration);
        ObjectAnimator duration2 = ObjectAnimator.ofFloat(view, "elevation", f).setDuration(j);
        stateListAnimator.addState(new int[]{16842766}, duration2);
        int[] iArr = new int[0];
        stateListAnimator.addState(iArr, ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(0L));
        view.setStateListAnimator(stateListAnimator);
    }

    @DexIgnore
    public static void c(View view, AttributeSet attributeSet, int i, int i2) {
        Context context = view.getContext();
        TypedArray k = jz3.k(context, attributeSet, f1388a, i, i2, new int[0]);
        try {
            if (k.hasValue(0)) {
                view.setStateListAnimator(AnimatorInflater.loadStateListAnimator(context, k.getResourceId(0, 0)));
            }
        } finally {
            k.recycle();
        }
    }
}
