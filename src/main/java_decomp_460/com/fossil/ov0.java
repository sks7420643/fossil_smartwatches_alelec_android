package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ov0 {
    @DexIgnore
    public static int a(RecyclerView.State state, lv0 lv0, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.K() == 0 || state.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(mVar.i0(view) - mVar.i0(view2)) + 1;
        }
        return Math.min(lv0.n(), lv0.d(view2) - lv0.g(view));
    }

    @DexIgnore
    public static int b(RecyclerView.State state, lv0 lv0, View view, View view2, RecyclerView.m mVar, boolean z, boolean z2) {
        if (mVar.K() == 0 || state.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        int max = z2 ? Math.max(0, (state.b() - Math.max(mVar.i0(view), mVar.i0(view2))) - 1) : Math.max(0, Math.min(mVar.i0(view), mVar.i0(view2)));
        if (!z) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(lv0.d(view2) - lv0.g(view))) / ((float) (Math.abs(mVar.i0(view) - mVar.i0(view2)) + 1)))) + ((float) (lv0.m() - lv0.g(view))));
    }

    @DexIgnore
    public static int c(RecyclerView.State state, lv0 lv0, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.K() == 0 || state.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return state.b();
        }
        return (int) ((((float) (lv0.d(view2) - lv0.g(view))) / ((float) (Math.abs(mVar.i0(view) - mVar.i0(view2)) + 1))) * ((float) state.b()));
    }
}
