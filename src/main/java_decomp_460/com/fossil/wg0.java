package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wg0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ImageView f3931a;
    @DexIgnore
    public rh0 b;
    @DexIgnore
    public rh0 c;
    @DexIgnore
    public rh0 d;

    @DexIgnore
    public wg0(ImageView imageView) {
        this.f3931a = imageView;
    }

    @DexIgnore
    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new rh0();
        }
        rh0 rh0 = this.d;
        rh0.a();
        ColorStateList a2 = gp0.a(this.f3931a);
        if (a2 != null) {
            rh0.d = true;
            rh0.f3111a = a2;
        }
        PorterDuff.Mode b2 = gp0.b(this.f3931a);
        if (b2 != null) {
            rh0.c = true;
            rh0.b = b2;
        }
        if (!rh0.d && !rh0.c) {
            return false;
        }
        ug0.i(drawable, rh0, this.f3931a.getDrawableState());
        return true;
    }

    @DexIgnore
    public void b() {
        Drawable drawable = this.f3931a.getDrawable();
        if (drawable != null) {
            dh0.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!j() || !a(drawable)) {
            rh0 rh0 = this.c;
            if (rh0 != null) {
                ug0.i(drawable, rh0, this.f3931a.getDrawableState());
                return;
            }
            rh0 rh02 = this.b;
            if (rh02 != null) {
                ug0.i(drawable, rh02, this.f3931a.getDrawableState());
            }
        }
    }

    @DexIgnore
    public ColorStateList c() {
        rh0 rh0 = this.c;
        if (rh0 != null) {
            return rh0.f3111a;
        }
        return null;
    }

    @DexIgnore
    public PorterDuff.Mode d() {
        rh0 rh0 = this.c;
        if (rh0 != null) {
            return rh0.b;
        }
        return null;
    }

    @DexIgnore
    public boolean e() {
        return Build.VERSION.SDK_INT < 21 || !(this.f3931a.getBackground() instanceof RippleDrawable);
    }

    @DexIgnore
    public void f(AttributeSet attributeSet, int i) {
        int n;
        th0 v = th0.v(this.f3931a.getContext(), attributeSet, ue0.AppCompatImageView, i, 0);
        ImageView imageView = this.f3931a;
        mo0.j0(imageView, imageView.getContext(), ue0.AppCompatImageView, attributeSet, v.r(), i, 0);
        try {
            Drawable drawable = this.f3931a.getDrawable();
            if (!(drawable != null || (n = v.n(ue0.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = gf0.d(this.f3931a.getContext(), n)) == null)) {
                this.f3931a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                dh0.b(drawable);
            }
            if (v.s(ue0.AppCompatImageView_tint)) {
                gp0.c(this.f3931a, v.c(ue0.AppCompatImageView_tint));
            }
            if (v.s(ue0.AppCompatImageView_tintMode)) {
                gp0.d(this.f3931a, dh0.e(v.k(ue0.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            v.w();
        }
    }

    @DexIgnore
    public void g(int i) {
        if (i != 0) {
            Drawable d2 = gf0.d(this.f3931a.getContext(), i);
            if (d2 != null) {
                dh0.b(d2);
            }
            this.f3931a.setImageDrawable(d2);
        } else {
            this.f3931a.setImageDrawable(null);
        }
        b();
    }

    @DexIgnore
    public void h(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new rh0();
        }
        rh0 rh0 = this.c;
        rh0.f3111a = colorStateList;
        rh0.d = true;
        b();
    }

    @DexIgnore
    public void i(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new rh0();
        }
        rh0 rh0 = this.c;
        rh0.b = mode;
        rh0.c = true;
        b();
    }

    @DexIgnore
    public final boolean j() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.b != null : i == 21;
    }
}
