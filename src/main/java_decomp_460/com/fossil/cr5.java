package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr5 implements Factory<br5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f642a;

    @DexIgnore
    public cr5(Provider<UserRepository> provider) {
        this.f642a = provider;
    }

    @DexIgnore
    public static cr5 a(Provider<UserRepository> provider) {
        return new cr5(provider);
    }

    @DexIgnore
    public static br5 c(UserRepository userRepository) {
        return new br5(userRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public br5 get() {
        return c(this.f642a.get());
    }
}
