package com.fossil;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f760a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(PluginRegistry.Registrar registrar) {
            pq7.c(registrar, "registrar");
            new MethodChannel(registrar.messenger(), "top.kikt/photo_manager").setMethodCallHandler(new ga8(registrar));
        }
    }

    /*
    static {
        new ta8(null, 1, null);
    }
    */

    @DexIgnore
    public static final void a(PluginRegistry.Registrar registrar) {
        f760a.a(registrar);
    }
}
