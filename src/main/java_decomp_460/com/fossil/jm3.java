package com.fossil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.qu2;
import com.fossil.ru2;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm3 extends zq3 implements hg3 {
    @DexIgnore
    public static int j; // = 65535;
    @DexIgnore
    public static int k; // = 2;
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> d; // = new zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> e; // = new zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> f; // = new zi0();
    @DexIgnore
    public /* final */ Map<String, ru2> g; // = new zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Integer>> h; // = new zi0();
    @DexIgnore
    public /* final */ Map<String, String> i; // = new zi0();

    @DexIgnore
    public jm3(yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    public static Map<String, String> w(ru2 ru2) {
        zi0 zi0 = new zi0();
        if (ru2 != null) {
            for (su2 su2 : ru2.L()) {
                zi0.put(su2.C(), su2.D());
            }
        }
        return zi0;
    }

    @DexIgnore
    public final boolean A(String str, String str2) {
        h();
        J(str);
        if (H(str) && kr3.B0(str2)) {
            return true;
        }
        if (I(str) && kr3.c0(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.e.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final void B(String str) {
        h();
        this.i.put(str, null);
    }

    @DexIgnore
    public final boolean C(String str, String str2) {
        h();
        J(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        if (s53.a() && m().s(xg3.J0) && ("purchase".equals(str2) || "refund".equals(str2))) {
            return true;
        }
        Map<String, Boolean> map = this.f.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final int D(String str, String str2) {
        h();
        J(str);
        Map<String, Integer> map = this.h.get(str);
        if (map == null) {
            return 1;
        }
        Integer num = map.get(str2);
        if (num == null) {
            return 1;
        }
        return num.intValue();
    }

    @DexIgnore
    public final void E(String str) {
        h();
        this.g.remove(str);
    }

    @DexIgnore
    public final boolean F(String str) {
        h();
        ru2 u = u(str);
        if (u == null) {
            return false;
        }
        return u.O();
    }

    @DexIgnore
    public final long G(String str) {
        String a2 = a(str, "measurement.account.time_zone_offset_minutes");
        if (!TextUtils.isEmpty(a2)) {
            try {
                return Long.parseLong(a2);
            } catch (NumberFormatException e2) {
                d().I().c("Unable to parse timezone offset. appId", kl3.w(str), e2);
            }
        }
        return 0;
    }

    @DexIgnore
    public final boolean H(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    @DexIgnore
    public final boolean I(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    @DexIgnore
    public final void J(String str) {
        r();
        h();
        rc2.g(str);
        if (this.g.get(str) == null) {
            byte[] p0 = o().p0(str);
            if (p0 == null) {
                this.d.put(str, null);
                this.e.put(str, null);
                this.f.put(str, null);
                this.g.put(str, null);
                this.i.put(str, null);
                this.h.put(str, null);
                return;
            }
            ru2.a aVar = (ru2.a) v(str, p0).x();
            x(str, aVar);
            this.d.put(str, w((ru2) ((e13) aVar.h())));
            this.g.put(str, (ru2) ((e13) aVar.h()));
            this.i.put(str, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.hg3
    public final String a(String str, String str2) {
        h();
        J(str);
        Map<String, String> map = this.d.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final ru2 u(String str) {
        r();
        h();
        rc2.g(str);
        J(str);
        return this.g.get(str);
    }

    @DexIgnore
    public final ru2 v(String str, byte[] bArr) {
        String str2 = null;
        if (bArr == null) {
            return ru2.Q();
        }
        try {
            ru2.a P = ru2.P();
            gr3.z(P, bArr);
            ru2 ru2 = (ru2) ((e13) P.h());
            nl3 N = d().N();
            Long valueOf = ru2.H() ? Long.valueOf(ru2.I()) : null;
            if (ru2.J()) {
                str2 = ru2.K();
            }
            N.c("Parsed config. version, gmp_app_id", valueOf, str2);
            return ru2;
        } catch (l13 e2) {
            d().I().c("Unable to merge remote config. appId", kl3.w(str), e2);
            return ru2.Q();
        } catch (RuntimeException e3) {
            d().I().c("Unable to merge remote config. appId", kl3.w(str), e3);
            return ru2.Q();
        }
    }

    @DexIgnore
    public final void x(String str, ru2.a aVar) {
        zi0 zi0 = new zi0();
        zi0 zi02 = new zi0();
        zi0 zi03 = new zi0();
        if (aVar != null) {
            for (int i2 = 0; i2 < aVar.x(); i2++) {
                qu2.a aVar2 = (qu2.a) aVar.y(i2).x();
                if (TextUtils.isEmpty(aVar2.y())) {
                    d().I().a("EventConfig contained null event name");
                } else {
                    String b = on3.b(aVar2.y());
                    if (!TextUtils.isEmpty(b)) {
                        aVar2.x(b);
                        aVar.z(i2, aVar2);
                    }
                    zi0.put(aVar2.y(), Boolean.valueOf(aVar2.z()));
                    zi02.put(aVar2.y(), Boolean.valueOf(aVar2.B()));
                    if (aVar2.C()) {
                        if (aVar2.E() < k || aVar2.E() > j) {
                            d().I().c("Invalid sampling rate. Event name, sample rate", aVar2.y(), Integer.valueOf(aVar2.E()));
                        } else {
                            zi03.put(aVar2.y(), Integer.valueOf(aVar2.E()));
                        }
                    }
                }
            }
        }
        this.e.put(str, zi0);
        this.f.put(str, zi02);
        this.h.put(str, zi03);
    }

    @DexIgnore
    public final boolean y(String str, byte[] bArr, String str2) {
        r();
        h();
        rc2.g(str);
        ru2.a aVar = (ru2.a) v(str, bArr).x();
        if (aVar == null) {
            return false;
        }
        x(str, aVar);
        this.g.put(str, (ru2) ((e13) aVar.h()));
        this.i.put(str, str2);
        this.d.put(str, w((ru2) ((e13) aVar.h())));
        o().N(str, new ArrayList(aVar.B()));
        try {
            aVar.C();
            bArr = ((ru2) ((e13) aVar.h())).c();
        } catch (RuntimeException e2) {
            d().I().c("Unable to serialize reduced-size config. Storing full config instead. appId", kl3.w(str), e2);
        }
        kg3 o = o();
        rc2.g(str);
        o.h();
        o.r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) o.v().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                o.d().F().b("Failed to update remote config (got 0). appId", kl3.w(str));
            }
        } catch (SQLiteException e3) {
            o.d().F().c("Error storing remote config. appId", kl3.w(str), e3);
        }
        this.g.put(str, (ru2) ((e13) aVar.h()));
        return true;
    }

    @DexIgnore
    public final String z(String str) {
        h();
        return this.i.get(str);
    }
}
