package com.fossil;

import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.manager.SoLibraryLoader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m07 extends i07 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ Runnable f; // = new e(this);
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ j07 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ do5 j;
    @DexIgnore
    public /* final */ mj5 k;
    @DexIgnore
    public /* final */ ThemeRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return m07.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter", f = "SplashPresenter.kt", l = {84}, m = "checkAuthToken")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ m07 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(m07 m07, qn7 qn7) {
            super(qn7);
            this.this$0 = m07;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkAuthToken$isAA$1", f = "SplashPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public c(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return ao7.a(SoLibraryLoader.f().c(PortfolioApp.h0.c()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1", f = "SplashPresenter.kt", l = {68, 69, 74, 77}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m07 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$mCurrentUser$1", f = "SplashPresenter.kt", l = {69}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.i;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(m07 m07, qn7 qn7) {
            super(2, qn7);
            this.this$0 = m07;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x004b  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x010f  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0130  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 330
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.m07.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ m07 b;

        @DexIgnore
        public e(m07 m07) {
            this.b = m07;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(m07.n.a(), "runnable");
            this.b.x();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$start$1", f = "SplashPresenter.kt", l = {51}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $currentAppVersion;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m07 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(m07 m07, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = m07;
            this.$currentAppVersion = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$currentAppVersion, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                do5 do5 = this.this$0.j;
                String str = this.$currentAppVersion;
                this.L$0 = iv7;
                this.label = 1;
                if (do5.f(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = m07.class.getSimpleName();
        pq7.b(simpleName, "SplashPresenter::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public m07(j07 j07, UserRepository userRepository, do5 do5, mj5 mj5, ThemeRepository themeRepository) {
        pq7.c(j07, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(do5, "mMigrationHelper");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(themeRepository, "mThemeRepository");
        this.h = j07;
        this.i = userRepository;
        this.j = do5;
        this.k = mj5;
        this.l = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        String P = PortfolioApp.h0.c().P();
        boolean d2 = this.j.d(P);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "currentAppVersion " + P + " complete " + d2);
        if (!d2) {
            this.g = true;
            xw7 unused = gu7.d(k(), null, null, new f(this, P, null), 3, null);
        }
        this.e.postDelayed(this.f, 1000);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.e.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.m07.b
            if (r0 == 0) goto L_0x004a
            r0 = r7
            com.fossil.m07$b r0 = (com.fossil.m07.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0058
            if (r3 != r5) goto L_0x0050
            java.lang.Object r0 = r0.L$0
            com.fossil.m07 r0 = (com.fossil.m07) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0071
            boolean r0 = r6.g
            if (r0 == 0) goto L_0x0042
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            com.fossil.mj5 r1 = r6.k
            r2 = 0
            r3 = 13
            r0.S1(r1, r2, r3)
        L_0x0042:
            com.fossil.j07 r0 = r6.h
            r0.l()
        L_0x0047:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0049:
            return r0
        L_0x004a:
            com.fossil.m07$b r0 = new com.fossil.m07$b
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.m07$c r3 = new com.fossil.m07$c
            r4 = 0
            r3.<init>(r4)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0049
        L_0x0071:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r0 = r0.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r1 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            java.lang.String r3 = r3.J()
            com.portfolio.platform.ApplicationEventListener$a r4 = com.portfolio.platform.ApplicationEventListener.J
            java.lang.String r4 = r4.a()
            java.lang.String r5 = "[App Authentication] Unauthorized"
            r0.i(r1, r2, r3, r4, r5)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r1.<init>()
            r0.F(r1)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.m07.w(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void x() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public void y() {
        this.h.M5(this);
    }
}
