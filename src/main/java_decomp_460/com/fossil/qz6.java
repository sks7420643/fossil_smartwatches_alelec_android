package com.fossil;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz6 extends pv5 implements pz6 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public oz6 g;
    @DexIgnore
    public g37<db5> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final qz6 a() {
            return new qz6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ qz6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ db5 b;

            @DexIgnore
            public a(db5 db5) {
                this.b = db5;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.b.L;
                pq7.b(flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.b.M;
                pq7.b(flexibleButton, "it.tvLogin");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, qz6 qz6) {
            this.b = view;
            this.c = qz6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                db5 db5 = (db5) qz6.K6(this.c).a();
                if (db5 != null) {
                    try {
                        FlexibleTextView flexibleTextView = db5.L;
                        pq7.b(flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = db5.M;
                        pq7.b(flexibleButton, "it.tvLogin");
                        flexibleButton.setVisibility(8);
                        tl7 tl7 = tl7.f3441a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e);
                        tl7 tl72 = tl7.f3441a;
                    }
                }
            } else {
                db5 db52 = (db5) qz6.K6(this.c).a();
                if (db52 != null) {
                    try {
                        pq7.b(db52, "it");
                        db52.n().postDelayed(new a(db52), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e2);
                        tl7 tl73 = tl7.f3441a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public c(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qz6.L6(this.b).v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public d(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qz6.L6(this.b).w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public e(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.F;
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ db5 b;
        @DexIgnore
        public /* final */ /* synthetic */ qz6 c;

        @DexIgnore
        public f(db5 db5, qz6 qz6) {
            this.b = db5;
            this.c = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.w;
            pq7.b(flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.b.v;
            pq7.b(flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.c.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public g(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            qz6.L6(this.b).p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public h(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            qz6.L6(this.b).o(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public i(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            qz6.L6(this.b).q(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextView.OnEditorActionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qz6 f3054a;

        @DexIgnore
        public j(qz6 qz6) {
            this.f3054a = qz6;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.f3054a.O6();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public k(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public l(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qz6.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public m(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qz6.L6(this.b).u();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qz6 b;

        @DexIgnore
        public n(qz6 qz6) {
            this.b = qz6;
        }

        @DexIgnore
        public final void onClick(View view) {
            qz6.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ db5 b;

        @DexIgnore
        public o(db5 db5) {
            this.b = db5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.b.J;
                pq7.b(flexibleTextView, "binding.tvErrorCheckCharacter");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.b.K;
                pq7.b(flexibleTextView2, "binding.tvErrorCheckCombine");
                flexibleTextView2.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView3 = this.b.J;
            pq7.b(flexibleTextView3, "binding.tvErrorCheckCharacter");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = this.b.K;
            pq7.b(flexibleTextView4, "binding.tvErrorCheckCombine");
            flexibleTextView4.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ g37 K6(qz6 qz6) {
        g37<db5> g37 = qz6.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ oz6 L6(qz6 qz6) {
        oz6 oz6 = qz6.g;
        if (oz6 != null) {
            return oz6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void B4() {
        if (isActive()) {
            g37<db5> g37 = this.h;
            if (g37 != null) {
                db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    pq7.b(flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.s;
                    pq7.b(flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.s;
                    pq7.b(flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(false);
                    a2.s.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void C(int i2, String str) {
        pq7.c(str, "errorMessage");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "SignUpFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(oz6 oz6) {
        pq7.c(oz6, "presenter");
        this.g = oz6;
    }

    @DexIgnore
    public final void O6() {
        g37<db5> g37 = this.h;
        if (g37 != null) {
            db5 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                pq7.b(flexibleButton, "this.btContinue");
                if (flexibleButton.isEnabled()) {
                    oz6 oz6 = this.g;
                    if (oz6 != null) {
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                        pq7.b(flexibleTextInputEditText, "etEmail");
                        String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                        pq7.b(flexibleTextInputEditText2, "etPassword");
                        oz6.s(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                        return;
                    }
                    pq7.n("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void R() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = h37.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.E;
            pq7.b(activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void U3(boolean z, boolean z2) {
        if (isActive()) {
            g37<db5> g37 = this.h;
            if (g37 != null) {
                db5 a2 = g37.a();
                if (a2 != null) {
                    if (z) {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void d3(boolean z) {
        if (isActive()) {
            g37<db5> g37 = this.h;
            if (g37 != null) {
                db5 a2 = g37.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.D;
                    pq7.b(constraintLayout, "it.ivWeibo");
                    constraintLayout.setVisibility(0);
                    ConstraintLayout constraintLayout2 = a2.C;
                    pq7.b(constraintLayout2, "it.ivWechat");
                    constraintLayout2.setVisibility(0);
                    ImageView imageView = a2.B;
                    pq7.b(imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.A;
                    pq7.b(imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.D;
                pq7.b(constraintLayout3, "it.ivWeibo");
                constraintLayout3.setVisibility(8);
                ConstraintLayout constraintLayout4 = a2.C;
                pq7.b(constraintLayout4, "it.ivWechat");
                constraintLayout4.setVisibility(8);
                ImageView imageView3 = a2.B;
                pq7.b(imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.A;
                pq7.b(imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void f() {
        g37<db5> g37 = this.h;
        if (g37 != null) {
            db5 a2 = g37.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.F, "progress", 0, 10);
                pq7.b(ofInt, "progressAnimator");
                ofInt.setDuration(500L);
                ofInt.start();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void g4(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.B;
            pq7.b(activity, "it");
            aVar.b(activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void g6(String str) {
        pq7.c(str, "errorMessage");
        g37<db5> g37 = this.h;
        if (g37 != null) {
            db5 a2 = g37.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.y;
                pq7.b(rTLImageView, "it.ivCheckedEmail");
                if (rTLImageView.getVisibility() == 0) {
                    RTLImageView rTLImageView2 = a2.y;
                    pq7.b(rTLImageView2, "it.ivCheckedEmail");
                    rTLImageView2.setVisibility(8);
                }
                FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                pq7.b(flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void i() {
        String string = getString(2131887002);
        pq7.b(string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void l() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.B;
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            HomeActivity.a.b(aVar, requireActivity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void l3(int i2, String str) {
        pq7.c(str, "errorMessage");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void n0(boolean z, boolean z2, String str) {
        RTLImageView rTLImageView;
        pq7.c(str, "errorMessage");
        if (isActive()) {
            g37<db5> g37 = this.h;
            if (g37 != null) {
                db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    pq7.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                    pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                }
                g37<db5> g372 = this.h;
                if (g372 != null) {
                    db5 a3 = g372.a();
                    if (a3 != null && (rTLImageView = a3.y) != null) {
                        pq7.b(rTLImageView, "it");
                        rTLImageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            oz6 oz6 = this.g;
            if (oz6 != null) {
                pq7.b(signUpSocialAuth, "authCode");
                oz6.r(signUpSocialAuth);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        db5 db5 = (db5) aq0.f(layoutInflater, 2131558622, viewGroup, false, A6());
        pq7.b(db5, "bindingLocal");
        View n2 = db5.n();
        pq7.b(n2, "bindingLocal.root");
        n2.getViewTreeObserver().addOnGlobalLayoutListener(new b(n2, this));
        g37<db5> g37 = new g37<>(this, db5);
        this.h = g37;
        if (g37 != null) {
            db5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        oz6 oz6 = this.g;
        if (oz6 != null) {
            oz6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        oz6 oz6 = this.g;
        if (oz6 != null) {
            oz6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<db5> g37 = this.h;
        if (g37 != null) {
            db5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new f(a2, this));
                a2.t.addTextChangedListener(new g(this));
                a2.t.setOnFocusChangeListener(new h(this));
                a2.u.addTextChangedListener(new i(this));
                a2.u.setOnFocusChangeListener(new o(a2));
                a2.u.setOnEditorActionListener(new j(this));
                a2.z.setOnClickListener(new k(this));
                a2.A.setOnClickListener(new l(this));
                a2.B.setOnClickListener(new m(this));
                a2.x.setOnClickListener(new n(this));
                a2.C.setOnClickListener(new c(this));
                a2.D.setOnClickListener(new d(this));
                a2.M.setOnClickListener(new e(this));
            }
            E6("sign_up_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void x2() {
        if (isActive()) {
            g37<db5> g37 = this.h;
            if (g37 != null) {
                db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    pq7.b(flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.s;
                    pq7.b(flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.s;
                    pq7.b(flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(true);
                    a2.s.d("flexible_button_primary");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pz6
    public void y3(SignUpEmailAuth signUpEmailAuth) {
        FragmentActivity activity;
        pq7.c(signUpEmailAuth, "emailAuth");
        if (isActive() && (activity = getActivity()) != null) {
            EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }
}
