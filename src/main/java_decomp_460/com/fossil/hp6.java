package com.fossil;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.iq4;
import com.fossil.jt5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp6 extends hq4 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ b v; // = new b(null);
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ MutableLiveData<u37<e>> j;
    @DexIgnore
    public /* final */ LiveData<u37<e>> k;
    @DexIgnore
    public c l; // = new c(0, 0, 0, 0, 0, 31, null);
    @DexIgnore
    public c m; // = new c(0, 0, 0, 0, 0, 31, null);
    @DexIgnore
    public /* final */ MutableLiveData<c> n;
    @DexIgnore
    public /* final */ LiveData<c> o;
    @DexIgnore
    public /* final */ SummariesRepository p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;
    @DexIgnore
    public /* final */ UserRepository s;
    @DexIgnore
    public /* final */ jt5 t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1", f = "ProfileGoalEditViewModel.kt", l = {61}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hp6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1", f = "ProfileGoalEditViewModel.kt", l = {78, 79, 80, 81, 82}, m = "invokeSuspend")
        /* renamed from: com.fossil.hp6$a$a  reason: collision with other inner class name */
        public static final class C0112a extends ko7 implements vp7<iv7, qn7<? super c>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$a$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$activitySettings$1", f = "ProfileGoalEditViewModel.kt", l = {63}, m = "invokeSuspend")
            /* renamed from: com.fossil.hp6$a$a$a  reason: collision with other inner class name */
            public static final class C0113a extends ko7 implements vp7<iv7, qn7<? super ActivitySettings>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0112a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0113a(C0112a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0113a aVar = new C0113a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super ActivitySettings> qn7) {
                    return ((C0113a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object activitySettings;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        SummariesRepository summariesRepository = this.this$0.this$0.this$0.p;
                        this.L$0 = iv7;
                        this.label = 1;
                        activitySettings = summariesRepository.getActivitySettings(this);
                        if (activitySettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        activitySettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySettings activitySettings2 = (ActivitySettings) activitySettings;
                    return activitySettings2 != null ? activitySettings2 : new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$a$a$b")
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$goalTracking$1", f = "ProfileGoalEditViewModel.kt", l = {73}, m = "invokeSuspend")
            /* renamed from: com.fossil.hp6$a$a$b */
            public static final class b extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0112a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(C0112a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                    return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object lastGoalSettings;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.this$0.r;
                        this.L$0 = iv7;
                        this.label = 1;
                        lastGoalSettings = goalTrackingRepository.getLastGoalSettings(this);
                        if (lastGoalSettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        lastGoalSettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) lastGoalSettings;
                    return ao7.e(num != null ? num.intValue() : 8);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$a$a$c")
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$sleepGoal$1", f = "ProfileGoalEditViewModel.kt", l = {68}, m = "invokeSuspend")
            /* renamed from: com.fossil.hp6$a$a$c */
            public static final class c extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0112a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(C0112a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                    return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object lastSleepGoal;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.this$0.q;
                        this.L$0 = iv7;
                        this.label = 1;
                        lastSleepGoal = sleepSummariesRepository.getLastSleepGoal(this);
                        if (lastSleepGoal == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        lastSleepGoal = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) lastSleepGoal;
                    return ao7.e(num != null ? num.intValue() : 480);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0112a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0112a aVar = new C0112a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super c> qn7) {
                return ((C0112a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0085  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x00bd  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x00f2  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0124  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x0160  */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0164  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x0167  */
            /* JADX WARNING: Removed duplicated region for block: B:34:0x016c  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 373
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.hp6.a.C0112a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hp6 hp6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hp6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            hp6 hp6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hq4.d(this.this$0, true, false, null, 6, null);
                hp6 hp62 = this.this$0;
                dv7 b = bw7.b();
                C0112a aVar = new C0112a(this, null);
                this.L$0 = iv7;
                this.L$1 = hp62;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
                hp6 = hp62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                hp6 = (hp6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            hp6.m = (c) g;
            hp6 hp63 = this.this$0;
            hp63.l = c.b(hp63.m, 0, 0, 0, 0, 0, 31, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = hp6.v.a();
            local.d(a2, "init - mUiModel: " + this.this$0.l);
            this.this$0.F();
            hq4.d(this.this$0, false, true, null, 5, null);
            this.this$0.I();
            this.this$0.t.q();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return hp6.u;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f1505a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public c() {
            this(0, 0, 0, 0, 0, 31, null);
        }

        @DexIgnore
        public c(int i, int i2, int i3, int i4, int i5) {
            this.f1505a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, int i2, int i3, int i4, int i5, int i6, kq7 kq7) {
            this((i6 & 1) != 0 ? 0 : i, (i6 & 2) != 0 ? 0 : i2, (i6 & 4) != 0 ? 0 : i3, (i6 & 8) != 0 ? 0 : i4, (i6 & 16) == 0 ? i5 : 0);
        }

        @DexIgnore
        public static /* synthetic */ c b(c cVar, int i, int i2, int i3, int i4, int i5, int i6, Object obj) {
            return cVar.a((i6 & 1) != 0 ? cVar.f1505a : i, (i6 & 2) != 0 ? cVar.b : i2, (i6 & 4) != 0 ? cVar.c : i3, (i6 & 8) != 0 ? cVar.d : i4, (i6 & 16) != 0 ? cVar.e : i5);
        }

        @DexIgnore
        public final c a(int i, int i2, int i3, int i4, int i5) {
            return new c(i, i2, i3, i4, i5);
        }

        @DexIgnore
        public final gl7<Boolean, Boolean, Boolean> c(c cVar) {
            boolean z = true;
            pq7.c(cVar, "that");
            boolean a2 = pq7.a(e(), cVar.e());
            boolean z2 = this.d != cVar.d;
            if (this.e == cVar.e) {
                z = false;
            }
            return new gl7<>(Boolean.valueOf(!a2), Boolean.valueOf(z2), Boolean.valueOf(z));
        }

        @DexIgnore
        public final int d() {
            return this.b;
        }

        @DexIgnore
        public final ActivitySettings e() {
            return new ActivitySettings(this.f1505a, this.c, this.b);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (!(this.f1505a == cVar.f1505a && this.b == cVar.b && this.c == cVar.c && this.d == cVar.d && this.e == cVar.e)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final int f() {
            return this.c;
        }

        @DexIgnore
        public final int g() {
            return this.e;
        }

        @DexIgnore
        public final int h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.f1505a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e;
        }

        @DexIgnore
        public final int i() {
            return this.f1505a;
        }

        @DexIgnore
        public final void j(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void k(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void l(int i) {
            this.e = i;
        }

        @DexIgnore
        public final void m(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void n(int i) {
            this.f1505a = i;
        }

        @DexIgnore
        public String toString() {
            return "UiModelWrapper(stepGoal=" + this.f1505a + ", activeTimeGoal=" + this.b + ", caloriesGoal=" + this.c + ", sleepGoal=" + this.d + ", goalTracking=" + this.e + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Throwable {
        @DexIgnore
        public /* final */ int errorCode;
        @DexIgnore
        public /* final */ String errorMessage;

        @DexIgnore
        public d(int i, String str) {
            super(str);
            this.errorCode = i;
            this.errorMessage = str;
        }

        @DexIgnore
        public final int getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getErrorMessage() {
            return this.errorMessage;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ a f1506a; // = new a();

            @DexIgnore
            public a() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ b f1507a; // = new b();

            @DexIgnore
            public b() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ c f1508a; // = new c();

            @DexIgnore
            public c() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ int f1509a;
            @DexIgnore
            public /* final */ String b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(int i, String str) {
                super(null);
                pq7.c(str, "message");
                this.f1509a = i;
                this.b = str;
            }

            @DexIgnore
            public final int a() {
                return this.f1509a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                if (this != obj) {
                    if (obj instanceof d) {
                        d dVar = (d) obj;
                        if (this.f1509a != dVar.f1509a || !pq7.a(this.b, dVar.b)) {
                            return false;
                        }
                    }
                    return false;
                }
                return true;
            }

            @DexIgnore
            public int hashCode() {
                int i = this.f1509a;
                String str = this.b;
                return (str != null ? str.hashCode() : 0) + (i * 31);
            }

            @DexIgnore
            public String toString() {
                return "ShowErrorDialog(code=" + this.f1509a + ", message=" + this.b + ")";
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$e$e")
        /* renamed from: com.fossil.hp6$e$e  reason: collision with other inner class name */
        public static final class C0114e extends e {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ List<uh5> f1510a;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.uh5> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0114e(List<? extends uh5> list) {
                super(null);
                pq7.c(list, "permissionCodes");
                this.f1510a = list;
            }

            @DexIgnore
            public final List<uh5> a() {
                return this.f1510a;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                return this == obj || ((obj instanceof C0114e) && pq7.a(this.f1510a, ((C0114e) obj).f1510a));
            }

            @DexIgnore
            public int hashCode() {
                List<uh5> list = this.f1510a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            @DexIgnore
            public String toString() {
                return "ShowPermissionPopups(permissionCodes=" + this.f1510a + ")";
            }
        }

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public /* synthetic */ e(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$checkUserUsingDefaultGoal$2", f = "ProfileGoalEditViewModel.kt", l = {255, 258}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hp6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(hp6 hp6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hp6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0067 A[RETURN, SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0050
                if (r0 == r4) goto L_0x0024
                if (r0 != r5) goto L_0x001c
                java.lang.Object r0 = r6.L$1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0019:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
                r2 = r0
                r1 = r7
            L_0x002d:
                r0 = r1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                if (r0 == 0) goto L_0x0067
                boolean r1 = r0.getUseDefaultGoals()
                if (r1 == 0) goto L_0x0019
                r1 = 0
                r0.setUseDefaultGoals(r1)
                com.fossil.hp6 r1 = r6.this$0
                com.portfolio.platform.data.source.UserRepository r1 = com.fossil.hp6.u(r1)
                r6.L$0 = r2
                r6.L$1 = r0
                r6.label = r5
                java.lang.Object r0 = r1.updateUser(r0, r4, r6)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0050:
                com.fossil.el7.b(r7)
                com.fossil.iv7 r0 = r6.p$
                com.fossil.hp6 r1 = r6.this$0
                com.portfolio.platform.data.source.UserRepository r1 = com.fossil.hp6.u(r1)
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r1 = r1.getCurrentUser(r6)
                if (r1 != r3) goto L_0x0069
                r0 = r3
                goto L_0x001b
            L_0x0067:
                r0 = 0
                goto L_0x001b
            L_0x0069:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hp6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1", f = "ProfileGoalEditViewModel.kt", l = {304, 305, 306, 130, 131, 132, 135}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hp6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super rv7<? extends iq5<? extends ActivitySettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isActivityGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hp6$g$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            /* renamed from: com.fossil.hp6$g$a$a  reason: collision with other inner class name */
            public static final class C0115a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends ActivitySettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0115a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0115a aVar = new C0115a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<? extends ActivitySettings>> qn7) {
                    return ((C0115a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateActivitySettings;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = hp6.v.a();
                        local.d(a2, "onSave - isActivityGoalChanged = " + this.this$0.$isActivityGoalChanged$inlined);
                        SummariesRepository summariesRepository = this.this$0.this$0.this$0.p;
                        ActivitySettings e = this.this$0.this$0.this$0.l.e();
                        this.L$0 = iv7;
                        this.L$1 = this;
                        this.label = 1;
                        updateActivitySettings = summariesRepository.updateActivitySettings(e, this);
                        if (updateActivitySettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        qn7 qn7 = (qn7) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        updateActivitySettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (iq5) updateActivitySettings;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(boolean z, qn7 qn7, g gVar, boolean z2) {
                super(2, qn7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isActivityGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$changed, qn7, this.this$0, this.$isActivityGoalChanged$inlined);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super rv7<? extends iq5<? extends ActivitySettings>>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return gu7.b(this.p$, null, null, new C0115a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super rv7<? extends iq5<? extends MFSleepSettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isSleepGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFSleepSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFSleepSettings>> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateLastSleepGoal;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = hp6.v.a();
                        local.d(a2, "onSave - isSleepGoalChanged = " + this.this$0.$isSleepGoalChanged$inlined);
                        SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.this$0.q;
                        int h = this.this$0.this$0.this$0.l.h();
                        this.L$0 = iv7;
                        this.L$1 = this;
                        this.label = 1;
                        updateLastSleepGoal = sleepSummariesRepository.updateLastSleepGoal(h, this);
                        if (updateLastSleepGoal == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        qn7 qn7 = (qn7) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        updateLastSleepGoal = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (iq5) updateLastSleepGoal;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(boolean z, qn7 qn7, g gVar, boolean z2) {
                super(2, qn7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isSleepGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.$changed, qn7, this.this$0, this.$isSleepGoalChanged$inlined);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super rv7<? extends iq5<? extends MFSleepSettings>>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return gu7.b(this.p$, null, null, new a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super rv7<? extends iq5<? extends GoalSetting>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalTrackingChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends GoalSetting>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = cVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<? extends GoalSetting>> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateGoalSetting;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = hp6.v.a();
                        local.d(a2, "onSave - isGoalTrackingChanged = " + this.this$0.$isGoalTrackingChanged$inlined);
                        GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.this$0.r;
                        GoalSetting goalSetting = new GoalSetting(this.this$0.this$0.this$0.l.g());
                        this.L$0 = iv7;
                        this.L$1 = this;
                        this.label = 1;
                        updateGoalSetting = goalTrackingRepository.updateGoalSetting(goalSetting, this);
                        if (updateGoalSetting == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        qn7 qn7 = (qn7) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        updateGoalSetting = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (iq5) updateGoalSetting;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(boolean z, qn7 qn7, g gVar, boolean z2) {
                super(2, qn7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isGoalTrackingChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.$changed, qn7, this.this$0, this.$isGoalTrackingChanged$inlined);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super rv7<? extends iq5<? extends GoalSetting>>> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return gu7.b(this.p$, null, null, new a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(hp6 hp6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = hp6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x016e  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0202  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0266  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x02c2  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0343  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0347  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x034c  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x03ae  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x03bc  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x03c1  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x03c8  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0037 A[Catch:{ d -> 0x0361 }] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
            // Method dump skipped, instructions count: 994
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hp6.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements iq4.e<jt5.d, jt5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hp6 f1511a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(hp6 hp6) {
            this.f1511a = hp6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(jt5.c cVar) {
            boolean z = false;
            pq7.c(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(hp6.v.a(), "executeSetActivityUseCase failed, errorCode=" + cVar.b());
            hq4.d(this.f1511a, false, true, null, 5, null);
            ArrayList<Integer> a2 = cVar.a();
            if (a2 == null || a2.isEmpty()) {
                z = true;
            }
            if (!z) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(cVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026errorValue.bleErrorCodes)");
                this.f1511a.S(convertBLEPermissionErrorCode);
                return;
            }
            hp6 hp6 = this.f1511a;
            int b = cVar.b();
            String c = cVar.c();
            if (c == null) {
                c = "";
            }
            hp6.R(b, c);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(jt5.d dVar) {
            pq7.c(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(hp6.v.a(), "executeSetActivityUseCase success");
            hq4.d(this.f1511a, false, true, null, 5, null);
            this.f1511a.N();
        }
    }

    /*
    static {
        String simpleName = hp6.class.getSimpleName();
        pq7.b(simpleName, "ProfileGoalEditViewModel::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public hp6(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, jt5 jt5) {
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(jt5, "mSetActivityGoalUserCase");
        this.p = summariesRepository;
        this.q = sleepSummariesRepository;
        this.r = goalTrackingRepository;
        this.s = userRepository;
        this.t = jt5;
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
        this.h = deviceBySerial;
        this.i = nk5.o.w(deviceBySerial);
        MutableLiveData<u37<e>> mutableLiveData = new MutableLiveData<>();
        this.j = mutableLiveData;
        this.k = mutableLiveData;
        MutableLiveData<c> mutableLiveData2 = new MutableLiveData<>(this.l);
        this.n = mutableLiveData2;
        this.o = mutableLiveData2;
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final <T> boolean D(iq5<T> iq5) {
        String str;
        if (!(iq5 instanceof hq5)) {
            return true;
        }
        hq5 hq5 = (hq5) iq5;
        int a2 = hq5.a();
        ServerError c2 = hq5.c();
        if (c2 == null || (str = c2.getMessage()) == null) {
            str = "";
        }
        throw new d(a2, str);
    }

    @DexIgnore
    public final /* synthetic */ Object E(qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new f(this, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final void F() {
        this.n.l(this.l);
    }

    @DexIgnore
    public final LiveData<u37<e>> G() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<c> H() {
        return this.o;
    }

    @DexIgnore
    public final void I() {
        this.j.l(new u37<>(e.a.f1506a));
    }

    @DexIgnore
    public final boolean J() {
        return this.i;
    }

    @DexIgnore
    public final boolean K() {
        return !pq7.a(this.l, this.m);
    }

    @DexIgnore
    public final void L() {
        if (K()) {
            Q();
        } else {
            N();
        }
    }

    @DexIgnore
    public final void M() {
        xw7 unused = gu7.d(us0.a(this), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    public final void N() {
        this.j.l(new u37<>(e.b.f1507a));
    }

    @DexIgnore
    public final void O() {
        String J = PortfolioApp.h0.c().J();
        if (!TextUtils.isEmpty(J)) {
            this.t.e(new jt5.b(J, this.l.e()), new h(this));
        }
    }

    @DexIgnore
    public final void P(int i2, rh5 rh5) {
        pq7.c(rh5, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "setSettingValue value=" + i2 + " type=" + rh5);
        int i3 = ip6.f1651a[rh5.ordinal()];
        if (i3 == 1) {
            this.l.n(i2);
        } else if (i3 == 2) {
            this.l.k(i2);
        } else if (i3 == 3) {
            this.l.j(i2);
        } else if (i3 == 4) {
            this.l.m(i2);
        } else if (i3 == 5) {
            this.l.l(i2);
        }
        F();
    }

    @DexIgnore
    public final void Q() {
        this.j.l(new u37<>(e.c.f1508a));
    }

    @DexIgnore
    public final void R(int i2, String str) {
        this.j.l(new u37<>(new e.d(i2, str)));
    }

    @DexIgnore
    public final void S(List<? extends uh5> list) {
        this.j.l(new u37<>(new e.C0114e(list)));
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        this.t.t();
    }
}
