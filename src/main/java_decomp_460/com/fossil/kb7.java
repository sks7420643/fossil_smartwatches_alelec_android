package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1896a;

    /*
    static {
        int[] iArr = new int[lb7.values().length];
        f1896a = iArr;
        iArr[lb7.STEP.ordinal()] = 1;
        f1896a[lb7.ACTIVE.ordinal()] = 2;
        f1896a[lb7.BATTERY.ordinal()] = 3;
        f1896a[lb7.CALORIES.ordinal()] = 4;
        f1896a[lb7.RAIN.ordinal()] = 5;
        f1896a[lb7.DATE.ordinal()] = 6;
        f1896a[lb7.SECOND_TIME.ordinal()] = 7;
        f1896a[lb7.WEATHER.ordinal()] = 8;
        f1896a[lb7.HEART.ordinal()] = 9;
    }
    */
}
