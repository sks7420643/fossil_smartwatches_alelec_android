package com.fossil;

import android.text.format.DateFormat;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s26 extends p26 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public InactivityNudgeTimeModel g;
    @DexIgnore
    public InactivityNudgeTimeModel h;
    @DexIgnore
    public /* final */ q26 i;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1", f = "InactivityNudgeTimePresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s26 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s26$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.s26$a$a  reason: collision with other inner class name */
        public static final class C0213a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s26$a$a$a")
            /* renamed from: com.fossil.s26$a$a$a  reason: collision with other inner class name */
            public static final class RunnableC0214a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ C0213a b;

                @DexIgnore
                public RunnableC0214a(C0213a aVar) {
                    this.b = aVar;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.b.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel inactivityNudgeTimeModel = this.b.this$0.this$0.g;
                    if (inactivityNudgeTimeModel != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(inactivityNudgeTimeModel);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0213a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0213a aVar = new C0213a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0213a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.j.runInTransaction(new RunnableC0214a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(s26 s26, qn7 qn7) {
            super(2, qn7);
            this.this$0 = s26;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                C0213a aVar = new C0213a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2", f = "InactivityNudgeTimePresenter.kt", l = {115}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s26$b$a$a")
            /* renamed from: com.fossil.s26$b$a$a  reason: collision with other inner class name */
            public static final class RunnableC0215a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ a b;

                @DexIgnore
                public RunnableC0215a(a aVar) {
                    this.b = aVar;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.b.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel inactivityNudgeTimeModel = this.b.this$0.this$0.h;
                    if (inactivityNudgeTimeModel != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(inactivityNudgeTimeModel);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.j.runInTransaction(new RunnableC0215a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(s26 s26, qn7 qn7) {
            super(2, qn7);
            this.this$0 = s26;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1", f = "InactivityNudgeTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1$listInactivityNudgeTimeModel$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends InactivityNudgeTimeModel>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.j.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(s26 s26, qn7 qn7) {
            super(2, qn7);
            this.this$0 = s26;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel : (List) g) {
                if (inactivityNudgeTimeModel.getNudgeTimeType() == this.this$0.f) {
                    boolean is24HourFormat = DateFormat.is24HourFormat(PortfolioApp.h0.c());
                    this.this$0.i.B5(is24HourFormat);
                    q26 q26 = this.this$0.i;
                    s26 s26 = this.this$0;
                    q26.t(s26.z(s26.f));
                    this.this$0.i.h6(inactivityNudgeTimeModel.getMinutes(), is24HourFormat);
                }
                if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                    this.this$0.g = inactivityNudgeTimeModel;
                } else {
                    this.this$0.h = inactivityNudgeTimeModel;
                }
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = s26.class.getSimpleName();
        pq7.b(simpleName, "InactivityNudgeTimePrese\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public s26(q26 q26, RemindersSettingsDatabase remindersSettingsDatabase) {
        pq7.c(q26, "mView");
        pq7.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.i = q26;
        this.j = remindersSettingsDatabase;
    }

    @DexIgnore
    public void A() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.p26
    public void n() {
        if (this.f == 0) {
            int i2 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel = this.h;
            if (inactivityNudgeTimeModel == null) {
                pq7.i();
                throw null;
            } else if (i2 > inactivityNudgeTimeModel.getMinutes()) {
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886115);
                q26 q26 = this.i;
                pq7.b(c2, "des");
                q26.Y0(c2);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = this.g;
                if (inactivityNudgeTimeModel2 != null) {
                    inactivityNudgeTimeModel2.setMinutes(this.e);
                    xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
                    return;
                }
                pq7.i();
                throw null;
            }
        } else {
            int i3 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel3 = this.g;
            if (inactivityNudgeTimeModel3 == null) {
                pq7.i();
                throw null;
            } else if (i3 < inactivityNudgeTimeModel3.getMinutes()) {
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886115);
                q26 q262 = this.i;
                pq7.b(c3, "des");
                q262.Y0(c3);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel4 = this.h;
                if (inactivityNudgeTimeModel4 != null) {
                    inactivityNudgeTimeModel4.setMinutes(this.e);
                    xw7 unused2 = gu7.d(k(), null, null, new b(this, null), 3, null);
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.p26
    public void o(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x005f  */
    @Override // com.fossil.p26
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void p(java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r3 = 12
            r1 = 0
            java.lang.String r0 = "hourValue"
            com.fossil.pq7.c(r9, r0)
            java.lang.String r0 = "minuteValue"
            com.fossil.pq7.c(r10, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.s26.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateTime: hourValue = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r5 = ", minuteValue = "
            r4.append(r5)
            r4.append(r10)
            java.lang.String r5 = ", isPM = "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r0.d(r2, r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r2 = "Integer.valueOf(hourValue)"
            com.fossil.pq7.b(r0, r2)     // Catch:{ Exception -> 0x0065 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0065 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r4 = "Integer.valueOf(minuteValue)"
            com.fossil.pq7.b(r2, r4)     // Catch:{ Exception -> 0x0097 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0097 }
        L_0x0053:
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            boolean r4 = android.text.format.DateFormat.is24HourFormat(r4)
            if (r4 == 0) goto L_0x0085
            int r0 = r0 * 60
            int r0 = r0 + r2
            r8.e = r0
        L_0x0064:
            return
        L_0x0065:
            r2 = move-exception
            r0 = r1
        L_0x0067:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.s26.k
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Exception when parse time e="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r2 = r6.toString()
            r4.e(r5, r2)
            r2 = r1
            goto L_0x0053
        L_0x0085:
            if (r11 == 0) goto L_0x0093
            if (r0 != r3) goto L_0x0090
            r1 = r3
        L_0x008a:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
            goto L_0x0064
        L_0x0090:
            int r1 = r0 + 12
            goto L_0x008a
        L_0x0093:
            if (r0 == r3) goto L_0x008a
            r1 = r0
            goto L_0x008a
        L_0x0097:
            r2 = move-exception
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.s26.p(java.lang.String, java.lang.String, boolean):void");
    }

    @DexIgnore
    public final String z(int i2) {
        if (i2 == 0) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886122);
            pq7.b(c2, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return c2;
        }
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886120);
        pq7.b(c3, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return c3;
    }
}
