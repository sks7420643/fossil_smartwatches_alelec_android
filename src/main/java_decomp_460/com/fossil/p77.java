package com.fossil;

import com.portfolio.platform.data.model.watchface.MetaData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p77 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2796a;
    @DexIgnore
    @rj4("category")
    public String b;
    @DexIgnore
    @rj4("name")
    public String c;
    @DexIgnore
    @rj4("data")
    public q77 d;
    @DexIgnore
    @rj4("_isNew")
    public boolean e;
    @DexIgnore
    @rj4("metadata")
    public MetaData f;
    @DexIgnore
    public l77 g;

    @DexIgnore
    public p77(String str, String str2, String str3, q77 q77, boolean z, MetaData metaData, l77 l77) {
        pq7.c(str, "id");
        pq7.c(str2, "category");
        pq7.c(str3, "name");
        pq7.c(q77, "data");
        pq7.c(l77, "assetType");
        this.f2796a = str;
        this.b = str2;
        this.c = str3;
        this.d = q77;
        this.e = z;
        this.f = metaData;
        this.g = l77;
    }

    @DexIgnore
    public final l77 a() {
        return this.g;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final q77 c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.f2796a;
    }

    @DexIgnore
    public final MetaData e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof p77) {
                p77 p77 = (p77) obj;
                if (!pq7.a(this.f2796a, p77.f2796a) || !pq7.a(this.b, p77.b) || !pq7.a(this.c, p77.c) || !pq7.a(this.d, p77.d) || this.e != p77.e || !pq7.a(this.f, p77.f) || !pq7.a(this.g, p77.g)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final boolean g() {
        return this.e;
    }

    @DexIgnore
    public final void h(l77 l77) {
        pq7.c(l77, "<set-?>");
        this.g = l77;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2796a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        q77 q77 = this.d;
        int hashCode4 = q77 != null ? q77.hashCode() : 0;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        MetaData metaData = this.f;
        int hashCode5 = metaData != null ? metaData.hashCode() : 0;
        l77 l77 = this.g;
        if (l77 != null) {
            i = l77.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WFAsset(id=" + this.f2796a + ", category=" + this.b + ", name=" + this.c + ", data=" + this.d + ", isNew=" + this.e + ", meta=" + this.f + ", assetType=" + this.g + ")";
    }
}
