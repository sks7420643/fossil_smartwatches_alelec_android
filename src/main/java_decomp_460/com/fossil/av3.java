package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av3 extends zc2 implements qu3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<av3> CREATOR; // = new bv3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ List<pv3> c;

    @DexIgnore
    public av3(String str, List<pv3> list) {
        this.b = str;
        this.c = list;
        rc2.k(str);
        rc2.k(this.c);
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || av3.class != obj.getClass()) {
            return false;
        }
        av3 av3 = (av3) obj;
        String str = this.b;
        if (str == null ? av3.b != null : !str.equals(av3.b)) {
            return false;
        }
        List<pv3> list = this.c;
        List<pv3> list2 = av3.c;
        if (list != null) {
            if (list.equals(list2)) {
                return true;
            }
        } else if (list2 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        List<pv3> list = this.c;
        if (list != null) {
            i = list.hashCode();
        }
        return ((hashCode + 31) * 31) + i;
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 2, c(), false);
        bd2.y(parcel, 3, this.c, false);
        bd2.b(parcel, a2);
    }
}
