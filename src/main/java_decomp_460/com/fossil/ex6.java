package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex6 implements MembersInjector<cx6> {
    @DexIgnore
    public static void a(cx6 cx6, ck5 ck5) {
        cx6.h = ck5;
    }

    @DexIgnore
    public static void b(cx6 cx6, vu5 vu5) {
        cx6.g = vu5;
    }

    @DexIgnore
    public static void c(cx6 cx6, jv5 jv5) {
        cx6.e = jv5;
    }

    @DexIgnore
    public static void d(cx6 cx6, kv5 kv5) {
        cx6.f = kv5;
    }

    @DexIgnore
    public static void e(cx6 cx6) {
        cx6.k0();
    }
}
