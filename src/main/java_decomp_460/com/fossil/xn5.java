package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xn5 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public SsoHandler f4146a;
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return xn5.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ xn5 b;
        @DexIgnore
        public /* final */ /* synthetic */ yn5 c;

        @DexIgnore
        public b(xn5 xn5, yn5 yn5) {
            this.b = xn5;
            this.c = yn5;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.c()) {
                this.c.b(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WbAuthListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xn5 f4147a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ yn5 c;

        @DexIgnore
        public c(xn5 xn5, Handler handler, yn5 yn5) {
            this.f4147a = xn5;
            this.b = handler;
            this.c = yn5;
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void cancel() {
            FLogger.INSTANCE.getLocal().d(xn5.d.a(), "Weibo loginWithEmail cancel");
            this.f4147a.e(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.b(2, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onFailure(WbConnectErrorMessage wbConnectErrorMessage) {
            pq7.c(wbConnectErrorMessage, "wbConnectErrorMessage");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xn5.d.a();
            local.d(a2, "Weibo loginWithEmail failed - message: " + wbConnectErrorMessage.getErrorMessage() + "- code: " + wbConnectErrorMessage.getErrorCode());
            this.f4147a.e(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.b(600, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onSuccess(Oauth2AccessToken oauth2AccessToken) {
            pq7.c(oauth2AccessToken, "oauth2AccessToken");
            this.f4147a.e(true);
            this.b.removeCallbacksAndMessages(null);
            String token = oauth2AccessToken.getToken();
            String format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).format(new Date(oauth2AccessToken.getExpiresTime()));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xn5.d.a();
            local.d(a2, "Get access token from weibo success: " + token + " ,expire date: " + format);
            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
            signUpSocialAuth.setClientId(dk5.g.a(""));
            pq7.b(token, "authToken");
            signUpSocialAuth.setToken(token);
            signUpSocialAuth.setService("weibo");
            this.c.a(signUpSocialAuth);
        }
    }

    /*
    static {
        String simpleName = xn5.class.getSimpleName();
        pq7.b(simpleName, "MFLoginWeiboManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final void b(int i, int i2, Intent intent) {
        pq7.c(intent, "data");
        SsoHandler ssoHandler = this.f4146a;
        if (ssoHandler == null) {
            return;
        }
        if (ssoHandler != null) {
            ssoHandler.authorizeCallBack(i, i2, intent);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final boolean c() {
        return this.b;
    }

    @DexIgnore
    public final void d(Activity activity, yn5 yn5) {
        pq7.c(activity, Constants.ACTIVITY);
        pq7.c(yn5, Constants.CALLBACK);
        try {
            this.b = false;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new b(this, yn5), 60000);
            SsoHandler ssoHandler = new SsoHandler(activity);
            this.f4146a = ssoHandler;
            if (ssoHandler != null) {
                ssoHandler.authorize(new c(this, handler, yn5));
            } else {
                pq7.i();
                throw null;
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "Weibo loginWithEmail failed - ex=" + e);
            yn5.b(600, null, "");
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3) {
        pq7.c(str, "apiKey");
        pq7.c(str2, "redirectUrl");
        pq7.c(str3, "scope");
        try {
            WbSdk.install(PortfolioApp.h0.c(), new AuthInfo(PortfolioApp.h0.c(), str, str2, str3));
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(c, "exception when install WBSdk");
        }
    }
}
