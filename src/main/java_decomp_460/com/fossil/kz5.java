package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz5 implements Factory<jz5> {
    @DexIgnore
    public static jz5 a(zy5 zy5, on5 on5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, yt5 yt5, uq4 uq4, mj5 mj5, bv5 bv5, n47 n47, ServerSettingRepository serverSettingRepository, ou5 ou5, pr4 pr4) {
        return new jz5(zy5, on5, deviceRepository, portfolioApp, yt5, uq4, mj5, bv5, n47, serverSettingRepository, ou5, pr4);
    }
}
