package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j03 {
    @DexIgnore
    public j03() {
    }

    @DexIgnore
    public static long a(long j) {
        return (-(1 & j)) ^ (j >>> 1);
    }

    @DexIgnore
    public static j03 b(byte[] bArr, int i, int i2, boolean z) {
        k03 k03 = new k03(bArr, 0, i2, false);
        try {
            k03.d(i2);
            return k03;
        } catch (l13 e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public static int c(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }
}
