package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wi0 {
    @DexIgnore
    void a(vi0 vi0, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    void b(vi0 vi0, float f);

    @DexIgnore
    float c(vi0 vi0);

    @DexIgnore
    float d(vi0 vi0);

    @DexIgnore
    void e(vi0 vi0);

    @DexIgnore
    void f(vi0 vi0, float f);

    @DexIgnore
    float g(vi0 vi0);

    @DexIgnore
    ColorStateList h(vi0 vi0);

    @DexIgnore
    void i(vi0 vi0);

    @DexIgnore
    Object j();  // void declaration

    @DexIgnore
    float k(vi0 vi0);

    @DexIgnore
    float l(vi0 vi0);

    @DexIgnore
    void m(vi0 vi0);

    @DexIgnore
    void n(vi0 vi0, ColorStateList colorStateList);

    @DexIgnore
    void o(vi0 vi0, float f);
}
