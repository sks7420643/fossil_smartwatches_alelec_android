package com.fossil;

import android.os.Parcel;
import com.fossil.ix1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m2 extends c2 {
    @DexIgnore
    public static /* final */ l2 CREATOR; // = new l2(null);
    @DexIgnore
    public /* final */ gu1 e;
    @DexIgnore
    public /* final */ fu1 f;
    @DexIgnore
    public /* final */ iu1 g;
    @DexIgnore
    public /* final */ byte[] h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore
    public m2(byte b, gu1 gu1, fu1 fu1, iu1 iu1, byte[] bArr, int i2, int i3) {
        super(lt.ENCRYPTED_DATA, b, true);
        this.e = gu1;
        this.f = fu1;
        this.g = iu1;
        this.h = bArr;
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public m2(Parcel parcel) {
        super(parcel);
        gu1 a2 = gu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.e = a2;
            fu1 a3 = fu1.d.a(parcel.readByte());
            if (a3 != null) {
                this.f = a3;
                iu1 a4 = iu1.d.a(parcel.readByte());
                if (a4 != null) {
                    this.g = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.h = createByteArray == null ? new byte[0] : createByteArray;
                    this.i = parcel.readInt();
                    this.j = parcel.readInt();
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(super.toJSONObject(), jd0.j0, ey1.a(this.e)), jd0.k5, ey1.a(this.f)), jd0.l5, ey1.a(this.g)), jd0.U0, Long.valueOf(ix1.f1688a.b(this.h, ix1.a.CRC32))), jd0.z5, Integer.valueOf(this.i)), jd0.A5, Integer.valueOf(this.j));
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i);
        }
        if (parcel != null) {
            parcel.writeInt(this.j);
        }
    }
}
