package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.fossil.pd7;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qd7 {
    @DexIgnore
    public static /* final */ AtomicInteger m; // = new AtomicInteger();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Picasso f2964a;
    @DexIgnore
    public /* final */ pd7.b b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Drawable j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Object l;

    @DexIgnore
    public qd7(Picasso picasso, Uri uri, int i2) {
        if (!picasso.o) {
            this.f2964a = picasso;
            this.b = new pd7.b(uri, i2, picasso.l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    @DexIgnore
    public final pd7 a(long j2) {
        int andIncrement = m.getAndIncrement();
        pd7 a2 = this.b.a();
        a2.f2819a = andIncrement;
        a2.b = j2;
        if (this.f2964a.n) {
            xd7.v("Main", "created", a2.g(), a2.toString());
        }
        this.f2964a.q(a2);
        return a2;
    }

    @DexIgnore
    public qd7 b(int i2) {
        if (i2 == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        } else if (this.k == null) {
            this.g = i2;
            return this;
        } else {
            throw new IllegalStateException("Error image already set.");
        }
    }

    @DexIgnore
    public final Drawable c() {
        return this.f != 0 ? this.f2964a.e.getResources().getDrawable(this.f) : this.j;
    }

    @DexIgnore
    public void d(ImageView imageView) {
        e(imageView, null);
    }

    @DexIgnore
    public void e(ImageView imageView, zc7 zc7) {
        Bitmap n;
        long nanoTime = System.nanoTime();
        xd7.c();
        if (imageView == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.b.b()) {
            this.f2964a.c(imageView);
            if (this.e) {
                nd7.d(imageView, c());
            }
        } else {
            if (this.d) {
                if (!this.b.c()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width == 0 || height == 0) {
                        if (this.e) {
                            nd7.d(imageView, c());
                        }
                        this.f2964a.f(imageView, new cd7(this, imageView, zc7));
                        return;
                    }
                    this.b.d(width, height);
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            pd7 a2 = a(nanoTime);
            String h2 = xd7.h(a2);
            if (!jd7.shouldReadFromMemoryCache(this.h) || (n = this.f2964a.n(h2)) == null) {
                if (this.e) {
                    nd7.d(imageView, c());
                }
                this.f2964a.h(new fd7(this.f2964a, imageView, a2, this.h, this.i, this.g, this.k, h2, this.l, zc7, this.c));
                return;
            }
            this.f2964a.c(imageView);
            Picasso picasso = this.f2964a;
            nd7.c(imageView, picasso.e, n, Picasso.LoadedFrom.MEMORY, this.c, picasso.m);
            if (this.f2964a.n) {
                String g2 = a2.g();
                xd7.v("Main", "completed", g2, "from " + Picasso.LoadedFrom.MEMORY);
            }
            if (zc7 != null) {
                zc7.onSuccess();
            }
        }
    }

    @DexIgnore
    public void f(Target target) {
        Bitmap n;
        Drawable drawable = null;
        long nanoTime = System.nanoTime();
        xd7.c();
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (this.d) {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        } else if (!this.b.b()) {
            this.f2964a.d(target);
            if (this.e) {
                drawable = c();
            }
            target.onPrepareLoad(drawable);
        } else {
            pd7 a2 = a(nanoTime);
            String h2 = xd7.h(a2);
            if (!jd7.shouldReadFromMemoryCache(this.h) || (n = this.f2964a.n(h2)) == null) {
                if (this.e) {
                    drawable = c();
                }
                target.onPrepareLoad(drawable);
                this.f2964a.h(new vd7(this.f2964a, target, a2, this.h, this.i, this.k, h2, this.l, this.g));
                return;
            }
            this.f2964a.d(target);
            target.onBitmapLoaded(n, Picasso.LoadedFrom.MEMORY);
        }
    }

    @DexIgnore
    public qd7 g() {
        this.c = true;
        return this;
    }

    @DexIgnore
    public qd7 h(int i2) {
        if (!this.e) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.j == null) {
            this.f = i2;
            return this;
        } else {
            throw new IllegalStateException("Placeholder image already set.");
        }
    }

    @DexIgnore
    public qd7 i(int i2, int i3) {
        this.b.d(i2, i3);
        return this;
    }

    @DexIgnore
    public qd7 j(Transformation transformation) {
        this.b.e(transformation);
        return this;
    }

    @DexIgnore
    public qd7 k() {
        this.d = false;
        return this;
    }
}
