package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ak4<K, V> extends AbstractMap<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
    @DexIgnore
    public static /* final */ Comparator<Comparable> b; // = new a();
    @DexIgnore
    public Comparator<? super K> comparator;
    @DexIgnore
    public ak4<K, V>.b entrySet;
    @DexIgnore
    public /* final */ e<K, V> header;
    @DexIgnore
    public ak4<K, V>.c keySet;
    @DexIgnore
    public int modCount;
    @DexIgnore
    public e<K, V> root;
    @DexIgnore
    public int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Comparator<Comparable> {
        @DexIgnore
        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AbstractSet<Map.Entry<K, V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ak4<K, V>.d {
            @DexIgnore
            public a(b bVar) {
                super();
            }

            @DexIgnore
            /* renamed from: b */
            public Map.Entry<K, V> next() {
                return a();
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void clear() {
            ak4.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && ak4.this.findByEntry((Map.Entry) obj) != null;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new a(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e<K, V> findByEntry;
            if (!(obj instanceof Map.Entry) || (findByEntry = ak4.this.findByEntry((Map.Entry) obj)) == null) {
                return false;
            }
            ak4.this.removeInternal(findByEntry, true);
            return true;
        }

        @DexIgnore
        public int size() {
            return ak4.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AbstractSet<K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ak4<K, V>.d {
            @DexIgnore
            public a(c cVar) {
                super();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public K next() {
                return a().g;
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void clear() {
            ak4.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return ak4.this.containsKey(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new a(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return ak4.this.removeInternalByKey(obj) != null;
        }

        @DexIgnore
        public int size() {
            return ak4.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class d<T> implements Iterator<T> {
        @DexIgnore
        public e<K, V> b;
        @DexIgnore
        public e<K, V> c; // = null;
        @DexIgnore
        public int d;

        @DexIgnore
        public d() {
            ak4 ak4 = ak4.this;
            this.b = ak4.header.e;
            this.d = ak4.modCount;
        }

        @DexIgnore
        public final e<K, V> a() {
            e<K, V> eVar = this.b;
            ak4 ak4 = ak4.this;
            if (eVar == ak4.header) {
                throw new NoSuchElementException();
            } else if (ak4.modCount == this.d) {
                this.b = eVar.e;
                this.c = eVar;
                return eVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        @DexIgnore
        public final boolean hasNext() {
            return this.b != ak4.this.header;
        }

        @DexIgnore
        public final void remove() {
            e<K, V> eVar = this.c;
            if (eVar != null) {
                ak4.this.removeInternal(eVar, true);
                this.c = null;
                this.d = ak4.this.modCount;
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public e<K, V> b;
        @DexIgnore
        public e<K, V> c;
        @DexIgnore
        public e<K, V> d;
        @DexIgnore
        public e<K, V> e;
        @DexIgnore
        public e<K, V> f;
        @DexIgnore
        public /* final */ K g;
        @DexIgnore
        public V h;
        @DexIgnore
        public int i;

        @DexIgnore
        public e() {
            this.g = null;
            this.f = this;
            this.e = this;
        }

        @DexIgnore
        public e(e<K, V> eVar, K k, e<K, V> eVar2, e<K, V> eVar3) {
            this.b = eVar;
            this.g = k;
            this.i = 1;
            this.e = eVar2;
            this.f = eVar3;
            eVar3.e = this;
            eVar2.f = this;
        }

        @DexIgnore
        public e<K, V> a() {
            for (e<K, V> eVar = this.c; eVar != null; eVar = eVar.c) {
                this = eVar;
            }
            return this;
        }

        @DexIgnore
        public e<K, V> b() {
            for (e<K, V> eVar = this.d; eVar != null; eVar = eVar.d) {
                this = eVar;
            }
            return this;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r1 = r4 instanceof java.util.Map.Entry
                r0 = 0
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.g
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.h
                if (r1 != 0) goto L_0x0028
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x0028:
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ak4.e.equals(java.lang.Object):boolean");
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            return this.g;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            K k = this.g;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.h;
            if (v != null) {
                i2 = v.hashCode();
            }
            return hashCode ^ i2;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            V v2 = this.h;
            this.h = v;
            return v2;
        }

        @DexIgnore
        public String toString() {
            return ((Object) this.g) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) this.h);
        }
    }

    @DexIgnore
    public ak4() {
        this(b);
    }

    @DexIgnore
    public ak4(Comparator<? super K> comparator2) {
        this.size = 0;
        this.modCount = 0;
        this.header = new e<>();
        this.comparator = comparator2 == null ? b : comparator2;
    }

    @DexIgnore
    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    @DexIgnore
    public final boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public final void b(e<K, V> eVar, boolean z) {
        while (eVar != null) {
            e<K, V> eVar2 = eVar.c;
            e<K, V> eVar3 = eVar.d;
            int i = eVar2 != null ? eVar2.i : 0;
            int i2 = eVar3 != null ? eVar3.i : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                e<K, V> eVar4 = eVar3.c;
                e<K, V> eVar5 = eVar3.d;
                int i4 = (eVar4 != null ? eVar4.i : 0) - (eVar5 != null ? eVar5.i : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    e(eVar);
                } else {
                    f(eVar3);
                    e(eVar);
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                e<K, V> eVar6 = eVar2.c;
                e<K, V> eVar7 = eVar2.d;
                int i5 = (eVar6 != null ? eVar6.i : 0) - (eVar7 != null ? eVar7.i : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    f(eVar);
                } else {
                    e(eVar2);
                    f(eVar);
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                eVar.i = i + 1;
                if (z) {
                    return;
                }
            } else {
                eVar.i = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            }
            eVar = eVar.b;
        }
    }

    @DexIgnore
    public final void c(e<K, V> eVar, e<K, V> eVar2) {
        e<K, V> eVar3 = eVar.b;
        eVar.b = null;
        if (eVar2 != null) {
            eVar2.b = eVar3;
        }
        if (eVar3 == null) {
            this.root = eVar2;
        } else if (eVar3.c == eVar) {
            eVar3.c = eVar2;
        } else {
            eVar3.d = eVar2;
        }
    }

    @DexIgnore
    public void clear() {
        this.root = null;
        this.size = 0;
        this.modCount++;
        e<K, V> eVar = this.header;
        eVar.f = eVar;
        eVar.e = eVar;
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return findByObject(obj) != null;
    }

    @DexIgnore
    public final void e(e<K, V> eVar) {
        int i = 0;
        e<K, V> eVar2 = eVar.c;
        e<K, V> eVar3 = eVar.d;
        e<K, V> eVar4 = eVar3.c;
        e<K, V> eVar5 = eVar3.d;
        eVar.d = eVar4;
        if (eVar4 != null) {
            eVar4.b = eVar;
        }
        c(eVar, eVar3);
        eVar3.c = eVar;
        eVar.b = eVar3;
        int max = Math.max(eVar2 != null ? eVar2.i : 0, eVar4 != null ? eVar4.i : 0) + 1;
        eVar.i = max;
        if (eVar5 != null) {
            i = eVar5.i;
        }
        eVar3.i = Math.max(max, i) + 1;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        ak4<K, V>.b bVar = this.entrySet;
        if (bVar != null) {
            return bVar;
        }
        ak4<K, V>.b bVar2 = new b();
        this.entrySet = bVar2;
        return bVar2;
    }

    @DexIgnore
    public final void f(e<K, V> eVar) {
        int i = 0;
        e<K, V> eVar2 = eVar.c;
        e<K, V> eVar3 = eVar.d;
        e<K, V> eVar4 = eVar2.c;
        e<K, V> eVar5 = eVar2.d;
        eVar.c = eVar5;
        if (eVar5 != null) {
            eVar5.b = eVar;
        }
        c(eVar, eVar2);
        eVar2.d = eVar;
        eVar.b = eVar2;
        int max = Math.max(eVar3 != null ? eVar3.i : 0, eVar5 != null ? eVar5.i : 0) + 1;
        eVar.i = max;
        if (eVar4 != null) {
            i = eVar4.i;
        }
        eVar2.i = Math.max(max, i) + 1;
    }

    @DexIgnore
    public e<K, V> find(K k, boolean z) {
        int i;
        e<K, V> eVar;
        Comparator<? super K> comparator2 = this.comparator;
        e<K, V> eVar2 = this.root;
        if (eVar2 != null) {
            K k2 = comparator2 == b ? k : null;
            while (true) {
                int compareTo = k2 != null ? k2.compareTo(eVar2.g) : comparator2.compare(k, eVar2.g);
                if (compareTo == 0) {
                    return eVar2;
                }
                e<K, V> eVar3 = compareTo < 0 ? eVar2.c : eVar2.d;
                if (eVar3 == null) {
                    i = compareTo;
                    break;
                }
                eVar2 = eVar3;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        e<K, V> eVar4 = this.header;
        if (eVar2 != null) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.f);
            if (i < 0) {
                eVar2.c = eVar;
            } else {
                eVar2.d = eVar;
            }
            b(eVar2, true);
        } else if (comparator2 != b || (k instanceof Comparable)) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.f);
            this.root = eVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.size++;
        this.modCount++;
        return eVar;
    }

    @DexIgnore
    public e<K, V> findByEntry(Map.Entry<?, ?> entry) {
        e<K, V> findByObject = findByObject(entry.getKey());
        if (findByObject != null && a(findByObject.h, entry.getValue())) {
            return findByObject;
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public e<K, V> findByObject(Object obj) {
        if (obj == 0) {
            return null;
        }
        try {
            return find(obj, false);
        } catch (ClassCastException e2) {
            return null;
        }
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        e<K, V> findByObject = findByObject(obj);
        if (findByObject != null) {
            return findByObject.h;
        }
        return null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        ak4<K, V>.c cVar = this.keySet;
        if (cVar != null) {
            return cVar;
        }
        ak4<K, V>.c cVar2 = new c();
        this.keySet = cVar2;
        return cVar2;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V put(K k, V v) {
        if (k != null) {
            e<K, V> find = find(k, true);
            V v2 = find.h;
            find.h = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        e<K, V> removeInternalByKey = removeInternalByKey(obj);
        if (removeInternalByKey != null) {
            return removeInternalByKey.h;
        }
        return null;
    }

    @DexIgnore
    public void removeInternal(e<K, V> eVar, boolean z) {
        int i;
        int i2 = 0;
        if (z) {
            e<K, V> eVar2 = eVar.f;
            eVar2.e = eVar.e;
            eVar.e.f = eVar2;
        }
        e<K, V> eVar3 = eVar.c;
        e<K, V> eVar4 = eVar.d;
        e<K, V> eVar5 = eVar.b;
        if (eVar3 == null || eVar4 == null) {
            if (eVar3 != null) {
                c(eVar, eVar3);
                eVar.c = null;
            } else if (eVar4 != null) {
                c(eVar, eVar4);
                eVar.d = null;
            } else {
                c(eVar, null);
            }
            b(eVar5, false);
            this.size--;
            this.modCount++;
            return;
        }
        e<K, V> b2 = eVar3.i > eVar4.i ? eVar3.b() : eVar4.a();
        removeInternal(b2, false);
        e<K, V> eVar6 = eVar.c;
        if (eVar6 != null) {
            i = eVar6.i;
            b2.c = eVar6;
            eVar6.b = b2;
            eVar.c = null;
        } else {
            i = 0;
        }
        e<K, V> eVar7 = eVar.d;
        if (eVar7 != null) {
            i2 = eVar7.i;
            b2.d = eVar7;
            eVar7.b = b2;
            eVar.d = null;
        }
        b2.i = Math.max(i, i2) + 1;
        c(eVar, b2);
    }

    @DexIgnore
    public e<K, V> removeInternalByKey(Object obj) {
        e<K, V> findByObject = findByObject(obj);
        if (findByObject != null) {
            removeInternal(findByObject, true);
        }
        return findByObject;
    }

    @DexIgnore
    public int size() {
        return this.size;
    }
}
