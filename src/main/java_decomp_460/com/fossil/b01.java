package com.fossil;

import android.os.Parcelable;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zi0<String, Method> f374a;
    @DexIgnore
    public /* final */ zi0<String, Method> b;
    @DexIgnore
    public /* final */ zi0<String, Class> c;

    @DexIgnore
    public b01(zi0<String, Method> zi0, zi0<String, Method> zi02, zi0<String, Class> zi03) {
        this.f374a = zi0;
        this.b = zi02;
        this.c = zi03;
    }

    @DexIgnore
    public abstract void A(byte[] bArr);

    @DexIgnore
    public void B(byte[] bArr, int i) {
        w(i);
        A(bArr);
    }

    @DexIgnore
    public abstract void C(CharSequence charSequence);

    @DexIgnore
    public void D(CharSequence charSequence, int i) {
        w(i);
        C(charSequence);
    }

    @DexIgnore
    public abstract void E(int i);

    @DexIgnore
    public void F(int i, int i2) {
        w(i2);
        E(i);
    }

    @DexIgnore
    public abstract void G(Parcelable parcelable);

    @DexIgnore
    public void H(Parcelable parcelable, int i) {
        w(i);
        G(parcelable);
    }

    @DexIgnore
    public abstract void I(String str);

    @DexIgnore
    public void J(String str, int i) {
        w(i);
        I(str);
    }

    @DexIgnore
    public <T extends d01> void K(T t, b01 b01) {
        try {
            e(t.getClass()).invoke(null, t, b01);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public void L(d01 d01) {
        if (d01 == null) {
            I(null);
            return;
        }
        N(d01);
        b01 b2 = b();
        K(d01, b2);
        b2.a();
    }

    @DexIgnore
    public void M(d01 d01, int i) {
        w(i);
        L(d01);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.b01 */
    /* JADX WARN: Multi-variable type inference failed */
    public final void N(d01 d01) {
        try {
            I(c(d01.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(d01.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract b01 b();

    @DexIgnore
    public final Class c(Class<? extends d01> cls) throws ClassNotFoundException {
        Class cls2 = this.c.get(cls.getName());
        if (cls2 != null) {
            return cls2;
        }
        Class<?> cls3 = Class.forName(String.format("%s.%sParcelizer", cls.getPackage().getName(), cls.getSimpleName()), false, cls.getClassLoader());
        this.c.put(cls.getName(), cls3);
        return cls3;
    }

    @DexIgnore
    public final Method d(String str) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.f374a.get(str);
        if (method != null) {
            return method;
        }
        System.currentTimeMillis();
        Method declaredMethod = Class.forName(str, true, b01.class.getClassLoader()).getDeclaredMethod(HardwareLog.COLUMN_READ, b01.class);
        this.f374a.put(str, declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public final Method e(Class cls) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.b.get(cls.getName());
        if (method != null) {
            return method;
        }
        Class c2 = c(cls);
        System.currentTimeMillis();
        Method declaredMethod = c2.getDeclaredMethod("write", cls, b01.class);
        this.b.put(cls.getName(), declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public boolean f() {
        return false;
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public boolean h(boolean z, int i) {
        return !m(i) ? z : g();
    }

    @DexIgnore
    public abstract byte[] i();

    @DexIgnore
    public byte[] j(byte[] bArr, int i) {
        return !m(i) ? bArr : i();
    }

    @DexIgnore
    public abstract CharSequence k();

    @DexIgnore
    public CharSequence l(CharSequence charSequence, int i) {
        return !m(i) ? charSequence : k();
    }

    @DexIgnore
    public abstract boolean m(int i);

    @DexIgnore
    public <T extends d01> T n(String str, b01 b01) {
        try {
            return (T) ((d01) d(str).invoke(null, b01));
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public abstract int o();

    @DexIgnore
    public int p(int i, int i2) {
        return !m(i2) ? i : o();
    }

    @DexIgnore
    public abstract <T extends Parcelable> T q();

    @DexIgnore
    public <T extends Parcelable> T r(T t, int i) {
        return !m(i) ? t : (T) q();
    }

    @DexIgnore
    public abstract String s();

    @DexIgnore
    public String t(String str, int i) {
        return !m(i) ? str : s();
    }

    @DexIgnore
    public <T extends d01> T u() {
        String s = s();
        if (s == null) {
            return null;
        }
        return (T) n(s, b());
    }

    @DexIgnore
    public <T extends d01> T v(T t, int i) {
        return !m(i) ? t : (T) u();
    }

    @DexIgnore
    public abstract void w(int i);

    @DexIgnore
    public void x(boolean z, boolean z2) {
    }

    @DexIgnore
    public abstract void y(boolean z);

    @DexIgnore
    public void z(boolean z, int i) {
        w(i);
        y(z);
    }
}
