package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb6 extends ab6 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ bb6 k;
    @DexIgnore
    public /* final */ WatchAppRepository l;
    @DexIgnore
    public /* final */ on5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fb6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fb6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
        /* renamed from: com.fossil.fb6$a$a  reason: collision with other inner class name */
        public static final class C0083a extends ko7 implements vp7<iv7, qn7<? super List<WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0083a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0083a aVar = new C0083a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<WatchApp>> qn7) {
                return ((C0083a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object queryWatchAppByName;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    String str = this.this$0.$query;
                    this.L$0 = iv7;
                    this.label = 1;
                    queryWatchAppByName = watchAppRepository.queryWatchAppByName(str, this);
                    if (queryWatchAppByName == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    queryWatchAppByName = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return pm7.j0((Collection) queryWatchAppByName);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fb6 fb6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fb6;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$query, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            List arrayList;
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                arrayList = new ArrayList();
                if (this.$query.length() > 0) {
                    dv7 i2 = this.this$0.i();
                    C0083a aVar = new C0083a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    g = eu7.g(i2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                }
                this.this$0.k.B(this.this$0.C(arrayList));
                this.this$0.h = this.$query;
                return tl7.f3441a;
            } else if (i == 1) {
                List list = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList = (List) g;
            this.this$0.k.B(this.this$0.C(arrayList));
            this.this$0.h = this.$query;
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1", f = "WatchAppSearchPresenter.kt", l = {40, 43}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allSearchedWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {43}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends WatchApp>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    List<String> T = this.this$0.this$0.m.T();
                    pq7.b(T, "sharedPreferencesManager.watchAppSearchedIdsRecent");
                    this.L$0 = iv7;
                    this.label = 1;
                    Object watchAppByIds = watchAppRepository.getWatchAppByIds(T, this);
                    return watchAppByIds == d ? d : watchAppByIds;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fb6$b$b")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {40}, m = "invokeSuspend")
        /* renamed from: com.fossil.fb6$b$b  reason: collision with other inner class name */
        public static final class C0084b extends ko7 implements vp7<iv7, qn7<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0084b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0084b bVar = new C0084b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends WatchApp>> qn7) {
                return ((C0084b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object allWatchAppRaw = watchAppRepository.getAllWatchAppRaw(this);
                    return allWatchAppRaw == d ? d : allWatchAppRaw;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fb6 fb6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0074  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0076
                if (r0 == r4) goto L_0x003f
                if (r0 != r5) goto L_0x0037
                java.lang.Object r0 = r7.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.List r0 = (java.util.List) r0
                com.fossil.fb6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.fb6.r(r1)
                r1.clear()
                com.fossil.fb6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.fb6.r(r1)
                r1.addAll(r0)
                com.fossil.fb6 r0 = r7.this$0
                com.fossil.fb6.w(r0)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0036:
                return r0
            L_0x0037:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003f:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0048:
                r0 = r1
                java.util.List r0 = (java.util.List) r0
                com.fossil.fb6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.fb6.s(r1)
                r1.clear()
                com.fossil.fb6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.fb6.s(r1)
                r1.addAll(r0)
                com.fossil.fb6 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.fb6.q(r1)
                com.fossil.fb6$b$a r4 = new com.fossil.fb6$b$a
                r4.<init>(r7, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0036
            L_0x0076:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.fb6 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.fb6.q(r1)
                com.fossil.fb6$b$b r2 = new com.fossil.fb6$b$b
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0092
                r0 = r3
                goto L_0x0036
            L_0x0092:
                r2 = r0
                goto L_0x0048
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.fb6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public fb6(bb6 bb6, WatchAppRepository watchAppRepository, on5 on5) {
        pq7.c(bb6, "mView");
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(on5, "sharedPreferencesManager");
        this.k = bb6;
        this.l = watchAppRepository;
        this.m = on5;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }

    @DexIgnore
    public void B() {
        this.k.M5(this);
    }

    @DexIgnore
    public final List<cl7<WatchApp, String>> C(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp watchApp : list) {
            if (!pq7.a(watchApp.getWatchappId(), "empty")) {
                String watchappId = watchApp.getWatchappId();
                if (pq7.a(watchappId, this.e)) {
                    arrayList.add(new cl7(watchApp, ViewHierarchy.DIMENSION_TOP_KEY));
                } else if (pq7.a(watchappId, this.f)) {
                    arrayList.add(new cl7(watchApp, "middle"));
                } else if (pq7.a(watchappId, this.g)) {
                    arrayList.add(new cl7(watchApp, "bottom"));
                } else {
                    arrayList.add(new cl7(watchApp, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void D(String str, String str2, String str3) {
        pq7.c(str, "watchAppTop");
        pq7.c(str2, "watchAppMiddle");
        pq7.c(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.ab6
    public void n() {
        this.h = "";
        this.k.D();
        z();
    }

    @DexIgnore
    @Override // com.fossil.ab6
    public void o(WatchApp watchApp) {
        pq7.c(watchApp, "selectedWatchApp");
        List<String> T = this.m.T();
        pq7.b(T, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!T.contains(watchApp.getWatchappId())) {
            T.add(0, watchApp.getWatchappId());
            if (T.size() > 5) {
                T = T.subList(0, 5);
            }
            this.m.W1(T);
        }
        this.k.D3(watchApp);
    }

    @DexIgnore
    @Override // com.fossil.ab6
    public void p(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        xw7 unused = gu7.d(k(), null, null, new a(this, str, null), 3, null);
    }

    @DexIgnore
    public final void z() {
        if (this.j.isEmpty()) {
            this.k.B(C(pm7.j0(this.i)));
        } else {
            this.k.K(C(pm7.j0(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            bb6 bb6 = this.k;
            String str = this.h;
            if (str != null) {
                bb6.z(str);
                String str2 = this.h;
                if (str2 != null) {
                    p(str2);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }
}
