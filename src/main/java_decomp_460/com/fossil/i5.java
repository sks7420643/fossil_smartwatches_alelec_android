package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ k5 b;
    @DexIgnore
    public /* final */ /* synthetic */ g7 c;
    @DexIgnore
    public /* final */ /* synthetic */ n6 d;
    @DexIgnore
    public /* final */ /* synthetic */ u6 e;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] f;

    @DexIgnore
    public i5(k5 k5Var, g7 g7Var, n6 n6Var, u6 u6Var, byte[] bArr) {
        this.b = k5Var;
        this.c = g7Var;
        this.d = n6Var;
        this.e = u6Var;
        this.f = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.b.f();
        this.b.z.b.c(new b7(this.c, this.d, this.e, this.f));
    }
}
