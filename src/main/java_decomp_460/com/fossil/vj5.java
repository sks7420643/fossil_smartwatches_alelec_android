package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vj5<TranscodeType> extends va1<TranscodeType> implements Cloneable {
    @DexIgnore
    public vj5(oa1 oa1, wa1 wa1, Class<TranscodeType> cls, Context context) {
        super(oa1, wa1, cls, context);
    }

    @DexIgnore
    /* renamed from: S0 */
    public vj5<TranscodeType> t0(ej1<TranscodeType> ej1) {
        super.t0(ej1);
        return this;
    }

    @DexIgnore
    /* renamed from: T0 */
    public vj5<TranscodeType> u0(yi1<?> yi1) {
        return (vj5) super.d(yi1);
    }

    @DexIgnore
    public vj5<TranscodeType> U0() {
        return (vj5) super.g();
    }

    @DexIgnore
    /* renamed from: V0 */
    public vj5<TranscodeType> y0() {
        return (vj5) super.i();
    }

    @DexIgnore
    /* renamed from: W0 */
    public vj5<TranscodeType> j(Class<?> cls) {
        return (vj5) super.j(cls);
    }

    @DexIgnore
    /* renamed from: X0 */
    public vj5<TranscodeType> l(wc1 wc1) {
        return (vj5) super.l(wc1);
    }

    @DexIgnore
    /* renamed from: Y0 */
    public vj5<TranscodeType> n() {
        return (vj5) super.n();
    }

    @DexIgnore
    /* renamed from: Z0 */
    public vj5<TranscodeType> o(fg1 fg1) {
        return (vj5) super.o(fg1);
    }

    @DexIgnore
    public vj5<TranscodeType> a1(va1<TranscodeType> va1) {
        super.z0(va1);
        return this;
    }

    @DexIgnore
    public vj5<TranscodeType> b1(ej1<TranscodeType> ej1) {
        return (vj5) super.H0(ej1);
    }

    @DexIgnore
    /* renamed from: c1 */
    public vj5<TranscodeType> I0(Bitmap bitmap) {
        return (vj5) super.I0(bitmap);
    }

    @DexIgnore
    /* renamed from: d1 */
    public vj5<TranscodeType> J0(Uri uri) {
        super.J0(uri);
        return this;
    }

    @DexIgnore
    /* renamed from: e1 */
    public vj5<TranscodeType> K0(File file) {
        super.K0(file);
        return this;
    }

    @DexIgnore
    /* renamed from: f1 */
    public vj5<TranscodeType> L0(Integer num) {
        return (vj5) super.L0(num);
    }

    @DexIgnore
    /* renamed from: g1 */
    public vj5<TranscodeType> M0(Object obj) {
        super.M0(obj);
        return this;
    }

    @DexIgnore
    /* renamed from: h1 */
    public vj5<TranscodeType> N0(String str) {
        super.N0(str);
        return this;
    }

    @DexIgnore
    /* renamed from: i1 */
    public vj5<TranscodeType> W() {
        return (vj5) super.W();
    }

    @DexIgnore
    /* renamed from: j1 */
    public vj5<TranscodeType> X() {
        return (vj5) super.X();
    }

    @DexIgnore
    /* renamed from: k1 */
    public vj5<TranscodeType> Y() {
        return (vj5) super.Y();
    }

    @DexIgnore
    /* renamed from: l1 */
    public vj5<TranscodeType> b0(int i, int i2) {
        return (vj5) super.b0(i, i2);
    }

    @DexIgnore
    /* renamed from: m1 */
    public vj5<TranscodeType> c0(int i) {
        return (vj5) super.c0(i);
    }

    @DexIgnore
    /* renamed from: n1 */
    public vj5<TranscodeType> d0(Drawable drawable) {
        return (vj5) super.d0(drawable);
    }

    @DexIgnore
    /* renamed from: o1 */
    public vj5<TranscodeType> e0(sa1 sa1) {
        return (vj5) super.e0(sa1);
    }

    @DexIgnore
    /* renamed from: p1 */
    public <Y> vj5<TranscodeType> j0(nb1<Y> nb1, Y y) {
        return (vj5) super.j0(nb1, y);
    }

    @DexIgnore
    /* renamed from: q1 */
    public vj5<TranscodeType> k0(mb1 mb1) {
        return (vj5) super.k0(mb1);
    }

    @DexIgnore
    /* renamed from: r1 */
    public vj5<TranscodeType> l0(float f) {
        return (vj5) super.l0(f);
    }

    @DexIgnore
    /* renamed from: s1 */
    public vj5<TranscodeType> m0(boolean z) {
        return (vj5) super.m0(z);
    }

    @DexIgnore
    public vj5<TranscodeType> t1(int i) {
        return (vj5) super.n0(i);
    }

    @DexIgnore
    /* renamed from: u1 */
    public vj5<TranscodeType> o0(sb1<Bitmap> sb1) {
        return (vj5) super.o0(sb1);
    }

    @DexIgnore
    /* renamed from: v1 */
    public vj5<TranscodeType> s0(boolean z) {
        return (vj5) super.s0(z);
    }
}
