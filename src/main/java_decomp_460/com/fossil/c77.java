package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f577a;
    @DexIgnore
    public /* final */ d77 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ bg5 f578a;
        @DexIgnore
        public /* final */ d77 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c77$a$a")
        /* renamed from: com.fossil.c77$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0031a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0031a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bg5 bg5, d77 d77) {
            super(bg5.n());
            pq7.c(bg5, "binding");
            pq7.c(d77, "photoListener");
            this.f578a = bg5;
            this.b = d77;
        }

        @DexIgnore
        public final void b() {
            this.f578a.q.setOnClickListener(new View$OnClickListenerC0031a(this));
        }
    }

    @DexIgnore
    public c77(int i, d77 d77) {
        pq7.c(d77, "photoListener");
        this.f577a = i;
        this.b = d77;
    }

    @DexIgnore
    public final int a() {
        return this.f577a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        a aVar = (a) (!(viewHolder instanceof a) ? null : viewHolder);
        if (aVar != null) {
            aVar.b();
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        bg5 z = bg5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemThemeBackgroundAddBi\u2026(inflater, parent, false)");
        return new a(z, this.b);
    }
}
