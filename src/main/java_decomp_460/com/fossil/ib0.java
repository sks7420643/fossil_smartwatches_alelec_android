package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib0 extends mb0 {
    @DexIgnore
    public static /* final */ hb0 CREATOR; // = new hb0(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexIgnore
    public /* synthetic */ ib0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.d = parcel.readInt() != 0;
        this.e = parcel.readInt();
    }

    @DexIgnore
    public ib0(bv1 bv1, ry1 ry1, int i, int i2) {
        super(bv1, ry1);
        this.d = false;
        this.e = 0;
        this.f = i;
        this.g = i2;
    }

    @DexIgnore
    public ib0(bv1 bv1, ry1 ry1, boolean z, int i) {
        super(bv1, ry1);
        this.d = z;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public List<va0> b() {
        short s;
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new za0());
        if (this.d) {
            arrayList.add(new ia0(new r90[]{new r90(ca0.c, w90.d, da0.POSITION, ea0.c, 0), new r90(ca0.d, w90.d, da0.POSITION, ea0.c, 0)}));
        }
        arrayList.add(new ma0(0.5d));
        arrayList.add(new ga0(fa0.c));
        ca0 ca0 = ca0.c;
        w90 w90 = w90.c;
        da0 c = c();
        ea0 ea0 = ea0.c;
        if (d()) {
            s = (short) ((this.f * 30) + ((this.g * 30) / 60));
        } else {
            int i2 = this.e;
            s = (short) (i2 >= 60 ? (i2 / 60) * 30 : i2 * 6);
        }
        r90 r90 = new r90(ca0, w90, c, ea0, s);
        ca0 ca02 = ca0.d;
        w90 w902 = w90.c;
        da0 c2 = c();
        ea0 ea02 = ea0.c;
        if (d()) {
            i = this.g;
        } else {
            i = this.e;
            if (i >= 60) {
                i %= 60;
            }
        }
        arrayList.add(new ia0(new r90[]{r90, new r90(ca02, w902, c2, ea02, (short) (i * 6))}));
        arrayList.add(new ma0(8.0d));
        arrayList.add(new ka0());
        return arrayList;
    }

    @DexIgnore
    public final da0 c() {
        return d() ? da0.POSITION : this.e >= 60 ? da0.POSITION : da0.DISTANCE;
    }

    @DexIgnore
    public final boolean d() {
        return this.e == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ib0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ib0 ib0 = (ib0) obj;
            if (this.d != ib0.d) {
                return false;
            }
            return this.e == ib0.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse");
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public int hashCode() {
        return (Boolean.valueOf(this.d).hashCode() * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.mb0
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.w3, Integer.valueOf(this.d ? 1 : 0)), jd0.x3, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.mb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
