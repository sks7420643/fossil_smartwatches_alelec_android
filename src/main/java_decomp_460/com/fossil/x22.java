package com.fossil;

import android.database.Cursor;
import com.fossil.j32;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class x22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map f4032a;

    @DexIgnore
    public x22(Map map) {
        this.f4032a = map;
    }

    @DexIgnore
    public static j32.b a(Map map) {
        return new x22(map);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.T(this.f4032a, (Cursor) obj);
    }
}
