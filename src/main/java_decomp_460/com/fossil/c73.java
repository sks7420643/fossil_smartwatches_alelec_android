package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c73 implements xw2<b73> {
    @DexIgnore
    public static c73 c; // = new c73();
    @DexIgnore
    public /* final */ xw2<b73> b;

    @DexIgnore
    public c73() {
        this(ww2.b(new e73()));
    }

    @DexIgnore
    public c73(xw2<b73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((b73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((b73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ b73 zza() {
        return this.b.zza();
    }
}
