package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i71 {
    @DexIgnore
    public i71() {
    }

    @DexIgnore
    public /* synthetic */ i71(kq7 kq7) {
        this();
    }

    @DexIgnore
    public void e() {
    }

    @DexIgnore
    public Object f(Drawable drawable, n81 n81, qn7<? super tl7> qn7) {
        return tl7.f3441a;
    }

    @DexIgnore
    public void h(BitmapDrawable bitmapDrawable, Drawable drawable) {
    }

    @DexIgnore
    public Object i(Drawable drawable, boolean z, n81 n81, qn7<? super tl7> qn7) {
        return tl7.f3441a;
    }
}
