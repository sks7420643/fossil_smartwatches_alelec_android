package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha7 implements Factory<ga7> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ ha7 f1459a; // = new ha7();
    }

    @DexIgnore
    public static ha7 a() {
        return a.f1459a;
    }

    @DexIgnore
    public static ga7 c() {
        return new ga7();
    }

    @DexIgnore
    /* renamed from: b */
    public ga7 get() {
        return c();
    }
}
