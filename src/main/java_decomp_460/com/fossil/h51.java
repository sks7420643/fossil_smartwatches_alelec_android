package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h51 implements g51 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ Set<Bitmap.Config> h;
    @DexIgnore
    public /* final */ j51 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final Set<Bitmap.Config> b() {
            aj0 a2 = bj0.a(Bitmap.Config.ALPHA_8, Bitmap.Config.RGB_565, Bitmap.Config.ARGB_4444, Bitmap.Config.ARGB_8888);
            if (Build.VERSION.SDK_INT >= 26) {
                a2.add(Bitmap.Config.RGBA_F16);
            }
            return a2;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.util.Set<? extends android.graphics.Bitmap$Config> */
    /* JADX WARN: Multi-variable type inference failed */
    public h51(long j2, Set<? extends Bitmap.Config> set, j51 j51) {
        pq7.c(set, "allowedConfigs");
        pq7.c(j51, "strategy");
        this.g = j2;
        this.h = set;
        this.i = j51;
        if (!(j2 >= 0)) {
            throw new IllegalArgumentException("maxSize must be >= 0.".toString());
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h51(long j2, Set set, j51 j51, int i2, kq7 kq7) {
        this(j2, (i2 & 2) != 0 ? j.b() : set, (i2 & 4) != 0 ? j51.f1711a.a() : j51);
    }

    @DexIgnore
    @Override // com.fossil.g51
    public void a(int i2) {
        synchronized (this) {
            if (q81.c.a() && q81.c.b() <= 3) {
                Log.println(3, "RealBitmapPool", "trimMemory, level=" + i2);
            }
            if (i2 >= 40) {
                f();
            } else if (10 <= i2 && 20 > i2) {
                k(this.b / ((long) 2));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.g51
    public void b(Bitmap bitmap) {
        synchronized (this) {
            pq7.c(bitmap, "bitmap");
            boolean z = true;
            if (!bitmap.isRecycled()) {
                int b2 = w81.b(bitmap);
                if (bitmap.isMutable()) {
                    long j2 = (long) b2;
                    if (j2 <= this.g && this.h.contains(bitmap.getConfig())) {
                        this.i.b(bitmap);
                        this.e++;
                        this.b += j2;
                        if (q81.c.a() && q81.c.b() <= 2) {
                            Log.println(2, "RealBitmapPool", "Put bitmap in pool=" + this.i.d(bitmap));
                        }
                        h();
                        k(this.g);
                        return;
                    }
                }
                if (q81.c.a() && q81.c.b() <= 2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Rejected bitmap from pool: bitmap: ");
                    sb.append(this.i.d(bitmap));
                    sb.append(", ");
                    sb.append("is mutable: ");
                    sb.append(bitmap.isMutable());
                    sb.append(", ");
                    sb.append("is greater than max size: ");
                    if (((long) b2) <= this.g) {
                        z = false;
                    }
                    sb.append(z);
                    sb.append("is allowed config: ");
                    sb.append(this.h.contains(bitmap.getConfig()));
                    Log.println(2, "RealBitmapPool", sb.toString());
                }
                bitmap.recycle();
                return;
            }
            throw new IllegalArgumentException("Cannot pool recycled bitmap!".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.g51
    public Bitmap c(int i2, int i3, Bitmap.Config config) {
        pq7.c(config, "config");
        Bitmap i4 = i(i2, i3, config);
        if (i4 != null) {
            return i4;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, config);
        pq7.b(createBitmap, "Bitmap.createBitmap(width, height, config)");
        return createBitmap;
    }

    @DexIgnore
    @Override // com.fossil.g51
    public Bitmap d(int i2, int i3, Bitmap.Config config) {
        Bitmap c2;
        synchronized (this) {
            pq7.c(config, "config");
            e(config);
            c2 = this.i.c(i2, i3, config);
            if (c2 == null) {
                if (q81.c.a() && q81.c.b() <= 3) {
                    Log.println(3, "RealBitmapPool", "Missing bitmap=" + this.i.a(i2, i3, config));
                }
                this.d++;
            } else {
                this.c++;
                this.b -= (long) w81.b(c2);
                j(c2);
            }
            if (q81.c.a() && q81.c.b() <= 2) {
                Log.println(2, "RealBitmapPool", "Get bitmap=" + this.i.a(i2, i3, config));
            }
            h();
        }
        return c2;
    }

    @DexIgnore
    public final void e(Bitmap.Config config) {
        if (!(Build.VERSION.SDK_INT < 26 || config != Bitmap.Config.HARDWARE)) {
            throw new IllegalArgumentException("Cannot create a mutable hardware Bitmap.".toString());
        }
    }

    @DexIgnore
    public final void f() {
        if (q81.c.a() && q81.c.b() <= 3) {
            Log.println(3, "RealBitmapPool", "clearMemory");
        }
        k(-1);
    }

    @DexIgnore
    public final String g() {
        return "Hits=" + this.c + ", misses=" + this.d + ", puts=" + this.e + ", evictions=" + this.f + ", currentSize=" + this.b + ", maxSize=" + this.g + ", strategy=" + this.i;
    }

    @DexIgnore
    public final void h() {
        if (q81.c.a() && q81.c.b() <= 2) {
            Log.println(2, "RealBitmapPool", g());
        }
    }

    @DexIgnore
    public Bitmap i(int i2, int i3, Bitmap.Config config) {
        pq7.c(config, "config");
        Bitmap d2 = d(i2, i3, config);
        if (d2 != null) {
            d2.eraseColor(0);
        }
        return d2;
    }

    @DexIgnore
    public final void j(Bitmap bitmap) {
        bitmap.setDensity(0);
        bitmap.setHasAlpha(true);
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    @DexIgnore
    public final void k(long j2) {
        synchronized (this) {
            while (this.b > j2) {
                Bitmap removeLast = this.i.removeLast();
                if (removeLast == null) {
                    if (q81.c.a() && q81.c.b() <= 5) {
                        Log.println(5, "RealBitmapPool", "Size mismatch, resetting.\n" + g());
                    }
                    this.b = 0;
                    return;
                }
                this.b -= (long) w81.b(removeLast);
                this.f++;
                if (q81.c.a() && q81.c.b() <= 3) {
                    Log.println(3, "RealBitmapPool", "Evicting bitmap=" + this.i.d(removeLast));
                }
                h();
                removeLast.recycle();
            }
        }
    }
}
