package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fn1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final fn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new fn1(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            }
            throw new IllegalArgumentException(e.c(e.e("Invalid data size: "), bArr.length, ", ", "require: 2"));
        }

        @DexIgnore
        public fn1 b(Parcel parcel) {
            return new fn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public fn1 createFromParcel(Parcel parcel) {
            return new fn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fn1[] newArray(int i) {
            return new fn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ fn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = (short) ((short) parcel.readInt());
        d();
    }

    @DexIgnore
    public fn1(short s) {
        super(zm1.SECOND_TIMEZONE_OFFSET);
        this.c = (short) s;
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(this.c).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.w2, Short.valueOf(this.c));
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        short s = this.c;
        if (!(s == 1024 || (-720 <= s && 840 >= s))) {
            StringBuilder e = e.e("secondTimezoneOffsetInMinute (");
            e.append((int) this.c);
            e.append(") ");
            e.append(" must be equal to 1024 ");
            e.append(" or in range ");
            e.append("[-720, ");
            e.append("840].");
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(fn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((fn1) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig");
    }

    @DexIgnore
    public final short getSecondTimezoneOffsetInMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.c));
        }
    }
}
