package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.qw5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb6 extends pv5 implements bb6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<ac5> g;
    @DexIgnore
    public qw5 h;
    @DexIgnore
    public ab6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return cb6.k;
        }

        @DexIgnore
        public final cb6 b() {
            return new cb6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cb6 b;

        @DexIgnore
        public b(cb6 cb6) {
            this.b = cb6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ac5 a2 = this.b.M6().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ cb6 b;

        @DexIgnore
        public c(cb6 cb6) {
            this.b = cb6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                ac5 a2 = this.b.M6().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.b.z("");
                this.b.N6().n();
                return;
            }
            ac5 a3 = this.b.M6().a();
            if (!(a3 == null || (imageView = a3.r) == null)) {
                imageView.setVisibility(0);
            }
            this.b.z(String.valueOf(charSequence));
            this.b.N6().p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cb6 b;

        @DexIgnore
        public d(cb6 cb6) {
            this.b = cb6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements qw5.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ cb6 f593a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(cb6 cb6) {
            this.f593a = cb6;
        }

        @DexIgnore
        @Override // com.fossil.qw5.c
        public void a(String str) {
            pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            ac5 a2 = this.f593a.M6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "it.tvNotFound");
                hr7 hr7 = hr7.f1520a;
                String c = um5.c(PortfolioApp.h0.c(), 2131886813);
                pq7.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements qw5.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ cb6 f594a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(cb6 cb6) {
            this.f594a = cb6;
        }

        @DexIgnore
        @Override // com.fossil.qw5.d
        public void a(WatchApp watchApp) {
            pq7.c(watchApp, "item");
            this.f594a.N6().o(watchApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ac5 f595a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(ac5 ac5, long j) {
            this.f595a = ac5;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.f595a.q;
            pq7.b(flexibleTextView, "binding.btnCancel");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.f595a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.f595a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = cb6.class.getSimpleName();
        pq7.b(simpleName, "WatchAppSearchFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.bb6
    public void B(List<cl7<WatchApp, String>> list) {
        pq7.c(list, "results");
        qw5 qw5 = this.h;
        if (qw5 != null) {
            qw5.m(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void D() {
        qw5 qw5 = this.h;
        if (qw5 != null) {
            qw5.m(null);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void D3(WatchApp watchApp) {
        pq7.c(watchApp, "selectedWatchApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            xk5 xk5 = xk5.f4136a;
            g37<ac5> g37 = this.g;
            if (g37 != null) {
                ac5 a2 = g37.a();
                FlexibleTextView flexibleTextView = a2 != null ? a2.q : null;
                if (flexibleTextView != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleTextView, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_WATCH_APP_RESULT_ID", watchApp.getWatchappId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void K(List<cl7<WatchApp, String>> list) {
        pq7.c(list, "recentSearchResult");
        qw5 qw5 = this.h;
        if (qw5 != null) {
            qw5.l(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void K6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            xk5 xk5 = xk5.f4136a;
            g37<ac5> g37 = this.g;
            if (g37 != null) {
                ac5 a2 = g37.a();
                FlexibleTextView flexibleTextView = a2 != null ? a2.q : null;
                if (flexibleTextView != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final g37<ac5> M6() {
        g37<ac5> g37 = this.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final ab6 N6() {
        ab6 ab6 = this.i;
        if (ab6 != null) {
            return ab6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = is5.f1661a.a(j2);
        Window window = fragmentActivity.getWindow();
        pq7.b(window, "context.window");
        window.setEnterTransition(a2);
        g37<ac5> g37 = this.g;
        if (g37 != null) {
            ac5 a3 = g37.a();
            if (a3 != null) {
                pq7.b(a3, "binding");
                P6(a2, j2, a3);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet P6(TransitionSet transitionSet, long j2, ac5 ac5) {
        FlexibleTextView flexibleTextView = ac5.q;
        pq7.b(flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new g(ac5, j2));
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(ab6 ab6) {
        pq7.c(ab6, "presenter");
        this.i = ab6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<ac5> g37 = new g37<>(this, (ac5) aq0.f(layoutInflater, 2131558636, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            ac5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ab6 ab6 = this.i;
        if (ab6 != null) {
            ab6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        ab6 ab6 = this.i;
        if (ab6 != null) {
            ab6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, "it");
            O6(activity, 550);
        }
        this.h = new qw5();
        g37<ac5> g37 = this.g;
        if (g37 != null) {
            ac5 a2 = g37.a();
            if (a2 != null) {
                ac5 ac5 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = ac5.v;
                pq7.b(recyclerViewEmptySupport, "this.rvResults");
                qw5 qw5 = this.h;
                if (qw5 != null) {
                    recyclerViewEmptySupport.setAdapter(qw5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = ac5.v;
                    pq7.b(recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = ac5.v;
                    FlexibleTextView flexibleTextView = ac5.w;
                    pq7.b(flexibleTextView, "this.tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = ac5.r;
                    pq7.b(imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    ac5.r.setOnClickListener(new b(this));
                    ac5.t.addTextChangedListener(new c(this));
                    ac5.q.setOnClickListener(new d(this));
                    qw5 qw52 = this.h;
                    if (qw52 != null) {
                        qw52.o(new e(this));
                        qw5 qw53 = this.h;
                        if (qw53 != null) {
                            qw53.p(new f(this));
                        } else {
                            pq7.n("mAdapter");
                            throw null;
                        }
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void z(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        qw5 qw5 = this.h;
        if (qw5 != null) {
            qw5.n(str);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }
}
