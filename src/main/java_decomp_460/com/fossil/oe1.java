package com.fossil;

import com.fossil.af1;
import com.fossil.wb1;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oe1<Data> implements af1<byte[], Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b<Data> f2671a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<byte[], ByteBuffer> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oe1$a$a")
        /* renamed from: com.fossil.oe1$a$a  reason: collision with other inner class name */
        public class C0179a implements b<ByteBuffer> {
            @DexIgnore
            public C0179a(a aVar) {
            }

            @DexIgnore
            /* renamed from: b */
            public ByteBuffer a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }

            @DexIgnore
            @Override // com.fossil.oe1.b
            public Class<ByteBuffer> getDataClass() {
                return ByteBuffer.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<byte[], ByteBuffer> b(ef1 ef1) {
            return new oe1(new C0179a(this));
        }
    }

    @DexIgnore
    public interface b<Data> {
        @DexIgnore
        Data a(byte[] bArr);

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<Data> implements wb1<Data> {
        @DexIgnore
        public /* final */ byte[] b;
        @DexIgnore
        public /* final */ b<Data> c;

        @DexIgnore
        public c(byte[] bArr, b<Data> bVar) {
            this.b = bArr;
            this.c = bVar;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super Data> aVar) {
            aVar.e(this.c.a(this.b));
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements bf1<byte[], InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements b<InputStream> {
            @DexIgnore
            public a(d dVar) {
            }

            @DexIgnore
            /* renamed from: b */
            public InputStream a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }

            @DexIgnore
            @Override // com.fossil.oe1.b
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<byte[], InputStream> b(ef1 ef1) {
            return new oe1(new a(this));
        }
    }

    @DexIgnore
    public oe1(b<Data> bVar) {
        this.f2671a = bVar;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(byte[] bArr, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(bArr), new c(bArr, this.f2671a));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(byte[] bArr) {
        return true;
    }
}
