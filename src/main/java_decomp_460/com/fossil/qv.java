package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qv extends ps {
    @DexIgnore
    public /* final */ n6 G;
    @DexIgnore
    public /* final */ n6 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qv(short s, k5 k5Var, int i, int i2) {
        super(hs.Y, k5Var, (i2 & 4) != 0 ? 3 : i);
        this.K = (short) s;
        n6 n6Var = n6.FTC;
        this.G = n6Var;
        this.H = n6Var;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_ERASE_FILE.b).putShort(this.K).array();
        pq7.b(array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_ERASE_FILE.a()).putShort(this.K).array();
        pq7.b(array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.J = array2;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public mt E(byte b) {
        return kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 K() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] M() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] P() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A0, hy1.l(this.K, null, 1, null));
    }
}
