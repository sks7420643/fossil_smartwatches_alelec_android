package com.fossil;

import android.graphics.Insets;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ql0 {
    @DexIgnore
    public static /* final */ ql0 e; // = new ql0(0, 0, 0, 0);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f2995a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public ql0(int i, int i2, int i3, int i4) {
        this.f2995a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    @DexIgnore
    public static ql0 a(int i, int i2, int i3, int i4) {
        return (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) ? e : new ql0(i, i2, i3, i4);
    }

    @DexIgnore
    public Insets b() {
        return Insets.of(this.f2995a, this.b, this.c, this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ql0.class != obj.getClass()) {
            return false;
        }
        ql0 ql0 = (ql0) obj;
        if (this.d != ql0.d) {
            return false;
        }
        if (this.f2995a != ql0.f2995a) {
            return false;
        }
        if (this.c != ql0.c) {
            return false;
        }
        return this.b == ql0.b;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.f2995a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public String toString() {
        return "Insets{left=" + this.f2995a + ", top=" + this.b + ", right=" + this.c + ", bottom=" + this.d + '}';
    }
}
