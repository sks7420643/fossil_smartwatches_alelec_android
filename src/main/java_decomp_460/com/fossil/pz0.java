package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pz0 implements rz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ IBinder f2895a;

    @DexIgnore
    public pz0(IBinder iBinder) {
        this.f2895a = iBinder;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof pz0) && ((pz0) obj).f2895a.equals(this.f2895a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f2895a.hashCode();
    }
}
