package com.fossil;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xw0 {
    @DexIgnore
    public /* final */ qw0 mDatabase;
    @DexIgnore
    public /* final */ AtomicBoolean mLock; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile px0 mStmt;

    @DexIgnore
    public xw0(qw0 qw0) {
        this.mDatabase = qw0;
    }

    @DexIgnore
    private px0 createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private px0 getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public px0 acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public void release(px0 px0) {
        if (px0 == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
