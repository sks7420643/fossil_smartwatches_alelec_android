package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.tj0;
import com.fossil.uj0;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vj0 extends ek0 {
    @DexIgnore
    public int A0; // = 0;
    @DexIgnore
    public int B0; // = 0;
    @DexIgnore
    public int C0; // = 7;
    @DexIgnore
    public boolean D0; // = false;
    @DexIgnore
    public boolean E0; // = false;
    @DexIgnore
    public boolean F0; // = false;
    @DexIgnore
    public boolean l0; // = false;
    @DexIgnore
    public kj0 m0; // = new kj0();
    @DexIgnore
    public dk0 n0;
    @DexIgnore
    public int o0;
    @DexIgnore
    public int p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0; // = 0;
    @DexIgnore
    public int t0; // = 0;
    @DexIgnore
    public sj0[] u0; // = new sj0[4];
    @DexIgnore
    public sj0[] v0; // = new sj0[4];
    @DexIgnore
    public List<wj0> w0; // = new ArrayList();
    @DexIgnore
    public boolean x0; // = false;
    @DexIgnore
    public boolean y0; // = false;
    @DexIgnore
    public boolean z0; // = false;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02de  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0300  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0370  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0403  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0232  */
    @Override // com.fossil.ek0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void K0() {
        /*
        // Method dump skipped, instructions count: 1033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vj0.K0():void");
    }

    @DexIgnore
    public void N0(uj0 uj0, int i) {
        if (i == 0) {
            P0(uj0);
        } else if (i == 1) {
            Q0(uj0);
        }
    }

    @DexIgnore
    public boolean O0(kj0 kj0) {
        b(kj0);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            uj0 uj0 = this.k0.get(i);
            if (uj0 instanceof vj0) {
                uj0.b[] bVarArr = uj0.C;
                uj0.b bVar = bVarArr[0];
                uj0.b bVar2 = bVarArr[1];
                if (bVar == uj0.b.WRAP_CONTENT) {
                    uj0.g0(uj0.b.FIXED);
                }
                if (bVar2 == uj0.b.WRAP_CONTENT) {
                    uj0.u0(uj0.b.FIXED);
                }
                uj0.b(kj0);
                if (bVar == uj0.b.WRAP_CONTENT) {
                    uj0.g0(bVar);
                }
                if (bVar2 == uj0.b.WRAP_CONTENT) {
                    uj0.u0(bVar2);
                }
            } else {
                zj0.c(this, kj0, uj0);
                uj0.b(kj0);
            }
        }
        if (this.s0 > 0) {
            rj0.a(this, kj0, 0);
        }
        if (this.t0 > 0) {
            rj0.a(this, kj0, 1);
        }
        return true;
    }

    @DexIgnore
    public final void P0(uj0 uj0) {
        int i = this.s0;
        sj0[] sj0Arr = this.v0;
        if (i + 1 >= sj0Arr.length) {
            this.v0 = (sj0[]) Arrays.copyOf(sj0Arr, sj0Arr.length * 2);
        }
        this.v0[this.s0] = new sj0(uj0, 0, U0());
        this.s0++;
    }

    @DexIgnore
    @Override // com.fossil.ek0, com.fossil.uj0
    public void Q() {
        this.m0.E();
        this.o0 = 0;
        this.q0 = 0;
        this.p0 = 0;
        this.r0 = 0;
        this.w0.clear();
        this.D0 = false;
        super.Q();
    }

    @DexIgnore
    public final void Q0(uj0 uj0) {
        int i = this.t0;
        sj0[] sj0Arr = this.u0;
        if (i + 1 >= sj0Arr.length) {
            this.u0 = (sj0[]) Arrays.copyOf(sj0Arr, sj0Arr.length * 2);
        }
        this.u0[this.t0] = new sj0(uj0, 1, U0());
        this.t0++;
    }

    @DexIgnore
    public int R0() {
        return this.C0;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public boolean T0() {
        return this.F0;
    }

    @DexIgnore
    public boolean U0() {
        return this.l0;
    }

    @DexIgnore
    public boolean V0() {
        return this.E0;
    }

    @DexIgnore
    public void W0() {
        if (!X0(8)) {
            d(this.C0);
        }
        e1();
    }

    @DexIgnore
    public boolean X0(int i) {
        return (this.C0 & i) == i;
    }

    @DexIgnore
    public void Y0(int i, int i2) {
        bk0 bk0;
        bk0 bk02;
        if (!(this.C[0] == uj0.b.WRAP_CONTENT || (bk02 = this.c) == null)) {
            bk02.h(i);
        }
        if (this.C[1] != uj0.b.WRAP_CONTENT && (bk0 = this.d) != null) {
            bk0.h(i2);
        }
    }

    @DexIgnore
    public void Z0() {
        int size = this.k0.size();
        S();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).S();
        }
    }

    @DexIgnore
    public void a1() {
        Z0();
        d(this.C0);
    }

    @DexIgnore
    public final void b1() {
        this.s0 = 0;
        this.t0 = 0;
    }

    @DexIgnore
    public void c1(int i) {
        this.C0 = i;
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void d(int i) {
        super.d(i);
        int size = this.k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k0.get(i2).d(i);
        }
    }

    @DexIgnore
    public void d1(boolean z) {
        this.l0 = z;
    }

    @DexIgnore
    public void e1() {
        ak0 f = h(tj0.d.LEFT).f();
        ak0 f2 = h(tj0.d.TOP).f();
        f.l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        f2.l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void f1(kj0 kj0, boolean[] zArr) {
        zArr[2] = false;
        G0(kj0);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            uj0 uj0 = this.k0.get(i);
            uj0.G0(kj0);
            if (uj0.C[0] == uj0.b.MATCH_CONSTRAINT && uj0.D() < uj0.F()) {
                zArr[2] = true;
            }
            if (uj0.C[1] == uj0.b.MATCH_CONSTRAINT && uj0.r() < uj0.E()) {
                zArr[2] = true;
            }
        }
    }
}
