package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i62 {
    @DexIgnore
    public static i62 b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1588a;

    @DexIgnore
    public i62(Context context) {
        this.f1588a = context.getApplicationContext();
    }

    @DexIgnore
    public static i62 a(Context context) {
        rc2.k(context);
        synchronized (i62.class) {
            try {
                if (b == null) {
                    dg2.c(context);
                    b = new i62(context);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return b;
    }

    @DexIgnore
    public static eg2 d(PackageInfo packageInfo, eg2... eg2Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        hg2 hg2 = new hg2(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < eg2Arr.length; i++) {
            if (eg2Arr[i].equals(hg2)) {
                return eg2Arr[i];
            }
        }
        return null;
    }

    @DexIgnore
    public static boolean f(PackageInfo packageInfo, boolean z) {
        eg2 d;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                d = d(packageInfo, jg2.f1755a);
            } else {
                d = d(packageInfo, jg2.f1755a[0]);
            }
            if (d != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean b(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (f(packageInfo, false)) {
            return true;
        }
        if (!f(packageInfo, true)) {
            return false;
        }
        if (h62.f(this.f1588a)) {
            return true;
        }
        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        return false;
    }

    @DexIgnore
    public boolean c(int i) {
        mg2 mg2;
        String[] f = ag2.a(this.f1588a).f(i);
        if (f == null || f.length == 0) {
            mg2 = mg2.d("no pkgs");
        } else {
            mg2 = null;
            for (String str : f) {
                mg2 = e(str, i);
                if (mg2.f2377a) {
                    break;
                }
            }
        }
        mg2.g();
        return mg2.f2377a;
    }

    @DexIgnore
    public final mg2 e(String str, int i) {
        try {
            PackageInfo h = ag2.a(this.f1588a).h(str, 64, i);
            boolean f = h62.f(this.f1588a);
            if (h == null) {
                return mg2.d("null pkg");
            }
            if (h.signatures == null || h.signatures.length != 1) {
                return mg2.d("single cert required");
            }
            hg2 hg2 = new hg2(h.signatures[0].toByteArray());
            String str2 = h.packageName;
            mg2 a2 = dg2.a(str2, hg2, f, false);
            return (!a2.f2377a || h.applicationInfo == null || (h.applicationInfo.flags & 2) == 0 || !dg2.a(str2, hg2, false, true).f2377a) ? a2 : mg2.d("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(str);
            return mg2.d(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }
}
