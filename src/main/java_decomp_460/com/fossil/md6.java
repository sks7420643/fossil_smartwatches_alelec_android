package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md6 extends kd6 {
    @DexIgnore
    public /* final */ mj5 A;
    @DexIgnore
    public /* final */ SummariesRepository B;
    @DexIgnore
    public /* final */ GoalTrackingRepository C;
    @DexIgnore
    public /* final */ SleepSummariesRepository D;
    @DexIgnore
    public /* final */ on5 E;
    @DexIgnore
    public /* final */ HeartRateSampleRepository F;
    @DexIgnore
    public /* final */ tt4 G;
    @DexIgnore
    public Date e;
    @DexIgnore
    public int f;
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h;
    @DexIgnore
    public MFSleepDay i;
    @DexIgnore
    public ActivitySummary j;
    @DexIgnore
    public GoalTrackingSummary k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public List<HeartRateSample> m;
    @DexIgnore
    public HashMap<Integer, Boolean> n; // = new HashMap<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public volatile boolean p;
    @DexIgnore
    public /* final */ b q; // = new b();
    @DexIgnore
    public /* final */ MutableLiveData<Date> r;
    @DexIgnore
    public /* final */ LiveData<h47<ActivitySummary>> s;
    @DexIgnore
    public /* final */ LiveData<h47<MFSleepDay>> t;
    @DexIgnore
    public /* final */ LiveData<h47<GoalTrackingSummary>> u;
    @DexIgnore
    public /* final */ LiveData<h47<Integer>> v;
    @DexIgnore
    public LiveData<h47<List<HeartRateSample>>> w;
    @DexIgnore
    public /* final */ ld6 x;
    @DexIgnore
    public /* final */ PortfolioApp y;
    @DexIgnore
    public /* final */ DeviceRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1", f = "HomeDashboardPresenter.kt", l = {125, 126, 127, 128}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ md6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.md6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1", f = "HomeDashboardPresenter.kt", l = {125}, m = "invokeSuspend")
        /* renamed from: com.fossil.md6$a$a  reason: collision with other inner class name */
        public static final class C0155a extends ko7 implements vp7<iv7, qn7<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0155a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0155a aVar = new C0155a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ActivityStatistic> qn7) {
                return ((C0155a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    SummariesRepository summariesRepository = this.this$0.this$0.B;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object activityStatisticAwait = summariesRepository.getActivityStatisticAwait(this);
                    return activityStatisticAwait == d ? d : activityStatisticAwait;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.getDeviceNameBySerial(this.this$0.this$0.g);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1", f = "HomeDashboardPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super SleepStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super SleepStatistic> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.D;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object sleepStatisticAwait = sleepSummariesRepository.getSleepStatisticAwait(this);
                    return sleepStatisticAwait == d ? d : sleepStatisticAwait;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Device>> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(md6 md6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = md6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0181  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x01ac  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01d1  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x01f1  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01f5  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0244  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 587
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2360a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalSettingTargetLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {103, 103}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends Integer>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends Integer>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r2 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r2) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r7)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r7)
                    r3 = r0
                    r2 = r7
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r6.L$0 = r1
                    r6.label = r5
                    java.lang.Object r0 = r3.a(r0, r6)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r7)
                    com.fossil.hs0 r0 = r6.p$
                    com.fossil.md6$c r1 = r6.this$0
                    com.fossil.md6 r1 = r1.f2360a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.fossil.md6.z(r1)
                    r6.L$0 = r0
                    r6.L$1 = r0
                    r6.label = r2
                    java.lang.Object r2 = r1.getLastGoalSettingLiveData(r6)
                    if (r2 != r4) goto L_0x0057
                    r0 = r4
                    goto L_0x0017
                L_0x0057:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(md6 md6) {
            this.f2360a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<Integer>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalSettingTargetLiveData on date changed " + date);
            return or0.c(null, 0, new a(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2361a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalTrackingSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {97, 97}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends GoalTrackingSummary>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends GoalTrackingSummary>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.md6$d r1 = r7.this$0
                    com.fossil.md6 r1 = r1.f2361a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.fossil.md6.z(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public d(md6 md6) {
            this.f2361a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<GoalTrackingSummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalTrackingSummaryLiveData on date changed " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2362a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mHeartRateSampleLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {109, 109}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<HeartRateSample>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<HeartRateSample>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.md6$e r1 = r8.this$0
                    com.fossil.md6 r1 = r1.f2362a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.fossil.md6.B(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public e(md6 md6) {
            this.f2362a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<HeartRateSample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mHeartRateSampleLiveData on date changed " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2363a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSleepSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {91, 91}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends MFSleepDay>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends MFSleepDay>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.md6$f r1 = r7.this$0
                    com.fossil.md6 r1 = r1.f2363a
                    com.portfolio.platform.data.source.SleepSummariesRepository r1 = com.fossil.md6.G(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSleepSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.f.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public f(md6 md6) {
            this.f2363a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<MFSleepDay>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSleepSummaryLiveData on date changed " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2364a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends ActivitySummary>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends ActivitySummary>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.md6$g r1 = r7.this$0
                    com.fossil.md6 r1 = r1.f2364a
                    com.portfolio.platform.data.source.SummariesRepository r1 = com.fossil.md6.H(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.g.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public g(md6 md6) {
            this.f2364a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<ActivitySummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSummaryLiveData on date changed " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$1", f = "HomeDashboardPresenter.kt", l = {371}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ md6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(md6 md6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = md6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Boolean l0 = this.this$0.E.l0();
                Boolean b = this.this$0.E.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync successfully - isNeedGetSyncDataForWatch: " + l0);
                if (pq7.a(l0, ao7.a(true))) {
                    pq7.b(b, "isBcOn");
                    if (b.booleanValue()) {
                        md6 md6 = this.this$0;
                        this.L$0 = iv7;
                        this.L$1 = l0;
                        this.L$2 = b;
                        this.label = 1;
                        if (md6.W(this) == d) {
                            return d;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$2", f = "HomeDashboardPresenter.kt", l = {404}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ md6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(md6 md6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = md6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, qn7);
            iVar.p$ = (iv7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Boolean l0 = this.this$0.E.l0();
                Boolean b = this.this$0.E.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync failed - isNeedGetSyncDataForWatch: " + l0);
                if (pq7.a(l0, ao7.a(true))) {
                    pq7.b(b, "isBcOn");
                    if (b.booleanValue() && PortfolioApp.h0.c().L()) {
                        md6 md6 = this.this$0;
                        this.L$0 = iv7;
                        this.L$1 = l0;
                        this.L$2 = b;
                        this.label = 1;
                        if (md6.W(this) == d) {
                            return d;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter", f = "HomeDashboardPresenter.kt", l = {425}, m = "retrySetSyncData")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ md6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(md6 md6, qn7 qn7) {
            super(qn7);
            this.this$0 = md6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.W(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1", f = "HomeDashboardPresenter.kt", l = {159}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ md6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1$activeDevice$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ k this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(k kVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = kVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.getDeviceBySerial(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(md6 md6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = md6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, qn7);
            kVar.p$ = (iv7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "showLayoutBattery device " + device);
            if (device != null) {
                if (pq7.a("release", "debug") || pq7.a("release", "staging")) {
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.h0.c().k0().M() || device.getBatteryLevel() <= 0) {
                        this.this$0.x.x3(false);
                    } else {
                        this.this$0.x.x3(true);
                    }
                } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 25 || device.getBatteryLevel() <= 0) {
                    this.this$0.x.x3(false);
                } else {
                    this.this$0.x.x3(true);
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<h47<? extends ActivitySummary>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2365a;

        @DexIgnore
        public l(md6 md6) {
            this.f2365a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<ActivitySummary> h47) {
            ActivitySummary activitySummary = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "XXX- summaryChanged -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.f2365a.n.get(0)));
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    activitySummary = h47.c();
                }
                if ((!pq7.a((Boolean) this.f2365a.n.get(0), Boolean.TRUE)) || (!pq7.a(this.f2365a.j, activitySummary))) {
                    this.f2365a.j = activitySummary;
                    this.f2365a.Z();
                    this.f2365a.n.put(0, Boolean.TRUE);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<h47<? extends MFSleepDay>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2366a;

        @DexIgnore
        public m(md6 md6) {
            this.f2366a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<MFSleepDay> h47) {
            MFSleepDay mFSleepDay = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSleepSummaryLiveData -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.f2366a.n.get(5)));
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    mFSleepDay = h47.c();
                }
                if ((!pq7.a((Boolean) this.f2366a.n.get(5), Boolean.TRUE)) || (!pq7.a(this.f2366a.i, mFSleepDay))) {
                    this.f2366a.i = mFSleepDay;
                    this.f2366a.Z();
                    this.f2366a.n.put(5, Boolean.TRUE);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<h47<? extends GoalTrackingSummary>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2367a;

        @DexIgnore
        public n(md6 md6) {
            this.f2367a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<GoalTrackingSummary> h47) {
            GoalTrackingSummary goalTrackingSummary = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalTrackingSummaryLiveData -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.f2367a.n.get(4)));
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    goalTrackingSummary = h47.c();
                }
                if ((!pq7.a((Boolean) this.f2367a.n.get(4), Boolean.TRUE)) || (!pq7.a(this.f2367a.k, goalTrackingSummary))) {
                    this.f2367a.k = goalTrackingSummary;
                    this.f2367a.Z();
                    this.f2367a.n.put(4, Boolean.TRUE);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements ls0<h47<? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2368a;

        @DexIgnore
        public o(md6 md6) {
            this.f2368a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<Integer> h47) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- resource=" + h47);
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                Integer c = h47 != null ? h47.c() : null;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- goal=" + c);
                if (!pq7.a(this.f2368a.l, c)) {
                    this.f2368a.l = c;
                    this.f2368a.Z();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<h47<? extends List<HeartRateSample>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ md6 f2369a;

        @DexIgnore
        public p(md6 md6) {
            this.f2369a = md6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<HeartRateSample>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSampleLiveData -- resource.status=");
            sb.append(a2);
            sb.append(", ");
            sb.append("data.size=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", mFirstDBLoad=");
            sb.append((Boolean) this.f2369a.n.get(3));
            local.d("HomeDashboardPresenter", sb.toString());
            if (a2 == xh5.DATABASE_LOADING) {
                return;
            }
            if ((!pq7.a((Boolean) this.f2369a.n.get(3), Boolean.TRUE)) || (!pq7.a(this.f2369a.m, list))) {
                this.f2369a.m = list;
                this.f2369a.Z();
                this.f2369a.n.put(3, Boolean.TRUE);
            }
        }
    }

    @DexIgnore
    public md6(ld6 ld6, PortfolioApp portfolioApp, DeviceRepository deviceRepository, mj5 mj5, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, on5 on5, HeartRateSampleRepository heartRateSampleRepository, tt4 tt4) {
        pq7.c(ld6, "mView");
        pq7.c(portfolioApp, "mApp");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(summariesRepository, "mSummaryRepository");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(sleepSummariesRepository, "mSleepSummaryRepository");
        pq7.c(on5, "sharedPreferencesManager");
        pq7.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        pq7.c(tt4, "challengeRepository");
        this.x = ld6;
        this.y = portfolioApp;
        this.z = deviceRepository;
        this.A = mj5;
        this.B = summariesRepository;
        this.C = goalTrackingRepository;
        this.D = sleepSummariesRepository;
        this.E = on5;
        this.F = heartRateSampleRepository;
        this.G = tt4;
        String J = portfolioApp.J();
        this.g = J;
        this.h = FossilDeviceSerialPatternUtil.getDeviceBySerial(J);
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.r = mutableLiveData;
        LiveData<h47<ActivitySummary>> c2 = ss0.c(mutableLiveData, new g(this));
        pq7.b(c2, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.s = c2;
        LiveData<h47<MFSleepDay>> c3 = ss0.c(this.r, new f(this));
        pq7.b(c3, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.t = c3;
        LiveData<h47<GoalTrackingSummary>> c4 = ss0.c(this.r, new d(this));
        pq7.b(c4, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.u = c4;
        LiveData<h47<Integer>> c5 = ss0.c(this.r, new c(this));
        pq7.b(c5, "Transformations.switchMa\u2026veData())\n        }\n    }");
        this.v = c5;
        LiveData<h47<List<HeartRateSample>>> c6 = ss0.c(this.r, new e(this));
        pq7.b(c6, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.w = c6;
    }

    @DexIgnore
    public final xw7 R() {
        return gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final void S() {
        PortfolioApp portfolioApp = this.y;
        if (portfolioApp.A0(portfolioApp.J()) && !this.p) {
            this.p = true;
            this.x.t6();
        }
    }

    @DexIgnore
    public void T(Intent intent) {
        pq7.c(intent, "intent");
        if (this.x.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, this.g, true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardPresenter", "mSyncReceiver - syncStatus: " + intExtra + " - mIntendingSync: " + this.p);
                if (intExtra != 0) {
                    if (intExtra == 1) {
                        this.p = false;
                        this.x.Z1(true);
                        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "sync complete, check for low battery");
                        Y();
                        xw7 unused = gu7.d(k(), bw7.b(), null, new h(this, null), 2, null);
                    } else if (intExtra == 2 || intExtra == 4) {
                        this.p = false;
                        this.x.Z1(false);
                        if (intExtra2 == 12) {
                            int intExtra3 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                            if (integerArrayListExtra == null) {
                                integerArrayListExtra = new ArrayList<>();
                            }
                            if (intExtra3 != 1101) {
                                if (intExtra3 == 1603) {
                                    this.x.Z1(false);
                                    return;
                                } else if (!(intExtra3 == 1112 || intExtra3 == 1113)) {
                                    this.x.q6();
                                    xw7 unused2 = gu7.d(k(), bw7.b(), null, new i(this, null), 2, null);
                                    return;
                                }
                            }
                            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(integerArrayListExtra);
                            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                            ld6 ld6 = this.x;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                            if (array != null) {
                                uh5[] uh5Arr = (uh5[]) array;
                                ld6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    } else if (intExtra == 5) {
                        this.x.t1();
                    } else if (intExtra == 6) {
                        this.p = false;
                        this.x.Z1(false);
                        this.x.U0();
                    }
                } else if (!this.p) {
                    this.p = true;
                    this.x.t6();
                }
            }
        }
    }

    @DexIgnore
    public void U(boolean z2) {
        this.x.A(z2);
    }

    @DexIgnore
    public void V() {
        this.x.Q5();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object W(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.md6.j
            if (r0 == 0) goto L_0x0042
            r0 = r7
            com.fossil.md6$j r0 = (com.fossil.md6.j) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0050
            if (r3 != r4) goto L_0x0048
            java.lang.Object r0 = r0.L$0
            com.fossil.md6 r0 = (com.fossil.md6) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.pt4 r0 = (com.fossil.pt4) r0
            if (r0 == 0) goto L_0x0061
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r0 = r0.a()
            r1.m1(r0)
        L_0x003f:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0041:
            return r0
        L_0x0042:
            com.fossil.md6$j r0 = new com.fossil.md6$j
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.el7.b(r1)
            com.fossil.tt4 r1 = r6.G
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.v(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0041
        L_0x0061:
            com.fossil.on5 r0 = r6.E
            java.lang.Long r0 = r0.d()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "retrySetSyncData - endTimeOfUnsetChallenge: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r3 = " - exact: "
            r2.append(r3)
            com.fossil.xy4 r3 = com.fossil.xy4.f4212a
            long r4 = r3.b()
            r2.append(r4)
            java.lang.String r3 = "HomeDashboardPresenter"
            java.lang.String r2 = r2.toString()
            r1.e(r3, r2)
            long r0 = r0.longValue()
            com.fossil.xy4 r2 = com.fossil.xy4.f4212a
            long r2 = r2.b()
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00a5
            com.fossil.cl5 r0 = com.fossil.cl5.c
            r0.j()
            goto L_0x003f
        L_0x00a5:
            com.fossil.on5 r0 = r6.E
            r2 = 0
            java.lang.Long r1 = com.fossil.ao7.f(r2)
            r0.e(r1)
            com.fossil.on5 r0 = r6.E
            r1 = 0
            java.lang.Boolean r1 = com.fossil.ao7.a(r1)
            r0.m0(r1)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.md6.W(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public void X() {
        this.x.M5(this);
    }

    @DexIgnore
    public final void Y() {
        xw7 unused = gu7.d(k(), null, null, new k(this, null), 3, null);
    }

    @DexIgnore
    public final void Z() {
        Integer num;
        HeartRateSample heartRateSample;
        Resting resting;
        boolean z2;
        boolean z3 = true;
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "updateHomeInfo");
        this.x.g5(this.j, this.i, this.k);
        List<HeartRateSample> list = this.m;
        if (list != null) {
            ListIterator<HeartRateSample> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    heartRateSample = null;
                    break;
                }
                HeartRateSample previous = listIterator.previous();
                if (previous.getResting() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    heartRateSample = previous;
                    break;
                }
            }
            HeartRateSample heartRateSample2 = heartRateSample;
            num = Integer.valueOf((heartRateSample2 == null || (resting = heartRateSample2.getResting()) == null) ? 0 : resting.getValue());
        } else {
            num = null;
        }
        if (!(this.g.length() == 0) || !this.o) {
            z3 = false;
        }
        this.x.J1(this.j, this.i, this.k, this.l, num, z3);
        ld6 ld6 = this.x;
        Date date = this.e;
        if (date != null) {
            ld6.r2(date);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void a0(int i2) {
        this.x.o0(i2);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "start - this=" + hashCode());
        Date date = new Date();
        Date date2 = this.e;
        if (date2 == null || !lk5.m0(date, date2)) {
            this.e = date;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HomeDashboardPresenter", "date change " + this.e);
            this.r.l(this.e);
        }
        PortfolioApp portfolioApp = this.y;
        b bVar = this.q;
        portfolioApp.registerReceiver(bVar, new IntentFilter(this.y.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        R();
        LiveData<h47<ActivitySummary>> liveData = this.s;
        ld6 ld6 = this.x;
        if (ld6 != null) {
            liveData.h(((az5) ld6).getViewLifecycleOwner(), new l(this));
            this.t.h(((az5) this.x).getViewLifecycleOwner(), new m(this));
            this.u.h(((az5) this.x).getViewLifecycleOwner(), new n(this));
            this.v.h(((az5) this.x).getViewLifecycleOwner(), new o(this));
            this.w.h(((az5) this.x).getViewLifecycleOwner(), new p(this));
            S();
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "stop");
        try {
            this.y.unregisterReceiver(this.q);
            ld6 ld6 = this.x;
            if (ld6 != null) {
                LifecycleOwner viewLifecycleOwner = ((az5) ld6).getViewLifecycleOwner();
                this.s.n(viewLifecycleOwner);
                this.t.n(viewLifecycleOwner);
                this.u.n(viewLifecycleOwner);
                this.v.n(viewLifecycleOwner);
                this.w.n(viewLifecycleOwner);
                PortfolioApp.h0.c().K().n(viewLifecycleOwner);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeDashboardPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.h;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public int o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public void p(String str, boolean z2) {
        pq7.c(str, "activeId");
        this.y.x(str, z2);
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public void q(int i2) {
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public void r() {
        String J = this.y.J();
        int R = this.y.R(J);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "Inside .onRefresh currentDeviceSession=" + R);
        if (TextUtils.isEmpty(J) || R == CommunicateMode.OTA.getValue()) {
            this.x.Z1(false);
            return;
        }
        this.p = true;
        this.x.t6();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.y.J(), "HomeDashboardPresenter", "[Sync Start] PULL TO SYNC");
        this.y.S1(this.A, false, 12);
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public void s(int i2) {
        if (i2 == this.f) {
            this.x.G0();
        }
    }

    @DexIgnore
    @Override // com.fossil.kd6
    public void t(boolean z2) {
        this.p = z2;
    }
}
