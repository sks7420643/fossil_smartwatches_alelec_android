package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt5 implements Factory<mt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<QuickResponseRepository> f2567a;

    @DexIgnore
    public nt5(Provider<QuickResponseRepository> provider) {
        this.f2567a = provider;
    }

    @DexIgnore
    public static nt5 a(Provider<QuickResponseRepository> provider) {
        return new nt5(provider);
    }

    @DexIgnore
    public static mt5 c(QuickResponseRepository quickResponseRepository) {
        return new mt5(quickResponseRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public mt5 get() {
        return c(this.f2567a.get());
    }
}
