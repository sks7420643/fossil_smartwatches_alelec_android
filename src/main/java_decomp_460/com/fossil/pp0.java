package com.fossil;

import android.database.Cursor;
import android.widget.Filter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pp0 extends Filter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public a f2853a;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        Cursor b();

        @DexIgnore
        CharSequence d(Cursor cursor);

        @DexIgnore
        Cursor e(CharSequence charSequence);
    }

    @DexIgnore
    public pp0(a aVar) {
        this.f2853a = aVar;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        return this.f2853a.d((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor e = this.f2853a.e(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (e != null) {
            filterResults.count = e.getCount();
            filterResults.values = e;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor b = this.f2853a.b();
        Object obj = filterResults.values;
        if (obj != null && obj != b) {
            this.f2853a.a((Cursor) obj);
        }
    }
}
