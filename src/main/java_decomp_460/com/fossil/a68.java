package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a68 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ char f205a; // = ((char) File.separatorChar);

    /*
    static {
        Character.toString('.');
        c();
    }
    */

    @DexIgnore
    public static String a(String str) {
        if (str == null) {
            return null;
        }
        return str.substring(b(str) + 1);
    }

    @DexIgnore
    public static int b(String str) {
        if (str == null) {
            return -1;
        }
        return Math.max(str.lastIndexOf(47), str.lastIndexOf(92));
    }

    @DexIgnore
    public static boolean c() {
        return f205a == '\\';
    }
}
