package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.zc6;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s66 extends pv5 {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<h55> h;
    @DexIgnore
    public zc6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final s66 a(String str) {
            pq7.c(str, "watchAppId");
            s66 s66 = new s66();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            s66.setArguments(bundle);
            return s66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s66 b;

        @DexIgnore
        public b(s66 s66) {
            this.b = s66;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<zc6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s66 f3210a;
        @DexIgnore
        public /* final */ /* synthetic */ h55 b;

        @DexIgnore
        public c(s66 s66, h55 h55) {
            this.f3210a = s66;
            this.b = h55;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(zc6.a aVar) {
            Integer a2 = aVar.a();
            if (a2 != null) {
                ((va1) ((va1) oa1.v(this.f3210a).r(Integer.valueOf(a2.intValue())).l(wc1.f3916a)).m0(true)).F0(this.b.r);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        h55 h55 = (h55) aq0.f(layoutInflater, 2131558541, viewGroup, false, A6());
        PortfolioApp.h0.c().M().U0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(zc6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.i = (zc6) a2;
            h55.q.setOnClickListener(new b(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                zc6 zc6 = this.i;
                if (zc6 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        pq7.b(string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        zc6.d(string);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            }
            zc6 zc62 = this.i;
            if (zc62 != null) {
                zc62.c().h(getViewLifecycleOwner(), new c(this, h55));
                g37<h55> g37 = new g37<>(this, h55);
                this.h = g37;
                if (g37 != null) {
                    h55 a3 = g37.a();
                    if (a3 != null) {
                        pq7.b(a3, "mBinding.get()!!");
                        return a3.n();
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
