package com.fossil;

import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw0 {
    @DexIgnore
    public static final dv7 a(qw0 qw0) {
        pq7.c(qw0, "$this$queryDispatcher");
        Map<String, Object> backingFieldMap = qw0.getBackingFieldMap();
        pq7.b(backingFieldMap, "backingFieldMap");
        Object obj = backingFieldMap.get("QueryDispatcher");
        if (obj == null) {
            Executor queryExecutor = qw0.getQueryExecutor();
            pq7.b(queryExecutor, "queryExecutor");
            obj = pw7.a(queryExecutor);
            backingFieldMap.put("QueryDispatcher", obj);
        }
        if (obj != null) {
            return (dv7) obj;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }

    @DexIgnore
    public static final dv7 b(qw0 qw0) {
        pq7.c(qw0, "$this$transactionDispatcher");
        Map<String, Object> backingFieldMap = qw0.getBackingFieldMap();
        pq7.b(backingFieldMap, "backingFieldMap");
        Object obj = backingFieldMap.get("TransactionDispatcher");
        if (obj == null) {
            Executor transactionExecutor = qw0.getTransactionExecutor();
            pq7.b(transactionExecutor, "transactionExecutor");
            obj = pw7.a(transactionExecutor);
            backingFieldMap.put("TransactionDispatcher", obj);
        }
        if (obj != null) {
            return (dv7) obj;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }
}
