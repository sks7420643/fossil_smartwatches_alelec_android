package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl1 extends fl1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jl1 createFromParcel(Parcel parcel) {
            fl1 b = fl1.CREATOR.createFromParcel(parcel);
            if (b != null) {
                return (jl1) b;
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotAlarm");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jl1[] newArray(int i) {
            return new jl1[i];
        }
    }

    @DexIgnore
    public jl1(kl1 kl1, ql1 ql1, il1 il1) throws IllegalArgumentException {
        super(kl1, ql1, il1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jl1(kl1 kl1, ql1 ql1, il1 il1, int i, kq7 kq7) throws IllegalArgumentException {
        this(kl1, (i & 2) != 0 ? null : ql1, (i & 4) != 0 ? null : il1);
    }

    @DexIgnore
    @Override // com.fossil.el1
    public kl1 getFireTime() {
        gl1[] b = b();
        for (gl1 gl1 : b) {
            if (gl1 instanceof kl1) {
                if (gl1 != null) {
                    return (kl1) gl1;
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotFireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
