package com.fossil;

import com.fossil.xb1;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wg1 implements xb1<ByteBuffer> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ByteBuffer f3932a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements xb1.a<ByteBuffer> {
        @DexIgnore
        /* renamed from: b */
        public xb1<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new wg1(byteBuffer);
        }

        @DexIgnore
        @Override // com.fossil.xb1.a
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore
    public wg1(ByteBuffer byteBuffer) {
        this.f3932a = byteBuffer;
    }

    @DexIgnore
    @Override // com.fossil.xb1
    public void a() {
    }

    @DexIgnore
    /* renamed from: c */
    public ByteBuffer b() {
        this.f3932a.position(0);
        return this.f3932a;
    }
}
