package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg2 implements Parcelable.Creator<b62> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b62 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str = ad2.f(parcel, t);
            } else if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                j = ad2.y(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new b62(str, i, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b62[] newArray(int i) {
        return new b62[i];
    }
}
