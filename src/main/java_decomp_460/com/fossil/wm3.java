package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm3 implements Callable<List<xr3>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ or3 f3971a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 d;

    @DexIgnore
    public wm3(qm3 qm3, or3 or3, String str, String str2) {
        this.d = qm3;
        this.f3971a = or3;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<xr3> call() throws Exception {
        this.d.b.d0();
        return this.d.b.U().j0(this.f3971a.b, this.b, this.c);
    }
}
