package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<dt4> f842a; // = new ArrayList();
    @DexIgnore
    public /* final */ int b; // = gl0.d(PortfolioApp.h0.c(), 2131099689);
    @DexIgnore
    public /* final */ int c; // = gl0.d(PortfolioApp.h0.c(), 2131099677);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ f57.b f843a;
        @DexIgnore
        public /* final */ uy4 b; // = uy4.d.b();
        @DexIgnore
        public /* final */ pd5 c;
        @DexIgnore
        public /* final */ /* synthetic */ dw4 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dw4 dw4, pd5 pd5) {
            super(pd5.n());
            pq7.c(pd5, "binding");
            this.d = dw4;
            this.c = pd5;
            f57.b f = f57.a().f();
            pq7.b(f, "TextDrawable.builder().round()");
            this.f843a = f;
        }

        @DexIgnore
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:181:0x050c  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0092  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00a6  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.fossil.dt4 r12) {
            /*
            // Method dump skipped, instructions count: 1330
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dw4.a.a(com.fossil.dt4):void");
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f842a.size();
    }

    @DexIgnore
    public final void i(List<dt4> list) {
        pq7.c(list, "data");
        this.f842a.clear();
        this.f842a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        ((a) viewHolder).a(this.f842a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        pd5 z = pd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemBcNotificationChalle\u2026tInflater, parent, false)");
        return new a(this, z);
    }
}
