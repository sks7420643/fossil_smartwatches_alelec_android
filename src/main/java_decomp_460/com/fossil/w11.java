package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3865a; // = x01.f("Alarms");

    @DexIgnore
    public static void a(Context context, s11 s11, String str) {
        g31 g = s11.p().g();
        f31 b = g.b(str);
        if (b != null) {
            b(context, str, b.b);
            x01.c().a(f3865a, String.format("Removing SystemIdInfo for workSpecId (%s)", str), new Throwable[0]);
            g.c(str);
        }
    }

    @DexIgnore
    public static void b(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, x11.b(context, str), 536870912);
        if (service != null && alarmManager != null) {
            x01.c().a(f3865a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i)), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    @DexIgnore
    public static void c(Context context, s11 s11, String str, long j) {
        WorkDatabase p = s11.p();
        g31 g = p.g();
        f31 b = g.b(str);
        if (b != null) {
            b(context, str, b.b);
            d(context, str, b.b, j);
            return;
        }
        int b2 = new x31(p).b();
        g.a(new f31(str, b2));
        d(context, str, b2, j);
    }

    @DexIgnore
    public static void d(Context context, String str, int i, long j) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, x11.b(context, str), 134217728);
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }
}
