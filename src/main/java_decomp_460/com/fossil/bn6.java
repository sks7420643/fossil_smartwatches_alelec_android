package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDaySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn6 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f459a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public ArrayList<b> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ OverviewSleepDayChart f460a;
        @DexIgnore
        public /* final */ OverviewSleepDaySummary b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ FlexibleTextView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ FlexibleTextView h;
        @DexIgnore
        public /* final */ FlexibleTextView i;
        @DexIgnore
        public /* final */ FlexibleTextView j;
        @DexIgnore
        public /* final */ FlexibleTextView k;
        @DexIgnore
        public /* final */ FlexibleTextView l;
        @DexIgnore
        public /* final */ FlexibleTextView m;
        @DexIgnore
        public /* final */ FlexibleTextView n;
        @DexIgnore
        public /* final */ FlexibleTextView o;
        @DexIgnore
        public /* final */ FlexibleTextView p;
        @DexIgnore
        public /* final */ FlexibleTextView q;
        @DexIgnore
        public /* final */ ConstraintLayout r;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            pq7.c(view, "item");
            View findViewById = view.findViewById(2131363122);
            pq7.b(findViewById, "item.findViewById(R.id.sleep_chart)");
            this.f460a = (OverviewSleepDayChart) findViewById;
            View findViewById2 = view.findViewById(2131363470);
            pq7.b(findViewById2, "item.findViewById(R.id.v_summary_chart)");
            this.b = (OverviewSleepDaySummary) findViewById2;
            View findViewById3 = view.findViewById(2131363276);
            pq7.b(findViewById3, "item.findViewById(R.id.tv_awake)");
            this.c = (FlexibleTextView) findViewById3;
            View findViewById4 = view.findViewById(2131363349);
            pq7.b(findViewById4, "item.findViewById(R.id.tv_light)");
            this.d = (FlexibleTextView) findViewById4;
            View findViewById5 = view.findViewById(2131363309);
            pq7.b(findViewById5, "item.findViewById(R.id.tv_deep)");
            this.e = (FlexibleTextView) findViewById5;
            View findViewById6 = view.findViewById(2131363280);
            pq7.b(findViewById6, "item.findViewById(R.id.tv_awake_min)");
            this.f = (FlexibleTextView) findViewById6;
            View findViewById7 = view.findViewById(2131363353);
            pq7.b(findViewById7, "item.findViewById(R.id.tv_light_min)");
            this.g = (FlexibleTextView) findViewById7;
            View findViewById8 = view.findViewById(2131363313);
            pq7.b(findViewById8, "item.findViewById(R.id.tv_deep_min)");
            this.h = (FlexibleTextView) findViewById8;
            View findViewById9 = view.findViewById(2131363278);
            pq7.b(findViewById9, "item.findViewById(R.id.tv_awake_hour)");
            this.i = (FlexibleTextView) findViewById9;
            View findViewById10 = view.findViewById(2131363351);
            pq7.b(findViewById10, "item.findViewById(R.id.tv_light_hour)");
            this.j = (FlexibleTextView) findViewById10;
            View findViewById11 = view.findViewById(2131363311);
            pq7.b(findViewById11, "item.findViewById(R.id.tv_deep_hour)");
            this.k = (FlexibleTextView) findViewById11;
            View findViewById12 = view.findViewById(2131363279);
            pq7.b(findViewById12, "item.findViewById(R.id.tv_awake_hour_unit)");
            this.l = (FlexibleTextView) findViewById12;
            View findViewById13 = view.findViewById(2131363352);
            pq7.b(findViewById13, "item.findViewById(R.id.tv_light_hour_unit)");
            this.m = (FlexibleTextView) findViewById13;
            View findViewById14 = view.findViewById(2131363312);
            pq7.b(findViewById14, "item.findViewById(R.id.tv_deep_hour_unit)");
            this.n = (FlexibleTextView) findViewById14;
            View findViewById15 = view.findViewById(2131363281);
            pq7.b(findViewById15, "item.findViewById(R.id.tv_awake_min_unit)");
            this.o = (FlexibleTextView) findViewById15;
            View findViewById16 = view.findViewById(2131363354);
            pq7.b(findViewById16, "item.findViewById(R.id.tv_light_min_unit)");
            this.p = (FlexibleTextView) findViewById16;
            View findViewById17 = view.findViewById(2131363314);
            pq7.b(findViewById17, "item.findViewById(R.id.tv_deep_min_unit)");
            this.q = (FlexibleTextView) findViewById17;
            View findViewById18 = view.findViewById(2131362108);
            pq7.b(findViewById18, "item.findViewById(R.id.cl_sleep_chart)");
            this.r = (ConstraintLayout) findViewById18;
        }

        @DexIgnore
        public final ConstraintLayout a() {
            return this.r;
        }

        @DexIgnore
        public final OverviewSleepDayChart b() {
            return this.f460a;
        }

        @DexIgnore
        public final OverviewSleepDaySummary c() {
            return this.b;
        }

        @DexIgnore
        public final FlexibleTextView d() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView e() {
            return this.i;
        }

        @DexIgnore
        public final FlexibleTextView f() {
            return this.l;
        }

        @DexIgnore
        public final FlexibleTextView g() {
            return this.f;
        }

        @DexIgnore
        public final FlexibleTextView h() {
            return this.o;
        }

        @DexIgnore
        public final FlexibleTextView i() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView j() {
            return this.k;
        }

        @DexIgnore
        public final FlexibleTextView k() {
            return this.n;
        }

        @DexIgnore
        public final FlexibleTextView l() {
            return this.h;
        }

        @DexIgnore
        public final FlexibleTextView m() {
            return this.q;
        }

        @DexIgnore
        public final FlexibleTextView n() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView o() {
            return this.j;
        }

        @DexIgnore
        public final FlexibleTextView p() {
            return this.m;
        }

        @DexIgnore
        public final FlexibleTextView q() {
            return this.g;
        }

        @DexIgnore
        public final FlexibleTextView r() {
            return this.p;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public mv5 f461a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public b(mv5 mv5, float f2, float f3, float f4, int i, int i2, int i3, int i4) {
            pq7.c(mv5, "sleepChartModel");
            this.f461a = mv5;
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = i;
            this.f = i2;
            this.g = i3;
            this.h = i4;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final float b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.g;
        }

        @DexIgnore
        public final float d() {
            return this.d;
        }

        @DexIgnore
        public final int e() {
            return this.f;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(pq7.a(this.f461a, bVar.f461a) && Float.compare(this.b, bVar.b) == 0 && Float.compare(this.c, bVar.c) == 0 && Float.compare(this.d, bVar.d) == 0 && this.e == bVar.e && this.f == bVar.f && this.g == bVar.g && this.h == bVar.h)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final float f() {
            return this.c;
        }

        @DexIgnore
        public final mv5 g() {
            return this.f461a;
        }

        @DexIgnore
        public final int h() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            mv5 mv5 = this.f461a;
            return ((((((((((((((mv5 != null ? mv5.hashCode() : 0) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d)) * 31) + this.e) * 31) + this.f) * 31) + this.g) * 31) + this.h;
        }

        @DexIgnore
        public String toString() {
            return "SleepUIData(sleepChartModel=" + this.f461a + ", awakePer=" + this.b + ", lightPer=" + this.c + ", deepPer=" + this.d + ", awakeMin=" + this.e + ", lightMin=" + this.f + ", deepMin=" + this.g + ", timeZoneOffsetInSecond=" + this.h + ")";
        }
    }

    @DexIgnore
    public bn6(ArrayList<b> arrayList) {
        pq7.c(arrayList, "data");
        this.e = arrayList;
        String d2 = qn5.l.a().d("awakeSleep");
        this.f459a = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("lightSleep");
        this.b = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("deepSleep");
        this.c = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandSurface");
        this.d = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        b bVar = this.e.get(i);
        pq7.b(bVar, "data[position]");
        b bVar2 = bVar;
        aVar.d().setText(jl5.b.l(2131886693, bVar2.b()));
        aVar.n().setText(jl5.b.l(2131886695, bVar2.f()));
        aVar.i().setText(jl5.b.l(2131886694, bVar2.d()));
        aVar.g().setText(String.valueOf(ll5.b(bVar2.a())));
        aVar.q().setText(String.valueOf(ll5.b(bVar2.e())));
        aVar.l().setText(String.valueOf(ll5.b(bVar2.c())));
        aVar.e().setText(String.valueOf(ll5.a(bVar2.a())));
        aVar.o().setText(String.valueOf(ll5.a(bVar2.e())));
        aVar.j().setText(String.valueOf(ll5.a(bVar2.c())));
        OverviewSleepDayChart b2 = aVar.b();
        BarChart.F(b2, 0, this.f459a, this.b, this.c, null, null, 49, null);
        b2.setTimeZoneOffsetInSecond(bVar2.h());
        b2.r(bVar2.g());
        aVar.c().c(bVar2.b(), bVar2.f(), bVar2.d(), this.f459a, this.b, this.c);
        aVar.d().setTextColor(this.f459a);
        aVar.n().setTextColor(this.b);
        aVar.i().setTextColor(this.c);
        aVar.e().setTextColor(this.f459a);
        aVar.g().setTextColor(this.f459a);
        aVar.f().setTextColor(this.f459a);
        aVar.h().setTextColor(this.f459a);
        aVar.o().setTextColor(this.b);
        aVar.q().setTextColor(this.b);
        aVar.p().setTextColor(this.b);
        aVar.r().setTextColor(this.b);
        aVar.j().setTextColor(this.c);
        aVar.l().setTextColor(this.c);
        aVar.k().setTextColor(this.c);
        aVar.m().setTextColor(this.c);
        aVar.a().setBackgroundColor(this.d);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558701, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026p_session, parent, false)");
        return new a(inflate);
    }

    @DexIgnore
    public final void i(List<b> list) {
        pq7.c(list, "data");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }
}
