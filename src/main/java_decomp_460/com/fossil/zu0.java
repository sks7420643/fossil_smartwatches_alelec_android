package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zu0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Comparator<g> f4531a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Comparator<g> {
        @DexIgnore
        /* renamed from: a */
        public int compare(g gVar, g gVar2) {
            int i = gVar.f4535a - gVar2.f4535a;
            return i == 0 ? gVar.b - gVar2.b : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public abstract boolean a(int i, int i2);

        @DexIgnore
        public abstract boolean b(int i, int i2);

        @DexIgnore
        public abstract Object c(int i, int i2);

        @DexIgnore
        public abstract int d();

        @DexIgnore
        public abstract int e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<g> f4532a;
        @DexIgnore
        public /* final */ int[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public /* final */ b d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;

        @DexIgnore
        public c(b bVar, List<g> list, int[] iArr, int[] iArr2, boolean z) {
            this.f4532a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(iArr, 0);
            Arrays.fill(this.c, 0);
            this.d = bVar;
            this.e = bVar.e();
            this.f = bVar.d();
            this.g = z;
            a();
            h();
        }

        @DexIgnore
        public static e j(List<e> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                e eVar = list.get(size);
                if (eVar.f4533a == i && eVar.c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        e eVar2 = list.get(size);
                        eVar2.b = (z ? 1 : -1) + eVar2.b;
                        size++;
                    }
                    return eVar;
                }
                size--;
            }
            return null;
        }

        @DexIgnore
        public final void a() {
            g gVar = this.f4532a.isEmpty() ? null : this.f4532a.get(0);
            if (gVar == null || gVar.f4535a != 0 || gVar.b != 0) {
                g gVar2 = new g();
                gVar2.f4535a = 0;
                gVar2.b = 0;
                gVar2.d = false;
                gVar2.c = 0;
                gVar2.e = false;
                this.f4532a.add(0, gVar2);
            }
        }

        @DexIgnore
        public int b(int i) {
            if (i < 0 || i >= this.e) {
                throw new IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.e);
            }
            int i2 = this.b[i];
            if ((i2 & 31) == 0) {
                return -1;
            }
            return i2 >> 5;
        }

        @DexIgnore
        public final void c(List<e> list, jv0 jv0, int i, int i2, int i3) {
            if (!this.g) {
                jv0.b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.c[i5] & 31;
                if (i6 == 0) {
                    jv0.b(i, 1);
                    for (e eVar : list) {
                        eVar.b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.c[i5] >> 5;
                    jv0.a(j(list, i7, true).b, i);
                    if (i6 == 4) {
                        jv0.d(i, 1, this.d.c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i, false));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public final void d(List<e> list, jv0 jv0, int i, int i2, int i3) {
            if (!this.g) {
                jv0.c(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.b[i5] & 31;
                if (i6 == 0) {
                    jv0.c(i + i4, 1);
                    for (e eVar : list) {
                        eVar.b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.b[i5] >> 5;
                    e j = j(list, i7, false);
                    jv0.a(i + i4, j.b - 1);
                    if (i6 == 4) {
                        jv0.d(j.b - 1, 1, this.d.c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i + i4, true));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public void e(jv0 jv0) {
            wu0 wu0 = jv0 instanceof wu0 ? (wu0) jv0 : new wu0(jv0);
            List<e> arrayList = new ArrayList<>();
            int i = this.e;
            int i2 = this.f;
            int size = this.f4532a.size() - 1;
            int i3 = i2;
            while (size >= 0) {
                g gVar = this.f4532a.get(size);
                int i4 = gVar.c;
                int i5 = gVar.f4535a + i4;
                int i6 = gVar.b + i4;
                if (i5 < i) {
                    d(arrayList, wu0, i5, i - i5, i5);
                }
                if (i6 < i3) {
                    c(arrayList, wu0, i5, i3 - i6, i6);
                }
                for (int i7 = i4 - 1; i7 >= 0; i7--) {
                    int[] iArr = this.b;
                    int i8 = gVar.f4535a;
                    if ((iArr[i8 + i7] & 31) == 2) {
                        wu0.d(i8 + i7, 1, this.d.c(i8 + i7, gVar.b + i7));
                    }
                }
                i = gVar.f4535a;
                size--;
                i3 = gVar.b;
            }
            wu0.e();
        }

        @DexIgnore
        public final void f(int i, int i2, int i3) {
            if (this.b[i - 1] == 0) {
                g(i, i2, i3, false);
            }
        }

        @DexIgnore
        public final boolean g(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                int i6 = i2 - 1;
                i4 = i6;
                i2 = i6;
                i5 = i;
            } else {
                int i7 = i - 1;
                i4 = i7;
                i5 = i7;
            }
            while (i3 >= 0) {
                g gVar = this.f4532a.get(i3);
                int i8 = gVar.f4535a;
                int i9 = gVar.c;
                int i10 = gVar.b;
                int i11 = 8;
                if (z) {
                    while (true) {
                        i5--;
                        if (i5 < i8 + i9) {
                            continue;
                            break;
                        } else if (this.d.b(i5, i4)) {
                            int i12 = this.d.a(i5, i4) ? 8 : 4;
                            this.c[i4] = (i5 << 5) | 16;
                            this.b[i5] = i12 | (i4 << 5);
                            return true;
                        }
                    }
                } else {
                    for (int i13 = i2 - 1; i13 >= i10 + i9; i13--) {
                        if (this.d.b(i4, i13)) {
                            if (!this.d.a(i4, i13)) {
                                i11 = 4;
                            }
                            int i14 = i - 1;
                            this.b[i14] = (i13 << 5) | 16;
                            this.c[i13] = i11 | (i14 << 5);
                            return true;
                        }
                    }
                    continue;
                }
                int i15 = gVar.f4535a;
                i3--;
                i2 = gVar.b;
                i5 = i15;
            }
            return false;
        }

        @DexIgnore
        public final void h() {
            int i = this.e;
            int i2 = this.f;
            for (int size = this.f4532a.size() - 1; size >= 0; size--) {
                g gVar = this.f4532a.get(size);
                int i3 = gVar.f4535a;
                int i4 = gVar.c;
                int i5 = gVar.b;
                if (this.g) {
                    while (i > i3 + i4) {
                        f(i, i2, size);
                        i--;
                    }
                    while (i2 > i5 + i4) {
                        i(i, i2, size);
                        i2--;
                    }
                }
                for (int i6 = 0; i6 < gVar.c; i6++) {
                    int i7 = gVar.f4535a + i6;
                    int i8 = gVar.b + i6;
                    int i9 = this.d.a(i7, i8) ? 1 : 2;
                    this.b[i7] = (i8 << 5) | i9;
                    this.c[i8] = i9 | (i7 << 5);
                }
                i = gVar.f4535a;
                i2 = gVar.b;
            }
        }

        @DexIgnore
        public final void i(int i, int i2, int i3) {
            if (this.c[i2 - 1] == 0) {
                g(i, i2, i3, true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<T> {
        @DexIgnore
        public abstract boolean areContentsTheSame(T t, T t2);

        @DexIgnore
        public abstract boolean areItemsTheSame(T t, T t2);

        @DexIgnore
        public Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4533a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public e(int i, int i2, boolean z) {
            this.f4533a = i;
            this.b = i2;
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4534a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public f(int i, int i2, int i3, int i4) {
            this.f4534a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4535a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
    }

    @DexIgnore
    public static c a(b bVar) {
        return b(bVar, true);
    }

    @DexIgnore
    public static c b(b bVar, boolean z) {
        int e2 = bVar.e();
        int d2 = bVar.d();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new f(0, e2, 0, d2));
        int abs = e2 + d2 + Math.abs(e2 - d2);
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            f fVar = (f) arrayList2.remove(arrayList2.size() - 1);
            g c2 = c(bVar, fVar.f4534a, fVar.b, fVar.c, fVar.d, iArr, iArr2, abs);
            if (c2 != null) {
                if (c2.c > 0) {
                    arrayList.add(c2);
                }
                c2.f4535a += fVar.f4534a;
                c2.b += fVar.c;
                f fVar2 = arrayList3.isEmpty() ? new f() : (f) arrayList3.remove(arrayList3.size() - 1);
                fVar2.f4534a = fVar.f4534a;
                fVar2.c = fVar.c;
                if (c2.e) {
                    fVar2.b = c2.f4535a;
                    fVar2.d = c2.b;
                } else if (c2.d) {
                    fVar2.b = c2.f4535a - 1;
                    fVar2.d = c2.b;
                } else {
                    fVar2.b = c2.f4535a;
                    fVar2.d = c2.b - 1;
                }
                arrayList2.add(fVar2);
                if (!c2.e) {
                    int i2 = c2.f4535a;
                    int i3 = c2.c;
                    fVar.f4534a = i2 + i3;
                    fVar.c = c2.b + i3;
                } else if (c2.d) {
                    int i4 = c2.f4535a;
                    int i5 = c2.c;
                    fVar.f4534a = i4 + i5 + 1;
                    fVar.c = c2.b + i5;
                } else {
                    int i6 = c2.f4535a;
                    int i7 = c2.c;
                    fVar.f4534a = i6 + i7;
                    fVar.c = c2.b + i7 + 1;
                }
                arrayList2.add(fVar);
            } else {
                arrayList3.add(fVar);
            }
        }
        Collections.sort(arrayList, f4531a);
        return new c(bVar, arrayList, iArr, iArr2, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        if (r20[r2 - 1] < r20[r2 + 1]) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00be, code lost:
        if (r21[r2 - 1] < r21[r2 + 1]) goto L_0x00c0;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.zu0.g c(com.fossil.zu0.b r15, int r16, int r17, int r18, int r19, int[] r20, int[] r21, int r22) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zu0.c(com.fossil.zu0$b, int, int, int, int, int[], int[], int):com.fossil.zu0$g");
    }
}
