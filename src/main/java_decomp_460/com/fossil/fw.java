package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fw extends tv {
    @DexIgnore
    public long L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fw(long j, long j2, long j3, short s, k5 k5Var, int i, int i2) {
        super(sv.LEGACY_VERIFY_SEGMENT, s, hs.R, k5Var, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = true;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.T2, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            long o = hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            this.L = o;
            g80.k(jSONObject, jd0.T2, Long.valueOf(o));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        pq7.b(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.tv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(g80.k(super.z(), jd0.Q2, Long.valueOf(this.N)), jd0.R2, Long.valueOf(this.O)), jd0.S2, Long.valueOf(this.P));
    }
}
