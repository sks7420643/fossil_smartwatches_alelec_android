package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp4 implements Factory<do5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3004a;
    @DexIgnore
    public /* final */ Provider<on5> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<oq4> e;

    @DexIgnore
    public qp4(uo4 uo4, Provider<on5> provider, Provider<DeviceRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<oq4> provider4) {
        this.f3004a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
    }

    @DexIgnore
    public static qp4 a(uo4 uo4, Provider<on5> provider, Provider<DeviceRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<oq4> provider4) {
        return new qp4(uo4, provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static do5 c(uo4 uo4, on5 on5, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, oq4 oq4) {
        do5 x = uo4.x(on5, deviceRepository, dianaPresetRepository, oq4);
        lk7.c(x, "Cannot return null from a non-@Nullable @Provides method");
        return x;
    }

    @DexIgnore
    /* renamed from: b */
    public do5 get() {
        return c(this.f3004a, this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
