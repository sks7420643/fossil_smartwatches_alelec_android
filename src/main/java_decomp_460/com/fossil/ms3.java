package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ms3 extends IInterface {
    @DexIgnore
    void F0(jc2 jc2, int i, boolean z) throws RemoteException;

    @DexIgnore
    void K(int i) throws RemoteException;

    @DexIgnore
    void Q1(ss3 ss3, ks3 ks3) throws RemoteException;
}
