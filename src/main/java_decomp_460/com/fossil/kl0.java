package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.Base64;
import android.util.TypedValue;
import android.util.Xml;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kl0 {

    @DexIgnore
    public interface a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c[] f1926a;

        @DexIgnore
        public b(c[] cVarArr) {
            this.f1926a = cVarArr;
        }

        @DexIgnore
        public c[] a() {
            return this.f1926a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1927a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public c(String str, int i, boolean z, String str2, int i2, int i3) {
            this.f1927a = str;
            this.b = i;
            this.c = z;
            this.d = str2;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public String a() {
            return this.f1927a;
        }

        @DexIgnore
        public int b() {
            return this.f;
        }

        @DexIgnore
        public int c() {
            return this.e;
        }

        @DexIgnore
        public String d() {
            return this.d;
        }

        @DexIgnore
        public int e() {
            return this.b;
        }

        @DexIgnore
        public boolean f() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ym0 f1928a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(ym0 ym0, int i, int i2) {
            this.f1928a = ym0;
            this.c = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public ym0 b() {
            return this.f1928a;
        }

        @DexIgnore
        public int c() {
            return this.b;
        }
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return typedArray.getType(i);
        }
        TypedValue typedValue = new TypedValue();
        typedArray.getValue(i, typedValue);
        return typedValue.type;
    }

    @DexIgnore
    public static a b(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        int next;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return d(xmlPullParser, resources);
        }
        throw new XmlPullParserException("No start tag found");
    }

    @DexIgnore
    public static List<List<byte[]>> c(Resources resources, int i) {
        if (i == 0) {
            return Collections.emptyList();
        }
        TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            if (a(obtainTypedArray, 0) == 1) {
                for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                    int resourceId = obtainTypedArray.getResourceId(i2, 0);
                    if (resourceId != 0) {
                        arrayList.add(h(resources.getStringArray(resourceId)));
                    }
                }
            } else {
                arrayList.add(h(resources.getStringArray(i)));
            }
            obtainTypedArray.recycle();
            return arrayList;
        } finally {
            obtainTypedArray.recycle();
        }
    }

    @DexIgnore
    public static a d(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return e(xmlPullParser, resources);
        }
        g(xmlPullParser);
        return null;
    }

    @DexIgnore
    public static a e(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), qk0.FontFamily);
        String string = obtainAttributes.getString(qk0.FontFamily_fontProviderAuthority);
        String string2 = obtainAttributes.getString(qk0.FontFamily_fontProviderPackage);
        String string3 = obtainAttributes.getString(qk0.FontFamily_fontProviderQuery);
        int resourceId = obtainAttributes.getResourceId(qk0.FontFamily_fontProviderCerts, 0);
        int integer = obtainAttributes.getInteger(qk0.FontFamily_fontProviderFetchStrategy, 1);
        int integer2 = obtainAttributes.getInteger(qk0.FontFamily_fontProviderFetchTimeout, 500);
        obtainAttributes.recycle();
        if (string == null || string2 == null || string3 == null) {
            ArrayList arrayList = new ArrayList();
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (xmlPullParser.getName().equals("font")) {
                        arrayList.add(f(xmlPullParser, resources));
                    } else {
                        g(xmlPullParser);
                    }
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            return new b((c[]) arrayList.toArray(new c[arrayList.size()]));
        }
        while (xmlPullParser.next() != 3) {
            g(xmlPullParser);
        }
        return new d(new ym0(string, string2, string3, c(resources, resourceId)), integer, integer2);
    }

    @DexIgnore
    public static c f(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        boolean z = true;
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), qk0.FontFamilyFont);
        int i = obtainAttributes.getInt(obtainAttributes.hasValue(qk0.FontFamilyFont_fontWeight) ? qk0.FontFamilyFont_fontWeight : qk0.FontFamilyFont_android_fontWeight, MFNetworkReturnCode.BAD_REQUEST);
        if (1 != obtainAttributes.getInt(obtainAttributes.hasValue(qk0.FontFamilyFont_fontStyle) ? qk0.FontFamilyFont_fontStyle : qk0.FontFamilyFont_android_fontStyle, 0)) {
            z = false;
        }
        int i2 = obtainAttributes.hasValue(qk0.FontFamilyFont_ttcIndex) ? qk0.FontFamilyFont_ttcIndex : qk0.FontFamilyFont_android_ttcIndex;
        String string = obtainAttributes.getString(obtainAttributes.hasValue(qk0.FontFamilyFont_fontVariationSettings) ? qk0.FontFamilyFont_fontVariationSettings : qk0.FontFamilyFont_android_fontVariationSettings);
        int i3 = obtainAttributes.getInt(i2, 0);
        int i4 = obtainAttributes.hasValue(qk0.FontFamilyFont_font) ? qk0.FontFamilyFont_font : qk0.FontFamilyFont_android_font;
        int resourceId = obtainAttributes.getResourceId(i4, 0);
        String string2 = obtainAttributes.getString(i4);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            g(xmlPullParser);
        }
        return new c(string2, i, z, string, i3, resourceId);
    }

    @DexIgnore
    public static void g(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            }
        }
    }

    @DexIgnore
    public static List<byte[]> h(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            arrayList.add(Base64.decode(str, 0));
        }
        return arrayList;
    }
}
