package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i57 {
    @DexIgnore
    public static Bitmap a(Context context, Bitmap bitmap, m57 m57) {
        int i = m57.f2304a;
        int i2 = m57.d;
        int i3 = i / i2;
        int i4 = m57.b / i2;
        if (l57.a(i3, i4)) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i3, i4, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        int i5 = m57.d;
        canvas.scale(1.0f / ((float) i5), 1.0f / ((float) i5));
        Paint paint = new Paint();
        paint.setFlags(3);
        paint.setColorFilter(new PorterDuffColorFilter(m57.e, PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, paint);
        try {
            b(context, createBitmap, m57.c);
        } catch (Exception e) {
            createBitmap = c(createBitmap, m57.c, true);
        }
        if (m57.d == 1) {
            return createBitmap;
        }
        if (createBitmap == null) {
            return null;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(createBitmap, m57.f2304a, m57.b, true);
        createBitmap.recycle();
        return createScaledBitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003a  */
    @android.annotation.TargetApi(18)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap b(android.content.Context r4, android.graphics.Bitmap r5, int r6) throws android.renderscript.RSRuntimeException {
        /*
            android.renderscript.RenderScript r1 = android.renderscript.RenderScript.create(r4)     // Catch:{ all -> 0x0036 }
            android.renderscript.RenderScript$RSMessageHandler r0 = new android.renderscript.RenderScript$RSMessageHandler     // Catch:{ all -> 0x003e }
            r0.<init>()     // Catch:{ all -> 0x003e }
            r1.setMessageHandler(r0)     // Catch:{ all -> 0x003e }
            android.renderscript.Allocation$MipmapControl r0 = android.renderscript.Allocation.MipmapControl.MIPMAP_NONE     // Catch:{ all -> 0x003e }
            r2 = 1
            android.renderscript.Allocation r0 = android.renderscript.Allocation.createFromBitmap(r1, r5, r0, r2)     // Catch:{ all -> 0x003e }
            android.renderscript.Type r2 = r0.getType()     // Catch:{ all -> 0x003e }
            android.renderscript.Allocation r2 = android.renderscript.Allocation.createTyped(r1, r2)     // Catch:{ all -> 0x003e }
            android.renderscript.Element r3 = android.renderscript.Element.U8_4(r1)     // Catch:{ all -> 0x003e }
            android.renderscript.ScriptIntrinsicBlur r3 = android.renderscript.ScriptIntrinsicBlur.create(r1, r3)     // Catch:{ all -> 0x003e }
            r3.setInput(r0)     // Catch:{ all -> 0x003e }
            float r0 = (float) r6     // Catch:{ all -> 0x003e }
            r3.setRadius(r0)     // Catch:{ all -> 0x003e }
            r3.forEach(r2)     // Catch:{ all -> 0x003e }
            r2.copyTo(r5)     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0035
            r1.destroy()
        L_0x0035:
            return r5
        L_0x0036:
            r0 = move-exception
            r1 = 0
        L_0x0038:
            if (r1 == 0) goto L_0x003d
            r1.destroy()
        L_0x003d:
            throw r0
        L_0x003e:
            r0 = move-exception
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i57.b(android.content.Context, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }

    @DexIgnore
    public static Bitmap c(Bitmap bitmap, int i, boolean z) {
        Bitmap copy = z ? bitmap : bitmap.copy(bitmap.getConfig(), true);
        if (i < 1) {
            return null;
        }
        int width = copy.getWidth();
        int height = copy.getHeight();
        int i2 = width * height;
        int[] iArr = new int[i2];
        copy.getPixels(iArr, 0, width, 0, 0, width, height);
        int i3 = width - 1;
        int i4 = height - 1;
        int i5 = i + i + 1;
        int[] iArr2 = new int[i2];
        int[] iArr3 = new int[i2];
        int[] iArr4 = new int[i2];
        int[] iArr5 = new int[Math.max(width, height)];
        int i6 = (i5 + 1) >> 1;
        int i7 = i6 * i6;
        int i8 = i7 * 256;
        int[] iArr6 = new int[i8];
        for (int i9 = 0; i9 < i8; i9++) {
            iArr6[i9] = i9 / i7;
        }
        int[][] iArr7 = (int[][]) Array.newInstance(Integer.TYPE, i5, 3);
        int i10 = i + 1;
        int i11 = 0;
        int i12 = 0;
        for (int i13 = 0; i13 < height; i13++) {
            int i14 = 0;
            int i15 = 0;
            int i16 = 0;
            int i17 = 0;
            int i18 = 0;
            int i19 = 0;
            int i20 = 0;
            int i21 = 0;
            int i22 = 0;
            for (int i23 = -i; i23 <= i; i23++) {
                int i24 = iArr[Math.min(i3, Math.max(i23, 0)) + i11];
                int[] iArr8 = iArr7[i23 + i];
                iArr8[0] = (16711680 & i24) >> 16;
                iArr8[1] = (65280 & i24) >> 8;
                iArr8[2] = i24 & 255;
                int abs = i10 - Math.abs(i23);
                i22 += iArr8[0] * abs;
                i14 += iArr8[1] * abs;
                i15 += abs * iArr8[2];
                if (i23 > 0) {
                    i19 += iArr8[0];
                    i20 += iArr8[1];
                    i21 += iArr8[2];
                } else {
                    i16 += iArr8[0];
                    i17 += iArr8[1];
                    i18 += iArr8[2];
                }
            }
            int i25 = 0;
            int i26 = i;
            int i27 = i20;
            int i28 = i17;
            int i29 = i16;
            int i30 = i18;
            while (i25 < width) {
                iArr2[i11] = iArr6[i22];
                iArr3[i11] = iArr6[i14];
                iArr4[i11] = iArr6[i15];
                int[] iArr9 = iArr7[((i26 - i) + i5) % i5];
                int i31 = iArr9[0];
                int i32 = iArr9[1];
                int i33 = iArr9[2];
                if (i13 == 0) {
                    iArr5[i25] = Math.min(i25 + i + 1, i3);
                }
                int i34 = iArr[iArr5[i25] + i12];
                iArr9[0] = (16711680 & i34) >> 16;
                iArr9[1] = (65280 & i34) >> 8;
                iArr9[2] = i34 & 255;
                int i35 = i19 + iArr9[0];
                int i36 = i27 + iArr9[1];
                int i37 = i21 + iArr9[2];
                int i38 = (i22 - i29) + i35;
                i14 = (i14 - i28) + i36;
                int i39 = (i15 - i30) + i37;
                int i40 = (i26 + 1) % i5;
                int[] iArr10 = iArr7[i40 % i5];
                i29 = (i29 - i31) + iArr10[0];
                i28 = (i28 - i32) + iArr10[1];
                i30 = (i30 - i33) + iArr10[2];
                i19 = i35 - iArr10[0];
                i27 = i36 - iArr10[1];
                i21 = i37 - iArr10[2];
                i11++;
                i22 = i38;
                i26 = i40;
                i25++;
                i15 = i39;
            }
            i12 += width;
        }
        for (int i41 = 0; i41 < width; i41++) {
            int i42 = -i;
            int i43 = 0;
            int i44 = 0;
            int i45 = 0;
            int i46 = 0;
            int i47 = 0;
            int i48 = 0;
            int i49 = 0;
            int i50 = i42 * width;
            int i51 = 0;
            int i52 = 0;
            while (i42 <= i) {
                int max = Math.max(0, i50) + i41;
                int[] iArr11 = iArr7[i42 + i];
                iArr11[0] = iArr2[max];
                iArr11[1] = iArr3[max];
                iArr11[2] = iArr4[max];
                int abs2 = i10 - Math.abs(i42);
                i51 += iArr2[max] * abs2;
                i52 += iArr3[max] * abs2;
                i43 += iArr4[max] * abs2;
                if (i42 > 0) {
                    i47 += iArr11[0];
                    i48 += iArr11[1];
                    i49 += iArr11[2];
                } else {
                    i44 += iArr11[0];
                    i45 += iArr11[1];
                    i46 += iArr11[2];
                }
                if (i42 < i4) {
                    i50 += width;
                }
                i42++;
            }
            int i53 = 0;
            int i54 = i47;
            int i55 = i41;
            int i56 = i;
            while (i53 < height) {
                iArr[i55] = (iArr[i55] & -16777216) | (iArr6[i51] << 16) | (iArr6[i52] << 8) | iArr6[i43];
                int[] iArr12 = iArr7[((i56 - i) + i5) % i5];
                int i57 = iArr12[0];
                int i58 = iArr12[1];
                int i59 = iArr12[2];
                if (i41 == 0) {
                    iArr5[i53] = Math.min(i53 + i10, i4) * width;
                }
                int i60 = iArr5[i53] + i41;
                iArr12[0] = iArr2[i60];
                iArr12[1] = iArr3[i60];
                iArr12[2] = iArr4[i60];
                int i61 = i54 + iArr12[0];
                int i62 = i48 + iArr12[1];
                int i63 = iArr12[2] + i49;
                i51 = (i51 - i44) + i61;
                i52 = (i52 - i45) + i62;
                i43 = (i43 - i46) + i63;
                i56 = (i56 + 1) % i5;
                int[] iArr13 = iArr7[i56];
                i44 = (i44 - i57) + iArr13[0];
                i45 = (i45 - i58) + iArr13[1];
                i46 = (i46 - i59) + iArr13[2];
                i54 = i61 - iArr13[0];
                i48 = i62 - iArr13[1];
                i49 = i63 - iArr13[2];
                i53++;
                i55 += width;
            }
        }
        copy.setPixels(iArr, 0, width, 0, 0, width, height);
        return copy;
    }
}
