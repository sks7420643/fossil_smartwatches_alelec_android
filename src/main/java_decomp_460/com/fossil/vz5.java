package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz5 implements Factory<sn6> {
    @DexIgnore
    public static sn6 a(oz5 oz5) {
        sn6 g = oz5.g();
        lk7.c(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }
}
