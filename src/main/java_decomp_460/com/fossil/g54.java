package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g54<F, T> implements Iterator<T> {
    @DexIgnore
    public /* final */ Iterator<? extends F> b;

    @DexIgnore
    public g54(Iterator<? extends F> it) {
        i14.l(it);
        this.b = it;
    }

    @DexIgnore
    public abstract T a(F f);

    @DexIgnore
    public final boolean hasNext() {
        return this.b.hasNext();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.g54<F, T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Iterator
    public final T next() {
        return (T) a(this.b.next());
    }

    @DexIgnore
    public final void remove() {
        this.b.remove();
    }
}
