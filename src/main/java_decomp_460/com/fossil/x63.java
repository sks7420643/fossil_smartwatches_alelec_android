package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x63 implements xw2<a73> {
    @DexIgnore
    public static x63 c; // = new x63();
    @DexIgnore
    public /* final */ xw2<a73> b;

    @DexIgnore
    public x63() {
        this(ww2.b(new z63()));
    }

    @DexIgnore
    public x63(xw2<a73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((a73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((a73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ a73 zza() {
        return this.b.zza();
    }
}
