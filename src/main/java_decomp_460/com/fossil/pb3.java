package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static yb3 f2811a;

    @DexIgnore
    public static ob3 a(CameraPosition cameraPosition) {
        try {
            return new ob3(l().J1(cameraPosition));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 b(LatLng latLng) {
        try {
            return new ob3(l().g0(latLng));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 c(LatLngBounds latLngBounds, int i) {
        try {
            return new ob3(l().u(latLngBounds, i));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 d(LatLng latLng, float f) {
        try {
            return new ob3(l().B2(latLng, f));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 e(float f, float f2) {
        try {
            return new ob3(l().C2(f, f2));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 f(float f) {
        try {
            return new ob3(l().x(f));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 g(float f, Point point) {
        try {
            return new ob3(l().Z0(f, point.x, point.y));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 h() {
        try {
            return new ob3(l().z0());
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 i() {
        try {
            return new ob3(l().c2());
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static ob3 j(float f) {
        try {
            return new ob3(l().v2(f));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static void k(yb3 yb3) {
        rc2.k(yb3);
        f2811a = yb3;
    }

    @DexIgnore
    public static yb3 l() {
        yb3 yb3 = f2811a;
        rc2.l(yb3, "CameraUpdateFactory is not initialized");
        return yb3;
    }
}
