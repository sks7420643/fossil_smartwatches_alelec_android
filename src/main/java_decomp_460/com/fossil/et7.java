package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Charset f986a;

    /*
    static {
        Charset forName = Charset.forName("UTF-8");
        pq7.b(forName, "Charset.forName(\"UTF-8\")");
        f986a = forName;
        pq7.b(Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        pq7.b(Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        pq7.b(Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        pq7.b(Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        pq7.b(Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }
    */
}
