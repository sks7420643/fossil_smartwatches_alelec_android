package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.DeclarationFile;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<pq5> f2556a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public List<pq5> a() {
        return this.f2556a;
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public void c(gj4 gj4) {
        this.f2556a = new ArrayList();
        if (gj4.s(CloudLogWriter.ITEMS_PARAM)) {
            try {
                bj4 q = gj4.q(CloudLogWriter.ITEMS_PARAM);
                if (q.size() > 0) {
                    for (int i = 0; i < q.size(); i++) {
                        gj4 d = q.m(i).d();
                        pq5 pq5 = new pq5();
                        if (d.s("appId")) {
                            pq5.f(d.p("appId").f());
                        }
                        if (d.s("name")) {
                            pq5.l(d.p("name").f());
                        }
                        if (d.s("description")) {
                            pq5.i(d.p("description").f());
                        }
                        if (d.s(MicroAppVariant.COLUMN_MAJOR_NUMBER)) {
                            pq5.j(d.p(MicroAppVariant.COLUMN_MAJOR_NUMBER).b());
                        }
                        if (d.s(MicroAppVariant.COLUMN_MINOR_NUMBER)) {
                            pq5.k(d.p(MicroAppVariant.COLUMN_MINOR_NUMBER).b());
                        }
                        if (d.s("updatedAt")) {
                            pq5.n(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.p("updatedAt").f()).getMillis());
                        }
                        if (d.s("createdAt")) {
                            pq5.g(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.p("createdAt").f()).getMillis());
                        }
                        if (d.s(MicroAppVariant.COLUMN_DECLARATION_FILES)) {
                            bj4 q2 = d.q(MicroAppVariant.COLUMN_DECLARATION_FILES);
                            ArrayList arrayList = new ArrayList();
                            if (q2.size() > 0) {
                                for (int i2 = 0; i2 < q2.size(); i2++) {
                                    gj4 d2 = q2.m(i2).d();
                                    DeclarationFile declarationFile = new DeclarationFile();
                                    if (d2.s(DeclarationFile.COLUMN_FILE_ID)) {
                                        declarationFile.setFileId(d2.p(DeclarationFile.COLUMN_FILE_ID).f());
                                    }
                                    if (d2.s("description")) {
                                        declarationFile.setDescription(d2.p("description").f());
                                    }
                                    if (d2.s("content")) {
                                        declarationFile.setContent(d2.p("content").f());
                                    }
                                    arrayList.add(declarationFile);
                                }
                            }
                            pq5.h(arrayList);
                        }
                        this.f2556a.add(pq5);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (gj4.s("_range")) {
            try {
                this.b = (Range) new Gson().k(gj4.r("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
