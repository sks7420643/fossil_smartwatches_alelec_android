package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx4 implements Factory<lx4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f2436a;
    @DexIgnore
    public /* final */ Provider<tt4> b;
    @DexIgnore
    public /* final */ Provider<zt4> c;

    @DexIgnore
    public mx4(Provider<on5> provider, Provider<tt4> provider2, Provider<zt4> provider3) {
        this.f2436a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static mx4 a(Provider<on5> provider, Provider<tt4> provider2, Provider<zt4> provider3) {
        return new mx4(provider, provider2, provider3);
    }

    @DexIgnore
    public static lx4 c(on5 on5, tt4 tt4, zt4 zt4) {
        return new lx4(on5, tt4, zt4);
    }

    @DexIgnore
    /* renamed from: b */
    public lx4 get() {
        return c(this.f2436a.get(), this.b.get(), this.c.get());
    }
}
