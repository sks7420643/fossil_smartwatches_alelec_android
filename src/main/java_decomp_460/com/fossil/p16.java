package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p16 implements Factory<o16> {
    @DexIgnore
    public static o16 a(j16 j16, uq4 uq4, d26 d26, g26 g26, LoaderManager loaderManager) {
        return new o16(j16, uq4, d26, g26, loaderManager);
    }
}
