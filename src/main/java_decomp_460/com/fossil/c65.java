package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.WindowInsetsFrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c65 extends b65 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362158, 1);
        A.put(2131362066, 2);
        A.put(2131363410, 3);
        A.put(2131363291, 4);
        A.put(2131363377, 5);
        A.put(2131363042, 6);
        A.put(2131362027, 7);
    }
    */

    @DexIgnore
    public c65(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public c65(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (CropImageView) objArr[7], (ConstraintLayout) objArr[0], (ConstraintLayout) objArr[2], (WindowInsetsFrameLayout) objArr[1], (RecyclerView) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3]);
        this.y = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
