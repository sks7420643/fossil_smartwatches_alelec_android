package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj6 implements Factory<bj6> {
    @DexIgnore
    public static bj6 a(zi6 zi6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new bj6(zi6, userRepository, heartRateSummaryRepository);
    }
}
