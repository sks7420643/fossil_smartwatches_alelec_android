package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os6 implements Factory<ns6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f2718a;

    @DexIgnore
    public os6(Provider<ThemeRepository> provider) {
        this.f2718a = provider;
    }

    @DexIgnore
    public static os6 a(Provider<ThemeRepository> provider) {
        return new os6(provider);
    }

    @DexIgnore
    public static ns6 c(ThemeRepository themeRepository) {
        return new ns6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ns6 get() {
        return c(this.f2718a.get());
    }
}
