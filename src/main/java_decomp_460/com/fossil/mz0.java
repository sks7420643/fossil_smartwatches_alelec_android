package com.fossil;

import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mz0 extends lz0 {
    @DexIgnore
    @Override // com.fossil.iz0, com.fossil.nz0
    public float c(View view) {
        return view.getTransitionAlpha();
    }

    @DexIgnore
    @Override // com.fossil.jz0, com.fossil.nz0
    public void e(View view, Matrix matrix) {
        view.setAnimationMatrix(matrix);
    }

    @DexIgnore
    @Override // com.fossil.kz0, com.fossil.nz0
    public void f(View view, int i, int i2, int i3, int i4) {
        view.setLeftTopRightBottom(i, i2, i3, i4);
    }

    @DexIgnore
    @Override // com.fossil.iz0, com.fossil.nz0
    public void g(View view, float f) {
        view.setTransitionAlpha(f);
    }

    @DexIgnore
    @Override // com.fossil.lz0, com.fossil.nz0
    public void h(View view, int i) {
        view.setTransitionVisibility(i);
    }

    @DexIgnore
    @Override // com.fossil.jz0, com.fossil.nz0
    public void i(View view, Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }

    @DexIgnore
    @Override // com.fossil.jz0, com.fossil.nz0
    public void j(View view, Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }
}
