package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NonNullDecl;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw2 {
    @DexIgnore
    public static int a(int i, int i2) {
        String b;
        if (i >= 0 && i < i2) {
            return i;
        }
        if (i < 0) {
            b = uw2.b("%s (%s) must not be negative", "index", Integer.valueOf(i));
        } else if (i2 < 0) {
            StringBuilder sb = new StringBuilder(26);
            sb.append("negative size: ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        } else {
            b = uw2.b("%s (%s) must be less than size (%s)", "index", Integer.valueOf(i), Integer.valueOf(i2));
        }
        throw new IndexOutOfBoundsException(b);
    }

    @DexIgnore
    @NonNullDecl
    public static <T> T b(@NonNullDecl T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    @NonNullDecl
    public static <T> T c(@NonNullDecl T t, @NullableDecl Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    @DexIgnore
    public static String d(int i, int i2, @NullableDecl String str) {
        if (i < 0) {
            return uw2.b("%s (%s) must not be negative", str, Integer.valueOf(i));
        } else if (i2 >= 0) {
            return uw2.b("%s (%s) must not be greater than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            StringBuilder sb = new StringBuilder(26);
            sb.append("negative size: ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    @DexIgnore
    public static void e(int i, int i2, int i3) {
        String d;
        if (i < 0 || i2 < i || i2 > i3) {
            if (i < 0 || i > i3) {
                d = d(i, i3, "start index");
            } else if (i2 < 0 || i2 > i3) {
                d = d(i2, i3, "end index");
            } else {
                d = uw2.b("end index (%s) must not be less than start index (%s)", Integer.valueOf(i2), Integer.valueOf(i));
            }
            throw new IndexOutOfBoundsException(d);
        }
    }

    @DexIgnore
    public static void f(boolean z, @NullableDecl Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    @DexIgnore
    public static int g(int i, int i2) {
        if (i >= 0 && i <= i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(d(i, i2, "index"));
    }

    @DexIgnore
    public static void h(boolean z, @NullableDecl Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }
}
