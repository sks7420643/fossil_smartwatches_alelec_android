package com.fossil;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zz0 extends Drawable implements bm0 {
    @DexIgnore
    public Drawable b;

    @DexIgnore
    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.a(drawable, theme);
        }
    }

    @DexIgnore
    public void clearColorFilter() {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    @DexIgnore
    public Drawable getCurrent() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getCurrent() : super.getCurrent();
    }

    @DexIgnore
    public int getMinimumHeight() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getMinimumHeight() : super.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getMinimumWidth() : super.getMinimumWidth();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getPadding(rect) : super.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getState() : super.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        Drawable drawable = this.b;
        return drawable != null ? drawable.getTransparentRegion() : super.getTransparentRegion();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.i(drawable);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.setLevel(i) : super.onLevelChange(i);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    @DexIgnore
    public void setColorFilter(int i, PorterDuff.Mode mode) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setFilterBitmap(z);
        }
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.k(drawable, f, f2);
        }
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        Drawable drawable = this.b;
        if (drawable != null) {
            am0.l(drawable, i, i2, i3, i4);
        }
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        Drawable drawable = this.b;
        return drawable != null ? drawable.setState(iArr) : super.setState(iArr);
    }
}
