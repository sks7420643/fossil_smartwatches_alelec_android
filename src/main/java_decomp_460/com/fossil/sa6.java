package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sa6 extends gq4<ra6> {
    @DexIgnore
    Object E4();  // void declaration

    @DexIgnore
    Object G1();  // void declaration

    @DexIgnore
    void H(PlacesClient placesClient);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void h2(List<WeatherLocationWrapper> list);

    @DexIgnore
    void x0(boolean z);
}
