package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.view.FlexibleEditText;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class il5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1642a; // = il5.class.toString();

    @DexIgnore
    public static boolean a(Context context) {
        return context.getResources().getConfiguration().getLayoutDirection() == 1;
    }

    @DexIgnore
    @SuppressLint({"RtlHardcoded"})
    public static void b(View view, Context context) {
        try {
            if (a(context) && (view instanceof FlexibleEditText)) {
                ((FlexibleEditText) view).setGravity(8388629);
                view.setTextDirection(4);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(f1642a, e.toString());
        }
    }
}
