package com.fossil;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface uq2 extends IInterface {
    @DexIgnore
    void O2(ia3 ia3, wq2 wq2, String str) throws RemoteException;

    @DexIgnore
    void X1(boolean z) throws RemoteException;

    @DexIgnore
    void c1(vr2 vr2) throws RemoteException;

    @DexIgnore
    void g2(kr2 kr2) throws RemoteException;

    @DexIgnore
    Location zza(String str) throws RemoteException;
}
