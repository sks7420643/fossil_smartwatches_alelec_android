package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ej7 extends AsyncTask<Uri, Void, List<BelvedereResult>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ BelvedereCallback<List<BelvedereResult>> f949a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ dj7 c;
    @DexIgnore
    public /* final */ gj7 d;

    @DexIgnore
    public ej7(Context context, dj7 dj7, gj7 gj7, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        this.b = context;
        this.c = dj7;
        this.d = gj7;
        this.f949a = belvedereCallback;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0096 A[SYNTHETIC, Splitter:B:29:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009b A[SYNTHETIC, Splitter:B:32:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e6 A[SYNTHETIC, Splitter:B:51:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00eb A[SYNTHETIC, Splitter:B:54:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0109 A[SYNTHETIC, Splitter:B:62:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x004e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x004e A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.zendesk.belvedere.BelvedereResult> doInBackground(android.net.Uri... r14) {
        /*
        // Method dump skipped, instructions count: 338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ej7.doInBackground(android.net.Uri[]):java.util.List");
    }

    @DexIgnore
    /* renamed from: b */
    public void onPostExecute(List<BelvedereResult> list) {
        super.onPostExecute(list);
        BelvedereCallback<List<BelvedereResult>> belvedereCallback = this.f949a;
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(list);
        }
    }
}
