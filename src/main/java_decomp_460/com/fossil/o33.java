package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o33 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> d;
    @DexIgnore
    public /* final */ /* synthetic */ g33 e;

    @DexIgnore
    public o33(g33 g33) {
        this.e = g33;
        this.b = -1;
    }

    @DexIgnore
    public /* synthetic */ o33(g33 g33, j33 j33) {
        this(g33);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.d == null) {
            this.d = this.e.d.entrySet().iterator();
        }
        return this.d;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b + 1 < this.e.c.size() || (!this.e.d.isEmpty() && a().hasNext());
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        this.c = true;
        int i = this.b + 1;
        this.b = i;
        return i < this.e.c.size() ? (Map.Entry) this.e.c.get(this.b) : (Map.Entry) a().next();
    }

    @DexIgnore
    public final void remove() {
        if (this.c) {
            this.c = false;
            this.e.q();
            if (this.b < this.e.c.size()) {
                g33 g33 = this.e;
                int i = this.b;
                this.b = i - 1;
                Object unused = g33.l(i);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }
}
