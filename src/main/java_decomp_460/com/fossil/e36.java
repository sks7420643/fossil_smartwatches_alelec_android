package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e36 implements Factory<d36> {
    @DexIgnore
    public static d36 a(y26 y26, on5 on5, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new d36(y26, on5, remindersSettingsDatabase);
    }
}
