package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface h81<T extends View> extends g81 {

    @DexIgnore
    /* renamed from: a */
    public static final a f1446a = a.f1447a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ a f1447a; // = new a();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h81$a$a")
        /* renamed from: com.fossil.h81$a$a */
        public static final class C0106a implements h81<T> {
            @DexIgnore
            public /* final */ T b;
            @DexIgnore
            public /* final */ boolean c;
            @DexIgnore
            public /* final */ /* synthetic */ View d;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: android.view.View */
            /* JADX WARN: Multi-variable type inference failed */
            public C0106a(View view, boolean z) {
                this.d = view;
                this.b = view;
                this.c = z;
            }

            @DexIgnore
            @Override // com.fossil.h81
            public boolean a() {
                return this.c;
            }

            @DexIgnore
            @Override // com.fossil.g81
            public Object b(qn7<? super f81> qn7) {
                return b.h(this, qn7);
            }

            @DexIgnore
            /* JADX WARN: Type inference failed for: r0v0, types: [T, T extends android.view.View] */
            @Override // com.fossil.h81
            public T getView() {
                return this.b;
            }
        }

        @DexIgnore
        public static /* synthetic */ h81 b(a aVar, View view, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = true;
            }
            return aVar.a(view, z);
        }

        @DexIgnore
        public final <T extends View> h81<T> a(T t, boolean z) {
            pq7.c(t, "view");
            return new C0106a(t, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver c;
            @DexIgnore
            public /* final */ /* synthetic */ ku7 d;
            @DexIgnore
            public /* final */ /* synthetic */ h81 e;

            @DexIgnore
            public a(ViewTreeObserver viewTreeObserver, ku7 ku7, h81 h81) {
                this.c = viewTreeObserver;
                this.d = ku7;
                this.e = h81;
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (!this.b) {
                    this.b = true;
                    h81 h81 = this.e;
                    ViewTreeObserver viewTreeObserver = this.c;
                    pq7.b(viewTreeObserver, "viewTreeObserver");
                    b.g(h81, viewTreeObserver, this);
                    c81 c81 = new c81(bs7.d(b.f(this.e, false), 1), bs7.d(b.e(this.e, false), 1));
                    ku7 ku7 = this.d;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(c81));
                }
                return true;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h81$b$b")
        /* renamed from: com.fossil.h81$b$b */
        public static final class C0107b extends qq7 implements rp7<Throwable, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ a $preDrawListener;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver $viewTreeObserver;
            @DexIgnore
            public /* final */ /* synthetic */ h81 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0107b(ViewTreeObserver viewTreeObserver, a aVar, h81 h81) {
                super(1);
                this.$viewTreeObserver = viewTreeObserver;
                this.$preDrawListener = aVar;
                this.this$0 = h81;
            }

            @DexIgnore
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
                invoke(th);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                h81 h81 = this.this$0;
                ViewTreeObserver viewTreeObserver = this.$viewTreeObserver;
                pq7.b(viewTreeObserver, "viewTreeObserver");
                b.g(h81, viewTreeObserver, this.$preDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> int d(h81<T> h81, int i, int i2, int i3, boolean z) {
            int i4 = i - i3;
            if (i4 > 0) {
                return i4;
            }
            int i5 = i2 - i3;
            if (i5 > 0) {
                return i5;
            }
            if (z || i != -2) {
                return -1;
            }
            if (q81.c.a() && q81.c.b() <= 4) {
                Log.println(4, "ViewSizeResolver", "A View's width and/or height is set to WRAP_CONTENT. Falling back to the size of the display.");
            }
            Context context = h81.getView().getContext();
            pq7.b(context, "view.context");
            Resources resources = context.getResources();
            pq7.b(resources, "view.context.resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            return Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }

        @DexIgnore
        public static <T extends View> int e(h81<T> h81, boolean z) {
            ViewGroup.LayoutParams layoutParams = h81.getView().getLayoutParams();
            return d(h81, layoutParams != null ? layoutParams.height : -1, h81.getView().getHeight(), h81.a() ? h81.getView().getPaddingTop() + h81.getView().getPaddingBottom() : 0, z);
        }

        @DexIgnore
        public static <T extends View> int f(h81<T> h81, boolean z) {
            ViewGroup.LayoutParams layoutParams = h81.getView().getLayoutParams();
            return d(h81, layoutParams != null ? layoutParams.width : -1, h81.getView().getWidth(), h81.a() ? h81.getView().getPaddingLeft() + h81.getView().getPaddingRight() : 0, z);
        }

        @DexIgnore
        public static <T extends View> void g(h81<T> h81, ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnPreDrawListener onPreDrawListener) {
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            } else {
                h81.getView().getViewTreeObserver().removeOnPreDrawListener(onPreDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> Object h(h81<T> h81, qn7<? super f81> qn7) {
            boolean isLayoutRequested = h81.getView().isLayoutRequested();
            int f = f(h81, isLayoutRequested);
            int e = e(h81, isLayoutRequested);
            if (f > 0 && e > 0) {
                return new c81(f, e);
            }
            lu7 lu7 = new lu7(xn7.c(qn7), 1);
            ViewTreeObserver viewTreeObserver = h81.getView().getViewTreeObserver();
            a aVar = new a(viewTreeObserver, lu7, h81);
            viewTreeObserver.addOnPreDrawListener(aVar);
            lu7.e(new C0107b(viewTreeObserver, aVar, h81));
            Object t = lu7.t();
            if (t != yn7.d()) {
                return t;
            }
            go7.c(qn7);
            return t;
        }
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    T getView();
}
