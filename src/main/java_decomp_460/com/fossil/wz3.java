package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz3 implements yz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f4023a;

    @DexIgnore
    public wz3(float f) {
        this.f4023a = f;
    }

    @DexIgnore
    @Override // com.fossil.yz3
    public float a(RectF rectF) {
        return this.f4023a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof wz3)) {
            return false;
        }
        return this.f4023a == ((wz3) obj).f4023a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.f4023a)});
    }
}
