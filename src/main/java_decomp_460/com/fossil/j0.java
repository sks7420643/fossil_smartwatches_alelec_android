package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j0 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ h0 CREATOR; // = new h0(null);
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte[] f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ long i;
    @DexIgnore
    public /* final */ boolean j;

    @DexIgnore
    public j0(String str, byte b2, byte b3, byte[] bArr, long j2, long j3, long j4, boolean z) {
        this.c = str;
        this.d = (byte) b2;
        this.e = (byte) b3;
        this.f = bArr;
        this.g = j2;
        this.h = j3;
        this.i = j4;
        this.j = z;
    }

    @DexIgnore
    public j0(String str, short s, long j2, long j3) {
        this(str, CREATOR.b(s), CREATOR.a(s), new byte[0], j2, j3, 0, false);
    }

    @DexIgnore
    public static /* synthetic */ j0 a(j0 j0Var, String str, byte b2, byte b3, byte[] bArr, long j2, long j3, long j4, boolean z, int i2) {
        return j0Var.a((i2 & 1) != 0 ? j0Var.c : str, (i2 & 2) != 0 ? j0Var.d : b2, (i2 & 4) != 0 ? j0Var.e : b3, (i2 & 8) != 0 ? j0Var.f : bArr, (i2 & 16) != 0 ? j0Var.g : j2, (i2 & 32) != 0 ? j0Var.h : j3, (i2 & 64) != 0 ? j0Var.i : j4, (i2 & 128) != 0 ? j0Var.j : z);
    }

    @DexIgnore
    public final long a() {
        return this.i;
    }

    @DexIgnore
    public final j0 a(String str, byte b2, byte b3, byte[] bArr, long j2, long j3, long j4, boolean z) {
        return new j0(str, b2, b3, bArr, j2, j3, j4, z);
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : ?: CAST (int) (wrap: short : 0x00c4: INVOKE  (r6v2 short) = 
      (wrap: byte : 0x00c2: AGET  (r6v1 byte A[IMMUTABLE_TYPE]) = (r0v24 byte[] A[IMMUTABLE_TYPE]), (1 Object[boolean, int, float, short, byte, char]))
     type: STATIC call: com.fossil.hy1.p(byte):short)), ('.' char), (wrap: int : ?: CAST (int) (wrap: short : 0x00d2: INVOKE  (r0v33 short) = 
      (wrap: byte : 0x00d0: AGET  (r0v32 byte A[IMMUTABLE_TYPE]) = (r0v24 byte[] A[IMMUTABLE_TYPE]), (2 Object[int, float, short, byte, char]))
     type: STATIC call: com.fossil.hy1.p(byte):short))] */
    @DexIgnore
    public final JSONObject a(boolean z) {
        JSONObject k = g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.k0, this.c), jd0.A0, hy1.l(b(), null, 1, null)), jd0.I, Long.valueOf(this.g)), jd0.J, Long.valueOf(this.h)), jd0.T0, Integer.valueOf(this.f.length)), jd0.C1, Double.valueOf(hy1.f(this.i))), jd0.N2, Boolean.valueOf(this.j)), jd0.g4, kb.e.a(b()));
        if (z) {
            g80.k(k, jd0.S0, dy1.e(this.f, null, 1, null));
        }
        ob a2 = ob.A.a(this.d);
        if (a2 != null) {
            int i2 = i0.f1564a[a2.ordinal()];
            if (i2 == 1) {
                g80.k(k, jd0.h4, -1);
                byte[] bArr = this.f;
                if (bArr.length >= 18) {
                    ByteBuffer order = ByteBuffer.wrap(dm7.k(bArr, 16, 18)).order(ByteOrder.LITTLE_ENDIAN);
                    pq7.b(order, "byteBuffer");
                    g80.k(k, jd0.h4, Integer.valueOf(hy1.n(order.getShort())));
                }
            } else if (i2 == 2) {
                g80.k(k, jd0.j2, "unknown");
                g80.k(k, jd0.h4, -1);
                int i3 = 0;
                while (true) {
                    int i4 = i3 + 16;
                    byte[] bArr2 = this.f;
                    if (i4 > bArr2.length) {
                        break;
                    }
                    byte[] k2 = dm7.k(bArr2, i3, i4);
                    byte b2 = k2[0];
                    if (b2 != -59) {
                        if (b2 == 0) {
                            jd0 jd0 = jd0.j2;
                            StringBuilder sb = new StringBuilder();
                            sb.append((int) hy1.p(k2[1]));
                            sb.append('.');
                            sb.append((int) hy1.p(k2[2]));
                            g80.k(k, jd0, sb.toString());
                        }
                    } else if (k2[1] == 9) {
                        ByteBuffer order2 = ByteBuffer.wrap(dm7.p(dm7.k(k2, 2, 5), (byte) 0)).order(ByteOrder.LITTLE_ENDIAN);
                        jd0 jd02 = jd0.h4;
                        pq7.b(order2, "byteBuffer");
                        g80.k(k, jd02, Integer.valueOf(order2.getInt()));
                    }
                    i3 = i4;
                }
            }
        }
        return k;
    }

    @DexIgnore
    public final short b() {
        return ByteBuffer.allocate(2).put(this.d).put(this.e).getShort(0);
    }

    @DexIgnore
    public final boolean c() {
        return this.j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(j0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            j0 j0Var = (j0) obj;
            if (!pq7.a(this.c, j0Var.c)) {
                return false;
            }
            if (this.d != j0Var.d) {
                return false;
            }
            if (this.e != j0Var.e) {
                return false;
            }
            if (!Arrays.equals(this.f, j0Var.f)) {
                return false;
            }
            if (this.g != j0Var.g) {
                return false;
            }
            if (this.h != j0Var.h) {
                return false;
            }
            if (this.i != j0Var.i) {
                return false;
            }
            return this.j == j0Var.j;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.database.entity.DeviceFile");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        byte b2 = this.d;
        byte b3 = this.e;
        int hashCode2 = Arrays.hashCode(this.f);
        int hashCode3 = Long.valueOf(this.g).hashCode();
        int hashCode4 = Long.valueOf(this.h).hashCode();
        return (((((((((((((hashCode * 31) + b2) * 31) + b3) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + Long.valueOf(this.i).hashCode()) * 31) + Boolean.valueOf(this.j).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a(true);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e2 = e.e("DeviceFile(deviceMacAddress=");
        e2.append(this.c);
        e2.append(", fileType=");
        e2.append((int) this.d);
        e2.append(", fileIndex=");
        e2.append((int) this.e);
        e2.append(", rawData=");
        e2.append(Arrays.toString(this.f));
        e2.append(", fileLength=");
        e2.append(this.g);
        e2.append(", fileCrc=");
        e2.append(this.h);
        e2.append(", createdTimeStamp=");
        e2.append(this.i);
        e2.append(", isCompleted=");
        e2.append(this.j);
        e2.append(")");
        return e2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeLong(this.i);
        }
        if (parcel != null) {
            parcel.writeInt(this.j ? 1 : 0);
        }
    }
}
