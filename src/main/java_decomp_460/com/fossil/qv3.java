package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qv3 implements Parcelable.Creator<pv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pv3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        String str = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                str2 = ad2.f(parcel, t);
            } else if (l == 3) {
                str = ad2.f(parcel, t);
            } else if (l == 4) {
                i = ad2.v(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                z = ad2.m(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new pv3(str2, str, i, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pv3[] newArray(int i) {
        return new pv3[i];
    }
}
