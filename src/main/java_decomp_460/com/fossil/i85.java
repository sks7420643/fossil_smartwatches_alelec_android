package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i85 extends h85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d M; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray N;
    @DexIgnore
    public long L;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        N = sparseIntArray;
        sparseIntArray.put(2131363376, 1);
        N.put(2131363291, 2);
        N.put(2131362528, 3);
        N.put(2131362054, 4);
        N.put(2131362790, 5);
        N.put(2131362791, 6);
        N.put(2131362789, 7);
        N.put(2131362770, 8);
        N.put(2131362028, 9);
        N.put(2131362573, 10);
        N.put(2131362084, 11);
        N.put(2131363531, 12);
        N.put(2131363423, 13);
        N.put(2131363530, 14);
        N.put(2131363422, 15);
        N.put(2131363529, 16);
        N.put(2131363421, 17);
        N.put(2131363447, 18);
        N.put(2131362176, 19);
        N.put(2131363049, 20);
    }
    */

    @DexIgnore
    public i85(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 21, M, N));
    }

    @DexIgnore
    public i85(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (View) objArr[9], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[11], (CardView) objArr[19], (ImageButton) objArr[3], (View) objArr[10], (ImageView) objArr[8], (View) objArr[7], (View) objArr[5], (View) objArr[6], (ConstraintLayout) objArr[0], (ViewPager2) objArr[20], (RTLImageView) objArr[2], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[13], (View) objArr[18], (CustomizeWidget) objArr[16], (CustomizeWidget) objArr[14], (CustomizeWidget) objArr[12]);
        this.L = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.L = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.L != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.L = 1;
        }
        w();
    }
}
