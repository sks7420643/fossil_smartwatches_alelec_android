package com.fossil;

import com.fossil.a34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t24<K, V> extends a34<K, V> implements y14<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends a34.b<K, V> {
        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b c(Object obj, Object obj2) {
            h(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b d(Map.Entry entry) {
            i(entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b e(Iterable iterable) {
            j(iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b f(Map map) {
            k(map);
            return this;
        }

        @DexIgnore
        /* renamed from: g */
        public t24<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return t24.of();
            }
            if (i == 1) {
                return t24.of((Object) this.b[0].getKey(), (Object) this.b[0].getValue());
            }
            if (this.f188a != null) {
                if (this.d) {
                    this.b = (b34[]) h44.a(this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, i44.from(this.f188a).onResultOf(x34.s()));
            }
            this.d = this.c == this.b.length;
            return n44.fromEntryArray(this.c, this.b);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> h(K k, V v) {
            super.c(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> i(Map.Entry<? extends K, ? extends V> entry) {
            super.d(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> j(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.e(iterable);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> k(Map<? extends K, ? extends V> map) {
            super.f(map);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a34.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public b(t24<?, ?> t24) {
            super(t24);
        }

        @DexIgnore
        @Override // com.fossil.a34.e
        public Object readResolve() {
            return createMap(new a());
        }
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    public static <K, V> t24<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) o34.i(iterable, a34.EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return n44.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static <K, V> t24<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof t24) {
            t24<K, V> t24 = (t24) map;
            if (!t24.isPartialView()) {
                return t24;
            }
        }
        return copyOf((Iterable) map.entrySet());
    }

    @DexIgnore
    public static <K, V> t24<K, V> of() {
        return n44.EMPTY;
    }

    @DexIgnore
    public static <K, V> t24<K, V> of(K k, V v) {
        return new y44(k, v);
    }

    @DexIgnore
    public static <K, V> t24<K, V> of(K k, V v, K k2, V v2) {
        return n44.fromEntries(a34.entryOf(k, v), a34.entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> t24<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return n44.fromEntries(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> t24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return n44.fromEntries(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3), a34.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> t24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return n44.fromEntries(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3), a34.entryOf(k4, v4), a34.entryOf(k5, v5));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public V forcePut(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract t24<V, K> inverse();

    @DexIgnore
    @Override // com.fossil.a34, com.fossil.a34, java.util.Map
    public h34<V> values() {
        return inverse().keySet();
    }

    @DexIgnore
    @Override // com.fossil.a34
    public Object writeReplace() {
        return new b(this);
    }
}
