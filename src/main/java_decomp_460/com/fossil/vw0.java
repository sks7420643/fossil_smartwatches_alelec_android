package com.fossil;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vw0 implements mx0 {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ mx0 f;
    @DexIgnore
    public hw0 g;
    @DexIgnore
    public boolean h;

    @DexIgnore
    public vw0(Context context, String str, File file, int i, mx0 mx0) {
        this.b = context;
        this.c = str;
        this.d = file;
        this.e = i;
        this.f = mx0;
    }

    @DexIgnore
    public final void a(File file) throws IOException {
        ReadableByteChannel channel;
        if (this.c != null) {
            channel = Channels.newChannel(this.b.getAssets().open(this.c));
        } else if (this.d != null) {
            channel = new FileInputStream(this.d).getChannel();
        } else {
            throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.b.getCacheDir());
        createTempFile.deleteOnExit();
        fx0.a(channel, new FileOutputStream(createTempFile).getChannel());
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Failed to create directories for " + file.getAbsolutePath());
        } else if (!createTempFile.renameTo(file)) {
            throw new IOException("Failed to move intermediate file (" + createTempFile.getAbsolutePath() + ") to destination (" + file.getAbsolutePath() + ").");
        }
    }

    @DexIgnore
    public void b(hw0 hw0) {
        this.g = hw0;
    }

    @DexIgnore
    public final void c() {
        String databaseName = getDatabaseName();
        File databasePath = this.b.getDatabasePath(databaseName);
        hw0 hw0 = this.g;
        cx0 cx0 = new cx0(databaseName, this.b.getFilesDir(), hw0 == null || hw0.j);
        try {
            cx0.b();
            if (!databasePath.exists()) {
                try {
                    a(databasePath);
                } catch (IOException e2) {
                    throw new RuntimeException("Unable to copy database file.", e2);
                }
            } else if (this.g == null) {
                cx0.c();
            } else {
                try {
                    int c2 = ex0.c(databasePath);
                    if (c2 == this.e) {
                        cx0.c();
                    } else if (this.g.a(c2, this.e)) {
                        cx0.c();
                    } else {
                        if (this.b.deleteDatabase(databaseName)) {
                            try {
                                a(databasePath);
                            } catch (IOException e3) {
                                Log.w("ROOM", "Unable to copy database file.", e3);
                            }
                        } else {
                            Log.w("ROOM", "Failed to delete database file (" + databaseName + ") for a copy destructive migration.");
                        }
                        cx0.c();
                    }
                } catch (IOException e4) {
                    Log.w("ROOM", "Unable to read database version.", e4);
                    cx0.c();
                }
            }
        } finally {
            cx0.c();
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.mx0, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            this.f.close();
            this.h = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public String getDatabaseName() {
        return this.f.getDatabaseName();
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public lx0 getWritableDatabase() {
        lx0 writableDatabase;
        synchronized (this) {
            if (!this.h) {
                c();
                this.h = true;
            }
            writableDatabase = this.f.getWritableDatabase();
        }
        return writableDatabase;
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public void setWriteAheadLoggingEnabled(boolean z) {
        this.f.setWriteAheadLoggingEnabled(z);
    }
}
