package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum oi5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    SPINNING("Spinning"),
    OUTDOOR_CYCLING("outdoor_cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALK("walk"),
    ROW_MACHINE("row_machine"),
    HIKING("hiking");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final cl7<Integer, Integer> a(oi5 oi5) {
            if (oi5 == null) {
                oi5 = oi5.UNKNOWN;
            }
            if (oi5 != null) {
                switch (ni5.e[oi5.ordinal()]) {
                    case 1:
                        return new cl7<>(2131231216, 2131886616);
                    case 2:
                        return new cl7<>(2131231217, 2131886617);
                    case 3:
                        return new cl7<>(2131231211, 2131886611);
                    case 4:
                        return new cl7<>(2131231218, 2131886618);
                    case 5:
                        return new cl7<>(2131231212, 2131886612);
                    case 6:
                        return new cl7<>(2131231220, 2131886620);
                    case 7:
                        return new cl7<>(2131231221, 2131886621);
                    case 8:
                        return new cl7<>(2131231219, 2131886619);
                    case 9:
                        return new cl7<>(2131231215, 2131886615);
                    case 10:
                        return new cl7<>(2131231214, 2131886613);
                    case 11:
                        return new cl7<>(2131231213, 2131886621);
                }
            }
            throw new al7();
        }

        @DexIgnore
        public final gi5 b(oi5 oi5) {
            pq7.c(oi5, "workoutWrapperType");
            switch (ni5.d[oi5.ordinal()]) {
                case 1:
                    return gi5.INDOOR;
                case 2:
                    return gi5.INDOOR;
                case 3:
                    return gi5.OUTDOOR;
                case 4:
                    return gi5.INDOOR;
                case 5:
                    return gi5.INDOOR;
                case 6:
                    return gi5.INDOOR;
                case 7:
                    return gi5.INDOOR;
                case 8:
                    return gi5.INDOOR;
                default:
                    return null;
            }
        }

        @DexIgnore
        public final mi5 c(oi5 oi5) {
            pq7.c(oi5, "workoutWrapperType");
            switch (ni5.c[oi5.ordinal()]) {
                case 1:
                    return mi5.UNKNOWN;
                case 2:
                    return mi5.RUNNING;
                case 3:
                    return mi5.SPINNING;
                case 4:
                    return mi5.CYCLING;
                case 5:
                    return mi5.TREADMILL;
                case 6:
                    return mi5.ELLIPTICAL;
                case 7:
                    return mi5.WEIGHTS;
                case 8:
                    return mi5.WORKOUT;
                case 9:
                    return mi5.WALKING;
                case 10:
                    return mi5.ROWING;
                case 11:
                    return mi5.HIKING;
                default:
                    throw new al7();
            }
        }

        @DexIgnore
        public final oi5 d(mi5 mi5, gi5 gi5) {
            if (mi5 != null) {
                switch (ni5.b[mi5.ordinal()]) {
                    case 1:
                        return oi5.UNKNOWN;
                    case 2:
                        return oi5.RUNNING;
                    case 3:
                        if (gi5 != null) {
                            int i = ni5.f2528a[gi5.ordinal()];
                            if (i == 1) {
                                return oi5.SPINNING;
                            }
                            if (i == 2) {
                                return oi5.OUTDOOR_CYCLING;
                            }
                        }
                        return oi5.OUTDOOR_CYCLING;
                    case 4:
                        return oi5.SPINNING;
                    case 5:
                        return oi5.TREADMILL;
                    case 6:
                        return oi5.ELLIPTICAL;
                    case 7:
                        return oi5.WEIGHTS;
                    case 8:
                        return oi5.WORKOUT;
                    case 9:
                        return oi5.UNKNOWN;
                    case 10:
                        return oi5.WALK;
                    case 11:
                        return oi5.ROW_MACHINE;
                    case 12:
                        return oi5.UNKNOWN;
                    case 13:
                        return oi5.UNKNOWN;
                    case 14:
                        return oi5.HIKING;
                }
            }
            return oi5.UNKNOWN;
        }
    }

    @DexIgnore
    public oi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
