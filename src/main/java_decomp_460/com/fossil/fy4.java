package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy4 implements Factory<ey4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f1237a;
    @DexIgnore
    public /* final */ Provider<zt4> b;

    @DexIgnore
    public fy4(Provider<on5> provider, Provider<zt4> provider2) {
        this.f1237a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static fy4 a(Provider<on5> provider, Provider<zt4> provider2) {
        return new fy4(provider, provider2);
    }

    @DexIgnore
    public static ey4 c(on5 on5, zt4 zt4) {
        return new ey4(on5, zt4);
    }

    @DexIgnore
    /* renamed from: b */
    public ey4 get() {
        return c(this.f1237a.get(), this.b.get());
    }
}
