package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl5 {
    @DexIgnore
    public static /* final */ String c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ DeviceRepository f3126a;
    @DexIgnore
    public /* final */ PortfolioApp b;

    /*
    static {
        String simpleName = rl5.class.getSimpleName();
        pq7.b(simpleName, "WatchParamHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public rl5(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(portfolioApp, "mApp");
        this.f3126a = deviceRepository;
        this.b = portfolioApp;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r3v0 int), ('.' char), (r4v0 int)] */
    public final float a(int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append('.');
        sb.append(i2);
        return Float.parseFloat(sb.toString());
    }

    @DexIgnore
    public final Object b(String str, float f, qn7<? super tl7> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local.d(str2, "handleFailureResponseWatchParam, currentWPVersion=" + f);
        WatchParam watchParamBySerialId = this.f3126a.getWatchParamBySerialId(str);
        if (watchParamBySerialId != null) {
            String versionMajor = watchParamBySerialId.getVersionMajor();
            if (versionMajor != null) {
                int parseInt = Integer.parseInt(versionMajor);
                String versionMinor = watchParamBySerialId.getVersionMinor();
                if (versionMinor == null) {
                    pq7.i();
                    throw null;
                } else if (f < a(parseInt, Integer.parseInt(versionMinor))) {
                    FLogger.INSTANCE.getLocal().d(c, "Newer version is available in database, set to device");
                    String data = watchParamBySerialId.getData();
                    PortfolioApp portfolioApp = this.b;
                    if (data != null) {
                        portfolioApp.H0(str, true, new WatchParamsFileMapping(data));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "The saved version in database is older than the current one, skip it");
                    this.b.H0(str, true, null);
                }
            } else {
                pq7.i();
                throw null;
            }
        } else if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database, but keep going because the current version is not empty");
            this.b.H0(str, true, null);
        } else {
            FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database and the current version is empty -> notify failed");
            this.b.H0(str, false, null);
        }
        return tl7.f3441a;
    }

    @DexIgnore
    public final Object c(String str, float f, WatchParameterResponse watchParameterResponse, qn7<? super tl7> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local.d(str2, "handleSuccessResponseWatchParam, response=" + watchParameterResponse + ", currentWPVersion=" + f);
        WatchParam watchParamModel = watchParameterResponse.toWatchParamModel(str);
        this.f3126a.saveWatchParamModel(watchParamModel);
        String versionMajor = watchParamModel.getVersionMajor();
        if (versionMajor != null) {
            int parseInt = Integer.parseInt(versionMajor);
            String versionMinor = watchParamModel.getVersionMinor();
            if (versionMinor != null) {
                if (f < a(parseInt, Integer.parseInt(versionMinor))) {
                    FLogger.INSTANCE.getLocal().d(c, "Need to update newer WP version from response");
                    String data = watchParamModel.getData();
                    PortfolioApp portfolioApp = this.b;
                    if (data != null) {
                        portfolioApp.H0(str, true, new WatchParamsFileMapping(data));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "No need to update WP version in device, it's the latest one");
                    this.b.H0(str, true, null);
                }
                return tl7.f3441a;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }
}
