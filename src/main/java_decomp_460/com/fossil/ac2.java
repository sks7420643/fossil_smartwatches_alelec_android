package com.fossil;

import android.accounts.Account;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ac2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Account f243a;
    @DexIgnore
    public /* final */ Set<Scope> b;
    @DexIgnore
    public /* final */ Set<Scope> c;
    @DexIgnore
    public /* final */ Map<m62<?>, b> d;
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ gs3 h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public Integer j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Account f244a;
        @DexIgnore
        public aj0<Scope> b;
        @DexIgnore
        public Map<m62<?>, b> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public gs3 h; // = gs3.k;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public final a a(Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new aj0<>();
            }
            this.b.addAll(collection);
            return this;
        }

        @DexIgnore
        public final ac2 b() {
            return new ac2(this.f244a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }

        @DexIgnore
        public final a c(Account account) {
            this.f244a = account;
            return this;
        }

        @DexIgnore
        public final a d(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public final a e(String str) {
            this.f = str;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Set<Scope> f245a;

        @DexIgnore
        public b(Set<Scope> set) {
            rc2.k(set);
            this.f245a = Collections.unmodifiableSet(set);
        }
    }

    @DexIgnore
    public ac2(Account account, Set<Scope> set, Map<m62<?>, b> map, int i2, View view, String str, String str2, gs3 gs3, boolean z) {
        this.f243a = account;
        this.b = set == null ? Collections.emptySet() : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.emptyMap() : map;
        this.e = view;
        this.f = str;
        this.g = str2;
        this.h = gs3;
        this.i = z;
        HashSet hashSet = new HashSet(this.b);
        for (b bVar : this.d.values()) {
            hashSet.addAll(bVar.f245a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public final Account a() {
        return this.f243a;
    }

    @DexIgnore
    @Deprecated
    public final String b() {
        Account account = this.f243a;
        if (account != null) {
            return account.name;
        }
        return null;
    }

    @DexIgnore
    public final Account c() {
        Account account = this.f243a;
        return account != null ? account : new Account("<<default account>>", "com.google");
    }

    @DexIgnore
    public final Set<Scope> d() {
        return this.c;
    }

    @DexIgnore
    public final Set<Scope> e(m62<?> m62) {
        b bVar = this.d.get(m62);
        if (bVar == null || bVar.f245a.isEmpty()) {
            return this.b;
        }
        HashSet hashSet = new HashSet(this.b);
        hashSet.addAll(bVar.f245a);
        return hashSet;
    }

    @DexIgnore
    public final Integer f() {
        return this.j;
    }

    @DexIgnore
    public final Map<m62<?>, b> g() {
        return this.d;
    }

    @DexIgnore
    public final String h() {
        return this.g;
    }

    @DexIgnore
    public final String i() {
        return this.f;
    }

    @DexIgnore
    public final Set<Scope> j() {
        return this.b;
    }

    @DexIgnore
    public final gs3 k() {
        return this.h;
    }

    @DexIgnore
    public final boolean l() {
        return this.i;
    }

    @DexIgnore
    public final void m(Integer num) {
        this.j = num;
    }
}
