package com.fossil;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.StateSet;
import androidx.collection.SparseArrayCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.if0;
import com.fossil.kf0;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class hf0 extends kf0 implements bm0 {
    @DexIgnore
    public c u;
    @DexIgnore
    public g v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Animatable f1468a;

        @DexIgnore
        public b(Animatable animatable) {
            super();
            this.f1468a = animatable;
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void c() {
            this.f1468a.start();
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void d() {
            this.f1468a.stop();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends kf0.a {
        @DexIgnore
        public dj0<Long> K;
        @DexIgnore
        public SparseArrayCompat<Integer> L;

        @DexIgnore
        public c(c cVar, hf0 hf0, Resources resources) {
            super(cVar, hf0, resources);
            if (cVar != null) {
                this.K = cVar.K;
                this.L = cVar.L;
                return;
            }
            this.K = new dj0<>();
            this.L = new SparseArrayCompat<>();
        }

        @DexIgnore
        public static long D(int i, int i2) {
            return (((long) i) << 32) | ((long) i2);
        }

        @DexIgnore
        public int B(int[] iArr, Drawable drawable, int i) {
            int z = super.z(iArr, drawable);
            this.L.q(z, Integer.valueOf(i));
            return z;
        }

        @DexIgnore
        public int C(int i, int i2, Drawable drawable, boolean z) {
            int a2 = super.a(drawable);
            long D = D(i, i2);
            long j = z ? 8589934592L : 0;
            long j2 = (long) a2;
            this.K.d(D, Long.valueOf(j2 | j));
            if (z) {
                this.K.d(D(i2, i), Long.valueOf(j | j2 | 4294967296L));
            }
            return a2;
        }

        @DexIgnore
        public int E(int i) {
            if (i < 0) {
                return 0;
            }
            return this.L.l(i, 0).intValue();
        }

        @DexIgnore
        public int F(int[] iArr) {
            int A = super.A(iArr);
            return A >= 0 ? A : super.A(StateSet.WILD_CARD);
        }

        @DexIgnore
        public int G(int i, int i2) {
            return (int) this.K.n(D(i, i2), -1L).longValue();
        }

        @DexIgnore
        public boolean H(int i, int i2) {
            return (this.K.n(D(i, i2), -1L).longValue() & 4294967296L) != 0;
        }

        @DexIgnore
        public boolean I(int i, int i2) {
            return (this.K.n(D(i, i2), -1L).longValue() & 8589934592L) != 0;
        }

        @DexIgnore
        @Override // com.fossil.kf0.a
        public Drawable newDrawable() {
            return new hf0(this, null);
        }

        @DexIgnore
        @Override // com.fossil.kf0.a
        public Drawable newDrawable(Resources resources) {
            return new hf0(this, resources);
        }

        @DexIgnore
        @Override // com.fossil.if0.c, com.fossil.kf0.a
        public void r() {
            this.K = this.K.clone();
            this.L = this.L.clone();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ uz0 f1469a;

        @DexIgnore
        public d(uz0 uz0) {
            super();
            this.f1469a = uz0;
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void c() {
            this.f1469a.start();
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void d() {
            this.f1469a.stop();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ObjectAnimator f1470a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            f fVar = new f(animationDrawable, z);
            ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", i, i2);
            if (Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration((long) fVar.a());
            ofInt.setInterpolator(fVar);
            this.b = z2;
            this.f1470a = ofInt;
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public boolean a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void b() {
            this.f1470a.reverse();
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void c() {
            this.f1470a.start();
        }

        @DexIgnore
        @Override // com.fossil.hf0.g
        public void d() {
            this.f1470a.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements TimeInterpolator {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int[] f1471a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public f(AnimationDrawable animationDrawable, boolean z) {
            b(animationDrawable, z);
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public int b(AnimationDrawable animationDrawable, boolean z) {
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.b = numberOfFrames;
            int[] iArr = this.f1471a;
            if (iArr == null || iArr.length < numberOfFrames) {
                this.f1471a = new int[numberOfFrames];
            }
            int[] iArr2 = this.f1471a;
            int i = 0;
            int i2 = 0;
            while (i2 < numberOfFrames) {
                int duration = animationDrawable.getDuration(z ? (numberOfFrames - i2) - 1 : i2);
                iArr2[i2] = duration;
                i2++;
                i = duration + i;
            }
            this.c = i;
            return i;
        }

        @DexIgnore
        public float getInterpolation(float f) {
            int i = (int) ((((float) this.c) * f) + 0.5f);
            int i2 = this.b;
            int[] iArr = this.f1471a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            return (i3 < i2 ? ((float) i) / ((float) this.c) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + (((float) i3) / ((float) i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public abstract void d();
    }

    @DexIgnore
    public hf0() {
        this(null, null);
    }

    @DexIgnore
    public hf0(c cVar, Resources resources) {
        super(null);
        this.w = -1;
        this.x = -1;
        h(new c(cVar, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }

    @DexIgnore
    public static hf0 m(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("animated-selector")) {
            hf0 hf0 = new hf0();
            hf0.n(context, resources, xmlPullParser, attributeSet, theme);
            return hf0;
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
    }

    @DexIgnore
    @Override // com.fossil.if0, com.fossil.kf0
    public void h(if0.c cVar) {
        super.h(cVar);
        if (cVar instanceof c) {
            this.u = (c) cVar;
        }
    }

    @DexIgnore
    @Override // com.fossil.kf0
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.if0
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        g gVar = this.v;
        if (gVar != null) {
            gVar.d();
            this.v = null;
            g(this.w);
            this.w = -1;
            this.x = -1;
        }
    }

    @DexIgnore
    /* renamed from: l */
    public c j() {
        return new c(this.u, this, null);
    }

    @DexIgnore
    @Override // com.fossil.if0, com.fossil.kf0
    public Drawable mutate() {
        if (!this.y) {
            super.mutate();
            this.u.r();
            this.y = true;
        }
        return this;
    }

    @DexIgnore
    public void n(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        TypedArray k = ol0.k(resources, theme, attributeSet, mf0.AnimatedStateListDrawableCompat);
        setVisible(k.getBoolean(mf0.AnimatedStateListDrawableCompat_android_visible, true), true);
        t(k);
        i(resources);
        k.recycle();
        o(context, resources, xmlPullParser, attributeSet, theme);
        p();
    }

    @DexIgnore
    public final void o(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                int depth2 = xmlPullParser.getDepth();
                if (depth2 < depth && next == 3) {
                    return;
                }
                if (next == 2 && depth2 <= depth) {
                    if (xmlPullParser.getName().equals("item")) {
                        q(context, resources, xmlPullParser, attributeSet, theme);
                    } else if (xmlPullParser.getName().equals("transition")) {
                        r(context, resources, xmlPullParser, attributeSet, theme);
                    }
                }
            } else {
                return;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.if0, com.fossil.kf0
    public boolean onStateChange(int[] iArr) {
        int F = this.u.F(iArr);
        boolean z = F != c() && (s(F) || g(F));
        Drawable current = getCurrent();
        return current != null ? z | current.setState(iArr) : z;
    }

    @DexIgnore
    public final void p() {
        onStateChange(getState());
    }

    @DexIgnore
    public final int q(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = ol0.k(resources, theme, attributeSet, mf0.AnimatedStateListDrawableItem);
        int resourceId = k.getResourceId(mf0.AnimatedStateListDrawableItem_android_id, 0);
        int resourceId2 = k.getResourceId(mf0.AnimatedStateListDrawableItem_android_drawable, -1);
        Drawable j = resourceId2 > 0 ? jh0.h().j(context, resourceId2) : null;
        k.recycle();
        int[] k2 = k(attributeSet);
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                j = xmlPullParser.getName().equals("vector") ? a01.c(resources, xmlPullParser, attributeSet, theme) : Build.VERSION.SDK_INT >= 21 ? Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme) : Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j != null) {
            return this.u.B(k2, j, resourceId);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    @DexIgnore
    public final int r(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = ol0.k(resources, theme, attributeSet, mf0.AnimatedStateListDrawableTransition);
        int resourceId = k.getResourceId(mf0.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = k.getResourceId(mf0.AnimatedStateListDrawableTransition_android_toId, -1);
        int resourceId3 = k.getResourceId(mf0.AnimatedStateListDrawableTransition_android_drawable, -1);
        Drawable j = resourceId3 > 0 ? jh0.h().j(context, resourceId3) : null;
        boolean z = k.getBoolean(mf0.AnimatedStateListDrawableTransition_android_reversible, false);
        k.recycle();
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                j = xmlPullParser.getName().equals("animated-vector") ? uz0.a(context, resources, xmlPullParser, attributeSet, theme) : Build.VERSION.SDK_INT >= 21 ? Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme) : Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j == null) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.u.C(resourceId, resourceId2, j, z);
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    @DexIgnore
    public final boolean s(int i) {
        int c2;
        g bVar;
        g gVar = this.v;
        if (gVar == null) {
            c2 = c();
        } else if (i == this.w) {
            return true;
        } else {
            if (i != this.x || !gVar.a()) {
                int i2 = this.w;
                gVar.d();
                c2 = i2;
            } else {
                gVar.b();
                this.w = this.x;
                this.x = i;
                return true;
            }
        }
        this.v = null;
        this.x = -1;
        this.w = -1;
        c cVar = this.u;
        int E = cVar.E(c2);
        int E2 = cVar.E(i);
        if (!(E2 == 0 || E == 0)) {
            int G = cVar.G(E, E2);
            if (G < 0) {
                return false;
            }
            boolean I = cVar.I(E, E2);
            g(G);
            Drawable current = getCurrent();
            if (current instanceof AnimationDrawable) {
                bVar = new e((AnimationDrawable) current, cVar.H(E, E2), I);
            } else if (current instanceof uz0) {
                bVar = new d((uz0) current);
            } else if (current instanceof Animatable) {
                bVar = new b((Animatable) current);
            }
            bVar.c();
            this.v = bVar;
            this.x = c2;
            this.w = i;
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.if0
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (this.v != null && (visible || z2)) {
            if (z) {
                this.v.c();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    @DexIgnore
    public final void t(TypedArray typedArray) {
        c cVar = this.u;
        if (Build.VERSION.SDK_INT >= 21) {
            cVar.d |= typedArray.getChangingConfigurations();
        }
        cVar.x(typedArray.getBoolean(mf0.AnimatedStateListDrawableCompat_android_variablePadding, cVar.i));
        cVar.t(typedArray.getBoolean(mf0.AnimatedStateListDrawableCompat_android_constantSize, cVar.l));
        cVar.u(typedArray.getInt(mf0.AnimatedStateListDrawableCompat_android_enterFadeDuration, cVar.A));
        cVar.v(typedArray.getInt(mf0.AnimatedStateListDrawableCompat_android_exitFadeDuration, cVar.B));
        setDither(typedArray.getBoolean(mf0.AnimatedStateListDrawableCompat_android_dither, cVar.x));
    }
}
