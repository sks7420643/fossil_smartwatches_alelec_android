package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ cc3 f3559a;

    @DexIgnore
    public ub3(cc3 cc3) {
        this.f3559a = cc3;
    }

    @DexIgnore
    public final LatLng a(Point point) {
        try {
            return this.f3559a.m2(tg2.n(point));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final we3 b() {
        try {
            return this.f3559a.H0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final Point c(LatLng latLng) {
        try {
            return (Point) tg2.i(this.f3559a.k0(latLng));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
