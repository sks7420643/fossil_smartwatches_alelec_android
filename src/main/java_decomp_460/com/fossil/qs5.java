package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qs5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object f3012a;
    @DexIgnore
    public String b;

    @DexIgnore
    public qs5(String str) {
        pq7.c(str, "tagName");
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object b() {
        return this.f3012a;
    }

    @DexIgnore
    public final void c(Object obj) {
        this.f3012a = obj;
    }
}
