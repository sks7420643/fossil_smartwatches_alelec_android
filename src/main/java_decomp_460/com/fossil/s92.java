package com.fossil;

import com.fossil.m62;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s92 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    boolean c();

    @DexIgnore
    void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean g(t72 t72);

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    <A extends m62.b, T extends i72<? extends z62, A>> T j(T t);

    @DexIgnore
    <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t);

    @DexIgnore
    Object l();  // void declaration

    @DexIgnore
    z52 m();
}
