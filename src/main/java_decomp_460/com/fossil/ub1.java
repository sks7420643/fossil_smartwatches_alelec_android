package com.fossil;

import android.content.res.AssetManager;
import android.util.Log;
import com.fossil.wb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ub1<T> implements wb1<T> {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ AssetManager c;
    @DexIgnore
    public T d;

    @DexIgnore
    public ub1(AssetManager assetManager, String str) {
        this.c = assetManager;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void a() {
        T t = this.d;
        if (t != null) {
            try {
                b(t);
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public abstract void b(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.wb1
    public gb1 c() {
        return gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void d(sa1 sa1, wb1.a<? super T> aVar) {
        try {
            T e = e(this.c, this.b);
            this.d = e;
            aVar.e(e);
        } catch (IOException e2) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e2);
            }
            aVar.b(e2);
        }
    }

    @DexIgnore
    public abstract T e(AssetManager assetManager, String str) throws IOException;
}
