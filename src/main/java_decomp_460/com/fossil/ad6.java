package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad6 implements Factory<zc6> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ ad6 f253a; // = new ad6();
    }

    @DexIgnore
    public static ad6 a() {
        return a.f253a;
    }

    @DexIgnore
    public static zc6 c() {
        return new zc6();
    }

    @DexIgnore
    /* renamed from: b */
    public zc6 get() {
        return c();
    }
}
