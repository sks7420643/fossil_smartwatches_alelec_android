package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh extends qq7 implements gp7<tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ bi b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bh(bi biVar) {
        super(0);
        this.b = biVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.gp7
    public tl7 invoke() {
        j0 j0Var = this.b.W();
        if (j0Var != null) {
            kb kbVar = new kb(j0Var.b());
            j0 j0Var2 = this.b.H(kbVar.b, kbVar.c);
            if (j0Var2 == null && (j0Var2 = this.b.W()) == null) {
                pq7.i();
                throw null;
            }
            this.b.O(j0Var2);
            return tl7.f3441a;
        }
        pq7.i();
        throw null;
    }
}
