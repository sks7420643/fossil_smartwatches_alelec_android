package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class ta0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3385a;

    /*
    static {
        int[] iArr = new int[aa0.values().length];
        f3385a = iArr;
        iArr[aa0.ANIMATION.ordinal()] = 1;
        f3385a[aa0.CLOSE.ordinal()] = 2;
        f3385a[aa0.SWITCH_ACTIVITY.ordinal()] = 3;
        f3385a[aa0.START_CRITICAL.ordinal()] = 4;
        f3385a[aa0.END_CRITICAL.ordinal()] = 5;
        f3385a[aa0.REMAP.ordinal()] = 6;
        f3385a[aa0.START_REPEAT.ordinal()] = 7;
        f3385a[aa0.END_REPEAT.ordinal()] = 8;
        f3385a[aa0.DELAY.ordinal()] = 9;
        f3385a[aa0.VIBE.ordinal()] = 10;
        f3385a[aa0.STREAM.ordinal()] = 11;
        f3385a[aa0.HID.ordinal()] = 12;
    }
    */
}
