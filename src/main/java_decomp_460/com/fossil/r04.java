package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.g04;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r04 extends s04 {
    @DexIgnore
    public static /* final */ boolean o; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.e e; // = new b(this.f3188a);
    @DexIgnore
    public /* final */ TextInputLayout.f f; // = new c();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public long i; // = Long.MAX_VALUE;
    @DexIgnore
    public StateListDrawable j;
    @DexIgnore
    public c04 k;
    @DexIgnore
    public AccessibilityManager l;
    @DexIgnore
    public ValueAnimator m;
    @DexIgnore
    public ValueAnimator n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.r04$a$a")
        /* renamed from: com.fossil.r04$a$a  reason: collision with other inner class name */
        public class RunnableC0203a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ AutoCompleteTextView b;

            @DexIgnore
            public RunnableC0203a(AutoCompleteTextView autoCompleteTextView) {
                this.b = autoCompleteTextView;
            }

            @DexIgnore
            public void run() {
                boolean isPopupShowing = this.b.isPopupShowing();
                r04.this.z(isPopupShowing);
                r04.this.g = isPopupShowing;
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            r04 r04 = r04.this;
            AutoCompleteTextView u = r04.u(r04.f3188a.getEditText());
            u.post(new RunnableC0203a(u));
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TextInputLayout.e {
        @DexIgnore
        public b(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        @DexIgnore
        @Override // com.fossil.rn0, com.google.android.material.textfield.TextInputLayout.e
        public void g(View view, yo0 yo0) {
            super.g(view, yo0);
            yo0.c0(Spinner.class.getName());
            if (yo0.M()) {
                yo0.o0(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.rn0
        public void h(View view, AccessibilityEvent accessibilityEvent) {
            super.h(view, accessibilityEvent);
            r04 r04 = r04.this;
            AutoCompleteTextView u = r04.u(r04.f3188a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && r04.this.l.isTouchExplorationEnabled()) {
                r04.this.C(u);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements TextInputLayout.f {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView u = r04.this.u(textInputLayout.getEditText());
            r04.this.A(u);
            r04.this.r(u);
            r04.this.B(u);
            u.setThreshold(0);
            u.removeTextChangedListener(r04.this.d);
            u.addTextChangedListener(r04.this.d);
            textInputLayout.setErrorIconDrawable((Drawable) null);
            textInputLayout.setTextInputAccessibilityDelegate(r04.this.e);
            textInputLayout.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            r04.this.C((AutoCompleteTextView) r04.this.f3188a.getEditText());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ AutoCompleteTextView b;

        @DexIgnore
        public e(AutoCompleteTextView autoCompleteTextView) {
            this.b = autoCompleteTextView;
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                if (r04.this.y()) {
                    r04.this.g = false;
                }
                r04.this.C(this.b);
                view.performClick();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements View.OnFocusChangeListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            r04.this.f3188a.setEndIconActivated(z);
            if (!z) {
                r04.this.z(false);
                r04.this.g = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements AutoCompleteTextView.OnDismissListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onDismiss() {
            r04.this.g = true;
            r04.this.i = System.currentTimeMillis();
            r04.this.z(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends AnimatorListenerAdapter {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            r04 r04 = r04.this;
            r04.c.setChecked(r04.h);
            r04.this.n.start();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            r04.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore
    public r04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public final void A(AutoCompleteTextView autoCompleteTextView) {
        if (o) {
            int boxBackgroundMode = this.f3188a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.k);
            } else if (boxBackgroundMode == 1) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.j);
            }
        }
    }

    @DexIgnore
    public final void B(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnTouchListener(new e(autoCompleteTextView));
        autoCompleteTextView.setOnFocusChangeListener(new f());
        if (o) {
            autoCompleteTextView.setOnDismissListener(new g());
        }
    }

    @DexIgnore
    public final void C(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView != null) {
            if (y()) {
                this.g = false;
            }
            if (!this.g) {
                if (o) {
                    z(!this.h);
                } else {
                    this.h = !this.h;
                    this.c.toggle();
                }
                if (this.h) {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.showDropDown();
                    return;
                }
                autoCompleteTextView.dismissDropDown();
                return;
            }
            this.g = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.s04
    public void a() {
        float dimensionPixelOffset = (float) this.b.getResources().getDimensionPixelOffset(lw3.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = (float) this.b.getResources().getDimensionPixelOffset(lw3.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = this.b.getResources().getDimensionPixelOffset(lw3.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        c04 w = w(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        c04 w2 = w(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.k = w;
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.j = stateListDrawable;
        stateListDrawable.addState(new int[]{16842922}, w);
        this.j.addState(new int[0], w2);
        this.f3188a.setEndIconDrawable(gf0.d(this.b, o ? mw3.mtrl_dropdown_arrow : mw3.mtrl_ic_arrow_drop_down));
        TextInputLayout textInputLayout = this.f3188a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(rw3.exposed_dropdown_menu_content_description));
        this.f3188a.setEndIconOnClickListener(new d());
        this.f3188a.c(this.f);
        x();
        mo0.v0(this.c, 2);
        this.l = (AccessibilityManager) this.b.getSystemService("accessibility");
    }

    @DexIgnore
    @Override // com.fossil.s04
    public boolean b(int i2) {
        return i2 != 0;
    }

    @DexIgnore
    @Override // com.fossil.s04
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final void r(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView.getKeyListener() == null) {
            int boxBackgroundMode = this.f3188a.getBoxBackgroundMode();
            c04 boxBackground = this.f3188a.getBoxBackground();
            int c2 = vx3.c(autoCompleteTextView, jw3.colorControlHighlight);
            int[][] iArr = {new int[]{16842919}, new int[0]};
            if (boxBackgroundMode == 2) {
                t(autoCompleteTextView, c2, iArr, boxBackground);
            } else if (boxBackgroundMode == 1) {
                s(autoCompleteTextView, c2, iArr, boxBackground);
            }
        }
    }

    @DexIgnore
    public final void s(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, c04 c04) {
        int boxBackgroundColor = this.f3188a.getBoxBackgroundColor();
        int[] iArr2 = {vx3.f(i2, boxBackgroundColor, 0.1f), boxBackgroundColor};
        if (o) {
            mo0.o0(autoCompleteTextView, new RippleDrawable(new ColorStateList(iArr, iArr2), c04, c04));
            return;
        }
        c04 c042 = new c04(c04.C());
        c042.V(new ColorStateList(iArr, iArr2));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{c04, c042});
        int E = mo0.E(autoCompleteTextView);
        int paddingTop = autoCompleteTextView.getPaddingTop();
        int D = mo0.D(autoCompleteTextView);
        int paddingBottom = autoCompleteTextView.getPaddingBottom();
        mo0.o0(autoCompleteTextView, layerDrawable);
        mo0.A0(autoCompleteTextView, E, paddingTop, D, paddingBottom);
    }

    @DexIgnore
    public final void t(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, c04 c04) {
        LayerDrawable layerDrawable;
        int c2 = vx3.c(autoCompleteTextView, jw3.colorSurface);
        c04 c042 = new c04(c04.C());
        int f2 = vx3.f(i2, c2, 0.1f);
        c042.V(new ColorStateList(iArr, new int[]{f2, 0}));
        if (o) {
            c042.setTint(c2);
            ColorStateList colorStateList = new ColorStateList(iArr, new int[]{f2, c2});
            c04 c043 = new c04(c04.C());
            c043.setTint(-1);
            layerDrawable = new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, c042, c043), c04});
        } else {
            layerDrawable = new LayerDrawable(new Drawable[]{c042, c04});
        }
        mo0.o0(autoCompleteTextView, layerDrawable);
    }

    @DexIgnore
    public final AutoCompleteTextView u(EditText editText) {
        if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        }
        throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
    }

    @DexIgnore
    public final ValueAnimator v(int i2, float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(uw3.f3651a);
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new i());
        return ofFloat;
    }

    @DexIgnore
    public final c04 w(float f2, float f3, float f4, int i2) {
        g04.b a2 = g04.a();
        a2.A(f2);
        a2.E(f2);
        a2.r(f3);
        a2.v(f3);
        g04 m2 = a2.m();
        c04 l2 = c04.l(this.b, f4);
        l2.setShapeAppearanceModel(m2);
        l2.X(0, i2, 0, i2);
        return l2;
    }

    @DexIgnore
    public final void x() {
        this.n = v(67, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        ValueAnimator v = v(50, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.m = v;
        v.addListener(new h());
    }

    @DexIgnore
    public final boolean y() {
        long currentTimeMillis = System.currentTimeMillis() - this.i;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    @DexIgnore
    public final void z(boolean z) {
        if (this.h != z) {
            this.h = z;
            this.n.cancel();
            this.m.start();
        }
    }
}
