package com.fossil;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fr5 extends kr5 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public LocationSource e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return fr5.f;
        }
    }

    /*
    static {
        String simpleName = fr5.class.getSimpleName();
        pq7.b(simpleName, "LocationSupportedService::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public final LocationSource g() {
        LocationSource locationSource = this.e;
        if (locationSource != null) {
            return locationSource;
        }
        pq7.n("mLocationSource");
        throw null;
    }

    @DexIgnore
    public final void h(LocationSource.ErrorState errorState) {
        String str;
        pq7.c(errorState, "errorState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f;
        local.d(str2, "sendNotificationWeather, errorState=" + errorState);
        Intent intent = new Intent();
        if (errorState == LocationSource.ErrorState.LOCATION_PERMISSION_OFF) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.h0.c().getPackageName(), null));
            str = um5.c(getApplicationContext(), 2131886928);
            pq7.b(str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else if (errorState == LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = um5.c(getApplicationContext(), 2131887295);
            pq7.b(str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (errorState == LocationSource.ErrorState.LOCATION_SERVICE_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = um5.c(getApplicationContext(), 2131886928);
            pq7.b(str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else {
            str = "";
        }
        PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 134217728);
        cl5 cl5 = cl5.c;
        String string = PortfolioApp.h0.c().getString(2131887305);
        pq7.b(string, "PortfolioApp.instance.ge\u2026ring(R.string.brand_name)");
        pq7.b(activity, "pendingIntent");
        cl5.h(this, 8, string, str, activity, null);
    }
}
