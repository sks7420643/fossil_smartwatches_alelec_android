package com.fossil;

import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class a32 implements j32.d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ p32 f187a;

    @DexIgnore
    public a32(p32 p32) {
        this.f187a = p32;
    }

    @DexIgnore
    public static j32.d b(p32 p32) {
        return new a32(p32);
    }

    @DexIgnore
    @Override // com.fossil.j32.d
    public Object a() {
        return this.f187a.getWritableDatabase();
    }
}
