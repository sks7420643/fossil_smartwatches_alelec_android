package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ls5 f4182a;
    @DexIgnore
    public /* final */ vu6 b;

    @DexIgnore
    public xu6(ls5 ls5, vu6 vu6) {
        pq7.c(ls5, "baseActivity");
        pq7.c(vu6, "mView");
        this.f4182a = ls5;
        this.b = vu6;
    }

    @DexIgnore
    public final ls5 a() {
        return this.f4182a;
    }

    @DexIgnore
    public final vu6 b() {
        return this.b;
    }
}
