package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d23 implements e33 {
    @DexIgnore
    public static /* final */ n23 b; // = new b23();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ n23 f722a;

    @DexIgnore
    public d23() {
        this(new f23(b13.a(), a()));
    }

    @DexIgnore
    public d23(n23 n23) {
        h13.f(n23, "messageInfoFactory");
        this.f722a = n23;
    }

    @DexIgnore
    public static n23 a() {
        try {
            return (n23) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            return b;
        }
    }

    @DexIgnore
    public static boolean b(k23 k23) {
        return k23.zza() == e13.f.i;
    }

    @DexIgnore
    @Override // com.fossil.e33
    public final <T> f33<T> zza(Class<T> cls) {
        h33.p(cls);
        k23 zzb = this.f722a.zzb(cls);
        return zzb.zzb() ? e13.class.isAssignableFrom(cls) ? s23.c(h33.B(), u03.a(), zzb.zzc()) : s23.c(h33.f(), u03.b(), zzb.zzc()) : e13.class.isAssignableFrom(cls) ? b(zzb) ? q23.j(cls, zzb, w23.b(), v13.c(), h33.B(), u03.a(), l23.b()) : q23.j(cls, zzb, w23.b(), v13.c(), h33.B(), null, l23.b()) : b(zzb) ? q23.j(cls, zzb, w23.a(), v13.a(), h33.f(), u03.b(), l23.a()) : q23.j(cls, zzb, w23.a(), v13.a(), h33.v(), null, l23.a());
    }
}
