package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yb0 extends ox1 implements Parcelable, Serializable, nx1 {
    @DexIgnore
    public static /* final */ xb0 CREATOR; // = new xb0(null);
    @DexIgnore
    public /* final */ jw1 b;
    @DexIgnore
    public /* final */ ry1 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore
    public yb0(jw1 jw1, ry1 ry1, boolean z, byte[] bArr) {
        this.b = jw1;
        this.c = ry1;
        this.d = z;
        this.e = bArr;
    }

    @DexIgnore
    public yb0(byte[] bArr) {
        boolean z = true;
        if (bArr.length >= 12) {
            jw1 a2 = jw1.d.a(bArr[0]);
            if (a2 != null) {
                this.b = a2;
                this.c = new ry1(bArr[1], bArr[2]);
                this.d = bArr[3] != ((byte) 1) ? false : z;
                this.e = dm7.k(bArr, 4, 12);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        throw new IllegalArgumentException("Invalid data.".toString());
    }

    @DexIgnore
    public byte[] a() {
        byte a2 = this.b.a();
        byte b2 = this.d;
        byte[] bArr = this.e;
        return dm7.q(new byte[]{a2, (byte) this.c.getMajor(), (byte) this.c.getMinor(), b2}, bArr);
    }

    @DexIgnore
    @Override // java.lang.Object
    public nx1 clone() {
        jw1 jw1 = this.b;
        ry1 clone = this.c.clone();
        boolean z = this.d;
        byte[] bArr = this.e;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new yb0(jw1, clone, z, copyOf);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.d(gy1.d(gy1.d(gy1.d(new JSONObject(), jd0.e, ey1.a(this.b)), jd0.K2, this.c.toJSONObject()), jd0.N4, Boolean.valueOf(this.d)), jd0.O4, dy1.e(this.e, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeParcelable(this.c, i);
        parcel.writeByte(this.d ? (byte) 1 : 0);
        parcel.writeByteArray(this.e);
    }
}
