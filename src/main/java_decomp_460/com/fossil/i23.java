package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i23 implements j23 {
    @DexIgnore
    @Override // com.fossil.j23
    public final Object a(Object obj) {
        return g23.zza().zzb();
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final Object b(Object obj) {
        ((g23) obj).zzc();
        return obj;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final int zza(int i, Object obj, Object obj2) {
        g23 g23 = (g23) obj;
        e23 e23 = (e23) obj2;
        if (!g23.isEmpty()) {
            Iterator it = g23.entrySet().iterator();
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                entry.getKey();
                entry.getValue();
                throw new NoSuchMethodError();
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final Object zza(Object obj, Object obj2) {
        g23 g23 = (g23) obj;
        g23 g232 = (g23) obj2;
        if (!g232.isEmpty()) {
            if (!g23.zzd()) {
                g23 = g23.zzb();
            }
            g23.zza(g232);
        }
        return g23;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final Map<?, ?> zza(Object obj) {
        return (g23) obj;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final h23<?, ?> zzb(Object obj) {
        e23 e23 = (e23) obj;
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final Map<?, ?> zzc(Object obj) {
        return (g23) obj;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final boolean zzd(Object obj) {
        return !((g23) obj).zzd();
    }
}
