package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl4 extends zl4 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public xl4(zl4 zl4, int i, int i2) {
        super(zl4);
        this.c = (short) ((short) i);
        this.d = (short) ((short) i2);
    }

    @DexIgnore
    @Override // com.fossil.zl4
    public void c(am4 am4, byte[] bArr) {
        am4.g(this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        short s = this.c;
        short s2 = this.d;
        return SimpleComparison.LESS_THAN_OPERATION + Integer.toBinaryString((s & ((1 << s2) - 1)) | (1 << s2) | (1 << this.d)).substring(1) + '>';
    }
}
