package com.fossil;

import com.fossil.tz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rz1 implements d12 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ tz1 f3186a;

    @DexIgnore
    public rz1(tz1 tz1) {
        this.f3186a = tz1;
    }

    @DexIgnore
    public static d12 a(tz1 tz1) {
        return new rz1(tz1);
    }

    @DexIgnore
    @Override // com.fossil.d12
    public Object apply(Object obj) {
        return this.f3186a.d((tz1.a) obj);
    }
}
