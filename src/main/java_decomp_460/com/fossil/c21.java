package com.fossil;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.q01;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c21 {
    @DexIgnore
    public static /* final */ String b; // = x01.f("SystemJobInfoConverter");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ComponentName f547a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f548a;

        /*
        static {
            int[] iArr = new int[y01.values().length];
            f548a = iArr;
            try {
                iArr[y01.NOT_REQUIRED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f548a[y01.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f548a[y01.UNMETERED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f548a[y01.NOT_ROAMING.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f548a[y01.METERED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
        */
    }

    @DexIgnore
    public c21(Context context) {
        this.f547a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    @DexIgnore
    public static JobInfo.TriggerContentUri b(q01.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    @DexIgnore
    public static int c(y01 y01) {
        int i = a.f548a[y01.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i != 4) {
            if (i == 5 && Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        x01.c().a(b, String.format("API version too low. Cannot convert network type value %s", y01), new Throwable[0]);
        return 1;
    }

    @DexIgnore
    public JobInfo a(o31 o31, int i) {
        p01 p01 = o31.j;
        int c = c(p01.b());
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", o31.f2626a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", o31.d());
        JobInfo.Builder extras = new JobInfo.Builder(i, this.f547a).setRequiredNetworkType(c).setRequiresCharging(p01.g()).setRequiresDeviceIdle(p01.h()).setExtras(persistableBundle);
        if (!p01.h()) {
            extras.setBackoffCriteria(o31.m, o31.l == n01.LINEAR ? 0 : 1);
        }
        long max = Math.max(o31.a() - System.currentTimeMillis(), 0L);
        if (Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (Build.VERSION.SDK_INT >= 24 && p01.e()) {
            for (q01.a aVar : p01.a().b()) {
                extras.addTriggerContentUri(b(aVar));
            }
            extras.setTriggerContentUpdateDelay(p01.c());
            extras.setTriggerContentMaxDelay(p01.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(p01.f());
            extras.setRequiresStorageNotLow(p01.i());
        }
        return extras.build();
    }
}
