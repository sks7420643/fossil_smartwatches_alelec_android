package com.fossil;

import com.fossil.tq4.a;
import com.fossil.tq4.b;
import com.fossil.tq4.c;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tq4<Q extends b, R extends c, E extends a> {
    @DexIgnore
    public static /* final */ String c; // = "tq4";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Q f3451a;
    @DexIgnore
    public d<R, E> b;

    @DexIgnore
    public interface a {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore
    public abstract void a(Q q);

    @DexIgnore
    public d<R, E> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        String simpleName = getClass().getSimpleName();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "Inside .run useCase=" + simpleName);
        Thread.currentThread().setName(simpleName);
        a(this.f3451a);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local2.d(str2, "Inside .done useCase=" + simpleName);
    }

    @DexIgnore
    public void d(Q q) {
        this.f3451a = q;
    }

    @DexIgnore
    public void e(d<R, E> dVar) {
        this.b = dVar;
    }
}
