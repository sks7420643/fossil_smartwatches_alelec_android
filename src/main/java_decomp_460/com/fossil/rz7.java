package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rz7 {
    @DexIgnore
    public abstract cz7<?> a();

    @DexIgnore
    public final boolean b(rz7 rz7) {
        cz7<?> a2;
        cz7<?> a3 = a();
        return (a3 == null || (a2 = rz7.a()) == null || a3.f() >= a2.f()) ? false : true;
    }

    @DexIgnore
    public abstract Object c(Object obj);

    @DexIgnore
    public String toString() {
        return ov7.a(this) + '@' + ov7.b(this);
    }
}
