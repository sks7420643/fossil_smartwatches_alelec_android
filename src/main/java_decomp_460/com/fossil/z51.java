package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.fossil.e61;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z51 implements e61<Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4418a;

    @DexIgnore
    public z51(Context context) {
        pq7.c(context, "context");
        this.f4418a = context;
    }

    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, Bitmap bitmap, f81 f81, x51 x51, qn7<? super d61> qn7) {
        Resources resources = this.f4418a.getResources();
        pq7.b(resources, "context.resources");
        return new c61(new BitmapDrawable(resources, bitmap), false, q51.MEMORY);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Bitmap bitmap) {
        pq7.c(bitmap, "data");
        return e61.a.a(this, bitmap);
    }

    @DexIgnore
    /* renamed from: f */
    public String b(Bitmap bitmap) {
        pq7.c(bitmap, "data");
        return null;
    }
}
