package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au5 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ GoogleApiService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ double f329a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public a(double d, double d2) {
            this.f329a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.f329a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f330a;

        @DexIgnore
        public c(String str) {
            pq7.c(str, "address");
            this.f330a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f330a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress", f = "GetAddress.kt", l = {20}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ au5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(au5 au5, qn7 qn7) {
            super(qn7);
            this.this$0 = au5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1", f = "GetAddress.kt", l = {20}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $requestValues;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ au5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(au5 au5, a aVar, qn7 qn7) {
            super(1, qn7);
            this.this$0 = au5;
            this.$requestValues = aVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$requestValues, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Double d = null;
            Object d2 = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                GoogleApiService googleApiService = this.this$0.d;
                StringBuilder sb = new StringBuilder();
                a aVar = this.$requestValues;
                sb.append(aVar != null ? ao7.c(aVar.a()) : null);
                sb.append(',');
                a aVar2 = this.$requestValues;
                if (aVar2 != null) {
                    d = ao7.c(aVar2.b());
                }
                sb.append(d);
                String sb2 = sb.toString();
                Locale a2 = um5.a();
                pq7.b(a2, "LanguageHelper.getLocale()");
                String language = a2.getLanguage();
                pq7.b(language, "LanguageHelper.getLocale().language");
                this.label = 1;
                Object address = googleApiService.getAddress(sb2, language, this);
                return address == d2 ? d2 : address;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public au5(GoogleApiService googleApiService) {
        pq7.c(googleApiService, "mGoogleService");
        this.d = googleApiService;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: n */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.au5.a r8, com.fossil.qn7<java.lang.Object> r9) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.au5.d
            if (r0 == 0) goto L_0x006b
            r0 = r9
            com.fossil.au5$d r0 = (com.fossil.au5.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006b
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x007a
            if (r0 != r6) goto L_0x0072
            java.lang.Object r0 = r1.L$1
            com.fossil.au5$a r0 = (com.fossil.au5.a) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.au5 r0 = (com.fossil.au5) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x00ab
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            if (r0 == 0) goto L_0x009d
            java.lang.String r1 = "results"
            com.fossil.bj4 r0 = r0.q(r1)
        L_0x0043:
            if (r0 == 0) goto L_0x00a5
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x00a5
            r1 = 0
            com.google.gson.JsonElement r0 = r0.m(r1)
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            if (r0 == 0) goto L_0x00bb
            java.lang.String r1 = "formatted_address"
            com.google.gson.JsonElement r0 = r0.p(r1)
        L_0x005a:
            if (r0 == 0) goto L_0x009f
            java.lang.String r1 = r0.f()
            java.lang.String r0 = "value.asString"
            com.fossil.pq7.b(r1, r0)
            com.fossil.au5$c r0 = new com.fossil.au5$c
            r0.<init>(r1)
        L_0x006a:
            return r0
        L_0x006b:
            com.fossil.au5$d r0 = new com.fossil.au5$d
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x0072:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007a:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "GetAddress"
            java.lang.String r5 = "executeUseCase"
            r0.d(r2, r5)
            com.fossil.au5$e r0 = new com.fossil.au5$e
            r0.<init>(r7, r8, r4)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r6
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x006a
        L_0x009d:
            r0 = r4
            goto L_0x0043
        L_0x009f:
            com.fossil.au5$b r0 = new com.fossil.au5$b
            r0.<init>()
            goto L_0x006a
        L_0x00a5:
            com.fossil.au5$b r0 = new com.fossil.au5$b
            r0.<init>()
            goto L_0x006a
        L_0x00ab:
            boolean r0 = r0 instanceof com.fossil.hq5
            if (r0 == 0) goto L_0x00b5
            com.fossil.au5$b r0 = new com.fossil.au5$b
            r0.<init>()
            goto L_0x006a
        L_0x00b5:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        L_0x00bb:
            r0 = r4
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.au5.k(com.fossil.au5$a, com.fossil.qn7):java.lang.Object");
    }
}
