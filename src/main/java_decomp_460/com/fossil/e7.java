package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e7 {
    @DexIgnore
    public /* synthetic */ e7(kq7 kq7) {
    }

    @DexIgnore
    public final f7 a(int i) {
        return i == 0 ? f7.SUCCESS : i == x6.k.b ? f7.BLUETOOTH_OFF : i == x6.f.b ? f7.START_FAIL : i == x6.g.b ? f7.HID_PROXY_NOT_CONNECTED : i == x6.h.b ? f7.HID_FAIL_TO_INVOKE_PRIVATE_METHOD : i == x6.i.b ? f7.HID_INPUT_DEVICE_DISABLED : i == x6.j.b ? f7.HID_UNKNOWN_ERROR : f7.GATT_ERROR;
    }
}
