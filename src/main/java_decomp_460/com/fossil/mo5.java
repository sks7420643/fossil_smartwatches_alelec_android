package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo5 {
    @rj4("createdAt")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2406a; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @rj4("updatedAt")
    public String b; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @zj5
    public int c; // = 1;
    @DexIgnore
    @rj4("id")
    public String d;
    @DexIgnore
    @rj4("buttons")
    public List<oo5> e;
    @DexIgnore
    @rj4("checksumFace")
    public String f;
    @DexIgnore
    @rj4("downloadFaceUrl")
    public String g;
    @DexIgnore
    @rj4("previewFaceUrl")
    public String h;
    @DexIgnore
    @rj4("isActive")
    public boolean i;
    @DexIgnore
    @rj4("name")
    public String j;
    @DexIgnore
    @rj4("serialNumber")
    public String k;
    @DexIgnore
    @rj4("uid")
    public String l;
    @DexIgnore
    @rj4("originalItemIdInStore")
    public String m;
    @DexIgnore
    @rj4("_isNew")
    public boolean n;

    @DexIgnore
    public mo5(String str, List<oo5> list, String str2, String str3, String str4, boolean z, String str5, String str6, String str7, String str8, boolean z2) {
        pq7.c(str, "id");
        pq7.c(str2, "checksumFace");
        pq7.c(str3, "faceUrl");
        pq7.c(str5, "name");
        pq7.c(str6, "serialNumber");
        pq7.c(str7, "uid");
        this.d = str;
        this.e = list;
        this.f = str2;
        this.g = str3;
        this.h = str4;
        this.i = z;
        this.j = str5;
        this.k = str6;
        this.l = str7;
        this.m = str8;
        this.n = z2;
    }

    @DexIgnore
    public final List<oo5> a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.f;
    }

    @DexIgnore
    public final String c() {
        return this.f2406a;
    }

    @DexIgnore
    public final String d() {
        return this.g;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof mo5) {
                mo5 mo5 = (mo5) obj;
                if (!pq7.a(this.d, mo5.d) || !pq7.a(this.e, mo5.e) || !pq7.a(this.f, mo5.f) || !pq7.a(this.g, mo5.g) || !pq7.a(this.h, mo5.h) || this.i != mo5.i || !pq7.a(this.j, mo5.j) || !pq7.a(this.k, mo5.k) || !pq7.a(this.l, mo5.l) || !pq7.a(this.m, mo5.m) || this.n != mo5.n) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.j;
    }

    @DexIgnore
    public final String g() {
        return this.m;
    }

    @DexIgnore
    public final int h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 1;
        int i3 = 0;
        String str = this.d;
        int hashCode = str != null ? str.hashCode() : 0;
        List<oo5> list = this.e;
        int hashCode2 = list != null ? list.hashCode() : 0;
        String str2 = this.f;
        int hashCode3 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.g;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.h;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        boolean z = this.i;
        if (z) {
            z = true;
        }
        String str5 = this.j;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.k;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.l;
        int hashCode8 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.m;
        if (str8 != null) {
            i3 = str8.hashCode();
        }
        boolean z2 = this.n;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i4) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i3) * 31) + i2;
    }

    @DexIgnore
    public final String i() {
        return this.h;
    }

    @DexIgnore
    public final String j() {
        return this.k;
    }

    @DexIgnore
    public final String k() {
        return this.l;
    }

    @DexIgnore
    public final String l() {
        return this.b;
    }

    @DexIgnore
    public final boolean m() {
        return this.i;
    }

    @DexIgnore
    public final boolean n() {
        return this.n;
    }

    @DexIgnore
    public final void o(boolean z) {
        this.i = z;
    }

    @DexIgnore
    public final void p(List<oo5> list) {
        this.e = list;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "<set-?>");
        this.f2406a = str;
    }

    @DexIgnore
    public final void r(String str) {
        pq7.c(str, "<set-?>");
        this.j = str;
    }

    @DexIgnore
    public final void s(int i2) {
        this.c = i2;
    }

    @DexIgnore
    public final void t(String str) {
        pq7.c(str, "<set-?>");
        this.b = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.d + ", buttons=" + this.e + ", checksumFace=" + this.f + ", faceUrl=" + this.g + ", previewFaceUrl=" + this.h + ", isActive=" + this.i + ", name=" + this.j + ", serialNumber=" + this.k + ", uid=" + this.l + ", originalItemIdInStore=" + this.m + ", isNew=" + this.n + ")";
    }
}
