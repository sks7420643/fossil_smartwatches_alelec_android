package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1099a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public l77 e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public k77 h;
    @DexIgnore
    public String i;

    @DexIgnore
    public fb7(String str, String str2, String str3, String str4, l77 l77, String str5, Drawable drawable, k77 k77, String str6) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(l77, "assetType");
        pq7.c(str5, "checkSum");
        this.f1099a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = l77;
        this.f = str5;
        this.g = drawable;
        this.h = k77;
        this.i = str6;
    }

    @DexIgnore
    public final l77 a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.f1099a;
    }

    @DexIgnore
    public final String c() {
        return this.i;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final Drawable e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof fb7) {
                fb7 fb7 = (fb7) obj;
                if (!pq7.a(this.f1099a, fb7.f1099a) || !pq7.a(this.b, fb7.b) || !pq7.a(this.c, fb7.c) || !pq7.a(this.d, fb7.d) || !pq7.a(this.e, fb7.e) || !pq7.a(this.f, fb7.f) || !pq7.a(this.g, fb7.g) || !pq7.a(this.h, fb7.h) || !pq7.a(this.i, fb7.i)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.f1099a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        l77 l77 = this.e;
        int hashCode5 = l77 != null ? l77.hashCode() : 0;
        String str5 = this.f;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        Drawable drawable = this.g;
        int hashCode7 = drawable != null ? drawable.hashCode() : 0;
        k77 k77 = this.h;
        int hashCode8 = k77 != null ? k77.hashCode() : 0;
        String str6 = this.i;
        if (str6 != null) {
            i2 = str6.hashCode();
        }
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "UIWFAsset(id=" + this.f1099a + ", name=" + this.b + ", urlData=" + this.c + ", previewUrl=" + this.d + ", assetType=" + this.e + ", checkSum=" + this.f + ", previewDrawable=" + this.g + ", colour=" + this.h + ", localDataPath=" + this.i + ")";
    }
}
