package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l63 implements xw2<o63> {
    @DexIgnore
    public static l63 c; // = new l63();
    @DexIgnore
    public /* final */ xw2<o63> b;

    @DexIgnore
    public l63() {
        this(ww2.b(new n63()));
    }

    @DexIgnore
    public l63(xw2<o63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((o63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((o63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ o63 zza() {
        return this.b.zza();
    }
}
