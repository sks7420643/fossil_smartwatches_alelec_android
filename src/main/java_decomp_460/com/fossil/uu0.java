package com.fossil;

import com.fossil.zu0;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f3645a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ zu0.d<T> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public static /* final */ Object d; // = new Object();
        @DexIgnore
        public static Executor e;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Executor f3646a;
        @DexIgnore
        public Executor b;
        @DexIgnore
        public /* final */ zu0.d<T> c;

        @DexIgnore
        public a(zu0.d<T> dVar) {
            this.c = dVar;
        }

        @DexIgnore
        public uu0<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new uu0<>(this.f3646a, this.b, this.c);
        }
    }

    @DexIgnore
    public uu0(Executor executor, Executor executor2, zu0.d<T> dVar) {
        this.f3645a = executor;
        this.b = executor2;
        this.c = dVar;
    }

    @DexIgnore
    public Executor a() {
        return this.b;
    }

    @DexIgnore
    public zu0.d<T> b() {
        return this.c;
    }

    @DexIgnore
    public Executor c() {
        return this.f3645a;
    }
}
