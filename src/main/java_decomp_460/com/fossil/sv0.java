package com.fossil;

import androidx.collection.SimpleArrayMap;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SimpleArrayMap<RecyclerView.ViewHolder, a> f3310a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ dj0<RecyclerView.ViewHolder> b; // = new dj0<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static mn0<a> d; // = new nn0(20);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3311a;
        @DexIgnore
        public RecyclerView.j.c b;
        @DexIgnore
        public RecyclerView.j.c c;

        @DexIgnore
        public static void a() {
            do {
            } while (d.b() != null);
        }

        @DexIgnore
        public static a b() {
            a b2 = d.b();
            return b2 == null ? new a() : b2;
        }

        @DexIgnore
        public static void c(a aVar) {
            aVar.f3311a = 0;
            aVar.b = null;
            aVar.c = null;
            d.a(aVar);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        void b(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void c(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void d(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.f3310a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.f3310a.put(viewHolder, aVar);
        }
        aVar.f3311a |= 2;
        aVar.b = cVar;
    }

    @DexIgnore
    public void b(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.f3310a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.f3310a.put(viewHolder, aVar);
        }
        aVar.f3311a |= 1;
    }

    @DexIgnore
    public void c(long j, RecyclerView.ViewHolder viewHolder) {
        this.b.r(j, viewHolder);
    }

    @DexIgnore
    public void d(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.f3310a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.f3310a.put(viewHolder, aVar);
        }
        aVar.c = cVar;
        aVar.f3311a |= 8;
    }

    @DexIgnore
    public void e(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.f3310a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.f3310a.put(viewHolder, aVar);
        }
        aVar.b = cVar;
        aVar.f3311a |= 4;
    }

    @DexIgnore
    public void f() {
        this.f3310a.clear();
        this.b.e();
    }

    @DexIgnore
    public RecyclerView.ViewHolder g(long j) {
        return this.b.l(j);
    }

    @DexIgnore
    public boolean h(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.f3310a.get(viewHolder);
        return (aVar == null || (aVar.f3311a & 1) == 0) ? false : true;
    }

    @DexIgnore
    public boolean i(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.f3310a.get(viewHolder);
        return (aVar == null || (aVar.f3311a & 4) == 0) ? false : true;
    }

    @DexIgnore
    public void j() {
        a.a();
    }

    @DexIgnore
    public void k(RecyclerView.ViewHolder viewHolder) {
        p(viewHolder);
    }

    @DexIgnore
    public final RecyclerView.j.c l(RecyclerView.ViewHolder viewHolder, int i) {
        a n;
        RecyclerView.j.c cVar = null;
        int g = this.f3310a.g(viewHolder);
        if (g >= 0 && (n = this.f3310a.n(g)) != null) {
            int i2 = n.f3311a;
            if ((i2 & i) != 0) {
                n.f3311a = i & i2;
                if (i == 4) {
                    cVar = n.b;
                } else if (i == 8) {
                    cVar = n.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((n.f3311a & 12) == 0) {
                    this.f3310a.l(g);
                    a.c(n);
                }
            }
        }
        return cVar;
    }

    @DexIgnore
    public RecyclerView.j.c m(RecyclerView.ViewHolder viewHolder) {
        return l(viewHolder, 8);
    }

    @DexIgnore
    public RecyclerView.j.c n(RecyclerView.ViewHolder viewHolder) {
        return l(viewHolder, 4);
    }

    @DexIgnore
    public void o(b bVar) {
        for (int size = this.f3310a.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder j = this.f3310a.j(size);
            a l = this.f3310a.l(size);
            int i = l.f3311a;
            if ((i & 3) == 3) {
                bVar.a(j);
            } else if ((i & 1) != 0) {
                RecyclerView.j.c cVar = l.b;
                if (cVar == null) {
                    bVar.a(j);
                } else {
                    bVar.c(j, cVar, l.c);
                }
            } else if ((i & 14) == 14) {
                bVar.b(j, l.b, l.c);
            } else if ((i & 12) == 12) {
                bVar.d(j, l.b, l.c);
            } else if ((i & 4) != 0) {
                bVar.c(j, l.b, null);
            } else if ((i & 8) != 0) {
                bVar.b(j, l.b, l.c);
            }
            a.c(l);
        }
    }

    @DexIgnore
    public void p(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.f3310a.get(viewHolder);
        if (aVar != null) {
            aVar.f3311a &= -2;
        }
    }

    @DexIgnore
    public void q(RecyclerView.ViewHolder viewHolder) {
        int u = this.b.u() - 1;
        while (true) {
            if (u < 0) {
                break;
            } else if (viewHolder == this.b.v(u)) {
                this.b.t(u);
                break;
            } else {
                u--;
            }
        }
        a remove = this.f3310a.remove(viewHolder);
        if (remove != null) {
            a.c(remove);
        }
    }
}
