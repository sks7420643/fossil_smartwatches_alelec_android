package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok4 extends IOException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;

    @DexIgnore
    public ok4(String str) {
        super(str);
    }

    @DexIgnore
    public ok4(String str, Throwable th) {
        super(str);
        initCause(th);
    }

    @DexIgnore
    public ok4(Throwable th) {
        initCause(th);
    }
}
