package com.fossil;

import androidx.lifecycle.MutableLiveData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hq4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ yk7 f1512a; // = zk7.a(f.INSTANCE);
    @DexIgnore
    public /* final */ yk7 b; // = zk7.a(g.INSTANCE);
    @DexIgnore
    public /* final */ yk7 c; // = zk7.a(h.INSTANCE);
    @DexIgnore
    public /* final */ yk7 d; // = zk7.a(i.INSTANCE);
    @DexIgnore
    public /* final */ yk7 e; // = zk7.a(d.INSTANCE);
    @DexIgnore
    public /* final */ yk7 f; // = zk7.a(e.INSTANCE);
    @DexIgnore
    public /* final */ yk7 g; // = zk7.a(j.INSTANCE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f1513a;
        @DexIgnore
        public String b;

        @DexIgnore
        public a(int i, String str) {
            pq7.c(str, "errorMessage");
            this.f1513a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f1513a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final void c(int i) {
            this.f1513a = i;
        }

        @DexIgnore
        public final void d(String str) {
            pq7.c(str, "<set-?>");
            this.b = str;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (this.f1513a != aVar.f1513a || !pq7.a(this.b, aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f1513a;
            String str = this.b;
            return (str != null ? str.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "DialogErrorState(errorCode=" + this.f1513a + ", errorMessage=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1514a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;

        @DexIgnore
        public b() {
            this(false, false, null, 7, null);
        }

        @DexIgnore
        public b(boolean z, boolean z2, String str) {
            pq7.c(str, "message");
            this.f1514a = z;
            this.b = z2;
            this.c = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(boolean z, boolean z2, String str, int i, kq7 kq7) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? "" : str);
        }

        @DexIgnore
        public final boolean a() {
            return this.f1514a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.f1514a = z;
        }

        @DexIgnore
        public final void d(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void e(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(this.f1514a == bVar.f1514a && this.b == bVar.b && pq7.a(this.c, bVar.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.f1514a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            String str = this.c;
            int hashCode = str != null ? str.hashCode() : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((i2 * 31) + i) * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "LoadingState(mStartLoading=" + this.f1514a + ", mStopLoading=" + this.b + ", message=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<uh5> f1515a;

        @DexIgnore
        public c() {
            this(null, 1, null);
        }

        @DexIgnore
        public c(List<uh5> list) {
            pq7.c(list, "permissionCodes");
            this.f1515a = list;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(List list, int i, kq7 kq7) {
            this((i & 1) != 0 ? new ArrayList() : list);
        }

        @DexIgnore
        public final List<uh5> a() {
            return this.f1515a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof c) && pq7.a(this.f1515a, ((c) obj).f1515a));
        }

        @DexIgnore
        public int hashCode() {
            List<uh5> list = this.f1515a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PermissionState(permissionCodes=" + this.f1515a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements gp7<a> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final a invoke() {
            return new a(-1, "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends qq7 implements gp7<MutableLiveData<a>> {
        @DexIgnore
        public static /* final */ e INSTANCE; // = new e();

        @DexIgnore
        public e() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final MutableLiveData<a> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends qq7 implements gp7<b> {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final b invoke() {
            return new b(false, false, null, 7, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends qq7 implements gp7<MutableLiveData<b>> {
        @DexIgnore
        public static /* final */ g INSTANCE; // = new g();

        @DexIgnore
        public g() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final MutableLiveData<b> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends qq7 implements gp7<c> {
        @DexIgnore
        public static /* final */ h INSTANCE; // = new h();

        @DexIgnore
        public h() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final c invoke() {
            return new c(null, 1, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends qq7 implements gp7<MutableLiveData<c>> {
        @DexIgnore
        public static /* final */ i INSTANCE; // = new i();

        @DexIgnore
        public i() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final MutableLiveData<c> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends qq7 implements gp7<MutableLiveData<Object>> {
        @DexIgnore
        public static /* final */ j INSTANCE; // = new j();

        @DexIgnore
        public j() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final MutableLiveData<Object> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore
    public static /* synthetic */ void b(hq4 hq4, int i2, String str, int i3, Object obj) {
        if (obj == null) {
            if ((i3 & 1) != 0) {
                i2 = -1;
            }
            if ((i3 & 2) != 0) {
                str = "";
            }
            hq4.a(i2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitDialogErrorState");
    }

    @DexIgnore
    public static /* synthetic */ void d(hq4 hq4, boolean z, boolean z2, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                z = false;
            }
            if ((i2 & 2) != 0) {
                z2 = false;
            }
            if ((i2 & 4) != 0) {
                str = "";
            }
            hq4.c(z, z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitLoadingState");
    }

    @DexIgnore
    public final void a(int i2, String str) {
        pq7.c(str, "errorMessage");
        g().c(i2);
        g().d(str);
        h().l(g());
    }

    @DexIgnore
    public final void c(boolean z, boolean z2, String str) {
        pq7.c(str, "message");
        i().c(z);
        i().d(z2);
        i().e(str);
        j().l(i());
    }

    @DexIgnore
    public final void e(uh5... uh5Arr) {
        pq7.c(uh5Arr, "permissionCodes");
        k().a().clear();
        k().a().addAll(em7.d0(uh5Arr));
        l().l(k());
    }

    @DexIgnore
    public final void f() {
        m().l(new Object());
    }

    @DexIgnore
    public final a g() {
        return (a) this.e.getValue();
    }

    @DexIgnore
    public final MutableLiveData<a> h() {
        return (MutableLiveData) this.f.getValue();
    }

    @DexIgnore
    public final b i() {
        return (b) this.f1512a.getValue();
    }

    @DexIgnore
    public final MutableLiveData<b> j() {
        return (MutableLiveData) this.b.getValue();
    }

    @DexIgnore
    public final c k() {
        return (c) this.c.getValue();
    }

    @DexIgnore
    public final MutableLiveData<c> l() {
        return (MutableLiveData) this.d.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Object> m() {
        return (MutableLiveData) this.g.getValue();
    }
}
