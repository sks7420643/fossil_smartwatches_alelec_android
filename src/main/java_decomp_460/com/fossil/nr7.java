package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr7<T> implements or7<Object, T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public T f2559a;

    @DexIgnore
    @Override // com.fossil.or7
    public void a(Object obj, ks7<?> ks7, T t) {
        pq7.c(ks7, "property");
        pq7.c(t, "value");
        this.f2559a = t;
    }

    @DexIgnore
    @Override // com.fossil.or7
    public T b(Object obj, ks7<?> ks7) {
        pq7.c(ks7, "property");
        T t = this.f2559a;
        if (t != null) {
            return t;
        }
        throw new IllegalStateException("Property " + ks7.getName() + " should be initialized before get.");
    }
}
