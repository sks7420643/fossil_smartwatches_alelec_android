package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.g04;
import com.google.android.material.card.MaterialCardView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qx3 {
    @DexIgnore
    public static /* final */ int[] t; // = {16842912};
    @DexIgnore
    public static /* final */ double u; // = Math.cos(Math.toRadians(45.0d));

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MaterialCardView f3042a;
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public /* final */ c04 c;
    @DexIgnore
    public /* final */ c04 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Drawable h;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public g04 l;
    @DexIgnore
    public ColorStateList m;
    @DexIgnore
    public Drawable n;
    @DexIgnore
    public LayerDrawable o;
    @DexIgnore
    public c04 p;
    @DexIgnore
    public c04 q;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends InsetDrawable {
        @DexIgnore
        public a(qx3 qx3, Drawable drawable, int i, int i2, int i3, int i4) {
            super(drawable, i, i2, i3, i4);
        }

        @DexIgnore
        public boolean getPadding(Rect rect) {
            return false;
        }
    }

    @DexIgnore
    public qx3(MaterialCardView materialCardView, AttributeSet attributeSet, int i2, int i3) {
        this.f3042a = materialCardView;
        c04 c04 = new c04(materialCardView.getContext(), attributeSet, i2, i3);
        this.c = c04;
        c04.M(materialCardView.getContext());
        this.c.a0(-12303292);
        g04.b v = this.c.C().v();
        TypedArray obtainStyledAttributes = materialCardView.getContext().obtainStyledAttributes(attributeSet, tw3.CardView, i2, sw3.CardView);
        if (obtainStyledAttributes.hasValue(tw3.CardView_cardCornerRadius)) {
            v.o(obtainStyledAttributes.getDimension(tw3.CardView_cardCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        this.d = new c04();
        L(v.m());
        Resources resources = materialCardView.getResources();
        this.e = resources.getDimensionPixelSize(lw3.mtrl_card_checked_icon_margin);
        this.f = resources.getDimensionPixelSize(lw3.mtrl_card_checked_icon_size);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public boolean A() {
        return this.s;
    }

    @DexIgnore
    public void B(TypedArray typedArray) {
        ColorStateList a2 = oz3.a(this.f3042a.getContext(), typedArray, tw3.MaterialCardView_strokeColor);
        this.m = a2;
        if (a2 == null) {
            this.m = ColorStateList.valueOf(-1);
        }
        this.g = typedArray.getDimensionPixelSize(tw3.MaterialCardView_strokeWidth, 0);
        boolean z = typedArray.getBoolean(tw3.MaterialCardView_android_checkable, false);
        this.s = z;
        this.f3042a.setLongClickable(z);
        this.k = oz3.a(this.f3042a.getContext(), typedArray, tw3.MaterialCardView_checkedIconTint);
        G(oz3.d(this.f3042a.getContext(), typedArray, tw3.MaterialCardView_checkedIcon));
        ColorStateList a3 = oz3.a(this.f3042a.getContext(), typedArray, tw3.MaterialCardView_rippleColor);
        this.j = a3;
        if (a3 == null) {
            this.j = ColorStateList.valueOf(vx3.c(this.f3042a, jw3.colorControlHighlight));
        }
        ColorStateList a4 = oz3.a(this.f3042a.getContext(), typedArray, tw3.MaterialCardView_cardForegroundColor);
        c04 c04 = this.d;
        if (a4 == null) {
            a4 = ColorStateList.valueOf(0);
        }
        c04.V(a4);
        W();
        T();
        X();
        this.f3042a.setBackgroundInternal(y(this.c));
        Drawable o2 = this.f3042a.isClickable() ? o() : this.d;
        this.h = o2;
        this.f3042a.setForeground(y(o2));
    }

    @DexIgnore
    public void C(int i2, int i3) {
        int i4;
        int i5;
        if (this.o != null) {
            int i6 = this.e;
            int i7 = this.f;
            int i8 = (i2 - i6) - i7;
            if (mo0.z(this.f3042a) == 1) {
                i4 = i8;
                i5 = i6;
            } else {
                i4 = i6;
                i5 = i8;
            }
            this.o.setLayerInset(2, i5, this.e, i4, (i3 - i6) - i7);
        }
    }

    @DexIgnore
    public void D(boolean z) {
        this.r = z;
    }

    @DexIgnore
    public void E(ColorStateList colorStateList) {
        this.c.V(colorStateList);
    }

    @DexIgnore
    public void F(boolean z) {
        this.s = z;
    }

    @DexIgnore
    public void G(Drawable drawable) {
        this.i = drawable;
        if (drawable != null) {
            Drawable r2 = am0.r(drawable.mutate());
            this.i = r2;
            am0.o(r2, this.k);
        }
        if (this.o != null) {
            this.o.setDrawableByLayerId(nw3.mtrl_card_checked_layer_id, f());
        }
    }

    @DexIgnore
    public void H(ColorStateList colorStateList) {
        this.k = colorStateList;
        Drawable drawable = this.i;
        if (drawable != null) {
            am0.o(drawable, colorStateList);
        }
    }

    @DexIgnore
    public void I(float f2) {
        L(this.l.w(f2));
        this.h.invalidateSelf();
        if (Q() || P()) {
            S();
        }
        if (Q()) {
            V();
        }
    }

    @DexIgnore
    public void J(float f2) {
        this.c.W(f2);
        c04 c04 = this.d;
        if (c04 != null) {
            c04.W(f2);
        }
        c04 c042 = this.q;
        if (c042 != null) {
            c042.W(f2);
        }
    }

    @DexIgnore
    public void K(ColorStateList colorStateList) {
        this.j = colorStateList;
        W();
    }

    @DexIgnore
    public void L(g04 g04) {
        this.l = g04;
        this.c.setShapeAppearanceModel(g04);
        c04 c04 = this.d;
        if (c04 != null) {
            c04.setShapeAppearanceModel(g04);
        }
        c04 c042 = this.q;
        if (c042 != null) {
            c042.setShapeAppearanceModel(g04);
        }
        c04 c043 = this.p;
        if (c043 != null) {
            c043.setShapeAppearanceModel(g04);
        }
    }

    @DexIgnore
    public void M(ColorStateList colorStateList) {
        if (this.m != colorStateList) {
            this.m = colorStateList;
            X();
        }
    }

    @DexIgnore
    public void N(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            X();
        }
    }

    @DexIgnore
    public void O(int i2, int i3, int i4, int i5) {
        this.b.set(i2, i3, i4, i5);
        S();
    }

    @DexIgnore
    public final boolean P() {
        return this.f3042a.getPreventCornerOverlap() && !e();
    }

    @DexIgnore
    public final boolean Q() {
        return this.f3042a.getPreventCornerOverlap() && e() && this.f3042a.getUseCompatPadding();
    }

    @DexIgnore
    public void R() {
        Drawable drawable = this.h;
        Drawable o2 = this.f3042a.isClickable() ? o() : this.d;
        this.h = o2;
        if (drawable != o2) {
            U(o2);
        }
    }

    @DexIgnore
    public void S() {
        int a2 = (int) ((P() || Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) - q());
        MaterialCardView materialCardView = this.f3042a;
        Rect rect = this.b;
        materialCardView.m(rect.left + a2, rect.top + a2, rect.right + a2, a2 + rect.bottom);
    }

    @DexIgnore
    public void T() {
        this.c.U(this.f3042a.getCardElevation());
    }

    @DexIgnore
    public final void U(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 23 || !(this.f3042a.getForeground() instanceof InsetDrawable)) {
            this.f3042a.setForeground(y(drawable));
        } else {
            ((InsetDrawable) this.f3042a.getForeground()).setDrawable(drawable);
        }
    }

    @DexIgnore
    public void V() {
        if (!z()) {
            this.f3042a.setBackgroundInternal(y(this.c));
        }
        this.f3042a.setForeground(y(this.h));
    }

    @DexIgnore
    public final void W() {
        Drawable drawable;
        if (!tz3.f3498a || (drawable = this.n) == null) {
            c04 c04 = this.p;
            if (c04 != null) {
                c04.V(this.j);
                return;
            }
            return;
        }
        ((RippleDrawable) drawable).setColor(this.j);
    }

    @DexIgnore
    public void X() {
        this.d.e0((float) this.g, this.m);
    }

    @DexIgnore
    public final float a() {
        return Math.max(Math.max(b(this.l.q(), this.c.F()), b(this.l.s(), this.c.G())), Math.max(b(this.l.k(), this.c.s()), b(this.l.i(), this.c.r())));
    }

    @DexIgnore
    public final float b(zz3 zz3, float f2) {
        return zz3 instanceof f04 ? (float) ((1.0d - u) * ((double) f2)) : zz3 instanceof a04 ? f2 / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float c() {
        return (Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + this.f3042a.getMaxCardElevation();
    }

    @DexIgnore
    public final float d() {
        return (Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + (this.f3042a.getMaxCardElevation() * 1.5f);
    }

    @DexIgnore
    public final boolean e() {
        return Build.VERSION.SDK_INT >= 21 && this.c.P();
    }

    @DexIgnore
    public final Drawable f() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = this.i;
        if (drawable != null) {
            stateListDrawable.addState(t, drawable);
        }
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable g() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        c04 i2 = i();
        this.p = i2;
        i2.V(this.j);
        int[] iArr = {16842919};
        stateListDrawable.addState(iArr, this.p);
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable h() {
        if (!tz3.f3498a) {
            return g();
        }
        this.q = i();
        return new RippleDrawable(this.j, null, this.q);
    }

    @DexIgnore
    public final c04 i() {
        return new c04(this.l);
    }

    @DexIgnore
    public void j() {
        Drawable drawable = this.n;
        if (drawable != null) {
            Rect bounds = drawable.getBounds();
            int i2 = bounds.bottom;
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2 - 1);
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2);
        }
    }

    @DexIgnore
    public c04 k() {
        return this.c;
    }

    @DexIgnore
    public ColorStateList l() {
        return this.c.w();
    }

    @DexIgnore
    public Drawable m() {
        return this.i;
    }

    @DexIgnore
    public ColorStateList n() {
        return this.k;
    }

    @DexIgnore
    public final Drawable o() {
        if (this.n == null) {
            this.n = h();
        }
        if (this.o == null) {
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{this.n, this.d, f()});
            this.o = layerDrawable;
            layerDrawable.setId(2, nw3.mtrl_card_checked_layer_id);
        }
        return this.o;
    }

    @DexIgnore
    public float p() {
        return this.c.F();
    }

    @DexIgnore
    public final float q() {
        return (!this.f3042a.getPreventCornerOverlap() || (Build.VERSION.SDK_INT >= 21 && !this.f3042a.getUseCompatPadding())) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (float) ((1.0d - u) * ((double) this.f3042a.getCardViewRadius()));
    }

    @DexIgnore
    public float r() {
        return this.c.x();
    }

    @DexIgnore
    public ColorStateList s() {
        return this.j;
    }

    @DexIgnore
    public g04 t() {
        return this.l;
    }

    @DexIgnore
    public int u() {
        ColorStateList colorStateList = this.m;
        if (colorStateList == null) {
            return -1;
        }
        return colorStateList.getDefaultColor();
    }

    @DexIgnore
    public ColorStateList v() {
        return this.m;
    }

    @DexIgnore
    public int w() {
        return this.g;
    }

    @DexIgnore
    public Rect x() {
        return this.b;
    }

    @DexIgnore
    public final Drawable y(Drawable drawable) {
        int i2;
        int i3;
        if ((Build.VERSION.SDK_INT < 21) || this.f3042a.getUseCompatPadding()) {
            i2 = (int) Math.ceil((double) d());
            i3 = (int) Math.ceil((double) c());
        } else {
            i2 = 0;
            i3 = 0;
        }
        return new a(this, drawable, i3, i2, i3, i2);
    }

    @DexIgnore
    public boolean z() {
        return this.r;
    }
}
