package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m90 {
    @DexIgnore
    public /* synthetic */ m90(kq7 kq7) {
    }

    @DexIgnore
    public final n90 a(String str) {
        n90[] values = n90.values();
        for (n90 n90 : values) {
            if (pq7.a(n90.b, str)) {
                return n90;
            }
        }
        return null;
    }
}
