package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tv extends ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ n6 I;
    @DexIgnore
    public /* final */ n6 J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    public tv(sv svVar, short s, hs hsVar, k5 k5Var, int i) {
        super(hsVar, k5Var, i);
        this.K = (short) s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(svVar.b).putShort(this.K).array();
        pq7.b(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(svVar.a()).putShort(this.K).array();
        pq7.b(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        n6 n6Var = n6.FTC;
        this.I = n6Var;
        this.J = n6Var;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final mt E(byte b) {
        return kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] P() {
        return this.H;
    }

    @DexIgnore
    public void Q(byte[] bArr) {
        this.H = bArr;
    }

    @DexIgnore
    public final short R() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A0, hy1.l(this.K, null, 1, null));
    }
}
