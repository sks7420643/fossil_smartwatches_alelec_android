package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw extends ox1 {
    @DexIgnore
    public static /* final */ iw g; // = new iw(null);
    @DexIgnore
    public /* final */ hs b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ lw d;
    @DexIgnore
    public /* final */ s5 e;
    @DexIgnore
    public /* final */ mt f;

    @DexIgnore
    public mw(hs hsVar, String str, lw lwVar, s5 s5Var, mt mtVar) {
        this.b = hsVar;
        this.c = str;
        this.d = lwVar;
        this.e = s5Var;
        this.f = mtVar;
    }

    @DexIgnore
    public /* synthetic */ mw(hs hsVar, String str, lw lwVar, s5 s5Var, mt mtVar, int i) {
        hsVar = (i & 1) != 0 ? hs.b : hsVar;
        str = (i & 2) != 0 ? "" : str;
        s5Var = (i & 8) != 0 ? new s5(null, r5.b, null, 5) : s5Var;
        mtVar = (i & 16) != 0 ? null : mtVar;
        this.b = hsVar;
        this.c = str;
        this.d = lwVar;
        this.e = s5Var;
        this.f = mtVar;
    }

    @DexIgnore
    public static /* synthetic */ mw a(mw mwVar, hs hsVar, String str, lw lwVar, s5 s5Var, mt mtVar, int i) {
        return mwVar.a((i & 1) != 0 ? mwVar.b : hsVar, (i & 2) != 0 ? mwVar.c : str, (i & 4) != 0 ? mwVar.d : lwVar, (i & 8) != 0 ? mwVar.e : s5Var, (i & 16) != 0 ? mwVar.f : mtVar);
    }

    @DexIgnore
    public final mw a(hs hsVar, String str, lw lwVar, s5 s5Var, mt mtVar) {
        return new mw(hsVar, str, lwVar, s5Var, mtVar);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof mw) {
                mw mwVar = (mw) obj;
                if (!pq7.a(this.b, mwVar.b) || !pq7.a(this.c, mwVar.c) || !pq7.a(this.d, mwVar.d) || !pq7.a(this.e, mwVar.e) || !pq7.a(this.f, mwVar.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        hs hsVar = this.b;
        int hashCode = hsVar != null ? hsVar.hashCode() : 0;
        String str = this.c;
        int hashCode2 = str != null ? str.hashCode() : 0;
        lw lwVar = this.d;
        int hashCode3 = lwVar != null ? lwVar.hashCode() : 0;
        s5 s5Var = this.e;
        int hashCode4 = s5Var != null ? s5Var.hashCode() : 0;
        mt mtVar = this.f;
        if (mtVar != null) {
            i = mtVar.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(g80.k(jSONObject, jd0.q, ey1.a(this.b)), jd0.O0, ey1.a(this.d));
            mt mtVar = this.f;
            if (mtVar != null && !mtVar.a()) {
                g80.k(jSONObject, jd0.l3, this.f.getLogName());
            }
            if (this.e.c != r5.b) {
                g80.k(jSONObject, jd0.l4, this.e.toJSONObject());
            }
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e2 = e.e("Result(requestId=");
        e2.append(this.b);
        e2.append(", requestUuid=");
        e2.append(this.c);
        e2.append(", resultCode=");
        e2.append(this.d);
        e2.append(", commandResult=");
        e2.append(this.e);
        e2.append(", responseStatus=");
        e2.append(this.f);
        e2.append(")");
        return e2.toString();
    }
}
