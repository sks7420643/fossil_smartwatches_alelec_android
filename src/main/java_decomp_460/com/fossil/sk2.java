package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sk2 extends IInterface {
    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    boolean x0(boolean z) throws RemoteException;
}
