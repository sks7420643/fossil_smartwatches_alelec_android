package com.fossil;

import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewTreeObserver.OnWindowFocusChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ EditText f1316a;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager b;

        @DexIgnore
        public a(EditText editText, InputMethodManager inputMethodManager) {
            this.f1316a = editText;
            this.b = inputMethodManager;
        }

        @DexIgnore
        public void onWindowFocusChanged(boolean z) {
            if (z) {
                gj5.c(this.f1316a, this.b);
                this.f1316a.getViewTreeObserver().removeOnWindowFocusChangeListener(this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager c;

        @DexIgnore
        public b(EditText editText, InputMethodManager inputMethodManager) {
            this.b = editText;
            this.c = inputMethodManager;
        }

        @DexIgnore
        public final void run() {
            this.c.showSoftInput(this.b, 1);
        }
    }

    @DexIgnore
    public static final void b(EditText editText, InputMethodManager inputMethodManager) {
        pq7.c(editText, "$this$focusAndShowKeyboard");
        pq7.c(inputMethodManager, "imm");
        editText.requestFocus();
        if (editText.hasWindowFocus()) {
            c(editText, inputMethodManager);
        } else {
            editText.getViewTreeObserver().addOnWindowFocusChangeListener(new a(editText, inputMethodManager));
        }
    }

    @DexIgnore
    public static final void c(EditText editText, InputMethodManager inputMethodManager) {
        if (editText.isFocused()) {
            editText.post(new b(editText, inputMethodManager));
        }
    }
}
