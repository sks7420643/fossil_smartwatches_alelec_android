package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3720a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public v57() {
        this(0, 0, 0, 7, null);
    }

    @DexIgnore
    public v57(int i, int i2, int i3) {
        this.f3720a = i;
        this.b = i2;
        this.c = i3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ v57(int i, int i2, int i3, int i4, kq7 kq7) {
        this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? -1 : i2, (i4 & 4) != 0 ? -1 : i3);
    }

    @DexIgnore
    public final int a() {
        return this.f3720a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof v57) {
                v57 v57 = (v57) obj;
                if (!(this.f3720a == v57.f3720a && this.b == v57.b && this.c == v57.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void f(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void g(int i) {
        this.b = i;
    }

    @DexIgnore
    public final void h(int i) {
        this.f3720a = i;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.f3720a * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateSleepSessionModel(value=" + this.f3720a + ", time=" + this.b + ", sleepState=" + this.c + ")";
    }
}
