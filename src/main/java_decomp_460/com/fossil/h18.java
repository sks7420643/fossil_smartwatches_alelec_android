package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h18 {
    @DexIgnore
    public static /* final */ Pattern j; // = Pattern.compile("(\\d{2,4})[^\\d]*");
    @DexIgnore
    public static /* final */ Pattern k; // = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
    @DexIgnore
    public static /* final */ Pattern l; // = Pattern.compile("(\\d{1,2})[^\\d]*");
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1412a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public h18(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f1412a = str;
        this.b = str2;
        this.c = j2;
        this.d = str3;
        this.e = str4;
        this.f = z;
        this.g = z2;
        this.i = z3;
        this.h = z4;
    }

    @DexIgnore
    public static int a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (((charAt < ' ' && charAt != '\t') || charAt >= '\u007f' || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i4;
            }
        }
        return i3;
    }

    @DexIgnore
    public static boolean b(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        return str.endsWith(str2) && str.charAt((str.length() - str2.length()) + -1) == '.' && !b28.J(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0155  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.h18 d(long r20, com.fossil.q18 r22, java.lang.String r23) {
        /*
        // Method dump skipped, instructions count: 362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.h18.d(long, com.fossil.q18, java.lang.String):com.fossil.h18");
    }

    @DexIgnore
    public static h18 e(q18 q18, String str) {
        return d(System.currentTimeMillis(), q18, str);
    }

    @DexIgnore
    public static List<h18> f(q18 q18, p18 p18) {
        ArrayList arrayList;
        List<String> j2 = p18.j("Set-Cookie");
        int size = j2.size();
        ArrayList arrayList2 = null;
        int i2 = 0;
        while (i2 < size) {
            h18 e2 = e(q18, j2.get(i2));
            if (e2 == null) {
                arrayList = arrayList2;
            } else {
                arrayList = arrayList2 == null ? new ArrayList() : arrayList2;
                arrayList.add(e2);
            }
            i2++;
            arrayList2 = arrayList;
        }
        return arrayList2 != null ? Collections.unmodifiableList(arrayList2) : Collections.emptyList();
    }

    @DexIgnore
    public static String g(String str) {
        if (!str.endsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
            if (str.startsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                str = str.substring(1);
            }
            String d2 = b28.d(str);
            if (d2 != null) {
                return d2;
            }
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static long h(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        Matcher matcher = m.matcher(str);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i5 == -1 && matcher.usePattern(m).matches()) {
                i5 = Integer.parseInt(matcher.group(1));
                i8 = Integer.parseInt(matcher.group(2));
                i9 = Integer.parseInt(matcher.group(3));
            } else if (i6 == -1 && matcher.usePattern(l).matches()) {
                i6 = Integer.parseInt(matcher.group(1));
            } else if (i7 == -1 && matcher.usePattern(k).matches()) {
                i7 = k.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i4 == -1 && matcher.usePattern(j).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (i4 >= 70 && i4 <= 99) {
            i4 += 1900;
        }
        if (i4 >= 0 && i4 <= 69) {
            i4 += 2000;
        }
        if (i4 < 1601) {
            throw new IllegalArgumentException();
        } else if (i7 == -1) {
            throw new IllegalArgumentException();
        } else if (i6 < 1 || i6 > 31) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 23) {
            throw new IllegalArgumentException();
        } else if (i8 < 0 || i8 > 59) {
            throw new IllegalArgumentException();
        } else if (i9 < 0 || i9 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(b28.o);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i4);
            gregorianCalendar.set(2, i7 - 1);
            gregorianCalendar.set(5, i6);
            gregorianCalendar.set(11, i5);
            gregorianCalendar.set(12, i8);
            gregorianCalendar.set(13, i9);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    @DexIgnore
    public static long i(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e2) {
            if (str.matches("-?\\d+")) {
                return !str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR) ? Long.MAX_VALUE : Long.MIN_VALUE;
            }
            throw e2;
        }
    }

    @DexIgnore
    public String c() {
        return this.f1412a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof h18)) {
            return false;
        }
        h18 h18 = (h18) obj;
        return h18.f1412a.equals(this.f1412a) && h18.b.equals(this.b) && h18.d.equals(this.d) && h18.e.equals(this.e) && h18.c == this.c && h18.f == this.f && h18.g == this.g && h18.h == this.h && h18.i == this.i;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f1412a.hashCode();
        int hashCode2 = this.b.hashCode();
        int hashCode3 = this.d.hashCode();
        int hashCode4 = this.e.hashCode();
        long j2 = this.c;
        return ((((((((((((((((hashCode + 527) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + ((int) ((j2 >>> 32) ^ j2))) * 31) + (!this.f ? 1 : 0)) * 31) + (!this.g ? 1 : 0)) * 31) + (!this.h ? 1 : 0)) * 31) + (!this.i ? 1 : 0);
    }

    @DexIgnore
    public String j(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f1412a);
        sb.append('=');
        sb.append(this.b);
        if (this.h) {
            if (this.c == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=");
                sb.append(t28.a(new Date(this.c)));
            }
        }
        if (!this.i) {
            sb.append("; domain=");
            if (z) {
                sb.append(CodelessMatcher.CURRENT_CLASS_NAME);
            }
            sb.append(this.d);
        }
        sb.append("; path=");
        sb.append(this.e);
        if (this.f) {
            sb.append("; secure");
        }
        if (this.g) {
            sb.append("; httponly");
        }
        return sb.toString();
    }

    @DexIgnore
    public String k() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return j(false);
    }
}
