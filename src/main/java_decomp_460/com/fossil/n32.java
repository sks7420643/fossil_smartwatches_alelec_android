package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.p32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class n32 implements p32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ n32 f2459a; // = new n32();

    @DexIgnore
    public static p32.a b() {
        return f2459a;
    }

    @DexIgnore
    @Override // com.fossil.p32.a
    public void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE events ADD COLUMN payload_encoding TEXT");
    }
}
