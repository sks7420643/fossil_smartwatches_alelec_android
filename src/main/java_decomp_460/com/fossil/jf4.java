package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf4 implements if4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1754a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public jf4(String str, String str2) {
        this.f1754a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // com.fossil.if4
    public final String a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.if4
    public final String getId() {
        return this.f1754a;
    }
}
