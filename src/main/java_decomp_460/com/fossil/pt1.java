package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ry1 d;
    @DexIgnore
    public /* final */ yu1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<pt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public pt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(lq1.class.getClassLoader());
            if (readParcelable != null) {
                lq1 lq1 = (lq1) readParcelable;
                nt1 nt1 = (nt1) parcel.readParcelable(nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new pt1(lq1, nt1, (ry1) readParcelable2, yu1.values()[parcel.readInt()]);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public pt1[] newArray(int i) {
            return new pt1[i];
        }
    }

    @DexIgnore
    public pt1(lq1 lq1, nt1 nt1, ry1 ry1, yu1 yu1) {
        super(lq1, nt1);
        this.e = yu1;
        this.d = ry1;
    }

    @DexIgnore
    public pt1(lq1 lq1, ry1 ry1, yu1 yu1) {
        super(lq1, null);
        this.e = yu1;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(g80.k(super.a(), jd0.t3, this.d.toString()), jd0.c4, ey1.a(this.e));
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        try {
            i9 i9Var = i9.d;
            jq1 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9Var.a(s, ry1, new kb0(((lq1) deviceRequest).c(), new ry1(this.d.getMajor(), this.d.getMinor()), this.e.a()).a());
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sx1 e2) {
            d90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(pt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            pt1 pt1 = (pt1) obj;
            if (!pq7.a(this.d, pt1.d)) {
                return false;
            }
            return this.e == pt1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.MicroAppErrorData");
    }

    @DexIgnore
    public final yu1 getErrorType() {
        return this.e;
    }

    @DexIgnore
    public final ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
    }
}
