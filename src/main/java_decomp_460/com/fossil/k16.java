package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hx5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k16 extends pv5 implements j16 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<b95> g;
    @DexIgnore
    public i16 h;
    @DexIgnore
    public hx5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return k16.k;
        }

        @DexIgnore
        public final k16 b() {
            return new k16();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements hx5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k16 f1853a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(k16 k16) {
            this.f1853a = k16;
        }

        @DexIgnore
        @Override // com.fossil.hx5.a
        public void a() {
            k16.L6(this.f1853a).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k16 b;

        @DexIgnore
        public c(k16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public final void onClick(View view) {
            k16.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ k16 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(k16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            hx5 hx5 = this.b.i;
            if (hx5 != null) {
                hx5.v(String.valueOf(charSequence));
            }
            this.b.P6(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ b95 b;

        @DexIgnore
        public e(b95 b95) {
            this.b = b95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                b95 b95 = this.b;
                pq7.b(b95, "binding");
                b95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k16 b;

        @DexIgnore
        public f(k16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public final void onClick(View view) {
            k16.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b95 b;

        @DexIgnore
        public g(b95 b95) {
            this.b = b95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.r.setText("");
        }
    }

    /*
    static {
        String simpleName = k16.class.getSimpleName();
        pq7.b(simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ i16 L6(k16 k16) {
        i16 i16 = k16.h;
        if (i16 != null) {
            return i16;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        i16 i16 = this.h;
        if (i16 != null) {
            i16.n();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: O6 */
    public void M5(i16 i16) {
        pq7.c(i16, "presenter");
        this.h = i16;
    }

    @DexIgnore
    public final void P6(boolean z) {
        g37<b95> g37 = this.g;
        if (g37 != null) {
            b95 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.t;
                pq7.b(imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.t;
            pq7.b(imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j16
    public void T(Cursor cursor) {
        hx5 hx5 = this.i;
        if (hx5 != null) {
            hx5.l(cursor);
        }
    }

    @DexIgnore
    @Override // com.fossil.j16
    public void V() {
    }

    @DexIgnore
    @Override // com.fossil.j16
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.j16
    public void f6(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        g37<b95> g37 = this.g;
        if (g37 != null) {
            b95 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                flexibleButton2.setEnabled(z);
            }
            g37<b95> g372 = this.g;
            if (g372 != null) {
                b95 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.q) != null) {
                    flexibleButton.setBackgroundColor(z ? gl0.d(PortfolioApp.h0.c(), 2131099967) : gl0.d(PortfolioApp.h0.c(), 2131099827));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j16
    public void j5(List<j06> list, FilterQueryProvider filterQueryProvider) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        pq7.c(list, "listContactWrapper");
        pq7.c(filterQueryProvider, "filterQueryProvider");
        hx5 hx5 = new hx5(null, list);
        this.i = hx5;
        if (hx5 != null) {
            hx5.y(new b(this));
            hx5 hx52 = this.i;
            if (hx52 != null) {
                hx52.k(filterQueryProvider);
                g37<b95> g37 = this.g;
                if (g37 != null) {
                    b95 a2 = g37.a();
                    if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.i);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        b95 b95 = (b95) aq0.f(layoutInflater, 2131558590, viewGroup, false, A6());
        b95.u.setOnClickListener(new c(this));
        b95.r.addTextChangedListener(new d(this));
        b95.r.setOnFocusChangeListener(new e(b95));
        b95.q.setOnClickListener(new f(this));
        b95.t.setOnClickListener(new g(b95));
        b95.w.setIndexBarVisibility(true);
        b95.w.setIndexBarHighLateTextVisibility(true);
        b95.w.setIndexbarHighLateTextColor(2131099703);
        b95.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        b95.w.setIndexTextSize(9);
        b95.w.setIndexBarTextColor(2131099703);
        Typeface b2 = nl0.b(requireContext(), 2131296256);
        String d2 = qn5.l.a().d("primaryText");
        Typeface f2 = qn5.l.a().f("nonBrandTextStyle5");
        if (f2 == null) {
            f2 = b2;
        }
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            b95.w.setIndexBarTextColor(parseColor);
            b95.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (f2 != null) {
            b95.w.setTypeface(f2);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = b95.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext(), 1, false));
        this.g = new g37<>(this, b95);
        pq7.b(b95, "binding");
        return b95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        i16 i16 = this.h;
        if (i16 != null) {
            i16.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        i16 i16 = this.h;
        if (i16 != null) {
            i16.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
