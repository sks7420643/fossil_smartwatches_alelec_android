package com.fossil;

import android.os.SystemClock;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static String f3553a; // = "Volley";
    @DexIgnore
    public static boolean b; // = Log.isLoggable("Volley", 2);
    @DexIgnore
    public static /* final */ String c; // = u91.class.getName();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ boolean c; // = u91.b;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<C0250a> f3554a; // = new ArrayList();
        @DexIgnore
        public boolean b; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.u91$a$a")
        /* renamed from: com.fossil.u91$a$a  reason: collision with other inner class name */
        public static class C0250a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ String f3555a;
            @DexIgnore
            public /* final */ long b;
            @DexIgnore
            public /* final */ long c;

            @DexIgnore
            public C0250a(String str, long j, long j2) {
                this.f3555a = str;
                this.b = j;
                this.c = j2;
            }
        }

        @DexIgnore
        public void a(String str, long j) {
            synchronized (this) {
                if (!this.b) {
                    this.f3554a.add(new C0250a(str, j, SystemClock.elapsedRealtime()));
                } else {
                    throw new IllegalStateException("Marker added to finished log");
                }
            }
        }

        @DexIgnore
        public void b(String str) {
            synchronized (this) {
                this.b = true;
                long c2 = c();
                if (c2 > 0) {
                    long j = this.f3554a.get(0).c;
                    u91.b("(%-4d ms) %s", Long.valueOf(c2), str);
                    long j2 = j;
                    for (C0250a aVar : this.f3554a) {
                        long j3 = aVar.c;
                        u91.b("(+%-4d) [%2d] %s", Long.valueOf(j3 - j2), Long.valueOf(aVar.b), aVar.f3555a);
                        j2 = j3;
                    }
                }
            }
        }

        @DexIgnore
        public final long c() {
            if (this.f3554a.size() == 0) {
                return 0;
            }
            long j = this.f3554a.get(0).c;
            List<C0250a> list = this.f3554a;
            return list.get(list.size() - 1).c - j;
        }

        @DexIgnore
        public void finalize() throws Throwable {
            if (!this.b) {
                b("Request on the loose");
                u91.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }
    }

    @DexIgnore
    public static String a(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClassName().equals(c)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + CodelessMatcher.CURRENT_CLASS_NAME + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    @DexIgnore
    public static void b(String str, Object... objArr) {
        Log.d(f3553a, a(str, objArr));
    }

    @DexIgnore
    public static void c(String str, Object... objArr) {
        Log.e(f3553a, a(str, objArr));
    }

    @DexIgnore
    public static void d(Throwable th, String str, Object... objArr) {
        Log.e(f3553a, a(str, objArr), th);
    }

    @DexIgnore
    public static void e(String str, Object... objArr) {
        if (b) {
            Log.v(f3553a, a(str, objArr));
        }
    }

    @DexIgnore
    public static void f(String str, Object... objArr) {
        Log.wtf(f3553a, a(str, objArr));
    }
}
