package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gj2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gj2> CREATOR; // = new ij2();
    @DexIgnore
    public /* final */ uh2 b;
    @DexIgnore
    public /* final */ si2 c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public gj2(uh2 uh2, IBinder iBinder, long j, long j2) {
        this.b = uh2;
        this.c = ri2.e(iBinder);
        this.d = j;
        this.e = j2;
    }

    @DexIgnore
    public uh2 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof gj2)) {
            return false;
        }
        gj2 gj2 = (gj2) obj;
        return pc2.a(this.b, gj2.b) && this.d == gj2.d && this.e == gj2.e;
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(this.b, Long.valueOf(this.d), Long.valueOf(this.e));
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, c(), i, false);
        bd2.m(parcel, 2, this.c.asBinder(), false);
        bd2.r(parcel, 3, this.d);
        bd2.r(parcel, 4, this.e);
        bd2.b(parcel, a2);
    }
}
