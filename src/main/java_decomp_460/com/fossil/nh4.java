package com.fossil;

import android.os.Bundle;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ long f2523a; // = TimeUnit.MINUTES.toMillis(3);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static zi0<String, String> a(Bundle bundle) {
            zi0<String, String> zi0 = new zi0<>();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                        zi0.put(str, str2);
                    }
                }
            }
            return zi0;
        }
    }
}
