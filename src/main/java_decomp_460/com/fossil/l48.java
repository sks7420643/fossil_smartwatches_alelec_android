package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.internal.Utility;
import com.google.maps.internal.UrlSigner;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l48 implements Serializable, Comparable<l48> {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public static /* final */ l48 EMPTY; // = new l48(new byte[0]);
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public transient int b;
    @DexIgnore
    public transient String c;
    @DexIgnore
    public /* final */ byte[] data;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final l48 a(String str) {
            pq7.c(str, "$this$decodeBase64");
            byte[] a2 = d48.a(str);
            if (a2 != null) {
                return new l48(a2);
            }
            return null;
        }

        @DexIgnore
        public final l48 b(String str) {
            pq7.c(str, "$this$decodeHex");
            if (str.length() % 2 == 0) {
                int length = str.length() / 2;
                byte[] bArr = new byte[length];
                for (int i = 0; i < length; i++) {
                    int i2 = i * 2;
                    bArr[i] = (byte) ((byte) (f58.b(str.charAt(i2 + 1)) + (f58.b(str.charAt(i2)) << 4)));
                }
                return new l48(bArr);
            }
            throw new IllegalArgumentException(("Unexpected hex string: " + str).toString());
        }

        @DexIgnore
        public final l48 c(String str, Charset charset) {
            pq7.c(str, "$this$encode");
            pq7.c(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return new l48(bytes);
        }

        @DexIgnore
        public final l48 d(String str) {
            pq7.c(str, "$this$encodeUtf8");
            l48 l48 = new l48(e48.a(str));
            l48.setUtf8$okio(str);
            return l48;
        }

        @DexIgnore
        public final l48 e(ByteBuffer byteBuffer) {
            pq7.c(byteBuffer, "$this$toByteString");
            byte[] bArr = new byte[byteBuffer.remaining()];
            byteBuffer.get(bArr);
            return new l48(bArr);
        }

        @DexIgnore
        public final l48 f(byte... bArr) {
            pq7.c(bArr, "data");
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
            return new l48(copyOf);
        }

        @DexIgnore
        public final l48 g(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "$this$toByteString");
            f48.b((long) bArr.length, (long) i, (long) i2);
            return new l48(dm7.k(bArr, i, i2 + i));
        }

        @DexIgnore
        public final l48 h(InputStream inputStream, int i) throws IOException {
            int i2 = 0;
            pq7.c(inputStream, "$this$readByteString");
            if (i >= 0) {
                byte[] bArr = new byte[i];
                while (i2 < i) {
                    int read = inputStream.read(bArr, i2, i - i2);
                    if (read != -1) {
                        i2 += read;
                    } else {
                        throw new EOFException();
                    }
                }
                return new l48(bArr);
            }
            throw new IllegalArgumentException(("byteCount < 0: " + i).toString());
        }
    }

    @DexIgnore
    public l48(byte[] bArr) {
        pq7.c(bArr, "data");
        this.data = bArr;
    }

    @DexIgnore
    public static final l48 decodeBase64(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public static final l48 decodeHex(String str) {
        return Companion.b(str);
    }

    @DexIgnore
    public static final l48 encodeString(String str, Charset charset) {
        return Companion.c(str, charset);
    }

    @DexIgnore
    public static final l48 encodeUtf8(String str) {
        return Companion.d(str);
    }

    @DexIgnore
    public static /* synthetic */ int indexOf$default(l48 l48, l48 l482, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return l48.indexOf(l482, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    @DexIgnore
    public static /* synthetic */ int indexOf$default(l48 l48, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return l48.indexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    @DexIgnore
    public static /* synthetic */ int lastIndexOf$default(l48 l48, l48 l482, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = l48.size();
            }
            return l48.lastIndexOf(l482, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    @DexIgnore
    public static /* synthetic */ int lastIndexOf$default(l48 l48, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = l48.size();
            }
            return l48.lastIndexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    @DexIgnore
    public static final l48 of(ByteBuffer byteBuffer) {
        return Companion.e(byteBuffer);
    }

    @DexIgnore
    public static final l48 of(byte... bArr) {
        return Companion.f(bArr);
    }

    @DexIgnore
    public static final l48 of(byte[] bArr, int i, int i2) {
        return Companion.g(bArr, i, i2);
    }

    @DexIgnore
    public static final l48 read(InputStream inputStream, int i) throws IOException {
        return Companion.h(inputStream, i);
    }

    @DexIgnore
    private final void readObject(ObjectInputStream objectInputStream) throws IOException {
        l48 h = Companion.h(objectInputStream, objectInputStream.readInt());
        Field declaredField = l48.class.getDeclaredField("data");
        pq7.b(declaredField, "field");
        declaredField.setAccessible(true);
        declaredField.set(this, h.data);
    }

    @DexIgnore
    public static /* synthetic */ l48 substring$default(l48 l48, int i, int i2, int i3, Object obj) {
        if (obj == null) {
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = l48.size();
            }
            return l48.substring(i, i2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: substring");
    }

    @DexIgnore
    private final void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    @DexIgnore
    /* renamed from: -deprecated_getByte  reason: not valid java name */
    public final byte m12deprecated_getByte(int i) {
        return getByte(i);
    }

    @DexIgnore
    /* renamed from: -deprecated_size  reason: not valid java name */
    public final int m13deprecated_size() {
        return size();
    }

    @DexIgnore
    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(this.data).asReadOnlyBuffer();
        pq7.b(asReadOnlyBuffer, "ByteBuffer.wrap(data).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    @DexIgnore
    public String base64() {
        return d48.c(getData$okio(), null, 1, null);
    }

    @DexIgnore
    public String base64Url() {
        return d48.b(getData$okio(), d48.d());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0028 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002e A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compareTo(com.fossil.l48 r8) {
        /*
            r7 = this;
            r0 = 0
            java.lang.String r1 = "other"
            com.fossil.pq7.c(r8, r1)
            int r2 = r7.size()
            int r3 = r8.size()
            int r4 = java.lang.Math.min(r2, r3)
            r1 = r0
        L_0x0013:
            if (r1 >= r4) goto L_0x002a
            byte r5 = r7.getByte(r1)
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r6 = r8.getByte(r1)
            r6 = r6 & 255(0xff, float:3.57E-43)
            if (r5 != r6) goto L_0x0026
            int r1 = r1 + 1
            goto L_0x0013
        L_0x0026:
            if (r5 >= r6) goto L_0x002e
        L_0x0028:
            r0 = -1
        L_0x0029:
            return r0
        L_0x002a:
            if (r2 == r3) goto L_0x0029
            if (r2 < r3) goto L_0x0028
        L_0x002e:
            r0 = 1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l48.compareTo(com.fossil.l48):int");
    }

    @DexIgnore
    public l48 digest$okio(String str) {
        pq7.c(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        pq7.b(digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new l48(digest);
    }

    @DexIgnore
    public final boolean endsWith(l48 l48) {
        pq7.c(l48, "suffix");
        return rangeEquals(size() - l48.size(), l48, 0, l48.size());
    }

    @DexIgnore
    public final boolean endsWith(byte[] bArr) {
        pq7.c(bArr, "suffix");
        return rangeEquals(size() - bArr.length, bArr, 0, bArr.length);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof l48) {
            l48 l48 = (l48) obj;
            if (l48.size() == getData$okio().length && l48.rangeEquals(0, getData$okio(), 0, getData$okio().length)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final byte getByte(int i) {
        return internalGet$okio(i);
    }

    @DexIgnore
    public final byte[] getData$okio() {
        return this.data;
    }

    @DexIgnore
    public final int getHashCode$okio() {
        return this.b;
    }

    @DexIgnore
    public int getSize$okio() {
        return getData$okio().length;
    }

    @DexIgnore
    public final String getUtf8$okio() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio != 0) {
            return hashCode$okio;
        }
        int hashCode = Arrays.hashCode(getData$okio());
        setHashCode$okio(hashCode);
        return hashCode;
    }

    @DexIgnore
    public String hex() {
        char[] cArr = new char[(getData$okio().length * 2)];
        byte[] data$okio = getData$okio();
        int i = 0;
        for (byte b2 : data$okio) {
            int i2 = i + 1;
            cArr[i] = (char) f58.f()[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = (char) f58.f()[b2 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new String(cArr);
    }

    @DexIgnore
    public l48 hmac$okio(String str, l48 l48) {
        pq7.c(str, "algorithm");
        pq7.c(l48, "key");
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(l48.toByteArray(), str));
            byte[] doFinal = instance.doFinal(this.data);
            pq7.b(doFinal, "mac.doFinal(data)");
            return new l48(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public l48 hmacSha1(l48 l48) {
        pq7.c(l48, "key");
        return hmac$okio(UrlSigner.ALGORITHM_HMAC_SHA1, l48);
    }

    @DexIgnore
    public l48 hmacSha256(l48 l48) {
        pq7.c(l48, "key");
        return hmac$okio("HmacSHA256", l48);
    }

    @DexIgnore
    public l48 hmacSha512(l48 l48) {
        pq7.c(l48, "key");
        return hmac$okio("HmacSHA512", l48);
    }

    @DexIgnore
    public final int indexOf(l48 l48) {
        return indexOf$default(this, l48, 0, 2, (Object) null);
    }

    @DexIgnore
    public final int indexOf(l48 l48, int i) {
        pq7.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return indexOf(l48.internalArray$okio(), i);
    }

    @DexIgnore
    public int indexOf(byte[] bArr) {
        return indexOf$default(this, bArr, 0, 2, (Object) null);
    }

    @DexIgnore
    public int indexOf(byte[] bArr, int i) {
        pq7.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        int length = getData$okio().length - bArr.length;
        int max = Math.max(i, 0);
        if (max <= length) {
            while (!f48.a(getData$okio(), max, bArr, 0, bArr.length)) {
                if (max != length) {
                    max++;
                }
            }
            return max;
        }
        return -1;
    }

    @DexIgnore
    public byte[] internalArray$okio() {
        return getData$okio();
    }

    @DexIgnore
    public byte internalGet$okio(int i) {
        return getData$okio()[i];
    }

    @DexIgnore
    public final int lastIndexOf(l48 l48) {
        return lastIndexOf$default(this, l48, 0, 2, (Object) null);
    }

    @DexIgnore
    public final int lastIndexOf(l48 l48, int i) {
        pq7.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return lastIndexOf(l48.internalArray$okio(), i);
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr) {
        return lastIndexOf$default(this, bArr, 0, 2, (Object) null);
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr, int i) {
        pq7.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        for (int min = Math.min(i, getData$okio().length - bArr.length); min >= 0; min--) {
            if (f48.a(getData$okio(), min, bArr, 0, bArr.length)) {
                return min;
            }
        }
        return -1;
    }

    @DexIgnore
    public l48 md5() {
        return digest$okio(Utility.HASH_ALGORITHM_MD5);
    }

    @DexIgnore
    public boolean rangeEquals(int i, l48 l48, int i2, int i3) {
        pq7.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return l48.rangeEquals(i2, getData$okio(), i, i3);
    }

    @DexIgnore
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        pq7.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return i >= 0 && i <= getData$okio().length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && f48.a(getData$okio(), i, bArr, i2, i3);
    }

    @DexIgnore
    public final void setHashCode$okio(int i) {
        this.b = i;
    }

    @DexIgnore
    public final void setUtf8$okio(String str) {
        this.c = str;
    }

    @DexIgnore
    public l48 sha1() {
        return digest$okio(Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public l48 sha256() {
        return digest$okio("SHA-256");
    }

    @DexIgnore
    public l48 sha512() {
        return digest$okio("SHA-512");
    }

    @DexIgnore
    public final int size() {
        return getSize$okio();
    }

    @DexIgnore
    public final boolean startsWith(l48 l48) {
        pq7.c(l48, "prefix");
        return rangeEquals(0, l48, 0, l48.size());
    }

    @DexIgnore
    public final boolean startsWith(byte[] bArr) {
        pq7.c(bArr, "prefix");
        return rangeEquals(0, bArr, 0, bArr.length);
    }

    @DexIgnore
    public String string(Charset charset) {
        pq7.c(charset, "charset");
        return new String(this.data, charset);
    }

    @DexIgnore
    public l48 substring() {
        return substring$default(this, 0, 0, 3, null);
    }

    @DexIgnore
    public l48 substring(int i) {
        return substring$default(this, i, 0, 2, null);
    }

    @DexIgnore
    public l48 substring(int i, int i2) {
        boolean z = true;
        if (i >= 0) {
            if (i2 <= getData$okio().length) {
                if (i2 - i < 0) {
                    z = false;
                }
                if (z) {
                    return (i == 0 && i2 == getData$okio().length) ? this : new l48(dm7.k(getData$okio(), i, i2));
                }
                throw new IllegalArgumentException("endIndex < beginIndex".toString());
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
        throw new IllegalArgumentException("beginIndex < 0".toString());
    }

    @DexIgnore
    public l48 toAsciiLowercase() {
        byte b2;
        for (int i = 0; i < getData$okio().length; i++) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 65;
            if (b3 >= b4 && b3 <= (b2 = (byte) 90)) {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) ((byte) (b3 + 32));
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) ((byte) (b5 + 32));
                    }
                }
                return new l48(copyOf);
            }
        }
        return this;
    }

    @DexIgnore
    public l48 toAsciiUppercase() {
        byte b2;
        for (int i = 0; i < getData$okio().length; i++) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 97;
            if (b3 >= b4 && b3 <= (b2 = (byte) 122)) {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) ((byte) (b3 - 32));
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) ((byte) (b5 - 32));
                    }
                }
                return new l48(copyOf);
            }
        }
        return this;
    }

    @DexIgnore
    public byte[] toByteArray() {
        byte[] data$okio = getData$okio();
        byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return copyOf;
    }

    @DexIgnore
    public String toString() {
        boolean z = true;
        if (getData$okio().length == 0) {
            return "[size=0]";
        }
        int a2 = f58.a(getData$okio(), 64);
        if (a2 != -1) {
            String utf8 = utf8();
            if (utf8 != null) {
                String substring = utf8.substring(0, a2);
                pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                String q = vt7.q(vt7.q(vt7.q(substring, "\\", "\\\\", false, 4, null), "\n", "\\n", false, 4, null), "\r", "\\r", false, 4, null);
                if (a2 < utf8.length()) {
                    return "[size=" + getData$okio().length + " text=" + q + "\u2026]";
                }
                return "[text=" + q + ']';
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        } else if (getData$okio().length <= 64) {
            return "[hex=" + hex() + ']';
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[size=");
            sb.append(getData$okio().length);
            sb.append(" hex=");
            if (64 > getData$okio().length) {
                z = false;
            }
            if (z) {
                if (64 != getData$okio().length) {
                    this = new l48(dm7.k(getData$okio(), 0, 64));
                }
                sb.append(this.hex());
                sb.append("\u2026]");
                return sb.toString();
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
    }

    @DexIgnore
    public String utf8() {
        String utf8$okio = getUtf8$okio();
        if (utf8$okio != null) {
            return utf8$okio;
        }
        String b2 = e48.b(internalArray$okio());
        setUtf8$okio(b2);
        return b2;
    }

    @DexIgnore
    public void write(OutputStream outputStream) throws IOException {
        pq7.c(outputStream, "out");
        outputStream.write(this.data);
    }

    @DexIgnore
    public void write$okio(i48 i48, int i, int i2) {
        pq7.c(i48, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        f58.d(this, i48, i, i2);
    }
}
