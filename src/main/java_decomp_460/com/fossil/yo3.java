package com.fossil;

import android.content.Context;
import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo3 {
    @DexIgnore
    public static String a(Context context, String str) {
        try {
            return new xc2(context).a(str);
        } catch (Resources.NotFoundException e) {
            return null;
        }
    }

    @DexIgnore
    public static String b(String str, String[] strArr, String[] strArr2) {
        rc2.k(strArr);
        rc2.k(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            String str2 = strArr[i];
            if ((str == null && str2 == null) ? true : str == null ? false : str.equals(str2)) {
                return strArr2[i];
            }
        }
        return null;
    }
}
