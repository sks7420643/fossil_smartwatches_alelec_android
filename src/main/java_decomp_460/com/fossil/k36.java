package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k36 implements Factory<j36> {
    @DexIgnore
    public static j36 a(h36 h36, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new j36(h36, remindersSettingsDatabase);
    }
}
