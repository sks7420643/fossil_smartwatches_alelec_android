package com.fossil;

import android.os.RemoteException;
import com.fossil.m62;
import com.fossil.m62.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w72<A extends m62.b, ResultT> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b62[] f3892a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, ot3<ResultT> ot3) throws RemoteException;

    @DexIgnore
    public boolean b() {
        return this.b;
    }

    @DexIgnore
    public final b62[] c() {
        return this.f3892a;
    }
}
