package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fq4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ dv7 f1178a; // = bw7.b();
    @DexIgnore
    public /* final */ dv7 b; // = bw7.a();
    @DexIgnore
    public /* final */ jx7 c; // = bw7.c();
    @DexIgnore
    public /* final */ iv7 d; // = jv7.b();

    @DexIgnore
    public final dv7 h() {
        return this.b;
    }

    @DexIgnore
    public final dv7 i() {
        return this.f1178a;
    }

    @DexIgnore
    public final jx7 j() {
        return this.c;
    }

    @DexIgnore
    public final iv7 k() {
        return this.d;
    }

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public void m() {
    }
}
