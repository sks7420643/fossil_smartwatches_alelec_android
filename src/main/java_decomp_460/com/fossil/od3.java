package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od3 extends as2 implements zb3 {
    @DexIgnore
    public od3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void B(vd3 vd3) throws RemoteException {
        Parcel d = d();
        es2.c(d, vd3);
        i(96, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void D2(rc3 rc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, rc3);
        i(30, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final fc3 F1() throws RemoteException {
        fc3 hd3;
        Parcel e = e(25, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            hd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            hd3 = queryLocalInterface instanceof fc3 ? (fc3) queryLocalInterface : new hd3(readStrongBinder);
        }
        e.recycle();
        return hd3;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void H(jc3 jc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, jc3);
        i(28, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void H2(nc3 nc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, nc3);
        i(29, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void I0(dd3 dd3, rg2 rg2) throws RemoteException {
        Parcel d = d();
        es2.c(d, dd3);
        es2.c(d, rg2);
        i(38, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void J(LatLngBounds latLngBounds) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLngBounds);
        i(95, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void J0(hc3 hc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, hc3);
        i(32, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final boolean N1() throws RemoteException {
        Parcel e = e(40, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final ls2 N2(le3 le3) throws RemoteException {
        Parcel d = d();
        es2.d(d, le3);
        Parcel e = e(11, d);
        ls2 e2 = ms2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final is2 Q(ee3 ee3) throws RemoteException {
        Parcel d = d();
        es2.d(d, ee3);
        Parcel e = e(35, d);
        is2 e2 = js2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void R(yc3 yc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, yc3);
        i(87, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final boolean R0() throws RemoteException {
        Parcel e = e(17, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void R2(tc3 tc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, tc3);
        i(31, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void S2(wc3 wc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, wc3);
        i(85, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void U0(rg2 rg2, kd3 kd3) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        es2.c(d, kd3);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void X(lc3 lc3) throws RemoteException {
        Parcel d = d();
        es2.c(d, lc3);
        i(42, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void Y0(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(93, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void a0(int i, int i2, int i3, int i4) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        d.writeInt(i2);
        d.writeInt(i3);
        d.writeInt(i4);
        i(39, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void a2(td3 td3) throws RemoteException {
        Parcel d = d();
        es2.c(d, td3);
        i(97, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final cc3 b2() throws RemoteException {
        cc3 cd3;
        Parcel e = e(26, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            cd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IProjectionDelegate");
            cd3 = queryLocalInterface instanceof cc3 ? (cc3) queryLocalInterface : new cd3(readStrongBinder);
        }
        e.recycle();
        return cd3;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void clear() throws RemoteException {
        i(14, d());
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void e1(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(92, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void h2(xd3 xd3) throws RemoteException {
        Parcel d = d();
        es2.c(d, xd3);
        i(89, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void j2(rg2 rg2) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void o0(rg2 rg2) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final CameraPosition p0() throws RemoteException {
        Parcel e = e(1, d());
        CameraPosition cameraPosition = (CameraPosition) es2.b(e, CameraPosition.CREATOR);
        e.recycle();
        return cameraPosition;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void setBuildingsEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(41, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final boolean setIndoorEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        Parcel e = e(20, d);
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void setMapType(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(16, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void setMyLocationEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(22, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void setTrafficEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void u1() throws RemoteException {
        i(94, d());
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final boolean v0(je3 je3) throws RemoteException {
        Parcel d = d();
        es2.d(d, je3);
        Parcel e = e(91, d);
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final os2 w1(oe3 oe3) throws RemoteException {
        Parcel d = d();
        es2.d(d, oe3);
        Parcel e = e(10, d);
        os2 e2 = ps2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final void x1(rd3 rd3) throws RemoteException {
        Parcel d = d();
        es2.c(d, rd3);
        i(99, d);
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final rs2 x2(qe3 qe3) throws RemoteException {
        Parcel d = d();
        es2.d(d, qe3);
        Parcel e = e(9, d);
        rs2 e2 = bs2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final float z() throws RemoteException {
        Parcel e = e(3, d());
        float readFloat = e.readFloat();
        e.recycle();
        return readFloat;
    }

    @DexIgnore
    @Override // com.fossil.zb3
    public final float z2() throws RemoteException {
        Parcel e = e(2, d());
        float readFloat = e.readFloat();
        e.recycle();
        return readFloat;
    }
}
