package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl0 {
    @DexIgnore
    public static Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, om0 om0) {
        Object obj;
        if (Build.VERSION.SDK_INT >= 16) {
            if (om0 != null) {
                try {
                    obj = om0.b();
                } catch (Exception e) {
                    if (e instanceof OperationCanceledException) {
                        throw new vm0();
                    }
                    throw e;
                }
            } else {
                obj = null;
            }
            return contentResolver.query(uri, strArr, str, strArr2, str2, (CancellationSignal) obj);
        }
        if (om0 != null) {
            om0.e();
        }
        return contentResolver.query(uri, strArr, str, strArr2, str2);
    }
}
