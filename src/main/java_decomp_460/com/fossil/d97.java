package com.fossil;

import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d97 {
    @DexIgnore
    public static final f97 a(g97 g97) {
        pq7.c(g97, "$this$toPhotoDraft");
        byte[] b = j37.b(new File(g97.e()));
        pq7.b(b, "File(localPath).toBinary()");
        return new f97(g97.d(), j37.a(b));
    }

    @DexIgnore
    public static final List<f97> b(List<g97> list) {
        pq7.c(list, "$this$toPhotoDrafts");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(a(it.next()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final fb7 c(g97 g97) {
        pq7.c(g97, "$this$toUIBackgroundPhoto");
        int dimensionPixelSize = PortfolioApp.h0.c().getResources().getDimensionPixelSize(2131165379);
        return new fb7(g97.d(), g97.d(), g97.c(), g97.c(), l77.BACKGROUND_PHOTO, g97.a(), qk5.f2994a.d(g97.e(), dimensionPixelSize, dimensionPixelSize, FileType.WATCH_FACE), null, g97.e());
    }

    @DexIgnore
    public static final List<fb7> d(List<g97> list) {
        pq7.c(list, "$this$toUIBackgroundPhotos");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(c(it.next()));
        }
        return arrayList;
    }
}
