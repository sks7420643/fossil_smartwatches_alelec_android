package com.fossil;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo4 implements Factory<Context> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4505a;

    @DexIgnore
    public zo4(uo4 uo4) {
        this.f4505a = uo4;
    }

    @DexIgnore
    public static zo4 a(uo4 uo4) {
        return new zo4(uo4);
    }

    @DexIgnore
    public static Context c(uo4 uo4) {
        Context h = uo4.h();
        lk7.c(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    /* renamed from: b */
    public Context get() {
        return c(this.f4505a);
    }
}
