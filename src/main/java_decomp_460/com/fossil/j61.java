package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j61 implements e61<Uri> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1718a;
    @DexIgnore
    public /* final */ v51 b;

    @DexIgnore
    public j61(Context context, v51 v51) {
        pq7.c(context, "context");
        pq7.c(v51, "drawableDecoder");
        this.f1718a = context;
        this.b = v51;
    }

    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, Uri uri, f81 f81, x51 x51, qn7<? super d61> qn7) {
        Integer c;
        Drawable c2;
        String authority = uri.getAuthority();
        if (authority != null) {
            pq7.b(authority, "it");
            String str = ao7.a(vt7.l(authority) ^ true).booleanValue() ? authority : null;
            if (str != null) {
                pq7.b(str, "data.authority?.takeIf {\u2026InvalidUriException(data)");
                List<String> pathSegments = uri.getPathSegments();
                pq7.b(pathSegments, "data.pathSegments");
                String str2 = (String) pm7.R(pathSegments);
                if (str2 == null || (c = ut7.c(str2)) == null) {
                    g(uri);
                    throw null;
                }
                int intValue = c.intValue();
                Resources resourcesForApplication = this.f1718a.getPackageManager().getResourcesForApplication(str);
                TypedValue typedValue = new TypedValue();
                resourcesForApplication.getValue(intValue, typedValue, true);
                CharSequence charSequence = typedValue.string;
                pq7.b(charSequence, "path");
                String obj = charSequence.subSequence(wt7.K(charSequence, '/', 0, false, 6, null), charSequence.length()).toString();
                MimeTypeMap singleton = MimeTypeMap.getSingleton();
                pq7.b(singleton, "MimeTypeMap.getSingleton()");
                String g = w81.g(singleton, obj);
                if (pq7.a(g, "text/xml")) {
                    if (pq7.a(str, this.f1718a.getPackageName())) {
                        c2 = t81.a(this.f1718a, intValue);
                    } else {
                        Context context = this.f1718a;
                        pq7.b(resourcesForApplication, "resources");
                        c2 = t81.c(context, resourcesForApplication, intValue);
                    }
                    boolean o = w81.o(c2);
                    if (o) {
                        Bitmap a2 = this.b.a(c2, f81, x51.d());
                        Resources resources = this.f1718a.getResources();
                        pq7.b(resources, "context.resources");
                        c2 = new BitmapDrawable(resources, a2);
                    }
                    return new c61(c2, o, q51.MEMORY);
                }
                InputStream openRawResource = resourcesForApplication.openRawResource(intValue);
                pq7.b(openRawResource, "resources.openRawResource(resId)");
                return new k61(s48.d(s48.l(openRawResource)), g, q51.MEMORY);
            }
        }
        g(uri);
        throw null;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        return pq7.a(uri.getScheme(), "android.resource");
    }

    @DexIgnore
    /* renamed from: f */
    public String b(Uri uri) {
        pq7.c(uri, "data");
        StringBuilder sb = new StringBuilder();
        sb.append(uri);
        sb.append('-');
        Resources resources = this.f1718a.getResources();
        pq7.b(resources, "context.resources");
        Configuration configuration = resources.getConfiguration();
        pq7.b(configuration, "context.resources.configuration");
        sb.append(w81.h(configuration));
        return sb.toString();
    }

    @DexIgnore
    public final Void g(Uri uri) {
        throw new IllegalStateException("Invalid android.resource URI: " + uri);
    }
}
