package com.fossil;

import com.fossil.kk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd1<Z> implements id1<Z>, kk1.f {
    @DexIgnore
    public static /* final */ mn0<hd1<?>> f; // = kk1.d(20, new a());
    @DexIgnore
    public /* final */ mk1 b; // = mk1.a();
    @DexIgnore
    public id1<Z> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements kk1.d<hd1<?>> {
        @DexIgnore
        /* renamed from: a */
        public hd1<?> create() {
            return new hd1<>();
        }
    }

    @DexIgnore
    public static <Z> hd1<Z> e(id1<Z> id1) {
        hd1<?> b2 = f.b();
        ik1.d(b2);
        hd1<Z> hd1 = (hd1<Z>) b2;
        hd1.a(id1);
        return hd1;
    }

    @DexIgnore
    public final void a(id1<Z> id1) {
        this.e = false;
        this.d = true;
        this.c = id1;
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
        synchronized (this) {
            this.b.c();
            this.e = true;
            if (!this.d) {
                this.c.b();
                g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return this.c.c();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<Z> d() {
        return this.c.d();
    }

    @DexIgnore
    @Override // com.fossil.kk1.f
    public mk1 f() {
        return this.b;
    }

    @DexIgnore
    public final void g() {
        this.c = null;
        f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Z get() {
        return this.c.get();
    }

    @DexIgnore
    public void h() {
        synchronized (this) {
            this.b.c();
            if (this.d) {
                this.d = false;
                if (this.e) {
                    b();
                }
            } else {
                throw new IllegalStateException("Already unlocked");
            }
        }
    }
}
