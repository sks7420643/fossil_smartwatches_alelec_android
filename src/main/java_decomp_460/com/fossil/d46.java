package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d46 extends u47 {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ zp0 u; // = new sr4(this);
    @DexIgnore
    public g37<t85> v;
    @DexIgnore
    public b w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return d46.y;
        }

        @DexIgnore
        public final d46 b() {
            return new d46();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d46 b;

        @DexIgnore
        public c(d46 d46) {
            this.b = d46;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                d46 d46 = this.b;
                pq7.b(dialog, "it");
                d46.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d46 b;
        @DexIgnore
        public /* final */ /* synthetic */ t85 c;

        @DexIgnore
        public d(d46 d46, t85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.v;
            pq7.b(rTLImageView, "binding.ivCallsMessageCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.v;
                pq7.b(rTLImageView2, "binding.ivCallsMessageCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.u;
                pq7.b(rTLImageView3, "binding.ivCallsCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.w;
                pq7.b(rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.b.m = true;
                this.b.l = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d46 b;
        @DexIgnore
        public /* final */ /* synthetic */ t85 c;

        @DexIgnore
        public e(d46 d46, t85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.u;
            pq7.b(rTLImageView, "binding.ivCallsCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.u;
                pq7.b(rTLImageView2, "binding.ivCallsCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.v;
                pq7.b(rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.w;
                pq7.b(rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.b.l = true;
                this.b.m = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d46 b;
        @DexIgnore
        public /* final */ /* synthetic */ t85 c;

        @DexIgnore
        public f(d46 d46, t85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.w;
            pq7.b(rTLImageView, "binding.ivMessagesCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.w;
                pq7.b(rTLImageView2, "binding.ivMessagesCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.v;
                pq7.b(rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.u;
                pq7.b(rTLImageView4, "binding.ivCallsCheck");
                rTLImageView4.setVisibility(4);
                this.b.m = true;
                this.b.l = false;
            }
        }
    }

    /*
    static {
        String simpleName = d46.class.getSimpleName();
        pq7.b(simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public final void D6(b bVar) {
        pq7.c(bVar, "onDismissListener");
        this.w = bVar;
    }

    @DexIgnore
    public final void E6(int i, boolean z2, boolean z3) {
        this.k = i;
        this.l = z2;
        this.m = z3;
        this.s = z2;
        this.t = z3;
    }

    @DexIgnore
    public final void F6() {
        if (this.m && this.l) {
            g37<t85> g37 = this.v;
            if (g37 != null) {
                t85 a2 = g37.a();
                if (a2 != null) {
                    RTLImageView rTLImageView = a2.v;
                    pq7.b(rTLImageView, "it.ivCallsMessageCheck");
                    rTLImageView.setVisibility(0);
                    RTLImageView rTLImageView2 = a2.u;
                    pq7.b(rTLImageView2, "it.ivCallsCheck");
                    rTLImageView2.setVisibility(4);
                    RTLImageView rTLImageView3 = a2.w;
                    pq7.b(rTLImageView3, "it.ivMessagesCheck");
                    rTLImageView3.setVisibility(4);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else if (this.l) {
            g37<t85> g372 = this.v;
            if (g372 != null) {
                t85 a3 = g372.a();
                if (a3 != null) {
                    RTLImageView rTLImageView4 = a3.v;
                    pq7.b(rTLImageView4, "it.ivCallsMessageCheck");
                    rTLImageView4.setVisibility(4);
                    RTLImageView rTLImageView5 = a3.u;
                    pq7.b(rTLImageView5, "it.ivCallsCheck");
                    rTLImageView5.setVisibility(0);
                    RTLImageView rTLImageView6 = a3.w;
                    pq7.b(rTLImageView6, "it.ivMessagesCheck");
                    rTLImageView6.setVisibility(4);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else {
            g37<t85> g373 = this.v;
            if (g373 != null) {
                t85 a4 = g373.a();
                if (a4 != null) {
                    RTLImageView rTLImageView7 = a4.v;
                    pq7.b(rTLImageView7, "it.ivCallsMessageCheck");
                    rTLImageView7.setVisibility(4);
                    RTLImageView rTLImageView8 = a4.u;
                    pq7.b(rTLImageView8, "it.ivCallsCheck");
                    rTLImageView8.setVisibility(4);
                    RTLImageView rTLImageView9 = a4.w;
                    pq7.b(rTLImageView9, "it.ivMessagesCheck");
                    rTLImageView9.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        t85 t85 = (t85) aq0.f(layoutInflater, 2131558587, viewGroup, false, this.u);
        t85.x.setOnClickListener(new c(this));
        this.v = new g37<>(this, t85);
        F6();
        t85.r.setOnClickListener(new d(this, t85));
        t85.q.setOnClickListener(new e(this, t85));
        t85.s.setOnClickListener(new f(this, t85));
        pq7.b(t85, "binding");
        return t85.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.u47, com.fossil.kq0
    public void onDismiss(DialogInterface dialogInterface) {
        b bVar;
        pq7.c(dialogInterface, "dialog");
        if (!((this.s == this.l && this.t == this.m) || (bVar = this.w) == null)) {
            bVar.a(this.k, this.l, this.m);
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        F6();
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
