package com.fossil;

import com.fossil.yg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ng4 extends yg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2516a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yg4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2517a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.yg4.a
        public yg4 a() {
            String str = "";
            if (this.f2517a == null) {
                str = " token";
            }
            if (this.b == null) {
                str = str + " tokenExpirationTimestamp";
            }
            if (this.c == null) {
                str = str + " tokenCreationTimestamp";
            }
            if (str.isEmpty()) {
                return new ng4(this.f2517a, this.b.longValue(), this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.yg4.a
        public yg4.a b(String str) {
            if (str != null) {
                this.f2517a = str;
                return this;
            }
            throw new NullPointerException("Null token");
        }

        @DexIgnore
        @Override // com.fossil.yg4.a
        public yg4.a c(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.yg4.a
        public yg4.a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public ng4(String str, long j, long j2) {
        this.f2516a = str;
        this.b = j;
        this.c = j2;
    }

    @DexIgnore
    @Override // com.fossil.yg4
    public String b() {
        return this.f2516a;
    }

    @DexIgnore
    @Override // com.fossil.yg4
    public long c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.yg4
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yg4)) {
            return false;
        }
        yg4 yg4 = (yg4) obj;
        return this.f2516a.equals(yg4.b()) && this.b == yg4.d() && this.c == yg4.c();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f2516a.hashCode();
        long j = this.b;
        int i = (int) (j ^ (j >>> 32));
        long j2 = this.c;
        return ((((hashCode ^ 1000003) * 1000003) ^ i) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "InstallationTokenResult{token=" + this.f2516a + ", tokenExpirationTimestamp=" + this.b + ", tokenCreationTimestamp=" + this.c + "}";
    }
}
