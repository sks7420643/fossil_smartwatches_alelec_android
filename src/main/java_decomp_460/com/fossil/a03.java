package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a03 implements c03 {
    @DexIgnore
    public a03() {
    }

    @DexIgnore
    public /* synthetic */ a03(wz2 wz2) {
        this();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
