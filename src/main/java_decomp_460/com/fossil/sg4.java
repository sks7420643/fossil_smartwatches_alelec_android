package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class sg4 implements Runnable {
    @DexIgnore
    public /* final */ tg4 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public sg4(tg4 tg4, boolean z) {
        this.b = tg4;
        this.c = z;
    }

    @DexIgnore
    public static Runnable a(tg4 tg4, boolean z) {
        return new sg4(tg4, z);
    }

    @DexIgnore
    public void run() {
        this.b.g(this.c);
    }
}
