package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.a94;
import com.fossil.bc4;
import com.fossil.ec4;
import com.fossil.v94;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u84 {
    @DexIgnore
    public static /* final */ Comparator<File> A; // = new p();
    @DexIgnore
    public static /* final */ Comparator<File> B; // = new q();
    @DexIgnore
    public static /* final */ Pattern C; // = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    @DexIgnore
    public static /* final */ Map<String, String> D; // = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    @DexIgnore
    public static /* final */ String[] E; // = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    @DexIgnore
    public static /* final */ FilenameFilter x; // = new j("BeginSession");
    @DexIgnore
    public static /* final */ FilenameFilter y; // = t84.a();
    @DexIgnore
    public static /* final */ FilenameFilter z; // = new o();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3525a;
    @DexIgnore
    public /* final */ c94 b;
    @DexIgnore
    public /* final */ x84 c;
    @DexIgnore
    public /* final */ s94 d;
    @DexIgnore
    public /* final */ s84 e;
    @DexIgnore
    public /* final */ kb4 f;
    @DexIgnore
    public /* final */ h94 g;
    @DexIgnore
    public /* final */ tb4 h;
    @DexIgnore
    public /* final */ l84 i;
    @DexIgnore
    public /* final */ bc4.b j;
    @DexIgnore
    public /* final */ a0 k;
    @DexIgnore
    public /* final */ v94 l;
    @DexIgnore
    public /* final */ ac4 m;
    @DexIgnore
    public /* final */ bc4.a n;
    @DexIgnore
    public /* final */ w74 o;
    @DexIgnore
    public /* final */ kd4 p;
    @DexIgnore
    public /* final */ String q;
    @DexIgnore
    public /* final */ b84 r;
    @DexIgnore
    public /* final */ q94 s;
    @DexIgnore
    public a94 t;
    @DexIgnore
    public ot3<Boolean> u; // = new ot3<>();
    @DexIgnore
    public ot3<Boolean> v; // = new ot3<>();
    @DexIgnore
    public ot3<Void> w; // = new ot3<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ long f3526a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(long j, String str) {
            this.f3526a = j;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            if (u84.this.f0()) {
                return null;
            }
            u84.this.l.i(this.f3526a, this.b);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 implements v94.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ tb4 f3527a;

        @DexIgnore
        public a0(tb4 tb4) {
            this.f3527a = tb4;
        }

        @DexIgnore
        @Override // com.fossil.v94.b
        public File a() {
            File file = new File(this.f3527a.b(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s94 f3528a;

        @DexIgnore
        public b(s94 s94) {
            this.f3528a = s94;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            u84.this.s.l();
            new k94(u84.this.W()).i(u84.this.T(), this.f3528a);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements bc4.c {
        @DexIgnore
        public b0() {
        }

        @DexIgnore
        public /* synthetic */ b0(u84 u84, j jVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.bc4.c
        public File[] a() {
            return u84.this.l0();
        }

        @DexIgnore
        @Override // com.fossil.bc4.c
        public File[] b() {
            return u84.this.i0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Map f3530a;

        @DexIgnore
        public c(Map map) {
            this.f3530a = map;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            new k94(u84.this.W()).h(u84.this.T(), this.f3530a);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements bc4.a {
        @DexIgnore
        public c0() {
        }

        @DexIgnore
        public /* synthetic */ c0(u84 u84, j jVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.bc4.a
        public boolean a() {
            return u84.this.f0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Void> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            u84.this.L();
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0 implements Runnable {
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ ec4 c;
        @DexIgnore
        public /* final */ bc4 d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public d0(Context context, ec4 ec4, bc4 bc4, boolean z) {
            this.b = context;
            this.c = ec4;
            this.d = bc4;
            this.e = z;
        }

        @DexIgnore
        public void run() {
            if (r84.c(this.b)) {
                x74.f().b("Attempting to send crash report at time of crash...");
                this.d.d(this.c, this.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            u84 u84 = u84.this;
            u84.I(u84.k0(new z()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e0 implements FilenameFilter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3533a;

        @DexIgnore
        public e0(String str) {
            this.f3533a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f3533a);
            sb.append(".cls");
            return !str.equals(sb.toString()) && str.contains(this.f3533a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements FilenameFilter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Set f3534a;

        @DexIgnore
        public f(u84 u84, Set set) {
            this.f3534a = set;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (str.length() < 35) {
                return false;
            }
            return this.f3534a.contains(str.substring(0, 35));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f3535a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        public g(u84 u84, String str, String str2, long j) {
            this.f3535a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.p(xb4, this.f3535a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f3536a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public h(String str, String str2, String str3, String str4, int i) {
            this.f3536a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.r(xb4, this.f3536a, this.b, this.c, this.d, this.e, u84.this.q);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f3537a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public i(u84 u84, String str, String str2, boolean z) {
            this.f3537a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.B(xb4, this.f3537a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends y {
        @DexIgnore
        public j(String str) {
            super(str);
        }

        @DexIgnore
        @Override // com.fossil.u84.y
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ int f3538a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ long e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ String h;
        @DexIgnore
        public /* final */ /* synthetic */ String i;

        @DexIgnore
        public k(u84 u84, int i2, String str, int i3, long j, long j2, boolean z, int i4, String str2, String str3) {
            this.f3538a = i2;
            this.b = str;
            this.c = i3;
            this.d = j;
            this.e = j2;
            this.f = z;
            this.g = i4;
            this.h = str2;
            this.i = str3;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.t(xb4, this.f3538a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s94 f3539a;

        @DexIgnore
        public l(u84 u84, s94 s94) {
            this.f3539a = s94;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.C(xb4, this.f3539a.b(), null, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements x {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f3540a;

        @DexIgnore
        public m(String str) {
            this.f3540a = str;
        }

        @DexIgnore
        @Override // com.fossil.u84.x
        public void a(xb4 xb4) throws Exception {
            yb4.s(xb4, this.f3540a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ long f3541a;

        @DexIgnore
        public n(long j) {
            this.f3541a = j;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            Bundle bundle = new Bundle();
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", this.f3541a);
            u84.this.r.a("_ae", bundle);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class p implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class q implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class r implements a94.a {
        @DexIgnore
        public r() {
        }

        @DexIgnore
        @Override // com.fossil.a94.a
        public void a(rc4 rc4, Thread thread, Throwable th) {
            u84.this.e0(rc4, thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class s implements Callable<nt3<Void>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Date f3543a;
        @DexIgnore
        public /* final */ /* synthetic */ Throwable b;
        @DexIgnore
        public /* final */ /* synthetic */ Thread c;
        @DexIgnore
        public /* final */ /* synthetic */ rc4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements mt3<wc4, Void> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ Executor f3544a;

            @DexIgnore
            public a(Executor executor) {
                this.f3544a = executor;
            }

            @DexIgnore
            /* renamed from: a */
            public nt3<Void> then(wc4 wc4) throws Exception {
                if (wc4 == null) {
                    x74.f().i("Received null app settings, cannot send reports at crash time.");
                    return qt3.f(null);
                }
                u84.this.u0(wc4, true);
                return qt3.h(u84.this.q0(), u84.this.s.n(this.f3544a, d94.getState(wc4)));
            }
        }

        @DexIgnore
        public s(Date date, Throwable th, Thread thread, rc4 rc4) {
            this.f3543a = date;
            this.b = th;
            this.c = thread;
            this.d = rc4;
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Void> call() throws Exception {
            u84.this.c.a();
            long b0 = u84.b0(this.f3543a);
            u84.this.s.k(this.b, this.c, b0);
            u84.this.F0(this.c, this.b, b0);
            u84.this.D0(this.f3543a.getTime());
            zc4 b2 = this.d.b();
            int i = b2.b().f4303a;
            int i2 = b2.b().b;
            u84.this.J(i);
            u84.this.L();
            u84.this.B0(i2);
            if (!u84.this.b.b()) {
                return qt3.f(null);
            }
            Executor c2 = u84.this.e.c();
            return this.d.a().s(c2, new a(c2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class t implements mt3<Void, Boolean> {
        @DexIgnore
        public t(u84 u84) {
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Boolean> then(Void r2) throws Exception {
            return qt3.f(Boolean.TRUE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class u implements mt3<Boolean, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nt3 f3545a;
        @DexIgnore
        public /* final */ /* synthetic */ float b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Callable<nt3<Void>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ Boolean f3546a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.u84$u$a$a")
            /* renamed from: com.fossil.u84$u$a$a  reason: collision with other inner class name */
            public class C0249a implements mt3<wc4, Void> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ List f3547a;
                @DexIgnore
                public /* final */ /* synthetic */ boolean b;
                @DexIgnore
                public /* final */ /* synthetic */ Executor c;

                @DexIgnore
                public C0249a(List list, boolean z, Executor executor) {
                    this.f3547a = list;
                    this.b = z;
                    this.c = executor;
                }

                @DexIgnore
                /* renamed from: a */
                public nt3<Void> then(wc4 wc4) throws Exception {
                    if (wc4 == null) {
                        x74.f().i("Received null app settings, cannot send reports during app startup.");
                        return qt3.f(null);
                    }
                    for (ec4 ec4 : this.f3547a) {
                        if (ec4.getType() == ec4.a.JAVA) {
                            u84.x(wc4.e, ec4.c());
                        }
                    }
                    u84.this.q0();
                    u84.this.j.a(wc4).e(this.f3547a, this.b, u.this.b);
                    u84.this.s.n(this.c, d94.getState(wc4));
                    u84.this.w.e(null);
                    return qt3.f(null);
                }
            }

            @DexIgnore
            public a(Boolean bool) {
                this.f3546a = bool;
            }

            @DexIgnore
            /* renamed from: a */
            public nt3<Void> call() throws Exception {
                List<ec4> d = u84.this.m.d();
                if (!this.f3546a.booleanValue()) {
                    x74.f().b("Reports are being deleted.");
                    u84.G(u84.this.h0());
                    u84.this.m.c(d);
                    u84.this.s.m();
                    u84.this.w.e(null);
                    return qt3.f(null);
                }
                x74.f().b("Reports are being sent.");
                boolean booleanValue = this.f3546a.booleanValue();
                u84.this.b.a(booleanValue);
                Executor c = u84.this.e.c();
                return u.this.f3545a.s(c, new C0249a(d, booleanValue, c));
            }
        }

        @DexIgnore
        public u(nt3 nt3, float f) {
            this.f3545a = nt3;
            this.b = f;
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Void> then(Boolean bool) throws Exception {
            return u84.this.e.i(new a(bool));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class v implements bc4.b {
        @DexIgnore
        public v() {
        }

        @DexIgnore
        @Override // com.fossil.bc4.b
        public bc4 a(wc4 wc4) {
            String str = wc4.c;
            String str2 = wc4.d;
            return new bc4(wc4.e, u84.this.i.f2158a, d94.getState(wc4), u84.this.m, u84.this.S(str, str2), u84.this.n);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class w implements FilenameFilter {
        @DexIgnore
        public w() {
        }

        @DexIgnore
        public /* synthetic */ w(j jVar) {
            this();
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return !u84.z.accept(file, str) && u84.C.matcher(str).matches();
        }
    }

    @DexIgnore
    public interface x {
        @DexIgnore
        void a(xb4 xb4) throws Exception;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class y implements FilenameFilter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3549a;

        @DexIgnore
        public y(String str) {
            this.f3549a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return str.contains(this.f3549a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class z implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return wb4.e.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    @DexIgnore
    public u84(Context context, s84 s84, kb4 kb4, h94 h94, c94 c94, tb4 tb4, x84 x84, l84 l84, ac4 ac4, bc4.b bVar, w74 w74, nd4 nd4, b84 b84, rc4 rc4) {
        new AtomicInteger(0);
        new AtomicBoolean(false);
        this.f3525a = context;
        this.e = s84;
        this.f = kb4;
        this.g = h94;
        this.b = c94;
        this.h = tb4;
        this.c = x84;
        this.i = l84;
        if (bVar != null) {
            this.j = bVar;
        } else {
            this.j = F();
        }
        this.o = w74;
        this.q = nd4.a();
        this.r = b84;
        this.d = new s94();
        this.k = new a0(tb4);
        this.l = new v94(context, this.k);
        this.m = ac4 == null ? new ac4(new b0(this, null)) : ac4;
        this.n = new c0(this, null);
        hd4 hd4 = new hd4(1024, new jd4(10));
        this.p = hd4;
        this.s = q94.b(context, h94, tb4, l84, this.l, this.d, hd4, rc4);
    }

    @DexIgnore
    public static void E(InputStream inputStream, xb4 xb4, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            int read = inputStream.read(bArr, i3, i2 - i3);
            if (read < 0) {
                break;
            }
            i3 += read;
        }
        xb4.U(bArr);
    }

    @DexIgnore
    public static void G(File[] fileArr) {
        if (fileArr != null) {
            for (File file : fileArr) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public static void H0(xb4 xb4, File[] fileArr, String str) {
        Arrays.sort(fileArr, r84.c);
        for (File file : fileArr) {
            try {
                x74.f().b(String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                P0(xb4, file);
            } catch (Exception e2) {
                x74.f().e("Error writting non-fatal to session.", e2);
            }
        }
    }

    @DexIgnore
    public static void P0(xb4 xb4, File file) throws IOException {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            x74.f().d("Tried to include a file that doesn't exist: " + file.getName());
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                E(fileInputStream, xb4, (int) file.length());
                r84.e(fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                r84.e(fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            r84.e(fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    @DexIgnore
    public static boolean Q() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public static long U() {
        return b0(new Date());
    }

    @DexIgnore
    public static List<l94> X(z74 z74, String str, Context context, File file, byte[] bArr) {
        byte[] bArr2;
        k94 k94 = new k94(file);
        File b2 = k94.b(str);
        File a2 = k94.a(str);
        try {
            bArr2 = eb4.a(z74.d(), context);
        } catch (Exception e2) {
            bArr2 = null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new p84("logs_file", FileLogWriter.LOG_FOLDER, bArr));
        arrayList.add(new p84("binary_images_file", "binaryImages", bArr2));
        arrayList.add(new g94("crash_meta_file", "metadata", z74.g()));
        arrayList.add(new g94("session_meta_file", Constants.SESSION, z74.f()));
        arrayList.add(new g94("app_meta_file", "app", z74.a()));
        arrayList.add(new g94("device_meta_file", "device", z74.c()));
        arrayList.add(new g94("os_meta_file", "os", z74.b()));
        arrayList.add(new g94("minidump_file", "minidump", z74.e()));
        arrayList.add(new g94("user_meta_file", "user", b2));
        arrayList.add(new g94("keys_file", "keys", a2));
        return arrayList;
    }

    @DexIgnore
    public static String a0(File file) {
        return file.getName().substring(0, 35);
    }

    @DexIgnore
    public static long b0(Date date) {
        return date.getTime() / 1000;
    }

    @DexIgnore
    public static String r0(String str) {
        return str.replaceAll(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "");
    }

    @DexIgnore
    public static void x(String str, File file) throws Exception {
        if (str != null) {
            y(file, new m(str));
        }
    }

    @DexIgnore
    public static void y(File file, x xVar) throws Exception {
        FileOutputStream fileOutputStream;
        xb4 xb4;
        xb4 xb42 = null;
        try {
            fileOutputStream = new FileOutputStream(file, true);
            try {
                xb42 = xb4.z(fileOutputStream);
                xVar.a(xb42);
                r84.j(xb42, "Failed to flush to append to " + file.getPath());
                r84.e(fileOutputStream, "Failed to close " + file.getPath());
            } catch (Throwable th) {
                th = th;
                xb4 = xb42;
                r84.j(xb4, "Failed to flush to append to " + file.getPath());
                r84.e(fileOutputStream, "Failed to close " + file.getPath());
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            xb4 = null;
            r84.j(xb4, "Failed to flush to append to " + file.getPath());
            r84.e(fileOutputStream, "Failed to close " + file.getPath());
            throw th;
        }
    }

    @DexIgnore
    public final void A(s94 s94) {
        this.e.h(new b(s94));
    }

    @DexIgnore
    public final void A0(String str, int i2) {
        File W = W();
        t94.d(W, new y(str + "SessionEvent"), i2, B);
    }

    @DexIgnore
    public void B() {
        this.e.g(new e());
    }

    @DexIgnore
    public void B0(int i2) {
        int f2 = i2 - t94.f(Y(), V(), i2, B);
        t94.d(W(), z, f2 - t94.c(Z(), f2, B), B);
    }

    @DexIgnore
    public final void C(File[] fileArr, int i2, int i3) {
        x74.f().b("Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String a02 = a0(file);
            x74 f2 = x74.f();
            f2.b("Closing session: " + a02);
            N0(file, a02, i3);
            i2++;
        }
    }

    @DexIgnore
    public final nt3<Boolean> C0() {
        if (this.b.b()) {
            x74.f().b("Automatic data collection is enabled. Allowing upload.");
            this.u.e(Boolean.FALSE);
            return qt3.f(Boolean.TRUE);
        }
        x74.f().b("Automatic data collection is disabled.");
        x74.f().b("Notifying that unsent reports are available.");
        this.u.e(Boolean.TRUE);
        nt3<TContinuationResult> r2 = this.b.c().r(new t(this));
        x74.f().b("Waiting for send/deleteUnsentReports to be called.");
        return t94.g(r2, this.v.a());
    }

    @DexIgnore
    public final void D(wb4 wb4) {
        if (wb4 != null) {
            try {
                wb4.a();
            } catch (IOException e2) {
                x74.f().e("Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    @DexIgnore
    public final void D0(long j2) {
        try {
            File W = W();
            new File(W, ".ae" + j2).createNewFile();
        } catch (IOException e2) {
            x74.f().b("Could not write app exception marker.");
        }
    }

    @DexIgnore
    public final void E0(String str, long j2) throws Exception {
        String format = String.format(Locale.US, "Crashlytics Android SDK/%s", w84.i());
        M0(str, "BeginSession", new g(this, str, format, j2));
        this.o.d(str, format, j2);
    }

    @DexIgnore
    public final bc4.b F() {
        return new v();
    }

    @DexIgnore
    public final void F0(Thread thread, Throwable th, long j2) {
        Throwable th2;
        wb4 wb4;
        xb4 xb4;
        Throwable th3;
        wb4 wb42;
        xb4 xb42;
        wb4 wb43;
        xb4 z2;
        xb4 xb43 = null;
        try {
            String T = T();
            if (T == null) {
                x74.f().d("Tried to write a fatal exception while no session was open.");
                r84.j(null, "Failed to flush to session begin file.");
                r84.e(null, "Failed to close fatal exception file output stream.");
                return;
            }
            wb43 = new wb4(W(), T + "SessionCrash");
            try {
                z2 = xb4.z(wb43);
            } catch (Exception e2) {
                e = e2;
                wb42 = wb43;
                xb42 = xb43;
                try {
                    x74.f().e("An error occurred in the fatal exception logger", e);
                    wb43 = wb42;
                    r84.j(xb42, "Failed to flush to session begin file.");
                    r84.e(wb43, "Failed to close fatal exception file output stream.");
                } catch (Throwable th4) {
                    th2 = th4;
                    wb4 = wb42;
                    xb43 = xb42;
                    xb4 = xb43;
                    th3 = th2;
                    r84.j(xb4, "Failed to flush to session begin file.");
                    r84.e(wb4, "Failed to close fatal exception file output stream.");
                    throw th3;
                }
            } catch (Throwable th5) {
                th2 = th5;
                wb4 = wb43;
                xb4 = xb43;
                th3 = th2;
                r84.j(xb4, "Failed to flush to session begin file.");
                r84.e(wb4, "Failed to close fatal exception file output stream.");
                throw th3;
            }
            try {
                K0(z2, thread, th, j2, CrashDumperPlugin.NAME, true);
                xb42 = z2;
            } catch (Exception e3) {
                e = e3;
                xb43 = z2;
                wb42 = wb43;
                xb42 = xb43;
                x74.f().e("An error occurred in the fatal exception logger", e);
                wb43 = wb42;
                r84.j(xb42, "Failed to flush to session begin file.");
                r84.e(wb43, "Failed to close fatal exception file output stream.");
            } catch (Throwable th6) {
                th2 = th6;
                wb4 = wb43;
                xb43 = z2;
                xb4 = xb43;
                th3 = th2;
                r84.j(xb4, "Failed to flush to session begin file.");
                r84.e(wb4, "Failed to close fatal exception file output stream.");
                throw th3;
            }
            r84.j(xb42, "Failed to flush to session begin file.");
            r84.e(wb43, "Failed to close fatal exception file output stream.");
        } catch (Exception e4) {
            e = e4;
            wb42 = null;
            xb42 = null;
            x74.f().e("An error occurred in the fatal exception logger", e);
            wb43 = wb42;
            r84.j(xb42, "Failed to flush to session begin file.");
            r84.e(wb43, "Failed to close fatal exception file output stream.");
        } catch (Throwable th7) {
            th3 = th7;
            wb4 = null;
            xb4 = null;
            r84.j(xb4, "Failed to flush to session begin file.");
            r84.e(wb4, "Failed to close fatal exception file output stream.");
            throw th3;
        }
    }

    @DexIgnore
    public final void G0(xb4 xb4, String str) throws IOException {
        String[] strArr = E;
        for (String str2 : strArr) {
            File[] k0 = k0(new y(str + str2 + ".cls"));
            if (k0.length == 0) {
                x74.f().b("Can't find " + str2 + " data for session ID " + str);
            } else {
                x74.f().b("Collecting " + str2 + " data for session ID " + str);
                P0(xb4, k0[0]);
            }
        }
    }

    @DexIgnore
    public boolean H() {
        if (!this.c.c()) {
            String T = T();
            return T != null && this.o.e(T);
        }
        x74.f().b("Found previous crash marker.");
        this.c.d();
        return true;
    }

    @DexIgnore
    public void I(File[] fileArr) {
        HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            x74.f().b("Found invalid session part file: " + file);
            hashSet.add(a0(file));
        }
        if (!hashSet.isEmpty()) {
            File[] k0 = k0(new f(this, hashSet));
            for (File file2 : k0) {
                x74.f().b("Deleting invalid session file: " + file2);
                file2.delete();
            }
        }
    }

    @DexIgnore
    public final void I0(String str) throws Exception {
        String d2 = this.g.d();
        l84 l84 = this.i;
        String str2 = l84.e;
        String str3 = l84.f;
        String a2 = this.g.a();
        int id = e94.determineFrom(this.i.c).getId();
        M0(str, "SessionApp", new h(d2, str2, str3, a2, id));
        this.o.f(str, d2, str2, str3, a2, id, this.q);
    }

    @DexIgnore
    public void J(int i2) throws Exception {
        K(i2, true);
    }

    @DexIgnore
    public final void J0(String str) throws Exception {
        Context R = R();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int m2 = r84.m();
        String str2 = Build.MODEL;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long v2 = r84.v();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean C2 = r84.C(R);
        int n2 = r84.n(R);
        String str3 = Build.MANUFACTURER;
        String str4 = Build.PRODUCT;
        M0(str, "SessionDevice", new k(this, m2, str2, availableProcessors, v2, blockCount, C2, n2, str3, str4));
        this.o.c(str, m2, str2, availableProcessors, v2, blockCount, C2, n2, str3, str4);
    }

    @DexIgnore
    public final void K(int i2, boolean z2) throws Exception {
        int i3 = !z2 ? 1 : 0;
        z0(i3 + 8);
        File[] o0 = o0();
        if (o0.length <= i3) {
            x74.f().b("No open sessions to be closed.");
            return;
        }
        String a02 = a0(o0[i3]);
        O0(a02);
        if (z2) {
            this.s.h();
        } else if (this.o.e(a02)) {
            O(a02);
            if (!this.o.a(a02)) {
                x74 f2 = x74.f();
                f2.b("Could not finalize native session: " + a02);
            }
        }
        C(o0, i3, i2);
        this.s.d(U());
    }

    @DexIgnore
    public final void K0(xb4 xb4, Thread thread, Throwable th, long j2, String str, boolean z2) throws Exception {
        Thread[] threadArr;
        Map<String, String> treeMap;
        ld4 ld4 = new ld4(th, this.p);
        Context R = R();
        o84 a2 = o84.a(R);
        Float b2 = a2.b();
        int c2 = a2.c();
        boolean q2 = r84.q(R);
        int i2 = R.getResources().getConfiguration().orientation;
        long v2 = r84.v();
        long a3 = r84.a(R);
        long b3 = r84.b(Environment.getDataDirectory().getPath());
        ActivityManager.RunningAppProcessInfo k2 = r84.k(R.getPackageName(), R);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = ld4.c;
        String str2 = this.i.b;
        String d2 = this.g.d();
        int i3 = 0;
        if (z2) {
            Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            threadArr = new Thread[allStackTraces.size()];
            for (Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
                threadArr[i3] = entry.getKey();
                linkedList.add(this.p.a(entry.getValue()));
                i3++;
            }
        } else {
            threadArr = new Thread[0];
        }
        if (!r84.l(R, "com.crashlytics.CollectCustomKeys", true)) {
            treeMap = new TreeMap<>();
        } else {
            Map<String, String> a4 = this.d.a();
            treeMap = (a4 == null || a4.size() <= 1) ? a4 : new TreeMap<>(a4);
        }
        yb4.u(xb4, j2, str, ld4, thread, stackTraceElementArr, threadArr, linkedList, 8, treeMap, this.l.c(), k2, i2, d2, str2, b2, c2, q2, v2 - a3, b3);
        this.l.a();
    }

    @DexIgnore
    public final void L() throws Exception {
        long U = U();
        String q84 = new q84(this.g).toString();
        x74 f2 = x74.f();
        f2.b("Opening a new session with ID " + q84);
        this.o.h(q84);
        E0(q84, U);
        I0(q84);
        L0(q84);
        J0(q84);
        this.l.g(q84);
        this.s.g(r0(q84), U);
    }

    @DexIgnore
    public final void L0(String str) throws Exception {
        String str2 = Build.VERSION.RELEASE;
        String str3 = Build.VERSION.CODENAME;
        boolean E2 = r84.E(R());
        M0(str, "SessionOS", new i(this, str2, str3, E2));
        this.o.g(str, str2, str3, E2);
    }

    @DexIgnore
    public void M(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, rc4 rc4) {
        s0();
        a94 a94 = new a94(new r(), rc4, uncaughtExceptionHandler);
        this.t = a94;
        Thread.setDefaultUncaughtExceptionHandler(a94);
    }

    @DexIgnore
    public final void M0(String str, String str2, x xVar) throws Exception {
        wb4 wb4;
        Throwable th;
        xb4 xb4;
        try {
            wb4 = new wb4(W(), str + str2);
            try {
                xb4 = xb4.z(wb4);
                try {
                    xVar.a(xb4);
                    r84.j(xb4, "Failed to flush to session " + str2 + " file.");
                    r84.e(wb4, "Failed to close session " + str2 + " file.");
                } catch (Throwable th2) {
                    th = th2;
                    r84.j(xb4, "Failed to flush to session " + str2 + " file.");
                    r84.e(wb4, "Failed to close session " + str2 + " file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                xb4 = null;
                r84.j(xb4, "Failed to flush to session " + str2 + " file.");
                r84.e(wb4, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            xb4 = null;
            wb4 = null;
            r84.j(xb4, "Failed to flush to session " + str2 + " file.");
            r84.e(wb4, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    @DexIgnore
    public final File[] N(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    @DexIgnore
    public final void N0(File file, String str, int i2) {
        x74 f2 = x74.f();
        f2.b("Collecting session parts for ID " + str);
        File[] k0 = k0(new y(str + "SessionCrash"));
        boolean z2 = k0 != null && k0.length > 0;
        x74.f().b(String.format(Locale.US, "Session %s has fatal exception: %s", str, Boolean.valueOf(z2)));
        File[] k02 = k0(new y(str + "SessionEvent"));
        boolean z3 = k02 != null && k02.length > 0;
        x74.f().b(String.format(Locale.US, "Session %s has non-fatal exceptions: %s", str, Boolean.valueOf(z3)));
        if (z2 || z3) {
            y0(file, str, c0(str, k02, i2), z2 ? k0[0] : null);
        } else {
            x74 f3 = x74.f();
            f3.b("No events present for session ID " + str);
        }
        x74 f4 = x74.f();
        f4.b("Removing session part files for ID " + str);
        G(n0(str));
    }

    @DexIgnore
    public final void O(String str) {
        x74 f2 = x74.f();
        f2.b("Finalizing native report for session " + str);
        z74 b2 = this.o.b(str);
        File e2 = b2.e();
        if (e2 == null || !e2.exists()) {
            x74 f3 = x74.f();
            f3.i("No minidump data found for session " + str);
            return;
        }
        long lastModified = e2.lastModified();
        v94 v94 = new v94(this.f3525a, this.k, str);
        File file = new File(Y(), str);
        if (!file.mkdirs()) {
            x74.f().b("Couldn't create native sessions directory");
            return;
        }
        D0(lastModified);
        List<l94> X = X(b2, str, R(), W(), v94.c());
        m94.b(file, X);
        this.s.c(r0(str), X);
        v94.a();
    }

    @DexIgnore
    public final void O0(String str) throws Exception {
        M0(str, "SessionUser", new l(this, d0(str)));
    }

    @DexIgnore
    public boolean P(int i2) {
        this.e.b();
        if (f0()) {
            x74.f().b("Skipping session finalization because a crash has already occurred.");
            return false;
        }
        x74.f().b("Finalizing previously open sessions.");
        try {
            K(i2, false);
            x74.f().b("Closed all previously open sessions");
            return true;
        } catch (Exception e2) {
            x74.f().e("Unable to finalize previously open sessions.", e2);
            return false;
        }
    }

    @DexIgnore
    public void Q0(long j2, String str) {
        this.e.h(new a(j2, str));
    }

    @DexIgnore
    public final Context R() {
        return this.f3525a;
    }

    @DexIgnore
    public final hc4 S(String str, String str2) {
        String u2 = r84.u(R(), "com.crashlytics.ApiEndpoint");
        return new gc4(new ic4(u2, str, this.f, w84.i()), new jc4(u2, str2, this.f, w84.i()));
    }

    @DexIgnore
    public final String T() {
        File[] o0 = o0();
        if (o0.length > 0) {
            return a0(o0[0]);
        }
        return null;
    }

    @DexIgnore
    public File V() {
        return new File(W(), "fatal-sessions");
    }

    @DexIgnore
    public File W() {
        return this.h.b();
    }

    @DexIgnore
    public File Y() {
        return new File(W(), "native-sessions");
    }

    @DexIgnore
    public File Z() {
        return new File(W(), "nonfatal-sessions");
    }

    @DexIgnore
    public final File[] c0(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        x74.f().b(String.format(Locale.US, "Trimming down to %d logged exceptions.", Integer.valueOf(i2)));
        A0(str, i2);
        return k0(new y(str + "SessionEvent"));
    }

    @DexIgnore
    public final s94 d0(String str) {
        return f0() ? this.d : new k94(W()).e(str);
    }

    @DexIgnore
    public void e0(rc4 rc4, Thread thread, Throwable th) {
        synchronized (this) {
            x74 f2 = x74.f();
            f2.b("Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
            try {
                t94.a(this.e.i(new s(new Date(), th, thread, rc4)));
            } catch (Exception e2) {
            }
        }
    }

    @DexIgnore
    public boolean f0() {
        a94 a94 = this.t;
        return a94 != null && a94.a();
    }

    @DexIgnore
    public File[] h0() {
        return k0(y);
    }

    @DexIgnore
    public File[] i0() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, j0(V(), z));
        Collections.addAll(linkedList, j0(Z(), z));
        Collections.addAll(linkedList, j0(W(), z));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    @DexIgnore
    public final File[] j0(File file, FilenameFilter filenameFilter) {
        return N(file.listFiles(filenameFilter));
    }

    @DexIgnore
    public final File[] k0(FilenameFilter filenameFilter) {
        return j0(W(), filenameFilter);
    }

    @DexIgnore
    public File[] l0() {
        return N(Y().listFiles());
    }

    @DexIgnore
    public File[] m0() {
        return k0(x);
    }

    @DexIgnore
    public final File[] n0(String str) {
        return k0(new e0(str));
    }

    @DexIgnore
    public final File[] o0() {
        File[] m0 = m0();
        Arrays.sort(m0, A);
        return m0;
    }

    @DexIgnore
    public final nt3<Void> p0(long j2) {
        if (!Q()) {
            return qt3.c(new ScheduledThreadPoolExecutor(1), new n(j2));
        }
        x74.f().b("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        return qt3.f(null);
    }

    @DexIgnore
    public final nt3<Void> q0() {
        ArrayList arrayList = new ArrayList();
        File[] h0 = h0();
        for (File file : h0) {
            try {
                arrayList.add(p0(Long.parseLong(file.getName().substring(3))));
            } catch (NumberFormatException e2) {
                x74.f().b("Could not parse timestamp from file " + file.getName());
            }
            file.delete();
        }
        return qt3.g(arrayList);
    }

    @DexIgnore
    public void s0() {
        this.e.h(new d());
    }

    @DexIgnore
    public final void t0(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = C.matcher(name);
            if (!matcher.matches()) {
                x74.f().b("Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                x74.f().b("Trimming session file: " + name);
                file.delete();
            }
        }
    }

    @DexIgnore
    public final void u0(wc4 wc4, boolean z2) throws Exception {
        Context R = R();
        bc4 a2 = this.j.a(wc4);
        File[] i0 = i0();
        for (File file : i0) {
            x(wc4.e, file);
            this.e.g(new d0(R, new fc4(file, D), a2, z2));
        }
    }

    @DexIgnore
    public void v0(String str, String str2) {
        try {
            this.d.d(str, str2);
            z(this.d.a());
        } catch (IllegalArgumentException e2) {
            Context context = this.f3525a;
            if (context == null || !r84.A(context)) {
                x74.f().d("Attempting to set custom attribute with null key, ignoring.");
                return;
            }
            throw e2;
        }
    }

    @DexIgnore
    public void w0(String str) {
        this.d.e(str);
        A(this.d);
    }

    @DexIgnore
    public nt3<Void> x0(float f2, nt3<wc4> nt3) {
        if (!this.m.a()) {
            x74.f().b("No reports are available.");
            this.u.e(Boolean.FALSE);
            return qt3.f(null);
        }
        x74.f().b("Unsent reports are available.");
        return C0().r(new u(nt3, f2));
    }

    @DexIgnore
    public final void y0(File file, String str, File[] fileArr, File file2) {
        Throwable th;
        wb4 wb4;
        wb4 wb42;
        xb4 xb4;
        wb4 wb43;
        Exception e2;
        xb4 xb42;
        xb4 xb43 = null;
        boolean z2 = file2 != null;
        File V = z2 ? V() : Z();
        if (!V.exists()) {
            V.mkdirs();
        }
        try {
            wb43 = new wb4(V, str);
            try {
                xb42 = xb4.z(wb43);
                try {
                    x74.f().b("Collecting SessionStart data for session ID " + str);
                    P0(xb42, file);
                    xb42.g0(4, U());
                    xb42.C(5, z2);
                    xb42.e0(11, 1);
                    xb42.H(12, 3);
                    G0(xb42, str);
                    H0(xb42, fileArr, str);
                    if (z2) {
                        P0(xb42, file2);
                    }
                    r84.j(xb42, "Error flushing session file stream");
                    r84.e(wb43, "Failed to close CLS file");
                } catch (Exception e3) {
                    e2 = e3;
                    try {
                        x74.f().e("Failed to write session file for session ID: " + str, e2);
                        r84.j(xb42, "Error flushing session file stream");
                        D(wb43);
                    } catch (Throwable th2) {
                        th = th2;
                        wb4 = wb43;
                        xb43 = xb42;
                        wb42 = wb4;
                        xb4 = xb43;
                        r84.j(xb4, "Error flushing session file stream");
                        r84.e(wb42, "Failed to close CLS file");
                        throw th;
                    }
                }
            } catch (Exception e4) {
                e2 = e4;
                xb42 = null;
                x74.f().e("Failed to write session file for session ID: " + str, e2);
                r84.j(xb42, "Error flushing session file stream");
                D(wb43);
            } catch (Throwable th3) {
                th = th3;
                wb4 = wb43;
                wb42 = wb4;
                xb4 = xb43;
                r84.j(xb4, "Error flushing session file stream");
                r84.e(wb42, "Failed to close CLS file");
                throw th;
            }
        } catch (Exception e5) {
            e2 = e5;
            xb42 = null;
            wb43 = null;
            x74.f().e("Failed to write session file for session ID: " + str, e2);
            r84.j(xb42, "Error flushing session file stream");
            D(wb43);
        } catch (Throwable th4) {
            wb42 = null;
            xb4 = null;
            th = th4;
            r84.j(xb4, "Error flushing session file stream");
            r84.e(wb42, "Failed to close CLS file");
            throw th;
        }
    }

    @DexIgnore
    public final void z(Map<String, String> map) {
        this.e.h(new c(map));
    }

    @DexIgnore
    public final void z0(int i2) {
        HashSet hashSet = new HashSet();
        File[] o0 = o0();
        int min = Math.min(i2, o0.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(a0(o0[i3]));
        }
        this.l.b(hashSet);
        t0(k0(new w(null)), hashSet);
    }
}
