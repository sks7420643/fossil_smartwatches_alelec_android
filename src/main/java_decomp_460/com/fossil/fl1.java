package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fl1 extends el1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final gl1[] a(gl1... gl1Arr) {
            Object[] array = em7.E(gl1Arr).toArray(new gl1[0]);
            if (array != null) {
                return (gl1[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: b */
        public fl1 createFromParcel(Parcel parcel) {
            return (fl1) el1.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fl1[] newArray(int i) {
            return new fl1[i];
        }
    }

    @DexIgnore
    public fl1(hl1 hl1, ql1 ql1, il1 il1) {
        super(o8.ALARM, CREATOR.a(hl1, ql1, il1));
    }
}
