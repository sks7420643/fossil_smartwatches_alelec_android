package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o02 implements Factory<m02> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<t32> f2602a;
    @DexIgnore
    public /* final */ Provider<t32> b;
    @DexIgnore
    public /* final */ Provider<k12> c;
    @DexIgnore
    public /* final */ Provider<b22> d;
    @DexIgnore
    public /* final */ Provider<f22> e;

    @DexIgnore
    public o02(Provider<t32> provider, Provider<t32> provider2, Provider<k12> provider3, Provider<b22> provider4, Provider<f22> provider5) {
        this.f2602a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static o02 a(Provider<t32> provider, Provider<t32> provider2, Provider<k12> provider3, Provider<b22> provider4, Provider<f22> provider5) {
        return new o02(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    /* renamed from: b */
    public m02 get() {
        return new m02(this.f2602a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
