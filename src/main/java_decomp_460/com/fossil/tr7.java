package com.fossil;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr7 extends pr7 {
    @DexIgnore
    @Override // com.fossil.pr7
    public Random d() {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        pq7.b(current, "ThreadLocalRandom.current()");
        return current;
    }
}
