package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2801a;

    /*
    static {
        int[] iArr = new int[o87.values().length];
        f2801a = iArr;
        iArr[o87.BLACK.ordinal()] = 1;
        f2801a[o87.WHITE.ordinal()] = 2;
        f2801a[o87.LIGHT_GRAY.ordinal()] = 3;
        f2801a[o87.DARK_GRAY.ordinal()] = 4;
    }
    */
}
