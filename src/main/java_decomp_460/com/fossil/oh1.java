package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh1 implements qb1<InputStream, hh1> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<ImageHeaderParser> f2680a;
    @DexIgnore
    public /* final */ qb1<ByteBuffer, hh1> b;
    @DexIgnore
    public /* final */ od1 c;

    @DexIgnore
    public oh1(List<ImageHeaderParser> list, qb1<ByteBuffer, hh1> qb1, od1 od1) {
        this.f2680a = list;
        this.b = qb1;
        this.c = od1;
    }

    @DexIgnore
    public static byte[] e(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (Log.isLoggable("StreamGifDecoder", 5)) {
                Log.w("StreamGifDecoder", "Error reading data from stream", e);
            }
            return null;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public id1<hh1> b(InputStream inputStream, int i, int i2, ob1 ob1) throws IOException {
        byte[] e = e(inputStream);
        if (e == null) {
            return null;
        }
        return this.b.b(ByteBuffer.wrap(e), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(InputStream inputStream, ob1 ob1) throws IOException {
        return !((Boolean) ob1.c(nh1.b)).booleanValue() && lb1.e(this.f2680a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }
}
