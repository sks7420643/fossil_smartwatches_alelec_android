package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j83 implements k83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1725a;
    @DexIgnore
    public static /* final */ xv2<Double> b;
    @DexIgnore
    public static /* final */ xv2<Long> c;
    @DexIgnore
    public static /* final */ xv2<Long> d;
    @DexIgnore
    public static /* final */ xv2<String> e;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f1725a = hw2.d("measurement.test.boolean_flag", false);
        b = hw2.a("measurement.test.double_flag", -3.0d);
        c = hw2.b("measurement.test.int_flag", -2);
        d = hw2.b("measurement.test.long_flag", -1);
        e = hw2.c("measurement.test.string_flag", "---");
    }
    */

    @DexIgnore
    @Override // com.fossil.k83
    public final boolean zza() {
        return f1725a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.k83
    public final double zzb() {
        return b.o().doubleValue();
    }

    @DexIgnore
    @Override // com.fossil.k83
    public final long zzc() {
        return c.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.k83
    public final long zzd() {
        return d.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.k83
    public final String zze() {
        return e.o();
    }
}
