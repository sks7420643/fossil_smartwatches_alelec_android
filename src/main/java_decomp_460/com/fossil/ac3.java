package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ac3 extends IInterface {
    @DexIgnore
    rg2 C0(rg2 rg2, rg2 rg22, Bundle bundle) throws RemoteException;

    @DexIgnore
    void l(pc3 pc3) throws RemoteException;

    @DexIgnore
    void o() throws RemoteException;

    @DexIgnore
    void onCreate(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onDestroy() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;

    @DexIgnore
    void onResume() throws RemoteException;

    @DexIgnore
    void onSaveInstanceState(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onStart() throws RemoteException;

    @DexIgnore
    void onStop() throws RemoteException;

    @DexIgnore
    void s0(rg2 rg2, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException;
}
