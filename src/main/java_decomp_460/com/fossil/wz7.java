package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz7 {
    @DexIgnore
    public static final int a() {
        return xz7.a();
    }

    @DexIgnore
    public static final int b(String str, int i, int i2, int i3) {
        return yz7.a(str, i, i2, i3);
    }

    @DexIgnore
    public static final long c(String str, long j, long j2, long j3) {
        return yz7.b(str, j, j2, j3);
    }

    @DexIgnore
    public static final String d(String str) {
        return xz7.b(str);
    }

    @DexIgnore
    public static final boolean e(String str, boolean z) {
        return yz7.c(str, z);
    }
}
