package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ee2> CREATOR; // = new de2();
    @DexIgnore
    public Bundle b;
    @DexIgnore
    public b62[] c;
    @DexIgnore
    public int d;

    @DexIgnore
    public ee2() {
    }

    @DexIgnore
    public ee2(Bundle bundle, b62[] b62Arr, int i) {
        this.b = bundle;
        this.c = b62Arr;
        this.d = i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.e(parcel, 1, this.b, false);
        bd2.x(parcel, 2, this.c, i, false);
        bd2.n(parcel, 3, this.d);
        bd2.b(parcel, a2);
    }
}
