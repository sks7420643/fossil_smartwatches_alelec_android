package com.fossil;

import android.database.Cursor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h31 implements g31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f1417a;
    @DexIgnore
    public /* final */ jw0<f31> b;
    @DexIgnore
    public /* final */ xw0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<f31> {
        @DexIgnore
        public a(h31 h31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, f31 f31) {
            String str = f31.f1044a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            px0.bindLong(2, (long) f31.b);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(h31 h31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public h31(qw0 qw0) {
        this.f1417a = qw0;
        this.b = new a(this, qw0);
        this.c = new b(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.g31
    public void a(f31 f31) {
        this.f1417a.assertNotSuspendingTransaction();
        this.f1417a.beginTransaction();
        try {
            this.b.insert((jw0<f31>) f31);
            this.f1417a.setTransactionSuccessful();
        } finally {
            this.f1417a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.g31
    public f31 b(String str) {
        f31 f31 = null;
        tw0 f = tw0.f("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f1417a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f1417a, f, false, null);
        try {
            int c2 = dx0.c(b2, "work_spec_id");
            int c3 = dx0.c(b2, "system_id");
            if (b2.moveToFirst()) {
                f31 = new f31(b2.getString(c2), b2.getInt(c3));
            }
            return f31;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.g31
    public void c(String str) {
        this.f1417a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f1417a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f1417a.setTransactionSuccessful();
        } finally {
            this.f1417a.endTransaction();
            this.c.release(acquire);
        }
    }
}
