package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf1 implements id1<Bitmap>, ed1 {
    @DexIgnore
    public /* final */ Bitmap b;
    @DexIgnore
    public /* final */ rd1 c;

    @DexIgnore
    public yf1(Bitmap bitmap, rd1 rd1) {
        ik1.e(bitmap, "Bitmap must not be null");
        this.b = bitmap;
        ik1.e(rd1, "BitmapPool must not be null");
        this.c = rd1;
    }

    @DexIgnore
    public static yf1 f(Bitmap bitmap, rd1 rd1) {
        if (bitmap == null) {
            return null;
        }
        return new yf1(bitmap, rd1);
    }

    @DexIgnore
    @Override // com.fossil.ed1
    public void a() {
        this.b.prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
        this.c.b(this.b);
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return jk1.h(this.b);
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<Bitmap> d() {
        return Bitmap.class;
    }

    @DexIgnore
    /* renamed from: e */
    public Bitmap get() {
        return this.b;
    }
}
