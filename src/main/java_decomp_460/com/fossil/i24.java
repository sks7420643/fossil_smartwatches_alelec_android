package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i24<E> extends p24<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Queue<E> delegate;
    @DexIgnore
    public /* final */ int maxSize;

    @DexIgnore
    public i24(int i) {
        i14.f(i >= 0, "maxSize (%s) must >= 0", i);
        this.delegate = new ArrayDeque(i);
        this.maxSize = i;
    }

    @DexIgnore
    public static <E> i24<E> create(int i) {
        return new i24<>(i);
    }

    @DexIgnore
    @Override // com.fossil.l24, java.util.Collection, java.util.Queue
    @CanIgnoreReturnValue
    public boolean add(E e) {
        i14.l(e);
        if (this.maxSize != 0) {
            if (size() == this.maxSize) {
                this.delegate.remove();
            }
            this.delegate.add(e);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.l24, java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        int size = collection.size();
        if (size < this.maxSize) {
            return standardAddAll(collection);
        }
        clear();
        return o34.a(this, o34.g(collection, size - this.maxSize));
    }

    @DexIgnore
    @Override // com.fossil.l24
    public boolean contains(Object obj) {
        Queue<E> delegate2 = delegate();
        i14.l(obj);
        return delegate2.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24, com.fossil.p24, com.fossil.p24, com.fossil.p24
    public Queue<E> delegate() {
        return this.delegate;
    }

    @DexIgnore
    @Override // com.fossil.p24, java.util.Queue
    @CanIgnoreReturnValue
    public boolean offer(E e) {
        return add(e);
    }

    @DexIgnore
    public int remainingCapacity() {
        return this.maxSize - size();
    }

    @DexIgnore
    @Override // com.fossil.l24
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        Queue<E> delegate2 = delegate();
        i14.l(obj);
        return delegate2.remove(obj);
    }
}
