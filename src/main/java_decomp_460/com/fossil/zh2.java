package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.pc2;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zh2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zh2> CREATOR; // = new ci2();
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ii2 h;
    @DexIgnore
    public /* final */ Long i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f4472a; // = 0;
        @DexIgnore
        public long b; // = 0;
        @DexIgnore
        public String c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public String e; // = "";
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public Long g;

        @DexIgnore
        public zh2 a() {
            boolean z = true;
            rc2.o(this.f4472a > 0, "Start time should be specified.");
            long j = this.b;
            if (j != 0 && j <= this.f4472a) {
                z = false;
            }
            rc2.o(z, "End time should be later than start time.");
            if (this.d == null) {
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                long j2 = this.f4472a;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 20);
                sb.append(str);
                sb.append(j2);
                this.d = sb.toString();
            }
            return new zh2(this);
        }

        @DexIgnore
        public a b(String str) {
            this.f = hp2.a(str);
            return this;
        }

        @DexIgnore
        public a c(String str) {
            rc2.c(str.length() <= 1000, "Session description cannot exceed %d characters", 1000);
            this.e = str;
            return this;
        }

        @DexIgnore
        public a d(long j, TimeUnit timeUnit) {
            rc2.o(j >= 0, "End time should be positive.");
            this.b = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a e(String str) {
            rc2.a(str != null && TextUtils.getTrimmedLength(str) > 0);
            this.d = str;
            return this;
        }

        @DexIgnore
        public a f(String str) {
            rc2.c(str.length() <= 100, "Session name cannot exceed %d characters", 100);
            this.c = str;
            return this;
        }

        @DexIgnore
        public a g(long j, TimeUnit timeUnit) {
            rc2.o(j > 0, "Start time should be positive.");
            this.f4472a = timeUnit.toMillis(j);
            return this;
        }
    }

    @DexIgnore
    public zh2(long j, long j2, String str, String str2, String str3, int i2, ii2 ii2, Long l) {
        this.b = j;
        this.c = j2;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = i2;
        this.h = ii2;
        this.i = l;
    }

    @DexIgnore
    public zh2(a aVar) {
        this(aVar.f4472a, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, null, aVar.g);
    }

    @DexIgnore
    public long A(TimeUnit timeUnit) {
        return timeUnit.convert(this.b, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public String c() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zh2)) {
            return false;
        }
        zh2 zh2 = (zh2) obj;
        return this.b == zh2.b && this.c == zh2.c && pc2.a(this.d, zh2.d) && pc2.a(this.e, zh2.e) && pc2.a(this.f, zh2.f) && pc2.a(this.h, zh2.h) && this.g == zh2.g;
    }

    @DexIgnore
    public long f(TimeUnit timeUnit) {
        return timeUnit.convert(this.c, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public String h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(Long.valueOf(this.b), Long.valueOf(this.c), this.e);
    }

    @DexIgnore
    public String k() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a(SampleRaw.COLUMN_START_TIME, Long.valueOf(this.b));
        c2.a(SampleRaw.COLUMN_END_TIME, Long.valueOf(this.c));
        c2.a("name", this.d);
        c2.a("identifier", this.e);
        c2.a("description", this.f);
        c2.a(Constants.ACTIVITY, Integer.valueOf(this.g));
        c2.a("application", this.h);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.r(parcel, 1, this.b);
        bd2.r(parcel, 2, this.c);
        bd2.u(parcel, 3, k(), false);
        bd2.u(parcel, 4, h(), false);
        bd2.u(parcel, 5, c(), false);
        bd2.n(parcel, 7, this.g);
        bd2.t(parcel, 8, this.h, i2, false);
        bd2.s(parcel, 9, this.i, false);
        bd2.b(parcel, a2);
    }
}
