package com.fossil;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le1 implements ExecutorService {
    @DexIgnore
    public static /* final */ long c; // = TimeUnit.SECONDS.toMillis(10);
    @DexIgnore
    public static volatile int d;
    @DexIgnore
    public /* final */ ExecutorService b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f2184a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public c d; // = c.b;
        @DexIgnore
        public String e;
        @DexIgnore
        public long f;

        @DexIgnore
        public a(boolean z) {
            this.f2184a = z;
        }

        @DexIgnore
        public le1 a() {
            if (!TextUtils.isEmpty(this.e)) {
                ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.b, this.c, this.f, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new b(this.e, this.d, this.f2184a));
                if (this.f != 0) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new le1(threadPoolExecutor);
            }
            throw new IllegalArgumentException("Name must be non-null and non-empty, but given: " + this.e);
        }

        @DexIgnore
        public a b(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public a c(int i) {
            this.b = i;
            this.c = i;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2185a;
        @DexIgnore
        public /* final */ c b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Thread {
            @DexIgnore
            public a(Runnable runnable, String str) {
                super(runnable, str);
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(9);
                if (b.this.c) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    b.this.b.a(th);
                }
            }
        }

        @DexIgnore
        public b(String str, c cVar, boolean z) {
            this.f2185a = str;
            this.b = cVar;
            this.c = z;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            a aVar;
            synchronized (this) {
                aVar = new a(runnable, "glide-" + this.f2185a + "-thread-" + this.d);
                this.d = this.d + 1;
            }
            return aVar;
        }
    }

    @DexIgnore
    public interface c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static final c f2186a = new a();
        @DexIgnore
        public static final c b = f2186a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements c {
            @DexIgnore
            @Override // com.fossil.le1.c
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        @DexIgnore
        void a(Throwable th);
    }

    @DexIgnore
    public le1(ExecutorService executorService) {
        this.b = executorService;
    }

    @DexIgnore
    public static int a() {
        if (d == 0) {
            d = Math.min(4, me1.a());
        }
        return d;
    }

    @DexIgnore
    public static a b() {
        int i = a() >= 4 ? 2 : 1;
        a aVar = new a(true);
        aVar.c(i);
        aVar.b("animation");
        return aVar;
    }

    @DexIgnore
    public static le1 c() {
        return b().a();
    }

    @DexIgnore
    public static a d() {
        a aVar = new a(true);
        aVar.c(1);
        aVar.b("disk-cache");
        return aVar;
    }

    @DexIgnore
    public static le1 e() {
        return d().a();
    }

    @DexIgnore
    public static a f() {
        a aVar = new a(false);
        aVar.c(a());
        aVar.b("source");
        return aVar;
    }

    @DexIgnore
    public static le1 g() {
        return f().a();
    }

    @DexIgnore
    public static le1 h() {
        return new le1(new ThreadPoolExecutor(0, Integer.MAX_VALUE, c, TimeUnit.MILLISECONDS, new SynchronousQueue(), new b("source-unlimited", c.b, false)));
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.b.awaitTermination(j, timeUnit);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.b.invokeAll(collection);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.b.invokeAll(collection, j, timeUnit);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return (T) this.b.invokeAny(collection);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return (T) this.b.invokeAny(collection, j, timeUnit);
    }

    @DexIgnore
    public boolean isShutdown() {
        return this.b.isShutdown();
    }

    @DexIgnore
    public boolean isTerminated() {
        return this.b.isTerminated();
    }

    @DexIgnore
    public void shutdown() {
        this.b.shutdown();
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        return this.b.shutdownNow();
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        return this.b.submit(runnable);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.b.submit(runnable, t);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Callable<T> callable) {
        return this.b.submit(callable);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
