package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m63 implements j63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2308a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f2308a = hw2.d("measurement.sdk.referrer.delayed_install_referrer_api", false);
        hw2.b("measurement.id.sdk.referrer.delayed_install_referrer_api", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.j63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.j63
    public final boolean zzb() {
        return f2308a.o().booleanValue();
    }
}
