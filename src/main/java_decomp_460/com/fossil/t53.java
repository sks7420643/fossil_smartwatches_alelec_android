package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t53 implements xw2<w53> {
    @DexIgnore
    public static t53 c; // = new t53();
    @DexIgnore
    public /* final */ xw2<w53> b;

    @DexIgnore
    public t53() {
        this(ww2.b(new v53()));
    }

    @DexIgnore
    public t53(xw2<w53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((w53) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ w53 zza() {
        return this.b.zza();
    }
}
