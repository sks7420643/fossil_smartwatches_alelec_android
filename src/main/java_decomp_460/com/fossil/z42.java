package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z42 extends q42 {
    @DexIgnore
    public /* final */ /* synthetic */ y42 b;

    @DexIgnore
    public z42(y42 y42) {
        this.b = y42;
    }

    @DexIgnore
    @Override // com.fossil.q42, com.fossil.e52
    public final void W(Status status) throws RemoteException {
        this.b.j(status);
    }
}
