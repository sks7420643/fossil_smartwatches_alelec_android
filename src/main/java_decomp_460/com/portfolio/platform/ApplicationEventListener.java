package com.portfolio.platform;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.bw7;
import com.fossil.ck5;
import com.fossil.co7;
import com.fossil.cs0;
import com.fossil.dr5;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.hu4;
import com.fossil.il7;
import com.fossil.iv7;
import com.fossil.jn5;
import com.fossil.jv7;
import com.fossil.k97;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.mj5;
import com.fossil.ms0;
import com.fossil.on5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.pr4;
import com.fossil.qn7;
import com.fossil.s77;
import com.fossil.so4;
import com.fossil.tl7;
import com.fossil.tt4;
import com.fossil.uo5;
import com.fossil.ux7;
import com.fossil.vp7;
import com.fossil.vt4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wt7;
import com.fossil.xw7;
import com.fossil.zo5;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApplicationEventListener implements cs0 {
    @DexIgnore
    public static /* final */ String I;
    @DexIgnore
    public static /* final */ a J; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutSettingRepository A;
    @DexIgnore
    public /* final */ pr4 B;
    @DexIgnore
    public /* final */ dr5 C;
    @DexIgnore
    public /* final */ ThemeRepository D;
    @DexIgnore
    public /* final */ s77 E;
    @DexIgnore
    public /* final */ uo5 F;
    @DexIgnore
    public /* final */ k97 G;
    @DexIgnore
    public /* final */ DianaAppSettingRepository H;
    @DexIgnore
    public /* final */ iv7 b; // = jv7.a(bw7.b().plus(ux7.b(null, 1, null)));
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ on5 d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ CategoryRepository f;
    @DexIgnore
    public /* final */ WatchAppRepository g;
    @DexIgnore
    public /* final */ ComplicationRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ RingStyleRepository j;
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ UserRepository l;
    @DexIgnore
    public /* final */ mj5 m;
    @DexIgnore
    public /* final */ AlarmsRepository s;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository t;
    @DexIgnore
    public /* final */ WatchLocalizationRepository u;
    @DexIgnore
    public /* final */ hu4 v;
    @DexIgnore
    public /* final */ zt4 w;
    @DexIgnore
    public /* final */ tt4 x;
    @DexIgnore
    public /* final */ FileRepository y;
    @DexIgnore
    public /* final */ vt4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ApplicationEventListener.I;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ApplicationEventListener", f = "ApplicationEventListener.kt", l = {128, 129, 130, 131, 134, 135, 136, 139, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 141, 142, 143, 144, 150, 157, 162, 163, DateTimeConstants.HOURS_PER_WEEK, 169, 170, 171, 172, 173, 174, 175, 176, 179, 180, 181}, m = "downloadResources")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ApplicationEventListener applicationEventListener, qn7 qn7) {
            super(qn7);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$1", f = "ApplicationEventListener.kt", l = {92, 93}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ApplicationEventListener applicationEventListener, qn7 qn7) {
            super(2, qn7);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r3 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0039
                if (r0 == r3) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r6)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r6)
            L_0x0027:
                com.portfolio.platform.ApplicationEventListener r2 = r5.this$0
                com.portfolio.platform.PortfolioApp r2 = com.portfolio.platform.ApplicationEventListener.c(r2)
                r5.L$0 = r0
                r5.label = r4
                java.lang.Object r0 = r2.d1(r5)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0039:
                com.fossil.el7.b(r6)
                com.fossil.iv7 r0 = r5.p$
                com.portfolio.platform.ApplicationEventListener r2 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r2 = com.portfolio.platform.ApplicationEventListener.e(r2)
                r5.L$0 = r0
                r5.label = r3
                java.lang.Object r2 = r2.initializeLocalTheme(r5)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2", f = "ApplicationEventListener.kt", l = {101, 102, 103, 108, 119}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMigrationComplete;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ApplicationEventListener applicationEventListener, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = applicationEventListener;
            this.$isMigrationComplete = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$isMigrationComplete, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00af  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x010a  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0157  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x015f  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 360
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = so4.INSTANCE.getClass().getSimpleName();
        pq7.b(simpleName, "ApplicationEventListener\u2026lass.javaClass.simpleName");
        I = simpleName;
    }
    */

    @DexIgnore
    public ApplicationEventListener(PortfolioApp portfolioApp, on5 on5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, RingStyleRepository ringStyleRepository, DeviceRepository deviceRepository, UserRepository userRepository, mj5 mj5, AlarmsRepository alarmsRepository, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, hu4 hu4, zt4 zt4, tt4 tt4, FileRepository fileRepository, vt4 vt4, WorkoutSettingRepository workoutSettingRepository, pr4 pr4, dr5 dr5, ThemeRepository themeRepository, s77 s77, uo5 uo5, zo5 zo5, k97 k97, DianaAppSettingRepository dianaAppSettingRepository) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(on5, "mSharedPrefs");
        pq7.c(hybridPresetRepository, "mHybridPresetRepository");
        pq7.c(categoryRepository, "mCategoryRepository");
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(dianaPresetRepository, "mDianaPresetRepository");
        pq7.c(ringStyleRepository, "mRingStyleRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(alarmsRepository, "mAlarmRepository");
        pq7.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        pq7.c(watchLocalizationRepository, "watchLocalization");
        pq7.c(hu4, "profileRepository");
        pq7.c(zt4, "friendRepository");
        pq7.c(tt4, "challengeRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(vt4, "fcmRepository");
        pq7.c(workoutSettingRepository, "mWorkoutSettingRepository");
        pq7.c(pr4, "flagRepository");
        pq7.c(dr5, "buddyChallengeManager");
        pq7.c(themeRepository, "mThemeRepository");
        pq7.c(s77, "assetRepository");
        pq7.c(uo5, "dianaPresetRepository");
        pq7.c(zo5, "dianaRecommendedPresetRepository");
        pq7.c(k97, "wfBackgroundPhotoRepository");
        pq7.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        this.c = portfolioApp;
        this.d = on5;
        this.e = hybridPresetRepository;
        this.f = categoryRepository;
        this.g = watchAppRepository;
        this.h = complicationRepository;
        this.i = microAppRepository;
        this.j = ringStyleRepository;
        this.k = deviceRepository;
        this.l = userRepository;
        this.m = mj5;
        this.s = alarmsRepository;
        this.t = dianaWatchFaceRepository;
        this.u = watchLocalizationRepository;
        this.v = hu4;
        this.w = zt4;
        this.x = tt4;
        this.y = fileRepository;
        this.z = vt4;
        this.A = workoutSettingRepository;
        this.B = pr4;
        this.C = dr5;
        this.D = themeRepository;
        this.E = s77;
        this.F = uo5;
        this.G = k97;
        this.H = dianaAppSettingRepository;
    }

    @DexIgnore
    public final /* synthetic */ Object i(qn7<? super tl7> qn7) {
        FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync");
        String J2 = this.c.J();
        if (TextUtils.isEmpty(J2)) {
            FLogger.INSTANCE.getLocal().d(I, "User has no active device, skip auto sync");
            return tl7.f3441a;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = I;
        local.d(str, "Inside .autoSync lastSyncSuccess=" + this.d.B(this.c.J()));
        long currentTimeMillis = System.currentTimeMillis() - this.d.C(this.c.J());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = I;
        remote.i(component, session, J2, str2, "[App Open] Last sync OK interval " + currentTimeMillis);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = I;
        local2.d(str3, "Inside .autoSync, last sync interval " + currentTimeMillis);
        if (currentTimeMillis >= ((long) CommuteTimeService.A) || currentTimeMillis < 0) {
            PortfolioApp portfolioApp = this.c;
            if (portfolioApp.R(portfolioApp.J()) == CommunicateMode.OTA.getValue()) {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, device is ota, wait for the next time");
            } else if (this.c.z0() || this.d.X()) {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, start auto-sync.");
                boolean c2 = jn5.c(jn5.b, this.c, jn5.a.SET_BLE_COMMAND, false, false, false, null, 56, null);
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String str4 = I;
                remote2.i(component2, session2, J2, str4, "[App Open] [Sync Start] AUTO SYNC isBlueToothEnabled " + BluetoothUtils.isBluetoothEnable() + ' ');
                if (c2) {
                    this.c.S1(this.m, false, 10);
                    return tl7.f3441a;
                }
                FLogger.INSTANCE.getLocal().d(I, "autoSync fail due to lack of permission");
            } else {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, doesn't start auto-sync.");
            }
        }
        return tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0351  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0373  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0376  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0385  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x039a  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x03b3  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x03c9  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x03ea  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x03ed  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0431  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0434  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0452  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0455  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0473  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0476  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0495  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0498  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0509  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x052e  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0531  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x053a  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x053f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0548  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x054c  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x054f  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0555  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0559  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0295  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02bd  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x02dd  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0310  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0332  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object j(java.lang.String r13, boolean r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 1438
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.j(java.lang.String, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void k() {
        Object systemService = this.c.getSystemService("accessibility");
        if (systemService != null) {
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = ((AccessibilityManager) systemService).getEnabledAccessibilityServiceList(-1);
            StringBuilder sb = new StringBuilder();
            for (AccessibilityServiceInfo accessibilityServiceInfo : enabledAccessibilityServiceList) {
                pq7.b(accessibilityServiceInfo, "accessibility");
                String id = accessibilityServiceInfo.getId();
                pq7.b(id, "accessibility.id");
                sb.append((String) pm7.P(wt7.Y(id, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null)));
                sb.append(" ");
            }
            String sb2 = sb.toString();
            pq7.b(sb2, "enabledAccessibilityStringBuilder.toString()");
            if (sb2 != null) {
                String obj = wt7.u0(sb2).toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = I;
                local.d(str, "Enabled Accessibility: " + obj);
                ck5.f.g().j("accessibility_config_on_launch", obj);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new il7("null cannot be cast to non-null type android.view.accessibility.AccessibilityManager");
    }

    @DexIgnore
    @ms0(Lifecycle.a.ON_STOP)
    public final void onAppEnterBackground() {
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.c.J(), I, "[App Close] User put app in background");
    }

    @DexIgnore
    @ms0(Lifecycle.a.ON_START)
    public final void onAppEnterForeground() {
        Log.d(I, "ApplicationEventListener onAppEnterForeground");
        xw7 unused = gu7.d(this.b, null, null, new c(this, null), 3, null);
        boolean k0 = this.d.k0(this.c.P());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = I;
        local.d(str, "onAppEnterForeground isMigrationComplete " + k0);
        if (k0) {
            xw7 unused2 = gu7.d(this.b, null, null, new d(this, k0, null), 3, null);
        }
    }
}
