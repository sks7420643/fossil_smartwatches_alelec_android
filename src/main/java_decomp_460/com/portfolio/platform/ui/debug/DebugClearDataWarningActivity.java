package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.portfolio.platform.PortfolioApp;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DebugClearDataWarningActivity extends AppCompatActivity {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a")
        /* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a  reason: collision with other inner class name */
        public class RunnableC0354a implements Runnable {
            @DexIgnore
            public RunnableC0354a(a aVar) {
            }

            @DexIgnore
            public void run() {
                PortfolioApp.d0.u();
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            Toast.makeText(DebugClearDataWarningActivity.this, "The app is being restarted...", 1).show();
            new Handler().postDelayed(new RunnableC0354a(this), 1500);
        }
    }

    @DexIgnore
    public static void d(Context context) {
        Intent intent = new Intent(context, DebugClearDataWarningActivity.class);
        intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        context.startActivity(intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558431);
        ((Button) findViewById(2131361974)).setOnClickListener(new a());
    }
}
