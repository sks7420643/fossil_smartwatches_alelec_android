package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ls5;
import com.portfolio.platform.PortfolioApp;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LogcatActivity extends ls5 {
    @DexIgnore
    public static boolean D;
    @DexIgnore
    public c A;
    @DexIgnore
    public RecyclerView B;
    @DexIgnore
    public AsyncTask<Void, Void, Void> C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements AdapterView.OnItemSelectedListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemSelectedListener
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            LogcatActivity.this.L(i);
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemSelectedListener
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Void, Void, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4721a;
        @DexIgnore
        public /* final */ c b;

        @DexIgnore
        public b(int i, c cVar) {
            this.f4721a = i;
            this.b = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            int i;
            String str = "";
            try {
                if (this.f4721a == 0) {
                    str = "logcat -v time -d " + PortfolioApp.d0.getPackageName() + ":V";
                } else if (this.f4721a == 1) {
                    str = "logcat -v time -d ButtonService:V BaseProfile:V TrackerProfile:V LinkProfile:V *:S";
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(str).getInputStream()));
                StringBuilder sb = new StringBuilder();
                loop0:
                while (true) {
                    i = 0;
                    do {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break loop0;
                        }
                        i++;
                        sb.insert(0, readLine + "<br>");
                    } while (i < 50);
                    this.b.g(LogcatActivity.M(sb.toString()));
                    sb.setLength(0);
                    sb = new StringBuilder();
                }
                if (i > 0) {
                    this.b.g(LogcatActivity.M(sb.toString()));
                }
                Runtime.getRuntime().exec("logcat -c");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(Void r2) {
            LogcatActivity.D = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<String> f4722a; // = new ArrayList();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String b;

            @DexIgnore
            public a(String str) {
                this.b = str;
            }

            @DexIgnore
            public void run() {
                if (!c.this.f4722a.contains(this.b)) {
                    c.this.f4722a.add(0, this.b);
                    c.this.notifyItemInserted(0);
                    LogcatActivity.this.B.scrollToPosition(0);
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void g(String str) {
            if (!LogcatActivity.this.isFinishing() && !LogcatActivity.this.isDestroyed()) {
                LogcatActivity.this.runOnUiThread(new a(str));
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            return this.f4722a.size();
        }

        @DexIgnore
        public void h() {
            this.f4722a.clear();
            notifyDataSetChanged();
        }

        @DexIgnore
        /* renamed from: i */
        public void onBindViewHolder(d dVar, int i) {
            String str = this.f4722a.get(i);
            if (str != null && dVar != null) {
                dVar.f4723a.setText(Html.fromHtml(str));
            }
        }

        @DexIgnore
        /* renamed from: j */
        public d onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new d(LayoutInflater.from(LogcatActivity.this).inflate(2131558735, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f4723a;

        @DexIgnore
        public d(View view) {
            super(view);
            this.f4723a = (TextView) view.findViewById(2131363357);
        }
    }

    @DexIgnore
    public static String M(String str) {
        Pattern compile = Pattern.compile("(E/[A-Za-z0-9_-]+)");
        Pattern compile2 = Pattern.compile("(I/[A-Za-z0-9_-]+)");
        Pattern compile3 = Pattern.compile("(W/[A-Za-z0-9_-]+)");
        StringBuffer stringBuffer = new StringBuffer(str.length());
        Matcher matcher = compile.matcher(str);
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, "<font color=\"#d8152a\">" + matcher.group(1) + "</font>");
        }
        matcher.appendTail(stringBuffer);
        Matcher matcher2 = compile2.matcher(stringBuffer);
        StringBuffer stringBuffer2 = new StringBuffer(str.length());
        while (matcher2.find()) {
            matcher2.appendReplacement(stringBuffer2, "<font color=\"#46a924\">" + matcher2.group(1) + "</font>");
        }
        matcher2.appendTail(stringBuffer2);
        Matcher matcher3 = compile3.matcher(stringBuffer2);
        StringBuffer stringBuffer3 = new StringBuffer(str.length());
        while (matcher3.find()) {
            matcher3.appendReplacement(stringBuffer3, "<font color=\"#f0da23\">" + matcher3.group(1) + "</font>");
        }
        matcher3.appendTail(stringBuffer3);
        return stringBuffer3.toString();
    }

    @DexIgnore
    public static void N(Context context) {
        context.startActivity(new Intent(context, LogcatActivity.class));
    }

    @DexIgnore
    public void L(int i) {
        if (!D) {
            this.A.h();
            D = true;
            Toast.makeText(this, "Collecting log...", 0).show();
            b bVar = new b(i, this.A);
            this.C = bVar;
            bVar.execute(new Void[0]);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558432);
        Spinner spinner = (Spinner) findViewById(2131363135);
        spinner.getBackground().setColorFilter(Color.parseColor("#AA7744"), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter<CharSequence> createFromResource = ArrayAdapter.createFromResource(this, 2130903041, 17367048);
        createFromResource.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter) createFromResource);
        spinner.setOnItemSelectedListener(new a());
        RecyclerView recyclerView = (RecyclerView) findViewById(2131362839);
        this.B = recyclerView;
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.E2(1);
        this.B.setLayoutManager(linearLayoutManager);
        c cVar = new c();
        this.A = cVar;
        this.B.setAdapter(cVar);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onStop() {
        super.onStop();
        AsyncTask<Void, Void, Void> asyncTask = this.C;
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
    }
}
