package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.k37;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepDaySummary extends View {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public RectF m;
    @DexIgnore
    public RectF s;
    @DexIgnore
    public RectF t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public RectF v;
    @DexIgnore
    public /* final */ PorterDuffXfermode w;
    @DexIgnore
    public /* final */ PorterDuffXfermode x;

    @DexIgnore
    public OverviewSleepDaySummary(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        this.b = "OverviewSleepDaySummary";
        this.m = new RectF();
        this.s = new RectF();
        this.t = new RectF();
        this.u = new Paint(1);
        this.v = new RectF();
        this.w = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        this.x = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
        if (!(attributeSet == null || context == null || (theme = context.getTheme()) == null || (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, sq4.OverviewSleepDaySummary, 0, 0)) == null)) {
            try {
                this.h = obtainStyledAttributes.getColor(3, 0);
                this.i = obtainStyledAttributes.getColor(4, 0);
                this.j = obtainStyledAttributes.getColor(0, 0);
                this.k = (float) obtainStyledAttributes.getDimensionPixelSize(2, 5);
                this.l = (float) obtainStyledAttributes.getDimensionPixelSize(1, 30);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.b;
                local.d(str, "constructor - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        b();
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, int i2) {
        this.u.setColor(i2);
        this.u.setXfermode(this.x);
        canvas.drawRect(rectF, this.u);
    }

    @DexIgnore
    public final void b() {
        this.u.setAntiAlias(true);
        this.u.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final void c(float f2, float f3, float f4, int i2, int i3, int i4) {
        this.e = f2;
        this.f = f3;
        this.g = f4;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        invalidate();
    }

    @DexIgnore
    public final String getTAG() {
        return this.b;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (canvas != null) {
            this.u.setColor(0);
            this.u.setXfermode(this.w);
            RectF rectF = this.v;
            float f2 = rectF.left;
            float f3 = rectF.top;
            float f4 = rectF.right;
            float f5 = rectF.bottom;
            float f6 = this.k;
            k37.a(canvas, f2, f3, f4, f5, f6, f6, true, true, true, true, this.u);
            a(canvas, this.m, this.h);
            a(canvas, this.s, this.i);
            a(canvas, this.t, this.j);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        setMeasuredDimension(size, size2);
        float f2 = (float) size;
        float f3 = this.l;
        float f4 = f2 - (((float) 2) * f3);
        this.c = this.e * f4;
        this.d = f4 * this.f;
        float f5 = (float) size2;
        this.v.set(f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f3, f5);
        RectF rectF = this.m;
        float f6 = this.l;
        rectF.set(f6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.c + f6, f5);
        RectF rectF2 = this.s;
        float f7 = this.l;
        float f8 = this.c;
        rectF2.set(f7 + f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7 + f8 + this.d, f5);
        RectF rectF3 = this.t;
        float f9 = this.l;
        rectF3.set(this.c + f9 + this.d, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f9, f5);
    }
}
