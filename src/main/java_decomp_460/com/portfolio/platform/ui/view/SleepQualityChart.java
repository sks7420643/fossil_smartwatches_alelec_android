package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.sq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepQualityChart extends View {
    @DexIgnore
    public Paint b; // = new Paint(1);
    @DexIgnore
    public float c; // = 10.0f;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public Bitmap g;
    @DexIgnore
    public int h; // = 50;

    @DexIgnore
    public SleepQualityChart(Context context) {
        super(context, null);
    }

    @DexIgnore
    public SleepQualityChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, sq4.SleepQualityChart) : null;
        if (obtainStyledAttributes != null) {
            this.c = obtainStyledAttributes.getDimension(4, 10.0f);
            this.d = obtainStyledAttributes.getColor(1, 0);
            this.e = obtainStyledAttributes.getColor(0, 0);
            this.f = obtainStyledAttributes.getDrawable(3);
            this.h = obtainStyledAttributes.getInt(2, 0);
        }
        a(this.c);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) this.f;
        this.g = bitmapDrawable != null ? bitmapDrawable.getBitmap() : null;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public final void a(float f2) {
        this.b.setDither(true);
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth(f2);
        this.b.setAntiAlias(true);
    }

    @DexIgnore
    public final void b(float f2) {
        this.b.setShader(new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.d, this.e, Shader.TileMode.CLAMP));
        this.b.setPathEffect(new DashPathEffect(new float[]{f2 / ((float) 5), 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        b((float) getWidth());
        if (canvas != null) {
            Bitmap bitmap = this.g;
            if (bitmap != null) {
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float width2 = ((float) (canvas.getWidth() * this.h)) / 100.0f;
                if (width2 + width > ((float) canvas.getWidth())) {
                    width2 = ((float) canvas.getWidth()) - width;
                }
                canvas.drawBitmap(bitmap, width2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.b);
                f2 = height;
            } else {
                f2 = 0.0f;
            }
            float f3 = 2.0f + f2 + (this.c / ((float) 2));
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, (float) canvas.getWidth(), f3, this.b);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r4 != 1073741824) goto L_0x0029;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = 0
            android.graphics.Bitmap r1 = r7.g
            if (r1 == 0) goto L_0x000c
            r1.getHeight()
        L_0x000c:
            int r3 = android.view.View.MeasureSpec.getMode(r8)
            int r2 = android.view.View.MeasureSpec.getSize(r8)
            int r4 = android.view.View.MeasureSpec.getMode(r9)
            int r1 = android.view.View.MeasureSpec.getSize(r9)
            if (r3 == r5) goto L_0x0023
            if (r3 == 0) goto L_0x0023
            if (r3 == r6) goto L_0x0023
            r2 = r0
        L_0x0023:
            if (r4 == r5) goto L_0x003c
            if (r4 == 0) goto L_0x002d
            if (r4 == r6) goto L_0x003c
        L_0x0029:
            r7.setMeasuredDimension(r2, r0)
            return
        L_0x002d:
            android.graphics.Bitmap r1 = r7.g
            if (r1 == 0) goto L_0x0035
            int r0 = r1.getHeight()
        L_0x0035:
            float r1 = r7.c
            int r1 = (int) r1
            int r1 = r1 * 2
            int r0 = r0 + r1
            goto L_0x0029
        L_0x003c:
            r0 = r1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.view.SleepQualityChart.onMeasure(int, int):void");
    }

    @DexIgnore
    public final void setPercent(int i) {
        this.h = i;
        invalidate();
    }
}
