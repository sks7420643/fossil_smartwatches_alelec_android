package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.dl5;
import com.fossil.hr7;
import com.fossil.pq7;
import com.fossil.um5;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepWeekChart extends OverviewWeekChart {
    @DexIgnore
    public OverviewSleepWeekChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart
    public String M(int i) {
        hr7 hr7 = hr7.f1520a;
        String c = um5.c(getContext(), 2131886647);
        pq7.b(c, "LanguageHelper.getString\u2026eep7days_Label__NumberHr)");
        String format = String.format(c, Arrays.copyOf(new Object[]{dl5.b(((float) i) / ((float) 60), 1).toString()}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public float q(float f) {
        return ((f - (getMBarWidth() * ((float) getMNumberBar()))) - ((float) 20)) / ((float) (getMNumberBar() - 1));
    }
}
