package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.fossil.gl0;
import com.fossil.h57;
import com.fossil.hm7;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.l47;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleTextView extends AppCompatTextView {
    @DexIgnore
    public static /* final */ ArrayList<String> m; // = hm7.c("nonBrandSurface", "primaryColor");
    @DexIgnore
    public static /* final */ ArrayList<String> s; // = hm7.c("primaryText", "nonBrandSwitchDisabledGray");
    @DexIgnore
    public static /* final */ int t; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final int a() {
            return FlexibleTextView.t;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context) {
        super(context);
        pq7.c(context, "context");
        j(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        j(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        j(attributeSet);
    }

    @DexIgnore
    public final String i(int i2) {
        String c = um5.c(PortfolioApp.h0.c(), i2);
        pq7.b(c, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return c;
    }

    @DexIgnore
    @SuppressLint({"ResourceType"})
    public final void j(AttributeSet attributeSet) {
        CharSequence charSequence;
        CharSequence charSequence2;
        this.j = 0;
        CharSequence text = getText();
        CharSequence hint = getHint();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, sq4.FlexibleTextView);
            this.j = obtainStyledAttributes.getInt(5, 0);
            this.k = obtainStyledAttributes.getInt(2, 0);
            String string = obtainStyledAttributes.getString(6);
            if (string == null) {
                string = "";
            }
            this.l = string;
            String string2 = obtainStyledAttributes.getString(3);
            if (string2 == null) {
                string2 = "";
            }
            this.f = string2;
            String string3 = obtainStyledAttributes.getString(4);
            if (string3 == null) {
                string3 = "";
            }
            this.g = string3;
            String string4 = obtainStyledAttributes.getString(0);
            if (string4 == null) {
                string4 = "";
            }
            this.h = string4;
            this.i = obtainStyledAttributes.getInt(1, 0);
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            charSequence2 = resourceId != -1 ? i(resourceId) : text;
            int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
            charSequence = resourceId2 != -1 ? i(resourceId2) : hint;
            obtainStyledAttributes2.recycle();
        } else {
            charSequence = hint;
            charSequence2 = text;
        }
        if (!TextUtils.isEmpty(charSequence2)) {
            setText(charSequence2);
        }
        if (!TextUtils.isEmpty(charSequence)) {
            pq7.b(charSequence, "hint");
            setHint(n(charSequence, this.k));
        }
        if (TextUtils.isEmpty(this.l)) {
            this.l = "primaryColor";
            String d = qn5.l.a().d(this.l);
            if (d != null) {
                h57.b(this, Color.parseColor(d));
            }
        }
        if (TextUtils.isEmpty(this.f)) {
            this.f = "primaryText";
        }
        if (TextUtils.isEmpty(this.g)) {
            this.g = "nonBrandTextStyle2";
        }
        l(this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public final CharSequence k(CharSequence charSequence) {
        return n(charSequence, this.j);
    }

    @DexIgnore
    public final void l(String str, String str2, String str3, int i2) {
        GradientDrawable gradientDrawable;
        LayerDrawable layerDrawable;
        String d = qn5.l.a().d(str);
        Typeface f2 = qn5.l.a().f(str2);
        String d2 = qn5.l.a().d(str3);
        if (d != null) {
            setTextColor(Color.parseColor(d));
        }
        if (f2 != null) {
            setTypeface(f2);
        }
        if (d2 != null) {
            if (i2 == 1) {
                Drawable f3 = gl0.f(getContext(), 2131230846);
                if (f3 != null) {
                    LayerDrawable layerDrawable2 = (LayerDrawable) f3;
                    gradientDrawable = (GradientDrawable) layerDrawable2.findDrawableByLayerId(2131363115);
                    layerDrawable = layerDrawable2;
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            } else if (i2 != 2) {
                gradientDrawable = null;
                layerDrawable = null;
            } else {
                Drawable f4 = gl0.f(getContext(), 2131230862);
                if (f4 != null) {
                    LayerDrawable layerDrawable3 = (LayerDrawable) f4;
                    gradientDrawable = (GradientDrawable) layerDrawable3.findDrawableByLayerId(2131363115);
                    layerDrawable = layerDrawable3;
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            }
            if (gradientDrawable != null && layerDrawable != null) {
                gradientDrawable.setColor(Color.parseColor(d2));
                gradientDrawable.setStroke(1, Color.parseColor(d2));
                setBackground(layerDrawable);
            }
        }
    }

    @DexIgnore
    public final void m(String str) {
        pq7.c(str, "state");
        int hashCode = str.hashCode();
        if (hashCode != -227352252) {
            if (hashCode == 636108957 && str.equals("flexible_tv_unselected")) {
                String str2 = s.get(0);
                pq7.b(str2, "UNSELECTED_TV_STYLES[0]");
                String str3 = this.g;
                String str4 = s.get(1);
                pq7.b(str4, "UNSELECTED_TV_STYLES[1]");
                l(str2, str3, str4, this.i);
            }
        } else if (str.equals("flexible_tv_selected")) {
            String str5 = m.get(0);
            pq7.b(str5, "SELECTED_TV_STYLES[0]");
            String str6 = this.g;
            String str7 = m.get(1);
            pq7.b(str7, "SELECTED_TV_STYLES[1]");
            l(str5, str6, str7, this.i);
        }
    }

    @DexIgnore
    public final CharSequence n(CharSequence charSequence, int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? i2 != 4 ? i2 != 5 ? charSequence : l47.c(charSequence) : l47.e(charSequence) : l47.d(charSequence) : l47.b(charSequence) : l47.a(charSequence);
    }

    @DexIgnore
    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        pq7.c(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(k(charSequence), bufferType);
    }
}
