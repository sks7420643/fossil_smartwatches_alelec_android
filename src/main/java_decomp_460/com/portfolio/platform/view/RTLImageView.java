package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.qn5;
import com.fossil.sq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLImageView extends AppCompatImageView {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public Boolean e; // = Boolean.TRUE;

    @DexIgnore
    public RTLImageView(Context context) {
        super(context);
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b(attributeSet);
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        b(attributeSet);
    }

    @DexIgnore
    public final void b(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, sq4.RTLImageView);
            this.d = obtainStyledAttributes.getString(0);
            this.e = Boolean.valueOf(obtainStyledAttributes.getBoolean(1, true));
            obtainStyledAttributes.recycle();
        }
        d();
    }

    @DexIgnore
    public final void d() {
        if (this.e.booleanValue()) {
            if (TextUtils.isEmpty(this.d)) {
                this.d = "primaryIconColor";
            }
            String d2 = qn5.l.a().d(this.d);
            if (!TextUtils.isEmpty(d2)) {
                setColorFilter(Color.parseColor(d2));
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            super.setImageDrawable(drawable);
        }
    }
}
