package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.gl0;
import com.fossil.hl7;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.sq4;
import com.fossil.tj5;
import com.misfit.frameworks.buttonservice.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppControl extends ConstraintLayout {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public String D;
    @DexIgnore
    public String E;
    @DexIgnore
    public int F;
    @DexIgnore
    public String G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public /* final */ ColorDrawable M; // = new ColorDrawable(gl0.d(getContext(), R.color.transparent));
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public TextView x;
    @DexIgnore
    public TextView y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppControl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray typedArray = null;
        int parseColor = Color.parseColor("#FFFF00");
        this.z = parseColor;
        this.F = parseColor;
        this.H = parseColor;
        this.I = parseColor;
        this.J = parseColor;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558845, (ViewGroup) this, true) : null;
        if (inflate != null) {
            View findViewById = inflate.findViewById(2131362726);
            pq7.b(findViewById, "view.findViewById(R.id.iv_icon)");
            this.w = (ImageView) findViewById;
            View findViewById2 = inflate.findViewById(2131363327);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_end)");
            this.x = (TextView) findViewById2;
            View findViewById3 = inflate.findViewById(2131363286);
            pq7.b(findViewById3, "view.findViewById(R.id.tv_bottom)");
            this.y = (TextView) findViewById3;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, sq4.WatchAppControl) : typedArray;
            if (typedArray != null) {
                this.A = typedArray.getDrawable(8);
                this.B = typedArray.getDrawable(9);
                this.C = typedArray.getDrawable(10);
                this.E = typedArray.getString(5);
                this.F = typedArray.getColor(6, this.z);
                typedArray.getDimension(7, -1.0f);
                this.G = typedArray.getString(0);
                this.H = typedArray.getColor(1, this.z);
                typedArray.getDimension(2, -1.0f);
                this.I = typedArray.getColor(3, this.z);
                this.J = typedArray.getColor(4, this.z);
                this.K = typedArray.getBoolean(12, false);
                boolean z2 = typedArray.getBoolean(11, false);
                this.L = z2;
                setRemoveMode(z2);
                typedArray.recycle();
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setIconSrc(Drawable drawable) {
        this.C = drawable;
        if (drawable != null) {
            this.w.setImageDrawable(drawable);
            this.w.setImageTintList(ColorStateList.valueOf(this.J));
        }
    }

    @DexIgnore
    private final void setRemoveMode(boolean z2) {
        this.L = z2;
        if (z2) {
            this.w.setVisibility(8);
            this.x.setVisibility(8);
            this.y.setVisibility(8);
            return;
        }
        G();
    }

    @DexIgnore
    public final void G() {
        if (!TextUtils.isEmpty(this.D)) {
            hl7.a(tj5.b(this.w).t(this.D).U0(), this.w);
        } else {
            Drawable drawable = this.C;
            if (drawable != null) {
                this.w.setImageDrawable(drawable);
                this.w.setImageTintList(ColorStateList.valueOf(this.J));
            }
        }
        if (!TextUtils.isEmpty(this.E)) {
            this.x.setText(this.E);
            this.x.setVisibility(0);
        } else {
            this.x.setVisibility(8);
        }
        if (!TextUtils.isEmpty(this.G)) {
            this.y.setText(this.G);
            this.y.setVisibility(0);
        } else {
            this.y.setVisibility(8);
        }
        if (this.K) {
            ImageView imageView = this.w;
            Drawable drawable2 = this.B;
            if (drawable2 == null) {
                drawable2 = this.A;
            }
            if (drawable2 == null) {
                drawable2 = this.M;
            }
            imageView.setBackground(drawable2);
            int i = this.J;
            int i2 = this.z;
            if (i != i2) {
                this.x.setTextColor(i);
            } else {
                int i3 = this.I;
                if (i3 != i2) {
                    this.x.setTextColor(i3);
                } else {
                    int i4 = this.F;
                    if (i4 != i2) {
                        this.x.setTextColor(i4);
                    }
                }
            }
            int i5 = this.J;
            int i6 = this.z;
            if (i5 != i6) {
                this.y.setTextColor(i5);
                return;
            }
            int i7 = this.I;
            if (i7 != i6) {
                this.y.setTextColor(i7);
                return;
            }
            int i8 = this.H;
            if (i8 != i6) {
                this.y.setTextColor(i8);
                return;
            }
            return;
        }
        ImageView imageView2 = this.w;
        Drawable drawable3 = this.A;
        if (drawable3 == null) {
            drawable3 = this.M;
        }
        imageView2.setBackground(drawable3);
        int i9 = this.I;
        int i10 = this.z;
        if (i9 != i10) {
            this.x.setTextColor(i9);
        } else {
            int i11 = this.F;
            if (i11 != i10) {
                this.x.setTextColor(i11);
            }
        }
        int i12 = this.I;
        int i13 = this.z;
        if (i12 != i13) {
            this.y.setTextColor(i12);
            return;
        }
        int i14 = this.H;
        if (i14 != i13) {
            this.y.setTextColor(i14);
        }
    }

    @DexIgnore
    public final void setDragMode(boolean z2) {
        if (z2) {
            setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else {
            setAlpha(1.0f);
        }
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.K = z2;
        setRemoveMode(this.L);
    }
}
