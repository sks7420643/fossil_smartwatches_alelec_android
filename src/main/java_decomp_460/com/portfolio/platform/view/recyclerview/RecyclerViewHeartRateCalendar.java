package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.e67;
import com.fossil.gl0;
import com.fossil.hr7;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.pl0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewHeartRateCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public View A;
    @DexIgnore
    public View B;
    @DexIgnore
    public TextView C;
    @DexIgnore
    public ConstraintLayout D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public String I;
    @DexIgnore
    public String J;
    @DexIgnore
    public String K;
    @DexIgnore
    public Calendar L;
    @DexIgnore
    public int M;
    @DexIgnore
    public GridLayoutManager w;
    @DexIgnore
    public e67 x;
    @DexIgnore
    public b y;
    @DexIgnore
    public c z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void k0(int i, Calendar calendar);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object next();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
            this.e = recyclerViewHeartRateCalendar;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int f(int i) {
            e67 mAdapter$app_fossilRelease = this.e.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                int itemViewType = mAdapter$app_fossilRelease.getItemViewType(i);
                return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView c;

        @DexIgnore
        public e(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar, RecyclerView recyclerView) {
            this.b = recyclerViewHeartRateCalendar;
            this.c = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            e67 mAdapter$app_fossilRelease = this.b.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                RecyclerView recyclerView = this.c;
                pq7.b(recyclerView, "recyclerView");
                mAdapter$app_fossilRelease.y(recyclerView.getMeasuredWidth() / 7);
                RecyclerView recyclerView2 = this.c;
                pq7.b(recyclerView2, "recyclerView");
                recyclerView2.setAdapter(this.b.getMAdapter$app_fossilRelease());
                RecyclerView recyclerView3 = this.c;
                pq7.b(recyclerView3, "recyclerView");
                recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    /*
    static {
        Color.parseColor("#FFFF00");
    }
    */

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        this.E = "";
        this.F = "";
        this.G = "";
        this.H = "";
        this.I = "";
        this.J = "";
        this.K = "";
        this.L = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.RecyclerViewHeartRateCalendar);
            try {
                String string = obtainStyledAttributes.getString(4);
                this.E = string == null ? "onDianaHeartRateTab" : string;
                String string2 = obtainStyledAttributes.getString(5);
                this.F = string2 == null ? "nonBrandDisableCalendarDay" : string2;
                String string3 = obtainStyledAttributes.getString(6);
                this.G = string3 == null ? "primaryText" : string3;
                String string4 = obtainStyledAttributes.getString(1);
                this.H = string4 == null ? "averageRestingHeartRate" : string4;
                String string5 = obtainStyledAttributes.getString(0);
                this.I = string5 == null ? "aboveAverageRestingHeartRate" : string5;
                String string6 = obtainStyledAttributes.getString(3);
                this.J = string6 == null ? "maxHeartRate" : string6;
                String string7 = obtainStyledAttributes.getString(2);
                this.K = string7 == null ? "nonBrandSurface" : string7;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("RecyclerViewHeartRateCalendar", "RecyclerViewHeartRateCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        I(context);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i, int i2, kq7 kq7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void G(int i) {
        c cVar = this.z;
        if (cVar == null) {
            return;
        }
        if (i != 2131362880) {
            if (i == 2131362961) {
                if (cVar != null) {
                    cVar.a();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        } else if (cVar != null) {
            cVar.next();
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final String H(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886856);
                pq7.b(c2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return c2;
            case 1:
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886855);
                pq7.b(c3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return c3;
            case 2:
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886859);
                pq7.b(c4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return c4;
            case 3:
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886852);
                pq7.b(c5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return c5;
            case 4:
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886860);
                pq7.b(c6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return c6;
            case 5:
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886858);
                pq7.b(c7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return c7;
            case 6:
                String c8 = um5.c(PortfolioApp.h0.c(), 2131886857);
                pq7.b(c8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return c8;
            case 7:
                String c9 = um5.c(PortfolioApp.h0.c(), 2131886853);
                pq7.b(c9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return c9;
            case 8:
                String c10 = um5.c(PortfolioApp.h0.c(), 2131886863);
                pq7.b(c10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return c10;
            case 9:
                String c11 = um5.c(PortfolioApp.h0.c(), 2131886862);
                pq7.b(c11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return c11;
            case 10:
                String c12 = um5.c(PortfolioApp.h0.c(), 2131886861);
                pq7.b(c12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return c12;
            case 11:
                String c13 = um5.c(PortfolioApp.h0.c(), 2131886854);
                pq7.b(c13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return c13;
            default:
                String c14 = um5.c(PortfolioApp.h0.c(), 2131886856);
                pq7.b(c14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return c14;
        }
    }

    @DexIgnore
    public final void I(Context context) {
        View inflate = View.inflate(context, 2131558839, this);
        this.D = (ConstraintLayout) inflate.findViewById(2131362124);
        this.C = (TextView) inflate.findViewById(2131362849);
        this.A = inflate.findViewById(2131362880);
        this.B = inflate.findViewById(2131362961);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(2131362197);
        this.x = new e67(context);
        int parseColor = Color.parseColor(qn5.l.a().d(this.I));
        int parseColor2 = Color.parseColor(qn5.l.a().d(this.H));
        int parseColor3 = Color.parseColor(qn5.l.a().d(this.E));
        int parseColor4 = Color.parseColor(qn5.l.a().d(this.F));
        int h = pl0.h(Color.parseColor(qn5.l.a().d(this.J)), 20);
        int parseColor5 = Color.parseColor(qn5.l.a().d(this.G));
        e67 e67 = this.x;
        if (e67 != null) {
            e67.w(parseColor3, parseColor4, parseColor5, parseColor2, parseColor, h);
            this.w = new RecyclerViewHeartRateCalendar$init$Anon1(context, context, 7, 0, true);
            String d2 = qn5.l.a().d(this.K);
            if (!TextUtils.isEmpty(d2)) {
                ConstraintLayout constraintLayout = this.D;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d2));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                ConstraintLayout constraintLayout2 = this.D;
                if (constraintLayout2 != null) {
                    constraintLayout2.setBackgroundColor(gl0.d(context, 2131099942));
                } else {
                    pq7.i();
                    throw null;
                }
            }
            GridLayoutManager gridLayoutManager = this.w;
            if (gridLayoutManager != null) {
                gridLayoutManager.h3(new d(this));
                pq7.b(recyclerView, "recyclerView");
                recyclerView.setLayoutManager(this.w);
                recyclerView.setItemAnimator(null);
                recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new e(this, recyclerView));
                View view = this.A;
                if (view != null) {
                    view.setOnClickListener(this);
                    View view2 = this.B;
                    if (view2 != null) {
                        view2.setOnClickListener(this);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void J() {
        Calendar calendar = null;
        e67 e67 = this.x;
        Calendar j = e67 != null ? e67.j() : null;
        e67 e672 = this.x;
        if (e672 != null) {
            calendar = e672.v();
        }
        b bVar = this.y;
        if (bVar != null) {
            Calendar calendar2 = this.L;
            pq7.b(calendar2, "chosenCalendar");
            bVar.a(calendar2);
        }
        L(calendar, j);
    }

    @DexIgnore
    public final void K(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        pq7.c(calendar, "currentCalendar");
        pq7.c(calendar2, "startCalendar");
        pq7.c(calendar3, "endCalendar");
        e67 e67 = this.x;
        if (e67 != null) {
            e67.D(calendar2);
            e67.z(calendar3);
            e67.B(calendar);
            e67.notifyDataSetChanged();
            L(calendar2, calendar3);
        }
    }

    @DexIgnore
    public final void L(Calendar calendar, Calendar calendar2) {
        int i = 8;
        if (calendar != null && calendar2 != null) {
            int i2 = this.L.get(2);
            int i3 = this.L.get(1);
            View view = this.B;
            if (view != null) {
                view.setVisibility((i2 == calendar.get(2) && i3 == calendar.get(1)) ? 8 : 0);
                View view2 = this.A;
                if (view2 != null) {
                    if (!(i2 == calendar2.get(2) && i3 == calendar2.get(1))) {
                        i = 0;
                    }
                    view2.setVisibility(i);
                    TextView textView = this.C;
                    if (textView != null) {
                        hr7 hr7 = hr7.f1520a;
                        Calendar calendar3 = this.L;
                        pq7.b(calendar3, "chosenCalendar");
                        String format = String.format("%s %s", Arrays.copyOf(new Object[]{H(calendar3), Integer.valueOf(i3)}, 2));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final e67 getMAdapter$app_fossilRelease() {
        return this.x;
    }

    @DexIgnore
    public void onClick(View view) {
        pq7.c(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == 2131362880) {
            this.M++;
        } else if (id == 2131362961) {
            this.M--;
        }
        e67 e67 = this.x;
        if (e67 != null) {
            this.L = lk5.w(this.M, e67.j());
        }
        J();
        G(view.getId());
    }

    @DexIgnore
    public final void setData(Map<Long, Integer> map) {
        int i = 0;
        pq7.c(map, "data");
        FLogger.INSTANCE.getLocal().d("RecyclerViewHeartRateCalendar", "setData dataSize=" + map.size());
        Collection<Integer> values = map.values();
        ArrayList arrayList = new ArrayList();
        for (T t : values) {
            if (t.intValue() > 0) {
                arrayList.add(t);
            }
        }
        Integer num = (Integer) pm7.T(arrayList);
        int intValue = num != null ? num.intValue() : 0;
        Collection<Integer> values2 = map.values();
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : values2) {
            if (t2.intValue() > 0) {
                arrayList2.add(t2);
            }
        }
        Integer num2 = (Integer) pm7.S(arrayList2);
        int intValue2 = num2 != null ? num2.intValue() : 100;
        if (intValue != intValue2) {
            i = intValue;
        } else if (intValue2 == 0) {
            intValue2 = 100;
        }
        e67 e67 = this.x;
        if (e67 != null) {
            Calendar calendar = this.L;
            pq7.b(calendar, "chosenCalendar");
            e67.x(map, i, intValue2, calendar);
        }
        e67 e672 = this.x;
        if (e672 != null) {
            e672.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(boolean z2) {
        View view = this.A;
        if (view != null) {
            view.setEnabled(z2);
        }
        View view2 = this.B;
        if (view2 != null) {
            view2.setEnabled(z2);
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        pq7.c(calendar, GoalPhase.COLUMN_END_DATE);
        this.L = lk5.w(this.M, calendar);
        e67 e67 = this.x;
        if (e67 != null) {
            e67.z(calendar);
            e67 e672 = this.x;
            if (e672 != null) {
                e672.notifyDataSetChanged();
                e67 e673 = this.x;
                if (e673 != null) {
                    L(e673.v(), calendar);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(e67 e67) {
        this.x = e67;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(a aVar) {
        pq7.c(aVar, "listener");
        e67 e67 = this.x;
        if (e67 != null) {
            e67.A(aVar);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(b bVar) {
        pq7.c(bVar, "listener");
        this.y = bVar;
    }

    @DexIgnore
    public final void setTintColor(int i) {
    }
}
