package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.d67;
import com.fossil.hr7;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String M; // = "RecyclerViewCalendar";
    @DexIgnore
    public static /* final */ int N; // = 7;
    @DexIgnore
    public View A;
    @DexIgnore
    public View B;
    @DexIgnore
    public TextView C;
    @DexIgnore
    public ConstraintLayout D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public String I;
    @DexIgnore
    public String J;
    @DexIgnore
    public int K;
    @DexIgnore
    public Calendar L;
    @DexIgnore
    public GridLayoutManager w;
    @DexIgnore
    public d67 x;
    @DexIgnore
    public b y;
    @DexIgnore
    public c z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void k0(int i, Calendar calendar);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object next();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(RecyclerViewCalendar recyclerViewCalendar) {
            this.e = recyclerViewCalendar;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int f(int i) {
            int itemViewType = this.e.getMAdapter$app_fossilRelease().getItemViewType(i);
            return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView c;

        @DexIgnore
        public e(RecyclerViewCalendar recyclerViewCalendar, RecyclerView recyclerView) {
            this.b = recyclerViewCalendar;
            this.c = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            d67 mAdapter$app_fossilRelease = this.b.getMAdapter$app_fossilRelease();
            RecyclerView recyclerView = this.c;
            pq7.b(recyclerView, "recyclerView");
            mAdapter$app_fossilRelease.l(recyclerView.getMeasuredWidth() / 7);
            RecyclerView recyclerView2 = this.c;
            pq7.b(recyclerView2, "recyclerView");
            recyclerView2.setAdapter(this.b.getMAdapter$app_fossilRelease());
            RecyclerView recyclerView3 = this.c;
            pq7.b(recyclerView3, "recyclerView");
            recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        this.E = "#FFFFFF";
        this.F = "#A7A7A7";
        this.G = "#CCCCCC";
        this.H = "#242424";
        this.I = "#FFFFFF";
        this.J = "#FFFFFF";
        this.L = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.RecyclerViewCalendar);
            try {
                qn5 a2 = qn5.l.a();
                String string = obtainStyledAttributes.getString(1);
                String d2 = a2.d(string == null ? "onDianaStepsTab" : string);
                if (d2 != null) {
                    this.E = d2;
                }
                qn5 a3 = qn5.l.a();
                String string2 = obtainStyledAttributes.getString(2);
                String d3 = a3.d(string2 == null ? "secondaryText" : string2);
                if (d3 != null) {
                    this.F = d3;
                }
                qn5 a4 = qn5.l.a();
                String string3 = obtainStyledAttributes.getString(3);
                String d4 = a4.d(string3 == null ? "nonBrandDisableCalendarDay" : string3);
                if (d4 != null) {
                    this.G = d4;
                }
                qn5 a5 = qn5.l.a();
                String string4 = obtainStyledAttributes.getString(5);
                String d5 = a5.d(string4 == null ? "primaryText" : string4);
                if (d5 != null) {
                    this.H = d5;
                }
                qn5 a6 = qn5.l.a();
                String string5 = obtainStyledAttributes.getString(4);
                String d6 = a6.d(string5 == null ? "onDianaStepsTab" : string5);
                if (d6 != null) {
                    this.I = d6;
                }
                qn5 a7 = qn5.l.a();
                String string6 = obtainStyledAttributes.getString(0);
                String d7 = a7.d(string6 == null ? "nonBrandSurface" : string6);
                if (d7 != null) {
                    this.J = d7;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = M;
                local.e(str, "RecyclerViewCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        I(context);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i, int i2, kq7 kq7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void G(int i) {
        c cVar = this.z;
        if (cVar == null) {
            return;
        }
        if (i != 2131362880) {
            if (i == 2131362961) {
                if (cVar != null) {
                    cVar.a();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        } else if (cVar != null) {
            cVar.next();
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final String H(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886856);
                pq7.b(c2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return c2;
            case 1:
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886855);
                pq7.b(c3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return c3;
            case 2:
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886859);
                pq7.b(c4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return c4;
            case 3:
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886852);
                pq7.b(c5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return c5;
            case 4:
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886860);
                pq7.b(c6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return c6;
            case 5:
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886858);
                pq7.b(c7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return c7;
            case 6:
                String c8 = um5.c(PortfolioApp.h0.c(), 2131886857);
                pq7.b(c8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return c8;
            case 7:
                String c9 = um5.c(PortfolioApp.h0.c(), 2131886853);
                pq7.b(c9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return c9;
            case 8:
                String c10 = um5.c(PortfolioApp.h0.c(), 2131886863);
                pq7.b(c10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return c10;
            case 9:
                String c11 = um5.c(PortfolioApp.h0.c(), 2131886862);
                pq7.b(c11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return c11;
            case 10:
                String c12 = um5.c(PortfolioApp.h0.c(), 2131886861);
                pq7.b(c12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return c12;
            case 11:
                String c13 = um5.c(PortfolioApp.h0.c(), 2131886854);
                pq7.b(c13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return c13;
            default:
                String c14 = um5.c(PortfolioApp.h0.c(), 2131886856);
                pq7.b(c14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return c14;
        }
    }

    @DexIgnore
    public final void I(Context context) {
        View inflate = View.inflate(context, 2131558839, this);
        this.D = (ConstraintLayout) inflate.findViewById(2131362124);
        this.C = (TextView) inflate.findViewById(2131362849);
        this.A = inflate.findViewById(2131362880);
        this.B = inflate.findViewById(2131362961);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(2131362197);
        d67 d67 = new d67(context);
        this.x = d67;
        if (d67 != null) {
            d67.j(Color.parseColor(this.E), Color.parseColor(this.F), Color.parseColor(this.G), Color.parseColor(this.H), Color.parseColor(this.I));
            ConstraintLayout constraintLayout = this.D;
            if (constraintLayout != null) {
                constraintLayout.setBackgroundColor(Color.parseColor(this.J));
                RecyclerViewCalendar$init$Anon1 recyclerViewCalendar$init$Anon1 = new RecyclerViewCalendar$init$Anon1(context, context, N, 0, true);
                this.w = recyclerViewCalendar$init$Anon1;
                if (recyclerViewCalendar$init$Anon1 != null) {
                    recyclerViewCalendar$init$Anon1.h3(new d(this));
                    pq7.b(recyclerView, "recyclerView");
                    recyclerView.setLayoutManager(this.w);
                    recyclerView.setItemAnimator(null);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new e(this, recyclerView));
                    View view = this.A;
                    if (view != null) {
                        view.setOnClickListener(this);
                        View view2 = this.B;
                        if (view2 != null) {
                            view2.setOnClickListener(this);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void J(String str) {
        pq7.c(str, "reachGoal");
        String d2 = qn5.l.a().d(str);
        if (d2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = M;
            local.d(str2, "invalidateStyle reachGoal=" + str + " color=" + d2);
            d67 d67 = this.x;
            if (d67 != null) {
                d67.j(Color.parseColor(this.E), Color.parseColor(this.F), Color.parseColor(this.G), Color.parseColor(this.H), Color.parseColor(this.I));
                d67 d672 = this.x;
                if (d672 != null) {
                    d672.notifyDataSetChanged();
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.n("mAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void K() {
        d67 d67 = this.x;
        if (d67 != null) {
            Calendar h = d67.h();
            d67 d672 = this.x;
            if (d672 != null) {
                Calendar i = d672.i();
                b bVar = this.y;
                if (bVar != null) {
                    if (bVar != null) {
                        Calendar calendar = this.L;
                        pq7.b(calendar, "chosenCalendar");
                        bVar.a(calendar);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                M(i, h);
                return;
            }
            pq7.n("mAdapter");
            throw null;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    public final void L(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        pq7.c(calendar, "currentCalendar");
        pq7.c(calendar2, "startCalendar");
        pq7.c(calendar3, "endCalendar");
        d67 d67 = this.x;
        if (d67 != null) {
            d67.p(calendar2);
            d67 d672 = this.x;
            if (d672 != null) {
                d672.m(calendar3);
                d67 d673 = this.x;
                if (d673 != null) {
                    d673.o(calendar);
                    M(calendar2, calendar3);
                    d67 d674 = this.x;
                    if (d674 != null) {
                        d674.notifyDataSetChanged();
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.n("mAdapter");
                throw null;
            }
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void M(Calendar calendar, Calendar calendar2) {
        int i = 8;
        if (calendar != null && calendar2 != null) {
            int i2 = this.L.get(2);
            int i3 = this.L.get(1);
            View view = this.B;
            if (view != null) {
                view.setVisibility((i2 == calendar.get(2) && i3 == calendar.get(1)) ? 8 : 0);
                View view2 = this.A;
                if (view2 != null) {
                    if (!(i2 == calendar2.get(2) && i3 == calendar2.get(1))) {
                        i = 0;
                    }
                    view2.setVisibility(i);
                    TextView textView = this.C;
                    if (textView != null) {
                        hr7 hr7 = hr7.f1520a;
                        Calendar calendar3 = this.L;
                        pq7.b(calendar3, "chosenCalendar");
                        String format = String.format("%s %s", Arrays.copyOf(new Object[]{H(calendar3), Integer.valueOf(i3)}, 2));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final d67 getMAdapter$app_fossilRelease() {
        d67 d67 = this.x;
        if (d67 != null) {
            return d67;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        pq7.c(view, "view");
        setEnableButtonNextAndPrevMonth(Boolean.FALSE);
        int id = view.getId();
        if (id == 2131362880) {
            this.K++;
        } else if (id == 2131362961) {
            this.K--;
        }
        int i = this.K;
        d67 d67 = this.x;
        if (d67 != null) {
            this.L = lk5.w(i, d67.h());
            K();
            G(view.getId());
            return;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    public final void setData(Map<Long, Float> map) {
        pq7.c(map, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = M;
        local.d(str, "setData dataSize=" + map.size());
        d67 d67 = this.x;
        if (d67 != null) {
            d67.k(map, this.L);
            d67 d672 = this.x;
            if (d672 != null) {
                d672.notifyDataSetChanged();
            } else {
                pq7.n("mAdapter");
                throw null;
            }
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(Boolean bool) {
        View view = this.A;
        if (view == null) {
            pq7.i();
            throw null;
        } else if (bool != null) {
            view.setEnabled(bool.booleanValue());
            View view2 = this.B;
            if (view2 != null) {
                view2.setEnabled(bool.booleanValue());
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        pq7.c(calendar, GoalPhase.COLUMN_END_DATE);
        this.L = lk5.w(this.K, calendar);
        d67 d67 = this.x;
        if (d67 != null) {
            d67.m(calendar);
            d67 d672 = this.x;
            if (d672 != null) {
                d672.notifyDataSetChanged();
                d67 d673 = this.x;
                if (d673 != null) {
                    M(d673.i(), calendar);
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.n("mAdapter");
                throw null;
            }
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(d67 d67) {
        pq7.c(d67, "<set-?>");
        this.x = d67;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(a aVar) {
        pq7.c(aVar, "listener");
        d67 d67 = this.x;
        if (d67 != null) {
            d67.n(aVar);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(b bVar) {
        this.y = bVar;
    }
}
