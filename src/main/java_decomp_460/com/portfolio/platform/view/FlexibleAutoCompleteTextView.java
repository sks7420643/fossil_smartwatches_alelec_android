package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import com.fossil.h57;
import com.fossil.l47;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    @DexIgnore
    public /* final */ String b; // = "FlexibleAutoCompleteTextView";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = FlexibleTextView.u.a();
    @DexIgnore
    public String h; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context) {
        super(context);
        pq7.c(context, "context");
        b(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    public final String a(int i) {
        String c2 = um5.c(PortfolioApp.h0.c(), i);
        pq7.b(c2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return c2;
    }

    @DexIgnore
    public final void b(AttributeSet attributeSet) {
        CharSequence charSequence;
        CharSequence charSequence2;
        Editable text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, sq4.FlexibleAutoCompleteTextView);
                obtainStyledAttributes.getInt(5, 0);
                this.f = obtainStyledAttributes.getInt(1, 0);
                this.g = obtainStyledAttributes.getColor(6, FlexibleTextView.u.a());
                String string = obtainStyledAttributes.getString(2);
                if (string == null) {
                    string = "";
                }
                this.c = string;
                String string2 = obtainStyledAttributes.getString(3);
                if (string2 == null) {
                    string2 = "";
                }
                this.d = string2;
                String string3 = obtainStyledAttributes.getString(0);
                if (string3 == null) {
                    string3 = "";
                }
                this.e = string3;
                String string4 = obtainStyledAttributes.getString(4);
                if (string4 == null) {
                    string4 = "";
                }
                this.h = string4;
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                charSequence2 = resourceId != -1 ? a(resourceId) : text;
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                charSequence = resourceId2 != -1 ? a(resourceId2) : hint;
                obtainStyledAttributes2.recycle();
            } else {
                charSequence = hint;
                charSequence2 = text;
            }
            if (!TextUtils.isEmpty(charSequence2)) {
                setText(charSequence2);
            }
            if (!TextUtils.isEmpty(charSequence)) {
                pq7.b(charSequence, "hint");
                setHint(d(charSequence, this.f));
            }
            if (this.g != FlexibleTextView.u.a()) {
                h57.b(this, this.g);
            }
            if (TextUtils.isEmpty(this.c)) {
                this.c = "primaryText";
            }
            if (TextUtils.isEmpty(this.d)) {
                this.d = "nonBrandTextStyle2";
            }
            c(this.c, this.d, this.e, this.h);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void c(String str, String str2, String str3, String str4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = this.b;
        local.d(str5, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String d2 = qn5.l.a().d(str);
        Typeface f2 = qn5.l.a().f(str2);
        String d3 = qn5.l.a().d(str3);
        String d4 = qn5.l.a().d(str4);
        if (d2 != null) {
            setTextColor(Color.parseColor(d2));
        }
        if (f2 != null) {
            setTypeface(f2);
        }
        if (d3 != null) {
            setBackgroundColor(Color.parseColor(d3));
        }
        if (d4 != null) {
            setHintTextColor(Color.parseColor(d4));
        }
    }

    @DexIgnore
    public final CharSequence d(CharSequence charSequence, int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? charSequence : l47.c(charSequence) : l47.e(charSequence) : l47.d(charSequence) : l47.b(charSequence) : l47.a(charSequence);
    }
}
