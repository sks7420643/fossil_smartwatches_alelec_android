package com.portfolio.platform.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.sq4;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CircleImageViewProgressBar extends AppCompatImageView {
    @DexIgnore
    public static /* final */ ImageView.ScaleType L; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public static /* final */ Bitmap.Config M; // = Bitmap.Config.ARGB_8888;
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public ValueAnimator D;
    @DexIgnore
    public ColorFilter E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public /* final */ Matrix g;
    @DexIgnore
    public /* final */ Paint h;
    @DexIgnore
    public /* final */ Paint i;
    @DexIgnore
    public /* final */ Paint j;
    @DexIgnore
    public /* final */ Paint k;
    @DexIgnore
    public float l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public Bitmap w;
    @DexIgnore
    public BitmapShader x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            CircleImageViewProgressBar.this.C = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            CircleImageViewProgressBar.this.invalidate();
        }
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context) {
        super(context);
        this.d = new RectF();
        this.e = new RectF();
        this.f = new RectF();
        this.g = new Matrix();
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.k = new Paint(1);
        this.m = -16777216;
        this.s = 0;
        this.t = 10;
        this.u = 0;
        this.v = -16776961;
        this.K = true;
        g();
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new RectF();
        this.e = new RectF();
        this.f = new RectF();
        this.g = new Matrix();
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.k = new Paint(1);
        this.m = -16777216;
        this.s = 0;
        this.t = 10;
        this.u = 0;
        this.v = -16776961;
        this.K = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.CircleImageViewProgressBar, i2, 0);
        this.t = obtainStyledAttributes.getDimensionPixelSize(5, 0);
        this.s = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.m = obtainStyledAttributes.getColor(3, -16777216);
        this.H = obtainStyledAttributes.getBoolean(4, false);
        this.I = obtainStyledAttributes.getBoolean(1, false);
        this.u = obtainStyledAttributes.getColor(7, 0);
        this.A = obtainStyledAttributes.getFloat(0, 0.805f);
        this.v = obtainStyledAttributes.getColor(6, -16776961);
        this.l = obtainStyledAttributes.getFloat(8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
        g();
    }

    @DexIgnore
    private int getBorderWidth() {
        return this.s;
    }

    @DexIgnore
    private int getProgressBorderWidth() {
        return this.t;
    }

    @DexIgnore
    public final void b() {
        this.h.setColorFilter(this.E);
    }

    @DexIgnore
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = (((float) (width - min)) / 2.0f) + ((float) getPaddingLeft());
        float paddingTop = (((float) (height - min)) / 2.0f) + ((float) getPaddingTop());
        int i2 = this.t;
        int i3 = this.s;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft + ((float) i3), (((float) i2) * 1.5f) + paddingTop + ((float) i3), ((paddingLeft + f2) - (((float) i2) * 1.5f)) - ((float) i3), ((paddingTop + f2) - (((float) i2) * 1.5f)) - ((float) i3));
    }

    @DexIgnore
    public final RectF e() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = (((float) (width - min)) / 2.0f) + ((float) getPaddingLeft());
        float paddingTop = (((float) (height - min)) / 2.0f) + ((float) getPaddingTop());
        int i2 = this.t;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft, (((float) i2) * 1.5f) + paddingTop, (paddingLeft + f2) - (((float) i2) * 1.5f), (paddingTop + f2) - (((float) i2) * 1.5f));
    }

    @DexIgnore
    public final Bitmap f(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            Bitmap createBitmap = drawable instanceof ColorDrawable ? Bitmap.createBitmap(2, 2, M) : Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), M);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return createBitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final void g() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, this.C);
        this.D = ofFloat;
        ofFloat.setDuration(800L);
        this.D.addUpdateListener(new a());
        super.setScaleType(L);
        this.F = true;
        if (this.G) {
            i();
            this.G = false;
        }
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.E;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.u;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return L;
    }

    @DexIgnore
    public final void h() {
        if (this.J) {
            this.w = null;
        } else {
            this.w = f(getDrawable());
        }
        i();
    }

    @DexIgnore
    public final void i() {
        int i2;
        if (!this.F) {
            this.G = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            if (this.w == null) {
                invalidate();
                return;
            }
            Bitmap bitmap = this.w;
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            this.x = new BitmapShader(bitmap, tileMode, tileMode);
            this.h.setAntiAlias(true);
            this.h.setShader(this.x);
            this.i.setStyle(Paint.Style.STROKE);
            this.i.setAntiAlias(true);
            this.i.setColor(this.m);
            this.i.setStrokeWidth((float) this.s);
            this.i.setStrokeCap(Paint.Cap.ROUND);
            this.j.setStyle(Paint.Style.STROKE);
            this.j.setAntiAlias(true);
            this.j.setColor(this.v);
            this.j.setStrokeWidth((float) this.t);
            this.j.setStrokeCap(Paint.Cap.ROUND);
            this.k.setStyle(Paint.Style.FILL);
            this.k.setAntiAlias(true);
            this.k.setColor(this.u);
            this.z = this.w.getHeight();
            this.y = this.w.getWidth();
            this.e.set(d());
            this.f.set(e());
            this.d.set(this.f);
            if (!this.H && (i2 = this.t) > 0) {
                this.d.inset((float) (i2 / 2), (float) (i2 / 2));
            }
            this.B = Math.min(this.d.height() / 2.0f, this.d.width() / 2.0f);
            if (this.A > 1.0f) {
                this.A = 1.0f;
            }
            this.B *= this.A;
            b();
            j();
            invalidate();
        }
    }

    @DexIgnore
    public final void j() {
        float width;
        float height;
        float f2;
        this.g.set(null);
        if (((float) this.y) * this.d.height() > this.d.width() * ((float) this.z)) {
            width = this.d.height() / ((float) this.z);
            f2 = (this.d.width() - (((float) this.y) * width)) * 0.5f;
            height = 0.0f;
        } else {
            width = this.d.width() / ((float) this.y);
            height = (this.d.height() - (((float) this.z) * width)) * 0.5f;
            f2 = 0.0f;
        }
        this.g.setScale(width, width);
        Matrix matrix = this.g;
        RectF rectF = this.d;
        matrix.postTranslate(((float) ((int) (f2 + 0.5f))) + rectF.left, rectF.top + ((float) ((int) (height + 0.5f))));
        BitmapShader bitmapShader = this.x;
        if (bitmapShader != null) {
            bitmapShader.setLocalMatrix(this.g);
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (this.J) {
            super.onDraw(canvas);
        } else if (this.w != null) {
            canvas.save();
            canvas.rotate(this.l - 90.0f, this.d.centerX(), this.d.centerY());
            int i2 = this.s;
            if (i2 > 0 && i2 < 10) {
                this.i.setColor(this.m);
                canvas.drawArc(this.e, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 360.0f, false, this.i);
            }
            this.i.setColor(this.m);
            this.j.setColor(this.v);
            float f2 = 360.0f * (this.C / 100.0f);
            RectF rectF = this.f;
            if (this.I) {
                f2 = -f2;
            }
            canvas.drawArc(rectF, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, false, this.j);
            canvas.restore();
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.B, this.h);
            if (this.u != 0) {
                canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.B, this.k);
            }
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        i();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z2) {
        if (z2) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        if (i2 != this.m) {
            this.m = i2;
            this.i.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderProgressColor(int i2) {
        if (i2 != this.v) {
            this.v = i2;
            this.j.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.s) {
            this.s = i2;
            i();
        }
    }

    @DexIgnore
    @Override // android.widget.ImageView
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.E)) {
            this.E = colorFilter;
            b();
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.u) {
            this.u = i2;
            this.k.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        h();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        h();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageResource(int i2) {
        super.setImageResource(i2);
        h();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        h();
    }

    @DexIgnore
    public void setInnrCircleDiammeter(float f2) {
        this.A = f2;
    }

    @DexIgnore
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        i();
    }

    @DexIgnore
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        i();
    }

    @DexIgnore
    public void setProgressBorderWidth(int i2) {
        if (i2 != this.t) {
            this.t = i2;
            i();
        }
    }

    @DexIgnore
    public void setProgressValue(float f2) {
        if (this.K) {
            if (this.D.isRunning()) {
                this.D.cancel();
            }
            this.D.setFloatValues(this.C, f2);
            this.D.start();
            return;
        }
        this.C = f2;
        invalidate();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != L) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }
}
