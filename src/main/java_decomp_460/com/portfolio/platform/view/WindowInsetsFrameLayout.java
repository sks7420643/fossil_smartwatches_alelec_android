package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.kq7;
import com.fossil.mo0;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WindowInsetsFrameLayout extends FrameLayout {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewGroup.OnHierarchyChangeListener {
        @DexIgnore
        public void onChildViewAdded(View view, View view2) {
            pq7.c(view, "parent");
            pq7.c(view2, "child");
            mo0.i0(view);
        }

        @DexIgnore
        public void onChildViewRemoved(View view, View view2) {
            pq7.c(view, "parent");
            pq7.c(view2, "child");
        }
    }

    @DexIgnore
    public WindowInsetsFrameLayout(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public WindowInsetsFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WindowInsetsFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        setOnHierarchyChangeListener(new a());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WindowInsetsFrameLayout(Context context, AttributeSet attributeSet, int i, int i2, kq7 kq7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
