package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.e01;
import com.fossil.gl0;
import com.fossil.il5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomPageIndicator extends View implements ViewPager.i {
    @DexIgnore
    public int A;
    @DexIgnore
    public HashMap<Integer, Integer> B;
    @DexIgnore
    public List<a> C;
    @DexIgnore
    public Rect D;
    @DexIgnore
    public float b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public RecyclerView g;
    @DexIgnore
    public ViewPager h;
    @DexIgnore
    public ViewPager.i i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public float u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public float x;
    @DexIgnore
    public int y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4762a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f4762a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a(null);
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
                this();
            }

            @DexIgnore
            /* renamed from: a */
            public b createFromParcel(Parcel parcel) {
                pq7.c(parcel, "parcel");
                return new b(parcel);
            }

            @DexIgnore
            /* renamed from: b */
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            pq7.c(parcel, "in");
            this.b = parcel.readInt();
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            pq7.c(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            pq7.c(parcel, "dest");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context) {
        super(context);
        pq7.c(context, "context");
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.f = new Paint(1);
        this.x = -1.0f;
        this.y = -1;
        this.B = new HashMap<>();
        this.C = new ArrayList();
        this.D = new Rect();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969880);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.f = new Paint(1);
        this.x = -1.0f;
        this.y = -1;
        this.B = new HashMap<>();
        this.C = new ArrayList();
        this.D = new Rect();
        if (!isInEditMode()) {
            Resources resources = getResources();
            int d2 = gl0.d(context, 2131099842);
            int d3 = gl0.d(context, 2131099829);
            int d4 = gl0.d(context, R.color.transparent);
            float dimension = resources.getDimension(2131165426);
            float dimension2 = resources.getDimension(2131165440);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, sq4.LinePageIndicator, i2, 0);
            this.s = obtainStyledAttributes.getBoolean(4, true);
            this.m = obtainStyledAttributes.getInt(0, 0);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setColor(obtainStyledAttributes.getColor(5, d2));
            this.d.setStyle(Paint.Style.STROKE);
            this.d.setColor(obtainStyledAttributes.getColor(9, d4));
            this.d.setStrokeWidth(obtainStyledAttributes.getDimension(10, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.e.setStyle(Paint.Style.FILL);
            this.e.setColor(obtainStyledAttributes.getColor(3, d3));
            String d5 = qn5.l.a().d("primaryColor");
            if (!TextUtils.isEmpty(d5)) {
                this.e.setColor(Color.parseColor(d5));
            }
            this.b = obtainStyledAttributes.getDimension(6, dimension);
            this.t = obtainStyledAttributes.getBoolean(8, false);
            this.u = obtainStyledAttributes2.getDimension(1, dimension2);
            this.v = obtainStyledAttributes.getBoolean(7, false);
            this.f.setColor(obtainStyledAttributes.getColor(2, 0));
            this.f.setStyle(Paint.Style.FILL);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            pq7.b(viewConfiguration, "configuration");
            this.w = viewConfiguration.getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        this.j = i2;
        invalidate();
        ViewPager.i iVar = this.i;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public final void b(Canvas canvas, float f2, float f3) {
        if (this.v) {
            float height = (float) getHeight();
            float f4 = height - LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f5 = (float) 2;
            canvas.drawRoundRect(f2 - f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (((float) (this.A - 1)) * f3) + f2 + f3, height, Math.abs(f4) / f5, Math.abs(f4) / f5, this.f);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void c(int i2) {
        this.l = i2;
        ViewPager.i iVar = this.i;
        if (iVar != null) {
            iVar.c(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void d(int i2) {
        int i3;
        if (this.t || this.l == 0) {
            if (il5.a(getContext())) {
                ViewPager viewPager = this.h;
                e01 adapter = viewPager != null ? viewPager.getAdapter() : null;
                if (adapter != null) {
                    pq7.b(adapter, "mViewPager?.adapter!!");
                    i3 = (adapter.e() - i2) - 1;
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                i3 = i2;
            }
            this.j = i3;
            this.k = i2;
            invalidate();
        }
        ViewPager.i iVar = this.i;
        if (iVar != null) {
            iVar.d(i2);
        }
    }

    @DexIgnore
    public final int e(int i2) {
        for (a aVar : this.C) {
            if (aVar.b() == i2) {
                return aVar.a();
            }
        }
        return 0;
    }

    @DexIgnore
    public final int f(int i2) {
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.h) == null) {
            return size;
        }
        if (viewPager != null) {
            e01 adapter = viewPager.getAdapter();
            if (adapter != null) {
                pq7.b(adapter, "mViewPager!!.adapter!!");
                int e2 = adapter.e();
                float f2 = this.b;
                int paddingLeft = (int) ((((float) (e2 - 1)) * f2) + ((float) getPaddingLeft()) + ((float) getPaddingRight()) + (2.0f * ((float) e2) * f2) + 1.0f);
                return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final int g(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((((float) 2) * this.b) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public final int getMCurrentPage$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    public final ViewPager.i getMListener$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public final RecyclerView getMRecyclerView$app_fossilRelease() {
        return this.g;
    }

    @DexIgnore
    public void h(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public final void i(HashMap<Integer, Integer> hashMap) {
        pq7.c(hashMap, Constants.MAP);
        this.B.clear();
        this.B = hashMap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r18) {
        /*
        // Method dump skipped, instructions count: 532
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.indicator.CustomPageIndicator.onDraw(android.graphics.Canvas):void");
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.m == 0) {
            setMeasuredDimension(f(i2), g(i3));
        } else {
            setMeasuredDimension(g(i2), f(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        pq7.c(parcelable, "state");
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        this.j = bVar.a();
        this.k = bVar.a();
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.b(this.j);
        return bVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        pq7.c(motionEvent, "ev");
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.h;
        if (viewPager != null) {
            if (viewPager == null) {
                pq7.i();
                throw null;
            } else if (viewPager.getAdapter() != null) {
                ViewPager viewPager2 = this.h;
                if (viewPager2 != null) {
                    e01 adapter = viewPager2.getAdapter();
                    if (adapter != null) {
                        pq7.b(adapter, "mViewPager!!.adapter!!");
                        if (adapter.e() != 0) {
                            int action = motionEvent.getAction() & 255;
                            if (action == 0) {
                                this.y = motionEvent.getPointerId(0);
                                this.x = motionEvent.getX();
                                return true;
                            } else if (action == 2) {
                                float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.y));
                                float f2 = x2 - this.x;
                                if (!this.z && Math.abs(f2) > ((float) this.w)) {
                                    this.z = true;
                                }
                                if (!this.z) {
                                    return true;
                                }
                                this.x = x2;
                                ViewPager viewPager3 = this.h;
                                if (viewPager3 != null) {
                                    if (!viewPager3.A()) {
                                        ViewPager viewPager4 = this.h;
                                        if (viewPager4 == null) {
                                            pq7.i();
                                            throw null;
                                        } else if (!viewPager4.e()) {
                                            return true;
                                        }
                                    }
                                    ViewPager viewPager5 = this.h;
                                    if (viewPager5 != null) {
                                        viewPager5.s(f2);
                                        return true;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                                pq7.i();
                                throw null;
                            } else if (action == 3 || action == 1) {
                                if (!this.z) {
                                    ViewPager viewPager6 = this.h;
                                    if (viewPager6 != null) {
                                        e01 adapter2 = viewPager6.getAdapter();
                                        if (adapter2 != null) {
                                            pq7.b(adapter2, "mViewPager!!.adapter!!");
                                            int e2 = adapter2.e();
                                            float width = (float) getWidth();
                                            float f3 = width / 2.0f;
                                            float f4 = width / 6.0f;
                                            if (this.j <= 0 || motionEvent.getX() >= f3 - f4) {
                                                if (this.j < e2 - 1 && motionEvent.getX() > f4 + f3) {
                                                    if (action == 3) {
                                                        return true;
                                                    }
                                                    ViewPager viewPager7 = this.h;
                                                    if (viewPager7 != null) {
                                                        viewPager7.setCurrentItem(this.j + 1);
                                                        return true;
                                                    }
                                                    pq7.i();
                                                    throw null;
                                                }
                                            } else if (action == 3) {
                                                return true;
                                            } else {
                                                ViewPager viewPager8 = this.h;
                                                if (viewPager8 != null) {
                                                    viewPager8.setCurrentItem(this.j - 1);
                                                    return true;
                                                }
                                                pq7.i();
                                                throw null;
                                            }
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                }
                                this.z = false;
                                this.y = -1;
                                ViewPager viewPager9 = this.h;
                                if (viewPager9 == null) {
                                    pq7.i();
                                    throw null;
                                } else if (!viewPager9.A()) {
                                    return true;
                                } else {
                                    ViewPager viewPager10 = this.h;
                                    if (viewPager10 != null) {
                                        viewPager10.q();
                                        return true;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                            } else if (action == 5) {
                                int actionIndex = motionEvent.getActionIndex();
                                this.x = motionEvent.getX(actionIndex);
                                this.y = motionEvent.getPointerId(actionIndex);
                                return true;
                            } else if (action != 6) {
                                return true;
                            } else {
                                int actionIndex2 = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex2) == this.y) {
                                    if (actionIndex2 == 0) {
                                        i2 = 1;
                                    }
                                    this.y = motionEvent.getPointerId(i2);
                                }
                                this.x = motionEvent.getX(motionEvent.findPointerIndex(this.y));
                                return true;
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.h == null && this.g == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.h;
        if (viewPager == null) {
            if (il5.a(getContext())) {
                RecyclerView recyclerView = this.g;
                if (recyclerView != null) {
                    RecyclerView.g adapter = recyclerView.getAdapter();
                    if (adapter != null) {
                        pq7.b(adapter, "mRecyclerView!!.adapter!!");
                        i2 = (adapter.getItemCount() - i2) - 1;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.j = i2;
        } else if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (il5.a(getContext())) {
                ViewPager viewPager2 = this.h;
                if (viewPager2 != null) {
                    e01 adapter2 = viewPager2.getAdapter();
                    if (adapter2 != null) {
                        pq7.b(adapter2, "mViewPager!!.adapter!!");
                        i2 = (adapter2.e() - i2) - 1;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.j = i2;
        } else {
            pq7.i();
            throw null;
        }
        invalidate();
    }

    @DexIgnore
    public final void setCurrentPage(int i2) {
        if (i2 < this.A) {
            this.j = i2;
            invalidate();
        }
    }

    @DexIgnore
    public final void setMCurrentPage$app_fossilRelease(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(ViewPager.i iVar) {
        this.i = iVar;
    }

    @DexIgnore
    public final void setMRecyclerView$app_fossilRelease(RecyclerView recyclerView) {
        this.g = recyclerView;
    }

    @DexIgnore
    public final void setNumberOfPage(int i2) {
        if (i2 > 0) {
            this.A = i2;
            invalidate();
        }
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!pq7.a(this.h, viewPager)) {
            ViewPager viewPager2 = this.h;
            if (viewPager2 != null) {
                viewPager2.N(this);
            }
            if ((viewPager != null ? viewPager.getAdapter() : null) != null) {
                this.h = viewPager;
                if (viewPager != null) {
                    viewPager.c(this);
                }
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }
}
