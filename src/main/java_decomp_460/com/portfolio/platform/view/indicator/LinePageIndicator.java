package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.gl0;
import com.fossil.il5;
import com.fossil.sq4;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LinePageIndicator extends View implements ViewPager.i {
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public RecyclerView e;
    @DexIgnore
    public ViewPager f;
    @DexIgnore
    public ViewPager.i g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float m;
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0359a();
        @DexIgnore
        public int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.LinePageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.LinePageIndicator$a$a  reason: collision with other inner class name */
        public static final class C0359a implements Parcelable.Creator<a> {
            @DexIgnore
            /* renamed from: a */
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            /* renamed from: b */
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(Parcel parcel) {
            super(parcel);
            this.b = parcel.readInt();
        }

        @DexIgnore
        public a(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    public LinePageIndicator(Context context) {
        this(context, null);
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969882);
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.m = -1.0f;
        this.s = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int d2 = gl0.d(getContext(), 2131099829);
            int d3 = gl0.d(getContext(), 2131099764);
            float dimension = resources.getDimension(2131165319);
            float dimension2 = resources.getDimension(2131165318);
            float dimension3 = resources.getDimension(2131165320);
            float dimension4 = resources.getDimension(2131165320);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.LinePageIndicator, i2, 0);
            this.i = obtainStyledAttributes.getBoolean(2, true);
            this.j = obtainStyledAttributes.getDimension(8, dimension);
            this.k = obtainStyledAttributes.getDimension(1, dimension2);
            setStrokeWidth(obtainStyledAttributes.getDimension(4, dimension3));
            setUnselectedBorderWidth(obtainStyledAttributes.getDimension(6, dimension4));
            this.b.setColor(obtainStyledAttributes.getColor(7, d3));
            this.c.setColor(obtainStyledAttributes.getColor(3, d2));
            this.d.setColor(obtainStyledAttributes.getColor(5, d3));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            this.l = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    private void setStrokeWidth(float f2) {
        this.c.setStrokeWidth(f2);
        this.b.setStrokeWidth(f2);
        invalidate();
    }

    @DexIgnore
    private void setUnselectedBorderWidth(float f2) {
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(f2);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public final int b(int i2) {
        float strokeWidth;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            strokeWidth = (float) size;
        } else {
            strokeWidth = this.c.getStrokeWidth() + ((float) getPaddingTop()) + ((float) getPaddingBottom());
            if (mode == Integer.MIN_VALUE) {
                strokeWidth = Math.min(strokeWidth, (float) size);
            }
        }
        return (int) Math.ceil((double) strokeWidth);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void c(int i2) {
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.c(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void d(int i2) {
        this.h = il5.a(getContext()) ? (this.f.getAdapter().e() - i2) - 1 : i2;
        invalidate();
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.d(i2);
        }
    }

    @DexIgnore
    public final int e(int i2) {
        float f2;
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.f) == null) {
            f2 = (float) size;
        } else {
            int e2 = viewPager.getAdapter().e();
            f2 = (((float) (e2 - 1)) * this.k) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) e2) * this.j);
            if (mode == Integer.MIN_VALUE) {
                f2 = Math.min(f2, (float) size);
            }
        }
        return (int) Math.ceil((double) f2);
    }

    @DexIgnore
    public void f(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int itemCount;
        super.onDraw(canvas);
        ViewPager viewPager = this.f;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.e;
            itemCount = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.e.getAdapter().getItemCount();
        } else {
            itemCount = this.f.getAdapter().e();
        }
        if (itemCount != 0) {
            if (this.h >= itemCount) {
                setCurrentItem(itemCount - 1);
                return;
            }
            float f2 = this.j;
            float f3 = this.k;
            float f4 = f2 + f3;
            float f5 = (float) itemCount;
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
            float width = this.i ? paddingLeft + ((((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (((f5 * f4) - f3) / 2.0f)) : paddingLeft;
            float strokeWidth = this.c.getStrokeWidth();
            float strokeWidth2 = this.d.getStrokeWidth();
            int i2 = 0;
            while (i2 < itemCount) {
                float f6 = (((float) i2) * f4) + width;
                float f7 = f6 + this.j;
                canvas.drawLine(f6, height, f7, height, i2 == this.h ? this.c : this.b);
                if (i2 != this.h) {
                    float f8 = strokeWidth2 / 2.0f;
                    canvas.drawRect(f6 + f8, height + ((strokeWidth2 - strokeWidth) / 2.0f), f7 - f8, height + ((strokeWidth - strokeWidth2) / 2.0f), this.d);
                }
                i2++;
            }
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(e(i2), b(i3));
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        this.h = aVar.b;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.b = this.h;
        return aVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.f;
        if (viewPager == null || viewPager.getAdapter() == null || this.f.getAdapter().e() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x = motionEvent.getX(motionEvent.findPointerIndex(this.s));
                    float f2 = x - this.m;
                    if (!this.t && Math.abs(f2) > ((float) this.l)) {
                        this.t = true;
                    }
                    if (!this.t) {
                        return true;
                    }
                    this.m = x;
                    if (!this.f.A() && !this.f.e()) {
                        return true;
                    }
                    this.f.s(f2);
                    return true;
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.m = motionEvent.getX(actionIndex);
                        this.s = motionEvent.getPointerId(actionIndex);
                        return true;
                    } else if (action != 6) {
                        return true;
                    } else {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.s) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.s = motionEvent.getPointerId(i2);
                        }
                        this.m = motionEvent.getX(motionEvent.findPointerIndex(this.s));
                        return true;
                    }
                }
            }
            if (!this.t) {
                int e2 = this.f.getAdapter().e();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.h <= 0 || motionEvent.getX() >= f3 - f4) {
                    if (this.h < e2 - 1 && motionEvent.getX() > f4 + f3) {
                        if (action == 3) {
                            return true;
                        }
                        this.f.setCurrentItem(this.h + 1);
                        return true;
                    }
                } else if (action == 3) {
                    return true;
                } else {
                    this.f.setCurrentItem(this.h - 1);
                    return true;
                }
            }
            this.t = false;
            this.s = -1;
            if (!this.f.A()) {
                return true;
            }
            this.f.q();
            return true;
        }
        this.s = motionEvent.getPointerId(0);
        this.m = motionEvent.getX();
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.f == null && this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.f;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (il5.a(getContext())) {
                i2 = (this.f.getAdapter().e() - i2) - 1;
            }
            this.h = i2;
        } else {
            if (il5.a(getContext())) {
                i2 = (this.e.getAdapter().getItemCount() - i2) - 1;
            }
            this.h = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.g = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.f, viewPager)) {
            if (viewPager.getAdapter() != null) {
                this.f = viewPager;
                viewPager.c(this);
                invalidate();
                setCurrentItem(0);
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }
}
