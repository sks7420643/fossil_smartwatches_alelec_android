package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ar7;
import com.fossil.br7;
import com.fossil.dl5;
import com.fossil.gl0;
import com.fossil.gl7;
import com.fossil.hr7;
import com.fossil.k37;
import com.fossil.nl0;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rh5;
import com.fossil.rp7;
import com.fossil.sq4;
import com.fossil.tl7;
import com.fossil.wt7;
import com.fossil.z57;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String l0;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public /* final */ Paint I;
    @DexIgnore
    public /* final */ Paint J;
    @DexIgnore
    public /* final */ int K;
    @DexIgnore
    public /* final */ String L;
    @DexIgnore
    public /* final */ String M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ int P;
    @DexIgnore
    public /* final */ int Q;
    @DexIgnore
    public /* final */ int R;
    @DexIgnore
    public /* final */ Typeface S;
    @DexIgnore
    public float T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public float W;
    @DexIgnore
    public float a0;
    @DexIgnore
    public int b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h;
    @DexIgnore
    public float h0;
    @DexIgnore
    public float i;
    @DexIgnore
    public float i0;
    @DexIgnore
    public List<gl7<Integer, Float, gl7<Float, Float, Float>>> j;
    @DexIgnore
    public int j0;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int k0;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public rh5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<RectF, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ar7 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ ar7 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ br7 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $leftIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, br7 br7, ar7 ar7, ar7 ar72) {
            super(1);
            this.$leftIndex = i;
            this.$goalPosition = br7;
            this.$goalDashLineBottom = ar7;
            this.$goalDashLineX = ar72;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(RectF rectF) {
            invoke(rectF);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            pq7.c(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$leftIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<RectF, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ar7 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ ar7 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ br7 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $rightIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, br7 br7, ar7 ar7, ar7 ar72) {
            super(1);
            this.$rightIndex = i;
            this.$goalPosition = br7;
            this.$goalDashLineBottom = ar7;
            this.$goalDashLineX = ar72;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(RectF rectF) {
            invoke(rectF);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            pq7.c(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$rightIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    /*
    static {
        String simpleName = ActivityDayDetailsChart.class.getSimpleName();
        pq7.b(simpleName, "ActivityDayDetailsChart::class.java.simpleName");
        l0 = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context) {
        this(context, null, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        pq7.c(context, "context");
        this.f = 0.5f;
        this.z = rh5.ACTIVE_TIME;
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        this.I = new Paint(1);
        this.J = new Paint(1);
        this.K = context.getResources().getDimensionPixelSize(2131165420);
        hr7 hr7 = hr7.f1520a;
        String string = context.getString(2131887268);
        pq7.b(string, "context.getString(R.string.am_hour)");
        String format = String.format(string, Arrays.copyOf(new Object[]{12}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        this.L = format;
        hr7 hr72 = hr7.f1520a;
        String string2 = context.getString(2131887514);
        pq7.b(string2, "context.getString(R.string.pm_hour)");
        String format2 = String.format(string2, Arrays.copyOf(new Object[]{12}, 1));
        pq7.b(format2, "java.lang.String.format(format, *args)");
        this.M = format2;
        this.N = gl0.d(context, 2131099849);
        this.O = 4;
        this.P = context.getResources().getDimensionPixelSize(2131165420);
        this.Q = context.getResources().getDimensionPixelSize(2131165426);
        this.R = context.getResources().getDimensionPixelSize(2131165385);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, sq4.ActivityDayDetailsChart, 0, 0));
        }
        this.S = nl0.b(context, 2131296258);
        int d = gl0.d(context, 2131099831);
        int d2 = gl0.d(context, 2131099849);
        int d3 = gl0.d(context, R.color.steps);
        int d4 = gl0.d(context, 2131099845);
        TypedArray mTypedArray = getMTypedArray();
        this.s = mTypedArray != null ? mTypedArray.getColor(4, d2) : d2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.t = mTypedArray2 != null ? mTypedArray2.getColor(5, d3) : d3;
        TypedArray mTypedArray3 = getMTypedArray();
        this.u = mTypedArray3 != null ? mTypedArray3.getColor(3, d4) : d4;
        TypedArray mTypedArray4 = getMTypedArray();
        this.v = mTypedArray4 != null ? mTypedArray4.getColor(1, d3) : d3;
        TypedArray mTypedArray5 = getMTypedArray();
        this.w = mTypedArray5 != null ? mTypedArray5.getColor(0, d3) : d3;
        TypedArray mTypedArray6 = getMTypedArray();
        this.k = mTypedArray6 != null ? mTypedArray6.getColor(7, d) : d;
        TypedArray mTypedArray7 = getMTypedArray();
        this.l = mTypedArray7 != null ? mTypedArray7.getColor(6, d2) : d2;
        TypedArray mTypedArray8 = getMTypedArray();
        this.m = mTypedArray8 != null ? mTypedArray8.getColor(2, d) : d;
        TypedArray mTypedArray9 = getMTypedArray();
        this.x = mTypedArray9 != null ? mTypedArray9.getColor(8, d) : d;
        TypedArray mTypedArray10 = getMTypedArray();
        this.y = mTypedArray10 != null ? mTypedArray10.getDimensionPixelSize(9, 40) : 40;
        TypedArray mTypedArray11 = getMTypedArray();
        if (mTypedArray11 != null) {
            mTypedArray11.recycle();
        }
    }

    @DexIgnore
    private final float getGoalForEachBar() {
        return this.i / ((float) 16);
    }

    @DexIgnore
    private final String getMHighLevelText() {
        int i2 = z57.f4419a[this.z.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887451 : 2131887435 : 2131887449;
        hr7 hr7 = hr7.f1520a;
        String string = getContext().getString(i3);
        pq7.b(string, "context.getString(rawText)");
        String format = String.format(string, Arrays.copyOf(new Object[]{dl5.b(this.h, 3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final String getMLowLevelText() {
        int i2 = z57.b[this.z.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887451 : 2131887435 : 2131887449;
        hr7 hr7 = hr7.f1520a;
        String string = getContext().getString(i3);
        pq7.b(string, "context.getString(rawText)");
        String format = String.format(string, Arrays.copyOf(new Object[]{dl5.b(this.g, 3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final float getMMax() {
        float f2;
        T t2;
        Float f3;
        List<gl7<Integer, Float, gl7<Float, Float, Float>>> list = this.j;
        if (list != null) {
            Iterator<T> it = list.iterator();
            if (!it.hasNext()) {
                t2 = null;
            } else {
                T next = it.next();
                if (it.hasNext()) {
                    float floatValue = ((Number) next.getSecond()).floatValue();
                    while (true) {
                        next = it.next();
                        floatValue = ((Number) next.getSecond()).floatValue();
                        if (Float.compare(floatValue, floatValue) >= 0) {
                            floatValue = floatValue;
                            next = next;
                        }
                        if (!it.hasNext()) {
                            break;
                        }
                    }
                }
                t2 = next;
            }
            T t3 = t2;
            if (!(t3 == null || (f3 = (Float) t3.getSecond()) == null)) {
                f2 = f3.floatValue();
                return Math.max(Math.max(getGoalForEachBar(), f2), this.h);
            }
        }
        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        return Math.max(Math.max(getGoalForEachBar(), f2), this.h);
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2, float f2, float f3, float f4, float f5, gl7<Integer, Float, gl7<Float, Float, Float>> gl7, rh5 rh5, boolean z2, rp7<? super RectF, tl7> rp7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l0;
        local.d(str, "Maximum: " + f5 + ", value: " + gl7);
        float f6 = f2 / ((float) 2);
        float f7 = (float) 0;
        if (gl7.getSecond().floatValue() <= f7) {
            rp7.invoke(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            return;
        }
        float floatValue = gl7.getSecond().floatValue() / f5;
        if (floatValue > 1.0f) {
            floatValue = 1.0f;
        }
        float f8 = (float) this.O;
        float f9 = (float) i2;
        float max = Math.max(f9 * floatValue, f6);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = l0;
        local2.d(str2, "Percentage: " + floatValue + ", chart height: " + i2 + ", real bar height: " + max);
        float max2 = Math.max((f9 - max) + f8, ((float) (z2 ? getStartBitmap().getHeight() * 2 : 0)) + f8);
        float round = (float) Math.round(f3);
        float round2 = (float) Math.round(f4);
        RectF rectF = new RectF(round, max2, round2, f9);
        gl7<Float, Float, Float> third = gl7.getThird();
        if (third == null) {
            k37.d(canvas, rectF, z57.e[rh5.ordinal()] != 1 ? this.I : this.J, f6);
        } else {
            k37.d(canvas, rectF, this.F, f6);
            float floatValue2 = (third.getThird().floatValue() / gl7.getSecond().floatValue()) * max;
            float floatValue3 = ((third.getThird().floatValue() + third.getSecond().floatValue()) / gl7.getSecond().floatValue()) * max;
            if (floatValue3 > f7) {
                k37.d(canvas, new RectF(round, f9 - floatValue3, round2, f9), this.G, f6);
            }
            if (floatValue2 > f7) {
                k37.d(canvas, new RectF(round, f9 - floatValue2, round2, f9), this.H, f6);
            }
        }
        rp7.invoke(rectF);
    }

    @DexIgnore
    public final void d(Paint paint, int i2, float f2, Paint.Style style) {
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(style);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        int i2 = 1;
        super.dispatchDraw(canvas);
        Paint paint = this.C;
        String str = this.L;
        this.T = paint.measureText(str, 0, wt7.A(str));
        this.U = ((float) getHeight()) - ((float) this.K);
        this.W = ((float) (getWidth() / 2)) - (this.T / ((float) 2));
        this.V = (float) this.K;
        this.a0 = (((float) getWidth()) - this.T) - ((float) (this.K * 3));
        Rect rect = new Rect();
        Paint paint2 = this.C;
        String str2 = this.L;
        paint2.getTextBounds(str2, 0, wt7.A(str2), rect);
        this.O = rect.height();
        this.b0 = (getHeight() - rect.height()) - (this.K * 2);
        Rect rect2 = new Rect();
        Rect rect3 = new Rect();
        this.C.getTextBounds(getMHighLevelText(), 0, wt7.A(getMHighLevelText()) > 0 ? wt7.A(getMHighLevelText()) : 1, rect2);
        Paint paint3 = this.C;
        String mLowLevelText = getMLowLevelText();
        if (wt7.A(getMLowLevelText()) > 0) {
            i2 = wt7.A(getMLowLevelText());
        }
        paint3.getTextBounds(mLowLevelText, 0, i2, rect3);
        this.c0 = (((float) getWidth()) - Math.max(this.C.measureText(getMHighLevelText()), this.C.measureText(getMLowLevelText()))) - ((float) (this.K * 3));
        this.k0 = 0;
        int i3 = this.b0;
        this.j0 = i3 - 4;
        this.g0 = Math.max(((float) i3) * this.e, (float) this.O);
        float max = Math.max(((float) this.b0) * this.f, (float) this.O);
        this.d0 = max;
        if (max < this.g0 + ((float) rect2.height())) {
            this.g0 = this.d0 - ((float) rect2.height());
        }
        float f2 = this.c0;
        int i4 = this.K;
        this.e0 = ((float) i4) + f2;
        this.h0 = f2 + ((float) i4);
        this.f0 = this.d0 + ((float) (rect3.height() / 2));
        this.i0 = this.g0 + ((float) (rect2.height() / 2));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawText(this.L, this.V, this.U, this.C);
            canvas.drawText(this.M, this.W, this.U, this.C);
            canvas.drawText(this.L, this.a0, this.U, this.C);
            List<gl7<Integer, Float, gl7<Float, Float, Float>>> list = this.j;
            if (list != null) {
                float f2 = this.c0 / ((float) 2);
                int i2 = this.P;
                float f3 = (float) (i2 / 2);
                float f4 = (((float) i2) + f2) - f3;
                br7 br7 = new br7();
                br7.element = -1;
                int size = list.size();
                int i3 = 0;
                float f5 = 0.0f;
                while (true) {
                    if (i3 >= size) {
                        break;
                    }
                    float floatValue = list.get(i3).getSecond().floatValue() + f5;
                    if (floatValue >= this.i) {
                        FLogger.INSTANCE.getLocal().d(l0, "Goal reach at " + i3);
                        br7.element = i3;
                        break;
                    }
                    i3++;
                    f5 = floatValue;
                }
                ar7 ar7 = new ar7();
                ar7.element = -1.0f;
                ar7 ar72 = new ar7();
                ar72.element = -1.0f;
                float mMax = getMMax();
                FLogger.INSTANCE.getLocal().d(l0, "Data max: " + getMMax() + ", goal for each bar: " + getGoalForEachBar() + ", chart max: " + mMax);
                int i4 = this.Q;
                float f6 = f2 - f3;
                int i5 = 11;
                while (true) {
                    boolean z2 = true;
                    if (i5 < 0) {
                        break;
                    }
                    gl7<Integer, Float, gl7<Float, Float, Float>> gl7 = list.get(i5);
                    int i6 = this.b0;
                    float f7 = (float) i4;
                    rh5 rh5 = this.z;
                    if (i5 != br7.element) {
                        z2 = false;
                    }
                    c(canvas, i6, f7, f6 - f7, f6, mMax, gl7, rh5, z2, new a(i5, br7, ar72, ar7));
                    f6 -= (float) (this.P + i4);
                    i5--;
                }
                int i7 = 12;
                float f8 = f4;
                while (i7 < 24) {
                    float f9 = (float) i4;
                    c(canvas, this.b0, f9, f8, f8 + f9, mMax, list.get(i7), this.z, i7 == br7.element, new b(i7, br7, ar72, ar7));
                    f8 += (float) (this.P + i4);
                    i7++;
                }
                float f10 = (float) 0;
                if (ar72.element >= f10 && ar7.element >= f10) {
                    int height = getStartBitmap().getHeight();
                    int width = getStartBitmap().getWidth();
                    float f11 = (float) (height + 10);
                    if (ar72.element >= f11) {
                        canvas.drawBitmap(getStartBitmap(), ar7.element - ((float) (width / 2)), (float) 10, this.D);
                        Path path = new Path();
                        path.moveTo(ar7.element, f11);
                        path.lineTo(ar7.element, ar72.element);
                        canvas.drawPath(path, this.B);
                    } else {
                        canvas.drawBitmap(getStartBitmap(), ar7.element - ((float) (width / 2)), (float) 10, this.D);
                    }
                }
            }
            canvas.drawRect(new Rect(0, this.j0, getWidth(), this.b0), this.A);
            canvas.drawRect(new Rect(0, this.k0, getWidth(), 4), this.A);
            FLogger.INSTANCE.getLocal().d(l0, "low level x: " + this.e0 + " y: " + this.f0 + ", high level x: " + this.h0 + " y: " + this.i0);
            float f12 = this.f0;
            if (f12 > ((float) this.k0) && f12 < ((float) this.j0)) {
                canvas.drawText(getMLowLevelText(), this.e0, this.f0, this.C);
            }
            float f13 = this.i0;
            if (f13 > ((float) this.k0) && f13 < ((float) this.j0)) {
                canvas.drawText(getMHighLevelText(), this.h0, this.i0, this.C);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 2131231085;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.R;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.A.setColor(this.k);
        float f2 = (float) 4;
        this.A.setStrokeWidth(f2);
        this.E.setColor(this.m);
        this.E.setStrokeWidth((float) this.Q);
        d(this.F, this.s, (float) this.Q, Paint.Style.FILL);
        d(this.G, this.t, (float) this.Q, Paint.Style.FILL);
        d(this.H, this.u, (float) this.Q, Paint.Style.FILL);
        d(this.I, this.v, (float) this.Q, Paint.Style.FILL);
        d(this.J, this.w, (float) this.Q, Paint.Style.FILL);
        this.B.setColor(this.l);
        this.B.setStyle(Paint.Style.STROKE);
        this.B.setStrokeWidth(f2);
        this.B.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.C.setColor(this.x);
        this.C.setStyle(Paint.Style.FILL);
        this.C.setTextSize((float) this.y);
        this.C.setTypeface(this.S);
        this.D.setStyle(Paint.Style.FILL);
        this.D.setColorFilter(new PorterDuffColorFilter(this.N, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
