package com.portfolio.platform.view.watchface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.a51;
import com.fossil.cl7;
import com.fossil.gp7;
import com.fossil.hl7;
import com.fossil.jo0;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.s67;
import com.fossil.s87;
import com.fossil.t67;
import com.fossil.tl7;
import com.fossil.u71;
import com.fossil.v87;
import com.fossil.w87;
import com.fossil.x41;
import com.fossil.z71;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFacePreviewView extends ConstraintLayout {
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public RelativeLayout x;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView c;
        @DexIgnore
        public /* final */ /* synthetic */ gp7 d;

        @DexIgnore
        public a(View view, WatchFacePreviewView watchFacePreviewView, gp7 gp7) {
            this.b = view;
            this.c = watchFacePreviewView;
            this.d = gp7;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d("WatchFacePreviewView", "preview task invoke doOnPreDraw");
            this.d.invoke();
            this.c.y = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $background;
        @DexIgnore
        public /* final */ /* synthetic */ List $elementConfigs;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFacePreviewView watchFacePreviewView, Bitmap bitmap, List list) {
            super(0);
            this.this$0 = watchFacePreviewView;
            this.$background = bitmap;
            this.$elementConfigs = list;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            int width = this.this$0.getWidth();
            ImageView imageView = this.this$0.w;
            if (imageView != null) {
                Bitmap bitmap = this.$background;
                a51 b = x41.b();
                Context context = imageView.getContext();
                pq7.b(context, "context");
                u71 u71 = new u71(context, b.a());
                u71.x(bitmap);
                u71.z(imageView);
                b.b(u71.w());
            }
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            for (T t : v87.a(this.$elementConfigs)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFacePreviewView", "preview " + ((Object) t));
                if (t instanceof s87.c) {
                    WatchFacePreviewView watchFacePreviewView = this.this$0;
                    w87 b2 = t.b();
                    if (b2 != null) {
                        t.d(watchFacePreviewView.N(b2));
                        T t2 = t;
                        t2.l(t2.h() * ((float) width));
                        this.this$0.T(t2);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (t instanceof s87.b) {
                    WatchFacePreviewView watchFacePreviewView2 = this.this$0;
                    w87 b3 = t.b();
                    if (b3 != null) {
                        t.d(watchFacePreviewView2.N(b3));
                        this.this$0.S(t);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (t instanceof s87.a) {
                    this.this$0.R(t);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements gp7<z71> {
        @DexIgnore
        public /* final */ /* synthetic */ String $previewURL;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFacePreviewView watchFacePreviewView, String str) {
            super(0);
            this.this$0 = watchFacePreviewView;
            this.$previewURL = str;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final z71 invoke() {
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            ImageView imageView = this.this$0.w;
            if (imageView == null) {
                return null;
            }
            String str = this.$previewURL;
            a51 b = x41.b();
            Context context = imageView.getContext();
            pq7.b(context, "context");
            u71 u71 = new u71(context, b.a());
            u71.x(str);
            u71.z(imageView);
            return b.b(u71.w());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements gp7<z71> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WatchFacePreviewView watchFacePreviewView) {
            super(0);
            this.this$0 = watchFacePreviewView;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final z71 invoke() {
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            ImageView imageView = this.this$0.w;
            if (imageView == null) {
                return null;
            }
            a51 b = x41.b();
            Context context = imageView.getContext();
            pq7.b(context, "context");
            u71 u71 = new u71(context, b.a());
            u71.x(2131231268);
            u71.z(imageView);
            return b.b(u71.w());
        }
    }

    @DexIgnore
    public WatchFacePreviewView(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public WatchFacePreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFacePreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        this.y = true;
        LayoutInflater.from(context).inflate(2131558860, (ViewGroup) this, true);
        this.w = (ImageView) findViewById(2131361908);
        this.x = (RelativeLayout) findViewById(2131362962);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFacePreviewView(Context context, AttributeSet attributeSet, int i, int i2, kq7 kq7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final w87 N(w87 w87) {
        float width = (float) getWidth();
        return new w87((float) Math.ceil((double) (w87.d() * width)), (float) Math.ceil((double) (w87.e() * width)), (float) Math.ceil((double) (w87.c() * width)), (float) Math.ceil((double) (width * w87.a())), 1.0f);
    }

    @DexIgnore
    public final void O(gp7<? extends Object> gp7) {
        if (this.y) {
            RelativeLayout relativeLayout = this.x;
            if (relativeLayout != null) {
                pq7.b(jo0.a(relativeLayout, new a(relativeLayout, this, gp7)), "OneShotPreDrawListener.add(this) { action(this) }");
                return;
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("WatchFacePreviewView", "preview task invoke after initialized");
        gp7.invoke();
    }

    @DexIgnore
    public final void P(String str) {
        pq7.c(str, "previewURL");
        O(new c(this, str));
    }

    @DexIgnore
    public final void Q(List<? extends s87> list, Bitmap bitmap) {
        pq7.c(list, "elementConfigs");
        O(new b(this, bitmap, list));
    }

    @DexIgnore
    public final void R(s87.a aVar) {
        Rect rect = new Rect();
        getDrawingRect(rect);
        w87 b2 = aVar.b();
        if (b2 != null) {
            float d2 = b2.d();
            w87 b3 = aVar.b();
            if (b3 != null) {
                CustomizeWidget customizeWidget = new CustomizeWidget(getContext(), rect, d2, b3.e(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 32, null);
                customizeWidget.setSetting(aVar.l());
                customizeWidget.O(aVar.i());
                customizeWidget.setContentTheme(aVar.h());
                customizeWidget.setBottomContent(aVar.j());
                customizeWidget.Z(aVar.k(), aVar.g());
                customizeWidget.Y(aVar.m());
                customizeWidget.setRemoveMode(false);
                customizeWidget.N();
                customizeWidget.T();
                RelativeLayout relativeLayout = this.x;
                if (relativeLayout != null) {
                    relativeLayout.addView(customizeWidget);
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void S(s87.b bVar) {
        s67.a aVar = s67.s;
        Context context = getContext();
        pq7.b(context, "this.context");
        cl7<s67, ViewGroup.LayoutParams> b2 = aVar.b(context, bVar);
        s67 component1 = b2.component1();
        ViewGroup.LayoutParams component2 = b2.component2();
        RelativeLayout relativeLayout = this.x;
        if (relativeLayout != null) {
            relativeLayout.addView(component1, component2);
        }
        w87 b3 = bVar.b();
        if (b3 != null) {
            cl7 a2 = hl7.a(Float.valueOf(b3.d()), Float.valueOf(b3.e()));
            component1.r(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void T(s87.c cVar) {
        t67.a aVar = t67.s;
        Context context = getContext();
        pq7.b(context, "this.context");
        cl7<t67, ViewGroup.LayoutParams> b2 = aVar.b(context, cVar);
        t67 component1 = b2.component1();
        ViewGroup.LayoutParams component2 = b2.component2();
        RelativeLayout relativeLayout = this.x;
        if (relativeLayout != null) {
            relativeLayout.addView(component1, component2);
        }
        w87 b3 = cVar.b();
        if (b3 != null) {
            cl7 a2 = hl7.a(Float.valueOf(b3.d()), Float.valueOf(b3.e()));
            component1.r(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void U() {
        O(new d(this));
    }
}
