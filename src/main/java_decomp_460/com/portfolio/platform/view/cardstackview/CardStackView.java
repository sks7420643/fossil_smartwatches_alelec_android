package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.il7;
import com.fossil.o57;
import com.fossil.p47;
import com.fossil.p57;
import com.fossil.pq7;
import com.fossil.r57;
import com.fossil.s57;
import com.fossil.sq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.cardstackview.CardContainerView;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CardStackView extends FrameLayout {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ o57 b;
    @DexIgnore
    public /* final */ p57 c;
    @DexIgnore
    public BaseAdapter d;
    @DexIgnore
    public /* final */ LinkedList<CardContainerView> e;
    @DexIgnore
    public a f;
    @DexIgnore
    public /* final */ c g;
    @DexIgnore
    public /* final */ b h;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(int i);

        @DexIgnore
        void c(float f, float f2);

        @DexIgnore
        void d(s57 s57);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CardContainerView.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CardStackView f4758a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(CardStackView cardStackView) {
            this.f4758a = cardStackView;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void a() {
            if (this.f4758a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.f4758a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.b(this.f4758a.getState$app_fossilRelease().f2784a);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void b(Point point, s57 s57) {
            pq7.c(point, "point");
            pq7.c(s57, "direction");
            this.f4758a.m(point, s57);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void c() {
            this.f4758a.e();
            if (this.f4758a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.f4758a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void d(float f, float f2) {
            this.f4758a.n(f, f2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends DataSetObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CardStackView f4759a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(CardStackView cardStackView) {
            this.f4759a = cardStackView;
        }

        @DexIgnore
        public void onChanged() {
            boolean z = false;
            if (this.f4759a.getAdapter$app_fossilRelease() != null) {
                if (this.f4759a.getState$app_fossilRelease().d) {
                    this.f4759a.getState$app_fossilRelease().d = false;
                } else {
                    int i = this.f4759a.getState$app_fossilRelease().c;
                    BaseAdapter adapter$app_fossilRelease = this.f4759a.getAdapter$app_fossilRelease();
                    if (adapter$app_fossilRelease != null) {
                        if (i == adapter$app_fossilRelease.getCount()) {
                            z = true;
                        }
                        z = !z;
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                this.f4759a.d(z);
                p57 state$app_fossilRelease = this.f4759a.getState$app_fossilRelease();
                BaseAdapter adapter$app_fossilRelease2 = this.f4759a.getAdapter$app_fossilRelease();
                if (adapter$app_fossilRelease2 != null) {
                    state$app_fossilRelease.c = adapter$app_fossilRelease2.getCount();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ CardStackView f4760a;
        @DexIgnore
        public /* final */ /* synthetic */ Point b;
        @DexIgnore
        public /* final */ /* synthetic */ s57 c;

        @DexIgnore
        public d(CardStackView cardStackView, Point point, s57 s57) {
            this.f4760a = cardStackView;
            this.b = point;
            this.c = s57;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            pq7.c(animator, "animator");
            this.f4760a.b(this.b, this.c);
        }
    }

    /*
    static {
        String simpleName = CardStackView.class.getSimpleName();
        pq7.b(simpleName, "CardStackView::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context) {
        this(context, null);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        pq7.c(context, "context");
        this.b = new o57();
        this.c = new p57();
        this.e = new LinkedList<>();
        this.g = new c(this);
        this.h = new b(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.CardStackView);
        setVisibleCount(obtainStyledAttributes.getInt(11, this.b.f2635a));
        setSwipeThreshold(obtainStyledAttributes.getFloat(8, this.b.b));
        setTranslationDiff(obtainStyledAttributes.getFloat(10, this.b.c));
        setScaleDiff(obtainStyledAttributes.getFloat(4, this.b.d));
        setStackFrom(r57.values()[obtainStyledAttributes.getInt(5, this.b.e.ordinal())]);
        setElevationEnabled(obtainStyledAttributes.getBoolean(1, this.b.f));
        setSwipeEnabled(obtainStyledAttributes.getBoolean(7, this.b.g));
        List<s57> from = s57.from(obtainStyledAttributes.getInt(6, 0));
        pq7.b(from, "SwipeDirection.from(arra\u2026kView_swipeDirection, 0))");
        setSwipeDirection(from);
        setLeftOverlay(obtainStyledAttributes.getResourceId(2, 0));
        setRightOverlay(obtainStyledAttributes.getResourceId(3, 0));
        setBottomOverlay(obtainStyledAttributes.getResourceId(0, 0));
        setTopOverlay(obtainStyledAttributes.getResourceId(9, 0));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final CardContainerView getBottomView() {
        CardContainerView last = this.e.getLast();
        pq7.b(last, "containers.last");
        return last;
    }

    @DexIgnore
    private final CardContainerView getTopView() {
        CardContainerView first = this.e.getFirst();
        pq7.b(first, "containers.first");
        return first;
    }

    @DexIgnore
    private final void setBottomOverlay(int i2) {
        this.b.j = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setElevationEnabled(boolean z) {
        this.b.f = z;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setLeftOverlay(int i2) {
        this.b.h = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setRightOverlay(int i2) {
        this.b.i = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setScaleDiff(float f2) {
        this.b.d = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setStackFrom(r57 r57) {
        this.b.e = r57;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.s57> */
    /* JADX WARN: Multi-variable type inference failed */
    private final void setSwipeDirection(List<? extends s57> list) {
        this.b.l = list;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setSwipeEnabled(boolean z) {
        this.b.g = z;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setSwipeThreshold(float f2) {
        this.b.b = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setTopOverlay(int i2) {
        this.b.k = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setTranslationDiff(float f2) {
        this.b.c = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setVisibleCount(int i2) {
        this.b.f2635a = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    public final void a() {
        int i2 = this.b.f2635a;
        for (int i3 = 0; i3 < i2; i3++) {
            CardContainerView cardContainerView = this.e.get(i3);
            pq7.b(cardContainerView, "containers[i]");
            CardContainerView cardContainerView2 = cardContainerView;
            cardContainerView2.e();
            cardContainerView2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setScaleX(1.0f);
            cardContainerView2.setScaleY(1.0f);
            cardContainerView2.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void b(Point point, s57 s57) {
        pq7.c(point, "point");
        pq7.c(s57, "direction");
        k();
        this.c.b = point;
        e();
        this.c.f2784a++;
        a aVar = this.f;
        if (aVar != null) {
            if (aVar != null) {
                aVar.d(s57);
            } else {
                pq7.i();
                throw null;
            }
        }
        h();
        this.e.getLast().setContainerEventListener(null);
        this.e.getFirst().setContainerEventListener(this.h);
    }

    @DexIgnore
    public final void c() {
        this.e.getFirst().setContainerEventListener(null);
        this.e.getFirst().setDraggable(false);
        if (this.e.size() > 1) {
            this.e.get(1).setContainerEventListener(this.h);
            this.e.get(1).setDraggable(true);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        l(z);
        g();
        e();
        f();
    }

    @DexIgnore
    public final void e() {
        a();
        n(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void f() {
        if (this.d != null) {
            int i2 = this.b.f2635a;
            for (int i3 = 0; i3 < i2; i3++) {
                CardContainerView cardContainerView = this.e.get(i3);
                pq7.b(cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                int i4 = this.c.f2784a + i3;
                BaseAdapter baseAdapter = this.d;
                if (baseAdapter != null) {
                    if (i4 < baseAdapter.getCount()) {
                        ViewGroup contentContainer = cardContainerView2.getContentContainer();
                        BaseAdapter baseAdapter2 = this.d;
                        if (baseAdapter2 != null) {
                            View view = baseAdapter2.getView(i4, contentContainer.getChildAt(0), contentContainer);
                            pq7.b(contentContainer, "parent");
                            if (contentContainer.getChildCount() == 0) {
                                contentContainer.addView(view);
                            }
                            cardContainerView2.setVisibility(0);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        cardContainerView2.setVisibility(8);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            BaseAdapter baseAdapter3 = this.d;
            if (baseAdapter3 == null) {
                pq7.i();
                throw null;
            } else if (!baseAdapter3.isEmpty()) {
                getTopView().setDraggable(true);
            }
        }
    }

    @DexIgnore
    public final void g() {
        removeAllViews();
        this.e.clear();
        int i2 = this.b.f2635a;
        for (int i3 = 0; i3 < i2; i3++) {
            View inflate = LayoutInflater.from(getContext()).inflate(2131558447, (ViewGroup) this, false);
            if (inflate != null) {
                CardContainerView cardContainerView = (CardContainerView) inflate;
                cardContainerView.setDraggable(false);
                cardContainerView.setCardStackOption(this.b);
                o57 o57 = this.b;
                cardContainerView.f(o57.h, o57.i, o57.j, o57.k);
                this.e.add(0, cardContainerView);
                addView(cardContainerView);
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.cardstackview.CardContainerView");
            }
        }
        this.e.getFirst().setContainerEventListener(this.h);
        this.c.e = true;
    }

    @DexIgnore
    public final BaseAdapter getAdapter$app_fossilRelease() {
        return this.d;
    }

    @DexIgnore
    public final a getCardEventListener$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final p57 getState$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final void h() {
        boolean z = false;
        int i2 = (this.c.f2784a + this.b.f2635a) - 1;
        BaseAdapter baseAdapter = this.d;
        if (baseAdapter == null) {
            return;
        }
        if (baseAdapter != null) {
            if (i2 < baseAdapter.getCount()) {
                CardContainerView bottomView = getBottomView();
                bottomView.setDraggable(false);
                ViewGroup contentContainer = bottomView.getContentContainer();
                BaseAdapter baseAdapter2 = this.d;
                if (baseAdapter2 != null) {
                    View view = baseAdapter2.getView(i2, contentContainer.getChildAt(0), contentContainer);
                    pq7.b(contentContainer, "parent");
                    if (contentContainer.getChildCount() == 0) {
                        contentContainer.addView(view);
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                CardContainerView bottomView2 = getBottomView();
                bottomView2.setDraggable(false);
                bottomView2.setVisibility(8);
            }
            int i3 = this.c.f2784a;
            BaseAdapter baseAdapter3 = this.d;
            if (baseAdapter3 != null) {
                if (i3 < baseAdapter3.getCount()) {
                    z = true;
                }
                if (z) {
                    getTopView().setDraggable(true);
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void i(CardContainerView cardContainerView) {
        CardStackView cardStackView = (CardStackView) cardContainerView.getParent();
        if (cardStackView != null) {
            cardStackView.removeView(cardContainerView);
            cardStackView.addView(cardContainerView, 0);
        }
    }

    @DexIgnore
    public final void j(Point point, Animator.AnimatorListener animatorListener) {
        getTopView().animate().translationX((float) point.x).translationY(-((float) point.y)).setDuration(400).setListener(animatorListener).start();
    }

    @DexIgnore
    public final void k() {
        i(getTopView());
        LinkedList<CardContainerView> linkedList = this.e;
        linkedList.addLast(linkedList.removeFirst());
    }

    @DexIgnore
    public final void l(boolean z) {
        if (z) {
            this.c.a();
        }
    }

    @DexIgnore
    public final void m(Point point, s57 s57) {
        pq7.c(point, "point");
        pq7.c(s57, "direction");
        c();
        j(point, new d(this, point, s57));
    }

    @DexIgnore
    public final void n(float f2, float f3) {
        a aVar = this.f;
        if (aVar != null) {
            if (aVar != null) {
                aVar.c(f2, f3);
            } else {
                pq7.i();
                throw null;
            }
        }
        o57 o57 = this.b;
        if (o57.f) {
            int i2 = o57.f2635a;
            for (int i3 = 1; i3 < i2; i3++) {
                CardContainerView cardContainerView = this.e.get(i3);
                pq7.b(cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                float f4 = (float) i3;
                float f5 = this.b.d;
                float f6 = 1.0f - (f4 * f5);
                float f7 = (float) (i3 - 1);
                float abs = (((1.0f - (f5 * f7)) - f6) * Math.abs(f2)) + f6;
                cardContainerView2.setScaleX(abs);
                cardContainerView2.setScaleY(abs);
                float b2 = f4 * p47.b(this.b.c);
                if (this.b.e == r57.Top) {
                    b2 *= -1.0f;
                }
                float b3 = p47.b(this.b.c) * f7;
                if (this.b.e == r57.Top) {
                    b3 *= -1.0f;
                }
                cardContainerView2.setTranslationY(b2 - ((b2 - b3) * Math.abs(f2)));
            }
        }
    }

    @DexIgnore
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.c.e && i2 == 0) {
            e();
        }
    }

    @DexIgnore
    public final void setAdapter(BaseAdapter baseAdapter) {
        pq7.c(baseAdapter, "adapter");
        BaseAdapter baseAdapter2 = this.d;
        if (baseAdapter2 != null) {
            try {
                baseAdapter2.unregisterDataSetObserver(this.g);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = i;
                local.d(str, "Exception when unregisterDataSetObserver e=" + e2);
            }
        }
        this.d = baseAdapter;
        if (baseAdapter != null) {
            baseAdapter.registerDataSetObserver(this.g);
            this.c.c = baseAdapter.getCount();
            d(true);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void setAdapter$app_fossilRelease(BaseAdapter baseAdapter) {
        this.d = baseAdapter;
    }

    @DexIgnore
    public final void setCardEventListener$app_fossilRelease(a aVar) {
        this.f = aVar;
    }
}
