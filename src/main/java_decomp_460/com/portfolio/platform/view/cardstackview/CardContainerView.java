package com.portfolio.platform.view.cardstackview;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bo0;
import com.fossil.o57;
import com.fossil.p47;
import com.fossil.q57;
import com.fossil.s57;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CardContainerView extends FrameLayout {
    @DexIgnore
    public o57 b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public ViewGroup i;
    @DexIgnore
    public ViewGroup j;
    @DexIgnore
    public View k;
    @DexIgnore
    public View l;
    @DexIgnore
    public View m;
    @DexIgnore
    public View s;
    @DexIgnore
    public c t;
    @DexIgnore
    public GestureDetector.SimpleOnGestureListener u;
    @DexIgnore
    public GestureDetector v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            c cVar = CardContainerView.this.t;
            if (cVar == null) {
                return true;
            }
            cVar.a();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f4757a;

        /*
        static {
            int[] iArr = new int[q57.values().length];
            f4757a = iArr;
            try {
                iArr[q57.TopLeft.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f4757a[q57.TopRight.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f4757a[q57.BottomLeft.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f4757a[q57.BottomRight.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
        */
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(Point point, s57 s57);

        @DexIgnore
        Object c();  // void declaration

        @DexIgnore
        void d(float f, float f2);
    }

    @DexIgnore
    public CardContainerView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.d = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.g = false;
        this.h = true;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.s = null;
        this.t = null;
        this.u = new a();
        this.v = new GestureDetector(getContext(), this.u);
    }

    @DexIgnore
    public final void a(MotionEvent motionEvent) {
        this.e = motionEvent.getRawX();
        this.f = motionEvent.getRawY();
    }

    @DexIgnore
    public final void b(MotionEvent motionEvent) {
        this.g = true;
        o(motionEvent);
        n();
        m();
        c cVar = this.t;
        if (cVar != null) {
            cVar.d(getPercentX(), getPercentY());
        }
    }

    @DexIgnore
    public final void c(MotionEvent motionEvent) {
        if (this.g) {
            this.g = false;
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            Point h2 = p47.h(this.e, this.f, rawX, rawY);
            q57 f2 = p47.f(this.e, this.f, rawX, rawY);
            double g2 = p47.g(this.e, this.f, rawX, rawY);
            s57 s57 = null;
            int i2 = b.f4757a[f2.ordinal()];
            if (i2 == 1) {
                s57 = Math.cos(Math.toRadians(180.0d - Math.toDegrees(g2))) < -0.5d ? s57.Left : s57.Top;
            } else if (i2 == 2) {
                s57 = Math.cos(Math.toRadians(Math.toDegrees(g2))) < 0.5d ? s57.Top : s57.Right;
            } else if (i2 == 3) {
                s57 = Math.cos(Math.toRadians(Math.toDegrees(g2) + 180.0d)) < -0.5d ? s57.Left : s57.Bottom;
            } else if (i2 == 4) {
                s57 = Math.cos(Math.toRadians(360.0d - Math.toDegrees(g2))) < 0.5d ? s57.Bottom : s57.Right;
            }
            float abs = Math.abs((s57 == s57.Left || s57 == s57.Right) ? getPercentX() : getPercentY());
            o57 o57 = this.b;
            if (abs <= o57.b) {
                d();
                c cVar = this.t;
                if (cVar != null) {
                    cVar.c();
                }
            } else if (o57.l.contains(s57)) {
                c cVar2 = this.t;
                if (cVar2 != null) {
                    cVar2.b(h2, s57);
                }
            } else {
                d();
                c cVar3 = this.t;
                if (cVar3 != null) {
                    cVar3.c();
                }
            }
        }
        this.e = motionEvent.getRawX();
        this.f = motionEvent.getRawY();
    }

    @DexIgnore
    public final void d() {
        animate().translationX(this.c).translationY(this.d).setDuration(300).setInterpolator(new OvershootInterpolator(1.0f)).setListener(null).start();
    }

    @DexIgnore
    public void e() {
        this.i.setAlpha(1.0f);
        this.j.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void f(int i2, int i3, int i4, int i5) {
        View view = this.k;
        if (view != null) {
            this.j.removeView(view);
        }
        if (i2 != 0) {
            View inflate = LayoutInflater.from(getContext()).inflate(i2, this.j, false);
            this.k = inflate;
            this.j.addView(inflate);
            this.k.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.l;
        if (view2 != null) {
            this.j.removeView(view2);
        }
        if (i3 != 0) {
            View inflate2 = LayoutInflater.from(getContext()).inflate(i3, this.j, false);
            this.l = inflate2;
            this.j.addView(inflate2);
            this.l.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.m;
        if (view3 != null) {
            this.j.removeView(view3);
        }
        if (i4 != 0) {
            View inflate3 = LayoutInflater.from(getContext()).inflate(i4, this.j, false);
            this.m = inflate3;
            this.j.addView(inflate3);
            this.m.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.s;
        if (view4 != null) {
            this.j.removeView(view4);
        }
        if (i5 != 0) {
            View inflate4 = LayoutInflater.from(getContext()).inflate(i5, this.j, false);
            this.s = inflate4;
            this.j.addView(inflate4);
            this.s.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public void g() {
        View view = this.k;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.m;
        if (view2 != null) {
            view2.setAlpha(1.0f);
        }
        View view3 = this.s;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.l;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public ViewGroup getContentContainer() {
        return this.i;
    }

    @DexIgnore
    public ViewGroup getOverlayContainer() {
        return this.j;
    }

    @DexIgnore
    public float getPercentX() {
        float f2 = 1.0f;
        float translationX = ((getTranslationX() - this.c) * 2.0f) / ((float) getWidth());
        if (translationX <= 1.0f) {
            f2 = translationX;
        }
        if (f2 < -1.0f) {
            return -1.0f;
        }
        return f2;
    }

    @DexIgnore
    public float getPercentY() {
        float f2 = 1.0f;
        float translationY = ((getTranslationY() - this.d) * 2.0f) / ((float) getHeight());
        if (translationY <= 1.0f) {
            f2 = translationY;
        }
        if (f2 < -1.0f) {
            return -1.0f;
        }
        return f2;
    }

    @DexIgnore
    public float getViewOriginX() {
        return this.c;
    }

    @DexIgnore
    public float getViewOriginY() {
        return this.d;
    }

    @DexIgnore
    public final void h(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            i();
        } else {
            j();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public void i() {
        View view = this.k;
        if (view != null) {
            view.setAlpha(1.0f);
        }
        View view2 = this.l;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.m;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.s;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public void j() {
        View view = this.k;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.m;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.s;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.l;
        if (view4 != null) {
            view4.setAlpha(1.0f);
        }
    }

    @DexIgnore
    public void k() {
        View view = this.k;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.m;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.s;
        if (view3 != null) {
            view3.setAlpha(1.0f);
        }
        View view4 = this.l;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void l(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            k();
        } else {
            g();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public final void m() {
        float percentX = getPercentX();
        float percentY = getPercentY();
        List<s57> list = this.b.l;
        if (list == s57.HORIZONTAL) {
            h(percentX);
        } else if (list == s57.VERTICAL) {
            l(percentY);
        } else if (list == s57.FREEDOM_NO_BOTTOM) {
            if (Math.abs(percentX) >= Math.abs(percentY) || percentY >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                h(percentX);
                return;
            }
            k();
            setOverlayAlpha(Math.abs(percentY));
        } else if (list == s57.FREEDOM) {
            if (Math.abs(percentX) > Math.abs(percentY)) {
                h(percentX);
            } else {
                l(percentY);
            }
        } else if (Math.abs(percentX) > Math.abs(percentY)) {
            if (percentX < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i();
            } else {
                j();
            }
            setOverlayAlpha(Math.abs(percentX));
        } else {
            if (percentY < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                k();
            } else {
                g();
            }
            setOverlayAlpha(Math.abs(percentY));
        }
    }

    @DexIgnore
    public final void n() {
        setRotation(getPercentX() * 20.0f);
    }

    @DexIgnore
    public final void o(MotionEvent motionEvent) {
        setTranslationX((this.c + motionEvent.getRawX()) - this.e);
        setTranslationY((this.d + motionEvent.getRawY()) - this.f);
    }

    @DexIgnore
    public void onFinishInflate() {
        super.onFinishInflate();
        FrameLayout.inflate(getContext(), 2131558448, this);
        this.i = (ViewGroup) findViewById(2131361991);
        this.j = (ViewGroup) findViewById(2131361992);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.v.onTouchEvent(motionEvent);
        if (this.b.g && this.h) {
            int a2 = bo0.a(motionEvent);
            if (a2 == 0) {
                a(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(true);
            } else if (a2 == 1) {
                c(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            } else if (a2 == 2) {
                b(motionEvent);
            } else if (a2 == 3) {
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
        return true;
    }

    @DexIgnore
    public void setCardStackOption(o57 o57) {
        this.b = o57;
    }

    @DexIgnore
    public void setContainerEventListener(c cVar) {
        this.t = cVar;
        this.c = getTranslationX();
        this.d = getTranslationY();
    }

    @DexIgnore
    public void setDraggable(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public void setOverlayAlpha(float f2) {
        this.j.setAlpha(f2);
    }

    @DexIgnore
    public void setOverlayAlpha(AnimatorSet animatorSet) {
        if (animatorSet != null) {
            animatorSet.start();
        }
    }
}
