package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.portfolio.platform.data.model.Explore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleImageButton extends AppCompatImageButton {
    @DexIgnore
    public String d; // = "#FFFFFF";
    @DexIgnore
    public String e; // = "#FFFFFF";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context) {
        super(context);
        pq7.c(context, "context");
        a(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, sq4.FlexibleImageButton);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = Explore.COLUMN_BACKGROUND;
            }
            pq7.b(string, "styledAttrs.getString(R.\u2026          ?: \"background\"");
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "primaryText";
            }
            pq7.b(string2, "styledAttrs.getString(R.\u2026         ?: \"primaryText\"");
            String d2 = qn5.l.a().d(string);
            if (d2 == null) {
                d2 = "#FFFFFF";
            }
            this.d = d2;
            String d3 = qn5.l.a().d(string2);
            if (d3 == null) {
                d3 = "#FFFFFF";
            }
            this.e = d3;
            b();
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        if (this.d != null) {
            Drawable background = getBackground();
            if (background instanceof ShapeDrawable) {
                Drawable background2 = getBackground();
                if (background2 != null) {
                    Paint paint = ((ShapeDrawable) background2).getPaint();
                    pq7.b(paint, "shapeDrawable.paint");
                    paint.setColor(Color.parseColor(this.d));
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.ShapeDrawable");
                }
            } else if (background instanceof GradientDrawable) {
                Drawable background3 = getBackground();
                if (background3 != null) {
                    ((GradientDrawable) background3).setColor(Color.parseColor(this.d));
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                }
            } else if (background instanceof ColorDrawable) {
                Drawable background4 = getBackground();
                if (background4 != null) {
                    ((ColorDrawable) background4).setColor(Color.parseColor(this.d));
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.ColorDrawable");
                }
            } else if (background instanceof StateListDrawable) {
                Drawable background5 = getBackground();
                if (background5 != null) {
                    ((StateListDrawable) background5).setColorFilter(Color.parseColor(this.d), PorterDuff.Mode.SRC_ATOP);
                } else {
                    throw new il7("null cannot be cast to non-null type android.graphics.drawable.StateListDrawable");
                }
            }
        }
        String str = this.e;
        if (str != null) {
            getDrawable().setColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_ATOP);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageButton
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            super.setImageDrawable(drawable);
        }
    }
}
