package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fossil.gl0;
import com.fossil.p47;
import com.fossil.sq4;
import com.fossil.um5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ImageButton extends RelativeLayout {
    @DexIgnore
    public /* final */ ImageView b; // = ((ImageView) findViewById(2131362330));
    @DexIgnore
    public /* final */ TextView c; // = ((TextView) findViewById(2131362331));
    @DexIgnore
    public /* final */ ViewGroup d; // = ((ViewGroup) findViewById(2131362329));

    @DexIgnore
    public ImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        RelativeLayout.inflate(context, 2131558841, this);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, sq4.ImageButton);
        setIcon(obtainStyledAttributes.getResourceId(1, -1));
        setTitle(obtainStyledAttributes.getResourceId(2, -1));
        setTitleColor(obtainStyledAttributes.getResourceId(3, -1));
        setBackground(obtainStyledAttributes.getResourceId(0, -1));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private void setBackground(int i) {
        Drawable f = gl0.f(getContext(), 2131230853);
        if (i != -1) {
            f = gl0.f(getContext(), i);
        }
        this.d.setBackground(f);
    }

    @DexIgnore
    private void setIcon(int i) {
        if (i != -1) {
            this.b.setImageDrawable(gl0.f(getContext(), i));
            this.b.setVisibility(0);
            return;
        }
        this.b.setVisibility(8);
    }

    @DexIgnore
    private void setTitle(int i) {
        if (i != -1) {
            um5.e(this.c, i);
            this.c.setVisibility(0);
            return;
        }
        this.c.setVisibility(8);
    }

    @DexIgnore
    private void setTitleColor(int i) {
        if (i != -1) {
            this.c.setTextColor(getResources().getColorStateList(i));
        }
    }

    @DexIgnore
    public void a(int i, int i2) {
        ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.b.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.b.getLayoutParams();
        layoutParams2.setMarginStart((int) (p47.b(70.0f) - (((float) i) - p47.b(30.0f))));
        this.b.setLayoutParams(layoutParams2);
    }

    @DexIgnore
    public TextView getTextView() {
        return this.c;
    }

    @DexIgnore
    public String getTitle() {
        return this.c.getText().toString();
    }

    @DexIgnore
    public void setTitle(String str) {
        this.c.setText(str);
        this.c.setVisibility(0);
    }
}
