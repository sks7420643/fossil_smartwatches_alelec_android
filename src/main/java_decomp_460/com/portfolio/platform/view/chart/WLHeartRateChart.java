package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hm7;
import com.fossil.im7;
import com.fossil.nl0;
import com.fossil.p47;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.sq4;
import com.fossil.t57;
import com.fossil.u57;
import com.fossil.um5;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WLHeartRateChart extends View {
    @DexIgnore
    public short A;
    @DexIgnore
    public short B;
    @DexIgnore
    public short C;
    @DexIgnore
    public short D;
    @DexIgnore
    public Paint E;
    @DexIgnore
    public Paint F;
    @DexIgnore
    public Paint G;
    @DexIgnore
    public Paint H;
    @DexIgnore
    public Paint I;
    @DexIgnore
    public Paint J;
    @DexIgnore
    public Paint K;
    @DexIgnore
    public List<RectF> L;
    @DexIgnore
    public a M; // = a.AVERAGE;
    @DexIgnore
    public List<u57> N;
    @DexIgnore
    public int O; // = b.NONE.ordinal();
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Integer f;
    @DexIgnore
    public float g;
    @DexIgnore
    public Integer h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public enum a {
        AVERAGE,
        RESTING,
        PEAK
    }

    @DexIgnore
    public enum b {
        GENERAL,
        DAY,
        WEEK,
        MONTH,
        NONE
    }

    /*
    static {
        pq7.b(WLHeartRateChart.class.getSimpleName(), "WLHeartRateChart::class.java.simpleName");
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WLHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.WLHeartRateChart);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.getColor(2, -3355444);
            this.b = obtainStyledAttributes.getColor(0, -3355444);
            this.c = obtainStyledAttributes.getColor(1, -3355444);
            this.d = obtainStyledAttributes.getColor(10, -3355444);
            this.e = obtainStyledAttributes.getColor(6, -3355444);
            this.f = Integer.valueOf(obtainStyledAttributes.getResourceId(4, -1));
            this.g = obtainStyledAttributes.getDimension(3, p47.n(13.0f));
            this.h = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
            this.i = obtainStyledAttributes.getDimension(7, p47.n(13.0f));
            this.j = obtainStyledAttributes.getDimension(5, p47.b(4.0f));
            this.O = obtainStyledAttributes.getInt(9, b.NONE.ordinal());
            Paint paint = new Paint();
            this.E = paint;
            paint.setColor(this.b);
            this.E.setStrokeWidth(2.0f);
            this.E.setStyle(Paint.Style.STROKE);
            Paint paint2 = new Paint();
            this.F = paint2;
            paint2.setColor(this.b);
            this.F.setStyle(Paint.Style.STROKE);
            this.F.setPathEffect(new DashPathEffect(new float[]{6.0f, 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            Paint paint3 = new Paint(1);
            this.G = paint3;
            paint3.setColor(this.e);
            this.G.setStyle(Paint.Style.FILL);
            this.G.setTextSize(this.g);
            Integer num = this.f;
            if (num == null || num.intValue() != -1) {
                Paint paint4 = this.G;
                Integer num2 = this.f;
                if (num2 != null) {
                    paint4.setTypeface(nl0.b(context, num2.intValue()));
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Paint paint5 = new Paint(1);
            this.H = paint5;
            paint5.setColor(this.e);
            this.H.setStyle(Paint.Style.FILL);
            this.H.setTextSize(this.i);
            Integer num3 = this.h;
            if (num3 == null || num3.intValue() != -1) {
                Paint paint6 = this.H;
                Integer num4 = this.h;
                if (num4 != null) {
                    paint6.setTypeface(nl0.b(context, num4.intValue()));
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Paint paint7 = new Paint(1);
            this.I = paint7;
            paint7.setColor(this.c);
            this.I.setStyle(Paint.Style.FILL);
            Paint paint8 = new Paint(1);
            this.J = paint8;
            paint8.setColor(-1);
            this.J.setStyle(Paint.Style.FILL);
            Paint paint9 = new Paint(1);
            this.K = paint9;
            paint9.setColor(this.d);
            this.K.setStyle(Paint.Style.STROKE);
            Rect rect = new Rect();
            this.G.getTextBounds("222", 0, 3, rect);
            this.x = (float) rect.width();
            this.y = (float) rect.height();
            Rect rect2 = new Rect();
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887231);
            this.H.getTextBounds(c2, 0, c2.length(), rect2);
            this.z = (float) rect2.height();
            this.N = new ArrayList();
            this.L = new ArrayList();
            obtainStyledAttributes.recycle();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        e(canvas);
        float size = (this.v - this.u) / ((float) ((this.N.size() + this.N.size()) - 1));
        g(canvas, size, ((this.v - this.u) - (((float) this.N.size()) * size)) / ((float) (this.N.size() - 1)));
        h(canvas, hm7.i(um5.c(PortfolioApp.h0.c(), 2131887231), um5.c(PortfolioApp.h0.c(), 2131887232), um5.c(PortfolioApp.h0.c(), 2131887231)), hm7.i(Float.valueOf(this.L.get(0).left), Float.valueOf(this.L.get(11).left), Float.valueOf(this.L.get(23).left)), this.l + (((((float) getMeasuredHeight()) - this.l) + this.z) / ((float) 2)));
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        this.w = this.l;
        f(canvas);
        float size = (this.v - this.u) / ((float) ((this.N.size() + this.N.size()) - 1));
        g(canvas, size, ((this.v - this.u) - (((float) this.N.size()) * size)) / ((float) (this.N.size() - 1)));
        h(canvas, hm7.i(um5.c(PortfolioApp.h0.c(), 2131887231), um5.c(PortfolioApp.h0.c(), 2131887232), um5.c(PortfolioApp.h0.c(), 2131887231)), hm7.i(Float.valueOf(this.L.get(0).left), Float.valueOf(this.L.get(11).left), Float.valueOf(this.L.get(23).left)), this.l + (((((float) getMeasuredHeight()) - this.l) + this.z) / ((float) 2)));
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        e(canvas);
        float size = (this.v - this.u) / ((float) ((this.N.size() + this.N.size()) - 1));
        g(canvas, size, ((this.v - this.u) - (((float) this.N.size()) * size)) / ((float) (this.N.size() - 1)));
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        float f2 = this.l;
        float measuredHeight = ((((float) getMeasuredHeight()) - this.l) + this.z) / ((float) 2);
        int size2 = this.N.size() / 4;
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.N.size()) {
            arrayList.add(String.valueOf(i3 + 1));
            arrayList2.add(Float.valueOf(this.L.get(i3).left));
            int i4 = i2 + 1;
            i3 = (size2 * i4) + i2;
            i2 = i4;
        }
        arrayList.add(String.valueOf(this.L.size()));
        List<RectF> list = this.L;
        arrayList2.add(Float.valueOf(list.get(hm7.g(list)).left));
        h(canvas, arrayList, arrayList2, f2 + measuredHeight);
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        e(canvas);
        float f2 = this.v;
        float f3 = this.u;
        float f4 = (f2 - f3) / ((float) 48);
        g(canvas, f4, ((f2 - f3) - (((float) 7) * f4)) / ((float) 6));
        List<String> i2 = hm7.i(um5.c(PortfolioApp.h0.c(), 2131886770), um5.c(PortfolioApp.h0.c(), 2131886769), um5.c(PortfolioApp.h0.c(), 2131886772), um5.c(PortfolioApp.h0.c(), 2131886774), um5.c(PortfolioApp.h0.c(), 2131886773), um5.c(PortfolioApp.h0.c(), 2131886768), um5.c(PortfolioApp.h0.c(), 2131886771));
        List<RectF> list = this.L;
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(Float.valueOf(((RectF) it.next()).left));
        }
        h(canvas, i2, pm7.j0(arrayList), this.l + (((((float) getMeasuredHeight()) - this.l) + this.z) / ((float) 2)));
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.m, this.l, (float) getMeasuredWidth(), this.l, this.E);
        canvas.drawLine(this.m, this.k, (float) getMeasuredWidth(), this.k, this.E);
        float f2 = this.s;
        float f3 = this.s;
        float measureText = this.G.measureText(String.valueOf((int) this.C));
        float f4 = (float) 2;
        float f5 = this.w;
        float f6 = (float) 1;
        float f7 = (float) 4;
        float f8 = f5 - (((f5 - this.k) * f6) / f7);
        canvas.drawText(String.valueOf((int) this.C), f2 + (((((float) getMeasuredWidth()) - f3) - measureText) / f4), (this.y / f4) + f8, this.G);
        float f9 = this.s;
        float measuredWidth = ((((float) getMeasuredWidth()) - this.s) - this.G.measureText(String.valueOf((int) this.D))) / f4;
        float f10 = this.t;
        float f11 = f10 + ((f6 * (this.w - f10)) / f7);
        canvas.drawText(String.valueOf((int) this.D), f9 + measuredWidth, (this.y / f4) + f11, this.G);
        Path path = new Path();
        path.moveTo(this.m, f8);
        path.lineTo(this.s, f8);
        canvas.drawPath(path, this.F);
        path.moveTo(this.m, f11);
        path.lineTo(this.s, f11);
        canvas.drawPath(path, this.F);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2 = this.s;
        float f3 = this.s;
        float measureText = this.G.measureText(String.valueOf((int) this.B));
        float f4 = (float) 2;
        float f5 = this.t;
        canvas.drawText(String.valueOf((int) this.B), f2 + (((((float) getMeasuredWidth()) - f3) - measureText) / f4), (this.y / f4) + f5, this.G);
        float f6 = this.s;
        float measuredWidth = ((((float) getMeasuredWidth()) - this.s) - this.G.measureText(String.valueOf((int) this.A))) / f4;
        float f7 = this.w;
        canvas.drawText(String.valueOf((int) this.A), f6 + measuredWidth, (this.y / f4) + f7, this.G);
        Path path = new Path();
        path.moveTo(this.m, f5);
        path.lineTo(this.s, f5);
        canvas.drawPath(path, this.F);
        path.moveTo(this.m, f7);
        path.lineTo(this.s, f7);
        canvas.drawPath(path, this.F);
    }

    @DexIgnore
    public final void g(Canvas canvas, float f2, float f3) {
        this.L.clear();
        int i2 = 0;
        for (T t2 : this.N) {
            if (i2 >= 0) {
                T t3 = t2;
                short a2 = t3.a();
                short b2 = t3.b();
                float f4 = this.t;
                float f5 = this.w;
                short s2 = this.B;
                short s3 = this.A;
                float f6 = (((f5 - f4) / ((float) (s2 - s3))) * ((float) (s2 - b2))) + f4;
                float f7 = (f5 - f4) / ((float) (s2 - s3));
                float f8 = this.u + ((f2 + f3) * ((float) i2));
                RectF rectF = new RectF(f8, f6, f8 + f2, (f7 * ((float) (b2 - a2))) + f6);
                float f9 = this.j;
                canvas.drawRoundRect(rectF, f9, f9, this.I);
                if (a2 == -1 && b2 == -1) {
                    rectF.top = 1.0f;
                    rectF.bottom = -1.0f;
                }
                this.L.add(rectF);
                i2++;
            } else {
                hm7.l();
                throw null;
            }
        }
        if (t57.f3368a[this.M.ordinal()] == 2 && (!this.L.isEmpty())) {
            float f10 = f2 / ((float) 2);
            this.K.setStrokeWidth(f10 / 1.5f);
            int i3 = 0;
            for (T t4 : this.L) {
                int i4 = i3 + 1;
                if (i3 >= 0) {
                    T t5 = t4;
                    if (i3 < hm7.g(this.L)) {
                        RectF rectF2 = this.L.get(i4);
                        float f11 = ((RectF) t5).top;
                        if (f11 <= ((RectF) t5).bottom) {
                            float f12 = rectF2.top;
                            if (f12 <= rectF2.bottom) {
                                canvas.drawLine(((RectF) t5).left + f10, f11 + f10, rectF2.left + f10, f12 + f10, this.K);
                            }
                        }
                    }
                    float f13 = ((RectF) t5).top;
                    if (f13 <= ((RectF) t5).bottom) {
                        canvas.drawCircle(((RectF) t5).left + f10, f13 + f10, f10, this.J);
                        canvas.drawCircle(((RectF) t5).left + f10, ((RectF) t5).top + f10, f10, this.K);
                    }
                    i3 = i4;
                } else {
                    hm7.l();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void h(Canvas canvas, List<String> list, List<Float> list2, float f2) {
        int i2 = 0;
        for (T t2 : list) {
            if (i2 >= 0) {
                canvas.drawText(t2, list2.get(i2).floatValue(), f2, this.H);
                i2++;
            } else {
                hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        pq7.c(canvas, "canvas");
        this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.l = ((float) getMeasuredHeight()) - (this.z * ((float) 2));
        this.m = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.s = ((float) getMeasuredWidth()) - (this.x * 1.5f);
        this.t = this.i;
        this.u = this.m + p47.b(5.0f);
        this.w = this.l - this.i;
        this.v = this.s - p47.b(5.0f);
        int i2 = this.O;
        if (i2 == b.GENERAL.ordinal()) {
            b(canvas);
        } else if (i2 == b.DAY.ordinal()) {
            if (!this.N.isEmpty()) {
                a(canvas);
            }
        } else if (i2 == b.WEEK.ordinal()) {
            if (!this.N.isEmpty()) {
                d(canvas);
            }
        } else if (i2 == b.MONTH.ordinal() && (!this.N.isEmpty())) {
            c(canvas);
        }
    }
}
