package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cl7;
import com.fossil.gl0;
import com.fossil.gl7;
import com.fossil.hm7;
import com.fossil.im7;
import com.fossil.p47;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.fossil.w57;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TodayHeartRateChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public float B;
    @DexIgnore
    public String C; // = "";
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public short O;
    @DexIgnore
    public short P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public List<w57> U;
    @DexIgnore
    public float V;
    @DexIgnore
    public String W; // = "";
    @DexIgnore
    public List<gl7<Integer, cl7<Integer, Float>, String>> a0; // = new ArrayList();
    @DexIgnore
    public int b0;
    @DexIgnore
    public String w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public String z; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TodayHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface f;
        Typeface f2;
        String d;
        String d2;
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.TodayHeartRateChart);
        String string = obtainStyledAttributes.getString(1);
        this.W = string == null ? "#ffffff" : string;
        String string2 = obtainStyledAttributes.getString(0);
        this.w = string2 == null ? "" : string2;
        this.x = obtainStyledAttributes.getColor(5, gl0.d(context, R.color.steps));
        this.y = obtainStyledAttributes.getColor(4, gl0.d(context, 2131099834));
        String string3 = obtainStyledAttributes.getString(6);
        this.z = string3 == null ? "" : string3;
        String string4 = obtainStyledAttributes.getString(3);
        this.A = string4 == null ? "" : string4;
        this.B = obtainStyledAttributes.getDimension(2, p47.n(13.0f));
        String string5 = obtainStyledAttributes.getString(8);
        this.C = string5 == null ? "" : string5;
        this.D = obtainStyledAttributes.getDimension(7, p47.n(13.0f));
        obtainStyledAttributes.recycle();
        this.Q = new Paint();
        if (!TextUtils.isEmpty(this.w) && (d2 = qn5.l.a().d(this.w)) != null) {
            this.Q.setColor(Color.parseColor(d2));
        }
        this.Q.setStrokeWidth(2.0f);
        this.Q.setStyle(Paint.Style.STROKE);
        this.R = new Paint(1);
        if (!TextUtils.isEmpty(this.z) && (d = qn5.l.a().d(this.z)) != null) {
            this.R.setColor(Color.parseColor(d));
        }
        this.R.setStyle(Paint.Style.FILL);
        this.R.setTextSize(this.B);
        if (!TextUtils.isEmpty(this.A) && (f2 = qn5.l.a().f(this.A)) != null) {
            this.R.setTypeface(f2);
        }
        Paint paint = new Paint(1);
        this.S = paint;
        paint.setColor(this.R.getColor());
        this.S.setStyle(Paint.Style.FILL);
        this.S.setTextSize(this.D);
        if (!TextUtils.isEmpty(this.C) && (f = qn5.l.a().f(this.C)) != null) {
            this.S.setTypeface(f);
        }
        Paint paint2 = new Paint(1);
        this.T = paint2;
        paint2.setStrokeWidth(p47.b(2.0f));
        this.T.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.R.getTextBounds("222", 0, 3, rect);
        this.L = (float) rect.width();
        this.M = (float) rect.height();
        Rect rect2 = new Rect();
        String c = um5.c(PortfolioApp.h0.c(), 2131887231);
        this.S.getTextBounds(c, 0, c.length(), rect2);
        this.N = (float) rect2.height();
        this.U = new ArrayList();
        n();
        a();
        u();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.f(canvas);
        this.E = ((float) canvas.getHeight()) - (this.N * ((float) 2));
        this.F = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.G = (float) canvas.getWidth();
        this.H = this.D;
        float b = p47.b(5.0f) + this.F;
        this.I = b;
        this.K = this.E - (this.D * ((float) 6));
        float f = this.G - (this.L * 2.0f);
        this.J = f;
        int i = this.b0;
        if (i == 0) {
            i = DateTimeConstants.MINUTES_PER_DAY;
        }
        this.V = (f - b) / ((float) i);
        q(canvas);
        u();
    }

    @DexIgnore
    public final int getDayInMinuteWithTimeZone() {
        return this.b0;
    }

    @DexIgnore
    public final List<gl7<Integer, cl7<Integer, Float>, String>> getListTimeZoneChange() {
        return this.a0;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.W;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void k(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.k(canvas);
        if (this.a0.isEmpty()) {
            List<String> i = hm7.i(um5.c(PortfolioApp.h0.c(), 2131886713), um5.c(PortfolioApp.h0.c(), 2131886715), um5.c(PortfolioApp.h0.c(), 2131886714), um5.c(PortfolioApp.h0.c(), 2131886716), um5.c(PortfolioApp.h0.c(), 2131886713));
            ArrayList arrayList = new ArrayList();
            int size = i.size();
            float f = (this.J - this.I) / ((float) (size - 1));
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(Float.valueOf(this.I + (((float) i2) * f)));
            }
            s(canvas, i, arrayList, this.E + (((((float) canvas.getHeight()) - this.E) + this.N) / ((float) 2)));
            return;
        }
        List<gl7<Integer, cl7<Integer, Float>, String>> list = this.a0;
        ArrayList arrayList2 = new ArrayList(im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList2.add((String) it.next().getThird());
        }
        List<String> j0 = pm7.j0(arrayList2);
        List<gl7<Integer, cl7<Integer, Float>, String>> list2 = this.a0;
        ArrayList arrayList3 = new ArrayList(im7.m(list2, 10));
        Iterator<T> it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList3.add(Integer.valueOf(((Number) it2.next().getFirst()).intValue()));
        }
        List<Number> j02 = pm7.j0(arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (Number number : j02) {
            int intValue = number.intValue();
            arrayList4.add(Float.valueOf((((float) intValue) * this.V) + this.I));
        }
        s(canvas, j0, arrayList4, this.E + (((((float) canvas.getHeight()) - this.E) + this.N) / ((float) 2)));
    }

    @DexIgnore
    public final void m(List<w57> list) {
        pq7.c(list, "heartRatePointList");
        this.U.clear();
        this.U.addAll(list);
        n();
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void n() {
        Object obj;
        Object obj2;
        List<w57> list = this.U;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.d() > 0) {
                arrayList.add(t);
            }
        }
        Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            Object next = it.next();
            if (!it.hasNext()) {
                obj = next;
            } else {
                int d = ((w57) next).d();
                while (true) {
                    next = it.next();
                    d = ((w57) next).d();
                    if (d <= d) {
                        d = d;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
                obj = next;
            }
        }
        w57 w57 = (w57) obj;
        this.O = (short) (w57 != null ? (short) w57.d() : 0);
        List<w57> list2 = this.U;
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : list2) {
            if (t2.b() > 0) {
                arrayList2.add(t2);
            }
        }
        Iterator it2 = arrayList2.iterator();
        if (!it2.hasNext()) {
            obj2 = null;
        } else {
            Object next2 = it2.next();
            if (!it2.hasNext()) {
                obj2 = next2;
            } else {
                int b = ((w57) next2).b();
                while (true) {
                    next2 = it2.next();
                    int b2 = ((w57) next2).b();
                    if (b >= b2) {
                        b2 = b;
                        next2 = next2;
                    }
                    if (!it2.hasNext()) {
                        break;
                    }
                    b = b2;
                }
                obj2 = next2;
            }
        }
        w57 w572 = (w57) obj2;
        short b3 = w572 != null ? (short) w572.b() : 0;
        this.P = (short) b3;
        if (b3 == ((short) 0)) {
            this.P = (short) 100;
        }
    }

    @DexIgnore
    public final void o(String str, String str2) {
        pq7.c(str, "maxHeartRate");
        pq7.c(str2, "lowestHeartRate");
        String d = qn5.l.a().d(str);
        String d2 = qn5.l.a().d(str2);
        if (d != null) {
            this.y = Color.parseColor(d);
        }
        if (d2 != null) {
            this.x = Color.parseColor(d2);
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void p(Canvas canvas) {
        canvas.drawLine(this.F, this.K, (float) canvas.getWidth(), this.K, this.Q);
        canvas.drawLine(this.F, this.H, (float) canvas.getWidth(), this.H, this.Q);
        float f = this.J;
        float f2 = this.J;
        float measureText = this.R.measureText(String.valueOf((int) this.O));
        float f3 = (float) 2;
        canvas.drawText(String.valueOf((int) this.O), f + (((((float) canvas.getWidth()) - f2) - measureText) / f3) + p47.b(2.0f), (this.M * 1.5f) + this.K, this.R);
        float f4 = this.J;
        float width = ((((float) canvas.getWidth()) - this.J) - this.R.measureText(String.valueOf((int) this.P))) / f3;
        canvas.drawText(String.valueOf((int) this.P), f4 + width + p47.b(2.0f), (this.M * 1.5f) + this.H, this.R);
    }

    @DexIgnore
    public final void q(Canvas canvas) {
        p(canvas);
        r(canvas);
    }

    @DexIgnore
    public final void r(Canvas canvas) {
        int i;
        Path path;
        ArrayList<Path> arrayList = new ArrayList();
        while (true) {
            Path path2 = null;
            do {
                i = 0;
                boolean z2 = true;
                for (T t : this.U) {
                    float c = this.I + (((float) t.c()) * this.V);
                    float f = this.H;
                    float f2 = this.K;
                    short s = this.P;
                    float d = f + (((float) (s - t.d())) * ((f2 - f) / ((float) (s - this.O))));
                    if (d <= this.K) {
                        if (z2) {
                            path = new Path();
                            path.moveTo(c, d);
                            z2 = false;
                        } else if (path2 != null) {
                            path2.lineTo(c, d);
                            path = path2;
                        } else {
                            pq7.i();
                            throw null;
                        }
                        i++;
                        path2 = path;
                    }
                }
                if (path2 != null) {
                    arrayList.add(path2);
                }
                Paint paint = this.T;
                float f3 = this.I;
                paint.setShader(new LinearGradient(f3, this.H, f3, this.K, this.y, this.x, Shader.TileMode.MIRROR));
                for (Path path3 : arrayList) {
                    canvas.drawPath(path3, this.T);
                }
                return;
            } while (path2 == null);
            if (i > 1) {
                arrayList.add(path2);
            }
        }
    }

    @DexIgnore
    public final void s(Canvas canvas, List<String> list, List<Float> list2, float f) {
        int i = 0;
        for (T t : list) {
            if (i >= 0) {
                canvas.drawText(t, list2.get(i).floatValue(), f, this.S);
                i++;
            } else {
                hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setDayInMinuteWithTimeZone(int i) {
        this.b0 = i;
    }

    @DexIgnore
    public final void setListTimeZoneChange(List<gl7<Integer, cl7<Integer, Float>, String>> list) {
        pq7.c(list, "value");
        this.a0 = list;
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        pq7.c(str, "<set-?>");
        this.W = str;
    }

    @DexIgnore
    public void t(String str, int i) {
        pq7.c(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " valueColor=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1222874814) {
            if (hashCode == -685682508 && str.equals("lowestHeartRate")) {
                this.x = i;
            }
        } else if (str.equals("maxHeartRate")) {
            this.y = i;
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void u() {
        if (!TextUtils.isEmpty(this.W)) {
            String d = qn5.l.a().d(this.W);
            if (!TextUtils.isEmpty(d)) {
                setBackgroundColor(Color.parseColor(d));
            }
        }
    }
}
