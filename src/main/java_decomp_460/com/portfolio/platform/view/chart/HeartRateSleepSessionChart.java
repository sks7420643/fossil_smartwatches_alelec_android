package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.gl0;
import com.fossil.nl0;
import com.fossil.p47;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn5;
import com.fossil.sq4;
import com.fossil.um5;
import com.fossil.v57;
import com.misfit.frameworks.buttonservice.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSleepSessionChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public String B; // = "";
    @DexIgnore
    public float C;
    @DexIgnore
    public Integer D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public int P;
    @DexIgnore
    public short Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public Paint U;
    @DexIgnore
    public Paint V;
    @DexIgnore
    public ArrayList<v57> W;
    @DexIgnore
    public float a0;
    @DexIgnore
    public boolean b0; // = true;
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Path f4761a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Path path, int i) {
            pq7.c(path, "path");
            this.f4761a = path;
            this.b = i;
        }

        @DexIgnore
        public final Path a() {
            return this.f4761a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f4761a, aVar.f4761a) || this.b != aVar.b) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Path path = this.f4761a;
            return ((path != null ? path.hashCode() : 0) * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "PathDist(path=" + this.f4761a + ", sleepState=" + this.b + ")";
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSleepSessionChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface f;
        String d;
        String d2;
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.HeartRateSleepSessionChart);
        String string = obtainStyledAttributes.getString(1);
        this.w = string == null ? "" : string;
        this.x = obtainStyledAttributes.getColor(0, gl0.d(context, R.color.activeTime));
        this.y = obtainStyledAttributes.getColor(5, gl0.d(context, 2131099834));
        this.z = obtainStyledAttributes.getColor(2, gl0.d(context, 2131099834));
        String string2 = obtainStyledAttributes.getString(6);
        this.A = string2 == null ? "" : string2;
        String string3 = obtainStyledAttributes.getString(4);
        this.B = string3 == null ? "" : string3;
        this.C = obtainStyledAttributes.getDimension(3, p47.n(13.0f));
        this.D = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
        this.E = obtainStyledAttributes.getDimension(7, p47.n(13.0f));
        obtainStyledAttributes.recycle();
        this.S = new Paint();
        if (!TextUtils.isEmpty(this.w) && (d2 = qn5.l.a().d(this.w)) != null) {
            this.S.setColor(Color.parseColor(d2));
        }
        this.S.setStrokeWidth(2.0f);
        this.S.setStyle(Paint.Style.STROKE);
        this.T = new Paint(1);
        if (!TextUtils.isEmpty(this.A) && (d = qn5.l.a().d(this.A)) != null) {
            this.T.setColor(Color.parseColor(d));
        }
        this.T.setStyle(Paint.Style.FILL);
        this.T.setTextSize(this.C);
        if (!TextUtils.isEmpty(this.B) && (f = qn5.l.a().f(this.B)) != null) {
            this.T.setTypeface(f);
        }
        Paint paint = new Paint(1);
        this.U = paint;
        paint.setColor(this.T.getColor());
        this.U.setStyle(Paint.Style.FILL);
        this.U.setTextSize(this.E);
        Integer num = this.D;
        if (num == null || num.intValue() != -1) {
            Paint paint2 = this.U;
            Integer num2 = this.D;
            if (num2 != null) {
                paint2.setTypeface(nl0.b(context, num2.intValue()));
            } else {
                pq7.i();
                throw null;
            }
        }
        Paint paint3 = new Paint(1);
        this.V = paint3;
        paint3.setStrokeWidth(p47.b(1.0f));
        this.V.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.T.getTextBounds("2222", 0, 4, rect);
        this.M = (float) rect.width();
        this.N = (float) rect.height();
        Rect rect2 = new Rect();
        String c = um5.c(PortfolioApp.h0.c(), 2131887231);
        this.U.getTextBounds(c, 0, c.length(), rect2);
        this.O = (float) rect2.height();
        this.W = new ArrayList<>();
        a();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        pq7.c(canvas, "canvas");
        super.f(canvas);
        float f = (float) 2;
        this.F = ((float) canvas.getHeight()) - (this.O * f);
        this.G = 25.0f;
        this.H = ((float) canvas.getWidth()) + 30.0f;
        this.I = this.E;
        this.J = this.G + p47.b(5.0f);
        this.L = this.F - (this.E * f);
        float b = (this.H - this.M) + p47.b(10.0f);
        this.K = b;
        this.a0 = (b - this.J) / ((float) this.P);
        o(canvas);
    }

    @DexIgnore
    public final int getMDuration() {
        return this.P;
    }

    @DexIgnore
    public final short getMMaxHRValue() {
        return this.R;
    }

    @DexIgnore
    public final short getMMinHRValue() {
        return this.Q;
    }

    @DexIgnore
    public final void m(ArrayList<v57> arrayList) {
        pq7.c(arrayList, "heartRatePointList");
        this.W.clear();
        this.W.addAll(arrayList);
        this.b0 = !this.W.isEmpty();
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void n(Canvas canvas) {
        canvas.drawLine(this.G, this.L, (float) canvas.getWidth(), this.L, this.S);
        canvas.drawLine(this.G, this.I, (float) canvas.getWidth(), this.I, this.S);
        if (this.b0) {
            float f = this.K;
            float f2 = this.K;
            canvas.drawText(String.valueOf((int) this.Q), f + ((((float) canvas.getWidth()) - f2) - this.T.measureText(String.valueOf((int) this.Q))), (1.5f * this.N) + this.L, this.T);
            float f3 = this.K;
            float f4 = this.K;
            canvas.drawText(String.valueOf((int) this.R), f3 + ((((float) canvas.getWidth()) - f4) - this.T.measureText(String.valueOf((int) this.R))), this.I - (0.5f * this.N), this.T);
        }
    }

    @DexIgnore
    public final void o(Canvas canvas) {
        n(canvas);
        p(canvas);
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void p(Canvas canvas) {
        int i;
        ArrayList<a> arrayList = new ArrayList();
        v57 v57 = (v57) pm7.H(this.W);
        int d = v57 != null ? v57.d() : -1;
        boolean z2 = false;
        int i2 = 0;
        int i3 = d;
        Path path = null;
        for (T t : this.W) {
            int a2 = t.a();
            int b = t.b();
            int c = t.c();
            float f = this.J + (((float) b) * this.a0);
            float f2 = this.I;
            float f3 = this.L;
            short s = this.R;
            float f4 = (((float) (s - a2)) * ((f3 - f2) / ((float) (s - this.Q)))) + f2;
            if (!z2 || f4 > f3 || i3 != c) {
                if (!(f4 > this.L || i3 == c || path == null)) {
                    path.lineTo(f, f4);
                }
                if (path != null) {
                    if (i2 > 1) {
                        arrayList.add(new a(path, i3));
                    }
                    path = null;
                }
                if (f4 <= this.L) {
                    path = new Path();
                    path.moveTo(f, f4);
                    z2 = true;
                    i = 1;
                } else {
                    z2 = false;
                    i = 0;
                }
                i2 = i;
                i3 = c;
            } else {
                if (path != null) {
                    path.lineTo(f, f4);
                }
                i2++;
            }
        }
        if (path != null && i2 > 1) {
            arrayList.add(new a(path, i3));
        }
        for (a aVar : arrayList) {
            Path a3 = aVar.a();
            int b2 = aVar.b();
            this.V.setColor(b2 != 0 ? b2 != 1 ? this.z : this.y : this.x);
            canvas.drawPath(a3, this.V);
        }
    }

    @DexIgnore
    public final void q(int i, int i2, int i3) {
        this.x = i3;
        this.y = i2;
        this.z = i;
    }

    @DexIgnore
    public final void setMDuration(int i) {
        this.P = i;
    }

    @DexIgnore
    public final void setMMaxHRValue(short s) {
        if (s == ((short) 0)) {
            s = (short) 100;
        }
        this.R = (short) s;
    }

    @DexIgnore
    public final void setMMinHRValue(short s) {
        this.Q = (short) s;
    }
}
