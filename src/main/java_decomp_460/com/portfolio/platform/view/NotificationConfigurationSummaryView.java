package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.hk5;
import com.fossil.p47;
import com.fossil.sq4;
import com.fossil.tj5;
import com.fossil.vn0;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.yj5;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationConfigurationSummaryView extends ViewGroup implements GestureDetector.OnGestureListener {
    @DexIgnore
    public static /* final */ int[] A; // = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Rect i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public vn0 y;
    @DexIgnore
    public a z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public NotificationConfigurationSummaryView(Context context) {
        super(context);
        g();
    }

    @DexIgnore
    public NotificationConfigurationSummaryView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.NotificationConfigurationSummaryView);
        this.b = (int) obtainStyledAttributes.getDimension(3, (float) p47.m(15.0f));
        this.c = obtainStyledAttributes.getColor(1, -1);
        this.d = obtainStyledAttributes.getString(2);
        this.e = obtainStyledAttributes.getColor(0, -65536);
        obtainStyledAttributes.recycle();
        g();
    }

    @DexIgnore
    public final boolean a(float f2, float f3, float f4) {
        return f3 <= f2 && f2 < f4;
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        int[] iArr = A;
        for (int i2 : iArr) {
            String valueOf = String.valueOf(i2);
            this.h.getTextBounds(valueOf, 0, valueOf.length(), this.i);
            double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
            int cos = (int) ((((double) (this.j / 2)) + (Math.cos(d2) * ((double) this.s))) - ((double) (this.i.width() / 2)));
            int sin = (int) ((Math.sin(d2) * ((double) this.s)) + ((double) (this.k / 2)) + ((double) (this.i.height() / 2)));
            this.g.setColor(-1);
            canvas.drawCircle((float) ((this.i.width() / 2) + cos), (float) (sin - (this.i.height() / 2)), (float) this.f, this.g);
            this.g.setColor(this.e);
            canvas.drawCircle((float) ((this.i.width() / 2) + cos), (float) (sin - (this.i.height() / 2)), ((float) this.f) - p47.b(1.0f), this.g);
            canvas.drawText(valueOf, ((float) cos) + ((((float) this.i.width()) - this.h.measureText(valueOf)) / 2.0f), (float) sin, this.h);
        }
    }

    @DexIgnore
    public final float c(float f2, float f3) {
        return ((float) Math.toDegrees(Math.atan2((double) (f3 - ((float) (getHeight() / 2))), (double) (f2 - ((float) (getWidth() / 2)))))) + 90.0f;
    }

    @DexIgnore
    public final int d(float f2) {
        if (a(f2, -15.0f, 15.0f)) {
            return 12;
        }
        if (a(f2, 15.0f, 45.0f)) {
            return 1;
        }
        if (a(f2, 45.0f, 75.0f)) {
            return 2;
        }
        if (a(f2, 75.0f, 105.0f)) {
            return 3;
        }
        if (a(f2, 105.0f, 135.0f)) {
            return 4;
        }
        if (a(f2, 135.0f, 165.0f)) {
            return 5;
        }
        if (a(f2, 165.0f, 195.0f)) {
            return 6;
        }
        if (a(f2, 195.0f, 225.0f)) {
            return 7;
        }
        if (a(f2, 225.0f, 255.0f)) {
            return 8;
        }
        if (a(f2, 255.0f, 270.0f) || a(f2, -90.0f, -70.0f)) {
            return 9;
        }
        if (a(f2, -70.0f, -40.0f)) {
            return 10;
        }
        return a(f2, -40.0f, -15.0f) ? 11 : 0;
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (!this.t) {
            f();
            this.t = true;
        }
        b(canvas);
    }

    @DexIgnore
    public final void e(AppCompatImageView appCompatImageView, int i2) {
        double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.u;
        int cos = (int) (((double) ((this.v / 2) - (i3 / 2))) + (((double) this.x) * Math.cos(d2)));
        int sin = (int) ((Math.sin(d2) * ((double) this.x)) + ((double) ((this.w / 2) - (i3 / 2))));
        appCompatImageView.layout(cos, sin, cos + i3, i3 + sin);
    }

    @DexIgnore
    public final void f() {
        this.j = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        this.k = measuredHeight;
        int min = Math.min(this.j, measuredHeight) / 2;
        this.l = min;
        int b2 = (int) (((float) min) - p47.b(50.0f));
        this.m = b2;
        this.s = (int) (((float) b2) - p47.b(20.0f));
        int i2 = this.l;
        this.f = (int) (((float) i2) / 8.5f);
        this.u = (int) (((float) i2) / 3.5f);
    }

    @DexIgnore
    public final void g() {
        Paint paint = new Paint(1);
        this.g = paint;
        paint.setStyle(Paint.Style.FILL);
        this.g.setColor(this.e);
        this.h = new Paint(1);
        if (!TextUtils.isEmpty(this.d)) {
            this.h.setTypeface(Typeface.createFromAsset(getResources().getAssets(), this.d));
        }
        this.h.setTextSize((float) this.b);
        this.h.setColor(this.c);
        this.h.setStrokeWidth(2.0f);
        this.i = new Rect();
        this.y = new vn0(getContext(), this);
    }

    @DexIgnore
    public void h(int i2, List<BaseFeatureModel> list) {
        AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        int i3 = this.u;
        layoutParams.width = i3;
        layoutParams.height = i3;
        appCompatImageView.setLayoutParams(layoutParams);
        tj5.b(appCompatImageView).I(new yj5(list)).o0(new hk5()).F0(appCompatImageView);
        appCompatImageView.setTag(123456789, Integer.valueOf(i2));
        addView(appCompatImageView);
    }

    @DexIgnore
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @DexIgnore
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.x = (int) (((float) (Math.min(i4 - i2, i5 - i3) / 2)) - p47.b(40.0f));
        this.v = getWidth();
        this.w = getHeight();
        for (int i6 = 0; i6 < childCount; i6++) {
            AppCompatImageView appCompatImageView = (AppCompatImageView) getChildAt(i6);
            e(appCompatImageView, ((Integer) appCompatImageView.getTag(123456789)).intValue());
        }
    }

    @DexIgnore
    public void onLongPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onShowPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int d2 = d(c(motionEvent.getX(), motionEvent.getY()));
        a aVar = this.z;
        if (aVar == null) {
            return true;
        }
        aVar.a(d2);
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && this.y.a(motionEvent);
    }

    @DexIgnore
    public void setOnItemClickListener(a aVar) {
        this.z = aVar;
    }
}
