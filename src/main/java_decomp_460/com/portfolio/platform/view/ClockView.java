package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.al5;
import com.fossil.gl0;
import com.fossil.p47;
import com.fossil.sq4;
import com.fossil.vn0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ClockView extends View implements GestureDetector.OnGestureListener {
    @DexIgnore
    public static /* final */ String b0; // = ClockView.class.getSimpleName();
    @DexIgnore
    public static /* final */ int[] c0; // = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public /* final */ Rect G; // = new Rect();
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public boolean P; // = true;
    @DexIgnore
    public boolean Q; // = false;
    @DexIgnore
    public boolean R; // = false;
    @DexIgnore
    public boolean S; // = false;
    @DexIgnore
    public boolean T; // = true;
    @DexIgnore
    public boolean U; // = false;
    @DexIgnore
    public float V;
    @DexIgnore
    public b W;
    @DexIgnore
    public a a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public Paint l;
    @DexIgnore
    public Paint m;
    @DexIgnore
    public Paint s;
    @DexIgnore
    public Bitmap t;
    @DexIgnore
    public vn0 u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(boolean z);

        @DexIgnore
        void b(float f);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public ClockView(Context context) {
        super(context);
        h();
    }

    @DexIgnore
    public ClockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sq4.ClockView);
        this.b = obtainStyledAttributes.getString(3);
        this.j = obtainStyledAttributes.getDimension(7, p47.b(202.0f));
        this.k = obtainStyledAttributes.getDimension(6, p47.b(314.0f));
        this.c = obtainStyledAttributes.getDimension(1, p47.b(50.0f));
        this.e = obtainStyledAttributes.getDimension(0, p47.b(50.0f));
        this.f = obtainStyledAttributes.getDimension(5, (float) p47.m(13.0f));
        this.g = obtainStyledAttributes.getColor(2, gl0.d(context, 2131099703));
        this.h = obtainStyledAttributes.getColor(4, gl0.d(context, 2131100360));
        this.i = obtainStyledAttributes.getColor(8, gl0.d(context, 2131099829));
        obtainStyledAttributes.recycle();
        h();
    }

    @DexIgnore
    private void setScale(boolean z2) {
        FLogger.INSTANCE.getLocal().d(b0, "setScale() called with: scale = [" + z2 + "]");
        if (this.F != z2) {
            if (z2) {
                float f2 = this.f;
                float f3 = this.C;
                this.f = f2 / f3;
                float f4 = this.v / f3;
                this.v = f4;
                this.B /= f3;
                this.y = f4 / 2.0f;
                this.D = 1.0f;
            } else {
                float f5 = this.f;
                float f6 = this.C;
                this.f = f5 * f6;
                this.v *= f6;
                this.B *= f6;
                this.y *= f6;
                this.D = f6;
            }
            this.F = z2;
            invalidate();
            a aVar = this.a0;
            if (aVar != null) {
                aVar.b(this.v);
                this.a0.a(this.K == this.L);
            }
        }
    }

    @DexIgnore
    public final boolean a(float f2, float f3, float f4) {
        return f3 <= f2 && f2 < f4;
    }

    @DexIgnore
    public final void b() {
        this.l.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        int i2 = this.H;
        if (i2 != -1 && this.T) {
            Matrix matrix = new Matrix();
            float f2 = this.D;
            matrix.postScale(f2, f2);
            matrix.postTranslate((((float) (-this.t.getWidth())) * this.D) / 2.0f, (((float) (-this.t.getHeight())) * this.D) / 2.0f);
            matrix.postRotate((float) Math.toDegrees(((double) i2) * 0.5235987755982988d), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (((float) this.t.getHeight()) * this.D) / 2.0f);
            matrix.postTranslate(this.x + ((float) this.I), this.y - ((((float) this.t.getHeight()) * this.D) / 2.0f));
            canvas.drawBitmap(this.t, matrix, this.s);
            matrix.reset();
        }
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        float cos;
        float sin;
        if (this.S) {
            int i2 = this.K;
            float f2 = ((float) i2) / ((float) this.L);
            float f3 = this.M * f2;
            float f4 = i2 == 0 ? this.f : this.f + f3;
            FLogger.INSTANCE.getLocal().d(b0, "drawNumeral() called with: deltaOffset = [" + f2 + "], deltaFontScale = [" + this.M + "], fontScale = [" + f3 + "], finalFontSize = [" + f4 + "], deltaXScale = [" + this.N + "], deltaYScale = [" + this.O + "]");
            float f5 = this.f;
            if (f4 <= f5) {
                f4 = f5;
            }
            this.l.setTextSize(f4);
            this.l.getTextBounds("22", 0, "22".length(), this.G);
            double d2 = ((double) (this.H - 3)) * 0.5235987755982988d;
            if (!this.U) {
                cos = ((float) (((int) ((((double) (this.w / 2.0f)) + (Math.cos(d2) * ((double) this.B))) - ((double) (this.G.width() / 2)))) + this.I)) - (this.N * f2);
                sin = ((float) ((int) (((Math.sin(d2) * ((double) this.B)) + ((double) (this.v / 2.0f))) + ((double) (this.G.height() / 2))))) - (f2 * this.O);
                if (cos - f4 <= this.c - p47.b(10.0f)) {
                    cos = (this.c - p47.b(10.0f)) + f4;
                }
                float f6 = this.d;
                if (sin - f4 <= f6) {
                    sin = f4 + f6;
                }
            } else {
                cos = ((float) (((int) ((((double) (this.w / 2.0f)) + (Math.cos(d2) * ((double) this.B))) - ((double) (this.G.width() / 2)))) + this.I)) + (this.N * f2);
                sin = ((float) ((int) (((Math.sin(d2) * ((double) this.B)) + ((double) (this.v / 2.0f))) + ((double) (this.G.height() / 2))))) - (f2 * this.O);
                if (cos + f4 >= this.c - p47.b(10.0f)) {
                    cos = (this.c - p47.b(10.0f)) - f4;
                }
                float f7 = this.d;
                if (sin - f4 <= f7) {
                    sin = f7 + f4;
                }
            }
            FLogger.INSTANCE.getLocal().d(b0, "drawNumeral() called with: x = [" + cos + "], y = [" + sin + "]");
            canvas.drawCircle(((float) (this.G.width() / 2)) + cos, sin - ((float) (this.G.height() / 2)), f4, this.m);
            this.l.setColor(this.h);
            String valueOf = String.valueOf(this.H);
            canvas.drawText(valueOf, (float) ((int) (cos + ((((float) this.G.width()) - this.l.measureText(valueOf)) / 2.0f))), sin, this.l);
            return;
        }
        int[] iArr = c0;
        for (int i3 : iArr) {
            this.l.setTextSize(this.f);
            this.l.getTextBounds("22", 0, "22".length(), this.G);
            double d3 = ((double) (i3 - 3)) * 0.5235987755982988d;
            int cos2 = ((int) ((((double) (this.w / 2.0f)) + (Math.cos(d3) * ((double) this.B))) - ((double) (this.G.width() / 2)))) + this.I;
            int sin2 = (int) ((Math.sin(d3) * ((double) this.B)) + ((double) (this.v / 2.0f)) + ((double) (this.G.height() / 2)));
            if (i3 == this.H) {
                canvas.drawCircle((float) ((this.G.width() / 2) + cos2), (float) (sin2 - (this.G.height() / 2)), this.f, this.m);
                this.l.setColor(this.h);
                if (!this.R) {
                    if (!this.U) {
                        this.N = (float) cos2;
                    } else {
                        this.N = ((float) al5.a().b()) - this.N;
                    }
                    this.O = (float) sin2;
                    this.R = true;
                }
            } else {
                this.l.setColor(this.g);
            }
            canvas.drawText(String.valueOf(i3), (float) ((int) (((((float) this.G.width()) - this.l.measureText(String.valueOf(i3))) / 2.0f) + ((float) cos2))), (float) sin2, this.l);
        }
    }

    @DexIgnore
    public final float e(float f2, float f3) {
        return ((float) Math.toDegrees(Math.atan2((double) (f3 - ((float) (getHeight() / 2))), (double) (f2 - ((float) (getWidth() / 2)))))) + 90.0f;
    }

    @DexIgnore
    public final int f(float f2) {
        if (a(f2, -15.0f, 15.0f)) {
            return 12;
        }
        if (a(f2, 15.0f, 45.0f)) {
            return 1;
        }
        if (a(f2, 45.0f, 75.0f)) {
            return 2;
        }
        if (a(f2, 75.0f, 105.0f)) {
            return 3;
        }
        if (a(f2, 105.0f, 135.0f)) {
            return 4;
        }
        if (a(f2, 135.0f, 165.0f)) {
            return 5;
        }
        if (a(f2, 165.0f, 195.0f)) {
            return 6;
        }
        if (a(f2, 195.0f, 225.0f)) {
            return 7;
        }
        if (a(f2, 225.0f, 255.0f)) {
            return 8;
        }
        if (a(f2, 255.0f, 270.0f) || a(f2, -90.0f, -70.0f)) {
            return 9;
        }
        if (a(f2, -70.0f, -40.0f)) {
            return 10;
        }
        return a(f2, -40.0f, -15.0f) ? 11 : 0;
    }

    @DexIgnore
    public final void g() {
        int i2 = this.J;
        float f2 = (float) i2;
        this.v = f2;
        float f3 = (float) i2;
        this.w = f3;
        this.x = f3 / 2.0f;
        this.y = f2 / 2.0f;
        this.z = (float) ((int) p47.b(15.0f));
        float min = (Math.min(this.v, this.w) / 2.0f) - this.z;
        this.A = min;
        float b2 = (float) ((int) (min - p47.b(15.0f)));
        this.B = b2;
        float f4 = this.f;
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), 2131231276);
        float height = (b2 - (f4 * 3.0f)) / ((float) decodeResource.getHeight());
        int width = (int) (((float) decodeResource.getWidth()) * height);
        int height2 = (int) (height * ((float) decodeResource.getHeight()));
        if (width <= 0) {
            width = decodeResource.getWidth();
        }
        if (height2 <= 0) {
            height2 = decodeResource.getHeight();
        }
        this.t = Bitmap.createScaledBitmap(decodeResource, width, height2, false);
        this.V = (this.C - 1.0f) / 20.0f;
    }

    @DexIgnore
    public final void h() {
        this.d = this.c;
        if (getContext().getResources().getConfiguration().getLayoutDirection() == 1) {
            this.c = ((float) al5.a().b()) - this.c;
            this.U = true;
        }
        this.C = this.k / this.j;
        this.F = true;
        setScale(false);
        this.l = new Paint();
        if (!TextUtils.isEmpty(this.b)) {
            this.l.setTypeface(Typeface.createFromAsset(getResources().getAssets(), this.b));
            this.l.setAntiAlias(true);
        }
        this.l.setTextSize(this.f);
        Paint paint = new Paint();
        this.m = paint;
        paint.setAntiAlias(true);
        this.m.setColor(this.i);
        this.m.setStyle(Paint.Style.FILL);
        Paint paint2 = new Paint();
        this.s = paint2;
        paint2.setAntiAlias(true);
        this.s.setFilterBitmap(true);
        this.s.setDither(true);
        this.u = new vn0(getContext(), this);
    }

    @DexIgnore
    public final int i(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            return 200;
        }
        return size;
    }

    @DexIgnore
    public void j(int i2, int i3) {
        int abs = Math.abs(i3);
        this.K = abs;
        this.L = i2;
        this.S = abs > 0;
        if (!this.Q) {
            float f2 = this.e;
            float f3 = this.f;
            this.M = f2 - f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? f2 - f3 : 0.0f;
            this.Q = true;
        }
        invalidate();
    }

    @DexIgnore
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.E) {
            g();
            this.E = true;
        }
        if (this.S) {
            d(canvas);
            return;
        }
        b();
        d(canvas);
        c(canvas);
    }

    @DexIgnore
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onLongPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4 = i(i2);
        int i5 = i(i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b0;
        local.d(str, "onMeasure() called with: measureWidth = [" + i4 + "], measureHeight = [" + i5 + "]");
        int min = Math.min(i4, i5);
        setMeasuredDimension(i4, min);
        if (this.P && i4 > i5) {
            this.J = min;
            this.I = (i4 - i5) / 2;
            this.P = false;
        }
    }

    @DexIgnore
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onShowPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int f2 = f(e(motionEvent.getX(), motionEvent.getY()));
        this.H = f2;
        b bVar = this.W;
        if (bVar != null) {
            bVar.a(f2);
        }
        invalidate();
        return true;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && this.u.a(motionEvent);
    }

    @DexIgnore
    public void setClockOnTouchListener(b bVar) {
        this.W = bVar;
    }

    @DexIgnore
    public void setCurrentHour(int i2) {
        this.H = i2;
        invalidate();
    }

    @DexIgnore
    public void setOnAnimationListener(a aVar) {
        this.a0 = aVar;
    }

    @DexIgnore
    public void setShowAnimation(boolean z2) {
        this.Q = false;
    }

    @DexIgnore
    public void setShowHand(boolean z2) {
        this.T = z2;
    }
}
