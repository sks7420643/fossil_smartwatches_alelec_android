package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LocaleChangedReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f4700a; // = LocaleChangedReceiver.class.getSimpleName();

    @DexIgnore
    public LocaleChangedReceiver() {
        PortfolioApp.d0.M().o1(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d(f4700a, "Inside .onLocaledChangedReceiver, startReloadData");
    }
}
