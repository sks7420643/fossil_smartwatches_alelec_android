package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.i47;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RestartServiceReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f4702a;

    /*
    static {
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        pq7.b(simpleName, "RestartServiceReceiver::class.java.simpleName");
        f4702a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        if (!(PortfolioApp.h0.c().J().length() == 0)) {
            FLogger.INSTANCE.getLocal().e(f4702a, "Service Tracking - startForegroundService in RestartServiceReceiver");
            i47.a.e(i47.f1583a, context, MFDeviceService.class, null, 4, null);
            i47.a.e(i47.f1583a, context, ButtonService.class, null, 4, null);
        }
    }
}
