package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.bk5;
import com.fossil.bw7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public UserRepository f4697a;
    @DexIgnore
    public on5 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public bk5 d;
    @DexIgnore
    public AlarmsRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$1", f = "AlarmReceiver.kt", l = {56, 59, 104}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AlarmReceiver alarmReceiver, int i, Intent intent, Context context, qn7 qn7) {
            super(2, qn7);
            this.this$0 = alarmReceiver;
            this.$action = i;
            this.$intent = intent;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$action, this.$intent, this.$context, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00e0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x015f  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x029f  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0313  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
            // Method dump skipped, instructions count: 929
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.receiver.AlarmReceiver.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public final bk5 a() {
        bk5 bk5 = this.d;
        if (bk5 != null) {
            return bk5;
        }
        pq7.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        pq7.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final on5 d() {
        on5 on5 = this.b;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.f4697a;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        pq7.c(context, "context");
        PortfolioApp.h0.c().M().i(this);
        FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        if (intent != null) {
            int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmReceiver", "onReceive - action=" + intExtra);
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, intExtra, intent, context, null), 3, null);
        }
    }
}
