package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.bw7;
import com.fossil.e47;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public NotificationsRepository f4698a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1", f = "AppPackageRemoveReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageRemoveReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AppPackageRemoveReceiver appPackageRemoveReceiver, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = appPackageRemoveReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$packageName, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                AppFilter f = e47.b.f(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_SAM);
                AppFilter f2 = e47.b.f(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_DIANA);
                if (f != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = AppPackageRemoveReceiver.b;
                    local.d(str, "remove app " + this.$packageName);
                    this.this$0.b().removeAppFilter(f);
                }
                if (f2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = AppPackageRemoveReceiver.b;
                    local2.d(str2, "remove app " + this.$packageName);
                    this.this$0.b().removeAppFilter(f2);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        pq7.b(simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.h0.c().M().s(this);
    }

    @DexIgnore
    public final NotificationsRepository b() {
        NotificationsRepository notificationsRepository = this.f4698a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        pq7.n("notificationsRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String schemeSpecificPart;
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onReceive " + intent.getAction());
        if (pq7.a("android.intent.action.PACKAGE_FULLY_REMOVED", intent.getAction())) {
            Uri data = intent.getData();
            String str2 = (data == null || (schemeSpecificPart = data.getSchemeSpecificPart()) == null) ? "" : schemeSpecificPart;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.d(str3, "package removed " + str2);
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, str2, null), 3, null);
        }
    }
}
