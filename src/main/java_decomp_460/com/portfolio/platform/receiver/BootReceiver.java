package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.bk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BootReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b; // = BootReceiver.class.getSimpleName();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public bk5 f4699a;

    @DexIgnore
    public BootReceiver() {
        PortfolioApp.d0.M().z(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        bk5 bk5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".BootReceiver, start hwlog sync scheduler");
        if (!TextUtils.isEmpty(intent.getAction()) && intent.getAction().equalsIgnoreCase("android.intent.action.BOOT_COMPLETED") && (bk5 = this.f4699a) != null) {
            bk5.f(context);
            this.f4699a.h(context);
        }
    }
}
