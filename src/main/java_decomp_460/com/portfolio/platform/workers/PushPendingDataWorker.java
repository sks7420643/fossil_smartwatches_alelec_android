package com.portfolio.platform.workers;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.CoroutineWorker;
import androidx.work.WorkerParameters;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ao7;
import com.fossil.bw7;
import com.fossil.co7;
import com.fossil.dl7;
import com.fossil.dr7;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.g11;
import com.fossil.gj4;
import com.fossil.gu7;
import com.fossil.h11;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jc7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.ku7;
import com.fossil.on5;
import com.fossil.p01;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.s01;
import com.fossil.tl7;
import com.fossil.tt4;
import com.fossil.vp7;
import com.fossil.vt4;
import com.fossil.xw7;
import com.fossil.y01;
import com.fossil.yn7;
import com.fossil.z01;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PushPendingDataWorker extends CoroutineWorker {
    @DexIgnore
    public static /* final */ a H; // = new a(null);
    @DexIgnore
    public /* final */ zt4 A;
    @DexIgnore
    public /* final */ FileRepository B;
    @DexIgnore
    public /* final */ UserRepository C;
    @DexIgnore
    public /* final */ WorkoutSettingRepository D;
    @DexIgnore
    public /* final */ vt4 E;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository F;
    @DexIgnore
    public /* final */ PortfolioApp G;
    @DexIgnore
    public /* final */ ActivitiesRepository i;
    @DexIgnore
    public /* final */ SummariesRepository j;
    @DexIgnore
    public /* final */ SleepSessionsRepository k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository t;
    @DexIgnore
    public /* final */ FitnessDataRepository u;
    @DexIgnore
    public /* final */ AlarmsRepository v;
    @DexIgnore
    public /* final */ on5 w;
    @DexIgnore
    public /* final */ HybridPresetRepository x;
    @DexIgnore
    public /* final */ ThirdPartyRepository y;
    @DexIgnore
    public /* final */ tt4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a() {
            z01.a aVar = new z01.a(PushPendingDataWorker.class);
            p01.a aVar2 = new p01.a();
            aVar2.b(y01.CONNECTED);
            p01 a2 = aVar2.a();
            pq7.b(a2, "Constraints.Builder().se\u2026rkType.CONNECTED).build()");
            aVar.e(a2);
            h11 b = aVar.b();
            pq7.b(b, "uploadBuilder.build()");
            z01 z01 = (z01) b;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "startScheduleUploadPendingData() - id = " + z01.a());
            g11.e(PortfolioApp.h0.c()).c("PushPendingDataWorker", s01.KEEP, z01);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements jc7<PushPendingDataWorker> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Provider<ActivitiesRepository> f4781a;
        @DexIgnore
        public /* final */ Provider<SummariesRepository> b;
        @DexIgnore
        public /* final */ Provider<SleepSessionsRepository> c;
        @DexIgnore
        public /* final */ Provider<SleepSummariesRepository> d;
        @DexIgnore
        public /* final */ Provider<GoalTrackingRepository> e;
        @DexIgnore
        public /* final */ Provider<HeartRateSampleRepository> f;
        @DexIgnore
        public /* final */ Provider<HeartRateSummaryRepository> g;
        @DexIgnore
        public /* final */ Provider<FitnessDataRepository> h;
        @DexIgnore
        public /* final */ Provider<AlarmsRepository> i;
        @DexIgnore
        public /* final */ Provider<on5> j;
        @DexIgnore
        public /* final */ Provider<HybridPresetRepository> k;
        @DexIgnore
        public /* final */ Provider<ThirdPartyRepository> l;
        @DexIgnore
        public /* final */ Provider<tt4> m;
        @DexIgnore
        public /* final */ Provider<zt4> n;
        @DexIgnore
        public /* final */ Provider<FileRepository> o;
        @DexIgnore
        public /* final */ Provider<UserRepository> p;
        @DexIgnore
        public /* final */ Provider<WorkoutSettingRepository> q;
        @DexIgnore
        public /* final */ Provider<vt4> r;
        @DexIgnore
        public /* final */ Provider<DianaWatchFaceRepository> s;
        @DexIgnore
        public /* final */ Provider<PortfolioApp> t;

        @DexIgnore
        public b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<on5> provider10, Provider<HybridPresetRepository> provider11, Provider<ThirdPartyRepository> provider12, Provider<tt4> provider13, Provider<zt4> provider14, Provider<FileRepository> provider15, Provider<UserRepository> provider16, Provider<WorkoutSettingRepository> provider17, Provider<vt4> provider18, Provider<DianaWatchFaceRepository> provider19, Provider<PortfolioApp> provider20) {
            pq7.c(provider, "mActivitiesRepository");
            pq7.c(provider2, "mSummariesRepository");
            pq7.c(provider3, "mSleepSessionRepository");
            pq7.c(provider4, "mSleepSummariesRepository");
            pq7.c(provider5, "mGoalTrackingRepository");
            pq7.c(provider6, "mHeartRateSampleRepository");
            pq7.c(provider7, "mHeartRateSummaryRepository");
            pq7.c(provider8, "mFitnessDataRepository");
            pq7.c(provider9, "mAlarmsRepository");
            pq7.c(provider10, "mSharedPreferencesManager");
            pq7.c(provider11, "mHybridPresetRepository");
            pq7.c(provider12, "mThirdPartyRepository");
            pq7.c(provider13, "mChallengeRepository");
            pq7.c(provider14, "friendRepository");
            pq7.c(provider15, "mFileRepository");
            pq7.c(provider16, "mUserRepository");
            pq7.c(provider17, "mWorkoutSettingRepository");
            pq7.c(provider18, "fcmRepository");
            pq7.c(provider19, "mDianaWatchFaceRepository");
            pq7.c(provider20, "mApp");
            this.f4781a = provider;
            this.b = provider2;
            this.c = provider3;
            this.d = provider4;
            this.e = provider5;
            this.f = provider6;
            this.g = provider7;
            this.h = provider8;
            this.i = provider9;
            this.j = provider10;
            this.k = provider11;
            this.l = provider12;
            this.m = provider13;
            this.n = provider14;
            this.o = provider15;
            this.p = provider16;
            this.q = provider17;
            this.r = provider18;
            this.s = provider19;
            this.t = provider20;
        }

        @DexIgnore
        /* renamed from: b */
        public PushPendingDataWorker a(Context context, WorkerParameters workerParameters) {
            pq7.c(context, "context");
            pq7.c(workerParameters, "parameterName");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "Factory - create()");
            ActivitiesRepository activitiesRepository = this.f4781a.get();
            pq7.b(activitiesRepository, "mActivitiesRepository.get()");
            SummariesRepository summariesRepository = this.b.get();
            pq7.b(summariesRepository, "mSummariesRepository.get()");
            SleepSessionsRepository sleepSessionsRepository = this.c.get();
            pq7.b(sleepSessionsRepository, "mSleepSessionRepository.get()");
            SleepSummariesRepository sleepSummariesRepository = this.d.get();
            pq7.b(sleepSummariesRepository, "mSleepSummariesRepository.get()");
            GoalTrackingRepository goalTrackingRepository = this.e.get();
            pq7.b(goalTrackingRepository, "mGoalTrackingRepository.get()");
            HeartRateSampleRepository heartRateSampleRepository = this.f.get();
            pq7.b(heartRateSampleRepository, "mHeartRateSampleRepository.get()");
            HeartRateSummaryRepository heartRateSummaryRepository = this.g.get();
            pq7.b(heartRateSummaryRepository, "mHeartRateSummaryRepository.get()");
            FitnessDataRepository fitnessDataRepository = this.h.get();
            pq7.b(fitnessDataRepository, "mFitnessDataRepository.get()");
            AlarmsRepository alarmsRepository = this.i.get();
            pq7.b(alarmsRepository, "mAlarmsRepository.get()");
            on5 on5 = this.j.get();
            pq7.b(on5, "mSharedPreferencesManager.get()");
            HybridPresetRepository hybridPresetRepository = this.k.get();
            pq7.b(hybridPresetRepository, "mHybridPresetRepository.get()");
            ThirdPartyRepository thirdPartyRepository = this.l.get();
            pq7.b(thirdPartyRepository, "mThirdPartyRepository.get()");
            tt4 tt4 = this.m.get();
            pq7.b(tt4, "mChallengeRepository.get()");
            zt4 zt4 = this.n.get();
            pq7.b(zt4, "friendRepository.get()");
            FileRepository fileRepository = this.o.get();
            pq7.b(fileRepository, "mFileRepository.get()");
            UserRepository userRepository = this.p.get();
            pq7.b(userRepository, "mUserRepository.get()");
            WorkoutSettingRepository workoutSettingRepository = this.q.get();
            pq7.b(workoutSettingRepository, "mWorkoutSettingRepository.get()");
            vt4 vt4 = this.r.get();
            pq7.b(vt4, "fcmRepository.get()");
            DianaWatchFaceRepository dianaWatchFaceRepository = this.s.get();
            pq7.b(dianaWatchFaceRepository, "mDianaWatchFaceRepository.get()");
            PortfolioApp portfolioApp = this.t.get();
            pq7.b(portfolioApp, "mApp.get()");
            return new PushPendingDataWorker(workerParameters, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, goalTrackingRepository, heartRateSampleRepository, heartRateSummaryRepository, fitnessDataRepository, alarmsRepository, on5, hybridPresetRepository, thirdPartyRepository, tt4, zt4, fileRepository, userRepository, workoutSettingRepository, vt4, dianaWatchFaceRepository, portfolioApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {96}, m = "doWork")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PushPendingDataWorker pushPendingDataWorker, qn7 qn7) {
            super(qn7);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ku7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ActivitiesRepository.PushPendingActivitiesCallback {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ d f4782a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$d$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$d$a$a  reason: collision with other inner class name */
            public static final class C0363a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ dr7 $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ dr7 $startDate;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0363a(a aVar, dr7 dr7, dr7 dr72, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$startDate = dr7;
                    this.$endDate = dr72;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0363a aVar = new C0363a(this.this$0, this.$startDate, this.$endDate, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0363a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        SummariesRepository summariesRepository = this.this$0.f4782a.this$0.j;
                        Date date = this.$startDate.element.toLocalDateTime().toDate();
                        pq7.b(date, "startDate.toLocalDateTime().toDate()");
                        Date date2 = this.$endDate.element.toLocalDateTime().toDate();
                        pq7.b(date2, "endDate.toLocalDateTime().toDate()");
                        this.L$0 = iv7;
                        this.label = 1;
                        if (summariesRepository.loadSummaries(date, date2, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(d dVar) {
                this.f4782a = dVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingActivities onFail, go to next, errorCode = " + i);
                if (this.f4782a.$cancellableContinuation.isActive()) {
                    ku7 ku7 = this.f4782a.$cancellableContinuation;
                    Boolean bool = Boolean.FALSE;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onSuccess(List<ActivitySample> list) {
                pq7.c(list, "activityList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingActivities onSuccess, go to next");
                if (!list.isEmpty()) {
                    dr7 dr7 = new dr7();
                    dr7.element = (T) list.get(0).getStartTime();
                    dr7 dr72 = new dr7();
                    dr72.element = (T) list.get(0).getEndTime();
                    for (ActivitySample activitySample : list) {
                        if (activitySample.getStartTime().getMillis() < dr7.element.getMillis()) {
                            dr7.element = (T) activitySample.getStartTime();
                        }
                        if (activitySample.getEndTime().getMillis() < dr72.element.getMillis()) {
                            dr72.element = (T) activitySample.getEndTime();
                        }
                    }
                    xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new C0363a(this, dr7, dr72, null), 3, null);
                }
                if (this.f4782a.$cancellableContinuation.isActive()) {
                    ku7 ku7 = this.f4782a.$cancellableContinuation;
                    Boolean bool = Boolean.TRUE;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(bool));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ku7 ku7, qn7 qn7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, qn7);
            this.$cancellableContinuation = ku7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.$cancellableContinuation, qn7, this.this$0);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ActivitiesRepository activitiesRepository = this.this$0.i;
                a aVar = new a(this);
                this.L$0 = iv7;
                this.label = 1;
                if (activitiesRepository.pushPendingActivities(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ku7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements SleepSessionsRepository.PushPendingSleepSessionsCallback {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ e f4783a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a  reason: collision with other inner class name */
            public static final class C0364a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $sleepSessionList;
                @DexIgnore
                public int I$0;
                @DexIgnore
                public int I$1;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a  reason: collision with other inner class name */
                public static final class C0365a extends ko7 implements vp7<iv7, qn7<? super iq5<gj4>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $end;
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $start;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0364a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0365a(C0364a aVar, Calendar calendar, Calendar calendar2, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                        this.$start = calendar;
                        this.$end = calendar2;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0365a aVar = new C0365a(this.this$0, this.$start, this.$end, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super iq5<gj4>> qn7) {
                        return ((C0365a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.f4783a.this$0.l;
                            Calendar calendar = this.$start;
                            pq7.b(calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            Date time = calendar.getTime();
                            pq7.b(time, "start.time");
                            Calendar calendar2 = this.$end;
                            pq7.b(calendar2, "end");
                            Date time2 = calendar2.getTime();
                            pq7.b(time2, "end.time");
                            this.L$0 = iv7;
                            this.label = 1;
                            Object fetchSleepSummaries = sleepSummariesRepository.fetchSleepSummaries(time, time2, this);
                            return fetchSleepSummaries == d ? d : fetchSleepSummaries;
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0364a(a aVar, List list, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$sleepSessionList = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0364a aVar = new C0364a(this.this$0, this.$sleepSessionList, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0364a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        if (!this.$sleepSessionList.isEmpty()) {
                            int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                            int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                            int i2 = realEndTime;
                            for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                                if (mFSleepSession.getRealStartTime() < realStartTime) {
                                    realStartTime = mFSleepSession.getRealStartTime();
                                }
                                if (mFSleepSession.getRealEndTime() > i2) {
                                    i2 = mFSleepSession.getRealEndTime();
                                }
                            }
                            Calendar instance = Calendar.getInstance();
                            pq7.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            instance.setTimeInMillis((long) realStartTime);
                            Calendar instance2 = Calendar.getInstance();
                            pq7.b(instance2, "end");
                            instance2.setTimeInMillis((long) i2);
                            dv7 b = bw7.b();
                            C0365a aVar = new C0365a(this, instance, instance2, null);
                            this.L$0 = iv7;
                            this.I$0 = realStartTime;
                            this.I$1 = i2;
                            this.L$1 = instance;
                            this.L$2 = instance2;
                            this.label = 1;
                            if (eu7.g(b, aVar, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        Calendar calendar = (Calendar) this.L$2;
                        Calendar calendar2 = (Calendar) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.f4783a.$cancellableContinuation.isActive()) {
                        ku7 ku7 = this.this$0.f4783a.$cancellableContinuation;
                        Boolean a2 = ao7.a(true);
                        dl7.a aVar2 = dl7.Companion;
                        ku7.resumeWith(dl7.m1constructorimpl(a2));
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(e eVar) {
                this.f4783a = eVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingSleepSessions onFail, go to next, errorCode = " + i);
                if (this.f4783a.$cancellableContinuation.isActive()) {
                    ku7 ku7 = this.f4783a.$cancellableContinuation;
                    Boolean bool = Boolean.TRUE;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onSuccess(List<MFSleepSession> list) {
                pq7.c(list, "sleepSessionList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingSleepSessions onSuccess, go to next");
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new C0364a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ku7 ku7, qn7 qn7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, qn7);
            this.$cancellableContinuation = ku7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$cancellableContinuation, qn7, this.this$0);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                SleepSessionsRepository sleepSessionsRepository = this.this$0.k;
                a aVar = new a(this);
                this.L$0 = iv7;
                this.label = 1;
                if (sleepSessionsRepository.pushPendingSleepSessions(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ku7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ f f4784a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a  reason: collision with other inner class name */
            public static final class C0366a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $goalTrackingList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a  reason: collision with other inner class name */
                public static final class C0367a extends ko7 implements vp7<iv7, qn7<? super iq5<ApiResponse<GoalDailySummary>>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ dr7 $endDate;
                    @DexIgnore
                    public /* final */ /* synthetic */ dr7 $startDate;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0366a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0367a(C0366a aVar, dr7 dr7, dr7 dr72, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                        this.$startDate = dr7;
                        this.$endDate = dr72;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0367a aVar = new C0367a(this.this$0, this.$startDate, this.$endDate, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super iq5<ApiResponse<GoalDailySummary>>> qn7) {
                        return ((C0367a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            this.L$0 = this.p$;
                            this.label = 1;
                            Object loadSummaries = this.this$0.this$0.f4784a.this$0.m.loadSummaries(this.$startDate.element, this.$endDate.element, this);
                            return loadSummaries == d ? d : loadSummaries;
                        } else if (i == 1) {
                            iv7 iv7 = (iv7) this.L$0;
                            el7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0366a(a aVar, List list, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$goalTrackingList = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0366a aVar = new C0366a(this.this$0, this.$goalTrackingList, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0366a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        dr7 dr7 = new dr7();
                        dr7.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        dr7 dr72 = new dr7();
                        dr72.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        for (GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                            if (goalTrackingData.getDate().getTime() < dr7.element.getTime()) {
                                dr7.element = (T) goalTrackingData.getDate();
                            }
                            if (goalTrackingData.getDate().getTime() > dr72.element.getTime()) {
                                dr72.element = (T) goalTrackingData.getDate();
                            }
                        }
                        dv7 b = bw7.b();
                        C0367a aVar = new C0367a(this, dr7, dr72, null);
                        this.L$0 = iv7;
                        this.L$1 = dr7;
                        this.L$2 = dr72;
                        this.label = 1;
                        if (eu7.g(b, aVar, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        dr7 dr73 = (dr7) this.L$2;
                        dr7 dr74 = (dr7) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.f4784a.$cancellableContinuation.isActive()) {
                        ku7 ku7 = this.this$0.f4784a.$cancellableContinuation;
                        Boolean a2 = ao7.a(true);
                        dl7.a aVar2 = dl7.Companion;
                        ku7.resumeWith(dl7.m1constructorimpl(a2));
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(f fVar) {
                this.f4784a = fVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
                if (this.f4784a.$cancellableContinuation.isActive()) {
                    ku7 ku7 = this.f4784a.$cancellableContinuation;
                    Boolean bool = Boolean.FALSE;
                    dl7.a aVar = dl7.Companion;
                    ku7.resumeWith(dl7.m1constructorimpl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onSuccess(List<GoalTrackingData> list) {
                pq7.c(list, "goalTrackingList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onSuccess, go to next");
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new C0366a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ku7 ku7, qn7 qn7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, qn7);
            this.$cancellableContinuation = ku7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.$cancellableContinuation, qn7, this.this$0);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                GoalTrackingRepository goalTrackingRepository = this.this$0.m;
                a aVar = new a(this);
                this.L$0 = iv7;
                this.label = 1;
                if (goalTrackingRepository.pushPendingGoalTrackingDataList(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {106, 290, 298, 184, 185, 187, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 192, 226, 229, 306, 263, 265, 271, 272}, m = VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE)
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PushPendingDataWorker pushPendingDataWorker, qn7 qn7) {
            super(qn7);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.z(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$4", f = "PushPendingDataWorker.kt", l = {208, 209, 211, 212, 214, 215, 216}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $startDate;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PushPendingDataWorker pushPendingDataWorker, dr7 dr7, dr7 dr72, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pushPendingDataWorker;
            this.$startDate = dr7;
            this.$endDate = dr72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$startDate, this.$endDate, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0070  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00ab  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0121  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x015b  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0197  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x019a  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0037  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 436
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ThirdPartyRepository.PushPendingThirdPartyDataCallback {
        @DexIgnore
        @Override // com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback
        public void onComplete() {
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushThirdPartyPendingData - Complete");
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PushPendingDataWorker(androidx.work.WorkerParameters r4, com.portfolio.platform.data.source.ActivitiesRepository r5, com.portfolio.platform.data.source.SummariesRepository r6, com.portfolio.platform.data.source.SleepSessionsRepository r7, com.portfolio.platform.data.source.SleepSummariesRepository r8, com.portfolio.platform.data.source.GoalTrackingRepository r9, com.portfolio.platform.data.source.HeartRateSampleRepository r10, com.portfolio.platform.data.source.HeartRateSummaryRepository r11, com.portfolio.platform.data.source.FitnessDataRepository r12, com.portfolio.platform.data.source.AlarmsRepository r13, com.fossil.on5 r14, com.portfolio.platform.data.source.HybridPresetRepository r15, com.portfolio.platform.data.source.ThirdPartyRepository r16, com.fossil.tt4 r17, com.fossil.zt4 r18, com.portfolio.platform.data.source.FileRepository r19, com.portfolio.platform.data.source.UserRepository r20, com.portfolio.platform.data.source.WorkoutSettingRepository r21, com.fossil.vt4 r22, com.portfolio.platform.data.source.DianaWatchFaceRepository r23, com.portfolio.platform.PortfolioApp r24) {
        /*
            r3 = this;
            java.lang.String r1 = "mWorkerParameters"
            com.fossil.pq7.c(r4, r1)
            java.lang.String r1 = "mActivitiesRepository"
            com.fossil.pq7.c(r5, r1)
            java.lang.String r1 = "mSummariesRepository"
            com.fossil.pq7.c(r6, r1)
            java.lang.String r1 = "mSleepSessionRepository"
            com.fossil.pq7.c(r7, r1)
            java.lang.String r1 = "mSleepSummariesRepository"
            com.fossil.pq7.c(r8, r1)
            java.lang.String r1 = "mGoalTrackingRepository"
            com.fossil.pq7.c(r9, r1)
            java.lang.String r1 = "mHeartRateSampleRepository"
            com.fossil.pq7.c(r10, r1)
            java.lang.String r1 = "mHeartRateSummaryRepository"
            com.fossil.pq7.c(r11, r1)
            java.lang.String r1 = "mFitnessDataRepository"
            com.fossil.pq7.c(r12, r1)
            java.lang.String r1 = "mAlarmsRepository"
            com.fossil.pq7.c(r13, r1)
            java.lang.String r1 = "mSharedPreferencesManager"
            com.fossil.pq7.c(r14, r1)
            java.lang.String r1 = "mHybridPresetRepository"
            com.fossil.pq7.c(r15, r1)
            java.lang.String r1 = "mThirdPartyRepository"
            r0 = r16
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mChallengeRepository"
            r0 = r17
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "friendRepository"
            r0 = r18
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mFileRepository"
            r0 = r19
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mUserRepository"
            r0 = r20
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mWorkoutSettingRepository"
            r0 = r21
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "fcmRepository"
            r0 = r22
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mDianaWatchFaceRepository"
            r0 = r23
            com.fossil.pq7.c(r0, r1)
            java.lang.String r1 = "mApp"
            r0 = r24
            com.fossil.pq7.c(r0, r1)
            android.content.Context r1 = r24.getApplicationContext()
            java.lang.String r2 = "mApp.applicationContext"
            com.fossil.pq7.b(r1, r2)
            r3.<init>(r1, r4)
            r3.i = r5
            r3.j = r6
            r3.k = r7
            r3.l = r8
            r3.m = r9
            r3.s = r10
            r3.t = r11
            r3.u = r12
            r3.v = r13
            r3.w = r14
            r3.x = r15
            r0 = r16
            r3.y = r0
            r0 = r17
            r3.z = r0
            r0 = r18
            r3.A = r0
            r0 = r19
            r3.B = r0
            r0 = r20
            r3.C = r0
            r0 = r21
            r3.D = r0
            r0 = r22
            r3.E = r0
            r0 = r23
            r3.F = r0
            r0 = r24
            r3.G = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.<init>(androidx.work.WorkerParameters, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.HeartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.AlarmsRepository, com.fossil.on5, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.fossil.tt4, com.fossil.zt4, com.portfolio.platform.data.source.FileRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.data.source.WorkoutSettingRepository, com.fossil.vt4, com.portfolio.platform.data.source.DianaWatchFaceRepository, com.portfolio.platform.PortfolioApp):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    @Override // androidx.work.CoroutineWorker
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object o(com.fossil.qn7<? super androidx.work.ListenableWorker.a> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.workers.PushPendingDataWorker.c
            if (r0 == 0) goto L_0x0030
            r0 = r7
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = (com.portfolio.platform.workers.PushPendingDataWorker.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0030
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003e
            if (r3 != r5) goto L_0x0036
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r0 = (com.portfolio.platform.workers.PushPendingDataWorker) r0
            com.fossil.el7.b(r2)
        L_0x0026:
            androidx.work.ListenableWorker$a r0 = androidx.work.ListenableWorker.a.c()
            java.lang.String r1 = "Result.success()"
            com.fossil.pq7.b(r0, r1)
        L_0x002f:
            return r0
        L_0x0030:
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = new com.portfolio.platform.workers.PushPendingDataWorker$c
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "doWork() - id = "
            r3.append(r4)
            java.util.UUID r4 = r6.d()
            r3.append(r4)
            java.lang.String r4 = "PushPendingDataWorker"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r6.z(r0)
            if (r0 != r1) goto L_0x0026
            r0 = r1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.o(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x041f  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0437  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x047c  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0485  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x048c  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0224  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0275  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02c3  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0310  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0315  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0381  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object z(com.fossil.qn7<? super com.fossil.tl7> r21) {
        /*
        // Method dump skipped, instructions count: 1206
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.z(com.fossil.qn7):java.lang.Object");
    }
}
