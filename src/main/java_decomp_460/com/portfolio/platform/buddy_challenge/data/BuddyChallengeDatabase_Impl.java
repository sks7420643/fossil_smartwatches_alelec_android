package com.portfolio.platform.buddy_challenge.data;

import com.facebook.share.internal.ShareConstants;
import com.fossil.ex0;
import com.fossil.ft4;
import com.fossil.gt4;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.jt4;
import com.fossil.kt4;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qs4;
import com.fossil.qw0;
import com.fossil.rs4;
import com.fossil.sw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.ys4;
import com.fossil.zs4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BuddyChallengeDatabase_Impl extends BuddyChallengeDatabase {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public volatile ys4 f4689a;
    @DexIgnore
    public volatile jt4 b;
    @DexIgnore
    public volatile qs4 c;
    @DexIgnore
    public volatile ft4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sw0.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `profile` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `createdAt` TEXT, `updatedAt` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `friend` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `confirmation` INTEGER NOT NULL, `pin` INTEGER NOT NULL, `friendType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `syncData` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `category` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `notification` (`id` TEXT NOT NULL, `titleKey` TEXT NOT NULL, `bodyKey` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `challengeData` TEXT, `profileData` TEXT, `confirmChallenge` INTEGER NOT NULL, `rank` INTEGER NOT NULL, `reachGoalAt` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `fitness` (`pinType` INTEGER NOT NULL, `id` INTEGER NOT NULL, `encryptMethod` INTEGER NOT NULL, `encryptedData` TEXT NOT NULL, `keyType` INTEGER NOT NULL, `sequence` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `display_player` (`challengeId` TEXT NOT NULL, `numberOfPlayers` INTEGER, `topPlayers` TEXT, `nearPlayers` TEXT, PRIMARY KEY(`challengeId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `challenge_player` (`challengeId` TEXT NOT NULL, `challengeName` TEXT NOT NULL, `players` TEXT NOT NULL, PRIMARY KEY(`challengeId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `history_challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `players` TEXT NOT NULL, `topPlayer` TEXT, `currentPlayer` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `completedAt` TEXT, `isFirst` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `players` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `isDeleted` INTEGER, `ranking` INTEGER, `status` TEXT, `stepsBase` INTEGER, `stepsOffset` INTEGER, `caloriesBase` INTEGER, `caloriesOffset` INTEGER, `profilePicture` TEXT, `lastSync` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b238af0aae08cbacb61a8a87455c3eee')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `profile`");
            lx0.execSQL("DROP TABLE IF EXISTS `friend`");
            lx0.execSQL("DROP TABLE IF EXISTS `challenge`");
            lx0.execSQL("DROP TABLE IF EXISTS `notification`");
            lx0.execSQL("DROP TABLE IF EXISTS `fitness`");
            lx0.execSQL("DROP TABLE IF EXISTS `display_player`");
            lx0.execSQL("DROP TABLE IF EXISTS `challenge_player`");
            lx0.execSQL("DROP TABLE IF EXISTS `history_challenge`");
            lx0.execSQL("DROP TABLE IF EXISTS `players`");
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            BuddyChallengeDatabase_Impl.this.mDatabase = lx0;
            BuddyChallengeDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("socialId", new ix0.a("socialId", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_FIRST_NAME, new ix0.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_LAST_NAME, new ix0.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put("points", new ix0.a("points", "INTEGER", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_PROFILE_PIC, new ix0.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            ix0 ix0 = new ix0("profile", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "profile");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "profile(com.portfolio.platform.buddy_challenge.data.Profile).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap2.put("socialId", new ix0.a("socialId", "TEXT", true, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_FIRST_NAME, new ix0.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_LAST_NAME, new ix0.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put("points", new ix0.a("points", "INTEGER", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_PROFILE_PIC, new ix0.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap2.put("confirmation", new ix0.a("confirmation", "INTEGER", true, 0, null, 1));
            hashMap2.put("pin", new ix0.a("pin", "INTEGER", true, 0, null, 1));
            hashMap2.put("friendType", new ix0.a("friendType", "INTEGER", true, 0, null, 1));
            ix0 ix02 = new ix0("friend", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "friend");
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "friend(com.portfolio.platform.buddy_challenge.data.Friend).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(17);
            hashMap3.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap3.put("type", new ix0.a("type", "TEXT", false, 0, null, 1));
            hashMap3.put("name", new ix0.a("name", "TEXT", false, 0, null, 1));
            hashMap3.put("des", new ix0.a("des", "TEXT", false, 0, null, 1));
            hashMap3.put("owner", new ix0.a("owner", "TEXT", false, 0, null, 1));
            hashMap3.put("numberOfPlayers", new ix0.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put("target", new ix0.a("target", "INTEGER", false, 0, null, 1));
            hashMap3.put("duration", new ix0.a("duration", "INTEGER", false, 0, null, 1));
            hashMap3.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new ix0.a(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap3.put("version", new ix0.a("version", "TEXT", false, 0, null, 1));
            hashMap3.put("status", new ix0.a("status", "TEXT", false, 0, null, 1));
            hashMap3.put("syncData", new ix0.a("syncData", "TEXT", false, 0, null, 1));
            hashMap3.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap3.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap3.put("category", new ix0.a("category", "TEXT", false, 0, null, 1));
            ix0 ix03 = new ix0("challenge", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "challenge");
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "challenge(com.portfolio.platform.buddy_challenge.data.Challenge).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(9);
            hashMap4.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap4.put("titleKey", new ix0.a("titleKey", "TEXT", true, 0, null, 1));
            hashMap4.put("bodyKey", new ix0.a("bodyKey", "TEXT", true, 0, null, 1));
            hashMap4.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap4.put("challengeData", new ix0.a("challengeData", "TEXT", false, 0, null, 1));
            hashMap4.put("profileData", new ix0.a("profileData", "TEXT", false, 0, null, 1));
            hashMap4.put("confirmChallenge", new ix0.a("confirmChallenge", "INTEGER", true, 0, null, 1));
            hashMap4.put("rank", new ix0.a("rank", "INTEGER", true, 0, null, 1));
            hashMap4.put("reachGoalAt", new ix0.a("reachGoalAt", "TEXT", false, 0, null, 1));
            ix0 ix04 = new ix0("notification", hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, "notification");
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "notification(com.portfolio.platform.buddy_challenge.data.Notification).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap5.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("encryptMethod", new ix0.a("encryptMethod", "INTEGER", true, 0, null, 1));
            hashMap5.put("encryptedData", new ix0.a("encryptedData", "TEXT", true, 0, null, 1));
            hashMap5.put("keyType", new ix0.a("keyType", "INTEGER", true, 0, null, 1));
            hashMap5.put("sequence", new ix0.a("sequence", "TEXT", true, 0, null, 1));
            ix0 ix05 = new ix0("fitness", hashMap5, new HashSet(0), new HashSet(0));
            ix0 a6 = ix0.a(lx0, "fitness");
            if (!ix05.equals(a6)) {
                return new sw0.b(false, "fitness(com.portfolio.platform.buddy_challenge.data.BCFitnessData).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("challengeId", new ix0.a("challengeId", "TEXT", true, 1, null, 1));
            hashMap6.put("numberOfPlayers", new ix0.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap6.put("topPlayers", new ix0.a("topPlayers", "TEXT", false, 0, null, 1));
            hashMap6.put("nearPlayers", new ix0.a("nearPlayers", "TEXT", false, 0, null, 1));
            ix0 ix06 = new ix0("display_player", hashMap6, new HashSet(0), new HashSet(0));
            ix0 a7 = ix0.a(lx0, "display_player");
            if (!ix06.equals(a7)) {
                return new sw0.b(false, "display_player(com.portfolio.platform.buddy_challenge.data.BCDisplayPlayer).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(3);
            hashMap7.put("challengeId", new ix0.a("challengeId", "TEXT", true, 1, null, 1));
            hashMap7.put("challengeName", new ix0.a("challengeName", "TEXT", true, 0, null, 1));
            hashMap7.put("players", new ix0.a("players", "TEXT", true, 0, null, 1));
            ix0 ix07 = new ix0("challenge_player", hashMap7, new HashSet(0), new HashSet(0));
            ix0 a8 = ix0.a(lx0, "challenge_player");
            if (!ix07.equals(a8)) {
                return new sw0.b(false, "challenge_player(com.portfolio.platform.buddy_challenge.data.BCChallengePlayer).\n Expected:\n" + ix07 + "\n Found:\n" + a8);
            }
            HashMap hashMap8 = new HashMap(20);
            hashMap8.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap8.put("type", new ix0.a("type", "TEXT", false, 0, null, 1));
            hashMap8.put("name", new ix0.a("name", "TEXT", false, 0, null, 1));
            hashMap8.put("des", new ix0.a("des", "TEXT", false, 0, null, 1));
            hashMap8.put("owner", new ix0.a("owner", "TEXT", false, 0, null, 1));
            hashMap8.put("numberOfPlayers", new ix0.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put("target", new ix0.a("target", "INTEGER", false, 0, null, 1));
            hashMap8.put("duration", new ix0.a("duration", "INTEGER", false, 0, null, 1));
            hashMap8.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new ix0.a(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap8.put("version", new ix0.a("version", "TEXT", false, 0, null, 1));
            hashMap8.put("status", new ix0.a("status", "TEXT", false, 0, null, 1));
            hashMap8.put("players", new ix0.a("players", "TEXT", true, 0, null, 1));
            hashMap8.put("topPlayer", new ix0.a("topPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("currentPlayer", new ix0.a("currentPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("createdAt", new ix0.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap8.put("updatedAt", new ix0.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("completedAt", new ix0.a("completedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("isFirst", new ix0.a("isFirst", "INTEGER", true, 0, null, 1));
            ix0 ix08 = new ix0("history_challenge", hashMap8, new HashSet(0), new HashSet(0));
            ix0 a9 = ix0.a(lx0, "history_challenge");
            if (!ix08.equals(a9)) {
                return new sw0.b(false, "history_challenge(com.portfolio.platform.buddy_challenge.data.HistoryChallenge).\n Expected:\n" + ix08 + "\n Found:\n" + a9);
            }
            HashMap hashMap9 = new HashMap(13);
            hashMap9.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap9.put("socialId", new ix0.a("socialId", "TEXT", true, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_FIRST_NAME, new ix0.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_LAST_NAME, new ix0.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put("isDeleted", new ix0.a("isDeleted", "INTEGER", false, 0, null, 1));
            hashMap9.put("ranking", new ix0.a("ranking", "INTEGER", false, 0, null, 1));
            hashMap9.put("status", new ix0.a("status", "TEXT", false, 0, null, 1));
            hashMap9.put("stepsBase", new ix0.a("stepsBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("stepsOffset", new ix0.a("stepsOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesBase", new ix0.a("caloriesBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesOffset", new ix0.a("caloriesOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_PROFILE_PIC, new ix0.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap9.put("lastSync", new ix0.a("lastSync", "TEXT", false, 0, null, 1));
            ix0 ix09 = new ix0("players", hashMap9, new HashSet(0), new HashSet(0));
            ix0 a10 = ix0.a(lx0, "players");
            if (ix09.equals(a10)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "players(com.portfolio.platform.buddy_challenge.data.BCPlayer).\n Expected:\n" + ix09 + "\n Found:\n" + a10);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public qs4 a() {
        qs4 qs4;
        if (this.c != null) {
            return this.c;
        }
        synchronized (this) {
            if (this.c == null) {
                this.c = new rs4(this);
            }
            qs4 = this.c;
        }
        return qs4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public ys4 b() {
        ys4 ys4;
        if (this.f4689a != null) {
            return this.f4689a;
        }
        synchronized (this) {
            if (this.f4689a == null) {
                this.f4689a = new zs4(this);
            }
            ys4 = this.f4689a;
        }
        return ys4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public ft4 c() {
        ft4 ft4;
        if (this.d != null) {
            return this.d;
        }
        synchronized (this) {
            if (this.d == null) {
                this.d = new gt4(this);
            }
            ft4 = this.d;
        }
        return ft4;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `profile`");
            writableDatabase.execSQL("DELETE FROM `friend`");
            writableDatabase.execSQL("DELETE FROM `challenge`");
            writableDatabase.execSQL("DELETE FROM `notification`");
            writableDatabase.execSQL("DELETE FROM `fitness`");
            writableDatabase.execSQL("DELETE FROM `display_player`");
            writableDatabase.execSQL("DELETE FROM `challenge_player`");
            writableDatabase.execSQL("DELETE FROM `history_challenge`");
            writableDatabase.execSQL("DELETE FROM `players`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "profile", "friend", "challenge", "notification", "fitness", "display_player", "challenge_player", "history_challenge", "players");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new a(1), "b238af0aae08cbacb61a8a87455c3eee", "5290fb2a20a9967b5a4b27ffa5f1bb99");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public jt4 d() {
        jt4 jt4;
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b == null) {
                this.b = new kt4(this);
            }
            jt4 = this.b;
        }
        return jt4;
    }
}
