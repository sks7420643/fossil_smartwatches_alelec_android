package com.portfolio.platform.buddy_challenge.data;

import com.fossil.ft4;
import com.fossil.jt4;
import com.fossil.qs4;
import com.fossil.qw0;
import com.fossil.ys4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BuddyChallengeDatabase extends qw0 {
    @DexIgnore
    public abstract qs4 a();

    @DexIgnore
    public abstract ys4 b();

    @DexIgnore
    public abstract ft4 c();

    @DexIgnore
    public abstract jt4 d();
}
