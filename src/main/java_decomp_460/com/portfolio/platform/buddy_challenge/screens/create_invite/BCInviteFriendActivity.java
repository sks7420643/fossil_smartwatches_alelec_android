package com.portfolio.platform.buddy_challenge.screens.create_invite;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ts4;
import com.fossil.vu4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCInviteFriendActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, ts4 ts4) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public final void b(Fragment fragment, ts4 ts4) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivityForResult(intent, 11);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((vu4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(vu4.u.b((ts4) getIntent().getParcelableExtra("challenge_draft_extra")), vu4.u.a(), 2131362158);
        }
    }
}
