package com.portfolio.platform.buddy_challenge.screens.create_input;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.kq7;
import com.fossil.ku4;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.tl7;
import com.fossil.ts4;
import com.fossil.vs4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateChallengeInputActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, ts4 ts4) {
            pq7.c(fragment, "fragment");
            pq7.c(ts4, "draft");
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivityForResult(intent, 14);
        }

        @DexIgnore
        public final void b(Fragment fragment, vs4 vs4) {
            pq7.c(fragment, "fragment");
            pq7.c(vs4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_template_extra", vs4);
            fragment.startActivityForResult(intent, 11);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((ku4) getSupportFragmentManager().Y(2131362158)) == null) {
            ku4 b = ku4.t.b((vs4) getIntent().getParcelableExtra("challenge_template_extra"), (ts4) getIntent().getParcelableExtra("challenge_draft_extra"));
            if (b != null) {
                k(b, ku4.t.a(), 2131362158);
                tl7 tl7 = tl7.f3441a;
                return;
            }
            pq7.i();
            throw null;
        }
    }
}
