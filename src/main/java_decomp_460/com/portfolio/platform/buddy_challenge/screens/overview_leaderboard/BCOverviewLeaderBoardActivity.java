package com.portfolio.platform.buddy_challenge.screens.overview_leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.fw4;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ps4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCOverviewLeaderBoardActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, ps4 ps4, String str) {
            pq7.c(fragment, "fragment");
            pq7.c(ps4, "challenge");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("BCOverviewLeaderBoardActivity", "startActivity - challenge: " + ps4 + " - historyId: " + str);
            Intent intent = new Intent(fragment.getContext(), BCOverviewLeaderBoardActivity.class);
            intent.putExtra("challenge_extra", ps4);
            intent.putExtra("challenge_history_id_extra", str);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (!(Y instanceof fw4)) {
            Y = null;
        }
        if (((fw4) Y) == null) {
            ps4 ps4 = (ps4) getIntent().getParcelableExtra("challenge_extra");
            String stringExtra = getIntent().getStringExtra("challenge_history_id_extra");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.e(r, "onCreate - challenge: " + ps4 + " - historyId: " + stringExtra);
            j(fw4.t.b(ps4, stringExtra), fw4.t.a());
        }
    }
}
