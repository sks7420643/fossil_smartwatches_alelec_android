package com.portfolio.platform.buddy_challenge.screens.leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.gv4;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCLeaderBoardActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, long j, int i, String str2) {
            pq7.c(fragment, "fragment");
            pq7.c(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCLeaderBoardActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_start_time_extra", j);
            intent.putExtra("challenge_target_extra", i);
            intent.putExtra("challenge_status_extra", str2);
            fragment.startActivityForResult(intent, 15);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((gv4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(gv4.v.b(getIntent().getStringExtra("challenge_id_extra"), getIntent().getLongExtra("challenge_start_time_extra", 0), getIntent().getIntExtra("challenge_target_extra", -1), getIntent().getStringExtra("challenge_status_extra")), gv4.v.a(), 2131362158);
        }
    }
}
