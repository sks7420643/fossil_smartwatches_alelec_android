package com.portfolio.platform.buddy_challenge.util;

import android.util.SparseArray;
import androidx.lifecycle.Lifecycle;
import com.fossil.bz4;
import com.fossil.cs0;
import com.fossil.ms0;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerViewObserver implements cs0 {
    @DexIgnore
    public SparseArray<bz4> b; // = new SparseArray<>();

    @DexIgnore
    public final void a() {
        this.b.clear();
    }

    @DexIgnore
    public final void c(bz4 bz4, int i) {
        pq7.c(bz4, "aware");
        this.b.put(i, bz4);
    }

    @DexIgnore
    public final void d(int i) {
        this.b.remove(i);
    }

    @DexIgnore
    @ms0(Lifecycle.a.ON_PAUSE)
    public final void onPause() {
        if (this.b.size() != 0) {
            SparseArray<bz4> sparseArray = this.b;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onPause();
            }
        }
    }

    @DexIgnore
    @ms0(Lifecycle.a.ON_RESUME)
    public final void onResume() {
        if (this.b.size() != 0) {
            SparseArray<bz4> sparseArray = this.b;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onResume();
            }
        }
    }
}
