package com.portfolio.platform.buddy_challenge.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ao7;
import com.fossil.bw7;
import com.fossil.cl5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kz4;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.pt4;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.tt4;
import com.fossil.ux7;
import com.fossil.vp7;
import com.fossil.xr4;
import com.fossil.xs4;
import com.fossil.xw7;
import com.fossil.xy4;
import com.fossil.yn7;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCNotificationActionReceiver extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public zt4 f4691a;
    @DexIgnore
    public tt4 b;
    @DexIgnore
    public on5 c;
    @DexIgnore
    public /* final */ iv7 d; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$1", f = "BCNotificationActionReceiver.kt", l = {47}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BCNotificationActionReceiver bCNotificationActionReceiver, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$friend, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                zt4 c = this.this$0.c();
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                p = c.p(true, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((kz4) p).c() != null) {
                xr4.f4164a.b("bc_accept_friend_request", PortfolioApp.h0.c().l0(), this.$friend.d(), PortfolioApp.h0.c());
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$2", f = "BCNotificationActionReceiver.kt", l = {59}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(BCNotificationActionReceiver bCNotificationActionReceiver, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$friend, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                zt4 c = this.this$0.c();
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                p = c.p(false, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((kz4) p).c() != null) {
                xr4.f4164a.b("bc_reject_friend_request", PortfolioApp.h0.c().l0(), this.$friend.d(), PortfolioApp.h0.c());
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$3", f = "BCNotificationActionReceiver.kt", l = {76}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BCNotificationActionReceiver bCNotificationActionReceiver, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bCNotificationActionReceiver;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object v;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Long d2 = this.this$0.d().d();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.this$0.e;
                local.e(str, "ACTION_SET_SYNC_DATA - endTimeOfChallenge: " + d2 + " - exact: " + xy4.f4212a.b());
                if (d2.longValue() > xy4.f4212a.b()) {
                    tt4 b = this.this$0.b();
                    this.L$0 = iv7;
                    this.L$1 = d2;
                    this.label = 1;
                    v = b.v(this);
                    if (v == d) {
                        return d;
                    }
                } else {
                    this.this$0.d().e(ao7.f(0));
                    this.this$0.d().m0(ao7.a(false));
                    return tl7.f3441a;
                }
            } else if (i == 1) {
                Long l = (Long) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                v = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pt4 pt4 = (pt4) ((kz4) v).c();
            if (pt4 != null) {
                PortfolioApp.h0.c().m1(pt4.a());
            } else {
                cl5.c.j();
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public BCNotificationActionReceiver() {
        String simpleName = BCNotificationActionReceiver.class.getSimpleName();
        pq7.b(simpleName, "BCNotificationActionRece\u2026er::class.java.simpleName");
        this.e = simpleName;
        PortfolioApp.h0.c().M().r1(this);
    }

    @DexIgnore
    public final tt4 b() {
        tt4 tt4 = this.b;
        if (tt4 != null) {
            return tt4;
        }
        pq7.n("challengeRepository");
        throw null;
    }

    @DexIgnore
    public final zt4 c() {
        zt4 zt4 = this.f4691a;
        if (zt4 != null) {
            return zt4;
        }
        pq7.n("friendRepository");
        throw null;
    }

    @DexIgnore
    public final on5 d() {
        on5 on5 = this.c;
        if (on5 != null) {
            return on5;
        }
        pq7.n("shared");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        xs4 xs4 = intent != null ? (xs4) intent.getParcelableExtra("friend_extra") : null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != -1294772761) {
                if (hashCode != -1088442688) {
                    if (hashCode == 1236721143 && action.equals("com.buddy_challenge.friend.decline") && xs4 != null) {
                        cl5.c.d(PortfolioApp.h0.c(), xs4.d().hashCode());
                        xw7 unused = gu7.d(this.d, null, null, new b(this, xs4, null), 3, null);
                    }
                } else if (action.equals("com.buddy_challenge.set_sync_data")) {
                    FLogger.INSTANCE.getLocal().e(this.e, "ACTION_SET_SYNC_DATA");
                    cl5.c.e();
                    xw7 unused2 = gu7.d(this.d, null, null, new c(this, null), 3, null);
                }
            } else if (action.equals("com.buddy_challenge.friend.accept") && xs4 != null) {
                cl5.c.d(PortfolioApp.h0.c(), xs4.d().hashCode());
                xw7 unused3 = gu7.d(this.d, null, null, new a(this, xs4, null), 3, null);
            }
        }
    }
}
