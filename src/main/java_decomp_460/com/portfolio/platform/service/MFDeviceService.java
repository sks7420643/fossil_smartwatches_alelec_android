package com.portfolio.platform.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import com.fossil.aj5;
import com.fossil.ao7;
import com.fossil.bj5;
import com.fossil.bw7;
import com.fossil.ck5;
import com.fossil.ct0;
import com.fossil.d37;
import com.fossil.dr5;
import com.fossil.el7;
import com.fossil.eo5;
import com.fossil.eo7;
import com.fossil.fitness.FitnessData;
import com.fossil.fs5;
import com.fossil.g47;
import com.fossil.go5;
import com.fossil.gu7;
import com.fossil.hn5;
import com.fossil.i47;
import com.fossil.iq4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.kq7;
import com.fossil.lo4;
import com.fossil.mn5;
import com.fossil.no4;
import com.fossil.on5;
import com.fossil.or5;
import com.fossil.ph5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.rl5;
import com.fossil.s27;
import com.fossil.sk5;
import com.fossil.tk5;
import com.fossil.tl7;
import com.fossil.vi5;
import com.fossil.vp7;
import com.fossil.vt7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wi5;
import com.fossil.wq5;
import com.fossil.xw7;
import com.fossil.yn7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.receiver.RestartServiceReceiver;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFDeviceService extends Service {
    @DexIgnore
    public static /* final */ String Y;
    @DexIgnore
    public static /* final */ String Z;
    @DexIgnore
    public static /* final */ a a0; // = new a(null);
    @DexIgnore
    public s27 A;
    @DexIgnore
    public d37 B;
    @DexIgnore
    public sk5 C;
    @DexIgnore
    public rl5 D;
    @DexIgnore
    public dr5 E;
    @DexIgnore
    public fs5 F;
    @DexIgnore
    public or5 G;
    @DexIgnore
    public MisfitDeviceProfile H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public /* final */ go5 J; // = new go5();
    @DexIgnore
    public /* final */ Gson K; // = new Gson();
    @DexIgnore
    public /* final */ i L; // = new i(this);
    @DexIgnore
    public boolean M;
    @DexIgnore
    public Handler N;
    @DexIgnore
    public /* final */ Runnable O; // = new m(this);
    @DexIgnore
    public /* final */ d P; // = new d(this);
    @DexIgnore
    public /* final */ e Q; // = new e(this);
    @DexIgnore
    public /* final */ k R; // = new k();
    @DexIgnore
    public /* final */ o S; // = new o();
    @DexIgnore
    public /* final */ j T; // = new j();
    @DexIgnore
    public /* final */ f U; // = new f();
    @DexIgnore
    public /* final */ h V; // = new h();
    @DexIgnore
    public /* final */ n W; // = new n();
    @DexIgnore
    public /* final */ c X; // = new c(this);
    @DexIgnore
    public /* final */ b b; // = new b();
    @DexIgnore
    public on5 c;
    @DexIgnore
    public DeviceRepository d;
    @DexIgnore
    public ActivitiesRepository e;
    @DexIgnore
    public SummariesRepository f;
    @DexIgnore
    public SleepSessionsRepository g;
    @DexIgnore
    public SleepSummariesRepository h;
    @DexIgnore
    public UserRepository i;
    @DexIgnore
    public HybridPresetRepository j;
    @DexIgnore
    public MicroAppRepository k;
    @DexIgnore
    public HeartRateSampleRepository l;
    @DexIgnore
    public HeartRateSummaryRepository m;
    @DexIgnore
    public WorkoutSessionRepository s;
    @DexIgnore
    public FitnessDataRepository t;
    @DexIgnore
    public GoalTrackingRepository u;
    @DexIgnore
    public no4 v;
    @DexIgnore
    public ck5 w;
    @DexIgnore
    public PortfolioApp x;
    @DexIgnore
    public hn5 y;
    @DexIgnore
    public ThirdPartyRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return MFDeviceService.Z;
        }

        @DexIgnore
        public final String b() {
            return MFDeviceService.Y;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Binder {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final MFDeviceService a() {
            return MFDeviceService.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MFDeviceService f4704a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(MFDeviceService mFDeviceService) {
            this.f4704a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            if (intent != null) {
                String stringExtra = intent.getStringExtra("message");
                MFDeviceService mFDeviceService = this.f4704a;
                pq7.b(stringExtra, "message");
                mFDeviceService.f(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MFDeviceService f4705a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {239, 291, 292, 321, 331, 336, 342, 350, 356, 363, 383, 393, 438, 477, 522, 540}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public int I$4;
            @DexIgnore
            public int I$5;
            @DexIgnore
            public int I$6;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public long J$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$a")
            @eo7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$10", f = "MFDeviceService.kt", l = {635}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$a  reason: collision with other inner class name */
            public static final class C0348a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public /* final */ /* synthetic */ String $uiVersion;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0348a(a aVar, String str, String str2, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$uiVersion = str;
                    this.$serial = str2;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0348a aVar = new C0348a(this.this$0, this.$uiVersion, this.$serial, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0348a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object C;
                    PortfolioApp portfolioApp;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        PortfolioApp o = this.this$0.this$0.f4705a.o();
                        String str = this.$uiVersion;
                        this.L$0 = iv7;
                        this.L$1 = o;
                        this.label = 1;
                        C = o.C(str, this);
                        if (C == d) {
                            return d;
                        }
                        portfolioApp = o;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        portfolioApp = (PortfolioApp) this.L$1;
                        C = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    boolean booleanValue = ((Boolean) C).booleanValue();
                    String str2 = this.$serial;
                    pq7.b(str2, "serial");
                    portfolioApp.F0(str2, booleanValue);
                    return tl7.f3441a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b implements iq4.e<d37.c, d37.b> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f4706a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public b(a aVar, String str) {
                    this.f4706a = aVar;
                    this.b = str;
                }

                @DexIgnore
                /* renamed from: b */
                public void a(d37.b bVar) {
                    pq7.c(bVar, "errorValue");
                    PortfolioApp o = this.f4706a.this$0.f4705a.o();
                    String str = this.b;
                    pq7.b(str, "serial");
                    o.e1(str, null);
                }

                @DexIgnore
                /* renamed from: c */
                public void onSuccess(d37.c cVar) {
                    pq7.c(cVar, "responseValue");
                    PortfolioApp o = this.f4706a.this$0.f4705a.o();
                    String str = this.b;
                    pq7.b(str, "serial");
                    o.e1(str, cVar.a());
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$8", f = "MFDeviceService.kt", l = {574}, m = "invokeSuspend")
            public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, String str, String str2, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, this.$serial, this.$secretKey, qn7);
                    cVar.p$ = (iv7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateDeviceSecretKey;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        DeviceRepository q = this.this$0.this$0.f4705a.q();
                        String str = this.$serial;
                        if (str != null) {
                            String str2 = this.$secretKey;
                            if (str2 != null) {
                                this.L$0 = iv7;
                                this.label = 1;
                                updateDeviceSecretKey = q.updateDeviceSecretKey(str, str2, this);
                                if (updateDeviceSecretKey == d) {
                                    return d;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        updateDeviceSecretKey = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (((iq5) updateDeviceSecretKey) instanceof kq5) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String b = MFDeviceService.a0.b();
                        local.d(b, "update secret key " + this.$secretKey + " for " + this.$serial + " to server success");
                        PortfolioApp.h0.c().K0(this.$serial, true);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.a0.b();
                        local2.d(b2, "update secret key " + this.$secretKey + " for " + this.$serial + " to server failed");
                        PortfolioApp.h0.c().K0(this.$serial, false);
                    }
                    return tl7.f3441a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$d")
            @eo7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$9", f = "MFDeviceService.kt", l = {599, 603}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$d  reason: collision with other inner class name */
            public static final class C0349d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0349d(a aVar, String str, String str2, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0349d dVar = new C0349d(this.this$0, this.$serial, this.$secretKey, qn7);
                    dVar.p$ = (iv7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0349d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                        r9 = this;
                        r8 = 2
                        r2 = 1
                        java.lang.Object r3 = com.fossil.yn7.d()
                        int r0 = r9.label
                        if (r0 == 0) goto L_0x0081
                        if (r0 == r2) goto L_0x002f
                        if (r0 != r8) goto L_0x0027
                        java.lang.Object r0 = r9.L$3
                        java.lang.String r0 = (java.lang.String) r0
                        java.lang.Object r0 = r9.L$2
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        java.lang.Object r0 = r9.L$1
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        java.lang.Object r0 = r9.L$0
                        com.fossil.iv7 r0 = (com.fossil.iv7) r0
                        com.fossil.el7.b(r10)
                        r0 = r10
                    L_0x0022:
                        com.fossil.iq4$c r0 = (com.fossil.iq4.c) r0
                    L_0x0024:
                        com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                    L_0x0026:
                        return r0
                    L_0x0027:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x002f:
                        java.lang.Object r0 = r9.L$0
                        com.fossil.iv7 r0 = (com.fossil.iv7) r0
                        com.fossil.el7.b(r10)
                        r2 = r0
                        r1 = r10
                    L_0x0038:
                        r0 = r1
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        if (r0 == 0) goto L_0x0024
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        r1.<init>()
                        java.lang.String r4 = r0.getUserId()
                        r1.append(r4)
                        r4 = 58
                        r1.append(r4)
                        java.lang.String r4 = r9.$serial
                        r1.append(r4)
                        java.lang.String r1 = r1.toString()
                        com.portfolio.platform.service.MFDeviceService$d$a r4 = r9.this$0
                        com.portfolio.platform.service.MFDeviceService$d r4 = r4.this$0
                        com.portfolio.platform.service.MFDeviceService r4 = r4.f4705a
                        com.fossil.s27 r4 = r4.r()
                        com.fossil.bi5 r5 = new com.fossil.bi5
                        r5.<init>()
                        java.lang.String r6 = r9.$secretKey
                        if (r6 == 0) goto L_0x009c
                        com.fossil.s27$a r7 = new com.fossil.s27$a
                        r7.<init>(r1, r5, r6)
                        r9.L$0 = r2
                        r9.L$1 = r0
                        r9.L$2 = r0
                        r9.L$3 = r1
                        r9.label = r8
                        java.lang.Object r0 = com.fossil.jq4.a(r4, r7, r9)
                        if (r0 != r3) goto L_0x0022
                        r0 = r3
                        goto L_0x0026
                    L_0x0081:
                        com.fossil.el7.b(r10)
                        com.fossil.iv7 r0 = r9.p$
                        com.portfolio.platform.service.MFDeviceService$d$a r1 = r9.this$0
                        com.portfolio.platform.service.MFDeviceService$d r1 = r1.this$0
                        com.portfolio.platform.service.MFDeviceService r1 = r1.f4705a
                        com.portfolio.platform.data.source.UserRepository r1 = r1.F()
                        r9.L$0 = r0
                        r9.label = r2
                        java.lang.Object r1 = r1.getCurrentUser(r9)
                        if (r1 != r3) goto L_0x00a1
                        r0 = r3
                        goto L_0x0026
                    L_0x009c:
                        com.fossil.pq7.i()
                        r0 = 0
                        throw r0
                    L_0x00a1:
                        r2 = r0
                        goto L_0x0038
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.d.a.C0349d.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class e extends ko7 implements vp7<iv7, qn7<? super iq5<Void>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ int $batteryLevel$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ Device $device$inlined;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public e(qn7 qn7, a aVar, int i, Device device) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$batteryLevel$inlined = i;
                    this.$device$inlined = device;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    e eVar = new e(qn7, this.this$0, this.$batteryLevel$inlined, this.$device$inlined);
                    eVar.p$ = (iv7) obj;
                    return eVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<Void>> qn7) {
                    return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        DeviceRepository q = this.this$0.this$0.f4705a.q();
                        Device device = this.$device$inlined;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object updateDevice = q.updateDevice(device, true, this);
                        return updateDevice == d ? d : updateDevice;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Intent intent, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$intent, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r7v35, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r5v176, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r5v206 */
            /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r4v318 int), ('.' char), (r5v121 int)] */
            /* JADX WARNING: Removed duplicated region for block: B:116:0x0778  */
            /* JADX WARNING: Removed duplicated region for block: B:14:0x00bb  */
            /* JADX WARNING: Removed duplicated region for block: B:168:0x09fa  */
            /* JADX WARNING: Removed duplicated region for block: B:204:0x0cdf  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x013b  */
            /* JADX WARNING: Removed duplicated region for block: B:236:0x0f98  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0148  */
            /* JADX WARNING: Removed duplicated region for block: B:250:0x1026  */
            /* JADX WARNING: Removed duplicated region for block: B:39:0x038f  */
            /* JADX WARNING: Removed duplicated region for block: B:473:0x18f1 A[SYNTHETIC, Splitter:B:473:0x18f1] */
            /* JADX WARNING: Removed duplicated region for block: B:48:0x04e2  */
            /* JADX WARNING: Removed duplicated region for block: B:509:0x19f4  */
            /* JADX WARNING: Removed duplicated region for block: B:511:0x1a6e  */
            /* JADX WARNING: Removed duplicated region for block: B:513:0x1ac1  */
            /* JADX WARNING: Removed duplicated region for block: B:625:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:627:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:628:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:629:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:638:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x006a  */
            /* JADX WARNING: Unknown variable types count: 2 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r54) {
                /*
                // Method dump skipped, instructions count: 8252
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(MFDeviceService mFDeviceService) {
            this.f4705a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MFDeviceService f4707a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {673}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$e$a$a")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$e$a$a  reason: collision with other inner class name */
            public static final class C0350a implements lo4.b {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f4708a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public C0350a(a aVar, String str) {
                    this.f4708a = aVar;
                    this.b = str;
                }

                @DexIgnore
                @Override // com.fossil.lo4.b
                public void a(Location location, int i) {
                    MFDeviceService mFDeviceService = this.f4708a.this$0.f4707a;
                    String str = this.b;
                    pq7.b(str, "serial");
                    mFDeviceService.L(str, location, i);
                    if (location != null) {
                        lo4.h(this.f4708a.this$0.f4707a.o()).p(this);
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$intent, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x007c  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r12) {
                /*
                // Method dump skipped, instructions count: 505
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(MFDeviceService mFDeviceService) {
            this.f4707a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Device app event receive");
            if (intent != null) {
                PortfolioApp.h0.g(new vi5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getExtras()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1", f = "MFDeviceService.kt", l = {795, 797, 799}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $currentWPVersion;
        @DexIgnore
        public /* final */ /* synthetic */ int $majorVersion;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(MFDeviceService mFDeviceService, String str, int i, float f, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$majorVersion = i;
            this.$currentWPVersion = f;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$majorVersion, this.$currentWPVersion, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x004d
                if (r0 == r5) goto L_0x0027
                if (r0 == r6) goto L_0x0011
                if (r0 != r7) goto L_0x001f
            L_0x0011:
                java.lang.Object r0 = r8.L$1
                com.portfolio.platform.data.model.WatchParameterResponse r0 = (com.portfolio.platform.data.model.WatchParameterResponse) r0
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x001c:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001e:
                return r0
            L_0x001f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0027:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
                r2 = r0
                r1 = r9
            L_0x0030:
                r0 = r1
                com.portfolio.platform.data.model.WatchParameterResponse r0 = (com.portfolio.platform.data.model.WatchParameterResponse) r0
                if (r0 == 0) goto L_0x0068
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.fossil.rl5 r1 = r1.H()
                java.lang.String r4 = r8.$serial
                float r5 = r8.$currentWPVersion
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r6
                java.lang.Object r0 = r1.c(r4, r5, r0, r8)
                if (r0 != r3) goto L_0x001c
                r0 = r3
                goto L_0x001e
            L_0x004d:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.portfolio.platform.data.source.DeviceRepository r1 = r1.q()
                java.lang.String r2 = r8.$serial
                int r4 = r8.$majorVersion
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r1 = r1.getLatestWatchParamFromServer(r2, r4, r8)
                if (r1 != r3) goto L_0x0080
                r0 = r3
                goto L_0x001e
            L_0x0068:
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.fossil.rl5 r1 = r1.H()
                java.lang.String r4 = r8.$serial
                float r5 = r8.$currentWPVersion
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r7
                java.lang.Object r0 = r1.b(r4, r5, r8)
                if (r0 != r3) goto L_0x001c
                r0 = r3
                goto L_0x001e
            L_0x0080:
                r2 = r0
                goto L_0x0030
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            if (intent != null) {
                int intExtra = intent.getIntExtra(Constants.DAILY_STEPS, 0);
                int intExtra2 = intent.getIntExtra(Constants.DAILY_POINTS, 0);
                Calendar instance = Calendar.getInstance();
                pq7.b(instance, "Calendar.getInstance()");
                long longExtra = intent.getLongExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
                FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), ".saveSyncResult - Update heartbeat step by heartbeat receiver");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.a0.b();
                local.i(b, "Heartbeat data received, dailySteps=" + intExtra + ", dailyPoints=" + intExtra2 + ", receivedTime=" + longExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ServiceConnection {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MFDeviceService f4709a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1", f = "MFDeviceService.kt", l = {DateTimeConstants.HOURS_PER_WEEK}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ IBinder $service;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(IBinder iBinder, qn7 qn7) {
                super(2, qn7);
                this.$service = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$service, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp.a aVar = PortfolioApp.h0;
                    IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
                    pq7.b(asInterface, "IButtonConnectivity.Stub.asInterface(service)");
                    this.L$0 = iv7;
                    this.label = 1;
                    if (aVar.m(asInterface, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(MFDeviceService mFDeviceService) {
            this.f4709a = mFDeviceService;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            pq7.c(componentName, "name");
            pq7.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Connection to the BLE has been established");
            this.f4709a.M = true;
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(iBinder, null), 3, null);
            this.f4709a.T();
            this.f4709a.R();
            eo5.c.d(this.f4709a);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            pq7.c(componentName, "name");
            this.f4709a.M = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Micro app cancel event receive");
            if (intent != null) {
                PortfolioApp.h0.g(new wi5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getIntExtra(Constants.VARIANT_ID, -1), intent.getIntExtra("gesture", -1)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Notification event receive");
            if (intent != null) {
                PortfolioApp.h0.g(new aj5(intent.getIntExtra(ButtonService.NOTIFICATION_ID, -1), intent.getBooleanExtra(ButtonService.NOTIFICATION_RESULT, false)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceLocation $deviceLocation;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(DeviceLocation deviceLocation, qn7 qn7) {
            super(2, qn7);
            this.$deviceLocation = deviceLocation;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.$deviceLocation, qn7);
            lVar.p$ = (iv7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                mn5.p.a().i().saveDeviceLocation(this.$deviceLocation);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService b;

        @DexIgnore
        public m(MFDeviceService mFDeviceService) {
            this.b = mFDeviceService;
        }

        @DexIgnore
        public final void run() {
            if (vt7.j("release", "debug", true) && pq7.a(this.b.o().Q(), ph5.PORTFOLIO.getName())) {
                this.b.o().T0();
                this.b.P();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            intent.setAction("SCAN_DEVICE_FOUND");
            ct0.b(context).d(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Streaming event receive");
            if (intent != null) {
                int intExtra = intent.getIntExtra("gesture", -1);
                PortfolioApp.h0.g(new bj5(intent.getStringExtra(Constants.SERIAL_NUMBER), intExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$1", f = "MFDeviceService.kt", l = {991, 1000}, m = "invokeSuspend")
    public static final class p extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $deviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(MFDeviceService mFDeviceService, String str, boolean z, MisfitDeviceProfile misfitDeviceProfile, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$isNeedUpdateRemote = z;
            this.$deviceProfile = misfitDeviceProfile;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            p pVar = new p(this.this$0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, qn7);
            pVar.p$ = (iv7) obj;
            return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((p) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x00bd  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 578
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.p.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1", f = "MFDeviceService.kt", l = {906}, m = "invokeSuspend")
    public static final class q extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(MFDeviceService mFDeviceService, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mFDeviceService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            q qVar = new q(this.this$0, qn7);
            qVar.p$ = (iv7) obj;
            return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((q) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            List<Device> list;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                List<Device> allDevice = this.this$0.q().getAllDevice();
                if (!allDevice.isEmpty()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.a0.b();
                    local.d(b, "Get all device success devicesList=" + allDevice);
                    UserRepository F = this.this$0.F();
                    this.L$0 = iv7;
                    this.L$1 = allDevice;
                    this.label = 1;
                    currentUser = F.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    list = allDevice;
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                list = (List) this.L$1;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) currentUser) != null) {
                for (Device device : list) {
                    this.this$0.o().F1(device.component1(), device.component2());
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $this_run;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(MisfitDeviceProfile misfitDeviceProfile, qn7 qn7, MFDeviceService mFDeviceService, String str) {
            super(2, qn7);
            this.$this_run = misfitDeviceProfile;
            this.this$0 = mFDeviceService;
            this.$serial$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            r rVar = new r(this.$this_run, qn7, this.this$0, this.$serial$inlined);
            rVar.p$ = (iv7) obj;
            return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((r) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Integer vibrationStrength;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Device deviceBySerial = this.this$0.q().getDeviceBySerial(this.$serial$inlined);
                int c = tk5.c(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.a0.b();
                local.d(b, "newVibrationLvl: " + c + " - device: " + deviceBySerial);
                if (deviceBySerial != null && ((vibrationStrength = deviceBySerial.getVibrationStrength()) == null || vibrationStrength.intValue() != c)) {
                    deviceBySerial.setVibrationStrength(ao7.e(c));
                    DeviceRepository q = this.this$0.q();
                    this.L$0 = iv7;
                    this.L$1 = deviceBySerial;
                    this.I$0 = c;
                    this.label = 1;
                    if (q.updateDevice(deviceBySerial, false, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Device device = (Device) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = MFDeviceService.class.getSimpleName();
        pq7.b(simpleName, "MFDeviceService::class.java.simpleName");
        Y = simpleName;
        StringBuilder sb = new StringBuilder();
        Package r1 = MFDeviceService.class.getPackage();
        if (r1 != null) {
            pq7.b(r1, "MFDeviceService::class.java.`package`!!");
            sb.append(r1.getName());
            sb.append(".location_updated");
            Z = sb.toString();
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static /* synthetic */ void j(MFDeviceService mFDeviceService, String str, int i2, int i3, ArrayList arrayList, Integer num, int i4, Object obj) {
        mFDeviceService.i(str, i2, i3, (i4 & 8) != 0 ? new ArrayList(i3) : arrayList, (i4 & 16) != 0 ? 10 : num);
    }

    @DexIgnore
    public final on5 A() {
        on5 on5 = this.c;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSessionsRepository B() {
        SleepSessionsRepository sleepSessionsRepository = this.g;
        if (sleepSessionsRepository != null) {
            return sleepSessionsRepository;
        }
        pq7.n("mSleepSessionsRepository");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository C() {
        SleepSummariesRepository sleepSummariesRepository = this.h;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        pq7.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository D() {
        SummariesRepository summariesRepository = this.f;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        pq7.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final ThirdPartyRepository E() {
        ThirdPartyRepository thirdPartyRepository = this.z;
        if (thirdPartyRepository != null) {
            return thirdPartyRepository;
        }
        pq7.n("mThirdPartyRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository F() {
        UserRepository userRepository = this.i;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final d37 G() {
        d37 d37 = this.B;
        if (d37 != null) {
            return d37;
        }
        pq7.n("mVerifySecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final rl5 H() {
        rl5 rl5 = this.D;
        if (rl5 != null) {
            return rl5;
        }
        pq7.n("mWatchParamHelper");
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionRepository I() {
        WorkoutSessionRepository workoutSessionRepository = this.s;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        pq7.n("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final fs5 J() {
        fs5 fs5 = this.F;
        if (fs5 != null) {
            return fs5;
        }
        pq7.n("mWorkoutTetherGpsManager");
        throw null;
    }

    @DexIgnore
    public final void K(SKUModel sKUModel, int i2, int i3) {
        if (sKUModel != null) {
            String str = "";
            if (i3 == 1) {
                ck5 ck5 = this.w;
                if (ck5 != null) {
                    String sku = sKUModel.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    String deviceName = sKUModel.getDeviceName();
                    if (deviceName != null) {
                        str = deviceName;
                    }
                    ck5.p(sku, str, i2);
                } else {
                    pq7.n("mAnalyticsHelper");
                    throw null;
                }
            } else if (i3 == 2) {
                ck5 ck52 = this.w;
                if (ck52 != null) {
                    String sku2 = sKUModel.getSku();
                    if (sku2 == null) {
                        sku2 = "";
                    }
                    String deviceName2 = sKUModel.getDeviceName();
                    if (deviceName2 == null) {
                        deviceName2 = str;
                    }
                    ck52.n(sku2, deviceName2, i2);
                } else {
                    pq7.n("mAnalyticsHelper");
                    throw null;
                }
            }
            on5 on5 = this.c;
            if (on5 != null) {
                on5.t1(Boolean.FALSE);
            } else {
                pq7.n("mSharedPreferencesManager");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void L(String str, Location location, int i2) {
        pq7.c(str, "serial");
        if (i2 >= 0) {
            if (location != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = Y;
                local.d(str2, "Inside " + Y + ".onLocationUpdated - location=[lat:" + location.getLatitude() + ", lon:" + location.getLongitude() + ", accuracy:" + location.getAccuracy() + "]");
                if (location.getAccuracy() <= 500.0f) {
                    try {
                        DeviceLocation deviceLocation = new DeviceLocation(str, location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                        h(str, deviceLocation);
                        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new l(deviceLocation, null), 3, null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = Y;
                        local2.e(str3, "Error inside " + Y + ".onLocationUpdated - e=" + e2);
                    }
                }
            }
        } else if (i2 != -1) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = Y;
            local3.e(str4, "Error inside " + Y + ".onLocationUpdated - code=" + i2);
        }
    }

    @DexIgnore
    public final void M() {
        if (this.I) {
            FLogger.INSTANCE.getLocal().e(Y, "Return from device service register receiver");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.i(str, "Inside " + Y + ".registerReceiver " + l(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE()));
        registerReceiver(this.P, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE())));
        registerReceiver(this.X, new IntentFilter(l(ButtonService.Companion.getACTION_ANALYTIC_EVENT())));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local2.i(str2, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT());
        registerReceiver(this.S, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT())));
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = Y;
        local3.i(str3, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT());
        registerReceiver(this.T, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT())));
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = Y;
        local4.i(str4, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT());
        registerReceiver(this.U, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT())));
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = Y;
        local5.i(str5, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA());
        registerReceiver(this.V, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA())));
        registerReceiver(this.Q, new IntentFilter(l(ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE())));
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = Y;
        local6.i(str6, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_NOTIFICATION_SENT());
        registerReceiver(this.R, new IntentFilter(l(ButtonService.Companion.getACTION_NOTIFICATION_SENT())));
        registerReceiver(this.W, new IntentFilter(l(ButtonService.Companion.getACTION_SCAN_DEVICE_FOUND())));
        registerReceiver(this.J, new IntentFilter("android.intent.action.TIME_TICK"));
        this.I = true;
    }

    @DexIgnore
    public final List<FitnessDataWrapper> N(List<? extends FitnessData> list, String str, DateTime dateTime) {
        pq7.c(list, "syncData");
        pq7.c(str, "serial");
        pq7.c(dateTime, "syncTime");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = Y;
            local.d(str2, "saveFitnessData=" + ((Object) t2));
            arrayList.add(new FitnessDataWrapper(t2, str, dateTime));
        }
        return arrayList;
    }

    @DexIgnore
    public final void O(MisfitDeviceProfile misfitDeviceProfile) {
        this.H = misfitDeviceProfile;
    }

    @DexIgnore
    public final void P() {
        Q();
        FLogger.INSTANCE.getLocal().d(Y, "Start reading realtime step timeoutHandler 30 mins");
        Handler handler = new Handler(getMainLooper());
        this.N = handler;
        if (handler != null) {
            handler.postDelayed(this.O, 1800000);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void Q() {
        if (this.N != null) {
            FLogger.INSTANCE.getLocal().d(Y, "Stop reading realtime step timeoutHandler");
            Handler handler = this.N;
            if (handler != null) {
                handler.removeCallbacks(this.O);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R() {
        boolean b2 = FossilNotificationListenerService.w.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.d(str, "triggerReconnectNotificationService() - isConnectedNotificationManager = " + b2);
        if (!b2 && g47.f1261a.h()) {
            FossilNotificationListenerService.w.d();
        }
    }

    @DexIgnore
    public final xw7 S(MisfitDeviceProfile misfitDeviceProfile, String str, boolean z2) {
        pq7.c(str, "serial");
        return gu7.d(jv7.a(bw7.a()), null, null, new p(this, str, z2, misfitDeviceProfile, null), 3, null);
    }

    @DexIgnore
    public final xw7 T() {
        return gu7.d(jv7.a(bw7.b()), null, null, new q(this, null), 3, null);
    }

    @DexIgnore
    public final void U(String str) {
        double d2 = 0.0d;
        pq7.c(str, "serial");
        LocationProvider i2 = mn5.p.a().i();
        DeviceLocation deviceLocation = i2.getDeviceLocation(str);
        double latitude = deviceLocation != null ? deviceLocation.getLatitude() : 0.0d;
        if (deviceLocation != null) {
            d2 = deviceLocation.getLongitude();
        }
        i2.saveDeviceLocation(new DeviceLocation(str, latitude, d2, System.currentTimeMillis()));
    }

    @DexIgnore
    public final void V(MisfitDeviceProfile misfitDeviceProfile, String str) {
        pq7.c(str, "serial");
        if (misfitDeviceProfile != null) {
            boolean isDefaultValue = misfitDeviceProfile.getVibrationStrength().isDefaultValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = Y;
            local.d(str2, "updateVibrationStrengthLevel - isDefaultValue: " + isDefaultValue);
            if (!isDefaultValue) {
                xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new r(misfitDeviceProfile, null, this, str), 3, null);
            }
        }
    }

    @DexIgnore
    public final void f(String str) {
        pq7.c(str, Constants.EVENT);
        ck5.f.g().i(str);
    }

    @DexIgnore
    public final void g(String str, Map<String, String> map) {
        pq7.c(str, Constants.EVENT);
        ck5.f.g().l(str, map);
    }

    @DexIgnore
    public final void h(String str, DeviceLocation deviceLocation) {
        Intent intent = new Intent();
        intent.putExtra("SERIAL", str);
        intent.putExtra("device_location", deviceLocation);
        intent.setAction(Z);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void i(String str, int i2, int i3, ArrayList<Integer> arrayList, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local.i(str2, "Broadcast sync status=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra("LAST_ERROR_CODE", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), num);
        intent.putIntegerArrayListExtra("LIST_ERROR_CODE", arrayList);
        intent.putExtra("SERIAL", str);
        wq5 wq5 = wq5.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        wq5.h(communicateMode, new wq5.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void k(String str, Bundle bundle) {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new g(this, str, bundle.getInt(ButtonService.WATCH_PARAMS_MAJOR), bundle.getFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION), null), 3, null);
    }

    @DexIgnore
    public final String l(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local.d(str2, "Key=" + getPackageName() + str);
        return getPackageName() + str;
    }

    @DexIgnore
    public final MisfitDeviceProfile m() {
        return this.H;
    }

    @DexIgnore
    public final ActivitiesRepository n() {
        ActivitiesRepository activitiesRepository = this.e;
        if (activitiesRepository != null) {
            return activitiesRepository;
        }
        pq7.n("mActivitiesRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp o() {
        PortfolioApp portfolioApp = this.x;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        pq7.n("mApp");
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        FLogger.INSTANCE.getLocal().i(Y, "on misfit service bind");
        return this.b;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.h0.c().M().Z(this);
        M();
        FLogger.INSTANCE.getLocal().d(Y, "Service TRacking - MFDeviceService onCreate");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = Y;
        remote.i(component, session, "", str, "[MFDeviceService] onCreate " + System.currentTimeMillis());
    }

    @DexIgnore
    public void onDestroy() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.d(str, "Inside " + Y + ".onDestroy");
        super.onDestroy();
        if (this.M) {
            i47.f1583a.g(this, this.L);
        }
        unregisterReceiver(this.P);
        unregisterReceiver(this.X);
        unregisterReceiver(this.S);
        unregisterReceiver(this.Q);
        unregisterReceiver(this.W);
        unregisterReceiver(this.U);
        unregisterReceiver(this.T);
        unregisterReceiver(this.V);
        unregisterReceiver(this.J);
        unregisterReceiver(this.R);
        this.I = false;
        dr5 dr5 = this.E;
        if (dr5 != null) {
            dr5.f();
        } else {
            pq7.n("mBuddyChallengeManager");
            throw null;
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        if (vt7.k(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, null)) {
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Start to bring service to foreground");
            eo5.c.c(this, this, false);
        } else {
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Stop service");
            eo5.c.c(this, this, true);
        }
        if (!this.M) {
            i47.f1583a.b(this, ButtonService.class, this.L, 1);
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Bound to service");
        }
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = Y;
        remote.i(component, session, "", str, "[MFDeviceService] onStartCommand " + System.currentTimeMillis());
        return 1;
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(Y, "onTaskRemoved()");
        stopSelf();
        sendBroadcast(new Intent(this, RestartServiceReceiver.class));
    }

    @DexIgnore
    public final dr5 p() {
        dr5 dr5 = this.E;
        if (dr5 != null) {
            return dr5;
        }
        pq7.n("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository q() {
        DeviceRepository deviceRepository = this.d;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final s27 r() {
        s27 s27 = this.A;
        if (s27 != null) {
            return s27;
        }
        pq7.n("mEncryptUseCase");
        throw null;
    }

    @DexIgnore
    public final FitnessDataRepository s() {
        FitnessDataRepository fitnessDataRepository = this.t;
        if (fitnessDataRepository != null) {
            return fitnessDataRepository;
        }
        pq7.n("mFitnessDataRepository");
        throw null;
    }

    @DexIgnore
    public final sk5 t() {
        sk5 sk5 = this.C;
        if (sk5 != null) {
            return sk5;
        }
        pq7.n("mFitnessHelper");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.u;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        pq7.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSampleRepository v() {
        HeartRateSampleRepository heartRateSampleRepository = this.l;
        if (heartRateSampleRepository != null) {
            return heartRateSampleRepository;
        }
        pq7.n("mHeartRateSampleRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryRepository w() {
        HeartRateSummaryRepository heartRateSummaryRepository = this.m;
        if (heartRateSummaryRepository != null) {
            return heartRateSummaryRepository;
        }
        pq7.n("mHeartRateSummaryRepository");
        throw null;
    }

    @DexIgnore
    public final MicroAppRepository x() {
        MicroAppRepository microAppRepository = this.k;
        if (microAppRepository != null) {
            return microAppRepository;
        }
        pq7.n("mMicroAppRepository");
        throw null;
    }

    @DexIgnore
    public final or5 y() {
        or5 or5 = this.G;
        if (or5 != null) {
            return or5;
        }
        pq7.n("mMusicControlComponent");
        throw null;
    }

    @DexIgnore
    public final HybridPresetRepository z() {
        HybridPresetRepository hybridPresetRepository = this.j;
        if (hybridPresetRepository != null) {
            return hybridPresetRepository;
        }
        pq7.n("mPresetRepository");
        throw null;
    }
}
