package com.portfolio.platform.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.AppLinkData;
import com.fossil.aj5;
import com.fossil.bw7;
import com.fossil.co7;
import com.fossil.do1;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.g47;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.k68;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.nk5;
import com.fossil.no4;
import com.fossil.np1;
import com.fossil.on5;
import com.fossil.or5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qp1;
import com.fossil.tc7;
import com.fossil.tl7;
import com.fossil.ux7;
import com.fossil.vi5;
import com.fossil.vp7;
import com.fossil.vr5;
import com.fossil.vt7;
import com.fossil.wt7;
import com.fossil.xi5;
import com.fossil.xw7;
import com.fossil.yn7;
import com.fossil.yr5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"OverrideAbstract"})
public final class FossilNotificationListenerService extends NotificationListenerService {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static boolean u;
    @DexIgnore
    public static /* final */ String[] v; // = {"android.title", "android.title.big", "android.text", "android.subText", "android.infoText", "android.summaryText", "android.bigText"};
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public /* final */ StringBuilder b; // = new StringBuilder();
    @DexIgnore
    public /* final */ SparseArray<b> c; // = new SparseArray<>();
    @DexIgnore
    public /* final */ iv7 d; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public /* final */ HashSet<String> e; // = new HashSet<>();
    @DexIgnore
    public /* final */ Handler f; // = new Handler();
    @DexIgnore
    public /* final */ Runnable g; // = new d(this);
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<StatusBarNotification> h; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public or5 i;
    @DexIgnore
    public vr5 j;
    @DexIgnore
    public yr5 k;
    @DexIgnore
    public no4 l;
    @DexIgnore
    public on5 m;
    @DexIgnore
    public UserRepository s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return FossilNotificationListenerService.t;
        }

        @DexIgnore
        public final boolean b() {
            return FossilNotificationListenerService.u;
        }

        @DexIgnore
        public final void c(ComponentName componentName) {
            FLogger.INSTANCE.getLocal().d(a(), "toggleNotificationListenerService()");
            PackageManager packageManager = PortfolioApp.h0.c().getPackageManager();
            packageManager.setComponentEnabledSetting(componentName, 2, 1);
            packageManager.setComponentEnabledSetting(componentName, 1, 1);
        }

        @DexIgnore
        public final void d() {
            FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService()");
            try {
                ComponentName componentName = new ComponentName(PortfolioApp.h0.c(), FossilNotificationListenerService.class);
                c(componentName);
                if (Build.VERSION.SDK_INT >= 24) {
                    FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService() - requestRebind");
                    NotificationListenerService.requestRebind(componentName);
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a();
                local.d(a2, "tryReconnectNotificationService() - ex = " + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4703a; // = -1;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ long e;

        @DexIgnore
        public b(String str, String str2, int i, long j) {
            pq7.c(str, "packageName");
            pq7.c(str2, "text");
            this.b = str;
            this.c = str2;
            this.d = i;
            this.e = j;
        }

        @DexIgnore
        public final long a() {
            return this.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                return pq7.a(this.b, bVar.b) && pq7.a(this.c, bVar.c) && this.d == bVar.d;
            }
        }

        @DexIgnore
        public int hashCode() {
            if (this.f4703a == -1) {
                this.f4703a = (this.b + this.d).hashCode();
            }
            return this.f4703a;
        }

        @DexIgnore
        public String toString() {
            return this.d + " | " + this.b + " | " + this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.FossilNotificationListenerService$filterQueue$1", f = "FossilNotificationListenerService.kt", l = {270}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FossilNotificationListenerService fossilNotificationListenerService, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0041  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 337
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService b;

        @DexIgnore
        public d(FossilNotificationListenerService fossilNotificationListenerService) {
            this.b = fossilNotificationListenerService;
        }

        @DexIgnore
        public final void run() {
            this.b.n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1", f = "FossilNotificationListenerService.kt", l = {94, 97}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(FossilNotificationListenerService fossilNotificationListenerService, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0056
                if (r0 == r2) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0027:
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                boolean r2 = r2.y0()
                if (r2 == 0) goto L_0x0015
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r3 = com.portfolio.platform.service.FossilNotificationListenerService.w
                java.lang.String r3 = r3.a()
                java.lang.String r4 = "onCreate() - enable Music Control Component."
                r2.d(r3, r4)
                com.portfolio.platform.service.FossilNotificationListenerService r2 = r6.this$0
                com.fossil.or5 r2 = r2.p()
                r6.L$0 = r0
                r6.label = r5
                java.lang.Object r0 = r2.m(r6)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0056:
                com.fossil.el7.b(r7)
                com.fossil.iv7 r0 = r6.p$
                r6.L$0 = r0
                r6.label = r2
                r2 = 5000(0x1388, double:2.4703E-320)
                java.lang.Object r2 = com.fossil.uv7.a(r2, r6)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.FossilNotificationListenerService$onNotificationPosted$1", f = "FossilNotificationListenerService.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FossilNotificationListenerService fossilNotificationListenerService, StatusBarNotification statusBarNotification, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fossilNotificationListenerService;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$sbn, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                boolean z = false;
                if (this.this$0.q().q0() && !this.this$0.s()) {
                    z = this.this$0.o().k(this.$sbn);
                }
                if (!z) {
                    this.this$0.j(this.$sbn);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.FossilNotificationListenerService", f = "FossilNotificationListenerService.kt", l = {361}, m = "processNotification")
    public static final class g extends co7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FossilNotificationListenerService fossilNotificationListenerService, qn7 qn7) {
            super(qn7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.y(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.FossilNotificationListenerService", f = "FossilNotificationListenerService.kt", l = {425}, m = "pushLogToFireBase")
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(FossilNotificationListenerService fossilNotificationListenerService, qn7 qn7) {
            super(qn7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.z(null, this);
        }
    }

    /*
    static {
        String simpleName = FossilNotificationListenerService.class.getSimpleName();
        pq7.b(simpleName, "FossilNotificationListen\u2026ce::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public final void j(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(t, "addNotificationToQueue()");
        if (statusBarNotification.getNotification().priority < -1) {
            FLogger.INSTANCE.getLocal().d(t, "addNotificationToQueue() - Ignoring Min priority notification");
            if (!PortfolioApp.h0.c().z0()) {
                w(statusBarNotification);
                return;
            }
            return;
        }
        this.h.add(statusBarNotification);
        this.f.removeCallbacks(this.g);
        this.f.postDelayed(this.g, 500);
    }

    @DexIgnore
    public final void k(String str) {
        if (!this.e.contains(str)) {
            Iterator<String> it = this.e.iterator();
            while (it.hasNext()) {
                String next = it.next();
                pq7.b(next, "oldString");
                if (wt7.v(next, str, false, 2, null)) {
                    return;
                }
            }
            this.b.append(' ');
            this.b.append(str);
            this.e.add(str);
        }
    }

    @DexIgnore
    public final void l() {
        long currentTimeMillis = System.currentTimeMillis();
        int size = this.c.size();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "cleanPastNotificationMap() - Size = " + size);
        LinkedList linkedList = new LinkedList();
        for (int i2 = 0; i2 < size; i2++) {
            int keyAt = this.c.keyAt(i2);
            if (this.c.get(keyAt) != null && currentTimeMillis - this.c.get(keyAt).a() > ButtonService.CONNECT_TIMEOUT) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = t;
                local2.d(str2, "cleanPastNotificationMap() - Adding key to remove - key = " + keyAt);
                linkedList.add(Integer.valueOf(keyAt));
            }
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            FLogger.INSTANCE.getLocal().d(t, "cleanPastNotificationMap() - Dumping old notification");
            SparseArray<b> sparseArray = this.c;
            pq7.b(num, "keyInt");
            sparseArray.remove(num.intValue());
        }
    }

    @DexIgnore
    public final Bundle m(Notification notification) {
        FLogger.INSTANCE.getLocal().d(t, "collectTextInfo()");
        Bundle bundle = notification.extras;
        this.b.setLength(0);
        this.e.clear();
        String[] strArr = v;
        for (String str : strArr) {
            Object obj = bundle.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = t;
            StringBuilder sb = new StringBuilder();
            sb.append("  Looking at ");
            sb.append(str);
            sb.append(" : ");
            sb.append(obj);
            sb.append(" : class ");
            sb.append(obj != null ? obj.getClass().getSimpleName() : "null");
            local.d(str2, sb.toString());
            if ((obj instanceof CharSequence) && !TextUtils.isEmpty((CharSequence) obj)) {
                FLogger.INSTANCE.getLocal().d(t, "  Adding " + str + " with value of " + obj);
                k(obj.toString());
            }
        }
        FLogger.INSTANCE.getLocal().d(t, "  Ticker Text = " + notification.tickerText);
        if (!TextUtils.isEmpty(notification.tickerText)) {
            k(notification.tickerText.toString());
        }
        pq7.b(bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        return bundle;
    }

    @DexIgnore
    public final xw7 n() {
        return gu7.d(this.d, null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final yr5 o() {
        yr5 yr5 = this.k;
        if (yr5 != null) {
            return yr5;
        }
        pq7.n("mHybridMessageNotificationComponent");
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.h0.c().M().A1(this);
        PortfolioApp.h0.h(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onCreate() - Notification Listener Service permission " + PortfolioApp.h0.c().y0());
        xw7 unused = gu7.d(this.d, null, null, new e(this, null), 3, null);
        FLogger.INSTANCE.getLocal().d(t, "onCreate() - Notification Listener Service Created");
    }

    @DexIgnore
    public void onDestroy() {
        vr5 vr5 = this.j;
        if (vr5 != null) {
            vr5.P();
            PortfolioApp.h0.l(this);
            super.onDestroy();
            or5 or5 = this.i;
            if (or5 != null) {
                or5.t();
                FLogger.INSTANCE.getLocal().d(t, "onDestroy() - Notification Listener Service Destroyed");
                return;
            }
            pq7.n("mMusicControlComponent");
            throw null;
        }
        pq7.n("mDianaNotificationComponent");
        throw null;
    }

    @DexIgnore
    @tc7
    public final void onDeviceAppEvent(vi5 vi5) {
        qp1 qp1;
        pq7.c(vi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("onDeviceAppEvent", "onDeviceAppEvent = " + vi5);
        if (vi5.a() == np1.APP_NOTIFICATION_CONTROL.ordinal() && (qp1 = (qp1) vi5.b().getParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA)) != null && qp1.getAction().getActionType() == do1.DISMISS_NOTIFICATION) {
            vr5 vr5 = this.j;
            if (vr5 != null) {
                vr5.M(qp1.getNotificationUid());
            } else {
                pq7.n("mDianaNotificationComponent");
                throw null;
            }
        }
    }

    @DexIgnore
    @tc7
    public final void onDeviceConnectionStateChange(xi5 xi5) {
        pq7.c(xi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onDeviceConnectionStateChange(), serial=" + xi5.a() + ", state=" + xi5.b());
        if (xi5.b() == ConnectionStateChange.GATT_ON.ordinal() && FossilDeviceSerialPatternUtil.isDianaDevice(xi5.a()) && PortfolioApp.h0.c().y0()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local2.d(str2, "deviceChange, event = " + xi5);
            vr5 vr5 = this.j;
            if (vr5 != null) {
                vr5.K();
            } else {
                pq7.n("mDianaNotificationComponent");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onListenerConnected() {
        super.onListenerConnected();
        FLogger.INSTANCE.getLocal().d(t, "onListenerConnected()");
        u = true;
        vr5 vr5 = this.j;
        if (vr5 != null) {
            vr5.O();
        } else {
            pq7.n("mDianaNotificationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        FLogger.INSTANCE.getLocal().d(t, "onListenerDisconnected()");
        u = false;
    }

    @DexIgnore
    @tc7
    public final void onNotificationEvent(aj5 aj5) {
        pq7.c(aj5, Constants.EVENT);
        FLogger.INSTANCE.getLocal().d(t, "onNotificationEvent");
        vr5 vr5 = this.j;
        if (vr5 != null) {
            vr5.N(aj5.a(), aj5.b());
        } else {
            pq7.n("mDianaNotificationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        pq7.c(statusBarNotification, "sbn");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onNotificationPosted() - Receive notification with permission enable is " + g47.f1261a.h());
        String J = PortfolioApp.h0.c().J();
        if (!nk5.o.u(J)) {
            FLogger.INSTANCE.getLocal().d(t, "onNotificationPosted() - Device is not supported");
        } else if (FossilDeviceSerialPatternUtil.isDianaDevice(J)) {
            x(statusBarNotification);
            w(statusBarNotification);
            vr5 vr5 = this.j;
            if (vr5 != null) {
                vr5.R(statusBarNotification);
            } else {
                pq7.n("mDianaNotificationComponent");
                throw null;
            }
        } else {
            xw7 unused = gu7.d(this.d, null, null, new f(this, statusBarNotification, null), 3, null);
        }
    }

    @DexIgnore
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(t, "onNotificationRemoved()");
        if (statusBarNotification != null) {
            try {
                super.onNotificationRemoved(statusBarNotification);
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.h0.c().J())) {
                    x(statusBarNotification);
                    w(statusBarNotification);
                    vr5 vr5 = this.j;
                    if (vr5 != null) {
                        vr5.L(statusBarNotification);
                    } else {
                        pq7.n("mDianaNotificationComponent");
                        throw null;
                    }
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = t;
                local.d(str, "onNotificationRemoved(): package = " + statusBarNotification.getPackageName() + " - notification = " + statusBarNotification.getNotification() + ", exception = " + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        pq7.c(intent, "intent");
        FLogger.INSTANCE.getLocal().d(t, "onStartCommand() - Notification Listener Service Started");
        return super.onStartCommand(intent, i2, i3);
    }

    @DexIgnore
    public final or5 p() {
        or5 or5 = this.i;
        if (or5 != null) {
            return or5;
        }
        pq7.n("mMusicControlComponent");
        throw null;
    }

    @DexIgnore
    public final on5 q() {
        on5 on5 = this.m;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final boolean r(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        boolean z;
        if (!v(statusBarNotification) || Build.VERSION.SDK_INT < 21) {
            z = true;
        } else {
            int size = list.size();
            int i2 = 0;
            boolean z2 = true;
            while (i2 < size) {
                String groupKey = statusBarNotification.getGroupKey();
                String groupKey2 = ((StatusBarNotification) list.get(i2)).getGroupKey();
                Notification notification = statusBarNotification.getNotification();
                pq7.b(notification, "notification.notification");
                String group = notification.getGroup();
                Notification notification2 = ((StatusBarNotification) list.get(i2)).getNotification();
                pq7.b(notification2, "filterList[j].notification");
                String group2 = notification2.getGroup();
                boolean z3 = (groupKey == null || groupKey2 == null || !vt7.j(groupKey, groupKey2, true)) ? z2 : false;
                if (!(group == null || group2 == null || !vt7.j(group, group2, true))) {
                    z3 = false;
                }
                i2++;
                z2 = z3;
            }
            z = z2;
        }
        FLogger.INSTANCE.getLocal().d(t, "handleSummaryNotification() - isAlone = " + z);
        return z;
    }

    @DexIgnore
    public final boolean s() {
        on5 on5 = this.m;
        if (on5 != null) {
            boolean Z = on5.Z();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "isBlockedByDND() = " + Z);
            return Z;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final boolean t(b bVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "isBlockedByIntervalTime() - thisNotification.hashCode() = " + bVar.hashCode());
        b bVar2 = this.c.get(bVar.hashCode());
        if (bVar2 != null) {
            FLogger.INSTANCE.getLocal().d(t, "isBlockedByIntervalTime() - Have an old notification with the same hash");
            if (bVar.a() - bVar2.a() > ButtonService.CONNECT_TIMEOUT) {
                FLogger.INSTANCE.getLocal().d(t, "isBlockedByIntervalTime() - Older than 10000 milliseconds. Allow update to this notification");
            } else {
                long abs = Math.abs(ButtonService.CONNECT_TIMEOUT - (bVar.a() - bVar2.a()));
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = t;
                local2.d(str2, "isBlockedByIntervalTime() - Block duplicate in " + k68.d(abs) + ", notification: " + bVar);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean u(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (pq7.a(statusBarNotification.getPackageName(), ((StatusBarNotification) list.get(i2)).getPackageName())) {
                FLogger.INSTANCE.getLocal().d(t, "isDuplicated() - Notification is duplicated");
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean v(StatusBarNotification statusBarNotification) {
        return (statusBarNotification.getNotification().flags & 512) == 512;
    }

    @DexIgnore
    public final void w(StatusBarNotification statusBarNotification) {
        String packageName = statusBarNotification.getPackageName();
        Notification notification = statusBarNotification.getNotification();
        Bundle bundle = notification.extras;
        String obj = !TextUtils.isEmpty(notification.tickerText) ? notification.tickerText.toString() : "";
        CharSequence charSequence = bundle.getCharSequence("android.title");
        CharSequence charSequence2 = bundle.getCharSequence("android.text");
        CharSequence charSequence3 = bundle.getCharSequence("android.subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "printDebugInfo() - Notification info:\n  package name: " + packageName + "\n  ticker: " + obj + "\n  title: " + charSequence + "\n  subtext: " + charSequence3 + "\n  text: " + charSequence2);
    }

    @DexIgnore
    public final void x(StatusBarNotification statusBarNotification) {
        Notification notification = statusBarNotification.getNotification();
        FLogger.INSTANCE.getLocal().d(t, "............");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "  Notification Posted: " + statusBarNotification.getPackageName());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local2.d(str2, "  Id: " + statusBarNotification.getId());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = t;
        local3.d(str3, "  Post Time: " + statusBarNotification.getPostTime());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = t;
        local4.d(str4, "  Tag: " + statusBarNotification.getTag());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = t;
        local5.d(str5, "  Content: " + notification.extras);
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = t;
        local6.d(str6, "  Notification Priority: " + notification.priority);
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = t;
        local7.d(str7, "  Notification Flags: " + Integer.toBinaryString(notification.flags));
        if (Build.VERSION.SDK_INT >= 21) {
            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
            String str8 = t;
            local8.d(str8, "  Group Key: " + statusBarNotification.getGroupKey());
            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
            String str9 = t;
            local9.d(str9, "  Key: " + statusBarNotification.getKey());
            ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
            String str10 = t;
            StringBuilder sb = new StringBuilder();
            sb.append("  Notification Group: ");
            pq7.b(notification, "notification");
            sb.append(notification.getGroup());
            local10.d(str10, sb.toString());
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            String str11 = t;
            local11.d(str11, "  Notification Sort Key: " + notification.getSortKey());
        }
        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
        String str12 = t;
        local12.d(str12, "  isSummary = " + v(statusBarNotification));
        FLogger.INSTANCE.getLocal().d(t, "............");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object y(android.service.notification.StatusBarNotification r14, com.fossil.qn7<? super java.lang.Boolean> r15) {
        /*
        // Method dump skipped, instructions count: 475
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.y(android.service.notification.StatusBarNotification, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object z(android.service.notification.StatusBarNotification r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.portfolio.platform.service.FossilNotificationListenerService.h
            if (r0 == 0) goto L_0x0072
            r0 = r10
            com.portfolio.platform.service.FossilNotificationListenerService$h r0 = (com.portfolio.platform.service.FossilNotificationListenerService.h) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0072
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0081
            if (r0 != r5) goto L_0x0079
            java.lang.Object r0 = r2.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r2.L$1
            android.service.notification.StatusBarNotification r1 = (android.service.notification.StatusBarNotification) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.service.FossilNotificationListenerService r2 = (com.portfolio.platform.service.FossilNotificationListenerService) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            if (r0 == 0) goto L_0x0044
            java.lang.String r3 = "user_id"
            java.lang.String r0 = r0.getUserId()
            r2.put(r3, r0)
        L_0x0044:
            java.lang.String r0 = r1.getPackageName()
            java.lang.String r3 = "statusBarNotification.packageName"
            com.fossil.pq7.b(r0, r3)
            java.lang.String r3 = "app_id"
            r2.put(r3, r0)
            java.lang.String r0 = "post_time"
            long r6 = r1.getPostTime()
            java.lang.String r1 = java.lang.String.valueOf(r6)
            r2.put(r0, r1)
            java.lang.String r0 = "blocked_by"
            r2.put(r0, r4)
            com.fossil.ck5$a r0 = com.fossil.ck5.f
            com.fossil.ck5 r0 = r0.g()
            java.lang.String r1 = "notification_blocked"
            r0.l(r1, r2)
        L_0x006f:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0071:
            return r0
        L_0x0072:
            com.portfolio.platform.service.FossilNotificationListenerService$h r0 = new com.portfolio.platform.service.FossilNotificationListenerService$h
            r0.<init>(r8, r10)
            r2 = r0
            goto L_0x0014
        L_0x0079:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0081:
            com.fossil.el7.b(r3)
            com.fossil.e47 r0 = com.fossil.e47.b
            java.lang.String r3 = r9.getPackageName()
            java.lang.String r4 = "statusBarNotification.packageName"
            com.fossil.pq7.b(r3, r4)
            boolean r0 = r0.i(r3)
            if (r0 == 0) goto L_0x006f
            boolean r0 = com.fossil.p47.k()
            if (r0 == 0) goto L_0x00b1
            java.lang.String r0 = "DND"
        L_0x009d:
            com.portfolio.platform.data.source.UserRepository r3 = r8.s
            if (r3 == 0) goto L_0x00b4
            r2.L$0 = r8
            r2.L$1 = r9
            r2.L$2 = r0
            r2.label = r5
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x00bb
            r0 = r1
            goto L_0x0071
        L_0x00b1:
            java.lang.String r0 = "Silence"
            goto L_0x009d
        L_0x00b4:
            java.lang.String r0 = "mUserRepository"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        L_0x00bb:
            r4 = r0
            r1 = r9
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.z(android.service.notification.StatusBarNotification, com.fossil.qn7):java.lang.Object");
    }
}
