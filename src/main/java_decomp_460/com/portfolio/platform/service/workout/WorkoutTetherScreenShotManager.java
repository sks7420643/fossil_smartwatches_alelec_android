package com.portfolio.platform.service.workout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import com.fossil.bw7;
import com.fossil.cs0;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.hr7;
import com.fossil.il7;
import com.fossil.im7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.ms0;
import com.fossil.p47;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.u08;
import com.fossil.uu7;
import com.fossil.ux7;
import com.fossil.v37;
import com.fossil.vp7;
import com.fossil.w08;
import com.fossil.xq0;
import com.fossil.xw7;
import com.fossil.yn7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterFragment;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformViewsController;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutTetherScreenShotManager implements cs0 {
    @DexIgnore
    public static /* final */ String v; // = (PortfolioApp.h0.c().getFilesDir() + File.separator + "%s");
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public u08 b; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ LinkedBlockingDeque<WorkoutScreenshotWrapper> c; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ uu7 e;
    @DexIgnore
    public /* final */ iv7 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public FlutterEngine h;
    @DexIgnore
    public MethodChannel i;
    @DexIgnore
    public FragmentActivity j;
    @DexIgnore
    public FrameLayout k;
    @DexIgnore
    public FlutterFragment l;
    @DexIgnore
    public int m;
    @DexIgnore
    public /* final */ Gson s;
    @DexIgnore
    public /* final */ WorkoutSessionRepository t;
    @DexIgnore
    public /* final */ FileRepository u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WorkoutTetherScreenShotManager.v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$enqueue$1", f = "WorkoutTetherScreenShotManager.kt", l = {206}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, qn7 qn7) {
            super(2, qn7);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job = workoutScreenshotWrapper;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$job, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            u08 u08;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.c.add(this.$job);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", this.$job.getWorkoutId() + " added to mQueue");
                u08 = this.this$0.b;
                this.L$0 = iv7;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                u08 = (u08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.r();
                tl7 tl7 = tl7.f3441a;
                u08.b(null);
                return tl7.f3441a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ JSONObject $mock$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f4717a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$c$a$a")
            /* renamed from: com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$c$a$a  reason: collision with other inner class name */
            public static final class C0352a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0352a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0352a aVar = new C0352a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0352a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.f4717a.this$0;
                        workoutTetherScreenShotManager.m--;
                        this.this$0.f4717a.this$0.d = null;
                        this.this$0.f4717a.this$0.g = false;
                        this.this$0.f4717a.this$0.r();
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Object $result;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, Object obj, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$result = obj;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, this.$result, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        Object obj2 = this.$result;
                        if (obj2 != null) {
                            String string = ((JSONObject) obj2).getString("imageData");
                            FileRepository fileRepository = this.this$0.f4717a.this$0.u;
                            String workoutId = this.this$0.f4717a.$job$inlined.getWorkoutId();
                            hr7 hr7 = hr7.f1520a;
                            String format = String.format(WorkoutTetherScreenShotManager.w.a(), Arrays.copyOf(new Object[]{this.this$0.f4717a.$job$inlined.getWorkoutId()}, 1));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                            String saveLocalDataFile = fileRepository.saveLocalDataFile(string, workoutId, format);
                            FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.this$0.f4717a.$job$inlined.getWorkoutId() + " success insert to DB, uri " + saveLocalDataFile);
                            WorkoutSessionRepository workoutSessionRepository = this.this$0.f4717a.this$0.t;
                            String workoutId2 = this.this$0.f4717a.$job$inlined.getWorkoutId();
                            this.L$0 = iv7;
                            this.L$1 = string;
                            this.L$2 = saveLocalDataFile;
                            this.label = 1;
                            if (workoutSessionRepository.insertWorkoutTetherScreenShot(workoutId2, saveLocalDataFile, this) == d) {
                                return d;
                            }
                        } else {
                            throw new il7("null cannot be cast to non-null type org.json.JSONObject");
                        }
                    } else if (i == 1) {
                        String str = (String) this.L$2;
                        String str2 = (String) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.f4717a.this$0;
                    workoutTetherScreenShotManager.m--;
                    this.this$0.f4717a.this$0.d = null;
                    this.this$0.f4717a.this$0.g = false;
                    this.this$0.f4717a.this$0.r();
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.f4717a = cVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.f4717a.$job$inlined.getWorkoutId() + " error " + str2 + ' ');
                xw7 unused = gu7.d(this.f4717a.this$0.f, null, null, new C0352a(this, null), 3, null);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.f4717a.$job$inlined.getWorkoutId() + " notImplemented ");
                this.f4717a.this$0.d = null;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.f4717a.$job$inlined.getWorkoutId() + " success");
                xw7 unused = gu7.d(this.f4717a.this$0.f, null, null, new b(this, obj, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qn7 qn7, WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, JSONObject jSONObject) {
            super(2, qn7);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job$inlined = workoutScreenshotWrapper;
            this.$mock$inlined = jSONObject;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(qn7, this.this$0, this.$job$inlined, this.$mock$inlined);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.l == null || this.this$0.k == null) {
                    this.this$0.q();
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.$job$inlined.getWorkoutId() + " invoke flutter module to take screenshot");
                MethodChannel methodChannel = this.this$0.i;
                if (methodChannel != null) {
                    methodChannel.invokeMethod("takeSnapShot", this.$mock$inlined, new a(this));
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public WorkoutTetherScreenShotManager(WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository) {
        DartExecutor dartExecutor;
        BinaryMessenger binaryMessenger = null;
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        pq7.c(fileRepository, "mFileRepository");
        this.t = workoutSessionRepository;
        this.u = fileRepository;
        uu7 b2 = ux7.b(null, 1, null);
        this.e = b2;
        this.f = jv7.a(b2.plus(bw7.b()));
        this.g = true;
        FlutterEngine b3 = v37.c.b();
        this.h = b3;
        if (!(b3 == null || (dartExecutor = b3.getDartExecutor()) == null)) {
            binaryMessenger = dartExecutor.getBinaryMessenger();
        }
        this.i = new MethodChannel(binaryMessenger, "detailTracking/screenShotMap", JSONMethodCodec.INSTANCE);
        this.s = new Gson();
    }

    @DexIgnore
    @ms0(Lifecycle.a.ON_STOP)
    public final void cancel() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "onActivity onStop currentTaskId " + this.d);
        this.c.clear();
        this.d = null;
        this.m = 0;
        s();
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "attachPlaceHolderView");
        FragmentActivity fragmentActivity = this.j;
        if (fragmentActivity != null) {
            FlutterFragment flutterFragment = (FlutterFragment) fragmentActivity.getSupportFragmentManager().Z("TAG_FLUTTER_FRAGMENT");
            this.l = flutterFragment;
            if (flutterFragment == null || this.k == null) {
                FrameLayout frameLayout = new FrameLayout(fragmentActivity);
                this.k = frameLayout;
                if (frameLayout != null) {
                    frameLayout.setId(View.generateViewId());
                    ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams((int) p47.b(350.0f), (int) p47.b(200.0f));
                    marginLayoutParams.setMargins(0, Constants.BOLT_RSSI_ERROR, 0, 0);
                    FrameLayout frameLayout2 = this.k;
                    if (frameLayout2 != null) {
                        frameLayout2.setLayoutParams(new FrameLayout.LayoutParams(marginLayoutParams));
                        View findViewById = fragmentActivity.findViewById(2131362158);
                        if (findViewById != null) {
                            ((ViewGroup) findViewById).addView(this.k);
                            this.l = FlutterFragment.withCachedEngine("screenshot_engine_id").build();
                            xq0 j2 = fragmentActivity.getSupportFragmentManager().j();
                            FrameLayout frameLayout3 = this.k;
                            if (frameLayout3 != null) {
                                int id = frameLayout3.getId();
                                FlutterFragment flutterFragment2 = this.l;
                                if (flutterFragment2 != null) {
                                    j2.b(id, flutterFragment2, "TAG_FLUTTER_FRAGMENT");
                                    j2.h();
                                    return;
                                }
                                throw new il7("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                            }
                            pq7.i();
                            throw null;
                        }
                        throw new il7("null cannot be cast to non-null type android.view.ViewGroup");
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void r() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutTetherScreenShotManager", "dequeue queueSize " + this.c.size() + " currentJobRunning " + this.m);
            if (this.c.size() > 0 && this.m == 0) {
                WorkoutScreenshotWrapper pop = this.c.pop();
                pop.setResetState(this.g);
                pq7.b(pop, "jobToRun");
                u(pop);
            }
        }
    }

    @DexIgnore
    public final void s() {
        PlatformViewsController platformViewsController;
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "detachPlaceHolderView");
        FragmentActivity fragmentActivity = this.j;
        if (fragmentActivity != null) {
            if (this.l != null) {
                xq0 j2 = fragmentActivity.getSupportFragmentManager().j();
                FlutterFragment flutterFragment = this.l;
                if (flutterFragment != null) {
                    j2.q(flutterFragment);
                    j2.h();
                } else {
                    throw new il7("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                }
            }
            if (this.k != null) {
                View findViewById = fragmentActivity.findViewById(2131362158);
                if (findViewById != null) {
                    ((ViewGroup) findViewById).removeView(this.k);
                } else {
                    throw new il7("null cannot be cast to non-null type android.view.ViewGroup");
                }
            }
            FlutterFragment flutterFragment2 = this.l;
            if (flutterFragment2 != null) {
                flutterFragment2.onDestroyView();
            }
            FlutterEngine flutterEngine = this.h;
            if (!(flutterEngine == null || (platformViewsController = flutterEngine.getPlatformViewsController()) == null)) {
                platformViewsController.detachFromView();
            }
        }
        this.j = null;
        this.l = null;
        this.k = null;
    }

    @DexIgnore
    public final void t(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        synchronized (this) {
            pq7.c(workoutScreenshotWrapper, "job");
            if (pq7.a(this.d, workoutScreenshotWrapper.getWorkoutId()) || this.c.contains(workoutScreenshotWrapper)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", workoutScreenshotWrapper.getWorkoutId() + " is already added");
                return;
            }
            xw7 unused = gu7.d(this.f, null, null, new b(this, workoutScreenshotWrapper, null), 3, null);
        }
    }

    @DexIgnore
    public final void u(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        JSONObject jSONObject = new JSONObject(this.s.t(workoutScreenshotWrapper));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + workoutScreenshotWrapper.getWorkoutId() + " resetState " + workoutScreenshotWrapper.getResetState());
        this.m = this.m + 1;
        this.d = workoutScreenshotWrapper.getWorkoutId();
        if (this.j != null) {
            xw7 unused = gu7.d(jv7.a(bw7.c()), null, null, new c(null, this, workoutScreenshotWrapper, jSONObject), 3, null);
        }
    }

    @DexIgnore
    public final String v(String str) {
        pq7.c(str, "workoutId");
        hr7 hr7 = hr7.f1520a;
        String format = String.format(v, Arrays.copyOf(new Object[]{str}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final void w(FragmentActivity fragmentActivity) {
        pq7.c(fragmentActivity, Constants.ACTIVITY);
        this.j = fragmentActivity;
    }

    @DexIgnore
    public final WorkoutScreenshotWrapper x(WorkoutSession workoutSession) {
        ArrayList arrayList;
        pq7.c(workoutSession, "workoutSession");
        String id = workoutSession.getId();
        int b2 = (int) p47.b(350.0f);
        int b3 = (int) p47.b(200.0f);
        List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
        if (workoutGpsPoints != null) {
            arrayList = new ArrayList(im7.m(workoutGpsPoints, 10));
            Iterator<T> it = workoutGpsPoints.iterator();
            while (it.hasNext()) {
                arrayList.add(new WorkoutRouterGpsWrapper(it.next()));
            }
        } else {
            arrayList = null;
        }
        return new WorkoutScreenshotWrapper(id, b2, b3, true, arrayList);
    }
}
