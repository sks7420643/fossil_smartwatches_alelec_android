package com.portfolio.platform.service.fcm;

import com.fossil.ao7;
import com.fossil.bw7;
import com.fossil.ci4;
import com.fossil.dr5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.pr4;
import com.fossil.pu6;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.ux7;
import com.fossil.vp7;
import com.fossil.vt4;
import com.fossil.ws4;
import com.fossil.xw7;
import com.fossil.yn7;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public pu6 h;
    @DexIgnore
    public dr5 i;
    @DexIgnore
    public on5 j;
    @DexIgnore
    public vt4 k;
    @DexIgnore
    public pr4 l;
    @DexIgnore
    public iv7 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$onMessageReceived$1", f = "FossilFirebaseMessagingService.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ci4 $remoteMessage;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$a$a")
        /* renamed from: com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$a$a  reason: collision with other inner class name */
        public static final class C0351a extends TypeToken<ws4> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(FossilFirebaseMessagingService fossilFirebaseMessagingService, ci4 ci4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fossilFirebaseMessagingService;
            this.$remoteMessage = ci4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$remoteMessage, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                Boolean b = this.this$0.A().b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("FossilFirebaseMessagingService", "onMessageReceived - isBCOn : " + b);
                pq7.b(b, "isBcOn");
                if (b.booleanValue()) {
                    try {
                        String obj2 = this.$remoteMessage.c().toString();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.e("FossilFirebaseMessagingService", "dataStr: " + obj2);
                        ws4 ws4 = (ws4) new Gson().l(obj2, new C0351a().getType());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("isActive: ");
                        iv7 iv7 = this.this$0.m;
                        sb.append(iv7 != null ? ao7.a(jv7.g(iv7)) : null);
                        sb.append(" - notification: ");
                        sb.append(ws4);
                        local3.e("FossilFirebaseMessagingService", sb.toString());
                        if (ws4 != null) {
                            this.this$0.y().k(ws4);
                            this.this$0.z().a(ws4, this.this$0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$registerFCM$1", f = "FossilFirebaseMessagingService.kt", l = {131}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FossilFirebaseMessagingService fossilFirebaseMessagingService, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fossilFirebaseMessagingService;
            this.$token = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$token, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                vt4 x = this.this$0.x();
                String str = this.$token;
                this.L$0 = iv7;
                this.label = 1;
                if (x.c(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public final on5 A() {
        on5 on5 = this.j;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public final void B(String str) {
        iv7 iv7 = this.m;
        if (iv7 != null) {
            xw7 unused = gu7.d(iv7, bw7.b(), null, new b(this, str, null), 2, null);
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.m = jv7.h(jv7.a(bw7.b()), ux7.b(null, 1, null));
        PortfolioApp.h0.c().M().l0(this);
    }

    @DexIgnore
    @Override // com.fossil.sh4
    public void onDestroy() {
        super.onDestroy();
        iv7 iv7 = this.m;
        if (iv7 != null) {
            jv7.d(iv7, null, 1, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void r(ci4 ci4) {
        iv7 iv7;
        pq7.c(ci4, "remoteMessage");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "onMessageReceived(), from:  " + ci4.f() + " - priority: " + ci4.A() + " - notification: " + ci4.k() + " - messageData: " + ci4.c());
        Map<String, String> c = ci4.c();
        pq7.b(c, "remoteMessage.data");
        if ((!c.isEmpty()) && (iv7 = this.m) != null) {
            xw7 unused = gu7.d(iv7, bw7.b(), null, new a(this, ci4, null), 2, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void t(String str) {
        pq7.c(str, "refreshedToken");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "Refreshed token: " + str);
        on5 on5 = this.j;
        if (on5 != null) {
            String p = on5.p();
            if (p == null || p.length() == 0) {
                on5 on52 = this.j;
                if (on52 != null) {
                    on52.e1(str);
                    on5 on53 = this.j;
                    if (on53 == null) {
                        pq7.n("mSharedPrefs");
                        throw null;
                    } else if (on53.Z1()) {
                        B(str);
                    }
                } else {
                    pq7.n("mSharedPrefs");
                    throw null;
                }
            } else {
                on5 on54 = this.j;
                if (on54 != null) {
                    on54.e1(str);
                    B(str);
                    return;
                }
                pq7.n("mSharedPrefs");
                throw null;
            }
        } else {
            pq7.n("mSharedPrefs");
            throw null;
        }
    }

    @DexIgnore
    public final vt4 x() {
        vt4 vt4 = this.k;
        if (vt4 != null) {
            return vt4;
        }
        pq7.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final dr5 y() {
        dr5 dr5 = this.i;
        if (dr5 != null) {
            return dr5;
        }
        pq7.n("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final pu6 z() {
        pu6 pu6 = this.h;
        if (pu6 != null) {
            return pu6;
        }
        pq7.n("mInAppNotificationManager");
        throw null;
    }
}
