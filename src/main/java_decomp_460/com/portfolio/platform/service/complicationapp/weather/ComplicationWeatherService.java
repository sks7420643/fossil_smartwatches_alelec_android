package com.portfolio.platform.service.complicationapp.weather;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.bw7;
import com.fossil.ck5;
import com.fossil.co7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.fr5;
import com.fossil.gu7;
import com.fossil.hr5;
import com.fossil.hr7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.sl5;
import com.fossil.tl7;
import com.fossil.tq4;
import com.fossil.ul5;
import com.fossil.uq4;
import com.fossil.ux7;
import com.fossil.vp7;
import com.fossil.w27;
import com.fossil.xw7;
import com.fossil.yh5;
import com.fossil.yn7;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationWeatherService extends fr5 {
    @DexIgnore
    public /* final */ int h; // = 1800000;
    @DexIgnore
    public w27 i;
    @DexIgnore
    public uq4 j;
    @DexIgnore
    public on5 k;
    @DexIgnore
    public PortfolioApp l;
    @DexIgnore
    public CustomizeRealDataRepository m;
    @DexIgnore
    public UserRepository s;
    @DexIgnore
    public /* final */ iv7 t; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public String u;
    @DexIgnore
    public WeatherSettings v;
    @DexIgnore
    public Weather w;
    @DexIgnore
    public a<String> x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ HashSet<T> f4710a; // = new HashSet<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.f4710a.add(t);
            }
        }

        @DexIgnore
        public final boolean b() {
            boolean isEmpty;
            synchronized (this.b) {
                isEmpty = this.f4710a.isEmpty();
            }
            return isEmpty;
        }

        @DexIgnore
        public final T c() {
            T next;
            synchronized (this.b) {
                Iterator<T> it = this.f4710a.iterator();
                pq7.b(it, "hashSet.iterator()");
                next = it.hasNext() ? it.next() : null;
            }
            return next;
        }

        @DexIgnore
        public final void d(T t) {
            synchronized (this.b) {
                this.f4710a.remove(t);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.d<w27.d, w27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ComplicationWeatherService f4711a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeather$1$onSuccess$1", f = "ComplicationWeatherService.kt", l = {147}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    this.this$0.f4711a.q().u1(new Gson().u(this.this$0.f4711a.s(), WeatherSettings.class));
                    ComplicationWeatherService complicationWeatherService = this.this$0.f4711a;
                    WeatherSettings s = complicationWeatherService.s();
                    if (s != null) {
                        this.L$0 = iv7;
                        this.label = 1;
                        if (ComplicationWeatherService.y(complicationWeatherService, s, false, this, 2, null) == d) {
                            return d;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.f4711a.a();
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ComplicationWeatherService complicationWeatherService) {
            this.f4711a = complicationWeatherService;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(w27.b bVar) {
            pq7.c(bVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(fr5.g.a(), "getWeather - onError");
            this.f4711a.w(bVar.a());
            this.f4711a.a();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(w27.d dVar) {
            pq7.c(dVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(fr5.g.a(), "getWeather - onSuccess");
            this.f4711a.z(dVar.a());
            if (this.f4711a.r() != null) {
                xw7 unused = gu7.d(this.f4711a.t, null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1", f = "ComplicationWeatherService.kt", l = {118, 121}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ComplicationWeatherService complicationWeatherService, qn7 qn7) {
            super(2, qn7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object location$default;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.this$0.s() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = fr5.g.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getWeatherBaseOnLocation - location=");
                    WeatherSettings s = this.this$0.s();
                    if (s != null) {
                        sb.append(s.getLocation());
                        local.d(a2, sb.toString());
                        WeatherSettings s2 = this.this$0.s();
                        if (s2 == null) {
                            pq7.i();
                            throw null;
                        } else if (!s2.isUseCurrentLocation()) {
                            this.this$0.t();
                        } else {
                            Calendar instance = Calendar.getInstance();
                            pq7.b(instance, "Calendar.getInstance()");
                            long timeInMillis = instance.getTimeInMillis();
                            WeatherSettings s3 = this.this$0.s();
                            if (s3 == null) {
                                pq7.i();
                                throw null;
                            } else if (timeInMillis - s3.getUpdatedAt() < ((long) this.this$0.h)) {
                                ComplicationWeatherService complicationWeatherService = this.this$0;
                                WeatherSettings s4 = complicationWeatherService.s();
                                if (s4 != null) {
                                    this.L$0 = iv7;
                                    this.label = 1;
                                    if (complicationWeatherService.x(s4, true, this) == d) {
                                        return d;
                                    }
                                    this.this$0.a();
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                LocationSource g = this.this$0.g();
                                PortfolioApp p = this.this$0.p();
                                this.L$0 = iv7;
                                this.label = 2;
                                location$default = LocationSource.getLocation$default(g, p, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0, this, 28, null);
                                if (location$default == d) {
                                    return d;
                                }
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                this.this$0.a();
                return tl7.f3441a;
            } else if (i == 2) {
                iv7 iv73 = (iv7) this.L$0;
                el7.b(obj);
                location$default = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LocationSource.Result result = (LocationSource.Result) location$default;
            if (result.getErrorState() == LocationSource.ErrorState.SUCCESS) {
                Location location = result.getLocation();
                WeatherSettings s5 = this.this$0.s();
                if (s5 == null) {
                    pq7.i();
                    throw null;
                } else if (location != null) {
                    s5.setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    this.this$0.t();
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                this.this$0.h(result.getErrorState());
                this.this$0.v(result.getErrorState());
                FLogger.INSTANCE.getLocal().d(fr5.g.a(), "getWeatherBaseOnLocation - using current location but location is null, stop service.");
                this.this$0.a();
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService", f = "ComplicationWeatherService.kt", l = {167}, m = "sendComplicationAppResponse")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ComplicationWeatherService complicationWeatherService, qn7 qn7) {
            super(qn7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1", f = "ComplicationWeatherService.kt", l = {}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ComplicationWeatherService complicationWeatherService, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = complicationWeatherService;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$rainProbabilityPercent, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.o().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService", f = "ComplicationWeatherService.kt", l = {177}, m = "showTemperature")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ComplicationWeatherService complicationWeatherService, qn7 qn7) {
            super(qn7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.B(null, false, this);
        }
    }

    @DexIgnore
    public static /* synthetic */ Object y(ComplicationWeatherService complicationWeatherService, WeatherSettings weatherSettings, boolean z, qn7 qn7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return complicationWeatherService.x(weatherSettings, z, qn7);
    }

    @DexIgnore
    public final void A(float f2, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = fr5.g.a();
        local.d(a2, "showChanceOfRain - probability=" + f2);
        int i2 = (int) (((float) 100) * f2);
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = new ChanceOfRainComplicationAppInfo(i2, 3600);
        PortfolioApp c2 = PortfolioApp.h0.c();
        String str = this.u;
        if (str != null) {
            c2.g1(chanceOfRainComplicationAppInfo, str);
            ul5 f3 = ck5.f.f("chance-of-rain");
            if (f3 != null) {
                String str2 = this.u;
                if (str2 != null) {
                    f3.d(str2, z, "");
                } else {
                    pq7.n("mSerial");
                    throw null;
                }
            }
            ck5.f.k("chance-of-rain");
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new e(this, i2, null), 3, null);
            return;
        }
        pq7.n("mSerial");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object B(com.portfolio.platform.data.model.microapp.weather.WeatherSettings r9, boolean r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 445
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.B(com.portfolio.platform.data.model.microapp.weather.WeatherSettings, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public void a() {
        FLogger.INSTANCE.getLocal().d(fr5.g.a(), VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public void b() {
        FLogger.INSTANCE.getLocal().d(fr5.g.a(), "forceStop");
    }

    @DexIgnore
    public final String n(LocationSource.ErrorState errorState) {
        int i2 = hr5.b[errorState.ordinal()];
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? yh5.UNKNOWN.getCode() : yh5.LOCATION_SERVICE_DISABLED.getCode() : yh5.BACKGROUND_LOCATION_ACCESS_DISABLED.getCode() : yh5.LOCATION_ACCESS_DISABLED.getCode();
    }

    @DexIgnore
    public final CustomizeRealDataRepository o() {
        CustomizeRealDataRepository customizeRealDataRepository = this.m;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        pq7.n("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.kr5
    public IBinder onBind(Intent intent) {
        pq7.c(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(fr5.g.a(), "onCreate");
        PortfolioApp.h0.c().M().f0(this);
        this.u = PortfolioApp.h0.c().J();
        this.x = new a<>();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(fr5.g.a(), "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        String string;
        pq7.c(intent, "intent");
        FLogger.INSTANCE.getLocal().d(fr5.g.a(), "onStartCommand");
        this.b = 3001;
        super.d();
        Bundle extras = intent.getExtras();
        if (extras == null || (string = extras.getString("action")) == null) {
            return 2;
        }
        a<String> aVar = this.x;
        if (aVar == null) {
            pq7.n("mWeatherTasks");
            throw null;
        } else if (aVar.b()) {
            a<String> aVar2 = this.x;
            if (aVar2 != null) {
                aVar2.a(string);
                on5 on5 = this.k;
                if (on5 != null) {
                    String w2 = on5.w();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = fr5.g.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Inside SteamingAction .run - WEATHER_ACTION json ");
                    if (w2 != null) {
                        sb.append(w2);
                        local.d(a2, sb.toString());
                        this.v = (WeatherSettings) new Gson().k(w2, WeatherSettings.class);
                        u();
                        return 2;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mSharedPreferencesManager");
                throw null;
            }
            pq7.n("mWeatherTasks");
            throw null;
        } else {
            a<String> aVar3 = this.x;
            if (aVar3 != null) {
                aVar3.a(string);
                return 2;
            }
            pq7.n("mWeatherTasks");
            throw null;
        }
    }

    @DexIgnore
    public final PortfolioApp p() {
        PortfolioApp portfolioApp = this.l;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        pq7.n("mPortfolioApp");
        throw null;
    }

    @DexIgnore
    public final on5 q() {
        on5 on5 = this.k;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final Weather r() {
        return this.w;
    }

    @DexIgnore
    public final WeatherSettings s() {
        return this.v;
    }

    @DexIgnore
    public final void t() {
        uq4 uq4 = this.j;
        if (uq4 != null) {
            w27 w27 = this.i;
            if (w27 != null) {
                WeatherSettings weatherSettings = this.v;
                if (weatherSettings != null) {
                    LatLng latLng = weatherSettings.getLatLng();
                    WeatherSettings weatherSettings2 = this.v;
                    if (weatherSettings2 != null) {
                        uq4.a(w27, new w27.c(latLng, weatherSettings2.getTempUnit()), new b(this));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mGetWeather");
                throw null;
            }
        } else {
            pq7.n("mUseCaseHandler");
            throw null;
        }
    }

    @DexIgnore
    public final xw7 u() {
        return gu7.d(jv7.a(bw7.a()), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void v(LocationSource.ErrorState errorState) {
        a<String> aVar = this.x;
        if (aVar != null) {
            String c2 = aVar.c();
            if (c2 == null) {
                return;
            }
            if (pq7.a(c2, "TEMPERATURE")) {
                ul5 f2 = ck5.f.f("weather");
                if (f2 != null) {
                    hr7 hr7 = hr7.f1520a;
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{"weather"}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    sl5 b2 = ck5.f.b(format);
                    b2.a("error_code", n(errorState));
                    f2.a(b2);
                    return;
                }
                return;
            }
            ul5 f3 = ck5.f.f("chance-of-rain");
            if (f3 != null) {
                hr7 hr72 = hr7.f1520a;
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{ck5.f.h("chance-of-rain")}, 1));
                pq7.b(format2, "java.lang.String.format(format, *args)");
                sl5 b3 = ck5.f.b(format2);
                b3.a("error_code", n(errorState));
                f3.a(b3);
                return;
            }
            return;
        }
        pq7.n("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final void w(String str) {
        pq7.c(str, "errorMessage");
        a<String> aVar = this.x;
        if (aVar != null) {
            String c2 = aVar.c();
            if (c2 == null) {
                return;
            }
            if (pq7.a(c2, "TEMPERATURE")) {
                ul5 f2 = ck5.f.f("weather");
                if (f2 != null) {
                    hr7 hr7 = hr7.f1520a;
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{"weather"}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    sl5 b2 = ck5.f.b(format);
                    b2.a("error_code", str);
                    f2.a(b2);
                    return;
                }
                return;
            }
            ul5 f3 = ck5.f.f("chance-of-rain");
            if (f3 != null) {
                hr7 hr72 = hr7.f1520a;
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{ck5.f.h("chance-of-rain")}, 1));
                pq7.b(format2, "java.lang.String.format(format, *args)");
                sl5 b3 = ck5.f.b(format2);
                b3.a("error_code", str);
                f3.a(b3);
                return;
            }
            return;
        }
        pq7.n("mWeatherTasks");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        if (com.fossil.pq7.a(r1, "TEMPERATURE") == false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0060, code lost:
        r3.L$0 = r6;
        r3.L$1 = r2;
        r3.Z$0 = r5;
        r3.L$2 = r1;
        r3.L$3 = r1;
        r3.label = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0071, code lost:
        if (r6.B(r2, r5, r3) != r7) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008f, code lost:
        r4 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0097, code lost:
        if (com.fossil.pq7.a(r1, "RAIN_CHANCE") == false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0099, code lost:
        r6.A(r2.getRainProbability(), r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return r7;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(com.portfolio.platform.data.model.microapp.weather.WeatherSettings r10, boolean r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
            r9 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 0
            monitor-enter(r9)
            boolean r1 = r12 instanceof com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.d     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0076
            r0 = r12
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d r0 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.d) r0     // Catch:{ all -> 0x007c }
            r1 = r0
            int r2 = r1.label     // Catch:{ all -> 0x007c }
            r2 = r2 & r3
            if (r2 == 0) goto L_0x0076
            int r2 = r1.label     // Catch:{ all -> 0x007c }
            int r2 = r2 + r3
            r1.label = r2     // Catch:{ all -> 0x007c }
            r4 = r1
        L_0x0018:
            java.lang.Object r6 = r4.result     // Catch:{ all -> 0x007c }
            java.lang.Object r7 = com.fossil.yn7.d()     // Catch:{ all -> 0x007c }
            int r1 = r4.label     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0087
            if (r1 != r5) goto L_0x007f
            java.lang.Object r1 = r4.L$3     // Catch:{ all -> 0x007c }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x007c }
            java.lang.Object r2 = r4.L$2     // Catch:{ all -> 0x007c }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x007c }
            boolean r5 = r4.Z$0     // Catch:{ all -> 0x007c }
            java.lang.Object r2 = r4.L$1     // Catch:{ all -> 0x007c }
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings r2 = (com.portfolio.platform.data.model.microapp.weather.WeatherSettings) r2     // Catch:{ all -> 0x007c }
            java.lang.Object r3 = r4.L$0     // Catch:{ all -> 0x007c }
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r3 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService) r3     // Catch:{ all -> 0x007c }
            com.fossil.el7.b(r6)     // Catch:{ all -> 0x007c }
            r6 = r3
        L_0x003a:
            r3 = r4
        L_0x003b:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r4 = r6.x     // Catch:{ all -> 0x007c }
            if (r4 == 0) goto L_0x00a1
            r4.d(r1)     // Catch:{ all -> 0x007c }
        L_0x0042:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r1 = r6.x     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x00b1
            boolean r1 = r1.b()     // Catch:{ all -> 0x007c }
            if (r1 != 0) goto L_0x00ad
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r1 = r6.x     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x00a7
            java.lang.Object r1 = r1.c()     // Catch:{ all -> 0x007c }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0042
            java.lang.String r4 = "TEMPERATURE"
            boolean r4 = com.fossil.pq7.a(r1, r4)     // Catch:{ all -> 0x007c }
            if (r4 == 0) goto L_0x0091
            r3.L$0 = r6     // Catch:{ all -> 0x007c }
            r3.L$1 = r2     // Catch:{ all -> 0x007c }
            r3.Z$0 = r5     // Catch:{ all -> 0x007c }
            r3.L$2 = r1     // Catch:{ all -> 0x007c }
            r3.L$3 = r1     // Catch:{ all -> 0x007c }
            r4 = 1
            r3.label = r4     // Catch:{ all -> 0x007c }
            java.lang.Object r4 = r6.B(r2, r5, r3)     // Catch:{ all -> 0x007c }
            if (r4 != r7) goto L_0x008f
            monitor-exit(r9)
            r1 = r7
        L_0x0075:
            return r1
        L_0x0076:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d r4 = new com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d
            r4.<init>(r9, r12)
            goto L_0x0018
        L_0x007c:
            r1 = move-exception
            monitor-exit(r9)
            throw r1
        L_0x007f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0087:
            com.fossil.el7.b(r6)
            r2 = r10
            r3 = r4
            r5 = r11
            r6 = r9
            goto L_0x0042
        L_0x008f:
            r4 = r3
            goto L_0x003a
        L_0x0091:
            java.lang.String r4 = "RAIN_CHANCE"
            boolean r4 = com.fossil.pq7.a(r1, r4)
            if (r4 == 0) goto L_0x003b
            float r4 = r2.getRainProbability()
            r6.A(r4, r5)
            goto L_0x003b
        L_0x00a1:
            java.lang.String r1 = "mWeatherTasks"
            com.fossil.pq7.n(r1)
            throw r8
        L_0x00a7:
            java.lang.String r1 = "mWeatherTasks"
            com.fossil.pq7.n(r1)
            throw r8
        L_0x00ad:
            com.fossil.tl7 r1 = com.fossil.tl7.f3441a
            monitor-exit(r9)
            goto L_0x0075
        L_0x00b1:
            java.lang.String r1 = "mWeatherTasks"
            com.fossil.pq7.n(r1)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.x(com.portfolio.platform.data.model.microapp.weather.WeatherSettings, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void z(Weather weather) {
        this.w = weather;
    }
}
