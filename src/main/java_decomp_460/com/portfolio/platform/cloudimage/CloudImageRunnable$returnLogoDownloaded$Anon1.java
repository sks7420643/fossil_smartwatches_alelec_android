package com.portfolio.platform.cloudimage;

import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.iv7;
import com.fossil.jx7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1", f = "CloudImageRunnable.kt", l = {183, 186, 189}, m = "invokeSuspend")
public final class CloudImageRunnable$returnLogoDownloaded$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzippedPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$3", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, qn7);
            anon3_Level2.p$ = (iv7) obj;
            return anon3_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon3_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), "");
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnLogoDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzippedPath = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1 = new CloudImageRunnable$returnLogoDownloaded$Anon1(this.this$0, this.$destinationUnzippedPath, qn7);
        cloudImageRunnable$returnLogoDownloaded$Anon1.p$ = (iv7) obj;
        return cloudImageRunnable$returnLogoDownloaded$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((CloudImageRunnable$returnLogoDownloaded$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            String str = this.$destinationUnzippedPath + '/' + this.this$0.type + ".webp";
            String str2 = this.$destinationUnzippedPath + '/' + this.this$0.type + ".png";
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                jx7 c = bw7.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, null);
                this.L$0 = iv7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 1;
                if (eu7.g(c, anon1_Level2, this) == d) {
                    return d;
                }
            } else if (AssetUtil.INSTANCE.checkFileExist(str2)) {
                jx7 c2 = bw7.c();
                Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, null);
                this.L$0 = iv7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 2;
                if (eu7.g(c2, anon2_Level2, this) == d) {
                    return d;
                }
            } else {
                jx7 c3 = bw7.c();
                Anon3_Level2 anon3_Level2 = new Anon3_Level2(this, null);
                this.L$0 = iv7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 3;
                if (eu7.g(c3, anon3_Level2, this) == d) {
                    return d;
                }
            }
        } else if (i == 1 || i == 2 || i == 3) {
            String str3 = (String) this.L$2;
            String str4 = (String) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
