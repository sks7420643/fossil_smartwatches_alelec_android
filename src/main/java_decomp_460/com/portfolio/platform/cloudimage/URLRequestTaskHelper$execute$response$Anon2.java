package com.portfolio.platform.cloudimage;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$2", f = "URLRequestTaskHelper.kt", l = {62}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$Anon2 extends ko7 implements rp7<qn7<? super q88<ApiResponse<gj4>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, qn7 qn7) {
        super(1, qn7);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new URLRequestTaskHelper$execute$response$Anon2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<gj4>>> qn7) {
        return ((URLRequestTaskHelper$execute$response$Anon2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 mApiService = this.this$0.getMApiService();
            String serialNumber$app_fossilRelease = this.this$0.getSerialNumber$app_fossilRelease();
            if (serialNumber$app_fossilRelease != null) {
                String feature$app_fossilRelease = this.this$0.getFeature$app_fossilRelease();
                if (feature$app_fossilRelease != null) {
                    String str = this.this$0.resolution;
                    if (str != null) {
                        this.label = 1;
                        Object deviceAssets$default = ApiServiceV2.DefaultImpls.getDeviceAssets$default(mApiService, 20, 0, serialNumber$app_fossilRelease, feature$app_fossilRelease, str, "ANDROID", null, this, 64, null);
                        return deviceAssets$default == d ? d : deviceAssets$default;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
