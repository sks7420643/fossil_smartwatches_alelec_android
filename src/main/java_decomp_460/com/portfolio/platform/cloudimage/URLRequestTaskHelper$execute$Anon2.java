package com.portfolio.platform.cloudimage;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$2", f = "URLRequestTaskHelper.kt", l = {}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ AssetsDeviceResponse $assetsDeviceResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, AssetsDeviceResponse assetsDeviceResponse, qn7 qn7) {
        super(2, qn7);
        this.this$0 = uRLRequestTaskHelper;
        this.$assetsDeviceResponse = assetsDeviceResponse;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(this.this$0, this.$assetsDeviceResponse, qn7);
        uRLRequestTaskHelper$execute$Anon2.p$ = (iv7) obj;
        return uRLRequestTaskHelper$execute$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((URLRequestTaskHelper$execute$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            URLRequestTaskHelper.OnNextTaskListener listener$app_fossilRelease = this.this$0.getListener$app_fossilRelease();
            if (listener$app_fossilRelease == null) {
                return null;
            }
            String zipFilePath$app_fossilRelease = this.this$0.getZipFilePath$app_fossilRelease();
            String destinationUnzipPath$app_fossilRelease = this.this$0.getDestinationUnzipPath$app_fossilRelease();
            AssetsDeviceResponse assetsDeviceResponse = this.$assetsDeviceResponse;
            pq7.b(assetsDeviceResponse, "assetsDeviceResponse");
            listener$app_fossilRelease.downloadFile(zipFilePath$app_fossilRelease, destinationUnzipPath$app_fossilRelease, assetsDeviceResponse);
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
