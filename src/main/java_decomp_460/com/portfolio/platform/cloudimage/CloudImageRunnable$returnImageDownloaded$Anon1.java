package com.portfolio.platform.cloudimage;

import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.er7;
import com.fossil.eu7;
import com.fossil.fs7;
import com.fossil.iv7;
import com.fossil.jx7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.rq7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1", f = "CloudImageRunnable.kt", l = {151, 162}, m = "invokeSuspend")
public final class CloudImageRunnable$returnImageDownloaded$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class Anon1_Level3 extends rq7 {
            @DexIgnore
            public Anon1_Level3(CloudImageRunnable cloudImageRunnable) {
                super(cloudImageRunnable);
            }

            @DexIgnore
            @Override // com.fossil.rq7
            public Object get() {
                return CloudImageRunnable.access$getSerialNumber$p((CloudImageRunnable) this.receiver);
            }

            @DexIgnore
            @Override // com.fossil.gq7, com.fossil.ds7
            public String getName() {
                return "serialNumber";
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public fs7 getOwner() {
                return er7.b(CloudImageRunnable.class);
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public String getSignature() {
                return "getSerialNumber()Ljava/lang/String;";
            }

            @DexIgnore
            @Override // com.fossil.rq7
            public void set(Object obj) {
                ((CloudImageRunnable) this.receiver).serialNumber = (String) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.this$0.serialNumber != null) {
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                    if (onImageCallbackListener == null) {
                        return null;
                    }
                    onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                    return tl7.f3441a;
                }
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener2 = this.this$0.this$0.mListener;
                if (onImageCallbackListener2 == null) {
                    return null;
                }
                onImageCallbackListener2.onImageCallback(CloudImageRunnable.access$getFastPairId$p(this.this$0.this$0), this.$filePath1);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class Anon1_Level3 extends rq7 {
            @DexIgnore
            public Anon1_Level3(CloudImageRunnable cloudImageRunnable) {
                super(cloudImageRunnable);
            }

            @DexIgnore
            @Override // com.fossil.rq7
            public Object get() {
                return CloudImageRunnable.access$getSerialNumber$p((CloudImageRunnable) this.receiver);
            }

            @DexIgnore
            @Override // com.fossil.gq7, com.fossil.ds7
            public String getName() {
                return "serialNumber";
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public fs7 getOwner() {
                return er7.b(CloudImageRunnable.class);
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public String getSignature() {
                return "getSerialNumber()Ljava/lang/String;";
            }

            @DexIgnore
            @Override // com.fossil.rq7
            public void set(Object obj) {
                ((CloudImageRunnable) this.receiver).serialNumber = (String) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.this$0.serialNumber != null) {
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                    if (onImageCallbackListener == null) {
                        return null;
                    }
                    onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                    return tl7.f3441a;
                }
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener2 = this.this$0.this$0.mListener;
                if (onImageCallbackListener2 == null) {
                    return null;
                }
                onImageCallbackListener2.onImageCallback(CloudImageRunnable.access$getFastPairId$p(this.this$0.this$0), this.$filePath2);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnImageDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzipPath = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1 = new CloudImageRunnable$returnImageDownloaded$Anon1(this.this$0, this.$destinationUnzipPath, qn7);
        cloudImageRunnable$returnImageDownloaded$Anon1.p$ = (iv7) obj;
        return cloudImageRunnable$returnImageDownloaded$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((CloudImageRunnable$returnImageDownloaded$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            String str = this.$destinationUnzipPath + '/' + this.this$0.type + ".webp";
            FLogger.INSTANCE.getLocal().d(CloudImageRunnable.TAG, "returnImageDownloaded(), filePath1=" + str);
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                jx7 c = bw7.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, null);
                this.L$0 = iv7;
                this.L$1 = str;
                this.label = 1;
                if (eu7.g(c, anon1_Level2, this) == d) {
                    return d;
                }
                return tl7.f3441a;
            }
            String str2 = this.$destinationUnzipPath + '/' + this.this$0.type + ".png";
            if (!AssetUtil.INSTANCE.checkFileExist(str2)) {
                return tl7.f3441a;
            }
            jx7 c2 = bw7.c();
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, null);
            this.L$0 = iv7;
            this.L$1 = str;
            this.L$2 = str2;
            this.label = 2;
            if (eu7.g(c2, anon2_Level2, this) == d) {
                return d;
            }
        } else if (i == 1) {
            String str3 = (String) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return tl7.f3441a;
        } else if (i == 2) {
            String str4 = (String) this.L$2;
            String str5 = (String) this.L$1;
            iv7 iv73 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
