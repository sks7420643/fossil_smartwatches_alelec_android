package com.portfolio.platform.cloudimage;

import com.fossil.pq7;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetUtil {
    @DexIgnore
    public static /* final */ AssetUtil INSTANCE; // = new AssetUtil();
    @DexIgnore
    public static /* final */ String TAG; // = "AssetUtil";

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAssetExist(java.io.File r26, java.lang.String r27, java.lang.String r28, java.lang.String r29, java.lang.String r30, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r31, com.fossil.qn7<? super java.lang.Boolean> r32) {
        /*
        // Method dump skipped, instructions count: 764
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkAssetExist(java.io.File, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final boolean checkFileExist(String str) {
        pq7.c(str, "filePath");
        return new File(str).exists();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x02ac  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkWearOSAssetExist(java.io.File r26, java.lang.String r27, java.lang.String r28, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r29, com.fossil.qn7<? super java.lang.Boolean> r30) {
        /*
        // Method dump skipped, instructions count: 727
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkWearOSAssetExist(java.io.File, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.fossil.qn7):java.lang.Object");
    }
}
