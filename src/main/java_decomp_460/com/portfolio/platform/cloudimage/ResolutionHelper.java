package com.portfolio.platform.cloudimage;

import android.content.Context;
import android.content.res.Resources;
import com.fossil.pq7;
import com.portfolio.platform.cloudimage.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResolutionHelper {
    @DexIgnore
    public static /* final */ ResolutionHelper INSTANCE; // = new ResolutionHelper();
    @DexIgnore
    public static float density;

    @DexIgnore
    public final Constants.Resolution getResolutionFromDevice() {
        float f = density;
        if (f < 1.5f) {
            return Constants.Resolution.MDPI;
        }
        if (f >= 1.5f && f < 2.0f) {
            return Constants.Resolution.HDPI;
        }
        float f2 = density;
        return (f2 < 2.0f || f2 >= 3.0f) ? Constants.Resolution.XXHDPI : Constants.Resolution.XHDPI;
    }

    @DexIgnore
    public final void initDeviceDensity(Context context) {
        pq7.c(context, "context");
        Resources resources = context.getResources();
        pq7.b(resources, "context.resources");
        density = resources.getDisplayMetrics().density;
    }
}
