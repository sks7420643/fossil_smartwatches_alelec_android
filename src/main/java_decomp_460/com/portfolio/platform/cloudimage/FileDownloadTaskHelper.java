package com.portfolio.platform.cloudimage;

import android.os.AsyncTask;
import com.fossil.kq7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDownloadTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + FileDownloadTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public OnDownloadFinishListener listener;
    @DexIgnore
    public AssetsDeviceResponse response;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class FileDownloadTask extends AsyncTask<Void, Void, Boolean> {
            @DexIgnore
            public /* final */ String destinationUnzipPath;
            @DexIgnore
            public OnDownloadFinishListener listener;
            @DexIgnore
            public /* final */ AssetsDeviceResponse objectResponse;
            @DexIgnore
            public /* final */ String zipFilePath;

            @DexIgnore
            public FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener) {
                pq7.c(str, "zipFilePath");
                pq7.c(str2, "destinationUnzipPath");
                pq7.c(assetsDeviceResponse, "objectResponse");
                this.zipFilePath = str;
                this.destinationUnzipPath = str2;
                this.objectResponse = assetsDeviceResponse;
                this.listener = onDownloadFinishListener;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener, int i, kq7 kq7) {
                this(str, str2, assetsDeviceResponse, (i & 8) != 0 ? null : onDownloadFinishListener);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0069  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
            /* JADX WARNING: Removed duplicated region for block: B:45:0x00b8  */
            /* JADX WARNING: Removed duplicated region for block: B:49:0x00c0  */
            /* JADX WARNING: Removed duplicated region for block: B:50:0x00c2  */
            /* JADX WARNING: Removed duplicated region for block: B:62:0x0114  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Boolean doInBackground(java.lang.Void... r9) {
                /*
                // Method dump skipped, instructions count: 382
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.FileDownloadTaskHelper.Companion.FileDownloadTask.doInBackground(java.lang.Void[]):java.lang.Boolean");
            }

            @DexIgnore
            public void onPostExecute(Boolean bool) {
                super.onPostExecute((FileDownloadTask) bool);
                if (bool == null) {
                    pq7.i();
                    throw null;
                } else if (bool.booleanValue()) {
                    OnDownloadFinishListener onDownloadFinishListener = this.listener;
                    if (onDownloadFinishListener != null) {
                        onDownloadFinishListener.onDownloadSuccess(this.zipFilePath, this.destinationUnzipPath);
                    }
                } else {
                    OnDownloadFinishListener onDownloadFinishListener2 = this.listener;
                    if (onDownloadFinishListener2 != null) {
                        onDownloadFinishListener2.onDownloadFail(this.zipFilePath, this.destinationUnzipPath, this.objectResponse);
                    }
                }
            }
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return FileDownloadTaskHelper.TAG;
        }

        @DexIgnore
        public final FileDownloadTaskHelper newInstance() {
            return new FileDownloadTaskHelper();
        }
    }

    @DexIgnore
    public interface OnDownloadFinishListener {
        @DexIgnore
        void onDownloadFail(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onDownloadSuccess(String str, String str2);
    }

    @DexIgnore
    public static final FileDownloadTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    public final void execute() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("execute() called with serialNumber = [");
        AssetsDeviceResponse assetsDeviceResponse = this.response;
        if (assetsDeviceResponse != null) {
            Metadata metadata = assetsDeviceResponse.getMetadata();
            sb.append(metadata != null ? metadata.getSerialNumber() : null);
            sb.append("], feature = [");
            AssetsDeviceResponse assetsDeviceResponse2 = this.response;
            if (assetsDeviceResponse2 != null) {
                Metadata metadata2 = assetsDeviceResponse2.getMetadata();
                sb.append(metadata2 != null ? metadata2.getFeature() : null);
                sb.append("]");
                local.d(str, sb.toString());
                String str2 = this.zipFilePath;
                if (str2 != null) {
                    String str3 = this.destinationUnzipPath;
                    if (str3 != null) {
                        AssetsDeviceResponse assetsDeviceResponse3 = this.response;
                        if (assetsDeviceResponse3 != null) {
                            new Companion.FileDownloadTask(str2, str3, assetsDeviceResponse3, this.listener).execute(new Void[0]);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void init(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        pq7.c(str, "zipFilePath");
        pq7.c(str2, "destinationUnzipPath");
        pq7.c(assetsDeviceResponse, "response");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.response = assetsDeviceResponse;
    }

    @DexIgnore
    public final void setOnDownloadFinishListener(OnDownloadFinishListener onDownloadFinishListener) {
        pq7.c(onDownloadFinishListener, "listener");
        this.listener = onDownloadFinishListener;
    }
}
