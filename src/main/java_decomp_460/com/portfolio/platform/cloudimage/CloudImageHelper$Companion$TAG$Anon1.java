package com.portfolio.platform.cloudimage;

import com.fossil.ep7;
import com.fossil.er7;
import com.fossil.fs7;
import com.fossil.ms7;
import com.fossil.xq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class CloudImageHelper$Companion$TAG$Anon1 extends xq7 {
    @DexIgnore
    public static /* final */ ms7 INSTANCE; // = new CloudImageHelper$Companion$TAG$Anon1();

    @DexIgnore
    @Override // com.fossil.xq7
    public Object get(Object obj) {
        return ep7.a((CloudImageHelper) obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public fs7 getOwner() {
        return er7.c(ep7.class, "app_fossilRelease");
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
