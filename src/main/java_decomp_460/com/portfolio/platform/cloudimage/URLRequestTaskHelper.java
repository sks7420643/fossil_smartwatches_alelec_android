package com.portfolio.platform.cloudimage;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class URLRequestTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + URLRequestTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public String feature;
    @DexIgnore
    public OnNextTaskListener listener;
    @DexIgnore
    public ApiServiceV2 mApiService;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return URLRequestTaskHelper.TAG;
        }

        @DexIgnore
        public final URLRequestTaskHelper newInstance() {
            return new URLRequestTaskHelper();
        }
    }

    @DexIgnore
    public interface OnNextTaskListener {
        @DexIgnore
        void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        Object onGetDeviceAssetFailed();  // void declaration
    }

    @DexIgnore
    public URLRequestTaskHelper() {
        PortfolioApp.h0.c().M().p(this);
    }

    @DexIgnore
    public static /* synthetic */ void init$default(URLRequestTaskHelper uRLRequestTaskHelper, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        uRLRequestTaskHelper.init(str, str2, str3, str4, str5, (i & 32) != 0 ? "" : str6);
    }

    @DexIgnore
    public static final URLRequestTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object execute(com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 488
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.URLRequestTaskHelper.execute(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final String getDestinationUnzipPath$app_fossilRelease() {
        return this.destinationUnzipPath;
    }

    @DexIgnore
    public final String getFastPairId$app_fossilRelease() {
        return this.fastPairId;
    }

    @DexIgnore
    public final String getFeature$app_fossilRelease() {
        return this.feature;
    }

    @DexIgnore
    public final OnNextTaskListener getListener$app_fossilRelease() {
        return this.listener;
    }

    @DexIgnore
    public final ApiServiceV2 getMApiService() {
        ApiServiceV2 apiServiceV2 = this.mApiService;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        pq7.n("mApiService");
        throw null;
    }

    @DexIgnore
    public final String getSerialNumber$app_fossilRelease() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getZipFilePath$app_fossilRelease() {
        return this.zipFilePath;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4, String str5, String str6) {
        pq7.c(str, "zipFilePath");
        pq7.c(str2, "destinationUnzipPath");
        pq7.c(str3, "serialNumber");
        pq7.c(str4, "feature");
        pq7.c(str5, "resolution");
        pq7.c(str6, "fastPairId");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.serialNumber = str3;
        this.feature = str4;
        this.resolution = str5;
        this.fastPairId = str6;
    }

    @DexIgnore
    public final void setDestinationUnzipPath$app_fossilRelease(String str) {
        this.destinationUnzipPath = str;
    }

    @DexIgnore
    public final void setFastPairId$app_fossilRelease(String str) {
        this.fastPairId = str;
    }

    @DexIgnore
    public final void setFeature$app_fossilRelease(String str) {
        this.feature = str;
    }

    @DexIgnore
    public final void setListener$app_fossilRelease(OnNextTaskListener onNextTaskListener) {
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setMApiService(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "<set-?>");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void setOnNextTaskListener(OnNextTaskListener onNextTaskListener) {
        pq7.c(onNextTaskListener, "listener");
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setSerialNumber$app_fossilRelease(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setZipFilePath$app_fossilRelease(String str) {
        this.zipFilePath = str;
    }
}
