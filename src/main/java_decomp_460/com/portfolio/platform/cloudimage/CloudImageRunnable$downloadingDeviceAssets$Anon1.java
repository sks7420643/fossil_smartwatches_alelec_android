package com.portfolio.platform.cloudimage;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1", f = "CloudImageRunnable.kt", l = {75}, m = "invokeSuspend")
public final class CloudImageRunnable$downloadingDeviceAssets$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public /* final */ /* synthetic */ String $feature;
    @DexIgnore
    public /* final */ /* synthetic */ String $zipFilePath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$downloadingDeviceAssets$Anon1(CloudImageRunnable cloudImageRunnable, String str, String str2, String str3, qn7 qn7) {
        super(2, qn7);
        this.this$0 = cloudImageRunnable;
        this.$zipFilePath = str;
        this.$destinationUnzipPath = str2;
        this.$feature = str3;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        CloudImageRunnable$downloadingDeviceAssets$Anon1 cloudImageRunnable$downloadingDeviceAssets$Anon1 = new CloudImageRunnable$downloadingDeviceAssets$Anon1(this.this$0, this.$zipFilePath, this.$destinationUnzipPath, this.$feature, qn7);
        cloudImageRunnable$downloadingDeviceAssets$Anon1.p$ = (iv7) obj;
        return cloudImageRunnable$downloadingDeviceAssets$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((CloudImageRunnable$downloadingDeviceAssets$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            URLRequestTaskHelper uRLRequestTaskHelper = this.this$0.prepareURLRequestTask();
            URLRequestTaskHelper.init$default(uRLRequestTaskHelper, this.$zipFilePath, this.$destinationUnzipPath, CloudImageRunnable.access$getSerialNumber$p(this.this$0), this.$feature, CloudImageRunnable.access$getResolution$p(this.this$0), null, 32, null);
            this.L$0 = iv7;
            this.L$1 = uRLRequestTaskHelper;
            this.label = 1;
            if (uRLRequestTaskHelper.execute(this) == d) {
                return d;
            }
        } else if (i == 1) {
            URLRequestTaskHelper uRLRequestTaskHelper2 = (URLRequestTaskHelper) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
