package com.portfolio.platform.response.sleep;

import com.fossil.oh5;
import com.fossil.pq7;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.google.gson.Gson;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionParse {
    @DexIgnore
    public /* final */ DateTime bookmarkTime;
    @DexIgnore
    public /* final */ DateTime createdAt;
    @DexIgnore
    public /* final */ Date date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ DateTime editedEndTime;
    @DexIgnore
    public /* final */ Integer editedSleepMinutes;
    @DexIgnore
    public /* final */ List<Integer> editedSleepStateDistInMinute;
    @DexIgnore
    public /* final */ DateTime editedStartTime;
    @DexIgnore
    public /* final */ SleepSessionHeartRate heartRate;
    @DexIgnore
    public /* final */ DateTime realEndTime;
    @DexIgnore
    public /* final */ int realSleepMinutes;
    @DexIgnore
    public /* final */ List<Integer> realSleepStateDistInMinute;
    @DexIgnore
    public /* final */ DateTime realStartTime;
    @DexIgnore
    public /* final */ double sleepQuality;
    @DexIgnore
    public /* final */ List<int[]> sleepStates;
    @DexIgnore
    public /* final */ String source;
    @DexIgnore
    public /* final */ DateTime syncTime;
    @DexIgnore
    public /* final */ int timezoneOffset;
    @DexIgnore
    public /* final */ DateTime updatedAt;

    @DexIgnore
    public SleepSessionParse(DateTime dateTime, DateTime dateTime2, Date date2, int i, String str, String str2, DateTime dateTime3, DateTime dateTime4, DateTime dateTime5, DateTime dateTime6, int i2, List<Integer> list, DateTime dateTime7, DateTime dateTime8, Integer num, List<Integer> list2, SleepSessionHeartRate sleepSessionHeartRate, double d, List<int[]> list3) {
        pq7.c(dateTime, "createdAt");
        pq7.c(dateTime2, "updatedAt");
        pq7.c(date2, "date");
        pq7.c(str, "source");
        pq7.c(dateTime5, MFSleepSession.COLUMN_REAL_START_TIME);
        pq7.c(dateTime6, MFSleepSession.COLUMN_REAL_END_TIME);
        pq7.c(list, MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
        pq7.c(list3, MFSleepSession.COLUMN_SLEEP_STATES);
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.date = date2;
        this.timezoneOffset = i;
        this.source = str;
        this.deviceSerialNumber = str2;
        this.syncTime = dateTime3;
        this.bookmarkTime = dateTime4;
        this.realStartTime = dateTime5;
        this.realEndTime = dateTime6;
        this.realSleepMinutes = i2;
        this.realSleepStateDistInMinute = list;
        this.editedStartTime = dateTime7;
        this.editedEndTime = dateTime8;
        this.editedSleepMinutes = num;
        this.editedSleepStateDistInMinute = list2;
        this.heartRate = sleepSessionHeartRate;
        this.sleepQuality = d;
        this.sleepStates = list3;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.room.sleep.MFSleepSession getMfSleepSessionBySleepSessionParse() {
        List<Integer> list = this.editedSleepStateDistInMinute;
        Integer num = null;
        SleepDistribution sleepDistribution = list != null ? new SleepDistribution(list) : null;
        SleepDistribution sleepDistribution2 = new SleepDistribution(this.realSleepStateDistInMinute);
        ArrayList arrayList = new ArrayList();
        int size = this.sleepStates.size();
        for (int i = 0; i < size; i++) {
            int[] iArr = this.sleepStates.get(i);
            if (iArr.length > 1) {
                arrayList.add(new WrapperSleepStateChange(iArr[0], (long) iArr[1]));
            }
        }
        String t = new Gson().t(arrayList);
        DateTime dateTime = this.editedStartTime;
        Integer valueOf = dateTime != null ? Integer.valueOf((int) (dateTime.getMillis() / ((long) 1000))) : null;
        DateTime dateTime2 = this.editedEndTime;
        Integer valueOf2 = dateTime2 != null ? Integer.valueOf((int) (dateTime2.getMillis() / ((long) 1000))) : null;
        DateTime dateTime3 = this.bookmarkTime;
        Long valueOf3 = dateTime3 != null ? Long.valueOf(dateTime3.getMillis() / ((long) 1000)) : null;
        DateTime dateTime4 = this.syncTime;
        Long valueOf4 = dateTime4 != null ? Long.valueOf(dateTime4.getMillis() / ((long) 1000)) : null;
        long time = this.date.getTime();
        Date date2 = this.date;
        int i2 = this.timezoneOffset;
        String str = this.deviceSerialNumber;
        Integer valueOf5 = valueOf4 != null ? Integer.valueOf((int) valueOf4.longValue()) : null;
        if (valueOf3 != null) {
            num = Integer.valueOf((int) valueOf3.longValue());
        }
        long j = (long) 1000;
        int i3 = this.realSleepMinutes;
        Integer num2 = this.editedSleepMinutes;
        pq7.b(t, "sleepStatesGson");
        return new com.portfolio.platform.data.model.room.sleep.MFSleepSession(time, date2, i2, str, valueOf5, num, this.sleepQuality, oh5.fromString(this.source).ordinal(), (int) (this.realStartTime.getMillis() / j), (int) (this.realEndTime.getMillis() / j), i3, sleepDistribution2, valueOf, valueOf2, num2, sleepDistribution, t, this.heartRate, this.createdAt, this.updatedAt);
    }
}
