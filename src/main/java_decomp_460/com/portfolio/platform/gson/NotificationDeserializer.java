package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.dt4;
import com.fossil.gj4;
import com.fossil.lk5;
import com.fossil.lt4;
import com.fossil.pq7;
import com.fossil.ss4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDeserializer implements dj4<dt4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ss4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<lt4> {
    }

    @DexIgnore
    /* renamed from: a */
    public dt4 deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        ss4 ss4;
        lt4 lt4;
        boolean z;
        int i;
        int i2;
        boolean z2;
        if (jsonElement == null) {
            return null;
        }
        gj4 d = jsonElement.d();
        JsonElement p = d.p("id");
        pq7.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
        String f = p.f();
        JsonElement p2 = d.p("createdAt");
        String f2 = p2 != null ? p2.f() : null;
        Date date = new Date();
        if (f2 != null) {
            date = lk5.q0(f2);
            pq7.b(date, "DateHelper.parseJodaTime(dateStr)");
        }
        JsonElement p3 = d.p("message");
        pq7.b(p3, "jsonObject.get(BCConst.NOTIFICATION_MESSAGE_KEY)");
        gj4 d2 = p3.d();
        JsonElement p4 = d2.p("titleLocKey");
        pq7.b(p4, "message.get(BCConst.NOTI\u2026ON_TITLE_LOC_MESSAGE_KEY)");
        String f3 = p4.f();
        pq7.b(f3, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement p5 = d2.p("bodyLocKey");
        pq7.b(p5, "message.get(BCConst.NOTI\u2026ION_BODY_LOC_MESSAGE_KEY)");
        String f4 = p5.f();
        pq7.b(f4, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement p6 = d2.p("payload");
        pq7.b(p6, "message.get(BCConst.NOTIFICATION_PAYLOAD_KEY)");
        gj4 d3 = p6.d();
        JsonElement p7 = d3.p("challenge");
        String f5 = p7 != null ? p7.f() : null;
        try {
            ss4 = (ss4) new Gson().l(f5, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String simpleName = NotificationDeserializer.class.getSimpleName();
            pq7.b(simpleName, "this::class.java.simpleName");
            local.e(simpleName, "exception when parse challengeData: " + f5);
            e.printStackTrace();
            ss4 = null;
        }
        JsonElement p8 = d3.p("profile");
        String f6 = p8 != null ? p8.f() : null;
        try {
            lt4 = (lt4) new Gson().l(f6, new b().getType());
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String simpleName2 = NotificationDeserializer.class.getSimpleName();
            pq7.b(simpleName2, "this::class.java.simpleName");
            local2.e(simpleName2, "exception when parse profileData: " + f6);
            e2.printStackTrace();
            lt4 = null;
        }
        JsonElement p9 = d3.p("confirm");
        String f7 = p9 != null ? p9.f() : null;
        if (f7 != null) {
            try {
                z2 = Boolean.parseBoolean(f7);
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String simpleName3 = NotificationDeserializer.class.getSimpleName();
                pq7.b(simpleName3, "this::class.java.simpleName");
                local3.e(simpleName3, "exception when parse confirmChallenge: " + f7);
                z2 = false;
            }
            z = z2;
        } else {
            z = false;
        }
        JsonElement p10 = d3.p("rank");
        String f8 = p10 != null ? p10.f() : null;
        if (f8 != null) {
            try {
                i2 = Integer.parseInt(f8);
            } catch (Exception e4) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String simpleName4 = NotificationDeserializer.class.getSimpleName();
                pq7.b(simpleName4, "this::class.java.simpleName");
                local4.e(simpleName4, "exception when parse rank: " + f8);
                i2 = 0;
            }
            i = i2;
        } else {
            i = 0;
        }
        JsonElement p11 = d2.p("reachGoalAt");
        String f9 = p11 != null ? p11.f() : null;
        Date q0 = f9 != null ? lk5.q0(f9) : null;
        pq7.b(f, "id");
        return new dt4(f, f3, f4, date, ss4, lt4, z, i, q0);
    }
}
