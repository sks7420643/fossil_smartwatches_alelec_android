package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.gj4;
import com.fossil.jj5;
import com.fossil.lk5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDeserializer implements dj4<HybridPreset> {
    @DexIgnore
    /* renamed from: a */
    public HybridPreset deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        String str;
        String str2;
        String str3;
        String w0;
        String str4;
        if (jsonElement != null) {
            gj4 d = jsonElement.d();
            JsonElement p = d.p("name");
            pq7.b(p, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = p.f();
            JsonElement p2 = d.p("id");
            pq7.b(p2, "jsonObject.get(\"id\")");
            String f2 = p2.f();
            if (d.s("serialNumber")) {
                JsonElement p3 = d.p("serialNumber");
                pq7.b(p3, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = p3.f();
            } else {
                str = "";
            }
            JsonElement p4 = d.p("isActive");
            pq7.b(p4, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a2 = p4.a();
            JsonElement p5 = d.p("updatedAt");
            pq7.b(p5, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = p5.f();
            JsonElement p6 = d.p("createdAt");
            pq7.b(p6, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = p6.f();
            ArrayList arrayList = new ArrayList();
            if (cj4 != null) {
                Iterator<JsonElement> it = d.q("buttons").iterator();
                while (it.hasNext()) {
                    JsonElement next = it.next();
                    pq7.b(next, "item");
                    gj4 d2 = next.d();
                    if (d2.s("buttonPosition")) {
                        JsonElement p7 = d2.p("buttonPosition");
                        pq7.b(p7, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = p7.f();
                    } else {
                        str2 = "";
                    }
                    if (d2.s("appId")) {
                        JsonElement p8 = d2.p("appId");
                        pq7.b(p8, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str3 = p8.f();
                    } else {
                        str3 = "";
                    }
                    if (d2.s("localUpdatedAt")) {
                        JsonElement p9 = d2.p("localUpdatedAt");
                        pq7.b(p9, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        w0 = p9.f();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        pq7.b(instance, "Calendar.getInstance()");
                        w0 = lk5.w0(instance.getTime());
                    }
                    if (d2.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p10 = d2.p(Constants.USER_SETTING);
                            pq7.b(p10, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str4 = jj5.a(p10.d());
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("HybridPresetDeserializer", "Exception when parse json string");
                        }
                        pq7.b(str2, "position");
                        pq7.b(str3, "appId");
                        pq7.b(w0, "localUpdatedAt");
                        arrayList.add(new HybridPresetAppSetting(str2, str3, w0, str4));
                    }
                    str4 = "";
                    pq7.b(str2, "position");
                    pq7.b(str3, "appId");
                    pq7.b(w0, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str3, w0, str4));
                }
            }
            pq7.b(f2, "id");
            pq7.b(str, "serialNumber");
            HybridPreset hybridPreset = new HybridPreset(f2, f, str, arrayList, a2);
            hybridPreset.setCreatedAt(f4);
            hybridPreset.setUpdatedAt(f3);
            return hybridPreset;
        }
        pq7.i();
        throw null;
    }
}
