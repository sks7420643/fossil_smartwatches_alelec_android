package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.gj4;
import com.fossil.jj5;
import com.fossil.lk5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDeserializer implements dj4<DianaPreset> {
    @DexIgnore
    /* renamed from: a */
    public DianaPreset deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String w0;
        String str6;
        String str7;
        String str8;
        String str9;
        if (jsonElement != null) {
            gj4 d = jsonElement.d();
            String str10 = "id";
            JsonElement p = d.p("id");
            pq7.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
            String f = p.f();
            JsonElement p2 = d.p("name");
            pq7.b(p2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f2 = p2.f();
            JsonElement p3 = d.p("updatedAt");
            pq7.b(p3, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = p3.f();
            JsonElement p4 = d.p("createdAt");
            pq7.b(p4, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = p4.f();
            JsonElement p5 = d.p("isActive");
            pq7.b(p5, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a2 = p5.a();
            String str11 = "serialNumber";
            JsonElement p6 = d.p("serialNumber");
            pq7.b(p6, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
            String f5 = p6.f();
            String str12 = "watchFaceId";
            JsonElement p7 = d.p("watchFaceId");
            pq7.b(p7, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f6 = p7.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (cj4 != null) {
                Iterator<JsonElement> it = d.q("buttons").iterator();
                while (true) {
                    String str13 = "";
                    if (!it.hasNext()) {
                        break;
                    }
                    JsonElement next = it.next();
                    pq7.b(next, "item");
                    gj4 d2 = next.d();
                    if (d2.s("buttonPosition")) {
                        JsonElement p8 = d2.p("buttonPosition");
                        pq7.b(p8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str7 = p8.f();
                    } else {
                        str7 = "";
                    }
                    if (d2.s("appId")) {
                        JsonElement p9 = d2.p("appId");
                        pq7.b(p9, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str8 = p9.f();
                    } else {
                        str8 = "";
                    }
                    if (d2.s("localUpdatedAt")) {
                        JsonElement p10 = d2.p("localUpdatedAt");
                        pq7.b(p10, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str9 = p10.f();
                    } else {
                        str9 = "";
                    }
                    if (d2.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p11 = d2.p(Constants.USER_SETTING);
                            pq7.b(p11, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str13 = jj5.a(p11.d());
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    pq7.b(str7, "position");
                    pq7.b(str8, "appId");
                    pq7.b(str9, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str7, str8, str9, str13));
                }
                Iterator<JsonElement> it2 = d.q("complications").iterator();
                while (it2.hasNext()) {
                    JsonElement next2 = it2.next();
                    pq7.b(next2, "item");
                    gj4 d3 = next2.d();
                    if (d3.s("complicationPosition")) {
                        JsonElement p12 = d3.p("complicationPosition");
                        pq7.b(p12, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str4 = p12.f();
                    } else {
                        str4 = "";
                    }
                    if (d3.s("appId")) {
                        JsonElement p13 = d3.p("appId");
                        pq7.b(p13, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str5 = p13.f();
                    } else {
                        str5 = "";
                    }
                    if (d3.s("localUpdatedAt")) {
                        JsonElement p14 = d3.p("localUpdatedAt");
                        pq7.b(p14, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        w0 = p14.f();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        pq7.b(instance, "Calendar.getInstance()");
                        w0 = lk5.w0(instance.getTime());
                    }
                    if (d3.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p15 = d3.p(Constants.USER_SETTING);
                            pq7.b(p15, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str6 = jj5.a(p15.d());
                        } catch (Exception e2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        pq7.b(str4, "position");
                        pq7.b(str5, "appId");
                        pq7.b(w0, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                    }
                    str6 = "";
                    pq7.b(str4, "position");
                    pq7.b(str5, "appId");
                    pq7.b(w0, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                }
                str = f2;
                str3 = "name";
                z = a2;
                str2 = f5;
            } else {
                str10 = "id";
                str3 = "name";
                str11 = "serialNumber";
                str = f2;
                str12 = "watchFaceId";
                z = a2;
                str2 = f5;
            }
            pq7.b(f, str10);
            pq7.b(str2, str11);
            pq7.b(str, str3);
            pq7.b(f6, str12);
            DianaPreset dianaPreset = new DianaPreset(f, str2, str, z, arrayList2, arrayList, f6);
            dianaPreset.setCreatedAt(f4);
            dianaPreset.setUpdatedAt(f3);
            return dianaPreset;
        }
        pq7.i();
        throw null;
    }
}
