package com.portfolio.platform.gson;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.gj4;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDeserializer implements dj4<Theme> {
    @DexIgnore
    /* renamed from: a */
    public Theme deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        gj4 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        if (d.s("id")) {
            JsonElement p = d.p("id");
            pq7.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
            str = p.f();
        } else {
            str = "";
        }
        if (d.s("name")) {
            JsonElement p2 = d.p("name");
            pq7.b(p2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            str2 = p2.f();
        } else {
            str2 = "";
        }
        ArrayList arrayList = new ArrayList();
        if (cj4 != null && d.s("styles")) {
            Iterator<JsonElement> it = d.q("styles").iterator();
            while (it.hasNext()) {
                JsonElement next = it.next();
                pq7.b(next, "item");
                gj4 d2 = next.d();
                if (d2.s("key")) {
                    JsonElement p3 = d2.p("key");
                    pq7.b(p3, "itemJsonObject.get(Constants.JSON_KEY_KEY)");
                    str3 = p3.f();
                } else {
                    str3 = "";
                }
                if (d2.s("type")) {
                    JsonElement p4 = d2.p("type");
                    pq7.b(p4, "itemJsonObject.get(Constants.JSON_KEY_TYPE)");
                    str4 = p4.f();
                } else {
                    str4 = "";
                }
                if (d2.s("value")) {
                    JsonElement p5 = d2.p("value");
                    if (p5 instanceof gj4) {
                        gj4 gj4 = (gj4) p5;
                        if (gj4.s("fileName")) {
                            JsonElement p6 = gj4.p("fileName");
                            pq7.b(p6, "valueJsonElement.get(Constants.JSON_KEY_FILE_NAME)");
                            str5 = p6.f();
                            pq7.b(str5, "valueJsonElement.get(Con\u2026N_KEY_FILE_NAME).asString");
                        }
                    } else {
                        pq7.b(p5, "valueJsonElement");
                        str5 = p5.f();
                        pq7.b(str5, "valueJsonElement.asString");
                    }
                    pq7.b(str3, "key");
                    pq7.b(str4, "type");
                    arrayList.add(new Style(str3, str4, str5));
                }
                str5 = "";
                pq7.b(str3, "key");
                pq7.b(str4, "type");
                arrayList.add(new Style(str3, str4, str5));
            }
        }
        pq7.b(str, "id");
        pq7.b(str2, "name");
        return new Theme(str, str2, arrayList);
    }
}
