package com.portfolio.platform.gson;

import android.text.TextUtils;
import com.fossil.bj4;
import com.fossil.fj4;
import com.fossil.gj4;
import com.fossil.ij4;
import com.fossil.il7;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetWatchAppSettingSerializer implements lj4<List<? extends DianaPresetWatchAppSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<DianaPresetWatchAppSetting> list, Type type, kj4 kj4) {
        pq7.c(list, "src");
        bj4 bj4 = new bj4();
        ij4 ij4 = new ij4();
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            String component1 = dianaPresetWatchAppSetting.component1();
            String component2 = dianaPresetWatchAppSetting.component2();
            String component3 = dianaPresetWatchAppSetting.component3();
            String component4 = dianaPresetWatchAppSetting.component4();
            gj4 gj4 = new gj4();
            gj4.n("buttonPosition", component1);
            gj4.n("appId", component2);
            gj4.n("localUpdatedAt", component3);
            if (!TextUtils.isEmpty(component4)) {
                try {
                    JsonElement c = ij4.c(component4);
                    if (c != null) {
                        gj4.k(Constants.USER_SETTING, (gj4) c);
                    } else {
                        throw new il7("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception e) {
                    gj4.k(Constants.USER_SETTING, fj4.f1138a);
                }
            } else {
                gj4.k(Constants.USER_SETTING, fj4.f1138a);
            }
            bj4.k(gj4);
        }
        return bj4;
    }
}
