package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDate implements dj4<Date> {
    @DexIgnore
    /* renamed from: a */
    public Date deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        pq7.c(jsonElement, "json");
        pq7.c(type, "typeOfT");
        pq7.c(cj4, "context");
        String f = jsonElement.f();
        pq7.b(f, "dateAsString");
        if (f.length() == 0) {
            return new Date(0);
        }
        try {
            Date date = lk5.R(DateTimeZone.getDefault(), f).toDate();
            pq7.b(date, "DateHelper.getServerDate\u2026), dateAsString).toDate()");
            return date;
        } catch (Exception e) {
            try {
                SimpleDateFormat simpleDateFormat = lk5.f2210a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(jsonElement.f());
                    if (parse != null) {
                        return parse;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("GsonConvertDate", "deserialize - json=" + jsonElement.f() + ", e=" + e2);
                e2.printStackTrace();
                return new Date(0);
            }
        }
    }
}
