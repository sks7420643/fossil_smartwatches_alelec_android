package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.fossil.lk5;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GsonConverterShortDateTime implements dj4<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public DateTime deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return lk5.M(f);
    }
}
