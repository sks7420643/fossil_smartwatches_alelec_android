package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.jj4;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.tl7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonISOConvertDateTime implements dj4<Date>, lj4<Date> {
    @DexIgnore
    /* renamed from: a */
    public Date deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        String f;
        if (!(jsonElement == null || (f = jsonElement.f()) == null)) {
            try {
                SimpleDateFormat simpleDateFormat = lk5.p.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    SimpleDateFormat simpleDateFormat2 = lk5.n.get();
                    if (simpleDateFormat2 != null) {
                        SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                        SimpleDateFormat simpleDateFormat4 = lk5.n.get();
                        if (simpleDateFormat4 != null) {
                            Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                            pq7.b(parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                            return parse2;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e.printStackTrace();
                sb.append(tl7.f3441a);
                local.e("GsonISOConvertDateTime", sb.toString());
            }
        }
        return new Date(0);
    }

    @DexIgnore
    /* renamed from: b */
    public JsonElement serialize(Date date, Type type, kj4 kj4) {
        String format;
        if (date == null) {
            format = "";
        } else {
            SimpleDateFormat simpleDateFormat = lk5.p.get();
            if (simpleDateFormat != null) {
                format = simpleDateFormat.format(date);
                pq7.b(format, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                pq7.i();
                throw null;
            }
        }
        return new jj4(format);
    }
}
