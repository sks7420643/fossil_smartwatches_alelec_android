package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.jj4;
import com.fossil.kj4;
import com.fossil.lj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.tl7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConverterShortDate implements dj4<Date>, lj4<Date> {
    @DexIgnore
    /* renamed from: a */
    public Date deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        pq7.c(jsonElement, "json");
        pq7.c(type, "typeOfT");
        pq7.c(cj4, "context");
        String f = jsonElement.f();
        if (f != null) {
            try {
                SimpleDateFormat simpleDateFormat = lk5.f2210a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    if (parse != null) {
                        return parse;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(f);
                sb.append(", e=");
                e.printStackTrace();
                sb.append(tl7.f3441a);
                local.e("GsonConverterShortDate", sb.toString());
            }
        }
        return new Date(0);
    }

    @DexIgnore
    /* renamed from: b */
    public JsonElement serialize(Date date, Type type, kj4 kj4) {
        String format;
        pq7.c(type, "typeOfSrc");
        pq7.c(kj4, "context");
        if (date == null) {
            format = "";
        } else {
            SimpleDateFormat simpleDateFormat = lk5.f2210a.get();
            if (simpleDateFormat != null) {
                format = simpleDateFormat.format(date);
                pq7.b(format, "DateHelper.SHORT_DATE_FO\u2026ATTER.get()!!.format(src)");
            } else {
                pq7.i();
                throw null;
            }
        }
        return new jj4(format);
    }
}
