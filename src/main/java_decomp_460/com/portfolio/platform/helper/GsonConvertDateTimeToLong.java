package com.portfolio.platform.helper;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.tl7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTimeToLong implements dj4<Long> {
    @DexIgnore
    /* renamed from: a */
    public Long deserialize(JsonElement jsonElement, Type type, cj4 cj4) {
        long j = 0;
        pq7.c(jsonElement, "json");
        String f = jsonElement.f();
        pq7.b(f, "dateAsString");
        if (f.length() == 0) {
            return 0L;
        }
        try {
            DateTime S = lk5.S(f);
            pq7.b(S, "DateHelper.getServerDate\u2026ffsetParsed(dateAsString)");
            j = S.getMillis();
        } catch (Exception e) {
            try {
                DateTime M = lk5.M(jsonElement.f());
                pq7.b(M, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
                j = M.getMillis();
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e2.printStackTrace();
                sb.append(tl7.f3441a);
                local.e("GsonConvertDateTimeToLong", sb.toString());
            }
        }
        return Long.valueOf(j);
    }
}
