package com.portfolio.platform.watchface.gallery;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import com.fossil.ao5;
import com.fossil.b77;
import com.fossil.ck5;
import com.fossil.ds0;
import com.fossil.ec5;
import com.fossil.ej5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.h37;
import com.fossil.hq4;
import com.fossil.hr7;
import com.fossil.il7;
import com.fossil.iv7;
import com.fossil.jn5;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.ls0;
import com.fossil.nk5;
import com.fossil.po4;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qv5;
import com.fossil.rj4;
import com.fossil.s37;
import com.fossil.t47;
import com.fossil.th5;
import com.fossil.tl7;
import com.fossil.ts0;
import com.fossil.uh5;
import com.fossil.vp7;
import com.fossil.wt7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.fossil.zn5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.zendesk.sdk.network.impl.DeviceInfo;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceGalleryFragment extends qv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public WatchFaceGalleryViewModel i;
    @DexIgnore
    public ec5 j;
    @DexIgnore
    public Uri k;
    @DexIgnore
    public /* final */ h l; // = new h(this);
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Body {
        @DexIgnore
        public /* final */ String checksum;
        @DexIgnore
        public /* final */ String date;
        @DexIgnore
        public /* final */ String downloadURL;
        @DexIgnore
        public /* final */ String host;
        @DexIgnore
        public /* final */ String id;
        @DexIgnore
        public /* final */ String loadItems;
        @DexIgnore
        public /* final */ String orderId;
        @DexIgnore
        public /* final */ String payload;
        @DexIgnore
        public /* final */ String requestLine;
        @DexIgnore
        @rj4("type")
        public /* final */ String type;
        @DexIgnore
        public /* final */ String url;
        @DexIgnore
        public /* final */ String watchFaceId;
        @DexIgnore
        public /* final */ String wf_type;

        @DexIgnore
        public Body(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13) {
            this.id = str;
            this.type = str2;
            this.downloadURL = str3;
            this.checksum = str4;
            this.loadItems = str5;
            this.url = str6;
            this.payload = str7;
            this.watchFaceId = str8;
            this.orderId = str9;
            this.wf_type = str10;
            this.host = str11;
            this.date = str12;
            this.requestLine = str13;
        }

        @DexIgnore
        public static /* synthetic */ Body copy$default(Body body, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, int i, Object obj) {
            return body.copy((i & 1) != 0 ? body.id : str, (i & 2) != 0 ? body.type : str2, (i & 4) != 0 ? body.downloadURL : str3, (i & 8) != 0 ? body.checksum : str4, (i & 16) != 0 ? body.loadItems : str5, (i & 32) != 0 ? body.url : str6, (i & 64) != 0 ? body.payload : str7, (i & 128) != 0 ? body.watchFaceId : str8, (i & 256) != 0 ? body.orderId : str9, (i & 512) != 0 ? body.wf_type : str10, (i & 1024) != 0 ? body.host : str11, (i & 2048) != 0 ? body.date : str12, (i & 4096) != 0 ? body.requestLine : str13);
        }

        @DexIgnore
        public final String component1() {
            return this.id;
        }

        @DexIgnore
        public final String component10() {
            return this.wf_type;
        }

        @DexIgnore
        public final String component11() {
            return this.host;
        }

        @DexIgnore
        public final String component12() {
            return this.date;
        }

        @DexIgnore
        public final String component13() {
            return this.requestLine;
        }

        @DexIgnore
        public final String component2() {
            return this.type;
        }

        @DexIgnore
        public final String component3() {
            return this.downloadURL;
        }

        @DexIgnore
        public final String component4() {
            return this.checksum;
        }

        @DexIgnore
        public final String component5() {
            return this.loadItems;
        }

        @DexIgnore
        public final String component6() {
            return this.url;
        }

        @DexIgnore
        public final String component7() {
            return this.payload;
        }

        @DexIgnore
        public final String component8() {
            return this.watchFaceId;
        }

        @DexIgnore
        public final String component9() {
            return this.orderId;
        }

        @DexIgnore
        public final Body copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13) {
            return new Body(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Body) {
                    Body body = (Body) obj;
                    if (!pq7.a(this.id, body.id) || !pq7.a(this.type, body.type) || !pq7.a(this.downloadURL, body.downloadURL) || !pq7.a(this.checksum, body.checksum) || !pq7.a(this.loadItems, body.loadItems) || !pq7.a(this.url, body.url) || !pq7.a(this.payload, body.payload) || !pq7.a(this.watchFaceId, body.watchFaceId) || !pq7.a(this.orderId, body.orderId) || !pq7.a(this.wf_type, body.wf_type) || !pq7.a(this.host, body.host) || !pq7.a(this.date, body.date) || !pq7.a(this.requestLine, body.requestLine)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getChecksum() {
            return this.checksum;
        }

        @DexIgnore
        public final String getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getDownloadURL() {
            return this.downloadURL;
        }

        @DexIgnore
        public final String getHost() {
            return this.host;
        }

        @DexIgnore
        public final String getId() {
            return this.id;
        }

        @DexIgnore
        public final String getLoadItems() {
            return this.loadItems;
        }

        @DexIgnore
        public final String getOrderId() {
            return this.orderId;
        }

        @DexIgnore
        public final String getPayload() {
            return this.payload;
        }

        @DexIgnore
        public final String getRequestLine() {
            return this.requestLine;
        }

        @DexIgnore
        public final String getType() {
            return this.type;
        }

        @DexIgnore
        public final String getUrl() {
            return this.url;
        }

        @DexIgnore
        public final String getWatchFaceId() {
            return this.watchFaceId;
        }

        @DexIgnore
        public final String getWf_type() {
            return this.wf_type;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.id;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.type;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.downloadURL;
            int hashCode3 = str3 != null ? str3.hashCode() : 0;
            String str4 = this.checksum;
            int hashCode4 = str4 != null ? str4.hashCode() : 0;
            String str5 = this.loadItems;
            int hashCode5 = str5 != null ? str5.hashCode() : 0;
            String str6 = this.url;
            int hashCode6 = str6 != null ? str6.hashCode() : 0;
            String str7 = this.payload;
            int hashCode7 = str7 != null ? str7.hashCode() : 0;
            String str8 = this.watchFaceId;
            int hashCode8 = str8 != null ? str8.hashCode() : 0;
            String str9 = this.orderId;
            int hashCode9 = str9 != null ? str9.hashCode() : 0;
            String str10 = this.wf_type;
            int hashCode10 = str10 != null ? str10.hashCode() : 0;
            String str11 = this.host;
            int hashCode11 = str11 != null ? str11.hashCode() : 0;
            String str12 = this.date;
            int hashCode12 = str12 != null ? str12.hashCode() : 0;
            String str13 = this.requestLine;
            if (str13 != null) {
                i = str13.hashCode();
            }
            return (((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Body(id=" + this.id + ", type=" + this.type + ", downloadURL=" + this.downloadURL + ", checksum=" + this.checksum + ", loadItems=" + this.loadItems + ", url=" + this.url + ", payload=" + this.payload + ", watchFaceId=" + this.watchFaceId + ", orderId=" + this.orderId + ", wf_type=" + this.wf_type + ", host=" + this.host + ", date=" + this.date + ", requestLine=" + this.requestLine + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MessageBody {
        @DexIgnore
        public /* final */ String action;
        @DexIgnore
        public /* final */ Body body;

        @DexIgnore
        public MessageBody(String str, Body body2) {
            this.action = str;
            this.body = body2;
        }

        @DexIgnore
        public static /* synthetic */ MessageBody copy$default(MessageBody messageBody, String str, Body body2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = messageBody.action;
            }
            if ((i & 2) != 0) {
                body2 = messageBody.body;
            }
            return messageBody.copy(str, body2);
        }

        @DexIgnore
        public final String component1() {
            return this.action;
        }

        @DexIgnore
        public final Body component2() {
            return this.body;
        }

        @DexIgnore
        public final MessageBody copy(String str, Body body2) {
            return new MessageBody(str, body2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof MessageBody) {
                    MessageBody messageBody = (MessageBody) obj;
                    if (!pq7.a(this.action, messageBody.action) || !pq7.a(this.body, messageBody.body)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getAction() {
            return this.action;
        }

        @DexIgnore
        public final Body getBody() {
            return this.body;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.action;
            int hashCode = str != null ? str.hashCode() : 0;
            Body body2 = this.body;
            if (body2 != null) {
                i = body2.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "MessageBody(action=" + this.action + ", body=" + this.body + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class WebAppInterface {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4771a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ WebAppInterface this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, WebAppInterface webAppInterface) {
                super(2, qn7);
                this.this$0 = webAppInterface;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object u;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(this.this$0.f4771a);
                    this.L$0 = iv7;
                    this.label = 1;
                    u = P6.u(this);
                    if (u == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    u = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                String str = (String) u;
                if (str != null) {
                    WatchFaceGalleryFragment.Z6(this.this$0.f4771a, str, null, 2, null);
                }
                this.this$0.f4771a.a();
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public WebAppInterface(WatchFaceGalleryFragment watchFaceGalleryFragment, Context context) {
            pq7.c(context, "context");
            this.f4771a = watchFaceGalleryFragment;
        }

        @DexIgnore
        @JavascriptInterface
        public final void postMessage(String str) {
            String str2;
            Body body;
            Body body2;
            Body body3;
            pq7.c(str, "messageBody");
            MessageBody messageBody = (MessageBody) new Gson().k(str, MessageBody.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceGalleryFragment.t.a();
            local.d(a2, "postMessage mb " + messageBody + " messageBody " + str);
            String str3 = "";
            if (messageBody == null || (str2 = messageBody.getAction()) == null) {
                str2 = "";
            }
            switch (str2.hashCode()) {
                case -1577635543:
                    if (str2.equals("finishLoading") && messageBody.getBody() != null) {
                        xw7 unused = gu7.d(ds0.a(this.f4771a), null, null, new a(null, this), 3, null);
                        return;
                    }
                    return;
                case -983638536:
                    if (str2.equals("navigateBack")) {
                        FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a(), "navigateBack");
                        this.f4771a.F6();
                        return;
                    }
                    return;
                case -942409627:
                    if (str2.equals("downloadedWatchFace") && (body = messageBody.getBody()) != null) {
                        String watchFaceId = body.getWatchFaceId();
                        if (watchFaceId == null) {
                            watchFaceId = "";
                        }
                        String orderId = body.getOrderId();
                        if (orderId != null) {
                            str3 = orderId;
                        }
                        this.f4771a.X6(watchFaceId, str3);
                        return;
                    }
                    return;
                case -452821854:
                    if (str2.equals("openWatchFace")) {
                        FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a(), "openWatchFace");
                        Body body4 = messageBody.getBody();
                        if (body4 != null) {
                            String id = body4.getId();
                            if (id == null) {
                                id = "";
                            }
                            String type = body4.getType();
                            if (type != null) {
                                str3 = type;
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = WatchFaceGalleryFragment.t.a();
                            local2.d(a3, "openWatchFace id " + id + " type " + str3);
                            this.f4771a.b7(id, str3);
                            return;
                        }
                        return;
                    }
                    return;
                case 105158973:
                    if (str2.equals("navigateScreen")) {
                        FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a(), "previewWatchFace");
                        Body body5 = messageBody.getBody();
                        if (body5 != null) {
                            String id2 = body5.getId();
                            if (id2 == null) {
                                id2 = "";
                            }
                            String type2 = body5.getType();
                            if (type2 == null) {
                                type2 = "";
                            }
                            String downloadURL = body5.getDownloadURL();
                            if (downloadURL == null) {
                                downloadURL = "";
                            }
                            String checksum = body5.getChecksum();
                            if (checksum != null) {
                                str3 = checksum;
                            }
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = WatchFaceGalleryFragment.t.a();
                            local3.d(a4, "previewWatchFace id " + id2 + " downloadURL " + downloadURL + " checksum " + str3);
                            this.f4771a.c7(id2, downloadURL, str3);
                            b77.f401a.c("wf_gallery_preview", id2, type2);
                            return;
                        }
                        return;
                    }
                    return;
                case 901118044:
                    if (str2.equals("sendAnalyticsEvent") && (body2 = messageBody.getBody()) != null) {
                        String payload = body2.getPayload();
                        if (payload != null) {
                            str3 = payload;
                        }
                        ck5.f.g().k("wf_web_event", "payload", str3);
                        return;
                    }
                    return;
                case 1465969200:
                    if (str2.equals("clickDownload")) {
                        FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a(), "clickDownload");
                        Body body6 = messageBody.getBody();
                        if (body6 != null) {
                            String id3 = body6.getId();
                            if (id3 == null) {
                                id3 = "";
                            }
                            String type3 = body6.getType();
                            if (type3 != null) {
                                str3 = type3;
                            }
                            b77.f401a.c("wf_gallery_download", id3, str3);
                            return;
                        }
                        return;
                    }
                    return;
                case 1538149037:
                    if (str2.equals("shareWatchFace") && (body3 = messageBody.getBody()) != null) {
                        String url = body3.getUrl();
                        if (url == null) {
                            url = "";
                        }
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String a5 = WatchFaceGalleryFragment.t.a();
                        local4.d(a5, "shareWatchFace url " + url);
                        String id4 = body3.getId();
                        if (id4 == null) {
                            id4 = "";
                        }
                        String type4 = body3.getType();
                        if (type4 == null) {
                            type4 = str3;
                        }
                        b77.f401a.c("wf_gallery_sharing", id4, type4);
                        this.f4771a.f7(url);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchFaceGalleryFragment.s;
        }

        @DexIgnore
        public final WatchFaceGalleryFragment b() {
            return new WatchFaceGalleryFragment();
        }

        @DexIgnore
        public final WatchFaceGalleryFragment c(Uri uri) {
            pq7.c(uri, "uri");
            Bundle bundle = new Bundle();
            WatchFaceGalleryFragment watchFaceGalleryFragment = new WatchFaceGalleryFragment();
            ej5.a(bundle, "EXTRA_URI", uri);
            watchFaceGalleryFragment.setArguments(bundle);
            return watchFaceGalleryFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$backFromHomePage$1", f = "WatchFaceGalleryFragment.kt", l = {290}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFaceGalleryFragment watchFaceGalleryFragment, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(this.this$0);
                this.L$0 = iv7;
                this.label = 1;
                if (P6.x(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WebView webView = WatchFaceGalleryFragment.O6(this.this$0).e;
            this.this$0.requireActivity().supportFinishAfterTransition();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$backFromQuickView$1", f = "WatchFaceGalleryFragment.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ValueCallback<String> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f4772a;

            @DexIgnore
            public a(c cVar) {
                this.f4772a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onReceiveValue(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchFaceGalleryFragment.t.a();
                local.d(a2, "backFromQuickView page stack length " + str);
                try {
                    pq7.b(str, "length");
                    if (Integer.parseInt(str) == 1) {
                        WatchFaceGalleryFragment watchFaceGalleryFragment = this.f4772a.this$0;
                        hr7 hr7 = hr7.f1520a;
                        String format = String.format("window.location.replace(\"%s\")", Arrays.copyOf(new Object[]{h37.b.a(7)}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        WatchFaceGalleryFragment.Z6(watchFaceGalleryFragment, format, null, 2, null);
                        return;
                    }
                    WatchFaceGalleryFragment.Z6(this.f4772a.this$0, "window.history.back()", null, 2, null);
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = WatchFaceGalleryFragment.t.a();
                    local2.e(a3, "backFromQuickView exception " + e.getMessage());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFaceGalleryFragment watchFaceGalleryFragment, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.Y6("window.history.length", new a(this));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WebChromeClient {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4773a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.f4773a = watchFaceGalleryFragment;
        }

        @DexIgnore
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            if (consoleMessage == null) {
                return true;
            }
            FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a() + " - ChromeClient", consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId() + " level " + consoleMessage.messageLevel());
            if (consoleMessage.messageLevel() != ConsoleMessage.MessageLevel.ERROR) {
                return true;
            }
            this.f4773a.a();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends WebViewClient {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4774a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$buildWebViewClient$1$onPageFinished$1", f = "WatchFaceGalleryFragment.kt", l = {229}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object t;
                WatchFaceGalleryFragment watchFaceGalleryFragment;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchFaceGalleryFragment watchFaceGalleryFragment2 = this.this$0.f4774a;
                    WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(watchFaceGalleryFragment2);
                    this.L$0 = iv7;
                    this.L$1 = watchFaceGalleryFragment2;
                    this.label = 1;
                    t = P6.t(this);
                    if (t == d) {
                        return d;
                    }
                    watchFaceGalleryFragment = watchFaceGalleryFragment2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    watchFaceGalleryFragment = (WatchFaceGalleryFragment) this.L$1;
                    t = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                WatchFaceGalleryFragment.Z6(watchFaceGalleryFragment, (String) t, null, 2, null);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.f4774a = watchFaceGalleryFragment;
        }

        @DexIgnore
        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceGalleryFragment.t.a();
            local.d(a2, "onPageFinished url " + str);
            xw7 unused = gu7.d(ds0.a(this.f4774a), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            super.onReceivedError(webView, webResourceRequest, webResourceError);
            this.f4774a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$downloadWatchFace$1", f = "WatchFaceGalleryFragment.kt", l = {348}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $orderId;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchFaceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WatchFaceGalleryFragment watchFaceGalleryFragment, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
            this.$orderId = str;
            this.$watchFaceId = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$orderId, this.$watchFaceId, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object v;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(this.this$0);
                String str = this.$orderId;
                String str2 = this.$watchFaceId;
                this.L$0 = iv7;
                this.label = 1;
                v = P6.v(str, str2, this);
                if (v == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                v = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WatchFaceGalleryFragment.Z6(this.this$0, (String) v, null, 2, null);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$evaluatePathName$1", f = "WatchFaceGalleryFragment.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ValueCallback<String> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ g f4775a;

            @DexIgnore
            public a(g gVar) {
                this.f4775a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onReceiveValue(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchFaceGalleryFragment.t.a();
                local.d(a2, "evaluatePathName pathName " + str);
                pq7.b(str, "pathName");
                if (wt7.v(str, "quick-view", false, 2, null)) {
                    this.f4775a.this$0.T6();
                } else {
                    this.f4775a.this$0.S6();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(WatchFaceGalleryFragment watchFaceGalleryFragment, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.Y6("window.location.pathname", new a(this));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends zn5 {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$mNetworkCallback$1$onNetworkStateChanged$1", f = "WatchFaceGalleryFragment.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ th5 $networkState;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, th5 th5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = hVar;
                this.$networkState = th5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$networkState, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = WatchFaceGalleryFragment.t.a();
                    local.d(a2, "onNetworkStateChanged networkState " + this.$networkState);
                    if (this.$networkState == th5.RECONNECTED) {
                        WatchFaceGalleryFragment.O6(this.this$0.c).e.reload();
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.c = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.zn5
        public void a(th5 th5) {
            pq7.c(th5, "networkState");
            xw7 unused = gu7.d(ds0.a(this.c), null, null, new a(this, th5, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$onActivityBackPressed$1", f = "WatchFaceGalleryFragment.kt", l = {}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ValueCallback<String> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ i f4776a;

            @DexIgnore
            public a(i iVar) {
                this.f4776a = iVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onReceiveValue(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchFaceGalleryFragment.t.a();
                local.d(a2, "onActivityBackPressed query " + str);
                pq7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
                if (wt7.v(str, "seeAll", false, 2, null)) {
                    WatchFaceGalleryFragment.Z6(this.f4776a.this$0, "clearSeeAll()", null, 2, null);
                } else {
                    this.f4776a.this$0.a7();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(WatchFaceGalleryFragment watchFaceGalleryFragment, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, qn7);
            iVar.p$ = (iv7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                FLogger.INSTANCE.getLocal().d(WatchFaceGalleryFragment.t.a(), "onActivityBackPressed");
                this.this$0.Y6("window.location.search", new a(this));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4777a;

        @DexIgnore
        public j(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.f4777a = watchFaceGalleryFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar.a()) {
                this.f4777a.b();
            }
            if (bVar.b()) {
                this.f4777a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<hq4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4778a;

        @DexIgnore
        public k(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.f4778a = watchFaceGalleryFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.c cVar) {
            if (!cVar.a().isEmpty()) {
                WatchFaceGalleryFragment watchFaceGalleryFragment = this.f4778a;
                Object[] array = cVar.a().toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    watchFaceGalleryFragment.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryFragment f4779a;

        @DexIgnore
        public l(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            this.f4779a = watchFaceGalleryFragment;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            if (this.f4779a.isActive()) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                FragmentActivity requireActivity = this.f4779a.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$openWatchFace$1", f = "WatchFaceGalleryFragment.kt", l = {324}, m = "invokeSuspend")
    public static final class m extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(WatchFaceGalleryFragment watchFaceGalleryFragment, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
            this.$id = str;
            this.$type = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            m mVar = new m(this.this$0, this.$id, this.$type, qn7);
            mVar.p$ = (iv7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((m) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(this.this$0);
                this.L$0 = iv7;
                this.label = 1;
                if (P6.x(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.isActive()) {
                WatchFaceListActivity.a aVar = WatchFaceListActivity.C;
                Context requireContext = this.this$0.requireContext();
                pq7.b(requireContext, "requireContext()");
                String str = this.$id;
                String str2 = this.$type;
                Uri uri = this.this$0.k;
                aVar.c(requireContext, str, str2, !TextUtils.isEmpty(uri != null ? uri.toString() : null));
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment$previewWatchFace$1", f = "WatchFaceGalleryFragment.kt", l = {333}, m = "invokeSuspend")
    public static final class n extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $checksum;
        @DexIgnore
        public /* final */ /* synthetic */ String $downloadUrl;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(WatchFaceGalleryFragment watchFaceGalleryFragment, String str, String str2, String str3, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryFragment;
            this.$id = str;
            this.$downloadUrl = str2;
            this.$checksum = str3;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            n nVar = new n(this.this$0, this.$id, this.$downloadUrl, this.$checksum, qn7);
            nVar.p$ = (iv7) obj;
            return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((n) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (jn5.c(jn5.b, this.this$0.getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
                    WatchFaceGalleryViewModel P6 = WatchFaceGalleryFragment.P6(this.this$0);
                    String str = this.$id;
                    String str2 = this.$downloadUrl;
                    String str3 = this.$checksum;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (P6.w(str, str2, str3, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = WatchFaceGalleryFragment.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceGalleryFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ec5 O6(WatchFaceGalleryFragment watchFaceGalleryFragment) {
        ec5 ec5 = watchFaceGalleryFragment.j;
        if (ec5 != null) {
            return ec5;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceGalleryViewModel P6(WatchFaceGalleryFragment watchFaceGalleryFragment) {
        WatchFaceGalleryViewModel watchFaceGalleryViewModel = watchFaceGalleryFragment.i;
        if (watchFaceGalleryViewModel != null) {
            return watchFaceGalleryViewModel;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void Z6(WatchFaceGalleryFragment watchFaceGalleryFragment, String str, ValueCallback valueCallback, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            valueCallback = null;
        }
        watchFaceGalleryFragment.Y6(str, valueCallback);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        xw7 unused = gu7.d(ds0.a(this), null, null, new i(this, null), 3, null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 1615039588 && str.equals("DIALOG_DEVICE_NOT_ACTIVE") && i2 == 2131363373 && (activity2 = getActivity()) != null) {
                activity2.finish();
            }
        } else if (str.equals("NETWORK_ERROR") && i2 == 2131363373 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void S6() {
        xw7 unused = gu7.d(ds0.a(this), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void T6() {
        xw7 unused = gu7.d(ds0.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final WebChromeClient U6() {
        return new d(this);
    }

    @DexIgnore
    public final WebViewClient V6() {
        return new e(this);
    }

    @DexIgnore
    public final String W6(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null) {
            lastPathSegment = "";
        }
        pq7.b(lastPathSegment, "uri.lastPathSegment ?: \"\"");
        String a2 = h37.b.a(8);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "convertUriToUrl urlWithoutId " + a2);
        if (!(lastPathSegment.length() > 0)) {
            return h37.b.a(7);
        }
        return a2 + '/' + lastPathSegment;
    }

    @DexIgnore
    public final void X6(String str, String str2) {
        pq7.c(str, "watchFaceId");
        pq7.c(str2, "orderId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = s;
        local.d(str3, "downloadWatchFace watchFaceId " + str + " orderId " + str2);
        xw7 unused = gu7.d(ds0.a(this), null, null, new f(this, str2, str, null), 3, null);
    }

    @DexIgnore
    public final void Y6(String str, ValueCallback<String> valueCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "evaluate script " + str);
        ec5 ec5 = this.j;
        if (ec5 != null) {
            ec5.e.evaluateJavascript(str, valueCallback);
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7() {
        xw7 unused = gu7.d(ds0.a(this), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    public final void b7(String str, String str2) {
        pq7.c(str, "id");
        pq7.c(str2, "type");
        xw7 unused = gu7.d(ds0.a(this), null, null, new m(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final void c7(String str, String str2, String str3) {
        pq7.c(str, "id");
        pq7.c(str2, Firmware.COLUMN_DOWNLOAD_URL);
        pq7.c(str3, "checksum");
        xw7 unused = gu7.d(ds0.a(this), null, null, new n(this, str, str2, str3, null), 3, null);
    }

    @DexIgnore
    public final void d7() {
        ec5 ec5 = this.j;
        if (ec5 != null) {
            Group group = ec5.c;
            pq7.b(group, "mBinding.groupError");
            group.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e7() {
        String a2;
        ec5 ec5 = this.j;
        if (ec5 != null) {
            WebView webView = ec5.e;
            pq7.b(webView, "mBinding.webView");
            webView.setWebViewClient(V6());
            ec5 ec52 = this.j;
            if (ec52 != null) {
                WebView webView2 = ec52.e;
                pq7.b(webView2, "mBinding.webView");
                webView2.setWebChromeClient(U6());
                ec5 ec53 = this.j;
                if (ec53 != null) {
                    WebView webView3 = ec53.e;
                    pq7.b(webView3, "mBinding.webView");
                    WebSettings settings = webView3.getSettings();
                    settings.setJavaScriptEnabled(true);
                    settings.setCacheMode(2);
                    Uri uri = this.k;
                    if (uri == null) {
                        a2 = h37.b.a(7);
                    } else if (uri != null) {
                        a2 = W6(uri);
                    } else {
                        pq7.i();
                        throw null;
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = s;
                    local.d(str, "setupWebView url " + a2);
                    ec5 ec54 = this.j;
                    if (ec54 != null) {
                        ec54.e.loadUrl(a2);
                        ec5 ec55 = this.j;
                        if (ec55 != null) {
                            WebView webView4 = ec55.e;
                            Context requireContext = requireContext();
                            pq7.b(requireContext, "requireContext()");
                            webView4.addJavascriptInterface(new WebAppInterface(this, requireContext), DeviceInfo.PLATFORM_ANDROID);
                            return;
                        }
                        pq7.n("mBinding");
                        throw null;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void f7(String str) {
        pq7.c(str, "url");
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", str);
        startActivity(Intent.createChooser(intent, getString(2131887333)));
    }

    @DexIgnore
    public final void g7() {
        String string = requireActivity().getString(2131886579);
        pq7.b(string, "requireActivity().getStr\u2026keSureYourHybridHrDevice)");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, string);
        fVar.e(2131363373, getString(2131887499));
        fVar.b(2131363373);
        fVar.k(getChildFragmentManager(), "DIALOG_DEVICE_NOT_ACTIVE");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ec5 c2 = ec5.c(layoutInflater);
        pq7.b(c2, "FragmentWatchFaceGalleryBinding.inflate(inflater)");
        this.j = c2;
        PortfolioApp.h0.c().M().s0().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = new ViewModelProvider(this, po4).a(WatchFaceGalleryViewModel.class);
            pq7.b(a2, "ViewModelProvider(this, \u2026eryViewModel::class.java)");
            this.i = (WatchFaceGalleryViewModel) a2;
            if (nk5.o.x(PortfolioApp.h0.c().J())) {
                d7();
                if (PortfolioApp.h0.c().p0()) {
                    b();
                    Bundle arguments = getArguments();
                    if (arguments != null) {
                        this.k = (Uri) arguments.get("EXTRA_URI");
                    }
                    e7();
                } else {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.O(childFragmentManager, 601, null);
                }
            } else {
                g7();
            }
            ec5 ec5 = this.j;
            if (ec5 != null) {
                return ec5.b();
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        ec5 ec5 = this.j;
        if (ec5 != null) {
            ec5.e.destroy();
            super.onDestroy();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        ao5.c.a(this.l);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        ao5.c.b(this.l);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        WatchFaceGalleryViewModel watchFaceGalleryViewModel = this.i;
        if (watchFaceGalleryViewModel != null) {
            watchFaceGalleryViewModel.j().h(getViewLifecycleOwner(), new j(this));
            WatchFaceGalleryViewModel watchFaceGalleryViewModel2 = this.i;
            if (watchFaceGalleryViewModel2 != null) {
                watchFaceGalleryViewModel2.l().h(getViewLifecycleOwner(), new k(this));
                WatchFaceGalleryViewModel watchFaceGalleryViewModel3 = this.i;
                if (watchFaceGalleryViewModel3 != null) {
                    watchFaceGalleryViewModel3.m().h(getViewLifecycleOwner(), new l(this));
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
