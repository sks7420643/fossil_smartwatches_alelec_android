package com.portfolio.platform.watchface.gallery;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.fossil.b77;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.xq0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceGalleryActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public WatchFaceGalleryFragment A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, WatchFaceGalleryActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(Context context, Uri uri) {
            pq7.c(context, "context");
            pq7.c(uri, "uri");
            Intent intent = new Intent(context, WatchFaceGalleryActivity.class);
            intent.setFlags(536870912);
            intent.putExtra("EXTRA_URI", uri);
            context.startActivity(intent);
        }
    }

    /*
    static {
        pq7.b(WatchFaceGalleryActivity.class.getSimpleName(), "WatchFaceGalleryActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
        WatchFaceGalleryFragment watchFaceGalleryFragment = this.A;
        if (watchFaceGalleryFragment != null) {
            watchFaceGalleryFragment.F6();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        WatchFaceGalleryFragment c;
        Bundle extras;
        super.onCreate(bundle);
        setContentView(2131558439);
        D(System.currentTimeMillis());
        WatchFaceGalleryFragment watchFaceGalleryFragment = (WatchFaceGalleryFragment) getSupportFragmentManager().Y(2131362158);
        this.A = watchFaceGalleryFragment;
        if (watchFaceGalleryFragment == null) {
            Intent intent = getIntent();
            if (intent == null || intent.hasExtra("EXTRA_URI")) {
                Intent intent2 = getIntent();
                Object obj = (intent2 == null || (extras = intent2.getExtras()) == null) ? null : extras.get("EXTRA_URI");
                if (obj != null) {
                    c = WatchFaceGalleryFragment.t.c((Uri) obj);
                } else {
                    throw new il7("null cannot be cast to non-null type android.net.Uri");
                }
            } else {
                c = WatchFaceGalleryFragment.t.b();
            }
            this.A = c;
        }
        xq0 j = getSupportFragmentManager().j();
        WatchFaceGalleryFragment watchFaceGalleryFragment2 = this.A;
        if (watchFaceGalleryFragment2 != null) {
            j.r(2131362158, watchFaceGalleryFragment2);
            j.j();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onDestroy() {
        b77.f401a.h("wf_gallery_session", p(), System.currentTimeMillis());
        super.onDestroy();
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String r = r();
        local.d(r, "receive on new intent " + intent);
        if (intent != null && intent.hasExtra("EXTRA_URI")) {
            Bundle extras = intent.getExtras();
            Object obj = extras != null ? extras.get("EXTRA_URI") : null;
            if (obj != null) {
                this.A = WatchFaceGalleryFragment.t.c((Uri) obj);
                xq0 j = getSupportFragmentManager().j();
                WatchFaceGalleryFragment watchFaceGalleryFragment = this.A;
                if (watchFaceGalleryFragment != null) {
                    j.r(2131362158, watchFaceGalleryFragment);
                    j.j();
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                throw new il7("null cannot be cast to non-null type android.net.Uri");
            }
        }
        super.onNewIntent(intent);
    }
}
