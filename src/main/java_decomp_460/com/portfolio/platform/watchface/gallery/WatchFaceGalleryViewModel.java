package com.portfolio.platform.watchface.gallery;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bw7;
import com.fossil.co7;
import com.fossil.dr7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.hq4;
import com.fossil.hr7;
import com.fossil.il7;
import com.fossil.im7;
import com.fossil.iq4;
import com.fossil.iv7;
import com.fossil.jj5;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.u37;
import com.fossil.uh5;
import com.fossil.um5;
import com.fossil.vp7;
import com.fossil.wb7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceGalleryViewModel extends hq4 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public /* final */ MutableLiveData<u37<Object>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository j;
    @DexIgnore
    public /* final */ wb7 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class OwnedWatchFaceStatus {
        @DexIgnore
        public /* final */ String id;
        @DexIgnore
        public /* final */ String packageVersion;
        @DexIgnore
        public /* final */ int status;

        @DexIgnore
        public OwnedWatchFaceStatus(String str, String str2, int i) {
            pq7.c(str, "id");
            pq7.c(str2, "packageVersion");
            this.id = str;
            this.packageVersion = str2;
            this.status = i;
        }

        @DexIgnore
        public static /* synthetic */ OwnedWatchFaceStatus copy$default(OwnedWatchFaceStatus ownedWatchFaceStatus, String str, String str2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = ownedWatchFaceStatus.id;
            }
            if ((i2 & 2) != 0) {
                str2 = ownedWatchFaceStatus.packageVersion;
            }
            if ((i2 & 4) != 0) {
                i = ownedWatchFaceStatus.status;
            }
            return ownedWatchFaceStatus.copy(str, str2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.id;
        }

        @DexIgnore
        public final String component2() {
            return this.packageVersion;
        }

        @DexIgnore
        public final int component3() {
            return this.status;
        }

        @DexIgnore
        public final OwnedWatchFaceStatus copy(String str, String str2, int i) {
            pq7.c(str, "id");
            pq7.c(str2, "packageVersion");
            return new OwnedWatchFaceStatus(str, str2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof OwnedWatchFaceStatus) {
                    OwnedWatchFaceStatus ownedWatchFaceStatus = (OwnedWatchFaceStatus) obj;
                    if (!pq7.a(this.id, ownedWatchFaceStatus.id) || !pq7.a(this.packageVersion, ownedWatchFaceStatus.packageVersion) || this.status != ownedWatchFaceStatus.status) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getId() {
            return this.id;
        }

        @DexIgnore
        public final String getPackageVersion() {
            return this.packageVersion;
        }

        @DexIgnore
        public final int getStatus() {
            return this.status;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.id;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.packageVersion;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return (((hashCode * 31) + i) * 31) + this.status;
        }

        @DexIgnore
        public String toString() {
            return "OwnedWatchFaceStatus(id=" + this.id + ", packageVersion=" + this.packageVersion + ", status=" + this.status + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$getAuthEvaluateScript$2", f = "WatchFaceGalleryViewModel.kt", l = {39}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WatchFaceGalleryViewModel watchFaceGalleryViewModel, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            dr7 dr7;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dr7 dr72 = new dr7();
                dr72.element = "";
                UserRepository userRepository = this.this$0.i;
                this.L$0 = iv7;
                this.L$1 = dr72;
                this.label = 1;
                currentUser = userRepository.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
                dr7 = dr72;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                dr7 = (dr7) this.L$1;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser != null) {
                hr7 hr7 = hr7.f1520a;
                String accessToken = mFUser.getAuth().getAccessToken();
                String refreshToken = mFUser.getAuth().getRefreshToken();
                Locale a2 = um5.a();
                pq7.b(a2, "LanguageHelper.getLocale()");
                T t = (T) String.format("AuthService.setToken(\"%s\",\"%s\",\"%s\")", Arrays.copyOf(new Object[]{accessToken, refreshToken, a2.getLanguage()}, 3));
                pq7.b(t, "java.lang.String.format(format, *args)");
                dr7.element = t;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = WatchFaceGalleryViewModel.l;
            local.d(str, "getAuthEvaluateScript " + ((String) dr7.element));
            return dr7.element;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$getOwnedWatchFaceCheckedScript$2", f = "WatchFaceGalleryViewModel.kt", l = {52}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFaceGalleryViewModel watchFaceGalleryViewModel, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            String str;
            Object allDianaWatchFaceUser;
            String str2;
            String str3;
            String str4;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.j;
                this.L$0 = iv7;
                this.L$1 = null;
                this.label = 1;
                allDianaWatchFaceUser = dianaWatchFaceRepository.getAllDianaWatchFaceUser(this);
                str = d;
                if (allDianaWatchFaceUser != d) {
                    str2 = null;
                }
                return str;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                str2 = (String) this.L$1;
                allDianaWatchFaceUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<DianaWatchFaceUser> list = (List) allDianaWatchFaceUser;
            FLogger.INSTANCE.getLocal().d(WatchFaceGalleryViewModel.l, "watchfaces list " + list);
            ArrayList arrayList = new ArrayList(im7.m(list, 10));
            for (DianaWatchFaceUser dianaWatchFaceUser : list) {
                DianaWatchFaceOrder order = dianaWatchFaceUser.getOrder();
                if (order == null || (str3 = order.getWatchFaceId()) == null) {
                    str3 = "";
                }
                DianaWatchFaceOrder order2 = dianaWatchFaceUser.getOrder();
                if (order2 == null || (str4 = order2.getPackageVersion()) == null) {
                    str4 = "";
                }
                arrayList.add(new OwnedWatchFaceStatus(str3, str4, 1));
            }
            String str5 = str2;
            if (!arrayList.isEmpty()) {
                hr7 hr7 = hr7.f1520a;
                String format = String.format("WFService.setOwnedWatchFaces(\"UPDATE_OWNED_WF\",%s)", Arrays.copyOf(new Object[]{jj5.a(arrayList)}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                str5 = format;
            }
            FLogger.INSTANCE.getLocal().d(WatchFaceGalleryViewModel.l, "getOwnedWatchFaceCheckedScript " + str5);
            str = str5;
            return str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel", f = "WatchFaceGalleryViewModel.kt", l = {82}, m = "getWatchFaceOrderScript")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFaceGalleryViewModel watchFaceGalleryViewModel, qn7 qn7) {
            super(qn7);
            this.this$0 = watchFaceGalleryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$getWatchFaceOrderScript$isSuccess$1", f = "WatchFaceGalleryViewModel.kt", l = {82}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $orderId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WatchFaceGalleryViewModel watchFaceGalleryViewModel, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = watchFaceGalleryViewModel;
            this.$orderId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$orderId, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.j;
                String str = this.$orderId;
                this.L$0 = iv7;
                this.label = 1;
                Object downloadDianaWatchFaceUserWithOrderId = dianaWatchFaceRepository.downloadDianaWatchFaceUserWithOrderId(str, this);
                return downloadDianaWatchFaceUserWithOrderId == d ? d : downloadDianaWatchFaceUserWithOrderId;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<wb7.c, wb7.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel f4780a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(WatchFaceGalleryViewModel watchFaceGalleryViewModel) {
            this.f4780a = watchFaceGalleryViewModel;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(wb7.a aVar) {
            pq7.c(aVar, "errorValue");
            this.f4780a.k.s();
            hq4.d(this.f4780a, false, true, null, 5, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = WatchFaceGalleryViewModel.l;
            local.e(str, "preview watch face failed! Last error code: " + aVar.b());
            int b = aVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                WatchFaceGalleryViewModel watchFaceGalleryViewModel = this.f4780a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    watchFaceGalleryViewModel.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.f4780a.f();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(wb7.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(WatchFaceGalleryViewModel.l, "preview watch face success");
            this.f4780a.k.s();
            hq4.d(this.f4780a, false, true, null, 5, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel", f = "WatchFaceGalleryViewModel.kt", l = {77}, m = "syncWatchFace")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceGalleryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WatchFaceGalleryViewModel watchFaceGalleryViewModel, qn7 qn7) {
            super(qn7);
            this.this$0 = watchFaceGalleryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(this);
        }
    }

    /*
    static {
        String simpleName = WatchFaceGalleryViewModel.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceGalleryViewModel::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public WatchFaceGalleryViewModel(UserRepository userRepository, DianaWatchFaceRepository dianaWatchFaceRepository, wb7 wb7) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        pq7.c(wb7, "mPreviewWatchFaceUseCase");
        this.i = userRepository;
        this.j = dianaWatchFaceRepository;
        this.k = wb7;
    }

    @DexIgnore
    public final Object t(qn7<? super String> qn7) {
        return eu7.g(bw7.b(), new a(this, null), qn7);
    }

    @DexIgnore
    public final Object u(qn7<? super String> qn7) {
        return eu7.g(bw7.b(), new b(this, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object v(java.lang.String r8, java.lang.String r9, com.fossil.qn7<? super java.lang.String> r10) {
        /*
            r7 = this;
            r6 = 2
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r10 instanceof com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.c
            if (r0 == 0) goto L_0x0054
            r0 = r10
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$c r0 = (com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0054
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x0063
            if (r1 != r5) goto L_0x005b
            java.lang.Object r0 = r2.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel r2 = (com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r9 = r0
        L_0x0032:
            r0 = r2
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0080
            com.fossil.hr7 r0 = com.fossil.hr7.f1520a
            java.lang.String r0 = "WFService.orderWatchFace(\"UPDATE_WF_LIST\", {\"orderID\":\"%s\",\"id\":\"%s\"})"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            r3 = 0
            r2[r3] = r1
            r2[r5] = r9
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r2, r6)
            java.lang.String r0 = java.lang.String.format(r0, r1)
            java.lang.String r1 = "java.lang.String.format(format, *args)"
            com.fossil.pq7.b(r0, r1)
        L_0x0053:
            return r0
        L_0x0054:
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$c r0 = new com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$c
            r0.<init>(r7, r10)
            r2 = r0
            goto L_0x0015
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.el7.b(r3)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$d r3 = new com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$d
            r4 = 0
            r3.<init>(r7, r8, r4)
            r2.L$0 = r7
            r2.L$1 = r8
            r2.L$2 = r9
            r2.label = r5
            java.lang.Object r2 = com.fossil.eu7.g(r1, r3, r2)
            if (r2 == r0) goto L_0x0053
            r1 = r8
            goto L_0x0032
        L_0x0080:
            java.lang.String r0 = "WFService.showError()"
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.v(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object w(String str, String str2, String str3, qn7<? super tl7> qn7) {
        hq4.d(this, true, false, null, 6, null);
        this.k.p();
        xw7 e2 = this.k.e(new wb7.b(str, str2, str3), new e(this));
        return e2 == yn7.d() ? e2 : tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object x(com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
            r10 = this;
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            r3 = 0
            r1 = 1
            boolean r0 = r11 instanceof com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.f
            if (r0 == 0) goto L_0x0035
            r0 = r11
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$f r0 = (com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.f) r0
            int r4 = r0.label
            r5 = r4 & r6
            if (r5 == 0) goto L_0x0035
            int r4 = r4 + r6
            r0.label = r4
            r6 = r0
        L_0x0016:
            java.lang.Object r4 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0044
            if (r0 != r1) goto L_0x003c
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel r0 = (com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel) r0
            com.fossil.el7.b(r4)
            r4 = r0
        L_0x002a:
            r8 = 5
            r5 = r2
            r6 = r1
            r7 = r3
            r9 = r3
            com.fossil.hq4.d(r4, r5, r6, r7, r8, r9)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0034:
            return r0
        L_0x0035:
            com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$f r0 = new com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel$f
            r0.<init>(r10, r11)
            r6 = r0
            goto L_0x0016
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.el7.b(r4)
            r4 = 6
            r0 = r10
            r5 = r3
            com.fossil.hq4.d(r0, r1, r2, r3, r4, r5)
            com.portfolio.platform.data.source.DianaWatchFaceRepository r0 = r10.j
            r6.L$0 = r10
            r6.label = r1
            java.lang.Object r0 = com.portfolio.platform.data.source.DianaWatchFaceRepository.downloadAllDianaWatchFaceUser$default(r0, r2, r6, r1, r3)
            if (r0 != r7) goto L_0x005b
            r0 = r7
            goto L_0x0034
        L_0x005b:
            r4 = r10
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel.x(com.fossil.qn7):java.lang.Object");
    }
}
