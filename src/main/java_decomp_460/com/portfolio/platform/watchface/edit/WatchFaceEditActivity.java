package com.portfolio.platform.watchface.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.b77;
import com.fossil.fc7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.nw3;
import com.fossil.pq7;
import com.fossil.x77;
import com.fossil.xq0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceEditActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, "watchFaceId");
            Intent intent = new Intent(context, WatchFaceEditActivity.class);
            intent.putExtra("preset_id", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(Context context, boolean z) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, WatchFaceEditActivity.class);
            intent.putExtra("from_faces", true);
            intent.putExtra("sharing_flow_extra", z);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(nw3.container);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
        x77 x77 = (x77) getSupportFragmentManager().Z(x77.D.a());
        if (x77 != null) {
            x77.onBackPressed();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558850);
        D(System.currentTimeMillis());
        if (bundle == null) {
            xq0 j = getSupportFragmentManager().j();
            j.s(nw3.container, fc7.b(fc7.f1105a, x77.class, getIntent().getStringExtra("preset_id"), getIntent().getIntExtra("TAB_EXTRA", 2), null, getIntent().getBooleanExtra("from_faces", false), getIntent().getBooleanExtra("sharing_flow_extra", false), 8, null), x77.D.a());
            j.j();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onDestroy() {
        b77.f401a.h("wf_editing_session", p(), System.currentTimeMillis());
        super.onDestroy();
    }
}
