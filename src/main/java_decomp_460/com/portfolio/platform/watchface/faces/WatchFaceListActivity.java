package com.portfolio.platform.watchface.faces;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProvider;
import com.fossil.b77;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.ma7;
import com.fossil.nw3;
import com.fossil.oa7;
import com.fossil.pq7;
import com.fossil.ts0;
import com.fossil.xq0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceListActivity extends ls5 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public oa7 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, WatchFaceListActivity.class));
        }

        @DexIgnore
        public final void b(Context context, Uri uri) {
            pq7.c(context, "context");
            pq7.c(uri, "uri");
            Intent intent = new Intent(context, WatchFaceListActivity.class);
            intent.setFlags(536870912);
            intent.putExtra("EXTRA_URI", uri);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void c(Context context, String str, String str2, boolean z) {
            pq7.c(context, "context");
            pq7.c(str, "id");
            pq7.c(str2, "type");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = WatchFaceListActivity.B;
            local.d(str3, "start with orderId id " + str);
            Intent intent = new Intent(context, WatchFaceListActivity.class);
            intent.putExtra("WATCH_FACE_ID", str);
            intent.putExtra("WATCH_FACE_ID_TYPE", str2);
            intent.putExtra("sharing_flow_extra", z);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchFaceListActivity.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceListActivity::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public final oa7 M() {
        oa7 oa7 = this.A;
        if (oa7 != null) {
            return oa7;
        }
        pq7.n("sharedViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        Bundle extras;
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558855);
        ts0 a2 = new ViewModelProvider(this).a(oa7.class);
        pq7.b(a2, "ViewModelProvider(this).\u2026redViewModel::class.java)");
        this.A = (oa7) a2;
        D(System.currentTimeMillis());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String r = r();
        local.d(r, "onCreate - active serial " + PortfolioApp.h0.c().J());
        Intent intent = getIntent();
        String str = "";
        if (intent == null || !intent.hasExtra("EXTRA_URI")) {
            String stringExtra = getIntent().hasExtra("WATCH_FACE_ID") ? getIntent().getStringExtra("WATCH_FACE_ID") : "";
            if (getIntent().hasExtra("WATCH_FACE_ID_TYPE")) {
                str = getIntent().getStringExtra("WATCH_FACE_ID_TYPE");
            }
            if (getIntent().hasExtra("sharing_flow_extra")) {
                z = getIntent().getBooleanExtra("sharing_flow_extra", false);
            }
            if (bundle == null) {
                xq0 j = getSupportFragmentManager().j();
                ma7.a aVar = ma7.t;
                pq7.b(stringExtra, "id");
                pq7.b(str, "type");
                j.r(nw3.container, aVar.a(stringExtra, str, z));
                j.j();
                return;
            }
            return;
        }
        Intent intent2 = getIntent();
        Object obj = (intent2 == null || (extras = intent2.getExtras()) == null) ? null : extras.get("EXTRA_URI");
        if (obj != null) {
            Uri uri = (Uri) obj;
            String lastPathSegment = uri.getLastPathSegment();
            if (lastPathSegment == null) {
                lastPathSegment = "";
            }
            pq7.b(lastPathSegment, "uri.lastPathSegment ?: \"\"");
            String queryParameter = uri.getQueryParameter("token");
            if (queryParameter == null) {
                queryParameter = str;
            }
            pq7.b(queryParameter, "uri.getQueryParameter(\"token\") ?: \"\"");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "id " + lastPathSegment + " token " + queryParameter);
            xq0 j2 = getSupportFragmentManager().j();
            j2.r(nw3.container, ma7.t.b(lastPathSegment, queryParameter));
            j2.j();
            return;
        }
        throw new il7("null cannot be cast to non-null type android.net.Uri");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onDestroy() {
        b77.f401a.h("wf_myfaces_session", p(), System.currentTimeMillis());
        super.onDestroy();
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String r = r();
        local.d(r, "receive on new intent " + intent);
        if (intent != null && intent.hasExtra("EXTRA_URI")) {
            Bundle extras = intent.getExtras();
            Object obj = extras != null ? extras.get("EXTRA_URI") : null;
            if (obj != null) {
                Uri uri = (Uri) obj;
                String lastPathSegment = uri.getLastPathSegment();
                if (lastPathSegment == null) {
                    lastPathSegment = "";
                }
                pq7.b(lastPathSegment, "uri.lastPathSegment ?: \"\"");
                String queryParameter = uri.getQueryParameter("token");
                if (queryParameter == null) {
                    queryParameter = "";
                }
                pq7.b(queryParameter, "uri.getQueryParameter(\"token\") ?: \"\"");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String r2 = r();
                local2.d(r2, "id " + lastPathSegment + " token " + queryParameter);
                xq0 j = getSupportFragmentManager().j();
                j.r(nw3.container, ma7.t.b(lastPathSegment, queryParameter));
                j.k();
            } else {
                throw new il7("null cannot be cast to non-null type android.net.Uri");
            }
        }
        super.onNewIntent(intent);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
