package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import com.fossil.cs0;
import com.fossil.dq4;
import com.fossil.ms0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AutoClearedValue$Anon1 implements cs0 {
    @DexIgnore
    public /* final */ /* synthetic */ dq4 b;

    @DexIgnore
    @ms0(Lifecycle.a.ON_DESTROY)
    public final void onDestroy() {
        this.b.c(null);
        throw null;
    }
}
