package com.portfolio.platform.data;

import com.fossil.pj4;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeModel {
    @DexIgnore
    @pj4
    public int minutes;
    @DexIgnore
    @pj4
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        pq7.c(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        pq7.c(str, "<set-?>");
        this.remindTimeName = str;
    }
}
