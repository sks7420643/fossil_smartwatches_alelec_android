package com.portfolio.platform.data;

import com.fossil.em7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserWrapper {
    @DexIgnore
    @rj4("activeDeviceId")
    public /* final */ String mActiveDeviceId;
    @DexIgnore
    @rj4("addresses")
    public /* final */ CommuteAddress mAddress;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_AUTHTYPE)
    public /* final */ String mAuthType;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_BIRTHDAY)
    public /* final */ String mBirthday;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_BRAND)
    public /* final */ String mBrand;
    @DexIgnore
    @rj4("createdAt")
    public /* final */ String mCreatedAt;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public /* final */ Boolean mDiagnosticEnabled;
    @DexIgnore
    @rj4(Constants.EMAIL)
    public /* final */ String mEmail;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_EMAIL_OPT_IN)
    public /* final */ Boolean mEmailOptIn;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_EMAIL_PROGRESS)
    public /* final */ Boolean mEmailProgress;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_EXTERNALID)
    public /* final */ String mExternalId;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_FIRST_NAME)
    public /* final */ String mFirstName;
    @DexIgnore
    @rj4("gender")
    public /* final */ String mGender;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_HEIGHT_IN_CM)
    public /* final */ Double mHeightInCentimeters;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_INTEGRATIONS)
    public /* final */ String[] mIntegrations;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE)
    public /* final */ Boolean mIsOnboardingComplete;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_LAST_NAME)
    public /* final */ String mLastName;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_PROFILE_PIC)
    public /* final */ String mProfilePicture;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_REGISTER_DATE)
    public /* final */ String mRegisterDate;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_REGISTRATION_COMPLETE)
    public /* final */ Boolean mRegistrationComplete;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_UNIT_GROUP)
    public /* final */ UnitGroup mUnitGroup;
    @DexIgnore
    @rj4("updatedAt")
    public /* final */ String mUpdatedAt;
    @DexIgnore
    @rj4("useDefaultGoals")
    public /* final */ Boolean mUseDefaultGoals;
    @DexIgnore
    @rj4("useDefaultBiometric")
    public /* final */ Boolean mUserDefaultBiometric;
    @DexIgnore
    @rj4("id")
    public /* final */ String mUserId;
    @DexIgnore
    @rj4("username")
    public /* final */ String mUsername;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS)
    public /* final */ Double mWeightInGrams;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CommuteAddress {
        @DexIgnore
        @rj4("home")
        public /* final */ String home;
        @DexIgnore
        @rj4("work")
        public /* final */ String work;

        @DexIgnore
        public CommuteAddress(String str, String str2) {
            pq7.c(str, "home");
            pq7.c(str2, "work");
            this.home = str;
            this.work = str2;
        }

        @DexIgnore
        public final String getHome$app_fossilRelease() {
            return this.home;
        }

        @DexIgnore
        public final String getWork$app_fossilRelease() {
            return this.work;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class UnitGroup {
        @DexIgnore
        @rj4("distance")
        public /* final */ String distance;
        @DexIgnore
        @rj4("height")
        public /* final */ String height;
        @DexIgnore
        @rj4("temperature")
        public /* final */ String temperature;
        @DexIgnore
        @rj4(Constants.PROFILE_KEY_UNITS_WEIGHT)
        public /* final */ String weight;

        @DexIgnore
        public UnitGroup(String str, String str2, String str3, String str4) {
            pq7.c(str, "distance");
            pq7.c(str2, Constants.PROFILE_KEY_UNITS_WEIGHT);
            pq7.c(str3, "height");
            pq7.c(str4, "temperature");
            this.distance = str;
            this.weight = str2;
            this.height = str3;
            this.temperature = str4;
        }

        @DexIgnore
        public final String getDistance$app_fossilRelease() {
            return this.distance;
        }

        @DexIgnore
        public final String getHeight$app_fossilRelease() {
            return this.height;
        }

        @DexIgnore
        public final String getTemperature$app_fossilRelease() {
            return this.temperature;
        }

        @DexIgnore
        public final String getWeight$app_fossilRelease() {
            return this.weight;
        }
    }

    @DexIgnore
    public UserWrapper(String str, String str2, String str3, String str4, String str5, String str6, UnitGroup unitGroup, CommuteAddress commuteAddress, Boolean bool, Boolean bool2, String str7, String[] strArr, String str8, String str9, String str10, Double d, Double d2, String str11, String str12, String str13, String str14, Boolean bool3, Boolean bool4, Boolean bool5, String str15, Boolean bool6, Boolean bool7) {
        pq7.c(str, "mUserId");
        pq7.c(str2, "mCreatedAt");
        pq7.c(str3, "mUpdatedAt");
        pq7.c(str5, "mAuthType");
        pq7.c(str8, "mFirstName");
        pq7.c(str9, "mLastName");
        pq7.c(str11, "mBirthday");
        this.mUserId = str;
        this.mCreatedAt = str2;
        this.mUpdatedAt = str3;
        this.mUsername = str4;
        this.mAuthType = str5;
        this.mExternalId = str6;
        this.mUnitGroup = unitGroup;
        this.mAddress = commuteAddress;
        this.mEmailProgress = bool;
        this.mEmailOptIn = bool2;
        this.mActiveDeviceId = str7;
        this.mIntegrations = strArr;
        this.mFirstName = str8;
        this.mLastName = str9;
        this.mEmail = str10;
        this.mWeightInGrams = d;
        this.mHeightInCentimeters = d2;
        this.mBirthday = str11;
        this.mGender = str12;
        this.mProfilePicture = str13;
        this.mBrand = str14;
        this.mDiagnosticEnabled = bool3;
        this.mRegistrationComplete = bool4;
        this.mIsOnboardingComplete = bool5;
        this.mRegisterDate = str15;
        this.mUserDefaultBiometric = bool6;
        this.mUseDefaultGoals = bool7;
    }

    @DexIgnore
    public final MFUser toMFUser() {
        String str;
        String str2;
        boolean z = true;
        boolean z2 = false;
        MFUser mFUser = new MFUser();
        mFUser.setUserId(this.mUserId);
        mFUser.setCreatedAt(this.mCreatedAt);
        mFUser.setUpdatedAt(this.mUpdatedAt);
        String str3 = this.mEmail;
        if (str3 == null) {
            str3 = "";
        }
        mFUser.setEmail(str3);
        mFUser.setAuthType(this.mAuthType);
        String str4 = this.mUsername;
        if (str4 == null) {
            str4 = "";
        }
        mFUser.setUsername(str4);
        String str5 = this.mActiveDeviceId;
        if (str5 == null) {
            str5 = "";
        }
        mFUser.setActiveDeviceId(str5);
        String str6 = this.mFirstName;
        if (str6 == null) {
            str6 = "";
        }
        mFUser.setFirstName(str6);
        String str7 = this.mLastName;
        if (str7 == null) {
            str7 = "";
        }
        mFUser.setLastName(str7);
        Double d = this.mWeightInGrams;
        mFUser.setWeightInGrams(d != null ? (int) d.doubleValue() : 0);
        Double d2 = this.mHeightInCentimeters;
        mFUser.setHeightInCentimeters(d2 != null ? (int) d2.doubleValue() : 0);
        Boolean bool = this.mUserDefaultBiometric;
        mFUser.setUseDefaultBiometric(bool != null ? bool.booleanValue() : true);
        Boolean bool2 = this.mUseDefaultGoals;
        if (bool2 != null) {
            z = bool2.booleanValue();
        }
        mFUser.setUseDefaultGoals(z);
        if (this.mUnitGroup != null) {
            mFUser.getUnitGroup().setHeight(this.mUnitGroup.getHeight$app_fossilRelease());
            mFUser.getUnitGroup().setWeight(this.mUnitGroup.getWeight$app_fossilRelease());
            mFUser.getUnitGroup().setDistance(this.mUnitGroup.getDistance$app_fossilRelease());
            mFUser.getUnitGroup().setTemperature(this.mUnitGroup.getTemperature$app_fossilRelease());
        }
        MFUser.Address addresses = mFUser.getAddresses();
        CommuteAddress commuteAddress = this.mAddress;
        if (commuteAddress == null || (str = commuteAddress.getHome$app_fossilRelease()) == null) {
            str = "";
        }
        addresses.setHome(str);
        MFUser.Address addresses2 = mFUser.getAddresses();
        CommuteAddress commuteAddress2 = this.mAddress;
        if (commuteAddress2 == null || (str2 = commuteAddress2.getWork$app_fossilRelease()) == null) {
            str2 = "";
        }
        addresses2.setWork(str2);
        Boolean bool3 = this.mEmailOptIn;
        mFUser.setEmailOptIn(bool3 != null ? bool3.booleanValue() : false);
        String str8 = this.mRegisterDate;
        if (str8 == null) {
            str8 = "";
        }
        mFUser.setRegisterDate(str8);
        String str9 = this.mBirthday;
        if (str9 == null) {
            str9 = "";
        }
        mFUser.setBirthday(str9);
        String str10 = this.mGender;
        if (str10 == null) {
            str10 = "M";
        }
        mFUser.setGender(str10);
        String str11 = this.mProfilePicture;
        if (str11 == null) {
            str11 = "";
        }
        mFUser.setProfilePicture(str11);
        String str12 = this.mBrand;
        if (str12 == null) {
            str12 = "";
        }
        mFUser.setBrand(str12);
        Boolean bool4 = this.mDiagnosticEnabled;
        mFUser.setDiagnosticEnabled(bool4 != null ? bool4.booleanValue() : false);
        Boolean bool5 = this.mIsOnboardingComplete;
        mFUser.setOnboardingComplete(bool5 != null ? bool5.booleanValue() : false);
        Boolean bool6 = this.mRegistrationComplete;
        if (bool6 != null) {
            z2 = bool6.booleanValue();
        }
        mFUser.setRegistrationComplete(z2);
        String[] strArr = this.mIntegrations;
        mFUser.setIntegrationsList(strArr != null ? em7.f0(strArr) : null);
        return mFUser;
    }
}
