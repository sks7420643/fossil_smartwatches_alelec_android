package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDao_Impl implements AddressDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<AddressOfWeather> __insertionAdapterOfAddressOfWeather;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<AddressOfWeather> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, AddressOfWeather addressOfWeather) {
            px0.bindLong(1, (long) addressOfWeather.getId());
            px0.bindDouble(2, addressOfWeather.getLat());
            px0.bindDouble(3, addressOfWeather.getLng());
            if (addressOfWeather.getAddress() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, addressOfWeather.getAddress());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `addressOfWeather` (`id`,`lat`,`lng`,`address`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM addressOfWeather";
        }
    }

    @DexIgnore
    public AddressDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfAddressOfWeather = new Anon1(qw0);
        this.__preparedStmtOfClearData = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public List<AddressOfWeather> getAllSavedAddress() {
        tw0 f = tw0.f("SELECT * FROM addressOfWeather", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, Constants.LAT);
            int c3 = dx0.c(b, "lng");
            int c4 = dx0.c(b, "address");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                AddressOfWeather addressOfWeather = new AddressOfWeather(b.getDouble(c2), b.getDouble(c3), b.getString(c4));
                addressOfWeather.setId(b.getInt(c));
                arrayList.add(addressOfWeather);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void saveAddress(AddressOfWeather addressOfWeather) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfAddressOfWeather.insert((jw0<AddressOfWeather>) addressOfWeather);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
