package com.portfolio.platform.data.source.loader;

import android.content.Context;
import android.util.SparseArray;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationsLoader extends BaseLoader<SparseArray<List<BaseFeatureModel>>> implements NotificationsRepository.NotificationRepositoryObserver {
    @DexIgnore
    public static /* final */ String TAG; // = "NotificationsLoader";
    @DexIgnore
    public /* final */ NotificationsRepository mRepository;

    @DexIgnore
    public NotificationsLoader(Context context, NotificationsRepository notificationsRepository) {
        super(context);
        this.mRepository = notificationsRepository;
    }

    @DexIgnore
    @Override // com.fossil.ys0
    public SparseArray<List<BaseFeatureModel>> loadInBackground() {
        String J = PortfolioApp.d0.J();
        MFDeviceFamily deviceFamily = DeviceIdentityUtils.getDeviceFamily(J);
        MFDeviceFamily mFDeviceFamily = MFDeviceFamily.DEVICE_FAMILY_DIANA;
        SparseArray<List<BaseFeatureModel>> allNotificationsByHour = deviceFamily == mFDeviceFamily ? this.mRepository.getAllNotificationsByHour(J, mFDeviceFamily.getValue()) : this.mRepository.getAllNotificationsByHour(J, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside loadInBackground result= " + allNotificationsByHour);
        return allNotificationsByHour;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsRepository.NotificationRepositoryObserver
    public void onNotificationsDataChanged() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .onNotificationDataChanged, isStarted=" + isStarted());
        if (isStarted()) {
            forceLoad();
        }
    }

    @DexIgnore
    @Override // com.fossil.at0, com.portfolio.platform.data.source.loader.BaseLoader
    public void onReset() {
        this.mRepository.removeContentObserver(this);
        super.onReset();
    }

    @DexIgnore
    @Override // com.fossil.at0
    public void onStartLoading() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside onStartLoading isCachedAvailable=" + this.mRepository.isCacheNotificationsAvailable());
        if (this.mRepository.isCacheNotificationsAvailable()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Inside onStartLoading return result from cached=");
            deliverResult(this.mRepository.getCachedNotifications());
        }
        this.mRepository.addContentObserver(this);
        if (!this.mRepository.isCacheNotificationsAvailable()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Inside onStartLoading forceReload");
            forceLoad();
        }
    }
}
