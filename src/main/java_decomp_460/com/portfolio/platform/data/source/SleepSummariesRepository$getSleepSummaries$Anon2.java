package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$2", f = "SleepSummariesRepository.kt", l = {180, 181}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<MFSleepDay>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ String $end;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ String $start;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<MFSleepDay>, gj4> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<gj4>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSleepSummaries(k, k2, 0, 100, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<List<MFSleepDay>>> qn7) {
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                String str = this.this$0.$start;
                pq7.b(str, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                String str2 = this.this$0.$end;
                pq7.b(str2, "end");
                return sleepDao.getSleepDaysLiveData(str, str2);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(SleepSummariesRepository.TAG, "getActivityList onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(gj4 gj4, qn7<? super tl7> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSummariesRepository.TAG;
                local.d(str, "getSleepSummaries saveCallResult onResponse: response = " + gj4);
                SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0.this$0;
                Object saveSleepSummaries = sleepSummariesRepository$getSleepSummaries$Anon2.this$0.saveSleepSummaries(gj4, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate, this.$downloadingDate, qn7);
                return saveSleepSummaries == yn7.d() ? saveSleepSummaries : tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(gj4 gj4, qn7 qn7) {
                return saveCallResult(gj4, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepDay> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2, SleepDatabase sleepDatabase, String str, String str2) {
            this.this$0 = sleepSummariesRepository$getSleepSummaries$Anon2;
            this.$sleepDatabase = sleepDatabase;
            this.$start = str;
            this.$end = str2;
        }

        @DexIgnore
        public final LiveData<h47<List<MFSleepDay>>> apply(List<FitnessDataWrapper> list) {
            pq7.b(list, "fitnessDataList");
            SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummaries$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = new SleepSummariesRepository$getSleepSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, qn7);
        sleepSummariesRepository$getSleepSummaries$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$getSleepSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<MFSleepDay>>>> qn7) {
        return ((SleepSummariesRepository$getSleepSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00ca  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            r8 = 0
            r7 = 2
            r6 = 1
            java.lang.Object r5 = com.fossil.yn7.d()
            int r0 = r9.label
            if (r0 == 0) goto L_0x0075
            if (r0 == r6) goto L_0x0046
            if (r0 != r7) goto L_0x003e
            java.lang.Object r0 = r9.L$3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r9.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r9.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r9.L$0
            com.fossil.iv7 r3 = (com.fossil.iv7) r3
            com.fossil.el7.b(r10)
            r3 = r10
            r4 = r1
            r5 = r0
        L_0x0025:
            r0 = r3
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r1 = r5.getFitnessDataDao()
            java.util.Date r3 = r9.$startDate
            java.util.Date r5 = r9.$endDate
            androidx.lifecycle.LiveData r1 = r1.getFitnessDataLiveData(r3, r5)
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$Anon1_Level2 r3 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$Anon1_Level2
            r3.<init>(r9, r0, r2, r4)
            androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r1, r3)
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r9.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r9.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r9.L$0
            com.fossil.iv7 r2 = (com.fossil.iv7) r2
            com.fossil.el7.b(r10)
            r4 = r0
            r3 = r10
        L_0x0057:
            r0 = r3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.fossil.dv7 r3 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2 r6 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2
            r6.<init>(r8)
            r9.L$0 = r2
            r9.L$1 = r1
            r9.L$2 = r4
            r9.L$3 = r0
            r9.label = r7
            java.lang.Object r3 = com.fossil.eu7.g(r3, r6, r9)
            if (r3 != r5) goto L_0x00ca
            r0 = r5
            goto L_0x003d
        L_0x0075:
            com.fossil.el7.b(r10)
            com.fossil.iv7 r2 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.SleepSummariesRepository.access$getTAG$cp()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getSleepSummaries: startDate = "
            r3.append(r4)
            java.util.Date r4 = r9.$startDate
            r3.append(r4)
            java.lang.String r4 = ", endDate = "
            r3.append(r4)
            java.util.Date r4 = r9.$endDate
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.d(r1, r3)
            java.util.Date r0 = r9.$startDate
            java.lang.String r1 = com.fossil.lk5.k(r0)
            java.util.Date r0 = r9.$endDate
            java.lang.String r0 = com.fossil.lk5.k(r0)
            com.fossil.dv7 r3 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2
            r4.<init>(r8)
            r9.L$0 = r2
            r9.L$1 = r1
            r9.L$2 = r0
            r9.label = r6
            java.lang.Object r3 = com.fossil.eu7.g(r3, r4, r9)
            if (r3 != r5) goto L_0x00ce
            r0 = r5
            goto L_0x003d
        L_0x00ca:
            r2 = r1
            r5 = r0
            goto L_0x0025
        L_0x00ce:
            r4 = r0
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
