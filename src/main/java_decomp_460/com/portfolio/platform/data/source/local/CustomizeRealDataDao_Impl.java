package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.CustomizeRealData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDao_Impl implements CustomizeRealDataDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<CustomizeRealData> __insertionAdapterOfCustomizeRealData;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<CustomizeRealData> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, CustomizeRealData customizeRealData) {
            if (customizeRealData.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, customizeRealData.getId());
            }
            if (customizeRealData.getValue() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, customizeRealData.getValue());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `customizeRealData` (`id`,`value`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM customizeRealData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<CustomizeRealData> call() throws Exception {
            Cursor b = ex0.b(CustomizeRealDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "value");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new CustomizeRealData(b.getString(c), b.getString(c2)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public CustomizeRealDataDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfCustomizeRealData = new Anon1(qw0);
        this.__preparedStmtOfCleanUp = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM customizeRealData", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"customizeRealData"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public List<CustomizeRealData> getAllRealDataRaw() {
        tw0 f = tw0.f("SELECT * FROM customizeRealData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "value");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new CustomizeRealData(b.getString(c), b.getString(c2)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public CustomizeRealData getRealData(String str) {
        CustomizeRealData customizeRealData = null;
        tw0 f = tw0.f("SELECT * FROM customizeRealData WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "value");
            if (b.moveToFirst()) {
                customizeRealData = new CustomizeRealData(b.getString(c), b.getString(c2));
            }
            return customizeRealData;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void upsertRealData(CustomizeRealData customizeRealData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCustomizeRealData.insert((jw0<CustomizeRealData>) customizeRealData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
