package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataRepository.class.getSimpleName();
        pq7.b(simpleName, "FitnessDataRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataRepository(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new FitnessDataRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object getFitnessData(Date date, Date date2, qn7<? super List<FitnessDataWrapper>> qn7) {
        return eu7.g(bw7.b(), new FitnessDataRepository$getFitnessData$Anon2(date, date2, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r18, com.fossil.qn7<? super com.fossil.iq5<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r19) {
        /*
        // Method dump skipped, instructions count: 531
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.insert(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01c0, code lost:
        if (r5 >= r13.size()) goto L_0x005c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object pushFitnessData(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r18, com.fossil.qn7<? super com.fossil.iq5<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r19) {
        /*
        // Method dump skipped, instructions count: 455
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.pushFitnessData(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingFitnessData(qn7<? super iq5<List<FitnessDataWrapper>>> qn7) {
        return eu7.g(bw7.b(), new FitnessDataRepository$pushPendingFitnessData$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object saveFitnessData(List<FitnessDataWrapper> list, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new FitnessDataRepository$saveFitnessData$Anon2(list, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
