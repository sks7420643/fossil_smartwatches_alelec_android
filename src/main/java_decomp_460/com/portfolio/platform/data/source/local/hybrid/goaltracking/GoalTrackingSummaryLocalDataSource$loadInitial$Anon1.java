package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.fl5;
import com.fossil.pq7;
import com.fossil.xw7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource$loadInitial$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.this$0;
        goalTrackingSummaryLocalDataSource.calculateStartDate(goalTrackingSummaryLocalDataSource.mCreatedDate);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.this$0;
        fl5.d dVar = fl5.d.INITIAL;
        Date mStartDate = goalTrackingSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        pq7.b(aVar, "helperCallback");
        xw7 unused = goalTrackingSummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
