package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory implements Factory<UserSettingDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static UserSettingDatabase provideUserSettingDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        UserSettingDatabase provideUserSettingDatabase = portfolioDatabaseModule.provideUserSettingDatabase(portfolioApp);
        lk7.c(provideUserSettingDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserSettingDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public UserSettingDatabase get() {
        return provideUserSettingDatabase(this.module, this.appProvider.get());
    }
}
