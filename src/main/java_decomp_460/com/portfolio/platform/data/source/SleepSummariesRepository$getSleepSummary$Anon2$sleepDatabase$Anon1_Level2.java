package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$2$sleepDatabase$1", f = "SleepSummariesRepository.kt", l = {141}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super SleepDatabase>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 sleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 = new SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2(qn7);
        sleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2.p$ = (iv7) obj;
        return sleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super SleepDatabase> qn7) {
        return ((SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            Object D = bn5.D(this);
            return D == d ? d : D;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
