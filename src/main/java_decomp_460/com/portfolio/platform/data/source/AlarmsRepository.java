package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmsRemoteDataSource mAlarmRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRepository.class.getSimpleName();
        pq7.b(simpleName, "AlarmsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRepository(AlarmsRemoteDataSource alarmsRemoteDataSource) {
        pq7.c(alarmsRemoteDataSource, "mAlarmRemoteDataSource");
        this.mAlarmRemoteDataSource = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new AlarmsRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object deleteAlarm(Alarm alarm, qn7<? super iq5<? extends Void>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$deleteAlarm$Anon2(this, alarm, null), qn7);
    }

    @DexIgnore
    public final Object downloadAlarms(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new AlarmsRepository$downloadAlarms$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object executePendingRequest(qn7<? super Boolean> qn7) {
        Object g;
        synchronized (this) {
            g = eu7.g(bw7.b(), new AlarmsRepository$executePendingRequest$Anon2(this, null), qn7);
        }
        return g;
    }

    @DexIgnore
    public final Object findIncomingActiveAlarm(qn7<? super Alarm> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$findIncomingActiveAlarm$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object findNextActiveAlarm(qn7<? super Alarm> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$findNextActiveAlarm$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object getActiveAlarms(qn7<? super List<Alarm>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$getActiveAlarms$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object getAlarmById(String str, qn7<? super Alarm> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$getAlarmById$Anon2(str, null), qn7);
    }

    @DexIgnore
    public final Object getAllAlarmIgnoreDeletePinType(qn7<? super List<Alarm>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object getInComingActiveAlarms(qn7<? super List<Alarm>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$getInComingActiveAlarms$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object insertAlarm(Alarm alarm, qn7<? super iq5<Alarm>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$insertAlarm$Anon2(this, alarm, null), qn7);
    }

    @DexIgnore
    public final Object updateAlarm(Alarm alarm, qn7<? super iq5<Alarm>> qn7) {
        return eu7.g(bw7.b(), new AlarmsRepository$updateAlarm$Anon2(this, alarm, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm r12, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.local.alarm.Alarm>> r13) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository.upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm, com.fossil.qn7):java.lang.Object");
    }
}
