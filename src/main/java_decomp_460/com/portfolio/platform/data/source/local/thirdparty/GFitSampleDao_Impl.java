package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSampleDao_Impl implements GFitSampleDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<GFitSample> __deletionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ jw0<GFitSample> __insertionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GFitSample> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitSample gFitSample) {
            px0.bindLong(1, (long) gFitSample.getId());
            px0.bindLong(2, (long) gFitSample.getStep());
            px0.bindDouble(3, (double) gFitSample.getDistance());
            px0.bindDouble(4, (double) gFitSample.getCalorie());
            px0.bindLong(5, gFitSample.getStartTime());
            px0.bindLong(6, gFitSample.getEndTime());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSample` (`id`,`step`,`distance`,`calorie`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<GFitSample> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitSample gFitSample) {
            px0.bindLong(1, (long) gFitSample.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `gFitSample` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM gFitSample";
        }
    }

    @DexIgnore
    public GFitSampleDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGFitSample = new Anon1(qw0);
        this.__deletionAdapterOfGFitSample = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void deleteListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public List<GFitSample> getAllGFitSample() {
        tw0 f = tw0.f("SELECT * FROM gFitSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "step");
            int c3 = dx0.c(b, "distance");
            int c4 = dx0.c(b, "calorie");
            int c5 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c6 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitSample gFitSample = new GFitSample(b.getInt(c2), b.getFloat(c3), b.getFloat(c4), b.getLong(c5), b.getLong(c6));
                gFitSample.setId(b.getInt(c));
                arrayList.add(gFitSample);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertGFitSample(GFitSample gFitSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert((jw0<GFitSample>) gFitSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
