package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.on5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ on5 mSharedPreferencesManager;
    @DexIgnore
    public /* final */ UserRemoteDataSource mUserRemoteDataSource;
    @DexIgnore
    public /* final */ UserSettingDao mUserSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRepository.class.getSimpleName();
        pq7.b(simpleName, "UserRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRepository(UserRemoteDataSource userRemoteDataSource, UserSettingDao userSettingDao, on5 on5) {
        pq7.c(userRemoteDataSource, "mUserRemoteDataSource");
        pq7.c(userSettingDao, "mUserSettingDao");
        pq7.c(on5, "mSharedPreferencesManager");
        this.mUserRemoteDataSource = userRemoteDataSource;
        this.mUserSettingDao = userSettingDao;
        this.mSharedPreferencesManager = on5;
    }

    @DexIgnore
    public final Object checkAuthenticationEmailExisting(String str, qn7<? super iq5<Boolean>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$checkAuthenticationEmailExisting$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object checkAuthenticationSocialExisting(String str, String str2, qn7<? super iq5<Boolean>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$checkAuthenticationSocialExisting$Anon2(this, str, str2, null), qn7);
    }

    @DexIgnore
    public final Object clearAllUser(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new UserRepository$clearAllUser$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final void clearUserSettings() {
        this.mUserSettingDao.cleanUp();
    }

    @DexIgnore
    public final Object deleteUser(qn7<? super Integer> qn7) {
        return eu7.g(bw7.b(), new UserRepository$deleteUser$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getCurrentUser(qn7<? super MFUser> qn7) {
        return eu7.g(bw7.b(), new UserRepository$getCurrentUser$Anon2(null), qn7);
    }

    @DexIgnore
    public final UserSettings getUserSetting() {
        return this.mUserSettingDao.getCurrentUserSetting();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getUserSettingFromServer(com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.model.UserSettings>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            if (r0 == 0) goto L_0x0043
            r0 = r6
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0051
            if (r3 != r4) goto L_0x0049
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.el7.b(r1)
            r5 = r0
        L_0x0027:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0062
            r1 = r0
            com.fossil.kq5 r1 = (com.fossil.kq5) r1
            java.lang.Object r2 = r1.a()
            if (r2 == 0) goto L_0x0042
            com.portfolio.platform.data.source.UserSettingDao r2 = r5.mUserSettingDao
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            r2.addOrUpdateUserSetting(r1)
        L_0x0042:
            return r0
        L_0x0043:
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r1 = r5.mUserRemoteDataSource
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.getUserSettingFromServer(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0042
        L_0x0062:
            boolean r1 = r0 instanceof com.fossil.hq5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.getUserSettingFromServer(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertUser(MFUser mFUser, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new UserRepository$insertUser$Anon2(mFUser, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final void insertUserSetting(UserSettings userSettings) {
        pq7.c(userSettings, "userSettings");
        this.mUserSettingDao.addOrUpdateUserSetting(userSettings);
    }

    @DexIgnore
    public final Object loadUserInfo(qn7<? super iq5<? extends MFUser>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$loadUserInfo$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object loginEmail(String str, String str2, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$loginEmail$Anon2(this, str, str2, null), qn7);
    }

    @DexIgnore
    public final Object loginWithSocial(String str, String str2, String str3, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$loginWithSocial$Anon2(this, str, str2, str3, null), qn7);
    }

    @DexIgnore
    public final Object logoutUser(qn7<? super Integer> qn7) {
        return eu7.g(bw7.b(), new UserRepository$logoutUser$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object pushPendingUser(MFUser mFUser, qn7<? super iq5<MFUser>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$pushPendingUser$Anon2(this, mFUser, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object pushPendingUserSetting(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            if (r0 == 0) goto L_0x002e
            r0 = r9
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x003d
            if (r3 != r7) goto L_0x0035
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.el7.b(r2)
        L_0x002b:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x002d:
            return r0
        L_0x002e:
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0014
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003d:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.UserSettingDao r2 = r8.mUserSettingDao
            com.portfolio.platform.data.model.UserSettings r2 = r2.getPendingUserSetting()
            if (r2 == 0) goto L_0x002b
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "pushPendingUserSetting(), userSetting="
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            r1.L$0 = r8
            r1.L$1 = r2
            r1.label = r7
            java.lang.Object r1 = r8.sendUserSettingToServer(r2, r1)
            if (r1 != r0) goto L_0x002b
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.pushPendingUserSetting(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object requestEmailOtp(String str, qn7<? super iq5<Void>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$requestEmailOtp$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object resetPassword(String str, qn7<? super iq5<Integer>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$resetPassword$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object sendUserSettingToServer(UserSettings userSettings, qn7<? super iq5<UserSettings>> qn7) {
        return this.mUserRemoteDataSource.sendUserSettingToServer(userSettings, qn7);
    }

    @DexIgnore
    public final Object signUpEmail(SignUpEmailAuth signUpEmailAuth, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$signUpEmail$Anon2(this, signUpEmailAuth, null), qn7);
    }

    @DexIgnore
    public final Object signUpSocial(SignUpSocialAuth signUpSocialAuth, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$signUpSocial$Anon2(this, signUpSocialAuth, null), qn7);
    }

    @DexIgnore
    public final Object updateUser(MFUser mFUser, boolean z, qn7<? super iq5<? extends MFUser>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$updateUser$Anon2(this, z, mFUser, null), qn7);
    }

    @DexIgnore
    public final Object verifyEmailOtp(String str, String str2, qn7<? super iq5<Void>> qn7) {
        return eu7.g(bw7.b(), new UserRepository$verifyEmailOtp$Anon2(this, str, str2, null), qn7);
    }
}
