package com.portfolio.platform.data.source;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1 extends qq7 implements rp7<GoalTrackingData, Boolean> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1 INSTANCE; // = new GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1();

    @DexIgnore
    public GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ Boolean invoke(GoalTrackingData goalTrackingData) {
        return Boolean.valueOf(invoke(goalTrackingData));
    }

    @DexIgnore
    public final boolean invoke(GoalTrackingData goalTrackingData) {
        pq7.c(goalTrackingData, "it");
        return goalTrackingData.getPinType() == 1;
    }
}
