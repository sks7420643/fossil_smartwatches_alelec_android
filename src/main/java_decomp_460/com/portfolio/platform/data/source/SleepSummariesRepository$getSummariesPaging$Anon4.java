package com.portfolio.platform.data.source;

import com.fossil.fl5;
import com.fossil.gp7;
import com.fossil.qq7;
import com.fossil.tl7;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon4 extends qq7 implements gp7<tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$Anon4(SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    @Override // com.fossil.gp7
    public final void invoke() {
        fl5 mHelper;
        SleepSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
        if (e != null && (mHelper = e.getMHelper()) != null) {
            mHelper.g();
        }
    }
}
