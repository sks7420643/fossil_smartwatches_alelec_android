package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSleepDao_Impl implements GFitSleepDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<GFitSleep> __deletionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ jw0<GFitSleep> __insertionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GFitSleep> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitSleep gFitSleep) {
            px0.bindLong(1, (long) gFitSleep.getId());
            px0.bindLong(2, (long) gFitSleep.getSleepMins());
            px0.bindLong(3, gFitSleep.getStartTime());
            px0.bindLong(4, gFitSleep.getEndTime());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSleep` (`id`,`sleepMins`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<GFitSleep> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitSleep gFitSleep) {
            px0.bindLong(1, (long) gFitSleep.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `gFitSleep` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM GFitSleep";
        }
    }

    @DexIgnore
    public GFitSleepDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGFitSleep = new Anon1(qw0);
        this.__deletionAdapterOfGFitSleep = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void deleteListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSleep.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public List<GFitSleep> getAllGFitSleep() {
        tw0 f = tw0.f("SELECT * FROM gFitSleep", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "sleepMins");
            int c3 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c4 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitSleep gFitSleep = new GFitSleep(b.getInt(c2), b.getLong(c3), b.getLong(c4));
                gFitSleep.setId(b.getInt(c));
                arrayList.add(gFitSleep);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertGFitSleep(GFitSleep gFitSleep) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert((jw0<GFitSleep>) gFitSleep);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
