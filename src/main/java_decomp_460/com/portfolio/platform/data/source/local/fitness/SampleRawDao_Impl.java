package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.lz4;
import com.fossil.px0;
import com.fossil.pz4;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.xw0;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ lz4 __activityIntensitiesConverter; // = new lz4();
    @DexIgnore
    public /* final */ pz4 __dateLongStringConverter; // = new pz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<SampleRaw> __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<SampleRaw> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, sampleRaw.getId());
            }
            px0.bindLong(2, (long) sampleRaw.getPinType());
            px0.bindLong(3, (long) sampleRaw.getUaPinType());
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a2 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a2);
            }
            String a3 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a3 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a3);
            }
            if (sampleRaw.getSourceId() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, sampleRaw.getMovementTypeValue());
            }
            px0.bindDouble(9, sampleRaw.getSteps());
            px0.bindDouble(10, sampleRaw.getCalories());
            px0.bindDouble(11, sampleRaw.getDistance());
            px0.bindLong(12, (long) sampleRaw.getActiveTime());
            String a4 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a4 == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, a4);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, sampleRaw.getTimeZoneID());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw` (`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfSampleRaw = new Anon1(qw0);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        tw0 f = tw0.f("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "pinType");
            int c3 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples() {
        tw0 f = tw0.f("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "pinType");
            int c3 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a2 = this.__dateLongStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateLongStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "pinType");
            int c3 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
