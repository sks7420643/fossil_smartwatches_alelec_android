package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.pq7;
import com.fossil.rj4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteWorkoutSetting {
    @DexIgnore
    @rj4("autoDetectWorkout")
    public /* final */ List<WorkoutSetting> autoDetectWorkout;

    @DexIgnore
    public RemoteWorkoutSetting(List<WorkoutSetting> list) {
        pq7.c(list, "autoDetectWorkout");
        this.autoDetectWorkout = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ RemoteWorkoutSetting copy$default(RemoteWorkoutSetting remoteWorkoutSetting, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = remoteWorkoutSetting.autoDetectWorkout;
        }
        return remoteWorkoutSetting.copy(list);
    }

    @DexIgnore
    public final List<WorkoutSetting> component1() {
        return this.autoDetectWorkout;
    }

    @DexIgnore
    public final RemoteWorkoutSetting copy(List<WorkoutSetting> list) {
        pq7.c(list, "autoDetectWorkout");
        return new RemoteWorkoutSetting(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof RemoteWorkoutSetting) && pq7.a(this.autoDetectWorkout, ((RemoteWorkoutSetting) obj).autoDetectWorkout));
    }

    @DexIgnore
    public final List<WorkoutSetting> getAutoDetectWorkout() {
        return this.autoDetectWorkout;
    }

    @DexIgnore
    public int hashCode() {
        List<WorkoutSetting> list = this.autoDetectWorkout;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "RemoteWorkoutSetting(autoDetectWorkout=" + this.autoDetectWorkout + ")";
    }
}
