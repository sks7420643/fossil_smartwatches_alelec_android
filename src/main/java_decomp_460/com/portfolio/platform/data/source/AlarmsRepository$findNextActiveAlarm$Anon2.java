package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$findNextActiveAlarm$2", f = "AlarmsRepository.kt", l = {34}, m = "invokeSuspend")
public final class AlarmsRepository$findNextActiveAlarm$Anon2 extends ko7 implements vp7<iv7, qn7<? super Alarm>, Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public AlarmsRepository$findNextActiveAlarm$Anon2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$findNextActiveAlarm$Anon2 alarmsRepository$findNextActiveAlarm$Anon2 = new AlarmsRepository$findNextActiveAlarm$Anon2(qn7);
        alarmsRepository$findNextActiveAlarm$Anon2.p$ = (iv7) obj;
        return alarmsRepository$findNextActiveAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Alarm> qn7) {
        return ((AlarmsRepository$findNextActiveAlarm$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        int i;
        Object u;
        Object d = yn7.d();
        int i2 = this.label;
        if (i2 == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            Calendar instance = Calendar.getInstance();
            i = (instance.get(11) * 60) + instance.get(12);
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.L$1 = instance;
            this.I$0 = i;
            this.label = 1;
            u = bn5.u(this);
            if (u == d) {
                return d;
            }
        } else if (i2 == 1) {
            i = this.I$0;
            Calendar calendar = (Calendar) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            u = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((AlarmDatabase) u).alarmDao().getInComingActiveAlarm(i);
    }
}
