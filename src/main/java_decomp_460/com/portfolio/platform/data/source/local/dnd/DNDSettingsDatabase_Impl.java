package com.portfolio.platform.data.source.local.dnd;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDSettingsDatabase_Impl extends DNDSettingsDatabase {
    @DexIgnore
    public volatile DNDScheduledTimeDao _dNDScheduledTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `dndScheduledTimeModel` (`scheduledTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `scheduledTimeType` INTEGER NOT NULL, PRIMARY KEY(`scheduledTimeName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'edbea9a651a6427903efe74c61fd6b87')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `dndScheduledTimeModel`");
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            DNDSettingsDatabase_Impl.this.mDatabase = lx0;
            DNDSettingsDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("scheduledTimeName", new ix0.a("scheduledTimeName", "TEXT", true, 1, null, 1));
            hashMap.put("minutes", new ix0.a("minutes", "INTEGER", true, 0, null, 1));
            hashMap.put("scheduledTimeType", new ix0.a("scheduledTimeType", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("dndScheduledTimeModel", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "dndScheduledTimeModel");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "dndScheduledTimeModel(com.portfolio.platform.data.model.DNDScheduledTimeModel).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `dndScheduledTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "dndScheduledTimeModel");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "edbea9a651a6427903efe74c61fd6b87", "1a7527e5c35fa2c8a6aef70719b78d76");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase
    public DNDScheduledTimeDao getDNDScheduledTimeDao() {
        DNDScheduledTimeDao dNDScheduledTimeDao;
        if (this._dNDScheduledTimeDao != null) {
            return this._dNDScheduledTimeDao;
        }
        synchronized (this) {
            if (this._dNDScheduledTimeDao == null) {
                this._dNDScheduledTimeDao = new DNDScheduledTimeDao_Impl(this);
            }
            dNDScheduledTimeDao = this._dNDScheduledTimeDao;
        }
        return dNDScheduledTimeDao;
    }
}
