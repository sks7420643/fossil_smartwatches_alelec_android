package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticAwait$2", f = "SummariesRepository.kt", l = {437, 437}, m = "invokeSuspend")
public final class SummariesRepository$getActivityStatisticAwait$Anon2 extends ko7 implements vp7<iv7, qn7<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getActivityStatisticAwait$Anon2(SummariesRepository summariesRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getActivityStatisticAwait$Anon2 summariesRepository$getActivityStatisticAwait$Anon2 = new SummariesRepository$getActivityStatisticAwait$Anon2(this.this$0, qn7);
        summariesRepository$getActivityStatisticAwait$Anon2.p$ = (iv7) obj;
        return summariesRepository$getActivityStatisticAwait$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super ActivityStatistic> qn7) {
        return ((SummariesRepository$getActivityStatisticAwait$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r7.label
            if (r0 == 0) goto L_0x003d
            if (r0 == r5) goto L_0x0021
            if (r0 != r6) goto L_0x0019
            java.lang.Object r0 = r7.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r8)
            r0 = r8
        L_0x0016:
            com.portfolio.platform.data.ActivityStatistic r0 = (com.portfolio.platform.data.ActivityStatistic) r0
        L_0x0018:
            return r0
        L_0x0019:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r7.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r8)
            r2 = r0
            r1 = r8
        L_0x002a:
            r0 = r1
            com.portfolio.platform.data.ActivityStatistic r0 = (com.portfolio.platform.data.ActivityStatistic) r0
            if (r0 != 0) goto L_0x0018
            com.portfolio.platform.data.source.SummariesRepository r0 = r7.this$0
            r7.L$0 = r2
            r7.label = r6
            java.lang.Object r0 = r0.fetchActivityStatistic(r7)
            if (r0 != r3) goto L_0x0016
            r0 = r3
            goto L_0x0018
        L_0x003d:
            com.fossil.el7.b(r8)
            com.fossil.iv7 r0 = r7.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "SummariesRepository"
            java.lang.String r4 = "getActivityStatisticAwait"
            r1.d(r2, r4)
            com.portfolio.platform.data.source.SummariesRepository r1 = r7.this$0
            r7.L$0 = r0
            r7.label = r5
            java.lang.Object r1 = r1.getActivityStatisticDB(r7)
            if (r1 != r3) goto L_0x005d
            r0 = r3
            goto L_0x0018
        L_0x005d:
            r2 = r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticAwait$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
