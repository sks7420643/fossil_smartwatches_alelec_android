package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.cl7;
import com.fossil.fl5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.xw7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource$loadAfter$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        cl7 cl7 = (cl7) pm7.F(this.this$0.mRequestAfterQueue);
        pq7.b(aVar, "helperCallback");
        xw7 unused = this.this$0.loadData(fl5.d.AFTER, (Date) cl7.getFirst(), (Date) cl7.getSecond(), aVar);
    }
}
