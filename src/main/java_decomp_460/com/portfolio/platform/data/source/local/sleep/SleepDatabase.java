package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ax0;
import com.fossil.kq7;
import com.fossil.qw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SleepDatabase extends qw0 {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_2_TO_5; // = new SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(2, 5);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_3_TO_9; // = new SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$Anon1(3, 9);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_5$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_3_TO_9$annotations() {
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_2_TO_5() {
            return SleepDatabase.MIGRATION_FROM_2_TO_5;
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_3_TO_9() {
            return SleepDatabase.MIGRATION_FROM_3_TO_9;
        }
    }

    @DexIgnore
    public abstract SleepDao sleepDao();
}
