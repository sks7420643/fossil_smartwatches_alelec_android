package com.portfolio.platform.data.source.remote;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.CategoryRemoteDataSource", f = "CategoryRemoteDataSource.kt", l = {12}, m = "getAllCategory")
public final class CategoryRemoteDataSource$getAllCategory$Anon1 extends co7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ CategoryRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CategoryRemoteDataSource$getAllCategory$Anon1(CategoryRemoteDataSource categoryRemoteDataSource, qn7 qn7) {
        super(qn7);
        this.this$0 = categoryRemoteDataSource;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.getAllCategory(this);
    }
}
