package com.portfolio.platform.data.source;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaWatchFaceRingRepository";
    @DexIgnore
    public /* final */ DianaRingRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public DianaWatchFaceRingRepository(DianaRingRemoteDataSource dianaRingRemoteDataSource) {
        pq7.c(dianaRingRemoteDataSource, "mRemoteSource");
        this.mRemoteSource = dianaRingRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaWatchFaceRingRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x002d
            r0 = r7
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002d
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0057
            if (r0 == r4) goto L_0x003c
            if (r0 != r5) goto L_0x0034
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r0
            com.fossil.el7.b(r1)
        L_0x002a:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x002c:
            return r0
        L_0x002d:
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRingRepository$cleanUp$Anon1
            r0.<init>(r6, r7)
            r2 = r0
            goto L_0x0015
        L_0x0034:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003c:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0044:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao r0 = r0.getDianaWatchFaceRingDao()
            r2.L$0 = r6
            r2.label = r5
            java.lang.Object r0 = r0.clearAll(r2)
            if (r0 != r3) goto L_0x002a
            r0 = r3
            goto L_0x002c
        L_0x0057:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r0 = com.fossil.bn5.j
            r2.L$0 = r6
            r2.label = r4
            java.lang.Object r1 = r0.v(r2)
            if (r1 != r3) goto L_0x0044
            r0 = r3
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRingRepository.cleanUp(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadDianaWatchFaceRings(com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRingRepository.downloadDianaWatchFaceRings(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllRings(com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.watchface.DianaWatchFaceRing>> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getAllRings$Anon1
            if (r0 == 0) goto L_0x002b
            r0 = r7
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getAllRings$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getAllRings$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002b
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0055
            if (r0 == r4) goto L_0x003a
            if (r0 != r5) goto L_0x0032
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r0
            com.fossil.el7.b(r1)
        L_0x002a:
            return r1
        L_0x002b:
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getAllRings$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getAllRings$Anon1
            r0.<init>(r6, r7)
            r2 = r0
            goto L_0x0015
        L_0x0032:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003a:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0042:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao r0 = r0.getDianaWatchFaceRingDao()
            r2.L$0 = r6
            r2.label = r5
            java.lang.Object r1 = r0.getAllDianaWatchFaceRings(r2)
            if (r1 != r3) goto L_0x002a
            r1 = r3
            goto L_0x002a
        L_0x0055:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r0 = com.fossil.bn5.j
            r2.L$0 = r6
            r2.label = r4
            java.lang.Object r1 = r0.v(r2)
            if (r1 != r3) goto L_0x0042
            r1 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRingRepository.getAllRings(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getDianaWatchFaceRing(java.lang.String r8, com.fossil.qn7<? super com.portfolio.platform.data.model.watchface.DianaWatchFaceRing> r9) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getDianaWatchFaceRing$Anon1
            if (r0 == 0) goto L_0x002f
            r0 = r9
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getDianaWatchFaceRing$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getDianaWatchFaceRing$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002f
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x005f
            if (r0 == r5) goto L_0x003e
            if (r0 != r6) goto L_0x0036
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r0
            com.fossil.el7.b(r2)
        L_0x002e:
            return r2
        L_0x002f:
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getDianaWatchFaceRing$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRingRepository$getDianaWatchFaceRing$Anon1
            r0.<init>(r7, r9)
            r3 = r0
            goto L_0x0015
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRingRepository r1 = (com.portfolio.platform.data.source.DianaWatchFaceRingRepository) r1
            com.fossil.el7.b(r2)
            r8 = r0
        L_0x004a:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao r0 = r0.getDianaWatchFaceRingDao()
            r3.L$0 = r1
            r3.L$1 = r8
            r3.label = r6
            java.lang.Object r2 = r0.getDianaWatchFaceRing(r8, r3)
            if (r2 != r4) goto L_0x002e
            r2 = r4
            goto L_0x002e
        L_0x005f:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r0 = com.fossil.bn5.j
            r3.L$0 = r7
            r3.L$1 = r8
            r3.label = r5
            java.lang.Object r2 = r0.v(r3)
            if (r2 != r4) goto L_0x0072
            r2 = r4
            goto L_0x002e
        L_0x0072:
            r1 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRingRepository.getDianaWatchFaceRing(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
