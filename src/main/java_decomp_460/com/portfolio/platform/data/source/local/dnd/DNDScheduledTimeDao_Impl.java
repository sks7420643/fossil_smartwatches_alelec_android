package com.portfolio.platform.data.source.local.dnd;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDScheduledTimeDao_Impl implements DNDScheduledTimeDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DNDScheduledTimeModel> __insertionAdapterOfDNDScheduledTimeModel;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DNDScheduledTimeModel> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DNDScheduledTimeModel dNDScheduledTimeModel) {
            if (dNDScheduledTimeModel.getScheduledTimeName() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dNDScheduledTimeModel.getScheduledTimeName());
            }
            px0.bindLong(2, (long) dNDScheduledTimeModel.getMinutes());
            px0.bindLong(3, (long) dNDScheduledTimeModel.getScheduledTimeType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dndScheduledTimeModel` (`scheduledTimeName`,`minutes`,`scheduledTimeType`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dndScheduledTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DNDScheduledTimeModel> call() throws Exception {
            Cursor b = ex0.b(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "scheduledTimeName");
                int c2 = dx0.c(b, "minutes");
                int c3 = dx0.c(b, "scheduledTimeType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<DNDScheduledTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DNDScheduledTimeModel call() throws Exception {
            DNDScheduledTimeModel dNDScheduledTimeModel = null;
            Cursor b = ex0.b(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "scheduledTimeName");
                int c2 = dx0.c(b, "minutes");
                int c3 = dx0.c(b, "scheduledTimeType");
                if (b.moveToFirst()) {
                    dNDScheduledTimeModel = new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
                }
                return dNDScheduledTimeModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DNDScheduledTimeDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDNDScheduledTimeModel = new Anon1(qw0);
        this.__preparedStmtOfDelete = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i) {
        DNDScheduledTimeModel dNDScheduledTimeModel = null;
        tw0 f = tw0.f("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "scheduledTimeName");
            int c2 = dx0.c(b, "minutes");
            int c3 = dx0.c(b, "scheduledTimeType");
            if (b.moveToFirst()) {
                dNDScheduledTimeModel = new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
            }
            return dNDScheduledTimeModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i) {
        tw0 f = tw0.f("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        f.bindLong(1, (long) i);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"dndScheduledTimeModel"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime() {
        tw0 f = tw0.f("SELECT * FROM dndScheduledTimeModel", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"dndScheduledTimeModel"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public List<DNDScheduledTimeModel> getListDNDScheduledTimeModel() {
        tw0 f = tw0.f("SELECT * FROM dndScheduledTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "scheduledTimeName");
            int c2 = dx0.c(b, "minutes");
            int c3 = dx0.c(b, "scheduledTimeType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert((jw0<DNDScheduledTimeModel>) dNDScheduledTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
