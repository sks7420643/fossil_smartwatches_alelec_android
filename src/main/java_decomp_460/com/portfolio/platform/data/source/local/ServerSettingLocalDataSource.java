package com.portfolio.platform.data.source.local;

import com.fossil.mn5;
import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingLocalDataSource implements ServerSettingDataSource {
    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        mn5.p.a().n().addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        mn5.p.a().n().k(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void clearData() {
        mn5.p.a().n().a();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public ServerSetting getServerSettingByKey(String str) {
        pq7.c(str, "key");
        return mn5.p.a().n().getServerSettingByKey(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        pq7.c(onGetServerSettingList, Constants.CALLBACK);
    }
}
