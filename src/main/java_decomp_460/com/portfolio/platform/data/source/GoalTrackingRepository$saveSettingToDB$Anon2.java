package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$saveSettingToDB$2", f = "GoalTrackingRepository.kt", l = {136}, m = "invokeSuspend")
public final class GoalTrackingRepository$saveSettingToDB$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalSetting $goalSetting;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$saveSettingToDB$Anon2(GoalSetting goalSetting, qn7 qn7) {
        super(2, qn7);
        this.$goalSetting = goalSetting;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$saveSettingToDB$Anon2 goalTrackingRepository$saveSettingToDB$Anon2 = new GoalTrackingRepository$saveSettingToDB$Anon2(this.$goalSetting, qn7);
        goalTrackingRepository$saveSettingToDB$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$saveSettingToDB$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((GoalTrackingRepository$saveSettingToDB$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object A;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            A = bn5.A(this);
            if (A == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            A = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDatabase goalTrackingDatabase = (GoalTrackingDatabase) A;
        goalTrackingDatabase.getGoalTrackingDao().upsertGoalSettings(this.$goalSetting);
        GoalTrackingSummary goalTrackingSummary = goalTrackingDatabase.getGoalTrackingDao().getGoalTrackingSummary(new Date());
        if (goalTrackingSummary == null) {
            goalTrackingSummary = new GoalTrackingSummary(new Date(), 0, this.$goalSetting.getCurrentTarget(), new Date().getTime(), new Date().getTime());
        } else {
            goalTrackingSummary.setGoalTarget(this.$goalSetting.getCurrentTarget());
        }
        goalTrackingDatabase.getGoalTrackingDao().upsertGoalTrackingSummary(goalTrackingSummary);
        return tl7.f3441a;
    }
}
