package com.portfolio.platform.data.source.local.fitness;

import com.facebook.places.PlaceManager;
import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.wearables.fsl.fitness.ActivitySettings;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.FitnessDataDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao_Impl;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase_Impl extends FitnessDatabase {
    @DexIgnore
    public volatile ActivitySampleDao _activitySampleDao;
    @DexIgnore
    public volatile ActivitySummaryDao _activitySummaryDao;
    @DexIgnore
    public volatile FitnessDataDao _fitnessDataDao;
    @DexIgnore
    public volatile HeartRateDailySummaryDao _heartRateDailySummaryDao;
    @DexIgnore
    public volatile HeartRateSampleDao _heartRateSampleDao;
    @DexIgnore
    public volatile SampleRawDao _sampleRawDao;
    @DexIgnore
    public volatile WorkoutDao _workoutDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sampleraw` (`id` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `uaPinType` INTEGER NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `sourceId` TEXT NOT NULL, `sourceTypeValue` TEXT NOT NULL, `movementTypeValue` TEXT, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneID` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `activity_sample` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneOffsetInSecond` INTEGER NOT NULL, `sourceId` TEXT NOT NULL, `syncTime` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sampleday` (`createdAt` INTEGER, `updatedAt` INTEGER, `pinType` INTEGER NOT NULL, `year` INTEGER NOT NULL, `month` INTEGER NOT NULL, `day` INTEGER NOT NULL, `timezoneName` TEXT, `dstOffset` INTEGER, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `intensities` TEXT NOT NULL, `activeTime` INTEGER NOT NULL, `stepGoal` INTEGER NOT NULL, `caloriesGoal` INTEGER NOT NULL, `activeTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`year`, `month`, `day`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `activitySettings` (`id` INTEGER NOT NULL, `currentStepGoal` INTEGER NOT NULL, `currentCaloriesGoal` INTEGER NOT NULL, `currentActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `activityRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedStepsGoal` INTEGER NOT NULL, `recommendedCaloriesGoal` INTEGER NOT NULL, `recommendedActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `workout_session` (`speed` TEXT, `screenShotUri` TEXT, `states` TEXT NOT NULL, `id` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `deviceSerialNumber` TEXT, `step` TEXT, `calorie` TEXT, `distance` TEXT, `heartRate` TEXT, `sourceType` TEXT, `workoutType` TEXT, `timezoneOffset` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `workoutGpsPoints` TEXT, `gpsDataPoints` TEXT, `mode` TEXT, `pace` TEXT, `cadence` TEXT, `editedStartTime` TEXT NOT NULL, `editedEndTime` TEXT NOT NULL, `editedType` TEXT, `editedMode` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `fitness_data` (`step` TEXT NOT NULL, `activeMinute` TEXT NOT NULL, `calorie` TEXT NOT NULL, `distance` TEXT NOT NULL, `stress` TEXT, `resting` TEXT NOT NULL, `heartRate` TEXT, `sleeps` TEXT NOT NULL, `workouts` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `syncTime` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`startTime`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `heart_rate_sample` (`id` TEXT NOT NULL, `average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `endTime` TEXT NOT NULL, `startTime` TEXT NOT NULL, `timezoneOffset` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `daily_heart_rate_summary` (`average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`date`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `activity_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `activeTimeBestDay` TEXT, `activeTimeBestStreak` TEXT, `caloriesBestDay` TEXT, `caloriesBestStreak` TEXT, `stepsBestDay` TEXT, `stepsBestStreak` TEXT, `totalActiveTime` INTEGER NOT NULL, `totalCalories` REAL NOT NULL, `totalDays` INTEGER NOT NULL, `totalDistance` REAL NOT NULL, `totalSteps` INTEGER NOT NULL, `totalIntensityDistInStep` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'abe564b531b636e9dfd52068284844ef')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `sampleraw`");
            lx0.execSQL("DROP TABLE IF EXISTS `activity_sample`");
            lx0.execSQL("DROP TABLE IF EXISTS `sampleday`");
            lx0.execSQL("DROP TABLE IF EXISTS `activitySettings`");
            lx0.execSQL("DROP TABLE IF EXISTS `activityRecommendedGoals`");
            lx0.execSQL("DROP TABLE IF EXISTS `workout_session`");
            lx0.execSQL("DROP TABLE IF EXISTS `fitness_data`");
            lx0.execSQL("DROP TABLE IF EXISTS `heart_rate_sample`");
            lx0.execSQL("DROP TABLE IF EXISTS `daily_heart_rate_summary`");
            lx0.execSQL("DROP TABLE IF EXISTS `activity_statistic`");
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            FitnessDatabase_Impl.this.mDatabase = lx0;
            FitnessDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(14);
            hashMap.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_UA_PIN_TYPE, new ix0.a(SampleRaw.COLUMN_UA_PIN_TYPE, "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_SOURCE_ID, new ix0.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, new ix0.a(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, new ix0.a(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, "TEXT", false, 0, null, 1));
            hashMap.put("steps", new ix0.a("steps", "REAL", true, 0, null, 1));
            hashMap.put("calories", new ix0.a("calories", "REAL", true, 0, null, 1));
            hashMap.put("distance", new ix0.a("distance", "REAL", true, 0, null, 1));
            hashMap.put(SampleDay.COLUMN_ACTIVE_TIME, new ix0.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put("intensityDistInSteps", new ix0.a("intensityDistInSteps", "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_TIMEZONE_ID, new ix0.a(SampleRaw.COLUMN_TIMEZONE_ID, "TEXT", false, 0, null, 1));
            ix0 ix0 = new ix0(SampleRaw.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, SampleRaw.TABLE_NAME);
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "sampleraw(com.portfolio.platform.data.model.room.fitness.SampleRaw).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(15);
            hashMap2.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap2.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap2.put("date", new ix0.a("date", "TEXT", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap2.put("steps", new ix0.a("steps", "REAL", true, 0, null, 1));
            hashMap2.put("calories", new ix0.a("calories", "REAL", true, 0, null, 1));
            hashMap2.put("distance", new ix0.a("distance", "REAL", true, 0, null, 1));
            hashMap2.put(SampleDay.COLUMN_ACTIVE_TIME, new ix0.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap2.put("intensityDistInSteps", new ix0.a("intensityDistInSteps", "TEXT", true, 0, null, 1));
            hashMap2.put("timeZoneOffsetInSecond", new ix0.a("timeZoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_SOURCE_ID, new ix0.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0, null, 1));
            hashMap2.put("syncTime", new ix0.a("syncTime", "INTEGER", true, 0, null, 1));
            hashMap2.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap2.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            ix0 ix02 = new ix0(ActivitySample.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, ActivitySample.TABLE_NAME);
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "activity_sample(com.portfolio.platform.data.model.room.fitness.ActivitySample).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(16);
            hashMap3.put("createdAt", new ix0.a("createdAt", "INTEGER", false, 0, null, 1));
            hashMap3.put("updatedAt", new ix0.a("updatedAt", "INTEGER", false, 0, null, 1));
            hashMap3.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap3.put("year", new ix0.a("year", "INTEGER", true, 1, null, 1));
            hashMap3.put("month", new ix0.a("month", "INTEGER", true, 2, null, 1));
            hashMap3.put("day", new ix0.a("day", "INTEGER", true, 3, null, 1));
            hashMap3.put(SampleDay.COLUMN_TIMEZONE_NAME, new ix0.a(SampleDay.COLUMN_TIMEZONE_NAME, "TEXT", false, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_DST_OFFSET, new ix0.a(SampleDay.COLUMN_DST_OFFSET, "INTEGER", false, 0, null, 1));
            hashMap3.put("steps", new ix0.a("steps", "REAL", true, 0, null, 1));
            hashMap3.put("calories", new ix0.a("calories", "REAL", true, 0, null, 1));
            hashMap3.put("distance", new ix0.a("distance", "REAL", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_INTENSITIES, new ix0.a(SampleDay.COLUMN_INTENSITIES, "TEXT", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME, new ix0.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_STEP_GOAL, new ix0.a(SampleDay.COLUMN_STEP_GOAL, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_CALORIES_GOAL, new ix0.a(SampleDay.COLUMN_CALORIES_GOAL, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME_GOAL, new ix0.a(SampleDay.COLUMN_ACTIVE_TIME_GOAL, "INTEGER", true, 0, null, 1));
            ix0 ix03 = new ix0(SampleDay.TABLE_NAME, hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, SampleDay.TABLE_NAME);
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "sampleday(com.portfolio.platform.data.model.room.fitness.ActivitySummary).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(4);
            hashMap4.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, new ix0.a(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, "INTEGER", true, 0, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, new ix0.a(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, "INTEGER", true, 0, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, new ix0.a(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, "INTEGER", true, 0, null, 1));
            ix0 ix04 = new ix0(ActivitySettings.TABLE_NAME, hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, ActivitySettings.TABLE_NAME);
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "activitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(4);
            hashMap5.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("recommendedStepsGoal", new ix0.a("recommendedStepsGoal", "INTEGER", true, 0, null, 1));
            hashMap5.put("recommendedCaloriesGoal", new ix0.a("recommendedCaloriesGoal", "INTEGER", true, 0, null, 1));
            hashMap5.put("recommendedActiveTimeGoal", new ix0.a("recommendedActiveTimeGoal", "INTEGER", true, 0, null, 1));
            ix0 ix05 = new ix0("activityRecommendedGoals", hashMap5, new HashSet(0), new HashSet(0));
            ix0 a6 = ix0.a(lx0, "activityRecommendedGoals");
            if (!ix05.equals(a6)) {
                return new sw0.b(false, "activityRecommendedGoals(com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(27);
            hashMap6.put(PlaceManager.PARAM_SPEED, new ix0.a(PlaceManager.PARAM_SPEED, "TEXT", false, 0, null, 1));
            hashMap6.put("screenShotUri", new ix0.a("screenShotUri", "TEXT", false, 0, null, 1));
            hashMap6.put("states", new ix0.a("states", "TEXT", true, 0, null, 1));
            hashMap6.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("date", new ix0.a("date", "TEXT", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new ix0.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0, null, 1));
            hashMap6.put("step", new ix0.a("step", "TEXT", false, 0, null, 1));
            hashMap6.put("calorie", new ix0.a("calorie", "TEXT", false, 0, null, 1));
            hashMap6.put("distance", new ix0.a("distance", "TEXT", false, 0, null, 1));
            hashMap6.put("heartRate", new ix0.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap6.put("sourceType", new ix0.a("sourceType", "TEXT", false, 0, null, 1));
            hashMap6.put("workoutType", new ix0.a("workoutType", "TEXT", false, 0, null, 1));
            hashMap6.put("timezoneOffset", new ix0.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap6.put("duration", new ix0.a("duration", "INTEGER", true, 0, null, 1));
            hashMap6.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap6.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap6.put("workoutGpsPoints", new ix0.a("workoutGpsPoints", "TEXT", false, 0, null, 1));
            hashMap6.put("gpsDataPoints", new ix0.a("gpsDataPoints", "TEXT", false, 0, null, 1));
            hashMap6.put("mode", new ix0.a("mode", "TEXT", false, 0, null, 1));
            hashMap6.put("pace", new ix0.a("pace", "TEXT", false, 0, null, 1));
            hashMap6.put("cadence", new ix0.a("cadence", "TEXT", false, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_EDITED_START_TIME, new ix0.a(MFSleepSession.COLUMN_EDITED_START_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_EDITED_END_TIME, new ix0.a(MFSleepSession.COLUMN_EDITED_END_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put("editedType", new ix0.a("editedType", "TEXT", false, 0, null, 1));
            hashMap6.put("editedMode", new ix0.a("editedMode", "TEXT", false, 0, null, 1));
            ix0 ix06 = new ix0("workout_session", hashMap6, new HashSet(0), new HashSet(0));
            ix0 a7 = ix0.a(lx0, "workout_session");
            if (!ix06.equals(a7)) {
                return new sw0.b(false, "workout_session(com.portfolio.platform.data.model.diana.workout.WorkoutSession).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(14);
            hashMap7.put("step", new ix0.a("step", "TEXT", true, 0, null, 1));
            hashMap7.put("activeMinute", new ix0.a("activeMinute", "TEXT", true, 0, null, 1));
            hashMap7.put("calorie", new ix0.a("calorie", "TEXT", true, 0, null, 1));
            hashMap7.put("distance", new ix0.a("distance", "TEXT", true, 0, null, 1));
            hashMap7.put("stress", new ix0.a("stress", "TEXT", false, 0, null, 1));
            hashMap7.put("resting", new ix0.a("resting", "TEXT", true, 0, null, 1));
            hashMap7.put("heartRate", new ix0.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap7.put("sleeps", new ix0.a("sleeps", "TEXT", true, 0, null, 1));
            hashMap7.put("workouts", new ix0.a("workouts", "TEXT", true, 0, null, 1));
            hashMap7.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 1, null, 1));
            hashMap7.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap7.put("syncTime", new ix0.a("syncTime", "TEXT", true, 0, null, 1));
            hashMap7.put("timezoneOffsetInSecond", new ix0.a("timezoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap7.put("serialNumber", new ix0.a("serialNumber", "TEXT", true, 0, null, 1));
            ix0 ix07 = new ix0("fitness_data", hashMap7, new HashSet(0), new HashSet(0));
            ix0 a8 = ix0.a(lx0, "fitness_data");
            if (!ix07.equals(a8)) {
                return new sw0.b(false, "fitness_data(com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper).\n Expected:\n" + ix07 + "\n Found:\n" + a8);
            }
            HashMap hashMap8 = new HashMap(12);
            hashMap8.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap8.put(GoalTrackingSummary.COLUMN_AVERAGE, new ix0.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0, null, 1));
            hashMap8.put("date", new ix0.a("date", "TEXT", true, 0, null, 1));
            hashMap8.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap8.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap8.put("timezoneOffset", new ix0.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap8.put("min", new ix0.a("min", "INTEGER", true, 0, null, 1));
            hashMap8.put("max", new ix0.a("max", "INTEGER", true, 0, null, 1));
            hashMap8.put("minuteCount", new ix0.a("minuteCount", "INTEGER", true, 0, null, 1));
            hashMap8.put("resting", new ix0.a("resting", "TEXT", false, 0, null, 1));
            ix0 ix08 = new ix0("heart_rate_sample", hashMap8, new HashSet(0), new HashSet(0));
            ix0 a9 = ix0.a(lx0, "heart_rate_sample");
            if (!ix08.equals(a9)) {
                return new sw0.b(false, "heart_rate_sample(com.portfolio.platform.data.model.diana.heartrate.HeartRateSample).\n Expected:\n" + ix08 + "\n Found:\n" + a9);
            }
            HashMap hashMap9 = new HashMap(8);
            hashMap9.put(GoalTrackingSummary.COLUMN_AVERAGE, new ix0.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0, null, 1));
            hashMap9.put("date", new ix0.a("date", "TEXT", true, 1, null, 1));
            hashMap9.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("min", new ix0.a("min", "INTEGER", true, 0, null, 1));
            hashMap9.put("max", new ix0.a("max", "INTEGER", true, 0, null, 1));
            hashMap9.put("minuteCount", new ix0.a("minuteCount", "INTEGER", true, 0, null, 1));
            hashMap9.put("resting", new ix0.a("resting", "TEXT", false, 0, null, 1));
            ix0 ix09 = new ix0("daily_heart_rate_summary", hashMap9, new HashSet(0), new HashSet(0));
            ix0 a10 = ix0.a(lx0, "daily_heart_rate_summary");
            if (!ix09.equals(a10)) {
                return new sw0.b(false, "daily_heart_rate_summary(com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary).\n Expected:\n" + ix09 + "\n Found:\n" + a10);
            }
            HashMap hashMap10 = new HashMap(16);
            hashMap10.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap10.put("uid", new ix0.a("uid", "TEXT", true, 0, null, 1));
            hashMap10.put("activeTimeBestDay", new ix0.a("activeTimeBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("activeTimeBestStreak", new ix0.a("activeTimeBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("caloriesBestDay", new ix0.a("caloriesBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("caloriesBestStreak", new ix0.a("caloriesBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("stepsBestDay", new ix0.a("stepsBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("stepsBestStreak", new ix0.a("stepsBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("totalActiveTime", new ix0.a("totalActiveTime", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalCalories", new ix0.a("totalCalories", "REAL", true, 0, null, 1));
            hashMap10.put("totalDays", new ix0.a("totalDays", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalDistance", new ix0.a("totalDistance", "REAL", true, 0, null, 1));
            hashMap10.put("totalSteps", new ix0.a("totalSteps", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalIntensityDistInStep", new ix0.a("totalIntensityDistInStep", "TEXT", true, 0, null, 1));
            hashMap10.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap10.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            ix0 ix010 = new ix0(ActivityStatistic.TABLE_NAME, hashMap10, new HashSet(0), new HashSet(0));
            ix0 a11 = ix0.a(lx0, ActivityStatistic.TABLE_NAME);
            if (ix010.equals(a11)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "activity_statistic(com.portfolio.platform.data.ActivityStatistic).\n Expected:\n" + ix010 + "\n Found:\n" + a11);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public ActivitySampleDao activitySampleDao() {
        ActivitySampleDao activitySampleDao;
        if (this._activitySampleDao != null) {
            return this._activitySampleDao;
        }
        synchronized (this) {
            if (this._activitySampleDao == null) {
                this._activitySampleDao = new ActivitySampleDao_Impl(this);
            }
            activitySampleDao = this._activitySampleDao;
        }
        return activitySampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public ActivitySummaryDao activitySummaryDao() {
        ActivitySummaryDao activitySummaryDao;
        if (this._activitySummaryDao != null) {
            return this._activitySummaryDao;
        }
        synchronized (this) {
            if (this._activitySummaryDao == null) {
                this._activitySummaryDao = new ActivitySummaryDao_Impl(this);
            }
            activitySummaryDao = this._activitySummaryDao;
        }
        return activitySummaryDao;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `sampleraw`");
            writableDatabase.execSQL("DELETE FROM `activity_sample`");
            writableDatabase.execSQL("DELETE FROM `sampleday`");
            writableDatabase.execSQL("DELETE FROM `activitySettings`");
            writableDatabase.execSQL("DELETE FROM `activityRecommendedGoals`");
            writableDatabase.execSQL("DELETE FROM `workout_session`");
            writableDatabase.execSQL("DELETE FROM `fitness_data`");
            writableDatabase.execSQL("DELETE FROM `heart_rate_sample`");
            writableDatabase.execSQL("DELETE FROM `daily_heart_rate_summary`");
            writableDatabase.execSQL("DELETE FROM `activity_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), SampleRaw.TABLE_NAME, ActivitySample.TABLE_NAME, SampleDay.TABLE_NAME, ActivitySettings.TABLE_NAME, "activityRecommendedGoals", "workout_session", "fitness_data", "heart_rate_sample", "daily_heart_rate_summary", ActivityStatistic.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(22), "abe564b531b636e9dfd52068284844ef", "6ab7daea4220f579f89a1c1e9992b5e3");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public FitnessDataDao getFitnessDataDao() {
        FitnessDataDao fitnessDataDao;
        if (this._fitnessDataDao != null) {
            return this._fitnessDataDao;
        }
        synchronized (this) {
            if (this._fitnessDataDao == null) {
                this._fitnessDataDao = new FitnessDataDao_Impl(this);
            }
            fitnessDataDao = this._fitnessDataDao;
        }
        return fitnessDataDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public HeartRateDailySummaryDao getHeartRateDailySummaryDao() {
        HeartRateDailySummaryDao heartRateDailySummaryDao;
        if (this._heartRateDailySummaryDao != null) {
            return this._heartRateDailySummaryDao;
        }
        synchronized (this) {
            if (this._heartRateDailySummaryDao == null) {
                this._heartRateDailySummaryDao = new HeartRateDailySummaryDao_Impl(this);
            }
            heartRateDailySummaryDao = this._heartRateDailySummaryDao;
        }
        return heartRateDailySummaryDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public HeartRateSampleDao getHeartRateDao() {
        HeartRateSampleDao heartRateSampleDao;
        if (this._heartRateSampleDao != null) {
            return this._heartRateSampleDao;
        }
        synchronized (this) {
            if (this._heartRateSampleDao == null) {
                this._heartRateSampleDao = new HeartRateSampleDao_Impl(this);
            }
            heartRateSampleDao = this._heartRateSampleDao;
        }
        return heartRateSampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public WorkoutDao getWorkoutDao() {
        WorkoutDao workoutDao;
        if (this._workoutDao != null) {
            return this._workoutDao;
        }
        synchronized (this) {
            if (this._workoutDao == null) {
                this._workoutDao = new WorkoutDao_Impl(this);
            }
            workoutDao = this._workoutDao;
        }
        return workoutDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public SampleRawDao sampleRawDao() {
        SampleRawDao sampleRawDao;
        if (this._sampleRawDao != null) {
            return this._sampleRawDao;
        }
        synchronized (this) {
            if (this._sampleRawDao == null) {
                this._sampleRawDao = new SampleRawDao_Impl(this);
            }
            sampleRawDao = this._sampleRawDao;
        }
        return sampleRawDao;
    }
}
