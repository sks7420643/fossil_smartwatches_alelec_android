package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.on5;
import com.fossil.sk5;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<sk5> {
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<on5> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<on5> provider) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<on5> provider) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static sk5 provideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, on5 on5) {
        sk5 provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(on5);
        lk7.c(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public sk5 get() {
        return provideFitnessHelper(this.module, this.sharedPreferencesManagerProvider.get());
    }
}
