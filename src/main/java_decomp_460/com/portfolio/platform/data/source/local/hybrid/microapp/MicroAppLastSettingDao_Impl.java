package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingDao_Impl implements MicroAppLastSettingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<MicroAppLastSetting> __insertionAdapterOfMicroAppLastSetting;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteMicroAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<MicroAppLastSetting> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, MicroAppLastSetting microAppLastSetting) {
            if (microAppLastSetting.getAppId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, microAppLastSetting.getAppId());
            }
            if (microAppLastSetting.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, microAppLastSetting.getUpdatedAt());
            }
            if (microAppLastSetting.getSetting() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, microAppLastSetting.getSetting());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppLastSetting` (`appId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM microAppLastSetting WHERE appId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM microAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<MicroAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MicroAppLastSetting> call() throws Exception {
            Cursor b = ex0.b(MicroAppLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "appId");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public MicroAppLastSettingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfMicroAppLastSetting = new Anon1(qw0);
        this.__preparedStmtOfDeleteMicroAppLastSettingById = new Anon2(qw0);
        this.__preparedStmtOfCleanUp = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void deleteMicroAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteMicroAppLastSettingById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public List<MicroAppLastSetting> getAllMicroAppLastSetting() {
        tw0 f = tw0.f("SELECT * FROM microAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "appId");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM microAppLastSetting", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"microAppLastSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public MicroAppLastSetting getMicroAppLastSetting(String str) {
        MicroAppLastSetting microAppLastSetting = null;
        tw0 f = tw0.f("SELECT * FROM microAppLastSetting WHERE appId=? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "appId");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                microAppLastSetting = new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3));
            }
            return microAppLastSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert((jw0<MicroAppLastSetting>) microAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
