package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ao7;
import com.fossil.br7;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {352}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataList$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<GoalTrackingData>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getGoalTrackingDataList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<GoalTrackingData>, ApiResponse<GoalEvent>> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, br7 br7, int i, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$offset = br7;
                this.$limit = i;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<ApiResponse<GoalEvent>>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getGoalTrackingDataList(k, k2, this.$offset.element, this.$limit, qn7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
            @Override // com.fossil.c47
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r6) {
                /*
                    r5 = this;
                    r4 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r6 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x003b
                    r0 = r6
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x003b
                    int r1 = r1 + r3
                    r0.label = r1
                L_0x0013:
                    java.lang.Object r1 = r0.result
                    java.lang.Object r2 = com.fossil.yn7.d()
                    int r3 = r0.label
                    if (r3 == 0) goto L_0x0049
                    if (r3 != r4) goto L_0x0041
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.el7.b(r1)
                    r5 = r0
                L_0x0027:
                    r0 = r1
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r1 = r5.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r1 = r1.this$0
                    java.util.Date r2 = r1.$startDate
                    java.util.Date r1 = r1.$endDate
                    androidx.lifecycle.LiveData r0 = r0.getGoalTrackingDataListLiveData(r2, r1)
                L_0x003a:
                    return r0
                L_0x003b:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r5, r6)
                    goto L_0x0013
                L_0x0041:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0049:
                    com.fossil.el7.b(r1)
                    com.fossil.bn5 r1 = com.fossil.bn5.j
                    r0.L$0 = r5
                    r0.label = r4
                    java.lang.Object r1 = r1.A(r0)
                    if (r1 != r2) goto L_0x0027
                    r0 = r2
                    goto L_0x003a
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, qn7<? super Boolean> qn7) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, qn7 qn7) {
                return processContinueFetching(apiResponse, (qn7<? super Boolean>) qn7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalEvent> r8, com.fossil.qn7<? super com.fossil.tl7> r9) {
                /*
                // Method dump skipped, instructions count: 286
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse, com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<GoalEvent> apiResponse, qn7 qn7) {
                return saveCallResult(apiResponse, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<GoalTrackingData> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2) {
            this.this$0 = goalTrackingRepository$getGoalTrackingDataList$Anon2;
        }

        @DexIgnore
        public final LiveData<h47<List<GoalTrackingData>>> apply(List<GoalTrackingData> list) {
            pq7.b(list, "pendingList");
            GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = this.this$0;
            cl7<Date, Date> calculateRangeDownload = GoalTrackingDataKt.calculateRangeDownload(list, goalTrackingRepository$getGoalTrackingDataList$Anon2.$startDate, goalTrackingRepository$getGoalTrackingDataList$Anon2.$endDate);
            br7 br7 = new br7();
            br7.element = 0;
            return new Anon1_Level3(this, br7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, calculateRangeDownload).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = new GoalTrackingRepository$getGoalTrackingDataList$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, qn7);
        goalTrackingRepository$getGoalTrackingDataList$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$getGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<GoalTrackingData>>>> qn7) {
        return ((GoalTrackingRepository$getGoalTrackingDataList$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object pendingGoalTrackingDataListLiveData;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getGoalTrackingDataList startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            GoalTrackingRepository goalTrackingRepository = this.this$0;
            Date date = this.$startDate;
            Date date2 = this.$endDate;
            this.L$0 = iv7;
            this.label = 1;
            pendingGoalTrackingDataListLiveData = goalTrackingRepository.getPendingGoalTrackingDataListLiveData(date, date2, this);
            if (pendingGoalTrackingDataListLiveData == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            pendingGoalTrackingDataListLiveData = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ss0.c((LiveData) pendingGoalTrackingDataListLiveData, new Anon1_Level2(this));
    }
}
