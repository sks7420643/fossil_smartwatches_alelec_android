package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.fl5;
import com.fossil.pq7;
import com.fossil.xw7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource$loadAfter$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

    @DexIgnore
    public WorkoutSessionLocalDataSource$loadAfter$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.this$0 = workoutSessionLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$0;
        pq7.b(aVar, "helperCallback");
        xw7 unused = workoutSessionLocalDataSource.loadData(aVar, this.this$0.mOffset);
    }
}
