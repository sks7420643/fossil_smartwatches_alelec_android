package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.g78;
import com.fossil.hx0;
import com.fossil.jw0;
import com.fossil.m05;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDao_Impl implements WatchAppDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WatchApp> __insertionAdapterOfWatchApp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ m05 __stringArrayConverter; // = new m05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WatchApp> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WatchApp watchApp) {
            if (watchApp.getWatchappId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, watchApp.getWatchappId());
            }
            if (watchApp.getName() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, watchApp.getName());
            }
            if (watchApp.getNameKey() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, watchApp.getNameKey());
            }
            if (watchApp.getDescription() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, watchApp.getDescription());
            }
            if (watchApp.getDescriptionKey() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, watchApp.getDescriptionKey());
            }
            String b = WatchAppDao_Impl.this.__stringArrayConverter.b(watchApp.getCategories());
            if (b == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, b);
            }
            if (watchApp.getIcon() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, watchApp.getIcon());
            }
            if (watchApp.getUpdatedAt() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, watchApp.getUpdatedAt());
            }
            if (watchApp.getCreatedAt() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, watchApp.getCreatedAt());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchApp` (`watchappId`,`name`,`nameKey`,`description`,`descriptionKey`,`categories`,`icon`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchApp";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchApp> call() throws Exception {
            Cursor b = ex0.b(WatchAppDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "watchappId");
                int c2 = dx0.c(b, "name");
                int c3 = dx0.c(b, "nameKey");
                int c4 = dx0.c(b, "description");
                int c5 = dx0.c(b, "descriptionKey");
                int c6 = dx0.c(b, "categories");
                int c7 = dx0.c(b, "icon");
                int c8 = dx0.c(b, "updatedAt");
                int c9 = dx0.c(b, "createdAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), WatchAppDao_Impl.this.__stringArrayConverter.a(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WatchAppDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWatchApp = new Anon1(qw0);
        this.__preparedStmtOfClearAll = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> getAllWatchApp() {
        tw0 f = tw0.f("SELECT * FROM watchApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "watchappId");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "nameKey");
            int c4 = dx0.c(b, "description");
            int c5 = dx0.c(b, "descriptionKey");
            int c6 = dx0.c(b, "categories");
            int c7 = dx0.c(b, "icon");
            int c8 = dx0.c(b, "updatedAt");
            int c9 = dx0.c(b, "createdAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), this.__stringArrayConverter.a(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public LiveData<List<WatchApp>> getAllWatchAppAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM watchApp", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"watchApp"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public WatchApp getWatchAppById(String str) {
        WatchApp watchApp = null;
        tw0 f = tw0.f("SELECT * FROM watchApp WHERE watchappId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "watchappId");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "nameKey");
            int c4 = dx0.c(b, "description");
            int c5 = dx0.c(b, "descriptionKey");
            int c6 = dx0.c(b, "categories");
            int c7 = dx0.c(b, "icon");
            int c8 = dx0.c(b, "updatedAt");
            int c9 = dx0.c(b, "createdAt");
            if (b.moveToFirst()) {
                watchApp = new WatchApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), this.__stringArrayConverter.a(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9));
            }
            return watchApp;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> getWatchAppByIds(List<String> list) {
        StringBuilder b = hx0.b();
        b.append("SELECT ");
        b.append(g78.ANY_MARKER);
        b.append(" FROM watchApp WHERE watchappId IN (");
        int size = list.size();
        hx0.a(b, size);
        b.append(")");
        tw0 f = tw0.f(b.toString(), size + 0);
        int i = 1;
        for (String str : list) {
            if (str == null) {
                f.bindNull(i);
            } else {
                f.bindString(i, str);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b2, "watchappId");
            int c2 = dx0.c(b2, "name");
            int c3 = dx0.c(b2, "nameKey");
            int c4 = dx0.c(b2, "description");
            int c5 = dx0.c(b2, "descriptionKey");
            int c6 = dx0.c(b2, "categories");
            int c7 = dx0.c(b2, "icon");
            int c8 = dx0.c(b2, "updatedAt");
            int c9 = dx0.c(b2, "createdAt");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new WatchApp(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), this.__stringArrayConverter.a(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> queryWatchAppByName(String str) {
        tw0 f = tw0.f("SELECT * FROM watchApp WHERE name LIKE '%' || ? || '%'", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "watchappId");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "nameKey");
            int c4 = dx0.c(b, "description");
            int c5 = dx0.c(b, "descriptionKey");
            int c6 = dx0.c(b, "categories");
            int c7 = dx0.c(b, "icon");
            int c8 = dx0.c(b, "updatedAt");
            int c9 = dx0.c(b, "createdAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), this.__stringArrayConverter.a(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public void upsertWatchAppList(List<WatchApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
