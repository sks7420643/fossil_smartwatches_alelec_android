package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.n05;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDao_Impl extends ThemeDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<Theme> __deletionAdapterOfTheme;
    @DexIgnore
    public /* final */ jw0<Theme> __insertionAdapterOfTheme;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearThemeTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfResetAllCurrentTheme;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfSetCurrentThemeId;
    @DexIgnore
    public /* final */ n05 __themeConverter; // = new n05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Theme> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Theme theme) {
            px0.bindLong(1, theme.isCurrentTheme() ? 1 : 0);
            if (theme.getType() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, theme.getType());
            }
            if (theme.getId() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, theme.getId());
            }
            if (theme.getName() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, theme.getName());
            }
            String b = ThemeDao_Impl.this.__themeConverter.b(theme.getStyles());
            if (b == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `theme` (`isCurrentTheme`,`type`,`id`,`name`,`styles`) VALUES (?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<Theme> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Theme theme) {
            if (theme.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, theme.getId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `theme` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM theme";
        }
    }

    @DexIgnore
    public ThemeDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfTheme = new Anon1(qw0);
        this.__deletionAdapterOfTheme = new Anon2(qw0);
        this.__preparedStmtOfResetAllCurrentTheme = new Anon3(qw0);
        this.__preparedStmtOfSetCurrentThemeId = new Anon4(qw0);
        this.__preparedStmtOfClearThemeTable = new Anon5(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void clearThemeTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearThemeTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearThemeTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void deleteTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfTheme.handle(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getCurrentTheme() {
        Theme theme = null;
        boolean z = false;
        tw0 f = tw0.f("SELECT * FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "isCurrentTheme");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "id");
            int c4 = dx0.c(b, "name");
            int c5 = dx0.c(b, "styles");
            if (b.moveToFirst()) {
                theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                if (b.getInt(c) != 0) {
                    z = true;
                }
                theme.setCurrentTheme(z);
                theme.setType(b.getString(c2));
            }
            return theme;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getCurrentThemeId() {
        String str = null;
        tw0 f = tw0.f("SELECT id FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str = b.getString(0);
            }
            return str;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<Theme> getListTheme() {
        tw0 f = tw0.f("SELECT * FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "isCurrentTheme");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "id");
            int c4 = dx0.c(b, "name");
            int c5 = dx0.c(b, "styles");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                Theme theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                theme.setCurrentTheme(b.getInt(c) != 0);
                theme.setType(b.getString(c2));
                arrayList.add(theme);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeId() {
        tw0 f = tw0.f("SELECT id FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(b.getString(0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeIdByType(String str) {
        tw0 f = tw0.f("SELECT id FROM theme WHERE type=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(b.getString(0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getNameById(String str) {
        String str2 = null;
        tw0 f = tw0.f("SELECT name FROM theme WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str2 = b.getString(0);
            }
            return str2;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getThemeById(String str) {
        Theme theme = null;
        boolean z = true;
        tw0 f = tw0.f("SELECT * FROM theme WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "isCurrentTheme");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "id");
            int c4 = dx0.c(b, "name");
            int c5 = dx0.c(b, "styles");
            if (b.moveToFirst()) {
                theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                if (b.getInt(c) == 0) {
                    z = false;
                }
                theme.setCurrentTheme(z);
                theme.setType(b.getString(c2));
            }
            return theme;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void insertListTheme(List<Theme> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void resetAllCurrentTheme() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfResetAllCurrentTheme.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetAllCurrentTheme.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void setCurrentThemeId(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfSetCurrentThemeId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetCurrentThemeId.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void upsertTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert((jw0<Theme>) theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
