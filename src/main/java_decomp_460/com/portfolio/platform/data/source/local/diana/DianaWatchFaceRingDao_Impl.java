package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.fw0;
import com.fossil.jw0;
import com.fossil.o77;
import com.fossil.px0;
import com.fossil.qn7;
import com.fossil.qw0;
import com.fossil.tl7;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRingDao_Impl implements DianaWatchFaceRingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DianaWatchFaceRing> __insertionAdapterOfDianaWatchFaceRing;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ o77 __wFAssetConverter; // = new o77();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaWatchFaceRing> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaWatchFaceRing dianaWatchFaceRing) {
            if (dianaWatchFaceRing.getCreatedAt() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaWatchFaceRing.getCreatedAt());
            }
            if (dianaWatchFaceRing.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaWatchFaceRing.getUpdatedAt());
            }
            if (dianaWatchFaceRing.getId() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dianaWatchFaceRing.getId());
            }
            if (dianaWatchFaceRing.getCategory() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, dianaWatchFaceRing.getCategory());
            }
            if (dianaWatchFaceRing.getName() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, dianaWatchFaceRing.getName());
            }
            String c = DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.c(dianaWatchFaceRing.getData());
            if (c == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, c);
            }
            String f = DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.f(dianaWatchFaceRing.getMetaData());
            if (f == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, f);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchFaceRing` (`createdAt`,`updatedAt`,`id`,`category`,`name`,`data`,`metaData`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM DianaWatchFaceRing";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaWatchFaceRing val$ring;

        @DexIgnore
        public Anon3(DianaWatchFaceRing dianaWatchFaceRing) {
            this.val$ring = dianaWatchFaceRing;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public tl7 call() throws Exception {
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                DianaWatchFaceRingDao_Impl.this.__insertionAdapterOfDianaWatchFaceRing.insert((jw0) this.val$ring);
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return tl7.f3441a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ List val$rings;

        @DexIgnore
        public Anon4(List list) {
            this.val$rings = list;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public tl7 call() throws Exception {
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                DianaWatchFaceRingDao_Impl.this.__insertionAdapterOfDianaWatchFaceRing.insert((Iterable) this.val$rings);
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return tl7.f3441a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<tl7> {
        @DexIgnore
        public Anon5() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public tl7 call() throws Exception {
            px0 acquire = DianaWatchFaceRingDao_Impl.this.__preparedStmtOfClearAll.acquire();
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                acquire.executeUpdateDelete();
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return tl7.f3441a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
                DianaWatchFaceRingDao_Impl.this.__preparedStmtOfClearAll.release(acquire);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<DianaWatchFaceRing>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon6(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaWatchFaceRing> call() throws Exception {
            Cursor b = ex0.b(DianaWatchFaceRingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "id");
                int c4 = dx0.c(b, "category");
                int c5 = dx0.c(b, "name");
                int c6 = dx0.c(b, "data");
                int c7 = dx0.c(b, "metaData");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaWatchFaceRing dianaWatchFaceRing = new DianaWatchFaceRing(b.getString(c3), b.getString(c4), b.getString(c5), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.d(b.getString(c6)), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.e(b.getString(c7)));
                    dianaWatchFaceRing.setCreatedAt(b.getString(c));
                    dianaWatchFaceRing.setUpdatedAt(b.getString(c2));
                    arrayList.add(dianaWatchFaceRing);
                }
                return arrayList;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<DianaWatchFaceRing> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon7(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaWatchFaceRing call() throws Exception {
            DianaWatchFaceRing dianaWatchFaceRing = null;
            Cursor b = ex0.b(DianaWatchFaceRingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "id");
                int c4 = dx0.c(b, "category");
                int c5 = dx0.c(b, "name");
                int c6 = dx0.c(b, "data");
                int c7 = dx0.c(b, "metaData");
                if (b.moveToFirst()) {
                    dianaWatchFaceRing = new DianaWatchFaceRing(b.getString(c3), b.getString(c4), b.getString(c5), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.d(b.getString(c6)), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.e(b.getString(c7)));
                    dianaWatchFaceRing.setCreatedAt(b.getString(c));
                    dianaWatchFaceRing.setUpdatedAt(b.getString(c2));
                }
                return dianaWatchFaceRing;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore
    public DianaWatchFaceRingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaWatchFaceRing = new Anon1(qw0);
        this.__preparedStmtOfClearAll = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object clearAll(qn7<? super tl7> qn7) {
        return fw0.a(this.__db, true, new Anon5(), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object getAllDianaWatchFaceRings(qn7<? super List<DianaWatchFaceRing>> qn7) {
        return fw0.a(this.__db, false, new Anon6(tw0.f("SELECT * FROM DianaWatchFaceRing", 0)), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object getDianaWatchFaceRing(String str, qn7<? super DianaWatchFaceRing> qn7) {
        tw0 f = tw0.f("SELECT * FROM DianaWatchFaceRing WHERE name = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        return fw0.a(this.__db, false, new Anon7(f), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object upsertDianaWatchFaceRing(DianaWatchFaceRing dianaWatchFaceRing, qn7<? super tl7> qn7) {
        return fw0.a(this.__db, true, new Anon3(dianaWatchFaceRing), qn7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object upsertDianaWatchFaceRings(List<DianaWatchFaceRing> list, qn7<? super tl7> qn7) {
        return fw0.a(this.__db, true, new Anon4(list), qn7);
    }
}
