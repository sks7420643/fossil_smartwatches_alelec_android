package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.sk5;
import com.fossil.xt0;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDataSourceFactory extends xt0.b<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ no4 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ sk5 fitnessHelper;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, sk5 sk5, FitnessDataRepository fitnessDataRepository2, FitnessDatabase fitnessDatabase2, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(summariesRepository2, "summariesRepository");
        pq7.c(sk5, "fitnessHelper");
        pq7.c(fitnessDataRepository2, "fitnessDataRepository");
        pq7.c(fitnessDatabase2, "fitnessDatabase");
        pq7.c(date, "createdDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        pq7.c(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = sk5;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = no4;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Date, ActivitySummary> create() {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.localDataSource = activitySummaryLocalDataSource;
        this.sourceLiveData.l(activitySummaryLocalDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.localDataSource;
        if (activitySummaryLocalDataSource2 != null) {
            return activitySummaryLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
