package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.bx0;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.qz4;
import com.fossil.sz4;
import com.fossil.tw0;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.fossil.xt0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDao_Impl extends GoalTrackingDao {
    @DexIgnore
    public /* final */ qz4 __dateShortStringConverter; // = new qz4();
    @DexIgnore
    public /* final */ sz4 __dateTimeISOStringConverter; // = new sz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<GoalSetting> __insertionAdapterOfGoalSetting;
    @DexIgnore
    public /* final */ jw0<GoalTrackingData> __insertionAdapterOfGoalTrackingData;
    @DexIgnore
    public /* final */ jw0<GoalTrackingSummary> __insertionAdapterOfGoalTrackingSummary;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllGoalTrackingData;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllGoalTrackingSummaries;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteGoalSetting;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveDeletedGoalTrackingData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GoalSetting> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GoalSetting goalSetting) {
            px0.bindLong(1, (long) goalSetting.getId());
            px0.bindLong(2, (long) goalSetting.getCurrentTarget());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalSetting` (`id`,`currentTarget`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon10(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "date");
                int c3 = dx0.c(b, "totalTracked");
                int c4 = dx0.c(b, "goalTarget");
                int c5 = dx0.c(b, "createdAt");
                int c6 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon11(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public GoalTrackingSummary call() throws Exception {
            GoalTrackingSummary goalTrackingSummary = null;
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "date");
                int c3 = dx0.c(b, "totalTracked");
                int c4 = dx0.c(b, "goalTarget");
                int c5 = dx0.c(b, "createdAt");
                int c6 = dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                }
                return goalTrackingSummary;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon12(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "date");
                int c3 = dx0.c(b, "totalTracked");
                int c4 = dx0.c(b, "goalTarget");
                int c5 = dx0.c(b, "createdAt");
                int c6 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon13(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "id");
                int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int c4 = dx0.c(b, "timezoneOffsetInSecond");
                int c5 = dx0.c(b, "date");
                int c6 = dx0.c(b, "createdAt");
                int c7 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                    goalTrackingData.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon14(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "id");
                int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int c4 = dx0.c(b, "timezoneOffsetInSecond");
                int c5 = dx0.c(b, "date");
                int c6 = dx0.c(b, "createdAt");
                int c7 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                    goalTrackingData.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<GoalTrackingSummary> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GoalTrackingSummary goalTrackingSummary) {
            px0.bindLong(1, (long) goalTrackingSummary.getPinType());
            String a2 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingSummary.getDate());
            if (a2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, a2);
            }
            px0.bindLong(3, (long) goalTrackingSummary.getTotalTracked());
            px0.bindLong(4, (long) goalTrackingSummary.getGoalTarget());
            px0.bindLong(5, goalTrackingSummary.getCreatedAt());
            px0.bindLong(6, goalTrackingSummary.getUpdatedAt());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingDay` (`pinType`,`date`,`totalTracked`,`goalTarget`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends jw0<GoalTrackingData> {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GoalTrackingData goalTrackingData) {
            px0.bindLong(1, (long) goalTrackingData.getPinType());
            if (goalTrackingData.getId() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, goalTrackingData.getId());
            }
            String a2 = GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(goalTrackingData.getTrackedAt());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
            px0.bindLong(4, (long) goalTrackingData.getTimezoneOffsetInSecond());
            String a3 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingData.getDate());
            if (a3 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a3);
            }
            px0.bindLong(6, goalTrackingData.getCreatedAt());
            px0.bindLong(7, goalTrackingData.getUpdatedAt());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingRaw` (`pinType`,`id`,`trackedAt`,`timezoneOffsetInSecond`,`date`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM goalSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM goalTrackingDay";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends xw0 {
        @DexIgnore
        public Anon6(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw WHERE  id == ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends xw0 {
        @DexIgnore
        public Anon7(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon8(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Integer call() throws Exception {
            Integer num = null;
            Cursor b = ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                if (b.moveToFirst() && !b.isNull(0)) {
                    num = Integer.valueOf(b.getInt(0));
                }
                return num;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends xt0.b<Integer, GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends bx0<GoalTrackingSummary> {
            @DexIgnore
            public Anon1_Level2(qw0 qw0, tw0 tw0, boolean z, String... strArr) {
                super(qw0, tw0, z, strArr);
            }

            @DexIgnore
            @Override // com.fossil.bx0
            public List<GoalTrackingSummary> convertRows(Cursor cursor) {
                int c = dx0.c(cursor, "pinType");
                int c2 = dx0.c(cursor, "date");
                int c3 = dx0.c(cursor, "totalTracked");
                int c4 = dx0.c(cursor, "goalTarget");
                int c5 = dx0.c(cursor, "createdAt");
                int c6 = dx0.c(cursor, "updatedAt");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(cursor.getString(c2)), cursor.getInt(c3), cursor.getInt(c4), cursor.getLong(c5), cursor.getLong(c6));
                    goalTrackingSummary.setPinType(cursor.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon9(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.bx0<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>' to match base method */
        @Override // com.fossil.xt0.b
        public xt0<Integer, GoalTrackingSummary> create() {
            return new Anon1_Level2(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, "goalTrackingDay");
        }
    }

    @DexIgnore
    public GoalTrackingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGoalSetting = new Anon1(qw0);
        this.__insertionAdapterOfGoalTrackingSummary = new Anon2(qw0);
        this.__insertionAdapterOfGoalTrackingData = new Anon3(qw0);
        this.__preparedStmtOfDeleteGoalSetting = new Anon4(qw0);
        this.__preparedStmtOfDeleteAllGoalTrackingSummaries = new Anon5(qw0);
        this.__preparedStmtOfRemoveDeletedGoalTrackingData = new Anon6(qw0);
        this.__preparedStmtOfDeleteAllGoalTrackingData = new Anon7(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingData() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllGoalTrackingData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingSummaries() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllGoalTrackingSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingSummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteGoalSetting() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteGoalSetting.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteGoalSetting.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataInDate(Date date) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date== ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date == ? AND updatedAt < ? AND trackedAt < ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 4);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, j);
        String a3 = this.__dateTimeISOStringConverter.a(dateTime);
        if (a3 == null) {
            f.bindNull(3);
        } else {
            f.bindString(3, a3);
        }
        f.bindLong(4, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date == ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon13 anon13 = new Anon13(f);
        return invalidationTracker.d(new String[]{"goalTrackingRaw"}, false, anon13);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "date");
            int c3 = dx0.c(b, "totalTracked");
            int c4 = dx0.c(b, "goalTarget");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon12 anon12 = new Anon12(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon12);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getGoalTrackingSummary(Date date) {
        GoalTrackingSummary goalTrackingSummary = null;
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "date");
            int c3 = dx0.c(b, "totalTracked");
            int c4 = dx0.c(b, "goalTarget");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
            }
            return goalTrackingSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaryList() {
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "date");
            int c3 = dx0.c(b, "totalTracked");
            int c4 = dx0.c(b, "goalTarget");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData() {
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon11 anon11 = new Anon11(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon11);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingDao.TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2) {
        GoalTrackingDao.TotalSummary totalSummary = null;
        tw0 f = tw0.f("SELECT SUM(totalTracked) as total_values, SUM(goalTarget) as total_targets FROM goalTrackingDay\n        WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "total_values");
            int c2 = dx0.c(b, "total_targets");
            if (b.moveToFirst()) {
                totalSummary = new GoalTrackingDao.TotalSummary();
                totalSummary.setValues(b.getInt(c));
                totalSummary.setTargets(b.getInt(c2));
            }
            return totalSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public Integer getLastGoalSetting() {
        Integer num = null;
        tw0 f = tw0.f("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst() && !b.isNull(0)) {
                num = Integer.valueOf(b.getInt(0));
            }
            return num;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<Integer> getLastGoalSettingLiveData() {
        tw0 f = tw0.f("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"goalSetting"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getNearestGoalTrackingFromDate(Date date) {
        GoalTrackingSummary goalTrackingSummary = null;
        tw0 f = tw0.f("SELECT * FROM goalTrackingDay WHERE date <= ? ORDER BY date DESC LIMIT 1", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "date");
            int c3 = dx0.c(b, "totalTracked");
            int c4 = dx0.c(b, "goalTarget");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
            }
            return goalTrackingSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList() {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = dx0.c(b, "timezoneOffsetInSecond");
            int c5 = dx0.c(b, "date");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon14 anon14 = new Anon14(f);
        return invalidationTracker.d(new String[]{"goalTrackingRaw"}, false, anon14);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public xt0.b<Integer, GoalTrackingSummary> getSummariesDataSource() {
        return new Anon9(tw0.f("SELECT * FROM goalTrackingDay ORDER BY date DESC", 0));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void removeDeletedGoalTrackingData(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveDeletedGoalTrackingData.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeletedGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalSettings(GoalSetting goalSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalSetting.insert((jw0<GoalSetting>) goalSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingData(GoalTrackingData goalTrackingData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert((jw0<GoalTrackingData>) goalTrackingData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingDataList(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert((jw0<GoalTrackingSummary>) goalTrackingSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertListGoalTrackingData(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
