package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingDao_Impl implements DianaAppSettingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DianaAppSetting> __insertionAdapterOfDianaAppSetting;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteDianaAppSetting;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaAppSetting> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaAppSetting dianaAppSetting) {
            px0.bindLong(1, (long) dianaAppSetting.getPinType());
            if (dianaAppSetting.getId() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaAppSetting.getId());
            }
            if (dianaAppSetting.getAppId() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dianaAppSetting.getAppId());
            }
            if (dianaAppSetting.getCategory() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, dianaAppSetting.getCategory());
            }
            if (dianaAppSetting.getSetting() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, dianaAppSetting.getSetting());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaAppSetting` (`pinType`,`id`,`appId`,`category`,`setting`) VALUES (?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM DianaAppSetting WHERE category=? AND appId=? ";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM DianaAppSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<DianaAppSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaAppSetting> call() throws Exception {
            Cursor b = ex0.b(DianaAppSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "id");
                int c3 = dx0.c(b, "appId");
                int c4 = dx0.c(b, "category");
                int c5 = dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                    dianaAppSetting.setPinType(b.getInt(c));
                    arrayList.add(dianaAppSetting);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaAppSettingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaAppSetting = new Anon1(qw0);
        this.__preparedStmtOfDeleteDianaAppSetting = new Anon2(qw0);
        this.__preparedStmtOfCleanUp = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void deleteDianaAppSetting(String str, String str2) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteDianaAppSetting.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        if (str2 == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str2);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteDianaAppSetting.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getAllDianaAppSetting() {
        tw0 f = tw0.f("SELECT * FROM DianaAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, "appId");
            int c4 = dx0.c(b, "category");
            int c5 = dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getAllDianaAppSetting(String str) {
        tw0 f = tw0.f("SELECT * FROM DianaAppSetting WHERE category=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, "appId");
            int c4 = dx0.c(b, "category");
            int c5 = dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public LiveData<List<DianaAppSetting>> getAllDianaAppSettingAsLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM DianaAppSetting WHERE category=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"DianaAppSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public DianaAppSetting getDianaAppSetting(String str, String str2) {
        DianaAppSetting dianaAppSetting = null;
        tw0 f = tw0.f("SELECT * FROM DianaAppSetting WHERE category=? AND appId=? ", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, "appId");
            int c4 = dx0.c(b, "category");
            int c5 = dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
            }
            return dianaAppSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getPendingDianaAppSetting() {
        tw0 f = tw0.f("SELECT * FROM DianaAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, "appId");
            int c4 = dx0.c(b, "category");
            int c5 = dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public Long[] insert(List<DianaAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfDianaAppSetting.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void upsertDianaAppSetting(DianaAppSetting dianaAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaAppSetting.insert((jw0<DianaAppSetting>) dianaAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void upsertDianaAppSettingList(List<DianaAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
