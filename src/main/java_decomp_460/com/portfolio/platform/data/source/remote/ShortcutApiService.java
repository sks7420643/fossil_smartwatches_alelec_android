package com.portfolio.platform.data.source.remote;

import com.fossil.m98;
import com.fossil.y98;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ShortcutApiService {
    @DexIgnore
    @m98("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@y98("offset") int i, @y98("size") int i2, @y98("serialNumber") String str);

    @DexIgnore
    @m98("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@y98("page") int i, @y98("size") int i2, @y98("serialNumber") String str);
}
