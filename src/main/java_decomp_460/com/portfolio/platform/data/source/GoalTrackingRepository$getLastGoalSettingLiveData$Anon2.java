package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.c47;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettingLiveData$2", f = "GoalTrackingRepository.kt", l = {87}, m = "invokeSuspend")
public final class GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends Integer>>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends c47<Integer, GoalSetting> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 this$0;

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getLastGoalSettingLiveData$Anon2;
            this.$fitnessDatabase = goalTrackingDatabase;
        }

        @DexIgnore
        @Override // com.fossil.c47
        public Object createCall(qn7<? super q88<GoalSetting>> qn7) {
            return this.this$0.this$0.mApiServiceV2.getGoalSetting(qn7);
        }

        @DexIgnore
        @Override // com.fossil.c47
        public Object loadFromDb(qn7<? super LiveData<Integer>> qn7) {
            return this.$fitnessDatabase.getGoalTrackingDao().getLastGoalSettingLiveData();
        }

        @DexIgnore
        @Override // com.fossil.c47
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getLastGoalSettingLiveData onFetchFailed");
        }

        @DexIgnore
        public Object saveCallResult(GoalSetting goalSetting, qn7<? super tl7> qn7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getLastGoalSettingLiveData saveCallResult goal: " + goalSetting);
            Object saveSettingToDB = this.this$0.this$0.saveSettingToDB(goalSetting, qn7);
            return saveSettingToDB == yn7.d() ? saveSettingToDB : tl7.f3441a;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
        @Override // com.fossil.c47
        public /* bridge */ /* synthetic */ Object saveCallResult(GoalSetting goalSetting, qn7 qn7) {
            return saveCallResult(goalSetting, (qn7<? super tl7>) qn7);
        }

        @DexIgnore
        public boolean shouldFetch(Integer num) {
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(GoalTrackingRepository goalTrackingRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2 = new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(this.this$0, qn7);
        goalTrackingRepository$getLastGoalSettingLiveData$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$getLastGoalSettingLiveData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends Integer>>> qn7) {
        return ((GoalTrackingRepository$getLastGoalSettingLiveData$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            dv7 b = bw7.b();
            GoalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2 = new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, goalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return new Anon1_Level2(this, (GoalTrackingDatabase) g).asLiveData();
    }
}
