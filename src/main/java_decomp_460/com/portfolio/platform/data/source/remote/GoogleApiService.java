package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.m98;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.y98;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GoogleApiService {
    @DexIgnore
    @m98("geocode/json")
    Object getAddress(@y98("latlng") String str, @y98("language") String str2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("geocode/json")
    Object getAddressWithType(@y98("latlng") String str, @y98("result_type") String str2, qn7<? super q88<gj4>> qn7);
}
