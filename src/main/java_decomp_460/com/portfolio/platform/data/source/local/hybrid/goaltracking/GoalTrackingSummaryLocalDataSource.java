package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.au0;
import com.fossil.bw7;
import com.fossil.cl7;
import com.fossil.fl5;
import com.fossil.gl5;
import com.fossil.gu7;
import com.fossil.hm7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.no4;
import com.fossil.nw0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource extends au0<Date, GoalTrackingSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public fl5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ nw0.c mObserver;
    @DexIgnore
    public List<cl7<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends nw0.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            pq7.c(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            pq7.c(date, "date");
            pq7.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(GoalTrackingSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = lk5.I(instance);
            if (lk5.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            pq7.b(time, "nextPagedKey.time");
            return time;
        }
    }

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource(GoalTrackingRepository goalTrackingRepository, Date date, GoalTrackingDatabase goalTrackingDatabase, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(date, "mCreatedDate");
        pq7.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        pq7.c(calendar, "key");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mCreatedDate = date;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.listener = aVar;
        this.key = calendar;
        fl5 fl5 = new fl5(no4.a());
        this.mHelper = fl5;
        this.mNetworkState = gl5.b(fl5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingDay", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = lk5.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        pq7.b(time, "calendar.time");
        this.mStartDate = time;
        if (lk5.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<GoalTrackingSummary> calculateSummaries(List<GoalTrackingSummary> list) {
        int i;
        int i2;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "endCalendar");
            instance.setTime(((GoalTrackingSummary) pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = lk5.b0(instance.getTime());
                pq7.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
                Date time = b0.getTime();
                pq7.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                pq7.b(time2, "endCalendar.time");
                GoalTrackingDao.TotalSummary goalTrackingValueAndTarget = goalTrackingDao.getGoalTrackingValueAndTarget(time, time2);
                i2 = goalTrackingValueAndTarget.getValues();
                i = goalTrackingValueAndTarget.getTargets();
            } else {
                i = 0;
                i2 = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            pq7.b(instance2, "calendar");
            instance2.setTime(((GoalTrackingSummary) pm7.F(list)).getDate());
            Calendar b02 = lk5.b0(instance2.getTime());
            pq7.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (T t : list) {
                if (i3 >= 0) {
                    T t2 = t;
                    Date component1 = t2.component1();
                    int component2 = t2.component2();
                    int component3 = t2.component3();
                    if (lk5.m0(component1, b02.getTime())) {
                        list.get(i6).setTotalValueOfWeek(i5);
                        list.get(i6).setTotalTargetOfWeek(i4);
                        b02.add(5, -7);
                        i4 = 0;
                        i5 = 0;
                        i6 = i3;
                    }
                    i5 += component2;
                    i4 += component3;
                    if (i3 == list.size() - 1) {
                        i5 += i2;
                        i4 += i;
                    }
                    i3++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            list.get(i6).setTotalValueOfWeek(i5);
            list.get(i6).setTotalTargetOfWeek(i4);
            FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
        }
        return list;
    }

    @DexIgnore
    private final GoalTrackingSummary dummySummary(Date date) {
        return new GoalTrackingSummary(date, 0, this.mGoalTrackingDatabase.getGoalTrackingDao().getNearestGoalTrackingTargetFromDate(date), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final List<GoalTrackingSummary> getDataInDatabase(Date date, Date date2) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<GoalTrackingSummary> goalTrackingSummaries = this.mGoalTrackingRepository.getGoalTrackingSummaries(date, date2, this.mGoalTrackingDatabase.getGoalTrackingDao());
        List<GoalTrackingSummary> arrayList = new ArrayList<>();
        int size = goalTrackingSummaries.size() + -1;
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - summaries.size=" + goalTrackingSummaries.size() + ", startDate=" + date + ", endDateToFill=" + date2);
        while (lk5.k0(date2, date)) {
            if (size < 0 || !lk5.m0(goalTrackingSummaries.get(size).getDate(), date2)) {
                arrayList.add(dummySummary(date2));
                size = size;
            } else {
                arrayList.add(goalTrackingSummaries.get(size));
                size--;
            }
            date2 = lk5.P(date2);
            pq7.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        calculateSummaries(arrayList);
        if (!arrayList.isEmpty()) {
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) pm7.F(arrayList);
            Boolean p0 = lk5.p0(goalTrackingSummary.getDate());
            pq7.b(p0, "DateHelper.isToday(todaySummary.date)");
            if (p0.booleanValue()) {
                arrayList.add(0, GoalTrackingSummary.copy$default(goalTrackingSummary, goalTrackingSummary.getDate(), goalTrackingSummary.getTotalTracked(), goalTrackingSummary.getGoalTarget(), 0, 0, 24, null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final xw7 loadData(fl5.d dVar, Date date, Date date2, fl5.b.a aVar) {
        return gu7.d(jv7.a(bw7.b()), null, null, new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final fl5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadAfter(au0.f<Date> fVar, au0.a<Date, GoalTrackingSummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.f326a));
        if (lk5.j0(fVar.f326a, this.mCreatedDate)) {
            Key key2 = fVar.f326a;
            pq7.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.f326a;
            pq7.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = lk5.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : lk5.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            pq7.b(O, "startQueryDate");
            aVar.a(getDataInDatabase(O, key3), calculateNextKey);
            if (lk5.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new cl7<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(fl5.d.AFTER, new GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadBefore(au0.f<Date> fVar, au0.a<Date, GoalTrackingSummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadInitial(au0.e<Date> eVar, au0.c<Date, GoalTrackingSummary> cVar) {
        pq7.c(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = lk5.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : lk5.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        pq7.b(O, "startQueryDate");
        cVar.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(fl5.d.INITIAL, new GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(fl5 fl5) {
        pq7.c(fl5, "<set-?>");
        this.mHelper = fl5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        pq7.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
