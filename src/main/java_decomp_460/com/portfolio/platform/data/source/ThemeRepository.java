package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.local.ThemeDao;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_THEME_URI; // = "theme/default/config";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ ThemeDao mThemeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return ThemeRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = ThemeRepository.class.getSimpleName();
        pq7.b(simpleName, "ThemeRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ThemeRepository(ThemeDao themeDao, PortfolioApp portfolioApp) {
        pq7.c(themeDao, "mThemeDao");
        pq7.c(portfolioApp, "mApp");
        this.mThemeDao = themeDao;
        this.mApp = portfolioApp;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007d, code lost:
        com.fossil.so7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        throw r2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void loadTheme(java.lang.String r7) {
        /*
            r6 = this;
            r1 = 0
            com.fossil.zi4 r0 = new com.fossil.zi4
            r0.<init>()
            java.lang.Class<com.portfolio.platform.data.model.Theme> r2 = com.portfolio.platform.data.model.Theme.class
            com.portfolio.platform.gson.ThemeDeserializer r3 = new com.portfolio.platform.gson.ThemeDeserializer
            r3.<init>()
            r0.f(r2, r3)
            com.google.gson.Gson r3 = r0.d()
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00ab }
            com.portfolio.platform.PortfolioApp r0 = r6.mApp     // Catch:{ Exception -> 0x00ab }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x00ab }
            java.io.InputStream r0 = r0.open(r7)     // Catch:{ Exception -> 0x00ab }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00ab }
            java.nio.charset.Charset r4 = com.fossil.et7.f986a     // Catch:{ Exception -> 0x00ab }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00ab }
            r0.<init>(r2, r4)     // Catch:{ Exception -> 0x00ab }
            boolean r2 = r0 instanceof java.io.BufferedReader     // Catch:{ Exception -> 0x00ab }
            if (r2 == 0) goto L_0x0071
            java.io.BufferedReader r0 = (java.io.BufferedReader) r0     // Catch:{ Exception -> 0x00ab }
            r1 = r0
        L_0x0031:
            java.lang.String r0 = com.fossil.dp7.c(r1)     // Catch:{ all -> 0x007a }
            r2 = 0
            com.fossil.so7.a(r1, r2)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            if (r2 != 0) goto L_0x006d
            java.lang.Class<com.portfolio.platform.data.model.Theme> r2 = com.portfolio.platform.data.model.Theme.class
            java.lang.Object r0 = r3.k(r0, r2)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            com.portfolio.platform.data.source.local.ThemeDao r2 = r6.mThemeDao     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            java.lang.String r3 = "theme"
            com.fossil.pq7.b(r0, r3)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            r2.upsertTheme(r0)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            java.lang.String r3 = com.portfolio.platform.data.source.ThemeRepository.TAG     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            r4.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            java.lang.String r5 = "initializeLocalTheme - add theme "
            r4.append(r5)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            r4.append(r0)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
            r2.d(r3, r0)     // Catch:{ Exception -> 0x0081, all -> 0x00ad }
        L_0x006d:
            r1.close()
        L_0x0070:
            return
        L_0x0071:
            java.io.BufferedReader r2 = new java.io.BufferedReader
            r4 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r0, r4)
            r1 = r2
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007c }
        L_0x007c:
            r2 = move-exception
            com.fossil.so7.a(r1, r0)
            throw r2
        L_0x0081:
            r0 = move-exception
        L_0x0082:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x00a4 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x00a4 }
            java.lang.String r3 = com.portfolio.platform.data.source.ThemeRepository.TAG     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a4 }
            r4.<init>()     // Catch:{ all -> 0x00a4 }
            java.lang.String r5 = "Exception when parse theme file "
            r4.append(r5)     // Catch:{ all -> 0x00a4 }
            r4.append(r0)     // Catch:{ all -> 0x00a4 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x00a4 }
            r2.d(r3, r0)     // Catch:{ all -> 0x00a4 }
            if (r1 == 0) goto L_0x0070
            r1.close()
            goto L_0x0070
        L_0x00a4:
            r0 = move-exception
        L_0x00a5:
            if (r1 == 0) goto L_0x00aa
            r1.close()
        L_0x00aa:
            throw r0
        L_0x00ab:
            r0 = move-exception
            goto L_0x0082
        L_0x00ad:
            r0 = move-exception
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThemeRepository.loadTheme(java.lang.String):void");
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThemeRepository$cleanUp$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object deleteTheme(Theme theme, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThemeRepository$deleteTheme$Anon2(this, theme, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object getCurrentTheme(qn7<? super Theme> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getCurrentTheme$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getCurrentThemeId(qn7<? super String> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getCurrentThemeId$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getListStyleById(String str, qn7<? super ArrayList<Style>> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getListStyleById$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object getListTheme(qn7<? super List<Theme>> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getListTheme$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getListThemeId(qn7<? super List<String>> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getListThemeId$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final Object getListThemeIdByType(String str, qn7<? super List<String>> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getListThemeIdByType$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object getNameById(String str, qn7<? super String> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getNameById$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object getThemeById(String str, qn7<? super Theme> qn7) {
        return eu7.g(bw7.b(), new ThemeRepository$getThemeById$Anon2(this, str, null), qn7);
    }

    @DexIgnore
    public final Object initializeLocalTheme(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThemeRepository$initializeLocalTheme$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object setCurrentThemeId(String str, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThemeRepository$setCurrentThemeId$Anon2(this, str, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object upsertUserTheme(Theme theme, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new ThemeRepository$upsertUserTheme$Anon2(this, theme, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
