package com.portfolio.platform.data.source;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WatchAppRepository";
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ WatchAppRemoteDataSource mWatchAppRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public WatchAppRepository(WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        pq7.c(watchAppRemoteDataSource, "mWatchAppRemoteDataSource");
        pq7.c(portfolioApp, "mPortfolioApp");
        this.mWatchAppRemoteDataSource = watchAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.WatchAppRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.data.source.WatchAppRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WatchAppRepository r0 = (com.portfolio.platform.data.source.WatchAppRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchAppDao r0 = r0.getWatchAppDao()
            r0.clearAll()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.WatchAppRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.cleanUp(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadWatchApp(java.lang.String r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.downloadWatchApp(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllWatchAppRaw(com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.WatchApp>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.WatchAppRepository$getAllWatchAppRaw$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.source.WatchAppRepository$getAllWatchAppRaw$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppRepository$getAllWatchAppRaw$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WatchAppRepository r0 = (com.portfolio.platform.data.source.WatchAppRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchAppDao r0 = r0.getWatchAppDao()
            java.util.List r0 = r0.getAllWatchApp()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.WatchAppRepository$getAllWatchAppRaw$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppRepository$getAllWatchAppRaw$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.getAllWatchAppRaw(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchAppByIds(java.util.List<java.lang.String> r6, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.WatchApp>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$Anon1
            if (r0 == 0) goto L_0x0042
            r0 = r7
            com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0051
            if (r3 != r4) goto L_0x0049
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WatchAppRepository r1 = (com.portfolio.platform.data.source.WatchAppRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchAppDao r0 = r0.getWatchAppDao()
            java.util.List r0 = r0.getWatchAppByIds(r6)
            com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1 r1 = new com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1
            r1.<init>(r6)
            java.util.List r0 = com.fossil.pm7.b0(r0, r1)
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppRepository$getWatchAppByIds$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.el7.b(r2)
            boolean r2 = r6.isEmpty()
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x006b
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0041
        L_0x006b:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.getWatchAppByIds(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:23:0x00a0 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object queryWatchAppByName(java.lang.String r10, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.WatchApp>> r11) {
        /*
            r9 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 1
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.WatchAppRepository$queryWatchAppByName$Anon1
            if (r0 == 0) goto L_0x0079
            r0 = r11
            com.portfolio.platform.data.source.WatchAppRepository$queryWatchAppByName$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppRepository$queryWatchAppByName$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0079
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0088
            if (r0 != r8) goto L_0x0080
            java.lang.Object r0 = r2.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.WatchAppRepository r2 = (com.portfolio.platform.data.source.WatchAppRepository) r2
            com.fossil.el7.b(r3)
            r4 = r0
        L_0x0030:
            r0 = r3
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchAppDao r0 = r0.getWatchAppDao()
            java.util.List r0 = r0.getAllWatchApp()
            java.util.Iterator r3 = r0.iterator()
        L_0x003f:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00a0
            java.lang.Object r0 = r3.next()
            com.portfolio.platform.data.model.diana.WatchApp r0 = (com.portfolio.platform.data.model.diana.WatchApp) r0
            com.portfolio.platform.PortfolioApp r5 = r2.mPortfolioApp
            java.lang.String r6 = r0.getNameKey()
            java.lang.String r7 = r0.getName()
            java.lang.String r5 = com.fossil.um5.d(r5, r6, r7)
            java.text.Normalizer$Form r6 = java.text.Normalizer.Form.NFC
            java.lang.String r5 = java.text.Normalizer.normalize(r5, r6)
            java.text.Normalizer$Form r6 = java.text.Normalizer.Form.NFC
            java.lang.String r6 = java.text.Normalizer.normalize(r1, r6)
            java.lang.String r7 = "name"
            com.fossil.pq7.b(r5, r7)
            java.lang.String r7 = "searchQuery"
            com.fossil.pq7.b(r6, r7)
            boolean r5 = com.fossil.wt7.u(r5, r6, r8)
            if (r5 == 0) goto L_0x003f
            r4.add(r0)
            goto L_0x003f
        L_0x0079:
            com.portfolio.platform.data.source.WatchAppRepository$queryWatchAppByName$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppRepository$queryWatchAppByName$Anon1
            r0.<init>(r9, r11)
            r2 = r0
            goto L_0x0014
        L_0x0080:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0088:
            com.fossil.el7.b(r3)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r2.L$0 = r9
            r2.L$1 = r10
            r2.L$2 = r0
            r2.label = r8
            java.lang.Object r3 = r1.v(r2)
            if (r3 != r4) goto L_0x00a1
        L_0x00a0:
            return r4
        L_0x00a1:
            r1 = r10
            r4 = r0
            r2 = r9
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.queryWatchAppByName(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
