package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.uz4;
import com.fossil.xw0;
import com.portfolio.platform.data.model.LocalFile;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDao_Impl extends FileDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<LocalFile> __deletionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ uz4 __fileTypeConverter; // = new uz4();
    @DexIgnore
    public /* final */ jw0<LocalFile> __insertionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearLocalFileTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteLocalFileByType;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteLocalFileByUri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<LocalFile> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, LocalFile localFile) {
            px0.bindLong(1, (long) localFile.getPinType());
            String a2 = FileDao_Impl.this.__fileTypeConverter.a(localFile.getType());
            if (a2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, a2);
            }
            if (localFile.getFileName() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, localFile.getFileName());
            }
            if (localFile.getLocalUri() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, localFile.getLocalUri());
            }
            if (localFile.getRemoteUrl() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, localFile.getRemoteUrl());
            }
            if (localFile.getChecksum() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, localFile.getChecksum());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `localfile` (`pinType`,`type`,`fileName`,`localUri`,`remoteUrl`,`checksum`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<LocalFile> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, LocalFile localFile) {
            if (localFile.getRemoteUrl() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, localFile.getRemoteUrl());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `localfile` WHERE `remoteUrl` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM localfile WHERE localUri=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM localfile WHERE type=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM localfile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<LocalFile>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon6(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<LocalFile> call() throws Exception {
            Cursor b = ex0.b(FileDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "type");
                int c3 = dx0.c(b, "fileName");
                int c4 = dx0.c(b, "localUri");
                int c5 = dx0.c(b, "remoteUrl");
                int c6 = dx0.c(b, "checksum");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                    localFile.setPinType(b.getInt(c));
                    localFile.setType(FileDao_Impl.this.__fileTypeConverter.b(b.getString(c2)));
                    arrayList.add(localFile);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public FileDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfLocalFile = new Anon1(qw0);
        this.__deletionAdapterOfLocalFile = new Anon2(qw0);
        this.__preparedStmtOfDeleteLocalFileByUri = new Anon3(qw0);
        this.__preparedStmtOfDeleteLocalFileByType = new Anon4(qw0);
        this.__preparedStmtOfClearLocalFileTable = new Anon5(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void clearLocalFileTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearLocalFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearLocalFileTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfLocalFile.handle(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByType(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteLocalFileByType.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByType.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByUri(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteLocalFileByUri.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByUri.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LocalFile getFileByFileName(String str) {
        LocalFile localFile = null;
        tw0 f = tw0.f("SELECT * FROM localfile WHERE fileName=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "fileName");
            int c4 = dx0.c(b, "localUri");
            int c5 = dx0.c(b, "remoteUrl");
            int c6 = dx0.c(b, "checksum");
            if (b.moveToFirst()) {
                localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
            }
            return localFile;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListLocalFile() {
        tw0 f = tw0.f("SELECT * FROM localfile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "fileName");
            int c4 = dx0.c(b, "localUri");
            int c5 = dx0.c(b, "remoteUrl");
            int c6 = dx0.c(b, "checksum");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListPendingFile() {
        tw0 f = tw0.f("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "fileName");
            int c4 = dx0.c(b, "localUri");
            int c5 = dx0.c(b, "remoteUrl");
            int c6 = dx0.c(b, "checksum");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LiveData<List<LocalFile>> getListPendingFileAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon6 anon6 = new Anon6(f);
        return invalidationTracker.d(new String[]{"localfile"}, false, anon6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LocalFile getLocalFileByRemoteUrl(String str) {
        LocalFile localFile = null;
        tw0 f = tw0.f("SELECT * FROM localfile WHERE remoteUrl=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "fileName");
            int c4 = dx0.c(b, "localUri");
            int c5 = dx0.c(b, "remoteUrl");
            int c6 = dx0.c(b, "checksum");
            if (b.moveToFirst()) {
                localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
            }
            return localFile;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void insertListLocalFile(List<LocalFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void upsertLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert((jw0<LocalFile>) localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
