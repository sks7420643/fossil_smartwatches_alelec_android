package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSampleRepository.class.getSimpleName();
        pq7.b(simpleName, "HeartRateSampleRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSampleRepository(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchHeartRateSamples$default(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, qn7 qn7, int i3, Object obj) {
        return heartRateSampleRepository.fetchHeartRateSamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, qn7);
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new HeartRateSampleRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object fetchHeartRateSamples(Date date, Date date2, int i, int i2, qn7<? super iq5<ApiResponse<HeartRate>>> qn7) {
        return eu7.g(bw7.b(), new HeartRateSampleRepository$fetchHeartRateSamples$Anon2(this, date, date2, i, i2, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getHeartRateSamples(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.qn7<? super androidx.lifecycle.LiveData<com.fossil.h47<java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.HeartRateSampleRepository r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.pq7.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.jx7 r8 = com.fossil.bw7.c()
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples(java.util.Date, java.util.Date, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<HeartRateSample> list, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new HeartRateSampleRepository$insertFromDevice$Anon2(list, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }
}
