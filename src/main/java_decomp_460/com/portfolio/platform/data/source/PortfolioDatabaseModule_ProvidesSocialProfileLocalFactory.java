package com.portfolio.platform.data.source;

import com.fossil.fu4;
import com.fossil.jt4;
import com.fossil.lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory implements Factory<fu4> {
    @DexIgnore
    public /* final */ Provider<jt4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<jt4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<jt4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static fu4 providesSocialProfileLocal(PortfolioDatabaseModule portfolioDatabaseModule, jt4 jt4) {
        fu4 providesSocialProfileLocal = portfolioDatabaseModule.providesSocialProfileLocal(jt4);
        lk7.c(providesSocialProfileLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialProfileLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fu4 get() {
        return providesSocialProfileLocal(this.module, this.daoProvider.get());
    }
}
