package com.portfolio.platform.data.source;

import com.fossil.on5;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRepository_Factory implements Factory<UserRepository> {
    @DexIgnore
    public /* final */ Provider<on5> mSharedPreferencesManagerProvider;
    @DexIgnore
    public /* final */ Provider<UserRemoteDataSource> mUserRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<UserSettingDao> mUserSettingDaoProvider;

    @DexIgnore
    public UserRepository_Factory(Provider<UserRemoteDataSource> provider, Provider<UserSettingDao> provider2, Provider<on5> provider3) {
        this.mUserRemoteDataSourceProvider = provider;
        this.mUserSettingDaoProvider = provider2;
        this.mSharedPreferencesManagerProvider = provider3;
    }

    @DexIgnore
    public static UserRepository_Factory create(Provider<UserRemoteDataSource> provider, Provider<UserSettingDao> provider2, Provider<on5> provider3) {
        return new UserRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static UserRepository newInstance(UserRemoteDataSource userRemoteDataSource, UserSettingDao userSettingDao, on5 on5) {
        return new UserRepository(userRemoteDataSource, userSettingDao, on5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public UserRepository get() {
        return newInstance(this.mUserRemoteDataSourceProvider.get(), this.mUserSettingDaoProvider.get(), this.mSharedPreferencesManagerProvider.get());
    }
}
