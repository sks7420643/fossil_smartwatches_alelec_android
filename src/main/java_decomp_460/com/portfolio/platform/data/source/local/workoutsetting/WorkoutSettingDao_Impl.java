package com.portfolio.platform.data.source.local.workoutsetting;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.q05;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDao_Impl implements WorkoutSettingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WorkoutSetting> __insertionAdapterOfWorkoutSetting;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ q05 __workoutTypeConverter; // = new q05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WorkoutSetting> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WorkoutSetting workoutSetting) {
            String l = WorkoutSettingDao_Impl.this.__workoutTypeConverter.l(workoutSetting.getType());
            if (l == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, l);
            }
            String f = WorkoutSettingDao_Impl.this.__workoutTypeConverter.f(workoutSetting.getMode());
            if (f == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, f);
            }
            px0.bindLong(3, workoutSetting.getEnable() ? 1 : 0);
            px0.bindLong(4, workoutSetting.getAskMeFirst() ? 1 : 0);
            px0.bindLong(5, (long) workoutSetting.getStartLatency());
            px0.bindLong(6, (long) workoutSetting.getPauseLatency());
            px0.bindLong(7, (long) workoutSetting.getResumeLatency());
            px0.bindLong(8, (long) workoutSetting.getStopLatency());
            px0.bindLong(9, workoutSetting.getCreatedAt());
            px0.bindLong(10, workoutSetting.getUpdatedAt());
            px0.bindLong(11, (long) workoutSetting.getPinType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workoutSetting` (`type`,`mode`,`enable`,`askMeFirst`,`startLatency`,`pauseLatency`,`resumeLatency`,`stopLatency`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM workoutSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WorkoutSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSetting> call() throws Exception {
            Cursor b = ex0.b(WorkoutSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "type");
                int c2 = dx0.c(b, "mode");
                int c3 = dx0.c(b, "enable");
                int c4 = dx0.c(b, "askMeFirst");
                int c5 = dx0.c(b, "startLatency");
                int c6 = dx0.c(b, "pauseLatency");
                int c7 = dx0.c(b, "resumeLatency");
                int c8 = dx0.c(b, "stopLatency");
                int c9 = dx0.c(b, "createdAt");
                int c10 = dx0.c(b, "updatedAt");
                int c11 = dx0.c(b, "pinType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WorkoutSetting(WorkoutSettingDao_Impl.this.__workoutTypeConverter.x(b.getString(c)), WorkoutSettingDao_Impl.this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WorkoutSettingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWorkoutSetting = new Anon1(qw0);
        this.__preparedStmtOfCleanUp = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getPendingWorkoutSettings() {
        tw0 f = tw0.f("SELECT * FROM workoutSetting where pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "type");
            int c2 = dx0.c(b, "mode");
            int c3 = dx0.c(b, "enable");
            int c4 = dx0.c(b, "askMeFirst");
            int c5 = dx0.c(b, "startLatency");
            int c6 = dx0.c(b, "pauseLatency");
            int c7 = dx0.c(b, "resumeLatency");
            int c8 = dx0.c(b, "stopLatency");
            int c9 = dx0.c(b, "createdAt");
            int c10 = dx0.c(b, "updatedAt");
            int c11 = dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.x(b.getString(c)), this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getWorkoutSettingList() {
        tw0 f = tw0.f("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "type");
            int c2 = dx0.c(b, "mode");
            int c3 = dx0.c(b, "enable");
            int c4 = dx0.c(b, "askMeFirst");
            int c5 = dx0.c(b, "startLatency");
            int c6 = dx0.c(b, "pauseLatency");
            int c7 = dx0.c(b, "resumeLatency");
            int c8 = dx0.c(b, "stopLatency");
            int c9 = dx0.c(b, "createdAt");
            int c10 = dx0.c(b, "updatedAt");
            int c11 = dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.x(b.getString(c)), this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public LiveData<List<WorkoutSetting>> getWorkoutSettingListAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"workoutSetting"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSetting(WorkoutSetting workoutSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert((jw0<WorkoutSetting>) workoutSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSettingList(List<WorkoutSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
