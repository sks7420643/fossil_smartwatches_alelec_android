package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSettingRepository$downloadWorkoutSettings$2", f = "WorkoutSettingRepository.kt", l = {38, 40, 47}, m = "invokeSuspend")
public final class WorkoutSettingRepository$downloadWorkoutSettings$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSettingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSettingRepository$downloadWorkoutSettings$Anon2(WorkoutSettingRepository workoutSettingRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSettingRepository$downloadWorkoutSettings$Anon2 workoutSettingRepository$downloadWorkoutSettings$Anon2 = new WorkoutSettingRepository$downloadWorkoutSettings$Anon2(this.this$0, qn7);
        workoutSettingRepository$downloadWorkoutSettings$Anon2.p$ = (iv7) obj;
        return workoutSettingRepository$downloadWorkoutSettings$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((WorkoutSettingRepository$downloadWorkoutSettings$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x012c  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 303
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository$downloadWorkoutSettings$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
