package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.WatchParameterResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1", f = "DeviceRemoteDataSource.kt", l = {143}, m = "invokeSuspend")
public final class DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 extends ko7 implements rp7<qn7<? super q88<ApiResponse<WatchParameterResponse>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(DeviceRemoteDataSource deviceRemoteDataSource, String str, int i, qn7 qn7) {
        super(1, qn7);
        this.this$0 = deviceRemoteDataSource;
        this.$serial = str;
        this.$majorVersion = i;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this.this$0, this.$serial, this.$majorVersion, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<WatchParameterResponse>>> qn7) {
        return ((DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiService;
            String str = this.$serial;
            int i2 = this.$majorVersion;
            this.label = 1;
            Object latestWatchParams = apiServiceV2.getLatestWatchParams(str, i2, "-metadata.version.minor", 0, 1, this);
            return latestWatchParams == d ? d : latestWatchParams;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
