package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository$loadSummaries$2", f = "HeartRateSummaryRepository.kt", l = {109, 121}, m = "invokeSuspend")
public final class HeartRateSummaryRepository$loadSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<gj4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$loadSummaries$Anon2(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        HeartRateSummaryRepository$loadSummaries$Anon2 heartRateSummaryRepository$loadSummaries$Anon2 = new HeartRateSummaryRepository$loadSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, qn7);
        heartRateSummaryRepository$loadSummaries$Anon2.p$ = (iv7) obj;
        return heartRateSummaryRepository$loadSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<gj4>> qn7) {
        return ((HeartRateSummaryRepository$loadSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0137  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
        // Method dump skipped, instructions count: 399
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSummaryRepository$loadSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
