package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRepository_Factory implements Factory<WatchAppRepository> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppRemoteDataSource> mWatchAppRemoteDataSourceProvider;

    @DexIgnore
    public WatchAppRepository_Factory(Provider<WatchAppRemoteDataSource> provider, Provider<PortfolioApp> provider2) {
        this.mWatchAppRemoteDataSourceProvider = provider;
        this.mPortfolioAppProvider = provider2;
    }

    @DexIgnore
    public static WatchAppRepository_Factory create(Provider<WatchAppRemoteDataSource> provider, Provider<PortfolioApp> provider2) {
        return new WatchAppRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static WatchAppRepository newInstance(WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        return new WatchAppRepository(watchAppRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppRepository get() {
        return newInstance(this.mWatchAppRemoteDataSourceProvider.get(), this.mPortfolioAppProvider.get());
    }
}
