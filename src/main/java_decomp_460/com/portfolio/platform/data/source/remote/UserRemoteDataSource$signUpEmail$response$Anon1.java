package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$1", f = "UserRemoteDataSource.kt", l = {197}, m = "invokeSuspend")
public final class UserRemoteDataSource$signUpEmail$response$Anon1 extends ko7 implements rp7<qn7<? super q88<Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpEmailAuth $emailAuth;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$signUpEmail$response$Anon1(UserRemoteDataSource userRemoteDataSource, SignUpEmailAuth signUpEmailAuth, qn7 qn7) {
        super(1, qn7);
        this.this$0 = userRemoteDataSource;
        this.$emailAuth = signUpEmailAuth;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new UserRemoteDataSource$signUpEmail$response$Anon1(this.this$0, this.$emailAuth, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<Auth>> qn7) {
        return ((UserRemoteDataSource$signUpEmail$response$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            AuthApiGuestService authApiGuestService = this.this$0.mAuthApiGuestService;
            SignUpEmailAuth signUpEmailAuth = this.$emailAuth;
            this.label = 1;
            Object registerEmail = authApiGuestService.registerEmail(signUpEmailAuth, this);
            return registerEmail == d ? d : registerEmail;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
