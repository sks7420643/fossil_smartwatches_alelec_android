package com.portfolio.platform.data.source;

import com.fossil.br7;
import com.fossil.dl7;
import com.fossil.it3;
import com.fossil.ku7;
import com.fossil.pq7;
import com.fossil.rh2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements it3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ku7 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ br7 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ rh2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfDataSetList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(int i, rh2 rh2, br7 br7, ku7 ku7, int i2, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$sizeOfDataSetList = i;
        this.$historyClient$inlined = rh2;
        this.$countSizeOfLists$inlined = br7;
        this.$continuation$inlined = ku7;
        this.$sizeOfListsOfGFitSampleList$inlined = i2;
        this.this$0 = thirdPartyRepository;
        this.$gFitSampleList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    @Override // com.fossil.it3
    public final void onFailure(Exception exc) {
        pq7.c(exc, "it");
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitSampleToGoogleFit - Fail");
        br7 br7 = this.$countSizeOfLists$inlined;
        int i = br7.element + 1;
        br7.element = i;
        if (i >= this.$sizeOfListsOfGFitSampleList$inlined * this.$sizeOfDataSetList && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSampleToGoogleFit - Fail");
            ku7 ku7 = this.$continuation$inlined;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(null));
        }
    }
}
