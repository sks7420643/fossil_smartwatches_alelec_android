package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.pq7;
import com.fossil.zu0;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDifference extends zu0.d<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        pq7.c(workoutSession, "oldItem");
        pq7.c(workoutSession2, "newItem");
        return pq7.a(workoutSession, workoutSession2) && pq7.a(workoutSession.getScreenShotUri(), workoutSession2.getScreenShotUri());
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        pq7.c(workoutSession, "oldItem");
        pq7.c(workoutSession2, "newItem");
        return pq7.a(workoutSession.getId(), workoutSession2.getId());
    }
}
