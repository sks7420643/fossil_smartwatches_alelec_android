package com.portfolio.platform.data.source;

import com.facebook.appevents.UserDataStore;
import com.fossil.bu4;
import com.fossil.cu4;
import com.fossil.dr7;
import com.fossil.ft4;
import com.fossil.fu4;
import com.fossil.gu4;
import com.fossil.h97;
import com.fossil.j97;
import com.fossil.jt4;
import com.fossil.lr4;
import com.fossil.nr4;
import com.fossil.on5;
import com.fossil.or4;
import com.fossil.pq7;
import com.fossil.pw0;
import com.fossil.qs4;
import com.fossil.qw0;
import com.fossil.r77;
import com.fossil.rt4;
import com.fossil.sk5;
import com.fossil.st4;
import com.fossil.xt4;
import com.fossil.yo5;
import com.fossil.ys4;
import com.fossil.yt4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule {
    @DexIgnore
    public final AddressDao provideAddressDao(AddressDatabase addressDatabase) {
        pq7.c(addressDatabase, UserDataStore.DATE_OF_BIRTH);
        return addressDatabase.addressDao();
    }

    @DexIgnore
    public final AddressDatabase provideAddressDatabase(PortfolioApp portfolioApp) {
        AddressDatabase addressDatabase;
        synchronized (this) {
            pq7.c(portfolioApp, "app");
            qw0.a a2 = pw0.a(portfolioApp, AddressDatabase.class, "address.db");
            a2.f();
            qw0 d = a2.d();
            pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
            addressDatabase = (AddressDatabase) d;
        }
        return addressDatabase;
    }

    @DexIgnore
    public final AppSettingsDatabase provideAppSettingsDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, AppSettingsDatabase.class, "app_settings_database.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (AppSettingsDatabase) d;
    }

    @DexIgnore
    public final CategoryDao provideCategoryDao(CategoryDatabase categoryDatabase) {
        pq7.c(categoryDatabase, UserDataStore.DATE_OF_BIRTH);
        return categoryDatabase.categoryDao();
    }

    @DexIgnore
    public final CategoryDatabase provideCategoryDatabase(PortfolioApp portfolioApp) {
        CategoryDatabase categoryDatabase;
        synchronized (this) {
            pq7.c(portfolioApp, "app");
            qw0.a a2 = pw0.a(portfolioApp, CategoryDatabase.class, "category.db");
            a2.f();
            qw0 d = a2.d();
            pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
            categoryDatabase = (CategoryDatabase) d;
        }
        return categoryDatabase;
    }

    @DexIgnore
    public final CustomizeRealDataDao provideCustomizeRealDataDao(CustomizeRealDataDatabase customizeRealDataDatabase) {
        pq7.c(customizeRealDataDatabase, UserDataStore.DATE_OF_BIRTH);
        return customizeRealDataDatabase.realDataDao();
    }

    @DexIgnore
    public final CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, CustomizeRealDataDatabase.class, "customizeRealData.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (CustomizeRealDataDatabase) d;
    }

    @DexIgnore
    public final DNDSettingsDatabase provideDNDSettingsDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, DNDSettingsDatabase.class, "dndSettings.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DNDSettingsDatabase) d;
    }

    @DexIgnore
    public final DeviceDao provideDeviceDao(DeviceDatabase deviceDatabase) {
        pq7.c(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.deviceDao();
    }

    @DexIgnore
    public final DeviceDatabase provideDeviceDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, DeviceDatabase.class, "devices.db");
        a2.b(DeviceDatabase.Companion.getMIGRATION_FROM_1_TO_2(), DeviceDatabase.Companion.getMIGRATION_FROM_2_TO_3(), DeviceDatabase.Companion.getMIGRATION_FROM_3_TO_4());
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DeviceDatabase) d;
    }

    @DexIgnore
    public final FileDao provideFileDao(FileDatabase fileDatabase) {
        pq7.c(fileDatabase, UserDataStore.DATE_OF_BIRTH);
        return fileDatabase.fileDao();
    }

    @DexIgnore
    public final FileDatabase provideFileDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, FileDatabase.class, "file.db");
        a2.b(FileDatabase.Companion.getMIGRATION_FROM_1_TO_2());
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (FileDatabase) d;
    }

    @DexIgnore
    public final sk5 provideFitnessHelper(on5 on5) {
        pq7.c(on5, "sharedPreferencesManager");
        return new sk5(on5);
    }

    @DexIgnore
    public final HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, HybridCustomizeDatabase.class, "hybridCustomize.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room\n                .da\u2026\n                .build()");
        return (HybridCustomizeDatabase) d;
    }

    @DexIgnore
    public final InAppNotificationDao provideInAppNotificationDao(InAppNotificationDatabase inAppNotificationDatabase) {
        pq7.c(inAppNotificationDatabase, UserDataStore.DATE_OF_BIRTH);
        return inAppNotificationDatabase.inAppNotificationDao();
    }

    @DexIgnore
    public final InAppNotificationDatabase provideInAppNotificationDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, InAppNotificationDatabase.class, "inAppNotification.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (InAppNotificationDatabase) d;
    }

    @DexIgnore
    public final MicroAppDao provideMicroAppDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        pq7.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppDao();
    }

    @DexIgnore
    public final MicroAppLastSettingDao provideMicroAppLastSettingDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        pq7.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppLastSettingDao();
    }

    @DexIgnore
    public final NotificationSettingsDao provideNotificationSettingsDao(NotificationSettingsDatabase notificationSettingsDatabase) {
        pq7.c(notificationSettingsDatabase, UserDataStore.DATE_OF_BIRTH);
        return notificationSettingsDatabase.getNotificationSettingsDao();
    }

    @DexIgnore
    public final NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, NotificationSettingsDatabase.class, "notificationSettings.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (NotificationSettingsDatabase) d;
    }

    @DexIgnore
    public final HybridPresetDao providePresetDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        pq7.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.presetDao();
    }

    @DexIgnore
    public final QuickResponseDatabase provideQuickResponseDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        dr7 dr7 = new dr7();
        dr7.element = null;
        qw0.a a2 = pw0.a(portfolioApp.getApplicationContext(), QuickResponseDatabase.class, "quickResponse.db");
        a2.f();
        a2.a(new PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1(dr7));
        T t = (T) ((QuickResponseDatabase) a2.d());
        dr7.element = t;
        return t;
    }

    @DexIgnore
    public final QuickResponseMessageDao provideQuickResponseMessageDao(QuickResponseDatabase quickResponseDatabase) {
        pq7.c(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseMessageDao();
    }

    @DexIgnore
    public final QuickResponseSenderDao provideQuickResponseSenderDao(QuickResponseDatabase quickResponseDatabase) {
        pq7.c(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseSenderDao();
    }

    @DexIgnore
    public final RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, RemindersSettingsDatabase.class, "remindersSettings.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (RemindersSettingsDatabase) d;
    }

    @DexIgnore
    public final SkuDao provideSkuDao(DeviceDatabase deviceDatabase) {
        pq7.c(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.skuDao();
    }

    @DexIgnore
    public final ThemeDao provideThemeDao(ThemeDatabase themeDatabase) {
        pq7.c(themeDatabase, UserDataStore.DATE_OF_BIRTH);
        return themeDatabase.themeDao();
    }

    @DexIgnore
    public final ThemeDatabase provideThemeDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, ThemeDatabase.class, "theme.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (ThemeDatabase) d;
    }

    @DexIgnore
    public final UserSettingDao provideUserSettingDao(UserSettingDatabase userSettingDatabase) {
        UserSettingDao userSettingDao;
        synchronized (this) {
            pq7.c(userSettingDatabase, UserDataStore.DATE_OF_BIRTH);
            userSettingDao = userSettingDatabase.userSettingDao();
        }
        return userSettingDao;
    }

    @DexIgnore
    public final UserSettingDatabase provideUserSettingDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, UserSettingDatabase.class, "userSetting.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (UserSettingDatabase) d;
    }

    @DexIgnore
    public final BuddyChallengeDatabase providesBuddyChallengeDatabase(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "app");
        qw0.a a2 = pw0.a(portfolioApp, BuddyChallengeDatabase.class, "buddy_challenge.db");
        a2.f();
        qw0 d = a2.d();
        pq7.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (BuddyChallengeDatabase) d;
    }

    @DexIgnore
    public final qs4 providesChallengeDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        pq7.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.a();
    }

    @DexIgnore
    public final rt4 providesChallengeLocalDataSource(qs4 qs4) {
        pq7.c(qs4, "dao");
        return new rt4(qs4);
    }

    @DexIgnore
    public final st4 providesChallengeRemoteDataSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new st4(apiServiceV2);
    }

    @DexIgnore
    public final yo5 providesDianaRecommendedPresetRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new yo5(apiServiceV2);
    }

    @DexIgnore
    public final lr4 providesFlagDao(AppSettingsDatabase appSettingsDatabase) {
        pq7.c(appSettingsDatabase, "appSettingsDatabase");
        return appSettingsDatabase.a();
    }

    @DexIgnore
    public final nr4 providesFlagLocalSource(lr4 lr4) {
        pq7.c(lr4, "dao");
        return new nr4(lr4);
    }

    @DexIgnore
    public final or4 providesFlagRemoteSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new or4(apiServiceV2);
    }

    @DexIgnore
    public final ft4 providesNotificationDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        pq7.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.c();
    }

    @DexIgnore
    public final bu4 providesNotificationLocal(ft4 ft4) {
        pq7.c(ft4, "dao");
        return new bu4(ft4);
    }

    @DexIgnore
    public final cu4 providesNotificationRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new cu4(apiServiceV2);
    }

    @DexIgnore
    public final ys4 providesSocialFriendDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        pq7.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.b();
    }

    @DexIgnore
    public final xt4 providesSocialFriendLocal(ys4 ys4) {
        pq7.c(ys4, "dao");
        return new xt4(ys4);
    }

    @DexIgnore
    public final yt4 providesSocialFriendRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new yt4(apiServiceV2);
    }

    @DexIgnore
    public final jt4 providesSocialProfileDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        pq7.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.d();
    }

    @DexIgnore
    public final fu4 providesSocialProfileLocal(jt4 jt4) {
        pq7.c(jt4, "dao");
        return new fu4(jt4);
    }

    @DexIgnore
    public final gu4 providesSocialProfileRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new gu4(apiServiceV2);
    }

    @DexIgnore
    public final h97 providesWFBackgroundPhotoDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        pq7.c(dianaCustomizeDatabase, "customizeDatabase");
        return dianaCustomizeDatabase.getWFBackgroundDao();
    }

    @DexIgnore
    public final j97 providesWFBackgroundPhotoRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new j97(apiServiceV2);
    }

    @DexIgnore
    public final r77 providesWFTemplateRemote(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        return new r77(apiServiceV2);
    }
}
