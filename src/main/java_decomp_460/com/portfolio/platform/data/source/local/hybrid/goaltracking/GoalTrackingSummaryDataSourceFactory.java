package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xt0;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryDataSourceFactory extends xt0.b<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ fl5.a mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        pq7.c(date, "mCreatedDate");
        pq7.c(no4, "mAppExecutors");
        pq7.c(aVar, "mListener");
        pq7.c(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = no4;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Date, GoalTrackingSummary> create() {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mCreatedDate, this.mGoalTrackingDatabase, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localDataSource = goalTrackingSummaryLocalDataSource;
        this.sourceLiveData.l(goalTrackingSummaryLocalDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource2 != null) {
            return goalTrackingSummaryLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
