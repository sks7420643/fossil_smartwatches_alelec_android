package com.portfolio.platform.data.source.local.alarm;

import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1 extends ax0 {
    @DexIgnore
    public AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "MIGRATION_FROM_5_TO_6 - start");
        lx0.beginTransaction();
        try {
            FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "Migrate Alarm - start");
            lx0.execSQL("ALTER TABLE alarm ADD COLUMN message TEXT NOT NULL DEFAULT ''");
            FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "Migrate Alarm - end");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = AlarmDatabase.TAG;
            local.e(str, "MIGRATION_FROM_5_TO_6 - end with exception -- e=" + e);
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
