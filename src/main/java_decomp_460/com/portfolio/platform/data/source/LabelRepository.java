package com.portfolio.platform.data.source;

import android.content.Context;
import com.fossil.iq5;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LabelRepository";
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ LabelRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public LabelRepository(Context context2, LabelRemoteDataSource labelRemoteDataSource) {
        pq7.c(context2, "context");
        pq7.c(labelRemoteDataSource, "mRemoteSource");
        this.context = context2;
        this.mRemoteSource = labelRemoteDataSource;
    }

    @DexIgnore
    public final Object downloadLabel(String str, qn7<? super iq5<Label>> qn7) {
        return this.mRemoteSource.getLabel(str, qn7);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }
}
