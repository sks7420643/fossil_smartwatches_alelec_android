package com.portfolio.platform.data.source;

import com.fossil.bu4;
import com.fossil.ft4;
import com.fossil.lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesNotificationLocalFactory implements Factory<bu4> {
    @DexIgnore
    public /* final */ Provider<ft4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesNotificationLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ft4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesNotificationLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ft4> provider) {
        return new PortfolioDatabaseModule_ProvidesNotificationLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static bu4 providesNotificationLocal(PortfolioDatabaseModule portfolioDatabaseModule, ft4 ft4) {
        bu4 providesNotificationLocal = portfolioDatabaseModule.providesNotificationLocal(ft4);
        lk7.c(providesNotificationLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesNotificationLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public bu4 get() {
        return providesNotificationLocal(this.module, this.daoProvider.get());
    }
}
