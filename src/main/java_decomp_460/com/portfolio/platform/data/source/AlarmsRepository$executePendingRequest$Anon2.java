package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2", f = "AlarmsRepository.kt", l = {203, 212, 214, 229, 231}, m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$Anon2 extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2$2", f = "AlarmsRepository.kt", l = {216}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super Long[]>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ iq5 $upsertResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(iq5 iq5, qn7 qn7) {
            super(2, qn7);
            this.$upsertResponse = iq5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.$upsertResponse, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Long[]> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object u;
            List<Alarm> list;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                List<Alarm> list2 = (List) ((kq5) this.$upsertResponse).a();
                if (list2 == null) {
                    return null;
                }
                bn5 bn5 = bn5.j;
                this.L$0 = iv7;
                this.L$1 = list2;
                this.label = 1;
                u = bn5.u(this);
                if (u == d) {
                    return d;
                }
                list = list2;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                list = (List) this.L$1;
                u = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return ((AlarmDatabase) u).alarmDao().insertAlarms(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2$4", f = "AlarmsRepository.kt", l = {232}, m = "invokeSuspend")
    public static final class Anon4_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $deleteAlarmPendingList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4_Level2(List list, qn7 qn7) {
            super(2, qn7);
            this.$deleteAlarmPendingList = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon4_Level2 anon4_Level2 = new Anon4_Level2(this.$deleteAlarmPendingList, qn7);
            anon4_Level2.p$ = (iv7) obj;
            return anon4_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon4_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Iterable] */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0037  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                r9 = 1
                java.lang.Object r5 = com.fossil.yn7.d()
                int r0 = r10.label
                if (r0 == 0) goto L_0x005b
                if (r0 != r9) goto L_0x0053
                java.lang.Object r0 = r10.L$4
                com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
                java.lang.Object r1 = r10.L$2
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r2 = r10.L$1
                java.lang.Iterable r2 = (java.lang.Iterable) r2
                java.lang.Object r3 = r10.L$0
                com.fossil.iv7 r3 = (com.fossil.iv7) r3
                com.fossil.el7.b(r11)
                r7 = r0
                r8 = r2
                r4 = r11
            L_0x0021:
                r0 = r4
                com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = (com.portfolio.platform.data.source.local.alarm.AlarmDatabase) r0
                com.portfolio.platform.data.source.local.alarm.AlarmDao r0 = r0.alarmDao()
                java.lang.String r2 = r7.getUri()
                r0.removeAlarm(r2)
                r6 = r1
                r2 = r5
            L_0x0031:
                boolean r0 = r6.hasNext()
                if (r0 == 0) goto L_0x006c
                java.lang.Object r1 = r6.next()
                r0 = r1
                com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
                com.fossil.bn5 r4 = com.fossil.bn5.j
                r10.L$0 = r3
                r10.L$1 = r8
                r10.L$2 = r6
                r10.L$3 = r1
                r10.L$4 = r0
                r10.label = r9
                java.lang.Object r4 = r4.u(r10)
                if (r4 != r2) goto L_0x0068
            L_0x0052:
                return r2
            L_0x0053:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x005b:
                com.fossil.el7.b(r11)
                com.fossil.iv7 r3 = r10.p$
                java.util.List r8 = r10.$deleteAlarmPendingList
                java.util.Iterator r6 = r8.iterator()
                r2 = r5
                goto L_0x0031
            L_0x0068:
                r5 = r2
                r1 = r6
                r7 = r0
                goto L_0x0021
            L_0x006c:
                com.fossil.tl7 r2 = com.fossil.tl7.f3441a
                goto L_0x0052
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$Anon2.Anon4_Level2.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$Anon2(AlarmsRepository alarmsRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$executePendingRequest$Anon2 alarmsRepository$executePendingRequest$Anon2 = new AlarmsRepository$executePendingRequest$Anon2(this.this$0, qn7);
        alarmsRepository$executePendingRequest$Anon2.p$ = (iv7) obj;
        return alarmsRepository$executePendingRequest$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
        return ((AlarmsRepository$executePendingRequest$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v80, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r1v20, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r0v84, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r1v23, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r0v86, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b9 A[LOOP:0: B:25:0x00b3->B:27:0x00b9, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0281  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x029e  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02b8  */
    /* JADX WARNING: Unknown variable types count: 5 */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 715
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
