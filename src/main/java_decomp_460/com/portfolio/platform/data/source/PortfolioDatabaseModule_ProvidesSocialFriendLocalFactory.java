package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.xt4;
import com.fossil.ys4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory implements Factory<xt4> {
    @DexIgnore
    public /* final */ Provider<ys4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ys4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ys4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static xt4 providesSocialFriendLocal(PortfolioDatabaseModule portfolioDatabaseModule, ys4 ys4) {
        xt4 providesSocialFriendLocal = portfolioDatabaseModule.providesSocialFriendLocal(ys4);
        lk7.c(providesSocialFriendLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public xt4 get() {
        return providesSocialFriendLocal(this.module, this.daoProvider.get());
    }
}
