package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.c05;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.mz4;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.rz4;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.xw0;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDao_Impl extends ActivitySummaryDao {
    @DexIgnore
    public /* final */ mz4 __activityStatisticConverter; // = new mz4();
    @DexIgnore
    public /* final */ rz4 __dateTimeConverter; // = new rz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<ActivityRecommendedGoals> __insertionAdapterOfActivityRecommendedGoals;
    @DexIgnore
    public /* final */ jw0<ActivitySettings> __insertionAdapterOfActivitySettings;
    @DexIgnore
    public /* final */ jw0<ActivityStatistic> __insertionAdapterOfActivityStatistic;
    @DexIgnore
    public /* final */ jw0<ActivitySummary> __insertionAdapterOfActivitySummary;
    @DexIgnore
    public /* final */ c05 __integerArrayConverter; // = new c05();
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllActivitySummaries;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfUpdateActivitySettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<ActivitySummary> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, ActivitySummary activitySummary) {
            px0.bindLong(1, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activitySummary.getCreatedAt()));
            px0.bindLong(2, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activitySummary.getUpdatedAt()));
            px0.bindLong(3, (long) activitySummary.getPinType());
            px0.bindLong(4, (long) activitySummary.getYear());
            px0.bindLong(5, (long) activitySummary.getMonth());
            px0.bindLong(6, (long) activitySummary.getDay());
            if (activitySummary.getTimezoneName() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, activitySummary.getTimezoneName());
            }
            if (activitySummary.getDstOffset() == null) {
                px0.bindNull(8);
            } else {
                px0.bindLong(8, (long) activitySummary.getDstOffset().intValue());
            }
            px0.bindDouble(9, activitySummary.getSteps());
            px0.bindDouble(10, activitySummary.getCalories());
            px0.bindDouble(11, activitySummary.getDistance());
            String b = ActivitySummaryDao_Impl.this.__integerArrayConverter.b(activitySummary.getIntensities());
            if (b == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, b);
            }
            px0.bindLong(13, (long) activitySummary.getActiveTime());
            px0.bindLong(14, (long) activitySummary.getStepGoal());
            px0.bindLong(15, (long) activitySummary.getCaloriesGoal());
            px0.bindLong(16, (long) activitySummary.getActiveTimeGoal());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleday` (`createdAt`,`updatedAt`,`pinType`,`year`,`month`,`day`,`timezoneName`,`dstOffset`,`steps`,`calories`,`distance`,`intensities`,`activeTime`,`stepGoal`,`caloriesGoal`,`activeTimeGoal`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<ActivityStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon10(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivityStatistic call() throws Exception {
            ActivityStatistic activityStatistic;
            Cursor b = ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "uid");
                int c3 = dx0.c(b, "activeTimeBestDay");
                int c4 = dx0.c(b, "activeTimeBestStreak");
                int c5 = dx0.c(b, "caloriesBestDay");
                int c6 = dx0.c(b, "caloriesBestStreak");
                int c7 = dx0.c(b, "stepsBestDay");
                int c8 = dx0.c(b, "stepsBestStreak");
                int c9 = dx0.c(b, "totalActiveTime");
                int c10 = dx0.c(b, "totalCalories");
                int c11 = dx0.c(b, "totalDays");
                int c12 = dx0.c(b, "totalDistance");
                int c13 = dx0.c(b, "totalSteps");
                int c14 = dx0.c(b, "totalIntensityDistInStep");
                int c15 = dx0.c(b, "createdAt");
                int c16 = dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(b.getString(c), b.getString(c2), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c3)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c4)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.d(b.getString(c5)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c6)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c7)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c8)), b.getInt(c9), b.getDouble(c10), b.getInt(c11), b.getDouble(c12), b.getInt(c13), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c14)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c15)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c16)));
                } else {
                    activityStatistic = null;
                }
                return activityStatistic;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<ActivitySettings> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, ActivitySettings activitySettings) {
            px0.bindLong(1, (long) activitySettings.getId());
            px0.bindLong(2, (long) activitySettings.getCurrentStepGoal());
            px0.bindLong(3, (long) activitySettings.getCurrentCaloriesGoal());
            px0.bindLong(4, (long) activitySettings.getCurrentActiveTimeGoal());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activitySettings` (`id`,`currentStepGoal`,`currentCaloriesGoal`,`currentActiveTimeGoal`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends jw0<ActivityRecommendedGoals> {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, ActivityRecommendedGoals activityRecommendedGoals) {
            px0.bindLong(1, (long) activityRecommendedGoals.getId());
            px0.bindLong(2, (long) activityRecommendedGoals.getRecommendedStepsGoal());
            px0.bindLong(3, (long) activityRecommendedGoals.getRecommendedCaloriesGoal());
            px0.bindLong(4, (long) activityRecommendedGoals.getRecommendedActiveTimeGoal());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activityRecommendedGoals` (`id`,`recommendedStepsGoal`,`recommendedCaloriesGoal`,`recommendedActiveTimeGoal`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends jw0<ActivityStatistic> {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, ActivityStatistic activityStatistic) {
            if (activityStatistic.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, activityStatistic.getId());
            }
            if (activityStatistic.getUid() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, activityStatistic.getUid());
            }
            String a2 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestDay());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
            String a3 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestStreak());
            if (a3 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a3);
            }
            String b = ActivitySummaryDao_Impl.this.__activityStatisticConverter.b(activityStatistic.getCaloriesBestDay());
            if (b == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, b);
            }
            String a4 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestStreak());
            if (a4 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, a4);
            }
            String a5 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestDay());
            if (a5 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a5);
            }
            String a6 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestStreak());
            if (a6 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, a6);
            }
            px0.bindLong(9, (long) activityStatistic.getTotalActiveTime());
            px0.bindDouble(10, activityStatistic.getTotalCalories());
            px0.bindLong(11, (long) activityStatistic.getTotalDays());
            px0.bindDouble(12, activityStatistic.getTotalDistance());
            px0.bindLong(13, (long) activityStatistic.getTotalSteps());
            String b2 = ActivitySummaryDao_Impl.this.__integerArrayConverter.b(activityStatistic.getTotalIntensityDistInStep());
            if (b2 == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, b2);
            }
            px0.bindLong(15, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activityStatistic.getCreatedAt()));
            px0.bindLong(16, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activityStatistic.getUpdatedAt()));
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_statistic` (`id`,`uid`,`activeTimeBestDay`,`activeTimeBestStreak`,`caloriesBestDay`,`caloriesBestStreak`,`stepsBestDay`,`stepsBestStreak`,`totalActiveTime`,`totalCalories`,`totalDays`,`totalDistance`,`totalSteps`,`totalIntensityDistInStep`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM sampleday";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends xw0 {
        @DexIgnore
        public Anon6(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE activitySettings SET currentStepGoal = ?, currentCaloriesGoal = ?, currentActiveTimeGoal = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<ActivitySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon7(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySummary call() throws Exception {
            ActivitySummary activitySummary;
            Cursor b = ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "year");
                int c5 = dx0.c(b, "month");
                int c6 = dx0.c(b, "day");
                int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
                int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
                int c9 = dx0.c(b, "steps");
                int c10 = dx0.c(b, "calories");
                int c11 = dx0.c(b, "distance");
                int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
                int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                return activitySummary;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon8(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ActivitySummary> call() throws Exception {
            Cursor b = ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "year");
                int c5 = dx0.c(b, "month");
                int c6 = dx0.c(b, "day");
                int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
                int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
                int c9 = dx0.c(b, "steps");
                int c10 = dx0.c(b, "calories");
                int c11 = dx0.c(b, "distance");
                int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
                int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    ActivitySummary activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                    arrayList.add(activitySummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<ActivitySettings> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon9(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySettings call() throws Exception {
            ActivitySettings activitySettings = null;
            Cursor b = ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
                int c3 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
                int c4 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySettings = new ActivitySettings(b.getInt(c2), b.getInt(c3), b.getInt(c4));
                    activitySettings.setId(b.getInt(c));
                }
                return activitySettings;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public ActivitySummaryDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfActivitySummary = new Anon1(qw0);
        this.__insertionAdapterOfActivitySettings = new Anon2(qw0);
        this.__insertionAdapterOfActivityRecommendedGoals = new Anon3(qw0);
        this.__insertionAdapterOfActivityStatistic = new Anon4(qw0);
        this.__preparedStmtOfDeleteAllActivitySummaries = new Anon5(qw0);
        this.__preparedStmtOfUpdateActivitySettings = new Anon6(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void deleteAllActivitySummaries() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllActivitySummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySettings getActivitySetting() {
        ActivitySettings activitySettings = null;
        tw0 f = tw0.f("SELECT * FROM activitySettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
            int c3 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
            int c4 = dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
            if (b.moveToFirst()) {
                activitySettings = new ActivitySettings(b.getInt(c2), b.getInt(c3), b.getInt(c4));
                activitySettings.setId(b.getInt(c));
            }
            return activitySettings;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySettings> getActivitySettingLiveData() {
        tw0 f = tw0.f("SELECT * FROM activitySettings LIMIT 1", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon9 anon9 = new Anon9(f);
        return invalidationTracker.d(new String[]{com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME}, false, anon9);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivityStatistic getActivityStatistic() {
        Throwable th;
        ActivityStatistic activityStatistic;
        tw0 f = tw0.f("SELECT * FROM activity_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uid");
            int c3 = dx0.c(b, "activeTimeBestDay");
            int c4 = dx0.c(b, "activeTimeBestStreak");
            int c5 = dx0.c(b, "caloriesBestDay");
            int c6 = dx0.c(b, "caloriesBestStreak");
            int c7 = dx0.c(b, "stepsBestDay");
            int c8 = dx0.c(b, "stepsBestStreak");
            int c9 = dx0.c(b, "totalActiveTime");
            int c10 = dx0.c(b, "totalCalories");
            int c11 = dx0.c(b, "totalDays");
            int c12 = dx0.c(b, "totalDistance");
            int c13 = dx0.c(b, "totalSteps");
            try {
                int c14 = dx0.c(b, "totalIntensityDistInStep");
                int c15 = dx0.c(b, "createdAt");
                int c16 = dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(b.getString(c), b.getString(c2), this.__activityStatisticConverter.c(b.getString(c3)), this.__activityStatisticConverter.c(b.getString(c4)), this.__activityStatisticConverter.d(b.getString(c5)), this.__activityStatisticConverter.c(b.getString(c6)), this.__activityStatisticConverter.c(b.getString(c7)), this.__activityStatisticConverter.c(b.getString(c8)), b.getInt(c9), b.getDouble(c10), b.getInt(c11), b.getDouble(c12), b.getInt(c13), this.__integerArrayConverter.a(b.getString(c14)), this.__dateTimeConverter.a(b.getLong(c15)), this.__dateTimeConverter.a(b.getLong(c16)));
                } else {
                    activityStatistic = null;
                }
                b.close();
                f.m();
                return activityStatistic;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivityStatistic> getActivityStatisticLiveData() {
        tw0 f = tw0.f("SELECT * FROM activity_statistic LIMIT 1", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{ActivityStatistic.TABLE_NAME}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6) {
        tw0 f = tw0.f("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year DESC, month DESC, day DESC\n    ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "year");
            int c5 = dx0.c(b, "month");
            int c6 = dx0.c(b, "day");
            int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    ActivitySummary activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                    arrayList.add(activitySummary);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6) {
        tw0 f = tw0.f("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year ASC, month ASC, day ASC\n    ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{SampleDay.TABLE_NAME}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getActivitySummary(int i, int i2, int i3) {
        Throwable th;
        ActivitySummary activitySummary;
        tw0 f = tw0.f("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ? LIMIT 1", 3);
        f.bindLong(1, (long) i);
        f.bindLong(2, (long) i2);
        f.bindLong(3, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "year");
            int c5 = dx0.c(b, "month");
            int c6 = dx0.c(b, "day");
            int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3) {
        tw0 f = tw0.f("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ?", 3);
        f.bindLong(1, (long) i);
        f.bindLong(2, (long) i2);
        f.bindLong(3, (long) i3);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon7 anon7 = new Anon7(f);
        return invalidationTracker.d(new String[]{SampleDay.TABLE_NAME}, false, anon7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getLastSummary() {
        Throwable th;
        ActivitySummary activitySummary;
        tw0 f = tw0.f("SELECT * FROM sampleday ORDER BY year ASC, month ASC, day ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "year");
            int c5 = dx0.c(b, "month");
            int c6 = dx0.c(b, "day");
            int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3) {
        Throwable th;
        ActivitySummary activitySummary;
        tw0 f = tw0.f("SELECT * FROM sampleday\n        WHERE year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?) ORDER BY year DESC, month DESC, day DESC LIMIT 1", 6);
        long j = (long) i;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "year");
            int c5 = dx0.c(b, "month");
            int c6 = dx0.c(b, "day");
            int c7 = dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = dx0.c(b, "steps");
            int c10 = dx0.c(b, "calories");
            int c11 = dx0.c(b, "distance");
            int c12 = dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6) {
        ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = null;
        tw0 f = tw0.f("SELECT SUM(steps) as totalStepsOfWeek,  SUM(calories) as totalCaloriesOfWeek,  SUM(activeTime) as totalActiveTimeOfWeek FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "totalStepsOfWeek");
            int c2 = dx0.c(b, "totalCaloriesOfWeek");
            int c3 = dx0.c(b, "totalActiveTimeOfWeek");
            if (b.moveToFirst()) {
                totalValuesOfWeek = new ActivitySummary.TotalValuesOfWeek(b.getDouble(c), b.getDouble(c2), b.getInt(c3));
            }
            return totalValuesOfWeek;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long insertActivitySettings(ActivitySettings activitySettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivitySettings.insertAndReturnId(activitySettings);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void updateActivitySettings(int i, int i2, int i3) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfUpdateActivitySettings.acquire();
        acquire.bindLong(1, (long) i);
        acquire.bindLong(2, (long) i2);
        acquire.bindLong(3, (long) i3);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateActivitySettings.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivityRecommendedGoals.insert((jw0<ActivityRecommendedGoals>) activityRecommendedGoals);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long upsertActivityStatistic(ActivityStatistic activityStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivityStatistic.insertAndReturnId(activityStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummaries(List<ActivitySummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummary(ActivitySummary activitySummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert((jw0<ActivitySummary>) activitySummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
