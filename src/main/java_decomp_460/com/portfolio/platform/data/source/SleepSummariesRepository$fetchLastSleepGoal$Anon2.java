package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchLastSleepGoal$2", f = "SleepSummariesRepository.kt", l = {122, 126}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchLastSleepGoal$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<MFSleepSettings>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchLastSleepGoal$Anon2(SleepSummariesRepository sleepSummariesRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$fetchLastSleepGoal$Anon2 sleepSummariesRepository$fetchLastSleepGoal$Anon2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2(this.this$0, qn7);
        sleepSummariesRepository$fetchLastSleepGoal$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$fetchLastSleepGoal$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<MFSleepSettings>> qn7) {
        return ((SleepSummariesRepository$fetchLastSleepGoal$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        iv7 iv7;
        String userMessage;
        String message;
        Object d2 = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv72 = this.p$;
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "fetchLastSleepGoal");
            SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = iv72;
            this.label = 1;
            d = jq5.d(sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
            iv7 = iv72;
        } else if (i == 1) {
            el7.b(obj);
            iv7 = (iv7) this.L$0;
            d = obj;
        } else if (i == 2) {
            iq5 iq5 = (iq5) this.L$1;
            iv7 iv73 = (iv7) this.L$0;
            el7.b(obj);
            return iq5;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        iq5 iq52 = (iq5) d;
        if (iq52 instanceof kq5) {
            kq5 kq5 = (kq5) iq52;
            if (kq5.a() == null || kq5.b()) {
                return iq52;
            }
            SleepSummariesRepository sleepSummariesRepository = this.this$0;
            int sleepGoal = ((MFSleepSettings) kq5.a()).getSleepGoal();
            this.L$0 = iv7;
            this.L$1 = iq52;
            this.label = 2;
            return sleepSummariesRepository.saveSleepSettingToDB(sleepGoal, this) == d2 ? d2 : iq52;
        } else if (!(iq52 instanceof hq5)) {
            return iq52;
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = SleepSummariesRepository.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("fetchActivitySettings Failure code=");
            hq5 hq5 = (hq5) iq52;
            sb.append(hq5.a());
            sb.append(" message=");
            ServerError c = hq5.c();
            if (c == null || (message = c.getMessage()) == null) {
                ServerError c2 = hq5.c();
                userMessage = c2 != null ? c2.getUserMessage() : null;
            } else {
                userMessage = message;
            }
            if (userMessage == null) {
                userMessage = "";
            }
            sb.append(userMessage);
            local.d(str, sb.toString());
            return iq52;
        }
    }
}
