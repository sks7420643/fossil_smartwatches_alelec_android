package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSettingRepository$cleanUp$2", f = "WorkoutSettingRepository.kt", l = {33}, m = "invokeSuspend")
public final class WorkoutSettingRepository$cleanUp$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public WorkoutSettingRepository$cleanUp$Anon2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSettingRepository$cleanUp$Anon2 workoutSettingRepository$cleanUp$Anon2 = new WorkoutSettingRepository$cleanUp$Anon2(qn7);
        workoutSettingRepository$cleanUp$Anon2.p$ = (iv7) obj;
        return workoutSettingRepository$cleanUp$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((WorkoutSettingRepository$cleanUp$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object J;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            FLogger.INSTANCE.getLocal().d(WorkoutSettingRepository.TAG, "cleanUp()");
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            J = bn5.J(this);
            if (J == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            J = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((WorkoutSettingDatabase) J).workoutSettingDao().cleanUp();
        return tl7.f3441a;
    }
}
