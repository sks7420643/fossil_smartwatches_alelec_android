package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.c47;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2", f = "GoalTrackingRepository.kt", l = {175, 177}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends GoalTrackingSummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $goalTrackingDb;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<GoalTrackingSummary, GoalDailySummary> {
            @DexIgnore
            public /* final */ /* synthetic */ List $pendingList;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, List list) {
                this.this$0 = anon1_Level2;
                this.$pendingList = list;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<GoalDailySummary>> qn7) {
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                String k = lk5.k(this.this$0.this$0.$date);
                pq7.b(k, "DateHelper.formatShortDate(date)");
                return apiServiceV2.getGoalTrackingSummary(k, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<GoalTrackingSummary>> qn7) {
                return this.this$0.$goalTrackingDb.getGoalTrackingDao().getGoalTrackingSummaryLiveData(this.this$0.this$0.$date);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x003d  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
                /*
                    r6 = this;
                    r4 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x002e
                    r0 = r8
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x002e
                    int r1 = r1 + r3
                    r0.label = r1
                    r1 = r0
                L_0x0014:
                    java.lang.Object r2 = r1.result
                    java.lang.Object r0 = com.fossil.yn7.d()
                    int r3 = r1.label
                    if (r3 == 0) goto L_0x003d
                    if (r3 != r4) goto L_0x0035
                    java.lang.Object r0 = r1.L$1
                    com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r0 = (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary) r0
                    java.lang.Object r0 = r1.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.el7.b(r2)     // Catch:{ Exception -> 0x00bb }
                L_0x002b:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x002d:
                    return r0
                L_0x002e:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r6, r8)
                    r1 = r0
                    goto L_0x0014
                L_0x0035:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x003d:
                    com.fossil.el7.b(r2)
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r3 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r3 = r3.getTAG()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getSummary date="
                    r4.append(r5)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r5 = r6.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$date
                    r4.append(r5)
                    java.lang.String r5 = " saveCallResult onResponse: response = "
                    r4.append(r5)
                    r4.append(r7)
                    java.lang.String r4 = r4.toString()
                    r2.d(r3, r4)
                    com.fossil.dv7 r2 = com.fossil.bw7.b()     // Catch:{ Exception -> 0x0086 }
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r3 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4     // Catch:{ Exception -> 0x0086 }
                    r4 = 0
                    r3.<init>(r6, r7, r4)     // Catch:{ Exception -> 0x0086 }
                    r1.L$0 = r6     // Catch:{ Exception -> 0x0086 }
                    r1.L$1 = r7     // Catch:{ Exception -> 0x0086 }
                    r4 = 1
                    r1.label = r4     // Catch:{ Exception -> 0x0086 }
                    java.lang.Object r1 = com.fossil.eu7.g(r2, r3, r1)     // Catch:{ Exception -> 0x0086 }
                    if (r1 != r0) goto L_0x002b
                    goto L_0x002d
                L_0x0086:
                    r1 = move-exception
                    r0 = r6
                L_0x0088:
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r3 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r3 = r3.getTAG()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getSummary date="
                    r4.append(r5)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r0 = r0.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r0 = r0.this$0
                    java.util.Date r0 = r0.$date
                    r4.append(r0)
                    java.lang.String r0 = " exception="
                    r4.append(r0)
                    r4.append(r1)
                    java.lang.String r0 = r4.toString()
                    r2.e(r3, r0)
                    r1.printStackTrace()
                    goto L_0x002b
                L_0x00bb:
                    r1 = move-exception
                    goto L_0x0088
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary, com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(GoalDailySummary goalDailySummary, qn7 qn7) {
                return saveCallResult(goalDailySummary, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
                return this.$pendingList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getSummary$Anon2;
            this.$goalTrackingDb = goalTrackingDatabase;
        }

        @DexIgnore
        public final LiveData<h47<GoalTrackingSummary>> apply(List<GoalTrackingData> list) {
            return new Anon1_Level3(this, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2 = new GoalTrackingRepository$getSummary$Anon2(this.this$0, this.$date, qn7);
        goalTrackingRepository$getSummary$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends GoalTrackingSummary>>> qn7) {
        return ((GoalTrackingRepository$getSummary$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008b  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x004e
            if (r0 == r6) goto L_0x0030
            if (r0 != r7) goto L_0x0028
            java.lang.Object r0 = r8.L$1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            java.lang.Object r1 = r8.L$0
            com.fossil.iv7 r1 = (com.fossil.iv7) r1
            com.fossil.el7.b(r9)
            r1 = r9
            r2 = r0
        L_0x001b:
            r0 = r1
            androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r1 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2
            r1.<init>(r8, r2)
            androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r0, r1)
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            java.lang.Object r0 = r8.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0039:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = r8.this$0
            java.util.Date r4 = r8.$date
            r8.L$0 = r2
            r8.L$1 = r0
            r8.label = r7
            java.lang.Object r1 = r1.getPendingGoalTrackingDataListLiveData(r4, r4, r8)
            if (r1 != r3) goto L_0x008b
            r0 = r3
            goto L_0x0027
        L_0x004e:
            com.fossil.el7.b(r9)
            com.fossil.iv7 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSummary date="
            r4.append(r5)
            java.util.Date r5 = r8.$date
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2
            r4 = 0
            r2.<init>(r4)
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
            if (r1 != r3) goto L_0x008d
            r0 = r3
            goto L_0x0027
        L_0x008b:
            r2 = r0
            goto L_0x001b
        L_0x008d:
            r2 = r0
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
