package com.portfolio.platform.data.source.remote;

import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.xw7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRemoteDataSource implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 apiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRemoteDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = ServerSettingRemoteDataSource.class.getSimpleName();
        pq7.b(simpleName, "ServerSettingRemoteDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "apiService");
        this.apiService = apiServiceV2;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void clearData() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public ServerSetting getServerSettingByKey(String str) {
        pq7.c(str, "key");
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        pq7.c(onGetServerSettingList, Constants.CALLBACK);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this, onGetServerSettingList, null), 3, null);
    }
}
