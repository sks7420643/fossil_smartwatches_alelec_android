package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yn7;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2", f = "ThirdPartyRepository.kt", l = {92, 148, 149, 150, 151, 152, 153, 154}, m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$1", f = "ThirdPartyRepository.kt", l = {108}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSampleList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$gFitSampleList, this.$activeDeviceSerial, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitSample> list = this.$gFitSampleList;
                String str = this.$activeDeviceSerial;
                this.L$0 = iv7;
                this.label = 1;
                Object saveGFitSampleToGoogleFit = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                return saveGFitSampleToGoogleFit == d ? d : saveGFitSampleToGoogleFit;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$2", f = "ThirdPartyRepository.kt", l = {116}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitActiveTimeList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$gFitActiveTimeList, this.$activeDeviceSerial, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitActiveTime> list = this.$gFitActiveTimeList;
                String str = this.$activeDeviceSerial;
                this.L$0 = iv7;
                this.label = 1;
                Object saveGFitActiveTimeToGoogleFit = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                return saveGFitActiveTimeToGoogleFit == d ? d : saveGFitActiveTimeToGoogleFit;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$3", f = "ThirdPartyRepository.kt", l = {124}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitHeartRateList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, this.$gFitHeartRateList, this.$activeDeviceSerial, qn7);
            anon3_Level2.p$ = (iv7) obj;
            return anon3_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((Anon3_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitHeartRate> list = this.$gFitHeartRateList;
                String str = this.$activeDeviceSerial;
                this.L$0 = iv7;
                this.label = 1;
                Object saveGFitHeartRateToGoogleFit = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                return saveGFitHeartRateToGoogleFit == d ? d : saveGFitHeartRateToGoogleFit;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$4", f = "ThirdPartyRepository.kt", l = {131}, m = "invokeSuspend")
    public static final class Anon4_Level2 extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSleepList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon4_Level2 anon4_Level2 = new Anon4_Level2(this.this$0, this.$gFitSleepList, this.$activeDeviceSerial, qn7);
            anon4_Level2.p$ = (iv7) obj;
            return anon4_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((Anon4_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitSleep> list = this.$gFitSleepList;
                String str = this.$activeDeviceSerial;
                this.L$0 = iv7;
                this.label = 1;
                Object saveGFitSleepDataToGoogleFit = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                return saveGFitSleepDataToGoogleFit == d ? d : saveGFitSleepDataToGoogleFit;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$5", f = "ThirdPartyRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class Anon5_Level2 extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitWorkoutSessionList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon5_Level2 anon5_Level2 = new Anon5_Level2(this.this$0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, qn7);
            anon5_Level2.p$ = (iv7) obj;
            return anon5_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
            return ((Anon5_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitWorkoutSession> list = this.$gFitWorkoutSessionList;
                String str = this.$activeDeviceSerial;
                this.L$0 = iv7;
                this.label = 1;
                Object saveGFitWorkoutSessionToGoogleFit = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                return saveGFitWorkoutSessionToGoogleFit == d ? d : saveGFitWorkoutSessionToGoogleFit;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$Anon2(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, qn7 qn7) {
        super(2, qn7);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2 = new ThirdPartyRepository$uploadData$Anon2(this.this$0, this.$pushPendingThirdPartyDataCallback, qn7);
        thirdPartyRepository$uploadData$Anon2.p$ = (iv7) obj;
        return thirdPartyRepository$uploadData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ThirdPartyRepository$uploadData$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0217  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x030c  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0360  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0500  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0065  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r18) {
        /*
        // Method dump skipped, instructions count: 1430
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
