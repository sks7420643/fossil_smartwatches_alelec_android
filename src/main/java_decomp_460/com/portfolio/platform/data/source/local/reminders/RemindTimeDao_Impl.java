package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeDao_Impl implements RemindTimeDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<RemindTimeModel> __insertionAdapterOfRemindTimeModel;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<RemindTimeModel> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, RemindTimeModel remindTimeModel) {
            if (remindTimeModel.getRemindTimeName() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, remindTimeModel.getRemindTimeName());
            }
            px0.bindLong(2, (long) remindTimeModel.getMinutes());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `remindTimeModel` (`remindTimeName`,`minutes`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM remindTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public RemindTimeModel call() throws Exception {
            RemindTimeModel remindTimeModel = null;
            Cursor b = ex0.b(RemindTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "remindTimeName");
                int c2 = dx0.c(b, "minutes");
                if (b.moveToFirst()) {
                    remindTimeModel = new RemindTimeModel(b.getString(c), b.getInt(c2));
                }
                return remindTimeModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public RemindTimeDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfRemindTimeModel = new Anon1(qw0);
        this.__preparedStmtOfDelete = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public LiveData<RemindTimeModel> getRemindTime() {
        tw0 f = tw0.f("SELECT * FROM remindTimeModel", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"remindTimeModel"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public RemindTimeModel getRemindTimeModel() {
        RemindTimeModel remindTimeModel = null;
        tw0 f = tw0.f("SELECT * FROM remindTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "remindTimeName");
            int c2 = dx0.c(b, "minutes");
            if (b.moveToFirst()) {
                remindTimeModel = new RemindTimeModel(b.getString(c), b.getInt(c2));
            }
            return remindTimeModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void upsertRemindTimeModel(RemindTimeModel remindTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfRemindTimeModel.insert((jw0<RemindTimeModel>) remindTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
