package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$saveData$2", f = "ThirdPartyRepository.kt", l = {49}, m = "invokeSuspend")
public final class ThirdPartyRepository$saveData$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listMFSleepSession;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyDatabase $thirdPartyDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveData$Anon2 this$0;

        @DexIgnore
        public Anon1_Level2(ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2, ThirdPartyDatabase thirdPartyDatabase) {
            this.this$0 = thirdPartyRepository$saveData$Anon2;
            this.$thirdPartyDatabase = thirdPartyDatabase;
        }

        @DexIgnore
        public final void run() {
            if (!this.this$0.$listGFitSample.isEmpty()) {
                this.$thirdPartyDatabase.getGFitSampleDao().insertListGFitSample(this.this$0.$listGFitSample);
            }
            if (this.this$0.$gFitActiveTime != null) {
                this.$thirdPartyDatabase.getGFitActiveTimeDao().insertGFitActiveTime(this.this$0.$gFitActiveTime);
            }
            if (!this.this$0.$listGFitHeartRate.isEmpty()) {
                this.$thirdPartyDatabase.getGFitHeartRateDao().insertListGFitHeartRate(this.this$0.$listGFitHeartRate);
            }
            if (!this.this$0.$listGFitWorkoutSession.isEmpty()) {
                this.$thirdPartyDatabase.getGFitWorkoutSessionDao().insertListGFitWorkoutSession(this.this$0.$listGFitWorkoutSession);
            }
            if (!this.this$0.$listMFSleepSession.isEmpty()) {
                ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = this.this$0;
                this.$thirdPartyDatabase.getGFitSleepDao().insertListGFitSleep(thirdPartyRepository$saveData$Anon2.this$0.convertListMFSleepSessionToListGFitSleep(thirdPartyRepository$saveData$Anon2.$listMFSleepSession));
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "listMFSleepSession.isNotEmpty");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$saveData$Anon2(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, qn7 qn7) {
        super(2, qn7);
        this.this$0 = thirdPartyRepository;
        this.$listGFitSample = list;
        this.$gFitActiveTime = gFitActiveTime;
        this.$listGFitHeartRate = list2;
        this.$listGFitWorkoutSession = list3;
        this.$listMFSleepSession = list4;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = new ThirdPartyRepository$saveData$Anon2(this.this$0, this.$listGFitSample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, qn7);
        thirdPartyRepository$saveData$Anon2.p$ = (iv7) obj;
        return thirdPartyRepository$saveData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ThirdPartyRepository$saveData$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object F;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            F = bn5.F(this);
            if (F == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            F = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ThirdPartyDatabase thirdPartyDatabase = (ThirdPartyDatabase) F;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "saveData = listGFitSample=" + this.$listGFitSample + ", gFitActiveTime=" + this.$gFitActiveTime + ", listGFitHeartRate=" + this.$listGFitHeartRate + ", listGFitWorkoutSession=" + this.$listGFitWorkoutSession + ", listMFSleepSession= " + this.$listMFSleepSession);
        thirdPartyDatabase.runInTransaction(new Anon1_Level2(this, thirdPartyDatabase));
        return tl7.f3441a;
    }
}
