package com.portfolio.platform.data.source.local.sleep;

import com.fossil.fl5;
import com.fossil.pq7;
import com.fossil.xw7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource$loadInitial$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$0;
        fl5.d dVar = fl5.d.INITIAL;
        Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        pq7.b(aVar, "helperCallback");
        xw7 unused = sleepSummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
