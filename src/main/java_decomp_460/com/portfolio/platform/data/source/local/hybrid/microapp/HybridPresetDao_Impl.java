package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.b05;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDao_Impl implements HybridPresetDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ b05 __hybridAppSettingTypeConverter; // = new b05();
    @DexIgnore
    public /* final */ jw0<HybridPreset> __insertionAdapterOfHybridPreset;
    @DexIgnore
    public /* final */ jw0<HybridRecommendPreset> __insertionAdapterOfHybridRecommendPreset;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllPresetTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllRecommendPresetTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveAllDeletePinTypePreset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<HybridRecommendPreset> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, HybridRecommendPreset hybridRecommendPreset) {
            if (hybridRecommendPreset.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, hybridRecommendPreset.getId());
            }
            if (hybridRecommendPreset.getName() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, hybridRecommendPreset.getName());
            }
            if (hybridRecommendPreset.getSerialNumber() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, hybridRecommendPreset.getSerialNumber());
            }
            String a2 = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridRecommendPreset.getButtons());
            if (a2 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a2);
            }
            px0.bindLong(5, hybridRecommendPreset.isDefault() ? 1 : 0);
            if (hybridRecommendPreset.getCreatedAt() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, hybridRecommendPreset.getCreatedAt());
            }
            if (hybridRecommendPreset.getUpdatedAt() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, hybridRecommendPreset.getUpdatedAt());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridRecommendPreset` (`id`,`name`,`serialNumber`,`buttons`,`isDefault`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<HybridPreset> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, HybridPreset hybridPreset) {
            px0.bindLong(1, (long) hybridPreset.getPinType());
            if (hybridPreset.getCreatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, hybridPreset.getCreatedAt());
            }
            if (hybridPreset.getUpdatedAt() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, hybridPreset.getUpdatedAt());
            }
            if (hybridPreset.getId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, hybridPreset.getId());
            }
            if (hybridPreset.getName() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, hybridPreset.getName());
            }
            if (hybridPreset.getSerialNumber() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, hybridPreset.getSerialNumber());
            }
            String a2 = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridPreset.getButtons());
            if (a2 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a2);
            }
            px0.bindLong(8, hybridPreset.isActive() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridPreset` (`pinType`,`createdAt`,`updatedAt`,`id`,`name`,`serialNumber`,`buttons`,`isActive`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM hybridPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM hybridRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends xw0 {
        @DexIgnore
        public Anon6(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends xw0 {
        @DexIgnore
        public Anon7(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon8(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HybridPreset> call() throws Exception {
            Cursor b = ex0.b(HybridPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "pinType");
                int c2 = dx0.c(b, "createdAt");
                int c3 = dx0.c(b, "updatedAt");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "name");
                int c6 = dx0.c(b, "serialNumber");
                int c7 = dx0.c(b, "buttons");
                int c8 = dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                    hybridPreset.setPinType(b.getInt(c));
                    hybridPreset.setCreatedAt(b.getString(c2));
                    hybridPreset.setUpdatedAt(b.getString(c3));
                    arrayList.add(hybridPreset);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public HybridPresetDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfHybridRecommendPreset = new Anon1(qw0);
        this.__insertionAdapterOfHybridPreset = new Anon2(qw0);
        this.__preparedStmtOfClearAllPresetTable = new Anon3(qw0);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon4(qw0);
        this.__preparedStmtOfClearAllRecommendPresetTable = new Anon5(qw0);
        this.__preparedStmtOfDeletePreset = new Anon6(qw0);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getActivePresetBySerial(String str) {
        HybridPreset hybridPreset = null;
        boolean z = true;
        tw0 f = tw0.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "createdAt");
            int c3 = dx0.c(b, "updatedAt");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "name");
            int c6 = dx0.c(b, "serialNumber");
            int c7 = dx0.c(b, "buttons");
            int c8 = dx0.c(b, "isActive");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                ArrayList<HybridPresetAppSetting> b2 = this.__hybridAppSettingTypeConverter.b(b.getString(c7));
                if (b.getInt(c8) == 0) {
                    z = false;
                }
                hybridPreset = new HybridPreset(string, string2, string3, b2, z);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
            }
            return hybridPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPendingPreset(String str) {
        tw0 f = tw0.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "createdAt");
            int c3 = dx0.c(b, "updatedAt");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "name");
            int c6 = dx0.c(b, "serialNumber");
            int c7 = dx0.c(b, "buttons");
            int c8 = dx0.c(b, "isActive");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPreset(String str) {
        tw0 f = tw0.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "createdAt");
            int c3 = dx0.c(b, "updatedAt");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "name");
            int c6 = dx0.c(b, "serialNumber");
            int c7 = dx0.c(b, "buttons");
            int c8 = dx0.c(b, "isActive");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"hybridPreset"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getPresetById(String str) {
        HybridPreset hybridPreset = null;
        boolean z = true;
        tw0 f = tw0.f("SELECT * FROM hybridPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "createdAt");
            int c3 = dx0.c(b, "updatedAt");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "name");
            int c6 = dx0.c(b, "serialNumber");
            int c7 = dx0.c(b, "buttons");
            int c8 = dx0.c(b, "isActive");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                ArrayList<HybridPresetAppSetting> b2 = this.__hybridAppSettingTypeConverter.b(b.getString(c7));
                if (b.getInt(c8) == 0) {
                    z = false;
                }
                hybridPreset = new HybridPreset(string, string2, string3, b2, z);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
            }
            return hybridPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridRecommendPreset> getRecommendPresetList(String str) {
        tw0 f = tw0.f("SELECT * FROM hybridRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "serialNumber");
            int c4 = dx0.c(b, "buttons");
            int c5 = dx0.c(b, "isDefault");
            int c6 = dx0.c(b, "createdAt");
            int c7 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new HybridRecommendPreset(b.getString(c), b.getString(c2), b.getString(c3), this.__hybridAppSettingTypeConverter.b(b.getString(c4)), b.getInt(c5) != 0, b.getString(c6), b.getString(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPreset(HybridPreset hybridPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert((jw0<HybridPreset>) hybridPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPresetList(List<HybridPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertRecommendPresetList(List<HybridRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
