package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRingRemoteDataSource_Factory implements Factory<DianaRingRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public DianaRingRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static DianaRingRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new DianaRingRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static DianaRingRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new DianaRingRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaRingRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
