package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.p05;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceDao_Impl implements WatchFaceDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WatchFace> __insertionAdapterOfWatchFace;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllBackgroundWatchface;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteWatchFace;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteWatchFacesWithSerial;
    @DexIgnore
    public /* final */ p05 __watchFaceTypeConverter; // = new p05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WatchFace> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WatchFace watchFace) {
            if (watchFace.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, watchFace.getId());
            }
            if (watchFace.getName() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, watchFace.getName());
            }
            String d = WatchFaceDao_Impl.this.__watchFaceTypeConverter.d(watchFace.getRingStyleItems());
            if (d == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, d);
            }
            String a2 = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getBackground());
            if (a2 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a2);
            }
            if (watchFace.getPreviewUrl() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, watchFace.getPreviewUrl());
            }
            if (watchFace.getSerial() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, watchFace.getSerial());
            }
            px0.bindLong(7, (long) watchFace.getWatchFaceType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watch_face` (`id`,`name`,`ringStyleItems`,`background`,`previewUrl`,`serial`,`watchFaceType`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watch_face WHERE serial = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watch_face WHERE id = ? and watchFaceType = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watch_face WHERE watchFaceType = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watch_face";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon6(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor b = ex0.b(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "name");
                int c3 = dx0.c(b, "ringStyleItems");
                int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
                int c5 = dx0.c(b, "previewUrl");
                int c6 = dx0.c(b, "serial");
                int c7 = dx0.c(b, "watchFaceType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchFace(b.getString(c), b.getString(c2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.c(b.getString(c3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon7(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor b = ex0.b(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "name");
                int c3 = dx0.c(b, "ringStyleItems");
                int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
                int c5 = dx0.c(b, "previewUrl");
                int c6 = dx0.c(b, "serial");
                int c7 = dx0.c(b, "watchFaceType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchFace(b.getString(c), b.getString(c2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.c(b.getString(c3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WatchFaceDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWatchFace = new Anon1(qw0);
        this.__preparedStmtOfDeleteWatchFacesWithSerial = new Anon2(qw0);
        this.__preparedStmtOfDeleteWatchFace = new Anon3(qw0);
        this.__preparedStmtOfDeleteAllBackgroundWatchface = new Anon4(qw0);
        this.__preparedStmtOfDeleteAll = new Anon5(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAllBackgroundWatchface() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllBackgroundWatchface.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllBackgroundWatchface.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFace(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteWatchFace.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        acquire.bindLong(2, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFace.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFacesWithSerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteWatchFacesWithSerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFacesWithSerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getAllWatchFaces() {
        tw0 f = tw0.f("SELECT * FROM watch_face", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "ringStyleItems");
            int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = dx0.c(b, "previewUrl");
            int c6 = dx0.c(b, "serial");
            int c7 = dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public String getLatestWatchFaceName(int i) {
        String str = null;
        tw0 f = tw0.f("SELECT name FROM watch_face WHERE watchFaceType = ? ORDER BY id DESC LIMIT 1", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str = b.getString(0);
            }
            return str;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public WatchFace getWatchFaceWithId(String str) {
        WatchFace watchFace = null;
        tw0 f = tw0.f("SELECT * FROM watch_face WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "ringStyleItems");
            int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = dx0.c(b, "previewUrl");
            int c6 = dx0.c(b, "serial");
            int c7 = dx0.c(b, "watchFaceType");
            if (b.moveToFirst()) {
                watchFace = new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7));
            }
            return watchFace;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM watch_face WHERE serial = ? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon6 anon6 = new Anon6(f);
        return invalidationTracker.d(new String[]{"watch_face"}, false, anon6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, int i) {
        tw0 f = tw0.f("SELECT * FROM watch_face WHERE serial = ? and watchFaceType = ? ", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        f.bindLong(2, (long) i);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon7 anon7 = new Anon7(f);
        return invalidationTracker.d(new String[]{"watch_face"}, false, anon7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithSerial(String str) {
        tw0 f = tw0.f("SELECT * FROM watch_face WHERE serial = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "ringStyleItems");
            int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = dx0.c(b, "previewUrl");
            int c6 = dx0.c(b, "serial");
            int c7 = dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithType(int i) {
        tw0 f = tw0.f("SELECT * FROM watch_face WHERE watchFaceType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "name");
            int c3 = dx0.c(b, "ringStyleItems");
            int c4 = dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = dx0.c(b, "previewUrl");
            int c6 = dx0.c(b, "serial");
            int c7 = dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertAllWatchFaces(List<WatchFace> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertWatchFace(WatchFace watchFace) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert((jw0<WatchFace>) watchFace);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
