package com.portfolio.platform.data.source;

import com.fossil.ao7;
import com.fossil.iq5;
import com.fossil.kq5;
import com.fossil.qn7;
import com.fossil.tl7;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSetting$suspendImpl(UserDataSource userDataSource, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSettingFromServer$suspendImpl(UserDataSource userDataSource, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, qn7 qn7) {
        return new kq5(ao7.e(200), false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ Object sendUserSettingToServer$suspendImpl(UserDataSource userDataSource, UserSettings userSettings, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, qn7 qn7) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, qn7<? super iq5<Boolean>> qn7) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, qn7);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, qn7<? super iq5<Boolean>> qn7) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, qn7);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, qn7<? super Integer> qn7) {
        return ao7.e(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public Object getUserSetting(qn7<? super UserSettings> qn7) {
        return getUserSetting$suspendImpl(this, qn7);
    }

    @DexIgnore
    public Object getUserSettingFromServer(qn7<? super iq5<UserSettings>> qn7) {
        return getUserSettingFromServer$suspendImpl(this, qn7);
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object insertUserSetting(UserSettings userSettings, qn7<? super tl7> qn7) {
        return tl7.f3441a;
    }

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, qn7<? super iq5<MFUser>> qn7) {
        return loadUserInfo$suspendImpl(this, mFUser, qn7);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, qn7<? super iq5<Auth>> qn7) {
        return loginEmail$suspendImpl(this, str, str2, qn7);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, qn7<? super iq5<Auth>> qn7) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, qn7);
    }

    @DexIgnore
    public Object logoutUser(qn7<? super Integer> qn7) {
        return ao7.e(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, qn7<? super iq5<Void>> qn7) {
        return requestEmailOtp$suspendImpl(this, str, qn7);
    }

    @DexIgnore
    public Object resetPassword(String str, qn7<? super iq5<Integer>> qn7) {
        return resetPassword$suspendImpl(this, str, qn7);
    }

    @DexIgnore
    public Object sendUserSettingToServer(UserSettings userSettings, qn7<? super iq5<UserSettings>> qn7) {
        return sendUserSettingToServer$suspendImpl(this, userSettings, qn7);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, qn7<? super iq5<Auth>> qn7) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, qn7);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, qn7<? super iq5<Auth>> qn7) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, qn7);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, qn7<? super iq5<MFUser>> qn7);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, qn7<? super iq5<Void>> qn7) {
        return verifyEmailOtp$suspendImpl(this, str, str2, qn7);
    }
}
