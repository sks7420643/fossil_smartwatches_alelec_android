package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$repoResponse$1", f = "AlarmsRemoteDataSource.kt", l = {85}, m = "invokeSuspend")
public final class AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 extends ko7 implements rp7<qn7<? super q88<ApiResponse<gj4>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ gj4 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1(AlarmsRemoteDataSource alarmsRemoteDataSource, gj4 gj4, qn7 qn7) {
        super(1, qn7);
        this.this$0 = alarmsRemoteDataSource;
        this.$jsonObject = gj4;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1(this.this$0, this.$jsonObject, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<gj4>>> qn7) {
        return ((AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            gj4 gj4 = this.$jsonObject;
            this.label = 1;
            Object deleteAlarms = apiServiceV2.deleteAlarms(gj4, this);
            return deleteAlarms == d ? d : deleteAlarms;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
