package com.portfolio.platform.data.source.local.dnd;

import com.fossil.qw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DNDSettingsDatabase extends qw0 {
    @DexIgnore
    public abstract DNDScheduledTimeDao getDNDScheduledTimeDao();
}
