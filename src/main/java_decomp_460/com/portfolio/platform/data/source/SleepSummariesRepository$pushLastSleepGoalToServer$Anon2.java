package com.portfolio.platform.data.source;

import com.fossil.ao7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
public final class SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<MFSleepSettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $sleepGoal;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2$1", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements rp7<qn7<? super q88<MFSleepSettings>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new Anon1_Level2(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<MFSleepSettings>> qn7) {
            return ((Anon1_Level2) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiService;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object sleepSetting = apiServiceV2.setSleepSetting(gj4, this);
                return sleepSetting == d ? d : sleepSetting;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(SleepSummariesRepository sleepSummariesRepository, int i, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSummariesRepository;
        this.$sleepGoal = i;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2 = new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this.this$0, this.$sleepGoal, qn7);
        sleepSummariesRepository$pushLastSleepGoalToServer$Anon2.p$ = (iv7) obj;
        return sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<MFSleepSettings>> qn7) {
        return ((SleepSummariesRepository$pushLastSleepGoalToServer$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = SleepSummariesRepository.TAG;
            local.d(str, "pushLastSleepGoalToServer sleepGoal=" + this.$sleepGoal);
            gj4 gj4 = new gj4();
            try {
                gj4.m("currentGoalMinutes", ao7.e(this.$sleepGoal));
                TimeZone timeZone = TimeZone.getDefault();
                pq7.b(timeZone, "TimeZone.getDefault()");
                gj4.m("timezoneOffset", ao7.e(timeZone.getRawOffset() / 1000));
            } catch (Exception e) {
            }
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, gj4, null);
            this.L$0 = iv7;
            this.L$1 = gj4;
            this.label = 1;
            Object d2 = jq5.d(anon1_Level2, this);
            return d2 == d ? d : d2;
        } else if (i == 1) {
            gj4 gj42 = (gj4) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
