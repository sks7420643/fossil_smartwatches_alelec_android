package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.fl5;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1", f = "WorkoutSessionLocalDataSource.kt", l = {80, 83, 88}, m = "invokeSuspend")
public final class WorkoutSessionLocalDataSource$loadData$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ fl5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$1", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, qn7 qn7) {
            super(2, qn7);
            this.this$0 = workoutSessionLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.$helperCallback.b();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$2", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ iq5 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, iq5 iq5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = workoutSessionLocalDataSource$loadData$Anon1;
            this.$data = iq5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (((hq5) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((hq5) this.$data).d());
                } else if (((hq5) this.$data).c() != null) {
                    ServerError c = ((hq5) this.$data).c();
                    fl5.b.a aVar = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionLocalDataSource$loadData$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, int i, fl5.b.a aVar, qn7 qn7) {
        super(2, qn7);
        this.this$0 = workoutSessionLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1 = new WorkoutSessionLocalDataSource$loadData$Anon1(this.this$0, this.$offset, this.$helperCallback, qn7);
        workoutSessionLocalDataSource$loadData$Anon1.p$ = (iv7) obj;
        return workoutSessionLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((WorkoutSessionLocalDataSource$loadData$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00e0  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
