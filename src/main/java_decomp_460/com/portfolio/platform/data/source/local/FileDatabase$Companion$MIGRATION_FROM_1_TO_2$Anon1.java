package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.util.Log;
import com.fossil.ax0;
import com.fossil.lx0;
import com.fossil.pq7;
import com.fossil.vt7;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends ax0 {
    @DexIgnore
    public FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.ax0
    public void migrate(lx0 lx0) {
        pq7.c(lx0, "database");
        Log.d(FileDatabase.TAG, "MIGRATION_FROM_1_TO_2 - START");
        lx0.beginTransaction();
        try {
            String directory = FileUtils.getDirectory(PortfolioApp.h0.c().getApplicationContext(), FileType.WATCH_FACE);
            File file = new File(directory);
            if (!file.exists()) {
                file.mkdirs();
            }
            Cursor query = lx0.query("SELECT * FROM localfile");
            query.moveToFirst();
            while (true) {
                pq7.b(query, "cursor");
                if (query.isAfterLast()) {
                    break;
                }
                String string = query.getString(query.getColumnIndex("fileName"));
                String string2 = query.getString(query.getColumnIndex("localUri"));
                String string3 = query.getString(query.getColumnIndex("remoteUrl"));
                if (!(string2 == null || vt7.l(string2))) {
                    String str = directory + File.separator + string;
                    File file2 = new File(string2);
                    if (file2.exists()) {
                        file2.renameTo(new File(str));
                    }
                    lx0.execSQL("UPDATE `localfile` SET  `localUri` = '" + str + "' WHERE `remoteUrl` = '" + string3 + '\'');
                }
                query.moveToNext();
            }
            query.close();
            lx0.execSQL("ALTER TABLE `localfile` ADD COLUMN `type` TEXT NOT NULL DEFAULT " + FileType.WATCH_FACE);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(FileDatabase.TAG, "MIGRATION_FROM_1_TO_2 - ERROR: " + e.getMessage());
        } catch (Throwable th) {
            lx0.setTransactionSuccessful();
            lx0.endTransaction();
            Log.d(FileDatabase.TAG, "MIGRATION_FROM_1_TO_2 - END");
            throw th;
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
        Log.d(FileDatabase.TAG, "MIGRATION_FROM_1_TO_2 - END");
    }
}
