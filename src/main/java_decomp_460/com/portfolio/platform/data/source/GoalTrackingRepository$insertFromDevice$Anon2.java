package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$insertFromDevice$2", f = "GoalTrackingRepository.kt", l = {440, 442}, m = "invokeSuspend")
public final class GoalTrackingRepository$insertFromDevice$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $goalTrackingDataList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$insertFromDevice$Anon2(GoalTrackingRepository goalTrackingRepository, List list, qn7 qn7) {
        super(2, qn7);
        this.this$0 = goalTrackingRepository;
        this.$goalTrackingDataList = list;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$insertFromDevice$Anon2 goalTrackingRepository$insertFromDevice$Anon2 = new GoalTrackingRepository$insertFromDevice$Anon2(this.this$0, this.$goalTrackingDataList, qn7);
        goalTrackingRepository$insertFromDevice$Anon2.p$ = (iv7) obj;
        return goalTrackingRepository$insertFromDevice$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((GoalTrackingRepository$insertFromDevice$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0054  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x0056
            if (r0 == r6) goto L_0x0020
            if (r0 != r7) goto L_0x0018
            java.lang.Object r0 = r8.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r9)
        L_0x0015:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0020:
            java.lang.Object r0 = r8.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0029:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            java.util.List r1 = r8.$goalTrackingDataList
            r0.addGoalTrackingRawDataList(r1)
            java.util.List r0 = r8.$goalTrackingDataList
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0048
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = r8.this$0
            com.fossil.on5 r0 = com.portfolio.platform.data.source.GoalTrackingRepository.access$getMSharedPreferencesManager$p(r0)
            r0.m1(r6)
        L_0x0048:
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = r8.this$0
            r8.L$0 = r2
            r8.label = r7
            java.lang.Object r0 = r0.pushPendingGoalTrackingDataList(r8)
            if (r0 != r3) goto L_0x0015
            r0 = r3
            goto L_0x0017
        L_0x0056:
            com.fossil.el7.b(r9)
            com.fossil.iv7 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insertFromDevice: goalTrackingDataList = "
            r4.append(r5)
            java.util.List r5 = r8.$goalTrackingDataList
            int r5 = r5.size()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = r1.A(r8)
            if (r1 != r3) goto L_0x008f
            r0 = r3
            goto L_0x0017
        L_0x008f:
            r2 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$insertFromDevice$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
