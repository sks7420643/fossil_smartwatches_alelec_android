package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$loginEmail$2", f = "UserRepository.kt", l = {145, 152}, m = "invokeSuspend")
public final class UserRepository$loginEmail$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser.Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $email;
    @DexIgnore
    public /* final */ /* synthetic */ String $password;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$loginEmail$Anon2(UserRepository userRepository, String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
        this.$email = str;
        this.$password = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$loginEmail$Anon2 userRepository$loginEmail$Anon2 = new UserRepository$loginEmail$Anon2(this.this$0, this.$email, this.$password, qn7);
        userRepository$loginEmail$Anon2.p$ = (iv7) obj;
        return userRepository$loginEmail$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return ((UserRepository$loginEmail$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
        // Method dump skipped, instructions count: 319
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository$loginEmail$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
