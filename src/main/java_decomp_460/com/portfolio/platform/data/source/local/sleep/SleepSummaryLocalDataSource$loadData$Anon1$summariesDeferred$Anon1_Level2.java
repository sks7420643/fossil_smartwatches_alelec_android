package com.portfolio.platform.data.source.local.sleep;

import com.fossil.cl7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$summariesDeferred$1", f = "SleepSummaryLocalDataSource.kt", l = {173}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super iq5<gj4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ cl7 $downloadingRange;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, cl7 cl7, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = cl7;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2 sleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2 = new SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2(this.this$0, this.$downloadingRange, qn7);
        sleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2.p$ = (iv7) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<gj4>> qn7) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            this.L$0 = this.p$;
            this.label = 1;
            Object fetchSleepSummaries = this.this$0.this$0.mSleepSummariesRepository.fetchSleepSummaries((Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), this);
            return fetchSleepSummaries == d ? d : fetchSleepSummaries;
        } else if (i == 1) {
            iv7 iv7 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
