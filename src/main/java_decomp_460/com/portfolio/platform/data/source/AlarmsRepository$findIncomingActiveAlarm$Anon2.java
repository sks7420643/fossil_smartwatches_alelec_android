package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$findIncomingActiveAlarm$2", f = "AlarmsRepository.kt", l = {42}, m = "invokeSuspend")
public final class AlarmsRepository$findIncomingActiveAlarm$Anon2 extends ko7 implements vp7<iv7, qn7<? super Alarm>, Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    public AlarmsRepository$findIncomingActiveAlarm$Anon2(qn7 qn7) {
        super(2, qn7);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$findIncomingActiveAlarm$Anon2 alarmsRepository$findIncomingActiveAlarm$Anon2 = new AlarmsRepository$findIncomingActiveAlarm$Anon2(qn7);
        alarmsRepository$findIncomingActiveAlarm$Anon2.p$ = (iv7) obj;
        return alarmsRepository$findIncomingActiveAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Alarm> qn7) {
        return ((AlarmsRepository$findIncomingActiveAlarm$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0058, code lost:
        if ((r6.length == 0) != false) goto L_0x005a;
     */
    @DexIgnore
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 249
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$findIncomingActiveAlarm$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
