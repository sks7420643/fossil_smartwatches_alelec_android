package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WorkoutSettingRepository";
    @DexIgnore
    public /* final */ WorkoutSettingRemoteDataSource mWorkoutSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public WorkoutSettingRepository(WorkoutSettingRemoteDataSource workoutSettingRemoteDataSource) {
        pq7.c(workoutSettingRemoteDataSource, "mWorkoutSettingRemoteDataSource");
        this.mWorkoutSettingRemoteDataSource = workoutSettingRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new WorkoutSettingRepository$cleanUp$Anon2(null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final Object downloadWorkoutSettings(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new WorkoutSettingRepository$downloadWorkoutSettings$Anon2(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object executePendingRequest(com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.executePendingRequest(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object getWorkoutSettingList(qn7<? super List<WorkoutSetting>> qn7) {
        return eu7.g(bw7.b(), new WorkoutSettingRepository$getWorkoutSettingList$Anon2(null), qn7);
    }

    @DexIgnore
    public final Object getWorkoutSettingListAsLiveData(qn7<? super LiveData<List<WorkoutSetting>>> qn7) {
        return eu7.g(bw7.c(), new WorkoutSettingRepository$getWorkoutSettingListAsLiveData$Anon2(null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertWorkoutSettingList(java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting> r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.upsertWorkoutSettingList(java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
