package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$2$response$1", f = "SummariesRepository.kt", l = {388}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 extends ko7 implements rp7<qn7<? super q88<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$downloadRecommendedGoals$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2, qn7 qn7) {
        super(1, qn7);
        this.this$0 = summariesRepository$downloadRecommendedGoals$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ActivityRecommendedGoals>> qn7) {
        return ((SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiServiceV2;
            SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2 = this.this$0;
            int i2 = summariesRepository$downloadRecommendedGoals$Anon2.$age;
            int i3 = summariesRepository$downloadRecommendedGoals$Anon2.$weightInGrams;
            int i4 = summariesRepository$downloadRecommendedGoals$Anon2.$heightInCentimeters;
            String str = summariesRepository$downloadRecommendedGoals$Anon2.$gender;
            this.label = 1;
            Object recommendedGoalsRaw = apiServiceV2.getRecommendedGoalsRaw(i2, i3, i4, str, this);
            return recommendedGoalsRaw == d ? d : recommendedGoalsRaw;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
