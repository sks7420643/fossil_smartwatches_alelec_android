package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getAllDevice$2", f = "DeviceRemoteDataSource.kt", l = {39}, m = "invokeSuspend")
public final class DeviceRemoteDataSource$getAllDevice$Anon2 extends ko7 implements rp7<qn7<? super q88<ApiResponse<Device>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$getAllDevice$Anon2(DeviceRemoteDataSource deviceRemoteDataSource, qn7 qn7) {
        super(1, qn7);
        this.this$0 = deviceRemoteDataSource;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new DeviceRemoteDataSource$getAllDevice$Anon2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<Device>>> qn7) {
        return ((DeviceRemoteDataSource$getAllDevice$Anon2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            SecureApiService2Dot1 secureApiService2Dot1 = this.this$0.mSecureApiService2Dot1;
            this.label = 1;
            Object devices = secureApiService2Dot1.getDevices(this);
            return devices == d ? d : devices;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
