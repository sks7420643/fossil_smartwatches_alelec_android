package com.portfolio.platform.data.source.local.inapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDao_Impl extends InAppNotificationDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<InAppNotification> __deletionAdapterOfInAppNotification;
    @DexIgnore
    public /* final */ jw0<InAppNotification> __insertionAdapterOfInAppNotification;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<InAppNotification> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, inAppNotification.getId());
            }
            if (inAppNotification.getTitle() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, inAppNotification.getTitle());
            }
            if (inAppNotification.getContent() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, inAppNotification.getContent());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inAppNotification` (`id`,`title`,`content`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<InAppNotification> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, inAppNotification.getId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `inAppNotification` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InAppNotification>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<InAppNotification> call() throws Exception {
            Cursor b = ex0.b(InAppNotificationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "title");
                int c3 = dx0.c(b, "content");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new InAppNotification(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public InAppNotificationDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfInAppNotification = new Anon1(qw0);
        this.__deletionAdapterOfInAppNotification = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void delete(InAppNotification inAppNotification) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfInAppNotification.handle(inAppNotification);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public LiveData<List<InAppNotification>> getAllInAppNotification() {
        tw0 f = tw0.f("SELECT * FROM inAppNotification", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"inAppNotification"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void insertListInAppNotification(List<InAppNotification> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInAppNotification.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
