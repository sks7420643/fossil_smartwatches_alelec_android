package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2$1$1$saveCallResult$2", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalDailySummary $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, GoalDailySummary goalDailySummary, qn7 qn7) {
        super(2, qn7);
        this.this$0 = anon1_Level3;
        this.$item = goalDailySummary;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 = new GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(this.this$0, this.$item, qn7);
        goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4.p$ = (iv7) obj;
        return goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            GoalTrackingDao goalTrackingDao = this.this$0.this$0.$goalTrackingDb.getGoalTrackingDao();
            GoalTrackingSummary goalTrackingSummary = this.$item.toGoalTrackingSummary();
            if (goalTrackingSummary != null) {
                goalTrackingDao.upsertGoalTrackingSummary(goalTrackingSummary);
                return tl7.f3441a;
            }
            pq7.i();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
