package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory implements Factory<CustomizeRealDataDao> {
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CustomizeRealDataDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CustomizeRealDataDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CustomizeRealDataDao provideCustomizeRealDataDao(PortfolioDatabaseModule portfolioDatabaseModule, CustomizeRealDataDatabase customizeRealDataDatabase) {
        CustomizeRealDataDao provideCustomizeRealDataDao = portfolioDatabaseModule.provideCustomizeRealDataDao(customizeRealDataDatabase);
        lk7.c(provideCustomizeRealDataDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideCustomizeRealDataDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public CustomizeRealDataDao get() {
        return provideCustomizeRealDataDao(this.module, this.dbProvider.get());
    }
}
