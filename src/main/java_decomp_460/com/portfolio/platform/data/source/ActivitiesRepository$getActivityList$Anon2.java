package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ao7;
import com.fossil.br7;
import com.fossil.bw7;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.gi0;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$2", f = "ActivitiesRepository.kt", l = {62}, m = "invokeSuspend")
public final class ActivitiesRepository$getActivityList$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<ActivitySample>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<ActivitySample>, ApiResponse<Activity>> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, br7 br7, int i, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$offset = br7;
                this.$limit = i;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<ApiResponse<Activity>>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getActivities(k, k2, this.$offset.element, this.$limit, qn7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0038  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
            @Override // com.fossil.c47
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> r8) {
                /*
                    r7 = this;
                    r6 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x002a
                    r0 = r8
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x002a
                    int r1 = r1 + r3
                    r0.label = r1
                L_0x0013:
                    java.lang.Object r1 = r0.result
                    java.lang.Object r2 = com.fossil.yn7.d()
                    int r3 = r0.label
                    if (r3 == 0) goto L_0x0038
                    if (r3 != r6) goto L_0x0030
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.ActivitiesRepository.getActivityList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.el7.b(r1)
                    r0 = r1
                L_0x0027:
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                L_0x0029:
                    return r0
                L_0x002a:
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r7, r8)
                    goto L_0x0013
                L_0x0030:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0038:
                    com.fossil.el7.b(r1)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r1 = r7.this$0
                    java.util.Date r1 = r1.$endDate
                    java.lang.Boolean r1 = com.fossil.lk5.p0(r1)
                    boolean r1 = r1.booleanValue()
                    if (r1 != 0) goto L_0x0085
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.data.source.ActivitiesRepository$Companion r3 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
                    java.lang.String r3 = r3.getTAG$app_fossilRelease()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getActivityList loadFromDb isNotToday day = "
                    r4.append(r5)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r5 = r7.this$0
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$end
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r1.d(r3, r4)
                    com.fossil.dv7 r1 = com.fossil.bw7.b()
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 r3 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4
                    r4 = 0
                    r3.<init>(r7, r4)
                    r0.L$0 = r7
                    r0.label = r6
                    java.lang.Object r0 = com.fossil.eu7.g(r1, r3, r0)
                    if (r0 != r2) goto L_0x0027
                    r0 = r2
                    goto L_0x0029
                L_0x0085:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.data.source.ActivitiesRepository$Companion r1 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
                    java.lang.String r1 = r1.getTAG$app_fossilRelease()
                    java.lang.String r2 = "getActivityList loadFromDb: isToday"
                    r0.d(r1, r2)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r0 = r7.this$0
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = r0.$fitnessDatabase
                    com.portfolio.platform.data.source.local.fitness.ActivitySampleDao r0 = r0.activitySampleDao()
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r1 = r7.this$0
                    java.util.Date r1 = r1.$startDate
                    java.lang.String r2 = "startDate"
                    com.fossil.pq7.b(r1, r2)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r2 = r7.this$0
                    java.util.Date r2 = r2.$endDate
                    java.util.Date r2 = com.fossil.lk5.P(r2)
                    java.lang.String r3 = "DateHelper.getPrevDate(endDate)"
                    com.fossil.pq7.b(r2, r3)
                    androidx.lifecycle.LiveData r0 = r0.getActivitySamplesLiveData(r1, r2)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4 r1 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4
                    r1.<init>(r7)
                    androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r0, r1)
                    java.lang.String r1 = "Transformations.switchMa\u2026                        }"
                    com.fossil.pq7.b(r0, r1)
                    goto L_0x0029
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.getActivityList.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<Activity> apiResponse, qn7<? super Boolean> qn7) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<Activity> apiResponse, qn7 qn7) {
                return processContinueFetching(apiResponse, (qn7<? super Boolean>) qn7);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<Activity> apiResponse, qn7<? super tl7> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getActivityList saveCallResult onResponse: response = ");
                sb.append(apiResponse.get_items().size());
                sb.append(" hasNext=");
                Range range = apiResponse.get_range();
                sb.append(range != null ? ao7.a(range.isHasNext()) : null);
                local.d(tAG$app_fossilRelease, sb.toString());
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = apiResponse.get_items().iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next().toActivitySample());
                }
                Object g = eu7.g(bw7.b(), new ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4(this, arrayList, null), qn7);
                return g == yn7.d() ? g : tl7.f3441a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<Activity> apiResponse, qn7 qn7) {
                return saveCallResult(apiResponse, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<ActivitySample> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(ActivitiesRepository$getActivityList$Anon2 activitiesRepository$getActivityList$Anon2, Date date, Date date2, FitnessDatabase fitnessDatabase) {
            this.this$0 = activitiesRepository$getActivityList$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<h47<List<ActivitySample>>> apply(List<FitnessDataWrapper> list) {
            br7 br7 = new br7();
            br7.element = 0;
            pq7.b(list, "fitnessDataList");
            Date date = this.$startDate;
            pq7.b(date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            pq7.b(date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, br7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getActivityList$Anon2(ActivitiesRepository activitiesRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$getActivityList$Anon2 activitiesRepository$getActivityList$Anon2 = new ActivitiesRepository$getActivityList$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, qn7);
        activitiesRepository$getActivityList$Anon2.p$ = (iv7) obj;
        return activitiesRepository$getActivityList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<ActivitySample>>>> qn7) {
        return ((ActivitiesRepository$getActivityList$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Date V;
        Object g;
        Date date;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getActivityList: start = " + this.$start + ", end = " + this.$end + " shouldFetch=" + this.$shouldFetch);
            V = lk5.V(this.$start);
            Date E = lk5.E(this.$end);
            dv7 b = bw7.b();
            ActivitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2 activitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2 = new ActivitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.L$1 = V;
            this.L$2 = E;
            this.label = 1;
            g = eu7.g(b, activitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
            date = E;
        } else if (i == 1) {
            V = (Date) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            date = (Date) this.L$2;
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        pq7.b(V, GoalPhase.COLUMN_START_DATE);
        pq7.b(date, GoalPhase.COLUMN_END_DATE);
        return ss0.c(fitnessDataDao.getFitnessDataLiveData(V, date), new Anon1_Level2(this, V, date, fitnessDatabase));
    }
}
