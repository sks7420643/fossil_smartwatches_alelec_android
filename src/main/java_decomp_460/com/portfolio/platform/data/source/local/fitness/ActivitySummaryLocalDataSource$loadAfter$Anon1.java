package com.portfolio.platform.data.source.local.fitness;

import com.fossil.cl7;
import com.fossil.fl5;
import com.fossil.pm7;
import com.fossil.pq7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadAfter$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadAfter$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        cl7 cl7 = (cl7) pm7.F(this.this$0.mRequestAfterQueue);
        pq7.b(aVar, "helperCallback");
        this.this$0.loadData(fl5.d.AFTER, (Date) cl7.getFirst(), (Date) cl7.getSecond(), aVar);
    }
}
