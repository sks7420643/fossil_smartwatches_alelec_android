package com.portfolio.platform.data.source.remote;

import com.fossil.m98;
import com.fossil.y98;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WechatApiService {
    @DexIgnore
    @m98("oauth2/access_token")
    Call<WechatToken> getWechatToken(@y98("appid") String str, @y98("secret") String str2, @y98("code") String str3, @y98("grant_type") String str4);
}
