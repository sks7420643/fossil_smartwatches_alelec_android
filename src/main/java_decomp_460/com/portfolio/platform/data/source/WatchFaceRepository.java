package com.portfolio.platform.data.source;

import android.content.Context;
import android.graphics.Bitmap;
import com.fossil.pq7;
import com.fossil.ry5;
import com.fossil.z58;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ FileRepository fileRepository;
    @DexIgnore
    public /* final */ WatchFaceRemoteDataSource watchFaceRemoteDataSource;

    @DexIgnore
    public WatchFaceRepository(Context context2, WatchFaceRemoteDataSource watchFaceRemoteDataSource2, FileRepository fileRepository2) {
        pq7.c(context2, "context");
        pq7.c(watchFaceRemoteDataSource2, "watchFaceRemoteDataSource");
        pq7.c(fileRepository2, "fileRepository");
        this.context = context2;
        this.watchFaceRemoteDataSource = watchFaceRemoteDataSource2;
        this.fileRepository = fileRepository2;
        String simpleName = WatchFaceRepository.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchFaceRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.data.source.WatchFaceRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r5) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r0 = (com.portfolio.platform.data.source.WatchFaceRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            r0.deleteAll()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.WatchFaceRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$cleanUp$Anon1
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r6.TAG
            java.lang.String r4 = "cleanUp()"
            r1.d(r3, r4)
            com.portfolio.platform.data.source.FileRepository r1 = r6.fileRepository
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r1.deletedFilesByType(r3)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.cleanUp(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void deleteBackgroundPhoto(String str, String str2) {
        pq7.c(str, "imageName");
        pq7.c(str2, "binaryName");
        FLogger.INSTANCE.getLocal().d(this.TAG, "deleteBackgroundPhoto()");
        this.fileRepository.deleteFileByName(str, FileType.WATCH_FACE);
        this.fileRepository.deleteFileByName(str2, FileType.WATCH_FACE);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteWatchFace(java.lang.String r6, com.fossil.ci5 r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFace$Anon1
            if (r0 == 0) goto L_0x0042
            r0 = r8
            com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFace$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFace$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x0051
            if (r1 != r4) goto L_0x0049
            java.lang.Object r0 = r2.L$2
            com.fossil.ci5 r0 = (com.fossil.ci5) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r2 = (com.portfolio.platform.data.source.WatchFaceRepository) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r7 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            int r2 = r7.getValue()
            r0.deleteWatchFace(r1, r2)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFace$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFace$Anon1
            r0.<init>(r5, r8)
            r2 = r0
            goto L_0x0014
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.el7.b(r3)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r7
            r2.label = r4
            java.lang.Object r2 = r1.v(r2)
            if (r2 == r0) goto L_0x0041
            r1 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.deleteWatchFace(java.lang.String, com.fossil.ci5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteWatchFacesWithSerial(java.lang.String r6, com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFacesWithSerial$Anon1
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFacesWithSerial$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFacesWithSerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r1 = (com.portfolio.platform.data.source.WatchFaceRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            r0.deleteWatchFacesWithSerial(r6)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFacesWithSerial$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$deleteWatchFacesWithSerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.deleteWatchFacesWithSerial(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFaceWithId(java.lang.String r6, com.fossil.qn7<? super com.portfolio.platform.data.model.diana.preset.WatchFace> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFaceWithId$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFaceWithId$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFaceWithId$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r1 = (com.portfolio.platform.data.source.WatchFaceRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            com.portfolio.platform.data.model.diana.preset.WatchFace r0 = r0.getWatchFaceWithId(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFaceWithId$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFaceWithId$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFaceWithId(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x011c A[LOOP:2: B:31:0x0116->B:33:0x011c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01b9 A[LOOP:3: B:53:0x01b3->B:55:0x01b9, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesFromServer(java.lang.String r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 603
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesFromServer(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesLiveDataWithSerial(java.lang.String r6, com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithSerial$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithSerial$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithSerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r1 = (com.portfolio.platform.data.source.WatchFaceRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            androidx.lifecycle.LiveData r0 = r0.getWatchFacesLiveData(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithSerial$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithSerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesLiveDataWithSerial(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesLiveDataWithType(java.lang.String r6, com.fossil.ci5 r7, com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>>> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithType$Anon1
            if (r0 == 0) goto L_0x0041
            r0 = r8
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithType$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithType$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x0050
            if (r1 != r4) goto L_0x0048
            java.lang.Object r0 = r2.L$2
            com.fossil.ci5 r0 = (com.fossil.ci5) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r2 = (com.portfolio.platform.data.source.WatchFaceRepository) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r7 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            int r2 = r7.getValue()
            androidx.lifecycle.LiveData r0 = r0.getWatchFacesLiveDataWithType(r1, r2)
        L_0x0040:
            return r0
        L_0x0041:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithType$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesLiveDataWithType$Anon1
            r0.<init>(r5, r8)
            r2 = r0
            goto L_0x0014
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.el7.b(r3)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r7
            r2.label = r4
            java.lang.Object r2 = r1.v(r2)
            if (r2 == r0) goto L_0x0040
            r1 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesLiveDataWithType(java.lang.String, com.fossil.ci5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesWithSerial(java.lang.String r6, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithSerial$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithSerial$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithSerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r1 = (com.portfolio.platform.data.source.WatchFaceRepository) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            java.util.List r0 = r0.getWatchFacesWithSerial(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithSerial$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithSerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesWithSerial(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesWithType(java.lang.String r6, com.fossil.ci5 r7, com.fossil.qn7<? super java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithType$Anon1
            if (r0 == 0) goto L_0x0041
            r0 = r8
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithType$Anon1 r0 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithType$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x0050
            if (r1 != r4) goto L_0x0048
            java.lang.Object r0 = r2.L$2
            com.fossil.ci5 r0 = (com.fossil.ci5) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r2.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r1 = (com.portfolio.platform.data.source.WatchFaceRepository) r1
            com.fossil.el7.b(r3)
            r7 = r0
            r1 = r3
        L_0x0031:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r0 = r0.getWatchFaceDao()
            int r1 = r7.getValue()
            java.util.List r0 = r0.getWatchFacesWithType(r1)
        L_0x0040:
            return r0
        L_0x0041:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithType$Anon1 r0 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesWithType$Anon1
            r0.<init>(r5, r8)
            r2 = r0
            goto L_0x0014
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.el7.b(r3)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r7
            r2.label = r4
            java.lang.Object r1 = r1.v(r2)
            if (r1 != r0) goto L_0x0031
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesWithType(java.lang.String, com.fossil.ci5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object insertWatchFace(java.lang.String r13, java.lang.String r14, java.lang.String r15, java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.RingStyleItem> r16, com.fossil.qn7<? super com.fossil.tl7> r17) {
        /*
        // Method dump skipped, instructions count: 236
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.insertWatchFace(java.lang.String, java.lang.String, java.lang.String, java.util.ArrayList, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void saveBackgroundPhoto(Context context2, String str, String str2, Bitmap bitmap, String str3, byte[] bArr) {
        pq7.c(context2, "context");
        pq7.c(str, "directory");
        pq7.c(str2, "fileName");
        pq7.c(bitmap, "bitmap");
        pq7.c(str3, "dataName");
        pq7.c(bArr, "data");
        FLogger.INSTANCE.getLocal().d(this.TAG, "saveBackgroundPhoto()");
        if (new File(str).exists()) {
            z58.d(new File(str + str3), bArr);
            ry5.H(context2, str, str2, bitmap);
        }
    }
}
