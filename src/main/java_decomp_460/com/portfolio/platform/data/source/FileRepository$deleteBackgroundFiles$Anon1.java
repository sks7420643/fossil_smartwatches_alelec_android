package com.portfolio.platform.data.source;

import com.fossil.pq7;
import com.fossil.vt7;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$deleteBackgroundFiles$Anon1 implements FileFilter {
    @DexIgnore
    public static /* final */ FileRepository$deleteBackgroundFiles$Anon1 INSTANCE; // = new FileRepository$deleteBackgroundFiles$Anon1();

    @DexIgnore
    public final boolean accept(File file) {
        pq7.b(file, "it");
        String name = file.getName();
        pq7.b(name, "it.name");
        if (!vt7.h(name, Constants.PHOTO_IMAGE_NAME_SUFFIX, true)) {
            String name2 = file.getName();
            pq7.b(name2, "it.name");
            if (!vt7.h(name2, Constants.PHOTO_BINARY_NAME_SUFFIX, true)) {
                return true;
            }
        }
        return false;
    }
}
