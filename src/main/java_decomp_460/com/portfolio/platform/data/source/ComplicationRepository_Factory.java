package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository_Factory implements Factory<ComplicationRepository> {
    @DexIgnore
    public /* final */ Provider<ComplicationRemoteDataSource> mComplicationRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;

    @DexIgnore
    public ComplicationRepository_Factory(Provider<ComplicationRemoteDataSource> provider, Provider<PortfolioApp> provider2) {
        this.mComplicationRemoteDataSourceProvider = provider;
        this.mPortfolioAppProvider = provider2;
    }

    @DexIgnore
    public static ComplicationRepository_Factory create(Provider<ComplicationRemoteDataSource> provider, Provider<PortfolioApp> provider2) {
        return new ComplicationRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static ComplicationRepository newInstance(ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        return new ComplicationRepository(complicationRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ComplicationRepository get() {
        return newInstance(this.mComplicationRemoteDataSourceProvider.get(), this.mPortfolioAppProvider.get());
    }
}
