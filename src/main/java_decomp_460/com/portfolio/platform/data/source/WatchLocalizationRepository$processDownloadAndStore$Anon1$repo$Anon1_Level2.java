package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.w18;
import com.fossil.yn7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1$repo$1", f = "WatchLocalizationRepository.kt", l = {65}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 extends ko7 implements rp7<qn7<? super q88<w18>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository$processDownloadAndStore$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1, qn7 qn7) {
        super(1, qn7);
        this.this$0 = watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<w18>> qn7) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.api;
            String str = this.this$0.$url;
            this.label = 1;
            Object downloadFile = apiServiceV2.downloadFile(str, this);
            return downloadFile == d ? d : downloadFile;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
