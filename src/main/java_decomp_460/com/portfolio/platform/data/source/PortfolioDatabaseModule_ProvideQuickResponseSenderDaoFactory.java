package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory implements Factory<QuickResponseSenderDao> {
    @DexIgnore
    public /* final */ Provider<QuickResponseDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<QuickResponseDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<QuickResponseDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static QuickResponseSenderDao provideQuickResponseSenderDao(PortfolioDatabaseModule portfolioDatabaseModule, QuickResponseDatabase quickResponseDatabase) {
        QuickResponseSenderDao provideQuickResponseSenderDao = portfolioDatabaseModule.provideQuickResponseSenderDao(quickResponseDatabase);
        lk7.c(provideQuickResponseSenderDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideQuickResponseSenderDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public QuickResponseSenderDao get() {
        return provideQuickResponseSenderDao(this.module, this.dbProvider.get());
    }
}
