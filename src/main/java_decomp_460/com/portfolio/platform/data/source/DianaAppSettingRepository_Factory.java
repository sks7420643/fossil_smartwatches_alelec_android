package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingRepository_Factory implements Factory<DianaAppSettingRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;

    @DexIgnore
    public DianaAppSettingRepository_Factory(Provider<ApiServiceV2> provider) {
        this.apiProvider = provider;
    }

    @DexIgnore
    public static DianaAppSettingRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new DianaAppSettingRepository_Factory(provider);
    }

    @DexIgnore
    public static DianaAppSettingRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new DianaAppSettingRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaAppSettingRepository get() {
        return newInstance(this.apiProvider.get());
    }
}
