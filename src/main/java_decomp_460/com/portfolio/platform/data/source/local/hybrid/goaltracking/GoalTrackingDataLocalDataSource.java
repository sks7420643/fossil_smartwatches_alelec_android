package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.bw7;
import com.fossil.fl5;
import com.fossil.gl5;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.nw0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.xw7;
import com.fossil.yt0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource extends yt0<Long, GoalTrackingData> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public DateTime latestTrackedTime;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public /* final */ Date mCurrentDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public fl5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ nw0.c mObserver; // = new Anon1(this, "goalTrackingRaw", new String[0]);
    @DexIgnore
    public int mOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends nw0.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingDataLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            pq7.c(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingDataLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = GoalTrackingDataLocalDataSource.class.getSimpleName();
        pq7.b(simpleName, "GoalTrackingDataLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingDataLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, no4 no4, fl5.a aVar) {
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        pq7.c(date, "mCurrentDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCurrentDate = date;
        this.listener = aVar;
        fl5 fl5 = new fl5(no4.a());
        this.mHelper = fl5;
        this.mNetworkState = gl5.b(fl5);
        this.mHelper.a(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<GoalTrackingData> getDataInDatabase(int i) {
        return this.mGoalTrackingDatabase.getGoalTrackingDao().getGoalTrackingDataListInitInDate(this.mCurrentDate, i);
    }

    @DexIgnore
    private final xw7 loadData(fl5.d dVar, fl5.b.a aVar, int i) {
        return gu7.d(jv7.a(bw7.b()), null, null, new GoalTrackingDataLocalDataSource$loadData$Anon1(this, i, aVar, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ xw7 loadData$default(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, fl5.d dVar, fl5.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return goalTrackingDataLocalDataSource.loadData(dVar, aVar, i);
    }

    @DexIgnore
    public Long getKey(GoalTrackingData goalTrackingData) {
        pq7.c(goalTrackingData, "item");
        return Long.valueOf(goalTrackingData.getUpdatedAt());
    }

    @DexIgnore
    public final fl5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadAfter(yt0.f<Long> fVar, yt0.a<GoalTrackingData> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - currentDate = " + this.mCurrentDate);
        GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
        Date date = this.mCurrentDate;
        DateTime dateTime = this.latestTrackedTime;
        if (dateTime != null) {
            Key key = fVar.f4367a;
            pq7.b(key, "params.key");
            List<GoalTrackingData> goalTrackingDataListAfterInDate = goalTrackingDao.getGoalTrackingDataListAfterInDate(date, dateTime, key.longValue(), fVar.b);
            if (!goalTrackingDataListAfterInDate.isEmpty()) {
                this.latestTrackedTime = ((GoalTrackingData) pm7.P(goalTrackingDataListAfterInDate)).getTrackedAt();
            }
            aVar.a(goalTrackingDataListAfterInDate);
            this.mHelper.h(fl5.d.AFTER, new GoalTrackingDataLocalDataSource$loadAfter$Anon1(this));
            return;
        }
        pq7.n("latestTrackedTime");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadBefore(yt0.f<Long> fVar, yt0.a<GoalTrackingData> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadInitial(yt0.e<Long> eVar, yt0.c<GoalTrackingData> cVar) {
        pq7.c(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - mCurrentDate = " + this.mCurrentDate + ' ');
        List<GoalTrackingData> dataInDatabase = getDataInDatabase(eVar.b);
        if (!dataInDatabase.isEmpty()) {
            this.latestTrackedTime = ((GoalTrackingData) pm7.P(dataInDatabase)).getTrackedAt();
        }
        cVar.a(dataInDatabase);
        this.mHelper.h(fl5.d.INITIAL, new GoalTrackingDataLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(fl5 fl5) {
        pq7.c(fl5, "<set-?>");
        this.mHelper = fl5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        pq7.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }
}
