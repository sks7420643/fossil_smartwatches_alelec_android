package com.portfolio.platform.data.source.local.quickresponse;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseDatabase_Impl extends QuickResponseDatabase {
    @DexIgnore
    public volatile QuickResponseMessageDao _quickResponseMessageDao;
    @DexIgnore
    public volatile QuickResponseSenderDao _quickResponseSenderDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `quickResponseMessage` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `response` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `quickResponseSender` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `content` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'e3a41f0570eccd0330bf7f76279c817f')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `quickResponseMessage`");
            lx0.execSQL("DROP TABLE IF EXISTS `quickResponseSender`");
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            QuickResponseDatabase_Impl.this.mDatabase = lx0;
            QuickResponseDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("response", new ix0.a("response", "TEXT", true, 0, null, 1));
            ix0 ix0 = new ix0("quickResponseMessage", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "quickResponseMessage");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "quickResponseMessage(com.portfolio.platform.data.model.QuickResponseMessage).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap2.put("content", new ix0.a("content", "TEXT", true, 0, null, 1));
            ix0 ix02 = new ix0("quickResponseSender", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "quickResponseSender");
            if (ix02.equals(a3)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "quickResponseSender(com.portfolio.platform.data.model.QuickResponseSender).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `quickResponseMessage`");
            writableDatabase.execSQL("DELETE FROM `quickResponseSender`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "quickResponseMessage", "quickResponseSender");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "e3a41f0570eccd0330bf7f76279c817f", "eb9908832be312d2d3b3e7c35c7e6720");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase
    public QuickResponseMessageDao quickResponseMessageDao() {
        QuickResponseMessageDao quickResponseMessageDao;
        if (this._quickResponseMessageDao != null) {
            return this._quickResponseMessageDao;
        }
        synchronized (this) {
            if (this._quickResponseMessageDao == null) {
                this._quickResponseMessageDao = new QuickResponseMessageDao_Impl(this);
            }
            quickResponseMessageDao = this._quickResponseMessageDao;
        }
        return quickResponseMessageDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase
    public QuickResponseSenderDao quickResponseSenderDao() {
        QuickResponseSenderDao quickResponseSenderDao;
        if (this._quickResponseSenderDao != null) {
            return this._quickResponseSenderDao;
        }
        synchronized (this) {
            if (this._quickResponseSenderDao == null) {
                this._quickResponseSenderDao = new QuickResponseSenderDao_Impl(this);
            }
            quickResponseSenderDao = this._quickResponseSenderDao;
        }
        return quickResponseSenderDao;
    }
}
