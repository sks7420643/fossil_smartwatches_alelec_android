package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bw7;
import com.fossil.cu0;
import com.fossil.dv7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.eu7;
import com.fossil.fl5;
import com.fossil.gi0;
import com.fossil.gp7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qq7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.fossil.zt0;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionsPaging$2", f = "WorkoutSessionRepository.kt", l = {127}, m = "invokeSuspend")
public final class WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2 extends ko7 implements vp7<iv7, qn7<? super Listing<WorkoutSession>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ no4 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $currentDate;
    @DexIgnore
    public /* final */ /* synthetic */ fl5.a $listener;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
            return workoutSessionLocalDataSource.getMNetworkState();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
            super(0);
            this.$sourceFactory = workoutSessionDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            WorkoutSessionLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
            super(0);
            this.$sourceFactory = workoutSessionDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            fl5 mHelper;
            WorkoutSessionLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(WorkoutSessionRepository workoutSessionRepository, WorkoutSessionRepository workoutSessionRepository2, Date date, no4 no4, fl5.a aVar, qn7 qn7) {
        super(2, qn7);
        this.this$0 = workoutSessionRepository;
        this.$workoutSessionRepository = workoutSessionRepository2;
        this.$currentDate = date;
        this.$appExecutors = no4;
        this.$listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2 workoutSessionRepository$getWorkoutSessionsPaging$Anon2 = new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(this.this$0, this.$workoutSessionRepository, this.$currentDate, this.$appExecutors, this.$listener, qn7);
        workoutSessionRepository$getWorkoutSessionsPaging$Anon2.p$ = (iv7) obj;
        return workoutSessionRepository$getWorkoutSessionsPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Listing<WorkoutSession>> qn7) {
        return ((WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            dv7 b = bw7.b();
            WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2 workoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2 = new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = iv7;
            this.label = 1;
            g = eu7.g(b, workoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory = new WorkoutSessionDataSourceFactory(this.$workoutSessionRepository, (FitnessDatabase) g, this.$currentDate, this.$appExecutors, this.$listener);
        this.this$0.mSourceDataFactoryList.add(workoutSessionDataSourceFactory);
        cu0.f.a aVar = new cu0.f.a();
        aVar.c(100);
        aVar.b(false);
        aVar.d(100);
        aVar.e(5);
        cu0.f a2 = aVar.a();
        pq7.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new zt0(workoutSessionDataSourceFactory, a2).a();
        pq7.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = ss0.c(workoutSessionDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        pq7.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(workoutSessionDataSourceFactory), new Anon3_Level2(workoutSessionDataSourceFactory));
    }
}
