package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.o05;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDao_Impl implements UserSettingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<UserSettings> __insertionAdapterOfUserSettings;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ o05 __userSettingConverter; // = new o05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<UserSettings> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, UserSettings userSettings) {
            px0.bindLong(1, userSettings.isShowGoalRing() ? 1 : 0);
            String a2 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedLocationDataSharing());
            if (a2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, a2);
            }
            String a3 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedPrivacies());
            if (a3 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a3);
            }
            String a4 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedTermsOfService());
            if (a4 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a4);
            }
            if (userSettings.getUid() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, userSettings.getUid());
            }
            if (userSettings.getId() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, userSettings.getId());
            }
            px0.bindLong(7, userSettings.isLatestLocationDataSharingAccepted() ? 1 : 0);
            px0.bindLong(8, userSettings.isLatestPrivacyAccepted() ? 1 : 0);
            px0.bindLong(9, userSettings.isLatestTermsOfServiceAccepted() ? 1 : 0);
            if (userSettings.getLatestLocationDataSharingVersion() == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, userSettings.getLatestLocationDataSharingVersion());
            }
            if (userSettings.getLatestPrivacyVersion() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, userSettings.getLatestPrivacyVersion());
            }
            if (userSettings.getLatestTermsOfServiceVersion() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, userSettings.getLatestTermsOfServiceVersion());
            }
            if (userSettings.getStartDayOfWeek() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, userSettings.getStartDayOfWeek());
            }
            if (userSettings.getCreatedAt() == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, userSettings.getCreatedAt());
            }
            if (userSettings.getUpdatedAt() == null) {
                px0.bindNull(15);
            } else {
                px0.bindString(15, userSettings.getUpdatedAt());
            }
            px0.bindLong(16, (long) userSettings.getPinType());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `userSettings` (`isShowGoalRing`,`acceptedLocationDataSharing`,`acceptedPrivacies`,`acceptedTermsOfService`,`uid`,`id`,`isLatestLocationDataSharingAccepted`,`isLatestPrivacyAccepted`,`isLatestTermsOfServiceAccepted`,`latestLocationDataSharingVersion`,`latestPrivacyVersion`,`latestTermsOfServiceVersion`,`startDayOfWeek`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM userSettings";
        }
    }

    @DexIgnore
    public UserSettingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfUserSettings = new Anon1(qw0);
        this.__preparedStmtOfCleanUp = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void addOrUpdateUserSetting(UserSettings userSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUserSettings.insert((jw0<UserSettings>) userSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getCurrentUserSetting() {
        Throwable th;
        UserSettings userSettings;
        tw0 f = tw0.f("SELECT * FROM userSettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "isShowGoalRing");
            int c2 = dx0.c(b, "acceptedLocationDataSharing");
            int c3 = dx0.c(b, "acceptedPrivacies");
            int c4 = dx0.c(b, "acceptedTermsOfService");
            int c5 = dx0.c(b, "uid");
            int c6 = dx0.c(b, "id");
            int c7 = dx0.c(b, "isLatestLocationDataSharingAccepted");
            int c8 = dx0.c(b, "isLatestPrivacyAccepted");
            int c9 = dx0.c(b, "isLatestTermsOfServiceAccepted");
            int c10 = dx0.c(b, "latestLocationDataSharingVersion");
            int c11 = dx0.c(b, "latestPrivacyVersion");
            int c12 = dx0.c(b, "latestTermsOfServiceVersion");
            int c13 = dx0.c(b, "startDayOfWeek");
            try {
                int c14 = dx0.c(b, "createdAt");
                int c15 = dx0.c(b, "updatedAt");
                int c16 = dx0.c(b, "pinType");
                if (b.moveToFirst()) {
                    userSettings = new UserSettings();
                    userSettings.setShowGoalRing(b.getInt(c) != 0);
                    userSettings.setAcceptedLocationDataSharing(this.__userSettingConverter.b(b.getString(c2)));
                    userSettings.setAcceptedPrivacies(this.__userSettingConverter.b(b.getString(c3)));
                    userSettings.setAcceptedTermsOfService(this.__userSettingConverter.b(b.getString(c4)));
                    userSettings.setUid(b.getString(c5));
                    userSettings.setId(b.getString(c6));
                    userSettings.setLatestLocationDataSharingAccepted(b.getInt(c7) != 0);
                    userSettings.setLatestPrivacyAccepted(b.getInt(c8) != 0);
                    userSettings.setLatestTermsOfServiceAccepted(b.getInt(c9) != 0);
                    userSettings.setLatestLocationDataSharingVersion(b.getString(c10));
                    userSettings.setLatestPrivacyVersion(b.getString(c11));
                    userSettings.setLatestTermsOfServiceVersion(b.getString(c12));
                    userSettings.setStartDayOfWeek(b.getString(c13));
                    userSettings.setCreatedAt(b.getString(c14));
                    userSettings.setUpdatedAt(b.getString(c15));
                    userSettings.setPinType(b.getInt(c16));
                } else {
                    userSettings = null;
                }
                b.close();
                f.m();
                return userSettings;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getPendingUserSetting() {
        Throwable th;
        UserSettings userSettings;
        tw0 f = tw0.f("SELECT * FROM userSettings WHERE pinType = 1 LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "isShowGoalRing");
            int c2 = dx0.c(b, "acceptedLocationDataSharing");
            int c3 = dx0.c(b, "acceptedPrivacies");
            int c4 = dx0.c(b, "acceptedTermsOfService");
            int c5 = dx0.c(b, "uid");
            int c6 = dx0.c(b, "id");
            int c7 = dx0.c(b, "isLatestLocationDataSharingAccepted");
            int c8 = dx0.c(b, "isLatestPrivacyAccepted");
            int c9 = dx0.c(b, "isLatestTermsOfServiceAccepted");
            int c10 = dx0.c(b, "latestLocationDataSharingVersion");
            int c11 = dx0.c(b, "latestPrivacyVersion");
            int c12 = dx0.c(b, "latestTermsOfServiceVersion");
            int c13 = dx0.c(b, "startDayOfWeek");
            try {
                int c14 = dx0.c(b, "createdAt");
                int c15 = dx0.c(b, "updatedAt");
                int c16 = dx0.c(b, "pinType");
                if (b.moveToFirst()) {
                    userSettings = new UserSettings();
                    userSettings.setShowGoalRing(b.getInt(c) != 0);
                    userSettings.setAcceptedLocationDataSharing(this.__userSettingConverter.b(b.getString(c2)));
                    userSettings.setAcceptedPrivacies(this.__userSettingConverter.b(b.getString(c3)));
                    userSettings.setAcceptedTermsOfService(this.__userSettingConverter.b(b.getString(c4)));
                    userSettings.setUid(b.getString(c5));
                    userSettings.setId(b.getString(c6));
                    userSettings.setLatestLocationDataSharingAccepted(b.getInt(c7) != 0);
                    userSettings.setLatestPrivacyAccepted(b.getInt(c8) != 0);
                    userSettings.setLatestTermsOfServiceAccepted(b.getInt(c9) != 0);
                    userSettings.setLatestLocationDataSharingVersion(b.getString(c10));
                    userSettings.setLatestPrivacyVersion(b.getString(c11));
                    userSettings.setLatestTermsOfServiceVersion(b.getString(c12));
                    userSettings.setStartDayOfWeek(b.getString(c13));
                    userSettings.setCreatedAt(b.getString(c14));
                    userSettings.setUpdatedAt(b.getString(c15));
                    userSettings.setPinType(b.getInt(c16));
                } else {
                    userSettings = null;
                }
                b.close();
                f.m();
                return userSettings;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }
}
