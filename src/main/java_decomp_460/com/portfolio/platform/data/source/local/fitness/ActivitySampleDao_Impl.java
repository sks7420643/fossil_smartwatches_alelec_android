package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.lz4;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.pz4;
import com.fossil.qw0;
import com.fossil.qz4;
import com.fossil.sz4;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySampleDao_Impl extends ActivitySampleDao {
    @DexIgnore
    public /* final */ lz4 __activityIntensitiesConverter; // = new lz4();
    @DexIgnore
    public /* final */ pz4 __dateLongStringConverter; // = new pz4();
    @DexIgnore
    public /* final */ qz4 __dateShortStringConverter; // = new qz4();
    @DexIgnore
    public /* final */ sz4 __dateTimeISOStringConverter; // = new sz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<ActivitySample> __insertionAdapterOfActivitySample;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<ActivitySample> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, ActivitySample activitySample) {
            if (activitySample.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, activitySample.getId());
            }
            if (activitySample.getUid() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, activitySample.getUid());
            }
            String a2 = ActivitySampleDao_Impl.this.__dateShortStringConverter.a(activitySample.getDate());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
            String a3 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getStartTime());
            if (a3 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a3);
            }
            String a4 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getEndTime());
            if (a4 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a4);
            }
            px0.bindDouble(6, activitySample.getSteps());
            px0.bindDouble(7, activitySample.getCalories());
            px0.bindDouble(8, activitySample.getDistance());
            px0.bindLong(9, (long) activitySample.getActiveTime());
            String a5 = ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(activitySample.getIntensityDistInSteps());
            if (a5 == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, a5);
            }
            px0.bindLong(11, (long) activitySample.getTimeZoneOffsetInSecond());
            if (activitySample.getSourceId() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, activitySample.getSourceId());
            }
            px0.bindLong(13, activitySample.getSyncTime());
            px0.bindLong(14, activitySample.getCreatedAt());
            px0.bindLong(15, activitySample.getUpdatedAt());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_sample` (`id`,`uid`,`date`,`startTime`,`endTime`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneOffsetInSecond`,`sourceId`,`syncTime`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM activity_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<ActivitySample>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon3(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ActivitySample> call() throws Exception {
            Cursor b = ex0.b(ActivitySampleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "uid");
                int c3 = dx0.c(b, "date");
                int c4 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c5 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c6 = dx0.c(b, "steps");
                int c7 = dx0.c(b, "calories");
                int c8 = dx0.c(b, "distance");
                int c9 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c10 = dx0.c(b, "intensityDistInSteps");
                int c11 = dx0.c(b, "timeZoneOffsetInSecond");
                int c12 = dx0.c(b, SampleRaw.COLUMN_SOURCE_ID);
                int c13 = dx0.c(b, "syncTime");
                int c14 = dx0.c(b, "createdAt");
                int c15 = dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    ActivitySample activitySample = new ActivitySample(b.getString(c2), ActivitySampleDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c4)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c5)), b.getDouble(c6), b.getDouble(c7), b.getDouble(c8), b.getInt(c9), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.b(b.getString(c10)), b.getInt(c11), b.getString(c12), b.getLong(c13), b.getLong(c14), b.getLong(c15));
                    activitySample.setId(b.getString(c));
                    arrayList.add(activitySample);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<com.portfolio.platform.data.model.room.fitness.SampleRaw> call() throws Exception {
            Cursor b = ex0.b(ActivitySampleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "pinType");
                int c3 = dx0.c(b, SampleRaw.COLUMN_UA_PIN_TYPE);
                int c4 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c5 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c6 = dx0.c(b, SampleRaw.COLUMN_SOURCE_ID);
                int c7 = dx0.c(b, SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
                int c8 = dx0.c(b, SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
                int c9 = dx0.c(b, "steps");
                int c10 = dx0.c(b, "calories");
                int c11 = dx0.c(b, "distance");
                int c12 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c13 = dx0.c(b, "intensityDistInSteps");
                int c14 = dx0.c(b, SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    com.portfolio.platform.data.model.room.fitness.SampleRaw sampleRaw = new com.portfolio.platform.data.model.room.fitness.SampleRaw(ActivitySampleDao_Impl.this.__dateLongStringConverter.b(b.getString(c4)), ActivitySampleDao_Impl.this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public ActivitySampleDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfActivitySample = new Anon1(qw0);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public ActivitySample getActivitySample(String str) {
        Throwable th;
        ActivitySample activitySample;
        tw0 f = tw0.f("SELECT * FROM activity_sample WHERE id = ? LIMIT 1", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "uid");
            int c3 = dx0.c(b, "date");
            int c4 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c5 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c6 = dx0.c(b, "steps");
            int c7 = dx0.c(b, "calories");
            int c8 = dx0.c(b, "distance");
            int c9 = dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c10 = dx0.c(b, "intensityDistInSteps");
            int c11 = dx0.c(b, "timeZoneOffsetInSecond");
            int c12 = dx0.c(b, SampleRaw.COLUMN_SOURCE_ID);
            int c13 = dx0.c(b, "syncTime");
            try {
                int c14 = dx0.c(b, "createdAt");
                int c15 = dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    activitySample = new ActivitySample(b.getString(c2), this.__dateShortStringConverter.b(b.getString(c3)), this.__dateTimeISOStringConverter.b(b.getString(c4)), this.__dateTimeISOStringConverter.b(b.getString(c5)), b.getDouble(c6), b.getDouble(c7), b.getDouble(c8), b.getInt(c9), this.__activityIntensitiesConverter.b(b.getString(c10)), b.getInt(c11), b.getString(c12), b.getLong(c13), b.getLong(c14), b.getLong(c15));
                    activitySample.setId(b.getString(c));
                } else {
                    activitySample = null;
                }
                b.close();
                f.m();
                return activitySample;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public LiveData<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateLongStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateLongStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{SampleRaw.TABLE_NAME}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2) {
        tw0 f = tw0.f("SELECT * FROM activity_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{ActivitySample.TABLE_NAME}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public void upsertListActivitySample(List<ActivitySample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
