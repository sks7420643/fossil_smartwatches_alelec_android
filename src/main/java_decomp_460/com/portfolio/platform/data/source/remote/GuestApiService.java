package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.m98;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.y98;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GuestApiService {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, boolean z, qn7 qn7, int i, Object obj) {
            if (obj == null) {
                return guestApiService.getFirmwares(str, str2, (i & 4) != 0 ? "android" : str3, (i & 8) != 0 ? true : z, qn7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @m98("firmwares")
    Object getFirmwares(@y98("appVersion") String str, @y98("deviceModel") String str2, @y98("os") String str3, @y98("includesInactive") boolean z, qn7<? super q88<ApiResponse<Firmware>>> qn7);

    @DexIgnore
    @m98("assets/app-localizations")
    Object getLocalizations(@y98("metadata.appVersion") String str, @y98("metadata.platform") String str2, qn7<? super q88<ApiResponse<gj4>>> qn7);
}
