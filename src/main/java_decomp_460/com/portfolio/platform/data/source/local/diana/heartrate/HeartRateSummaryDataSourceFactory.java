package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.fossil.no4;
import com.fossil.pq7;
import com.fossil.xt0;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryDataSourceFactory extends xt0.b<Date, DailyHeartRateSummary> {
    @DexIgnore
    public /* final */ no4 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public HeartRateSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<HeartRateSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository summariesRepository;

    @DexIgnore
    public HeartRateSummaryDataSourceFactory(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository2, FitnessDatabase fitnessDatabase2, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(heartRateSummaryRepository, "summariesRepository");
        pq7.c(fitnessDataRepository2, "fitnessDataRepository");
        pq7.c(fitnessDatabase2, "fitnessDatabase");
        pq7.c(date, "createdDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        pq7.c(calendar, "mStartCalendar");
        this.summariesRepository = heartRateSummaryRepository;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = no4;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.xt0.b
    public xt0<Date, DailyHeartRateSummary> create() {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = new HeartRateSummaryLocalDataSource(this.summariesRepository, this.fitnessDataRepository, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.localDataSource = heartRateSummaryLocalDataSource;
        this.sourceLiveData.l(heartRateSummaryLocalDataSource);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.localDataSource;
        if (heartRateSummaryLocalDataSource2 != null) {
            return heartRateSummaryLocalDataSource2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<HeartRateSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.localDataSource = heartRateSummaryLocalDataSource;
    }
}
