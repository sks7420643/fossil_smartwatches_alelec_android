package com.portfolio.platform.data.source.local.hybrid.microapp;

import com.fossil.qw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class HybridCustomizeDatabase extends qw0 {
    @DexIgnore
    public abstract MicroAppDao microAppDao();

    @DexIgnore
    public abstract MicroAppLastSettingDao microAppLastSettingDao();

    @DexIgnore
    public abstract HybridPresetDao presetDao();
}
