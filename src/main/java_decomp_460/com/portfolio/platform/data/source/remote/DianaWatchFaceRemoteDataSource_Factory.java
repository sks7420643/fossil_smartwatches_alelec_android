package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRemoteDataSource_Factory implements Factory<DianaWatchFaceRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<SecureApiService> mSecureApiServiceProvider;

    @DexIgnore
    public DianaWatchFaceRemoteDataSource_Factory(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        this.mApiServiceV2Provider = provider;
        this.mSecureApiServiceProvider = provider2;
    }

    @DexIgnore
    public static DianaWatchFaceRemoteDataSource_Factory create(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        return new DianaWatchFaceRemoteDataSource_Factory(provider, provider2);
    }

    @DexIgnore
    public static DianaWatchFaceRemoteDataSource newInstance(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        return new DianaWatchFaceRemoteDataSource(apiServiceV2, secureApiService);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaWatchFaceRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get(), this.mSecureApiServiceProvider.get());
    }
}
