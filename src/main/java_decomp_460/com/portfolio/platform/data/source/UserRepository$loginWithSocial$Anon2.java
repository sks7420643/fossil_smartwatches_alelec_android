package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$loginWithSocial$2", f = "UserRepository.kt", l = {166, 180}, m = "invokeSuspend")
public final class UserRepository$loginWithSocial$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser.Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $clientId;
    @DexIgnore
    public /* final */ /* synthetic */ String $service;
    @DexIgnore
    public /* final */ /* synthetic */ String $token;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$loginWithSocial$Anon2(UserRepository userRepository, String str, String str2, String str3, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
        this.$service = str;
        this.$token = str2;
        this.$clientId = str3;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$loginWithSocial$Anon2 userRepository$loginWithSocial$Anon2 = new UserRepository$loginWithSocial$Anon2(this.this$0, this.$service, this.$token, this.$clientId, qn7);
        userRepository$loginWithSocial$Anon2.p$ = (iv7) obj;
        return userRepository$loginWithSocial$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser.Auth>> qn7) {
        return ((UserRepository$loginWithSocial$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x015f  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 472
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository$loginWithSocial$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
