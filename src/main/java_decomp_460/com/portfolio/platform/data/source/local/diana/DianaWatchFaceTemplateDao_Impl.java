package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceTemplate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceTemplateDao_Impl implements DianaWatchFaceTemplateDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<DianaWatchFaceTemplate> __deletionAdapterOfDianaWatchFaceTemplate;
    @DexIgnore
    public /* final */ jw0<DianaWatchFaceTemplate> __insertionAdapterOfDianaWatchFaceTemplate;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaWatchFaceTemplate> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            if (dianaWatchFaceTemplate.getCreatedAt() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaWatchFaceTemplate.getCreatedAt());
            }
            if (dianaWatchFaceTemplate.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaWatchFaceTemplate.getUpdatedAt());
            }
            if (dianaWatchFaceTemplate.getId() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dianaWatchFaceTemplate.getId());
            }
            if (dianaWatchFaceTemplate.getDownloadURL() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, dianaWatchFaceTemplate.getDownloadURL());
            }
            if (dianaWatchFaceTemplate.getChecksum() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, dianaWatchFaceTemplate.getChecksum());
            }
            if (dianaWatchFaceTemplate.getFirmwareOSVersion() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, dianaWatchFaceTemplate.getFirmwareOSVersion());
            }
            if (dianaWatchFaceTemplate.getPackageVersion() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, dianaWatchFaceTemplate.getPackageVersion());
            }
            if (dianaWatchFaceTemplate.getThemeClass() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, dianaWatchFaceTemplate.getThemeClass());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchfaceTemplate` (`createdAt`,`updatedAt`,`id`,`downloadURL`,`checksum`,`firmwareOSVersion`,`packageVersion`,`themeClass`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<DianaWatchFaceTemplate> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            if (dianaWatchFaceTemplate.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaWatchFaceTemplate.getId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `dianaWatchfaceTemplate` WHERE `id` = ?";
        }
    }

    @DexIgnore
    public DianaWatchFaceTemplateDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaWatchFaceTemplate = new Anon1(qw0);
        this.__deletionAdapterOfDianaWatchFaceTemplate = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public void deleteDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfDianaWatchFaceTemplate.handle(dianaWatchFaceTemplate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public List<DianaWatchFaceTemplate> getAllDianaWatchFaceTemplate() {
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceTemplate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "id");
            int c4 = dx0.c(b, "downloadURL");
            int c5 = dx0.c(b, "checksum");
            int c6 = dx0.c(b, "firmwareOSVersion");
            int c7 = dx0.c(b, "packageVersion");
            int c8 = dx0.c(b, "themeClass");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaWatchFaceTemplate dianaWatchFaceTemplate = new DianaWatchFaceTemplate(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8));
                dianaWatchFaceTemplate.setCreatedAt(b.getString(c));
                dianaWatchFaceTemplate.setUpdatedAt(b.getString(c2));
                arrayList.add(dianaWatchFaceTemplate);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public DianaWatchFaceTemplate getDianaWatchFaceTemplateById(String str) {
        DianaWatchFaceTemplate dianaWatchFaceTemplate = null;
        tw0 f = tw0.f("SELECT * FROM dianaWatchfaceTemplate WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "id");
            int c4 = dx0.c(b, "downloadURL");
            int c5 = dx0.c(b, "checksum");
            int c6 = dx0.c(b, "firmwareOSVersion");
            int c7 = dx0.c(b, "packageVersion");
            int c8 = dx0.c(b, "themeClass");
            if (b.moveToFirst()) {
                dianaWatchFaceTemplate = new DianaWatchFaceTemplate(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8));
                dianaWatchFaceTemplate.setCreatedAt(b.getString(c));
                dianaWatchFaceTemplate.setUpdatedAt(b.getString(c2));
            }
            return dianaWatchFaceTemplate;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public void upsertDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceTemplate.insert((jw0<DianaWatchFaceTemplate>) dianaWatchFaceTemplate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
