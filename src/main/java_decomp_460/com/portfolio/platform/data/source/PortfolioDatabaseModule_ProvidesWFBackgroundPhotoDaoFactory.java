package com.portfolio.platform.data.source;

import com.fossil.h97;
import com.fossil.lk7;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWFBackgroundPhotoDaoFactory implements Factory<h97> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> customizeDatabaseProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWFBackgroundPhotoDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.customizeDatabaseProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWFBackgroundPhotoDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesWFBackgroundPhotoDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static h97 providesWFBackgroundPhotoDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        h97 providesWFBackgroundPhotoDao = portfolioDatabaseModule.providesWFBackgroundPhotoDao(dianaCustomizeDatabase);
        lk7.c(providesWFBackgroundPhotoDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesWFBackgroundPhotoDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public h97 get() {
        return providesWFBackgroundPhotoDao(this.module, this.customizeDatabaseProvider.get());
    }
}
