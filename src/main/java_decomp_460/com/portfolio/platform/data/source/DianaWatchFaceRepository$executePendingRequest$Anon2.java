package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$executePendingRequest$2", f = "DianaWatchFaceRepository.kt", l = {76, 84, 87, 91}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$executePendingRequest$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$executePendingRequest$Anon2(DianaWatchFaceRepository dianaWatchFaceRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$executePendingRequest$Anon2 dianaWatchFaceRepository$executePendingRequest$Anon2 = new DianaWatchFaceRepository$executePendingRequest$Anon2(this.this$0, qn7);
        dianaWatchFaceRepository$executePendingRequest$Anon2.p$ = (iv7) obj;
        return dianaWatchFaceRepository$executePendingRequest$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DianaWatchFaceRepository$executePendingRequest$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01a9  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
        // Method dump skipped, instructions count: 442
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository$executePendingRequest$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
