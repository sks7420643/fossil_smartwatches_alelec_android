package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$2", f = "ActivitiesRepository.kt", l = {145, 146, 158}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon2(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2 = new ActivitiesRepository$fetchActivitySamples$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, qn7);
        activitiesRepository$fetchActivitySamples$Anon2.p$ = (iv7) obj;
        return activitiesRepository$fetchActivitySamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<ApiResponse<Activity>>> qn7) {
        return ((ActivitiesRepository$fetchActivitySamples$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016a  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 452
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
