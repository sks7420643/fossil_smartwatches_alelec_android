package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.room.UserDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$clearAllUser$2", f = "UserRepository.kt", l = {260}, m = "invokeSuspend")
public final class UserRepository$clearAllUser$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$clearAllUser$Anon2(UserRepository userRepository, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$clearAllUser$Anon2 userRepository$clearAllUser$Anon2 = new UserRepository$clearAllUser$Anon2(this.this$0, qn7);
        userRepository$clearAllUser$Anon2.p$ = (iv7) obj;
        return userRepository$clearAllUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((UserRepository$clearAllUser$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object H;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            FLogger.INSTANCE.getLocal().e(UserRepository.TAG, "clearAllUser");
            this.this$0.clearUserSettings();
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            H = bn5.H(this);
            if (H == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            H = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((UserDatabase) H).userDao().clearAllUser();
        return tl7.f3441a;
    }
}
