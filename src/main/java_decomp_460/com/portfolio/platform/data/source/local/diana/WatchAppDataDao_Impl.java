package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.rz4;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.WatchAppData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataDao_Impl implements WatchAppDataDao {
    @DexIgnore
    public /* final */ rz4 __dateTimeConverter; // = new rz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WatchAppData> __insertionAdapterOfWatchAppData;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteWatchAppDataById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WatchAppData> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WatchAppData watchAppData) {
            if (watchAppData.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, watchAppData.getId());
            }
            if (watchAppData.getType() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, watchAppData.getType());
            }
            if (watchAppData.getMaxAppVersion() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, watchAppData.getMaxAppVersion());
            }
            if (watchAppData.getMaxFirmwareOSVersion() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, watchAppData.getMaxFirmwareOSVersion());
            }
            if (watchAppData.getMinAppVersion() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, watchAppData.getMinAppVersion());
            }
            if (watchAppData.getMinFirmwareOSVersion() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, watchAppData.getMinFirmwareOSVersion());
            }
            if (watchAppData.getVersion() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, watchAppData.getVersion());
            }
            if (watchAppData.getExecutableBinaryDataUrl() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, watchAppData.getExecutableBinaryDataUrl());
            }
            px0.bindLong(9, WatchAppDataDao_Impl.this.__dateTimeConverter.b(watchAppData.getUpdatedAt()));
            px0.bindLong(10, WatchAppDataDao_Impl.this.__dateTimeConverter.b(watchAppData.getCreatedAt()));
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppData` (`id`,`type`,`maxAppVersion`,`maxFirmwareOSVersion`,`minAppVersion`,`minFirmwareOSVersion`,`version`,`executableBinaryDataUrl`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchAppData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchAppData WHERE id=?";
        }
    }

    @DexIgnore
    public WatchAppDataDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWatchAppData = new Anon1(qw0);
        this.__preparedStmtOfClearAll = new Anon2(qw0);
        this.__preparedStmtOfDeleteWatchAppDataById = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void deleteWatchAppDataById(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteWatchAppDataById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppDataById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public List<WatchAppData> getAllWatchAppData() {
        tw0 f = tw0.f("SELECT * FROM watchAppData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "type");
            int c3 = dx0.c(b, "maxAppVersion");
            int c4 = dx0.c(b, "maxFirmwareOSVersion");
            int c5 = dx0.c(b, "minAppVersion");
            int c6 = dx0.c(b, "minFirmwareOSVersion");
            int c7 = dx0.c(b, "version");
            int c8 = dx0.c(b, "executableBinaryDataUrl");
            int c9 = dx0.c(b, "updatedAt");
            int c10 = dx0.c(b, "createdAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchAppData(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), this.__dateTimeConverter.a(b.getLong(c9)), this.__dateTimeConverter.a(b.getLong(c10))));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppData(WatchAppData watchAppData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert((jw0<WatchAppData>) watchAppData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppDataList(List<WatchAppData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
