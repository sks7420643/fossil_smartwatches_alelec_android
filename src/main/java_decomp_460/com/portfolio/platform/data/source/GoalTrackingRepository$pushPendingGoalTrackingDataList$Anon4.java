package com.portfolio.platform.data.source;

import com.fossil.bw7;
import com.fossil.dr7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.pq7;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4(GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onFail(int i) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onSuccess(List<GoalTrackingData> list) {
        pq7.c(list, "goalTrackingList");
        dr7 dr7 = new dr7();
        dr7.element = (T) list.get(0).getDate();
        dr7 dr72 = new dr7();
        dr72.element = (T) list.get(0).getDate();
        for (GoalTrackingData goalTrackingData : list) {
            if (goalTrackingData.getDate().getTime() < dr7.element.getTime()) {
                dr7.element = (T) goalTrackingData.getDate();
            }
            if (goalTrackingData.getDate().getTime() > dr72.element.getTime()) {
                dr72.element = (T) goalTrackingData.getDate();
            }
        }
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4$onSuccess$Anon1_Level2(this, dr7, dr72, null), 3, null);
    }
}
