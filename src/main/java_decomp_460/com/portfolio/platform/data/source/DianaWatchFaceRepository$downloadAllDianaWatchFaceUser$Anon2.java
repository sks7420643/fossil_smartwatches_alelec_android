package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$2", f = "DianaWatchFaceRepository.kt", l = {153, 158, 160, 165}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2(DianaWatchFaceRepository dianaWatchFaceRepository, int i, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
        this.$offset = i;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2 dianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2 = new DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2(this.this$0, this.$offset, qn7);
        dianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2.p$ = (iv7) obj;
        return dianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x019b  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 424
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
