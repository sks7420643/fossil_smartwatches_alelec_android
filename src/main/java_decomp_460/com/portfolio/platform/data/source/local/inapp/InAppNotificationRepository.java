package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "InAppNotificationRepository";
    @DexIgnore
    public /* final */ InAppNotificationDao mInAppNotificationDao;
    @DexIgnore
    public /* final */ List<InAppNotification> mNotificationList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public InAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        pq7.c(inAppNotificationDao, "mInAppNotificationDao");
        this.mInAppNotificationDao = inAppNotificationDao;
    }

    @DexIgnore
    public final Object delete(InAppNotification inAppNotification, qn7<? super tl7> qn7) {
        this.mInAppNotificationDao.delete(inAppNotification);
        return tl7.f3441a;
    }

    @DexIgnore
    public final LiveData<List<InAppNotification>> getAllInAppNotifications() {
        return this.mInAppNotificationDao.getAllInAppNotification();
    }

    @DexIgnore
    public final void handleReceivingNotification(InAppNotification inAppNotification) {
        pq7.c(inAppNotification, "inAppNotification");
        this.mNotificationList.add(inAppNotification);
    }

    @DexIgnore
    public final Object insert(List<InAppNotification> list, qn7<? super tl7> qn7) {
        this.mInAppNotificationDao.insertListInAppNotification(list);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void removeNotificationAfterSending(InAppNotification inAppNotification) {
        pq7.c(inAppNotification, "inAppNotification");
        this.mNotificationList.remove(inAppNotification);
    }
}
