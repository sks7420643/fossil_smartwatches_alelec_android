package com.portfolio.platform.data.source.remote;

import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationRemoteDataSource implements NotificationsDataSource {
    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllNotificationSetting() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllPhoneFavoritesContacts() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilters(int i) {
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getAllContactGroups(int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public AppFilter getAppFilterByType(String str, int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public Contact getContactById(int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getContactGroupId(List<Integer> list) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getLocalContactId() {
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAllAppFilters() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAppFilter(AppFilter appFilter) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContact(Contact contact) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroup(ContactGroup contactGroup) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroupList(List<ContactGroup> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListAppFilter(List<AppFilter> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListContact(List<Contact> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContact(List<Integer> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContactGroup(List<Integer> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(String str) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumber(List<Integer> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumberByContactGroupId(int i) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeRedundantContact() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveAppFilter(AppFilter appFilter) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContact(Contact contact) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroup(ContactGroup contactGroup) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroupList(List<ContactGroup> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveEmailAddress(EmailAddress emailAddress) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListAppFilters(List<AppFilter> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListContact(List<Contact> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListPhoneNumber(List<PhoneNumber> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneNumber(PhoneNumber phoneNumber) {
    }
}
