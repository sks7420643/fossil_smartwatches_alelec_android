package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.ThirdPartyRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$pushPendingData$2", f = "ThirdPartyRepository.kt", l = {86}, m = "invokeSuspend")
public final class ThirdPartyRepository$pushPendingData$Anon2 extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$pushPendingData$Anon2(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, qn7 qn7) {
        super(2, qn7);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ThirdPartyRepository$pushPendingData$Anon2 thirdPartyRepository$pushPendingData$Anon2 = new ThirdPartyRepository$pushPendingData$Anon2(this.this$0, this.$pushPendingThirdPartyDataCallback, qn7);
        thirdPartyRepository$pushPendingData$Anon2.p$ = (iv7) obj;
        return thirdPartyRepository$pushPendingData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
        return ((ThirdPartyRepository$pushPendingData$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ThirdPartyRepository thirdPartyRepository = this.this$0;
            ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback = this.$pushPendingThirdPartyDataCallback;
            this.L$0 = iv7;
            this.label = 1;
            Object uploadData = thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, this);
            return uploadData == d ? d : uploadData;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
