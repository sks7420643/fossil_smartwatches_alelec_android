package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.zq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$2", f = "DianaWatchFaceRepository.kt", l = {173, 178, 180}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ zq7 $isSuccess;
    @DexIgnore
    public /* final */ /* synthetic */ String $orderId;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2(DianaWatchFaceRepository dianaWatchFaceRepository, String str, zq7 zq7, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
        this.$orderId = str;
        this.$isSuccess = zq7;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2 dianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2 = new DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2(this.this$0, this.$orderId, this.$isSuccess, qn7);
        dianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2.p$ = (iv7) obj;
        return dianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0113  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
