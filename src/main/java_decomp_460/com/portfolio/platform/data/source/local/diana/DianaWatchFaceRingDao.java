package com.portfolio.platform.data.source.local.diana;

import com.fossil.qn7;
import com.fossil.tl7;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaWatchFaceRingDao {
    @DexIgnore
    Object clearAll(qn7<? super tl7> qn7);

    @DexIgnore
    Object getAllDianaWatchFaceRings(qn7<? super List<DianaWatchFaceRing>> qn7);

    @DexIgnore
    Object getDianaWatchFaceRing(String str, qn7<? super DianaWatchFaceRing> qn7);

    @DexIgnore
    Object upsertDianaWatchFaceRing(DianaWatchFaceRing dianaWatchFaceRing, qn7<? super tl7> qn7);

    @DexIgnore
    Object upsertDianaWatchFaceRings(List<DianaWatchFaceRing> list, qn7<? super tl7> qn7);
}
