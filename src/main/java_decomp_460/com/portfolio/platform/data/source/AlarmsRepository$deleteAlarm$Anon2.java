package com.portfolio.platform.data.source;

import com.fossil.ao7;
import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$2", f = "AlarmsRepository.kt", l = {162, 165, 171}, m = "invokeSuspend")
public final class AlarmsRepository$deleteAlarm$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<? extends Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$2$1", f = "AlarmsRepository.kt", l = {173}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super Long>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmsRepository$deleteAlarm$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = alarmsRepository$deleteAlarm$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Long> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object u;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.$alarm.setPinType(3);
                bn5 bn5 = bn5.j;
                this.L$0 = iv7;
                this.label = 1;
                u = bn5.u(this);
                if (u == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                u = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return ao7.f(((AlarmDatabase) u).alarmDao().insertAlarm(this.this$0.$alarm));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$deleteAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, qn7 qn7) {
        super(2, qn7);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2 = new AlarmsRepository$deleteAlarm$Anon2(this.this$0, this.$alarm, qn7);
        alarmsRepository$deleteAlarm$Anon2.p$ = (iv7) obj;
        return alarmsRepository$deleteAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<? extends Void>> qn7) {
        return ((AlarmsRepository$deleteAlarm$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f0  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
