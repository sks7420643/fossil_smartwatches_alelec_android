package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummary$4", f = "SummariesRepository.kt", l = {384}, m = "invokeSuspend")
public final class SummariesRepository$getSummary$Anon4 extends ko7 implements vp7<iv7, qn7<? super ActivitySummary>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Calendar $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummary$Anon4(Calendar calendar, qn7 qn7) {
        super(2, qn7);
        this.$date = calendar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$getSummary$Anon4 summariesRepository$getSummary$Anon4 = new SummariesRepository$getSummary$Anon4(this.$date, qn7);
        summariesRepository$getSummary$Anon4.p$ = (iv7) obj;
        return summariesRepository$getSummary$Anon4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super ActivitySummary> qn7) {
        return ((SummariesRepository$getSummary$Anon4) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object y;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            y = bn5.y(this);
            if (y == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivitySummaryDao activitySummaryDao = ((FitnessDatabase) y).activitySummaryDao();
        Date time = this.$date.getTime();
        pq7.b(time, "date.time");
        return activitySummaryDao.getActivitySummary(time);
    }
}
