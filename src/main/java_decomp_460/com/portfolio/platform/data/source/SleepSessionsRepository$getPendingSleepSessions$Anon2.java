package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getPendingSleepSessions$2", f = "SleepSessionsRepository.kt", l = {252}, m = "invokeSuspend")
public final class SleepSessionsRepository$getPendingSleepSessions$Anon2 extends ko7 implements vp7<iv7, qn7<? super List<MFSleepSession>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$getPendingSleepSessions$Anon2(Date date, Date date2, qn7 qn7) {
        super(2, qn7);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSessionsRepository$getPendingSleepSessions$Anon2 sleepSessionsRepository$getPendingSleepSessions$Anon2 = new SleepSessionsRepository$getPendingSleepSessions$Anon2(this.$startDate, this.$endDate, qn7);
        sleepSessionsRepository$getPendingSleepSessions$Anon2.p$ = (iv7) obj;
        return sleepSessionsRepository$getPendingSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super List<MFSleepSession>> qn7) {
        return ((SleepSessionsRepository$getPendingSleepSessions$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object D;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            D = bn5.D(this);
            if (D == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            D = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SleepDao sleepDao = ((SleepDatabase) D).sleepDao();
        Date V = lk5.V(this.$startDate);
        pq7.b(V, "DateHelper.getStartOfDay(startDate)");
        Date E = lk5.E(this.$endDate);
        pq7.b(E, "DateHelper.getEndOfDay(endDate)");
        return sleepDao.getPendingSleepSessions(V, E);
    }
}
