package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaWatchFaceUserDao {
    @DexIgnore
    Object deleteAll();  // void declaration

    @DexIgnore
    void deleteDianaWatchFaceUserById(String str);

    @DexIgnore
    List<DianaWatchFaceUser> getAllDianaWatchFaceUser();

    @DexIgnore
    LiveData<List<DianaWatchFaceUser>> getAllDianaWatchFaceUserLiveData();

    @DexIgnore
    DianaWatchFaceUser getDianaWatchFaceUserById(String str);

    @DexIgnore
    DianaWatchFaceUser getDianaWatchFaceUserByOrderId(String str);

    @DexIgnore
    List<DianaWatchFaceUser> getPendingDianaWatchFace();

    @DexIgnore
    DianaWatchFaceUser getWatchFaceByOrderWatchFaceId(String str);

    @DexIgnore
    void updatePinType(String str, int i);

    @DexIgnore
    void upsertDianaWatchFaceUser(DianaWatchFaceUser dianaWatchFaceUser);

    @DexIgnore
    void upsertDianaWatchFaceUser(List<DianaWatchFaceUser> list);
}
