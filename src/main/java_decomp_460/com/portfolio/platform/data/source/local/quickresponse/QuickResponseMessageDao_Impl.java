package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.QuickResponseMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseMessageDao_Impl extends QuickResponseMessageDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<QuickResponseMessage> __insertionAdapterOfQuickResponseMessage;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveAll;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<QuickResponseMessage> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, QuickResponseMessage quickResponseMessage) {
            px0.bindLong(1, (long) quickResponseMessage.getId());
            if (quickResponseMessage.getResponse() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, quickResponseMessage.getResponse());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseMessage` (`id`,`response`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM quickResponseMessage WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM quickResponseMessage";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<QuickResponseMessage> call() throws Exception {
            Cursor b = ex0.b(QuickResponseMessageDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "id");
                int c2 = dx0.c(b, "response");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(b.getString(c2));
                    quickResponseMessage.setId(b.getInt(c));
                    arrayList.add(quickResponseMessage);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public QuickResponseMessageDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfQuickResponseMessage = new Anon1(qw0);
        this.__preparedStmtOfRemoveById = new Anon2(qw0);
        this.__preparedStmtOfRemoveAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public List<QuickResponseMessage> getAllRawResponse() {
        tw0 f = tw0.f("SELECT * FROM quickResponseMessage", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "response");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                QuickResponseMessage quickResponseMessage = new QuickResponseMessage(b.getString(c2));
                quickResponseMessage.setId(b.getInt(c));
                arrayList.add(quickResponseMessage);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public LiveData<List<QuickResponseMessage>> getAllResponse() {
        tw0 f = tw0.f("SELECT * FROM quickResponseMessage", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"quickResponseMessage"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponse(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert((jw0<QuickResponseMessage>) quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponses(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert((jw0<QuickResponseMessage>) quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
