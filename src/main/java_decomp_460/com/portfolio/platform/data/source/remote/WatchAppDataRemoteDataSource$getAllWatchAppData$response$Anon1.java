package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.diana.WatchAppData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$response$1", f = "WatchAppDataRemoteDataSource.kt", l = {14}, m = "invokeSuspend")
public final class WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1 extends ko7 implements rp7<qn7<? super q88<ApiResponse<WatchAppData>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $appVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $firmwareVersion;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppDataRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1(WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, String str, String str2, qn7 qn7) {
        super(1, qn7);
        this.this$0 = watchAppDataRemoteDataSource;
        this.$appVersion = str;
        this.$firmwareVersion = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1(this.this$0, this.$appVersion, this.$firmwareVersion, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<WatchAppData>>> qn7) {
        return ((WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$appVersion;
            String str2 = this.$firmwareVersion;
            this.label = 1;
            Object allWatchAppData = apiServiceV2.getAllWatchAppData(str, str2, this);
            return allWatchAppData == d ? d : allWatchAppData;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
