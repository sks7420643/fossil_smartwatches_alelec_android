package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.qs4;
import com.fossil.rt4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory implements Factory<rt4> {
    @DexIgnore
    public /* final */ Provider<qs4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<qs4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<qs4> provider) {
        return new PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static rt4 providesChallengeLocalDataSource(PortfolioDatabaseModule portfolioDatabaseModule, qs4 qs4) {
        rt4 providesChallengeLocalDataSource = portfolioDatabaseModule.providesChallengeLocalDataSource(qs4);
        lk7.c(providesChallengeLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesChallengeLocalDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rt4 get() {
        return providesChallengeLocalDataSource(this.module, this.daoProvider.get());
    }
}
