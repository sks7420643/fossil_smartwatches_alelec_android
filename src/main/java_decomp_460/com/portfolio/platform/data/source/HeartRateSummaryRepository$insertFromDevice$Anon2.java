package com.portfolio.platform.data.source;

import com.fossil.bn5;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository$insertFromDevice$2", f = "HeartRateSummaryRepository.kt", l = {48}, m = "invokeSuspend")
public final class HeartRateSummaryRepository$insertFromDevice$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $heartRateSummaries;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$insertFromDevice$Anon2(List list, qn7 qn7) {
        super(2, qn7);
        this.$heartRateSummaries = list;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        HeartRateSummaryRepository$insertFromDevice$Anon2 heartRateSummaryRepository$insertFromDevice$Anon2 = new HeartRateSummaryRepository$insertFromDevice$Anon2(this.$heartRateSummaries, qn7);
        heartRateSummaryRepository$insertFromDevice$Anon2.p$ = (iv7) obj;
        return heartRateSummaryRepository$insertFromDevice$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((HeartRateSummaryRepository$insertFromDevice$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object y;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = HeartRateSummaryRepository.TAG;
            local.d(str, "insertFromDevice: heartRateSummaries = " + this.$heartRateSummaries);
            bn5 bn5 = bn5.j;
            this.L$0 = iv7;
            this.label = 1;
            y = bn5.y(this);
            if (y == d) {
                return d;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((FitnessDatabase) y).getHeartRateDailySummaryDao().insertHeartRateSummaries(this.$heartRateSummaries);
        return tl7.f3441a;
    }
}
