package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.c47;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.ss0;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon2$result$Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2 this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level3 extends c47<ActivitySummary, gj4> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2$result$Anon1_Level2 this$0;

        @DexIgnore
        public Anon1_Level3(SummariesRepository$getSummary$Anon2$result$Anon1_Level2 summariesRepository$getSummary$Anon2$result$Anon1_Level2, List list) {
            this.this$0 = summariesRepository$getSummary$Anon2$result$Anon1_Level2;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        @Override // com.fossil.c47
        public Object createCall(qn7<? super q88<gj4>> qn7) {
            Date V = lk5.V(this.this$0.this$0.$date);
            Date E = lk5.E(this.this$0.this$0.$date);
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
            String k = lk5.k(V);
            pq7.b(k, "DateHelper.formatShortDate(startDate)");
            String k2 = lk5.k(E);
            pq7.b(k2, "DateHelper.formatShortDate(endDate)");
            return apiServiceV2.getSummaries(k, k2, 0, 100, qn7);
        }

        @DexIgnore
        @Override // com.fossil.c47
        public Object loadFromDb(qn7<? super LiveData<ActivitySummary>> qn7) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(this.this$0.this$0.$date);
            LiveData<ActivitySummary> activitySummaryLiveData = this.this$0.$fitnessDatabase.activitySummaryDao().getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!lk5.p0(this.this$0.this$0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            LiveData b = ss0.b(activitySummaryLiveData, new SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(this));
            pq7.b(b, "Transformations.map(acti\u2026ary\n                    }");
            return b;
        }

        @DexIgnore
        @Override // com.fossil.c47
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x013d  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0140  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object saveCallResult(com.fossil.gj4 r15, com.fossil.qn7<? super com.fossil.tl7> r16) {
            /*
            // Method dump skipped, instructions count: 526
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.gj4, com.fossil.qn7):java.lang.Object");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
        @Override // com.fossil.c47
        public /* bridge */ /* synthetic */ Object saveCallResult(gj4 gj4, qn7 qn7) {
            return saveCallResult(gj4, (qn7<? super tl7>) qn7);
        }

        @DexIgnore
        public boolean shouldFetch(ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$Anon2$result$Anon1_Level2(SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2, FitnessDatabase fitnessDatabase) {
        this.this$0 = summariesRepository$getSummary$Anon2;
        this.$fitnessDatabase = fitnessDatabase;
    }

    @DexIgnore
    public final LiveData<h47<ActivitySummary>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - date=" + this.this$0.$date + " fitnessDataList=" + list.size());
        return new Anon1_Level3(this, list).asLiveData();
    }
}
