package com.portfolio.platform.data.source.local.fitness;

import com.fossil.fl5;
import com.fossil.pq7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadInitial$Anon1 implements fl5.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.fl5.b
    public final void run(fl5.b.a aVar) {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$0;
        fl5.d dVar = fl5.d.INITIAL;
        Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        pq7.b(aVar, "helperCallback");
        activitySummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
