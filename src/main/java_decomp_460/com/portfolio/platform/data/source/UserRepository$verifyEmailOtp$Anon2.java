package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.UserRepository$verifyEmailOtp$2", f = "UserRepository.kt", l = {281}, m = "invokeSuspend")
public final class UserRepository$verifyEmailOtp$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $emailAddress;
    @DexIgnore
    public /* final */ /* synthetic */ String $otpCode;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$verifyEmailOtp$Anon2(UserRepository userRepository, String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = userRepository;
        this.$emailAddress = str;
        this.$otpCode = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        UserRepository$verifyEmailOtp$Anon2 userRepository$verifyEmailOtp$Anon2 = new UserRepository$verifyEmailOtp$Anon2(this.this$0, this.$emailAddress, this.$otpCode, qn7);
        userRepository$verifyEmailOtp$Anon2.p$ = (iv7) obj;
        return userRepository$verifyEmailOtp$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<Void>> qn7) {
        return ((UserRepository$verifyEmailOtp$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            UserRemoteDataSource userRemoteDataSource = this.this$0.mUserRemoteDataSource;
            String str = this.$emailAddress;
            String str2 = this.$otpCode;
            this.L$0 = iv7;
            this.label = 1;
            Object verifyEmailOtp = userRemoteDataSource.verifyEmailOtp(str, str2, this);
            return verifyEmailOtp == d ? d : verifyEmailOtp;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
