package com.portfolio.platform.data.source.remote;

import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.j98;
import com.fossil.m98;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.s98;
import com.fossil.t98;
import com.fossil.u98;
import com.fossil.x98;
import com.portfolio.platform.data.model.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SecureApiService2Dot1 {
    @DexIgnore
    @j98("users/me/devices/{deviceId}")
    Object deleteDevice(@x98("deviceId") String str, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @t98("rpc/device/generate-pairing-key")
    Object generatePairingKey(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/devices/{deviceId}")
    Object getDevice(@x98("deviceId") String str, qn7<? super q88<Device>> qn7);

    @DexIgnore
    @m98("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@x98("id") String str, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/device-secret-keys")
    Object getDeviceSecretKeys(qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/devices")
    Object getDevices(qn7<? super q88<ApiResponse<Device>>> qn7);

    @DexIgnore
    @m98("users/me/devices/latest-active")
    Object getLastActiveDevice(qn7<? super q88<Device>> qn7);

    @DexIgnore
    @m98("users/me/device-secret-keys/{id}")
    Object getSecretKey(@x98("id") String str, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @u98("users/me/devices")
    Object linkDevice(@i98 Device device, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @t98("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @s98("users/me/devices/{deviceId}")
    Object updateDevice(@x98("deviceId") String str, @i98 Device device, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @s98("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@x98("id") String str, @i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @s98("users/me/device-secret-keys/{id}")
    Object updateSecretKey(@x98("id") String str, qn7<? super q88<gj4>> qn7);
}
