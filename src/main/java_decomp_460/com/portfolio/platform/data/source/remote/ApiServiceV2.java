package com.portfolio.platform.data.source.remote;

import com.fossil.bt4;
import com.fossil.ca8;
import com.fossil.dt4;
import com.fossil.g97;
import com.fossil.gj4;
import com.fossil.i98;
import com.fossil.it4;
import com.fossil.j98;
import com.fossil.ks4;
import com.fossil.m98;
import com.fossil.mo5;
import com.fossil.ms4;
import com.fossil.mt4;
import com.fossil.no5;
import com.fossil.o98;
import com.fossil.p77;
import com.fossil.p98;
import com.fossil.ps4;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.s98;
import com.fossil.t98;
import com.fossil.u98;
import com.fossil.w18;
import com.fossil.x98;
import com.fossil.xs4;
import com.fossil.y98;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.UserWrapper;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppData;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ApiServiceV2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getDeviceAssets$default(ApiServiceV2 apiServiceV2, int i, int i2, String str, String str2, String str3, String str4, String str5, qn7 qn7, int i3, Object obj) {
            if (obj == null) {
                return apiServiceV2.getDeviceAssets(i, i2, str, str2, str3, str4, (i3 & 64) != 0 ? null : str5, qn7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getDeviceAssets");
        }
    }

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@i98 gj4 gj4, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/block-user")
    Object block(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/cancel-current-user-friend-request")
    Object cancelFriendRequest(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/users/me/challenges")
    Object createChallenge(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @j98("users/me/alarms/{id}")
    Object deleteAlarm(@x98("id") String str, qn7<? super q88<Void>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@i98 gj4 gj4, qn7<? super q88<ApiResponse<gj4>>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "/v2/users/me/diana-photos")
    Object deleteBackgroundPhotos(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @j98("/v2/users/me/challenges/{id}")
    Object deleteChallenge(@x98("id") String str, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "/v2/users/me/diana-watch-face-presets")
    Object deleteDianaPresets(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@i98 gj4 gj4, qn7<? super q88<GoalEvent>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "/v2/users/me/notifications")
    Object deleteNotification(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @j98("users/me")
    Object deleteUser(qn7<? super q88<Void>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/diana-faces")
    Object deleteUserDianaWatchFace(@i98 gj4 gj4, qn7<? super q88<Object>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/workout-sessions")
    Object deleteWorkoutSession(@i98 gj4 gj4, qn7<? super q88<ApiResponse<ServerWorkoutSession>>> qn7);

    @DexIgnore
    @m98
    Object downloadFile(@ca8 String str, qn7<? super q88<w18>> qn7);

    @DexIgnore
    @s98("/v2/users/me/challenges/{id}")
    Object editChallenge(@x98("id") String str, @i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("rpc/feature-flag/evaluate-flags")
    Object featureFlag(@p98("X-Active-Device") String str, @p98("User-Agent") String str2, @i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/list-available-challenges")
    Object fetchAvailableChallenges(qn7<? super q88<ApiResponse<ps4>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/diana-photos")
    Object fetchBackgroundPhotos(qn7<? super q88<ApiResponse<g97>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/challenges")
    Object fetchChallengesWithStatus(@y98("status") String[] strArr, @y98("limit") Integer num, qn7<? super q88<ApiResponse<ps4>>> qn7);

    @DexIgnore
    @m98("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/users/me/diana-app-settings")
    Object fetchDianaAppSetting(qn7<? super q88<ApiResponse<DianaAppSetting>>> qn7);

    @DexIgnore
    @m98("users/me/diana-watch-face-presets")
    Object fetchDianaPreset(@y98("serialNumber") String str, @y98("limit") int i, @y98("offset") int i2, qn7<? super q88<ApiResponse<mo5>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/challenge-histories")
    Object fetchHistoryChallenges(@y98("limit") int i, @y98("offset") int i2, qn7<? super q88<ApiResponse<bt4>>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/list-pending-invitations")
    Object fetchPendingChallenges(qn7<? super q88<ApiResponse<ps4>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/sent-friend-requests")
    Object fetchReceivedRequestFriends(@y98("limit") Integer num, qn7<? super q88<ApiResponse<xs4>>> qn7);

    @DexIgnore
    @m98("/v2/diana-recommended-watch-face-presets")
    Object fetchRecommendedDianaPreset(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<no5>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/received-friend-requests")
    Object fetchSentRequestFriends(@y98("limit") Integer num, qn7<? super q88<ApiResponse<xs4>>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-watch-face-editor-background")
    Object fetchWFBackgroundTemplates(@y98("sortBy") String str, @y98("limit") int i, qn7<? super q88<ApiResponse<p77>>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-watch-face-editor-sticker")
    Object fetchWFTicker(@y98("limit") int i, qn7<? super q88<ApiResponse<p77>>> qn7);

    @DexIgnore
    @m98("/v2/rpc/social/search-user-social-profiles")
    Object findFriend(@y98("keyword") String str, qn7<? super q88<ApiResponse<xs4>>> qn7);

    @DexIgnore
    @t98("/v2/rpc/diana-watch-face-presets/generate-shareable-link")
    Object generateSharableLink(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/users/me/social-profile")
    Object generateSocialProfile(qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/activities")
    Object getActivities(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<Activity>>> qn7);

    @DexIgnore
    @m98("users/me/activity-settings")
    Object getActivitySetting(qn7<? super q88<ActivitySettings>> qn7);

    @DexIgnore
    @m98("users/me/activity-statistic")
    Object getActivityStatistic(qn7<? super q88<ActivityStatistic>> qn7);

    @DexIgnore
    @m98("users/me/alarms")
    Object getAlarms(@y98("limit") int i, qn7<? super q88<ApiResponse<Alarm>>> qn7);

    @DexIgnore
    @m98("diana-complication-apps")
    Object getAllComplication(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<Complication>>> qn7);

    @DexIgnore
    @m98("hybrid-apps")
    Object getAllMicroApp(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<MicroApp>>> qn7);

    @DexIgnore
    @m98("hybrid-app-variants")
    Object getAllMicroAppVariant(@y98("serialNumber") String str, @y98("majorNumber") String str2, @y98("minorNumber") String str3, qn7<? super q88<ApiResponse<MicroAppVariant>>> qn7);

    @DexIgnore
    @m98("users/me/diana-faces")
    Object getAllUserDianaWatchFace(@y98("limit") int i, @y98("offset") int i2, qn7<? super q88<ApiResponse<DianaWatchFaceUser>>> qn7);

    @DexIgnore
    @m98("diana-pusher-apps")
    Object getAllWatchApp(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<WatchApp>>> qn7);

    @DexIgnore
    @m98("diana-watch-apps")
    Object getAllWatchAppData(@y98("appVersion") String str, @y98("firmwareOSVersion") String str2, qn7<? super q88<ApiResponse<WatchAppData>>> qn7);

    @DexIgnore
    @m98("app-categories")
    Object getCategories(qn7<? super q88<ApiResponse<Category>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/challenges/{id}")
    Object getChallengeById(@x98("id") String str, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<gj4>>> qn7);

    @DexIgnore
    @m98("assets/app-sku-images")
    Object getDeviceAssets(@y98("size") int i, @y98("offset") int i2, @y98("metadata.serialNumber") String str, @y98("metadata.feature") String str2, @y98("metadata.resolution") String str3, @y98("metadata.platform") String str4, @y98("metadata.fastPairId") String str5, qn7<? super q88<ApiResponse<gj4>>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-complication-ringstyles")
    Object getDianaComplicationRingStyles(@y98("metadata.isDefault") boolean z, @y98("metadata.serialNumber") String str, qn7<? super q88<ApiResponse<DianaComplicationRingStyle>>> qn7);

    @DexIgnore
    @m98("users/me/diana-presets")
    Object getDianaPresetList(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<DianaPreset>>> qn7);

    @DexIgnore
    @m98("diana-recommended-presets")
    Object getDianaRecommendPresetList(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<DianaRecommendPreset>>> qn7);

    @DexIgnore
    @m98("users/me/diana-watch-face-presets/{id}")
    Object getDianaWatchFacePreset(@x98("id") String str, qn7<? super q88<ApiResponse<mo5>>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-watch-face-editor-ring-style")
    Object getDianaWatchFaceRings(@y98("sortBy") String str, qn7<? super q88<ApiResponse<DianaWatchFaceRing>>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/display-players")
    Object getDisplayPlayers(@y98("challengeIds") List<String> list, qn7<? super q88<ApiResponse<ks4>>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/list-compact-players-current-user-challenges")
    Object getFocusedPlayers(@y98("challengeIds") List<String> list, @y98("numberOfTopPlayers") int i, @y98("numberOfNearPlayers") int i2, qn7<? super q88<ApiResponse<ks4>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/friends")
    Object getFriends(@y98("status") String str, qn7<? super q88<ApiResponse<xs4>>> qn7);

    @DexIgnore
    @m98("users/me/goal-settings")
    Object getGoalSetting(qn7<? super q88<GoalSetting>> qn7);

    @DexIgnore
    @m98("users/me/goal-events")
    Object getGoalTrackingDataList(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<GoalEvent>>> qn7);

    @DexIgnore
    @m98("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<GoalDailySummary>>> qn7);

    @DexIgnore
    @m98("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@x98("date") String str, qn7<? super q88<GoalDailySummary>> qn7);

    @DexIgnore
    @m98("users/me/heart-rates")
    Object getHeartRateSamples(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<HeartRate>>> qn7);

    @DexIgnore
    @m98("users/me/hybrid-presets")
    Object getHybridPresetList(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<HybridPreset>>> qn7);

    @DexIgnore
    @m98("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<HybridRecommendPreset>>> qn7);

    @DexIgnore
    @m98("assets/e-label")
    Object getLabel(@y98("metadata.serialNumber") String str, qn7<? super q88<ApiResponse<Label>>> qn7);

    @DexIgnore
    @m98("assets/watch-params")
    Object getLatestWatchParams(@y98("metadata.serialNumber") String str, @y98("metadata.version.major") int i, @y98("sortBy") String str2, @y98("offset") int i2, @y98("limit") int i3, qn7<? super q88<ApiResponse<WatchParameterResponse>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/social-profile")
    Object getMySocialProfile(qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/users/me/challenges/{id}/pending-players")
    Object getPendingPlayers(@x98("id") String str, @y98("limit") Integer num, qn7<? super q88<ApiResponse<xs4>>> qn7);

    @DexIgnore
    @m98("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@y98("age") int i, @y98("weightInGrams") int i2, @y98("heightInCentimeters") int i3, @y98("gender") String str, qn7<? super q88<ActivityRecommendedGoals>> qn7);

    @DexIgnore
    @m98("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@y98("age") int i, @y98("weightInGrams") int i2, @y98("heightInCentimeters") int i3, @y98("gender") String str, qn7<? super q88<SleepRecommendedGoal>> qn7);

    @DexIgnore
    @m98("server-settings")
    Object getServerSettingList(@y98("limit") int i, @y98("offset") int i2, qn7<? super q88<ServerSettingList>> qn7);

    @DexIgnore
    @m98("/v2/rpc/diana-watch-face-presets/get-sharing-face")
    Object getSharingFace(@y98("id") String str, @y98("token") String str2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/sleep-sessions")
    Object getSleepSessions(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/sleep-settings")
    Object getSleepSetting(qn7<? super q88<MFSleepSettings>> qn7);

    @DexIgnore
    @m98("users/me/sleep-statistic")
    Object getSleepStatistic(qn7<? super q88<SleepStatistic>> qn7);

    @DexIgnore
    @m98("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/activity-daily-summaries")
    Object getSummaries(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/get-current-user-sync-status-data")
    Object getSyncStatusData(qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@i98 TrafficRequest trafficRequest, qn7<? super q88<TrafficResponse>> qn7);

    @DexIgnore
    @m98("users/me/profile")
    Object getUser(qn7<? super q88<UserWrapper>> qn7);

    @DexIgnore
    @m98("users/me/diana-faces/{id}")
    Object getUserDianaWatchFace(@x98("id") String str, qn7<? super q88<DianaWatchFaceUser>> qn7);

    @DexIgnore
    @m98("users/me/diana-faces")
    Object getUserDianaWatchFaceWithOrderId(@y98("orderId") String str, qn7<? super q88<ApiResponse<DianaWatchFaceUser>>> qn7);

    @DexIgnore
    @m98("users/me/settings")
    Object getUserSettings(qn7<? super q88<UserSettings>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-watch-face-editor-ring-style")
    Object getWFRings(@y98("sortBy") String str, @y98("limit") int i, qn7<? super q88<ApiResponse<p77>>> qn7);

    @DexIgnore
    @m98("/v2/diana-watch-faces")
    Object getWatchFaces(@y98("serialNumber") String str, qn7<? super q88<ApiResponse<WatchFace>>> qn7);

    @DexIgnore
    @m98("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@y98("metadata.locale") String str, qn7<? super q88<ApiResponse<WatchLocalization>>> qn7);

    @DexIgnore
    @m98("weather-info")
    Object getWeather(@y98("lat") String str, @y98("lng") String str2, @y98("temperatureUnit") String str3, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("users/me/workout-sessions")
    Object getWorkoutSessions(@y98("startDate") String str, @y98("endDate") String str2, @y98("offset") int i, @y98("limit") int i2, qn7<? super q88<ApiResponse<ServerWorkoutSession>>> qn7);

    @DexIgnore
    @m98("/v2/users/me/workout-settings")
    Object getWorkoutSettingList(qn7<? super q88<RemoteWorkoutSetting>> qn7);

    @DexIgnore
    @t98("users/me/activities")
    Object insertActivities(@i98 gj4 gj4, qn7<? super q88<ApiResponse<Activity>>> qn7);

    @DexIgnore
    @t98("users/me/fitness-files")
    Object insertFitnessDataFiles(@i98 ApiResponse<ServerFitnessDataWrapper> apiResponse, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("users/me/goal-events")
    Object insertGoalTrackingDataList(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @u98("users/me/installations")
    Object insertInstallation(@i98 Installation installation, qn7<? super q88<Installation>> qn7);

    @DexIgnore
    @t98("users/me/sleep-sessions")
    Object insertSleepSessions(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/users/me/challenges/{id}/players")
    Object invitedPlayers(@x98("id") String str, @y98("status") String[] strArr, qn7<? super q88<ApiResponse<ms4>>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/join-challenge")
    Object joinChallenge(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/leave-challenge")
    Object leaveChallenge(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/users/me/notifications")
    Object notifications(@y98("limit") Integer num, qn7<? super q88<ApiResponse<dt4>>> qn7);

    @DexIgnore
    @s98("/v2/users/me/diana-app-settings")
    Object patchDianaAppSetting(@i98 gj4 gj4, qn7<? super q88<ApiResponse<DianaAppSetting>>> qn7);

    @DexIgnore
    @t98("users/me/notification-clients")
    Object pushFCMToken(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/player/push-current-user-step-data")
    Object pushStepData(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @m98("/v2/rpc/challenges/list-pending-or-available-challenges")
    Object recommendedChallenges(qn7<? super q88<ApiResponse<mt4>>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/rematch-challenge")
    Object rematchChallenge(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @o98(hasBody = true, method = "DELETE", path = "users/me/notification-clients")
    Object removeFCMToken(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @u98("users/me/diana-presets")
    Object replaceDianaPresetList(@i98 gj4 gj4, qn7<? super q88<ApiResponse<DianaPreset>>> qn7);

    @DexIgnore
    @u98("users/me/hybrid-presets")
    Object replaceHybridPresetList(@i98 gj4 gj4, qn7<? super q88<ApiResponse<HybridPreset>>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/update-current-user-friend-request")
    Object respondFriendRequest(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/respond-invitation")
    Object respondInvitation(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/invite")
    Object sendInvitation(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/add-current-user-friend")
    Object sendRequest(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @s98("users/me/sleep-settings")
    Object setSleepSetting(@i98 gj4 gj4, qn7<? super q88<MFSleepSettings>> qn7);

    @DexIgnore
    @u98("/v2/users/me/social-profile")
    Object socialId(@i98 gj4 gj4, qn7<? super q88<it4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/challenges/start-challenge")
    Object startChallenge(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/unfriend-current-user")
    Object unFriend(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @t98("/v2/rpc/social/friend/unblock-user")
    Object unblock(@i98 gj4 gj4, qn7<? super q88<gj4>> qn7);

    @DexIgnore
    @s98("users/me/activity-settings")
    Object updateActivitySetting(@i98 gj4 gj4, qn7<? super q88<ActivitySettings>> qn7);

    @DexIgnore
    @s98("users/me/profile")
    Object updateUser(@i98 gj4 gj4, qn7<? super q88<UserWrapper>> qn7);

    @DexIgnore
    @s98("users/me/settings")
    Object updateUserSetting(@i98 gj4 gj4, qn7<? super q88<UserSettings>> qn7);

    @DexIgnore
    @s98("users/me/workout-sessions")
    Object updateWorkoutSessions(@i98 gj4 gj4, qn7<? super q88<ApiResponse<ServerWorkoutSession>>> qn7);

    @DexIgnore
    @s98("users/me/alarms")
    Object upsertAlarms(@i98 gj4 gj4, qn7<? super q88<ApiResponse<Alarm>>> qn7);

    @DexIgnore
    @s98("/v2/users/me/diana-photos")
    Object upsertBackgroundPhotos(@i98 gj4 gj4, qn7<? super q88<ApiResponse<g97>>> qn7);

    @DexIgnore
    @s98("users/me/diana-presets")
    Object upsertDianaPresetList(@i98 gj4 gj4, qn7<? super q88<ApiResponse<DianaPreset>>> qn7);

    @DexIgnore
    @s98("users/me/goal-settings")
    Object upsertGoalSetting(@i98 gj4 gj4, qn7<? super q88<GoalSetting>> qn7);

    @DexIgnore
    @s98("users/me/hybrid-presets")
    Object upsertHybridPresetList(@i98 gj4 gj4, qn7<? super q88<ApiResponse<HybridPreset>>> qn7);

    @DexIgnore
    @u98("/v2/users/me/workout-settings")
    Object upsertWorkoutSetting(@i98 gj4 gj4, qn7<? super q88<RemoteWorkoutSetting>> qn7);
}
