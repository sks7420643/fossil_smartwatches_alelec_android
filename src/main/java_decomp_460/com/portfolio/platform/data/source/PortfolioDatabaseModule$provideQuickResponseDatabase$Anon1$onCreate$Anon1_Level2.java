package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.hm7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.um5;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.PortfolioDatabaseModule$provideQuickResponseDatabase$1$onCreate$1", f = "PortfolioDatabaseModule.kt", l = {}, m = "invokeSuspend")
public final class PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2(PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1 portfolioDatabaseModule$provideQuickResponseDatabase$Anon1, qn7 qn7) {
        super(2, qn7);
        this.this$0 = portfolioDatabaseModule$provideQuickResponseDatabase$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 = new PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2(this.this$0, qn7);
        portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2.p$ = (iv7) obj;
        return portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        QuickResponseMessageDao quickResponseMessageDao;
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            String c = um5.c(PortfolioApp.h0.c(), 2131886131);
            pq7.b(c, "LanguageHelper.getString\u2026an_Text__CanICallYouBack)");
            QuickResponseMessage quickResponseMessage = new QuickResponseMessage(c);
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886132);
            pq7.b(c2, "LanguageHelper.getString\u2026_MessageOn_Text__OnMyWay)");
            QuickResponseMessage quickResponseMessage2 = new QuickResponseMessage(c2);
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886133);
            pq7.b(c3, "LanguageHelper.getString\u2026y_Text__SorryCantTalkNow)");
            List<QuickResponseMessage> h = hm7.h(quickResponseMessage, quickResponseMessage2, new QuickResponseMessage(c3));
            T t = this.this$0.$dbInstance.element;
            if (!(t == null || (quickResponseMessageDao = t.quickResponseMessageDao()) == null)) {
                quickResponseMessageDao.insertResponses(h);
            }
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
