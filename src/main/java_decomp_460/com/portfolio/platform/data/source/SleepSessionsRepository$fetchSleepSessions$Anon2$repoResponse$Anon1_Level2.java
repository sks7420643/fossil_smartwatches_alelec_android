package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$2$repoResponse$1", f = "SleepSessionsRepository.kt", l = {DateTimeConstants.HOURS_PER_WEEK}, m = "invokeSuspend")
public final class SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2 extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository$fetchSleepSessions$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2(SleepSessionsRepository$fetchSleepSessions$Anon2 sleepSessionsRepository$fetchSleepSessions$Anon2, qn7 qn7) {
        super(1, qn7);
        this.this$0 = sleepSessionsRepository$fetchSleepSessions$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2(this.this$0, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<gj4>> qn7) {
        return ((SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiService;
            String k = lk5.k(this.this$0.$start);
            pq7.b(k, "DateHelper.formatShortDate(start)");
            String k2 = lk5.k(this.this$0.$end);
            pq7.b(k2, "DateHelper.formatShortDate(end)");
            SleepSessionsRepository$fetchSleepSessions$Anon2 sleepSessionsRepository$fetchSleepSessions$Anon2 = this.this$0;
            int i2 = sleepSessionsRepository$fetchSleepSessions$Anon2.$offset;
            int i3 = sleepSessionsRepository$fetchSleepSessions$Anon2.$limit;
            this.label = 1;
            Object sleepSessions = apiServiceV2.getSleepSessions(k, k2, i2, i3, this);
            return sleepSessions == d ? d : sleepSessions;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
