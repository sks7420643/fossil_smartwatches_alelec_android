package com.portfolio.platform.data.source;

import com.fossil.lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDeviceDaoFactory implements Factory<DeviceDao> {
    @DexIgnore
    public /* final */ Provider<DeviceDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDeviceDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDeviceDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideDeviceDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DeviceDao provideDeviceDao(PortfolioDatabaseModule portfolioDatabaseModule, DeviceDatabase deviceDatabase) {
        DeviceDao provideDeviceDao = portfolioDatabaseModule.provideDeviceDao(deviceDatabase);
        lk7.c(provideDeviceDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideDeviceDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DeviceDao get() {
        return provideDeviceDao(this.module, this.dbProvider.get());
    }
}
