package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDatabase_Impl extends WorkoutSettingDatabase {
    @DexIgnore
    public volatile WorkoutSettingDao _workoutSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `workoutSetting` (`type` TEXT NOT NULL, `mode` TEXT, `enable` INTEGER NOT NULL, `askMeFirst` INTEGER NOT NULL, `startLatency` INTEGER NOT NULL, `pauseLatency` INTEGER NOT NULL, `resumeLatency` INTEGER NOT NULL, `stopLatency` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`type`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '8ebd946c2b78fdb442012d32eb1b6201')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `workoutSetting`");
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            WorkoutSettingDatabase_Impl.this.mDatabase = lx0;
            WorkoutSettingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("type", new ix0.a("type", "TEXT", true, 1, null, 1));
            hashMap.put("mode", new ix0.a("mode", "TEXT", false, 0, null, 1));
            hashMap.put("enable", new ix0.a("enable", "INTEGER", true, 0, null, 1));
            hashMap.put("askMeFirst", new ix0.a("askMeFirst", "INTEGER", true, 0, null, 1));
            hashMap.put("startLatency", new ix0.a("startLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("pauseLatency", new ix0.a("pauseLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("resumeLatency", new ix0.a("resumeLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("stopLatency", new ix0.a("stopLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("workoutSetting", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "workoutSetting");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "workoutSetting(com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `workoutSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "workoutSetting");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(2), "8ebd946c2b78fdb442012d32eb1b6201", "2baade49018ab002e0858caff419b24c");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase
    public WorkoutSettingDao workoutSettingDao() {
        WorkoutSettingDao workoutSettingDao;
        if (this._workoutSettingDao != null) {
            return this._workoutSettingDao;
        }
        synchronized (this) {
            if (this._workoutSettingDao == null) {
                this._workoutSettingDao = new WorkoutSettingDao_Impl(this);
            }
            workoutSettingDao = this._workoutSettingDao;
        }
        return workoutSettingDao;
    }
}
