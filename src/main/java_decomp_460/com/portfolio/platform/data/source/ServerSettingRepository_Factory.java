package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository_Factory implements Factory<ServerSettingRepository> {
    @DexIgnore
    public /* final */ Provider<ServerSettingDataSource> mServerSettingLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<ServerSettingDataSource> mServerSettingRemoteDataSourceProvider;

    @DexIgnore
    public ServerSettingRepository_Factory(Provider<ServerSettingDataSource> provider, Provider<ServerSettingDataSource> provider2) {
        this.mServerSettingLocalDataSourceProvider = provider;
        this.mServerSettingRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static ServerSettingRepository_Factory create(Provider<ServerSettingDataSource> provider, Provider<ServerSettingDataSource> provider2) {
        return new ServerSettingRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static ServerSettingRepository newInstance(ServerSettingDataSource serverSettingDataSource, ServerSettingDataSource serverSettingDataSource2) {
        return new ServerSettingRepository(serverSettingDataSource, serverSettingDataSource2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ServerSettingRepository get() {
        return newInstance(this.mServerSettingLocalDataSourceProvider.get(), this.mServerSettingRemoteDataSourceProvider.get());
    }
}
