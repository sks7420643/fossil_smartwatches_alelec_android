package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UASampleDao_Impl implements UASampleDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<UASample> __deletionAdapterOfUASample;
    @DexIgnore
    public /* final */ jw0<UASample> __insertionAdapterOfUASample;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<UASample> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, UASample uASample) {
            px0.bindLong(1, (long) uASample.getId());
            px0.bindLong(2, (long) uASample.getStep());
            px0.bindDouble(3, uASample.getDistance());
            px0.bindDouble(4, uASample.getCalorie());
            px0.bindLong(5, uASample.getTime());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `uaSample` (`id`,`step`,`distance`,`calorie`,`time`) VALUES (nullif(?, 0),?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<UASample> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, UASample uASample) {
            px0.bindLong(1, (long) uASample.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `uaSample` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM uaSample";
        }
    }

    @DexIgnore
    public UASampleDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfUASample = new Anon1(qw0);
        this.__deletionAdapterOfUASample = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.UASampleDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.UASampleDao
    public void deleteListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfUASample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.UASampleDao
    public List<UASample> getAllUASample() {
        tw0 f = tw0.f("SELECT * FROM uaSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "step");
            int c3 = dx0.c(b, "distance");
            int c4 = dx0.c(b, "calorie");
            int c5 = dx0.c(b, LogBuilder.KEY_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                UASample uASample = new UASample(b.getInt(c2), b.getDouble(c3), b.getDouble(c4), b.getLong(c5));
                uASample.setId(b.getInt(c));
                arrayList.add(uASample);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.UASampleDao
    public void insertListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.UASampleDao
    public void insertUASample(UASample uASample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert((jw0<UASample>) uASample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
