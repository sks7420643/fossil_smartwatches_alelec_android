package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.gj4;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$loadSummaries$2", f = "SummariesRepository.kt", l = {Action.Presenter.PREVIOUS, Action.Presenter.BLACKOUT}, m = "invokeSuspend")
public final class SummariesRepository$loadSummaries$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<gj4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends TypeToken<ApiResponse<FitnessDayData>> {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$loadSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$loadSummaries$Anon2 summariesRepository$loadSummaries$Anon2 = new SummariesRepository$loadSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, qn7);
        summariesRepository$loadSummaries$Anon2.p$ = (iv7) obj;
        return summariesRepository$loadSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<gj4>> qn7) {
        return ((SummariesRepository$loadSummaries$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011b  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
        // Method dump skipped, instructions count: 476
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
