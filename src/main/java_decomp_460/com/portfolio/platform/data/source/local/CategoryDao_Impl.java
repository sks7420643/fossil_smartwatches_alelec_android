package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDao_Impl implements CategoryDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<Category> __insertionAdapterOfCategory;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Category> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Category category) {
            if (category.getId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, category.getId());
            }
            if (category.getEnglishName() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, category.getEnglishName());
            }
            if (category.getName() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, category.getName());
            }
            if (category.getUpdatedAt() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, category.getUpdatedAt());
            }
            if (category.getCreatedAt() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, category.getCreatedAt());
            }
            px0.bindLong(6, (long) category.getPriority());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `category` (`id`,`englishName`,`name`,`updatedAt`,`createdAt`,`priority`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM category";
        }
    }

    @DexIgnore
    public CategoryDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfCategory = new Anon1(qw0);
        this.__preparedStmtOfClearData = new Anon2(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public List<Category> getAllCategory() {
        tw0 f = tw0.f("SELECT * FROM category ORDER BY priority DESC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "englishName");
            int c3 = dx0.c(b, "name");
            int c4 = dx0.c(b, "updatedAt");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "priority");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new Category(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public Category getCategoryById(String str) {
        Category category = null;
        tw0 f = tw0.f("SELECT * FROM category WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "englishName");
            int c3 = dx0.c(b, "name");
            int c4 = dx0.c(b, "updatedAt");
            int c5 = dx0.c(b, "createdAt");
            int c6 = dx0.c(b, "priority");
            if (b.moveToFirst()) {
                category = new Category(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6));
            }
            return category;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void upsertCategoryList(List<Category> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCategory.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
