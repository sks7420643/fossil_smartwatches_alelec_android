package com.portfolio.platform.data.source.remote;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource", f = "DianaWatchFaceRemoteDataSource.kt", l = {32}, m = "createWatchFace")
public final class DianaWatchFaceRemoteDataSource$createWatchFace$Anon1 extends co7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRemoteDataSource$createWatchFace$Anon1(DianaWatchFaceRemoteDataSource dianaWatchFaceRemoteDataSource, qn7 qn7) {
        super(qn7);
        this.this$0 = dianaWatchFaceRemoteDataSource;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.createWatchFace(null, this);
    }
}
