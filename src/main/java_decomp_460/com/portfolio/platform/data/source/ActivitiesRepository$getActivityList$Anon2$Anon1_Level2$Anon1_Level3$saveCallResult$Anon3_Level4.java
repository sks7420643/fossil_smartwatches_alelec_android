package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$2$1$1$saveCallResult$3", f = "ActivitiesRepository.kt", l = {}, m = "invokeSuspend")
public final class ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $activityList;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4(ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, List list, qn7 qn7) {
        super(2, qn7);
        this.this$0 = anon1_Level3;
        this.$activityList = list;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4 activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4 = new ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4(this.this$0, this.$activityList, qn7);
        activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4.p$ = (iv7) obj;
        return activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            this.this$0.this$0.$fitnessDatabase.activitySampleDao().upsertListActivitySample(this.$activityList);
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
