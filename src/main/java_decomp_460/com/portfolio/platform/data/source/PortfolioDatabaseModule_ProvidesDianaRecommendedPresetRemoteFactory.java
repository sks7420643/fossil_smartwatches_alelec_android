package com.portfolio.platform.data.source;

import com.fossil.lk7;
import com.fossil.yo5;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory implements Factory<yo5> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static yo5 providesDianaRecommendedPresetRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        yo5 providesDianaRecommendedPresetRemote = portfolioDatabaseModule.providesDianaRecommendedPresetRemote(apiServiceV2);
        lk7.c(providesDianaRecommendedPresetRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesDianaRecommendedPresetRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public yo5 get() {
        return providesDianaRecommendedPresetRemote(this.module, this.apiProvider.get());
    }
}
