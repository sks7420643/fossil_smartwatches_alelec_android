package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.NotificationLocalDataSource;
import com.portfolio.platform.data.source.remote.NotificationRemoteDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationsRepositoryModule {
    @DexIgnore
    @Local
    public NotificationsDataSource provideLocalNotificationsDataSource() {
        return new NotificationLocalDataSource();
    }

    @DexIgnore
    @Remote
    public NotificationsDataSource provideRemoteNotificationsDataSource() {
        return new NotificationRemoteDataSource();
    }
}
