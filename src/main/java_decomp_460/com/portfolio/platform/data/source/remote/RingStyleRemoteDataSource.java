package com.portfolio.platform.data.source.remote;

import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRemoteDataSource(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "mService");
        this.mService = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRingStyleList(java.lang.String r9, boolean r10, com.fossil.qn7<? super com.fossil.iq5<java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle>>> r11) {
        /*
        // Method dump skipped, instructions count: 242
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource.getRingStyleList(java.lang.String, boolean, com.fossil.qn7):java.lang.Object");
    }
}
