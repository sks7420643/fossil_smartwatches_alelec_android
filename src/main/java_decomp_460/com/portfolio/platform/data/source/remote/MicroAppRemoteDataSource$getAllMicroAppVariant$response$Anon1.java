package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1", f = "MicroAppRemoteDataSource.kt", l = {40}, m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 extends ko7 implements rp7<qn7<? super q88<ApiResponse<MicroAppVariant>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $majorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $minorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, String str, String str2, String str3, qn7 qn7) {
        super(1, qn7);
        this.this$0 = microAppRemoteDataSource;
        this.$serial = str;
        this.$majorNumber = str2;
        this.$minorNumber = str3;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(qn7<?> qn7) {
        pq7.c(qn7, "completion");
        return new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this.this$0, this.$serial, this.$majorNumber, this.$minorNumber, qn7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public final Object invoke(qn7<? super q88<ApiResponse<MicroAppVariant>>> qn7) {
        return ((MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1) create(qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$serial;
            String str2 = this.$majorNumber;
            String str3 = this.$minorNumber;
            this.label = 1;
            Object allMicroAppVariant = apiServiceV2.getAllMicroAppVariant(str, str2, str3, this);
            return allMicroAppVariant == d ? d : allMicroAppVariant;
        } else if (i == 1) {
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
