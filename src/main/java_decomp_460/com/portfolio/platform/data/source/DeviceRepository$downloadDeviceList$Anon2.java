package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.nk5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2", f = "DeviceRepository.kt", l = {106}, m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ iq5 $response;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$Anon2(DeviceRepository deviceRepository, iq5 iq5, qn7 qn7) {
        super(2, qn7);
        this.this$0 = deviceRepository;
        this.$response = iq5;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(this.this$0, this.$response, qn7);
        deviceRepository$downloadDeviceList$Anon2.p$ = (iv7) obj;
        return deviceRepository$downloadDeviceList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((DeviceRepository$downloadDeviceList$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        List<Device> list;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ApiResponse apiResponse = (ApiResponse) ((kq5) this.$response).a();
            list = apiResponse != null ? apiResponse.get_items() : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = DeviceRepository.Companion.getTAG();
            local.d(tag, "downloadDeviceList() - isFromCache = false " + list);
            DeviceRepository deviceRepository = this.this$0;
            this.L$0 = iv7;
            this.L$1 = list;
            this.label = 1;
            if (deviceRepository.removeStealDevice(list, this) == d) {
                return d;
            }
        } else if (i == 1) {
            list = (List) this.L$1;
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (list != null) {
            for (Device device : list) {
                if (nk5.o.v(device.getDeviceId())) {
                    Device deviceByDeviceId = this.this$0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                    if (deviceByDeviceId != null) {
                        if (device.getBatteryLevel() <= 0) {
                            device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                        }
                        device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = DeviceRepository.Companion.getTAG();
                    local2.d(tag2, "update device: " + device);
                    this.this$0.mDeviceDao.addOrUpdateDevice(device);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tag3 = DeviceRepository.Companion.getTAG();
                    local3.d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                }
            }
            return tl7.f3441a;
        }
        pq7.i();
        throw null;
    }
}
