package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitHeartRateDao_Impl implements GFitHeartRateDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<GFitHeartRate> __deletionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ jw0<GFitHeartRate> __insertionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GFitHeartRate> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitHeartRate gFitHeartRate) {
            px0.bindLong(1, (long) gFitHeartRate.getId());
            px0.bindDouble(2, (double) gFitHeartRate.getValue());
            px0.bindLong(3, gFitHeartRate.getStartTime());
            px0.bindLong(4, gFitHeartRate.getEndTime());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitHeartRate` (`id`,`value`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<GFitHeartRate> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitHeartRate gFitHeartRate) {
            px0.bindLong(1, (long) gFitHeartRate.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `gFitHeartRate` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM gFitHeartRate";
        }
    }

    @DexIgnore
    public GFitHeartRateDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGFitHeartRate = new Anon1(qw0);
        this.__deletionAdapterOfGFitHeartRate = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void deleteListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitHeartRate.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public List<GFitHeartRate> getAllGFitHeartRate() {
        tw0 f = tw0.f("SELECT * FROM gFitHeartRate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "value");
            int c3 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c4 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitHeartRate gFitHeartRate = new GFitHeartRate(b.getFloat(c2), b.getLong(c3), b.getLong(c4));
                gFitHeartRate.setId(b.getInt(c));
                arrayList.add(gFitHeartRate);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertGFitHeartRate(GFitHeartRate gFitHeartRate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert((jw0<GFitHeartRate>) gFitHeartRate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
