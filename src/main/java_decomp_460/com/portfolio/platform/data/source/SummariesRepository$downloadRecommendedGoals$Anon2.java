package com.portfolio.platform.data.source;

import com.fossil.al7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$2", f = "SummariesRepository.kt", l = {388}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$Anon2(SummariesRepository summariesRepository, int i, int i2, int i3, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = summariesRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2 = new SummariesRepository$downloadRecommendedGoals$Anon2(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, qn7);
        summariesRepository$downloadRecommendedGoals$Anon2.p$ = (iv7) obj;
        return summariesRepository$downloadRecommendedGoals$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<ActivityRecommendedGoals>> qn7) {
        return ((SummariesRepository$downloadRecommendedGoals$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Object d2 = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 = new SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = iv7;
            this.label = 1;
            d = jq5.d(summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            d = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        iq5 iq5 = (iq5) d;
        if (iq5 instanceof kq5) {
            Object a2 = ((kq5) iq5).a();
            if (a2 != null) {
                return new kq5((ActivityRecommendedGoals) a2, false, 2, null);
            }
            pq7.i();
            throw null;
        } else if (iq5 instanceof hq5) {
            hq5 hq5 = (hq5) iq5;
            return new hq5(hq5.a(), hq5.c(), null, null, null, 16, null);
        } else {
            throw new al7();
        }
    }
}
