package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSettingDao_Impl implements WatchAppLastSettingDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<WatchAppLastSetting> __insertionAdapterOfWatchAppLastSetting;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteWatchAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<WatchAppLastSetting> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, WatchAppLastSetting watchAppLastSetting) {
            if (watchAppLastSetting.getWatchAppId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, watchAppLastSetting.getWatchAppId());
            }
            if (watchAppLastSetting.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, watchAppLastSetting.getUpdatedAt());
            }
            if (watchAppLastSetting.getSetting() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, watchAppLastSetting.getSetting());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppLastSetting` (`watchAppId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xw0 {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting WHERE watchAppId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WatchAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchAppLastSetting> call() throws Exception {
            Cursor b = ex0.b(WatchAppLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "watchAppId");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WatchAppLastSettingDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfWatchAppLastSetting = new Anon1(qw0);
        this.__preparedStmtOfDeleteWatchAppLastSettingById = new Anon2(qw0);
        this.__preparedStmtOfCleanUp = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void deleteWatchAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteWatchAppLastSettingById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public List<WatchAppLastSetting> getAllWatchAppLastSetting() {
        tw0 f = tw0.f("SELECT * FROM watchAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "watchAppId");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData() {
        tw0 f = tw0.f("SELECT * FROM watchAppLastSetting", 0);
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"watchAppLastSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public WatchAppLastSetting getWatchAppLastSetting(String str) {
        WatchAppLastSetting watchAppLastSetting = null;
        tw0 f = tw0.f("SELECT * FROM watchAppLastSetting WHERE watchAppId=? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "watchAppId");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                watchAppLastSetting = new WatchAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3));
            }
            return watchAppLastSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppLastSetting.insert((jw0<WatchAppLastSetting>) watchAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
