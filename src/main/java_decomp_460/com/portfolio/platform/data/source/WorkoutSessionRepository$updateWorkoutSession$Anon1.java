package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository", f = "WorkoutSessionRepository.kt", l = {178, 187}, m = "updateWorkoutSession")
public final class WorkoutSessionRepository$updateWorkoutSession$Anon1 extends co7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$updateWorkoutSession$Anon1(WorkoutSessionRepository workoutSessionRepository, qn7 qn7) {
        super(qn7);
        this.this$0 = workoutSessionRepository;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.updateWorkoutSession(null, this);
    }
}
