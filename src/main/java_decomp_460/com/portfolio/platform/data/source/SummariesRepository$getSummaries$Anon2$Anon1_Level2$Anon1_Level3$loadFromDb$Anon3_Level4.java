package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bs7;
import com.fossil.gi0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4<I, O> implements gi0<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ LiveData $resultList;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    public SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4(SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, LiveData liveData) {
        this.this$0 = anon1_Level3;
        this.$resultList = liveData;
    }

    @DexIgnore
    public final List<ActivitySummary> apply(List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday - resultList=" + ((List) this.$resultList.e()));
        pq7.b(list, "activitySummaries");
        if (!list.isEmpty()) {
            ((ActivitySummary) pm7.P(list)).setSteps(bs7.b((double) this.this$0.this$0.this$0.this$0.mFitnessHelper.d(new Date()), ((ActivitySummary) pm7.P(list)).getSteps()));
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(SummariesRepository.TAG, "XXX- getSummary - onDataChange -- endDate=" + this.this$0.this$0.this$0.$endDate + ", activitySummaries=" + list);
        return list;
    }
}
