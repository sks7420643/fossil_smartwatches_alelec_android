package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.source.ActivitiesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.ActivitiesRepository$pushPendingActivities$2", f = "ActivitiesRepository.kt", l = {188, 191}, m = "invokeSuspend")
public final class ActivitiesRepository$pushPendingActivities$Anon2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository.PushPendingActivitiesCallback $pushPendingActivitiesCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$pushPendingActivities$Anon2(ActivitiesRepository activitiesRepository, ActivitiesRepository.PushPendingActivitiesCallback pushPendingActivitiesCallback, qn7 qn7) {
        super(2, qn7);
        this.this$0 = activitiesRepository;
        this.$pushPendingActivitiesCallback = pushPendingActivitiesCallback;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ActivitiesRepository$pushPendingActivities$Anon2 activitiesRepository$pushPendingActivities$Anon2 = new ActivitiesRepository$pushPendingActivities$Anon2(this.this$0, this.$pushPendingActivitiesCallback, qn7);
        activitiesRepository$pushPendingActivities$Anon2.p$ = (iv7) obj;
        return activitiesRepository$pushPendingActivities$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ActivitiesRepository$pushPendingActivities$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 2
            r2 = 1
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0050
            if (r0 == r2) goto L_0x0024
            if (r0 != r5) goto L_0x001c
            java.lang.Object r0 = r6.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r6.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r7)
        L_0x0019:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x001b:
            return r0
        L_0x001c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            java.lang.Object r0 = r6.L$0
            com.fossil.iv7 r0 = (com.fossil.iv7) r0
            com.fossil.el7.b(r7)
            r2 = r0
            r1 = r7
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.fitness.SampleRawDao r0 = r0.sampleRawDao()
            java.util.List r0 = r0.getPendingActivitySamples()
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x0063
            com.portfolio.platform.data.source.ActivitiesRepository r1 = r6.this$0
            com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback r4 = r6.$pushPendingActivitiesCallback
            r6.L$0 = r2
            r6.L$1 = r0
            r6.label = r5
            java.lang.Object r0 = r1.saveActivitiesToServer(r0, r4, r6)
            if (r0 != r3) goto L_0x0019
            r0 = r3
            goto L_0x001b
        L_0x0050:
            com.fossil.el7.b(r7)
            com.fossil.iv7 r0 = r6.p$
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r6.L$0 = r0
            r6.label = r2
            java.lang.Object r1 = r1.y(r6)
            if (r1 != r3) goto L_0x0071
            r0 = r3
            goto L_0x001b
        L_0x0063:
            com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback r0 = r6.$pushPendingActivitiesCallback
            if (r0 == 0) goto L_0x006f
            r1 = 404(0x194, float:5.66E-43)
            r0.onFail(r1)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x001b
        L_0x006f:
            r0 = 0
            goto L_0x001b
        L_0x0071:
            r2 = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$pushPendingActivities$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
