package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.tz4;
import com.fossil.vz4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.xw0;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataDao_Impl extends FitnessDataDao {
    @DexIgnore
    public /* final */ tz4 __dateTimeUTCStringConverter; // = new tz4();
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<FitnessDataWrapper> __deletionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ vz4 __fitnessDataConverter; // = new vz4();
    @DexIgnore
    public /* final */ jw0<FitnessDataWrapper> __insertionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeleteAllFitnessData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<FitnessDataWrapper> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, FitnessDataWrapper fitnessDataWrapper) {
            String g = FitnessDataDao_Impl.this.__fitnessDataConverter.g(fitnessDataWrapper.step);
            if (g == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, g);
            }
            String a2 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.activeMinute);
            if (a2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, a2);
            }
            String b = FitnessDataDao_Impl.this.__fitnessDataConverter.b(fitnessDataWrapper.calorie);
            if (b == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, b);
            }
            String c = FitnessDataDao_Impl.this.__fitnessDataConverter.c(fitnessDataWrapper.distance);
            if (c == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, c);
            }
            String h = FitnessDataDao_Impl.this.__fitnessDataConverter.h(fitnessDataWrapper.getStress());
            if (h == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, h);
            }
            String e = FitnessDataDao_Impl.this.__fitnessDataConverter.e(fitnessDataWrapper.getResting());
            if (e == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, e);
            }
            String d = FitnessDataDao_Impl.this.__fitnessDataConverter.d(fitnessDataWrapper.getHeartRate());
            if (d == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, d);
            }
            String f = FitnessDataDao_Impl.this.__fitnessDataConverter.f(fitnessDataWrapper.getSleeps());
            if (f == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, f);
            }
            String r = FitnessDataDao_Impl.this.__fitnessDataConverter.r(fitnessDataWrapper.getWorkouts());
            if (r == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, r);
            }
            String a3 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a3 == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, a3);
            }
            String a4 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getEndTime());
            if (a4 == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, a4);
            }
            String a5 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getSyncTime());
            if (a5 == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, a5);
            }
            px0.bindLong(13, (long) fitnessDataWrapper.getTimezoneOffsetInSecond());
            if (fitnessDataWrapper.getSerialNumber() == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, fitnessDataWrapper.getSerialNumber());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR IGNORE INTO `fitness_data` (`step`,`activeMinute`,`calorie`,`distance`,`stress`,`resting`,`heartRate`,`sleeps`,`workouts`,`startTime`,`endTime`,`syncTime`,`timezoneOffsetInSecond`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<FitnessDataWrapper> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, FitnessDataWrapper fitnessDataWrapper) {
            String a2 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a2 == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, a2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `fitness_data` WHERE `startTime` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM fitness_data";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<FitnessDataWrapper>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon4(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<FitnessDataWrapper> call() throws Exception {
            Cursor b = ex0.b(FitnessDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "step");
                int c2 = dx0.c(b, "activeMinute");
                int c3 = dx0.c(b, "calorie");
                int c4 = dx0.c(b, "distance");
                int c5 = dx0.c(b, "stress");
                int c6 = dx0.c(b, "resting");
                int c7 = dx0.c(b, "heartRate");
                int c8 = dx0.c(b, "sleeps");
                int c9 = dx0.c(b, "workouts");
                int c10 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c11 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c12 = dx0.c(b, "syncTime");
                int c13 = dx0.c(b, "timezoneOffsetInSecond");
                int c14 = dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c10)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c11)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = FitnessDataDao_Impl.this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = FitnessDataDao_Impl.this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = FitnessDataDao_Impl.this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = FitnessDataDao_Impl.this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(FitnessDataDao_Impl.this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(FitnessDataDao_Impl.this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(FitnessDataDao_Impl.this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(FitnessDataDao_Impl.this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(FitnessDataDao_Impl.this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public FitnessDataDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfFitnessDataWrapper = new Anon1(qw0);
        this.__deletionAdapterOfFitnessDataWrapper = new Anon2(qw0);
        this.__preparedStmtOfDeleteAllFitnessData = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteAllFitnessData() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeleteAllFitnessData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllFitnessData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteFitnessData(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfFitnessDataWrapper.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2) {
        tw0 f = tw0.f("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "step");
            int c2 = dx0.c(b, "activeMinute");
            int c3 = dx0.c(b, "calorie");
            int c4 = dx0.c(b, "distance");
            int c5 = dx0.c(b, "stress");
            int c6 = dx0.c(b, "resting");
            int c7 = dx0.c(b, "heartRate");
            int c8 = dx0.c(b, "sleeps");
            int c9 = dx0.c(b, "workouts");
            int c10 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c11 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c12 = dx0.c(b, "syncTime");
            int c13 = dx0.c(b, "timezoneOffsetInSecond");
            try {
                int c14 = dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.b(b.getString(c10)), this.__dateTimeUTCStringConverter.b(b.getString(c11)), this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2) {
        tw0 f = tw0.f("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"fitness_data"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getPendingFitnessData() {
        tw0 f = tw0.f("SELECT * FROM fitness_data ORDER BY startTime ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "step");
            int c2 = dx0.c(b, "activeMinute");
            int c3 = dx0.c(b, "calorie");
            int c4 = dx0.c(b, "distance");
            int c5 = dx0.c(b, "stress");
            int c6 = dx0.c(b, "resting");
            int c7 = dx0.c(b, "heartRate");
            int c8 = dx0.c(b, "sleeps");
            int c9 = dx0.c(b, "workouts");
            int c10 = dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c11 = dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c12 = dx0.c(b, "syncTime");
            int c13 = dx0.c(b, "timezoneOffsetInSecond");
            try {
                int c14 = dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.b(b.getString(c10)), this.__dateTimeUTCStringConverter.b(b.getString(c11)), this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void insertFitnessDataList(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFitnessDataWrapper.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
