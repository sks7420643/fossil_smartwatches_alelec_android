package com.portfolio.platform.data.source;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qk5;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.w18;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1", f = "WatchLocalizationRepository.kt", l = {65}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1 extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, String str2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1(this.this$0, this.$url, this.$path, qn7);
        watchLocalizationRepository$processDownloadAndStore$Anon1.p$ = (iv7) obj;
        return watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Object d2 = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 = new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(this, null);
            this.L$0 = iv7;
            this.label = 1;
            d = jq5.d(watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            d = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        iq5 iq5 = (iq5) d;
        if (iq5 instanceof kq5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.this$0.TAG;
            local.d(str, "start storing for url: " + this.$url + " to path: " + this.$path);
            qk5 qk5 = qk5.f2994a;
            Object a2 = ((kq5) iq5).a();
            if (a2 == null) {
                pq7.i();
                throw null;
            } else if (qk5.g((w18) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.this$0.TAG;
            local2.d(str2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
