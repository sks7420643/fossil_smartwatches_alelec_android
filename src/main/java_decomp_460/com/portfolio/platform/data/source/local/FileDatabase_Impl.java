package com.portfolio.platform.data.source.local;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase_Impl extends FileDatabase {
    @DexIgnore
    public volatile FileDao _fileDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `localfile` (`pinType` INTEGER NOT NULL, `type` TEXT NOT NULL, `fileName` TEXT NOT NULL, `localUri` TEXT NOT NULL, `remoteUrl` TEXT NOT NULL, `checksum` TEXT, PRIMARY KEY(`remoteUrl`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '415513f336004021a41b46790e07d643')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `localfile`");
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FileDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FileDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            FileDatabase_Impl.this.mDatabase = lx0;
            FileDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) FileDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("pinType", new ix0.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("type", new ix0.a("type", "TEXT", true, 0, null, 1));
            hashMap.put("fileName", new ix0.a("fileName", "TEXT", true, 0, null, 1));
            hashMap.put("localUri", new ix0.a("localUri", "TEXT", true, 0, null, 1));
            hashMap.put("remoteUrl", new ix0.a("remoteUrl", "TEXT", true, 1, null, 1));
            hashMap.put("checksum", new ix0.a("checksum", "TEXT", false, 0, null, 1));
            ix0 ix0 = new ix0("localfile", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "localfile");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "localfile(com.portfolio.platform.data.model.LocalFile).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `localfile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "localfile");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(2), "415513f336004021a41b46790e07d643", "f5892aa2843b6abf4eab005d74132c1f");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDatabase
    public FileDao fileDao() {
        FileDao fileDao;
        if (this._fileDao != null) {
            return this._fileDao;
        }
        synchronized (this) {
            if (this._fileDao == null) {
                this._fileDao = new FileDao_Impl(this);
            }
            fileDao = this._fileDao;
        }
        return fileDao;
    }
}
