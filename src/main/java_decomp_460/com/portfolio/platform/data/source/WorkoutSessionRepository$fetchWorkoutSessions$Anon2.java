package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$2", f = "WorkoutSessionRepository.kt", l = {204, 215, 224}, m = "invokeSuspend")
public final class WorkoutSessionRepository$fetchWorkoutSessions$Anon2 extends ko7 implements vp7<iv7, qn7<? super iq5<ApiResponse<ServerWorkoutSession>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$fetchWorkoutSessions$Anon2(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        WorkoutSessionRepository$fetchWorkoutSessions$Anon2 workoutSessionRepository$fetchWorkoutSessions$Anon2 = new WorkoutSessionRepository$fetchWorkoutSessions$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, qn7);
        workoutSessionRepository$fetchWorkoutSessions$Anon2.p$ = (iv7) obj;
        return workoutSessionRepository$fetchWorkoutSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super iq5<ApiResponse<ServerWorkoutSession>>> qn7) {
        return ((WorkoutSessionRepository$fetchWorkoutSessions$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v26, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01cc  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 555
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
