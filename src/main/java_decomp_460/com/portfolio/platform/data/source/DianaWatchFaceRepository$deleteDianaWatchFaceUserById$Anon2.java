package com.portfolio.platform.data.source;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.DianaWatchFaceRepository$deleteDianaWatchFaceUserById$2", f = "DianaWatchFaceRepository.kt", l = {56, 58, 65, 67}, m = "invokeSuspend")
public final class DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2 extends ko7 implements vp7<iv7, qn7<? super kq5<Object>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2(DianaWatchFaceRepository dianaWatchFaceRepository, String str, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dianaWatchFaceRepository;
        this.$id = str;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2 dianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2 = new DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2(this.this$0, this.$id, qn7);
        dianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2.p$ = (iv7) obj;
        return dianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super kq5<Object>> qn7) {
        return ((DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b9  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 308
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
