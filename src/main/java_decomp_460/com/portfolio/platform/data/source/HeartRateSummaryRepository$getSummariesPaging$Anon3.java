package com.portfolio.platform.data.source;

import com.fossil.gp7;
import com.fossil.qq7;
import com.fossil.tl7;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$getSummariesPaging$Anon3 extends qq7 implements gp7<tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getSummariesPaging$Anon3(HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = heartRateSummaryDataSourceFactory;
    }

    @DexIgnore
    @Override // com.fossil.gp7
    public final void invoke() {
        HeartRateSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
        if (e != null) {
            e.invalidate();
        }
    }
}
