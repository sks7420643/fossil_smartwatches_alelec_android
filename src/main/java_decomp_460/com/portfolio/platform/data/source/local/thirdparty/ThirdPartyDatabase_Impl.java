package com.portfolio.platform.data.source.local.thirdparty;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `gFitSample`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitActiveTime`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitHeartRate`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            lx0.execSQL("DROP TABLE IF EXISTS `uaSample`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitSleep`");
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            ThirdPartyDatabase_Impl.this.mDatabase = lx0;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("step", new ix0.a("step", "INTEGER", true, 0, null, 1));
            hashMap.put("distance", new ix0.a("distance", "REAL", true, 0, null, 1));
            hashMap.put("calorie", new ix0.a("calorie", "REAL", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "gFitSample");
            if (!ix0.equals(a2)) {
                return new sw0.b(false, "gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap2.put("activeTimes", new ix0.a("activeTimes", "TEXT", true, 0, null, 1));
            ix0 ix02 = new ix0("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
            ix0 a3 = ix0.a(lx0, "gFitActiveTime");
            if (!ix02.equals(a3)) {
                return new sw0.b(false, "gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("value", new ix0.a("value", "REAL", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ix0 ix03 = new ix0("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
            ix0 a4 = ix0.a(lx0, "gFitHeartRate");
            if (!ix03.equals(a4)) {
                return new sw0.b(false, "gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(8);
            hashMap4.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put("workoutType", new ix0.a("workoutType", "INTEGER", true, 0, null, 1));
            hashMap4.put("steps", new ix0.a("steps", "TEXT", true, 0, null, 1));
            hashMap4.put("calories", new ix0.a("calories", "TEXT", true, 0, null, 1));
            hashMap4.put("distances", new ix0.a("distances", "TEXT", true, 0, null, 1));
            hashMap4.put("heartRates", new ix0.a("heartRates", "TEXT", true, 0, null, 1));
            ix0 ix04 = new ix0("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
            ix0 a5 = ix0.a(lx0, "gFitWorkoutSession");
            if (!ix04.equals(a5)) {
                return new sw0.b(false, "gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(5);
            hashMap5.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("step", new ix0.a("step", "INTEGER", true, 0, null, 1));
            hashMap5.put("distance", new ix0.a("distance", "REAL", true, 0, null, 1));
            hashMap5.put("calorie", new ix0.a("calorie", "REAL", true, 0, null, 1));
            hashMap5.put(LogBuilder.KEY_TIME, new ix0.a(LogBuilder.KEY_TIME, "INTEGER", true, 0, null, 1));
            ix0 ix05 = new ix0("uaSample", hashMap5, new HashSet(0), new HashSet(0));
            ix0 a6 = ix0.a(lx0, "uaSample");
            if (!ix05.equals(a6)) {
                return new sw0.b(false, "uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap6.put("sleepMins", new ix0.a("sleepMins", "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_START_TIME, new ix0.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_END_TIME, new ix0.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ix0 ix06 = new ix0("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
            ix0 a7 = ix0.a(lx0, "gFitSleep");
            if (ix06.equals(a7)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `gFitSample`");
            writableDatabase.execSQL("DELETE FROM `gFitActiveTime`");
            writableDatabase.execSQL("DELETE FROM `gFitHeartRate`");
            writableDatabase.execSQL("DELETE FROM `gFitWorkoutSession`");
            writableDatabase.execSQL("DELETE FROM `uaSample`");
            writableDatabase.execSQL("DELETE FROM `gFitSleep`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
