package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.wz4;
import com.fossil.xw0;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTimeDao_Impl implements GFitActiveTimeDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<GFitActiveTime> __deletionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ wz4 __gFitActiveTimeConverter; // = new wz4();
    @DexIgnore
    public /* final */ jw0<GFitActiveTime> __insertionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<GFitActiveTime> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitActiveTime gFitActiveTime) {
            px0.bindLong(1, (long) gFitActiveTime.getId());
            String b = GFitActiveTimeDao_Impl.this.__gFitActiveTimeConverter.b(gFitActiveTime.getActiveTimes());
            if (b == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitActiveTime` (`id`,`activeTimes`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<GFitActiveTime> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, GFitActiveTime gFitActiveTime) {
            px0.bindLong(1, (long) gFitActiveTime.getId());
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `gFitActiveTime` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM gFitActiveTime";
        }
    }

    @DexIgnore
    public GFitActiveTimeDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfGFitActiveTime = new Anon1(qw0);
        this.__deletionAdapterOfGFitActiveTime = new Anon2(qw0);
        this.__preparedStmtOfClearAll = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void deleteGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitActiveTime.handle(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public List<GFitActiveTime> getAllGFitActiveTime() {
        tw0 f = tw0.f("SELECT * FROM gFitActiveTime", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "activeTimes");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitActiveTime gFitActiveTime = new GFitActiveTime(this.__gFitActiveTimeConverter.a(b.getString(c2)));
                gFitActiveTime.setId(b.getInt(c));
                arrayList.add(gFitActiveTime);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void insertGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert((jw0<GFitActiveTime>) gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void insertListGFitActiveTime(List<GFitActiveTime> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
