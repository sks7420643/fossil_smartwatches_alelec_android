package com.portfolio.platform.data.source;

import com.fossil.kq7;
import com.fossil.pq7;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WatchAppDataRepository";
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ WatchAppDataRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public WatchAppDataRepository(WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, FileRepository fileRepository) {
        pq7.c(watchAppDataRemoteDataSource, "mRemoteSource");
        pq7.c(fileRepository, "mFileRepository");
        this.mRemoteSource = watchAppDataRemoteDataSource;
        this.mFileRepository = fileRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.WatchAppDataRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.data.source.WatchAppDataRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppDataRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WatchAppDataRepository r0 = (com.portfolio.platform.data.source.WatchAppDataRepository) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.WatchAppDataDao r0 = r0.getWatchAppDataDao()
            r0.clearAll()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.WatchAppDataRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppDataRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.FileRepository r1 = r5.mFileRepository
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_APP
            r1.deletedFilesByType(r3)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppDataRepository.cleanUp(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v35, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x02e9  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadWatchAppData(java.lang.String r28, java.lang.String r29, com.fossil.qn7<? super java.lang.Boolean> r30) {
        /*
        // Method dump skipped, instructions count: 1077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppDataRepository.downloadWatchAppData(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
