package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.au0;
import com.fossil.bw7;
import com.fossil.cl7;
import com.fossil.fl5;
import com.fossil.gj4;
import com.fossil.gl5;
import com.fossil.gu7;
import com.fossil.hm7;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.jv7;
import com.fossil.kq5;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.no4;
import com.fossil.nw0;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource extends au0<Date, SleepSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ fl5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public fl5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ nw0.c mObserver;
    @DexIgnore
    public List<cl7<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends nw0.c {
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = sleepSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            pq7.c(set, "tables");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "XXX- sleep summary invalidate");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            pq7.c(date, "date");
            pq7.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = lk5.I(instance);
            if (lk5.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            pq7.b(time, "nextPagedKey.time");
            return time;
        }
    }

    @DexIgnore
    public SleepSummaryLocalDataSource(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, no4 no4, fl5.a aVar, Calendar calendar) {
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(sleepDatabase, "mSleepDatabase");
        pq7.c(date, "mCreatedDate");
        pq7.c(no4, "appExecutors");
        pq7.c(aVar, "listener");
        pq7.c(calendar, "key");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        fl5 fl5 = new fl5(no4.a());
        this.mHelper = fl5;
        this.mNetworkState = gl5.b(fl5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, MFSleepDay.TABLE_NAME, new String[]{MFSleepSession.TABLE_NAME});
        this.mSleepDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = lk5.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        pq7.b(time, "calendar.time");
        this.mStartDate = time;
        if (lk5.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<SleepSummary> calculateSummaries(List<SleepSummary> list) {
        int i;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "endCalendar");
            instance.setTime(((SleepSummary) pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = lk5.b0(instance.getTime());
                pq7.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                SleepDao sleepDao = this.mSleepDatabase.sleepDao();
                Date time = b0.getTime();
                pq7.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                pq7.b(time2, "endCalendar.time");
                i = sleepDao.getTotalSleep(time, time2);
            } else {
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            pq7.b(instance2, "calendar");
            instance2.setTime(((SleepSummary) pm7.F(list)).getDate());
            Calendar b02 = lk5.b0(instance2.getTime());
            pq7.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            Iterator<T> it = list.iterator();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            double d = 0.0d;
            while (true) {
                double d2 = d;
                if (it.hasNext()) {
                    T next = it.next();
                    if (i3 >= 0) {
                        T t = next;
                        if (lk5.m0(t.getDate(), b02.getTime())) {
                            com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = list.get(i2).getSleepDay();
                            if (sleepDay != null) {
                                if (i4 > 1) {
                                    d2 /= (double) i4;
                                }
                                sleepDay.setAverageSleepOfWeek(Double.valueOf(d2));
                            }
                            b02.add(5, -7);
                            i4 = 0;
                            d2 = 0.0d;
                            i2 = i3;
                        }
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay2 = t.getSleepDay();
                        double sleepMinutes = d2 + ((double) (sleepDay2 != null ? sleepDay2.getSleepMinutes() : 0));
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay3 = t.getSleepDay();
                        if ((sleepDay3 != null ? sleepDay3.getSleepMinutes() : 0) > 0) {
                            i4++;
                        }
                        d = i3 == list.size() + -1 ? ((double) i) + sleepMinutes : sleepMinutes;
                        i3++;
                    } else {
                        hm7.l();
                        throw null;
                    }
                } else {
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay4 = list.get(i2).getSleepDay();
                    if (sleepDay4 != null) {
                        if (i4 > 1) {
                            d2 /= (double) i4;
                        }
                        sleepDay4.setAverageSleepOfWeek(Double.valueOf(d2));
                    }
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
        return list;
    }

    @DexIgnore
    private final void combineData(fl5.d dVar, iq5<gj4> iq5, iq5<gj4> iq52, fl5.b.a aVar) {
        if (!(iq5 instanceof kq5) || !(iq52 instanceof kq5)) {
            String str = "";
            if (iq5 instanceof hq5) {
                hq5 hq5 = (hq5) iq5;
                if (hq5.d() != null) {
                    aVar.a(hq5.d());
                } else if (hq5.c() != null) {
                    ServerError c = hq5.c();
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = str;
                    }
                    aVar.a(new Throwable(userMessage));
                }
            } else if (iq52 instanceof hq5) {
                hq5 hq52 = (hq5) iq52;
                if (hq52.d() != null) {
                    aVar.a(hq52.d());
                } else if (hq52.c() != null) {
                    ServerError c2 = hq52.c();
                    String userMessage2 = c2.getUserMessage();
                    if (userMessage2 == null) {
                        userMessage2 = c2.getMessage();
                    }
                    if (userMessage2 != null) {
                        str = userMessage2;
                    }
                    aVar.a(new Throwable(str));
                }
            }
        } else {
            if (dVar == fl5.d.AFTER && (!this.mRequestAfterQueue.isEmpty())) {
                this.mRequestAfterQueue.remove(0);
            }
            aVar.b();
        }
    }

    @DexIgnore
    private final SleepSummary dummySummary(SleepSummary sleepSummary, Date date) {
        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = sleepSummary.getSleepDay();
        return new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date, sleepDay != null ? sleepDay.getGoalMinutes() : 0, 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null);
    }

    @DexIgnore
    private final List<SleepSummary> getDataInDatabase(Date date, Date date2) {
        SleepSummary sleepSummary;
        Date date3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<SleepSummary> calculateSummaries = calculateSummaries(this.mSleepDatabase.sleepDao().getSleepSummariesDesc(lk5.j0(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        com.portfolio.platform.data.model.room.sleep.MFSleepDay lastSleepDay = this.mSleepDatabase.sleepDao().getLastSleepDay();
        Date date4 = (lastSleepDay == null || (date3 = lastSleepDay.getDate()) == null) ? date : date3;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        SleepSummary sleepSummary2 = this.mSleepDatabase.sleepDao().getSleepSummary(date2);
        SleepSummary sleepSummary3 = sleepSummary2 == null ? new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date2, this.mSleepDatabase.sleepDao().getNearestSleepGoalFromDate(date2), 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null) : sleepSummary2;
        if (!lk5.j0(date, date4)) {
            date = date4;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary3 + ", lastDate=" + date4 + ", startDateToFill=" + date);
        while (lk5.k0(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    sleepSummary = null;
                    break;
                }
                Object next = it.next();
                if (lk5.m0(((SleepSummary) next).getDate(), date2)) {
                    sleepSummary = next;
                    break;
                }
            }
            SleepSummary sleepSummary4 = sleepSummary;
            if (sleepSummary4 == null) {
                arrayList.add(dummySummary(sleepSummary3, date2));
            } else {
                arrayList.add(sleepSummary4);
                arrayList2.remove(sleepSummary4);
            }
            date2 = lk5.P(date2);
            pq7.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            SleepSummary sleepSummary5 = (SleepSummary) pm7.F(arrayList);
            Boolean p0 = lk5.p0(sleepSummary5.getDate());
            pq7.b(p0, "DateHelper.isToday(todaySummary.getDate())");
            if (p0.booleanValue()) {
                arrayList.add(0, sleepSummary5.copy(sleepSummary5.getSleepDay(), sleepSummary5.getSleepSessions()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final xw7 loadData(fl5.d dVar, Date date, Date date2, fl5.b.a aVar) {
        return gu7.d(jv7.a(bw7.b()), null, null, new SleepSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final fl5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mSleepDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadAfter(au0.f<Date> fVar, au0.a<Date, SleepSummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.f326a));
        if (lk5.j0(fVar.f326a, this.mCreatedDate)) {
            Key key2 = fVar.f326a;
            pq7.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.f326a;
            pq7.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = lk5.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : lk5.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            pq7.b(O, "startQueryDate");
            aVar.a(getDataInDatabase(O, key3), this.key.getTime());
            if (lk5.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new cl7<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(fl5.d.AFTER, new SleepSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadBefore(au0.f<Date> fVar, au0.a<Date, SleepSummary> aVar) {
        pq7.c(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadInitial(au0.e<Date> eVar, au0.c<Date, SleepSummary> cVar) {
        pq7.c(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        pq7.c(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = lk5.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : lk5.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        pq7.b(O, "startQueryDate");
        cVar.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(fl5.d.INITIAL, new SleepSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mSleepDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(fl5 fl5) {
        pq7.c(fl5, "<set-?>");
        this.mHelper = fl5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        pq7.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        pq7.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
