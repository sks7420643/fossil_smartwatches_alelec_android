package com.portfolio.platform.data.source.local;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDatabase_Impl extends CategoryDatabase {
    @DexIgnore
    public volatile CategoryDao _categoryDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `category` (`id` TEXT NOT NULL, `englishName` TEXT NOT NULL, `name` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `priority` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'deca3c3756d2b01ba1167f57e573ed39')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `category`");
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            CategoryDatabase_Impl.this.mDatabase = lx0;
            CategoryDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new ix0.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("englishName", new ix0.a("englishName", "TEXT", true, 0, null, 1));
            hashMap.put("name", new ix0.a("name", "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap.put("priority", new ix0.a("priority", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("category", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "category");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "category(com.portfolio.platform.data.model.Category).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDatabase
    public CategoryDao categoryDao() {
        CategoryDao categoryDao;
        if (this._categoryDao != null) {
            return this._categoryDao;
        }
        synchronized (this) {
            if (this._categoryDao == null) {
                this._categoryDao = new CategoryDao_Impl(this);
            }
            categoryDao = this._categoryDao;
        }
        return categoryDao;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `category`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "category");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(2), "deca3c3756d2b01ba1167f57e573ed39", "e8ca7dfeb46b68975e9271e7b97e5f11");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }
}
