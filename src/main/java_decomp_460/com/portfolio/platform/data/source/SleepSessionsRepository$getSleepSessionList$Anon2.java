package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ao7;
import com.fossil.br7;
import com.fossil.c47;
import com.fossil.cl7;
import com.fossil.eo7;
import com.fossil.gi0;
import com.fossil.gj4;
import com.fossil.h47;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zi4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$2", f = "SleepSessionsRepository.kt", l = {68, 69}, m = "invokeSuspend")
public final class SleepSessionsRepository$getSleepSessionList$Anon2 extends ko7 implements vp7<iv7, qn7<? super LiveData<h47<? extends List<MFSleepSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements gi0<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository$getSleepSessionList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends c47<List<MFSleepSession>, gj4> {
            @DexIgnore
            public /* final */ /* synthetic */ cl7 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, br7 br7, int i, cl7 cl7) {
                this.this$0 = anon1_Level2;
                this.$offset = br7;
                this.$limit = i;
                this.$downloadingDate = cl7;
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object createCall(qn7<? super q88<gj4>> qn7) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                cl7 cl7 = this.$downloadingDate;
                if (cl7 == null || (date = (Date) cl7.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String k = lk5.k(date);
                pq7.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                cl7 cl72 = this.$downloadingDate;
                if (cl72 == null || (date2 = (Date) cl72.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String k2 = lk5.k(date2);
                pq7.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSleepSessions(k, k2, this.$offset.element, this.$limit, qn7);
            }

            @DexIgnore
            @Override // com.fossil.c47
            public Object loadFromDb(qn7<? super LiveData<List<MFSleepSession>>> qn7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSessionsRepository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("getSleepSessionList: startMilli = ");
                Date date = this.this$0.$startDate;
                pq7.b(date, GoalPhase.COLUMN_START_DATE);
                sb.append(date.getTime());
                sb.append(", endMilli = ");
                Date date2 = this.this$0.$endDate;
                pq7.b(date2, GoalPhase.COLUMN_END_DATE);
                sb.append(date2.getTime());
                local.d(str, sb.toString());
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                Date date3 = this.this$0.$startDate;
                pq7.b(date3, GoalPhase.COLUMN_START_DATE);
                long time = date3.getTime();
                Date date4 = this.this$0.$endDate;
                pq7.b(date4, GoalPhase.COLUMN_END_DATE);
                return sleepDao.getSleepSessionsLiveData(time, date4.getTime());
            }

            @DexIgnore
            @Override // com.fossil.c47
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(gj4 gj4, qn7<? super Boolean> qn7) {
                Boolean a2;
                zi4 zi4 = new zi4();
                zi4.f(DateTime.class, new GsonConvertDateTime());
                zi4.f(Date.class, new GsonConverterShortDate());
                Range range = ((ApiResponse) zi4.d().l(gj4.toString(), new SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$processContinueFetching$sleepSessionList$Anon1_Level4().getType())).get_range();
                if (range == null || (a2 = ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList getActivityList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object processContinueFetching(gj4 gj4, qn7 qn7) {
                return processContinueFetching(gj4, (qn7<? super Boolean>) qn7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.fossil.gj4 r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
                /*
                // Method dump skipped, instructions count: 245
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.gj4, com.fossil.qn7):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.qn7] */
            @Override // com.fossil.c47
            public /* bridge */ /* synthetic */ Object saveCallResult(gj4 gj4, qn7 qn7) {
                return saveCallResult(gj4, (qn7<? super tl7>) qn7);
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2, Date date, Date date2, SleepDatabase sleepDatabase) {
            this.this$0 = sleepSessionsRepository$getSleepSessionList$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
            this.$sleepDatabase = sleepDatabase;
        }

        @DexIgnore
        public final LiveData<h47<List<MFSleepSession>>> apply(List<FitnessDataWrapper> list) {
            br7 br7 = new br7();
            br7.element = 0;
            pq7.b(list, "fitnessDataList");
            Date date = this.$startDate;
            pq7.b(date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            pq7.b(date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, br7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$getSleepSessionList$Anon2(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, boolean z, qn7 qn7) {
        super(2, qn7);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2 = new SleepSessionsRepository$getSleepSessionList$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, qn7);
        sleepSessionsRepository$getSleepSessionList$Anon2.p$ = (iv7) obj;
        return sleepSessionsRepository$getSleepSessionList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super LiveData<h47<? extends List<MFSleepSession>>>> qn7) {
        return ((SleepSessionsRepository$getSleepSessionList$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00c6  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            r10 = 0
            r9 = 2
            r8 = 1
            java.lang.Object r5 = com.fossil.yn7.d()
            int r0 = r11.label
            if (r0 == 0) goto L_0x0075
            if (r0 == r8) goto L_0x0046
            if (r0 != r9) goto L_0x003e
            java.lang.Object r0 = r11.L$3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r11.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r11.L$1
            java.util.Date r2 = (java.util.Date) r2
            java.lang.Object r3 = r11.L$0
            com.fossil.iv7 r3 = (com.fossil.iv7) r3
            com.fossil.el7.b(r12)
            r3 = r12
            r4 = r1
            r5 = r0
        L_0x0025:
            r0 = r3
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r1 = r5.getFitnessDataDao()
            java.util.Date r3 = r11.$start
            java.util.Date r5 = r11.$end
            androidx.lifecycle.LiveData r1 = r1.getFitnessDataLiveData(r3, r5)
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2 r3 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2
            r3.<init>(r11, r2, r4, r0)
            androidx.lifecycle.LiveData r0 = com.fossil.ss0.c(r1, r3)
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r11.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r1 = r11.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r11.L$0
            com.fossil.iv7 r2 = (com.fossil.iv7) r2
            com.fossil.el7.b(r12)
            r4 = r0
            r3 = r12
        L_0x0057:
            r0 = r3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.fossil.dv7 r3 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 r6 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2
            r6.<init>(r10)
            r11.L$0 = r2
            r11.L$1 = r1
            r11.L$2 = r4
            r11.L$3 = r0
            r11.label = r9
            java.lang.Object r3 = com.fossil.eu7.g(r3, r6, r11)
            if (r3 != r5) goto L_0x00c6
            r0 = r5
            goto L_0x003d
        L_0x0075:
            com.fossil.el7.b(r12)
            com.fossil.iv7 r2 = r11.p$
            java.util.Date r0 = r11.$start
            java.util.Date r1 = com.fossil.lk5.V(r0)
            java.util.Date r0 = r11.$end
            java.util.Date r0 = com.fossil.lk5.E(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.access$getTAG$cp()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "getSleepSessionList: start = "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r7 = ", end = "
            r6.append(r7)
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r3.d(r4, r6)
            com.fossil.dv7 r3 = com.fossil.bw7.b()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2
            r4.<init>(r10)
            r11.L$0 = r2
            r11.L$1 = r1
            r11.L$2 = r0
            r11.label = r8
            java.lang.Object r3 = com.fossil.eu7.g(r3, r4, r11)
            if (r3 != r5) goto L_0x00ca
            r0 = r5
            goto L_0x003d
        L_0x00c6:
            r2 = r1
            r5 = r0
            goto L_0x0025
        L_0x00ca:
            r4 = r0
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
