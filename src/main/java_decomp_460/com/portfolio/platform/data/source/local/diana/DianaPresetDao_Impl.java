package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.f05;
import com.fossil.g05;
import com.fossil.jw0;
import com.fossil.nw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDao_Impl implements DianaPresetDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<DianaPreset> __insertionAdapterOfDianaPreset;
    @DexIgnore
    public /* final */ jw0<DianaRecommendPreset> __insertionAdapterOfDianaRecommendPreset;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearDianaPresetTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearDianaRecommendPresetTable;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfRemoveAllDeletePinTypePreset;
    @DexIgnore
    public /* final */ f05 __presetComplicationSettingTypeConverter; // = new f05();
    @DexIgnore
    public /* final */ g05 __presetWatchAppSettingTypeConverter; // = new g05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<DianaRecommendPreset> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaRecommendPreset dianaRecommendPreset) {
            if (dianaRecommendPreset.getSerialNumber() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaRecommendPreset.getSerialNumber());
            }
            if (dianaRecommendPreset.getId() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaRecommendPreset.getId());
            }
            if (dianaRecommendPreset.getName() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, dianaRecommendPreset.getName());
            }
            px0.bindLong(4, dianaRecommendPreset.isDefault() ? 1 : 0);
            String a2 = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaRecommendPreset.getComplications());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            String a3 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaRecommendPreset.getWatchapps());
            if (a3 == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, a3);
            }
            if (dianaRecommendPreset.getWatchFaceId() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, dianaRecommendPreset.getWatchFaceId());
            }
            if (dianaRecommendPreset.getCreatedAt() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, dianaRecommendPreset.getCreatedAt());
            }
            if (dianaRecommendPreset.getUpdatedAt() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, dianaRecommendPreset.getUpdatedAt());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaRecommendPreset` (`serialNumber`,`id`,`name`,`isDefault`,`complications`,`watchapps`,`watchFaceId`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon10(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            boolean z = false;
            Cursor b = ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "serialNumber");
                int c6 = dx0.c(b, "name");
                int c7 = dx0.c(b, "isActive");
                int c8 = dx0.c(b, "complications");
                int c9 = dx0.c(b, "watchapps");
                int c10 = dx0.c(b, "watchFaceId");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    if (b.getInt(c7) != 0) {
                        z = true;
                    }
                    dianaPreset = new DianaPreset(string, string2, string3, z, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                }
                return dianaPreset;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends jw0<DianaPreset> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, DianaPreset dianaPreset) {
            if (dianaPreset.getCreatedAt() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, dianaPreset.getCreatedAt());
            }
            if (dianaPreset.getUpdatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, dianaPreset.getUpdatedAt());
            }
            px0.bindLong(3, (long) dianaPreset.getPinType());
            if (dianaPreset.getId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, dianaPreset.getId());
            }
            if (dianaPreset.getSerialNumber() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, dianaPreset.getSerialNumber());
            }
            if (dianaPreset.getName() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, dianaPreset.getName());
            }
            px0.bindLong(7, dianaPreset.isActive() ? 1 : 0);
            String a2 = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaPreset.getComplications());
            if (a2 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, a2);
            }
            String a3 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaPreset.getWatchapps());
            if (a3 == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, a3);
            }
            if (dianaPreset.getWatchFaceId() == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, dianaPreset.getWatchFaceId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaPreset` (`createdAt`,`updatedAt`,`pinType`,`id`,`serialNumber`,`name`,`isActive`,`complications`,`watchapps`,`watchFaceId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xw0 {
        @DexIgnore
        public Anon4(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends xw0 {
        @DexIgnore
        public Anon5(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends xw0 {
        @DexIgnore
        public Anon6(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends xw0 {
        @DexIgnore
        public Anon7(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon8(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            boolean z = false;
            Cursor b = ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "serialNumber");
                int c6 = dx0.c(b, "name");
                int c7 = dx0.c(b, "isActive");
                int c8 = dx0.c(b, "complications");
                int c9 = dx0.c(b, "watchapps");
                int c10 = dx0.c(b, "watchFaceId");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    if (b.getInt(c7) != 0) {
                        z = true;
                    }
                    dianaPreset = new DianaPreset(string, string2, string3, z, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                }
                return dianaPreset;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<List<DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ tw0 val$_statement;

        @DexIgnore
        public Anon9(tw0 tw0) {
            this.val$_statement = tw0;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaPreset> call() throws Exception {
            Cursor b = ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = dx0.c(b, "createdAt");
                int c2 = dx0.c(b, "updatedAt");
                int c3 = dx0.c(b, "pinType");
                int c4 = dx0.c(b, "id");
                int c5 = dx0.c(b, "serialNumber");
                int c6 = dx0.c(b, "name");
                int c7 = dx0.c(b, "isActive");
                int c8 = dx0.c(b, "complications");
                int c9 = dx0.c(b, "watchapps");
                int c10 = dx0.c(b, "watchFaceId");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                    arrayList.add(dianaPreset);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaPresetDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfDianaRecommendPreset = new Anon1(qw0);
        this.__insertionAdapterOfDianaPreset = new Anon2(qw0);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon3(qw0);
        this.__preparedStmtOfClearDianaPresetTable = new Anon4(qw0);
        this.__preparedStmtOfClearDianaRecommendPresetTable = new Anon5(qw0);
        this.__preparedStmtOfDeletePreset = new Anon6(qw0);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearDianaPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearDianaRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getActivePresetBySerial(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            if (b.moveToFirst()) {
                dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
            }
            return dianaPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPendingPreset(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPreset(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon9 anon9 = new Anon9(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon9);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPresets() {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE pinType != 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaRecommendPreset> getDianaRecommendPresetList(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "serialNumber");
            int c2 = dx0.c(b, "id");
            int c3 = dx0.c(b, "name");
            int c4 = dx0.c(b, "isDefault");
            int c5 = dx0.c(b, "complications");
            int c6 = dx0.c(b, "watchapps");
            int c7 = dx0.c(b, "watchFaceId");
            int c8 = dx0.c(b, "createdAt");
            int c9 = dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DianaRecommendPreset(b.getString(c), b.getString(c2), b.getString(c3), b.getInt(c4) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c5)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getPresetById(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            if (b.moveToFirst()) {
                dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
            }
            return dianaPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<DianaPreset> getPresetByIdLive(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getPresetListByWatchFaceId(String str) {
        tw0 f = tw0.f("SELECT * FROM dianaPreset WHERE watchFaceId=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "createdAt");
            int c2 = dx0.c(b, "updatedAt");
            int c3 = dx0.c(b, "pinType");
            int c4 = dx0.c(b, "id");
            int c5 = dx0.c(b, "serialNumber");
            int c6 = dx0.c(b, "name");
            int c7 = dx0.c(b, "isActive");
            int c8 = dx0.c(b, "complications");
            int c9 = dx0.c(b, "watchapps");
            int c10 = dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPreset(DianaPreset dianaPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert((jw0<DianaPreset>) dianaPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPresetList(List<DianaPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
