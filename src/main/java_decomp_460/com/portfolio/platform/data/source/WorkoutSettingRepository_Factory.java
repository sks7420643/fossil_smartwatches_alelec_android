package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRepository_Factory implements Factory<WorkoutSettingRepository> {
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRemoteDataSource> mWorkoutSettingRemoteDataSourceProvider;

    @DexIgnore
    public WorkoutSettingRepository_Factory(Provider<WorkoutSettingRemoteDataSource> provider) {
        this.mWorkoutSettingRemoteDataSourceProvider = provider;
    }

    @DexIgnore
    public static WorkoutSettingRepository_Factory create(Provider<WorkoutSettingRemoteDataSource> provider) {
        return new WorkoutSettingRepository_Factory(provider);
    }

    @DexIgnore
    public static WorkoutSettingRepository newInstance(WorkoutSettingRemoteDataSource workoutSettingRemoteDataSource) {
        return new WorkoutSettingRepository(workoutSettingRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WorkoutSettingRepository get() {
        return newInstance(this.mWorkoutSettingRemoteDataSourceProvider.get());
    }
}
