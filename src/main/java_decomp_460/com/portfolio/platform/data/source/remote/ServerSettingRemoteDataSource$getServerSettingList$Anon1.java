package com.portfolio.platform.data.source.remote;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.iv7;
import com.fossil.jq5;
import com.fossil.ko7;
import com.fossil.kq5;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1", f = "ServerSettingRemoteDataSource.kt", l = {32}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1(ServerSettingRemoteDataSource serverSettingRemoteDataSource, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, qn7 qn7) {
        super(2, qn7);
        this.this$0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this.this$0, this.$callback, qn7);
        serverSettingRemoteDataSource$getServerSettingList$Anon1.p$ = (iv7) obj;
        return serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Object d2 = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2(this, null);
            this.L$0 = iv7;
            this.label = 1;
            d = jq5.d(serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            d = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        iq5 iq5 = (iq5) d;
        if (iq5 instanceof kq5) {
            FLogger.INSTANCE.getLocal().e(ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((ServerSettingList) ((kq5) iq5).a());
        } else if (iq5 instanceof hq5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            hq5 hq5 = (hq5) iq5;
            sb.append(hq5.a());
            local.e(tAG$app_fossilRelease, sb.toString());
            if (hq5.d() instanceof SocketTimeoutException) {
                this.$callback.onFailed(MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return tl7.f3441a;
    }
}
