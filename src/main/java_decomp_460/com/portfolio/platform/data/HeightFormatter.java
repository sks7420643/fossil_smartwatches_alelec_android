package com.portfolio.platform.data;

import com.fossil.um5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HeightFormatter implements NumberPickerLarge.f, Serializable {
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public /* final */ int unit;

    @DexIgnore
    public HeightFormatter(int i) {
        this.unit = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.NumberPickerLarge.f
    public String format(int i) {
        int i2 = this.unit;
        if (i2 == 0) {
            return String.format(Locale.US, um5.c(PortfolioApp.d0, 2131887406), Integer.valueOf(i));
        } else if (i2 == 1) {
            return String.format(Locale.US, um5.c(PortfolioApp.d0, 2131887431), Integer.valueOf(i));
        } else {
            return String.format(Locale.US, um5.c(PortfolioApp.d0, 2131887489), Integer.valueOf(i));
        }
    }
}
