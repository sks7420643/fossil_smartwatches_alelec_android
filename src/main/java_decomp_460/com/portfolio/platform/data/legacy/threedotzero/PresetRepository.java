package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.a15;
import com.fossil.b15;
import com.fossil.c15;
import com.fossil.d15;
import com.fossil.lk7;
import com.fossil.no4;
import com.fossil.oj5;
import com.fossil.t05;
import com.fossil.u05;
import com.fossil.um5;
import com.fossil.v05;
import com.fossil.w05;
import com.fossil.x05;
import com.fossil.y05;
import com.fossil.z05;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PresetRepository extends PresetDataSource {
    @DexIgnore
    public static /* final */ String CACHE_MAPPING_SET_KEY; // = "cacheMappingSetKey";
    @DexIgnore
    public static /* final */ String TAG; // = "PresetRepository";
    @DexIgnore
    public /* final */ List<ActivePresetRepositoryObserver> mActivePresetObservers; // = new CopyOnWriteArrayList();
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public boolean mCacheIsDirty;
    @DexIgnore
    public ActivePreset mCachedActivePreset;
    @DexIgnore
    public Map<String, List<SavedPreset>> mCachedSavedPresetList;
    @DexIgnore
    public boolean mIsActivePresetCacheDirty;
    @DexIgnore
    public /* final */ PresetDataSource mMappingSetLocalDataSource;
    @DexIgnore
    public /* final */ PresetDataSource mMappingSetRemoteDataSource;
    @DexIgnore
    public /* final */ List<SavedPresetRepositoryObserver> mSavedPresetObservers; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<ShortcutDownloadingObserver> mShortcutDownloadingObservers;

    @DexIgnore
    public interface ActivePresetRepositoryObserver {
        @DexIgnore
        Object onActivePresetChange();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements PresetDataSource.GetSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;

        @DexIgnore
        public Anon1(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onFail() {
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .getSavedPresetList get from local DB, result=" + list);
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onSuccess(list);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetListCallback val$callback;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onFail() {
                MFLogger.d(PresetRepository.TAG, "Inside .addOrUpdateSavedPresetList onRemote onFail");
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onSuccess(List<SavedPreset> list) {
                MFLogger.d(PresetRepository.TAG, "Inside .addOrUpdateSavedPresetList onRemote success");
            }
        }

        @DexIgnore
        public Anon10(PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
            this.val$callback = addOrUpdateSavedPresetListCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .addOrUpdateSavedPresetList onLocal onFail");
            this.val$callback.onFail();
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .addOrUpdateSavedPresetList onLocal onSuccess savedPresetListSize=" + list.size());
            PresetRepository.this.mMappingSetRemoteDataSource.addOrUpdateSavedPresetList(list, new Anon1_Level2());
            PresetRepository presetRepository = PresetRepository.this;
            presetRepository.mCacheIsDirty = true;
            presetRepository.notifySavedPresetObserver();
            this.val$callback.onSuccess(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements PresetDataSource.GetRecommendedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$deviceSerial;

        @DexIgnore
        public Anon11(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
            this.val$deviceSerial = str;
            this.val$callback = getRecommendedPresetListCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(String str, List list, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
            String str2 = PresetRepository.TAG;
            MFLogger.d(str2, "saveRecommendedPresetList serial=" + str);
            MFLogger.d(PresetRepository.TAG, "diskIO enter downloadRecommendPreset onSuccess");
            ((PresetLocalDataSource) PresetRepository.this.mMappingSetLocalDataSource).deleteAllRecommendedPresets(str, null);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                RecommendedPreset recommendedPreset = (RecommendedPreset) it.next();
                recommendedPreset.setSerialNumber(str);
                ((PresetLocalDataSource) PresetRepository.this.mMappingSetLocalDataSource).addOrUpdateRecommendedPresets(recommendedPreset);
            }
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onSuccess(list);
            }
            MFLogger.d(PresetRepository.TAG, "diskIO exit downloadRecommendPreset onSuccess");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetListCallback
        public void onFail() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "downloadRecommendPreset on remote onFail deviceSerial=" + this.val$deviceSerial);
            PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback = this.val$callback;
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetListCallback
        public void onSuccess(List<RecommendedPreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "downloadRecommendPreset on remote onSuccess deviceSerial=" + this.val$deviceSerial);
            PresetRepository.this.mAppExecutors.a().execute(new t05(this, this.val$deviceSerial, list, this.val$callback));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements PresetDataSource.GetSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onFail() {
                MFLogger.d(PresetRepository.TAG, "Inside .addOrUpdateSavedPresetList onFail");
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onSuccess(List<SavedPreset> list) {
                MFLogger.d(PresetRepository.TAG, "Inside .addOrUpdateSavedPresetList onSuccess");
                PresetRepository presetRepository = PresetRepository.this;
                presetRepository.mCacheIsDirty = true;
                presetRepository.notifySavedPresetObserver();
            }
        }

        @DexIgnore
        public Anon12(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(List list) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter downloadSavedPresets addOrUpdateSavedPresetList");
            PresetRepository.this.mMappingSetLocalDataSource.addOrUpdateSavedPresetList(list, new Anon1_Level2());
            MFLogger.d(PresetRepository.TAG, "diskIO exit downloadSavedPresets addOrUpdateSavedPresetList");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "downloadSavedPresets onFail");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            MFLogger.d(PresetRepository.TAG, "Inside .getAllUserPresets get from remote, success");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onSuccess(list);
            }
            PresetRepository.this.mAppExecutors.a().execute(new u05(this, list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements PresetDataSource.GetActivePresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetActivePresetListCallback val$callback;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.AddOrUpdateActivePresetCallback {
            @DexIgnore
            public /* final */ /* synthetic */ ActivePreset val$activePreset;

            @DexIgnore
            public Anon1_Level2(ActivePreset activePreset) {
                this.val$activePreset = activePreset;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
            public void onFail() {
                String str = PresetRepository.TAG;
                MFLogger.d(str, "downloadActivePresetList save DB Failed serial=" + this.val$activePreset.getSerialNumber());
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
            public void onSuccess(ActivePreset activePreset) {
                String str = PresetRepository.TAG;
                MFLogger.d(str, "downloadActivePresetList save DB Success serial=" + activePreset.getSerialNumber());
                PresetRepository presetRepository = PresetRepository.this;
                presetRepository.mIsActivePresetCacheDirty = true;
                presetRepository.notifyActivePresetObserver();
            }
        }

        @DexIgnore
        public Anon13(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
            this.val$callback = getActivePresetListCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(List list) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter downloadActivePresetList onSuccess");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ActivePreset activePreset = (ActivePreset) it.next();
                PresetRepository.this.mMappingSetLocalDataSource.addOrUpdateActivePreset(activePreset, new Anon1_Level2(activePreset));
            }
            MFLogger.d(PresetRepository.TAG, "diskIO exit downloadActivePresetList onSuccess");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetListCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "downloadActivePresetList onFail");
            PresetDataSource.GetActivePresetListCallback getActivePresetListCallback = this.val$callback;
            if (getActivePresetListCallback != null) {
                getActivePresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetListCallback
        public void onSuccess(List<ActivePreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "downloadActivePresetList onSuccess size=" + list.size());
            PresetDataSource.GetActivePresetListCallback getActivePresetListCallback = this.val$callback;
            if (getActivePresetListCallback != null) {
                getActivePresetListCallback.onSuccess(list);
            }
            PresetRepository.this.mAppExecutors.a().execute(new v05(this, list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements PresetDataSource.GetRecommendedPresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$deviceSerial;

        @DexIgnore
        public Anon14(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
            this.val$deviceSerial = str;
            this.val$callback = getRecommendedPresetCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(RecommendedPreset recommendedPreset) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter onSuccess downloadDefaultMappingSet");
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Download default mapping set from server success, defaultPresetId=" + recommendedPreset.getId());
            if (((PresetLocalDataSource) PresetRepository.this.mMappingSetLocalDataSource).addOrUpdateDefaultPreset(recommendedPreset)) {
                MFLogger.d(PresetRepository.TAG, "getRecommendedPresets on remote onSuccess saveDB success");
                PresetRepository presetRepository = PresetRepository.this;
                presetRepository.mCacheIsDirty = true;
                presetRepository.notifySavedPresetObserver();
            }
            MFLogger.d(PresetRepository.TAG, "diskIO exit onSuccess downloadDefaultMappingSet");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetCallback
        public void onFail() {
            PresetRepository.this.generateDefaultPreset(this.val$deviceSerial, this.val$callback);
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetCallback
        public void onSuccess(RecommendedPreset recommendedPreset) {
            recommendedPreset.setSerialNumber(this.val$deviceSerial);
            PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback = this.val$callback;
            if (getRecommendedPresetCallback != null) {
                getRecommendedPresetCallback.onSuccess(recommendedPreset);
            }
            PresetRepository.this.mAppExecutors.a().execute(new w05(this, recommendedPreset));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon15 implements PresetDataSource.PushActivePresetToServerCallback {
        @DexIgnore
        public /* final */ /* synthetic */ CountDownLatch val$countDownLatch;

        @DexIgnore
        public Anon15(CountDownLatch countDownLatch) {
            this.val$countDownLatch = countDownLatch;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.PushActivePresetToServerCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "pushActivePresetToServer failed :(((");
            CountDownLatch countDownLatch = this.val$countDownLatch;
            if (countDownLatch != null) {
                countDownLatch.countDown();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.PushActivePresetToServerCallback
        public void onSuccess() {
            MFLogger.d(PresetRepository.TAG, "pushActivePresetToServer success, bravo!!!");
            CountDownLatch countDownLatch = this.val$countDownLatch;
            if (countDownLatch != null) {
                countDownLatch.countDown();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon16 implements PresetDataSource.DeleteMappingSetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback val$pushPendingSavedPresetsCallback;

        @DexIgnore
        public Anon16(PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
            this.val$pushPendingSavedPresetsCallback = pushPendingSavedPresetsCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onFail() {
            PresetRepository.this.pushPendingUserPresetList(this.val$pushPendingSavedPresetsCallback);
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onSuccess() {
            PresetRepository.this.pushPendingUserPresetList(this.val$pushPendingSavedPresetsCallback);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon17 implements PresetDataSource.DeleteMappingSetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$deletedUserPresets;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.DeleteMappingSetCallback {
            @DexIgnore
            public /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback val$callback;

            @DexIgnore
            public Anon1_Level2(PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
                this.val$callback = deleteMappingSetCallback;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
            public void onFail() {
                this.val$callback.onFail();
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
            public void onSuccess() {
                this.val$callback.onSuccess();
            }
        }

        @DexIgnore
        public Anon17(List list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
            this.val$deletedUserPresets = list;
            this.val$callback = deleteMappingSetCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(List list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter deleteSavedPresetInServer");
            PresetRepository.this.mMappingSetLocalDataSource.deleteSavedPresetList(list, new Anon1_Level2(deleteMappingSetCallback));
            MFLogger.d(PresetRepository.TAG, "diskIO exit deleteSavedPresetInServer");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPresetList delete failed on remote");
            this.val$callback.onSuccess();
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onSuccess() {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPresetList delete success on remote");
            PresetRepository.this.mAppExecutors.a().execute(new x05(this, this.val$deletedUserPresets, this.val$callback));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon18 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$pendingUserPresetList;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
            @DexIgnore
            public /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback val$callback;

            @DexIgnore
            public Anon1_Level2(PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
                this.val$callback = pushPendingSavedPresetsCallback;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onFail() {
                PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback = this.val$callback;
                if (pushPendingSavedPresetsCallback != null) {
                    pushPendingSavedPresetsCallback.onDone();
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
            public void onSuccess(List<SavedPreset> list) {
                PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback = this.val$callback;
                if (pushPendingSavedPresetsCallback != null) {
                    pushPendingSavedPresetsCallback.onDone();
                }
            }
        }

        @DexIgnore
        public Anon18(List list, PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
            this.val$pendingUserPresetList = list;
            this.val$callback = pushPendingSavedPresetsCallback;
        }

        @DexIgnore
        public /* synthetic */ void a(List list, PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter pushPendingUserPresetList addOrUpdateSavedPresetList");
            PresetRepository.this.mMappingSetLocalDataSource.addOrUpdateSavedPresetList(list, new Anon1_Level2(pushPendingSavedPresetsCallback));
            MFLogger.d(PresetRepository.TAG, "diskIO exit pushPendingUserPresetList addOrUpdateSavedPresetList");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .pushPendingUserPresetList failed on remote");
            PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback = this.val$callback;
            if (pushPendingSavedPresetsCallback != null) {
                pushPendingSavedPresetsCallback.onDone();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            MFLogger.d(PresetRepository.TAG, "Inside .pushPendingUserPresetList success on remote");
            PresetRepository.this.mAppExecutors.a().execute(new y05(this, this.val$pendingUserPresetList, this.val$callback));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon19 implements PresetDataSource.AddOrUpdateActivePresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.PushActivePresetToServerCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ ActivePreset val$savedActivePreset;

        @DexIgnore
        public Anon19(PresetDataSource.PushActivePresetToServerCallback pushActivePresetToServerCallback, ActivePreset activePreset) {
            this.val$callback = pushActivePresetToServerCallback;
            this.val$savedActivePreset = activePreset;
        }

        @DexIgnore
        public /* synthetic */ void a(ActivePreset activePreset) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter onSuccess pushActivePresetToServer");
            ((PresetLocalDataSource) PresetRepository.this.mMappingSetLocalDataSource).updateActivePresetPinType(activePreset.getSerialNumber(), 0);
            MFLogger.d(PresetRepository.TAG, "diskIO exit onSuccess pushActivePresetToServer");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
        public void onFail() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "pushActivePresetToServer onFail serial=" + this.val$savedActivePreset.getSerialNumber());
            PresetDataSource.PushActivePresetToServerCallback pushActivePresetToServerCallback = this.val$callback;
            if (pushActivePresetToServerCallback != null) {
                pushActivePresetToServerCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
        public void onSuccess(ActivePreset activePreset) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "pushActivePresetToServer onSuccess serial=" + activePreset.getSerialNumber());
            PresetDataSource.PushActivePresetToServerCallback pushActivePresetToServerCallback = this.val$callback;
            if (pushActivePresetToServerCallback != null) {
                pushActivePresetToServerCallback.onSuccess();
            }
            PresetRepository.this.mAppExecutors.a().execute(new z05(this, activePreset));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements PresetDataSource.AddOrUpdateActivePresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ ActivePreset val$activePreset;
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateActivePresetCallback val$callback;

        @DexIgnore
        public Anon2(PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback, ActivePreset activePreset) {
            this.val$callback = addOrUpdateActivePresetCallback;
            this.val$activePreset = activePreset;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
        public void onFail() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .addOrUpdateActivePreset local onFail activePreset=" + this.val$activePreset.getSerialNumber());
            PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback = this.val$callback;
            if (addOrUpdateActivePresetCallback != null) {
                addOrUpdateActivePresetCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
        public void onSuccess(ActivePreset activePreset) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .addOrUpdateActivePreset local onSuccess activePreset=" + activePreset.getSerialNumber());
            PresetRepository.this.pushActivePresetToServer(activePreset, null);
            PresetRepository presetRepository = PresetRepository.this;
            presetRepository.mIsActivePresetCacheDirty = true;
            presetRepository.notifyActivePresetObserver();
            PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback = this.val$callback;
            if (addOrUpdateActivePresetCallback != null) {
                addOrUpdateActivePresetCallback.onSuccess(activePreset);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon20 implements PresetDataSource.AddOrUpdateSavedPresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon20(SavedPreset savedPreset) {
            this.val$savedPreset = savedPreset;
        }

        @DexIgnore
        public /* synthetic */ void a(SavedPreset savedPreset) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter pushActivePresetToServer updateSavedPresetPinType");
            ((PresetLocalDataSource) PresetRepository.this.mMappingSetLocalDataSource).updateSavedPresetPinType(savedPreset.getId(), 0);
            MFLogger.d(PresetRepository.TAG, "diskIO exit pushActivePresetToServer updateSavedPresetPinType");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onFail() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset remote failed objectId=" + this.val$savedPreset.getId());
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onSuccess(SavedPreset savedPreset) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset remote success id=" + savedPreset.getId());
            PresetRepository.this.mAppExecutors.a().execute(new a15(this, savedPreset));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon21 implements PresetDataSource.DeleteMappingSetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$finalSavedPreset;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.DeleteMappingSetCallback {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
            public void onFail() {
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
            public void onSuccess() {
            }
        }

        @DexIgnore
        public Anon21(SavedPreset savedPreset) {
            this.val$finalSavedPreset = savedPreset;
        }

        @DexIgnore
        public /* synthetic */ void a(SavedPreset savedPreset) {
            MFLogger.d(PresetRepository.TAG, "diskIO enter deleteSavedPresetInServer");
            PresetRepository.this.mMappingSetLocalDataSource.deleteSavedPreset(savedPreset, new Anon1_Level2());
            MFLogger.d(PresetRepository.TAG, "diskIO exit deleteSavedPresetInServer");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPresetList delete failed on remote");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onSuccess() {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPresetList delete failed on remote");
            PresetRepository.this.mAppExecutors.a().execute(new b15(this, this.val$finalSavedPreset));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon22 implements PresetDataSource.GetSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;

        @DexIgnore
        public Anon22(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .getAllUserPresetsFromDB get from local DB, fail");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .getAllUserPresetsFromDB get from local DB, result=" + list);
            PresetRepository.this.processLoadedMappingSetList(list);
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onSuccess(PresetRepository.this.getCachedMappingSetList());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements PresetDataSource.AddOrUpdateSavedPresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetCallback val$callback;

        @DexIgnore
        public Anon3(PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
            this.val$callback = addOrUpdateSavedPresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onFail() {
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onSuccess(SavedPreset savedPreset) {
            PresetRepository.this.notifySavedPresetObserver();
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onSuccess(savedPreset);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements PresetDataSource.DeleteMappingSetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon4(SavedPreset savedPreset, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
            this.val$savedPreset = savedPreset;
            this.val$callback = deleteMappingSetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "deleteSavedPreset PinType.TYPE_PIN_EDIT onFail");
            this.val$callback.onFail();
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.DeleteMappingSetCallback
        public void onSuccess() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "deleteSavedPreset PinType.TYPE_PIN_EDIT onSuccess id=" + this.val$savedPreset.getId());
            PresetRepository presetRepository = PresetRepository.this;
            presetRepository.mCacheIsDirty = true;
            presetRepository.notifySavedPresetObserver();
            this.val$callback.onSuccess();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements PresetDataSource.AddOrUpdateSavedPresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback val$callback;

        @DexIgnore
        public Anon5(PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
            this.val$callback = deleteMappingSetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPreset delete failed on local");
            this.val$callback.onFail();
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetCallback
        public void onSuccess(SavedPreset savedPreset) {
            MFLogger.d(PresetRepository.TAG, "Inside .deleteSavedPreset delete successfully on local");
            PresetRepository presetRepository = PresetRepository.this;
            presetRepository.mCacheIsDirty = true;
            presetRepository.notifySavedPresetObserver();
            PresetRepository.this.deleteSavedPresetInServer(savedPreset);
            this.val$callback.onSuccess();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements PresetDataSource.GetActivePresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback val$callback;

        @DexIgnore
        public Anon6(PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
            this.val$callback = getActivePresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset on DB onFail");
            PresetDataSource.GetActivePresetCallback getActivePresetCallback = this.val$callback;
            if (getActivePresetCallback != null) {
                getActivePresetCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetCallback
        public void onSuccess(ActivePreset activePreset) {
            MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset on DB onSuccess");
            PresetRepository.this.processLoadedActivePreset(activePreset);
            PresetDataSource.GetActivePresetCallback getActivePresetCallback = this.val$callback;
            if (getActivePresetCallback != null) {
                getActivePresetCallback.onSuccess(activePreset);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements PresetDataSource.GetActivePresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$serial;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements PresetDataSource.GetActivePresetListCallback {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Anon1_Level3 implements PresetDataSource.AddOrUpdateActivePresetCallback {
                @DexIgnore
                public /* final */ /* synthetic */ ActivePreset val$activePreset;
                @DexIgnore
                public /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback val$callback;
                @DexIgnore
                public /* final */ /* synthetic */ AtomicBoolean val$isSuccess;
                @DexIgnore
                public /* final */ /* synthetic */ String val$serial;

                @DexIgnore
                public Anon1_Level3(String str, AtomicBoolean atomicBoolean, PresetDataSource.GetActivePresetCallback getActivePresetCallback, ActivePreset activePreset) {
                    this.val$serial = str;
                    this.val$isSuccess = atomicBoolean;
                    this.val$callback = getActivePresetCallback;
                    this.val$activePreset = activePreset;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
                public void onFail() {
                    String str = PresetRepository.TAG;
                    MFLogger.d(str, "Inside .getActivePreset save to local onFail serial=" + this.val$activePreset.getSerialNumber());
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateActivePresetCallback
                public void onSuccess(ActivePreset activePreset) {
                    String str = PresetRepository.TAG;
                    MFLogger.d(str, "Inside .getActivePreset save to local onSuccess serial=" + activePreset.getSerialNumber());
                    if (activePreset.getSerialNumber().equals(this.val$serial)) {
                        PresetRepository.this.notifyStatusChanged("ACTIVE_PRESET_DOWNLOADED", this.val$serial);
                        this.val$isSuccess.getAndSet(true);
                        PresetDataSource.GetActivePresetCallback getActivePresetCallback = this.val$callback;
                        if (getActivePresetCallback != null) {
                            getActivePresetCallback.onSuccess(activePreset);
                        }
                    }
                }
            }

            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            public /* synthetic */ void a(List list, String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
                MFLogger.d(PresetRepository.TAG, "diskIO enter onFail onSuccess getActivePreset");
                AtomicBoolean atomicBoolean = new AtomicBoolean(false);
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    ActivePreset activePreset = (ActivePreset) it.next();
                    PresetRepository.this.mMappingSetLocalDataSource.addOrUpdateActivePreset(activePreset, new Anon1_Level3(str, atomicBoolean, getActivePresetCallback, activePreset));
                }
                String str2 = PresetRepository.TAG;
                MFLogger.d(str2, "Inside .getActivePreset done update on local isSuccess=" + atomicBoolean.get());
                if (!atomicBoolean.get() && getActivePresetCallback != null) {
                    getActivePresetCallback.onFail();
                }
                MFLogger.d(PresetRepository.TAG, "diskIO exit onFail onSuccess getActivePreset");
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetListCallback
            public void onFail() {
                MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset download on remote onFail");
                PresetDataSource.GetActivePresetCallback getActivePresetCallback = Anon7.this.val$callback;
                if (getActivePresetCallback != null) {
                    getActivePresetCallback.onFail();
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetListCallback
            public void onSuccess(List<ActivePreset> list) {
                MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset download on remote onSuccess");
                Executor a2 = PresetRepository.this.mAppExecutors.a();
                Anon7 anon7 = Anon7.this;
                a2.execute(new c15(this, list, anon7.val$serial, anon7.val$callback));
            }
        }

        @DexIgnore
        public Anon7(String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
            this.val$serial = str;
            this.val$callback = getActivePresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetCallback
        public void onFail() {
            MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset on DB onFail");
            PresetRepository.this.notifyStatusChanged("PRESETS_DOWNLOADING", this.val$serial);
            PresetRepository.this.mMappingSetRemoteDataSource.downloadActivePresetList(new Anon1_Level2());
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetActivePresetCallback
        public void onSuccess(ActivePreset activePreset) {
            MFLogger.d(PresetRepository.TAG, "Inside .getActivePreset on DB onSuccess");
            PresetRepository.this.notifyStatusChanged("ACTIVE_PRESET_DOWNLOADED", this.val$serial);
            PresetRepository.this.processLoadedActivePreset(activePreset);
            PresetDataSource.GetActivePresetCallback getActivePresetCallback = this.val$callback;
            if (getActivePresetCallback != null) {
                getActivePresetCallback.onSuccess(activePreset);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements PresetDataSource.GetRecommendedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$deviceSerial;

        @DexIgnore
        public Anon8(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
            this.val$deviceSerial = str;
            this.val$callback = getRecommendedPresetListCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetListCallback
        public void onFail() {
            PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback = this.val$callback;
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetListCallback
        public void onSuccess(List<RecommendedPreset> list) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "getRecommendedPresets on local onSuccess deviceSerial=" + this.val$deviceSerial);
            PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback = this.val$callback;
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onSuccess(list);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements PresetDataSource.GetRecommendedPresetCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ String val$deviceSerial;

        @DexIgnore
        public Anon9(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
            this.val$deviceSerial = str;
            this.val$callback = getRecommendedPresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetCallback
        public void onFail() {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .getDefaultPreset onFail serial=" + this.val$deviceSerial);
            PresetRepository.this.downloadDefaultPreset(this.val$deviceSerial, this.val$callback);
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.GetRecommendedPresetCallback
        public void onSuccess(RecommendedPreset recommendedPreset) {
            String str = PresetRepository.TAG;
            MFLogger.d(str, "Inside .getDefaultPreset onSuccess serial=" + this.val$deviceSerial);
            PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback = this.val$callback;
            if (getRecommendedPresetCallback != null) {
                getRecommendedPresetCallback.onSuccess(recommendedPreset);
            }
        }
    }

    @DexIgnore
    public interface SavedPresetRepositoryObserver {
        @DexIgnore
        Object onSavedPresetChange();  // void declaration
    }

    @DexIgnore
    public PresetRepository(@Remote PresetDataSource presetDataSource, @Local PresetDataSource presetDataSource2, no4 no4) {
        lk7.c(presetDataSource, "mappingRemoteSetDataSource cannot be null!");
        this.mMappingSetRemoteDataSource = presetDataSource;
        lk7.c(presetDataSource2, "mappingLocalSetDataSource cannot be null!");
        this.mMappingSetLocalDataSource = presetDataSource2;
        lk7.c(no4, "appExecutors cannot be null!");
        this.mAppExecutors = no4;
    }

    @DexIgnore
    private void deletePendingUserPresetList(PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        List<SavedPreset> pendingDeletedUserPresets = ((PresetLocalDataSource) this.mMappingSetLocalDataSource).getPendingDeletedUserPresets();
        if (pendingDeletedUserPresets == null || pendingDeletedUserPresets.isEmpty()) {
            deleteMappingSetCallback.onSuccess();
        } else {
            this.mMappingSetRemoteDataSource.deleteSavedPresetList(pendingDeletedUserPresets, new Anon17(pendingDeletedUserPresets, deleteMappingSetCallback));
        }
    }

    @DexIgnore
    private void deleteSavedPresetInServer(SavedPreset savedPreset) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(savedPreset);
        this.mMappingSetRemoteDataSource.deleteSavedPresetList(arrayList, new Anon21(savedPreset));
    }

    @DexIgnore
    private void pushSavedPresetToServer(SavedPreset savedPreset) {
        this.mMappingSetRemoteDataSource.addOrUpdateSavedPreset(savedPreset, new Anon20(savedPreset));
    }

    @DexIgnore
    public /* synthetic */ void a(RecommendedPreset recommendedPreset) {
        MFLogger.d(TAG, "diskIO enter generateDefaultMappingSet");
        if (((PresetLocalDataSource) this.mMappingSetLocalDataSource).addOrUpdateDefaultPreset(recommendedPreset)) {
            MFLogger.d(TAG, "getRecommendedPresets generateDefaultMappingSet saveDB success");
            this.mCacheIsDirty = true;
            notifySavedPresetObserver();
        }
        MFLogger.d(TAG, "diskIO exit generateDefaultMappingSet");
    }

    @DexIgnore
    public void addActivePresetObserver(ActivePresetRepositoryObserver activePresetRepositoryObserver) {
        if (!this.mActivePresetObservers.contains(activePresetRepositoryObserver)) {
            this.mActivePresetObservers.add(activePresetRepositoryObserver);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "Inside .addOrUpdateActivePreset activePreset=" + activePreset.getSerialNumber());
        activePreset.setPinType(2);
        this.mMappingSetLocalDataSource.addOrUpdateActivePreset(activePreset, new Anon2(addOrUpdateActivePresetCallback, activePreset));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        MFLogger.d(TAG, "Inside .addOrUpdateSavedPreset");
        MFLogger.d(TAG, "addOrUpdateSavedPreset");
        savedPreset.setPinType(2);
        this.mMappingSetLocalDataSource.addOrUpdateSavedPreset(savedPreset, new Anon3(addOrUpdateSavedPresetCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "Inside .addOrUpdateSavedPresetList savedPresetListSize=" + list.size());
        this.mMappingSetLocalDataSource.addOrUpdateSavedPresetList(list, new Anon10(addOrUpdateSavedPresetListCallback));
    }

    @DexIgnore
    public void addSavedPresetObserver(SavedPresetRepositoryObserver savedPresetRepositoryObserver) {
        if (!this.mSavedPresetObservers.contains(savedPresetRepositoryObserver)) {
            this.mSavedPresetObservers.add(savedPresetRepositoryObserver);
        }
    }

    @DexIgnore
    public void addStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        if (this.mShortcutDownloadingObservers == null) {
            this.mShortcutDownloadingObservers = new CopyOnWriteArrayList();
        }
        this.mShortcutDownloadingObservers.add(shortcutDownloadingObserver);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void clearData() {
        this.mMappingSetLocalDataSource.clearData();
        this.mCachedActivePreset = null;
        this.mCachedSavedPresetList = null;
        this.mIsActivePresetCacheDirty = true;
        this.mCacheIsDirty = true;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void deleteSavedPreset(SavedPreset savedPreset, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPreset savedPresetId=" + savedPreset.getId());
        lk7.c(deleteMappingSetCallback, "deleteSavedPreset can not be null");
        if (savedPreset.getPinType() != 0) {
            String str2 = TAG;
            MFLogger.d(str2, "deleteSavedPreset PinType.TYPE_PIN_EDIT id=" + savedPreset.getId());
            ArrayList arrayList = new ArrayList();
            arrayList.add(savedPreset);
            this.mMappingSetLocalDataSource.deleteSavedPresetList(arrayList, new Anon4(savedPreset, deleteMappingSetCallback));
            return;
        }
        savedPreset.setPinType(3);
        this.mMappingSetLocalDataSource.addOrUpdateSavedPreset(savedPreset, new Anon5(deleteMappingSetCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
        MFLogger.d(TAG, "downloadActivePresetList");
        this.mMappingSetRemoteDataSource.downloadActivePresetList(new Anon13(getActivePresetListCallback));
    }

    @DexIgnore
    public void downloadDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        MFLogger.d(TAG, "Inside .downloadDefaultPreset");
        this.mMappingSetRemoteDataSource.getDefaultPreset(str, new Anon14(str, getRecommendedPresetCallback));
    }

    @DexIgnore
    public void downloadRecommendPreset(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "downloadRecommendPreset serial=" + str);
        this.mMappingSetRemoteDataSource.getRecommendedPresets(str, new Anon11(str, getRecommendedPresetListCallback));
    }

    @DexIgnore
    public void downloadSavedPresets(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "downloadSavedPresets");
        this.mMappingSetRemoteDataSource.getSavedPresetList(new Anon12(getSavedPresetListCallback));
    }

    @DexIgnore
    public void generateDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside start .generateDefaultPreset deviceSerial=" + str);
        List<ButtonMapping> a2 = new oj5(str).a();
        RecommendedPreset recommendedPreset = new RecommendedPreset();
        recommendedPreset.setName(um5.c(PortfolioApp.d0, 2131886864));
        recommendedPreset.setCreateAt(System.currentTimeMillis());
        try {
            recommendedPreset.setButtonMappingList(a2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        recommendedPreset.setType(SavedPreset.MappingSetType.DEFAULT);
        recommendedPreset.setSerialNumber(str);
        if (getRecommendedPresetCallback != null) {
            getRecommendedPresetCallback.onSuccess(recommendedPreset);
        }
        this.mAppExecutors.a().execute(new d15(this, recommendedPreset));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getActivePreset(String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        MFLogger.d(TAG, "Inside .getActivePreset");
        this.mMappingSetLocalDataSource.getActivePreset(str, new Anon7(str, getActivePresetCallback));
    }

    @DexIgnore
    public void getActivePresetFromDB(String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        MFLogger.d(TAG, "Inside .getActivePreset");
        if (this.mIsActivePresetCacheDirty || this.mCachedActivePreset == null || getActivePresetCallback == null) {
            this.mMappingSetLocalDataSource.getActivePreset(str, new Anon6(getActivePresetCallback));
            return;
        }
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getAllUserPresetsFromDB get from cache, result=" + this.mCachedActivePreset);
        getActivePresetCallback.onSuccess(this.mCachedActivePreset);
    }

    @DexIgnore
    public void getAllUserPresetsFromDB(String str, PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getAllUserPresetsFromDB deviceSerial=" + str + ",isCacheDirty=" + this.mCacheIsDirty + ", mCachedList=" + this.mCachedSavedPresetList);
        if (this.mCacheIsDirty || this.mCachedSavedPresetList == null || getSavedPresetListCallback == null) {
            this.mMappingSetLocalDataSource.getSavedPresetList(new Anon22(getSavedPresetListCallback));
            return;
        }
        String str3 = TAG;
        MFLogger.d(str3, "Inside .getAllUserPresetsFromDB get from cache, result=" + getCachedMappingSetList());
        getSavedPresetListCallback.onSuccess(getCachedMappingSetList());
    }

    @DexIgnore
    public ActivePreset getCachedActivePreset() {
        return this.mCachedActivePreset;
    }

    @DexIgnore
    public List<SavedPreset> getCachedMappingSetList() {
        Map<String, List<SavedPreset>> map = this.mCachedSavedPresetList;
        return map != null ? map.get(CACHE_MAPPING_SET_KEY) : new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getDefaultPreset deviceSerial=" + str);
        if (!TextUtils.isEmpty(str)) {
            this.mMappingSetLocalDataSource.getDefaultPreset(str, new Anon9(str, getRecommendedPresetCallback));
        } else if (getRecommendedPresetCallback != null) {
            getRecommendedPresetCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getRecommendedPresets deviceSerial=" + str);
        if (!TextUtils.isEmpty(str)) {
            this.mMappingSetLocalDataSource.getRecommendedPresets(str, new Anon8(str, getRecommendedPresetListCallback));
        } else if (getRecommendedPresetListCallback != null) {
            getRecommendedPresetListCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPreset(String str, PresetDataSource.GetSavedPresetCallback getSavedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "Inside .getSavedPreset presetId=" + str);
        this.mMappingSetLocalDataSource.getSavedPreset(str, getSavedPresetCallback);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "Inside .getSavedPresetList deviceSerial=,isCacheDirty=" + this.mCacheIsDirty + ", mCachedList=" + this.mCachedSavedPresetList);
        this.mMappingSetLocalDataSource.getSavedPresetList(new Anon1(getSavedPresetListCallback));
    }

    @DexIgnore
    public boolean isCachedActivePreset() {
        return this.mCachedActivePreset != null && !this.mIsActivePresetCacheDirty;
    }

    @DexIgnore
    public boolean isCachedMappingSetAvailable() {
        return this.mCachedSavedPresetList != null && !this.mCacheIsDirty;
    }

    @DexIgnore
    public void notifyActivePresetObserver() {
        String str = TAG;
        MFLogger.d(str, "notifyActivePresetObserver, observerSize=" + this.mActivePresetObservers.size());
        for (ActivePresetRepositoryObserver activePresetRepositoryObserver : this.mActivePresetObservers) {
            activePresetRepositoryObserver.onActivePresetChange();
        }
    }

    @DexIgnore
    public void notifySavedPresetObserver() {
        String str = TAG;
        MFLogger.d(str, "notifySavedPresetObserver, observerSize=" + this.mSavedPresetObservers.size());
        for (SavedPresetRepositoryObserver savedPresetRepositoryObserver : this.mSavedPresetObservers) {
            savedPresetRepositoryObserver.onSavedPresetChange();
        }
    }

    @DexIgnore
    public void notifyStatusChanged(String str, String str2) {
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (!(list == null || list.isEmpty())) {
            for (ShortcutDownloadingObserver shortcutDownloadingObserver : this.mShortcutDownloadingObservers) {
                shortcutDownloadingObserver.onStatusChanged(str, str2);
            }
        }
    }

    @DexIgnore
    public void processLoadedActivePreset(ActivePreset activePreset) {
        this.mIsActivePresetCacheDirty = false;
        if (activePreset == null) {
            this.mCachedActivePreset = null;
        } else {
            this.mCachedActivePreset = activePreset;
        }
    }

    @DexIgnore
    public void processLoadedMappingSetList(List<SavedPreset> list) {
        this.mCacheIsDirty = false;
        if (list == null) {
            this.mCachedSavedPresetList = null;
            return;
        }
        if (this.mCachedSavedPresetList == null) {
            this.mCachedSavedPresetList = new LinkedHashMap();
        }
        this.mCachedSavedPresetList.clear();
        this.mCachedSavedPresetList.put(CACHE_MAPPING_SET_KEY, list);
    }

    @DexIgnore
    public void pushActivePresetToServer(ActivePreset activePreset, PresetDataSource.PushActivePresetToServerCallback pushActivePresetToServerCallback) {
        this.mMappingSetRemoteDataSource.addOrUpdateActivePreset(activePreset, new Anon19(pushActivePresetToServerCallback, activePreset));
    }

    @DexIgnore
    public void pushPendingActivePresets(PresetDataSource.PushPendingActivePresetsCallback pushPendingActivePresetsCallback) {
        MFLogger.d(TAG, "pushPendingActivePresets");
        List<ActivePreset> allPendingActivePresets = ((PresetLocalDataSource) this.mMappingSetLocalDataSource).getAllPendingActivePresets();
        CountDownLatch countDownLatch = (pushPendingActivePresetsCallback == null || allPendingActivePresets.isEmpty()) ? null : new CountDownLatch(allPendingActivePresets.size());
        for (ActivePreset activePreset : allPendingActivePresets) {
            String str = TAG;
            MFLogger.d(str, "pushPendingActivePresets serial=" + activePreset.getSerialNumber() + " pinType=" + activePreset.getPinType());
            pushActivePresetToServer(activePreset, new Anon15(countDownLatch));
        }
        if (countDownLatch != null) {
            try {
                String str2 = TAG;
                MFLogger.d(str2, "Await on thread=" + Thread.currentThread().getName());
                countDownLatch.await();
                String str3 = TAG;
                MFLogger.d(str3, "Await done on thread=" + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        if (pushPendingActivePresetsCallback != null) {
            pushPendingActivePresetsCallback.onDone();
        }
    }

    @DexIgnore
    public void pushPendingSavedPresets(PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
        MFLogger.d(TAG, "pushPendingSavedPresets");
        deletePendingUserPresetList(new Anon16(pushPendingSavedPresetsCallback));
    }

    @DexIgnore
    public void pushPendingUserPresetList(PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
        MFLogger.d(TAG, "pushPendingUserPresetList");
        List<SavedPreset> pendingUserPresets = ((PresetLocalDataSource) this.mMappingSetLocalDataSource).getPendingUserPresets();
        this.mMappingSetRemoteDataSource.addOrUpdateSavedPresetList(pendingUserPresets, new Anon18(pendingUserPresets, pushPendingSavedPresetsCallback));
    }

    @DexIgnore
    public void removeActivePresetObserver(ActivePresetRepositoryObserver activePresetRepositoryObserver) {
        if (this.mActivePresetObservers.contains(activePresetRepositoryObserver)) {
            this.mActivePresetObservers.remove(activePresetRepositoryObserver);
        }
    }

    @DexIgnore
    public void removeSavedPresetObserver(SavedPresetRepositoryObserver savedPresetRepositoryObserver) {
        if (this.mSavedPresetObservers.contains(savedPresetRepositoryObserver)) {
            this.mSavedPresetObservers.remove(savedPresetRepositoryObserver);
        }
    }

    @DexIgnore
    public void removeStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list != null && list.contains(shortcutDownloadingObserver)) {
            this.mShortcutDownloadingObservers.remove(shortcutDownloadingObserver);
        }
    }

    @DexIgnore
    public void setCacheIsDirty(boolean z) {
        this.mCacheIsDirty = z;
    }

    @DexIgnore
    public void setIsActivePresetCacheDirty(boolean z) {
        this.mIsActivePresetCacheDirty = z;
    }
}
