package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.lk7;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory implements Factory<MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ MicroAppGalleryRepositoryModule module;

    @DexIgnore
    public MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        this.module = microAppGalleryRepositoryModule;
    }

    @DexIgnore
    public static MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory create(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        return new MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory(microAppGalleryRepositoryModule);
    }

    @DexIgnore
    public static MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryLocalDataSource$app_fossilRelease();
        lk7.c(provideGalleryLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppGalleryDataSource get() {
        return provideGalleryLocalDataSource$app_fossilRelease(this.module);
    }
}
