package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.rj4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "microApp")
public class MicroApp implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_APP_ID; // = "appId";
    @DexIgnore
    public static /* final */ String COLUMN_APP_SETTING; // = "appSetting";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_ICON; // = "icon";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_LIKE; // = "like";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_PLATFORM; // = "platform";
    @DexIgnore
    public static /* final */ String COLUMN_RELEASE_DATE; // = "releaseDate";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<MicroApp> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "appId")
    public String appId;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_APP_SETTING)
    public String appSettings;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    @rj4("createdAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = "description")
    @rj4("description")
    public String description;
    @DexIgnore
    @DatabaseField(columnName = "icon")
    @rj4("icon")
    public String iconUrl;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_LIKE)
    @rj4(COLUMN_LIKE)
    public int like;
    @DexIgnore
    @DatabaseField(columnName = "name")
    @rj4("name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "platform")
    @rj4("platform")
    public String platform;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_RELEASE_DATE)
    @rj4(COLUMN_RELEASE_DATE)
    public long releaseDate;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    @rj4("updatedAt")
    public long updateAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<MicroApp> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MicroApp createFromParcel(Parcel parcel) {
            return new MicroApp(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MicroApp[] newArray(int i) {
            return new MicroApp[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon2 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID;

        /*
        static {
            int[] iArr = new int[MicroAppInstruction.MicroAppID.values().length];
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID = iArr;
            try {
                iArr[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
        }
        */
    }

    @DexIgnore
    public MicroApp() {
        this.id = UUID.randomUUID().toString();
        this.appId = MicroAppInstruction.MicroAppID.UAPP_UNKNOWN.getValue();
        this.description = "";
        this.name = "";
        this.iconUrl = "";
    }

    @DexIgnore
    public MicroApp(Parcel parcel) {
        this.id = UUID.randomUUID().toString();
        this.appId = parcel.readString();
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.platform = parcel.readString();
        this.like = parcel.readInt();
        this.appSettings = parcel.readString();
        this.iconUrl = parcel.readString();
        this.releaseDate = parcel.readLong();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public String getAppSettings() {
        return this.appSettings;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public int getDefaultIconId() {
        switch (Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.appId).ordinal()]) {
            case 2:
                return 2131231171;
            case 3:
                return 2131231163;
            case 4:
                return 2131231164;
            case 5:
                return 2131231169;
            case 6:
                return 2131231167;
            case 7:
                return 2131231166;
            case 8:
                return 2131231168;
            case 9:
                return 2131231170;
            case 10:
                return 2131231174;
            case 11:
                return 2131231175;
            case 12:
                return 2131231162;
            case 13:
                return 2131231176;
            case 14:
                return 2131231172;
            case 15:
                return 2131231173;
            default:
                return 2131231104;
        }
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getIconUrl() {
        return this.iconUrl;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public int getLike() {
        return this.like;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public long getReleaseDate() {
        return this.releaseDate;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setAppId(String str) {
        this.appId = str;
    }

    @DexIgnore
    public void setAppSettings(String str) {
        this.appSettings = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setIconUrl(String str) {
        this.iconUrl = str;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setLike(int i) {
        this.like = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setPlatform(String str) {
        this.platform = str;
    }

    @DexIgnore
    public void setReleaseDate(long j) {
        this.releaseDate = j;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.appId);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.platform);
        parcel.writeInt(this.like);
        parcel.writeString(this.appSettings);
        parcel.writeString(this.iconUrl);
        parcel.writeLong(this.releaseDate);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
    }
}
