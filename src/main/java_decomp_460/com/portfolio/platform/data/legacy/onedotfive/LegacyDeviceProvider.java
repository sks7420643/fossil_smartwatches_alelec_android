package com.portfolio.platform.data.legacy.onedotfive;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface LegacyDeviceProvider extends BaseProvider {
    @DexIgnore
    List<LegacyDeviceModel> getAllDevice();

    @DexIgnore
    List<FavoriteMappingSet> getAllFavoriteMappingSet();

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    /* synthetic */ String getDbPath();

    @DexIgnore
    LegacyDeviceModel getDeviceById(String str);
}
