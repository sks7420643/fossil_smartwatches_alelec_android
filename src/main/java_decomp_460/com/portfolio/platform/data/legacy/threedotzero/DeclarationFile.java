package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.rj4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "declarationFile")
public class DeclarationFile implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_CONTENT; // = "content";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_FILE_ID; // = "fileId";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_VARIANT; // = "variant";
    @DexIgnore
    public static /* final */ Parcelable.Creator<DeclarationFile> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "content")
    @rj4("content")
    public String content;
    @DexIgnore
    @DatabaseField(columnName = "description")
    @rj4("description")
    public String description;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_FILE_ID)
    @rj4(COLUMN_FILE_ID)
    public String fileId;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    @rj4("id")
    public String id;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_VARIANT, foreign = true, foreignAutoRefresh = true)
    public MicroAppVariant microAppVariant;
    @DexIgnore
    public String microAppVariantId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<DeclarationFile> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DeclarationFile createFromParcel(Parcel parcel) {
            return new DeclarationFile(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DeclarationFile[] newArray(int i) {
            return new DeclarationFile[i];
        }
    }

    @DexIgnore
    public DeclarationFile() {
        this.id = UUID.randomUUID().toString();
    }

    @DexIgnore
    public DeclarationFile(Parcel parcel) {
        this.id = parcel.readString();
        this.fileId = parcel.readString();
        this.description = parcel.readString();
        this.content = parcel.readString();
        this.content = parcel.readString();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getContent() {
        return this.content;
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getFileId() {
        return this.fileId;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public MicroAppVariant getMicroAppVariant() {
        return this.microAppVariant;
    }

    @DexIgnore
    public String getMicroAppVariantId() {
        return this.microAppVariantId;
    }

    @DexIgnore
    public void setContent(String str) {
        this.content = str;
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setFileId(String str) {
        this.fileId = str;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setMicroAppVariant(MicroAppVariant microAppVariant2) {
        this.microAppVariant = microAppVariant2;
    }

    @DexIgnore
    public void setMicroAppVariantId(String str) {
        this.microAppVariantId = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.fileId);
        parcel.writeString(this.description);
        parcel.writeString(this.content);
    }
}
