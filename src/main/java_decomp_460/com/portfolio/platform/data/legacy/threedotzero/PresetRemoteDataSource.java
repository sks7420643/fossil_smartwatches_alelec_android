package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.bj4;
import com.fossil.c88;
import com.fossil.gj4;
import com.fossil.mq5;
import com.fossil.no4;
import com.fossil.q88;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PresetRemoteDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ no4 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements c88<gj4> {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$savedPresets;

        @DexIgnore
        public Anon1(List list, PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$savedPresets = list;
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onFailure(Call<gj4> call, Throwable th) {
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure");
            if (!this.val$savedPresets.isEmpty()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList not null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                if (getSavedPresetListCallback != null) {
                    getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                    return;
                }
                return;
            }
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList is null");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
            if (getSavedPresetListCallback2 != null) {
                getSavedPresetListCallback2.onFail();
            }
        }

        @DexIgnore
        @Override // com.fossil.c88
        public void onResponse(Call<gj4> call, q88<gj4> q88) {
            if (!q88.e() || q88.a() == null) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful");
                if (!this.val$savedPresets.isEmpty()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList not null");
                    PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                    if (getSavedPresetListCallback != null) {
                        getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                        return;
                    }
                    return;
                }
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList is null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
                if (getSavedPresetListCallback2 != null) {
                    getSavedPresetListCallback2.onFail();
                    return;
                }
                return;
            }
            mq5 mq5 = new mq5();
            mq5.c(q88.a());
            this.val$savedPresets.addAll(mq5.b());
            Range a2 = mq5.a();
            if (a2 != null && a2.isHasNext()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=true");
                PresetRemoteDataSource.this.getUserPresets(a2.getOffset() + a2.getLimit() + 1, a2.getLimit(), this);
            } else if (this.val$callback != null) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=false");
                if (!this.val$savedPresets.isEmpty()) {
                    this.val$callback.onSuccess(this.val$savedPresets);
                } else {
                    this.val$callback.onFail();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon2(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
            this.val$savedPreset = savedPreset;
            this.val$callback = addOrUpdateSavedPresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onFail() {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onFail mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onSuccess mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            if (!list.isEmpty()) {
                PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
                if (addOrUpdateSavedPresetCallback != null) {
                    addOrUpdateSavedPresetCallback.onSuccess(list.get(0));
                    return;
                }
                return;
            }
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback2 = this.val$callback;
            if (addOrUpdateSavedPresetCallback2 != null) {
                addOrUpdateSavedPresetCallback2.onFail();
            }
        }
    }

    @DexIgnore
    public PresetRemoteDataSource(ShortcutApiService shortcutApiService, no4 no4) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = no4;
    }

    @DexIgnore
    private gj4 createListItemJsonObject(bj4 bj4) {
        gj4 gj4 = new gj4();
        gj4.k(CloudLogWriter.ITEMS_PARAM, bj4);
        return gj4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serialNumber=" + activePreset.getSerialNumber() + " originalId=" + activePreset.getOriginalId());
        bj4 bj4 = new bj4();
        bj4.k(activePreset.getJsonObject());
        gj4 createListItemJsonObject = createListItemJsonObject(bj4);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset mappingSetId=" + savedPreset.getId() + " mappingSetName=" + savedPreset.getName());
        ArrayList arrayList = new ArrayList();
        arrayList.add(savedPreset);
        addOrUpdateSavedPresetList(arrayList, new Anon2(savedPreset, addOrUpdateSavedPresetCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetListSize=" + list.size());
        bj4 bj4 = new bj4();
        for (SavedPreset savedPreset : list) {
            bj4.k(savedPreset.getJsonObject());
        }
        gj4 createListItemJsonObject = createListItemJsonObject(bj4);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void clearData() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList presetListSize=" + list.size());
        bj4 bj4 = new bj4();
        try {
            for (SavedPreset savedPreset : list) {
                gj4 gj4 = new gj4();
                gj4.n("id", savedPreset.getId());
                bj4.k(gj4);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        createListItemJsonObject(bj4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        getUserPresets(0, 100, new Anon1(new ArrayList(), getSavedPresetListCallback));
    }

    @DexIgnore
    public void getUserPresets(int i, int i2, c88<gj4> c88) {
    }
}
