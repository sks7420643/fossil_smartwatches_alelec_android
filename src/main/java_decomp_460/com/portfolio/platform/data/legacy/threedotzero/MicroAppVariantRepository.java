package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.hm7;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.no4;
import com.fossil.pq5;
import com.fossil.pq7;
import com.fossil.um7;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppVariantRepository extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ no4 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantRemoteDataSource;
    @DexIgnore
    public List<ShortcutDownloadingObserver> mShortcutDownloadingObservers;
    @DexIgnore
    public /* final */ UAppSystemVersionRepository mUAppSystemVersionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            pq7.c(str, "<set-?>");
            MicroAppVariantRepository.TAG = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[MicroAppInstruction.MicroAppID.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = MicroAppVariantRepository.class.getSimpleName();
        pq7.b(simpleName, "MicroAppVariantRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRepository(@Remote MicroAppVariantDataSource microAppVariantDataSource, @Local MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, no4 no4) {
        pq7.c(microAppVariantDataSource, "mMicroAppVariantRemoteDataSource");
        pq7.c(microAppVariantDataSource2, "mMicroAppVariantLocalDataSource");
        pq7.c(uAppSystemVersionRepository, "mUAppSystemVersionRepository");
        pq7.c(no4, "mAppExecutors");
        this.mMicroAppVariantRemoteDataSource = microAppVariantDataSource;
        this.mMicroAppVariantLocalDataSource = microAppVariantDataSource2;
        this.mUAppSystemVersionRepository = uAppSystemVersionRepository;
        this.mAppExecutors = no4;
    }

    @DexIgnore
    private final void saveDeclarationFileList(List<? extends DeclarationFile> list, MicroAppVariant microAppVariant) {
        for (DeclarationFile declarationFile : list) {
            declarationFile.setMicroAppVariant(microAppVariant);
            addOrUpdateDeclarationFile(declarationFile, new MicroAppVariantRepository$saveDeclarationFileList$Anon1());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        pq7.c(declarationFile, "declarationFile");
        pq7.c(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile fileId=" + declarationFile.getId());
        this.mMicroAppVariantLocalDataSource.addOrUpdateDeclarationFile(declarationFile, addOrUpdateDeclarationFileCallback);
    }

    @DexIgnore
    public final void addStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        pq7.c(shortcutDownloadingObserver, "observer");
        if (this.mShortcutDownloadingObservers == null) {
            this.mShortcutDownloadingObservers = new CopyOnWriteArrayList();
        }
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list != null) {
            list.add(shortcutDownloadingObserver);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public void downloadAllVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        pq7.c(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$downloadAllVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final ArrayList<pq5> filterVariantList$app_fossilRelease(List<? extends pq5> list) {
        pq7.c(list, "microAppVariants");
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Iterator it = hm7.f(list).iterator();
            pq5 pq5 = (pq5) list.get(i);
            while (it.hasNext()) {
                pq5 pq52 = (pq5) list.get(((um7) it).b());
                if (pq7.a(pq52.b(), pq5.b()) && pq7.a(pq52.e(), pq5.e()) && pq52.d() > pq5.d()) {
                    pq5 = pq52;
                }
            }
            hashMap.put(pq5.b() + pq5.e(), pq5);
        }
        return new ArrayList<>(hashMap.values());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        pq7.c(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str + " major=" + i + " minor=" + i2);
        this.mMicroAppVariantLocalDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$getAllMicroAppVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        pq7.c(str, "serialNumber");
        pq7.c(str2, "microAppId");
        pq7.c(str3, "variantName");
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppVariant serialNumber=" + str + "microAppId=" + str2 + " major=" + i + " minor=" + i2 + " name=" + str3);
        this.mMicroAppVariantLocalDataSource.getMicroAppVariant(str, str2, str3, i, i2, new MicroAppVariantRepository$getMicroAppVariant$Anon1(this, str, i, i2, getVariantCallback, str2, str3));
    }

    @DexIgnore
    public final void migrateMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        pq7.c(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$migrateMicroAppVariants$Anon1(this, str, i, i2, migrateVariantCallback));
    }

    @DexIgnore
    public final void notifyStatusChanged(String str, String str2) {
        pq7.c(str, "status");
        pq7.c(str2, "serial");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            return;
        }
        if (list == null) {
            pq7.i();
            throw null;
        } else if (!list.isEmpty()) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                Iterator<T> it = list2.iterator();
                while (it.hasNext()) {
                    it.next().onStatusChanged(str, str2);
                }
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void removeStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        pq7.c(shortcutDownloadingObserver, "observer");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            pq7.i();
            throw null;
        } else if (list.contains(shortcutDownloadingObserver)) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                list2.remove(shortcutDownloadingObserver);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void saveMicroAppVariant$app_fossilRelease(String str, List<? extends pq5> list) {
        pq7.c(str, "serialNumber");
        pq7.c(list, "filter");
        for (pq5 pq5 : list) {
            pq5.m(str);
            MicroAppVariant a2 = pq5.a();
            pq7.b(a2, "parser.convertToMicroAppVariant()");
            String str2 = TAG;
            MFLogger.d(str2, "saveMicroAppVariant variantId=" + a2.getId());
            MicroAppVariantDataSource microAppVariantDataSource = this.mMicroAppVariantLocalDataSource;
            if (microAppVariantDataSource != null) {
                ((MicroAppVariantLocalDataSource) microAppVariantDataSource).addOrUpDateVariant(a2);
                List<DeclarationFile> c = pq5.c();
                pq7.b(c, "declarationFileList");
                saveDeclarationFileList(c, a2);
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantLocalDataSource");
            }
        }
    }
}
