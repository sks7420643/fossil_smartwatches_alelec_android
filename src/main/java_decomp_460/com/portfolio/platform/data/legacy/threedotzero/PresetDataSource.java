package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class PresetDataSource {

    @DexIgnore
    public interface AddOrUpdateActivePresetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(ActivePreset activePreset);
    }

    @DexIgnore
    public interface AddOrUpdateSavedPresetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(SavedPreset savedPreset);
    }

    @DexIgnore
    public interface AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<SavedPreset> list);
    }

    @DexIgnore
    public interface DeleteMappingSetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        Object onSuccess();  // void declaration
    }

    @DexIgnore
    public interface DeleteSavedPresetInServerCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        Object onSuccess();  // void declaration
    }

    @DexIgnore
    public interface GetActivePresetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(ActivePreset activePreset);
    }

    @DexIgnore
    public interface GetActivePresetListCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<ActivePreset> list);
    }

    @DexIgnore
    public interface GetRecommendedPresetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(RecommendedPreset recommendedPreset);
    }

    @DexIgnore
    public interface GetRecommendedPresetListCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<RecommendedPreset> list);
    }

    @DexIgnore
    public interface GetSavedPresetCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(SavedPreset savedPreset);
    }

    @DexIgnore
    public interface GetSavedPresetListCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<SavedPreset> list);
    }

    @DexIgnore
    public interface PushActivePresetToServerCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        Object onSuccess();  // void declaration
    }

    @DexIgnore
    public interface PushPendingActivePresetsCallback {
        @DexIgnore
        Object onDone();  // void declaration
    }

    @DexIgnore
    public interface PushPendingSavedPresetsCallback {
        @DexIgnore
        Object onDone();  // void declaration
    }

    @DexIgnore
    public interface PushSavedPresetToServerCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        Object onSuccess();  // void declaration
    }

    @DexIgnore
    public abstract void addOrUpdateActivePreset(ActivePreset activePreset, AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback);

    @DexIgnore
    public abstract void addOrUpdateSavedPreset(SavedPreset savedPreset, AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback);

    @DexIgnore
    public abstract void addOrUpdateSavedPresetList(List<SavedPreset> list, AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback);

    @DexIgnore
    public abstract void clearData();

    @DexIgnore
    public void deleteSavedPreset(SavedPreset savedPreset, DeleteMappingSetCallback deleteMappingSetCallback) {
        pq7.c(savedPreset, "savedPreset");
        pq7.c(deleteMappingSetCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public void deleteSavedPresetList(List<SavedPreset> list, DeleteMappingSetCallback deleteMappingSetCallback) {
        pq7.c(list, "savedPresetList");
    }

    @DexIgnore
    public abstract void downloadActivePresetList(GetActivePresetListCallback getActivePresetListCallback);

    @DexIgnore
    public void getActivePreset(String str, GetActivePresetCallback getActivePresetCallback) {
        pq7.c(str, "serial");
    }

    @DexIgnore
    public abstract void getDefaultPreset(String str, GetRecommendedPresetCallback getRecommendedPresetCallback);

    @DexIgnore
    public abstract void getRecommendedPresets(String str, GetRecommendedPresetListCallback getRecommendedPresetListCallback);

    @DexIgnore
    public void getSavedPreset(String str, GetSavedPresetCallback getSavedPresetCallback) {
        pq7.c(str, "savedPresetId");
        pq7.c(getSavedPresetCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public abstract void getSavedPresetList(GetSavedPresetListCallback getSavedPresetListCallback);
}
