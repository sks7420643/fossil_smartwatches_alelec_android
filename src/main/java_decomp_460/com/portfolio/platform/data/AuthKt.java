package com.portfolio.platform.data;

import com.portfolio.platform.data.model.MFUser;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AuthKt {
    @DexIgnore
    public static final MFUser.Auth toAuth(Auth auth) {
        String str;
        if (auth == null) {
            return null;
        }
        String accessToken = auth.getAccessToken();
        if (accessToken == null) {
            accessToken = "";
        }
        String refreshToken = auth.getRefreshToken();
        if (refreshToken == null) {
            refreshToken = "";
        }
        Date accessTokenExpiresAt = auth.getAccessTokenExpiresAt();
        if (accessTokenExpiresAt == null || (str = accessTokenExpiresAt.toString()) == null) {
            str = "";
        }
        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
        return new MFUser.Auth(accessToken, refreshToken, str, accessTokenExpiresIn != null ? accessTokenExpiresIn.intValue() : 0);
    }
}
