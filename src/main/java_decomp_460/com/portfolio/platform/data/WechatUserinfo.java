package com.portfolio.platform.data;

import com.facebook.appevents.UserDataStore;
import com.fossil.pq7;
import com.fossil.qh5;
import com.fossil.rj4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatUserinfo {
    @DexIgnore
    @rj4("city")
    public /* final */ String mCity;
    @DexIgnore
    @rj4(UserDataStore.COUNTRY)
    public /* final */ String mCountry;
    @DexIgnore
    @rj4("headimgurl")
    public /* final */ String mHeadimgurl;
    @DexIgnore
    @rj4("nickname")
    public /* final */ String mNickname;
    @DexIgnore
    @rj4("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @rj4("privilege")
    public /* final */ List<String> mPrivilege;
    @DexIgnore
    @rj4("province")
    public /* final */ String mProvince;
    @DexIgnore
    @rj4("sex")
    public /* final */ int mSex;
    @DexIgnore
    @rj4("unionId")
    public /* final */ String mUnionId;

    @DexIgnore
    public WechatUserinfo(String str, String str2, int i, String str3, String str4, String str5, String str6, List<String> list, String str7) {
        pq7.c(str, "mOpenid");
        pq7.c(str2, "mNickname");
        pq7.c(str3, "mCity");
        pq7.c(str4, "mProvince");
        pq7.c(str5, "mCountry");
        pq7.c(str6, "mHeadimgurl");
        pq7.c(list, "mPrivilege");
        pq7.c(str7, "mUnionId");
        this.mOpenid = str;
        this.mNickname = str2;
        this.mSex = i;
        this.mCity = str3;
        this.mProvince = str4;
        this.mCountry = str5;
        this.mHeadimgurl = str6;
        this.mPrivilege = list;
        this.mUnionId = str7;
    }

    @DexIgnore
    public final String getHeadimgurl() {
        return this.mHeadimgurl;
    }

    @DexIgnore
    public final String getSex() {
        int i = this.mSex;
        return i != 1 ? i != 2 ? qh5.OTHER.toString() : qh5.FEMALE.toString() : qh5.MALE.toString();
    }
}
