package com.portfolio.platform.data;

import android.util.Base64;
import com.facebook.places.PlaceManager;
import com.fossil.fitness.GpsDataEncoder;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.gi5;
import com.fossil.im7;
import com.fossil.ji5;
import com.fossil.kq7;
import com.fossil.mi5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.tl7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerWorkoutSession extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    @rj4("cadence")
    public /* final */ WorkoutCadence cadence;
    @DexIgnore
    @rj4(MFSleepSession.COLUMN_EDITED_END_TIME)
    public /* final */ String editedEndTime;
    @DexIgnore
    @rj4("editedMode")
    public /* final */ gi5 editedMode;
    @DexIgnore
    @rj4(MFSleepSession.COLUMN_EDITED_START_TIME)
    public /* final */ String editedStartTime;
    @DexIgnore
    @rj4("editedType")
    public /* final */ mi5 editedType;
    @DexIgnore
    @rj4("gpsDataPoints")
    public /* final */ String gpsDataPoints;
    @DexIgnore
    @rj4("calorie")
    public /* final */ WorkoutCalorie mCalorie;
    @DexIgnore
    @rj4("createdAt")
    public /* final */ DateTime mCreatedAt;
    @DexIgnore
    @rj4("date")
    public /* final */ Date mDate;
    @DexIgnore
    @rj4(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER)
    public /* final */ String mDeviceSerialNumber;
    @DexIgnore
    @rj4("distance")
    public /* final */ WorkoutDistance mDistance;
    @DexIgnore
    @rj4("duration")
    public /* final */ int mDuration;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @rj4("heartRate")
    public /* final */ WorkoutHeartRate mHeartRate;
    @DexIgnore
    @rj4("id")
    public /* final */ String mId;
    @DexIgnore
    @rj4("source")
    public /* final */ ji5 mSourceType;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @rj4("step")
    public /* final */ WorkoutStep mStep;
    @DexIgnore
    @rj4("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @rj4("updatedAt")
    public /* final */ DateTime mUpdatedAt;
    @DexIgnore
    @rj4("type")
    public /* final */ mi5 mWorkoutType;
    @DexIgnore
    @rj4("mode")
    public /* final */ gi5 mode;
    @DexIgnore
    @rj4("pace")
    public /* final */ WorkoutPace pace;
    @DexIgnore
    @rj4(PlaceManager.PARAM_SPEED)
    public WorkoutSpeed speed;
    @DexIgnore
    @rj4("states")
    public List<WorkoutStateChange> states;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerWorkoutSession.class.getSimpleName();
        pq7.b(simpleName, "ServerWorkoutSession::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, ji5 ji5, WorkoutStep workoutStep, int i, mi5 mi5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List<WorkoutStateChange> list, String str5, String str6, mi5 mi52, gi5 gi5, String str7, gi5 gi52, WorkoutPace workoutPace, WorkoutCadence workoutCadence) {
        pq7.c(str, "mId");
        pq7.c(date, "mDate");
        pq7.c(str3, "mStartTime");
        pq7.c(str4, "mEndTime");
        pq7.c(dateTime, "mCreatedAt");
        pq7.c(dateTime2, "mUpdatedAt");
        pq7.c(list, "states");
        this.mId = str;
        this.mCalorie = workoutCalorie;
        this.mDate = date;
        this.mDeviceSerialNumber = str2;
        this.mDistance = workoutDistance;
        this.mStartTime = str3;
        this.mEndTime = str4;
        this.mHeartRate = workoutHeartRate;
        this.mSourceType = ji5;
        this.mStep = workoutStep;
        this.mTimeZoneOffsetInSecond = i;
        this.mWorkoutType = mi5;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
        this.mDuration = i2;
        this.speed = workoutSpeed;
        this.states = list;
        this.editedStartTime = str5;
        this.editedEndTime = str6;
        this.editedType = mi52;
        this.editedMode = gi5;
        this.gpsDataPoints = str7;
        this.mode = gi52;
        this.pace = workoutPace;
        this.cadence = workoutCadence;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, ji5 ji5, WorkoutStep workoutStep, int i, mi5 mi5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List list, String str5, String str6, mi5 mi52, gi5 gi5, String str7, gi5 gi52, WorkoutPace workoutPace, WorkoutCadence workoutCadence, int i3, kq7 kq7) {
        this(str, workoutCalorie, date, str2, workoutDistance, str3, str4, workoutHeartRate, ji5, workoutStep, i, mi5, dateTime, dateTime2, i2, workoutSpeed, (65536 & i3) != 0 ? new ArrayList() : list, str5, str6, mi52, gi5, str7, gi52, workoutPace, workoutCadence);
    }

    @DexIgnore
    public final WorkoutCadence getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final String getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final gi5 getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final String getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final mi5 getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final WorkoutCalorie getMCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMDeviceSerialNumber() {
        return this.mDeviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getMDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public final int getMDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public final String getMEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getMHeartRate() {
        return this.mHeartRate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final ji5 getMSourceType() {
        return this.mSourceType;
    }

    @DexIgnore
    public final String getMStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public final WorkoutStep getMStep() {
        return this.mStep;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final mi5 getMWorkoutType() {
        return this.mWorkoutType;
    }

    @DexIgnore
    public final gi5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace getPace() {
        return this.pace;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        pq7.c(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public final WorkoutSession toWorkoutSession() {
        WorkoutSession workoutSession;
        Exception e;
        DateTime dateTime;
        DateTime dateTime2;
        List list;
        try {
            DateTimeFormatter withZone = ISODateTimeFormat.dateTime().withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            DateTime parseDateTime = withZone.parseDateTime(this.mStartTime);
            DateTime parseDateTime2 = withZone.parseDateTime(this.mEndTime);
            if (this.editedStartTime == null || (dateTime = withZone.parseDateTime(this.editedStartTime)) == null) {
                dateTime = parseDateTime;
            }
            if (this.editedEndTime == null || (dateTime2 = withZone.parseDateTime(this.editedEndTime)) == null) {
                dateTime2 = parseDateTime2;
            }
            mi5 mi5 = this.editedType;
            if (mi5 == null) {
                mi5 = this.mWorkoutType;
            }
            gi5 gi5 = this.editedMode;
            if (gi5 == null) {
                gi5 = this.mode;
            }
            String str = this.gpsDataPoints;
            if (str != null) {
                byte[] decode = Base64.decode(str, 0);
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.APP;
                FLogger.Session session = FLogger.Session.SYNC;
                StringBuilder sb = new StringBuilder();
                sb.append("[Calculate GPS Points] Encoded GPS string from server ");
                sb.append(decode != null ? Integer.valueOf(decode.length) : null);
                sb.append(' ');
                remote.i(component, session, "", "WorkoutSessionWrapper", sb.toString());
                ArrayList<GpsDataPoint> decode2 = GpsDataEncoder.decode(decode);
                pq7.b(decode2, "GpsDataEncoder.decode(encodedRawData)");
                ArrayList arrayList = new ArrayList(im7.m(decode2, 10));
                for (T t : decode2) {
                    pq7.b(t, "it");
                    arrayList.add(new WorkoutGpsPoint(new GpsDataPointWrapper(t, this.mTimeZoneOffsetInSecond)));
                }
                list = pm7.j0(arrayList);
            } else {
                list = null;
            }
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.APP;
            FLogger.Session session2 = FLogger.Session.SYNC;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("[Calculate GPS Points] GPS points size after decrypt from server ");
            sb2.append(list != null ? Integer.valueOf(list.size()) : null);
            sb2.append(' ');
            remote2.i(component2, session2, "", "WorkoutSessionWrapper", sb2.toString());
            String str2 = this.mId;
            Date date = this.mDate;
            pq7.b(parseDateTime, SampleRaw.COLUMN_START_TIME);
            pq7.b(parseDateTime2, SampleRaw.COLUMN_END_TIME);
            String str3 = this.mDeviceSerialNumber;
            WorkoutStep workoutStep = this.mStep;
            WorkoutCalorie workoutCalorie = this.mCalorie;
            WorkoutDistance workoutDistance = this.mDistance;
            WorkoutHeartRate workoutHeartRate = this.mHeartRate;
            ji5 ji5 = this.mSourceType;
            mi5 mi52 = this.mWorkoutType;
            int i = this.mTimeZoneOffsetInSecond;
            int i2 = this.mDuration;
            long millis = this.mCreatedAt.getMillis();
            long millis2 = this.mUpdatedAt.getMillis();
            String str4 = this.gpsDataPoints;
            gi5 gi52 = this.mode;
            WorkoutPace workoutPace = this.pace;
            WorkoutCadence workoutCadence = this.cadence;
            pq7.b(dateTime, MFSleepSession.COLUMN_EDITED_START_TIME);
            pq7.b(dateTime2, MFSleepSession.COLUMN_EDITED_END_TIME);
            workoutSession = new WorkoutSession(str2, date, parseDateTime, parseDateTime2, str3, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, ji5, mi52, i, i2, millis, millis2, list, str4, gi52, workoutPace, workoutCadence, dateTime, dateTime2, mi5, gi5);
            try {
                try {
                    workoutSession.setSpeed(this.speed);
                    workoutSession.getStates().addAll(this.states);
                } catch (Exception e2) {
                    e = e2;
                }
            } catch (Exception e3) {
                e = e3;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("toWorkoutSession exception=");
                e.printStackTrace();
                sb3.append(tl7.f3441a);
                local.d(str5, sb3.toString());
                e.printStackTrace();
                return workoutSession;
            }
        } catch (Exception e4) {
            e = e4;
            workoutSession = null;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str52 = TAG;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb32.append(tl7.f3441a);
            local2.d(str52, sb32.toString());
            e.printStackTrace();
            return workoutSession;
        }
        return workoutSession;
    }
}
