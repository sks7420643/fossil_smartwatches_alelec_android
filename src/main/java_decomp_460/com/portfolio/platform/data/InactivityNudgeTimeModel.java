package com.portfolio.platform.data;

import com.fossil.pj4;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimeModel {
    @DexIgnore
    @pj4
    public int minutes;
    @DexIgnore
    @pj4
    public String nudgeTimeName;
    @DexIgnore
    @pj4
    public int nudgeTimeType;

    @DexIgnore
    public InactivityNudgeTimeModel(String str, int i, int i2) {
        pq7.c(str, "nudgeTimeName");
        this.nudgeTimeName = str;
        this.minutes = i;
        this.nudgeTimeType = i2;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getNudgeTimeName() {
        return this.nudgeTimeName;
    }

    @DexIgnore
    public final int getNudgeTimeType() {
        return this.nudgeTimeType;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setNudgeTimeName(String str) {
        pq7.c(str, "<set-?>");
        this.nudgeTimeName = str;
    }

    @DexIgnore
    public final void setNudgeTimeType(int i) {
        this.nudgeTimeType = i;
    }
}
