package com.portfolio.platform.data;

import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppleAuth {
    @DexIgnore
    public /* final */ String email;
    @DexIgnore
    public /* final */ Name name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Name {
        @DexIgnore
        public /* final */ String firstName;
        @DexIgnore
        public /* final */ String lastName;
        @DexIgnore
        public /* final */ String middleName;

        @DexIgnore
        public Name(String str, String str2, String str3) {
            pq7.c(str, Constants.PROFILE_KEY_FIRST_NAME);
            pq7.c(str2, "middleName");
            pq7.c(str3, Constants.PROFILE_KEY_LAST_NAME);
            this.firstName = str;
            this.middleName = str2;
            this.lastName = str3;
        }

        @DexIgnore
        public static /* synthetic */ Name copy$default(Name name, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = name.firstName;
            }
            if ((i & 2) != 0) {
                str2 = name.middleName;
            }
            if ((i & 4) != 0) {
                str3 = name.lastName;
            }
            return name.copy(str, str2, str3);
        }

        @DexIgnore
        public final String component1() {
            return this.firstName;
        }

        @DexIgnore
        public final String component2() {
            return this.middleName;
        }

        @DexIgnore
        public final String component3() {
            return this.lastName;
        }

        @DexIgnore
        public final Name copy(String str, String str2, String str3) {
            pq7.c(str, Constants.PROFILE_KEY_FIRST_NAME);
            pq7.c(str2, "middleName");
            pq7.c(str3, Constants.PROFILE_KEY_LAST_NAME);
            return new Name(str, str2, str3);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Name) {
                    Name name = (Name) obj;
                    if (!pq7.a(this.firstName, name.firstName) || !pq7.a(this.middleName, name.middleName) || !pq7.a(this.lastName, name.lastName)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getFirstName() {
            return this.firstName;
        }

        @DexIgnore
        public final String getLastName() {
            return this.lastName;
        }

        @DexIgnore
        public final String getMiddleName() {
            return this.middleName;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.firstName;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.middleName;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.lastName;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return (((hashCode * 31) + hashCode2) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Name(firstName=" + this.firstName + ", middleName=" + this.middleName + ", lastName=" + this.lastName + ")";
        }
    }

    @DexIgnore
    public AppleAuth(String str, Name name2) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(name2, "name");
        this.email = str;
        this.name = name2;
    }

    @DexIgnore
    public static /* synthetic */ AppleAuth copy$default(AppleAuth appleAuth, String str, Name name2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = appleAuth.email;
        }
        if ((i & 2) != 0) {
            name2 = appleAuth.name;
        }
        return appleAuth.copy(str, name2);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final Name component2() {
        return this.name;
    }

    @DexIgnore
    public final AppleAuth copy(String str, Name name2) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(name2, "name");
        return new AppleAuth(str, name2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AppleAuth) {
                AppleAuth appleAuth = (AppleAuth) obj;
                if (!pq7.a(this.email, appleAuth.email) || !pq7.a(this.name, appleAuth.name)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final Name getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.email;
        int hashCode = str != null ? str.hashCode() : 0;
        Name name2 = this.name;
        if (name2 != null) {
            i = name2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "AppleAuth(email=" + this.email + ", name=" + this.name + ")";
    }
}
