package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.cl7;
import com.fossil.im7;
import com.fossil.lk5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataWrapperKt {
    @DexIgnore
    public static final cl7<Date, Date> calculateRangeDownload(List<FitnessDataWrapper> list, Date date, Date date2) {
        pq7.c(list, "$this$calculateRangeDownload");
        pq7.c(date, GoalPhase.COLUMN_START_DATE);
        pq7.c(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new cl7<>(date, date2);
        }
        if (!lk5.m0(((FitnessDataWrapper) pm7.P(list)).getStartTimeTZ().toDate(), date2)) {
            return new cl7<>(((FitnessDataWrapper) pm7.P(list)).getStartTimeTZ().toDate(), date2);
        }
        if (lk5.m0(((FitnessDataWrapper) pm7.F(list)).getStartTimeTZ().toDate(), date)) {
            return null;
        }
        return new cl7<>(date, lk5.P(((FitnessDataWrapper) pm7.F(list)).getStartTimeTZ().toDate()));
    }

    @DexIgnore
    public static final List<ServerFitnessDataWrapper> toServerFitnessDataWrapperList(List<FitnessDataWrapper> list) {
        pq7.c(list, "$this$toServerFitnessDataWrapperList");
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        for (T t : list) {
            arrayList.add(new ServerFitnessDataWrapper(t.getStartTime(), t.getEndTime(), t.getSyncTime(), t.getTimezoneOffsetInSecond(), t.getSerialNumber(), t.getStep(), t.getActiveMinute(), t.getCalorie(), t.getDistance(), t.getStress(), t.getResting(), t.getHeartRate(), t.getSleeps(), WorkoutSessionWrapperKt.toServerWorkoutSessionWrapperList(t.getWorkouts()), t.getGoalTrackings()));
        }
        return arrayList;
    }
}
