package com.portfolio.platform.data.model;

import com.fossil.kq7;
import com.fossil.rj4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = ServerSetting.TABLE_NAME)
public final class ServerSetting {
    @DexIgnore
    public static /* final */ String CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String KEY; // = "key";
    @DexIgnore
    public static /* final */ String OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "SERVER_SETTING";
    @DexIgnore
    public static /* final */ String UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String VALUE; // = "value";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    @rj4("createdAt")
    public String createdAt;
    @DexIgnore
    @DatabaseField(columnName = "key", id = true)
    @rj4("key")
    public String key;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    @rj4("id")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    @rj4("updatedAt")
    public String updateAt;
    @DexIgnore
    @DatabaseField(columnName = "value")
    @rj4("value")
    public String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getKey() {
        return this.key;
    }

    @DexIgnore
    public final String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setKey(String str) {
        this.key = str;
    }

    @DexIgnore
    public final void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setValue(String str) {
        this.value = str;
    }

    @DexIgnore
    public String toString() {
        return "key: " + this.key + " - value: " + this.value;
    }
}
