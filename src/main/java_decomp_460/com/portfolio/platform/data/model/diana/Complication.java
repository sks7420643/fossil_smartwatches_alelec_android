package com.portfolio.platform.data.model.diana;

import com.fossil.kq7;
import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.rj4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Complication {
    @DexIgnore
    @pj4
    @rj4("categoryIds")
    public ArrayList<String> categories;
    @DexIgnore
    @pj4
    @rj4("id")
    public String complicationId;
    @DexIgnore
    @pj4
    @rj4("createdAt")
    public String createdAt;
    @DexIgnore
    @pj4
    @rj4("englishDescription")
    public String description;
    @DexIgnore
    @pj4
    @rj4("description")
    public String descriptionKey;
    @DexIgnore
    @pj4
    public String icon;
    @DexIgnore
    @pj4
    @rj4("englishName")
    public String name;
    @DexIgnore
    @pj4
    @rj4("name")
    public String nameKey;
    @DexIgnore
    @pj4
    @rj4("updatedAt")
    public String updatedAt;

    @DexIgnore
    public Complication(String str, String str2, String str3, ArrayList<String> arrayList, String str4, String str5, String str6, String str7, String str8) {
        pq7.c(str, "complicationId");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(arrayList, "categories");
        pq7.c(str4, "description");
        pq7.c(str5, "descriptionKey");
        pq7.c(str7, "createdAt");
        pq7.c(str8, "updatedAt");
        this.complicationId = str;
        this.name = str2;
        this.nameKey = str3;
        this.categories = arrayList;
        this.description = str4;
        this.descriptionKey = str5;
        this.icon = str6;
        this.createdAt = str7;
        this.updatedAt = str8;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Complication(String str, String str2, String str3, ArrayList arrayList, String str4, String str5, String str6, String str7, String str8, int i, kq7 kq7) {
        this(str, str2, str3, arrayList, str4, str5, (i & 64) != 0 ? "" : str6, str7, str8);
    }

    @DexIgnore
    public static /* synthetic */ Complication copy$default(Complication complication, String str, String str2, String str3, ArrayList arrayList, String str4, String str5, String str6, String str7, String str8, int i, Object obj) {
        return complication.copy((i & 1) != 0 ? complication.complicationId : str, (i & 2) != 0 ? complication.name : str2, (i & 4) != 0 ? complication.nameKey : str3, (i & 8) != 0 ? complication.categories : arrayList, (i & 16) != 0 ? complication.description : str4, (i & 32) != 0 ? complication.descriptionKey : str5, (i & 64) != 0 ? complication.icon : str6, (i & 128) != 0 ? complication.createdAt : str7, (i & 256) != 0 ? complication.updatedAt : str8);
    }

    @DexIgnore
    public final String component1() {
        return this.complicationId;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final ArrayList<String> component4() {
        return this.categories;
    }

    @DexIgnore
    public final String component5() {
        return this.description;
    }

    @DexIgnore
    public final String component6() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String component7() {
        return this.icon;
    }

    @DexIgnore
    public final String component8() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Complication copy(String str, String str2, String str3, ArrayList<String> arrayList, String str4, String str5, String str6, String str7, String str8) {
        pq7.c(str, "complicationId");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(arrayList, "categories");
        pq7.c(str4, "description");
        pq7.c(str5, "descriptionKey");
        pq7.c(str7, "createdAt");
        pq7.c(str8, "updatedAt");
        return new Complication(str, str2, str3, arrayList, str4, str5, str6, str7, str8);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Complication) {
                Complication complication = (Complication) obj;
                if (!pq7.a(this.complicationId, complication.complicationId) || !pq7.a(this.name, complication.name) || !pq7.a(this.nameKey, complication.nameKey) || !pq7.a(this.categories, complication.categories) || !pq7.a(this.description, complication.description) || !pq7.a(this.descriptionKey, complication.descriptionKey) || !pq7.a(this.icon, complication.icon) || !pq7.a(this.createdAt, complication.createdAt) || !pq7.a(this.updatedAt, complication.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final String getComplicationId() {
        return this.complicationId;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.complicationId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.nameKey;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        ArrayList<String> arrayList = this.categories;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        String str4 = this.description;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.descriptionKey;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.icon;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.createdAt;
        int hashCode8 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.updatedAt;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setComplicationId(String str) {
        pq7.c(str, "<set-?>");
        this.complicationId = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDescription(String str) {
        pq7.c(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        pq7.c(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        pq7.c(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Complication(complicationId=" + this.complicationId + ", name=" + this.name + ", nameKey=" + this.nameKey + ", categories=" + this.categories + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", icon=" + this.icon + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
