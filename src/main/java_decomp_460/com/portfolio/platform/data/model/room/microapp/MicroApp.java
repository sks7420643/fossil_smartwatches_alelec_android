package com.portfolio.platform.data.model.room.microapp;

import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroApp {
    @DexIgnore
    @rj4("categoryIds")
    public ArrayList<String> categories;
    @DexIgnore
    @rj4("englishDescription")
    public String description;
    @DexIgnore
    @rj4("description")
    public String descriptionKey;
    @DexIgnore
    @rj4("icon")
    public String icon;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("englishName")
    public String name;
    @DexIgnore
    @rj4("name")
    public String nameKey;
    @DexIgnore
    public String serialNumber;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[MicroAppInstruction.MicroAppID.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
        }
        */
    }

    @DexIgnore
    public MicroApp(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(str4, "serialNumber");
        pq7.c(arrayList, "categories");
        pq7.c(str5, "description");
        pq7.c(str6, "descriptionKey");
        this.id = str;
        this.name = str2;
        this.nameKey = str3;
        this.serialNumber = str4;
        this.categories = arrayList;
        this.description = str5;
        this.descriptionKey = str6;
        this.icon = str7;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroApp(String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, kq7 kq7) {
        this(str, str2, str3, str4, arrayList, str5, str6, (i & 128) != 0 ? "" : str7);
    }

    @DexIgnore
    public static /* synthetic */ MicroApp copy$default(MicroApp microApp, String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, Object obj) {
        return microApp.copy((i & 1) != 0 ? microApp.id : str, (i & 2) != 0 ? microApp.name : str2, (i & 4) != 0 ? microApp.nameKey : str3, (i & 8) != 0 ? microApp.serialNumber : str4, (i & 16) != 0 ? microApp.categories : arrayList, (i & 32) != 0 ? microApp.description : str5, (i & 64) != 0 ? microApp.descriptionKey : str6, (i & 128) != 0 ? microApp.icon : str7);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final String component4() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<String> component5() {
        return this.categories;
    }

    @DexIgnore
    public final String component6() {
        return this.description;
    }

    @DexIgnore
    public final String component7() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String component8() {
        return this.icon;
    }

    @DexIgnore
    public final MicroApp copy(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(str3, "nameKey");
        pq7.c(str4, "serialNumber");
        pq7.c(arrayList, "categories");
        pq7.c(str5, "description");
        pq7.c(str6, "descriptionKey");
        return new MicroApp(str, str2, str3, str4, arrayList, str5, str6, str7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MicroApp) {
                MicroApp microApp = (MicroApp) obj;
                if (!pq7.a(this.id, microApp.id) || !pq7.a(this.name, microApp.name) || !pq7.a(this.nameKey, microApp.nameKey) || !pq7.a(this.serialNumber, microApp.serialNumber) || !pq7.a(this.categories, microApp.categories) || !pq7.a(this.description, microApp.description) || !pq7.a(this.descriptionKey, microApp.descriptionKey) || !pq7.a(this.icon, microApp.icon)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final int getDefaultIconId() {
        switch (WhenMappings.$EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.id).ordinal()]) {
            case 2:
                return 2131231171;
            case 3:
                return 2131231163;
            case 4:
                return 2131231164;
            case 5:
                return 2131231169;
            case 6:
                return 2131231167;
            case 7:
                return 2131231166;
            case 8:
                return 2131231168;
            case 9:
                return 2131231170;
            case 10:
                return 2131231174;
            case 11:
                return 2131231175;
            case 12:
                return 2131231162;
            case 13:
                return 2131231176;
            case 14:
                return 2131231172;
            case 15:
                return 2131231173;
            default:
                return 2131231104;
        }
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.nameKey;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.serialNumber;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        ArrayList<String> arrayList = this.categories;
        int hashCode5 = arrayList != null ? arrayList.hashCode() : 0;
        String str5 = this.description;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.descriptionKey;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.icon;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setDescription(String str) {
        pq7.c(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        pq7.c(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        pq7.c(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        pq7.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroApp(id=" + this.id + ", name=" + this.name + ", nameKey=" + this.nameKey + ", serialNumber=" + this.serialNumber + ", categories=" + this.categories + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", icon=" + this.icon + ")";
    }
}
