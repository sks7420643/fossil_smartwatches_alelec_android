package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.jn5;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new Creator();
    @DexIgnore
    public ArrayList<String> androidPermissionSet;
    @DexIgnore
    public String externalLink;
    @DexIgnore
    public boolean isGranted;
    @DexIgnore
    public String longDescription;
    @DexIgnore
    public String permissionGroup;
    @DexIgnore
    public jn5.c settingPermissionId;
    @DexIgnore
    public String shortDescription;
    @DexIgnore
    public String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Creator implements Parcelable.Creator {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object createFromParcel(Parcel parcel) {
            pq7.c(parcel, "in");
            String readString = parcel.readString();
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            while (readInt != 0) {
                arrayList.add(parcel.readString());
                readInt--;
            }
            return new PermissionData(readString, arrayList, parcel.readInt() != 0 ? (jn5.c) Enum.valueOf(jn5.c.class, parcel.readString()) : null, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object[] newArray(int i) {
            return new PermissionData[i];
        }
    }

    @DexIgnore
    public PermissionData(String str, ArrayList<String> arrayList, jn5.c cVar, String str2, String str3, String str4, String str5, boolean z) {
        pq7.c(str, "type");
        pq7.c(arrayList, "androidPermissionSet");
        pq7.c(str2, "permissionGroup");
        pq7.c(str3, "shortDescription");
        pq7.c(str4, "longDescription");
        pq7.c(str5, "externalLink");
        this.type = str;
        this.androidPermissionSet = arrayList;
        this.settingPermissionId = cVar;
        this.permissionGroup = str2;
        this.shortDescription = str3;
        this.longDescription = str4;
        this.externalLink = str5;
        this.isGranted = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PermissionData(String str, ArrayList arrayList, jn5.c cVar, String str2, String str3, String str4, String str5, boolean z, int i, kq7 kq7) {
        this((i & 1) != 0 ? "PERMISSION_REQUEST_TYPE" : str, arrayList, cVar, str2, (i & 16) != 0 ? "" : str3, (i & 32) != 0 ? "" : str4, str5, z);
    }

    @DexIgnore
    public static /* synthetic */ PermissionData copy$default(PermissionData permissionData, String str, ArrayList arrayList, jn5.c cVar, String str2, String str3, String str4, String str5, boolean z, int i, Object obj) {
        return permissionData.copy((i & 1) != 0 ? permissionData.type : str, (i & 2) != 0 ? permissionData.androidPermissionSet : arrayList, (i & 4) != 0 ? permissionData.settingPermissionId : cVar, (i & 8) != 0 ? permissionData.permissionGroup : str2, (i & 16) != 0 ? permissionData.shortDescription : str3, (i & 32) != 0 ? permissionData.longDescription : str4, (i & 64) != 0 ? permissionData.externalLink : str5, (i & 128) != 0 ? permissionData.isGranted : z);
    }

    @DexIgnore
    public final String component1() {
        return this.type;
    }

    @DexIgnore
    public final ArrayList<String> component2() {
        return this.androidPermissionSet;
    }

    @DexIgnore
    public final jn5.c component3() {
        return this.settingPermissionId;
    }

    @DexIgnore
    public final String component4() {
        return this.permissionGroup;
    }

    @DexIgnore
    public final String component5() {
        return this.shortDescription;
    }

    @DexIgnore
    public final String component6() {
        return this.longDescription;
    }

    @DexIgnore
    public final String component7() {
        return this.externalLink;
    }

    @DexIgnore
    public final boolean component8() {
        return this.isGranted;
    }

    @DexIgnore
    public final PermissionData copy(String str, ArrayList<String> arrayList, jn5.c cVar, String str2, String str3, String str4, String str5, boolean z) {
        pq7.c(str, "type");
        pq7.c(arrayList, "androidPermissionSet");
        pq7.c(str2, "permissionGroup");
        pq7.c(str3, "shortDescription");
        pq7.c(str4, "longDescription");
        pq7.c(str5, "externalLink");
        return new PermissionData(str, arrayList, cVar, str2, str3, str4, str5, z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof PermissionData) {
                PermissionData permissionData = (PermissionData) obj;
                if (!pq7.a(this.type, permissionData.type) || !pq7.a(this.androidPermissionSet, permissionData.androidPermissionSet) || !pq7.a(this.settingPermissionId, permissionData.settingPermissionId) || !pq7.a(this.permissionGroup, permissionData.permissionGroup) || !pq7.a(this.shortDescription, permissionData.shortDescription) || !pq7.a(this.longDescription, permissionData.longDescription) || !pq7.a(this.externalLink, permissionData.externalLink) || this.isGranted != permissionData.isGranted) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<String> getAndroidPermissionSet() {
        return this.androidPermissionSet;
    }

    @DexIgnore
    public final String getExternalLink() {
        return this.externalLink;
    }

    @DexIgnore
    public final String getLongDescription() {
        return this.longDescription;
    }

    @DexIgnore
    public final String getPermissionGroup() {
        return this.permissionGroup;
    }

    @DexIgnore
    public final jn5.c getSettingPermissionId() {
        return this.settingPermissionId;
    }

    @DexIgnore
    public final String getShortDescription() {
        return this.shortDescription;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.type;
        int hashCode = str != null ? str.hashCode() : 0;
        ArrayList<String> arrayList = this.androidPermissionSet;
        int hashCode2 = arrayList != null ? arrayList.hashCode() : 0;
        jn5.c cVar = this.settingPermissionId;
        int hashCode3 = cVar != null ? cVar.hashCode() : 0;
        String str2 = this.permissionGroup;
        int hashCode4 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.shortDescription;
        int hashCode5 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.longDescription;
        int hashCode6 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.externalLink;
        if (str5 != null) {
            i = str5.hashCode();
        }
        boolean z = this.isGranted;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public final boolean isGranted() {
        return this.isGranted;
    }

    @DexIgnore
    public final void setAndroidPermissionSet(ArrayList<String> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.androidPermissionSet = arrayList;
    }

    @DexIgnore
    public final void setExternalLink(String str) {
        pq7.c(str, "<set-?>");
        this.externalLink = str;
    }

    @DexIgnore
    public final void setGranted(boolean z) {
        this.isGranted = z;
    }

    @DexIgnore
    public final void setLongDescription(String str) {
        pq7.c(str, "<set-?>");
        this.longDescription = str;
    }

    @DexIgnore
    public final void setPermissionGroup(String str) {
        pq7.c(str, "<set-?>");
        this.permissionGroup = str;
    }

    @DexIgnore
    public final void setSettingPermissionId(jn5.c cVar) {
        this.settingPermissionId = cVar;
    }

    @DexIgnore
    public final void setShortDescription(String str) {
        pq7.c(str, "<set-?>");
        this.shortDescription = str;
    }

    @DexIgnore
    public final void setType(String str) {
        pq7.c(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public String toString() {
        return "PermissionData(type=" + this.type + ", androidPermissionSet=" + this.androidPermissionSet + ", settingPermissionId=" + this.settingPermissionId + ", permissionGroup=" + this.permissionGroup + ", shortDescription=" + this.shortDescription + ", longDescription=" + this.longDescription + ", externalLink=" + this.externalLink + ", isGranted=" + this.isGranted + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.type);
        ArrayList<String> arrayList = this.androidPermissionSet;
        parcel.writeInt(arrayList.size());
        for (String str : arrayList) {
            parcel.writeString(str);
        }
        jn5.c cVar = this.settingPermissionId;
        if (cVar != null) {
            parcel.writeInt(1);
            parcel.writeString(cVar.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.permissionGroup);
        parcel.writeString(this.shortDescription);
        parcel.writeString(this.longDescription);
        parcel.writeString(this.externalLink);
        parcel.writeInt(this.isGranted ? 1 : 0);
    }
}
