package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public WorkoutHeartRate(int i, float f, int i2, int i3, List<Short> list) {
        pq7.c(list, "values");
        this.resolutionInSecond = i;
        this.average = f;
        this.max = i2;
        this.min = i3;
        this.values = list;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutHeartRate(com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r7) {
        /*
            r6 = this;
            java.lang.String r0 = "heartRate"
            com.fossil.pq7.c(r7, r0)
            int r1 = r7.getResolutionInSecond()
            short r0 = r7.getAverage()
            float r2 = (float) r0
            short r3 = r7.getMaximum()
            java.util.List r0 = r7.getValues()
            java.lang.Comparable r0 = com.fossil.pm7.T(r0)
            java.lang.Short r0 = (java.lang.Short) r0
            if (r0 == 0) goto L_0x002b
            short r4 = r0.shortValue()
        L_0x0022:
            java.util.List r5 = r7.getValues()
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x002b:
            r4 = 0
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate.<init>(com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper):void");
    }

    @DexIgnore
    public static /* synthetic */ WorkoutHeartRate copy$default(WorkoutHeartRate workoutHeartRate, int i, float f, int i2, int i3, List list, int i4, Object obj) {
        return workoutHeartRate.copy((i4 & 1) != 0 ? workoutHeartRate.resolutionInSecond : i, (i4 & 2) != 0 ? workoutHeartRate.average : f, (i4 & 4) != 0 ? workoutHeartRate.max : i2, (i4 & 8) != 0 ? workoutHeartRate.min : i3, (i4 & 16) != 0 ? workoutHeartRate.values : list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final float component2() {
        return this.average;
    }

    @DexIgnore
    public final int component3() {
        return this.max;
    }

    @DexIgnore
    public final int component4() {
        return this.min;
    }

    @DexIgnore
    public final List<Short> component5() {
        return this.values;
    }

    @DexIgnore
    public final WorkoutHeartRate copy(int i, float f, int i2, int i3, List<Short> list) {
        pq7.c(list, "values");
        return new WorkoutHeartRate(i, f, i2, i3, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutHeartRate) {
                WorkoutHeartRate workoutHeartRate = (WorkoutHeartRate) obj;
                if (!(this.resolutionInSecond == workoutHeartRate.resolutionInSecond && Float.compare(this.average, workoutHeartRate.average) == 0 && this.max == workoutHeartRate.max && this.min == workoutHeartRate.min && pq7.a(this.values, workoutHeartRate.values))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond;
        int floatToIntBits = Float.floatToIntBits(this.average);
        int i2 = this.max;
        int i3 = this.min;
        List<Short> list = this.values;
        return (list != null ? list.hashCode() : 0) + (((((((i * 31) + floatToIntBits) * 31) + i2) * 31) + i3) * 31);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutHeartRate(resolutionInSecond=" + this.resolutionInSecond + ", average=" + this.average + ", max=" + this.max + ", min=" + this.min + ", values=" + this.values + ")";
    }
}
