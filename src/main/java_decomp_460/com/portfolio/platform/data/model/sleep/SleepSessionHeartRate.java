package com.portfolio.platform.data.model.sleep;

import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public SleepSessionHeartRate(float f, int i, int i2, int i3, List<Short> list) {
        pq7.c(list, "values");
        this.average = f;
        this.max = i;
        this.min = i2;
        this.resolutionInSecond = i3;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionHeartRate copy$default(SleepSessionHeartRate sleepSessionHeartRate, float f, int i, int i2, int i3, List list, int i4, Object obj) {
        return sleepSessionHeartRate.copy((i4 & 1) != 0 ? sleepSessionHeartRate.average : f, (i4 & 2) != 0 ? sleepSessionHeartRate.max : i, (i4 & 4) != 0 ? sleepSessionHeartRate.min : i2, (i4 & 8) != 0 ? sleepSessionHeartRate.resolutionInSecond : i3, (i4 & 16) != 0 ? sleepSessionHeartRate.values : list);
    }

    @DexIgnore
    public final float component1() {
        return this.average;
    }

    @DexIgnore
    public final int component2() {
        return this.max;
    }

    @DexIgnore
    public final int component3() {
        return this.min;
    }

    @DexIgnore
    public final int component4() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component5() {
        return this.values;
    }

    @DexIgnore
    public final SleepSessionHeartRate copy(float f, int i, int i2, int i3, List<Short> list) {
        pq7.c(list, "values");
        return new SleepSessionHeartRate(f, i, i2, i3, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepSessionHeartRate) {
                SleepSessionHeartRate sleepSessionHeartRate = (SleepSessionHeartRate) obj;
                if (!(Float.compare(this.average, sleepSessionHeartRate.average) == 0 && this.max == sleepSessionHeartRate.max && this.min == sleepSessionHeartRate.min && this.resolutionInSecond == sleepSessionHeartRate.resolutionInSecond && pq7.a(this.values, sleepSessionHeartRate.values))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = Float.floatToIntBits(this.average);
        int i = this.max;
        int i2 = this.min;
        int i3 = this.resolutionInSecond;
        List<Short> list = this.values;
        return (list != null ? list.hashCode() : 0) + (((((((floatToIntBits * 31) + i) * 31) + i2) * 31) + i3) * 31);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionHeartRate(average=" + this.average + ", max=" + this.max + ", min=" + this.min + ", resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ")";
    }
}
