package com.portfolio.platform.data.model.goaltracking;

import com.fossil.cl7;
import com.fossil.lk5;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataKt {
    @DexIgnore
    public static final cl7<Date, Date> calculateRangeDownload(List<GoalTrackingData> list, Date date, Date date2) {
        pq7.c(list, "$this$calculateRangeDownload");
        pq7.c(date, GoalPhase.COLUMN_START_DATE);
        pq7.c(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new cl7<>(date, date2);
        }
        if (!lk5.m0(((GoalTrackingData) pm7.P(list)).getDate(), date2)) {
            return new cl7<>(((GoalTrackingData) pm7.P(list)).getDate(), date2);
        }
        if (lk5.m0(((GoalTrackingData) pm7.F(list)).getDate(), date)) {
            return null;
        }
        return new cl7<>(date, lk5.P(((GoalTrackingData) pm7.F(list)).getDate()));
    }
}
