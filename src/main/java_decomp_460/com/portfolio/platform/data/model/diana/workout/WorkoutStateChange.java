package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ki5;
import com.fossil.pq7;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChange {
    @DexIgnore
    public /* final */ ki5 state;
    @DexIgnore
    public /* final */ Date time;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStateChange(int i, com.fossil.fitness.WorkoutStateChange workoutStateChange) {
        this(ki5.Companion.a(workoutStateChange.getState().name()), new Date(((long) (workoutStateChange.getIndexInSecond() + i)) * 1000));
        pq7.c(workoutStateChange, "workoutStageChange");
    }

    @DexIgnore
    public WorkoutStateChange(ki5 ki5, Date date) {
        pq7.c(ki5, "state");
        pq7.c(date, LogBuilder.KEY_TIME);
        this.state = ki5;
        this.time = date;
    }

    @DexIgnore
    private final ki5 component1() {
        return this.state;
    }

    @DexIgnore
    private final Date component2() {
        return this.time;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChange copy$default(WorkoutStateChange workoutStateChange, ki5 ki5, Date date, int i, Object obj) {
        if ((i & 1) != 0) {
            ki5 = workoutStateChange.state;
        }
        if ((i & 2) != 0) {
            date = workoutStateChange.time;
        }
        return workoutStateChange.copy(ki5, date);
    }

    @DexIgnore
    public final WorkoutStateChange copy(ki5 ki5, Date date) {
        pq7.c(ki5, "state");
        pq7.c(date, LogBuilder.KEY_TIME);
        return new WorkoutStateChange(ki5, date);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutStateChange) {
                WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
                if (!pq7.a(this.state, workoutStateChange.state) || !pq7.a(this.time, workoutStateChange.time)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        ki5 ki5 = this.state;
        int hashCode = ki5 != null ? ki5.hashCode() : 0;
        Date date = this.time;
        if (date != null) {
            i = date.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange(state=" + this.state + ", time=" + this.time + ")";
    }
}
