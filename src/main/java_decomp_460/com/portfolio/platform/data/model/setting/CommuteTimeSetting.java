package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("commuteAddress")
    public String address;
    @DexIgnore
    @rj4("commuteAvoidTolls")
    public boolean avoidTolls;
    @DexIgnore
    @rj4("commuteFormat")
    public String format;
    @DexIgnore
    @rj4("commuteMovement")
    public String movement;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new CommuteTimeSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting[] newArray(int i) {
            return new CommuteTimeSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeSetting() {
        this(null, null, false, null, 15, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommuteTimeSetting(android.os.Parcel r7) {
        /*
            r6 = this;
            r2 = 0
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r7, r0)
            java.lang.String r0 = r7.readString()
            java.lang.String r4 = ""
            if (r0 == 0) goto L_0x0026
        L_0x000e:
            java.lang.String r1 = r7.readString()
            if (r1 == 0) goto L_0x0029
        L_0x0014:
            byte r3 = r7.readByte()
            byte r5 = (byte) r2
            if (r3 == r5) goto L_0x001c
            r2 = 1
        L_0x001c:
            java.lang.String r3 = r7.readString()
            if (r3 == 0) goto L_0x002c
        L_0x0022:
            r6.<init>(r0, r1, r2, r3)
            return
        L_0x0026:
            java.lang.String r0 = ""
            goto L_0x000e
        L_0x0029:
            java.lang.String r1 = ""
            goto L_0x0014
        L_0x002c:
            r3 = r4
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeSetting.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public CommuteTimeSetting(String str, String str2, boolean z, String str3) {
        pq7.c(str, "address");
        pq7.c(str2, "format");
        pq7.c(str3, "movement");
        this.address = str;
        this.format = str2;
        this.avoidTolls = z;
        this.movement = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CommuteTimeSetting(String str, String str2, boolean z, String str3, int i, kq7 kq7) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "travel" : str2, (i & 4) != 0 ? true : z, (i & 8) != 0 ? "car" : str3);
    }

    @DexIgnore
    public static /* synthetic */ CommuteTimeSetting copy$default(CommuteTimeSetting commuteTimeSetting, String str, String str2, boolean z, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = commuteTimeSetting.address;
        }
        if ((i & 2) != 0) {
            str2 = commuteTimeSetting.format;
        }
        if ((i & 4) != 0) {
            z = commuteTimeSetting.avoidTolls;
        }
        if ((i & 8) != 0) {
            str3 = commuteTimeSetting.movement;
        }
        return commuteTimeSetting.copy(str, str2, z, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.address;
    }

    @DexIgnore
    public final String component2() {
        return this.format;
    }

    @DexIgnore
    public final boolean component3() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String component4() {
        return this.movement;
    }

    @DexIgnore
    public final CommuteTimeSetting copy(String str, String str2, boolean z, String str3) {
        pq7.c(str, "address");
        pq7.c(str2, "format");
        pq7.c(str3, "movement");
        return new CommuteTimeSetting(str, str2, z, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CommuteTimeSetting) {
                CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) obj;
                if (!pq7.a(this.address, commuteTimeSetting.address) || !pq7.a(this.format, commuteTimeSetting.format) || this.avoidTolls != commuteTimeSetting.avoidTolls || !pq7.a(this.movement, commuteTimeSetting.movement)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final boolean getAvoidTolls() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String getFormat() {
        return this.format;
    }

    @DexIgnore
    public final String getMovement() {
        return this.movement;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.address;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.format;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        boolean z = this.avoidTolls;
        if (z) {
            z = true;
        }
        String str3 = this.movement;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        pq7.c(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setAvoidTolls(boolean z) {
        this.avoidTolls = z;
    }

    @DexIgnore
    public final void setFormat(String str) {
        pq7.c(str, "<set-?>");
        this.format = str;
    }

    @DexIgnore
    public final void setMovement(String str) {
        pq7.c(str, "<set-?>");
        this.movement = str;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeSetting(address=" + this.address + ", format=" + this.format + ", avoidTolls=" + this.avoidTolls + ", movement=" + this.movement + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.address);
        parcel.writeString(this.format);
        parcel.writeByte(this.avoidTolls ? (byte) 1 : 0);
        parcel.writeString(this.movement);
    }
}
