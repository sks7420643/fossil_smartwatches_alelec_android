package com.portfolio.platform.data.model.diana.preset;

import com.fossil.kj5;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.qj4;
import com.fossil.rj4;
import com.fossil.um5;
import com.fossil.zj5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    @qj4(DianaPresetComplicationSettingSerializer.class)
    @rj4("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @rj4("createdAt")
    public String createdAt; // = "";
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("isActive")
    public boolean isActive;
    @DexIgnore
    @pj4
    @rj4("name")
    public String name;
    @DexIgnore
    @zj5
    public int pinType; // = 1;
    @DexIgnore
    @rj4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @rj4("updatedAt")
    public String updatedAt; // = "";
    @DexIgnore
    @rj4("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @pj4
    @qj4(DianaPresetWatchAppSettingSerializer.class)
    @rj4("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final DianaPreset cloneFrom(DianaPreset dianaPreset) {
            pq7.c(dianaPreset, "preset");
            String w0 = lk5.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            pq7.b(uuid, "UUID.randomUUID().toString()");
            String serialNumber = dianaPreset.getSerialNumber();
            String c = um5.c(PortfolioApp.h0.c(), 2131886539);
            pq7.b(c, "LanguageHelper.getString\u2026wPreset_Title__NewPreset)");
            DianaPreset dianaPreset2 = new DianaPreset(uuid, serialNumber, c, false, kj5.c(dianaPreset.getComplications()), kj5.d(dianaPreset.getWatchapps()), dianaPreset.getWatchFaceId());
            dianaPreset2.setCreatedAt(w0);
            dianaPreset2.setUpdatedAt(w0);
            return dianaPreset2;
        }

        @DexIgnore
        public final DianaPreset cloneFromDefaultPreset(DianaRecommendPreset dianaRecommendPreset) {
            pq7.c(dianaRecommendPreset, "recommendPreset");
            String w0 = lk5.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            pq7.b(uuid, "UUID.randomUUID().toString()");
            DianaPreset dianaPreset = new DianaPreset(uuid, dianaRecommendPreset.getSerialNumber(), dianaRecommendPreset.getName(), dianaRecommendPreset.isDefault(), kj5.c(dianaRecommendPreset.getComplications()), kj5.d(dianaRecommendPreset.getWatchapps()), dianaRecommendPreset.getWatchFaceId());
            dianaPreset.setCreatedAt(w0);
            dianaPreset.setUpdatedAt(w0);
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (it.hasNext()) {
                pq7.b(w0, "timestamp");
                it.next().setLocalUpdateAt(w0);
            }
            Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
            while (it2.hasNext()) {
                pq7.b(w0, "timestamp");
                it2.next().setLocalUpdateAt(w0);
            }
            return dianaPreset;
        }
    }

    @DexIgnore
    public DianaPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        pq7.c(str, "id");
        pq7.c(str2, "serialNumber");
        pq7.c(str3, "name");
        pq7.c(arrayList, "complications");
        pq7.c(arrayList2, "watchapps");
        pq7.c(str4, "watchFaceId");
        this.id = str;
        this.serialNumber = str2;
        this.name = str3;
        this.isActive = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPreset copy$default(DianaPreset dianaPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, int i, Object obj) {
        return dianaPreset.copy((i & 1) != 0 ? dianaPreset.id : str, (i & 2) != 0 ? dianaPreset.serialNumber : str2, (i & 4) != 0 ? dianaPreset.name : str3, (i & 8) != 0 ? dianaPreset.isActive : z, (i & 16) != 0 ? dianaPreset.complications : arrayList, (i & 32) != 0 ? dianaPreset.watchapps : arrayList2, (i & 64) != 0 ? dianaPreset.watchFaceId : str4);
    }

    @DexIgnore
    public final DianaPreset clone() {
        DianaPreset dianaPreset = new DianaPreset(this.id, this.serialNumber, this.name, this.isActive, kj5.c(this.complications), kj5.d(this.watchapps), this.watchFaceId);
        dianaPreset.updatedAt = this.updatedAt;
        dianaPreset.createdAt = this.createdAt;
        return dianaPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isActive;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final DianaPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        pq7.c(str, "id");
        pq7.c(str2, "serialNumber");
        pq7.c(str3, "name");
        pq7.c(arrayList, "complications");
        pq7.c(arrayList2, "watchapps");
        pq7.c(str4, "watchFaceId");
        return new DianaPreset(str, str2, str3, z, arrayList, arrayList2, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaPreset) {
                DianaPreset dianaPreset = (DianaPreset) obj;
                if (!pq7.a(this.id, dianaPreset.id) || !pq7.a(this.serialNumber, dianaPreset.serialNumber) || !pq7.a(this.name, dianaPreset.name) || this.isActive != dianaPreset.isActive || !pq7.a(this.complications, dianaPreset.complications) || !pq7.a(this.watchapps, dianaPreset.watchapps) || !pq7.a(this.watchFaceId, dianaPreset.watchFaceId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.serialNumber;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = arrayList2 != null ? arrayList2.hashCode() : 0;
        String str4 = this.watchFaceId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        pq7.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        pq7.c(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.id + ", serialNumber=" + this.serialNumber + ", name=" + this.name + ", isActive=" + this.isActive + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ")";
    }
}
