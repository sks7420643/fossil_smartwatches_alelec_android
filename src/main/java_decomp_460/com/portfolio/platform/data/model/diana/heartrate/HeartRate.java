package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.lk5;
import com.fossil.pq7;
import com.fossil.rj4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.ServerError;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRate extends ServerError {
    @DexIgnore
    @rj4(GoalTrackingSummary.COLUMN_AVERAGE)
    public /* final */ float mAverage;
    @DexIgnore
    @rj4("createdAt")
    public /* final */ Date mCreatedAt;
    @DexIgnore
    @rj4("date")
    public /* final */ Date mDate;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @rj4("id")
    public /* final */ String mId;
    @DexIgnore
    @rj4("max")
    public /* final */ int mMax;
    @DexIgnore
    @rj4("min")
    public /* final */ int mMin;
    @DexIgnore
    @rj4("minuteCount")
    public /* final */ int mMinuteCount;
    @DexIgnore
    @rj4("resting")
    public /* final */ Resting mResting;
    @DexIgnore
    @rj4(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @rj4("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @rj4("updatedAt")
    public /* final */ Date mUpdatedAt;

    @DexIgnore
    public HeartRate(String str, float f, Date date, Date date2, Date date3, String str2, String str3, int i, int i2, int i3, int i4, Resting resting) {
        pq7.c(str, "mId");
        pq7.c(date, "mDate");
        pq7.c(date2, "mCreatedAt");
        pq7.c(date3, "mUpdatedAt");
        pq7.c(str2, "mEndTime");
        pq7.c(str3, "mStartTime");
        this.mId = str;
        this.mAverage = f;
        this.mDate = date;
        this.mCreatedAt = date2;
        this.mUpdatedAt = date3;
        this.mEndTime = str2;
        this.mStartTime = str3;
        this.mTimeZoneOffsetInSecond = i;
        this.mMin = i2;
        this.mMax = i3;
        this.mMinuteCount = i4;
        this.mResting = resting;
    }

    @DexIgnore
    public final HeartRateSample toHeartRateSample() {
        Date date;
        Date date2;
        Date date3;
        Date date4;
        try {
            Calendar instance = Calendar.getInstance();
            Date a2 = lk5.a(this.mTimeZoneOffsetInSecond, this.mStartTime);
            try {
                pq7.b(instance, "calendar");
                instance.setTime(a2);
                instance.set(13, 0);
                instance.set(14, 0);
                date = instance.getTime();
                Date a3 = lk5.a(this.mTimeZoneOffsetInSecond, this.mEndTime);
                try {
                    instance.setTime(a3);
                    instance.set(13, 0);
                    instance.set(14, 0);
                    date3 = date;
                    date4 = instance.getTime();
                } catch (ParseException e) {
                    e = e;
                    date2 = a3;
                    e.printStackTrace();
                    date3 = date;
                    date4 = date2;
                    String str = this.mId;
                    float f = this.mAverage;
                    Date date5 = this.mDate;
                    long time = this.mCreatedAt.getTime();
                    long time2 = this.mUpdatedAt.getTime();
                    DateTime b = lk5.b(date4, this.mTimeZoneOffsetInSecond);
                    pq7.b(b, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                    DateTime b2 = lk5.b(date3, this.mTimeZoneOffsetInSecond);
                    pq7.b(b2, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                    return new HeartRateSample(str, f, date5, time, time2, b, b2, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
                }
            } catch (ParseException e2) {
                e = e2;
                date2 = null;
                e.printStackTrace();
                date3 = date;
                date4 = date2;
                String str2 = this.mId;
                float f2 = this.mAverage;
                Date date52 = this.mDate;
                long time3 = this.mCreatedAt.getTime();
                long time22 = this.mUpdatedAt.getTime();
                DateTime b3 = lk5.b(date4, this.mTimeZoneOffsetInSecond);
                pq7.b(b3, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime b22 = lk5.b(date3, this.mTimeZoneOffsetInSecond);
                pq7.b(b22, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str2, f2, date52, time3, time22, b3, b22, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
        } catch (ParseException e3) {
            e = e3;
            date = null;
            date2 = null;
            e.printStackTrace();
            date3 = date;
            date4 = date2;
            String str22 = this.mId;
            float f22 = this.mAverage;
            Date date522 = this.mDate;
            long time32 = this.mCreatedAt.getTime();
            long time222 = this.mUpdatedAt.getTime();
            DateTime b32 = lk5.b(date4, this.mTimeZoneOffsetInSecond);
            pq7.b(b32, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime b222 = lk5.b(date3, this.mTimeZoneOffsetInSecond);
            pq7.b(b222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str22, f22, date522, time32, time222, b32, b222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        }
        try {
            String str222 = this.mId;
            float f222 = this.mAverage;
            Date date5222 = this.mDate;
            long time322 = this.mCreatedAt.getTime();
            long time2222 = this.mUpdatedAt.getTime();
            DateTime b322 = lk5.b(date4, this.mTimeZoneOffsetInSecond);
            pq7.b(b322, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime b2222 = lk5.b(date3, this.mTimeZoneOffsetInSecond);
            pq7.b(b2222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str222, f222, date5222, time322, time2222, b322, b2222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
