package com.portfolio.platform.data.model.watchface;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Data {
    @DexIgnore
    @rj4("url")
    public String url;

    @DexIgnore
    public Data(String str) {
        this.url = str;
    }

    @DexIgnore
    public static /* synthetic */ Data copy$default(Data data, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = data.url;
        }
        return data.copy(str);
    }

    @DexIgnore
    public final String component1() {
        return this.url;
    }

    @DexIgnore
    public final Data copy(String str) {
        return new Data(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Data) && pq7.a(this.url, ((Data) obj).url));
    }

    @DexIgnore
    public final String getUrl() {
        return this.url;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.url;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setUrl(String str) {
        this.url = str;
    }

    @DexIgnore
    public String toString() {
        return "Data(url=" + this.url + ")";
    }
}
