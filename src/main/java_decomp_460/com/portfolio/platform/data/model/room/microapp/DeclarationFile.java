package com.portfolio.platform.data.model.room.microapp;

import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeclarationFile {
    @DexIgnore
    public String appId;
    @DexIgnore
    @rj4("content")
    public /* final */ String content;
    @DexIgnore
    @rj4("description")
    public /* final */ String description;
    @DexIgnore
    @rj4("id")
    public /* final */ String id;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String variantName;

    @DexIgnore
    public DeclarationFile(String str, String str2, String str3) {
        pq7.c(str, "id");
        this.id = str;
        this.description = str2;
        this.content = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DeclarationFile(String str, String str2, String str3, int i, kq7 kq7) {
        this(str, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3);
    }

    @DexIgnore
    public static /* synthetic */ DeclarationFile copy$default(DeclarationFile declarationFile, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = declarationFile.id;
        }
        if ((i & 2) != 0) {
            str2 = declarationFile.description;
        }
        if ((i & 4) != 0) {
            str3 = declarationFile.content;
        }
        return declarationFile.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.description;
    }

    @DexIgnore
    public final String component3() {
        return this.content;
    }

    @DexIgnore
    public final DeclarationFile copy(String str, String str2, String str3) {
        pq7.c(str, "id");
        return new DeclarationFile(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DeclarationFile) {
                DeclarationFile declarationFile = (DeclarationFile) obj;
                if (!pq7.a(this.id, declarationFile.id) || !pq7.a(this.description, declarationFile.description) || !pq7.a(this.content, declarationFile.content)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppId() {
        String str = this.appId;
        if (str != null) {
            return str;
        }
        pq7.n("appId");
        throw null;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getSerialNumber() {
        String str = this.serialNumber;
        if (str != null) {
            return str;
        }
        pq7.n("serialNumber");
        throw null;
    }

    @DexIgnore
    public final String getVariantName() {
        String str = this.variantName;
        if (str != null) {
            return str;
        }
        pq7.n("variantName");
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.description;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.content;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        pq7.c(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        pq7.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setVariantName(String str) {
        pq7.c(str, "<set-?>");
        this.variantName = str;
    }

    @DexIgnore
    public String toString() {
        return "DeclarationFile(id=" + this.id + ", description=" + this.description + ", content=" + this.content + ")";
    }
}
