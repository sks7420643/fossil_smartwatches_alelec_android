package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.c;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.ActivityIntensities;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activity_sample";
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public double distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int timeZoneOffsetInSecond;
    @DexIgnore
    public /* final */ String uid;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivitySample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ActivitySample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample[] newArray(int i) {
            return new ActivitySample[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActivitySample(android.os.Parcel r31) {
        /*
            r30 = this;
            java.lang.String r2 = "parcel"
            r0 = r31
            com.fossil.pq7.c(r0, r2)
            java.lang.String r2 = r31.readString()
            if (r2 == 0) goto L_0x0071
            r26 = r2
        L_0x000f:
            java.io.Serializable r2 = r31.readSerializable()
            if (r2 == 0) goto L_0x0096
            java.util.Date r2 = (java.util.Date) r2
            java.io.Serializable r3 = r31.readSerializable()
            if (r3 == 0) goto L_0x008e
            r24 = r3
            org.joda.time.DateTime r24 = (org.joda.time.DateTime) r24
            java.io.Serializable r3 = r31.readSerializable()
            if (r3 == 0) goto L_0x0086
            r25 = r3
            org.joda.time.DateTime r25 = (org.joda.time.DateTime) r25
            double r28 = r31.readDouble()
            double r10 = r31.readDouble()
            double r12 = r31.readDouble()
            int r14 = r31.readInt()
            java.lang.Class<com.portfolio.platform.data.ActivityIntensities> r3 = com.portfolio.platform.data.ActivityIntensities.class
            java.lang.ClassLoader r3 = r3.getClassLoader()
            r0 = r31
            android.os.Parcelable r3 = r0.readParcelable(r3)
            com.portfolio.platform.data.ActivityIntensities r3 = (com.portfolio.platform.data.ActivityIntensities) r3
            if (r3 == 0) goto L_0x0076
            r15 = r3
        L_0x004c:
            int r16 = r31.readInt()
            java.lang.String r17 = r31.readString()
            if (r17 == 0) goto L_0x0083
        L_0x0056:
            long r18 = r31.readLong()
            long r20 = r31.readLong()
            long r22 = r31.readLong()
            r3 = r30
            r4 = r26
            r5 = r2
            r6 = r24
            r7 = r25
            r8 = r28
            r3.<init>(r4, r5, r6, r7, r8, r10, r12, r14, r15, r16, r17, r18, r20, r22)
            return
        L_0x0071:
            java.lang.String r2 = ""
            r26 = r2
            goto L_0x000f
        L_0x0076:
            com.portfolio.platform.data.ActivityIntensities r3 = new com.portfolio.platform.data.ActivityIntensities
            r4 = 0
            r6 = 0
            r8 = 0
            r3.<init>(r4, r6, r8)
            r15 = r3
            goto L_0x004c
        L_0x0083:
            java.lang.String r17 = ""
            goto L_0x0056
        L_0x0086:
            com.fossil.il7 r2 = new com.fossil.il7
            java.lang.String r3 = "null cannot be cast to non-null type org.joda.time.DateTime"
            r2.<init>(r3)
            throw r2
        L_0x008e:
            com.fossil.il7 r2 = new com.fossil.il7
            java.lang.String r3 = "null cannot be cast to non-null type org.joda.time.DateTime"
            r2.<init>(r3)
            throw r2
        L_0x0096:
            com.fossil.il7 r2 = new com.fossil.il7
            java.lang.String r3 = "null cannot be cast to non-null type java.util.Date"
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.ActivitySample.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j) {
        this(str, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, System.currentTimeMillis(), System.currentTimeMillis());
        pq7.c(str, "uid");
        pq7.c(date2, "date");
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(activityIntensities, "intensityDistInSteps");
        pq7.c(str2, SampleRaw.COLUMN_SOURCE_ID);
    }

    @DexIgnore
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        pq7.c(str, "uid");
        pq7.c(date2, "date");
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(activityIntensities, "intensityDistInSteps");
        pq7.c(str2, SampleRaw.COLUMN_SOURCE_ID);
        this.uid = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities;
        this.timeZoneOffsetInSecond = i2;
        this.sourceId = str2;
        this.syncTime = j;
        this.createdAt = j2;
        this.updatedAt = j3;
        this.id = generateId();
    }

    @DexIgnore
    public static /* synthetic */ ActivitySample copy$default(ActivitySample activitySample, String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3, int i3, Object obj) {
        return activitySample.copy((i3 & 1) != 0 ? activitySample.uid : str, (i3 & 2) != 0 ? activitySample.date : date2, (i3 & 4) != 0 ? activitySample.startTime : dateTime, (i3 & 8) != 0 ? activitySample.endTime : dateTime2, (i3 & 16) != 0 ? activitySample.steps : d, (i3 & 32) != 0 ? activitySample.calories : d2, (i3 & 64) != 0 ? activitySample.distance : d3, (i3 & 128) != 0 ? activitySample.activeTime : i, (i3 & 256) != 0 ? activitySample.intensityDistInSteps : activityIntensities, (i3 & 512) != 0 ? activitySample.timeZoneOffsetInSecond : i2, (i3 & 1024) != 0 ? activitySample.sourceId : str2, (i3 & 2048) != 0 ? activitySample.syncTime : j, (i3 & 4096) != 0 ? activitySample.createdAt : j2, (i3 & 8192) != 0 ? activitySample.updatedAt : j3);
    }

    @DexIgnore
    private final String generateId() {
        return this.uid + ":device:" + (this.startTime.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public final String component1() {
        return this.uid;
    }

    @DexIgnore
    public final int component10() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String component11() {
        return this.sourceId;
    }

    @DexIgnore
    public final long component12() {
        return this.syncTime;
    }

    @DexIgnore
    public final long component13() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component14() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final double component5() {
        return this.steps;
    }

    @DexIgnore
    public final double component6() {
        return this.calories;
    }

    @DexIgnore
    public final double component7() {
        return this.distance;
    }

    @DexIgnore
    public final int component8() {
        return this.activeTime;
    }

    @DexIgnore
    public final ActivityIntensities component9() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final ActivitySample copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        pq7.c(str, "uid");
        pq7.c(date2, "date");
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        pq7.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        pq7.c(activityIntensities, "intensityDistInSteps");
        pq7.c(str2, SampleRaw.COLUMN_SOURCE_ID);
        return new ActivitySample(str, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, j2, j3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivitySample) {
                ActivitySample activitySample = (ActivitySample) obj;
                if (!(pq7.a(this.uid, activitySample.uid) && pq7.a(this.date, activitySample.date) && pq7.a(this.startTime, activitySample.startTime) && pq7.a(this.endTime, activitySample.endTime) && Double.compare(this.steps, activitySample.steps) == 0 && Double.compare(this.calories, activitySample.calories) == 0 && Double.compare(this.distance, activitySample.distance) == 0 && this.activeTime == activitySample.activeTime && pq7.a(this.intensityDistInSteps, activitySample.intensityDistInSteps) && this.timeZoneOffsetInSecond == activitySample.timeZoneOffsetInSecond && pq7.a(this.sourceId, activitySample.sourceId) && this.syncTime == activitySample.syncTime && this.createdAt == activitySample.createdAt && this.updatedAt == activitySample.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimeZoneOffsetInSecond() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.uid;
        int hashCode = str != null ? str.hashCode() : 0;
        Date date2 = this.date;
        int hashCode2 = date2 != null ? date2.hashCode() : 0;
        DateTime dateTime = this.startTime;
        int hashCode3 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = dateTime2 != null ? dateTime2.hashCode() : 0;
        int doubleToLongBits = Double.doubleToLongBits(this.steps);
        int doubleToLongBits2 = Double.doubleToLongBits(this.calories);
        int doubleToLongBits3 = Double.doubleToLongBits(this.distance);
        int i2 = this.activeTime;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode5 = activityIntensities != null ? activityIntensities.hashCode() : 0;
        int i3 = this.timeZoneOffsetInSecond;
        String str2 = this.sourceId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + doubleToLongBits) * 31) + doubleToLongBits2) * 31) + doubleToLongBits3) * 31) + i2) * 31) + hashCode5) * 31) + i3) * 31) + i) * 31) + c.a(this.syncTime)) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt);
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        pq7.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        pq7.c(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        pq7.c(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        pq7.c(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        pq7.c(dateTime, SampleRaw.COLUMN_START_TIME);
        this.startTime = dateTime;
        this.id = generateId();
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.timeZoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySample(uid=" + this.uid + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneOffsetInSecond=" + this.timeZoneOffsetInSecond + ", sourceId=" + this.sourceId + ", syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.uid);
        parcel.writeSerializable(this.date);
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeInt(this.timeZoneOffsetInSecond);
        parcel.writeLong(this.syncTime);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
    }
}
