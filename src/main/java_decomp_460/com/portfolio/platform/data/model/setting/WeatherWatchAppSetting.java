package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("locations")
    public List<WeatherLocationWrapper> locations; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppSetting createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new WeatherWatchAppSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppSetting[] newArray(int i) {
            return new WeatherWatchAppSetting[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppSetting() {
    }

    @DexIgnore
    public WeatherWatchAppSetting(Parcel parcel) {
        pq7.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, WeatherLocationWrapper.class.getClassLoader());
        this.locations = arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WeatherLocationWrapper> getLocations() {
        return this.locations;
    }

    @DexIgnore
    public final void setLocations(List<WeatherLocationWrapper> list) {
        pq7.c(list, "<set-?>");
        this.locations = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeList(pm7.h0(this.locations));
    }
}
