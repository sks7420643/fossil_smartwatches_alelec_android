package com.portfolio.platform.data.model.room;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDatabase_Impl extends UserDatabase {
    @DexIgnore
    public volatile UserDao _userDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `user` (`pinType` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `uid` TEXT NOT NULL, `authType` TEXT NOT NULL, `birthday` TEXT NOT NULL, `brand` TEXT NOT NULL, `diagnosticEnabled` INTEGER NOT NULL, `email` TEXT NOT NULL, `emailOptIn` INTEGER NOT NULL, `emailVerified` INTEGER NOT NULL, `firstName` TEXT NOT NULL, `gender` TEXT NOT NULL, `heightInCentimeters` INTEGER NOT NULL, `integrations` TEXT NOT NULL, `lastName` TEXT NOT NULL, `profilePicture` TEXT NOT NULL, `registerDate` TEXT NOT NULL, `registrationComplete` INTEGER NOT NULL, `useDefaultBiometric` INTEGER NOT NULL, `isOnboardingComplete` INTEGER NOT NULL, `useDefaultGoals` INTEGER NOT NULL, `username` TEXT NOT NULL, `averageSleep` INTEGER NOT NULL, `averageStep` INTEGER NOT NULL, `activeDeviceId` TEXT NOT NULL, `weightInGrams` INTEGER NOT NULL, `userAccessToken` TEXT NOT NULL, `refreshToken` TEXT NOT NULL, `accessTokenExpiresAt` TEXT NOT NULL, `accessTokenExpiresIn` INTEGER NOT NULL, `home` TEXT NOT NULL, `work` TEXT NOT NULL, `distanceUnit` TEXT NOT NULL, `weightUnit` TEXT NOT NULL, `heightUnit` TEXT NOT NULL, `temperatureUnit` TEXT NOT NULL, PRIMARY KEY(`uid`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '086181bfc70c5b6f8463fdc232f2ef29')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `user`");
            if (UserDatabase_Impl.this.mCallbacks != null) {
                int size = UserDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (UserDatabase_Impl.this.mCallbacks != null) {
                int size = UserDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            UserDatabase_Impl.this.mDatabase = lx0;
            UserDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (UserDatabase_Impl.this.mCallbacks != null) {
                int size = UserDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UserDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(37);
            hashMap.put("pinType", new ix0.a("pinType", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("uid", new ix0.a("uid", "TEXT", true, 1, null, 1));
            hashMap.put(Constants.PROFILE_KEY_AUTHTYPE, new ix0.a(Constants.PROFILE_KEY_AUTHTYPE, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_BIRTHDAY, new ix0.a(Constants.PROFILE_KEY_BIRTHDAY, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_BRAND, new ix0.a(Constants.PROFILE_KEY_BRAND, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE, new ix0.a(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE, "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.EMAIL, new ix0.a(Constants.EMAIL, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_EMAIL_OPT_IN, new ix0.a(Constants.PROFILE_KEY_EMAIL_OPT_IN, "INTEGER", true, 0, null, 1));
            hashMap.put("emailVerified", new ix0.a("emailVerified", "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_FIRST_NAME, new ix0.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", true, 0, null, 1));
            hashMap.put("gender", new ix0.a("gender", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_HEIGHT_IN_CM, new ix0.a(Constants.PROFILE_KEY_HEIGHT_IN_CM, "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_INTEGRATIONS, new ix0.a(Constants.PROFILE_KEY_INTEGRATIONS, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_LAST_NAME, new ix0.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_PROFILE_PIC, new ix0.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REGISTER_DATE, new ix0.a(Constants.PROFILE_KEY_REGISTER_DATE, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REGISTRATION_COMPLETE, new ix0.a(Constants.PROFILE_KEY_REGISTRATION_COMPLETE, "INTEGER", true, 0, null, 1));
            hashMap.put("useDefaultBiometric", new ix0.a("useDefaultBiometric", "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE, new ix0.a(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE, "INTEGER", true, 0, null, 1));
            hashMap.put("useDefaultGoals", new ix0.a("useDefaultGoals", "INTEGER", true, 0, null, 1));
            hashMap.put("username", new ix0.a("username", "TEXT", true, 0, null, 1));
            hashMap.put("averageSleep", new ix0.a("averageSleep", "INTEGER", true, 0, null, 1));
            hashMap.put("averageStep", new ix0.a("averageStep", "INTEGER", true, 0, null, 1));
            hashMap.put("activeDeviceId", new ix0.a("activeDeviceId", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS, new ix0.a(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS, "INTEGER", true, 0, null, 1));
            hashMap.put("userAccessToken", new ix0.a("userAccessToken", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REFRESH_TOKEN, new ix0.a(Constants.PROFILE_KEY_REFRESH_TOKEN, "TEXT", true, 0, null, 1));
            hashMap.put("accessTokenExpiresAt", new ix0.a("accessTokenExpiresAt", "TEXT", true, 0, null, 1));
            hashMap.put("accessTokenExpiresIn", new ix0.a("accessTokenExpiresIn", "INTEGER", true, 0, null, 1));
            hashMap.put("home", new ix0.a("home", "TEXT", true, 0, null, 1));
            hashMap.put("work", new ix0.a("work", "TEXT", true, 0, null, 1));
            hashMap.put("distanceUnit", new ix0.a("distanceUnit", "TEXT", true, 0, null, 1));
            hashMap.put("weightUnit", new ix0.a("weightUnit", "TEXT", true, 0, null, 1));
            hashMap.put("heightUnit", new ix0.a("heightUnit", "TEXT", true, 0, null, 1));
            hashMap.put("temperatureUnit", new ix0.a("temperatureUnit", "TEXT", true, 0, null, 1));
            ix0 ix0 = new ix0("user", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "user");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "user(com.portfolio.platform.data.model.MFUser).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `user`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "user");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(5), "086181bfc70c5b6f8463fdc232f2ef29", "1bc666ada9d537e47c26bd62c6cc46c4");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDatabase
    public UserDao userDao() {
        UserDao userDao;
        if (this._userDao != null) {
            return this._userDao;
        }
        synchronized (this) {
            if (this._userDao == null) {
                this._userDao = new UserDao_Impl(this);
            }
            userDao = this._userDao;
        }
        return userDao;
    }
}
