package com.portfolio.platform.data.model.diana;

import com.fossil.pq7;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSetting {
    @DexIgnore
    public String setting;
    @DexIgnore
    public String updatedAt;
    @DexIgnore
    public String watchAppId;

    @DexIgnore
    public WatchAppLastSetting(String str, String str2, String str3) {
        pq7.c(str, "watchAppId");
        pq7.c(str2, "updatedAt");
        pq7.c(str3, MicroAppSetting.SETTING);
        this.watchAppId = str;
        this.updatedAt = str2;
        this.setting = str3;
    }

    @DexIgnore
    public static /* synthetic */ WatchAppLastSetting copy$default(WatchAppLastSetting watchAppLastSetting, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = watchAppLastSetting.watchAppId;
        }
        if ((i & 2) != 0) {
            str2 = watchAppLastSetting.updatedAt;
        }
        if ((i & 4) != 0) {
            str3 = watchAppLastSetting.setting;
        }
        return watchAppLastSetting.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.watchAppId;
    }

    @DexIgnore
    public final String component2() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final WatchAppLastSetting copy(String str, String str2, String str3) {
        pq7.c(str, "watchAppId");
        pq7.c(str2, "updatedAt");
        pq7.c(str3, MicroAppSetting.SETTING);
        return new WatchAppLastSetting(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchAppLastSetting) {
                WatchAppLastSetting watchAppLastSetting = (WatchAppLastSetting) obj;
                if (!pq7.a(this.watchAppId, watchAppLastSetting.watchAppId) || !pq7.a(this.updatedAt, watchAppLastSetting.updatedAt) || !pq7.a(this.setting, watchAppLastSetting.setting)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchAppId() {
        return this.watchAppId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.watchAppId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.updatedAt;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.setting;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setSetting(String str) {
        pq7.c(str, "<set-?>");
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchAppId(String str) {
        pq7.c(str, "<set-?>");
        this.watchAppId = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchAppLastSetting(watchAppId=" + this.watchAppId + ", updatedAt=" + this.updatedAt + ", setting=" + this.setting + ")";
    }
}
