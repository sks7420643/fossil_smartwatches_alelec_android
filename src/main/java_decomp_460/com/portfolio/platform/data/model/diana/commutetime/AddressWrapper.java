package com.portfolio.platform.data.model.diana.commutetime;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.a;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.pq7;
import java.io.Serializable;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressWrapper implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String address;
    @DexIgnore
    public boolean avoidTolls;
    @DexIgnore
    public String id;
    @DexIgnore
    public double lat;
    @DexIgnore
    public double lng;
    @DexIgnore
    public String name;
    @DexIgnore
    public AddressType type;

    @DexIgnore
    public enum AddressType {
        HOME("HOME"),
        WORK("WORK"),
        OTHER("OTHER");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public AddressType(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AddressWrapper> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AddressWrapper createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new AddressWrapper(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AddressWrapper[] newArray(int i) {
            return new AddressWrapper[i];
        }
    }

    @DexIgnore
    public AddressWrapper() {
        String uuid = UUID.randomUUID().toString();
        pq7.b(uuid, "UUID.randomUUID().toString()");
        this.id = uuid;
        this.address = "";
        this.name = "";
        this.type = AddressType.OTHER;
        this.lat = 0.0d;
        this.lng = 0.0d;
        this.avoidTolls = false;
    }

    @DexIgnore
    public AddressWrapper(Parcel parcel) {
        pq7.c(parcel, "in");
        String readString = parcel.readString();
        this.id = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.name = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
        this.address = readString3 == null ? "" : readString3;
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            this.type = (AddressType) readSerializable;
            this.lat = parcel.readDouble();
            this.lng = parcel.readDouble();
            Boolean valueOf = Boolean.valueOf(parcel.readString());
            pq7.b(valueOf, "java.lang.Boolean.valueOf(`in`.readString())");
            this.avoidTolls = valueOf.booleanValue();
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType");
    }

    @DexIgnore
    public AddressWrapper(String str, AddressType addressType) {
        pq7.c(str, "name");
        pq7.c(addressType, "type");
        String uuid = UUID.randomUUID().toString();
        pq7.b(uuid, "UUID.randomUUID().toString()");
        this.id = uuid;
        this.address = "";
        this.name = str;
        this.type = addressType;
        this.lat = 0.0d;
        this.lng = 0.0d;
        this.avoidTolls = false;
    }

    @DexIgnore
    public AddressWrapper(String str, String str2, String str3, AddressType addressType, double d, double d2, boolean z) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(str3, "address");
        pq7.c(addressType, "type");
        this.address = str3;
        this.id = str;
        this.name = str2;
        this.type = addressType;
        this.lat = d;
        this.lng = d2;
        this.avoidTolls = z;
    }

    @DexIgnore
    @Override // java.lang.Object
    public final AddressWrapper clone() {
        return new AddressWrapper(this.id, this.name, this.address, this.type, this.lat, this.lng, this.avoidTolls);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(AddressWrapper.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            AddressWrapper addressWrapper = (AddressWrapper) obj;
            if (!pq7.a(this.id, addressWrapper.id)) {
                return false;
            }
            if (!pq7.a(this.name, addressWrapper.name)) {
                return false;
            }
            if (!pq7.a(this.address, addressWrapper.address)) {
                return false;
            }
            if (this.type != addressWrapper.type) {
                return false;
            }
            if (this.lat != addressWrapper.lat) {
                return false;
            }
            if (this.lng != addressWrapper.lng) {
                return false;
            }
            return this.avoidTolls == addressWrapper.avoidTolls;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.data.model.diana.commutetime.AddressWrapper");
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final boolean getAvoidTolls() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final AddressType getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((this.id.hashCode() * 31) + this.name.hashCode()) * 31) + this.address.hashCode()) * 31) + this.type.hashCode()) * 31) + Double.doubleToLongBits(this.lat)) * 31) + Double.doubleToLongBits(this.lng)) * 31) + a.a(this.avoidTolls);
    }

    @DexIgnore
    public final void setAddress(String str) {
        pq7.c(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setAvoidTolls(boolean z) {
        this.avoidTolls = z;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setType(AddressType addressType) {
        pq7.c(addressType, "<set-?>");
        this.type = addressType;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.address);
        parcel.writeSerializable(this.type);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
        parcel.writeString(String.valueOf(this.avoidTolls));
    }
}
