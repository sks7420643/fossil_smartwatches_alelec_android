package com.portfolio.platform.data.model;

import com.portfolio.platform.data.NotificationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationInfo {
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String body;
    @DexIgnore
    public String packageName;
    @DexIgnore
    public /* final */ String senderInfo;
    @DexIgnore
    public /* final */ NotificationSource source;

    @DexIgnore
    public NotificationInfo(NotificationSource notificationSource, String str, String str2, String str3) {
        this(notificationSource, str, str2, str3, "");
    }

    @DexIgnore
    public NotificationInfo(NotificationSource notificationSource, String str, String str2, String str3, String str4) {
        this.source = notificationSource;
        this.senderInfo = str;
        this.body = str2;
        this.packageName = str3;
        this.appName = str4;
    }

    @DexIgnore
    public String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public String getBody() {
        return this.body;
    }

    @DexIgnore
    public String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public String getSenderInfo() {
        return this.senderInfo;
    }

    @DexIgnore
    public NotificationSource getSource() {
        return this.source;
    }

    @DexIgnore
    public void setPackageName(String str) {
        this.packageName = str;
    }
}
