package com.portfolio.platform.data.model.sleep;

import com.fossil.pq7;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionData {
    @DexIgnore
    public int durationInMinutes;
    @DexIgnore
    public String endTimeString;
    @DexIgnore
    public List<WrapperSleepStateChange> sleepStates;
    @DexIgnore
    public String startTimeString;

    @DexIgnore
    public SleepSessionData(int i, String str, String str2, List<WrapperSleepStateChange> list) {
        pq7.c(str, "startTimeString");
        pq7.c(str2, "endTimeString");
        pq7.c(list, MFSleepSession.COLUMN_SLEEP_STATES);
        this.durationInMinutes = i;
        this.startTimeString = str;
        this.endTimeString = str2;
        this.sleepStates = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.sleep.SleepSessionData */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SleepSessionData copy$default(SleepSessionData sleepSessionData, int i, String str, String str2, List list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = sleepSessionData.durationInMinutes;
        }
        if ((i2 & 2) != 0) {
            str = sleepSessionData.startTimeString;
        }
        if ((i2 & 4) != 0) {
            str2 = sleepSessionData.endTimeString;
        }
        if ((i2 & 8) != 0) {
            list = sleepSessionData.sleepStates;
        }
        return sleepSessionData.copy(i, str, str2, list);
    }

    @DexIgnore
    public final int component1() {
        return this.durationInMinutes;
    }

    @DexIgnore
    public final String component2() {
        return this.startTimeString;
    }

    @DexIgnore
    public final String component3() {
        return this.endTimeString;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> component4() {
        return this.sleepStates;
    }

    @DexIgnore
    public final SleepSessionData copy(int i, String str, String str2, List<WrapperSleepStateChange> list) {
        pq7.c(str, "startTimeString");
        pq7.c(str2, "endTimeString");
        pq7.c(list, MFSleepSession.COLUMN_SLEEP_STATES);
        return new SleepSessionData(i, str, str2, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepSessionData) {
                SleepSessionData sleepSessionData = (SleepSessionData) obj;
                if (this.durationInMinutes != sleepSessionData.durationInMinutes || !pq7.a(this.startTimeString, sleepSessionData.startTimeString) || !pq7.a(this.endTimeString, sleepSessionData.endTimeString) || !pq7.a(this.sleepStates, sleepSessionData.sleepStates)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getDurationInMinutes() {
        return this.durationInMinutes;
    }

    @DexIgnore
    public final String getEndTimeString() {
        return this.endTimeString;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> getSleepStates() {
        return this.sleepStates;
    }

    @DexIgnore
    public final String getStartTimeString() {
        return this.startTimeString;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.durationInMinutes;
        String str = this.startTimeString;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.endTimeString;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        List<WrapperSleepStateChange> list = this.sleepStates;
        if (list != null) {
            i = list.hashCode();
        }
        return ((((hashCode + (i2 * 31)) * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setDurationInMinutes(int i) {
        this.durationInMinutes = i;
    }

    @DexIgnore
    public final void setEndTimeString(String str) {
        pq7.c(str, "<set-?>");
        this.endTimeString = str;
    }

    @DexIgnore
    public final void setSleepStates(List<WrapperSleepStateChange> list) {
        pq7.c(list, "<set-?>");
        this.sleepStates = list;
    }

    @DexIgnore
    public final void setStartTimeString(String str) {
        pq7.c(str, "<set-?>");
        this.startTimeString = str;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionData(durationInMinutes=" + this.durationInMinutes + ", startTimeString=" + this.startTimeString + ", endTimeString=" + this.endTimeString + ", sleepStates=" + this.sleepStates + ")";
    }
}
