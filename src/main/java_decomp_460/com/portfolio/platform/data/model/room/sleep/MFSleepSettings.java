package com.portfolio.platform.data.model.room.sleep;

import com.fossil.rj4;
import com.fossil.zj5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFSleepSettings {
    @DexIgnore
    @zj5
    public int id;
    @DexIgnore
    @rj4("currentGoalMinutes")
    public int sleepGoal;
    @DexIgnore
    @rj4("timezoneOffset")
    public int timezoneOffset;

    @DexIgnore
    public MFSleepSettings(int i) {
        this.sleepGoal = i;
    }

    @DexIgnore
    public static /* synthetic */ MFSleepSettings copy$default(MFSleepSettings mFSleepSettings, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = mFSleepSettings.sleepGoal;
        }
        return mFSleepSettings.copy(i);
    }

    @DexIgnore
    public final int component1() {
        return this.sleepGoal;
    }

    @DexIgnore
    public final MFSleepSettings copy(int i) {
        return new MFSleepSettings(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof MFSleepSettings) && this.sleepGoal == ((MFSleepSettings) obj).sleepGoal);
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getSleepGoal() {
        return this.sleepGoal;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        return this.sleepGoal;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setSleepGoal(int i) {
        this.sleepGoal = i;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepSettings(sleepGoal=" + this.sleepGoal + ")";
    }
}
