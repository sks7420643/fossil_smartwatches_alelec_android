package com.portfolio.platform.data.model.diana.preset;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleItem {
    @DexIgnore
    @rj4("complicationPosition")
    public String position;
    @DexIgnore
    @rj4("ringStyle")
    public RingStyle ringStyle;

    @DexIgnore
    public RingStyleItem(String str, RingStyle ringStyle2) {
        pq7.c(str, "position");
        pq7.c(ringStyle2, "ringStyle");
        this.position = str;
        this.ringStyle = ringStyle2;
    }

    @DexIgnore
    public static /* synthetic */ RingStyleItem copy$default(RingStyleItem ringStyleItem, String str, RingStyle ringStyle2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringStyleItem.position;
        }
        if ((i & 2) != 0) {
            ringStyle2 = ringStyleItem.ringStyle;
        }
        return ringStyleItem.copy(str, ringStyle2);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final RingStyle component2() {
        return this.ringStyle;
    }

    @DexIgnore
    public final RingStyleItem copy(String str, RingStyle ringStyle2) {
        pq7.c(str, "position");
        pq7.c(ringStyle2, "ringStyle");
        return new RingStyleItem(str, ringStyle2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof RingStyleItem) {
                RingStyleItem ringStyleItem = (RingStyleItem) obj;
                if (!pq7.a(this.position, ringStyleItem.position) || !pq7.a(this.ringStyle, ringStyleItem.ringStyle)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final RingStyle getRingStyle() {
        return this.ringStyle;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.position;
        int hashCode = str != null ? str.hashCode() : 0;
        RingStyle ringStyle2 = this.ringStyle;
        if (ringStyle2 != null) {
            i = ringStyle2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setPosition(String str) {
        pq7.c(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setRingStyle(RingStyle ringStyle2) {
        pq7.c(ringStyle2, "<set-?>");
        this.ringStyle = ringStyle2;
    }

    @DexIgnore
    public String toString() {
        return "RingStyleItem(position=" + this.position + ", ringStyle=" + this.ringStyle + ")";
    }
}
