package com.portfolio.platform.data.model.watchface;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRing {
    @DexIgnore
    @rj4("category")
    public String category;
    @DexIgnore
    @rj4("createdAt")
    public String createdAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @rj4("data")
    public Data data;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("metadata")
    public MetaData metaData;
    @DexIgnore
    @rj4("name")
    public String name;
    @DexIgnore
    @rj4("updatedAt")
    public String updatedAt; // = "2016-01-01T01:01:01.001Z";

    @DexIgnore
    public DianaWatchFaceRing(String str, String str2, String str3, Data data2, MetaData metaData2) {
        pq7.c(str, "id");
        pq7.c(str2, "category");
        pq7.c(str3, "name");
        pq7.c(data2, "data");
        pq7.c(metaData2, "metaData");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final void setCategory(String str) {
        pq7.c(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        pq7.c(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        pq7.c(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.updatedAt = str;
    }
}
