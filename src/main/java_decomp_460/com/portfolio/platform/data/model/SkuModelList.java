package com.portfolio.platform.data.model;

import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.rj4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkuModelList {
    @DexIgnore
    @pj4
    @rj4("items")
    public List<SKUModel> skuModelList;

    @DexIgnore
    public SkuModelList(List<SKUModel> list) {
        pq7.c(list, "skuModelList");
        this.skuModelList = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.SkuModelList */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SkuModelList copy$default(SkuModelList skuModelList2, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = skuModelList2.skuModelList;
        }
        return skuModelList2.copy(list);
    }

    @DexIgnore
    public final List<SKUModel> component1() {
        return this.skuModelList;
    }

    @DexIgnore
    public final SkuModelList copy(List<SKUModel> list) {
        pq7.c(list, "skuModelList");
        return new SkuModelList(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof SkuModelList) && pq7.a(this.skuModelList, ((SkuModelList) obj).skuModelList));
    }

    @DexIgnore
    public final List<SKUModel> getSkuModelList() {
        return this.skuModelList;
    }

    @DexIgnore
    public int hashCode() {
        List<SKUModel> list = this.skuModelList;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setSkuModelList(List<SKUModel> list) {
        pq7.c(list, "<set-?>");
        this.skuModelList = list;
    }

    @DexIgnore
    public String toString() {
        return "SkuModelList(skuModelList=" + this.skuModelList + ")";
    }
}
