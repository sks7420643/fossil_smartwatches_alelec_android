package com.portfolio.platform.data.model.watchface;

import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceOrder {
    @DexIgnore
    @rj4("id")
    public String orderId;
    @DexIgnore
    @rj4("packageVersion")
    public String packageVersion;
    @DexIgnore
    @rj4("itemId")
    public String watchFaceId;

    @DexIgnore
    public DianaWatchFaceOrder(String str, String str2, String str3) {
        this.orderId = str;
        this.watchFaceId = str2;
        this.packageVersion = str3;
    }

    @DexIgnore
    public static /* synthetic */ DianaWatchFaceOrder copy$default(DianaWatchFaceOrder dianaWatchFaceOrder, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaWatchFaceOrder.orderId;
        }
        if ((i & 2) != 0) {
            str2 = dianaWatchFaceOrder.watchFaceId;
        }
        if ((i & 4) != 0) {
            str3 = dianaWatchFaceOrder.packageVersion;
        }
        return dianaWatchFaceOrder.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.orderId;
    }

    @DexIgnore
    public final String component2() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final String component3() {
        return this.packageVersion;
    }

    @DexIgnore
    public final DianaWatchFaceOrder copy(String str, String str2, String str3) {
        return new DianaWatchFaceOrder(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaWatchFaceOrder) {
                DianaWatchFaceOrder dianaWatchFaceOrder = (DianaWatchFaceOrder) obj;
                if (!pq7.a(this.orderId, dianaWatchFaceOrder.orderId) || !pq7.a(this.watchFaceId, dianaWatchFaceOrder.watchFaceId) || !pq7.a(this.packageVersion, dianaWatchFaceOrder.packageVersion)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getOrderId() {
        return this.orderId;
    }

    @DexIgnore
    public final String getPackageVersion() {
        return this.packageVersion;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.orderId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.watchFaceId;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.packageVersion;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setOrderId(String str) {
        this.orderId = str;
    }

    @DexIgnore
    public final void setPackageVersion(String str) {
        this.packageVersion = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        this.watchFaceId = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaWatchFaceOrder(orderId=" + this.orderId + ", watchFaceId=" + this.watchFaceId + ", packageVersion=" + this.packageVersion + ")";
    }
}
