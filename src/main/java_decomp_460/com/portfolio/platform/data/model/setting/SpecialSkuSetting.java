package com.portfolio.platform.data.model.setting;

import com.fossil.kq7;
import com.fossil.nk5;
import com.fossil.pq7;
import com.fossil.vt7;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SpecialSkuSetting {

    @DexIgnore
    public enum AngleSubeye {
        NONE(0, 0, 0),
        MOVEMBER(113, 158, Action.Selfie.TAKE_BURST);
        
        @DexIgnore
        public /* final */ int angleForApp;
        @DexIgnore
        public /* final */ int angleForCall;
        @DexIgnore
        public /* final */ int angleForSms;

        @DexIgnore
        public AngleSubeye(int i, int i2, int i3) {
            this.angleForApp = i;
            this.angleForCall = i2;
            this.angleForSms = i3;
        }

        @DexIgnore
        public final int getAngleForApp() {
            return this.angleForApp;
        }

        @DexIgnore
        public final int getAngleForCall() {
            return this.angleForCall;
        }

        @DexIgnore
        public final int getAngleForSms() {
            return this.angleForSms;
        }
    }

    @DexIgnore
    public enum SpecialSku {
        NONE("NONE", AngleSubeye.NONE, ""),
        MOVEMBER("FTW1175", AngleSubeye.MOVEMBER, "W0FA01");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ AngleSubeye angleSubeye;
        @DexIgnore
        public /* final */ String prefixSerialNumber;
        @DexIgnore
        public /* final */ String sku;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final SpecialSku fromSerialNumber(String str) {
                pq7.c(str, "serial");
                SpecialSku[] values = SpecialSku.values();
                for (SpecialSku specialSku : values) {
                    if (vt7.j(nk5.o.m(str), specialSku.getPrefixSerialNumber(), true)) {
                        return specialSku;
                    }
                }
                return SpecialSku.NONE;
            }

            @DexIgnore
            public final SpecialSku fromType(String str) {
                pq7.c(str, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                SpecialSku[] values = SpecialSku.values();
                for (SpecialSku specialSku : values) {
                    if (vt7.j(specialSku.getSku(), str, true)) {
                        return specialSku;
                    }
                }
                return SpecialSku.NONE;
            }
        }

        @DexIgnore
        public SpecialSku(String str, AngleSubeye angleSubeye2, String str2) {
            this.sku = str;
            this.angleSubeye = angleSubeye2;
            this.prefixSerialNumber = str2;
        }

        @DexIgnore
        public final AngleSubeye getAngleSubeye() {
            return this.angleSubeye;
        }

        @DexIgnore
        public final String getPrefixSerialNumber() {
            return this.prefixSerialNumber;
        }

        @DexIgnore
        public final String getSku() {
            return this.sku;
        }
    }
}
