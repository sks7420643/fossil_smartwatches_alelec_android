package com.portfolio.platform.data.model.microapp.weather;

import com.facebook.places.model.PlaceFields;
import com.fossil.di5;
import com.fossil.kq7;
import com.fossil.lk5;
import com.fossil.lr7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.CurrentWeatherInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherDayForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherHourForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Weather {
    @DexIgnore
    public String address; // = "";
    @DexIgnore
    @rj4("currently")
    public /* final */ Currently currently;
    @DexIgnore
    @rj4("daily")
    public /* final */ ArrayList<Daily> daily;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_EXPIRED_AT)
    public /* final */ DateTime expiredAt;
    @DexIgnore
    @rj4("hourly")
    public /* final */ ArrayList<Hourly> hourly;
    @DexIgnore
    @rj4(PlaceFields.LOCATION)
    public /* final */ Location location;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Currently {
        @DexIgnore
        @rj4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @rj4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @rj4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @rj4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @rj4("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @rj4(LogBuilder.KEY_TIME)
        public /* final */ DateTime time;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Currently() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Daily {
        @DexIgnore
        @rj4("date")
        public /* final */ Date date;
        @DexIgnore
        @rj4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @rj4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @rj4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @rj4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @rj4("temperature")
        public /* final */ Temperature temperature;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Daily() {
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Hourly {
        @DexIgnore
        @rj4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @rj4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @rj4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @rj4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @rj4("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @rj4(LogBuilder.KEY_TIME)
        public /* final */ DateTime time;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hourly() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Latlong {
        @DexIgnore
        @rj4(Constants.LAT)
        public /* final */ float lat;
        @DexIgnore
        @rj4("lng")
        public /* final */ float lng;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Latlong() {
        }

        @DexIgnore
        public final float getLat() {
            return this.lat;
        }

        @DexIgnore
        public final float getLng() {
            return this.lng;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Location {
        @DexIgnore
        @rj4("cachedAt")
        public /* final */ Latlong cachedAt;
        @DexIgnore
        @rj4("requestedAt")
        public /* final */ Latlong requestedAt;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Location() {
        }

        @DexIgnore
        public final Latlong getCachedAt() {
            return this.cachedAt;
        }

        @DexIgnore
        public final Latlong getRequestedAt() {
            return this.requestedAt;
        }
    }

    @DexIgnore
    public enum TEMP_UNIT {
        CELSIUS("c"),
        FAHRENHEIT("f");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final TEMP_UNIT getTempUnit(String str) {
                pq7.c(str, "value");
                TEMP_UNIT[] values = TEMP_UNIT.values();
                for (TEMP_UNIT temp_unit : values) {
                    if (pq7.a(temp_unit.getValue(), str)) {
                        return temp_unit;
                    }
                }
                return TEMP_UNIT.CELSIUS;
            }
        }

        @DexIgnore
        public TEMP_UNIT(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[TEMP_UNIT.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[TEMP_UNIT.CELSIUS.ordinal()] = 1;
        }
        */
    }

    @DexIgnore
    public Weather(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        pq7.c(location2, PlaceFields.LOCATION);
        pq7.c(currently2, "currently");
        pq7.c(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        pq7.c(arrayList, "daily");
        pq7.c(arrayList2, "hourly");
        this.location = location2;
        this.currently = currently2;
        this.expiredAt = dateTime;
        this.daily = arrayList;
        this.hourly = arrayList2;
    }

    @DexIgnore
    public static /* synthetic */ Weather copy$default(Weather weather, Location location2, Currently currently2, DateTime dateTime, ArrayList arrayList, ArrayList arrayList2, int i, Object obj) {
        return weather.copy((i & 1) != 0 ? weather.location : location2, (i & 2) != 0 ? weather.currently : currently2, (i & 4) != 0 ? weather.expiredAt : dateTime, (i & 8) != 0 ? weather.daily : arrayList, (i & 16) != 0 ? weather.hourly : arrayList2);
    }

    @DexIgnore
    private final long getExpiredTimeInSecondForWatchApp() {
        Calendar X = lk5.X(Long.valueOf(this.expiredAt.getMillis()));
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        if (X.compareTo(lk5.X(Long.valueOf(instance.getTimeInMillis()))) <= 0) {
            return this.expiredAt.getMillis() / ((long) 1000);
        }
        Calendar X2 = lk5.X(Long.valueOf(this.expiredAt.getMillis()));
        pq7.b(X2, "calendar");
        return X2.getTimeInMillis() / ((long) 1000);
    }

    @DexIgnore
    private final WeatherDayForecast.WeatherWeekDay getWeatherWeekDay(Date date) {
        switch (lk5.B(Long.valueOf(date.getTime()))) {
            case 1:
                return WeatherDayForecast.WeatherWeekDay.SUNDAY;
            case 2:
                return WeatherDayForecast.WeatherWeekDay.MONDAY;
            case 3:
                return WeatherDayForecast.WeatherWeekDay.TUESDAY;
            case 4:
                return WeatherDayForecast.WeatherWeekDay.WEDNESDAY;
            case 5:
                return WeatherDayForecast.WeatherWeekDay.THURSDAY;
            case 6:
                return WeatherDayForecast.WeatherWeekDay.FRIDAY;
            default:
                return WeatherDayForecast.WeatherWeekDay.SATURDAY;
        }
    }

    @DexIgnore
    public final Location component1() {
        return this.location;
    }

    @DexIgnore
    public final Currently component2() {
        return this.currently;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Daily> component4() {
        return this.daily;
    }

    @DexIgnore
    public final ArrayList<Hourly> component5() {
        return this.hourly;
    }

    @DexIgnore
    public final Weather copy(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        pq7.c(location2, PlaceFields.LOCATION);
        pq7.c(currently2, "currently");
        pq7.c(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        pq7.c(arrayList, "daily");
        pq7.c(arrayList2, "hourly");
        return new Weather(location2, currently2, dateTime, arrayList, arrayList2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Weather) {
                Weather weather = (Weather) obj;
                if (!pq7.a(this.location, weather.location) || !pq7.a(this.currently, weather.currently) || !pq7.a(this.expiredAt, weather.expiredAt) || !pq7.a(this.daily, weather.daily) || !pq7.a(this.hourly, weather.hourly)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final Currently getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public final ArrayList<Daily> getDaily() {
        return this.daily;
    }

    @DexIgnore
    public final DateTime getExpiredAt() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Hourly> getHourly() {
        return this.hourly;
    }

    @DexIgnore
    public final Location getLocation() {
        return this.location;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Location location2 = this.location;
        int hashCode = location2 != null ? location2.hashCode() : 0;
        Currently currently2 = this.currently;
        int hashCode2 = currently2 != null ? currently2.hashCode() : 0;
        DateTime dateTime = this.expiredAt;
        int hashCode3 = dateTime != null ? dateTime.hashCode() : 0;
        ArrayList<Daily> arrayList = this.daily;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        ArrayList<Hourly> arrayList2 = this.hourly;
        if (arrayList2 != null) {
            i = arrayList2.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        pq7.c(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public final ChanceOfRainComplicationAppInfo toChanceOfRainComplicationAppInfo() {
        return new ChanceOfRainComplicationAppInfo(lr7.b(this.currently.getRainProbability() * ((float) 100)), this.expiredAt.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public String toString() {
        return "Weather(location=" + this.location + ", currently=" + this.currently + ", expiredAt=" + this.expiredAt + ", daily=" + this.daily + ", hourly=" + this.hourly + ")";
    }

    @DexIgnore
    public final WeatherComplicationAppInfo toWeatherComplicationAppInfo() {
        WeatherComplicationAppInfo.WeatherCondition a2 = di5.Companion.a(this.currently.getForecast());
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            return new WeatherComplicationAppInfo(this.currently.getTemperature().getCurrently(), pq7.a(temperature.getUnit(), TEMP_UNIT.FAHRENHEIT.getValue()) ? WeatherComplicationAppInfo.TemperatureUnit.F : WeatherComplicationAppInfo.TemperatureUnit.C, a2, this.expiredAt.getMillis() / ((long) 1000));
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final WeatherWatchAppInfo toWeatherWatchAppInfo() {
        int b = lr7.b(this.currently.getRainProbability() * ((float) 100));
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            CurrentWeatherInfo currentWeatherInfo = new CurrentWeatherInfo(b, temperature.getCurrently(), di5.Companion.a(this.currently.getForecast()), this.currently.getTemperature().getMax(), this.currently.getTemperature().getMin());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<Daily> it = this.daily.iterator();
            while (it.hasNext()) {
                Daily next = it.next();
                Temperature temperature2 = next.getTemperature();
                if (temperature2 != null) {
                    float max = temperature2.getMax();
                    float min = next.getTemperature().getMin();
                    WeatherComplicationAppInfo.WeatherCondition a2 = di5.Companion.a(next.getForecast());
                    Date date = next.getDate();
                    if (date != null) {
                        arrayList.add(new WeatherDayForecast(max, min, a2, getWeatherWeekDay(date)));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            int K = lk5.K(Long.valueOf(instance.getTimeInMillis()));
            Iterator<Hourly> it2 = this.hourly.iterator();
            while (it2.hasNext()) {
                Hourly next2 = it2.next();
                DateTime time = next2.getTime();
                if (time != null) {
                    int K2 = lk5.K(Long.valueOf(time.getMillis()));
                    if ((((K2 - K) + 24) - 1) % 4 == 0) {
                        Temperature temperature3 = next2.getTemperature();
                        if (temperature3 != null) {
                            arrayList2.add(new WeatherHourForecast(K2, temperature3.getCurrently(), di5.Companion.a(next2.getForecast())));
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            TEMP_UNIT.Companion companion = TEMP_UNIT.Companion;
            String unit = this.currently.getTemperature().getUnit();
            pq7.b(unit, "currently.temperature.unit");
            return new WeatherWatchAppInfo(this.address, WhenMappings.$EnumSwitchMapping$0[companion.getTempUnit(unit).ordinal()] != 1 ? UserDisplayUnit.TemperatureUnit.F : UserDisplayUnit.TemperatureUnit.C, currentWeatherInfo, arrayList, arrayList2, getExpiredTimeInSecondForWatchApp());
        }
        pq7.i();
        throw null;
    }
}
