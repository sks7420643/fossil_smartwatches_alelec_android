package com.portfolio.platform.data.model.room;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.iw0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.fossil.xw0;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDao_Impl implements UserDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ iw0<MFUser> __deletionAdapterOfMFUser;
    @DexIgnore
    public /* final */ jw0<MFUser> __insertionAdapterOfMFUser;
    @DexIgnore
    public /* final */ xw0 __preparedStmtOfClearAllUser;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<MFUser> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, MFUser mFUser) {
            if (mFUser.getPinType() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, mFUser.getPinType());
            }
            if (mFUser.getCreatedAt() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, mFUser.getCreatedAt());
            }
            if (mFUser.getUpdatedAt() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, mFUser.getUpdatedAt());
            }
            if (mFUser.getUserId() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, mFUser.getUserId());
            }
            if (mFUser.getAuthType() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, mFUser.getAuthType());
            }
            if (mFUser.getBirthday() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, mFUser.getBirthday());
            }
            if (mFUser.getBrand() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, mFUser.getBrand());
            }
            px0.bindLong(8, mFUser.getDiagnosticEnabled() ? 1 : 0);
            if (mFUser.getEmail() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, mFUser.getEmail());
            }
            px0.bindLong(10, mFUser.getEmailOptIn() ? 1 : 0);
            px0.bindLong(11, mFUser.getEmailVerified() ? 1 : 0);
            if (mFUser.getFirstName() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, mFUser.getFirstName());
            }
            if (mFUser.getGender() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, mFUser.getGender());
            }
            px0.bindLong(14, (long) mFUser.getHeightInCentimeters());
            if (mFUser.getIntegrations() == null) {
                px0.bindNull(15);
            } else {
                px0.bindString(15, mFUser.getIntegrations());
            }
            if (mFUser.getLastName() == null) {
                px0.bindNull(16);
            } else {
                px0.bindString(16, mFUser.getLastName());
            }
            if (mFUser.getProfilePicture() == null) {
                px0.bindNull(17);
            } else {
                px0.bindString(17, mFUser.getProfilePicture());
            }
            if (mFUser.getRegisterDate() == null) {
                px0.bindNull(18);
            } else {
                px0.bindString(18, mFUser.getRegisterDate());
            }
            px0.bindLong(19, mFUser.getRegistrationComplete() ? 1 : 0);
            px0.bindLong(20, mFUser.getUseDefaultBiometric() ? 1 : 0);
            px0.bindLong(21, mFUser.isOnboardingComplete() ? 1 : 0);
            px0.bindLong(22, mFUser.getUseDefaultGoals() ? 1 : 0);
            if (mFUser.getUsername() == null) {
                px0.bindNull(23);
            } else {
                px0.bindString(23, mFUser.getUsername());
            }
            px0.bindLong(24, (long) mFUser.getAverageSleep());
            px0.bindLong(25, (long) mFUser.getAverageStep());
            if (mFUser.getActiveDeviceId() == null) {
                px0.bindNull(26);
            } else {
                px0.bindString(26, mFUser.getActiveDeviceId());
            }
            px0.bindLong(27, (long) mFUser.getWeightInGrams());
            MFUser.Auth auth = mFUser.getAuth();
            if (auth != null) {
                if (auth.getAccessToken() == null) {
                    px0.bindNull(28);
                } else {
                    px0.bindString(28, auth.getAccessToken());
                }
                if (auth.getRefreshToken() == null) {
                    px0.bindNull(29);
                } else {
                    px0.bindString(29, auth.getRefreshToken());
                }
                if (auth.getAccessTokenExpiresAt() == null) {
                    px0.bindNull(30);
                } else {
                    px0.bindString(30, auth.getAccessTokenExpiresAt());
                }
                px0.bindLong(31, (long) auth.getAccessTokenExpiresIn());
            } else {
                px0.bindNull(28);
                px0.bindNull(29);
                px0.bindNull(30);
                px0.bindNull(31);
            }
            MFUser.Address addresses = mFUser.getAddresses();
            if (addresses != null) {
                if (addresses.getHome() == null) {
                    px0.bindNull(32);
                } else {
                    px0.bindString(32, addresses.getHome());
                }
                if (addresses.getWork() == null) {
                    px0.bindNull(33);
                } else {
                    px0.bindString(33, addresses.getWork());
                }
            } else {
                px0.bindNull(32);
                px0.bindNull(33);
            }
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                if (unitGroup.getDistance() == null) {
                    px0.bindNull(34);
                } else {
                    px0.bindString(34, unitGroup.getDistance());
                }
                if (unitGroup.getWeight() == null) {
                    px0.bindNull(35);
                } else {
                    px0.bindString(35, unitGroup.getWeight());
                }
                if (unitGroup.getHeight() == null) {
                    px0.bindNull(36);
                } else {
                    px0.bindString(36, unitGroup.getHeight());
                }
                if (unitGroup.getTemperature() == null) {
                    px0.bindNull(37);
                } else {
                    px0.bindString(37, unitGroup.getTemperature());
                }
            } else {
                px0.bindNull(34);
                px0.bindNull(35);
                px0.bindNull(36);
                px0.bindNull(37);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `user` (`pinType`,`createdAt`,`updatedAt`,`uid`,`authType`,`birthday`,`brand`,`diagnosticEnabled`,`email`,`emailOptIn`,`emailVerified`,`firstName`,`gender`,`heightInCentimeters`,`integrations`,`lastName`,`profilePicture`,`registerDate`,`registrationComplete`,`useDefaultBiometric`,`isOnboardingComplete`,`useDefaultGoals`,`username`,`averageSleep`,`averageStep`,`activeDeviceId`,`weightInGrams`,`userAccessToken`,`refreshToken`,`accessTokenExpiresAt`,`accessTokenExpiresIn`,`home`,`work`,`distanceUnit`,`weightUnit`,`heightUnit`,`temperatureUnit`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends iw0<MFUser> {
        @DexIgnore
        public Anon2(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, MFUser mFUser) {
            if (mFUser.getUserId() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, mFUser.getUserId());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `user` WHERE `uid` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xw0 {
        @DexIgnore
        public Anon3(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM user";
        }
    }

    @DexIgnore
    public UserDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfMFUser = new Anon1(qw0);
        this.__deletionAdapterOfMFUser = new Anon2(qw0);
        this.__preparedStmtOfClearAllUser = new Anon3(qw0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void clearAllUser() {
        this.__db.assertNotSuspendingTransaction();
        px0 acquire = this.__preparedStmtOfClearAllUser.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllUser.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void deleteUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfMFUser.handle(mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public MFUser getCurrentUser() {
        Throwable th;
        MFUser mFUser;
        tw0 f = tw0.f("SELECT * FROM user", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "pinType");
            int c2 = dx0.c(b, "createdAt");
            int c3 = dx0.c(b, "updatedAt");
            int c4 = dx0.c(b, "uid");
            int c5 = dx0.c(b, Constants.PROFILE_KEY_AUTHTYPE);
            int c6 = dx0.c(b, Constants.PROFILE_KEY_BIRTHDAY);
            int c7 = dx0.c(b, Constants.PROFILE_KEY_BRAND);
            int c8 = dx0.c(b, Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE);
            int c9 = dx0.c(b, Constants.EMAIL);
            int c10 = dx0.c(b, Constants.PROFILE_KEY_EMAIL_OPT_IN);
            int c11 = dx0.c(b, "emailVerified");
            int c12 = dx0.c(b, Constants.PROFILE_KEY_FIRST_NAME);
            int c13 = dx0.c(b, "gender");
            int c14 = dx0.c(b, Constants.PROFILE_KEY_HEIGHT_IN_CM);
            try {
                int c15 = dx0.c(b, Constants.PROFILE_KEY_INTEGRATIONS);
                int c16 = dx0.c(b, Constants.PROFILE_KEY_LAST_NAME);
                int c17 = dx0.c(b, Constants.PROFILE_KEY_PROFILE_PIC);
                int c18 = dx0.c(b, Constants.PROFILE_KEY_REGISTER_DATE);
                int c19 = dx0.c(b, Constants.PROFILE_KEY_REGISTRATION_COMPLETE);
                int c20 = dx0.c(b, "useDefaultBiometric");
                int c21 = dx0.c(b, Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE);
                int c22 = dx0.c(b, "useDefaultGoals");
                int c23 = dx0.c(b, "username");
                int c24 = dx0.c(b, "averageSleep");
                int c25 = dx0.c(b, "averageStep");
                int c26 = dx0.c(b, "activeDeviceId");
                int c27 = dx0.c(b, Constants.PROFILE_KEY_WEIGHT_IN_GRAMS);
                int c28 = dx0.c(b, "userAccessToken");
                int c29 = dx0.c(b, Constants.PROFILE_KEY_REFRESH_TOKEN);
                int c30 = dx0.c(b, "accessTokenExpiresAt");
                int c31 = dx0.c(b, "accessTokenExpiresIn");
                int c32 = dx0.c(b, "home");
                int c33 = dx0.c(b, "work");
                int c34 = dx0.c(b, "distanceUnit");
                int c35 = dx0.c(b, "weightUnit");
                int c36 = dx0.c(b, "heightUnit");
                int c37 = dx0.c(b, "temperatureUnit");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    String string4 = b.getString(c7);
                    boolean z = b.getInt(c8) != 0;
                    String string5 = b.getString(c9);
                    boolean z2 = b.getInt(c10) != 0;
                    boolean z3 = b.getInt(c11) != 0;
                    String string6 = b.getString(c12);
                    String string7 = b.getString(c13);
                    int i = b.getInt(c14);
                    String string8 = b.getString(c15);
                    String string9 = b.getString(c16);
                    String string10 = b.getString(c17);
                    String string11 = b.getString(c18);
                    boolean z4 = b.getInt(c19) != 0;
                    boolean z5 = b.getInt(c20) != 0;
                    boolean z6 = b.getInt(c21) != 0;
                    boolean z7 = b.getInt(c22) != 0;
                    String string12 = b.getString(c23);
                    int i2 = b.getInt(c24);
                    int i3 = b.getInt(c25);
                    String string13 = b.getString(c26);
                    int i4 = b.getInt(c27);
                    MFUser.Auth auth = (!b.isNull(c28) || !b.isNull(c29) || !b.isNull(c30) || !b.isNull(c31)) ? new MFUser.Auth(b.getString(c28), b.getString(c29), b.getString(c30), b.getInt(c31)) : null;
                    mFUser = new MFUser(string, (!b.isNull(c32) || !b.isNull(c33)) ? new MFUser.Address(b.getString(c32), b.getString(c33)) : null, string2, string3, string4, z, string5, z2, z3, string6, string7, i, string8, string9, string10, string11, z4, (!b.isNull(c34) || !b.isNull(c35) || !b.isNull(c36) || !b.isNull(c37)) ? new MFUser.UnitGroup(b.getString(c34), b.getString(c35), b.getString(c36), b.getString(c37)) : null, z5, z6, z7, string12, i2, i3, string13, i4);
                    mFUser.setPinType(b.getString(c));
                    mFUser.setCreatedAt(b.getString(c2));
                    mFUser.setUpdatedAt(b.getString(c3));
                    mFUser.setAuth(auth);
                } else {
                    mFUser = null;
                }
                b.close();
                f.m();
                return mFUser;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void insertUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert((jw0<MFUser>) mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void updateUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert((jw0<MFUser>) mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
