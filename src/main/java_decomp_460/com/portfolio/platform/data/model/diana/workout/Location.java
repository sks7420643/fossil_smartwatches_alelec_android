package com.portfolio.platform.data.model.diana.workout;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Location {
    @DexIgnore
    public /* final */ long latitude;
    @DexIgnore
    public /* final */ long longitude;

    @DexIgnore
    public Location(long j, long j2) {
        this.latitude = j;
        this.longitude = j2;
    }

    @DexIgnore
    private final long component1() {
        return this.latitude;
    }

    @DexIgnore
    private final long component2() {
        return this.longitude;
    }

    @DexIgnore
    public static /* synthetic */ Location copy$default(Location location, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = location.latitude;
        }
        if ((i & 2) != 0) {
            j2 = location.longitude;
        }
        return location.copy(j, j2);
    }

    @DexIgnore
    public final Location copy(long j, long j2) {
        return new Location(j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Location) {
                Location location = (Location) obj;
                if (!(this.latitude == location.latitude && this.longitude == location.longitude)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (c.a(this.latitude) * 31) + c.a(this.longitude);
    }

    @DexIgnore
    public String toString() {
        return "Location(latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
    }
}
