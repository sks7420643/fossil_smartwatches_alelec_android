package com.portfolio.platform.data.model.diana.preset;

import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyle {
    @DexIgnore
    @rj4("data")
    public Data data;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("metadata")
    public MetaData metadata;

    @DexIgnore
    public RingStyle(String str, Data data2, MetaData metaData) {
        pq7.c(str, "id");
        pq7.c(data2, "data");
        this.id = str;
        this.data = data2;
        this.metadata = metaData;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RingStyle(String str, Data data2, MetaData metaData, int i, kq7 kq7) {
        this(str, data2, (i & 4) != 0 ? null : metaData);
    }

    @DexIgnore
    public static /* synthetic */ RingStyle copy$default(RingStyle ringStyle, String str, Data data2, MetaData metaData, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringStyle.id;
        }
        if ((i & 2) != 0) {
            data2 = ringStyle.data;
        }
        if ((i & 4) != 0) {
            metaData = ringStyle.metadata;
        }
        return ringStyle.copy(str, data2, metaData);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component3() {
        return this.metadata;
    }

    @DexIgnore
    public final RingStyle copy(String str, Data data2, MetaData metaData) {
        pq7.c(str, "id");
        pq7.c(data2, "data");
        return new RingStyle(str, data2, metaData);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof RingStyle) {
                RingStyle ringStyle = (RingStyle) obj;
                if (!pq7.a(this.id, ringStyle.id) || !pq7.a(this.data, ringStyle.data) || !pq7.a(this.metadata, ringStyle.metadata)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        Data data2 = this.data;
        int hashCode2 = data2 != null ? data2.hashCode() : 0;
        MetaData metaData = this.metadata;
        if (metaData != null) {
            i = metaData.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setData(Data data2) {
        pq7.c(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetadata(MetaData metaData) {
        this.metadata = metaData;
    }

    @DexIgnore
    public String toString() {
        return "RingStyle(id=" + this.id + ", data=" + this.data + ", metadata=" + this.metadata + ")";
    }
}
