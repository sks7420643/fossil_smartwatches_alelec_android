package com.portfolio.platform.data.model.room;

import com.fossil.ax0;
import com.fossil.kq7;
import com.fossil.qw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UserDatabase extends qw0 {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_3_TO_4; // = new UserDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1(3, 4);
    @DexIgnore
    public static /* final */ ax0 MIGRATION_FROM_4_TO_5; // = new UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1(4, 5);
    @DexIgnore
    public static /* final */ String TAG; // = "UserDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_3_TO_4$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_4_TO_5$annotations() {
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_3_TO_4() {
            return UserDatabase.MIGRATION_FROM_3_TO_4;
        }

        @DexIgnore
        public final ax0 getMIGRATION_FROM_4_TO_5() {
            return UserDatabase.MIGRATION_FROM_4_TO_5;
        }
    }

    @DexIgnore
    public abstract UserDao userDao();
}
