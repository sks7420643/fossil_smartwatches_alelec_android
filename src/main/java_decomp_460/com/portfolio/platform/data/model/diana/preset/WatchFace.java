package com.portfolio.platform.data.model.diana.preset;

import com.fossil.pq7;
import com.fossil.rj4;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFace {
    @DexIgnore
    @rj4(Explore.COLUMN_BACKGROUND)
    public Background background;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("name")
    public String name;
    @DexIgnore
    @rj4("previewUrl")
    public String previewUrl;
    @DexIgnore
    @rj4("ringStyles")
    public ArrayList<RingStyleItem> ringStyleItems;
    @DexIgnore
    public String serial;
    @DexIgnore
    public int watchFaceType;

    @DexIgnore
    public WatchFace(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4, int i) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(background2, Explore.COLUMN_BACKGROUND);
        pq7.c(str3, "previewUrl");
        pq7.c(str4, "serial");
        this.id = str;
        this.name = str2;
        this.ringStyleItems = arrayList;
        this.background = background2;
        this.previewUrl = str3;
        this.serial = str4;
        this.watchFaceType = i;
    }

    @DexIgnore
    public static /* synthetic */ WatchFace copy$default(WatchFace watchFace, String str, String str2, ArrayList arrayList, Background background2, String str3, String str4, int i, int i2, Object obj) {
        return watchFace.copy((i2 & 1) != 0 ? watchFace.id : str, (i2 & 2) != 0 ? watchFace.name : str2, (i2 & 4) != 0 ? watchFace.ringStyleItems : arrayList, (i2 & 8) != 0 ? watchFace.background : background2, (i2 & 16) != 0 ? watchFace.previewUrl : str3, (i2 & 32) != 0 ? watchFace.serial : str4, (i2 & 64) != 0 ? watchFace.watchFaceType : i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> component3() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final Background component4() {
        return this.background;
    }

    @DexIgnore
    public final String component5() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String component6() {
        return this.serial;
    }

    @DexIgnore
    public final int component7() {
        return this.watchFaceType;
    }

    @DexIgnore
    public final WatchFace copy(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4, int i) {
        pq7.c(str, "id");
        pq7.c(str2, "name");
        pq7.c(background2, Explore.COLUMN_BACKGROUND);
        pq7.c(str3, "previewUrl");
        pq7.c(str4, "serial");
        return new WatchFace(str, str2, arrayList, background2, str3, str4, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchFace) {
                WatchFace watchFace = (WatchFace) obj;
                if (!pq7.a(this.id, watchFace.id) || !pq7.a(this.name, watchFace.name) || !pq7.a(this.ringStyleItems, watchFace.ringStyleItems) || !pq7.a(this.background, watchFace.background) || !pq7.a(this.previewUrl, watchFace.previewUrl) || !pq7.a(this.serial, watchFace.serial) || this.watchFaceType != watchFace.watchFaceType) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Background getBackground() {
        return this.background;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getPreviewUrl() {
        return this.previewUrl;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> getRingStyleItems() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final int getWatchFaceType() {
        return this.watchFaceType;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        ArrayList<RingStyleItem> arrayList = this.ringStyleItems;
        int hashCode3 = arrayList != null ? arrayList.hashCode() : 0;
        Background background2 = this.background;
        int hashCode4 = background2 != null ? background2.hashCode() : 0;
        String str3 = this.previewUrl;
        int hashCode5 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.serial;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i) * 31) + this.watchFaceType;
    }

    @DexIgnore
    public final void setBackground(Background background2) {
        pq7.c(background2, "<set-?>");
        this.background = background2;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPreviewUrl(String str) {
        pq7.c(str, "<set-?>");
        this.previewUrl = str;
    }

    @DexIgnore
    public final void setRingStyleItems(ArrayList<RingStyleItem> arrayList) {
        this.ringStyleItems = arrayList;
    }

    @DexIgnore
    public final void setSerial(String str) {
        pq7.c(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public final void setWatchFaceType(int i) {
        this.watchFaceType = i;
    }

    @DexIgnore
    public String toString() {
        return "WatchFace(id=" + this.id + ", name=" + this.name + ", ringStyleItems=" + this.ringStyleItems + ", background=" + this.background + ", previewUrl=" + this.previewUrl + ", serial=" + this.serial + ", watchFaceType=" + this.watchFaceType + ")";
    }
}
