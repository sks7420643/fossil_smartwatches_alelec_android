package com.portfolio.platform.data.model.diana.workout;

import com.fossil.pq7;
import com.portfolio.platform.data.model.fitnessdata.PaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutPace {
    @DexIgnore
    public Float average;
    @DexIgnore
    public Float best;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutPace(PaceWrapper paceWrapper) {
        this(paceWrapper.getAverage(), paceWrapper.getBest());
        pq7.c(paceWrapper, "pace");
    }

    @DexIgnore
    public WorkoutPace(Float f, Float f2) {
        this.average = f;
        this.best = f2;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutPace copy$default(WorkoutPace workoutPace, Float f, Float f2, int i, Object obj) {
        if ((i & 1) != 0) {
            f = workoutPace.average;
        }
        if ((i & 2) != 0) {
            f2 = workoutPace.best;
        }
        return workoutPace.copy(f, f2);
    }

    @DexIgnore
    public final Float component1() {
        return this.average;
    }

    @DexIgnore
    public final Float component2() {
        return this.best;
    }

    @DexIgnore
    public final WorkoutPace copy(Float f, Float f2) {
        return new WorkoutPace(f, f2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutPace) {
                WorkoutPace workoutPace = (WorkoutPace) obj;
                if (!pq7.a(this.average, workoutPace.average) || !pq7.a(this.best, workoutPace.best)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Float getBest() {
        return this.best;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Float f = this.average;
        int hashCode = f != null ? f.hashCode() : 0;
        Float f2 = this.best;
        if (f2 != null) {
            i = f2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setAverage(Float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setBest(Float f) {
        this.best = f;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutPace(average=" + this.average + ", best=" + this.best + ")";
    }
}
