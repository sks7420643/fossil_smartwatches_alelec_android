package com.portfolio.platform.data.model.watchface;

import com.fossil.pq7;
import com.fossil.rj4;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetaData {
    @DexIgnore
    @rj4("checksum")
    public String checksum;
    @DexIgnore
    @rj4("colorId")
    public Integer colourId;
    @DexIgnore
    @rj4(Constants.GROUP_ID)
    public String groupId;
    @DexIgnore
    @rj4("name")
    public String name;

    @DexIgnore
    public MetaData(String str, Integer num, String str2, String str3) {
        this.checksum = str;
        this.colourId = num;
        this.groupId = str2;
        this.name = str3;
    }

    @DexIgnore
    public static /* synthetic */ MetaData copy$default(MetaData metaData, String str, Integer num, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metaData.checksum;
        }
        if ((i & 2) != 0) {
            num = metaData.colourId;
        }
        if ((i & 4) != 0) {
            str2 = metaData.groupId;
        }
        if ((i & 8) != 0) {
            str3 = metaData.name;
        }
        return metaData.copy(str, num, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.checksum;
    }

    @DexIgnore
    public final Integer component2() {
        return this.colourId;
    }

    @DexIgnore
    public final String component3() {
        return this.groupId;
    }

    @DexIgnore
    public final String component4() {
        return this.name;
    }

    @DexIgnore
    public final MetaData copy(String str, Integer num, String str2, String str3) {
        return new MetaData(str, num, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MetaData) {
                MetaData metaData = (MetaData) obj;
                if (!pq7.a(this.checksum, metaData.checksum) || !pq7.a(this.colourId, metaData.colourId) || !pq7.a(this.groupId, metaData.groupId) || !pq7.a(this.name, metaData.name)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final Integer getColourId() {
        return this.colourId;
    }

    @DexIgnore
    public final String getGroupId() {
        return this.groupId;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.checksum;
        int hashCode = str != null ? str.hashCode() : 0;
        Integer num = this.colourId;
        int hashCode2 = num != null ? num.hashCode() : 0;
        String str2 = this.groupId;
        int hashCode3 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setChecksum(String str) {
        this.checksum = str;
    }

    @DexIgnore
    public final void setColourId(Integer num) {
        this.colourId = num;
    }

    @DexIgnore
    public final void setGroupId(String str) {
        this.groupId = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        return "MetaData(checksum=" + this.checksum + ", colourId=" + this.colourId + ", groupId=" + this.groupId + ", name=" + this.name + ")";
    }
}
