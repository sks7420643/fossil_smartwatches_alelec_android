package com.portfolio.platform.data.model.ua;

import com.fossil.an7;
import com.fossil.bj4;
import com.fossil.gj4;
import com.fossil.il7;
import com.fossil.jj4;
import com.fossil.pq7;
import com.fossil.vt7;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public gj4 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            pq7.c(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UAActivityTimeSeries(com.portfolio.platform.data.model.ua.UAActivityTimeSeries.Builder r5) {
        /*
            r4 = this;
            java.lang.String r0 = "builder"
            com.fossil.pq7.c(r5, r0)
            java.lang.String r0 = r5.getMExternalId()
            if (r0 == 0) goto L_0x001b
            java.util.Map r1 = r5.getStepsMap()
            java.util.Map r2 = r5.getDistanceMap()
            java.util.Map r3 = r5.getCaloriesMap()
            r4.<init>(r0, r1, r2, r3)
            return
        L_0x001b:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.ua.UAActivityTimeSeries.<init>(com.portfolio.platform.data.model.ua.UAActivityTimeSeries$Builder):void");
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        pq7.c(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final gj4 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final gj4 getJsonData() {
        gj4 gj4 = new gj4();
        gj4.k("external_id", new jj4(this.mExternalId));
        gj4 gj42 = new gj4();
        if (this.stepsMap != null) {
            gj4 gj43 = new gj4();
            gj43.k("interval", new jj4((Number) 100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                gj43.k("values", (JsonElement) gson.k(vt7.o(vt7.o(an7.q((HashMap) map).toString(), "(", "[", false), ")", "]", false), bj4.class));
                gj42.k("steps", gj43);
            } else {
                throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            gj4 gj44 = new gj4();
            gj44.k("interval", new jj4((Number) 100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                gj44.k("values", (JsonElement) gson2.k(vt7.o(vt7.o(an7.q((HashMap) map2).toString(), "(", "[", false), ")", "]", false), bj4.class));
                gj42.k("distance", gj44);
            } else {
                throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            gj4 gj45 = new gj4();
            gj45.k("interval", new jj4((Number) 100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                gj45.k("values", (JsonElement) gson3.k(vt7.o(vt7.o(an7.q((HashMap) map3).toString(), "(", "[", false), ")", "]", false), bj4.class));
                gj42.k("energy_expended", gj45);
            } else {
                throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        gj4.k("time_series", gj42);
        if (this.extraJsonObject != null) {
            bj4 bj4 = new bj4();
            bj4.k(this.extraJsonObject);
            gj4 gj46 = new gj4();
            gj46.k("data_source", bj4);
            gj4.k("_links", gj46);
        }
        return gj4;
    }

    @DexIgnore
    public final void setExtraJsonObject(gj4 gj4) {
        this.extraJsonObject = gj4;
    }
}
