package com.portfolio.platform.data.model.diana.preset;

import com.fossil.kj5;
import com.fossil.pj4;
import com.fossil.pq7;
import com.fossil.qj4;
import com.fossil.rj4;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendPreset {
    @DexIgnore
    @pj4
    @qj4(DianaPresetComplicationSettingSerializer.class)
    @rj4("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @rj4("createdAt")
    public String createdAt;
    @DexIgnore
    @rj4("id")
    public String id;
    @DexIgnore
    @rj4("isDefault")
    public boolean isDefault;
    @DexIgnore
    @pj4
    @rj4("name")
    public String name;
    @DexIgnore
    @rj4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @rj4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @rj4("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @pj4
    @qj4(DianaPresetWatchAppSettingSerializer.class)
    @rj4("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore
    public DianaRecommendPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        pq7.c(str, "serialNumber");
        pq7.c(str2, "id");
        pq7.c(str3, "name");
        pq7.c(arrayList, "complications");
        pq7.c(arrayList2, "watchapps");
        pq7.c(str4, "watchFaceId");
        pq7.c(str5, "createdAt");
        pq7.c(str6, "updatedAt");
        this.serialNumber = str;
        this.id = str2;
        this.name = str3;
        this.isDefault = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
        this.createdAt = str5;
        this.updatedAt = str6;
    }

    @DexIgnore
    public static /* synthetic */ DianaRecommendPreset copy$default(DianaRecommendPreset dianaRecommendPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, String str5, String str6, int i, Object obj) {
        return dianaRecommendPreset.copy((i & 1) != 0 ? dianaRecommendPreset.serialNumber : str, (i & 2) != 0 ? dianaRecommendPreset.id : str2, (i & 4) != 0 ? dianaRecommendPreset.name : str3, (i & 8) != 0 ? dianaRecommendPreset.isDefault : z, (i & 16) != 0 ? dianaRecommendPreset.complications : arrayList, (i & 32) != 0 ? dianaRecommendPreset.watchapps : arrayList2, (i & 64) != 0 ? dianaRecommendPreset.watchFaceId : str4, (i & 128) != 0 ? dianaRecommendPreset.createdAt : str5, (i & 256) != 0 ? dianaRecommendPreset.updatedAt : str6);
    }

    @DexIgnore
    public final DianaRecommendPreset clone() {
        return new DianaRecommendPreset(this.serialNumber, this.id, this.name, this.isDefault, kj5.c(this.complications), kj5.d(this.watchapps), this.watchFaceId, this.createdAt, this.updatedAt);
    }

    @DexIgnore
    public final String component1() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isDefault;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final String component8() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DianaRecommendPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        pq7.c(str, "serialNumber");
        pq7.c(str2, "id");
        pq7.c(str3, "name");
        pq7.c(arrayList, "complications");
        pq7.c(arrayList2, "watchapps");
        pq7.c(str4, "watchFaceId");
        pq7.c(str5, "createdAt");
        pq7.c(str6, "updatedAt");
        return new DianaRecommendPreset(str, str2, str3, z, arrayList, arrayList2, str4, str5, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaRecommendPreset) {
                DianaRecommendPreset dianaRecommendPreset = (DianaRecommendPreset) obj;
                if (!pq7.a(this.serialNumber, dianaRecommendPreset.serialNumber) || !pq7.a(this.id, dianaRecommendPreset.id) || !pq7.a(this.name, dianaRecommendPreset.name) || this.isDefault != dianaRecommendPreset.isDefault || !pq7.a(this.complications, dianaRecommendPreset.complications) || !pq7.a(this.watchapps, dianaRecommendPreset.watchapps) || !pq7.a(this.watchFaceId, dianaRecommendPreset.watchFaceId) || !pq7.a(this.createdAt, dianaRecommendPreset.createdAt) || !pq7.a(this.updatedAt, dianaRecommendPreset.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.serialNumber;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.id;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = arrayList2 != null ? arrayList2.hashCode() : 0;
        String str4 = this.watchFaceId;
        int hashCode6 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.createdAt;
        int hashCode7 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.updatedAt;
        if (str6 != null) {
            i = str6.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        pq7.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        pq7.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        pq7.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        pq7.c(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaRecommendPreset(serialNumber=" + this.serialNumber + ", id=" + this.id + ", name=" + this.name + ", isDefault=" + this.isDefault + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
