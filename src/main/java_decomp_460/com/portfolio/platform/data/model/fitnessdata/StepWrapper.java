package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.pq7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StepWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public int total;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public StepWrapper(int i, List<Short> list, int i2) {
        pq7.c(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = i2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StepWrapper(com.fossil.fitness.Step r4) {
        /*
            r3 = this;
            java.lang.String r0 = "step"
            com.fossil.pq7.c(r4, r0)
            int r0 = r4.getResolutionInSecond()
            java.util.ArrayList r1 = r4.getValues()
            java.lang.String r2 = "step.values"
            com.fossil.pq7.b(r1, r2)
            int r2 = r4.getTotal()
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.StepWrapper.<init>(com.fossil.fitness.Step):void");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.fitnessdata.StepWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StepWrapper copy$default(StepWrapper stepWrapper, int i, List list, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = stepWrapper.resolutionInSecond;
        }
        if ((i3 & 2) != 0) {
            list = stepWrapper.values;
        }
        if ((i3 & 4) != 0) {
            i2 = stepWrapper.total;
        }
        return stepWrapper.copy(i, list, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component2() {
        return this.values;
    }

    @DexIgnore
    public final int component3() {
        return this.total;
    }

    @DexIgnore
    public final StepWrapper copy(int i, List<Short> list, int i2) {
        pq7.c(list, "values");
        return new StepWrapper(i, list, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof StepWrapper) {
                StepWrapper stepWrapper = (StepWrapper) obj;
                if (!(this.resolutionInSecond == stepWrapper.resolutionInSecond && pq7.a(this.values, stepWrapper.values) && this.total == stepWrapper.total)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond;
        List<Short> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + this.total;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        pq7.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "StepWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
