package com.portfolio.platform.data;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.co7;
import com.fossil.eo7;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.portfolio.platform.data.LocationSource", f = "LocationSource.kt", l = {71}, m = "getLocation")
public final class LocationSource$getLocation$Anon1 extends co7 {
    @DexIgnore
    public float F$0;
    @DexIgnore
    public float F$1;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocation$Anon1(LocationSource locationSource, qn7 qn7) {
        super(qn7);
        this.this$0 = locationSource;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.getLocation(null, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0, this);
    }
}
