package com.portfolio.platform.data;

import android.content.Context;
import android.location.Location;
import com.fossil.by7;
import com.fossil.dl7;
import com.fossil.dr7;
import com.fossil.ea3;
import com.fossil.el7;
import com.fossil.it3;
import com.fossil.iv7;
import com.fossil.jt3;
import com.fossil.ko7;
import com.fossil.ku7;
import com.fossil.nt3;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.google.android.gms.location.LocationRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ku7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ ea3 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ dr7 $location;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends ko7 implements vp7<iv7, qn7<? super Location>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1, qn7 qn7) {
            super(2, qn7);
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, qn7);
            anon1_Level2.p$ = (iv7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Location> qn7) {
            return ((Anon1_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = this.this$0;
                LocationSource locationSource = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.this$0;
                ea3 ea3 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.$fusedLocationClient;
                pq7.b(ea3, "fusedLocationClient");
                LocationRequest locationRequest = this.this$0.$locationRequest;
                this.L$0 = iv7;
                this.label = 1;
                Object requestLocationUpdates = locationSource.requestLocationUpdates(ea3, locationRequest, this);
                return requestLocationUpdates == d ? d : requestLocationUpdates;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2<TResult> implements jt3<Location> {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon2_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final void onSuccess(Location location) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation success " + location);
            ku7 ku7 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(location, LocationSource.ErrorState.SUCCESS, false, 4, null);
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(result));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 implements it3 {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon3_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            pq7.c(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation fail " + exc);
            ku7 ku7 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(null, LocationSource.ErrorState.UNKNOWN, false, 4, null);
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(result));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(dr7 dr7, ea3 ea3, LocationRequest locationRequest, ku7 ku7, qn7 qn7, LocationSource locationSource, Context context) {
        super(2, qn7);
        this.$location = dr7;
        this.$fusedLocationClient = ea3;
        this.$locationRequest = locationRequest;
        this.$continuation = ku7;
        this.this$0 = locationSource;
        this.$context$inlined = context;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$location, this.$fusedLocationClient, this.$locationRequest, this.$continuation, qn7, this.this$0, this.$context$inlined);
        locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (iv7) obj;
        return locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object c;
        dr7 dr7;
        Object d = yn7.d();
        int i = this.label;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.p$;
            dr7 dr72 = this.$location;
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, null);
            this.L$0 = iv7;
            this.L$1 = dr72;
            this.label = 1;
            c = by7.c(5000, anon1_Level2, this);
            if (c == d) {
                return d;
            }
            dr7 = dr72;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.L$0;
            el7.b(obj);
            c = obj;
            dr7 = (dr7) this.L$1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        dr7.element = (T) ((Location) c);
        if (this.$location.element == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager timeout, try to get lastLocation=" + ((Object) this.$location.element));
            ea3 ea3 = this.$fusedLocationClient;
            pq7.b(ea3, "fusedLocationClient");
            nt3<Location> s = ea3.s();
            s.f(new Anon2_Level2(this));
            s.d(new Anon3_Level2(this));
            pq7.b(s, "fusedLocationClient.last\u2026                        }");
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = LocationSource.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "getLocationFromFuseLocationManager success lastLocation=" + ((Object) this.$location.element));
            ku7 ku7 = this.$continuation;
            LocationSource.Result result = new LocationSource.Result(this.$location.element, LocationSource.ErrorState.SUCCESS, false, 4, null);
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(result));
        }
        return tl7.f3441a;
    }
}
