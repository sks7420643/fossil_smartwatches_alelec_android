package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.dl7;
import com.fossil.ea3;
import com.fossil.fa3;
import com.fossil.ku7;
import com.fossil.pq7;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends fa3 {
    @DexIgnore
    public /* final */ /* synthetic */ ku7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ea3 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(ku7 ku7, ea3 ea3, LocationRequest locationRequest) {
        this.$continuation = ku7;
        this.$this_requestLocationUpdates$inlined = ea3;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    @Override // com.fossil.fa3
    public void onLocationResult(LocationResult locationResult) {
        pq7.c(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        Location c = locationResult.c();
        if (c != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "requestLocationUpdates lastLocation=" + c);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "requestLocationUpdates lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.t(this);
        if (this.$continuation.isActive()) {
            ku7 ku7 = this.$continuation;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(c));
        }
    }
}
