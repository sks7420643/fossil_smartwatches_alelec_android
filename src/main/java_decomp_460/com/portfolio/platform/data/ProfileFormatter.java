package com.portfolio.platform.data;

import com.fossil.hr7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.um5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileFormatter implements NumberPickerLarge.f, RulerValuePicker.a, Serializable {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int UNIT_DEFAULT; // = -1;
    @DexIgnore
    public static /* final */ int UNIT_DOT; // = 2;
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_FEET_INCHES; // = 3;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public static /* final */ int UNIT_WEIGHT; // = 4;
    @DexIgnore
    public /* final */ int mUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public ProfileFormatter(int i) {
        this.mUnit = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.NumberPickerLarge.f, com.portfolio.platform.view.ruler.RulerValuePicker.a
    public String format(int i) {
        int i2 = this.mUnit;
        if (i2 == 0) {
            hr7 hr7 = hr7.f1520a;
            Locale locale = Locale.US;
            pq7.b(locale, "Locale.US");
            String c = um5.c(PortfolioApp.h0.c(), 2131887406);
            pq7.b(c, "LanguageHelper.getString\u2026ce, R.string.feet_format)");
            String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(locale, format, *args)");
            return format;
        } else if (i2 == 1) {
            hr7 hr72 = hr7.f1520a;
            Locale locale2 = Locale.US;
            pq7.b(locale2, "Locale.US");
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887431);
            pq7.b(c2, "LanguageHelper.getString\u2026, R.string.inches_format)");
            String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format2, "java.lang.String.format(locale, format, *args)");
            return format2;
        } else if (i2 == 2) {
            hr7 hr73 = hr7.f1520a;
            Locale locale3 = Locale.US;
            pq7.b(locale3, "Locale.US");
            String c3 = um5.c(PortfolioApp.h0.c(), 2131887394);
            pq7.b(c3, "LanguageHelper.getString\u2026nce, R.string.dot_format)");
            String format3 = String.format(locale3, c3, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format3, "java.lang.String.format(locale, format, *args)");
            return format3;
        } else if (i2 == 3) {
            hr7 hr74 = hr7.f1520a;
            Locale locale4 = Locale.US;
            pq7.b(locale4, "Locale.US");
            String c4 = um5.c(PortfolioApp.h0.c(), 2131887407);
            pq7.b(c4, "LanguageHelper.getString\u2026tring.feet_inches_format)");
            String format4 = String.format(locale4, c4, Arrays.copyOf(new Object[]{Integer.valueOf(i / 12), Integer.valueOf(i % 12)}, 2));
            pq7.b(format4, "java.lang.String.format(locale, format, *args)");
            return format4;
        } else if (i2 != 4) {
            hr7 hr75 = hr7.f1520a;
            Locale locale5 = Locale.US;
            pq7.b(locale5, "Locale.US");
            String c5 = um5.c(PortfolioApp.h0.c(), 2131887489);
            pq7.b(c5, "LanguageHelper.getString\u2026, R.string.normal_format)");
            String format5 = String.format(locale5, c5, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format5, "java.lang.String.format(locale, format, *args)");
            return format5;
        } else {
            hr7 hr76 = hr7.f1520a;
            Locale locale6 = Locale.US;
            pq7.b(locale6, "Locale.US");
            String c6 = um5.c(PortfolioApp.h0.c(), 2131887613);
            pq7.b(c6, "LanguageHelper.getString\u2026, R.string.weight_format)");
            String format6 = String.format(locale6, c6, Arrays.copyOf(new Object[]{Integer.valueOf(i / 10), Integer.valueOf(i % 10)}, 2));
            pq7.b(format6, "java.lang.String.format(locale, format, *args)");
            return format6;
        }
    }

    @DexIgnore
    public final int getMUnit() {
        return this.mUnit;
    }
}
