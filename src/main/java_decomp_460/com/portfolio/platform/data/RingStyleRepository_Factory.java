package com.portfolio.platform.data;

import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRepository_Factory implements Factory<RingStyleRepository> {
    @DexIgnore
    public /* final */ Provider<FileRepository> mFileRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<RingStyleRemoteDataSource> mRingStyleRemoveSourceProvider;

    @DexIgnore
    public RingStyleRepository_Factory(Provider<RingStyleRemoteDataSource> provider, Provider<FileRepository> provider2) {
        this.mRingStyleRemoveSourceProvider = provider;
        this.mFileRepositoryProvider = provider2;
    }

    @DexIgnore
    public static RingStyleRepository_Factory create(Provider<RingStyleRemoteDataSource> provider, Provider<FileRepository> provider2) {
        return new RingStyleRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static RingStyleRepository newInstance(RingStyleRemoteDataSource ringStyleRemoteDataSource, FileRepository fileRepository) {
        return new RingStyleRepository(ringStyleRemoteDataSource, fileRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public RingStyleRepository get() {
        return newInstance(this.mRingStyleRemoveSourceProvider.get(), this.mFileRepositoryProvider.get());
    }
}
