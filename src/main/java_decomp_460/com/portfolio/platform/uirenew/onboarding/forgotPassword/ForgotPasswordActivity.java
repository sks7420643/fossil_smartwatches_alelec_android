package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.fossil.aw6;
import com.fossil.cw6;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.uv5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForgotPasswordActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public cw6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, Constants.EMAIL);
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        uv5 uv5 = (uv5) getSupportFragmentManager().Y(2131362158);
        if (uv5 == null) {
            uv5 = uv5.k.b();
            k(uv5, uv5.k.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (uv5 != null) {
            M.r0(new aw6(uv5)).a(this);
            if (bundle == null) {
                cw6 cw6 = this.A;
                if (cw6 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    cw6.p(stringExtra);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                cw6 cw62 = this.A;
                if (cw62 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string == null) {
                        string = "";
                    }
                    cw62.p(string);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            cw6 cw6 = this.A;
            if (cw6 != null) {
                cw6.s("EMAIL", bundle);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle, persistableBundle);
    }
}
