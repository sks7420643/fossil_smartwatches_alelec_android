package com.portfolio.platform.uirenew.home.profile.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.ju6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            FLogger.INSTANCE.getLocal().d("WorkoutSettingActivity", "start()");
            context.startActivity(new Intent(context, WorkoutSettingActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((ju6) getSupportFragmentManager().Y(2131362158)) == null) {
            i(ju6.m.a(), 2131362158);
        }
    }
}
