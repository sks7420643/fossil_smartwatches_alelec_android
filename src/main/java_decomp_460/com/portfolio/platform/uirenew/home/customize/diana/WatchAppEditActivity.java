package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.h76;
import com.fossil.k76;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.oo5;
import com.fossil.po4;
import com.fossil.pq7;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppEditActivity extends ls5 {
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public h76 A;
    @DexIgnore
    public po4 B;
    @DexIgnore
    public k76 C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            pq7.c(context, "context");
            pq7.c(str, "presetId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "start - presetId=" + str + " watchAppPos=" + str2);
            Intent intent = new Intent(context, WatchAppEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends oo5>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<List<? extends oo5>> {
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v10, resolved type: com.fossil.k76 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0142  */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity.onCreate(android.os.Bundle):void");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        h76 h76 = this.A;
        if (h76 != null) {
            h76.C(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }
}
