package com.portfolio.platform.uirenew.home.details.workout;

import android.content.Context;
import android.content.Intent;
import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.il7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.ux7;
import com.fossil.v37;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetailActivity extends FlutterActivity {
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public WorkoutSessionRepository b;
    @DexIgnore
    public MethodChannel c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public /* final */ iv7 e; // = jv7.a(bw7.c().plus(ux7.b(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            pq7.c(context, "context");
            pq7.c(str, "workoutSessionId");
            pq7.c(str2, "unitType");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutDetailActivity", "start with workout id " + str + " unitType " + str2);
            Intent intent = new Intent(context, WorkoutDetailActivity.class);
            intent.putExtra("EXTRA_WORKOUT_ID", str);
            intent.putExtra("EXTRA_UNIT_TYPE", str2);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1", f = "WorkoutDetailActivity.kt", l = {94, 98}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutDetailActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {
            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutDetailActivity", "Unable to load data: " + str2);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "Unable to load data: notImplemented");
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "success to load data");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b")
        /* renamed from: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b  reason: collision with other inner class name */
        public static final class C0355b extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $unitType$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession $workoutSession$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0355b(qn7 qn7, b bVar, WorkoutSession workoutSession, String str) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$workoutSession$inlined = workoutSession;
                this.$unitType$inlined = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0355b bVar = new C0355b(qn7, this.this$0, this.$workoutSession$inlined, this.$unitType$inlined);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                return ((C0355b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    WorkoutDetailActivity workoutDetailActivity = this.this$0.this$0;
                    WorkoutSession workoutSession = this.$workoutSession$inlined;
                    String str = this.$unitType$inlined;
                    pq7.b(str, "unitType");
                    if (str != null) {
                        String lowerCase = str.toLowerCase();
                        pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                        return workoutDetailActivity.f(workoutSession, lowerCase);
                    }
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements MethodChannel.MethodCallHandler {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public c(String str, b bVar, WorkoutSession workoutSession, String str2) {
                this.b = bVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
            public final void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                pq7.c(methodCall, "call");
                pq7.c(result, "<anonymous parameter 1>");
                if (pq7.a(methodCall.method, "dismissMap")) {
                    this.b.this$0.d = true;
                    FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "MethodCallHandler dismiss");
                    this.b.this$0.finish();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1$workoutSession$1", f = "WorkoutDetailActivity.kt", l = {94}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super WorkoutSession>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $workoutId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$workoutId = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, this.$workoutId, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super WorkoutSession> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WorkoutSessionRepository d2 = this.this$0.this$0.d();
                    String str = this.$workoutId;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object workoutSessionById = d2.getWorkoutSessionById(str, this);
                    return workoutSessionById == d ? d : workoutSessionById;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutDetailActivity workoutDetailActivity, qn7 qn7) {
            super(2, qn7);
            this.this$0 = workoutDetailActivity;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0096  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 296
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public WorkoutDetailActivity() {
        PortfolioApp.h0.c().M().V1(this);
    }

    @DexIgnore
    public static final /* synthetic */ MethodChannel a(WorkoutDetailActivity workoutDetailActivity) {
        MethodChannel methodChannel = workoutDetailActivity.c;
        if (methodChannel != null) {
            return methodChannel;
        }
        pq7.n("mChannel");
        throw null;
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineConfigurator, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        pq7.c(flutterEngine, "flutterEngine");
        DartExecutor dartExecutor = flutterEngine.getDartExecutor();
        pq7.b(dartExecutor, "flutterEngine.dartExecutor");
        this.c = new MethodChannel(dartExecutor.getBinaryMessenger(), "detailTracking/map", JSONMethodCodec.INSTANCE);
    }

    @DexIgnore
    public final WorkoutSessionRepository d() {
        WorkoutSessionRepository workoutSessionRepository = this.b;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        pq7.n("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final xw7 e() {
        return gu7.d(this.e, null, null, new b(this, null), 3, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r4 != null) goto L_0x001f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String f(com.portfolio.platform.data.model.diana.workout.WorkoutSession r23, java.lang.String r24) {
        /*
        // Method dump skipped, instructions count: 331
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.f(com.portfolio.platform.data.model.diana.workout.WorkoutSession, java.lang.String):java.lang.String");
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void onFlutterUiDisplayed() {
        super.onFlutterUiDisplayed();
        if (this.d) {
            this.d = false;
            e();
        }
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineProvider, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public FlutterEngine provideFlutterEngine(Context context) {
        pq7.c(context, "context");
        return v37.c.a();
    }
}
