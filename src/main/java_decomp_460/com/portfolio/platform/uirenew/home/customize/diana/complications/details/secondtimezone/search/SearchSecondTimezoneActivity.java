package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.o76;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.v86;
import com.fossil.x86;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezoneActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public x86 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "fragment");
            pq7.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), SearchSecondTimezoneActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(r(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        o76 o76 = (o76) getSupportFragmentManager().Y(2131362158);
        if (o76 == null) {
            o76 = o76.l.a();
            k(o76, o76.l.b(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (o76 != null) {
            M.J(new v86(o76)).a(this);
            Intent intent = getIntent();
            if (intent == null || (str = intent.getStringExtra(Constants.USER_SETTING)) == null) {
                str = "";
            }
            x86 x86 = this.A;
            if (x86 != null) {
                x86.q(str);
            } else {
                pq7.n("mSearchSecondTimezonePresenter");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneContract.View");
        }
    }
}
