package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.i46;
import com.fossil.il7;
import com.fossil.j46;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.m46;
import com.fossil.pq7;
import com.fossil.ro4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsAndAppsAssignedActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public m46 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsAndAppsAssignedActivity.class);
            intent.putExtra("EXTRA_HAND_NUMBER", i);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        i46 i46 = (i46) getSupportFragmentManager().Y(2131362158);
        if (i46 == null) {
            i46 = i46.s.b();
            k(i46, i46.s.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (i46 != null) {
            int intExtra = getIntent().getIntExtra("EXTRA_HAND_NUMBER", 0);
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            pq7.b(supportLoaderManager, "supportLoaderManager");
            M.m(new j46(i46, intExtra, supportLoaderManager)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedContract.View");
    }
}
