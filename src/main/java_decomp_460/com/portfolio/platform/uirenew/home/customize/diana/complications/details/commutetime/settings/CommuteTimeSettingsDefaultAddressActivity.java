package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.m76;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.x76;
import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public z76 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle) {
            pq7.c(fragment, "fragment");
            pq7.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 111);
        }

        @DexIgnore
        public final void b(Fragment fragment, Bundle bundle) {
            pq7.c(fragment, "fragment");
            pq7.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 112);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        m76 m76 = (m76) getSupportFragmentManager().Y(2131362158);
        if (m76 == null) {
            m76 = m76.s.a();
            k(m76, m76.s.b(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (m76 != null) {
            M.m0(new x76(m76)).a(this);
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS");
            if (bundleExtra != null) {
                z76 z76 = this.A;
                if (z76 != null) {
                    String string = bundleExtra.getString("AddressType");
                    if (string == null) {
                        string = "Home";
                    }
                    String string2 = bundleExtra.getString("KEY_DEFAULT_PLACE");
                    if (string2 == null) {
                        string2 = "";
                    }
                    z76.A(string, string2);
                    return;
                }
                pq7.n("mCommuteTimeSettingsDefaultAddressPresenter");
                throw null;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressContract.View");
    }
}
