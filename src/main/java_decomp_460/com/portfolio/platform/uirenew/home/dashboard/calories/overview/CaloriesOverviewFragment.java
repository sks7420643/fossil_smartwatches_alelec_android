package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.aq0;
import com.fossil.g37;
import com.fossil.g67;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.mg6;
import com.fossil.mo0;
import com.fossil.mv0;
import com.fossil.nk5;
import com.fossil.pq7;
import com.fossil.pv5;
import com.fossil.qn5;
import com.fossil.ro4;
import com.fossil.sg6;
import com.fossil.tg6;
import com.fossil.x25;
import com.fossil.yg6;
import com.fossil.zg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewFragment extends pv5 {
    @DexIgnore
    public g37<x25> g;
    @DexIgnore
    public ig6 h;
    @DexIgnore
    public zg6 i;
    @DexIgnore
    public tg6 j;
    @DexIgnore
    public hg6 k;
    @DexIgnore
    public yg6 l;
    @DexIgnore
    public sg6 m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment b;

        @DexIgnore
        public a(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.b = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.b;
            g37 g37 = caloriesOverviewFragment.g;
            caloriesOverviewFragment.O6(7, g37 != null ? (x25) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment b;

        @DexIgnore
        public b(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.b = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.b;
            g37 g37 = caloriesOverviewFragment.g;
            caloriesOverviewFragment.O6(4, g37 != null ? (x25) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment b;

        @DexIgnore
        public c(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.b = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.b;
            g37 g37 = caloriesOverviewFragment.g;
            caloriesOverviewFragment.O6(2, g37 != null ? (x25) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "CaloriesOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(x25 x25) {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "initUI");
        this.k = (hg6) getChildFragmentManager().Z("CaloriesOverviewDayFragment");
        this.l = (yg6) getChildFragmentManager().Z("CaloriesOverviewWeekFragment");
        this.m = (sg6) getChildFragmentManager().Z("CaloriesOverviewMonthFragment");
        if (this.k == null) {
            this.k = new hg6();
        }
        if (this.l == null) {
            this.l = new yg6();
        }
        if (this.m == null) {
            this.m = new sg6();
        }
        ArrayList arrayList = new ArrayList();
        hg6 hg6 = this.k;
        if (hg6 != null) {
            arrayList.add(hg6);
            yg6 yg6 = this.l;
            if (yg6 != null) {
                arrayList.add(yg6);
                sg6 sg6 = this.m;
                if (sg6 != null) {
                    arrayList.add(sg6);
                    RecyclerView recyclerView = x25.w;
                    pq7.b(recyclerView, "it");
                    recyclerView.setAdapter(new g67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new CaloriesOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mv0().b(recyclerView);
                    O6(this.s, x25);
                    ro4 M = PortfolioApp.h0.c().M();
                    hg6 hg62 = this.k;
                    if (hg62 != null) {
                        yg6 yg62 = this.l;
                        if (yg62 != null) {
                            sg6 sg62 = this.m;
                            if (sg62 != null) {
                                M.s1(new mg6(hg62, yg62, sg62)).a(this);
                                x25.u.setOnClickListener(new a(this));
                                x25.r.setOnClickListener(new b(this));
                                x25.s.setOnClickListener(new c(this));
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        g37<x25> g37;
        x25 a2;
        g37<x25> g372;
        x25 a3;
        ConstraintLayout constraintLayout;
        g37<x25> g373;
        x25 a4;
        FlexibleTextView flexibleTextView;
        g37<x25> g374;
        x25 a5;
        nk5.a aVar = nk5.o;
        ig6 ig6 = this.h;
        if (ig6 != null) {
            String d = aVar.w(ig6.n()) ? qn5.l.a().d("dianaActiveCaloriesTab") : qn5.l.a().d("hybridActiveCaloriesTab");
            nk5.a aVar2 = nk5.o;
            ig6 ig62 = this.h;
            if (ig62 != null) {
                String d2 = aVar2.w(ig62.n()) ? qn5.l.a().d("onDianaActiveCaloriesTab") : qn5.l.a().d("onHybridActiveCaloriesTab");
                if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(d));
                    a5.y.setBackgroundColor(Color.parseColor(d));
                }
                if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(d2));
                }
                nk5.a aVar3 = nk5.o;
                ig6 ig63 = this.h;
                if (ig63 != null) {
                    this.t = aVar3.w(ig63.n()) ? qn5.l.a().d("onDianaInactiveTab") : qn5.l.a().d("onHybridInactiveTab");
                    this.u = qn5.l.a().d("primaryText");
                    String d3 = qn5.l.a().d("nonBrandSurface");
                    if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                    if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        pq7.b(flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        pq7.b(flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        }
                    }
                } else {
                    pq7.n("mCaloriesOverviewDayPresenter");
                    throw null;
                }
            } else {
                pq7.n("mCaloriesOverviewDayPresenter");
                throw null;
            }
        } else {
            pq7.n("mCaloriesOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2, x25 x25) {
        x25 a2;
        RecyclerView recyclerView;
        g37<x25> g37;
        x25 a3;
        x25 a4;
        RecyclerView recyclerView2;
        x25 a5;
        RecyclerView recyclerView3;
        x25 a6;
        RecyclerView recyclerView4;
        if (x25 != null) {
            FlexibleTextView flexibleTextView = x25.u;
            pq7.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = x25.r;
            pq7.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = x25.s;
            pq7.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = x25.u;
            pq7.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = x25.r;
            pq7.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = x25.s;
            pq7.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = x25.s;
                pq7.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = x25.s;
                pq7.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = x25.r;
                pq7.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                g37<x25> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = x25.r;
                pq7.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = x25.r;
                pq7.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = x25.r;
                pq7.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                g37<x25> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = x25.u;
                pq7.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = x25.u;
                pq7.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = x25.r;
                pq7.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                g37<x25> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = x25.u;
                pq7.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = x25.u;
                pq7.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = x25.r;
                pq7.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                g37<x25> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                pq7.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                pq7.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                pq7.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        x25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onCreateView");
        x25 x25 = (x25) aq0.f(layoutInflater, 2131558508, viewGroup, false, A6());
        mo0.y0(x25.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        pq7.b(x25, "binding");
        M6(x25);
        this.g = new g37<>(this, x25);
        N6();
        g37<x25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
