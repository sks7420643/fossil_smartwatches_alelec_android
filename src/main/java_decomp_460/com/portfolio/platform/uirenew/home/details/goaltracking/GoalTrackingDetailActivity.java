package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.am6;
import com.fossil.bm6;
import com.fossil.dm6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.portfolio.platform.PortfolioApp;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailActivity extends ls5 {
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public dm6 A;
    @DexIgnore
    public Date B; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            pq7.c(date, "date");
            pq7.c(context, "context");
            Intent intent = new Intent(context, GoalTrackingDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        am6 am6 = (am6) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        if (intent != null) {
            this.B = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (am6 == null) {
            am6 = am6.A.a(this.B);
            i(am6, 2131362158);
        }
        PortfolioApp.h0.c().M().c(new bm6(am6)).a(this);
    }
}
