package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.content.Intent;
import android.os.Bundle;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.e96;
import com.fossil.f96;
import com.fossil.h96;
import com.fossil.il7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchActivity extends ls5 {
    @DexIgnore
    public h96 A;

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        e96 e96 = (e96) getSupportFragmentManager().Y(2131362158);
        if (e96 == null) {
            e96 = e96.l.b();
            k(e96, e96.l.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (e96 != null) {
            M.P(new f96(e96)).a(this);
            Intent intent = getIntent();
            pq7.b(intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                h96 h96 = this.A;
                if (h96 != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("bottom");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("right");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    String string4 = extras.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    h96.D(string, string2, string3, string4);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                h96 h962 = this.A;
                if (h962 != null) {
                    String string5 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    String string7 = bundle.getString("right");
                    if (string7 == null) {
                        string7 = "empty";
                    }
                    String string8 = bundle.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string8 == null) {
                        string8 = "empty";
                    }
                    h962.D(string5, string6, string7, string8);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        h96 h96 = this.A;
        if (h96 != null) {
            h96.A(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }
}
