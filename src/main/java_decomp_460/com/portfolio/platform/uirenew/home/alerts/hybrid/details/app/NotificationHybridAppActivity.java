package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.e56;
import com.fossil.f56;
import com.fossil.i56;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridAppActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public i56 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<String> arrayList) {
            pq7.c(fragment, "fragment");
            pq7.c(arrayList, "stringAppsSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridAppActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putStringArrayListExtra("LIST_APPS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 4567);
        }
    }

    /*
    static {
        pq7.b(NotificationHybridAppActivity.class.getSimpleName(), "NotificationHybridAppAct\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        e56 e56 = (e56) getSupportFragmentManager().Y(2131362158);
        if (e56 == null) {
            e56 = e56.k.b();
            k(e56, e56.k.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (e56 != null) {
            M.j(new f56(e56, getIntent().getIntExtra("HAND_NUMBER", 0), getIntent().getStringArrayListExtra("LIST_APPS_SELECTED"))).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppContract.View");
    }
}
