package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.o06;
import com.fossil.p06;
import com.fossil.pq7;
import com.fossil.r06;
import com.fossil.ro4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public r06 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            pq7.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationAppsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        o06 o06 = (o06) getSupportFragmentManager().Y(2131362158);
        if (o06 == null) {
            o06 = o06.s.b();
            k(o06, o06.s.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (o06 != null) {
            M.y0(new p06(o06)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsContract.View");
    }
}
