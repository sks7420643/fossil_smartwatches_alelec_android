package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.ez5;
import com.fossil.hz5;
import com.fossil.jz5;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public jz5 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Context context, Integer num, int i, Object obj) {
            if ((i & 2) != 0) {
                num = 0;
            }
            aVar.a(context, num);
        }

        @DexIgnore
        public final void a(Context context, Integer num) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", num);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void c(Context context, String str, boolean z) {
            pq7.c(context, "context");
            pq7.c(str, "presetId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeActivity", "startCustomizeWithPresetId id " + str + " isSharingFlow " + z);
            Intent intent = new Intent(context, HomeActivity.class);
            if (z) {
                intent.addFlags(268468224);
            } else {
                intent.addFlags(67108864);
            }
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", 2);
            intent.putExtra("OUT_STATE_PRESET_ID", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void d(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", 0);
            intent.putExtra("KEY_DIANA_REQUIRE", true);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        jz5 jz5 = this.A;
        if (jz5 != null) {
            jz5.P(i, i2, intent);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        Bundle extras;
        super.onCreate(bundle);
        setContentView(2131558439);
        ez5 ez5 = (ez5) getSupportFragmentManager().Y(2131362158);
        if (ez5 == null) {
            ez5 = (!getIntent().hasExtra("KEY_DIANA_REQUIRE") || !getIntent().getBooleanExtra("KEY_DIANA_REQUIRE", false)) ? ez5.a.b(ez5.A, null, 1, null) : ez5.A.c();
            i(ez5, 2131362158);
        }
        PortfolioApp.h0.c().M().z1(new hz5(ez5)).a(this);
        if (bundle != null) {
            jz5 jz5 = this.A;
            if (jz5 != null) {
                jz5.t(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            Intent intent = getIntent();
            if (intent != null && (extras = intent.getExtras()) != null) {
                if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                    jz5 jz52 = this.A;
                    if (jz52 != null) {
                        jz52.t(extras.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                }
                if (extras.containsKey("OUT_STATE_PRESET_ID")) {
                    String string = extras.getString("OUT_STATE_PRESET_ID");
                    if (string == null) {
                        string = "";
                    }
                    pq7.b(string, "it.getString(OUT_STATE_PRESET_ID) ?: \"\"");
                    ez5.V6(string);
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String r = r();
        local.d(r, "on new intent " + intent);
        if (intent != null && intent.hasExtra("OUT_STATE_PRESET_ID")) {
            String stringExtra = intent.getStringExtra("OUT_STATE_PRESET_ID");
            String str = stringExtra != null ? stringExtra : "";
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "on new intent receive preset id " + str);
            ez5 ez5 = (ez5) getSupportFragmentManager().Y(2131362158);
            if (ez5 != null) {
                ez5.V6(str);
            }
        }
        super.onNewIntent(intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        jz5 jz5 = this.A;
        if (jz5 != null) {
            bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", jz5.p());
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onStart() {
        super.onStart();
        m(MFDeviceService.class, ButtonService.class);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onStop() {
        super.onStop();
        J(MFDeviceService.class, ButtonService.class);
    }
}
