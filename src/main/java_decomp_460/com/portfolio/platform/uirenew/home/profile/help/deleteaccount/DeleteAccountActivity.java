package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.bq6;
import com.fossil.kp6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.zp6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAccountActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public bq6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, DeleteAccountActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        kp6 kp6 = (kp6) getSupportFragmentManager().Y(2131362158);
        if (kp6 == null) {
            kp6 = kp6.m.a();
            i(kp6, 2131362158);
        }
        PortfolioApp.h0.c().M().G1(new zp6(kp6)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
