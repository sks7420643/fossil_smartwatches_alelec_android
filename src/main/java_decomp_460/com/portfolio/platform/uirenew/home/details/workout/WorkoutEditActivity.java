package com.portfolio.platform.uirenew.home.details.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.fn6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutEditActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, "workoutSessionId");
            Intent intent = new Intent(context, WorkoutEditActivity.class);
            intent.setFlags(536870912);
            intent.putExtra("EXTRA_WORKOUT_SESSION_ID", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((fn6) getSupportFragmentManager().Y(2131362158)) == null) {
            fn6.a aVar = fn6.s;
            String stringExtra = getIntent().getStringExtra("EXTRA_WORKOUT_SESSION_ID");
            pq7.b(stringExtra, "intent.getStringExtra(EXTRA_WORKOUT_SESSION_ID)");
            i(aVar.a(stringExtra), 2131362158);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
