package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.fossil.il7;
import com.fossil.ls5;
import com.fossil.m96;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoActivity extends ls5 {
    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((m96) getSupportFragmentManager().Y(2131362158)) == null && (intent = getIntent()) != null) {
            Uri uri = null;
            if (intent.hasExtra("IMAGE_URI_EXTRA")) {
                uri = (Uri) intent.getParcelableExtra("IMAGE_URI_EXTRA");
            }
            if (uri != null) {
                m96 a2 = m96.u.a(uri);
                if (a2 != null) {
                    k(a2, "EditPhotoFragment", 2131362158);
                    return;
                }
                throw new il7("null cannot be cast to non-null type androidx.fragment.app.Fragment");
            }
            FLogger.INSTANCE.getLocal().d(r(), "Can not start EditPhotoFragment with invalid params");
        }
    }
}
