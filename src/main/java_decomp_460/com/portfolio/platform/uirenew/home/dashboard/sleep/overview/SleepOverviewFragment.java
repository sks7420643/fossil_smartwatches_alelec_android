package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.aq0;
import com.fossil.ek6;
import com.fossil.fk6;
import com.fossil.g37;
import com.fossil.g67;
import com.fossil.hb5;
import com.fossil.kk6;
import com.fossil.lk6;
import com.fossil.mo0;
import com.fossil.mv0;
import com.fossil.nk5;
import com.fossil.pq7;
import com.fossil.pv5;
import com.fossil.qn5;
import com.fossil.ro4;
import com.fossil.tj6;
import com.fossil.uj6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewFragment extends pv5 {
    @DexIgnore
    public g37<hb5> g;
    @DexIgnore
    public uj6 h;
    @DexIgnore
    public lk6 i;
    @DexIgnore
    public fk6 j;
    @DexIgnore
    public tj6 k;
    @DexIgnore
    public kk6 l;
    @DexIgnore
    public ek6 m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment b;

        @DexIgnore
        public a(SleepOverviewFragment sleepOverviewFragment) {
            this.b = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.b;
            g37 g37 = sleepOverviewFragment.g;
            sleepOverviewFragment.O6(7, g37 != null ? (hb5) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment b;

        @DexIgnore
        public b(SleepOverviewFragment sleepOverviewFragment) {
            this.b = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.b;
            g37 g37 = sleepOverviewFragment.g;
            sleepOverviewFragment.O6(4, g37 != null ? (hb5) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment b;

        @DexIgnore
        public c(SleepOverviewFragment sleepOverviewFragment) {
            this.b = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.b;
            g37 g37 = sleepOverviewFragment.g;
            sleepOverviewFragment.O6(2, g37 != null ? (hb5) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "SleepOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(hb5 hb5) {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "initUI");
        this.k = (tj6) getChildFragmentManager().Z("SleepOverviewDayFragment");
        this.l = (kk6) getChildFragmentManager().Z("SleepOverviewWeekFragment");
        this.m = (ek6) getChildFragmentManager().Z("SleepOverviewMonthFragment");
        if (this.k == null) {
            this.k = new tj6();
        }
        if (this.l == null) {
            this.l = new kk6();
        }
        if (this.m == null) {
            this.m = new ek6();
        }
        ArrayList arrayList = new ArrayList();
        tj6 tj6 = this.k;
        if (tj6 != null) {
            arrayList.add(tj6);
            kk6 kk6 = this.l;
            if (kk6 != null) {
                arrayList.add(kk6);
                ek6 ek6 = this.m;
                if (ek6 != null) {
                    arrayList.add(ek6);
                    RecyclerView recyclerView = hb5.w;
                    pq7.b(recyclerView, "it");
                    recyclerView.setAdapter(new g67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new SleepOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mv0().b(recyclerView);
                    O6(this.s, hb5);
                    ro4 M = PortfolioApp.h0.c().M();
                    tj6 tj62 = this.k;
                    if (tj62 != null) {
                        kk6 kk62 = this.l;
                        if (kk62 != null) {
                            ek6 ek62 = this.m;
                            if (ek62 != null) {
                                M.X1(new yj6(tj62, kk62, ek62)).a(this);
                                hb5.u.setOnClickListener(new a(this));
                                hb5.r.setOnClickListener(new b(this));
                                hb5.s.setOnClickListener(new c(this));
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        g37<hb5> g37;
        hb5 a2;
        g37<hb5> g372;
        hb5 a3;
        ConstraintLayout constraintLayout;
        g37<hb5> g373;
        hb5 a4;
        FlexibleTextView flexibleTextView;
        g37<hb5> g374;
        hb5 a5;
        nk5.a aVar = nk5.o;
        uj6 uj6 = this.h;
        if (uj6 != null) {
            String d = aVar.w(uj6.z()) ? qn5.l.a().d("dianaSleepTab") : qn5.l.a().d("hybridSleepTab");
            nk5.a aVar2 = nk5.o;
            uj6 uj62 = this.h;
            if (uj62 != null) {
                String d2 = aVar2.w(uj62.z()) ? qn5.l.a().d("onDianaSleepTab") : qn5.l.a().d("onHybridSleepTab");
                if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(d));
                    a5.y.setBackgroundColor(Color.parseColor(d));
                }
                if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(d2));
                }
                nk5.a aVar3 = nk5.o;
                uj6 uj63 = this.h;
                if (uj63 != null) {
                    this.t = aVar3.w(uj63.z()) ? qn5.l.a().d("onDianaInactiveTab") : qn5.l.a().d("onHybridInactiveTab");
                    String d3 = qn5.l.a().d("nonBrandSurface");
                    this.u = qn5.l.a().d("primaryText");
                    if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                    if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        pq7.b(flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        pq7.b(flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        }
                    }
                } else {
                    pq7.n("mSleepOverviewDayPresenter");
                    throw null;
                }
            } else {
                pq7.n("mSleepOverviewDayPresenter");
                throw null;
            }
        } else {
            pq7.n("mSleepOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2, hb5 hb5) {
        hb5 a2;
        RecyclerView recyclerView;
        g37<hb5> g37;
        hb5 a3;
        hb5 a4;
        RecyclerView recyclerView2;
        hb5 a5;
        RecyclerView recyclerView3;
        hb5 a6;
        RecyclerView recyclerView4;
        if (hb5 != null) {
            FlexibleTextView flexibleTextView = hb5.u;
            pq7.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = hb5.r;
            pq7.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = hb5.s;
            pq7.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = hb5.u;
            pq7.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = hb5.r;
            pq7.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = hb5.s;
            pq7.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = hb5.s;
                pq7.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = hb5.s;
                pq7.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = hb5.r;
                pq7.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                g37<hb5> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = hb5.r;
                pq7.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = hb5.r;
                pq7.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = hb5.r;
                pq7.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                g37<hb5> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = hb5.u;
                pq7.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = hb5.u;
                pq7.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = hb5.r;
                pq7.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                g37<hb5> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = hb5.u;
                pq7.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = hb5.u;
                pq7.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = hb5.r;
                pq7.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                g37<hb5> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                pq7.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                pq7.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                pq7.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        hb5 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onCreateView");
        hb5 hb5 = (hb5) aq0.f(layoutInflater, 2131558624, viewGroup, false, A6());
        mo0.y0(hb5.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        pq7.b(hb5, "binding");
        M6(hb5);
        this.g = new g37<>(this, hb5);
        N6();
        g37<hb5> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
