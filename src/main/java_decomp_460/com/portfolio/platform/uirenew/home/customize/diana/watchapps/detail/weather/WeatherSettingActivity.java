package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ia6;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.ta6;
import com.fossil.va6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public va6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "fragment");
            pq7.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), WeatherSettingActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            fragment.startActivityForResult(intent, 105);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(r(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        ia6 ia6 = (ia6) getSupportFragmentManager().Y(2131362158);
        if (ia6 == null) {
            ia6 = ia6.m.a();
            k(ia6, ia6.m.b(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (ia6 != null) {
            M.X(new ta6(ia6)).a(this);
            Intent intent = getIntent();
            if (intent == null || (str = intent.getStringExtra(Constants.USER_SETTING)) == null) {
                str = "";
            }
            va6 va6 = this.A;
            if (va6 != null) {
                va6.w(str);
            } else {
                pq7.n("mWeatherSettingPresenter");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingContract.View");
        }
    }
}
