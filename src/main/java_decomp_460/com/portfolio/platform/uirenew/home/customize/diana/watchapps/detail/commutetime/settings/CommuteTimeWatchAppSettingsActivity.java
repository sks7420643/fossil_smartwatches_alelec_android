package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ga6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.n76;
import com.fossil.pq7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSettingsActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "fragment");
            pq7.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeWatchAppSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((ga6) getSupportFragmentManager().Y(2131362158)) == null) {
            if (getIntent() == null || (str = getIntent().getStringExtra(Constants.USER_SETTING)) == null) {
                str = "";
            }
            k(ga6.k.a(str), n76.s.b(), 2131362158);
        }
    }
}
