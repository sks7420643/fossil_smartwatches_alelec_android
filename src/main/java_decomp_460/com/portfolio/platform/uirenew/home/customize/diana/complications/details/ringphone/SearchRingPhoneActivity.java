package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.l86;
import com.fossil.ls5;
import com.fossil.m86;
import com.fossil.o86;
import com.fossil.pq7;
import com.fossil.ro4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhoneActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public o86 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            pq7.c(fragment, "context");
            pq7.c(str, "selectedRingtone");
            Intent intent = new Intent(fragment.getContext(), SearchRingPhoneActivity.class);
            intent.putExtra("KEY_SELECTED_RINGPHONE", str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 104);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        Ringtone ringtone;
        super.onCreate(bundle);
        setContentView(2131558439);
        l86 l86 = (l86) getSupportFragmentManager().Y(2131362158);
        if (l86 == null) {
            l86 = l86.l.b();
            k(l86, l86.l.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (l86 != null) {
            M.k0(new m86(l86)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                o86 o86 = this.A;
                if (o86 != null) {
                    String stringExtra = intent.getStringExtra("KEY_SELECTED_RINGPHONE");
                    pq7.b(stringExtra, "it.getStringExtra(KEY_SELECTED_RINGPHONE)");
                    o86.w(stringExtra);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null && (ringtone = (Ringtone) bundle.getParcelable("KEY_SELECTED_RINGPHONE")) != null) {
                o86 o862 = this.A;
                if (o862 != null) {
                    o862.v(ringtone);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        o86 o86 = this.A;
        if (o86 != null) {
            bundle.putParcelable("KEY_SELECTED_RINGPHONE", o86.n());
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }
}
