package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.aj6;
import com.fossil.aq0;
import com.fossil.b75;
import com.fossil.bj6;
import com.fossil.g37;
import com.fossil.g67;
import com.fossil.gj6;
import com.fossil.hj6;
import com.fossil.mo0;
import com.fossil.mv0;
import com.fossil.pi6;
import com.fossil.pq7;
import com.fossil.pv5;
import com.fossil.qi6;
import com.fossil.qn5;
import com.fossil.ro4;
import com.fossil.ui6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewFragment extends pv5 {
    @DexIgnore
    public g37<b75> g;
    @DexIgnore
    public qi6 h;
    @DexIgnore
    public hj6 i;
    @DexIgnore
    public bj6 j;
    @DexIgnore
    public pi6 k;
    @DexIgnore
    public gj6 l;
    @DexIgnore
    public aj6 m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment b;

        @DexIgnore
        public a(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.b = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.b;
            g37 g37 = heartRateOverviewFragment.g;
            heartRateOverviewFragment.O6(7, g37 != null ? (b75) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment b;

        @DexIgnore
        public b(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.b = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.b;
            g37 g37 = heartRateOverviewFragment.g;
            heartRateOverviewFragment.O6(4, g37 != null ? (b75) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment b;

        @DexIgnore
        public c(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.b = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.b;
            g37 g37 = heartRateOverviewFragment.g;
            heartRateOverviewFragment.O6(2, g37 != null ? (b75) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HeartRateOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        MFLogger.d("HeartRateOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(b75 b75) {
        MFLogger.d("HeartRateOverviewFragment", "initUI");
        this.k = (pi6) getChildFragmentManager().Z("HeartRateOverviewDayFragment");
        this.l = (gj6) getChildFragmentManager().Z("HeartRateOverviewWeekFragment");
        this.m = (aj6) getChildFragmentManager().Z("HeartRateOverviewMonthFragment");
        if (this.k == null) {
            this.k = new pi6();
        }
        if (this.l == null) {
            this.l = new gj6();
        }
        if (this.m == null) {
            this.m = new aj6();
        }
        ArrayList arrayList = new ArrayList();
        pi6 pi6 = this.k;
        if (pi6 != null) {
            arrayList.add(pi6);
            gj6 gj6 = this.l;
            if (gj6 != null) {
                arrayList.add(gj6);
                aj6 aj6 = this.m;
                if (aj6 != null) {
                    arrayList.add(aj6);
                    RecyclerView recyclerView = b75.w;
                    pq7.b(recyclerView, "it");
                    recyclerView.setAdapter(new g67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new HeartRateOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mv0().b(recyclerView);
                    O6(this.s, b75);
                    ro4 M = PortfolioApp.h0.c().M();
                    pi6 pi62 = this.k;
                    if (pi62 != null) {
                        gj6 gj62 = this.l;
                        if (gj62 != null) {
                            aj6 aj62 = this.m;
                            if (aj62 != null) {
                                M.t(new ui6(pi62, gj62, aj62)).a(this);
                                b75.u.setOnClickListener(new a(this));
                                b75.r.setOnClickListener(new b(this));
                                b75.s.setOnClickListener(new c(this));
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        g37<b75> g37;
        b75 a2;
        g37<b75> g372;
        b75 a3;
        ConstraintLayout constraintLayout;
        g37<b75> g373;
        b75 a4;
        FlexibleTextView flexibleTextView;
        g37<b75> g374;
        b75 a5;
        String d = qn5.l.a().d("dianaHeartRateTab");
        String d2 = qn5.l.a().d("onDianaHeartRateTab");
        if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
            a5.x.setBackgroundColor(Color.parseColor(d));
            a5.y.setBackgroundColor(Color.parseColor(d));
        }
        if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
            flexibleTextView.setTextColor(Color.parseColor(d2));
        }
        this.t = qn5.l.a().d("onHybridInactiveTab");
        String d3 = qn5.l.a().d("nonBrandSurface");
        this.u = qn5.l.a().d("primaryText");
        if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
            constraintLayout.setBackgroundColor(Color.parseColor(d3));
        }
        if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
            FlexibleTextView flexibleTextView2 = a2.u;
            pq7.b(flexibleTextView2, "it.ftvToday");
            if (flexibleTextView2.isSelected()) {
                a2.u.setTextColor(Color.parseColor(this.u));
            } else {
                a2.u.setTextColor(Color.parseColor(this.t));
            }
            FlexibleTextView flexibleTextView3 = a2.r;
            pq7.b(flexibleTextView3, "it.ftv7Days");
            if (flexibleTextView3.isSelected()) {
                a2.r.setTextColor(Color.parseColor(this.u));
            } else {
                a2.r.setTextColor(Color.parseColor(this.t));
            }
            FlexibleTextView flexibleTextView4 = a2.s;
            pq7.b(flexibleTextView4, "it.ftvMonth");
            if (flexibleTextView4.isSelected()) {
                a2.s.setTextColor(Color.parseColor(this.u));
            } else {
                a2.s.setTextColor(Color.parseColor(this.t));
            }
        }
    }

    @DexIgnore
    public final void O6(int i2, b75 b75) {
        b75 a2;
        RecyclerView recyclerView;
        g37<b75> g37;
        b75 a3;
        b75 a4;
        RecyclerView recyclerView2;
        b75 a5;
        RecyclerView recyclerView3;
        b75 a6;
        RecyclerView recyclerView4;
        if (b75 != null) {
            FlexibleTextView flexibleTextView = b75.u;
            pq7.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = b75.r;
            pq7.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = b75.s;
            pq7.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = b75.u;
            pq7.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = b75.r;
            pq7.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = b75.s;
            pq7.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = b75.s;
                pq7.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = b75.s;
                pq7.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = b75.r;
                pq7.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                g37<b75> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = b75.r;
                pq7.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = b75.r;
                pq7.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = b75.r;
                pq7.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                g37<b75> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = b75.u;
                pq7.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = b75.u;
                pq7.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = b75.r;
                pq7.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                g37<b75> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = b75.u;
                pq7.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = b75.u;
                pq7.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = b75.r;
                pq7.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                g37<b75> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                pq7.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                pq7.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                pq7.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b75 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewFragment", "onCreateView");
        b75 b75 = (b75) aq0.f(layoutInflater, 2131558564, viewGroup, false, A6());
        mo0.y0(b75.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        pq7.b(b75, "binding");
        M6(b75);
        this.g = new g37<>(this, b75);
        N6();
        g37<b75> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
