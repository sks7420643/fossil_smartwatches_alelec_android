package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.h27;
import com.fossil.i27;
import com.fossil.i47;
import com.fossil.j27;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomeActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public i27 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, WelcomeActivity.class));
        }

        @DexIgnore
        public final void b(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void c(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("KEY_DIANA_REQUIRE", true);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        h27 h27 = (h27) getSupportFragmentManager().Y(2131362158);
        if (h27 == null) {
            h27 = (!getIntent().hasExtra("KEY_DIANA_REQUIRE") || !getIntent().getBooleanExtra("KEY_DIANA_REQUIRE", false)) ? new h27() : h27.k.a();
            k(h27, "WelcomeFragment", 2131362158);
        }
        PortfolioApp.h0.c().M().w1(new j27(h27)).a(this);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().e(r(), "Service Tracking - startForegroundService in WelcomeActivity");
        i47.f1583a.d(this, MFDeviceService.class, Constants.STOP_FOREGROUND_ACTION);
        i47.f1583a.d(this, ButtonService.class, Constants.STOP_FOREGROUND_ACTION);
    }
}
