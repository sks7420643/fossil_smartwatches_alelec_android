package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import com.fossil.al7;
import com.fossil.bz4;
import com.fossil.hr7;
import com.fossil.jl5;
import com.fossil.jy5;
import com.fossil.pq7;
import com.fossil.um5;
import com.fossil.wy4;
import com.fossil.xy4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Locale;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerTextView extends FlexibleTextView implements bz4 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public String A;
    @DexIgnore
    public int B; // = 2131099677;
    @DexIgnore
    public long v;
    @DexIgnore
    public int w;
    @DexIgnore
    public CountDownTimer x;
    @DexIgnore
    public TimerViewObserver y;
    @DexIgnore
    public wy4 z; // = wy4.TIME_LEFT;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends CountDownTimer {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ TimerTextView f4732a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(TimerTextView timerTextView, long j, long j2, long j3) {
            super(j2, j3);
            this.f4732a = timerTextView;
        }

        @DexIgnore
        public void onFinish() {
            this.f4732a.u();
        }

        @DexIgnore
        public void onTick(long j) {
            this.f4732a.v(j);
        }
    }

    /*
    static {
        String simpleName = TimerTextView.class.getSimpleName();
        pq7.b(simpleName, "TimerTextView::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context) {
        super(context);
        pq7.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
    }

    @DexIgnore
    private final void setDayText(int i) {
        hr7 hr7 = hr7.f1520a;
        Locale locale = Locale.US;
        pq7.b(locale, "Locale.US");
        String c = um5.c(getContext(), 2131886202);
        pq7.b(c, "LanguageHelper.getString\u2026lenge_Label__StartInDays)");
        String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(locale, format, *args)");
        hr7 hr72 = hr7.f1520a;
        Locale locale2 = Locale.US;
        pq7.b(locale2, "Locale.US");
        String c2 = um5.c(getContext(), 2131886207);
        pq7.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{format}, 1));
        pq7.b(format2, "java.lang.String.format(locale, format, *args)");
        int i2 = jy5.c[this.z.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(jl5.b.a(format, format2, this.B));
        }
    }

    @DexIgnore
    private final void setHourText(int i) {
        hr7 hr7 = hr7.f1520a;
        Locale locale = Locale.US;
        pq7.b(locale, "Locale.US");
        String c = um5.c(getContext(), 2131886203);
        pq7.b(c, "LanguageHelper.getString\u2026enge_Label__StartInHours)");
        String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(locale, format, *args)");
        hr7 hr72 = hr7.f1520a;
        Locale locale2 = Locale.US;
        pq7.b(locale2, "Locale.US");
        String c2 = um5.c(getContext(), 2131886207);
        pq7.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{format}, 1));
        pq7.b(format2, "java.lang.String.format(locale, format, *args)");
        int i2 = jy5.d[this.z.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(jl5.b.a(format, format2, this.B));
        }
    }

    @DexIgnore
    private final void setMinuteText(int i) {
        hr7 hr7 = hr7.f1520a;
        Locale locale = Locale.US;
        pq7.b(locale, "Locale.US");
        String c = um5.c(getContext(), 2131886205);
        pq7.b(c, "LanguageHelper.getString\u2026lenge_Label__StartInMins)");
        String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(locale, format, *args)");
        hr7 hr72 = hr7.f1520a;
        Locale locale2 = Locale.US;
        pq7.b(locale2, "Locale.US");
        String c2 = um5.c(getContext(), 2131886207);
        pq7.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{format}, 1));
        pq7.b(format2, "java.lang.String.format(locale, format, *args)");
        int i2 = jy5.e[this.z.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(jl5.b.a(format, format2, this.B));
        }
    }

    @DexIgnore
    private final void setSecText(int i) {
        hr7 hr7 = hr7.f1520a;
        Locale locale = Locale.US;
        pq7.b(locale, "Locale.US");
        String c = um5.c(getContext(), 2131886206);
        pq7.b(c, "LanguageHelper.getString\u2026lenge_Label__StartInSecs)");
        String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        pq7.b(format, "java.lang.String.format(locale, format, *args)");
        hr7 hr72 = hr7.f1520a;
        Locale locale2 = Locale.US;
        pq7.b(locale2, "Locale.US");
        String c2 = um5.c(getContext(), 2131886207);
        pq7.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{format}, 1));
        pq7.b(format2, "java.lang.String.format(locale, format, *args)");
        int i2 = jy5.f[this.z.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(jl5.b.a(format, format2, this.B));
        }
    }

    @DexIgnore
    public final wy4 getDisplayType() {
        return this.z;
    }

    @DexIgnore
    public final String getEndingText() {
        return this.A;
    }

    @DexIgnore
    public final int getHighlightColour() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.bz4
    public void onPause() {
        FLogger.INSTANCE.getLocal().e(C, "onPause");
        CountDownTimer countDownTimer = this.x;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.bz4
    public void onResume() {
        FLogger.INSTANCE.getLocal().e(C, "onResume");
        q(this.v - xy4.f4212a.b());
    }

    @DexIgnore
    public final void q(long j) {
        CountDownTimer countDownTimer = this.x;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.x = new a(this, j, j, 1000).start();
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().e(C, "clean");
        CountDownTimer countDownTimer = this.x;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        TimerViewObserver timerViewObserver = this.y;
        if (timerViewObserver != null) {
            timerViewObserver.d(s());
        }
    }

    @DexIgnore
    public final int s() {
        return System.identityHashCode(this);
    }

    @DexIgnore
    public final void setDisplayType(wy4 wy4) {
        pq7.c(wy4, "<set-?>");
        this.z = wy4;
    }

    @DexIgnore
    public final void setEndingText(String str) {
        this.A = str;
    }

    @DexIgnore
    public final void setHighlightColour(int i) {
        this.B = i;
    }

    @DexIgnore
    public final void setObserver(TimerViewObserver timerViewObserver) {
        if (this.y == null) {
            this.y = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.c(this, s());
            }
        }
    }

    @DexIgnore
    public final void setTime(long j) {
        this.v = j;
        q(j - xy4.f4212a.b());
    }

    @DexIgnore
    public final void t(long j) {
        long j2 = (long) DateTimeConstants.MILLIS_PER_DAY;
        if (j >= j2) {
            int i = (int) (j / j2);
            if (this.w != i) {
                this.w = i;
                setDayText(i);
                return;
            }
            return;
        }
        long j3 = (long) DateTimeConstants.MILLIS_PER_HOUR;
        if (j >= j3) {
            int i2 = (int) (j / j3);
            if (this.w != i2) {
                this.w = i2;
                setHourText(i2);
                return;
            }
            return;
        }
        long j4 = (long) 60000;
        if (j >= j4) {
            int i3 = (int) (j / j4);
            if (this.w != i3) {
                this.w = i3;
                setMinuteText(i3);
                return;
            }
            return;
        }
        setSecText((int) (j / ((long) 1000)));
    }

    @DexIgnore
    public final void u() {
        CharSequence c;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = C;
        local.e(str, "parseTextOnFinish - " + this.A + " - " + this.z);
        String str2 = this.A;
        if (str2 != null) {
            setText(str2);
            return;
        }
        int i = jy5.b[this.z.ordinal()];
        if (i == 1) {
            c = um5.c(getContext(), 2131886208);
        } else if (i == 2) {
            String c2 = um5.c(getContext(), 2131886208);
            jl5 jl5 = jl5.b;
            pq7.b(c2, "starting");
            c = jl5.a(c2, c2, this.B);
        } else if (i == 3) {
            c = jl5.b.s(0);
        } else {
            throw new al7();
        }
        setText(c);
    }

    @DexIgnore
    public final void v(long j) {
        int i = jy5.f1835a[this.z.ordinal()];
        if (i == 1 || i == 2) {
            t(j);
        } else if (i == 3) {
            setText(jl5.b.s((int) (j / ((long) 1000))));
        }
    }

    @DexIgnore
    public final void w(TimerViewObserver timerViewObserver, int i) {
        if (this.y == null) {
            this.y = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.c(this, getId() + i);
            }
        }
    }
}
