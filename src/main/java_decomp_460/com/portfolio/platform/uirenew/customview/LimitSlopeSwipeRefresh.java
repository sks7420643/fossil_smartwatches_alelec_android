package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LimitSlopeSwipeRefresh extends SwipeRefreshLayout {
    @DexIgnore
    public /* final */ int W;
    @DexIgnore
    public Float a0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LimitSlopeSwipeRefresh(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attrs");
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        pq7.b(viewConfiguration, "ViewConfiguration.get(context)");
        this.W = viewConfiguration.getScaledTouchSlop();
    }

    @DexIgnore
    @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Integer valueOf = motionEvent != null ? Integer.valueOf(motionEvent.getAction()) : null;
        if (valueOf != null && valueOf.intValue() == 0) {
            this.a0 = Float.valueOf(motionEvent.getX());
        } else if (valueOf != null && valueOf.intValue() == 2) {
            float x = motionEvent.getX();
            Float f = this.a0;
            if (f == null) {
                pq7.i();
                throw null;
            } else if (Math.abs(x - f.floatValue()) > ((float) this.W)) {
                return false;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}
