package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.at4;
import com.fossil.f57;
import com.fossil.hm7;
import com.fossil.hr7;
import com.fossil.hz4;
import com.fossil.jl5;
import com.fossil.pq7;
import com.fossil.tj5;
import com.fossil.ty4;
import com.fossil.um5;
import com.fossil.uy4;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FriendsInView extends ConstraintLayout {
    @DexIgnore
    public ImageView A;
    @DexIgnore
    public FlexibleTextView B;
    @DexIgnore
    public f57.b C;
    @DexIgnore
    public uy4 D;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public ImageView y;
    @DexIgnore
    public ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context) {
        super(context);
        pq7.c(context, "context");
        G(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        pq7.c(context, "context");
        pq7.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    public final void G(Context context, AttributeSet attributeSet) {
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.C = f;
        this.D = uy4.d.b();
        View inflate = View.inflate(context, 2131558646, this);
        View findViewById = inflate.findViewById(2131362618);
        pq7.b(findViewById, "view.findViewById(R.id.img1)");
        this.w = (ImageView) findViewById;
        View findViewById2 = inflate.findViewById(2131362619);
        pq7.b(findViewById2, "view.findViewById(R.id.img2)");
        this.x = (ImageView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362620);
        pq7.b(findViewById3, "view.findViewById(R.id.img3)");
        this.y = (ImageView) findViewById3;
        View findViewById4 = inflate.findViewById(2131362621);
        pq7.b(findViewById4, "view.findViewById(R.id.img4)");
        this.z = (ImageView) findViewById4;
        View findViewById5 = inflate.findViewById(2131362622);
        pq7.b(findViewById5, "view.findViewById(R.id.img5)");
        this.A = (ImageView) findViewById5;
        View findViewById6 = inflate.findViewById(2131362406);
        pq7.b(findViewById6, "view.findViewById(R.id.ftv_description)");
        this.B = (FlexibleTextView) findViewById6;
    }

    @DexIgnore
    public final void setData(List<at4> list) {
        String str;
        String e;
        String str2;
        pq7.c(list, "friendsIn");
        if (list.isEmpty()) {
            setVisibility(8);
            return;
        }
        pq7.b(tj5.a(getContext()), "GlideApp.with(context)");
        int size = list.size();
        Iterator<T> it = list.iterator();
        int i = 0;
        while (true) {
            boolean hasNext = it.hasNext();
            String str3 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            if (hasNext) {
                T next = it.next();
                if (i >= 0) {
                    T t = next;
                    hz4 hz4 = hz4.f1561a;
                    String b = t != null ? t.b() : null;
                    String d = t != null ? t.d() : null;
                    if (t == null || (str2 = t.e()) == null) {
                        str2 = str3;
                    }
                    String a2 = hz4.a(b, d, str2);
                    if (i == 0) {
                        ImageView imageView = this.w;
                        if (imageView != null) {
                            imageView.setVisibility(0);
                            ImageView imageView2 = this.w;
                            if (imageView2 != null) {
                                String a3 = t != null ? t.a() : null;
                                f57.b bVar = this.C;
                                if (bVar != null) {
                                    uy4 uy4 = this.D;
                                    if (uy4 != null) {
                                        ty4.b(imageView2, a3, a2, bVar, uy4);
                                    } else {
                                        pq7.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                pq7.n("img1");
                                throw null;
                            }
                        } else {
                            pq7.n("img1");
                            throw null;
                        }
                    } else if (i == 1) {
                        ImageView imageView3 = this.x;
                        if (imageView3 != null) {
                            imageView3.setVisibility(0);
                            ImageView imageView4 = this.x;
                            if (imageView4 != null) {
                                String a4 = t != null ? t.a() : null;
                                f57.b bVar2 = this.C;
                                if (bVar2 != null) {
                                    uy4 uy42 = this.D;
                                    if (uy42 != null) {
                                        ty4.b(imageView4, a4, a2, bVar2, uy42);
                                    } else {
                                        pq7.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                pq7.n("img2");
                                throw null;
                            }
                        } else {
                            pq7.n("img2");
                            throw null;
                        }
                    } else if (i == 2) {
                        ImageView imageView5 = this.y;
                        if (imageView5 != null) {
                            imageView5.setVisibility(0);
                            ImageView imageView6 = this.y;
                            if (imageView6 != null) {
                                String a5 = t != null ? t.a() : null;
                                f57.b bVar3 = this.C;
                                if (bVar3 != null) {
                                    uy4 uy43 = this.D;
                                    if (uy43 != null) {
                                        ty4.b(imageView6, a5, a2, bVar3, uy43);
                                    } else {
                                        pq7.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                pq7.n("img3");
                                throw null;
                            }
                        } else {
                            pq7.n("img3");
                            throw null;
                        }
                    } else if (i == 3) {
                        ImageView imageView7 = this.z;
                        if (imageView7 != null) {
                            imageView7.setVisibility(0);
                            ImageView imageView8 = this.z;
                            if (imageView8 != null) {
                                String a6 = t != null ? t.a() : null;
                                f57.b bVar4 = this.C;
                                if (bVar4 != null) {
                                    uy4 uy44 = this.D;
                                    if (uy44 != null) {
                                        ty4.b(imageView8, a6, a2, bVar4, uy44);
                                    } else {
                                        pq7.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                pq7.n("img4");
                                throw null;
                            }
                        } else {
                            pq7.n("img4");
                            throw null;
                        }
                    } else {
                        continue;
                    }
                    i++;
                } else {
                    hm7.l();
                    throw null;
                }
            } else {
                if (size > 5) {
                    ImageView imageView9 = this.A;
                    if (imageView9 != null) {
                        imageView9.setVisibility(0);
                        ImageView imageView10 = this.A;
                        if (imageView10 != null) {
                            f57.b bVar5 = this.C;
                            if (bVar5 != null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append('+');
                                sb.append(list.size() - 4);
                                String sb2 = sb.toString();
                                uy4 uy45 = this.D;
                                if (uy45 != null) {
                                    imageView10.setImageDrawable(bVar5.e(sb2, uy45.c()));
                                } else {
                                    pq7.n("colorGenerator");
                                    throw null;
                                }
                            } else {
                                pq7.n("drawableBuilder");
                                throw null;
                            }
                        } else {
                            pq7.n("img5");
                            throw null;
                        }
                    } else {
                        pq7.n("img5");
                        throw null;
                    }
                } else if (size == 5) {
                    ImageView imageView11 = this.A;
                    if (imageView11 != null) {
                        imageView11.setVisibility(0);
                        ImageView imageView12 = this.A;
                        if (imageView12 != null) {
                            at4 at4 = list.get(4);
                            String a7 = at4 != null ? at4.a() : null;
                            hz4 hz42 = hz4.f1561a;
                            at4 at42 = list.get(4);
                            String b2 = at42 != null ? at42.b() : null;
                            at4 at43 = list.get(4);
                            String d2 = at43 != null ? at43.d() : null;
                            at4 at44 = list.get(4);
                            if (at44 == null || (str = at44.e()) == null) {
                                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                            }
                            String a8 = hz42.a(b2, d2, str);
                            f57.b bVar6 = this.C;
                            if (bVar6 != null) {
                                uy4 uy46 = this.D;
                                if (uy46 != null) {
                                    ty4.b(imageView12, a7, a8, bVar6, uy46);
                                } else {
                                    pq7.n("colorGenerator");
                                    throw null;
                                }
                            } else {
                                pq7.n("drawableBuilder");
                                throw null;
                            }
                        } else {
                            pq7.n("img5");
                            throw null;
                        }
                    } else {
                        pq7.n("img5");
                        throw null;
                    }
                }
                hz4 hz43 = hz4.f1561a;
                at4 at45 = list.get(0);
                String b3 = at45 != null ? at45.b() : null;
                at4 at46 = list.get(0);
                String d3 = at46 != null ? at46.d() : null;
                at4 at47 = list.get(0);
                if (!(at47 == null || (e = at47.e()) == null)) {
                    str3 = e;
                }
                String a9 = hz43.a(b3, d3, str3);
                if (size > 1) {
                    hr7 hr7 = hr7.f1520a;
                    String c = um5.c(getContext(), 2131886209);
                    pq7.b(c, "LanguageHelper.getString\u2026ameAndNumberOthersJoined)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{a9, String.valueOf(size - 1)}, 2));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    Spannable b4 = jl5.b(jl5.b, a9, format, 0, 4, null);
                    FlexibleTextView flexibleTextView = this.B;
                    if (flexibleTextView != null) {
                        flexibleTextView.setText(b4);
                        return;
                    } else {
                        pq7.n("ftvDes");
                        throw null;
                    }
                } else {
                    hr7 hr72 = hr7.f1520a;
                    String c2 = um5.c(getContext(), 2131886210);
                    pq7.b(c2, "LanguageHelper.getString\u2026ge_Label__UsernameJoined)");
                    String format2 = String.format(c2, Arrays.copyOf(new Object[]{a9}, 1));
                    pq7.b(format2, "java.lang.String.format(format, *args)");
                    Spannable b5 = jl5.b(jl5.b, a9, format2, 0, 4, null);
                    FlexibleTextView flexibleTextView2 = this.B;
                    if (flexibleTextView2 != null) {
                        flexibleTextView2.setText(b5);
                        return;
                    } else {
                        pq7.n("ftvDes");
                        throw null;
                    }
                }
            }
        }
    }
}
