package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.fossil.i47;
import com.fossil.jy6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.ly6;
import com.fossil.pq7;
import com.fossil.rx6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingActivity extends ls5 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public ly6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return PairingActivity.B;
        }

        @DexIgnore
        public final void b(Context context, boolean z) {
            pq7.c(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a();
            local.d(a2, "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = PairingActivity.class.getSimpleName();
        pq7.b(simpleName, "PairingActivity::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public final void M() {
        if (TextUtils.isEmpty(PortfolioApp.h0.c().J())) {
            FLogger.INSTANCE.getLocal().d(r(), "Service Tracking - startForegroundService in PairingActivity");
            i47.f1583a.d(this, MFDeviceService.class, Constants.START_FOREGROUND_ACTION);
            i47.f1583a.d(this, ButtonService.class, Constants.START_FOREGROUND_ACTION);
            return;
        }
        FLogger.INSTANCE.getLocal().d(r(), "Skip start service");
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.ls5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        rx6 rx6 = (rx6) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (rx6 == null) {
            rx6 = rx6.k.a(z);
            i(rx6, 2131362158);
        }
        PortfolioApp.h0.c().M().Q0(new jy6(rx6)).a(this);
        if (bundle != null) {
            ly6 ly6 = this.A;
            if (ly6 != null) {
                ly6.m0(bundle.getBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP"));
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        ly6 ly6 = this.A;
        if (ly6 != null) {
            bundle.putBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP", ly6.h0());
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onStart() {
        super.onStart();
        M();
    }
}
