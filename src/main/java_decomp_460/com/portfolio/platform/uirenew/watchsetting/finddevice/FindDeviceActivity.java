package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.b17;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.w17;
import com.fossil.y17;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public y17 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, "serial");
            Intent intent = new Intent(context, FindDeviceActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDeviceActivity", "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        b17 b17 = (b17) getSupportFragmentManager().Y(2131362158);
        if (b17 == null) {
            b17 = b17.w.a();
            k(b17, "FindDeviceFragment", 2131362158);
        }
        PortfolioApp.h0.c().M().S(new w17(b17)).a(this);
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.d(r, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                y17 y17 = this.A;
                if (y17 != null) {
                    y17.H(string);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "retrieve serial from intent " + stringExtra);
            y17 y172 = this.A;
            if (y172 != null) {
                pq7.b(stringExtra, "serial");
                y172.H(stringExtra);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        y17 y17 = this.A;
        if (y17 != null) {
            bundle.putString("SERIAL", y17.B());
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }
}
