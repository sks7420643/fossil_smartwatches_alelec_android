package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.d17;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingActivity extends ls5 {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public static /* final */ a B; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchSettingActivity.A;
        }

        @DexIgnore
        public final void b(Context context, String str) {
            pq7.c(context, "context");
            pq7.c(str, "serial");
            Intent intent = new Intent(context, WatchSettingActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a();
            local.d(a2, "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchSettingActivity.class.getSimpleName();
        pq7.b(simpleName, "WatchSettingActivity::class.java.simpleName");
        A = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        d17 d17 = (d17) getSupportFragmentManager().Y(2131362158);
        if (d17 == null) {
            d17 = d17.t.b();
            k(d17, d17.t.a(), 2131362158);
        }
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.d(r, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("SERIAL", string);
                d17.setArguments(bundle2);
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "retrieve serial from intent " + stringExtra);
            Bundle bundle3 = new Bundle();
            bundle3.putString("SERIAL", stringExtra);
            d17.setArguments(bundle3);
        }
    }
}
