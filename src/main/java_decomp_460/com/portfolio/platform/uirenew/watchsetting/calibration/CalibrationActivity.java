package com.portfolio.platform.uirenew.watchsetting.calibration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.a17;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.m17;
import com.fossil.o17;
import com.fossil.pq7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public o17 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            Intent intent = new Intent(context, CalibrationActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        a17 a17 = (a17) getSupportFragmentManager().Y(2131362158);
        if (a17 == null) {
            a17 = a17.u.a();
            i(a17, 2131362158);
        }
        PortfolioApp.h0.c().M().e1(new m17(a17)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onResume() {
        super.onResume();
        l(false);
    }
}
