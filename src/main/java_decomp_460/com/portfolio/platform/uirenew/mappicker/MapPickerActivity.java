package com.portfolio.platform.uirenew.mappicker;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ev6;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerActivity extends ls5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Fragment fragment, double d, double d2, String str, int i, Object obj) {
            double d3 = 0.0d;
            double d4 = (i & 2) != 0 ? 0.0d : d;
            if ((i & 4) == 0) {
                d3 = d2;
            }
            aVar.a(fragment, d4, d3, (i & 8) != 0 ? "" : str);
        }

        @DexIgnore
        public final void a(Fragment fragment, double d, double d2, String str) {
            pq7.c(fragment, "context");
            pq7.c(str, "address");
            Intent intent = new Intent(fragment.getContext(), MapPickerActivity.class);
            intent.putExtra("latitude", d);
            intent.putExtra("longitude", d2);
            intent.putExtra("address", str);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        String str;
        double d;
        double d2;
        String stringExtra;
        double d3 = 0.0d;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((ev6) getSupportFragmentManager().Y(2131362158)) == null) {
            Intent intent = getIntent();
            if (intent != null) {
                double doubleExtra = intent.hasExtra("latitude") ? intent.getDoubleExtra("latitude", 0.0d) : 0.0d;
                if (intent.hasExtra("longitude")) {
                    d3 = intent.getDoubleExtra("longitude", 0.0d);
                }
                str = (!intent.hasExtra("address") || (stringExtra = intent.getStringExtra("address")) == null) ? "" : stringExtra;
                d = doubleExtra;
                d2 = d3;
            } else {
                str = "";
                d = 0.0d;
                d2 = 0.0d;
            }
            k(ev6.m.a(d, d2, str), "MapPickerFragment", 2131362158);
        }
    }
}
