package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.nk5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.u07;
import com.fossil.v07;
import com.fossil.x07;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TroubleshootingActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public x07 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void c(a aVar, Context context, String str, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                str = "";
            }
            if ((i & 4) != 0) {
                z = false;
            }
            if ((i & 8) != 0) {
                z2 = false;
            }
            aVar.b(context, str, z, z2);
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public final void b(Context context, String str, boolean z, boolean z2) {
            pq7.c(context, "context");
            pq7.c(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            if (TextUtils.isEmpty(str) || nk5.o.x(str)) {
                intent.putExtra("DEVICE_TYPE", false);
            } else {
                intent.putExtra("DEVICE_TYPE", true);
            }
            intent.putExtra("IS_PAIRING_FLOW", z);
            intent.putExtra("IS_ONBOARDING_FLOW", z2);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean L(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        u07 u07 = (u07) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        pq7.b(intent, "intent");
        boolean L = L(intent);
        Intent intent2 = getIntent();
        pq7.b(intent2, "intent");
        Bundle extras = intent2.getExtras();
        boolean z2 = extras != null ? extras.getBoolean("IS_PAIRING_FLOW", false) : false;
        Intent intent3 = getIntent();
        pq7.b(intent3, "intent");
        Bundle extras2 = intent3.getExtras();
        if (extras2 != null) {
            z = extras2.getBoolean("IS_ONBOARDING_FLOW", false);
        }
        if (u07 == null) {
            u07 = u07.u.e(L, z2, z);
            k(u07, u07.u.d(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (u07 != null) {
            M.H(new v07(u07)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
