package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.av6;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.fossil.un5;
import com.fossil.vn5;
import com.fossil.wn5;
import com.fossil.wu6;
import com.fossil.xn5;
import com.fossil.xu6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginActivity extends ls5 {
    @DexIgnore
    public static /* final */ a F; // = new a(null);
    @DexIgnore
    public un5 A;
    @DexIgnore
    public vn5 B;
    @DexIgnore
    public xn5 C;
    @DexIgnore
    public wn5 D;
    @DexIgnore
    public av6 E;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            pq7.c(context, "context");
            context.startActivity(new Intent(context, LoginActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            un5 un5 = this.A;
            if (un5 != null) {
                un5.d(i, i2, intent);
                vn5 vn5 = this.B;
                if (vn5 != null) {
                    vn5.h(i, i2, intent);
                    xn5 xn5 = this.C;
                    if (xn5 != null) {
                        xn5.b(i, i2, intent);
                        Fragment Y = getSupportFragmentManager().Y(2131362158);
                        if (Y != null) {
                            Y.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    pq7.n("mLoginWeiboManager");
                    throw null;
                }
                pq7.n("mLoginGoogleManager");
                throw null;
            }
            pq7.n("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        wu6 wu6 = (wu6) getSupportFragmentManager().Y(2131362158);
        if (wu6 == null) {
            wu6 = wu6.k.b();
            k(wu6, wu6.k.a(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (wu6 != null) {
            M.N0(new xu6(this, wu6)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.login.LoginContract.View");
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        pq7.c(intent, "intent");
        super.onNewIntent(intent);
        wn5 wn5 = this.D;
        if (wn5 != null) {
            Intent intent2 = getIntent();
            pq7.b(intent2, "getIntent()");
            wn5.g(intent2);
            return;
        }
        pq7.n("mLoginWechatManager");
        throw null;
    }
}
