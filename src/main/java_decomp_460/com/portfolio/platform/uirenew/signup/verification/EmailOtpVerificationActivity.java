package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.fossil.a07;
import com.fossil.b07;
import com.fossil.f07;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.ls5;
import com.fossil.pq7;
import com.fossil.ro4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EmailOtpVerificationActivity extends ls5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public b07 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            pq7.c(context, "context");
            pq7.c(signUpEmailAuth, "emailAuth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EmailOtpVerificationActivity", "start verify Otp with Email Auth =" + signUpEmailAuth);
            Intent intent = new Intent(context, EmailOtpVerificationActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.ls5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        a07 a07 = (a07) getSupportFragmentManager().Y(2131362158);
        if (a07 == null) {
            a07 = a07.w.a();
            k(a07, r(), 2131362158);
        }
        ro4 M = PortfolioApp.h0.c().M();
        if (a07 != null) {
            M.H1(new f07(this, a07)).a(this);
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra("EMAIL_AUTH")) {
                FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent emailAddress");
                b07 b07 = this.A;
                if (b07 != null) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("EMAIL_AUTH");
                    pq7.b(parcelableExtra, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                    b07.u((SignUpEmailAuth) parcelableExtra);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationContract.View");
    }
}
