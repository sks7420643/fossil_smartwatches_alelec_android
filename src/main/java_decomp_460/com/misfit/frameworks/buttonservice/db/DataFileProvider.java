package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataFileProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "data_file.db";
    @DexIgnore
    public static DataFileProvider sInstance;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.db.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE datafile ADD COLUMN syncTime BIGINT");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
        }
    }

    @DexIgnore
    public DataFileProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return this.databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static DataFileProvider getInstance(Context context) {
        DataFileProvider dataFileProvider;
        synchronized (DataFileProvider.class) {
            try {
                if (sInstance == null) {
                    sInstance = new DataFileProvider(context, DB_NAME);
                }
                dataFileProvider = sInstance;
            } catch (Throwable th) {
                throw th;
            }
        }
        return dataFileProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.i(str, "Inside " + this.TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.e(str2, "Error inside " + this.TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public int deleteDataFiles(String str) {
        try {
            DeleteBuilder<DataFile, String> deleteBuilder = getDataFileDao().deleteBuilder();
            deleteBuilder.where().eq("serial", str);
            return deleteBuilder.delete();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".deleteDataFile - e=" + e);
            return 0;
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            String str2 = "SELECT key FROM dataFile WHERE serial = '" + str + "' AND syncTime <= " + j;
            GenericRawResults<String[]> queryRaw = getDataFileDao().queryRaw(str2, new String[0]);
            FLogger.INSTANCE.getLocal().d(this.TAG, str2);
            ArrayList arrayList2 = new ArrayList();
            for (String[] strArr : queryRaw) {
                if (strArr.length > 0) {
                    arrayList2.add(strArr[0]);
                }
            }
            queryRaw.close();
            if (arrayList2.size() > 0) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    arrayList.add(getDataFile((String) it.next()));
                }
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e(this.TAG, "Error inside " + this.TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0122, code lost:
        if (r3 != null) goto L_0x0124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0124, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0127, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0164, code lost:
        if (r3 != null) goto L_0x0124;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:55:0x016a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.buttonservice.db.DataFile getDataFile(java.lang.String r19, java.lang.String r20) {
        /*
        // Method dump skipped, instructions count: 378
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.db.DataFileProvider.getDataFile(java.lang.String, java.lang.String):com.misfit.frameworks.buttonservice.db.DataFile");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                local2.d(str2, "Inside " + this.TAG + ".saveDataFile - Save dataFile=" + dataFile);
                getDataFileDao().create((Dao<DataFile, String>) dataFile);
                return;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.e(str3, "Error inside " + this.TAG + ".saveDataFile -e=Data file existed");
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = this.TAG;
            local4.e(str4, "Error inside " + this.TAG + ".saveDataFile - e=" + e);
        }
    }
}
