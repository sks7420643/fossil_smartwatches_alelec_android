package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.al7;
import com.fossil.fp1;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.qq1;
import com.fossil.rn1;
import com.fossil.ry1;
import com.fossil.tn1;
import com.fossil.xt1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public float temperature;
    @DexIgnore
    public TemperatureUnit temperatureUnit;
    @DexIgnore
    public WeatherCondition weatherCondition;

    @DexIgnore
    public enum TemperatureUnit {
        C,
        F;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public final rn1 toSdkTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return rn1.C;
            }
            if (i == 2) {
                return rn1.F;
            }
            throw new al7();
        }
    }

    @DexIgnore
    public enum WeatherCondition {
        CLEAR_DAY,
        CLEAR_NIGHT,
        RAIN,
        SNOW,
        SLEET,
        WIND,
        FOG,
        CLOUDY,
        PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherCondition.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherCondition.CLEAR_DAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherCondition.CLEAR_NIGHT.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherCondition.RAIN.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherCondition.SNOW.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherCondition.SLEET.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherCondition.WIND.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherCondition.FOG.ordinal()] = 7;
                $EnumSwitchMapping$0[WeatherCondition.CLOUDY.ordinal()] = 8;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_DAY.ordinal()] = 9;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_NIGHT.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public final tn1 toSdkWeatherCondition() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return tn1.CLEAR_DAY;
                case 2:
                    return tn1.CLEAR_NIGHT;
                case 3:
                    return tn1.RAIN;
                case 4:
                    return tn1.SNOW;
                case 5:
                    return tn1.SLEET;
                case 6:
                    return tn1.WIND;
                case 7:
                    return tn1.FOG;
                case 8:
                    return tn1.CLOUDY;
                case 9:
                    return tn1.PARTLY_CLOUDY_DAY;
                case 10:
                    return tn1.PARTLY_CLOUDY_NIGHT;
                default:
                    throw new al7();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(float f, TemperatureUnit temperatureUnit2, WeatherCondition weatherCondition2, long j) {
        super(np1.WEATHER_COMPLICATION);
        pq7.c(temperatureUnit2, "temperatureUnit");
        pq7.c(weatherCondition2, "weatherCondition");
        this.temperature = f;
        this.temperatureUnit = temperatureUnit2;
        this.weatherCondition = weatherCondition2;
        this.expiredAt = j;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.temperature = parcel.readFloat();
        this.temperatureUnit = TemperatureUnit.values()[parcel.readInt()];
        this.weatherCondition = WeatherCondition.values()[parcel.readInt()];
        this.expiredAt = parcel.readLong();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return new xt1(new fp1(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof qq1)) {
            return null;
        }
        return new xt1((qq1) jq1, new fp1(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.temperatureUnit.ordinal());
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeLong(this.expiredAt);
    }
}
