package com.misfit.frameworks.buttonservice.model.watchapp.response;

import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicResponseFactory {
    @DexIgnore
    public static /* final */ MusicResponseFactory INSTANCE; // = new MusicResponseFactory();

    @DexIgnore
    public final NotifyMusicEventResponse createMusicEventResponse(NotifyMusicEventResponse.MusicMediaAction musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus) {
        pq7.c(musicMediaAction, "musicAction");
        pq7.c(musicMediaStatus, "status");
        return new NotifyMusicEventResponse(musicMediaAction, musicMediaStatus);
    }

    @DexIgnore
    public final MusicTrackInfoResponse createMusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        pq7.c(str, "appName");
        pq7.c(str2, "trackTitle");
        pq7.c(str3, "artistName");
        pq7.c(str4, "albumName");
        return new MusicTrackInfoResponse(str, b, str2, str3, str4);
    }
}
