package com.misfit.frameworks.buttonservice.model.workout;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WorkoutRequestData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutRequestData> CREATOR; // = new Anon1();
    @DexIgnore
    public Boolean isRequiredGPS;
    @DexIgnore
    public Long sessionId;
    @DexIgnore
    public WorkoutType workoutType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<WorkoutRequestData> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutRequestData createFromParcel(Parcel parcel) {
            return new WorkoutRequestData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutRequestData[] newArray(int i) {
            return new WorkoutRequestData[i];
        }
    }

    @DexIgnore
    public WorkoutRequestData(Parcel parcel) {
        this.sessionId = Long.valueOf(parcel.readLong());
        this.isRequiredGPS = Boolean.valueOf(parcel.readString());
        this.workoutType = WorkoutType.values()[parcel.readInt()];
    }

    @DexIgnore
    public WorkoutRequestData(Long l, Boolean bool, WorkoutType workoutType2) {
        this.sessionId = l;
        this.isRequiredGPS = bool;
        this.workoutType = workoutType2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.sessionId.longValue());
        parcel.writeString(this.isRequiredGPS.toString());
        parcel.writeInt(this.workoutType.ordinal());
    }
}
