package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.cq1;
import com.fossil.gt1;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.ws1;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCChallengeInfoWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public ws1 challenge;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.challenge = (ws1) parcel.readParcelable(ws1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(ws1 ws1) {
        super(np1.BUDDY_CHALLENGE_GET_INFO);
        pq7.c(ws1, "challenge");
        this.challenge = ws1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof cq1)) {
            return null;
        }
        cq1 cq1 = (cq1) jq1;
        ws1 ws1 = this.challenge;
        if (ws1 != null) {
            return new gt1(cq1, ws1);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.challenge, i);
    }
}
