package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import android.os.Parcel;
import android.util.Base64;
import com.fossil.tu1;
import com.fossil.wu1;
import com.fossil.xu1;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.common.enums.Gesture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppMapping extends BLEMapping implements Comparable {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppMapping";
    @DexIgnore
    public BLECustomization customization;
    @DexIgnore
    public String[] declarationFiles;
    @DexIgnore
    public Gesture gesture;
    @DexIgnore
    public String microAppId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture;

        /*
        static {
            int[] iArr = new int[Gesture.values().length];
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture = iArr;
            try {
                iArr[Gesture.SAM_BT1_PRESSED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_PRESSED.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_PRESSED.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 19;
            } catch (NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 20;
            } catch (NoSuchFieldError e20) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 21;
            } catch (NoSuchFieldError e21) {
            }
        }
        */
    }

    @DexIgnore
    public MicroAppMapping(Parcel parcel) {
        super(parcel);
        this.gesture = Gesture.fromInt(parcel.readInt());
        this.microAppId = parcel.readString();
        int readInt = parcel.readInt();
        this.declarationFiles = new String[readInt];
        for (int i = 0; i < readInt; i++) {
            this.declarationFiles[i] = parcel.readString();
        }
        this.customization = BLECustomization.Companion.getCREATOR().createFromParcel(parcel);
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String str2, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        String[] strArr = new String[1];
        this.declarationFiles = strArr;
        strArr[0] = str2;
        this.customization = new BLENonCustomization();
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, BLECustomization bLECustomization, Long l) {
        this(gesture2, str, strArr, l);
        this.customization = bLECustomization;
    }

    @DexIgnore
    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        this.declarationFiles = strArr;
        this.customization = new BLENonCustomization();
    }

    @DexIgnore
    public static List<BLEMapping> convertToBLEMapping(List<MicroAppMapping> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        return arrayList;
    }

    @DexIgnore
    public static List<MicroAppMapping> convertToMicroAppMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((MicroAppMapping) it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public static List<tu1> convertToSDKMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            MicroAppMapping microAppMapping = (MicroAppMapping) it.next();
            ArrayList arrayList2 = new ArrayList();
            byte[] bArr = new byte[0];
            String[] strArr = microAppMapping.declarationFiles;
            for (String str : strArr) {
                if (str != null) {
                    bArr = Base64.decode(str, 0);
                }
                FLogger.INSTANCE.getLocal().d(TAG, "convertToMicroAppMapping appId is " + microAppMapping.microAppId + " and extraInfo=" + str);
                arrayList2.add(new wu1(bArr, microAppMapping.customization.getCustomizationFrame()));
            }
            arrayList.add(new tu1(microAppMapping.getButton(), (wu1[]) arrayList2.toArray(new wu1[0])));
        }
        return arrayList;
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        if (obj == null || !(obj instanceof MicroAppMapping)) {
            return 1;
        }
        MicroAppMapping microAppMapping = (MicroAppMapping) obj;
        if (microAppMapping.getGesture() == getGesture()) {
            return 0;
        }
        return microAppMapping.getGesture().getValue() > getGesture().getValue() ? -1 : 1;
    }

    @DexIgnore
    public xu1 getButton() {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[this.gesture.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return xu1.TOP;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return xu1.MIDDLE;
            default:
                return xu1.BOTTOM;
        }
    }

    @DexIgnore
    public BLECustomization getCustomization() {
        return this.customization;
    }

    @DexIgnore
    public String[] getDeclarationFiles() {
        return this.declarationFiles;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public Gesture getGesture() {
        return this.gesture;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.mType);
        sb.append(":");
        sb.append(this.gesture);
        sb.append(":");
        sb.append(this.microAppId);
        sb.append(":");
        for (String str : this.declarationFiles) {
            sb.append(str);
            sb.append(":");
        }
        sb.append(this.customization.getHash());
        return sb.toString();
    }

    @DexIgnore
    public String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedHID() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedHID();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedStreaming() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedStreaming();
    }

    @DexIgnore
    public void setCustomization(BLECustomization bLECustomization) {
        this.customization = bLECustomization;
    }

    @DexIgnore
    public void setGesture(Gesture gesture2) {
        this.gesture = gesture2;
    }

    @DexIgnore
    public void setMicroAppId(String str) {
        this.microAppId = str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.gesture.getValue());
        parcel.writeString(this.microAppId);
        parcel.writeInt(this.declarationFiles.length);
        for (String str : this.declarationFiles) {
            parcel.writeString(str);
        }
        this.customization.writeToParcel(parcel, i);
    }
}
