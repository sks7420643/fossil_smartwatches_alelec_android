package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fu1;
import com.fossil.gu1;
import com.fossil.iu1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.wp1;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EncryptedData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ EncryptMethod encryptMethod;
    @DexIgnore
    public /* final */ byte[] encryptedData;
    @DexIgnore
    public /* final */ EncryptedDataType encryptedDataType;
    @DexIgnore
    public /* final */ KeyType keyType;
    @DexIgnore
    public /* final */ short sequence;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<EncryptedData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new EncryptedData(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData[] newArray(int i) {
            return new EncryptedData[i];
        }
    }

    @DexIgnore
    public enum EncryptMethod {
        NONE,
        AES_CTR,
        AES_CBC_PKCS5;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[fu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[fu1.AES_CBC_PKCS5.ordinal()] = 1;
                    $EnumSwitchMapping$0[fu1.AES_CTR.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final EncryptMethod fromSDKEncryptMethod(fu1 fu1) {
                pq7.c(fu1, "sdkEncryptMethod");
                int i = WhenMappings.$EnumSwitchMapping$0[fu1.ordinal()];
                return i != 1 ? i != 2 ? EncryptMethod.NONE : EncryptMethod.AES_CTR : EncryptMethod.AES_CBC_PKCS5;
            }
        }
    }

    @DexIgnore
    public enum EncryptedDataType {
        BUDDY_CHALLENGE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[gu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[gu1.BUDDY_CHALLENGE.ordinal()] = 1;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final EncryptedDataType fromSDKEncryptedDataType(gu1 gu1) {
                pq7.c(gu1, "sdkEncryptedDataType");
                return WhenMappings.$EnumSwitchMapping$0[gu1.ordinal()] != 1 ? EncryptedDataType.BUDDY_CHALLENGE : EncryptedDataType.BUDDY_CHALLENGE;
            }
        }
    }

    @DexIgnore
    public enum KeyType {
        DEFAULT,
        PRE_SHARED_KEY,
        SHARED_SECRET_KEY;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[iu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[iu1.PRE_SHARED_KEY.ordinal()] = 1;
                    $EnumSwitchMapping$0[iu1.SHARED_SECRET_KEY.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final KeyType fromSDKKeyType(iu1 iu1) {
                pq7.c(iu1, "sdkKeyType");
                int i = WhenMappings.$EnumSwitchMapping$0[iu1.ordinal()];
                return i != 1 ? i != 2 ? KeyType.DEFAULT : KeyType.SHARED_SECRET_KEY : KeyType.PRE_SHARED_KEY;
            }
        }
    }

    @DexIgnore
    public EncryptedData(Parcel parcel) {
        this.encryptMethod = EncryptMethod.values()[parcel.readInt()];
        byte[] bArr = new byte[parcel.readInt()];
        this.encryptedData = bArr;
        parcel.readByteArray(bArr);
        this.encryptedDataType = EncryptedDataType.values()[parcel.readInt()];
        this.keyType = KeyType.values()[parcel.readInt()];
        this.sequence = (short) ((short) parcel.readInt());
    }

    @DexIgnore
    public /* synthetic */ EncryptedData(Parcel parcel, kq7 kq7) {
        this(parcel);
    }

    @DexIgnore
    public EncryptedData(wp1 wp1) {
        pq7.c(wp1, "encryptedDataNotification");
        this.encryptMethod = EncryptMethod.Companion.fromSDKEncryptMethod(wp1.getEncryptMethod());
        this.encryptedData = wp1.getEncryptedData();
        this.encryptedDataType = EncryptedDataType.Companion.fromSDKEncryptedDataType(wp1.getEncryptedDataType());
        this.keyType = KeyType.Companion.fromSDKKeyType(wp1.getKeyType());
        this.sequence = wp1.getSequence();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final EncryptMethod getEncryptMethod() {
        return this.encryptMethod;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.encryptedData;
    }

    @DexIgnore
    public final EncryptedDataType getEncryptedDataType() {
        return this.encryptedDataType;
    }

    @DexIgnore
    public final KeyType getKeyType() {
        return this.keyType;
    }

    @DexIgnore
    public final short getSequence() {
        return this.sequence;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeInt(this.encryptMethod.ordinal());
        parcel.writeInt(this.encryptedData.length);
        parcel.writeByteArray(this.encryptedData);
        parcel.writeInt(this.encryptedDataType.ordinal());
        parcel.writeInt(this.keyType.ordinal());
        parcel.writeInt(this.sequence);
    }
}
