package com.misfit.frameworks.buttonservice.model.notification;

import com.fossil.er7;
import com.fossil.fs7;
import com.fossil.wq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationBaseObj$toExistedNotification$Anon1 extends wq7 {
    @DexIgnore
    public NotificationBaseObj$toExistedNotification$Anon1(NotificationBaseObj notificationBaseObj) {
        super(notificationBaseObj);
    }

    @DexIgnore
    @Override // com.fossil.wq7
    public Object get() {
        return ((NotificationBaseObj) this.receiver).getNotificationFlags();
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public String getName() {
        return "notificationFlags";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public fs7 getOwner() {
        return er7.b(NotificationBaseObj.class);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public String getSignature() {
        return "getNotificationFlags()Ljava/util/List;";
    }
}
