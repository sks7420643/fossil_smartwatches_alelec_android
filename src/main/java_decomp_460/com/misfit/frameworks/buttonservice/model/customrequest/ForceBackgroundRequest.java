package com.misfit.frameworks.buttonservice.model.customrequest;

import android.os.Parcel;
import com.fossil.kq7;
import com.fossil.pq7;
import com.google.gson.Gson;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForceBackgroundRequest extends CustomRequest {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static byte requestId;
    @DexIgnore
    public BackgroundRequestType backgroundRequestType;

    @DexIgnore
    public enum BackgroundRequestType {
        SET_OTA_APPLICATION,
        SET_OTA_BOOT_LOADER,
        GET_ACTIVITY_FILE,
        GET_HW_LOG,
        SET_MUSIC_CONTROL_ANDROID,
        SET_UI_SCRIPT,
        SET_BACKGROUND_AND_COMPLICATION_IMAGE,
        SET_NOTIFICATION_IMAGE,
        SET_LOCALIZATION_FILE,
        SET_CONFIG_FILE,
        GET_CONFIG_FILE,
        SET_NOTIFICATION_ANDROID,
        SET_MULTI_ALARM,
        GET_MULTI_ALARM,
        GET_INFO_FILE,
        SET_NOTIFICATION_FILTER,
        GET_NOTIFICATION_FILTER,
        SET_WATCH_PARAMS_FILE
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final byte getRequestId() {
            byte b = ForceBackgroundRequest.requestId;
            ForceBackgroundRequest.access$setRequestId$cp((byte) (b + 1));
            if (ForceBackgroundRequest.requestId >= Byte.MAX_VALUE) {
                ForceBackgroundRequest.access$setRequestId$cp((byte) 0);
            }
            return b;
        }

        @DexIgnore
        public final void setRequestId(byte b) {
            ForceBackgroundRequest.access$setRequestId$cp(b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[BackgroundRequestType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[BackgroundRequestType.SET_OTA_APPLICATION.ordinal()] = 1;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_OTA_BOOT_LOADER.ordinal()] = 2;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_ACTIVITY_FILE.ordinal()] = 3;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_HW_LOG.ordinal()] = 4;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_MUSIC_CONTROL_ANDROID.ordinal()] = 5;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_UI_SCRIPT.ordinal()] = 6;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_BACKGROUND_AND_COMPLICATION_IMAGE.ordinal()] = 7;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_NOTIFICATION_IMAGE.ordinal()] = 8;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_LOCALIZATION_FILE.ordinal()] = 9;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_CONFIG_FILE.ordinal()] = 10;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_CONFIG_FILE.ordinal()] = 11;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_NOTIFICATION_ANDROID.ordinal()] = 12;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_MULTI_ALARM.ordinal()] = 13;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_MULTI_ALARM.ordinal()] = 14;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_INFO_FILE.ordinal()] = 15;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_NOTIFICATION_FILTER.ordinal()] = 16;
            $EnumSwitchMapping$0[BackgroundRequestType.GET_NOTIFICATION_FILTER.ordinal()] = 17;
            $EnumSwitchMapping$0[BackgroundRequestType.SET_WATCH_PARAMS_FILE.ordinal()] = 18;
        }
        */
    }

    @DexIgnore
    public ForceBackgroundRequest(Parcel parcel) {
        this.backgroundRequestType = BackgroundRequestType.values()[parcel.readInt()];
    }

    @DexIgnore
    public ForceBackgroundRequest(BackgroundRequestType backgroundRequestType2) {
        pq7.c(backgroundRequestType2, "backgroundRequestType");
        this.backgroundRequestType = backgroundRequestType2;
    }

    @DexIgnore
    public static final /* synthetic */ void access$setRequestId$cp(byte b) {
        requestId = (byte) b;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final BackgroundRequestType getBackgroundRequestType() {
        return this.backgroundRequestType;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest
    public byte[] getCustomCommand() {
        ByteBuffer allocate = ByteBuffer.allocate(12);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put((byte) 2);
        allocate.put((byte) 241);
        allocate.put((byte) 82);
        allocate.put((byte) 78);
        byte b = (byte) 0;
        allocate.put(b);
        byte b2 = (byte) 1;
        allocate.put(b2);
        allocate.put((byte) 6);
        allocate.put(Companion.getRequestId());
        allocate.put((byte) 3);
        switch (WhenMappings.$EnumSwitchMapping$0[this.backgroundRequestType.ordinal()]) {
            case 1:
                allocate.put(b2);
                allocate.putShort(1);
                break;
            case 2:
                allocate.put(b2);
                allocate.putShort(2);
                break;
            case 3:
                allocate.put(b);
                allocate.putShort(511);
                break;
            case 4:
                allocate.put(b);
                allocate.putShort(767);
                break;
            case 5:
                allocate.put(b2);
                allocate.putShort(1024);
                break;
            case 6:
                allocate.put(b2);
                allocate.putShort(1280);
                break;
            case 7:
                allocate.put(b2);
                allocate.putShort(1792);
                break;
            case 8:
                allocate.put(b2);
                allocate.putShort(1793);
                break;
            case 9:
                allocate.put(b2);
                allocate.putShort(1794);
                break;
            case 10:
                allocate.put(b2);
                allocate.putShort(2048);
                break;
            case 11:
                allocate.put(b);
                allocate.putShort(2303);
                break;
            case 12:
                allocate.put(b2);
                allocate.putShort(2304);
                break;
            case 13:
                allocate.put(b2);
                allocate.putShort(2560);
                break;
            case 14:
                allocate.put(b);
                allocate.putShort(2815);
                break;
            case 15:
                allocate.put(b);
                allocate.putShort(3071);
                break;
            case 16:
                allocate.put(b2);
                allocate.putShort(3072);
                break;
            case 17:
                allocate.put(b);
                allocate.putShort(3327);
                break;
            case 18:
                allocate.put(b2);
                allocate.putShort(3584);
                break;
        }
        byte[] array = allocate.array();
        pq7.b(array, "buffer.array()");
        return array;
    }

    @DexIgnore
    public final void setBackgroundRequestType(BackgroundRequestType backgroundRequestType2) {
        pq7.c(backgroundRequestType2, "<set-?>");
        this.backgroundRequestType = backgroundRequestType2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.backgroundRequestType.ordinal());
    }
}
