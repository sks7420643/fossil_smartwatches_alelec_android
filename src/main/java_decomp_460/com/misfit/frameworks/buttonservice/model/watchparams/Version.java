package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Version implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("major")
    public /* final */ int major;
    @DexIgnore
    @rj4("minor")
    public /* final */ int minor;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Version> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Version createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new Version(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Version[] newArray(int i) {
            return new Version[i];
        }
    }

    @DexIgnore
    public Version(int i, int i2) {
        this.major = i;
        this.minor = i2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Version(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt());
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: IGET  (r1v0 int) = (r2v0 'this' com.misfit.frameworks.buttonservice.model.watchparams.Version A[IMMUTABLE_TYPE, THIS]) com.misfit.frameworks.buttonservice.model.watchparams.Version.major int), ('.' char), (wrap: int : 0x000f: IGET  (r1v2 int) = (r2v0 'this' com.misfit.frameworks.buttonservice.model.watchparams.Version A[IMMUTABLE_TYPE, THIS]) com.misfit.frameworks.buttonservice.model.watchparams.Version.minor int)] */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeInt(this.major);
        parcel.writeInt(this.minor);
    }
}
