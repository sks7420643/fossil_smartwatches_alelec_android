package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pm7;
import com.fossil.pq7;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public Background background;
    @DexIgnore
    public ArrayList<MovingObject> movingObjects;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ThemeData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ThemeData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ThemeData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ThemeData[] newArray(int i) {
            return new ThemeData[i];
        }
    }

    @DexIgnore
    public ThemeData() {
        this(null, null, 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ThemeData(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r4, r0)
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.Background> r0 = com.misfit.frameworks.buttonservice.model.watchface.Background.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r4.readParcelable(r0)
            com.misfit.frameworks.buttonservice.model.watchface.Background r0 = (com.misfit.frameworks.buttonservice.model.watchface.Background) r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.MovingObject> r2 = com.misfit.frameworks.buttonservice.model.watchface.MovingObject.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r4.readList(r1, r2)
            r3.<init>(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.ThemeData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ThemeData(Background background2, ArrayList<MovingObject> arrayList) {
        pq7.c(arrayList, "movingObjects");
        this.background = background2;
        this.movingObjects = arrayList;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ThemeData(Background background2, ArrayList arrayList, int i, kq7 kq7) {
        this((i & 1) != 0 ? null : background2, (i & 2) != 0 ? new ArrayList() : arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.misfit.frameworks.buttonservice.model.watchface.ThemeData */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ThemeData copy$default(ThemeData themeData, Background background2, ArrayList arrayList, int i, Object obj) {
        if ((i & 1) != 0) {
            background2 = themeData.background;
        }
        if ((i & 2) != 0) {
            arrayList = themeData.movingObjects;
        }
        return themeData.copy(background2, arrayList);
    }

    @DexIgnore
    public final Background component1() {
        return this.background;
    }

    @DexIgnore
    public final ArrayList<MovingObject> component2() {
        return this.movingObjects;
    }

    @DexIgnore
    public final ThemeData copy(Background background2, ArrayList<MovingObject> arrayList) {
        pq7.c(arrayList, "movingObjects");
        return new ThemeData(background2, arrayList);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThemeData) {
                ThemeData themeData = (ThemeData) obj;
                if (!pq7.a(this.background, themeData.background) || !pq7.a(this.movingObjects, themeData.movingObjects)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Background getBackground() {
        return this.background;
    }

    @DexIgnore
    public final ArrayList<MovingObject> getMovingObjects() {
        return this.movingObjects;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Background background2 = this.background;
        int hashCode = background2 != null ? background2.hashCode() : 0;
        ArrayList<MovingObject> arrayList = this.movingObjects;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setBackground(Background background2) {
        this.background = background2;
    }

    @DexIgnore
    public final void setMovingObjects(ArrayList<MovingObject> arrayList) {
        pq7.c(arrayList, "<set-?>");
        this.movingObjects = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "ThemeData(background=" + this.background + ", movingObjects=" + this.movingObjects + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeParcelable(this.background, i);
        parcel.writeList(pm7.h0(this.movingObjects));
    }
}
