package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.nm1;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class UserBiometricData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ int age;
    @DexIgnore
    public /* final */ BiometricGender gender;
    @DexIgnore
    public float heightInMeter;
    @DexIgnore
    public /* final */ BiometricWearingPosition wearingPosition;
    @DexIgnore
    public float weightInKilogram;

    @DexIgnore
    public enum BiometricGender {
        UNSPECIFIED(0),
        MALE(1),
        FEMALE(2);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final BiometricGender fromValue(int i) {
                BiometricGender biometricGender;
                BiometricGender[] values = BiometricGender.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        biometricGender = null;
                        break;
                    }
                    biometricGender = values[i2];
                    if (biometricGender.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return biometricGender != null ? biometricGender : BiometricGender.UNSPECIFIED;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[BiometricGender.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[BiometricGender.MALE.ordinal()] = 1;
                $EnumSwitchMapping$0[BiometricGender.FEMALE.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public BiometricGender(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public final nm1.a toSDKBiometricProfileData() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            return i != 1 ? i != 2 ? nm1.a.UNSPECIFIED : nm1.a.FEMALE : nm1.a.MALE;
        }
    }

    @DexIgnore
    public enum BiometricWearingPosition {
        UNSPECIFIED(0),
        LEFT_WRIST(1),
        RIGHT_WRIST(2),
        UNSPECIFIED_WRIST(3);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(kq7 kq7) {
                this();
            }

            @DexIgnore
            public final BiometricWearingPosition fromValue(int i) {
                BiometricWearingPosition biometricWearingPosition;
                BiometricWearingPosition[] values = BiometricWearingPosition.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        biometricWearingPosition = null;
                        break;
                    }
                    biometricWearingPosition = values[i2];
                    if (biometricWearingPosition.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return biometricWearingPosition != null ? biometricWearingPosition : BiometricWearingPosition.UNSPECIFIED;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[BiometricWearingPosition.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[BiometricWearingPosition.LEFT_WRIST.ordinal()] = 1;
                $EnumSwitchMapping$0[BiometricWearingPosition.RIGHT_WRIST.ordinal()] = 2;
                $EnumSwitchMapping$0[BiometricWearingPosition.UNSPECIFIED_WRIST.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public BiometricWearingPosition(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public final nm1.b toSDKBiometricProfileData() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            return i != 1 ? i != 2 ? i != 3 ? nm1.b.UNSPECIFIED : nm1.b.UNSPECIFIED_WRIST : nm1.b.RIGHT_WRIST : nm1.b.LEFT_WRIST;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<UserBiometricData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserBiometricData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new UserBiometricData(parcel);
        }

        @DexIgnore
        public final boolean isSame(UserBiometricData userBiometricData, UserBiometricData userBiometricData2) {
            if ((userBiometricData != null || userBiometricData2 == null) && (userBiometricData == null || userBiometricData2 != null)) {
                return pq7.a(userBiometricData, userBiometricData2);
            }
            return false;
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserBiometricData[] newArray(int i) {
            return new UserBiometricData[i];
        }
    }

    @DexIgnore
    public UserBiometricData() {
        String name = UserBiometricData.class.getName();
        pq7.b(name, "UserBiometricData::class.java.name");
        this.TAG = name;
        this.age = 20;
        this.gender = BiometricGender.MALE;
        this.heightInMeter = 1.7f;
        this.weightInKilogram = 60.0f;
        this.wearingPosition = BiometricWearingPosition.LEFT_WRIST;
    }

    @DexIgnore
    public UserBiometricData(int i, BiometricGender biometricGender, float f, float f2, BiometricWearingPosition biometricWearingPosition) {
        pq7.c(biometricGender, "gender");
        pq7.c(biometricWearingPosition, "wearingPosition");
        String name = UserBiometricData.class.getName();
        pq7.b(name, "UserBiometricData::class.java.name");
        this.TAG = name;
        this.age = i;
        this.gender = biometricGender;
        this.heightInMeter = f;
        this.weightInKilogram = f2;
        this.wearingPosition = biometricWearingPosition;
    }

    @DexIgnore
    public UserBiometricData(Parcel parcel) {
        pq7.c(parcel, "parcel");
        String name = UserBiometricData.class.getName();
        pq7.b(name, "UserBiometricData::class.java.name");
        this.TAG = name;
        this.age = parcel.readInt();
        this.gender = BiometricGender.Companion.fromValue(parcel.readInt());
        this.heightInMeter = parcel.readFloat();
        this.weightInKilogram = parcel.readFloat();
        this.wearingPosition = BiometricWearingPosition.Companion.fromValue(parcel.readInt());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof UserBiometricData)) {
            return false;
        }
        UserBiometricData userBiometricData = (UserBiometricData) obj;
        return this.age == userBiometricData.age && this.gender == userBiometricData.gender && this.heightInMeter == userBiometricData.heightInMeter && this.weightInKilogram == userBiometricData.weightInKilogram && this.wearingPosition == userBiometricData.wearingPosition;
    }

    @DexIgnore
    public final int getAge() {
        return this.age;
    }

    @DexIgnore
    public final BiometricGender getGender() {
        return this.gender;
    }

    @DexIgnore
    public final float getHeightInMeter() {
        return this.heightInMeter;
    }

    @DexIgnore
    public final BiometricWearingPosition getWearingPosition() {
        return this.wearingPosition;
    }

    @DexIgnore
    public final float getWeightInKilogram() {
        return this.weightInKilogram;
    }

    @DexIgnore
    public final void setHeightInMeter(float f) {
        this.heightInMeter = f;
    }

    @DexIgnore
    public final void setWeightInKilogram(float f) {
        this.weightInKilogram = f;
    }

    @DexIgnore
    public final nm1 toSDKBiometricProfile() {
        float f = this.heightInMeter;
        if (f < 1.0f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, "UserBiometric Height is out of range, reset Height from " + this.heightInMeter + " to 1.0");
            this.heightInMeter = 1.0f;
        } else if (f > 2.5f) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.d(str2, "UserBiometric Height is out of range, reset Height from " + this.heightInMeter + " to 2.5");
            this.heightInMeter = 2.5f;
        }
        float f2 = this.weightInKilogram;
        if (f2 < 35.0f) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.d(str3, "UserBiometric Weight is out of range, reset Weight from " + this.weightInKilogram + " to 35.0");
            this.weightInKilogram = 35.0f;
        } else if (f2 > 250.0f) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = this.TAG;
            local4.d(str4, "UserBiometric Weight is out of range, reset Weight from " + this.weightInKilogram + " to 250.0");
            this.weightInKilogram = 250.0f;
        }
        return new nm1((byte) this.age, this.gender.toSDKBiometricProfileData(), (short) ((int) (this.heightInMeter * ((float) 100))), (short) ((int) this.weightInKilogram), this.wearingPosition.toSDKBiometricProfileData());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeInt(this.age);
        parcel.writeInt(this.gender.getValue());
        parcel.writeFloat(this.heightInMeter);
        parcel.writeFloat(this.weightInKilogram);
        parcel.writeInt(this.wearingPosition.getValue());
    }
}
