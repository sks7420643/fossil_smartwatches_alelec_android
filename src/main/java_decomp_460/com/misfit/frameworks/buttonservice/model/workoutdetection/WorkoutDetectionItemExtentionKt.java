package com.misfit.frameworks.buttonservice.model.workoutdetection;

import com.fossil.il7;
import com.fossil.im7;
import com.fossil.in1;
import com.fossil.kn1;
import com.fossil.pq7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionItemExtentionKt {
    @DexIgnore
    public static final kn1[] toAutoWorkoutDetectionItemConfig(List<WorkoutDetectionItem> list) {
        pq7.c(list, "$this$toAutoWorkoutDetectionItemConfig");
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        for (T t : list) {
            arrayList.add(new kn1(t.getType(), t.isEnable(), t.getUserConfirmation(), new in1((short) t.getStartLatency(), (short) t.getPauseLatency(), (short) t.getResumeLatency(), (short) t.getStopLatency())));
        }
        Object[] array = arrayList.toArray(new kn1[0]);
        if (array != null) {
            return (kn1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
