package com.misfit.frameworks.buttonservice.model.vibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.hn1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VibrationStrengthObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<VibrationStrengthObj> CREATOR; // = new VibrationStrengthObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ boolean isDefaultValue;
    @DexIgnore
    public /* final */ int vibrationStrengthLevel;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[hn1.a.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[hn1.a.LOW.ordinal()] = 1;
                $EnumSwitchMapping$0[hn1.a.MEDIUM.ordinal()] = 2;
                $EnumSwitchMapping$0[hn1.a.HIGH.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ VibrationStrengthObj consumeSDKVibrationStrengthLevel$default(Companion companion, hn1.a aVar, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return companion.consumeSDKVibrationStrengthLevel(aVar, z);
        }

        @DexIgnore
        public final VibrationStrengthObj consumeSDKVibrationStrengthLevel(hn1.a aVar, boolean z) {
            int i = 3;
            pq7.c(aVar, "vibrationStrengthLevel");
            int i2 = WhenMappings.$EnumSwitchMapping$0[aVar.ordinal()];
            if (i2 == 1) {
                i = 1;
            } else if (i2 == 2 || i2 != 3) {
                i = 2;
            }
            return new VibrationStrengthObj(i, z);
        }
    }

    @DexIgnore
    public VibrationStrengthObj(int i, boolean z) {
        this.vibrationStrengthLevel = i;
        this.isDefaultValue = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ VibrationStrengthObj(int i, boolean z, int i2, kq7 kq7) {
        this(i, (i2 & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public VibrationStrengthObj(android.os.Parcel r4) {
        /*
            r3 = this;
            r0 = 1
            java.lang.String r1 = "parcel"
            com.fossil.pq7.c(r4, r1)
            int r1 = r4.readInt()
            int r2 = r4.readInt()
            if (r2 != r0) goto L_0x0014
        L_0x0010:
            r3.<init>(r1, r0)
            return
        L_0x0014:
            r0 = 0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getVibrationStrengthLevel() {
        return this.vibrationStrengthLevel;
    }

    @DexIgnore
    public final boolean isDefaultValue() {
        return this.isDefaultValue;
    }

    @DexIgnore
    public final hn1.a toSDKVibrationStrengthLevel() {
        int i = this.vibrationStrengthLevel;
        return i != 1 ? i != 2 ? i != 3 ? hn1.a.MEDIUM : hn1.a.HIGH : hn1.a.MEDIUM : hn1.a.LOW;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(VibrationStrengthObj.class.getName());
        parcel.writeInt(this.vibrationStrengthLevel);
        parcel.writeInt(this.isDefaultValue ? 1 : 0);
    }
}
