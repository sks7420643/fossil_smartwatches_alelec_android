package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.dm1;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ComplicationAppMapping implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<ComplicationAppMapping> CREATOR; // = new ComplicationAppMapping$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FIELD_TYPE; // = "mType";
    @DexIgnore
    public int mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String getFIELD_TYPE() {
            return ComplicationAppMapping.FIELD_TYPE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ComplicationAppMappingType {
        @DexIgnore
        public static /* final */ int ACTIVE_MINUTES; // = 9;
        @DexIgnore
        public static /* final */ int BATTERY; // = 11;
        @DexIgnore
        public static /* final */ int CALORIES; // = 10;
        @DexIgnore
        public static /* final */ int CHANCE_OF_RAIN; // = 7;
        @DexIgnore
        public static /* final */ int COMMUTE_TIME_TYPE; // = 2;
        @DexIgnore
        public static /* final */ int DATE_TYPE; // = 5;
        @DexIgnore
        public static /* final */ int EMPTY_TYPE; // = 6;
        @DexIgnore
        public static /* final */ int HEART_RATE_TYPE; // = 3;
        @DexIgnore
        public static /* final */ ComplicationAppMappingType INSTANCE; // = new ComplicationAppMappingType();
        @DexIgnore
        public static /* final */ int STEPS_TYPE; // = 4;
        @DexIgnore
        public static /* final */ int TIMEZONE_2_TYPE; // = 8;
        @DexIgnore
        public static /* final */ int WEATHER_TYPE; // = 1;

        @DexIgnore
        public final int getACTIVE_MINUTES() {
            return ACTIVE_MINUTES;
        }

        @DexIgnore
        public final int getBATTERY() {
            return BATTERY;
        }

        @DexIgnore
        public final int getCALORIES() {
            return CALORIES;
        }

        @DexIgnore
        public final int getCHANCE_OF_RAIN() {
            return CHANCE_OF_RAIN;
        }

        @DexIgnore
        public final int getCOMMUTE_TIME_TYPE() {
            return COMMUTE_TIME_TYPE;
        }

        @DexIgnore
        public final int getDATE_TYPE() {
            return DATE_TYPE;
        }

        @DexIgnore
        public final int getEMPTY_TYPE() {
            return EMPTY_TYPE;
        }

        @DexIgnore
        public final int getHEART_RATE_TYPE() {
            return HEART_RATE_TYPE;
        }

        @DexIgnore
        public final int getSTEPS_TYPE() {
            return STEPS_TYPE;
        }

        @DexIgnore
        public final int getTIMEZONE_2_TYPE() {
            return TIMEZONE_2_TYPE;
        }

        @DexIgnore
        public final int getWEATHER_TYPE() {
            return WEATHER_TYPE;
        }
    }

    @DexIgnore
    public ComplicationAppMapping() {
        this.mType = ComplicationAppMappingType.INSTANCE.getCOMMUTE_TIME_TYPE();
    }

    @DexIgnore
    public ComplicationAppMapping(int i) {
        this.mType = i;
    }

    @DexIgnore
    public ComplicationAppMapping(Parcel parcel) {
        pq7.c(parcel, "parcel");
        this.mType = parcel.readInt();
    }

    @DexIgnore
    public static /* synthetic */ dm1 toSDKSetting$default(ComplicationAppMapping complicationAppMapping, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            return complicationAppMapping.toSDKSetting(z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toSDKSetting");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ComplicationAppMapping)) {
            return false;
        }
        return pq7.a(getHash(), ((ComplicationAppMapping) obj).getHash());
    }

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final int getMType() {
        return this.mType;
    }

    @DexIgnore
    public final void setMType(int i) {
        this.mType = i;
    }

    @DexIgnore
    public abstract dm1 toSDKSetting(boolean z);

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.mType);
    }
}
