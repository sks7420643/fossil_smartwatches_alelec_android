package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.pu1;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalizationData implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<LocalizationData> CREATOR; // = new LocalizationData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String filePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        private final String bytesToString(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bArr) {
                String num = Integer.toString((b & 255) + 256, 16);
                pq7.b(num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    pq7.b(substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            pq7.b(sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLocalizationDataCheckSum(LocalizationData localizationData) {
            pq7.c(localizationData, "localizationData");
            return pq7.a(localizationData.getCheckSum(), "") ? bytesToString(localizationData.toLocalizationFile().getData()) : localizationData.getCheckSum();
        }

        @DexIgnore
        public final boolean isTheSameFile(LocalizationData localizationData, LocalizationData localizationData2) {
            pq7.c(localizationData2, "newLocalizationData");
            if (localizationData == null) {
                return false;
            }
            return pq7.a(localizationData.getFilePath(), localizationData2.getFilePath()) && pq7.a(localizationData.getCheckSum(), getLocalizationDataCheckSum(localizationData2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LocalizationData(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r4, r0)
            java.lang.String r0 = r4.readString()
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x001a
        L_0x000d:
            java.lang.String r1 = r4.readString()
            if (r1 == 0) goto L_0x001d
        L_0x0013:
            r3.<init>(r0, r1)
            r3.initialize()
            return
        L_0x001a:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x001d:
            r1 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.LocalizationData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public LocalizationData(String str, String str2) {
        pq7.c(str, "filePath");
        pq7.c(str2, "checkSum");
        this.filePath = str;
        this.checkSum = str2;
        initialize();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ LocalizationData(String str, String str2, int i, kq7 kq7) {
        this(str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getFilePath() {
        return this.filePath;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        com.fossil.so7.a(r1, r0);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void initialize() {
        /*
            r3 = this;
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x001b }
            java.lang.String r0 = r3.filePath     // Catch:{ Exception -> 0x001b }
            r1.<init>(r0)     // Catch:{ Exception -> 0x001b }
            byte[] r0 = com.fossil.ro7.c(r1)     // Catch:{ all -> 0x0014 }
            r3.data = r0     // Catch:{ all -> 0x0014 }
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x0014 }
            r0 = 0
            com.fossil.so7.a(r1, r0)
        L_0x0013:
            return
        L_0x0014:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0016 }
        L_0x0016:
            r2 = move-exception
            com.fossil.so7.a(r1, r0)
            throw r2
        L_0x001b:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            r0.printStackTrace()
            java.lang.String r0 = "LocalizationData"
            com.fossil.tl7 r2 = com.fossil.tl7.f3441a
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.e(r0, r2)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.LocalizationData.initialize():void");
    }

    @DexIgnore
    public final boolean isDataValid() {
        return this.data != null;
    }

    @DexIgnore
    public final void setCheckSum(String str) {
        pq7.c(str, "<set-?>");
        this.checkSum = str;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public final pu1 toLocalizationFile() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new pu1(bArr);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(LocalizationData.class.getName());
        parcel.writeString(this.filePath);
        parcel.writeString(this.checkSum);
    }
}
