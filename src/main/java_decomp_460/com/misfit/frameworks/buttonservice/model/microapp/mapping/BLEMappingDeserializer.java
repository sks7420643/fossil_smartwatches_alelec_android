package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.fossil.zi4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingDeserializer implements dj4<BLEMapping> {
    @DexIgnore
    @Override // com.fossil.dj4
    public BLEMapping deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().p("mType").b();
        zi4 zi4 = new zi4();
        zi4.f(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) zi4.d().g(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) zi4.d().g(jsonElement, MicroAppMapping.class);
    }
}
