package com.misfit.frameworks.buttonservice.model.alarm;

import com.fossil.al7;
import com.fossil.fl1;
import com.fossil.il1;
import com.fossil.im7;
import com.fossil.jl1;
import com.fossil.kl1;
import com.fossil.kq7;
import com.fossil.nl1;
import com.fossil.ol1;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.ql1;
import com.fossil.un1;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmSetting {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HashSet<AlarmDay> alarmDays;
    @DexIgnore
    public /* final */ int hour;
    @DexIgnore
    public /* final */ boolean isRepeat;
    @DexIgnore
    public /* final */ String message;
    @DexIgnore
    public /* final */ int minute;
    @DexIgnore
    public /* final */ String title;

    @DexIgnore
    public enum AlarmDay {
        SUNDAY(1),
        MONDAY(2),
        TUESDAY(3),
        WEDNESDAY(4),
        THURSDAY(5),
        FRIDAY(6),
        SATURDAY(7);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public AlarmDay(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[AlarmDay.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[AlarmDay.SUNDAY.ordinal()] = 1;
            $EnumSwitchMapping$0[AlarmDay.MONDAY.ordinal()] = 2;
            $EnumSwitchMapping$0[AlarmDay.TUESDAY.ordinal()] = 3;
            $EnumSwitchMapping$0[AlarmDay.WEDNESDAY.ordinal()] = 4;
            $EnumSwitchMapping$0[AlarmDay.THURSDAY.ordinal()] = 5;
            $EnumSwitchMapping$0[AlarmDay.FRIDAY.ordinal()] = 6;
            $EnumSwitchMapping$0[AlarmDay.SATURDAY.ordinal()] = 7;
        }
        */
    }

    /*
    static {
        String name = AlarmSetting.class.getName();
        pq7.b(name, "AlarmSetting::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public AlarmSetting(String str, String str2, int i, int i2) {
        this(str, str2, i, i2, false, new HashSet());
        pq7.c(str, "title");
        pq7.c(str2, "message");
    }

    @DexIgnore
    public AlarmSetting(String str, String str2, int i, int i2, boolean z, HashSet<AlarmDay> hashSet) {
        this.title = str;
        this.message = str2;
        this.hour = i;
        this.minute = i2;
        this.isRepeat = z;
        this.alarmDays = hashSet;
    }

    @DexIgnore
    private final un1 convertCalendarDayToSDKV2Day(AlarmDay alarmDay) {
        switch (WhenMappings.$EnumSwitchMapping$0[alarmDay.ordinal()]) {
            case 1:
                return un1.SUNDAY;
            case 2:
                return un1.MONDAY;
            case 3:
                return un1.TUESDAY;
            case 4:
                return un1.WEDNESDAY;
            case 5:
                return un1.THURSDAY;
            case 6:
                return un1.FRIDAY;
            case 7:
                return un1.SATURDAY;
            default:
                throw new al7();
        }
    }

    @DexIgnore
    public final HashSet<AlarmDay> getAlarmDays() {
        return this.alarmDays;
    }

    @DexIgnore
    public final int getAlarmDaysAsInt() {
        int i = 0;
        HashSet<AlarmDay> hashSet = this.alarmDays;
        if (hashSet == null) {
            return 0;
        }
        Iterator<T> it = hashSet.iterator();
        while (it.hasNext()) {
            i = it.next().getValue() | i;
        }
        return i;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final boolean isRepeat() {
        return this.isRepeat;
    }

    @DexIgnore
    public final fl1 toSDKV2Setting() {
        String str = this.title;
        if (str == null) {
            str = "";
        }
        ql1 ql1 = new ql1(str);
        String str2 = this.message;
        if (str2 == null) {
            str2 = "";
        }
        il1 il1 = new il1(str2);
        HashSet<AlarmDay> hashSet = this.alarmDays;
        if (hashSet == null) {
            return new jl1(new kl1((byte) this.hour, (byte) this.minute, null), ql1, il1);
        }
        if (hashSet.isEmpty()) {
            return new jl1(new kl1((byte) this.hour, (byte) this.minute, null), ql1, il1);
        }
        if (this.isRepeat) {
            HashSet<AlarmDay> hashSet2 = this.alarmDays;
            ArrayList arrayList = new ArrayList(im7.m(hashSet2, 10));
            Iterator<T> it = hashSet2.iterator();
            while (it.hasNext()) {
                arrayList.add(convertCalendarDayToSDKV2Day(it.next()));
            }
            return new nl1(new ol1((byte) this.hour, (byte) this.minute, pm7.l0(arrayList)), ql1, il1);
        } else if (this.alarmDays.size() > 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.e(str3, ".toSDKV2Setting(), the alarm setting is wrong format, alarmSetting=" + this);
            return null;
        } else {
            return new jl1(new kl1((byte) this.hour, (byte) this.minute, this.alarmDays.isEmpty() ? null : convertCalendarDayToSDKV2Day((AlarmDay) pm7.h0(this.alarmDays).get(0))), ql1, il1);
        }
    }

    @DexIgnore
    public String toString() {
        return "AlarmSetting{alarmHour=" + this.hour + ", alarmMinute=" + this.minute + ", alarmDays=" + this.alarmDays + ", alarmRepeat=" + this.isRepeat + "}";
    }
}
