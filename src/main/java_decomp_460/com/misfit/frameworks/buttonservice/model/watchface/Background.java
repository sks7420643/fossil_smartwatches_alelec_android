package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Background implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public String id;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Background> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Background createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new Background(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Background[] newArray(int i) {
            return new Background[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Background(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r4, r0)
            byte[] r0 = r4.createByteArray()
            java.lang.String r1 = r4.readString()
            if (r1 == 0) goto L_0x0018
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.pq7.b(r1, r2)
            r3.<init>(r0, r1)
            return
        L_0x0018:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.Background.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Background(byte[] bArr, String str) {
        pq7.c(str, "id");
        this.data = bArr;
        this.id = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Background(byte[] bArr, String str, int i, kq7 kq7) {
        this((i & 1) != 0 ? null : bArr, str);
    }

    @DexIgnore
    public static /* synthetic */ Background copy$default(Background background, byte[] bArr, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            bArr = background.data;
        }
        if ((i & 2) != 0) {
            str = background.id;
        }
        return background.copy(bArr, str);
    }

    @DexIgnore
    public final byte[] component1() {
        return this.data;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final Background copy(byte[] bArr, String str) {
        pq7.c(str, "id");
        return new Background(bArr, str);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Background) {
                Background background = (Background) obj;
                if (!pq7.a(this.data, background.data) || !pq7.a(this.id, background.id)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        byte[] bArr = this.data;
        int hashCode = bArr != null ? Arrays.hashCode(bArr) : 0;
        String str = this.id;
        if (str != null) {
            i = str.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public final void setId(String str) {
        pq7.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public String toString() {
        return "Background(data=" + Arrays.toString(this.data) + ", id=" + this.id + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeByteArray(this.data);
        parcel.writeString(this.id);
    }
}
