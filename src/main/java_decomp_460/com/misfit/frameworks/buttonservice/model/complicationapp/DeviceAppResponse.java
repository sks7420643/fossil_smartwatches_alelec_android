package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.jq1;
import com.fossil.kq7;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeTimeObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceAppResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<DeviceAppResponse> CREATOR; // = new DeviceAppResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ long LIFE_TIME; // = 8000;
    @DexIgnore
    public np1 deviceEventId;
    @DexIgnore
    public boolean isForceUpdate;
    @DexIgnore
    public /* final */ LifeTimeObject lifeTimeObject;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeviceAppResponse(Parcel parcel) {
        this(np1.values()[parcel.readInt()]);
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public DeviceAppResponse(np1 np1) {
        pq7.c(np1, "deviceEventId");
        this.deviceEventId = np1;
        this.lifeTimeObject = new LifeTimeObject(LIFE_TIME);
    }

    @DexIgnore
    public static /* synthetic */ mt1 getSDKDeviceResponse$default(DeviceAppResponse deviceAppResponse, jq1 jq1, ry1 ry1, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                ry1 = null;
            }
            return deviceAppResponse.getSDKDeviceResponse(jq1, ry1);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSDKDeviceResponse");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final np1 getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final LifeTimeObject getLifeTimeObject() {
        return this.lifeTimeObject;
    }

    @DexIgnore
    public abstract mt1 getSDKDeviceData();

    @DexIgnore
    public abstract mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1);

    @DexIgnore
    public final boolean isForceUpdate() {
        return this.isForceUpdate;
    }

    @DexIgnore
    public final void setDeviceEventId(np1 np1) {
        pq7.c(np1, "<set-?>");
        this.deviceEventId = np1;
    }

    @DexIgnore
    public final void setForceUpdate(boolean z) {
        this.isForceUpdate = z;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.deviceEventId.ordinal());
    }
}
