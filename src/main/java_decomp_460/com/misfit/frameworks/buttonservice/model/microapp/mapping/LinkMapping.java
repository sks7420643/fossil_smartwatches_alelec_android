package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import android.os.Parcel;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.Gesture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LinkMapping extends BLEMapping {
    @DexIgnore
    public int action;
    @DexIgnore
    public String extraInfo;
    @DexIgnore
    public Gesture gesture;

    @DexIgnore
    public LinkMapping(Parcel parcel) {
        super(parcel);
        this.gesture = Gesture.fromInt(parcel.readInt());
        this.action = parcel.readInt();
        this.extraInfo = parcel.readString();
    }

    @DexIgnore
    public LinkMapping(Gesture gesture2, int i, String str) {
        super(1);
        this.gesture = gesture2;
        this.action = i;
        this.extraInfo = str;
    }

    @DexIgnore
    public static List<BLEMapping> convertToBLEMapping(List<LinkMapping> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        return arrayList;
    }

    @DexIgnore
    public static List<LinkMapping> convertToLinkMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((LinkMapping) it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public int getAction() {
        return this.action;
    }

    @DexIgnore
    public String getExtraInfo() {
        return this.extraInfo;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public Gesture getGesture() {
        return this.gesture;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public String getHash() {
        return this.mType + ":" + getGesture().getValue() + ":" + this.action + ":" + this.extraInfo;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedHID() {
        return Action.HIDAction.isActionBelongToThisType(this.action);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedStreaming() {
        return Action.StreamingAction.isActionBelongToThisType(this.action);
    }

    @DexIgnore
    public void setAction(int i) {
        this.action = i;
    }

    @DexIgnore
    public void setExtraInfo(String str) {
        this.extraInfo = str;
    }

    @DexIgnore
    public void setGesture(Gesture gesture2) {
        this.gesture = gesture2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.gesture.getValue());
        parcel.writeInt(this.action);
        parcel.writeString(this.extraInfo);
    }
}
