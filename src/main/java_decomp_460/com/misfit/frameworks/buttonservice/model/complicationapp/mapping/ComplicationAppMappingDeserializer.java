package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.fossil.pq7;
import com.fossil.zi4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingDeserializer implements dj4<ComplicationAppMapping> {
    @DexIgnore
    @Override // com.fossil.dj4
    public ComplicationAppMapping deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        pq7.c(jsonElement, "json");
        pq7.c(type, "typeOfT");
        pq7.c(cj4, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = ComplicationAppMappingDeserializer.class.getName();
        pq7.b(name, "ComplicationAppMappingDe\u2026rializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement p = jsonElement.d().p(ComplicationAppMapping.Companion.getFIELD_TYPE());
        pq7.b(p, "jsonType");
        int b = p.b();
        zi4 zi4 = new zi4();
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, WeatherComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getHEART_RATE_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, HeartRateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getSTEPS_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, StepsComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getDATE_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, DateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getEMPTY_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, NoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCHANCE_OF_RAIN()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, ChanceOfRainComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, SecondTimezoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getACTIVE_MINUTES()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, ActiveMinutesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCALORIES()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, CaloriesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getBATTERY()) {
            return (ComplicationAppMapping) zi4.d().g(jsonElement, BatteryComplicationAppMapping.class);
        }
        return null;
    }
}
