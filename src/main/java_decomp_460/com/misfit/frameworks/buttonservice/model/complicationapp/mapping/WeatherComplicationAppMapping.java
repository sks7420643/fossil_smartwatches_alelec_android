package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.dm1;
import com.fossil.lm1;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public WeatherComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE());
    }

    @DexIgnore
    public WeatherComplicationAppMapping(Parcel parcel) {
        super(parcel);
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.complicationapp.mapping.WeatherComplicationAppMapping A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping.getMType():int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        pq7.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public dm1 toSDKSetting(boolean z) {
        return new lm1();
    }
}
