package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import com.fossil.il7;
import com.fossil.ko1;
import com.fossil.mo1;
import com.fossil.pq7;
import com.fossil.zn1;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridNotificationObj extends NotificationBaseObj {
    @DexIgnore
    public FNotification fNotification;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HybridNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, FNotification fNotification2, String str, String str2, int i2, String str3, List<NotificationBaseObj.ANotificationFlag> list) {
        this(fNotification2);
        pq7.c(aNotificationType, "notificationType");
        pq7.c(fNotification2, "fNotification");
        pq7.c(str, "title");
        pq7.c(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        pq7.c(str3, "message");
        pq7.c(list, "notificationFlags");
        setUid(i);
        setNotificationType(aNotificationType);
        setTitle(str);
        setSender(str2);
        setMessage(str3);
        setSenderId(i2);
        setNotificationFlags(list);
    }

    @DexIgnore
    public HybridNotificationObj(Parcel parcel) {
        super(parcel);
        setUid(parcel.readInt());
        setNotificationType(NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        String readString = parcel.readString();
        setTitle(readString == null ? "" : readString);
        String readString2 = parcel.readString();
        setSender(readString2 == null ? "" : readString2);
        String readString3 = parcel.readString();
        setMessage(readString3 == null ? "" : readString3);
        setNotificationFlags(new ArrayList());
        ArrayList<Number> arrayList = new ArrayList();
        parcel.readList(arrayList, null);
        for (Number number : arrayList) {
            getNotificationFlags().add(NotificationBaseObj.ANotificationFlag.values()[number.intValue()]);
        }
    }

    @DexIgnore
    public HybridNotificationObj(FNotification fNotification2) {
        pq7.c(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public String toRemoteLogString() {
        return "UID=" + getUid() + ", fNotification=" + this.fNotification + ", flag=" + getNotificationFlags();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public zn1 toSDKNotification() {
        mo1 sDKNotificationType = getNotificationType().toSDKNotificationType();
        int uid = getUid();
        String packageName = this.fNotification.getPackageName();
        String title = getTitle();
        String sender = getSender();
        int senderId = getSenderId();
        String message = getMessage();
        Object[] array = toSDKNotificationFlags(getNotificationFlags()).toArray(new ko1[0]);
        if (array != null) {
            return new zn1(sDKNotificationType, uid, packageName, title, sender, senderId, message, (ko1[]) array, System.currentTimeMillis());
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(getUid());
        parcel.writeInt(getNotificationType().ordinal());
        parcel.writeParcelable(this.fNotification, 0);
        parcel.writeString(getTitle());
        parcel.writeString(getSender());
        parcel.writeString(getMessage());
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = getNotificationFlags().iterator();
        while (it.hasNext()) {
            arrayList.add(Integer.valueOf(it.next().ordinal()));
        }
        parcel.writeList(arrayList);
    }
}
