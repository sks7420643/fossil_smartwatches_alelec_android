package com.misfit.frameworks.buttonservice.model.calibration;

import com.fossil.al7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.tl1;
import com.fossil.vl1;
import com.fossil.wl1;
import com.fossil.xl1;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCalibrationObj {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ tl1 handId;
    @DexIgnore
    public /* final */ vl1 handMovingDirection;
    @DexIgnore
    public /* final */ wl1 handMovingSpeed;
    @DexIgnore
    public /* final */ xl1 handMovingType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$2;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$3;

            /*
            static {
                int[] iArr = new int[CalibrationEnums.MovingType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[CalibrationEnums.MovingType.DISTANCE.ordinal()] = 1;
                $EnumSwitchMapping$0[CalibrationEnums.MovingType.POSITION.ordinal()] = 2;
                int[] iArr2 = new int[CalibrationEnums.HandId.values().length];
                $EnumSwitchMapping$1 = iArr2;
                iArr2[CalibrationEnums.HandId.HOUR.ordinal()] = 1;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.MINUTE.ordinal()] = 2;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.SUB_EYE.ordinal()] = 3;
                int[] iArr3 = new int[CalibrationEnums.Direction.values().length];
                $EnumSwitchMapping$2 = iArr3;
                iArr3[CalibrationEnums.Direction.CLOCKWISE.ordinal()] = 1;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.COUNTER_CLOCKWISE.ordinal()] = 2;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.SHORTEST_PATH.ordinal()] = 3;
                int[] iArr4 = new int[CalibrationEnums.Speed.values().length];
                $EnumSwitchMapping$3 = iArr4;
                iArr4[CalibrationEnums.Speed.SIXTEENTH.ordinal()] = 1;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.EIGHTH.ordinal()] = 2;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.QUARTER.ordinal()] = 3;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.HALF.ordinal()] = 4;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.FULL.ordinal()] = 5;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final DianaCalibrationObj consume(HandCalibrationObj handCalibrationObj) {
            xl1 xl1;
            tl1 tl1;
            vl1 vl1;
            wl1 wl1;
            pq7.c(handCalibrationObj, "handCalibrationObj");
            int i = WhenMappings.$EnumSwitchMapping$0[handCalibrationObj.getMovingType().ordinal()];
            if (i == 1) {
                xl1 = xl1.DISTANCE;
            } else if (i == 2) {
                xl1 = xl1.POSITION;
            } else {
                throw new al7();
            }
            int i2 = WhenMappings.$EnumSwitchMapping$1[handCalibrationObj.getHandId().ordinal()];
            if (i2 == 1) {
                tl1 = tl1.HOUR;
            } else if (i2 == 2) {
                tl1 = tl1.MINUTE;
            } else if (i2 == 3) {
                tl1 = tl1.SUB_EYE;
            } else {
                throw new al7();
            }
            int i3 = WhenMappings.$EnumSwitchMapping$2[handCalibrationObj.getDirection().ordinal()];
            if (i3 == 1) {
                vl1 = vl1.CLOCKWISE;
            } else if (i3 == 2) {
                vl1 = vl1.COUNTER_CLOCKWISE;
            } else if (i3 == 3) {
                vl1 = vl1.SHORTEST_PATH;
            } else {
                throw new al7();
            }
            int i4 = WhenMappings.$EnumSwitchMapping$3[handCalibrationObj.getSpeed().ordinal()];
            if (i4 == 1) {
                wl1 = wl1.SIXTEENTH;
            } else if (i4 == 2) {
                wl1 = wl1.EIGHTH;
            } else if (i4 == 3) {
                wl1 = wl1.QUARTER;
            } else if (i4 == 4) {
                wl1 = wl1.HALF;
            } else if (i4 == 5) {
                wl1 = wl1.FULL;
            } else {
                throw new al7();
            }
            return new DianaCalibrationObj(xl1, tl1, vl1, wl1, handCalibrationObj.getDegree());
        }
    }

    @DexIgnore
    public DianaCalibrationObj(xl1 xl1, tl1 tl1, vl1 vl1, wl1 wl1, int i) {
        pq7.c(xl1, "handMovingType");
        pq7.c(tl1, "handId");
        pq7.c(vl1, "handMovingDirection");
        pq7.c(wl1, "handMovingSpeed");
        this.handMovingType = xl1;
        this.handId = tl1;
        this.handMovingDirection = vl1;
        this.handMovingSpeed = wl1;
        this.degree = i;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final tl1 getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final vl1 getHandMovingDirection() {
        return this.handMovingDirection;
    }

    @DexIgnore
    public final wl1 getHandMovingSpeed() {
        return this.handMovingSpeed;
    }

    @DexIgnore
    public final xl1 getHandMovingType() {
        return this.handMovingType;
    }
}
