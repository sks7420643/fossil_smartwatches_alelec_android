package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TextData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public ThemeColour colour;
    @DexIgnore
    public String font;
    @DexIgnore
    public float fontScaled;
    @DexIgnore
    public String text;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<TextData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TextData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new TextData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TextData[] newArray(int i) {
            return new TextData[i];
        }
    }

    @DexIgnore
    public TextData() {
        this(null, null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 15, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TextData(android.os.Parcel r6) {
        /*
            r5 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r6, r0)
            java.lang.String r0 = r6.readString()
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x0025
        L_0x000d:
            java.lang.String r1 = r6.readString()
            if (r1 == 0) goto L_0x0028
        L_0x0013:
            float r4 = r6.readFloat()
            java.lang.String r2 = r6.readString()
            if (r2 == 0) goto L_0x002b
        L_0x001d:
            com.misfit.frameworks.buttonservice.model.watchface.ThemeColour r2 = com.misfit.frameworks.buttonservice.model.watchface.ThemeColour.valueOf(r2)
            r5.<init>(r0, r1, r4, r2)
            return
        L_0x0025:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x0028:
            java.lang.String r1 = ""
            goto L_0x0013
        L_0x002b:
            r2 = r3
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.TextData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public TextData(String str, String str2, float f, ThemeColour themeColour) {
        pq7.c(str, "text");
        pq7.c(str2, "font");
        pq7.c(themeColour, "colour");
        this.text = str;
        this.font = str2;
        this.fontScaled = f;
        this.colour = themeColour;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TextData(String str, String str2, float f, ThemeColour themeColour, int i, kq7 kq7) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f, (i & 8) != 0 ? ThemeColour.DEFAULT : themeColour);
    }

    @DexIgnore
    public static /* synthetic */ TextData copy$default(TextData textData, String str, String str2, float f, ThemeColour themeColour, int i, Object obj) {
        if ((i & 1) != 0) {
            str = textData.text;
        }
        if ((i & 2) != 0) {
            str2 = textData.font;
        }
        if ((i & 4) != 0) {
            f = textData.fontScaled;
        }
        if ((i & 8) != 0) {
            themeColour = textData.colour;
        }
        return textData.copy(str, str2, f, themeColour);
    }

    @DexIgnore
    public final String component1() {
        return this.text;
    }

    @DexIgnore
    public final String component2() {
        return this.font;
    }

    @DexIgnore
    public final float component3() {
        return this.fontScaled;
    }

    @DexIgnore
    public final ThemeColour component4() {
        return this.colour;
    }

    @DexIgnore
    public final TextData copy(String str, String str2, float f, ThemeColour themeColour) {
        pq7.c(str, "text");
        pq7.c(str2, "font");
        pq7.c(themeColour, "colour");
        return new TextData(str, str2, f, themeColour);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof TextData) {
                TextData textData = (TextData) obj;
                if (!pq7.a(this.text, textData.text) || !pq7.a(this.font, textData.font) || Float.compare(this.fontScaled, textData.fontScaled) != 0 || !pq7.a(this.colour, textData.colour)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ThemeColour getColour() {
        return this.colour;
    }

    @DexIgnore
    public final String getFont() {
        return this.font;
    }

    @DexIgnore
    public final float getFontScaled() {
        return this.fontScaled;
    }

    @DexIgnore
    public final String getText() {
        return this.text;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.text;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.font;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        int floatToIntBits = Float.floatToIntBits(this.fontScaled);
        ThemeColour themeColour = this.colour;
        if (themeColour != null) {
            i = themeColour.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + floatToIntBits) * 31) + i;
    }

    @DexIgnore
    public final void setColour(ThemeColour themeColour) {
        pq7.c(themeColour, "<set-?>");
        this.colour = themeColour;
    }

    @DexIgnore
    public final void setFont(String str) {
        pq7.c(str, "<set-?>");
        this.font = str;
    }

    @DexIgnore
    public final void setFontScaled(float f) {
        this.fontScaled = f;
    }

    @DexIgnore
    public final void setText(String str) {
        pq7.c(str, "<set-?>");
        this.text = str;
    }

    @DexIgnore
    public String toString() {
        return "TextData(text=" + this.text + ", font=" + this.font + ", fontScaled=" + this.fontScaled + ", colour=" + this.colour + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.text);
        parcel.writeString(this.font);
        parcel.writeFloat(this.fontScaled);
        parcel.writeString(this.colour.name());
    }
}
