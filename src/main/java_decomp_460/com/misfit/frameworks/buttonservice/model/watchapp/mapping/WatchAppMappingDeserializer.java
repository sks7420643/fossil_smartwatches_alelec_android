package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.fossil.pq7;
import com.fossil.zi4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingDeserializer implements dj4<WatchAppMapping> {
    @DexIgnore
    @Override // com.fossil.dj4
    public WatchAppMapping deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        pq7.c(jsonElement, "json");
        pq7.c(type, "typeOfT");
        pq7.c(cj4, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = WatchAppMappingDeserializer.class.getName();
        pq7.b(name, "WatchAppMappingDeserializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement p = jsonElement.d().p(WatchAppMapping.Companion.getFIELD_TYPE());
        pq7.b(p, "jsonType");
        int b = p.b();
        zi4 zi4 = new zi4();
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, DiagnosticsWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WellnessWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WorkoutWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, NoneWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getMUSIC()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, MusicWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getNOTIFICATION_PANEL()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, NotificationPanelWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, StopWatchWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, TimerWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WeatherWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, CommuteTimeWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getBUDDY_CHALLENGE()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, BuddyChallengeMapping.class);
        }
        return null;
    }
}
