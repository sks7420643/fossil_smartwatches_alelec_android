package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetricObject implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float scaledHeight;
    @DexIgnore
    public float scaledWidth;
    @DexIgnore
    public float scaledX;
    @DexIgnore
    public float scaledY;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<MetricObject> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MetricObject createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new MetricObject(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MetricObject[] newArray(int i) {
            return new MetricObject[i];
        }
    }

    @DexIgnore
    public MetricObject() {
        this(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 15, null);
    }

    @DexIgnore
    public MetricObject(float f, float f2, float f3, float f4) {
        this.scaledX = f;
        this.scaledY = f2;
        this.scaledWidth = f3;
        this.scaledHeight = f4;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MetricObject(float f, float f2, float f3, float f4, int i, kq7 kq7) {
        this((i & 1) != 0 ? 0.0f : f, (i & 2) != 0 ? 0.0f : f2, (i & 4) != 0 ? 0.0f : f3, (i & 8) != 0 ? 0.0f : f4);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MetricObject(Parcel parcel) {
        this(parcel.readFloat(), parcel.readFloat(), parcel.readFloat(), parcel.readFloat());
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public static /* synthetic */ MetricObject copy$default(MetricObject metricObject, float f, float f2, float f3, float f4, int i, Object obj) {
        if ((i & 1) != 0) {
            f = metricObject.scaledX;
        }
        if ((i & 2) != 0) {
            f2 = metricObject.scaledY;
        }
        if ((i & 4) != 0) {
            f3 = metricObject.scaledWidth;
        }
        if ((i & 8) != 0) {
            f4 = metricObject.scaledHeight;
        }
        return metricObject.copy(f, f2, f3, f4);
    }

    @DexIgnore
    public final float component1() {
        return this.scaledX;
    }

    @DexIgnore
    public final float component2() {
        return this.scaledY;
    }

    @DexIgnore
    public final float component3() {
        return this.scaledWidth;
    }

    @DexIgnore
    public final float component4() {
        return this.scaledHeight;
    }

    @DexIgnore
    public final MetricObject copy(float f, float f2, float f3, float f4) {
        return new MetricObject(f, f2, f3, f4);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MetricObject) {
                MetricObject metricObject = (MetricObject) obj;
                if (!(Float.compare(this.scaledX, metricObject.scaledX) == 0 && Float.compare(this.scaledY, metricObject.scaledY) == 0 && Float.compare(this.scaledWidth, metricObject.scaledWidth) == 0 && Float.compare(this.scaledHeight, metricObject.scaledHeight) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getScaledHeight() {
        return this.scaledHeight;
    }

    @DexIgnore
    public final float getScaledWidth() {
        return this.scaledWidth;
    }

    @DexIgnore
    public final float getScaledX() {
        return this.scaledX;
    }

    @DexIgnore
    public final float getScaledY() {
        return this.scaledY;
    }

    @DexIgnore
    public int hashCode() {
        return (((((Float.floatToIntBits(this.scaledX) * 31) + Float.floatToIntBits(this.scaledY)) * 31) + Float.floatToIntBits(this.scaledWidth)) * 31) + Float.floatToIntBits(this.scaledHeight);
    }

    @DexIgnore
    public final void setScaledHeight(float f) {
        this.scaledHeight = f;
    }

    @DexIgnore
    public final void setScaledWidth(float f) {
        this.scaledWidth = f;
    }

    @DexIgnore
    public final void setScaledX(float f) {
        this.scaledX = f;
    }

    @DexIgnore
    public final void setScaledY(float f) {
        this.scaledY = f;
    }

    @DexIgnore
    public String toString() {
        return "MetricObject(scaledX=" + this.scaledX + ", scaledY=" + this.scaledY + ", scaledWidth=" + this.scaledWidth + ", scaledHeight=" + this.scaledHeight + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeFloat(this.scaledX);
        parcel.writeFloat(this.scaledY);
        parcel.writeFloat(this.scaledWidth);
        parcel.writeFloat(this.scaledHeight);
    }
}
