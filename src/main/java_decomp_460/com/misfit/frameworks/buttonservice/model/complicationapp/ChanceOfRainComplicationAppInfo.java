package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.ep1;
import com.fossil.fq1;
import com.fossil.it1;
import com.fossil.jq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.pq7;
import com.fossil.ry1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChanceOfRainComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public int probability;

    @DexIgnore
    public ChanceOfRainComplicationAppInfo(int i, long j) {
        super(np1.CHANCE_OF_RAIN_COMPLICATION);
        this.probability = i;
        this.expiredAt = j;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.probability = parcel.readInt();
        this.expiredAt = parcel.readLong();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return new it1(new ep1(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (!(jq1 instanceof fq1)) {
            return null;
        }
        return new it1((fq1) jq1, new ep1(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.probability);
        parcel.writeLong(this.expiredAt);
    }
}
