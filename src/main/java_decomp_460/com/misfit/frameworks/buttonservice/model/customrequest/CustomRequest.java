package com.misfit.frameworks.buttonservice.model.customrequest;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class CustomRequest implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<CustomRequest> CREATOR; // = new CustomRequest$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public CustomRequest() {
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CustomRequest(Parcel parcel) {
        this();
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public abstract byte[] getCustomCommand();

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }
}
