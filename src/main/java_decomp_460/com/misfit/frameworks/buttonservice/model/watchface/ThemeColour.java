package com.misfit.frameworks.buttonservice.model.watchface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ThemeColour {
    DEFAULT,
    WHITE,
    LIGHT_GREY,
    DARK_GREY,
    BLACK
}
