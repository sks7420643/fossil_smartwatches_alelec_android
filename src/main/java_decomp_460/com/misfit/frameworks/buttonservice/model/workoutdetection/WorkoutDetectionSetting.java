package com.misfit.frameworks.buttonservice.model.workoutdetection;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public List<WorkoutDetectionItem> workoutDetectionList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WorkoutDetectionSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionSetting createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new WorkoutDetectionSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionSetting[] newArray(int i) {
            return new WorkoutDetectionSetting[i];
        }
    }

    @DexIgnore
    public WorkoutDetectionSetting(Parcel parcel) {
        pq7.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.workoutDetectionList = arrayList;
        parcel.readTypedList(arrayList, WorkoutDetectionItem.CREATOR);
    }

    @DexIgnore
    public WorkoutDetectionSetting(List<WorkoutDetectionItem> list) {
        pq7.c(list, "workoutDetectionList");
        this.workoutDetectionList = list;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WorkoutDetectionItem> getWorkoutDetectionList() {
        return this.workoutDetectionList;
    }

    @DexIgnore
    public final void setWorkoutDetectionList(List<WorkoutDetectionItem> list) {
        pq7.c(list, "<set-?>");
        this.workoutDetectionList = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeTypedList(this.workoutDetectionList);
    }
}
