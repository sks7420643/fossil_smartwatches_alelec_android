package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.util.Log;
import com.fossil.cj4;
import com.fossil.dj4;
import com.fossil.hj4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLECustomizationDeserializer implements dj4<BLECustomization> {
    @DexIgnore
    @Override // com.fossil.dj4
    public BLECustomization deserialize(JsonElement jsonElement, Type type, cj4 cj4) throws hj4 {
        Log.d(BLECustomizationDeserializer.class.getName(), jsonElement.toString());
        return jsonElement.d().p("type").b() != 1 ? new BLENonCustomization() : (BLECustomization) new Gson().g(jsonElement, BLEGoalTrackingCustomization.class);
    }
}
