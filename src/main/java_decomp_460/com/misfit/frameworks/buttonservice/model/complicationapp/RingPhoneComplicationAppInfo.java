package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.jq1;
import com.fossil.ku1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.oq1;
import com.fossil.pq7;
import com.fossil.ry1;
import com.fossil.tt1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingPhoneComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public ku1 mRingPhoneState; // = ku1.OFF;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Parcel parcel) {
        super(parcel);
        pq7.c(parcel, "parcel");
        this.mRingPhoneState = ku1.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(ku1 ku1) {
        super(np1.RING_PHONE);
        pq7.c(ku1, "ringPhoneState");
        this.mRingPhoneState = ku1;
    }

    @DexIgnore
    public final ku1 getMRingPhoneState() {
        return this.mRingPhoneState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceData() {
        return new tt1(this.mRingPhoneState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public mt1 getSDKDeviceResponse(jq1 jq1, ry1 ry1) {
        pq7.c(jq1, "deviceRequest");
        if (jq1 instanceof oq1) {
            return new tt1((oq1) jq1, this.mRingPhoneState);
        }
        return null;
    }

    @DexIgnore
    public final void setMRingPhoneState(ku1 ku1) {
        pq7.c(ku1, "<set-?>");
        this.mRingPhoneState = ku1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mRingPhoneState.ordinal());
    }
}
