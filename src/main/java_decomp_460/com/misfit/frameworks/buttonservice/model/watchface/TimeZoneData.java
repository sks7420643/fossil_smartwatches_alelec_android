package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.model.PlaceFields;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeZoneData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String location;
    @DexIgnore
    public int offsetMin;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<TimeZoneData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TimeZoneData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new TimeZoneData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TimeZoneData[] newArray(int i) {
            return new TimeZoneData[i];
        }
    }

    @DexIgnore
    public TimeZoneData() {
        this(null, 0, 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TimeZoneData(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r3, r0)
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0018
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            int r1 = r3.readInt()
            r2.<init>(r0, r1)
            return
        L_0x0018:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public TimeZoneData(String str, int i) {
        pq7.c(str, PlaceFields.LOCATION);
        this.location = str;
        this.offsetMin = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TimeZoneData(String str, int i, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? 1 : i);
    }

    @DexIgnore
    public static /* synthetic */ TimeZoneData copy$default(TimeZoneData timeZoneData, String str, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = timeZoneData.location;
        }
        if ((i2 & 2) != 0) {
            i = timeZoneData.offsetMin;
        }
        return timeZoneData.copy(str, i);
    }

    @DexIgnore
    public final String component1() {
        return this.location;
    }

    @DexIgnore
    public final int component2() {
        return this.offsetMin;
    }

    @DexIgnore
    public final TimeZoneData copy(String str, int i) {
        pq7.c(str, PlaceFields.LOCATION);
        return new TimeZoneData(str, i);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof TimeZoneData) {
                TimeZoneData timeZoneData = (TimeZoneData) obj;
                if (!pq7.a(this.location, timeZoneData.location) || this.offsetMin != timeZoneData.offsetMin) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getLocation() {
        return this.location;
    }

    @DexIgnore
    public final int getOffsetMin() {
        return this.offsetMin;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.location;
        return ((str != null ? str.hashCode() : 0) * 31) + this.offsetMin;
    }

    @DexIgnore
    public final void setLocation(String str) {
        pq7.c(str, "<set-?>");
        this.location = str;
    }

    @DexIgnore
    public final void setOffsetMin(int i) {
        this.offsetMin = i;
    }

    @DexIgnore
    public String toString() {
        return "TimeZoneData(location=" + this.location + ", offsetMin=" + this.offsetMin + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeInt(this.offsetMin);
    }
}
