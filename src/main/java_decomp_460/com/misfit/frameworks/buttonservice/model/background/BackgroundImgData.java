package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rl1;
import com.misfit.frameworks.common.log.MFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundImgData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public String imgData;
    @DexIgnore
    public String imgName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundImgData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundImgData createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new BackgroundImgData(parcel, (kq7) null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundImgData[] newArray(int i) {
            return new BackgroundImgData[i];
        }
    }

    /*
    static {
        String simpleName = BackgroundImgData.class.getSimpleName();
        pq7.b(simpleName, "BackgroundImgData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public BackgroundImgData(Parcel parcel) {
        String readString = parcel.readString();
        this.imgName = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.imgData = readString2 == null ? "" : readString2;
    }

    @DexIgnore
    public /* synthetic */ BackgroundImgData(Parcel parcel, kq7 kq7) {
        this(parcel);
    }

    @DexIgnore
    public BackgroundImgData(String str, String str2) {
        pq7.c(str, "imgName");
        pq7.c(str2, "imgData");
        this.imgName = str;
        this.imgData = str2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getHash() {
        return this.imgName + ':' + this.imgData;
    }

    @DexIgnore
    public final String getImgData() {
        return this.imgData;
    }

    @DexIgnore
    public final String getImgName() {
        return this.imgName;
    }

    @DexIgnore
    public final void setImgData(String str) {
        pq7.c(str, "<set-?>");
        this.imgData = str;
    }

    @DexIgnore
    public final void setImgName(String str) {
        pq7.c(str, "<set-?>");
        this.imgName = str;
    }

    @DexIgnore
    public final rl1 toSDKBackgroundImage() {
        byte[] bArr;
        try {
            bArr = Base64.decode(this.imgData, 0);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, ".toSDKBackgroundImage(), ex: " + e);
            bArr = new byte[0];
        }
        String str2 = this.imgName;
        pq7.b(bArr, "byteData");
        return new rl1(str2, bArr, null, 4, null);
    }

    @DexIgnore
    public String toString() {
        return "{imgName: " + this.imgName + ", imgData: BASE64_DATA_SIZE_" + this.imgData.length() + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.imgName);
        parcel.writeString(this.imgData);
    }
}
