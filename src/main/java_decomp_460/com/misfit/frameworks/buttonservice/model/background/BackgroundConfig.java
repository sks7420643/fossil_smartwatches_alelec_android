package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.gv1;
import com.fossil.hv1;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rl1;
import com.fossil.sl1;
import com.fossil.vt7;
import com.fossil.wt7;
import com.fossil.z58;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String TAG; // = "BackgroundConfig";
    @DexIgnore
    public BackgroundImgData bottomComplicationBackground;
    @DexIgnore
    public BackgroundImgData leftComplicationBackground;
    @DexIgnore
    public BackgroundImgData mainBackground;
    @DexIgnore
    public BackgroundImgData rightComplicationBackground;
    @DexIgnore
    public /* final */ long timestamp;
    @DexIgnore
    public BackgroundImgData topComplicationBackground;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundConfig createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new BackgroundConfig(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundConfig[] newArray(int i) {
            return new BackgroundConfig[i];
        }
    }

    @DexIgnore
    public BackgroundConfig(long j, BackgroundImgData backgroundImgData, BackgroundImgData backgroundImgData2, BackgroundImgData backgroundImgData3, BackgroundImgData backgroundImgData4, BackgroundImgData backgroundImgData5) {
        pq7.c(backgroundImgData, "mainBackground");
        pq7.c(backgroundImgData2, "topComplicationBackground");
        pq7.c(backgroundImgData3, "rightComplicationBackground");
        pq7.c(backgroundImgData4, "bottomComplicationBackground");
        pq7.c(backgroundImgData5, "leftComplicationBackground");
        this.timestamp = j;
        this.mainBackground = backgroundImgData;
        this.topComplicationBackground = backgroundImgData2;
        this.rightComplicationBackground = backgroundImgData3;
        this.bottomComplicationBackground = backgroundImgData4;
        this.leftComplicationBackground = backgroundImgData5;
    }

    @DexIgnore
    public BackgroundConfig(Parcel parcel) {
        this.timestamp = parcel.readLong();
        this.mainBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.topComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.rightComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.bottomComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.leftComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
    }

    @DexIgnore
    public /* synthetic */ BackgroundConfig(Parcel parcel, kq7 kq7) {
        this(parcel);
    }

    @DexIgnore
    private final String getHash() {
        return this.mainBackground.getHash() + ':' + this.topComplicationBackground.getHash() + ':' + this.rightComplicationBackground.getHash() + ':' + this.bottomComplicationBackground.getHash() + ':' + this.leftComplicationBackground.getHash();
    }

    @DexIgnore
    private final rl1 getMainBackgroundImage(String str) {
        String imgData = this.mainBackground.getImgData();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "imageData = " + imgData);
        if (imgData.length() == 0) {
            return new rl1(this.mainBackground.getImgName(), new byte[0], null, 4, null);
        }
        if (!wt7.v(imgData, str, false, 2, null) || !vt7.h(imgData, Constants.PHOTO_BINARY_NAME_SUFFIX, true)) {
            return this.mainBackground.toSDKBackgroundImage();
        }
        try {
            byte[] c = z58.c(new File(this.mainBackground.getImgData()));
            String imgName = this.mainBackground.getImgName();
            pq7.b(c, "data");
            return new rl1(imgName, c, null, 4, null);
        } catch (Exception e) {
            e.printStackTrace();
            return this.mainBackground.toSDKBackgroundImage();
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BackgroundConfig)) {
            return false;
        }
        return vt7.j(getHash(), ((BackgroundConfig) obj).getHash(), true);
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final gv1 toSDKBackgroundImageConfig(String str) {
        pq7.c(str, "directory");
        return new gv1(new hv1[]{new sl1(pq7.a(this.mainBackground.getImgName(), Constants.MAIN_BACKGROUND_NAME) ? getMainBackgroundImage(str) : this.mainBackground.toSDKBackgroundImage(), this.topComplicationBackground.toSDKBackgroundImage(), this.rightComplicationBackground.toSDKBackgroundImage(), this.bottomComplicationBackground.toSDKBackgroundImage(), this.leftComplicationBackground.toSDKBackgroundImage())});
    }

    @DexIgnore
    public String toString() {
        return "{timestamp: " + this.timestamp + ", mainBackground: " + this.mainBackground + ", topComplicationBackground: " + this.topComplicationBackground + ", rightComplicationBackground: " + this.rightComplicationBackground + ", bottomComplicationBackground: " + this.bottomComplicationBackground + ", leftComplicationBackground: " + this.leftComplicationBackground + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeLong(this.timestamp);
        this.mainBackground.writeToParcel(parcel, i);
        this.topComplicationBackground.writeToParcel(parcel, i);
        this.rightComplicationBackground.writeToParcel(parcel, i);
        this.bottomComplicationBackground.writeToParcel(parcel, i);
        this.leftComplicationBackground.writeToParcel(parcel, i);
    }
}
