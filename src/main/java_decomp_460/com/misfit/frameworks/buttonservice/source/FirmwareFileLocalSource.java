package com.misfit.frameworks.buttonservice.source;

import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileLocalSource {
    @DexIgnore
    public /* final */ String TAG;

    @DexIgnore
    public FirmwareFileLocalSource() {
        String name = FirmwareFileLocalSource.class.getName();
        pq7.b(name, "FirmwareFileLocalSource::class.java.name");
        this.TAG = name;
    }

    @DexIgnore
    public boolean deleteFile(String str) {
        pq7.c(str, "filePath");
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, ".saveFile(), success=true, filePath=" + str);
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".deleteFile(), cannot delete file, ex=" + e);
            return false;
        }
    }

    @DexIgnore
    public File getFile(String str) {
        pq7.c(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".getFile(), filePath=" + str);
        try {
            return new File(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".getFile(), cannot get file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    public FileInputStream readFile(String str) {
        pq7.c(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".readFile(), filePath=" + str);
        try {
            return new FileInputStream(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".readFile(), cannot read file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0094 A[SYNTHETIC, Splitter:B:19:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bc A[SYNTHETIC, Splitter:B:27:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean saveFile(java.io.InputStream r11, java.lang.String r12) {
        /*
        // Method dump skipped, instructions count: 231
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource.saveFile(java.io.InputStream, java.lang.String):boolean");
    }

    @DexIgnore
    public boolean verify(String str, String str2) {
        pq7.c(str, "filePath");
        pq7.c(str2, "checkSum");
        boolean verifyDownloadFile = FileUtils.verifyDownloadFile(str, str2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, ".verify(), isValid=" + verifyDownloadFile + ", filePath=" + str + ", checkSum=" + str2);
        return verifyDownloadFile;
    }
}
