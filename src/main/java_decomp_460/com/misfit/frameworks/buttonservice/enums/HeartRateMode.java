package com.misfit.frameworks.buttonservice.enums;

import com.fossil.al7;
import com.fossil.cn1;
import com.fossil.kq7;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum HeartRateMode {
    NONE(0),
    CONTINUOUS(1),
    LOW_POWER(2),
    DISABLE(3);
    
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[cn1.a.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[cn1.a.CONTINUOUS.ordinal()] = 1;
                $EnumSwitchMapping$0[cn1.a.LOW_POWER.ordinal()] = 2;
                $EnumSwitchMapping$0[cn1.a.DISABLE.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final HeartRateMode consume(cn1.a aVar) {
            pq7.c(aVar, "sdkHeartRateMode");
            int i = WhenMappings.$EnumSwitchMapping$0[aVar.ordinal()];
            if (i == 1) {
                return HeartRateMode.CONTINUOUS;
            }
            if (i == 2) {
                return HeartRateMode.LOW_POWER;
            }
            if (i == 3) {
                return HeartRateMode.DISABLE;
            }
            throw new al7();
        }

        @DexIgnore
        public final HeartRateMode fromValue(int i) {
            HeartRateMode heartRateMode;
            HeartRateMode[] values = HeartRateMode.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    heartRateMode = null;
                    break;
                }
                heartRateMode = values[i2];
                if (heartRateMode.getValue() == i) {
                    break;
                }
                i2++;
            }
            return heartRateMode != null ? heartRateMode : HeartRateMode.NONE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[HeartRateMode.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[HeartRateMode.CONTINUOUS.ordinal()] = 1;
            $EnumSwitchMapping$0[HeartRateMode.LOW_POWER.ordinal()] = 2;
            $EnumSwitchMapping$0[HeartRateMode.DISABLE.ordinal()] = 3;
            $EnumSwitchMapping$0[HeartRateMode.NONE.ordinal()] = 4;
        }
        */
    }

    @DexIgnore
    public HeartRateMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final cn1.a toSDKHeartRateMode() {
        int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
        if (i == 1) {
            return cn1.a.CONTINUOUS;
        }
        if (i == 2) {
            return cn1.a.LOW_POWER;
        }
        if (i == 3) {
            return cn1.a.DISABLE;
        }
        if (i == 4) {
            return cn1.a.DISABLE;
        }
        throw new al7();
    }
}
