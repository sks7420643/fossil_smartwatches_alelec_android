package com.misfit.frameworks.buttonservice;

import android.os.Bundle;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.lw1;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.ry1;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.ButtonService$themeDataToBinary$1", f = "ButtonService.kt", l = {}, m = "invokeSuspend")
public final class ButtonService$themeDataToBinary$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeData $themeData;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ButtonService$themeDataToBinary$Anon1(ButtonService buttonService, String str, ThemeData themeData, qn7 qn7) {
        super(2, qn7);
        this.this$0 = buttonService;
        this.$serial = str;
        this.$themeData = themeData;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        ButtonService$themeDataToBinary$Anon1 buttonService$themeDataToBinary$Anon1 = new ButtonService$themeDataToBinary$Anon1(this.this$0, this.$serial, this.$themeData, qn7);
        buttonService$themeDataToBinary$Anon1.p$ = (iv7) obj;
        return buttonService$themeDataToBinary$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((ButtonService$themeDataToBinary$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        ry1 ry1;
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            Version uiVersion = this.this$0.getUiVersion(this.$serial);
            if (uiVersion != null) {
                ry1 = new ry1(uiVersion.getMajor(), uiVersion.getMinor());
            } else {
                int uiMajorVersion = DevicePreferenceUtils.getUiMajorVersion(this.this$0, this.$serial);
                int uiMinorVersion = DevicePreferenceUtils.getUiMinorVersion(this.this$0, this.$serial);
                ry1 = (uiMajorVersion <= 0 || uiMinorVersion <= 0) ? null : new ry1(uiMajorVersion, uiMinorVersion);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ButtonService.TAG;
            local.d(str, "themeDataToBinar deviceUiVersion " + uiVersion + " themeData " + this.$themeData + ' ');
            if (ry1 != null) {
                lw1 f = ThemeExtentionKt.toThemeEditor(this.$themeData).c(ry1).f();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ButtonService.TAG;
                local2.e(str2, "themeDataToBinary - data: " + f);
                if (f == null) {
                    this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.FAILED, null);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putByteArray(ButtonService.Companion.getTHEME_BINARY_EXTRA(), f.getData());
                    this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.SUCCEEDED, bundle);
                }
            } else {
                this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.FAILED, null);
            }
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
