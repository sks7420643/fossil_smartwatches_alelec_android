package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.bw7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.gu7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.xw7;
import com.fossil.yn7;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendMusicResponseFromQueue$Anon2 extends qq7 implements rp7<yx1, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendMusicResponseFromQueue$2$2", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendMusicResponseFromQueue$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = deviceCommunicator$sendMusicResponseFromQueue$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, qn7);
            anon2_Level2.p$ = (iv7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((Anon2_Level2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2 = this.this$0;
                deviceCommunicator$sendMusicResponseFromQueue$Anon2.this$0.startSendMusicAppResponse(deviceCommunicator$sendMusicResponseFromQueue$Anon2.$response);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendMusicResponseFromQueue$Anon2(DeviceCommunicator deviceCommunicator, MusicResponse musicResponse) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$response = musicResponse;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(yx1 yx1) {
        invoke(yx1);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(yx1 yx1) {
        pq7.c(yx1, "error");
        MusicResponse musicResponse = this.$response;
        ErrorCodeBuilder.Step step = musicResponse instanceof NotifyMusicEventResponse ? ErrorCodeBuilder.Step.NOTIFY_MUSIC_EVENT : musicResponse instanceof MusicTrackInfoResponse ? ErrorCodeBuilder.Step.SEND_TRACK_INFO : null;
        if (step != null) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = this.this$0.getSerial();
            String tag = this.this$0.getTAG();
            remote.e(component, session, serial, tag, "Send respond: " + this.$response.getType() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag2 = this.this$0.getTAG();
        local.d(tag2, "device with serial = " + this.this$0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", push back by result error=" + yx1.getErrorCode());
        xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new Anon2_Level2(this, null), 3, null);
    }
}
