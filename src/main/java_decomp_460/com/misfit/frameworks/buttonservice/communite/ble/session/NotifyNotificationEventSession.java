package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyNotificationEventSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ NotificationBaseObj mNotifyNotificationEvent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class NotifyNotificationEventState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public NotifyNotificationEventState() {
            super(NotifyNotificationEventSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> notifyNotificationEvent = NotifyNotificationEventSession.this.getBleAdapter().notifyNotificationEvent(NotifyNotificationEventSession.this.getLogSession(), NotifyNotificationEventSession.this.mNotifyNotificationEvent, this);
            this.task = notifyNotificationEvent;
            if (notifyNotificationEvent == null) {
                NotifyNotificationEventSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onNotifyNotificationEventFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            NotifyNotificationEventSession.this.stop(FailureCode.FAILED_TO_NOTIFY_NOTIFICATION_EVENT);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onNotifyNotificationEventSuccess() {
            stopTimeout();
            NotifyNotificationEventSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout cancel task");
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            NotifyNotificationEventSession.this.stop(FailureCode.FAILED_TO_NOTIFY_NOTIFICATION_EVENT);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyNotificationEventSession(NotificationBaseObj notificationBaseObj, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_COMPLICATION_APPS, bleAdapterImpl, bleSessionCallback);
        pq7.c(notificationBaseObj, "mNotifyNotificationEvent");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mNotifyNotificationEvent = notificationBaseObj;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        pq7.c(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.NOTIFY_NOTIFICATION_EVENT) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        NotifyNotificationEventSession notifyNotificationEventSession = new NotifyNotificationEventSession(this.mNotifyNotificationEvent, getBleAdapter(), getBleSessionCallback());
        notifyNotificationEventSession.setDevice(getDevice());
        return notifyNotificationEventSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.NOTIFY_NOTIFICATION_EVENT_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.NOTIFY_NOTIFICATION_EVENT_STATE;
        String name = NotifyNotificationEventState.class.getName();
        pq7.b(name, "NotifyNotificationEventState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
