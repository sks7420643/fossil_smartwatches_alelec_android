package com.misfit.frameworks.buttonservice.communite.ble.device;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.facebook.places.model.PlaceFields;
import com.fossil.aq1;
import com.fossil.bq1;
import com.fossil.cq1;
import com.fossil.du1;
import com.fossil.fitness.FitnessData;
import com.fossil.il7;
import com.fossil.iq1;
import com.fossil.jq1;
import com.fossil.kw1;
import com.fossil.lq1;
import com.fossil.lw1;
import com.fossil.mp1;
import com.fossil.mq1;
import com.fossil.mt1;
import com.fossil.np1;
import com.fossil.oq1;
import com.fossil.pp1;
import com.fossil.pq1;
import com.fossil.pq7;
import com.fossil.qp1;
import com.fossil.qy1;
import com.fossil.rp1;
import com.fossil.ry1;
import com.fossil.tl7;
import com.fossil.tp1;
import com.fossil.up1;
import com.fossil.vp1;
import com.fossil.wp1;
import com.fossil.xn1;
import com.fossil.xp1;
import com.fossil.yk1;
import com.fossil.yp1;
import com.fossil.zk1;
import com.fossil.zp1;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ApplyThemeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ClearLinkMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.NotifyNotificationEventSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PreviewThemeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBiometricDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoLocalizationDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetFrontLightEnableSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetHeartRateModeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDeviceConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDisplayUnitSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetInactiveNudgeConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetLinkMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMinimumStepThresholdSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetReplyMessageMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWorkoutConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.StopCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.VerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator extends BleCommunicatorAbs {
    @DexIgnore
    public /* final */ HashMap<String, jq1> mDeviceAppRequests; // = new HashMap<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator(Context context, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(new BleAdapterImpl(context, str, str2), context, str, communicationResultCallback);
        pq7.c(context, "context");
        pq7.c(str, "serial");
        pq7.c(str2, "macAddress");
        pq7.c(communicationResultCallback, "communicationResultCallback");
    }

    @DexIgnore
    private final void sendDeviceAppResponseFromQueue(DeviceAppResponse deviceAppResponse) {
        synchronized (this) {
            if (!deviceAppResponse.getLifeTimeObject().isExpire()) {
                if (deviceAppResponse.isForceUpdate()) {
                    sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceData());
                } else {
                    String name = deviceAppResponse.getDeviceEventId().name();
                    jq1 jq1 = this.mDeviceAppRequests.get(name);
                    if (jq1 != null) {
                        sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceResponse(jq1, new ry1((byte) getBleAdapter().getMicroAppMajorVersion(), (byte) getBleAdapter().getMicroAppMinorVersion())));
                    } else {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.BLE;
                        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
                        String serial = getSerial();
                        String tag = getTAG();
                        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
                        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
                        remote.e(component, session, serial, tag, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. deviceAppRequest with key[" + name + "] does not exist.");
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String tag2 = getTAG();
                        local.e(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue(), deviceAppRequest with key[" + name + "] does not exist.");
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void sendDianaNotification(NotificationBaseObj notificationBaseObj) {
        synchronized (this) {
            addToQuickCommandQueue(notificationBaseObj);
        }
    }

    @DexIgnore
    private final void sendMusicResponseFromQueue(MusicResponse musicResponse) {
        qy1<tl7> qy1;
        qy1<tl7> s;
        synchronized (this) {
            if (!musicResponse.getLifeCountDownObject().isExpire()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() = " + musicResponse.toString());
                if (musicResponse instanceof NotifyMusicEventResponse) {
                    xn1 sDKMusicEvent = ((NotifyMusicEventResponse) musicResponse).toSDKMusicEvent();
                    if (sDKMusicEvent != null) {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.BLE;
                        FLogger.Session session = FLogger.Session.OTHER;
                        String serial = getSerial();
                        String tag2 = getTAG();
                        remote.d(component, session, serial, tag2, "NotifyMusicEvent: " + musicResponse.toRemoteLogString());
                        qy1 = getBleAdapter().notifyMusicEvent(FLogger.Session.OTHER, sDKMusicEvent);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String tag3 = getTAG();
                        local2.e(tag3, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() fail cause by sdkMusicEvent=" + sDKMusicEvent);
                        qy1 = null;
                    }
                } else if (musicResponse instanceof MusicTrackInfoResponse) {
                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component2 = FLogger.Component.BLE;
                    FLogger.Session session2 = FLogger.Session.OTHER;
                    String serial2 = getSerial();
                    String tag4 = getTAG();
                    remote2.d(component2, session2, serial2, tag4, "SendTrackInfo: " + musicResponse.toRemoteLogString());
                    qy1 = getBleAdapter().sendTrackInfo(FLogger.Session.OTHER, ((MusicTrackInfoResponse) musicResponse).toSDKTrackInfo());
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tag5 = getTAG();
                    local3.d(tag5, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() no support value = " + musicResponse.toRemoteLogString());
                    qy1 = null;
                }
                musicResponse.getLifeCountDownObject().goDown();
                if (!(qy1 == null || (s = qy1.m(new DeviceCommunicator$sendMusicResponseFromQueue$Anon1(this, musicResponse))) == null)) {
                    s.l(new DeviceCommunicator$sendMusicResponseFromQueue$Anon2(this, musicResponse));
                }
            }
        }
    }

    @DexIgnore
    private final void sendNotificationFromQueue(NotificationBaseObj notificationBaseObj) {
        qy1<tl7> s;
        synchronized (this) {
            if (!notificationBaseObj.getLifeCountDownObject().isExpire()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, " .sendNotificationFromQueue() = " + notificationBaseObj + ", serial=" + getSerial());
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.BLE;
                FLogger.Session session = FLogger.Session.OTHER;
                String serial = getSerial();
                String tag2 = getTAG();
                remote.d(component, session, serial, tag2, "Send notification: " + notificationBaseObj.toRemoteLogString());
                qy1<tl7> sendNotification = getBleAdapter().sendNotification(FLogger.Session.OTHER, notificationBaseObj);
                notificationBaseObj.getLifeCountDownObject().goDown();
                if (!(sendNotification == null || (s = sendNotification.m(new DeviceCommunicator$sendNotificationFromQueue$Anon1(this, notificationBaseObj))) == null)) {
                    s.l(new DeviceCommunicator$sendNotificationFromQueue$Anon2(this, notificationBaseObj));
                }
            } else {
                getCommunicationResultCallback().onNotificationSent(notificationBaseObj.getUid(), false);
            }
        }
    }

    @DexIgnore
    private final void sendResponseToWatch(DeviceAppResponse deviceAppResponse, mt1 mt1) {
        qy1<tl7> s;
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = getSerial();
        String tag = getTAG();
        remote.i(component, session, serial, tag, "Send respond: " + deviceAppResponse);
        if (mt1 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local.d(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + deviceAppResponse.toString());
            qy1<tl7> sendRespond = getBleAdapter().sendRespond(FLogger.Session.HANDLE_WATCH_REQUEST, mt1);
            if (sendRespond != null && (s = sendRespond.m(new DeviceCommunicator$sendResponseToWatch$Anon1(this, deviceAppResponse))) != null) {
                s.l(new DeviceCommunicator$sendResponseToWatch$Anon2(this, deviceAppResponse));
                return;
            }
            return;
        }
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial2 = getSerial();
        String tag3 = getTAG();
        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
        remote2.e(component2, session2, serial2, tag3, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. device request type is not match with device response");
        FLogger.INSTANCE.getLocal().e(getTAG(), " .sendDeviceAppResponseFromQueue(), device request type is not match with device response or could not get DeviceData from DeviceAppResponse when forceUpdate");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean applyTheme(ThemeConfig themeConfig) {
        pq7.c(themeConfig, "themeConfig");
        queueSessionAndStart(new ApplyThemeSession(themeConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void confirmStopWorkout(String str, boolean z) {
        pq7.c(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IReadWorkoutStateSession) {
            ((IReadWorkoutStateSession) currentSession).confirmStopWorkout(str, z);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".confirmStopWorkout() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.i(component, session, str, tag2, "confirmStopWorkout FAILED, currentSession=" + currentSession);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean disableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean enableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public List<FitnessData> getSyncData() {
        if (getCurrentSession() instanceof SyncSession) {
            log("Current session is Sync Session, start get sync data.");
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                return ((SyncSession) currentSession).getSyncData();
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession");
        }
        log("Current session is not Sync Session, empty sync data.");
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public Version getUiOSVersion() {
        zk1 M;
        yk1 deviceObj = getBleAdapter().getDeviceObj();
        ry1 uiPackageOSVersion = (deviceObj == null || (M = deviceObj.M()) == null) ? null : M.getUiPackageOSVersion();
        if (uiPackageOSVersion == null || uiPackageOSVersion.getMajor() == 0 || uiPackageOSVersion.getMinor() == 0) {
            return null;
        }
        return new Version(uiPackageOSVersion.getMajor(), uiPackageOSVersion.getMinor());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void handleEventReceived(yk1 yk1, mp1 mp1) {
        pq7.c(yk1, "device");
        pq7.c(mp1, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleEventReceived, device = " + yk1 + ".deviceInformation.serialNumber, event = " + mp1);
        if (mp1 instanceof jq1) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.i(component, session, serial, tag2, "Receive device request, device=" + getSerial() + ", device event=" + mp1);
            this.mDeviceAppRequests.put(mp1.getDeviceEventId().name(), mp1);
            Bundle bundle = new Bundle();
            if (mp1 instanceof oq1) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((oq1) mp1).getAction().ordinal());
            } else if (mp1 instanceof iq1) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, du1.START.ordinal());
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((iq1) mp1).getDestination());
            } else if (mp1 instanceof cq1) {
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((cq1) mp1).getChallengeId());
            } else if (mp1 instanceof pq1) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            } else if (mp1 instanceof mq1) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), bundle, getSerial());
        } else if (mp1 instanceof vp1) {
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.BLE;
            FLogger.Session session2 = FLogger.Session.OTHER;
            String serial2 = getSerial();
            String tag3 = getTAG();
            remote2.i(component2, session2, serial2, tag3, "Receive device notification, device=" + getSerial() + ", event=" + mp1);
            Bundle bundle2 = new Bundle();
            if (mp1 instanceof xp1) {
                bundle2.putInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.Companion.fromSDKMusicAction(((xp1) mp1).getAction()).ordinal());
            } else if (mp1 instanceof yp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((yp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof pp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((pp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof up1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((up1) mp1).getAction().ordinal());
            } else if (mp1 instanceof qp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, np1.APP_NOTIFICATION_CONTROL.ordinal());
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            } else if (mp1 instanceof tp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((tp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof wp1) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, new EncryptedData((wp1) mp1));
            } else if (mp1 instanceof zp1) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((zp1) mp1).getSessionId());
            } else if (mp1 instanceof aq1) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((aq1) mp1).getSessionId());
            } else if (mp1 instanceof bq1) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), bundle2, getSerial());
        } else if (mp1 instanceof lq1) {
            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
            FLogger.Component component3 = FLogger.Component.BLE;
            FLogger.Session session3 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial3 = getSerial();
            String tag4 = getTAG();
            remote3.i(component3, session3, serial3, tag4, "Receive micro app request, device=" + getSerial() + ", event=" + mp1);
            this.mDeviceAppRequests.put(mp1.getDeviceEventId().name(), mp1);
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else if (mp1 instanceof rp1) {
            IRemoteFLogger remote4 = FLogger.INSTANCE.getRemote();
            FLogger.Component component4 = FLogger.Component.BLE;
            FLogger.Session session4 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial4 = getSerial();
            String tag5 = getTAG();
            remote4.i(component4, session4, serial4, tag5, "Receive authentication request notification, device=" + getSerial() + ", event=" + mp1);
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else {
            super.handleEventReceived(yk1, mp1);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onPing() {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onPing();
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onPing() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onPing FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void onQuickCommandAction(Object obj) {
        pq7.c(obj, Constants.COMMAND);
        if (obj instanceof DeviceAppResponse) {
            sendDeviceAppResponseFromQueue((DeviceAppResponse) obj);
        } else if (obj instanceof MusicResponse) {
            sendMusicResponseFromQueue((MusicResponse) obj);
        } else if (obj instanceof NotificationBaseObj) {
            sendNotificationFromQueue((NotificationBaseObj) obj);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveCurrentSecretKey(byte[] bArr) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveCurrentSecretKey(bArr);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveCurrentSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveCurrentSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceivePushSecretKeyResponse(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IVerifySecretKeySession) {
            ((IVerifySecretKeySession) currentSession).onReceiveServerResponse(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceivePushSecretKeyResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceivePushSecretKeyResponse FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerRandomKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveRandomKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerRandomKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerRandomKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerSecretKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveServerSecretKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) {
        pq7.c(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (!(currentSession instanceof ISetWatchParamStateSession)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onSetWatchParamResponse() FAILED, current session=" + currentSession);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String tag2 = getTAG();
            remote.i(component, session, str, tag2, "onSetWatchParamResponse FAILED, currentSession=" + currentSession);
        } else if (!z) {
            ((ISetWatchParamStateSession) currentSession).onGetWatchParamFailed();
        } else if (watchParamsFileMapping == null) {
            ((ISetWatchParamStateSession) currentSession).doNextState();
        } else {
            ((ISetWatchParamStateSession) currentSession).setLatestWatchParam(str, watchParamsFileMapping);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public ThemeData parseBinaryToThemeData(byte[] bArr) {
        pq7.c(bArr, "byteArray");
        return getBleAdapter().parseBinaryToThemeData(bArr);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean previewTheme(lw1 lw1, kw1 kw1) {
        queueSessionAndStart(new PreviewThemeSession(kw1, lw1, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean sendingEncryptedDataSession(byte[] bArr, boolean z) {
        pq7.c(bArr, "encryptedData");
        SharePreferencesUtils.getInstance(getBleAdapter().getContext()).setBoolean(SharePreferencesUtils.BC_STATUS, z);
        queueSessionAndStart(new SendingEncryptedDataSession(bArr, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setMinimumStepThresholdSession(long j) {
        queueSessionAndStart(new SetMinimumStepThresholdSession(j, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setSecretKey(byte[] bArr) {
        BleAdapterImpl mBleAdapter = getMBleAdapter();
        FLogger.Session session = FLogger.Session.OTHER;
        if (bArr != null) {
            mBleAdapter.setSecretKey(session, bArr);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutConfigSession(WorkoutConfigData workoutConfigData) {
        pq7.c(workoutConfigData, "workoutConfigData");
        queueSessionAndStart(new SetWorkoutConfigSession(workoutConfigData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutGPSData(Location location) {
        pq7.c(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .setWorkoutGPSData() = " + location.toString());
        getBleAdapter().setWorkoutGPSData(location, FLogger.Session.WORKOUT_TETHER_GPS);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCalibrationSession() {
        queueSessionAndStart(new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCleanLinkMappingSession(List<? extends BLEMapping> list) {
        pq7.c(list, "mappings");
        return queueSessionAndStart(new ClearLinkMappingSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startConnectionDeviceSession(boolean z, UserProfile userProfile) {
        queueSessionAndStart(new ConnectDeviceSession(z, userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startInstallWatchApps(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISetWatchAppFileSession) {
            ((ISetWatchAppFileSession) currentSession).onWatchAppFilesReady(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onWatchAppFilesReady() FAILED, current session=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startNotifyNotificationEvent(NotificationBaseObj notificationBaseObj) {
        pq7.c(notificationBaseObj, "notifyNotificationEvent");
        queueSessionAndStart(new NotifyNotificationEventSession(notificationBaseObj, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile) {
        pq7.c(firmwareData, "firmwareData");
        pq7.c(userProfile, "userProfile");
        queueSessionAndStart(new OtaSession(firmwareData, userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startPairingSession(UserProfile userProfile) {
        pq7.c(userProfile, "userProfile");
        queueSessionAndStart(new PairingNewDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startReadCurrentWorkoutSession() {
        queueSessionAndStart(new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z) {
        pq7.c(deviceAppResponse, "deviceAppResponse");
        deviceAppResponse.getLifeTimeObject().startExpireTimeCountDown();
        deviceAppResponse.setForceUpdate(z);
        addToQuickCommandQueue(deviceAppResponse);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSendMusicAppResponse(MusicResponse musicResponse) {
        synchronized (this) {
            pq7.c(musicResponse, "musicResponse");
            addToQuickCommandQueue(musicResponse);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        pq7.c(notificationBaseObj, "newNotification");
        sendDianaNotification(notificationBaseObj);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        pq7.c(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetAutoBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBiometricData(UserBiometricData userBiometricData) {
        pq7.c(userBiometricData, "userBiometricData");
        queueSessionAndStart(new SetAutoBiometricDataSession(userBiometricData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMapping(List<? extends BLEMapping> list) {
        pq7.c(list, "mappings");
        return queueSessionAndStart(new SetAutoMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        pq7.c(list, "multipleAlarmList");
        queueSessionAndStart(new SetAutoMultiAlarmsSession(list, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        pq7.c(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetAutoNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoSecondTimezone(String str) {
        pq7.c(str, "secondTimezoneId");
        return queueSessionAndStart(new SetAutoSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        pq7.c(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetAutoWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        pq7.c(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetFrontLightEnable(boolean z) {
        queueSessionAndStart(new SetFrontLightEnableSession(z, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetHeartRateMode(HeartRateMode heartRateMode) {
        pq7.c(heartRateMode, "heartRateMode");
        queueSessionAndStart(new SetHeartRateModeSession(heartRateMode, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDeviceConfig(UserProfile userProfile) {
        pq7.c(userProfile, "userProfile");
        queueSessionAndStart(new SetImplicitDeviceConfigSession(userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit) {
        pq7.c(userDisplayUnit, "userDisplayUnit");
        queueSessionAndStart(new SetImplicitDisplayUnitSession(userDisplayUnit, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData) {
        pq7.c(inactiveNudgeData, "inactiveNudgeData");
        queueSessionAndStart(new SetInactiveNudgeConfigSession(inactiveNudgeData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLinkMappingSession(List<? extends BLEMapping> list) {
        pq7.c(list, "mappings");
        return queueSessionAndStart(new SetLinkMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        pq7.c(localizationData, "localizationData");
        queueSessionAndStart(new SetAutoLocalizationDataSession(localizationData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        pq7.c(list, "multipleAlarmList");
        queueSessionAndStart(new SetMultiAlarmsSession(getBleAdapter(), list, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        pq7.c(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetReplyMessageMappingSettings(ReplyMessageMappingGroup replyMessageMappingGroup) {
        pq7.c(replyMessageMappingGroup, "replyMessageMappings");
        queueSessionAndStart(new SetReplyMessageMappingSession(replyMessageMappingGroup, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetSecondTimezoneSession(String str) {
        pq7.c(str, "secondTimezoneId");
        return queueSessionAndStart(new SetSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        pq7.c(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startStopCurrentWorkoutSession() {
        queueSessionAndStart(new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSwitchDeviceSession(UserProfile userProfile) {
        pq7.c(userProfile, "userProfile");
        queueSessionAndStart(new SwitchActiveDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSyncingSession(UserProfile userProfile) {
        pq7.c(userProfile, "userProfile");
        queueSessionAndStart(new SyncSession(getBleAdapter(), getBleSessionCallback(), userProfile));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startVerifySecretKeySession() {
        return queueSessionAndStart(new VerifySecretKeySession(getBleAdapter(), getBleSessionCallback()));
    }
}
