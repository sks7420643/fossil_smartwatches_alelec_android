package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.hs1;
import com.fossil.oy1;
import com.fossil.pq7;
import com.fossil.us1;
import com.fossil.yx1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoLocalizationDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public LocalizationData mNewLocalizationData;
    @DexIgnore
    public LocalizationData mOldLocalizationData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoLocalizationDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoLocalizationDataSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalizationState extends BleStateAbs {
        @DexIgnore
        public oy1<String> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetLocalizationState() {
            super(SetAutoLocalizationDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (SetAutoLocalizationDataSession.this.mNewLocalizationData.isDataValid()) {
                oy1<String> localizationData = SetAutoLocalizationDataSession.this.getBleAdapter().setLocalizationData(SetAutoLocalizationDataSession.this.mNewLocalizationData, SetAutoLocalizationDataSession.this.getLogSession(), this);
                this.task = localizationData;
                if (localizationData == null) {
                    SetAutoLocalizationDataSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetAutoLocalizationDataSession.this.log("Localization data is invalid!");
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataFail(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            if (!retry(SetAutoLocalizationDataSession.this.getContext(), SetAutoLocalizationDataSession.this.getSerial())) {
                SetAutoLocalizationDataSession.this.log("Reach the limit retry. Stop.");
                SetAutoLocalizationDataSession setAutoLocalizationDataSession = SetAutoLocalizationDataSession.this;
                setAutoLocalizationDataSession.storeMappings(setAutoLocalizationDataSession.mNewLocalizationData, true);
                SetAutoLocalizationDataSession.this.stop(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            SetAutoLocalizationDataSession setAutoLocalizationDataSession = SetAutoLocalizationDataSession.this;
            setAutoLocalizationDataSession.storeMappings(setAutoLocalizationDataSession.mNewLocalizationData, false);
            SetAutoLocalizationDataSession setAutoLocalizationDataSession2 = SetAutoLocalizationDataSession.this;
            setAutoLocalizationDataSession2.enterStateAsync(setAutoLocalizationDataSession2.createConcreteState((SetAutoLocalizationDataSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            oy1<String> oy1 = this.task;
            if (oy1 != null) {
                us1.a(oy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoLocalizationDataSession(LocalizationData localizationData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_LOCALIZATION_DATA, bleAdapterImpl, bleSessionCallback);
        pq7.c(localizationData, "mNewLocalizationData");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mNewLocalizationData = localizationData;
        setLogSession(FLogger.Session.OTHER);
    }

    @DexIgnore
    private final void storeMappings(LocalizationData localizationData, boolean z) {
        localizationData.setCheckSum(LocalizationData.Companion.getLocalizationDataCheckSum(localizationData));
        DevicePreferenceUtils.setAutoLocalizationData(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().t(localizationData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoLocalizationDataSession setAutoLocalizationDataSession = new SetAutoLocalizationDataSession(this.mNewLocalizationData, getBleAdapter(), getBleSessionCallback());
        setAutoLocalizationDataSession.setDevice(getDevice());
        return setAutoLocalizationDataSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState createConcreteState;
        super.initSettings();
        this.mOldLocalizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(hs1.class) == null) {
            log("This device does not support set localization data.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (!this.mNewLocalizationData.isDataValid()) {
            log("New localization data is invalid.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (!LocalizationData.Companion.isTheSameFile(this.mOldLocalizationData, this.mNewLocalizationData)) {
            storeMappings(this.mNewLocalizationData, true);
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_LOCALIZATION_STATE);
        } else {
            log("Old localization data is the same with the new one, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(createConcreteState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LOCALIZATION_STATE;
        String name = SetLocalizationState.class.getName();
        pq7.b(name, "SetLocalizationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        pq7.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        pq7.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
