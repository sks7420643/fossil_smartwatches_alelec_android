package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.fossil.cl1;
import com.fossil.cl7;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.WorkoutState;
import com.fossil.il7;
import com.fossil.lp1;
import com.fossil.lr1;
import com.fossil.nm0;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseVerifySecretKeySubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SyncSession extends EnableMaintainingSession implements ISyncSession, IExchangeKeySession, IReadWorkoutStateSession, IVerifySecretKeySession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public ThemeConfig themeConfig;
    @DexIgnore
    public UserProfile userProfile;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadCurrentWorkoutSession extends BleStateAbs {
        @DexIgnore
        public qy1<lp1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadCurrentWorkoutSession() {
            super(SyncSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<lp1> readCurrentWorkoutSession = SyncSession.this.getBleAdapter().readCurrentWorkoutSession(SyncSession.this.getLogSession(), this);
            this.task = readCurrentWorkoutSession;
            if (readCurrentWorkoutSession != null) {
                startTimeout();
                return true;
            } else if (SyncSession.this.getBleAdapter().isSupportedFeature(lr1.class) != null) {
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState((SyncSession) BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
                return true;
            } else {
                SyncSession syncSession2 = SyncSession.this;
                syncSession2.enterStateAsync(syncSession2.createConcreteState((SyncSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadCurrentWorkoutSessionFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SyncSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadCurrentWorkoutSessionSuccess(lp1 lp1) {
            pq7.c(lp1, "workoutSession");
            BleSession.BleSessionCallback bleSessionCallback = SyncSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReadCurrentWorkoutBeforeSyncSuccess(SyncSession.this.getSerial(), nm0.a(new cl7(Constants.CURRENT_WORKOUT_SESSION, lp1)));
            }
            if (lp1.getWorkoutState() == WorkoutState.END) {
                stopTimeout();
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState((SyncSession) BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
            } else if (SyncSession.this.userProfile.getOriginalSyncMode() == 15 || SyncSession.this.userProfile.getOriginalSyncMode() == 12) {
                stopTimeout();
                BleSession.BleSessionCallback bleSessionCallback2 = SyncSession.this.getBleSessionCallback();
                if (bleSessionCallback2 != null) {
                    bleSessionCallback2.onAskForStopWorkout(SyncSession.this.getSerial());
                }
            } else {
                stopTimeout();
                setMaxRetries(0);
                SyncSession.this.errorLog((SyncSession) "Read current workout session; workout is running", (String) ErrorCodeBuilder.Step.READ_WORKOUT, (ErrorCodeBuilder.Step) ErrorCodeBuilder.AppError.UNKNOWN);
                SyncSession.this.stop(FailureCode.FAILED_TO_WORKOUT_IS_RUNNING);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<lp1> qy1 = this.task;
            if (qy1 == null) {
                SyncSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public StopCurrentWorkoutState() {
            super(SyncSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> stopCurrentWorkoutSession = SyncSession.this.getBleAdapter().stopCurrentWorkoutSession(SyncSession.this.getLogSession(), this);
            this.task = stopCurrentWorkoutSession;
            if (stopCurrentWorkoutSession == null) {
                SyncSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SyncSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            SyncSession syncSession = SyncSession.this;
            syncSession.enterStateAsync(syncSession.createConcreteState((SyncSession) BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 == null) {
                SyncSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
            } else if (qy1 != null) {
                us1.a(qy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferDataSubFlow() {
            super(SyncSession.this.getTAG(), SyncSession.this, SyncSession.this.syncLog, SyncSession.this.getLogSession(), SyncSession.this.getSerial(), SyncSession.this.getBleAdapter(), SyncSession.this.userProfile, SyncSession.this.getBleSessionCallback(), true);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i != 0) {
                SyncSession.this.stop(i);
                return;
            }
            SyncSession syncSession = SyncSession.this;
            syncSession.enterStateAsync(syncSession.createConcreteState((SyncSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferSettingsSubFlow() {
            super(SyncSession.this.getTAG(), SyncSession.this, SyncSession.this.syncLog, SyncSession.this.getLogSession(), SyncSession.this.isFullSync(), SyncSession.this.getSerial(), SyncSession.this.getBleAdapter(), SyncSession.this.userProfile, SyncSession.this.multiAlarmSettings, SyncSession.this.complicationAppMappingSettings, SyncSession.this.watchAppMappingSettings, SyncSession.this.backgroundConfig, SyncSession.this.themeConfig, SyncSession.this.notificationFilterSettings, SyncSession.this.localizationData, SyncSession.this.microAppMappings, SyncSession.this.secondTimezoneOffset, SyncSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            SyncSession.this.stop(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BaseVerifySecretKeySubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public VerifySecretKeySessionState() {
            super(CommunicateMode.LINK, SyncSession.this.getTAG(), SyncSession.this, SyncSession.this.getMfLog(), SyncSession.this.getLogSession(), SyncSession.this.getSerial(), SyncSession.this.getBleAdapter(), SyncSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "VerifySecretKeySessionState onStop failureCode " + i);
            if (i == cl1.REQUEST_UNSUPPORTED.getCode() || i == cl1.UNSUPPORTED_FORMAT.getCode() || i == 0) {
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState((SyncSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return;
            }
            SyncSession.this.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, UserProfile userProfile2) {
        super(SessionType.SYNC, CommunicateMode.SYNC, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapter");
        pq7.c(userProfile2, "userProfile");
        this.userProfile = userProfile2;
        setLogSession(FLogger.Session.SYNC);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        pq7.c(bleSession, "bleSession");
        if (bleSession.getCommunicateMode() == CommunicateMode.SYNC) {
            return ((SyncSession) bleSession).getSyncMode() == 13;
        }
        if (getCommunicateMode() != bleSession.getCommunicateMode()) {
            return true;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        MFLog end = MFLogManager.getInstance(getBleAdapter().getContext()).end(CommunicateMode.SYNC, getSerial());
        getExtraInfoReturned().putInt(ButtonService.Companion.getLOG_ID(), end != null ? end.getStartTimeEpoch() : 0);
        getExtraInfoReturned().putLong(com.misfit.frameworks.common.constants.Constants.SYNC_ID, this.userProfile.getSyncId());
        getExtraInfoReturned().putInt(ButtonService.Companion.getSYNC_MODE(), this.userProfile.getSyncMode());
        getExtraInfoReturned().putInt(ButtonService.Companion.getORIGINAL_SYNC_MODE(), this.userProfile.getOriginalSyncMode());
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession
    public void confirmStopWorkout(String str, boolean z) {
        pq7.c(str, "serial");
        if (!(getCurrentState() instanceof ReadCurrentWorkoutSession)) {
            return;
        }
        if (z) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
            return;
        }
        errorLog("User canceled stop workout", ErrorCodeBuilder.Step.STOP_WORKOUT, ErrorCodeBuilder.AppError.USER_CANCELLED);
        stop(FailureCode.USER_CANCELLED);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SyncSession syncSession = new SyncSession(getBleAdapter(), getBleSessionCallback(), this.userProfile);
        syncSession.setDevice(getDevice());
        return syncSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData currentState " + getCurrentState());
        if (!(getCurrentState() instanceof TransferDataSubFlow)) {
            return new ArrayList();
        }
        BleState currentState = getCurrentState();
        if (currentState != null) {
            return ((TransferDataSubFlow) currentState).getSyncData();
        }
        throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferDataSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISyncSession
    public int getSyncMode() {
        return this.userProfile.getSyncMode();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
        this.themeConfig = DevicePreferenceUtils.getThemeConfig(getContext(), getSerial());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE;
        String name = ReadCurrentWorkoutSession.class.getName();
        pq7.b(name, "ReadCurrentWorkoutSession::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name2 = StopCurrentWorkoutState.class.getName();
        pq7.b(name2, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.VERIFY_SECRET_KEY;
        String name3 = VerifySecretKeySessionState.class.getName();
        pq7.b(name3, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name4 = TransferDataSubFlow.class.getName();
        pq7.b(name4, "TransferDataSubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name5 = TransferSettingsSubFlow.class.getName();
        pq7.b(name5, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return getSyncMode() == 13;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession
    public void onReceiveServerResponse(boolean z) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerResponse isSuccess " + z);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceivePushSecretKeyResponse(z);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public boolean onStart(Object... objArr) {
        pq7.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        MFSyncLog startSyncLog = MFLogManager.getInstance(getBleAdapter().getContext()).startSyncLog(getSerial());
        this.syncLog = startSyncLog;
        setMfLog(startSyncLog);
        MFSyncLog mFSyncLog = this.syncLog;
        if (mFSyncLog != null) {
            if (mFSyncLog != null) {
                mFSyncLog.setSerial(getSerial());
                MFSyncLog mFSyncLog2 = this.syncLog;
                if (mFSyncLog2 != null) {
                    mFSyncLog2.setSdkVersion(ButtonService.Companion.getSDKVersion());
                    MFSyncLog mFSyncLog3 = this.syncLog;
                    if (mFSyncLog3 != null) {
                        mFSyncLog3.setFirmwareVersion(getBleAdapter().getFirmwareVersion());
                        MFSyncLog mFSyncLog4 = this.syncLog;
                        if (mFSyncLog4 != null) {
                            mFSyncLog4.setSyncMode(getSyncMode());
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        log("Start new sync session: newDevice=" + this.userProfile.isNewDevice() + ", syncMode=" + this.userProfile.getSyncMode() + ", bluetoothOn=" + BluetoothUtils.isBluetoothEnable() + ", locationServiceEnabled=" + LocationUtils.isLocationEnable(getContext()) + ", locationPermissionAllowed=" + LocationUtils.isLocationPermissionGranted(getContext()));
        return super.onStart(Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onWatchAppFilesReady(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onWatchAppFileReady, isSuccess = " + z);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setWatchAppFiles(z);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        pq7.c(str, "serial");
        pq7.c(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.SYNC;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISyncSession
    public void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        pq7.c(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - currentState=" + getCurrentState());
        if (getCurrentState() instanceof TransferDataSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferDataSubFlow) currentState).updateCurrentStepAndStepGoalFromApp(z, userProfile2);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferDataSubFlow");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal to device caused by current state null or not an instance of TransferDataSubFlow.");
    }
}
