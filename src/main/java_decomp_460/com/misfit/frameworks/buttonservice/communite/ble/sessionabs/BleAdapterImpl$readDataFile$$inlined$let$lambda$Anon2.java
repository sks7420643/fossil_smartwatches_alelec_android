package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.fitness.FitnessData;
import com.fossil.nm1;
import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2 extends qq7 implements rp7<FitnessData[], tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ nm1 $biometricProfile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, nm1 nm1, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$biometricProfile$inlined = nm1;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(FitnessData[] fitnessDataArr) {
        invoke(fitnessDataArr);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(FitnessData[] fitnessDataArr) {
        pq7.c(fitnessDataArr, "it");
        this.this$0.log(this.$logSession$inlined, "Read Data Files Success");
        this.$callback$inlined.onReadDataFilesSuccess(fitnessDataArr);
    }
}
