package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.fitness.FitnessData;
import com.fossil.lp1;
import com.fossil.uk1;
import com.fossil.yk1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zk1;
import com.fossil.zm1;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(yx1 yx1);

    @DexIgnore
    Object onApplyHandPositionSuccess();  // void declaration

    @DexIgnore
    void onApplyThemeError(yx1 yx1);

    @DexIgnore
    void onApplyThemeProgressChanged(float f);

    @DexIgnore
    void onApplyThemeSuccess(byte[] bArr);

    @DexIgnore
    void onAuthenticateDeviceFail(yx1 yx1);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    Object onAuthorizeDeviceFailed();  // void declaration

    @DexIgnore
    Object onAuthorizeDeviceSuccess();  // void declaration

    @DexIgnore
    void onConfigureMicroAppFail(yx1 yx1);

    @DexIgnore
    Object onConfigureMicroAppSuccess();  // void declaration

    @DexIgnore
    Object onDataTransferCompleted();  // void declaration

    @DexIgnore
    void onDataTransferFailed(yx1 yx1);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(yk1 yk1, int i);

    @DexIgnore
    void onEraseDataFilesFailed(yx1 yx1);

    @DexIgnore
    Object onEraseDataFilesSuccess();  // void declaration

    @DexIgnore
    void onEraseHWLogFailed(yx1 yx1);

    @DexIgnore
    Object onEraseHWLogSuccess();  // void declaration

    @DexIgnore
    void onExchangeSecretKeyFail(yx1 yx1);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(yx1 yx1);

    @DexIgnore
    void onFetchDeviceInfoSuccess(zk1 zk1);

    @DexIgnore
    void onGetDeviceConfigFailed(yx1 yx1);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<zm1, ym1> hashMap);

    @DexIgnore
    Object onGetWatchParamsFail();  // void declaration

    @DexIgnore
    void onMoveHandFailed(yx1 yx1);

    @DexIgnore
    Object onMoveHandSuccess();  // void declaration

    @DexIgnore
    Object onNextSession();  // void declaration

    @DexIgnore
    void onNotifyNotificationEventFailed(yx1 yx1);

    @DexIgnore
    Object onNotifyNotificationEventSuccess();  // void declaration

    @DexIgnore
    void onPlayAnimationFail(yx1 yx1);

    @DexIgnore
    Object onPlayAnimationSuccess();  // void declaration

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, yx1 yx1);

    @DexIgnore
    void onPreviewThemeError(yx1 yx1);

    @DexIgnore
    void onPreviewThemeProgressChanged(float f);

    @DexIgnore
    Object onPreviewThemeSuccess();  // void declaration

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(yx1 yx1);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(lp1 lp1);

    @DexIgnore
    void onReadDataFilesFailed(yx1 yx1);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(yx1 yx1);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    Object onReadHWLogSuccess();  // void declaration

    @DexIgnore
    void onReadRssiFailed(yx1 yx1);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(yx1 yx1);

    @DexIgnore
    Object onReleaseHandControlSuccess();  // void declaration

    @DexIgnore
    void onRequestHandControlFailed(yx1 yx1);

    @DexIgnore
    Object onRequestHandControlSuccess();  // void declaration

    @DexIgnore
    void onResetHandsFailed(yx1 yx1);

    @DexIgnore
    Object onResetHandsSuccess();  // void declaration

    @DexIgnore
    void onScanFail(uk1 uk1);

    @DexIgnore
    void onSendMicroAppDataFail(yx1 yx1);

    @DexIgnore
    Object onSendMicroAppDataSuccess();  // void declaration

    @DexIgnore
    void onSendingEncryptedDataSessionFailed(yx1 yx1);

    @DexIgnore
    Object onSendingEncryptedDataSessionSuccess();  // void declaration

    @DexIgnore
    void onSetAlarmFailed(yx1 yx1);

    @DexIgnore
    Object onSetAlarmSuccess();  // void declaration

    @DexIgnore
    void onSetBackgroundImageFailed(yx1 yx1);

    @DexIgnore
    Object onSetBackgroundImageSuccess();  // void declaration

    @DexIgnore
    void onSetComplicationFailed(yx1 yx1);

    @DexIgnore
    Object onSetComplicationSuccess();  // void declaration

    @DexIgnore
    void onSetDeviceConfigFailed(yx1 yx1);

    @DexIgnore
    Object onSetDeviceConfigSuccess();  // void declaration

    @DexIgnore
    void onSetFrontLightFailed(yx1 yx1);

    @DexIgnore
    Object onSetFrontLightSuccess();  // void declaration

    @DexIgnore
    void onSetLabelFileFailed(yx1 yx1);

    @DexIgnore
    Object onSetLabelFileSuccess();  // void declaration

    @DexIgnore
    void onSetLocalizationDataFail(yx1 yx1);

    @DexIgnore
    Object onSetLocalizationDataSuccess();  // void declaration

    @DexIgnore
    void onSetMinimumStepThresholdSessionFailed(yx1 yx1);

    @DexIgnore
    Object onSetMinimumStepThresholdSessionSuccess();  // void declaration

    @DexIgnore
    void onSetNotificationFilterFailed(yx1 yx1);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    Object onSetNotificationFilterSuccess();  // void declaration

    @DexIgnore
    void onSetReplyMessageMappingError(yx1 yx1);

    @DexIgnore
    Object onSetReplyMessageMappingSuccess();  // void declaration

    @DexIgnore
    void onSetWatchAppFailed(yx1 yx1);

    @DexIgnore
    Object onSetWatchAppFileFailed();  // void declaration

    @DexIgnore
    void onSetWatchAppFileProgressChanged(float f);

    @DexIgnore
    Object onSetWatchAppFileSuccess();  // void declaration

    @DexIgnore
    Object onSetWatchAppSuccess();  // void declaration

    @DexIgnore
    void onSetWatchParamsFail(yx1 yx1);

    @DexIgnore
    Object onSetWatchParamsSuccess();  // void declaration

    @DexIgnore
    void onSetWorkoutGPSDataSessionFailed(yx1 yx1);

    @DexIgnore
    Object onSetWorkoutGPSDataSessionSuccess();  // void declaration

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(yx1 yx1);

    @DexIgnore
    Object onStopCurrentWorkoutSessionSuccess();  // void declaration

    @DexIgnore
    void onVerifySecretKeyFail(yx1 yx1);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
