package com.misfit.frameworks.buttonservice.communite;

import android.content.Context;
import com.fossil.er7;
import com.fossil.fs7;
import com.fossil.kq7;
import com.fossil.nq7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.rp7;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommunicateManager {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, BleCommunicator> bleCommunicators;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public BleCommunicator currentCommunicator;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion extends SingletonHolder<CommunicateManager, Context> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class Anon1 extends nq7 implements rp7<Context, CommunicateManager> {
            @DexIgnore
            public static /* final */ Anon1 INSTANCE; // = new Anon1();

            @DexIgnore
            public Anon1() {
                super(1);
            }

            @DexIgnore
            @Override // com.fossil.gq7, com.fossil.ds7
            public final String getName() {
                return "<init>";
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public final fs7 getOwner() {
                return er7.b(CommunicateManager.class);
            }

            @DexIgnore
            @Override // com.fossil.gq7
            public final String getSignature() {
                return "<init>(Landroid/content/Context;)V";
            }

            @DexIgnore
            public final CommunicateManager invoke(Context context) {
                pq7.c(context, "p1");
                return new CommunicateManager(context, null);
            }
        }

        @DexIgnore
        public Companion() {
            super(Anon1.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    /*
    static {
        String simpleName = CommunicateManager.class.getSimpleName();
        pq7.b(simpleName, "CommunicateManager::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CommunicateManager(Context context2) {
        this.context = context2;
        this.bleCommunicators = new ConcurrentHashMap<>();
    }

    @DexIgnore
    public /* synthetic */ CommunicateManager(Context context2, kq7 kq7) {
        this(context2);
    }

    @DexIgnore
    private final BleCommunicator createDeviceCommunicator(Context context2, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        return new DeviceCommunicator(context2, str, str2, communicationResultCallback);
    }

    @DexIgnore
    public final void clearCommunicatorSessionQueue(String str) {
        pq7.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator != null) {
            bleCommunicator.clearSessionQueue();
        }
    }

    @DexIgnore
    public final void clearCurrentCommunicatorSessionQueueIfNot(String str) {
        pq7.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        BleCommunicator bleCommunicator2 = this.currentCommunicator;
        if (bleCommunicator != bleCommunicator2 && bleCommunicator2 != null) {
            if (bleCommunicator2 != null) {
                bleCommunicator2.clearSessionQueue();
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int getBatteryLevel(String str) {
        pq7.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return -1;
        }
        pq7.b(bleCommunicator, "bleCommunicators[serial] ?: return -1");
        return bleCommunicator.getBleAdapter().getBatteryLevel();
    }

    @DexIgnore
    public final ConcurrentHashMap<String, BleCommunicator> getBleCommunicators() {
        return this.bleCommunicators;
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        pq7.c(str, "serial");
        pq7.c(communicationResultCallback, "communicationResultCallback");
        String macAddress = DeviceUtils.getInstance(this.context).getMacAddress(this.context, str);
        pq7.b(macAddress, "DeviceUtils.getInstance(\u2026cAddress(context, serial)");
        return getCommunicator(str, macAddress, communicationResultCallback);
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        BleCommunicator bleCommunicator;
        synchronized (this) {
            pq7.c(str, "serial");
            pq7.c(str2, "macAddress");
            pq7.c(communicationResultCallback, "communicationResultCallback");
            bleCommunicator = this.bleCommunicators.get(str);
            if (bleCommunicator == null) {
                bleCommunicator = createDeviceCommunicator(this.context, str, str2, communicationResultCallback);
                this.bleCommunicators.put(str, bleCommunicator);
            }
            this.currentCommunicator = bleCommunicator;
        }
        return bleCommunicator;
    }

    @DexIgnore
    public final String getLocale(String str) {
        pq7.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return "";
        }
        pq7.b(bleCommunicator, "bleCommunicators[serial] ?: return \"\"");
        return bleCommunicator.getBleAdapter().getLocale();
    }

    @DexIgnore
    public final List<BleCommunicator> getRunningCommunicator() {
        Collection<BleCommunicator> values = this.bleCommunicators.values();
        pq7.b(values, "bleCommunicators.values");
        ArrayList arrayList = new ArrayList();
        for (T t : values) {
            if (t.isRunning()) {
                arrayList.add(t);
            }
        }
        return pm7.j0(arrayList);
    }

    @DexIgnore
    public final void removeCommunicator(String str) {
        pq7.c(str, "serial");
        this.bleCommunicators.remove(str);
    }
}
