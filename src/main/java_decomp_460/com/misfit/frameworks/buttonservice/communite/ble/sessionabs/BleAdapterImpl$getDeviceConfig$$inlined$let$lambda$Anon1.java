package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.fossil.ym1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1 extends qq7 implements rp7<HashMap<zm1, ym1>, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(HashMap<zm1, ym1> hashMap) {
        invoke(hashMap);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(HashMap<zm1, ym1> hashMap) {
        pq7.c(hashMap, "it");
        this.this$0.log(this.$logSession$inlined, "Get Device Configuration Success");
        this.this$0.mDeviceConfiguration = hashMap;
        this.$callback$inlined.onGetDeviceConfigSuccess(hashMap);
    }
}
