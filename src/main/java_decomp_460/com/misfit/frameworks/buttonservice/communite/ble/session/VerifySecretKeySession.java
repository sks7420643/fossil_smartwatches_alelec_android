package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseVerifySecretKeySubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VerifySecretKeySession extends QuickResponseSession implements IExchangeKeySession, IVerifySecretKeySession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BaseVerifySecretKeySubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public VerifySecretKeySessionState() {
            super(CommunicateMode.VERIFY_SECRET_KEY_SESSION, VerifySecretKeySession.this.getTAG(), VerifySecretKeySession.this, VerifySecretKeySession.this.getMfLog(), VerifySecretKeySession.this.getLogSession(), VerifySecretKeySession.this.getSerial(), VerifySecretKeySession.this.getBleAdapter(), VerifySecretKeySession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "VerifySecretKeySessionState onStop failureCode " + i);
            VerifySecretKeySession.this.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifySecretKeySession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.VERIFY_SECRET_KEY_SESSION, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        pq7.c(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.VERIFY_SECRET_KEY_SESSION) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        VerifySecretKeySession verifySecretKeySession = new VerifySecretKeySession(getBleAdapter(), getBleSessionCallback());
        verifySecretKeySession.setDevice(getDevice());
        return verifySecretKeySession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.VERIFY_SECRET_KEY_STATE;
        String name = VerifySecretKeySessionState.class.getName();
        pq7.b(name, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onPing() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveCurrentSecretKey(byte[] bArr) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession
    public void onReceiveServerResponse(boolean z) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerResponse isSuccess " + z);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceivePushSecretKeyResponse(z);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }
}
