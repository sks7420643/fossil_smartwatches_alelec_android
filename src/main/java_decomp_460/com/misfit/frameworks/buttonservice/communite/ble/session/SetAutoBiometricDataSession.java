package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.js1;
import com.fossil.ln1;
import com.fossil.nm1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoBiometricDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public UserBiometricData mNewBiometricData;
    @DexIgnore
    public UserBiometricData mOldBiometricData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoBiometricDataSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBiometricDataState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBiometricDataState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            ln1 ln1 = new ln1();
            try {
                nm1 sDKBiometricProfile = SetAutoBiometricDataSession.this.mNewBiometricData.toSDKBiometricProfile();
                ln1.d(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
                setAutoBiometricDataSession.log("Set Biometric Data: exception=" + e.getMessage());
            }
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.log("Set Biometric Data, " + SetAutoBiometricDataSession.this.mNewBiometricData);
            qy1<zm1[]> deviceConfig = SetAutoBiometricDataSession.this.getBleAdapter().setDeviceConfig(SetAutoBiometricDataSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetAutoBiometricDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, true);
            SetAutoBiometricDataSession.this.stop(FailureCode.FAILED_TO_SET_BIOMETRIC_DATA);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, false);
            SetAutoBiometricDataSession setAutoBiometricDataSession2 = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession2.enterStateAsync(setAutoBiometricDataSession2.createConcreteState((SetAutoBiometricDataSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBiometricDataSession(UserBiometricData userBiometricData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BIOMETRIC_DATA, bleAdapterImpl, bleSessionCallback);
        pq7.c(userBiometricData, "mNewBiometricData");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.mNewBiometricData = userBiometricData;
    }

    @DexIgnore
    private final void storeBiometricData(UserBiometricData userBiometricData, boolean z) {
        DevicePreferenceUtils.setAutoBiometricSettings(getBleAdapter().getContext(), new Gson().t(userBiometricData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BIOMETRIC);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoBiometricDataSession setAutoBiometricDataSession = new SetAutoBiometricDataSession(this.mNewBiometricData, getBleAdapter(), getBleSessionCallback());
        setAutoBiometricDataSession.setDevice(getDevice());
        return setAutoBiometricDataSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState createConcreteState;
        super.initSettings();
        this.mOldBiometricData = DevicePreferenceUtils.getAutoBiometricSettings(getContext());
        if (getBleAdapter().isSupportedFeature(js1.class) == null) {
            log("This device does not support set complication apps.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (UserBiometricData.CREATOR.isSame(this.mOldBiometricData, this.mNewBiometricData)) {
            log("New biometric data and old biometric data are the same. No need to set again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            storeBiometricData(this.mNewBiometricData, true);
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE);
        }
        setStartState(createConcreteState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE;
        String name = SetBiometricDataState.class.getName();
        pq7.b(name, "SetBiometricDataState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        pq7.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        pq7.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
