package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.oy1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.ru1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.buttonservice.utils.WatchAppUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SendingEncryptedDataSession extends EnableMaintainingSession {
    @DexIgnore
    public boolean isBcOn;
    @DexIgnore
    public /* final */ byte[] mEncryptedData;
    @DexIgnore
    public SharePreferencesUtils shared;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SendingEncryptedDataState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SendingEncryptedDataState() {
            super(SendingEncryptedDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> sendingEncryptedData = SendingEncryptedDataSession.this.getBleAdapter().sendingEncryptedData(SendingEncryptedDataSession.this.mEncryptedData, SendingEncryptedDataSession.this.getLogSession(), this);
            this.task = sendingEncryptedData;
            if (sendingEncryptedData == null) {
                SendingEncryptedDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSendingEncryptedDataSessionFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SENDING_ENCRYPTED_DATA_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSendingEncryptedDataSessionSuccess() {
            stopTimeout();
            SendingEncryptedDataSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppFiles extends BleStateAbs {
        @DexIgnore
        public oy1<ru1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppFiles() {
            super(SendingEncryptedDataSession.this.getTAG());
        }

        @DexIgnore
        private final ru1[] prepareWatchAppFiles() {
            return WatchAppUtils.INSTANCE.getWatchAppFiles(SendingEncryptedDataSession.this.getBleAdapter().getContext());
        }

        @DexIgnore
        private final BleState sendingEncryptedDataState() {
            return SendingEncryptedDataSession.this.createConcreteState((SendingEncryptedDataSession) BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0084  */
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onEnter() {
            /*
                r9 = this;
                r1 = 0
                r3 = 0
                r4 = 1
                super.onEnter()
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.String r2 = "Enter Set Watch App Files"
                r0.log(r2)
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.String r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.access$getSerial$p(r0)
                boolean r0 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r0)
                if (r0 == 0) goto L_0x008c
                com.fossil.ru1[] r2 = r9.prepareWatchAppFiles()
                if (r2 == 0) goto L_0x007c
                int r6 = r2.length
                r5 = r3
            L_0x0021:
                if (r5 >= r6) goto L_0x007c
                r0 = r2[r5]
                java.lang.String r7 = r0.getBundleId()
                java.lang.String r8 = "buddyChallengeApp"
                boolean r7 = com.fossil.wt7.u(r7, r8, r4)
                if (r7 == 0) goto L_0x0078
            L_0x0031:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r5 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                boolean r5 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.access$isBcOn$p(r5)
                if (r5 == 0) goto L_0x0098
                if (r0 != 0) goto L_0x0098
            L_0x003b:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r5 = "Enter Set Watch App Files - watchApps: "
                r2.append(r5)
                r2.append(r1)
                java.lang.String r2 = r2.toString()
                r0.log(r2)
                if (r1 == 0) goto L_0x0059
                int r0 = r1.length
                if (r0 != 0) goto L_0x007e
                r0 = r4
            L_0x0057:
                if (r0 == 0) goto L_0x0096
            L_0x0059:
                r0 = r4
            L_0x005a:
                if (r0 != 0) goto L_0x0084
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.access$getBleAdapter$p(r0)
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r2 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.access$getLogSession$p(r2)
                com.fossil.oy1 r0 = r0.setWatchAppFiles(r2, r1, r9)
                r9.task = r0
                if (r0 != 0) goto L_0x0080
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.stop(r1)
            L_0x0077:
                return r4
            L_0x0078:
                int r0 = r5 + 1
                r5 = r0
                goto L_0x0021
            L_0x007c:
                r0 = r1
                goto L_0x0031
            L_0x007e:
                r0 = r3
                goto L_0x0057
            L_0x0080:
                r9.startTimeout()
                goto L_0x0077
            L_0x0084:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                r1 = 1935(0x78f, float:2.712E-42)
                r0.stop(r1)
                goto L_0x0077
            L_0x008c:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.communite.ble.BleState r1 = r9.sendingEncryptedDataState()
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.access$enterSendingEncryptedState(r0, r1)
                goto L_0x0077
            L_0x0096:
                r0 = r3
                goto L_0x005a
            L_0x0098:
                r1 = r2
                goto L_0x003b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.SetWatchAppFiles.onEnter():boolean");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileFailed() {
            stopTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files Failed");
            SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileSuccess() {
            stopTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files success");
            SendingEncryptedDataSession.access$getShared$p(SendingEncryptedDataSession.this).setBoolean(SharePreferencesUtils.BC_STATUS, true);
            SharePreferencesUtils access$getShared$p = SendingEncryptedDataSession.access$getShared$p(SendingEncryptedDataSession.this);
            access$getShared$p.setBoolean(SharePreferencesUtils.BC_APP_READY + SendingEncryptedDataSession.this.getSerial(), true);
            SendingEncryptedDataSession.this.enterSendingEncryptedState(sendingEncryptedDataState());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files Timeout");
            oy1<ru1[]> oy1 = this.task;
            if (oy1 == null) {
                SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
            } else if (oy1 != null) {
                us1.a(oy1);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SendingEncryptedDataSession(byte[] bArr, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.SENDING_ENCRYPTED_DATA_SESSION, bleAdapterImpl, bleSessionCallback);
        pq7.c(bArr, "mEncryptedData");
        pq7.c(bleAdapterImpl, "bleAdapterV2");
        this.mEncryptedData = bArr;
    }

    @DexIgnore
    public static final /* synthetic */ SharePreferencesUtils access$getShared$p(SendingEncryptedDataSession sendingEncryptedDataSession) {
        SharePreferencesUtils sharePreferencesUtils = sendingEncryptedDataSession.shared;
        if (sharePreferencesUtils != null) {
            return sharePreferencesUtils;
        }
        pq7.n("shared");
        throw null;
    }

    @DexIgnore
    private final void enterSendingEncryptedState(BleState bleState) {
        enterStateAsync(bleState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SendingEncryptedDataSession sendingEncryptedDataSession = new SendingEncryptedDataSession(this.mEncryptedData, getBleAdapter(), getBleSessionCallback());
        sendingEncryptedDataSession.setDevice(getDevice());
        return sendingEncryptedDataSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        SharePreferencesUtils sharePreferencesUtils = this.shared;
        if (sharePreferencesUtils != null) {
            Boolean bool = sharePreferencesUtils.getBoolean(SharePreferencesUtils.BC_APP_READY + getSerial(), false);
            log("stateAfterEnableMaintainingConnection - isBcOn: " + this.isBcOn + " - bcAppReady: " + bool);
            if (this.isBcOn && !bool.booleanValue()) {
                return createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
            }
            getSessionStateMap().remove(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
            return createConcreteState(BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE);
        }
        pq7.n("shared");
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        SharePreferencesUtils instance = SharePreferencesUtils.getInstance(getContext());
        pq7.b(instance, "SharePreferencesUtils.getInstance(context)");
        this.shared = instance;
        if (instance != null) {
            Boolean bool = instance.getBoolean(SharePreferencesUtils.BC_STATUS, true);
            pq7.b(bool, "shared.getBoolean(ShareP\u2026cesUtils.BC_STATUS, true)");
            this.isBcOn = bool.booleanValue();
            SharePreferencesUtils sharePreferencesUtils = this.shared;
            if (sharePreferencesUtils != null) {
                Boolean bool2 = sharePreferencesUtils.getBoolean(SharePreferencesUtils.BC_APP_READY + getSerial(), false);
                log("initStateMap - isBcOn: " + this.isBcOn + " - bcAppReady: " + bool2);
                if (this.isBcOn && !bool2.booleanValue()) {
                    HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
                    BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
                    String name = SetWatchAppFiles.class.getName();
                    pq7.b(name, "SetWatchAppFiles::class.java.name");
                    sessionStateMap.put(sessionState, name);
                }
                HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
                BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE;
                String name2 = SendingEncryptedDataState.class.getName();
                pq7.b(name2, "SendingEncryptedDataState::class.java.name");
                sessionStateMap2.put(sessionState2, name2);
                return;
            }
            pq7.n("shared");
            throw null;
        }
        pq7.n("shared");
        throw null;
    }
}
