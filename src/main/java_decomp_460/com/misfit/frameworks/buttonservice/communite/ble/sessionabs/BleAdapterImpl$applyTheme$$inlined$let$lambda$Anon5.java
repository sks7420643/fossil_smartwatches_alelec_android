package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon5 extends qq7 implements rp7<Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeData $themeData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon5(BleAdapterImpl bleAdapterImpl, ThemeData themeData, ISessionSdkCallback iSessionSdkCallback, FLogger.Session session) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$themeData$inlined = themeData;
        this.$callback$inlined = iSessionSdkCallback;
        this.$logSession$inlined = session;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Float f) {
        invoke(f.floatValue());
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(float f) {
        this.$callback$inlined.onApplyThemeProgressChanged(f);
    }
}
