package com.misfit.frameworks.buttonservice.communite.ble;

import android.os.Bundle;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicator$bleSessionCallback$Anon1 implements BleSession.BleSessionCallback {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicator this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public BleCommunicator$bleSessionCallback$Anon1(BleCommunicator bleCommunicator) {
        this.this$0 = bleCommunicator;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void broadcastExchangeSecretKeySuccess(String str, String str2) {
        pq7.c(str, "serial");
        pq7.c(str2, "secretKey");
        this.this$0.getCommunicationResultCallback().onExchangeSecretKeySuccess(str, str2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForCurrentSecretKey(String str) {
        pq7.c(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForCurrentSecretKey(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForLabelFile(String str, CommunicateMode communicateMode) {
        pq7.c(str, "serial");
        pq7.c(communicateMode, "communicateMode");
        this.this$0.getCommunicationResultCallback().onAskForLabelFile(str, communicateMode);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForLinkServer(CommunicateMode communicateMode, Bundle bundle) {
        pq7.c(communicateMode, "communicateMode");
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK || this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.SWITCH_DEVICE) {
            this.this$0.getCommunicationResultCallback().onAskForLinkServer(this.this$0.getSerial(), communicateMode, bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForRandomKey(String str) {
        pq7.c(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForRandomKey(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForSecretKey(Bundle bundle) {
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onAskForServerSecretKey(this.this$0.getSerial(), bundle);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForStopWorkout(String str) {
        pq7.c(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForStopWorkout(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAskForWatchAppFiles(String str, Bundle bundle) {
        pq7.c(str, "serial");
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onAskForWatchAppFiles(str, bundle);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onAuthorizeDeviceSuccess(String str) {
        pq7.c(str, "serial");
        this.this$0.getCommunicationResultCallback().onAuthorizeDeviceSuccess(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onBleStateResult(int i, Bundle bundle) {
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (!BleSession.Companion.isNull(this.this$0.getCurrentSession())) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(this.this$0.getCurrentSession().getCommunicateMode(), this.this$0.getSerial(), i, new ArrayList(), bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onFirmwareLatest() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onFirmwareLatest(this.this$0.getSerial());
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onNeedStartTimer(String str) {
        pq7.c(str, "serial");
        this.this$0.getCommunicationResultCallback().onNeedStartTimer(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onReadCurrentWorkoutBeforeSyncSuccess(String str, Bundle bundle) {
        pq7.c(str, "serial");
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onReadCurrentWorkoutSuccess(str, bundle);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onReceivedSyncData(Bundle bundle) {
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.SYNC) {
            this.this$0.getCommunicationResultCallback().onReceivedSyncData(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onRequestLatestFirmware(Bundle bundle) {
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onRequestLatestFirmware(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onRequestLatestWatchParams(String str, Bundle bundle) {
        pq7.c(str, "serial");
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onRequestLatestWatchParams(str, bundle);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onRequestPushSecretKeyToServer(String str, String str2) {
        pq7.c(str, "serial");
        pq7.c(str2, "secretKey");
        this.this$0.getCommunicationResultCallback().onRequestPushSecretKeyToServer(str, str2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onStop(int i, List<Integer> list, Bundle bundle, BleSession bleSession) {
        pq7.c(list, "requiredPermissionCodes");
        pq7.c(bundle, Mapping.COLUMN_EXTRA_INFO);
        pq7.c(bleSession, Constants.SESSION);
        if (pq7.a(bleSession, this.this$0.getCurrentSession()) || bleSession.requireBroadCastInAnyCase()) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(bleSession.getCommunicateMode(), this.this$0.getSerial(), i, list, bundle);
        }
        if (pq7.a(bleSession, this.this$0.getCurrentSession())) {
            this.this$0.setNullCurrentSession();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.this$0.getTAG();
            local.d(tag, "Inside " + this.this$0.getTAG() + ".bleSessionCallback.onStop");
            this.this$0.startSessionInQueue();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onUpdateFirmwareFailed() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareFailed(this.this$0.getSerial());
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback
    public void onUpdateFirmwareSuccess() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareSuccess(this.this$0.getSerial());
        }
    }
}
