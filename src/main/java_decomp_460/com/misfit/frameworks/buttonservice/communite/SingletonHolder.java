package com.misfit.frameworks.buttonservice.communite;

import com.fossil.pq7;
import com.fossil.rp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SingletonHolder<T, A> {
    @DexIgnore
    public rp7<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(rp7<? super A, ? extends T> rp7) {
        pq7.c(rp7, "creator");
        this.creator = rp7;
    }

    @DexIgnore
    public final T getInstance(A a2) {
        T t = this.instance;
        if (t == null) {
            synchronized (this) {
                t = this.instance;
                if (t == null) {
                    rp7<? super A, ? extends T> rp7 = this.creator;
                    if (rp7 != null) {
                        t = (T) rp7.invoke(a2);
                        this.instance = t;
                        this.creator = null;
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }
        return t;
    }
}
