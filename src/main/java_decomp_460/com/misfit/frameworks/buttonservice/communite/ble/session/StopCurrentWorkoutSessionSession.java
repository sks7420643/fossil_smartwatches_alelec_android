package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StopCurrentWorkoutSessionSession extends EnableMaintainingSession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public StopCurrentWorkoutState() {
            super(StopCurrentWorkoutSessionSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<tl7> stopCurrentWorkoutSession = StopCurrentWorkoutSessionSession.this.getBleAdapter().stopCurrentWorkoutSession(StopCurrentWorkoutSessionSession.this.getLogSession(), this);
            this.task = stopCurrentWorkoutSession;
            if (stopCurrentWorkoutSession == null) {
                StopCurrentWorkoutSessionSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            StopCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            StopCurrentWorkoutSessionSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
            StopCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StopCurrentWorkoutSessionSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        StopCurrentWorkoutSessionSession stopCurrentWorkoutSessionSession = new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback());
        stopCurrentWorkoutSessionSession.setDevice(getDevice());
        return stopCurrentWorkoutSessionSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name = StopCurrentWorkoutState.class.getName();
        pq7.b(name, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
