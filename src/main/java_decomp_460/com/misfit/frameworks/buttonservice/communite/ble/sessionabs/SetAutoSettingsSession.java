package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SetAutoSettingsSession extends EnableMaintainingSession {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoSettingsSession(CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.DEVICE_SETTING, communicateMode, bleAdapterImpl, bleSessionCallback);
        pq7.c(communicateMode, "communicateMode");
        pq7.c(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        pq7.c(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public abstract BleState getStartState();

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return getStartState();
    }
}
