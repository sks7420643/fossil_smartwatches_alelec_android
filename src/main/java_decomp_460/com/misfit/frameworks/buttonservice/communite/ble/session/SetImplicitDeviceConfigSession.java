package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.en1;
import com.fossil.ln1;
import com.fossil.mn1;
import com.fossil.nm1;
import com.fossil.on1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDeviceConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE);
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDeviceConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public qy1<zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDeviceConfigState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        private final ym1[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            short offset = (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60);
            ln1 ln1 = new ln1();
            ln1.r(j2, (short) ((int) (currentTimeMillis - (j * j2))), offset);
            ln1.k(SetImplicitDeviceConfigSession.this.userProfile.getCurrentSteps());
            ln1.l(SetImplicitDeviceConfigSession.this.userProfile.getGoalSteps());
            ln1.m(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinute());
            ln1.f(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinuteGoal());
            ln1.g(SetImplicitDeviceConfigSession.this.userProfile.getCalories());
            ln1.h(SetImplicitDeviceConfigSession.this.userProfile.getCaloriesGoal());
            ln1.i(SetImplicitDeviceConfigSession.this.userProfile.getDistanceInCentimeter());
            ln1.j(SetImplicitDeviceConfigSession.this.userProfile.getTotalSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getAwakeInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getLightSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getDeepSleepInMinute());
            UserDisplayUnit displayUnit = SetImplicitDeviceConfigSession.this.userProfile.getDisplayUnit();
            if (displayUnit != null) {
                ln1.n(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), mn1.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDeviceConfigSession.this.getBleAdapter().getContext()), on1.MONTH_DAY_YEAR);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No user display unit.");
                tl7 tl7 = tl7.f3441a;
            }
            InactiveNudgeData inactiveNudgeData = SetImplicitDeviceConfigSession.this.userProfile.getInactiveNudgeData();
            if (inactiveNudgeData != null) {
                ln1.p(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? en1.a.ENABLE : en1.a.DISABLE);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No inactive nudge config.");
                tl7 tl72 = tl7.f3441a;
            }
            try {
                nm1 sDKBiometricProfile = SetImplicitDeviceConfigSession.this.userProfile.getUserBiometricData().toSDKBiometricProfile();
                ln1.d(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
                setImplicitDeviceConfigSession.log("Set Device Config: exception=" + e.getMessage());
            }
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            qy1<zm1[]> deviceConfig = SetImplicitDeviceConfigSession.this.getBleAdapter().setDeviceConfig(SetImplicitDeviceConfigSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetImplicitDeviceConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SetImplicitDeviceConfigSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
            setImplicitDeviceConfigSession.enterStateAsync(setImplicitDeviceConfigSession.createConcreteState((SetImplicitDeviceConfigSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDeviceConfigSession.this.log("Set Device Config timeout. Cancel.");
            qy1<zm1[]> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDeviceConfigSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, bleAdapterImpl, bleSessionCallback);
        pq7.c(userProfile2, "userProfile");
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = new SetImplicitDeviceConfigSession(this.userProfile, getBleAdapter(), getBleSessionCallback());
        setImplicitDeviceConfigSession.setDevice(getDevice());
        return setImplicitDeviceConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE;
        String name = SetDeviceConfigState.class.getName();
        pq7.b(name, "SetDeviceConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        pq7.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        pq7.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
