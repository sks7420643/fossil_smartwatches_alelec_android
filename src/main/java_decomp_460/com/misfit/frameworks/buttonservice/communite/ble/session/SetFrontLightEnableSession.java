package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.tl7;
import com.fossil.us1;
import com.fossil.yx1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetFrontLightEnableSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ boolean isFrontLightEnable;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetFrontLightEnableState extends BleStateAbs {
        @DexIgnore
        public qy1<tl7> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetFrontLightEnableState() {
            super(SetFrontLightEnableSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            SetFrontLightEnableSession setFrontLightEnableSession = SetFrontLightEnableSession.this;
            setFrontLightEnableSession.log("Set Front Light Enable: " + SetFrontLightEnableSession.this.isFrontLightEnable);
            qy1<tl7> frontLightEnable = SetFrontLightEnableSession.this.getBleAdapter().setFrontLightEnable(SetFrontLightEnableSession.this.getLogSession(), SetFrontLightEnableSession.this.isFrontLightEnable, this);
            this.task = frontLightEnable;
            if (frontLightEnable == null) {
                SetFrontLightEnableSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetFrontLightFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            SetFrontLightEnableSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetFrontLightSuccess() {
            stopTimeout();
            SetFrontLightEnableSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<tl7> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetFrontLightEnableSession(boolean z, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_FRONT_LIGHT_ENABLE, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapter");
        this.isFrontLightEnable = z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetFrontLightEnableSession setFrontLightEnableSession = new SetFrontLightEnableSession(this.isFrontLightEnable, getBleAdapter(), getBleSessionCallback());
        setFrontLightEnableSession.setDevice(getDevice());
        return setFrontLightEnableSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_FRONT_LIGHT_ENABLE_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_FRONT_LIGHT_ENABLE_STATE;
        String name = SetFrontLightEnableState.class.getName();
        pq7.b(name, "SetFrontLightEnableState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
