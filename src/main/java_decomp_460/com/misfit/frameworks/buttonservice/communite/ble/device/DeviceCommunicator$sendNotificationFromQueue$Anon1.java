package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendNotificationFromQueue$Anon1 extends qq7 implements rp7<tl7, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$Anon1(DeviceCommunicator deviceCommunicator, NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(tl7 tl7) {
        invoke(tl7);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(tl7 tl7) {
        pq7.c(tl7, "it");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = this.this$0.getSerial();
        String tag = this.this$0.getTAG();
        remote.i(component, session, serial, tag, "Send notification: " + this.$notification.toRemoteLogString() + " Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag2 = this.this$0.getTAG();
        local.d(tag2, " .sendNotificationFromQueue() = " + this.$notification + ", result=success");
        this.this$0.getCommunicationResultCallback().onNotificationSent(this.$notification.getUid(), true);
    }
}
