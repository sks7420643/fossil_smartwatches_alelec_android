package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1", f = "BleCommunicatorAbs.kt", l = {}, m = "invokeSuspend")
public final class BleCommunicatorAbs$processQuickCommandQueue$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs$processQuickCommandQueue$Anon1(BleCommunicatorAbs bleCommunicatorAbs, qn7 qn7) {
        super(2, qn7);
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        BleCommunicatorAbs$processQuickCommandQueue$Anon1 bleCommunicatorAbs$processQuickCommandQueue$Anon1 = new BleCommunicatorAbs$processQuickCommandQueue$Anon1(this.this$0, qn7);
        bleCommunicatorAbs$processQuickCommandQueue$Anon1.p$ = (iv7) obj;
        return bleCommunicatorAbs$processQuickCommandQueue$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((BleCommunicatorAbs$processQuickCommandQueue$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            this.this$0.processQuickCommandQueue();
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
