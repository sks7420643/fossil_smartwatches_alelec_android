package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.il7;
import com.fossil.mm1;
import com.fossil.pq7;
import com.fossil.qy1;
import com.fossil.us1;
import com.fossil.ym1;
import com.fossil.yx1;
import com.fossil.zm1;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetBatteryLevelSession extends EnableMaintainingSession {
    @DexIgnore
    public int mBatteryLevel; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetBatteryLevelState extends BleStateAbs {
        @DexIgnore
        public qy1<HashMap<zm1, ym1>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetBatteryLevelState() {
            super(GetBatteryLevelSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            GetBatteryLevelSession.this.log("Get Battery Level");
            qy1<HashMap<zm1, ym1>> deviceConfig = GetBatteryLevelSession.this.getBleAdapter().getDeviceConfig(GetBatteryLevelSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                GetBatteryLevelSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(yx1 yx1) {
            pq7.c(yx1, "error");
            stopTimeout();
            GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
            getBatteryLevelSession.mBatteryLevel = getBatteryLevelSession.getBleAdapter().getBatteryLevel();
            GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
            getBatteryLevelSession2.log("Get Battery Level Failed, return the old one:" + GetBatteryLevelSession.this.mBatteryLevel);
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<zm1, ym1> hashMap) {
            pq7.c(hashMap, "deviceConfiguration");
            stopTimeout();
            if (hashMap.containsKey(zm1.BATTERY)) {
                GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
                ym1 ym1 = hashMap.get(zm1.BATTERY);
                if (ym1 != null) {
                    getBatteryLevelSession.mBatteryLevel = ((mm1) ym1).getPercentage();
                    GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
                    getBatteryLevelSession2.log("Get Battery Level Success, value=" + GetBatteryLevelSession.this.mBatteryLevel);
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
                }
            } else {
                GetBatteryLevelSession.this.log("Get Battery Level Success, but no value.");
            }
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            qy1<HashMap<zm1, ym1>> qy1 = this.task;
            if (qy1 != null) {
                us1.a(qy1);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetBatteryLevelSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.GET_BATTERY_LEVEL, bleAdapterImpl, bleSessionCallback);
        pq7.c(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt(Constants.BATTERY, this.mBatteryLevel);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        GetBatteryLevelSession getBatteryLevelSession = new GetBatteryLevelSession(getBleAdapter(), getBleSessionCallback());
        getBatteryLevelSession.setDevice(getDevice());
        return getBatteryLevelSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_BATTERY_LEVEL_STATE;
        String name = GetBatteryLevelState.class.getName();
        pq7.b(name, "GetBatteryLevelState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
