package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.fossil.tl7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1 extends qq7 implements rp7<tl7, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ BackgroundConfig $backgroundConfig$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $directory$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, BackgroundConfig backgroundConfig, String str, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$backgroundConfig$inlined = backgroundConfig;
        this.$directory$inlined = str;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(tl7 tl7) {
        invoke(tl7);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void invoke(tl7 tl7) {
        pq7.c(tl7, "it");
        this.this$0.log(this.$logSession$inlined, "Set Background Image Success");
        this.$callback$inlined.onSetBackgroundImageSuccess();
    }
}
