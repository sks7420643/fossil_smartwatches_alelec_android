package com.misfit.frameworks.buttonservice.extensions;

import android.location.Location;
import android.os.Build;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.pq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationExtensionKt {
    @DexIgnore
    public static final GpsDataPoint toGpsDataPoint(Location location) {
        pq7.c(location, "$this$toGpsDataPoint");
        return new GpsDataPoint((int) (((double) location.getTime()) / 1000.0d), location.getLongitude(), location.getLatitude(), location.getAccuracy(), (Build.VERSION.SDK_INT < 26 || !location.hasVerticalAccuracy()) ? -1.0f : location.getVerticalAccuracyMeters(), location.getAltitude(), (double) location.getSpeed(), (double) location.getBearing());
    }

    @DexIgnore
    public static final Location toLocation(GpsDataPoint gpsDataPoint) {
        pq7.c(gpsDataPoint, "$this$toLocation");
        Location location = new Location("gpsDataPoint");
        location.setLongitude(gpsDataPoint.getLongitude());
        location.setLatitude(gpsDataPoint.getLatitude());
        location.setTime((long) (gpsDataPoint.getTimestamp() * 1000));
        location.setAltitude(gpsDataPoint.getAltitude());
        location.setAccuracy(gpsDataPoint.getHorizontalAccuracy());
        if (Build.VERSION.SDK_INT >= 26) {
            location.setVerticalAccuracyMeters(gpsDataPoint.getVerticalAccuracy());
        }
        location.setSpeed((float) gpsDataPoint.getSpeed());
        location.setBearing((float) gpsDataPoint.getHeading());
        return location;
    }
}
