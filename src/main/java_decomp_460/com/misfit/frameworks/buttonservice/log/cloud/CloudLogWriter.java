package com.misfit.frameworks.buttonservice.log.cloud;

import com.fossil.kq7;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.IRemoteLogWriter;
import com.misfit.frameworks.buttonservice.log.LogApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudLogWriter implements IRemoteLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String ITEMS_PARAM; // = "_items";
    @DexIgnore
    public static /* final */ int REQUEST_SIZE; // = 500;
    @DexIgnore
    public /* final */ LogApiService mApi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public CloudLogWriter(LogApiService logApiService) {
        pq7.c(logApiService, "mApi");
        this.mApi = logApiService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteLogWriter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object sendLog(java.util.List<com.misfit.frameworks.buttonservice.log.LogEvent> r16, com.fossil.qn7<? super java.util.List<com.misfit.frameworks.buttonservice.log.LogEvent>> r17) {
        /*
        // Method dump skipped, instructions count: 226
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter.sendLog(java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
