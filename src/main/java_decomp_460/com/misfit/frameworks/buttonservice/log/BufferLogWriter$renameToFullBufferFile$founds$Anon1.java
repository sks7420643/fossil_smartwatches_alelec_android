package com.misfit.frameworks.buttonservice.log;

import com.fossil.jt7;
import com.fossil.pq7;
import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$renameToFullBufferFile$founds$Anon1 implements FileFilter {
    @DexIgnore
    public static /* final */ BufferLogWriter$renameToFullBufferFile$founds$Anon1 INSTANCE; // = new BufferLogWriter$renameToFullBufferFile$founds$Anon1();

    @DexIgnore
    public final boolean accept(File file) {
        pq7.b(file, "subFile");
        if (file.isFile()) {
            String name = file.getName();
            pq7.b(name, "subFile.name");
            if (new jt7(BufferLogWriter.FULL_BUFFER_FILE_NAME_REGEX_PATTERN).matches(name)) {
                return true;
            }
        }
        return false;
    }
}
