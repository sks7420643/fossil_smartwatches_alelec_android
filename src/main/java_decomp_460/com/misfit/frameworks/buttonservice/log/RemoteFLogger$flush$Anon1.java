package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1", f = "RemoteFLogger.kt", l = {}, m = "invokeSuspend")
public final class RemoteFLogger$flush$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$Anon1(RemoteFLogger remoteFLogger, qn7 qn7) {
        super(2, qn7);
        this.this$0 = remoteFLogger;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        RemoteFLogger$flush$Anon1 remoteFLogger$flush$Anon1 = new RemoteFLogger$flush$Anon1(this.this$0, qn7);
        remoteFLogger$flush$Anon1.p$ = (iv7) obj;
        return remoteFLogger$flush$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((RemoteFLogger$flush$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            if ((this.this$0.isMainFLogger) && !(this.this$0.isFlushing)) {
                this.this$0.isFlushing = true;
                RemoteFLogger remoteFLogger = this.this$0;
                remoteFLogger.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new Bundle());
                this.this$0.isFlushing = false;
            }
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
