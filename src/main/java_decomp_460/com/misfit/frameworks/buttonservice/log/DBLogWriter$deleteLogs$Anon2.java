package com.misfit.frameworks.buttonservice.log;

import com.fossil.ao7;
import com.fossil.el7;
import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$deleteLogs$Anon2 extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $logIds;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$deleteLogs$Anon2(DBLogWriter dBLogWriter, List list, qn7 qn7) {
        super(2, qn7);
        this.this$0 = dBLogWriter;
        this.$logIds = list;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        DBLogWriter$deleteLogs$Anon2 dBLogWriter$deleteLogs$Anon2 = new DBLogWriter$deleteLogs$Anon2(this.this$0, this.$logIds, qn7);
        dBLogWriter$deleteLogs$Anon2.p$ = (iv7) obj;
        return dBLogWriter$deleteLogs$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
        return ((DBLogWriter$deleteLogs$Anon2) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.label == 0) {
            el7.b(obj);
            int i = 0;
            for (List list : pm7.A(this.$logIds, 500)) {
                i = DBLogWriter.access$getLogDao$p(this.this$0).delete(pm7.j0(list)) + i;
            }
            DBLogWriter.IDBLogWriterCallback access$getCallback$p = DBLogWriter.access$getCallback$p(this.this$0);
            if (access$getCallback$p != null) {
                access$getCallback$p.onDeleteLogs();
            }
            return ao7.e(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
