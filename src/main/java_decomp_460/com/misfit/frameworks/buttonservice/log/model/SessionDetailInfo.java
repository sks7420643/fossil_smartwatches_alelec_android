package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.rj4;
import com.google.gson.Gson;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SessionDetailInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @rj4("battery_levels")
    public int batteryLevel;
    @DexIgnore
    @rj4(Constants.DAILY_STEPS)
    public int dailySteps;
    @DexIgnore
    @rj4("realtime_steps")
    public int realTimeSteps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SessionDetailInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SessionDetailInfo createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new SessionDetailInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SessionDetailInfo[] newArray(int i) {
            return new SessionDetailInfo[i];
        }
    }

    @DexIgnore
    public SessionDetailInfo(int i, int i2, int i3) {
        this.batteryLevel = i;
        this.realTimeSteps = i2;
        this.dailySteps = i3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SessionDetailInfo(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
        pq7.c(parcel, "parcel");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final int getDailySteps() {
        return this.dailySteps;
    }

    @DexIgnore
    public final int getRealTimeSteps() {
        return this.realTimeSteps;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setDailySteps(int i) {
        this.dailySteps = i;
    }

    @DexIgnore
    public final void setRealTimeSteps(int i) {
        this.realTimeSteps = i;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeInt(this.batteryLevel);
        parcel.writeInt(this.realTimeSteps);
        parcel.writeInt(this.dailySteps);
    }
}
