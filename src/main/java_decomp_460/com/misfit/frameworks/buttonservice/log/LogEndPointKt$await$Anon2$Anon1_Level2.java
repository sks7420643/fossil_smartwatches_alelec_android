package com.misfit.frameworks.buttonservice.log;

import com.fossil.c88;
import com.fossil.dl7;
import com.fossil.pq7;
import com.fossil.q88;
import com.fossil.qn7;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt$await$Anon2$Anon1_Level2 implements c88<T> {
    @DexIgnore
    public /* final */ /* synthetic */ qn7 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1_Level2(qn7 qn7) {
        this.$continuation = qn7;
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onFailure(Call<T> call, Throwable th) {
        pq7.c(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        qn7 qn7 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        dl7.a aVar = dl7.Companion;
        qn7.resumeWith(dl7.m1constructorimpl(create));
    }

    @DexIgnore
    @Override // com.fossil.c88
    public void onResponse(Call<T> call, q88<T> q88) {
        pq7.c(q88, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + q88);
        qn7 qn7 = this.$continuation;
        RepoResponse create = RepoResponse.Companion.create(q88);
        dl7.a aVar = dl7.Companion;
        qn7.resumeWith(dl7.m1constructorimpl(create));
    }
}
