package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.fossil.ao7;
import com.fossil.at7;
import com.fossil.bw7;
import com.fossil.eu7;
import com.fossil.gu7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.pw0;
import com.fossil.qn7;
import com.fossil.qw0;
import com.fossil.tl7;
import com.fossil.u08;
import com.fossil.w08;
import com.fossil.xw7;
import com.fossil.yn7;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.misfit.frameworks.buttonservice.log.db.Log;
import com.misfit.frameworks.buttonservice.log.db.LogDao;
import com.misfit.frameworks.buttonservice.log.db.LogDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int LIMIT_INPUT_IDS_PARAM; // = 500;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ IDBLogWriterCallback callback;
    @DexIgnore
    public /* final */ LogDao logDao;
    @DexIgnore
    public /* final */ u08 mMutex; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ int thresholdValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public interface IDBLogWriterCallback {
        @DexIgnore
        Object onDeleteLogs();  // void declaration

        @DexIgnore
        Object onReachDBThreshold();  // void declaration
    }

    /*
    static {
        String simpleName = DBLogWriter.class.getSimpleName();
        pq7.b(simpleName, "DBLogWriter::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DBLogWriter(Context context, int i, IDBLogWriterCallback iDBLogWriterCallback) {
        pq7.c(context, "context");
        this.thresholdValue = i;
        this.callback = iDBLogWriterCallback;
        qw0.a a2 = pw0.a(context, LogDatabase.class, DBConstants.LOG_DB_NAME);
        a2.f();
        this.logDao = ((LogDatabase) a2.d()).getLogDao();
    }

    @DexIgnore
    public final /* synthetic */ Object deleteLogs(List<Integer> list, qn7<? super Integer> qn7) {
        return list.isEmpty() ^ true ? eu7.g(bw7.a(), new DBLogWriter$deleteLogs$Anon2(this, list, null), qn7) : ao7.e(0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01aa A[Catch:{ all -> 0x021b }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0205  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0208  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r16, com.fossil.qn7<? super com.fossil.tl7> r17) {
        /*
        // Method dump skipped, instructions count: 580
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.DBLogWriter.flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object getAllLogsExceptSyncing(qn7<? super List<LogEvent>> qn7) {
        return eu7.g(bw7.a(), new DBLogWriter$getAllLogsExceptSyncing$Anon2(this, null), qn7);
    }

    @DexIgnore
    public final /* synthetic */ Object updateCloudFlag(List<Integer> list, Log.Flag flag, qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.a(), new DBLogWriter$updateCloudFlag$Anon2(this, list, flag, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final void writeLog(List<LogEvent> list) {
        synchronized (this) {
            pq7.c(list, "logEvents");
            this.logDao.insertLogEvent(at7.u(at7.o(pm7.z(list), DBLogWriter$writeLog$Anon1.INSTANCE)));
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new DBLogWriter$writeLog$Anon2(this, null), 3, null);
        }
    }
}
