package com.misfit.frameworks.buttonservice.log;

import okhttp3.logging.HttpLoggingInterceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPoint$init$httpLogInterceptor$Anon1 implements HttpLoggingInterceptor.b {
    @DexIgnore
    public static /* final */ LogEndPoint$init$httpLogInterceptor$Anon1 INSTANCE; // = new LogEndPoint$init$httpLogInterceptor$Anon1();

    @DexIgnore
    @Override // okhttp3.logging.HttpLoggingInterceptor.b
    public final void log(String str) {
        FLogger.INSTANCE.getLocal().d("OkHttp", str);
    }
}
