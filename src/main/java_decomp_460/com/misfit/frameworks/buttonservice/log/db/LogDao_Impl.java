package com.misfit.frameworks.buttonservice.log.db;

import android.database.Cursor;
import com.fossil.dx0;
import com.fossil.ex0;
import com.fossil.hx0;
import com.fossil.jw0;
import com.fossil.px0;
import com.fossil.qw0;
import com.fossil.tw0;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDao_Impl implements LogDao {
    @DexIgnore
    public /* final */ qw0 __db;
    @DexIgnore
    public /* final */ jw0<Log> __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ LogFlagConverter __logFlagConverter; // = new LogFlagConverter();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends jw0<Log> {
        @DexIgnore
        public Anon1(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        public void bind(px0 px0, Log log) {
            px0.bindLong(1, (long) log.getId());
            px0.bindLong(2, log.getTimeStamp());
            if (log.getContent() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, log.getContent());
            }
            String logFlagEnumToString = LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, logFlagEnumToString);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `log` (`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore
    public LogDao_Impl(qw0 qw0) {
        this.__db = qw0;
        this.__insertionAdapterOfLog = new Anon1(qw0);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int countExcept(Log.Flag flag) {
        int i = 0;
        tw0 f = tw0.f("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                i = b.getInt(0);
            }
            return i;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int delete(List<Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder b = hx0.b();
        b.append("DELETE FROM log WHERE id IN (");
        hx0.a(b, list.size());
        b.append(")");
        px0 compileStatement = this.__db.compileStatement(b.toString());
        int i = 1;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public List<Log> getAllLogEventsExcept(Log.Flag flag) {
        tw0 f = tw0.f("SELECT * FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = ex0.b(this.__db, f, false, null);
        try {
            int c = dx0.c(b, "id");
            int c2 = dx0.c(b, "timeStamp");
            int c3 = dx0.c(b, "content");
            int c4 = dx0.c(b, "cloudFlag");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                Log log = new Log(b.getLong(c2), b.getString(c3), this.__logFlagConverter.stringToLogFlag(b.getString(c4)));
                log.setId(b.getInt(c));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void insertLogEvent(List<Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void updateCloudFlagByIds(List<Integer> list, Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder b = hx0.b();
        b.append("UPDATE log SET cloudFlag = ");
        b.append("?");
        b.append(" WHERE id IN (");
        hx0.a(b, list.size());
        b.append(")");
        px0 compileStatement = this.__db.compileStatement(b.toString());
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.bindNull(1);
        } else {
            compileStatement.bindString(1, logFlagEnumToString);
        }
        int i = 2;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
