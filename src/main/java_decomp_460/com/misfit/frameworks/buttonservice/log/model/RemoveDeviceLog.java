package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.pq7;
import com.fossil.rj4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoveDeviceLog {
    @DexIgnore
    @rj4("current_device")
    public String currentDevice;

    @DexIgnore
    public RemoveDeviceLog(String str) {
        pq7.c(str, "currentDevice");
        this.currentDevice = str;
    }

    @DexIgnore
    public final String getCurrentDevice() {
        return this.currentDevice;
    }

    @DexIgnore
    public final void setCurrentDevice(String str) {
        pq7.c(str, "<set-?>");
        this.currentDevice = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        pq7.b(t, "Gson().toJson(this)");
        return t;
    }
}
