package com.misfit.frameworks.buttonservice.log;

import com.fossil.jj4;
import com.fossil.kj4;
import com.fossil.lj4;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FLogUtils$getGsonForLogEvent$Anon1<T> implements lj4<Long> {
    @DexIgnore
    public static /* final */ FLogUtils$getGsonForLogEvent$Anon1 INSTANCE; // = new FLogUtils$getGsonForLogEvent$Anon1();

    @DexIgnore
    public final jj4 serialize(Long l, Type type, kj4 kj4) {
        return new jj4((Number) new BigDecimal(((double) l.longValue()) / ((double) 1000)).setScale(6, RoundingMode.HALF_UP));
    }
}
