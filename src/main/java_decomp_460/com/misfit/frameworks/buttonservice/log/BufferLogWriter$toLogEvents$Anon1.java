package com.misfit.frameworks.buttonservice.log;

import com.fossil.pq7;
import com.fossil.qq7;
import com.fossil.rp7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$toLogEvents$Anon1 extends qq7 implements rp7<String, LogEvent> {
    @DexIgnore
    public /* final */ /* synthetic */ Gson $gson;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$toLogEvents$Anon1(Gson gson) {
        super(1);
        this.$gson = gson;
    }

    @DexIgnore
    public final LogEvent invoke(String str) {
        pq7.c(str, "it");
        try {
            return (LogEvent) this.$gson.k(str, LogEvent.class);
        } catch (Exception e) {
            return null;
        }
    }
}
