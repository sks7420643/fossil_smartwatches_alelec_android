package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.bw7;
import com.fossil.gu7;
import com.fossil.hw0;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.qw0;
import com.fossil.xw7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class LogDatabase extends qw0 {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LogDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public abstract LogDao getLogDao();

    @DexIgnore
    @Override // com.fossil.qw0
    public void init(hw0 hw0) {
        pq7.c(hw0, "configuration");
        super.init(hw0);
        try {
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new LogDatabase$init$Anon1(this, null), 3, null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when init database " + e);
        }
    }
}
