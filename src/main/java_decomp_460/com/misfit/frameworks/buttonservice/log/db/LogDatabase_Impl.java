package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDatabase_Impl extends LogDatabase {
    @DexIgnore
    public volatile LogDao _logDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends sw0.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `log` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timeStamp` INTEGER NOT NULL, `content` TEXT NOT NULL, `cloudFlag` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1e76ae5948872f78452bd16b18a13dc4')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `log`");
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) LogDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) LogDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            LogDatabase_Impl.this.mDatabase = lx0;
            LogDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) LogDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("timeStamp", new ix0.a("timeStamp", "INTEGER", true, 0, null, 1));
            hashMap.put("content", new ix0.a("content", "TEXT", true, 0, null, 1));
            hashMap.put("cloudFlag", new ix0.a("cloudFlag", "TEXT", true, 0, null, 1));
            ix0 ix0 = new ix0("log", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "log");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "log(com.misfit.frameworks.buttonservice.log.db.Log).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `log`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "log");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new Anon1(1), "1e76ae5948872f78452bd16b18a13dc4", "3ef7199be598e6814da70cf3eed15c90");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDatabase
    public LogDao getLogDao() {
        LogDao logDao;
        if (this._logDao != null) {
            return this._logDao;
        }
        synchronized (this) {
            if (this._logDao == null) {
                this._logDao = new LogDao_Impl(this);
            }
            logDao = this._logDao;
        }
        return logDao;
    }
}
