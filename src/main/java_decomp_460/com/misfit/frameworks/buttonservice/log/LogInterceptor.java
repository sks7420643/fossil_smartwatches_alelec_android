package com.misfit.frameworks.buttonservice.log;

import com.fossil.pq7;
import com.fossil.r18;
import com.fossil.t18;
import com.fossil.v18;
import com.fossil.w18;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.zendesk.sdk.network.Constants;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Calendar;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogInterceptor implements Interceptor {
    @DexIgnore
    public /* final */ String accessKey;
    @DexIgnore
    public /* final */ String logBrandName;
    @DexIgnore
    public /* final */ String secretKey;

    @DexIgnore
    public LogInterceptor(String str, String str2, String str3) {
        pq7.c(str, "logBrandName");
        pq7.c(str2, "accessKey");
        pq7.c(str3, "secretKey");
        this.logBrandName = str;
        this.accessKey = str2;
        this.secretKey = str3;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        pq7.c(chain, "chain");
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        long timeInMillis = instance.getTimeInMillis() / ((long) 1000);
        v18 c = chain.c();
        v18.a h = c.h();
        h.e("X-Cyc-Brand", this.logBrandName);
        h.e("X-Cyc-Timestamp", String.valueOf(timeInMillis));
        h.e("X-Cyc-Auth-Method", "signature");
        h.e("X-Cyc-Access-Key-Id", this.accessKey);
        StringBuilder sb = new StringBuilder();
        sb.append("Signature=");
        ConversionUtils conversionUtils = ConversionUtils.INSTANCE;
        sb.append(conversionUtils.SHA1(timeInMillis + this.secretKey));
        h.e("Authorization", sb.toString());
        h.e("Content-Type", Constants.APPLICATION_JSON);
        h.g(c.g(), c.a());
        v18 b = h.b();
        try {
            Response d = chain.d(b);
            pq7.b(d, "chain.proceed(request)");
            return d;
        } catch (Exception e) {
            ServerError serverError = e instanceof ServerErrorException ? ((ServerErrorException) e).getServerError() : e instanceof UnknownHostException ? new ServerError(601, "") : e instanceof SocketTimeoutException ? new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "") : new ServerError(600, "");
            Response.a aVar = new Response.a();
            aVar.p(b);
            aVar.n(t18.HTTP_1_1);
            Integer code = serverError.getCode();
            pq7.b(code, "serverError.code");
            aVar.g(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.k(message);
            aVar.b(w18.create(r18.d(Constants.APPLICATION_JSON), new Gson().t(serverError)));
            aVar.a("Content-Type", com.misfit.frameworks.common.constants.Constants.CONTENT_TYPE);
            Response c2 = aVar.c();
            pq7.b(c2, "okhttp3.Response.Builder\u2026                 .build()");
            return c2;
        }
    }
}
