package com.misfit.frameworks.buttonservice.log;

import com.fossil.eo7;
import com.fossil.iv7;
import com.fossil.ko7;
import com.fossil.pq7;
import com.fossil.qn7;
import com.fossil.tl7;
import com.fossil.vp7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$1", f = "BufferLogWriter.kt", l = {191, 80, 99, 104, 109}, m = "invokeSuspend")
public final class BufferLogWriter$pollEventQueue$Anon1 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1(BufferLogWriter bufferLogWriter, qn7 qn7) {
        super(2, qn7);
        this.this$0 = bufferLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1 = new BufferLogWriter$pollEventQueue$Anon1(this.this$0, qn7);
        bufferLogWriter$pollEventQueue$Anon1.p$ = (iv7) obj;
        return bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        return ((BufferLogWriter$pollEventQueue$Anon1) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:106:0x022c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x022d, code lost:
        com.fossil.so7.a(r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0230, code lost:
        throw r1;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0072 A[Catch:{ Exception -> 0x0231, all -> 0x0259 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x012c A[Catch:{ all -> 0x022c }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018b  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 616
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
