package com.misfit.frameworks.buttonservice.log;

import com.fossil.at7;
import com.fossil.bw7;
import com.fossil.em7;
import com.fossil.gu7;
import com.fossil.hr7;
import com.fossil.iv7;
import com.fossil.jv7;
import com.fossil.kq7;
import com.fossil.pm7;
import com.fossil.pq7;
import com.fossil.u08;
import com.fossil.ux7;
import com.fossil.vt7;
import com.fossil.w08;
import com.fossil.xw7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ u08 mBufferLogMutex; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ iv7 mBufferLogScope; // = jv7.a(bw7.b().plus(ux7.b(null, 1, null)));
    @DexIgnore
    public /* final */ int threshold;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        pq7.b(name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        pq7.c(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        hr7 hr7 = hr7.f1520a;
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{this.floggerName, Long.valueOf(j), Locale.US}, 3));
        pq7.b(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final xw7 pollEventQueue() {
        return gu7.d(this.mBufferLogScope, null, null, new BufferLogWriter$pollEventQueue$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return at7.u(at7.p(pm7.z(list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    @DexIgnore
    public final List<File> exportLogs() {
        List<File> f0;
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        return (listFiles == null || (f0 = em7.f0(listFiles)) == null) ? new ArrayList() : f0;
    }

    @DexIgnore
    public final void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    @DexIgnore
    public final float getLogFileSize() {
        return ConversionUtils.INSTANCE.convertByteToMegaBytes(new File(this.logFilePath).length());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0088, code lost:
        if (r0 != null) goto L_0x008a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object renameToFullBufferFile(java.io.File r11, boolean r12, com.fossil.qn7<? super com.fossil.tl7> r13) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.BufferLogWriter.renameToFullBufferFile(java.io.File, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        pq7.c(str, "directoryPath");
        pq7.c(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        String str3 = str + File.separatorChar + LOG_FOLDER;
        this.directoryPath = str3;
        if (!vt7.l(str3)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.directoryPath);
            sb.append(File.separatorChar);
            hr7 hr7 = hr7.f1520a;
            String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(new Object[]{this.floggerName, Locale.US}, 2));
            pq7.b(format, "java.lang.String.format(format, *args)");
            sb.append(format);
            str2 = sb.toString();
        } else {
            str2 = "";
        }
        this.logFilePath = str2;
        this.isMainFLogger = z;
        pollEventQueue();
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        pq7.c(str, "directoryPath");
        pq7.c(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }

    @DexIgnore
    public final void writeLog(LogEvent logEvent) {
        pq7.c(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        pollEventQueue();
    }
}
