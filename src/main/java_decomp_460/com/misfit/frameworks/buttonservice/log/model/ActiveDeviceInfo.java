package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.wearables.fsl.location.DeviceLocation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveDeviceInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String deviceModel;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public String fwVersion;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActiveDeviceInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActiveDeviceInfo createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ActiveDeviceInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActiveDeviceInfo[] newArray(int i) {
            return new ActiveDeviceInfo[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActiveDeviceInfo(android.os.Parcel r5) {
        /*
            r4 = this;
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r5, r0)
            java.lang.String r0 = r5.readString()
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x001d
        L_0x000d:
            java.lang.String r1 = r5.readString()
            if (r1 == 0) goto L_0x0020
        L_0x0013:
            java.lang.String r2 = r5.readString()
            if (r2 == 0) goto L_0x0023
        L_0x0019:
            r4.<init>(r0, r1, r2)
            return
        L_0x001d:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x0020:
            java.lang.String r1 = ""
            goto L_0x0013
        L_0x0023:
            r2 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ActiveDeviceInfo(String str, String str2, String str3) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        pq7.c(str2, "deviceModel");
        pq7.c(str3, "fwVersion");
        this.deviceSerial = str;
        this.deviceModel = str2;
        this.fwVersion = str3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final String getFwVersion() {
        return this.fwVersion;
    }

    @DexIgnore
    public final void setDeviceSerial(String str) {
        pq7.c(str, "<set-?>");
        this.deviceSerial = str;
    }

    @DexIgnore
    public final void setFwVersion(String str) {
        pq7.c(str, "<set-?>");
        this.fwVersion = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.deviceSerial);
        parcel.writeString(this.deviceModel);
        parcel.writeString(this.fwVersion);
    }
}
