package com.misfit.frameworks.buttonservice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import com.fossil.il7;
import com.fossil.kq7;
import com.fossil.pq7;
import com.fossil.yk7;
import com.fossil.zk0;
import com.fossil.zk7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE; // = "com.fossil.wearables.fossil.channel.defaultService";
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE_NAME; // = "Sync service";
    @DexIgnore
    public static /* final */ int DEVICE_STATUS_NOTIFICATION_ID; // = 1;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ yk7 instance$delegate; // = zk7.a(NotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static zk0.e mNotificationBuilder;
    @DexIgnore
    public String mLastContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final NotificationUtils getInstance() {
            yk7 yk7 = NotificationUtils.instance$delegate;
            Companion companion = NotificationUtils.Companion;
            return (NotificationUtils) yk7.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Singleton {
        @DexIgnore
        public static /* final */ Singleton INSTANCE; // = new Singleton();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ NotificationUtils f0INSTANCE; // = new NotificationUtils(null);

        @DexIgnore
        public final NotificationUtils getINSTANCE() {
            return f0INSTANCE;
        }
    }

    /*
    static {
        String simpleName = NotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        pq7.b(simpleName, "NotificationUtils::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public NotificationUtils() {
        this.mLastContent = "";
    }

    @DexIgnore
    public /* synthetic */ NotificationUtils(kq7 kq7) {
        this();
    }

    @DexIgnore
    private final Notification createDeviceStatusNotification(Context context, String str) {
        ArrayList<zk0.a> arrayList;
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new zk0.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        zk0.e eVar = mNotificationBuilder;
        if (!(eVar == null || (arrayList = eVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            zk0.e eVar2 = mNotificationBuilder;
            if (eVar2 != null) {
                eVar2.n(context.getResources().getString(R.string.app_name));
                eVar2.i(Constants.SERVICE);
                eVar2.v(false);
                eVar2.m(str);
                eVar2.A(null);
                eVar2.y(R.drawable.ic_launcher_transparent);
                eVar2.x(false);
                eVar2.w(-2);
                Notification c = eVar2.c();
                pq7.b(c, "mNotificationBuilder!!\n \u2026                 .build()");
                return c;
            }
            pq7.i();
            throw null;
        }
        zk0.e eVar3 = mNotificationBuilder;
        if (eVar3 != null) {
            eVar3.y(R.drawable.ic_launcher_transparent);
            eVar3.i(Constants.SERVICE);
            eVar3.n(str);
            eVar3.A(null);
            eVar3.v(false);
            eVar3.x(false);
            eVar3.w(-2);
            Notification c2 = eVar3.c();
            pq7.b(c2, "builder.build()");
            return c2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    private final Notification createDeviceStatusWithRichTextNotification(Context context, String str, String str2) {
        ArrayList<zk0.a> arrayList;
        if (!(str2.length() == 0)) {
            this.mLastContent = str2;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new zk0.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        zk0.e eVar = mNotificationBuilder;
        if (!(eVar == null || (arrayList = eVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            zk0.e eVar2 = mNotificationBuilder;
            if (eVar2 != null) {
                eVar2.n(context.getResources().getString(R.string.app_name));
                eVar2.i(Constants.SERVICE);
                zk0.c cVar = new zk0.c();
                cVar.g(str2);
                eVar2.A(cVar);
                eVar2.v(false);
                eVar2.m(str);
                eVar2.y(R.drawable.ic_launcher_transparent);
                eVar2.x(false);
                eVar2.w(-2);
                Notification c = eVar2.c();
                pq7.b(c, "mNotificationBuilder!!\n \u2026                 .build()");
                return c;
            }
            pq7.i();
            throw null;
        }
        zk0.e eVar3 = mNotificationBuilder;
        if (eVar3 != null) {
            eVar3.y(R.drawable.ic_launcher_transparent);
            eVar3.i(Constants.SERVICE);
            zk0.c cVar2 = new zk0.c();
            cVar2.g(str2);
            eVar3.A(cVar2);
            eVar3.n(str);
            eVar3.v(false);
            eVar3.x(false);
            eVar3.w(-2);
            Notification c2 = eVar3.c();
            pq7.b(c2, "builder.build()");
            return c2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public static final NotificationUtils getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    private final void riseNotification(NotificationManager notificationManager, int i, Notification notification) {
        try {
            notificationManager.notify(i, notification);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    public static /* synthetic */ void updateNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        PendingIntent pendingIntent3 = null;
        PendingIntent pendingIntent4 = (i2 & 8) != 0 ? null : pendingIntent;
        if ((i2 & 16) == 0) {
            pendingIntent3 = pendingIntent2;
        }
        notificationUtils.updateNotification(context, i, str, pendingIntent4, pendingIntent3, z);
    }

    @DexIgnore
    public final Notification createDefaultDeviceStatusNotification(Context context, String str, String str2) {
        pq7.c(context, "context");
        pq7.c(str, "content");
        pq7.c(str2, "subText");
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        zk0.e eVar = new zk0.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        ArrayList<zk0.a> arrayList = eVar.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            eVar.n(context.getResources().getString(R.string.app_name));
            eVar.m(str2);
            eVar.y(R.drawable.ic_launcher_transparent);
            eVar.i(Constants.SERVICE);
            eVar.v(false);
            eVar.x(false);
            eVar.A(null);
            eVar.w(-2);
            Notification c = eVar.c();
            pq7.b(c, "builder\n                \u2026                 .build()");
            return c;
        }
        eVar.y(R.drawable.ic_launcher_transparent);
        eVar.i(Constants.SERVICE);
        eVar.n(str2);
        eVar.A(null);
        eVar.v(false);
        eVar.x(false);
        eVar.w(-2);
        Notification c2 = eVar.c();
        pq7.b(c2, "builder\n                \u2026                 .build()");
        return c2;
    }

    @DexIgnore
    public final NotificationManager createNotificationChannels(Context context) {
        pq7.c(context, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "createNotificationChannels - context=" + context);
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "createNotificationChannels - currentChannel=" + notificationChannel);
                if (notificationChannel == null) {
                    NotificationChannel notificationChannel2 = new NotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE, DEFAULT_CHANNEL_WATCH_SERVICE_NAME, 1);
                    notificationChannel2.setShowBadge(false);
                    notificationManager.createNotificationChannel(notificationChannel2);
                }
            }
            return notificationManager;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void startForegroundNotification(Context context, Service service, String str, String str2, boolean z) {
        pq7.c(context, "context");
        pq7.c(service, Constants.SERVICE);
        pq7.c(str, "content");
        pq7.c(str2, "subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "Service Tracking - startForegroundNotification - context=" + context + ", service=" + service + ", content=" + str + ", subText=" + str2 + ", isStopForeground = " + z);
        createNotificationChannels(context);
        service.startForeground(1, createDefaultDeviceStatusNotification(context, str, str2));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Service Tracking - startForegroundNotification done bring to foreground for service ");
        sb.append(service);
        local2.d(str4, sb.toString());
        if (z) {
            FLogger.INSTANCE.getLocal().d(TAG, "Service Tracking - startForegroundNotification() - stop foreground service and remove notification");
            service.stopForeground(true);
        }
    }

    @DexIgnore
    public final void updateNotification(Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        pq7.c(context, "context");
        pq7.c(str, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "updateNotification - context=" + context + ", content=" + str + " notificationId=" + i + ", isReset=" + z);
        if (!(str.length() == 0) || z) {
            this.mLastContent = str;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusNotification(context, str));
    }

    @DexIgnore
    public final void updateRichTextNotification(Context context, int i, String str, String str2, boolean z) {
        pq7.c(context, "context");
        pq7.c(str, "title");
        pq7.c(str2, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "updateNotification - context=" + context + ", title=" + str + ", content=" + str2 + ", notificationId=" + i + ", isReset=" + z);
        if ((str2.length() > 0) || z) {
            this.mLastContent = str2;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusWithRichTextNotification(context, str, str2));
    }
}
