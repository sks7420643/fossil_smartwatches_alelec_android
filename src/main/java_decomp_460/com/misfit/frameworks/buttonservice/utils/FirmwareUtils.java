package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.facebook.internal.Utility;
import com.fossil.i68;
import com.fossil.il7;
import com.fossil.pq7;
import com.fossil.z68;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FirmwareUtils {
    @DexIgnore
    public static /* final */ FirmwareUtils INSTANCE; // = new FirmwareUtils();
    @DexIgnore
    public static /* final */ String TAG; // = TAG;

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        String str = "";
        for (byte b : bArr) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            String num = Integer.toString((b & 255) + 256, 16);
            pq7.b(num, "Integer.toString((input[\u2026() and 0xff) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                pq7.b(substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                str = sb.toString();
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        if (str != null) {
            String lowerCase = str.toLowerCase();
            pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean isLatestFirmware(String str, String str2) {
        pq7.c(str, "oldFw");
        pq7.c(str2, "latestFw");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return true;
        }
        return new z68(str2).compareTo(new z68(str)) >= 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] readFirmware(com.misfit.frameworks.buttonservice.model.FirmwareData r6, android.content.Context r7) {
        /*
            r5 = this;
            java.lang.String r0 = "firmwareData"
            com.fossil.pq7.c(r6, r0)
            java.lang.String r0 = "context"
            com.fossil.pq7.c(r7, r0)
            java.lang.String r0 = r6.getFirmwareVersion()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x004a
            boolean r1 = r6.isEmbedded()
            if (r1 == 0) goto L_0x004c
            android.content.res.Resources r0 = r7.getResources()     // Catch:{ Exception -> 0x002d }
            int r1 = r6.getRawBundleResource()     // Catch:{ Exception -> 0x002d }
            java.io.InputStream r0 = r0.openRawResource(r1)     // Catch:{ Exception -> 0x002d }
            byte[] r0 = com.fossil.b68.g(r0)     // Catch:{ Exception -> 0x002d }
        L_0x002a:
            if (r0 == 0) goto L_0x0057
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.misfit.frameworks.buttonservice.utils.FirmwareUtils.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "readFirmware() - e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
        L_0x004a:
            r0 = 0
            goto L_0x002a
        L_0x004c:
            com.misfit.frameworks.buttonservice.source.Injection r1 = com.misfit.frameworks.buttonservice.source.Injection.INSTANCE
            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r1 = r1.provideFilesRepository(r7)
            byte[] r0 = r1.readFirmware(r0)
            goto L_0x002a
        L_0x0057:
            r0 = 0
            byte[] r0 = new byte[r0]
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.utils.FirmwareUtils.readFirmware(com.misfit.frameworks.buttonservice.model.FirmwareData, android.content.Context):byte[]");
    }

    @DexIgnore
    public final boolean verifyFirmware(byte[] bArr, String str) {
        pq7.c(str, "checksum");
        if (!i68.b(str) && bArr != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                instance.update(bArr);
                byte[] digest = instance.digest();
                pq7.b(digest, "md5");
                String bytesToString = bytesToString(digest);
                String lowerCase = str.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return pq7.a(lowerCase, bytesToString);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.e(str2, "Error inside " + TAG + ".verifyFirmware - e=" + e);
            }
        }
        return false;
    }
}
