package com.misfit.frameworks.buttonservice;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.pq7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonService$bondChangedReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (bluetoothDevice != null) {
            switch (intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1)) {
                case 10:
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = ButtonService.TAG;
                    local.d(str, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: NONE");
                    return;
                case 11:
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = ButtonService.TAG;
                    local2.d(str2, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDING");
                    return;
                case 12:
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = ButtonService.TAG;
                    local3.d(str3, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDED");
                    return;
                default:
                    return;
            }
        }
    }
}
