package com.misfit.frameworks.common.model.Cucumber;

import android.util.Log;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CucumberSessionInfo {
    @DexIgnore
    public float averageRPM;
    @DexIgnore
    public float averageSpeed;
    @DexIgnore
    public int calories;
    @DexIgnore
    public JSONArray charts;
    @DexIgnore
    public float distance;
    @DexIgnore
    public int duration;
    @DexIgnore
    public float elevationGain;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public JSONArray maps;
    @DexIgnore
    public String name;
    @DexIgnore
    public CucumberPauseResume[] pauseResume;
    @DexIgnore
    public CucumberRegion region;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public String thumbnailMapContent;
    @DexIgnore
    public String thumbnailMapLink;
    @DexIgnore
    public int tzOffset;
    @DexIgnore
    public long udpatedTime;

    @DexIgnore
    public float getAverageRPM() {
        return this.averageRPM;
    }

    @DexIgnore
    public float getAverageSpeed() {
        return this.averageSpeed;
    }

    @DexIgnore
    public int getCalories() {
        return this.calories;
    }

    @DexIgnore
    public JSONArray getCharts() {
        return this.charts;
    }

    @DexIgnore
    public float getDistance() {
        return this.distance;
    }

    @DexIgnore
    public int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public float getElevationGain() {
        return this.elevationGain;
    }

    @DexIgnore
    public long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public JSONArray getMaps() {
        return this.maps;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public CucumberPauseResume[] getPauseResume() {
        return this.pauseResume;
    }

    @DexIgnore
    public String getPauseResumeString() {
        String str = "";
        int i = 0;
        while (true) {
            CucumberPauseResume[] cucumberPauseResumeArr = this.pauseResume;
            if (i >= cucumberPauseResumeArr.length) {
                return str;
            }
            CucumberPauseResume cucumberPauseResume = cucumberPauseResumeArr[i];
            str = str + String.format("(%d|%d)", Long.valueOf(cucumberPauseResume.getStartTS() * 1000), Long.valueOf(cucumberPauseResume.getEndTS() * 1000));
            i++;
        }
    }

    @DexIgnore
    public CucumberRegion getRegion() {
        return this.region;
    }

    @DexIgnore
    public long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public String getThumbnailMapContent() {
        return this.thumbnailMapContent;
    }

    @DexIgnore
    public String getThumbnailMapLink() {
        return this.thumbnailMapLink;
    }

    @DexIgnore
    public int getTzOffset() {
        return this.tzOffset;
    }

    @DexIgnore
    public long getUdpatedTime() {
        return this.udpatedTime;
    }

    @DexIgnore
    public void setAverageRPM(float f) {
        this.averageRPM = f;
    }

    @DexIgnore
    public void setAverageSpeed(float f) {
        this.averageSpeed = f;
    }

    @DexIgnore
    public void setCalories(int i) {
        this.calories = i;
    }

    @DexIgnore
    public void setCharts(JSONArray jSONArray) {
        this.charts = jSONArray;
    }

    @DexIgnore
    public void setDistance(float f) {
        this.distance = f;
    }

    @DexIgnore
    public void setDuration(int i) {
        this.duration = i;
    }

    @DexIgnore
    public void setElevationGain(float f) {
        this.elevationGain = f;
    }

    @DexIgnore
    public void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setMaps(JSONArray jSONArray) {
        this.maps = jSONArray;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setPauseResume(CucumberPauseResume[] cucumberPauseResumeArr) {
        this.pauseResume = cucumberPauseResumeArr;
    }

    @DexIgnore
    public void setRegion(CucumberRegion cucumberRegion) {
        this.region = cucumberRegion;
    }

    @DexIgnore
    public void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public void setThumbnailMapContent(String str) {
        this.thumbnailMapContent = str;
    }

    @DexIgnore
    public void setThumbnailMapLink(String str) {
        this.thumbnailMapLink = str;
    }

    @DexIgnore
    public void setTzOffset(int i) {
        this.tzOffset = i;
    }

    @DexIgnore
    public void setUdpatedTime(long j) {
        this.udpatedTime = j;
    }

    @DexIgnore
    public JSONObject toJson() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("name", this.name);
            jSONObject.put(Constants.AVERAGE_SPEED, (double) this.averageSpeed);
            jSONObject.put("distance", (double) this.distance);
            jSONObject.put("calories", this.calories);
            jSONObject.put(Constants.AVERAGE_RPM, (double) this.averageRPM);
            jSONObject.put("duration", this.duration);
            jSONObject.put(Constants.START_TIME, this.startTime);
            jSONObject.put(Constants.END_TIME, this.endTime);
            jSONObject.put(Constants.UPDATED_TIME, this.udpatedTime);
            jSONObject.put(Constants.ELEVATION_GAIN, (double) this.elevationGain);
            jSONObject.put(Constants.THUMBNAIL_MAP_CONTENT, this.thumbnailMapContent);
            jSONObject.put(Constants.REGION, this.region.toJson());
            JSONArray jSONArray = new JSONArray();
            for (CucumberPauseResume cucumberPauseResume : this.pauseResume) {
                jSONArray.put(cucumberPauseResume.toJson());
            }
            jSONObject.put(Constants.PAUSE_RESUME, jSONArray);
            return jSONObject;
        } catch (Exception e) {
            Log.d("ss,", "Error " + e);
            return null;
        }
    }
}
