package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.r68;
import com.fossil.v68;
import com.fossil.w68;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFHeader;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MFMultipartRequest extends MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFMultipartRequest";
    @DexIgnore
    public r68 multipartEntity;

    @DexIgnore
    public MFMultipartRequest(Context context) {
        super(context);
    }

    @DexIgnore
    private void appendFormDataToEntity(String str, r68 r68) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                r68.a(next, new w68(jSONObject.get(next).toString()));
            }
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.d(str2, "Exception when appenFormDataToEntity " + e);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    public void buildHeader(MFHeader mFHeader) {
        HashMap<String, String> headerMap;
        if (!(mFHeader == null || (headerMap = this.configuration.getHeader().getHeaderMap()) == null)) {
            for (String str : headerMap.keySet()) {
                String str2 = headerMap.get(str);
                if (!TextUtils.isEmpty(str2)) {
                    this.httpURLConnection.setRequestProperty(str, str2);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    public void buildRequest() {
        this.configuration = initConfiguration();
        this.jsonData = initJsonData();
        this.method = initHttpMethod();
        this.apiMethod = initApiMethod();
        this.buttonApiResponse = initResponse();
        JSONObject initUploadFileUrl = initUploadFileUrl();
        Uri parse = Uri.parse(this.configuration.getBaseServerUrl() + this.apiMethod);
        r68 r68 = new r68();
        try {
            if (this.jsonData != null) {
                if (this.jsonData instanceof JSONObject) {
                    JSONObject jSONObject = (JSONObject) this.jsonData;
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        appendFormDataToEntity(jSONObject.get(keys.next()).toString(), r68);
                    }
                } else {
                    throw new Exception("For MultipartRequest, jsonData must be an object");
                }
            }
            if (initUploadFileUrl != null) {
                Iterator<String> keys2 = initUploadFileUrl.keys();
                while (keys2.hasNext()) {
                    String next = keys2.next();
                    r68.a(next, new v68(new File(initUploadFileUrl.get(next).toString()), "image/jpeg"));
                }
            }
            URL url = new URL(parse.toString());
            this.url = url;
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            this.httpURLConnection = httpURLConnection;
            httpURLConnection.setDoOutput(true);
            this.httpURLConnection.setRequestMethod("POST");
            buildHeader(this.configuration.getHeader());
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error when build request multipart " + e);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a4 A[SYNTHETIC, Splitter:B:27:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01c1 A[SYNTHETIC, Splitter:B:61:0x01c1] */
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.network.responses.MFResponse execute() {
        /*
        // Method dump skipped, instructions count: 493
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.network.request.MFMultipartRequest.execute():com.misfit.frameworks.network.responses.MFResponse");
    }

    @DexIgnore
    public abstract JSONObject initUploadFileUrl();
}
